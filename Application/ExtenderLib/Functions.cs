using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Text;


namespace Riskmaster.Application.ExtenderLib
{
	/// <summary> 
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,August 2004
	///Purpose :   Contains generic Functions to be used in the Library
	/// </summary>
	public class Functions
	{
		#region Variable Declarations
		
		/// <summary>
		/// Private string variable for Connection String
		/// </summary>
		private string m_sConnectString="";
        private int m_iClientId = 0;//sharishkumar Jira 827
		#endregion

		#region Constructors
		/// <summary>
		/// Constructor for Functions class. 
		/// </summary>
		/// <param name="p_sConnectionstring">
		/// Connection string, to be used to connect to the underlying RM Database.
		/// </param>
        public Functions(string p_sConnectString, int p_iClientId)//sharishkumar Jira 827
		{
			m_sConnectString = p_sConnectString;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
		}

		#endregion

		#region Methods
		/// <summary>Get Org's Parent based on Department ID</summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_iDepartID">Department ID</param>
		/// <param name="p_iTableID">Table ID</param>
		/// <returns>Org's Parent</returns>
        internal static int GetOrgParentByDepartID(string p_sConnectString, int p_iDepartID, int p_iTableID, int p_iClientId)//sharishkumar Jira 827
		{
			string sFieldName="";
			DbReader objRdr=null;
			int iEID= 0;
			try
			{
				sFieldName=Conversion.EntityTableIdToOrgTableName(p_iTableID);
				if(sFieldName == "")
					sFieldName = "CLIENT";
				objRdr=DbFactory.GetDbReader(p_sConnectString ,"SELECT "+ sFieldName + "_EID " +
					" FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + p_iDepartID);
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iEID= objRdr.GetInt32(0); 
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("Functions.GetOrgParentByDepartID.DataError", p_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
			return iEID;
		}

	
		/// <summary>Gets FROI_OPTIONS</summary>
		/// <param name="p_iDeptAssignedEID">DEPARTMENT_EID</param>		
		/// <param name="p_iJurisRowID">JURIS_ROW_ID</param>		
		/// <returns>FROI_PREPARER</returns>
        internal int GetFROIPreparerSource(int p_iDeptAssignedEID, int p_iJurisRowID, int p_iClientId)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			int iFROIPreparer= 1; 
			StringBuilder sbOrgs = null;
			StringBuilder sbSQL = null;
			try
			{
				sbOrgs = new StringBuilder(p_iDeptAssignedEID.ToString());
				sbSQL = new StringBuilder();

				//1,2,3,4 are only valid values, 1 is default (WorkStation User)
				if (p_iDeptAssignedEID<=0) return iFROIPreparer;

				// Build list of org. parents based on this department 
				for(short iCtr=1005;iCtr<=1011;iCtr++)
				{
					sbOrgs.Append (",");
					sbOrgs.Append (GetOrgParentByDepartID(m_sConnectString, p_iDeptAssignedEID,iCtr,p_iClientId));//sharishkumar Jira 827
				}

				//With a JURIS_ROW_ID there are four different ways to get to the preparer option
				sbSQL.Append ("SELECT FROI_OPTIONS.FROI_PREPARER ,ENTITY.ENTITY_TABLE_ID FROM FROI_OPTIONS,ENTITY WHERE FROI_OPTIONS.SELECTED_EID IN (");
				sbSQL.Append (sbOrgs.ToString() ) ;
                sbSQL.Append (") AND FROI_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID AND FROI_OPTIONS.JURIS_ROW_ID = ");
				sbSQL.Append (p_iJurisRowID);
				sbSQL.Append (" ORDER BY ENTITY.ENTITY_TABLE_ID DESC");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString ());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				//In case no records found proceed with the next query
				sbSQL.Remove(0,sbSQL.Length );
				sbSQL.Append("SELECT FROI_OPTIONS.FROI_PREPARER, ENTITY.ENTITY_TABLE_ID FROM FROI_OPTIONS,ENTITY WHERE FROI_OPTIONS.SELECTED_EID IN (");
				sbSQL.Append(sbOrgs.ToString());
				sbSQL.Append(") AND FROI_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID AND FROI_OPTIONS.JURIS_ROW_ID = -1");
				sbSQL.Append(" ORDER BY ENTITY.ENTITY_TABLE_ID DESC");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString ());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				//In case no records found proceed with the next query
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT FROI_OPTIONS.FROI_PREPARER FROM FROI_OPTIONS WHERE FROI_OPTIONS.SELECTED_EID = -1 AND FROI_OPTIONS.JURIS_ROW_ID = "+ p_iJurisRowID );
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}
				if(objRdr!=null)objRdr.Dispose();


				//In case no records found proceed with the next query
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT FROI_OPTIONS.FROI_PREPARER FROM FROI_OPTIONS WHERE FROI_OPTIONS.SELECTED_EID = -1 AND FROI_OPTIONS.JURIS_ROW_ID = -1");
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}

			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("Functions.GetFROIPreparerSource.DataErr",p_iClientId) , p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
				sbOrgs =null;
			}
			return iFROIPreparer;
		}


		/// <summary>Gets UNIT_ROW_ID for a particular UNIT_ID , CLAIM_ID from UNIT_X_CLAIM table</summary>
		/// <param name="p_sConnectString">Connection String</param>	
		/// <param name="p_iUnitID">UNIT_ID</param>	
		/// <param name="p_iClaimID">CLAIM_ID</param>	
		/// <returns>UNIT_ROW_ID</returns>
        internal static int GetUnitXClaimRowID(string p_sConnectString, int p_iUnitID, int p_iClaimID, int p_iClientId)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			int iUnitRowID= 0;
			try
			{
				objRdr=DbFactory.GetDbReader(p_sConnectString , "SELECT UNIT_ROW_ID FROM UNIT_X_CLAIM WHERE UNIT_ID = " + 
					p_iUnitID + " AND CLAIM_ID = " + p_iClaimID);
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iUnitRowID= objRdr.GetInt32(0); 
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("Functions.GetUnitXClaimRowID.DataError", p_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
			return iUnitRowID;
		}


		/// <summary>Gets EVENT_ID from PERSON_INVOLVED</summary>
		/// <param name="p_sConnectString">Connection String</param>	
		/// <param name="p_iPIRowID">PI_ROW_ID from PERSON_INVOLVED table</param>	
		/// <returns>EVENT_ID</returns>
        internal static int GetEventID_PersonInvolved(string p_sConnectString, int p_iPIRowID, int p_iClientId)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			int iEventID= 0;
			try
			{
				objRdr=DbFactory.GetDbReader(p_sConnectString , "SELECT EVENT_ID FROM PERSON_INVOLVED WHERE PI_ROW_ID = " + p_iPIRowID);
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iEventID= objRdr.GetInt32(0); 
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("Functions.GetEventID_PersonInvolved.DataError", p_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
			return iEventID;
		}


		/// <summary>Gets JURIS_OPTIONS</summary>
		/// <param name="p_iDeptAssignedEID">DEPARTMENT_EID</param>		
		/// <param name="p_iJurisRowID">JURIS_ROW_ID</param>		
		/// <returns>FROI_PREPARER</returns>
        internal int GetJURISPreparerSource(int p_iDeptAssignedEID, int p_iJurisRowID, int p_iClientId)//sharishkumar Jira 827
		{
			DbReader objRdr=null;
			int iFROIPreparer= 1; 
			StringBuilder sbOrgs = null;
			StringBuilder sbSQL = null;
			try
			{
				sbOrgs = new StringBuilder(p_iDeptAssignedEID.ToString());
				sbSQL = new StringBuilder();

				//1,2,3,4 are only valid values, 1 is default (WorkStation User)
				if (p_iDeptAssignedEID<=0) return iFROIPreparer;

				// Build list of org. parents based on this department 
				for(short iCtr=1005;iCtr<=1011;iCtr++)
				{
					sbOrgs.Append (",");
                    sbOrgs.Append(GetOrgParentByDepartID(m_sConnectString, p_iDeptAssignedEID, iCtr, p_iClientId));//sharishkumar Jira 827
				}

				//With a JURIS_ROW_ID there are four different ways to get to the preparer option
				sbSQL.Append ("SELECT JURIS_OPTIONS.FROI_PREPARER ,ENTITY.ENTITY_TABLE_ID FROM JURIS_OPTIONS,ENTITY WHERE JURIS_OPTIONS.SELECTED_EID IN (");
				sbSQL.Append (sbOrgs.ToString() ) ;
				sbSQL.Append (") AND JURIS_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID AND JURIS_OPTIONS.JURIS_ROW_ID = ");
				sbSQL.Append (p_iJurisRowID);
				sbSQL.Append (" ORDER BY ENTITY.ENTITY_TABLE_ID DESC");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString ());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				//In case no records found proceed with the next query
				sbSQL.Remove(0,sbSQL.Length );
				sbSQL.Append("SELECT JURIS_OPTIONS.FROI_PREPARER, ENTITY.ENTITY_TABLE_ID FROM JURIS_OPTIONS,ENTITY WHERE JURIS_OPTIONS.SELECTED_EID IN (");
				sbSQL.Append(sbOrgs.ToString());
				sbSQL.Append(") AND JURIS_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID AND JURIS_OPTIONS.JURIS_ROW_ID = -1");
				sbSQL.Append(" ORDER BY ENTITY.ENTITY_TABLE_ID DESC");

				objRdr=DbFactory.GetDbReader(m_sConnectString ,sbSQL.ToString ());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				//In case no records found proceed with the next query
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT JURIS_OPTIONS.FROI_PREPARER FROM JURIS_OPTIONS WHERE JURIS_OPTIONS.SELECTED_EID = -1 AND JURIS_OPTIONS.JURIS_ROW_ID = "+ p_iJurisRowID );
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}
				if(objRdr!=null)objRdr.Dispose();

				//In case no records found proceed with the next query
				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT JURIS_OPTIONS.FROI_PREPARER FROM JURIS_OPTIONS WHERE JURIS_OPTIONS.SELECTED_EID = -1 AND JURIS_OPTIONS.JURIS_ROW_ID = -1");
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						iFROIPreparer= objRdr.GetInt32("FROI_PREPARER"); 
						return iFROIPreparer;
					}
				}

			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("Functions.GetJURISPreparerSource.DataError", p_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();

				sbSQL=null;
				sbOrgs =null;
			}
			return iFROIPreparer;
		}

		#endregion

	}// end class
}// end namespace