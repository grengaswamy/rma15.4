using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common ;
using System.Data; 
using System.Text; 
using System.Xml; 


namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Layer to the JURIS_OPTIONS
	/// </summary>
	public class JURISOptionsMajor:OptionsMajor 
	{

		#region Variable Declarations

		/// <summary>Private variable for JURISPreparer property</summary>
		private int m_iJURISPreparer=0;

		/// <summary>Private variable for TPAEID property</summary>
		private int m_iTPAEID=0;

		/// <summary>Private variable for BaseOrgHierarchy property</summary>
		private int m_iBaseOrgHierarchy=0;

		/// <summary>Private variable for DTTMRcdLastUpd property</summary>
		private string m_sDTTMRcdLastUpd="";

		/// <summary>Private variable for DTTMRcdAdded property</summary>
		private string m_sDTTMRcdAdded="";

		/// <summary>Private variable for UpdateByUser property</summary>
		private string m_sUpdateByUser="";

		/// <summary>Private variable for AddedByUser property</summary>
		private string m_sAddedByUser="";

		/// <summary>Private variable for AttachToClaimByDefault_JURIS property</summary>
		private int m_iAttachToClaimByDefault_JURIS=0;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor maps to base class constructor. Loads Instance data.
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_sUserLogin">User Login Name</param>
		/// <param name="p_iJurisRowID">Juris Row ID</param>
		/// <param name="p_iSelectedEID">Selected EID</param>
        public JURISOptionsMajor(string p_sConnectString, string p_sUserLogin, int p_iJurisRowID, int p_iSelectedEID, int p_iClientId) :
            base(p_sConnectString, p_sUserLogin, p_iClientId)//sharishkumar Jira 827
		{
			m_sConnectString=p_sConnectString;
			m_iJurisRowID = p_iJurisRowID;
			m_iSelectedEID=p_iSelectedEID;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
			//Load data to the instance
			LoadData(p_iJurisRowID, p_iSelectedEID);
		}

		/// <summary>
		/// Default constructor. Does not populate instance data
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
		/// <param name="p_sUserLogin">User Login</param>
        public JURISOptionsMajor(string p_sConnectString, string p_sUserLogin, string p_sPassword, string p_sDsnName, int p_iClientId) :
            base(p_sConnectString, p_sUserLogin, p_sPassword, p_sDsnName, p_iClientId) { }//sharishkumar Jira 827

		/// <summary>
		/// Default Constructor
		/// </summary>
		public JURISOptionsMajor(){}

		#endregion

		#region Public Properties
		/// <summary>Property JURISPreparer</summary>		
		public int JURISPreparer
		{
			get
			{
				return m_iJURISPreparer;
			}	
			set
			{
				if(m_iJURISPreparer!=value)
				{
					m_bDataHasChanged=true; 
					m_iJURISPreparer=value;
				}
			}
		}

		/// <summary>Property TPAEID</summary>	
		public int TPAEID
		{
			get
			{
				return m_iTPAEID;
			}	
			set
			{
				if(m_iTPAEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iTPAEID=value;
				}
			}
		}


		/// <summary>Property BaseOrgHierarchy</summary>	
		public int BaseOrgHierarchy
		{
			get
			{
				return m_iBaseOrgHierarchy;
			}	
			set
			{
				if(m_iBaseOrgHierarchy!=value)
				{
					m_bDataHasChanged=true; 
					m_iBaseOrgHierarchy=value;
				}
			}
		}

		/// <summary>Property DTTMRcdLastUpd</summary>	
		public string DTTMRcdLastUpd
		{
			get
			{
				return m_sDTTMRcdLastUpd;
			}	
			set
			{
				if(m_sDTTMRcdLastUpd!=value)
				{
					m_bDataHasChanged=true; 
					m_sDTTMRcdLastUpd=value;
				}
			}
		}

		/// <summary>Property DTTMRcdAdded</summary>	
		public string DTTMRcdAdded
		{
			get
			{
				return m_sDTTMRcdAdded;
			}	
			set
			{
				if(m_sDTTMRcdAdded!=value)
				{
					m_bDataHasChanged=true; 
					m_sDTTMRcdAdded=value;
				}
			}
		}

		/// <summary>Property UpdateByUser</summary>	
		public string UpdateByUser
		{
			get
			{
				return m_sUpdateByUser;
			}	
			set
			{
				if(m_sUpdateByUser!=value)
				{
					m_bDataHasChanged=true; 
					m_sUpdateByUser=value;
				}
			}
		}

		/// <summary>Property UpdateByUser</summary>	
		public string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}	
			set
			{
				if(m_sAddedByUser!=value)
				{
					m_bDataHasChanged=true; 
					m_sAddedByUser=value;
				}
			}
		}

		/// <summary>Property AttachToClaimByDefault_JURIS</summary>	
		public int AttachToClaimByDefault_JURIS
		{
			get
			{
				return m_iAttachToClaimByDefault_JURIS;
			}	
			set
			{
				if(m_iAttachToClaimByDefault_JURIS!=value)
				{
					m_bDataHasChanged=true; 
					m_iAttachToClaimByDefault_JURIS=value;
				}
			}
		}

		/// Following properties were moved from the base class OptionsMajor to here 
		/// while doing FROI Integration
		/// <summary>Property CarrierOrgHierarchyLevel</summary>		
		public int CarrierOrgHierarchyLevel
		{
			get
			{
				return m_iCarrierOrgHierarchyLevel;
			}	
			set
			{
				if(m_iCarrierOrgHierarchyLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iCarrierOrgHierarchyLevel=value;
				}
			}
		}

		/// <summary>Property ClaimAdminOrgHierarchyLevel</summary>		
		public int ClaimAdminOrgHierarchyLevel
		{
			get
			{
				return m_iClaimAdminOrgHierarchyLevel;
			}	
			set
			{
				if(m_iClaimAdminOrgHierarchyLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminOrgHierarchyLevel=value;
				}
			}
		}

		/// <summary>Property DataChanged</summary>		
		public bool DataHasChanged
		{
			get
			{
				return m_bDataHasChanged;
			}	
		}

		/// <summary>Property VirginiaParentLevel</summary>		
		public int VirginiaParentLevel
		{
			get
			{
				return m_iVirginiaParentLevel;
			}	
			set
			{
				if(m_iVirginiaParentLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iVirginiaParentLevel=value;
				}
			}
		}

		/// <summary>Property ClaimAdminDefaultEID</summary>		
		public int ClaimAdminDefaultEID
		{
			get
			{
				return m_iClaimAdminDefaultEID;
			}	
			set
			{
				if(m_iClaimAdminDefaultEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminDefaultEID=value;
				}
			}
		}

		/// <summary>Property CarrierDefaultEID</summary>		
		public int CarrierDefaultEID
		{
			get
			{
				return m_iCarrierDefaultEID;
			}	
			set
			{
				if(m_iCarrierDefaultEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iCarrierDefaultEID=value;
				}
			}
		}

		/// <summary>Property CarrierDefaultEID</summary>		
		public int CarrierOption
		{
			get
			{
				return m_iCarrierOption ;
			}	
			set
			{
				if(m_iCarrierOption!=value)
				{
					m_bDataHasChanged=true; 
					m_iCarrierOption=value;
				}
			}
		}

		
		/// <summary>Property ClaimAdminOption</summary>		
		public int ClaimAdminOption
		{
			get
			{
				return m_iClaimAdminOption;
			}	
			set
			{
				if(m_iCarrierDefaultEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminOption=value;
				}
			}
		}

		/// <summary>Property PrintOSHADesc</summary>		
		public int PrintOSHADesc
		{
			get
			{
				return m_iPrintOSHADesc;
			}	
			set
			{
				if(m_iPrintOSHADesc!=value)
				{
					m_bDataHasChanged=true; 
					m_iPrintOSHADesc=value;
				}
			}
		}

		/// <summary>Property EmployerLevel</summary>		
		public int EmployerLevel
		{
			get
			{
				return m_iEmployerLevel;
			}	
			set
			{
				if(m_iEmployerLevel!=value)
				{
					m_bDataHasChanged=true; 
					m_iEmployerLevel=value;
				}
			}
		}

		/// <summary>Property SelectedEID</summary>		
		public int SelectedEID
		{
			get
			{
				return m_iSelectedEID;
			}	
			set
			{
				if(m_iSelectedEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iSelectedEID=value;
				}
			}
		}


		/// <summary>Property ClaimAdminTPAEID</summary>	
		public int ClaimAdminTPAEID
		{
			get
			{
				return m_iClaimAdminTPAEID;
			}	
			set
			{
				if(m_iClaimAdminTPAEID!=value)
				{
					m_bDataHasChanged=true; 
					m_iClaimAdminTPAEID=value;
				}
			}
		}


		/// <summary>Property JurisRowId</summary>		
		public int JurisRowId
		{
			get
			{
				return m_iJurisRowID;
			}	
			set
			{
				if(m_iJurisRowID!=value)
				{
					m_bDataHasChanged=true; 
					m_iJurisRowID=value;
				}
			}
		}

        /// <summary>
        /// Gets the carrier claim number option.
        /// </summary>
        /// <value>
        /// The carrier claim number option.Added for Froi Migration- Mits 33585,33586,33587
        /// </value>
        public int CarrierClaimNumberOption
        {
            get
            {
                return base.m_CarrierClaimNumberOption;
            }
        }


		#endregion

		#region Methods

		/// <summary>Reset all internal variables</summary>
		public override bool ClearObject()
		{
			base.ClearObject(); 
			m_iBaseOrgHierarchy = -1;
			m_iJURISPreparer=-1;
			m_iVirginiaParentLevel = -1;
			m_iAttachToClaimByDefault_JURIS = -1;
			m_sDTTMRcdLastUpd = "";
			m_sDTTMRcdAdded = "";
			m_sUpdateByUser = "";
			m_sAddedByUser = "";
			m_iClaimAdminTPAEID = 0;
			m_iTPAEID = 0;
			return true;
		}

	/// <summary>
	/// Deletes data row from JURIS_OPTIONS
	/// </summary>
	/// <param name="p_iJurisRowID">JURIS_ROW_ID</param>
	/// <param name="p_iSelectedEID">SELECTED_EID</param>
	/// <returns></returns>
		public bool DeleteData(int p_iJurisRowID, int p_iSelectedEID)
		{
			DbConnection objConn = null; 
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectString );
				objConn.Open();
				objConn.ExecuteNonQuery("DELETE FROM JURIS_OPTIONS WHERE SELECTED_EID = " + p_iSelectedEID + " AND JURIS_ROW_ID = " + p_iJurisRowID);
				return true;
			}
			catch (Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("JURISOptionsMajor.DeleteData.DataError",m_iClientId),p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Dispose();
				}
			}
		}


		/// <summary>
		/// Loads Data from Database
		/// </summary>
		/// <param name="p_iJurisRowID">JURIS ROW ID</param>
		/// <param name="p_iSelectedEID">Selected EID</param>
		/// <returns>Boolean</returns>
		private bool LoadData(int p_iJurisRowID, int p_iSelectedEID)
		{
			DbReader objRdr=null;	
			DataRow[] arrRows=null;		
			DataTable objDtTbl=null;
			bool bDataFound=false;
			try
			{
				if (!ClearObject()){ return false;}
				switch(p_iSelectedEID)
				{
					case 0:
						if (!LoadDataFromSysParms()){ return false;}
						break;
					default:			
						objRdr=DbFactory.GetDbReader(m_sConnectString   ,"SELECT * FROM JURIS_OPTIONS " + 
							" WHERE SELECTED_EID = " + p_iSelectedEID + " AND JURIS_ROW_ID = "+ p_iJurisRowID );
						objDtTbl=objRdr.GetSchemaTable();
						if (objRdr!=null)
						{					
							if(objRdr.Read())
							{
								bDataFound=true;								
								m_bDataHasChanged=false;
								m_iBaseOrgHierarchy =objRdr.GetInt32 ("BASE_LEVEL");
								m_iCarrierOrgHierarchyLevel  =objRdr.GetInt32 ("CARRIER_ORG_LEVEL");
								m_iClaimAdminOrgHierarchyLevel   =objRdr.GetInt32 ("CLMADMIN_ORG_LEVEL");
								m_iSelectedEID=objRdr.GetInt32 ("SELECTED_EID");
								m_iEmployerLevel=objRdr.GetInt32 ("EMPLOYER_LEVEL");
								m_iPrintOSHADesc=objRdr.GetInt32 ("PRINT_OSHA_DESC");
								m_iJURISPreparer=objRdr.GetInt32 ("FROI_PREPARER");
								m_iClaimAdminOption =objRdr.GetInt32 ("PRINT_CLAIM_ADMIN");
								m_iCarrierOption =objRdr.GetInt32 ("USE_DEF_CARRIER");
								m_iCarrierDefaultEID =objRdr.GetInt32 ("DEF_CARRIER_EID");
								m_iClaimAdminDefaultEID =objRdr.GetInt32 ("DEF_CLAM_ADMIN_EID");
								m_iVirginiaParentLevel =objRdr.GetInt32 ("VIRGIN_PARNT_LEVEL");
								m_iAttachToClaimByDefault_JURIS =objRdr.GetInt32 ("ATTACH_TO_CLAIM");
								m_sDTTMRcdLastUpd=objRdr.GetString ("DTTM_RCD_LAST_UPD");
								m_sDTTMRcdAdded=objRdr.GetString("DTTM_RCD_ADDED");
								m_sUpdateByUser =objRdr.GetString("UPDATED_BY_USER");
								m_sAddedByUser=objRdr.GetString("ADDED_BY_USER");
								m_iJurisRowID= objRdr.GetInt32 ("JURIS_ROW_ID");

								arrRows = objDtTbl.Select("ColumnName='CLAIMADMIN_TPA_EID'");
								if(arrRows.GetLength(0)==1)
								{
									m_iClaimAdminTPAEID =objRdr.GetInt32 ("CLAIMADMIN_TPA_EID");
									m_iTPAEID =objRdr.GetInt32 ("TPA_EID");
								}
								else
								{
									m_iClaimAdminTPAEID =0;
									m_iTPAEID =0;
								}
							}
							if (!bDataFound )
							{
								if (!LoadDataFromSysParms() ) return false;
							}
						}
						break;
				}						
			}		
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("JURISOptionsMajor.LoadData.DataError",m_iClientId) , p_objExp);	//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if(objDtTbl!=null)objDtTbl.Dispose();
				arrRows=null;
			}
			return true;	
		}


        //	XML Input string format for SaveData
		//		<JURIS>
		//		<JURISPreparer>1</JURISPreparer>
		//		<TPAEID>2</TPAEID>
		//		<BaseOrgHierarchy>3</BaseOrgHierarchy>
		//		<AttachToClaimByDefaultJURIS>4</AttachToClaimByDefaultJURIS>
		//		<CarrierOrgHierarchyLevel>5</CarrierOrgHierarchyLevel>
		//		<ClaimAdminOrgHierarchyLevel>6</ClaimAdminOrgHierarchyLevel>
		//		<VirginiaParentLevel>7</VirginiaParentLevel>
		//		<ClaimAdminDefaultEID>8</ClaimAdminDefaultEID>
		//		<CarrierDefaultEID>9</CarrierDefaultEID>
		//		<CarrierOption>10</CarrierOption>
		//		<ClaimAdminOption>11</ClaimAdminOption>
		//		<PrintOSHADesc>12</PrintOSHADesc>
		//		<EmployerLevel>13</EmployerLevel>
		//		<ClaimAdminTPAEID>14</ClaimAdminTPAEID>
		//		</JURIS>
		/// Name		: SaveData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 08/24/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment			*		Author
		/// 01/17/2004		Modified with xml input			Pankaj
		///************************************************************
		/// <summary>
		/// Save the Object data to the database
		/// </summary>
		/// <param name="p_iSelectedEID">SELECTED_EID</param>
		/// <param name="p_iJurisRowID">JURIS_ROW_ID</param>
		/// <param name="p_sInputData">Input data for saving as xml string</param>
		/// <returns>Boolean success</returns>
		public bool SaveData(int p_iSelectedEID, int p_iJurisRowID, string p_sInputData)
		{
			DataTable  objDtTbl=null;
			DataRow[] arrRows=null;
			DbWriter objWriter=null;
			DbReader objRdr=null;
			XmlDocument objXmlDoc=null;
			XmlNodeList objJURISNodLst=null;
			XmlElement objElem = null;
			try
			{
				if ((p_iSelectedEID <-2 )||(p_iSelectedEID ==0 ))
                    throw new RMAppException(Globalization.GetString("JURISOptionsMajor.SaveData.InvalidSelectId", m_iClientId));//sharishkumar Jira 827
				try
				{	
					//Blank Input string is allowed for default record
					if (p_sInputData.Trim()!="")
					{
						//Load the Xml Documents
						objXmlDoc =  new XmlDocument();
						objXmlDoc.LoadXml(p_sInputData);					
						objJURISNodLst =  objXmlDoc.GetElementsByTagName("JURIS");
						objElem = (XmlElement)objJURISNodLst.Item(0); 
						//Populate the member variables
						m_iJURISPreparer = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("JURISPreparer")[0].InnerXml);  
						m_iTPAEID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TPAEID")[0].InnerXml);  
						m_iBaseOrgHierarchy = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("BaseOrgHierarchy")[0].InnerXml);
						m_iAttachToClaimByDefault_JURIS = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("AttachToClaimByDefaultJURIS")[0].InnerXml);
						m_iCarrierOrgHierarchyLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("CarrierOrgHierarchyLevel")[0].InnerXml);
						m_iClaimAdminOrgHierarchyLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminOrgHierarchyLevel")[0].InnerXml);
						m_iVirginiaParentLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("VirginiaParentLevel")[0].InnerXml);
						m_iClaimAdminDefaultEID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminDefaultEID")[0].InnerXml);
						m_iCarrierDefaultEID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("CarrierDefaultEID")[0].InnerXml);
						m_iCarrierOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("CarrierOption")[0].InnerXml);
						m_iClaimAdminOption = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminOption")[0].InnerXml);
						m_iPrintOSHADesc = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("PrintOSHADesc")[0].InnerXml);
						m_iEmployerLevel = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("EmployerLevel")[0].InnerXml);
						m_iClaimAdminTPAEID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("ClaimAdminTPAEID")[0].InnerXml);
					}
				}
				catch(Exception p_objEx)
				{
                    throw new RMAppException(Globalization.GetString("JURISOptionsMajor.SaveData.XmlErr", m_iClientId), p_objEx);//sharishkumar Jira 827
				}

					objWriter=DbFactory.GetDbWriter(m_sConnectString);
					objWriter.Tables.Add("JURIS_OPTIONS");
					objRdr=DbFactory.GetDbReader  (m_sConnectString,  "SELECT * FROM JURIS_OPTIONS " + 
						" WHERE SELECTED_EID = "+ p_iSelectedEID + " AND JURIS_ROW_ID = " +p_iJurisRowID);
					objDtTbl=objRdr.GetSchemaTable();
					if(objRdr!=null)
					{
						if(objRdr.Read())  
						{
							// Existing Record, add filter for the update query.
							objWriter.Where.Add(" SELECTED_EID=" + p_iSelectedEID + " AND JURIS_ROW_ID=" + p_iJurisRowID);
						}
						else
						{
							objWriter.Fields.Add("ADDED_BY_USER", m_sUserLoginName );						
							objWriter.Fields.Add("DTTM_RCD_ADDED",  DateTime.Now.ToString("yyyyMMddHHmmss") );
						}
						objRdr.Close();
					}						
					objWriter.Fields.Add("BASE_LEVEL",  m_iBaseOrgHierarchy );
					objWriter.Fields.Add("CLMADMIN_ORG_LEVEL", m_iClaimAdminOrgHierarchyLevel);
					objWriter.Fields.Add("SELECTED_EID", p_iSelectedEID);
					objWriter.Fields.Add("EMPLOYER_LEVEL", m_iEmployerLevel);
					objWriter.Fields.Add("PRINT_OSHA_DESC", m_iPrintOSHADesc);
					objWriter.Fields.Add("FROI_PREPARER", m_iFROIPreparer);
					objWriter.Fields.Add("PRINT_CLAIM_ADMIN", m_iClaimAdminOption);
					objWriter.Fields.Add("USE_DEF_CARRIER", m_iCarrierOption);
					objWriter.Fields.Add("DEF_CARRIER_EID", m_iCarrierDefaultEID);
					objWriter.Fields.Add("DEF_CLAM_ADMIN_EID", m_iClaimAdminDefaultEID);
					objWriter.Fields.Add("VIRGIN_PARNT_LEVEL", m_iVirginiaParentLevel);
					objWriter.Fields.Add("ATTACH_TO_CLAIM", m_iAttachToClaimByDefault_JURIS );
					objWriter.Fields.Add("DTTM_RCD_LAST_UPD", DateTime.Now.ToString("yyyyMMddHHmmss"));
					objWriter.Fields.Add("UPDATED_BY_USER", m_sUserLoginName );
					objWriter.Fields.Add("JURIS_ROW_ID", p_iJurisRowID  );
					objWriter.Fields.Add("CARRIER_ORG_LEVEL", m_iCarrierOrgHierarchyLevel);

					arrRows = objDtTbl.Select("ColumnName='CLAIMADMIN_TPA_EID'");
					if(arrRows.GetLength(0)==1)
					{
						objWriter.Fields.Add("CLAIMADMIN_TPA_EID", Convert.ToInt32(m_iClaimAdminTPAEID ));
						objWriter.Fields.Add("TPA_EID", m_iTPAEID);
					}
                objWriter.Execute();
				m_bDataHasChanged=false;					
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("JURISOptionsMajor.SaveData.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				objDtTbl=null;
				arrRows=null;			
				objWriter=null;
				objXmlDoc=null;
				objJURISNodLst=null;
				objElem = null;
			}
			return true;
		}


		/// <summary>
		/// Load JURIS_OPTIONS
		/// </summary>
		/// <param name="p_iAssignedDept">Department ID</param>
		/// <returns>Boolean</returns>
        internal bool LoadDataByAssignedDepartment(int p_iAssignedDept)//sharishkumar Jira 827
		{
			StringBuilder sbOrgs = null;
			StringBuilder sbSQL = null;
			DbReader objRdr = null;
			try
			{
				sbOrgs= new StringBuilder();
				sbSQL = new StringBuilder();
				// the case of lAssignedDept<-1 has been brought to the following statement
				//-1 is the default record in FROI_OPTIONS. 'something very wrong
				if ( !ClearObject()|| p_iAssignedDept<-1)return false;

				switch(p_iAssignedDept)
				{
					case 0:
						//as of 07/15/2002 there is no provision for a valid client id of 0
						return false;
					default:
						sbOrgs.Append(p_iAssignedDept.ToString() ) ;
						for(int iCtr =1005; iCtr <= 1011; iCtr++)
						{
							sbOrgs.Append (",");
							sbOrgs.Append (Functions.GetOrgParentByDepartID(m_sConnectString,  p_iAssignedDept,iCtr,m_iClientId) ); //sharishkumar Jira 827
						}

						sbSQL.Append ("SELECT JURIS_OPTIONS.*,ENTITY.ENTITY_TABLE_ID FROM JURIS_OPTIONS,ENTITY");
						sbSQL.Append (" WHERE JURIS_OPTIONS.SELECTED_EID IN (") ;
						sbSQL.Append(sbOrgs.ToString());
						sbSQL.Append(") AND JURIS_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID");
						sbSQL.Append(" AND JURIS_OPTIONS.JURIS_ROW_ID = ") ;
						sbSQL.Append(m_iJurisRowID);
						sbSQL.Append(" ORDER BY ENTITY.ENTITY_TABLE_ID DESC");

						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{					
							if (objRdr.Read())
							{
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						sbSQL.Remove(0,sbSQL.Length );
						sbSQL.Append ("SELECT JURIS_OPTIONS.*,ENTITY.ENTITY_TABLE_ID FROM JURIS_OPTIONS,ENTITY");
						sbSQL.Append (" WHERE JURIS_OPTIONS.SELECTED_EID IN (");
						sbSQL.Append (sbOrgs.ToString());
						sbSQL.Append (") AND JURIS_OPTIONS.SELECTED_EID = ENTITY.ENTITY_ID");
						sbSQL.Append (" AND JURIS_OPTIONS.JURIS_ROW_ID = -1 ORDER BY ENTITY.ENTITY_TABLE_ID DESC");
						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{	
							if (objRdr.Read())
							{
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						sbSQL.Remove(0,sbSQL.Length );
						sbSQL.Append ("SELECT JURIS_OPTIONS.* FROM JURIS_OPTIONS WHERE JURIS_OPTIONS.SELECTED_EID = -1");
						sbSQL.Append (" AND JURIS_OPTIONS.JURIS_ROW_ID = ");
						sbSQL.Append (m_iJurisRowID );
						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{	
							if (objRdr.Read())
							{
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						sbSQL.Remove(0,sbSQL.Length );
						sbSQL.Append ("SELECT JURIS_OPTIONS.* FROM JURIS_OPTIONS");
						sbSQL.Append (" WHERE JURIS_OPTIONS.SELECTED_EID = -1");
						sbSQL.Append (" AND JURIS_OPTIONS.JURIS_ROW_ID = -1");
						objRdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString() );
						if (objRdr!=null)
						{	
							if (objRdr.Read())
							{				
								AssignData(objRdr);
								return true;
							}
						}
						if(objRdr!=null)objRdr.Dispose();

						//If we have gotten to here, there is no valid JURIS options record.
						//Create a default value record and use it.
						if (CreateDefaultRecord()!=-1)
							return false;
						else
							return true;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("JURISOptionsMajor.LoadDataByAssignedDepartment.DataLoadError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
				sbOrgs =null;
			}
		}

		/// <summary>Creates Default Record in case of no FROI Options</summary>
		private int CreateDefaultRecord()
		{
			DbReader objRdr=null;
			JURISOptionsMajor objJuris =null;
			DbReader objRdrEnt =null;
			try
			{
				objJuris =new JURISOptionsMajor (m_sConnectString,m_sUserLoginName , m_iJurisRowID,m_iSelectedEID,m_iClientId);//sharishkumar Jira 827

				objRdr=DbFactory.GetDbReader(m_sConnectString ,"SELECT PRINT_CLAIMS_ADMIN,PRINT_OSHA_DESC," + 
						" HIERARCHY_LEVEL, USE_DEF_CARRIER, FROI_PREPARER FROM SYS_PARMS");
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						objJuris.m_iEmployerLevel = Conversion.GetOrgHierarchyLevel(objRdr.GetString("HIERARCHY_LEVEL") ) ;
						objJuris.m_iFROIPreparer = objRdr.GetInt32("FROI_PREPARER");
						objJuris.m_iClaimAdminOption = objRdr.GetInt32("PRINT_CLAIMS_ADMIN");
						if (objJuris.m_iClaimAdminOption ==4)
						{
							objRdrEnt=DbFactory.GetDbReader(m_sConnectString ,"SELECT * FROM ENTITY, " + 
								" GLOSSARY WHERE ((ENTITY.ENTITY_TABLE_ID = GLOSSARY.TABLE_ID) AND " + 
								"(GLOSSARY.SYSTEM_TABLE_NAME = 'WC_DEF_CLAIM_ADMIN')) AND ENTITY.DELETED_FLAG = 0");
							if (objRdrEnt!=null )
							{
								if (objRdrEnt.Read())
								{
									objJuris.m_iClaimAdminDefaultEID =objRdrEnt.GetInt32("ENTITY_ID");                                    
								}
							}
						}
						objJuris.m_iPrintOSHADesc = objRdr.GetInt32("PRINT_OSHA_DESC");
						objJuris.CarrierOption   =3;
						objJuris.m_iSelectedEID =-1;
						objJuris.m_iJurisRowID=-1;
					}
				}
				objJuris.m_iJurisRowID =-1;
				if (objJuris.SaveData(-1,-1,""))
					return -1;
				else 
					return 0;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OptionsMajor.CreateDefaultRecord.AssigError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if(objRdrEnt!=null)	objRdrEnt.Dispose();
				objJuris=null;
			}
		}

		/// <summary>Assigns values to the instance variables</summary>
		/// <param name="p_objRdr">DbReader</param>
		private void AssignData(DbReader p_objRdr)
		{
			DataRow[] arrRows=null;
			DataTable objDtTbl=null;
			try
			{						
						m_iBaseOrgHierarchy= p_objRdr.GetInt32("BASE_LEVEL");
						m_iCarrierOrgHierarchyLevel= p_objRdr.GetInt32("CARRIER_ORG_LEVEL");
						m_iClaimAdminOrgHierarchyLevel= p_objRdr.GetInt32("CLMADMIN_ORG_LEVEL");
						m_bDataHasChanged= false;
						m_iSelectedEID= p_objRdr.GetInt32("SELECTED_EID");
						m_iEmployerLevel= p_objRdr.GetInt32("EMPLOYER_LEVEL");
						m_iPrintOSHADesc= p_objRdr.GetInt32("PRINT_OSHA_DESC");
                        //caggarwal4 MITS 36830 (Preparer information is not pulled in on state forms)
						m_iJURISPreparer= p_objRdr.GetInt32("FROI_PREPARER");
						m_iClaimAdminOption= p_objRdr.GetInt32("PRINT_CLAIM_ADMIN");
						m_iCarrierOption= p_objRdr.GetInt32("USE_DEF_CARRIER");
						m_iCarrierDefaultEID= p_objRdr.GetInt32("DEF_CARRIER_EID");
						m_iClaimAdminDefaultEID= p_objRdr.GetInt32("DEF_CLAM_ADMIN_EID");
						m_iVirginiaParentLevel= p_objRdr.GetInt32("VIRGIN_PARNT_LEVEL");
						m_iAttachToClaimByDefault_JURIS = p_objRdr.GetInt32("ATTACH_TO_CLAIM");
						m_sDTTMRcdLastUpd = p_objRdr.GetString ("DTTM_RCD_LAST_UPD");
						m_sDTTMRcdAdded = p_objRdr.GetString ("DTTM_RCD_ADDED");
						m_sUpdateByUser = p_objRdr.GetString ("UPDATED_BY_USER");
						m_sAddedByUser = p_objRdr.GetString ("ADDED_BY_USER");
						m_iJurisRowID = p_objRdr.GetInt32("JURIS_ROW_ID") ;
						objDtTbl=p_objRdr.GetSchemaTable();
						arrRows = objDtTbl.Select("ColumnName='CLAIMADMIN_TPA_EID'");
						if(arrRows.GetLength(0)==1)
						{
							m_iClaimAdminTPAEID = p_objRdr.GetInt32("CLAIMADMIN_TPA_EID");
							m_iTPAEID = p_objRdr.GetInt32("TPA_EID");
						}
						else
						{
							m_iClaimAdminTPAEID = 0;
							m_iTPAEID = 0;
						}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("JURISOptionsMajor.AssignData.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				arrRows=null;
				objDtTbl=null;
			}
		}

		#endregion

	} //Class End
} //Namespace End
