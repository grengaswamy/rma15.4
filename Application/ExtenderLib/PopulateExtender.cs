using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel ;
using System.Text ;
using System.Collections; 

namespace Riskmaster.Application.ExtenderLib
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   24th,Aug 2004
	///Purpose :   Contains common function to populate the extender objects
	/// </summary>
	public class PopulateExtender :IDisposable
	{
		
		#region Variable Declaration

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		protected string m_sUserName="";

		/// <summary>
		/// Private variable to store password
		/// </summary>
		protected string m_sPassword="";

		/// <summary>
		/// Private variable to store dsn
		/// </summary>
		protected string m_sDsnName="";

		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		protected string m_sConnectString="";
        protected  int m_iClientId = 0; //sharishkumar Jira 827
		/// <summary>
		/// Private variable for Datamodel factory object
		/// </summary>
		protected DataModelFactory m_objDmf=null;

		/// Enum added while doing FROI Integration
		/// <summary>
		/// Enumerated type for FROI Execution status
		/// </summary>
		public enum FROIFails : int
		{
			FF_NoError = 0,
			FF_NCCIBodyParts = 2,
			FF_NCCICauseCode = 4,
			FF_NCCIClassCode = 8,
			FF_NCCIIllness = 16,
			FF_NoDepartmentAssigned = 32,
			FF_NoClaimant = 64,
			FF_LoadFROIOptionsFailed = 128,
			FF_NonNumericClassCode = 256,
			FF_IncompleteMissingClassCode = 512,
			FF_NoClaim = 1024
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Constructor that creates the object using username, password and dsnname
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public PopulateExtender(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)//sharishkumar Jira 827
		{
			m_sUserName=p_sUserName;
			m_sPassword=p_sPassword;
			m_sDsnName= p_sDsnName;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
			//create an instance of DataModelFactory
            m_objDmf = new DataModel.DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);	//sharishkumar Jira 827
			m_sConnectString = m_objDmf.Context.DbConn.ConnectionString;
		}

		#endregion

		/// <summary>
		/// Dispose destroys the instance of DataModelFactory
		/// </summary>
		public virtual void Dispose()
		{
			m_objDmf.Dispose();
		}

		/// <summary>
		/// Populates FROIExtender object for ACORD
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>FROIExtender Object</returns>
		public FROIExtender InvokeACORD(int p_iClaimID)
		{
			FROIExtender objExt=null;
			int iUnitXClaimID=0;
			bool bEventFound = false;
			try
			{
                objExt = new FROIExtender(m_sUserName, m_sPassword, m_sDsnName, m_sConnectString, m_iClientId);//sharishkumar Jira 827
                objExt.m_objOptions = new FROIOptionsMajor(m_sConnectString, m_sUserName, m_sPassword, m_sDsnName, m_iClientId);//sharishkumar Jira 827

				//Populate claim object of the Extender OBject
                objExt.m_objClaim = m_objDmf.GetDataModelObject("ClaimExtension", false) as ClaimExtension; //Modified for Froi Migration- Mits 33585,33586,33587
				if (p_iClaimID>0)
				{
					objExt.m_objClaim.MoveTo(p_iClaimID);
				}
				else
                    throw new RMAppException(Globalization.GetString("PopulateExtender.InvalidClaim", m_iClientId));//sharishkumar Jira 827

				//Populate Vehicle Object of the Extender Object
				objExt.m_objVehicle  = (Vehicle)m_objDmf.GetDataModelObject("Vehicle",false);
				if (objExt.m_objClaim.UnitList.Count >0 )
				{
					foreach(UnitXClaim objUnit in objExt.m_objClaim.UnitList)
					{
						objExt.m_objVehicle.MoveTo(objUnit.UnitId);							
						break;					
					}						
				}
				else
					objExt.m_objVehicle =null;

				//Populate UnitxClaim of the Extender Object
				objExt.m_objUnitXClaim = (UnitXClaim)m_objDmf.GetDataModelObject("UnitXClaim",false);
				iUnitXClaimID = Functions.GetUnitXClaimRowID(m_sConnectString,objExt.m_objUnitXClaim.UnitId,
                    objExt.m_objClaim.ClaimId, m_iClientId);//sharishkumar Jira 827
				if (iUnitXClaimID>0)
					objExt.m_objUnitXClaim.MoveTo(iUnitXClaimID);
				else
					objExt.m_objUnitXClaim =null;

				//Populate Event Object of the Extender object
				objExt.m_objEvent = (Event)m_objDmf.GetDataModelObject("Event",false);
				if (objExt.m_objClaim.EventId >0)
				{
					objExt.m_objEvent.MoveTo(objExt.m_objClaim.EventId);
					objExt.m_iEventID = objExt.m_objEvent.EventId;  
					bEventFound = true;
				}
				else
					objExt.m_objEvent=null;				

				foreach(Claimant objClaimant in objExt.m_objClaim.ClaimantList )
				{
					if (objClaimant.ClaimantEid >0)
					{
						if (objClaimant.ClaimantEntity.GetType().ToString().ToUpper()  ==  "RISKMASTER.DATAMODEL.PIEMPLOYEE")
						{
							objExt.m_objPiEmp =(PiEmployee)objExt.m_objEvent.PiList.GetPiByEID(objClaimant.ClaimantEid);   
							break;
						}
						else
							objExt.m_objPiEmp =null;
					}					
				}
				
				//Populate supplement object of the Extender class
				//objExt.m_objSupp = (Riskmaster.DataModel.Supplementals)objExt.m_objClaim.Acord; 
				if(bEventFound)
				{
                    if (objExt.m_objOptions.LoadDataByAssignedDepartment(objExt.m_objEvent.DeptEid))//sharishkumar Jira 827
						objExt.m_iPrintClaimNumberAtTop = objExt.m_objOptions.m_iPrintClaimNumberAtTop;
					else
						objExt.m_iPrintClaimNumberAtTop =0;		
				}
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokeACORD.AppError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			return objExt;	
		}


		/// <summary>
		/// Populates JURISExtender for Workers Compensation
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>JURISExtender object </returns>
		public JURISExtender InvokeWorkComp(int p_iClaimID)
		{
			JURISExtender objExt=null;
			try
			{
				objExt = new JURISExtender(m_sUserName,m_sPassword,m_sDsnName,m_sConnectString,m_iClientId);//sharishkumar Jira 827
                objExt.m_objOptions = new JURISOptionsMajor(m_sConnectString, m_sUserName, m_sPassword, m_sDsnName, m_iClientId);//sharishkumar Jira 827

				//populate claim object
                objExt.m_objClaim = (ClaimExtension)m_objDmf.GetDataModelObject("ClaimExtension", false); //Modified for Froi Migration- Mits 33585,33586,33587
				if (p_iClaimID>0)
					objExt.m_objClaim.MoveTo(p_iClaimID);
				else
                    throw new RMAppException(Globalization.GetString("PopulateExtender.InvalidClaim", m_iClientId));//sharishkumar Jira 827

				objExt.m_iClaimID =  p_iClaimID;

				//populate event object
				objExt.m_objEvent = (Event)m_objDmf.GetDataModelObject("Event",false);
				if (objExt.m_objClaim.EventId >0)
				{
					objExt.m_objEvent.MoveTo(objExt.m_objClaim.EventId);
					objExt.m_iEventID = objExt.m_objEvent.EventId;
				}
				else
					objExt.m_objEvent=null;
                
				
				//objExt.m_objSupp = (Riskmaster.DataModel.Supplementals)objExt.m_objClaim.Acord;

				objExt.m_objPiEmp =  (PiEmployee)m_objDmf.GetDataModelObject("PiEmployee",false);

				objExt.m_objClaim.ClaimantList.Refresh();
 
				foreach(Claimant objClaimant in objExt.m_objClaim.ClaimantList)
				{
					if (objClaimant.ClaimantEid >0)
					{
						objExt.m_objPiEmp = objExt.m_objEvent.PiList.GetPiByEID (objClaimant.ClaimantEid) as PiEmployee ;
						break;
					}
				}

				objExt.m_objOptions.JurisRowId = objExt.m_objClaim.FilingStateId;
     
				if (objExt.m_objPiEmp.PiEid >0 )
				{
					objExt.PopulateObjExt(m_objDmf, objExt.m_objOptions,"JURISOptionsMajor");  
					objExt.PopulateObjExtObjAdministrator(m_objDmf,objExt.m_objOptions,"JURISOptionsMajor");  
				}

                // akaushik5 Added for MITS 37849 Starts
                switch (objExt.m_objOptions.PrintOSHADesc)
                {
                    case 0:
                        objExt.m_sIncidentDesc_objExt = objExt.m_objEvent.EventDescription;
                        break;
                    case 1:
                        objExt.m_sIncidentDesc_objExt = objExt.m_objEvent.EventOsha.HowAccOccurred;
                        break;
                }
                // akaushik5 Added for MITS 37849 Ends

				//populate self insured
				objExt.m_objSelfInsured =  (EntityXSelfInsured )m_objDmf.GetDataModelObject("EntityXSelfInsured",false);
				if (objExt.m_objPiEmp!=null)
					if (objExt.m_objPiEmp.PiEid >0 )
                        objExt.GetSelfInsured(m_objDmf, objExt.m_objPiEmp.DeptAssignedEid, m_iClientId);

				objExt.m_objTPA = (Entity)m_objDmf.GetDataModelObject("Entity",false);
				if (objExt.m_objOptions.TPAEID>0)
				{
					if (p_iClaimID>0)
						objExt.m_objTPA.MoveTo(objExt.m_objOptions.TPAEID);
					else
						objExt.m_objTPA=null;
				}
				else
					objExt.m_objTPA=null;

                objExt.m_objJurisLicenseOrCode = m_objDmf.GetDataModelObject("JurisLicenseOrCode", false) as JurisLicenseOrCode; //Modified for Froi Migration- Mits 33585,33586,33587

			}

			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokeWorkComp.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			return objExt;
		}


		/// <summary>
		/// Populates FROIExtender Object for EventPIEmployee
		/// </summary>
		/// <param name="p_iPIRowID">Person Involved Row ID</param>
		/// <param name="p_iErrCod">Error Code Id</param>
		/// <returns>FROIExtender Object</returns>
		public FROIExtender InvokeEventPIEmployee(int p_iPIRowID, ref int p_iErrCod)
		{
			FROIExtender objExt=null;
			int iEventID=0;
			int iAdminID=0;
			try
			{
                objExt = new FROIExtender(m_sUserName, m_sPassword, m_sDsnName, m_sConnectString, m_iClientId);//sharishkumar Jira 827
                objExt.m_objOptions = new FROIOptionsMajor(m_sConnectString, m_sUserName, m_sPassword, m_sDsnName, m_iClientId);//sharishkumar Jira 827

				iEventID= Functions.GetEventID_PersonInvolved(m_sConnectString,p_iPIRowID,m_iClientId);//sharishkumar Jira 827
				objExt.m_objEvent  =  (Event)m_objDmf.GetDataModelObject("Event",false);
				//populate event
				if (iEventID>0)
					objExt.m_objEvent.MoveTo(iEventID);
				else
					objExt.m_objEvent=null;

				objExt.m_objPiEmp  =  (PiEmployee)m_objDmf.GetDataModelObject("PiEmployee",false);

				//populate employee
				foreach(PersonInvolved objPI in objExt.m_objEvent.PiList)
				{
					if (objPI.GetType().ToString().ToUpper() =="RISKMASTER.DATAMODEL.PIEMPLOYEE")
					{
						objExt.m_objPiEmp.MoveTo(p_iPIRowID);
						break;
					}					
				}
				
				if (objExt.m_objPiEmp.PiRowId != p_iPIRowID)
					objExt.m_objPiEmp=null;
				else
				{
					if (objExt.m_objPiEmp.DeptAssignedEid >0)
					{
                        if (!objExt.m_objOptions.LoadDataByAssignedDepartment(objExt.m_objPiEmp.DeptAssignedEid))
							p_iErrCod = p_iErrCod + (int)FROIFails.FF_LoadFROIOptionsFailed;							
					}
					else
						p_iErrCod = p_iErrCod + (int)FROIFails.FF_NoDepartmentAssigned;
                    objExt.m_iCompanyEID = Functions.GetOrgParentByDepartID(m_sConnectString, objExt.m_objPiEmp.DeptAssignedEid, objExt.m_objOptions.EmployerLevel, m_iClientId);  //sharishkumar Jira 827     

					if (objExt.m_objPiEmp.EventId >0 )
						objExt.m_objEvent.MoveTo(objExt.m_objPiEmp.EventId);
					else
						objExt.m_objEvent=null;
				}			
				if (objExt.m_iCompanyEID >0 )
				{
					objExt.m_objCompany = (Entity)m_objDmf.GetDataModelObject("Entity",false);
					objExt.m_objCompany.MoveTo(objExt.m_iCompanyEID);
				}
				else
					objExt.m_objCompany=null;

				//Populate Administrator
				iAdminID = objExt.GetAdministratorID(0);
				if (iAdminID>0)
					objExt.m_objAdministrator.MoveTo(iAdminID);
				else
					objExt.m_objAdministrator =null;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokeEventPIEmployee.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			return objExt;
		}


		/// <summary>Gets Body part affected code from CODES, CODES_TEXT</summary>
		/// <param name="p_iRowID">Person Involved Row ID - PI_ROW_ID</param>			
		/// <returns>SHORT_CODE from CODES_TEXT</returns>
		private string NCCIBodyPartAffectedCode(int p_iRowID)
		{
			DbReader objRdr=null;
			StringBuilder sbShortCode = null;
			StringBuilder sbSQL=null;
			try
			{
				sbShortCode= new StringBuilder() ;
				sbSQL=new StringBuilder();

				sbSQL.Append("SELECT CODES_TEXT.SHORT_CODE FROM CODES, CODES_TEXT, PI_X_BODY_PART");
				sbSQL.Append(" WHERE (CODES.IND_STANDARD_CODE = CODES_TEXT.CODE_ID)");
				sbSQL.Append(" AND (CODES.CODE_ID=PI_X_BODY_PART.BODY_PART_CODE)  AND PI_X_BODY_PART.PI_ROW_ID = ");
				sbSQL.Append(p_iRowID);
				sbSQL.Append(" AND CODES.IND_STANDARD_CODE > 0");

				objRdr=DbFactory.GetDbReader(m_sConnectString , sbSQL.ToString());
				if (objRdr!=null)
				{
					while(objRdr.Read())
					{
						if (sbShortCode.ToString()=="")
							sbShortCode.Append(objRdr.GetString(0));
						else
						{
							sbShortCode.Append(",");
							sbShortCode.Append(objRdr.GetString(0));
						}
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.NCCIBodyPartAffectedCode.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
			}
			return sbShortCode.ToString() ;
		}


		/// <summary>Gets Cause of Injury Code</summary>
		/// <param name="p_iCodeID">CODE_ID from CODES table</param>			
		/// <returns>SHORT_CODE from CODES_TEXT</returns>
		private string NCCICauseOfInjuryCode(int p_iCodeID)
		{
			DbReader objRdr=null;
			string sShortCode= "" ;
			StringBuilder sbSQL=null;
			try
			{
				sbSQL=new StringBuilder();
				sbSQL.Append("SELECT CODES_TEXT.SHORT_CODE FROM CODES, CODES_TEXT");
				sbSQL.Append(" WHERE (CODES.IND_STANDARD_CODE = CODES_TEXT.CODE_ID)");
				sbSQL.Append(" AND CODES.CODE_ID = ");
				sbSQL.Append(p_iCodeID);
				sbSQL.Append(" AND CODES.IND_STANDARD_CODE > 0");

				objRdr=DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString());
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						sShortCode= objRdr.GetString(0);
					}
				}
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.NCCICauseOfInjuryCode.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				sbSQL=null;
			}
			return sShortCode ;
		}


		/// <summary>Gets List of type of Injury</summary>
		/// <param name="p_objPiEmployee">PersonInvolved Employee</param>			
		/// <returns>SHORT_CODE from CODES_TEXT</returns>
		private string NCCITypeOfIllnessInjury(PiEmployee p_objPiEmployee)
		{
			DbReader objRdr=null;
			StringBuilder sbTempCode = null;
			StringBuilder sbTmpDisabilityCode = null;
			string sShortCode="";
			string sSQL="";
			IEnumerator objEnumerator=null;
			Common.LocalCache objChache=null;
			try
			{
				if(p_objPiEmployee==null)
					return "";
				sbTempCode = new StringBuilder() ;
				sbTmpDisabilityCode= new StringBuilder();
				objChache = new Common.LocalCache(m_objDmf.Context.DbConn.ConnectionString,m_iClientId);
				sShortCode= objChache.GetShortCode(p_objPiEmployee.DisabilityCode);
				switch(sShortCode.ToUpper())
				{
					case "INJ":
						
						sbTmpDisabilityCode.Append("INJURY:  ");
						(p_objPiEmployee.InjuryCodes as IDataModel).IsStale =true;
						objEnumerator =p_objPiEmployee.InjuryCodes.GetEnumerator();  
						while(objEnumerator.MoveNext())
						{
							sbTmpDisabilityCode.Append(objChache.GetCodeDesc(Int32.Parse(((DictionaryEntry)objEnumerator.Current).Key.ToString())));
						}
						sSQL = "SELECT CODES_TEXT.SHORT_CODE FROM CODES, CODES_TEXT, PI_X_INJURY" +
								" WHERE (CODES.IND_STANDARD_CODE = CODES_TEXT.CODE_ID)" +
								" AND (CODES.CODE_ID=PI_X_INJURY.INJURY_CODE)" +
								" AND PI_X_INJURY.PI_ROW_ID = " + p_objPiEmployee.PiRowId  +
								" AND CODES.IND_STANDARD_CODE > 0";
						objEnumerator =null;
						break;
					case "ILL":
						sbTmpDisabilityCode.Append("ILLNESS:  ");
						sbTmpDisabilityCode.Append(objChache.GetCodeDesc(p_objPiEmployee.IllnessCode));
						sSQL = "SELECT CODES_TEXT.SHORT_CODE  FROM CODES, CODES_TEXT, PERSON_INVOLVED" +
								" WHERE (CODES.IND_STANDARD_CODE = CODES_TEXT.CODE_ID)" +
								" AND (CODES.CODE_ID=PERSON_INVOLVED.ILLNESS_CODE)" +
								" AND PERSON_INVOLVED.PI_ROW_ID = " + p_objPiEmployee.PiRowId +
								" AND CODES.IND_STANDARD_CODE > 0";
						break;
				}
				if (sSQL=="")
					return "";

				objRdr=DbFactory.GetDbReader(m_objDmf.Context.DbConn.ConnectionString, sSQL);
				if (objRdr!=null)
				{
					while(objRdr.Read())
					{
						string  sTmp= objRdr.GetString(0);
						if (sTmp!="")
						{
							if (sbTempCode.ToString()=="")  
							{
								sbTempCode.Append(sTmp);
							}
							else
							{
								sbTempCode.Append(",");
								sbTempCode.Append(sTmp);	
							}
						}
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.NCCITypeOfIllnessInjury.DataError", m_iClientId), p_objExp);//sharishkumar Jira 827
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
				if (objChache!=null)objChache.Dispose();
				sbTmpDisabilityCode =null;
				objEnumerator=null;
			}
			return sbTempCode.ToString();
		}

		/// <summary>
		/// Populates FROIExtender object for FROI
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iErrCod">Error Code</param>
		/// <returns>FROIExtender Object</returns>
		public FROIExtender InvokeFROI(int p_iClaimID, ref int p_iErrCod)
        {
            FROIExtender objExt = null;
            string sTemp = "";
            string sClaimantTitle = "";
            int iTest = 0;
            Common.LocalCache objCache = null;
            string sStateCode = "";
            try
            {
                objCache = new Common.LocalCache(m_objDmf.Context.DbConn.ConnectionString,m_iClientId);
                objExt = new FROIExtender(m_sUserName, m_sPassword, m_sDsnName, m_sConnectString,m_iClientId);//sharishkumar Jira 827
                objExt.m_objOptions = new FROIOptionsMajor(m_sConnectString,m_sUserName,m_sPassword,m_sDsnName,m_iClientId);//sharishkumar Jira 827

                //populate claim object
                objExt.m_objClaim = (ClaimExtension)m_objDmf.GetDataModelObject("ClaimExtension", false); //Modified for Froi Migration- Mits 33585,33586,33587
                if (p_iClaimID > 0)
                {
                    objExt.m_objClaim.MoveTo(p_iClaimID);
                    objExt.m_iClaimID = p_iClaimID;
                }
                else
                    throw new InvalidValueException("PopulateExtender.InvalidClaim");

                objExt.m_iEventID = objExt.m_objClaim.EventId;

                objExt.objJur = objExt.objClaim.Jurisdictionals; //Modified for Froi Migration- Mits 33585,33586,33587
                            

                //populate event
                objExt.m_objEvent = (Event)m_objDmf.GetDataModelObject("Event", false);
                if (objExt.m_objClaim.EventId > 0)
                    objExt.m_objEvent.MoveTo(objExt.m_objClaim.EventId);
                else
                    objExt.m_objEvent = null;

                objExt.m_objClaim.ClaimantList.Refresh();
                foreach (Claimant objClaimant in objExt.m_objClaim.ClaimantList)
                {
                    if (objClaimant.ClaimantEid > 0)
                    {
                        objExt.m_objPiEmp = (PiEmployee)m_objDmf.GetDataModelObject("PiEmployee", false);
                        objExt.m_objPiEmp = objExt.m_objEvent.PiList.GetPiByEID(objClaimant.ClaimantEid) as PiEmployee;
                        sClaimantTitle = objClaimant.ClaimantEntity.Title;
                        break;
                    }
                }

                if (objExt.m_objPiEmp == null)
                {
                    p_iErrCod = p_iErrCod + (int)FROIFails.FF_NoClaimant;
                    return objExt;
                }

                if (objExt.m_objPiEmp.DeptAssignedEid > 0)
                {
                    objExt.m_objOptions.JurisRowId = objExt.m_objClaim.FilingStateId;
                    if (!objExt.m_objOptions.LoadDataByAssignedDepartment(objExt.m_objPiEmp.DeptAssignedEid))
                        p_iErrCod = p_iErrCod + (int)FROIFails.FF_LoadFROIOptionsFailed;
                }
                else
                    p_iErrCod = p_iErrCod + (int)FROIFails.FF_NoDepartmentAssigned;


                switch (objExt.m_objOptions.m_iNCCIForce)
                {
                    case 1:		//force printing of NCCI codes
                        if (NCCIBodyPartAffectedCode(objExt.m_objPiEmp.PiRowId) == "")
                            p_iErrCod = p_iErrCod + (int)FROIFails.FF_NCCIBodyParts;

                        if (NCCICauseOfInjuryCode(objExt.m_objEvent.CauseCode) == "")
                            p_iErrCod = p_iErrCod + (int)FROIFails.FF_NCCICauseCode;

                        if (NCCITypeOfIllnessInjury(objExt.m_objPiEmp) == "")
                            p_iErrCod = p_iErrCod + (int)FROIFails.FF_NCCIIllness;

                        sTemp = objCache.GetShortCode(objExt.m_objPiEmp.NcciClassCode);
                        if (sTemp.Length > 4)
                            sTemp = sTemp.Substring(0, 4);

                        if (!(Conversion.IsNumeric(sTemp) && (sTemp.Length == 4)))
                            p_iErrCod = p_iErrCod + (int)FROIFails.FF_NonNumericClassCode;
                        break;

                    case 2:		//do not force printing of NCCI codes
                        break;
                }

                if (p_iErrCod > 0)
                    return objExt;

                //populate other objects
                objExt.PopulateObjExt(m_objDmf, objExt.m_objOptions, "FROIOptionsMajor");
                objExt.PopulateObjExtObjAdministrator(m_objDmf, objExt.m_objOptions, "FROIOptionsMajor");

                switch (objExt.m_objOptions.PrintOSHADesc)
                {
                    case 0:
                        objExt.m_sIncidentDesc_objExt = objExt.m_objEvent.EventDescription;
                        break;
                    case 1:
                        objExt.m_sIncidentDesc_objExt = objExt.m_objEvent.EventOsha.HowAccOccurred;
                        break;
                }

                //Added for issue Variable is undefined: 'objTPA' in PDS //Modified for Froi Migration- Mits 33585,33586,33587
                objExt.m_objTPA = (Entity)m_objDmf.GetDataModelObject("Entity", false);

                //Mits 33586 Defect 1572 
                if (objExt.m_objOptions.ClaimAdminTPAEID > 0 && p_iClaimID > 0)
                {
                    objExt.m_objTPA.MoveTo(objExt.m_objOptions.ClaimAdminTPAEID);
                }
                else
                {
                    objExt.m_objTPA = null;
                }
                //Mits 33586 Defect 1572 

                switch (objExt.m_objOptions.m_iUseTitle)
                {
                    case 0:		//use title
                        if (sClaimantTitle != "")
                            sTemp = objCache.GetCodeDesc(objExt.m_objPiEmp.PositionCode);
                        break;
                    case 1:		//use position code
                        sTemp = objCache.GetCodeDesc(objExt.m_objPiEmp.PositionCode);
                        break;
                    default:	//invalid value
                        break;
                }

                objExt.m_sOccupation_objExt = sTemp;
                if (objExt.m_objOptions.m_iWorkLossField < 1)
                    objExt.m_objOptions.m_iWorkLossField = 3;

                objExt.m_iWorkLossOption_objExt = objExt.m_objOptions.m_iWorkLossField;
                objExt.m_iContactOption_objExt = objExt.m_objOptions.m_iContactOption;
                objExt.m_iAttachToClaimByDefault_FROI = objExt.m_objOptions.m_iAttachToClaimByDefault_FROI;
                objExt.m_objContact = (Entity)m_objDmf.GetDataModelObject("Entity", false);
                objExt.GetContact(objExt.m_objOptions.m_iContactOption);
                objExt.GetPersonNotified(m_objDmf);
                objExt.m_iPrintClaimNumberAtTop = objExt.m_objOptions.m_iPrintClaimNumberAtTop;
                sStateCode = objCache.GetStateCode(objExt.m_objClaim.FilingStateId);

                switch (sStateCode)
                {
                    case "NE":
                        {
                            switch (objExt.m_objOptions.m_iNEParentOption)
                            {
                                case 1:
                                    iTest = Functions.GetOrgParentByDepartID(m_sConnectString, objExt.m_objPiEmp.DeptAssignedEid, objExt.m_objOptions.m_iNEParentOrgLevel,m_iClientId);//sharishkumar Jira 827
                                    if (iTest > 0)
                                        objExt.m_objJurisParent_objExt = Extender.GetEntity(m_objDmf, iTest,m_iClientId);//sharishkumar Jira 827
                                    break;
                                case 2:
                                    if (objExt.m_objOptions.m_iNEParentDefaultID > 0)
                                        objExt.m_objJurisParent_objExt = Extender.GetEntity(m_objDmf, objExt.m_objOptions.m_iNEParentDefaultID,m_iClientId);//sharishkumar Jira 827
                                    break;
                                case 3:
                                    objExt.GetTopInsured(m_objDmf);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        }
                    case "TN":
                        {
                            switch (objExt.m_objOptions.m_iTNParentOption)
                            {
                                case 1:
                                    iTest = Functions.GetOrgParentByDepartID(m_sConnectString, objExt.m_objPiEmp.DeptAssignedEid, objExt.m_objOptions.m_iTNParentOrgLevel,m_iClientId);//sharishkumar Jira 827
                                    if (iTest > 0)
                                        objExt.m_objJurisParent_objExt = Extender.GetEntity(m_objDmf, iTest,m_iClientId);//sharishkumar Jira 827
                                    break;
                                case 2:
                                    if (objExt.m_objOptions.m_iTNParentDefaultID > 0)
                                        objExt.m_objJurisParent_objExt = Extender.GetEntity(m_objDmf, objExt.m_objOptions.m_iTNParentDefaultID,m_iClientId);//sharishkumar Jira 827
                                    break;
                                case 3:
                                    objExt.GetTopInsured(m_objDmf);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        }
                    case "VA":
                        {
                            switch (objExt.m_objOptions.m_iVAParentOption)
                            {
                                case 1:
                                    iTest = Functions.GetOrgParentByDepartID(m_sConnectString, objExt.m_objPiEmp.DeptAssignedEid, objExt.m_objOptions.m_iVirginiaParentLevel,m_iClientId);//sharishkumar Jira 827
                                    if (iTest > 0)
                                        objExt.m_objJurisParent_objExt = Extender.GetEntity(m_objDmf, iTest,m_iClientId);//sharishkumar Jira 827
                                    break;
                                case 2:
                                    if (objExt.m_objOptions.m_iVAParentDefaultID > 0)
                                        objExt.m_objJurisParent_objExt = Extender.GetEntity(m_objDmf, objExt.m_objOptions.m_iVAParentDefaultID,m_iClientId);//sharishkumar Jira 827
                                    break;
                                default:
                                    break;
                            }
                            break;
                        }
                }
                objExt.GetSelfInsured(m_objDmf, objExt.m_objPiEmp.DeptAssignedEid, m_iClientId);//sharishkumar Jira 827
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokeFROI.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }
            return objExt;
        }


		/// <summary>
		/// Populates FROIExtender for EventMedWatch
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <returns>FROIExtender Object</returns>
		public FROIExtender InvokeEventMedWatch(int p_iEventID)
		{
			FROIExtender objExt=null;
			try
			{
				objExt = new FROIExtender(m_sUserName,m_sPassword,m_sDsnName,m_sConnectString,m_iClientId);//sharishkumar Jira 827
				objExt.m_objEvent   =  (Event)m_objDmf.GetDataModelObject("Event",false);

				if (p_iEventID>0)
					objExt.m_objEvent.MoveTo(p_iEventID);
				else
                    throw new InvalidValueException(Globalization.GetString("PopulateExtender.InvalidEvent", m_iClientId));//sharishkumar Jira 827

                objExt.m_objPatient = Extender.GetPatient(m_objDmf, FROIExtender.GetPatientID(m_sConnectString, p_iEventID, m_iClientId),m_iClientId);//sharishkumar Jira 827
				objExt.GetFacility(m_objDmf);
				objExt.GetRelevantTests(p_iEventID);
				objExt.GetConcomitantProducts(p_iEventID);
				objExt.GetUFDistReportNumber();
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokeEventMedWatch.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			return objExt;
		}


		/// <summary>
		/// Populates FROIExtender for EventVehAccNewYork
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <returns>FROIExtender Object</returns>
		public FROIExtender InvokeEventVehAccNewYork(int p_iEventID)
		{
			FROIExtender objExt=null;
			Common.LocalCache objChache=null;
			int iCodeID=0;
			try
			{
                objExt = new FROIExtender(m_sUserName, m_sPassword, m_sDsnName, m_sConnectString, m_iClientId);//sharishkumar Jira 827
                objExt.m_objOptions = new FROIOptionsMajor(m_sConnectString, m_sUserName, m_sPassword, m_sDsnName, m_iClientId);  //sharishkumar Jira 827
				objExt.m_objEvent = (Event)m_objDmf.GetDataModelObject("Event",false);

				if (p_iEventID>0)
					objExt.m_objEvent.MoveTo(p_iEventID);
				else
                    throw new InvalidValueException(Globalization.GetString("PopulateExtender.InvalidEvent", m_iClientId));//sharishkumar Jira 827

				objExt.m_iEventID = p_iEventID;
				objExt.m_objSupp = objExt.m_objEvent.Supplementals;
				objExt.m_objVAPI = (Entity)m_objDmf.GetDataModelObject("Entity",false);
				objExt.m_objVAPI= objExt.m_objEvent.ReporterEntity;

                objChache = new Common.LocalCache(m_objDmf.Context.DbConn.ConnectionString, m_iClientId);
				foreach( PersonInvolved objPI in objExt.m_objEvent.PiList )
				{
					iCodeID = objPI.PiTypeCode;
					if (objChache.GetShortCode(iCodeID)=="E")						
					{
						objExt.m_objPiEmp =  objPI as PiEmployee ;
                        break;
					}
				}
				objChache.Dispose();

				if (objExt.m_objPiEmp!=null)
					if (objExt.m_objPiEmp.DeptAssignedEid >0)
                        if (objExt.m_objOptions.LoadDataByAssignedDepartment(objExt.m_objPiEmp.DeptAssignedEid)) { }//sharishkumar Jira 827

				
				if (objExt.m_objEvent.DeptEid>0)
				{
                    objExt.m_iCompanyEID = Functions.GetOrgParentByDepartID(m_sConnectString, objExt.m_objEvent.DeptEid, objExt.m_objOptions.EmployerLevel, m_iClientId);//sharishkumar Jira 827
					if (objExt.m_iCompanyEID>0)
					{
						objExt.m_objCompany =(Entity)m_objDmf.GetDataModelObject("Entity",false);
						objExt.m_objCompany.MoveTo(objExt.m_iCompanyEID);
					}
					else 
						objExt.m_objCompany=null;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokeEventVehAccNewYork.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			finally
			{
				if (objChache!=null)objChache.Dispose();
			}
			return objExt;
		}


		/// <summary>
		/// Populates FROIExtender object for ClaimVehAccNewYork
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iErrCod">Error Code Id</param>
		/// <returns>FROIExtender Object</returns>
		public FROIExtender InvokeClaimVehAccNewYork(int p_iClaimID, ref int p_iErrCod)
		{
			FROIExtender objExt=null;
			int iUnitXClaim=0;
			try
			{
                objExt = new FROIExtender(m_sUserName, m_sPassword, m_sDsnName, m_sConnectString, m_iClientId);//sharishkumar Jira 827

				objExt.m_objClaim  = (Claim)m_objDmf.GetDataModelObject("Claim",false);
				if (p_iClaimID>0)
				{
					objExt.m_objClaim.MoveTo(p_iClaimID);
					objExt.m_iClaimID = p_iClaimID;
					objExt.m_objEvent  = (Event)m_objDmf.GetDataModelObject("Event",false);
					objExt.m_objEvent.MoveTo(objExt.m_objClaim.EventId);
					//objExt.m_objSupp=(Riskmaster.DataModel.Supplementals)objExt.m_objClaim.Acord;
				}
				else
				{
					p_iErrCod = p_iErrCod + (int)FROIFails.FF_NoClaim; 
					throw new InvalidValueException("PopulateExtender.InvalidClaim");
				}

				foreach( PersonInvolved objPI in objExt.m_objEvent.PiList )
				{
					if (objPI.GetType().ToString().ToUpper()  ==  "RISKMASTER.DATAMODEL.PIEMPLOYEE" )
					{
						objExt.m_objPiEmp =  objPI as PiEmployee ;
						break;
					}
				}

				objExt.m_objVehicle  = (Vehicle)m_objDmf.GetDataModelObject("Vehicle",false);
				if (objExt.m_objClaim.UnitList.Count >0 )
				{
					foreach(UnitXClaim objUnit in objExt.m_objClaim.UnitList)
					{
						objExt.m_objVehicle.MoveTo(objUnit.UnitId);
						break;					
					}						
				}
				else
					objExt.m_objVehicle =null;

				//Populate UnitxClaim of the Extender Object
				objExt.m_objUnitXClaim = (UnitXClaim)m_objDmf.GetDataModelObject("UnitXClaim",false);
                iUnitXClaim = Functions.GetUnitXClaimRowID(m_sConnectString, objExt.m_objUnitXClaim.UnitId, objExt.m_objClaim.ClaimId, m_iClientId);//sharishkumar Jira 827
				if (iUnitXClaim>0)
					objExt.m_objUnitXClaim.MoveTo(iUnitXClaim);
				else
					objExt.m_objUnitXClaim =null;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokeClaimVehAccNewYork.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			return objExt;
		}


		/// <summary>
		/// Populates PolicyExtender Object for Policy
		/// </summary>
		/// <param name="p_iPolicyID">Policy ID</param>
		/// <returns>PolicyExtender Object</returns>
		public PolicyExtender InvokePolicy(int p_iPolicyID)
		{
			PolicyExtender objExt=null;
			try
			{
				objExt = new PolicyExtender();
				objExt.m_objPolicy= (Policy)m_objDmf.GetDataModelObject("Policy",false);
				if (p_iPolicyID>0)
					objExt.m_objPolicy.MoveTo(p_iPolicyID);
				else
					throw new InvalidValueException("PopulateExtender.InvalidPolicy");
				
				objExt.m_obbSupp = objExt.m_objPolicy.Supplementals;
			}		
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokePolicy.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			return objExt;
		}


		/// <summary>
		/// Populates PolicyEnhExtender for PolicyEnh
		/// </summary>
		/// <param name="p_iPolicyID">Policy ID</param>
		/// <returns>PolicyEnhExtender Object</returns>
		public PolicyEnhExtender InvokePolicyEnh(int p_iPolicyID)
		{
			PolicyEnhExtender objExt=null;
			try
			{
                
                //objExt = new PolicyEnhExtender();
				objExt = new PolicyEnhExtender(m_sUserName, m_sPassword, m_sDsnName, m_sConnectString, m_iClientId);
              
                objExt.m_objPolEnh = (PolicyEnh)m_objDmf.GetDataModelObject("PolicyEnh", false);
				if (p_iPolicyID>0)
					objExt.m_objPolEnh.MoveTo(p_iPolicyID);
				else
					throw new InvalidValueException("PopulateExtender.InvalidPolicy");
				
				objExt.m_objSupp  = objExt.m_objPolEnh.Supplementals;
			}			
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PopulateExtender.InvokePolicyEnh.AppError", m_iClientId), p_objEx);//sharishkumar Jira 827
			}
			return objExt;
		}

	}// end class
}// end namespace
