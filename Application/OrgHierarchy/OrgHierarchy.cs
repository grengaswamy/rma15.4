﻿
using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;
using System.Text;
using System.Collections;
using System.IO;
using System.Drawing;
using C1.C1PrintDocument;
using Riskmaster.Settings;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Xml.Linq;

namespace Riskmaster.Application.OrgHierarchy
{
	/// <summary>
	///Author  :   Sumeet Rathod
	///Dated   :   29th Nov 2004
	///Purpose :   This class performs the operations related to Organization Hierarchy functionality.
	/// </summary>
	public class OrgHierarchy
	{
		#region Class level fields
        private XmlDocument m_objXDoc;
        //private XmlElement objXMLElement;

		/// <summary>
		/// Flag indicating error in processing
		/// </summary>
		private bool m_bIsError = false;

		/// <summary>
		/// Level of the Organization Hierarchy
		/// </summary>
		private long m_lLevel = 0;

		/// <summary>
		/// Row id
		/// </summary>
		private long m_lRowId = 0;

		/// <summary>
		/// Search id
		/// </summary>
		private long m_lSearchId = 0;

		/// <summary>
		/// User id
		/// </summary>
		private long m_lUserId = 0;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";

		/// <summary>
		/// Table name
		/// </summary>
		private string m_sTable = "";

		/// <summary>
		/// Event Date
		/// </summary>
		private string m_sEventDate = "";

		/// <summary>
		/// Policy Date
		/// </summary>
		private string m_sPolicyDate = "";

		/// <summary>
		/// Contains generic SQL statements
		/// </summary>
		private string m_sSql = "";

		/// <summary>
		/// Claim Date
		/// </summary>
		private string m_sClaimDate = "";

		/// <summary>
		/// System Date
		/// </summary>
		private string m_sSystemDate = "";

		/// <summary>
		/// Node category
		/// </summary>
		private string m_sNodCat = "";

		/// <summary>
		/// Filter by Effective Date status
		/// </summary>
		private string m_sFilter = "";

		/// <summary>
		/// Claim
		/// </summary>
		private const string CLAIM = "CLAIM";

		/// <summary>
		/// Event
		/// </summary>
		private const string EVENT = "EVENT";

		/// <summary>
		/// Policy
		/// </summary>
		private const string POLICY = "POLICY";
		/// <summary>
		/// Policy Enhanced added by Shivendu for MITS 17249
		/// </summary>
		private const string POLICYENH = "POLICYENH";

		/// <summary>
		/// No entity
		/// </summary>
		private const string NO_ENTITY = "no entity";

		/// <summary>
		/// Client short name
		/// </summary>
		private const string CLIENT = "C";

		/// <summary>
		/// Company short name
		/// </summary>
		private const string COMPANY = "CO";

		/// <summary>
		/// Operation short name
		/// </summary>
		private const string OPERATION = "O";

		/// <summary>
		/// Region short name
		/// </summary>
		private const string REGION = "R";

		/// <summary>
		/// Location short name
		/// </summary>
		private const string LOCATION = "L";

		/// <summary>
		/// Division short name
		/// </summary>
		private const string DIVISION = "D";

		/// <summary>
		/// Facility short name
		/// </summary>
		private const string FACILITY = "F";

		/// <summary>
		/// Department short name
		/// </summary>
		private const string DEPARTMENT = "DT";

		/// <summary>
		/// All levels of Organization Hierarchy
		/// </summary>
		private const string ALL = "A";

		/// <summary>
		/// Database Type
		/// </summary>
		private string m_sDBType = "";

		//Changed by Gagan for MITS 8599 : Start

		/// <summary>
		/// C1PrintDocumnet object instance.
		/// </summary>
		private C1PrintDocument m_objPrintDoc = null;

		/// <summary>
		/// ArrayList for Org Hierarahcy Names
		/// </summary>
		ArrayList arrListName = null;

		/// <summary>
		/// ArrayList for Org Hierarahcy Levels
		/// </summary>
		ArrayList arrListLevel = null;

		private double m_dblCurrentX = 0;
		/// <summary>
		/// CurrentY
		/// </summary>
		private double m_dblCurrentY = 0;
		/// <summary>
		/// Font object.
		/// </summary>
		private Font m_objFont = null;
		/// <summary>
		/// Text object.
		/// </summary>
		private RenderText m_objText = null;
		/// <summary>
		/// PageWidth
		/// </summary>
		private double m_dblPageWidth = 0.0;
		/// <summary>
		/// PageHeight
		/// </summary>
		private double m_dblPageHeight = 0.0;
		/// <summary>
		/// ClientWidth
		/// </summary>
		private double m_dblClientWidth = 0.0;
		private const string m_sFuncIdDelimiter = "|";
		private const string m_sNameDelimiter = "@%";
		private const string m_sInsideIdDelimiter = ",";
		private string m_sTempNodCat = "";

        private int m_iClientId = 0; //ash - cloud
        
        //Ash - cloud, load orghierarchy and message config settings from DB
		//private Hashtable m_OrgHierarchySettings = RMConfigurationManager.GetDictionarySectionSettings("OrgHierarchy");
		//private Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_OrgHierarchySettings = null;
        private Hashtable m_MessageSettings = null;
		
		//pmittal5 Confidential Record
		private bool m_bOrgSec = false;
		private int m_iUserId = 0;
		//End - pmittal5
		#endregion


		#region Property Declarations
		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		private double PageHeight
		{
			get
			{
				return (m_dblPageHeight);
			}
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double PageWidth
		{
			get
			{
				return (m_dblPageWidth);
			}
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double ClientWidth
		{
			get
			{
				return (m_dblClientWidth);
			}
			set
			{
				m_dblClientWidth = value;
			}
		}

		/// <summary>
		/// Read/Write property for CurrentX.
		/// </summary>
		private double CurrentX
		{
			get
			{
				return (m_dblCurrentX);
			}
			set
			{
				m_dblCurrentX = value;
			}
		}
		/// <summary>
		/// Read/Write property for CurrentY.
		/// </summary>
		private double CurrentY
		{
			get
			{
				return (m_dblCurrentY);
			}
			set
			{
				m_dblCurrentY = value;
			}
		}

		/// <summary>
		/// Read/Write property for TableName. MITS 17157 Pankaj 7/15/09
		/// </summary>
		public string TableName
		{
			get
			{
				return (m_sTable);
			}
			set
			{
				m_sTable = value;
			}
		}

		//pmittal5 Confidential Record
		public bool IsBesEnabled
		{
			get
			{
				return m_bOrgSec;
			}
			set
			{
				m_bOrgSec = value;
			}

		}
		public int UserID
		{
			get
			{
				return m_iUserId;
			}
			set
			{
				m_iUserId = value;
			}
		}
		//End

		/// <summary>
		/// Gets unique filename
		/// </summary>
		private string TempFile
		{
			get
			{
				string sFileName = string.Empty;
				string strTempDirPath = string.Empty;

				strTempDirPath = RMConfigurator.TempPath;

				sFileName = String.Format("{0}\\{1}", strTempDirPath, Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
				
				return sFileName;
			}
		}

		//Changed by Gagan for MITS 8599 : End


		#endregion

		#region Constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public OrgHierarchy()
		{
		}

		/// <summary>
		/// Overloaded constructor, which sets the connection string.
        /// Added ClientID to handle multi-tenant environment, cloud
		/// </summary>
		/// <param name="p_sConnectionstring">Database connection string</param>
        public OrgHierarchy(string p_sConnectionstring, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionstring;
            m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
            m_OrgHierarchySettings = RMConfigurationManager.GetDictionarySectionSettings("OrgHierarchy", m_sConnectionString, m_iClientId);
            m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId);
		}
        
		#endregion

		# region Constants

		//Changed by Gagan for MITS 8599 : Start

		private const string LEFT_ALIGN = "1";
		/// <summary>
		/// Right alignment
		/// </summary>
		private const string RIGHT_ALIGN = "2";
		/// <summary>
		/// Center alignment
		/// </summary>
		private const string CENTER_ALIGN = "3";
		/// <summary>
		/// Number of rows in a report page
		/// </summary>
		private const int NO_OF_ROWS = 40;
		/// <summary>
		/// Level constants
		/// </summary>
		private const string CLIENT_NAME = "client";
		private const string COMPANY_NAME = "company";
		private const string OPERATION_NAME = "operation";
		private const string REGION_NAME = "region";
		private const string DIVISION_NAME = "division";
		private const string LOCATION_NAME = "location";
		private const string FACILITY_NAME = "facility";
		private const string DEPARTMENT_NAME = "department";

		//Changed by Gagan for MITS 8599 : End

        /// <summary>
        /// Auto implemented property for OrgSec
        /// </summary>
        public bool OrgSec { get; set; }

		#endregion

		# region Delete Entity
		public string DeleteCompanyInformation(int p_iEntityId, string p_sUserName,
			string p_sPassword, string p_sDsnName)
		{
			DataModelFactory objDMF = null;
			Entity objEntity = null;
			XmlDocument objXML = null;
			string strRetVal = "UnSuccessful";

			try
			{
				objXML = new XmlDocument();
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);//sharishkumar Rmacloud
				objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
				if (p_iEntityId > 0)
					objEntity.MoveTo(p_iEntityId);
				else
					throw new RMAppException(Globalization.GetString
						("OrgHierarchy.EditCompanyInformation.InvalidId",m_iClientId));//sharishkumar Rmacloud
				objEntity.Delete();
				strRetVal = "Success";
			}
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("OrgHierarchy.DeleteCompanyInformation.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDMF != null) objDMF.Dispose();
				if (objEntity != null) objEntity.Dispose();
				objXML = null;
			}
			//return (bRetVal);
			return (strRetVal);
		}
		#endregion

		#region Update/Save Company Information
		/// Name		: UpdateCompanyInformation
		/// Author		: Rahul Sharma
		/// Date Created: 06/29/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		/// 
		///*************************************************************************
		/// <summary>
		/// Add the company information in the database.
		/// </summary>
		/// <param name="p_sCompanyInfo">XML string containing company information to be added.</param>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DSN name</param>
		/// <returns>True if department is added successfully else false</returns>
		public string UpdateCompanyInformation(int p_iEntityId, string p_sCompanyInfo, string p_sUserName,
			string p_sPassword, string p_sDsnName, string p_sConnectionString)
		{
			DataModelFactory objDMF = null;
			//	DbTransaction objTrans = null;
			Entity objEntity = null;
			XmlDocument objXML = null;
			XmlNodeList objCompanyInfoNode = null;
			XmlElement objXmlElem = null;
			XmlNodeList objOperationInfoList = null;
			XmlNodeList objEntityXContactInfoList = null;
			XmlNodeList objEntityXSelfInsuredList = null;
			XmlNodeList objEntityXExposureList = null;
			//XmlNodeList objJurisAndLicenseCodesList=null;
			XmlNodeList objClientLimitsList = null;
			XmlNode objExposureInfo = null;
			XmlNode objOperation = null;
			XmlNode objContact = null;
			XmlNode objSelfInsured = null;
			XmlNode obj_ClientLimits = null;
			EntityXOperatingAs[] objEntityXOperatingAs = null;
			EntityXContactInfo[] objEntityXContactInfo = null;
			EntityXSelfInsured[] objEntityXSelfInsured = null;
			EntityXExposure[] objEntityExposure = null;
			ClientLimits[] objClientLimits = null;
			// JP 12-05-2005   Cleanup. Not used.    DbFactory objDbFactory=null;
			DbReader objReader = null;
			//JurisAndLicenseCodes [] objJurisAndLicense = null;
			// JP 12-05-2005   Cleanup. Not used.    int iCount=0;
			int iEntityTableId = 0;

			// JP 12-05-2005   Cleanup. Not used.    bool bRetVal = false;
			string strRetVal = "";
			string sSQLQuery = string.Empty;

			//Anurag - Defect #001586
			XmlDocument objSuppXMlDoc = new XmlDocument();

			try
			{
				objXML = new XmlDocument();
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);//sharishkumar Rmacloud
				objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);

				//Load the company information in a DOM object for further processing.
				objXML.LoadXml(p_sCompanyInfo);
				objCompanyInfoNode = objXML.GetElementsByTagName("DepartmentInfo");

				//In case of invalid XML throw error
				if (objCompanyInfoNode == null)
                    throw new XmlOperationException(Globalization.GetString("OrgHierarchy.InvalidXML", m_iClientId));//sharishkumar Rmacloud

				//Begin Transaction.
				//objTrans = objDMF.Context.DbConn.BeginTransaction();
				//objDMF.Context.DbTrans = objTrans;

				for (int iCnt = 0; iCnt < objCompanyInfoNode.Count; iCnt++)
				{
					objXmlElem = (XmlElement)objCompanyInfoNode[iCnt];

					//Move to the selected enity
					if (p_iEntityId > 0)
						objEntity.MoveTo(p_iEntityId);

					//Set the Enity columns
					objEntity.BusinessTypeCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("BusinessTypeCode")[0].Attributes["codeid"].InnerText);
					objEntity.County = objXmlElem.GetElementsByTagName("County")[0].InnerText;
					objEntity.LastName = objXmlElem.GetElementsByTagName("LastName")[0].InnerText;
					//MITS_8517  Umesh    12/12/2006
					objEntity.LastName = objEntity.LastName;
					//MITS_8517  Umesh
					objEntity.NatureOfBusiness = objXmlElem.GetElementsByTagName("NatureOfBusiness")[0].InnerText;
					objEntity.SicCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("SicCode")[0].Attributes["codeid"].InnerText);
					objEntity.SicCodeDesc = objXmlElem.GetElementsByTagName("SicCodeDesc")[0].InnerText;
					objEntity.NaicsCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("NAICSCode")[0].Attributes["codeid"].InnerText);
					objEntity.WcFillingNumber = objXmlElem.GetElementsByTagName("WCFillingNumber")[0].InnerText;
					objEntity.EntityTableId = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("EntityTableId")[0].InnerText);
					iEntityTableId = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("EntityTableId")[0].InnerText);
					objEntity.AlsoKnownAs = objXmlElem.GetElementsByTagName("AlsoKnownAs")[0].InnerText;
					objEntity.Abbreviation = objXmlElem.GetElementsByTagName("Abbreviation")[0].InnerText;
					//MITS_8517  Umesh   12/12/2006
					objEntity.Abbreviation = objEntity.Abbreviation.Replace("'", "''");
					//MITS_8517  Umesh
					sSQLQuery = "SELECT ENTITY_ID FROM ENTITY WHERE ABBREVIATION LIKE '" + objEntity.Abbreviation + "' AND ENTITY_TABLE_ID='" + objEntity.EntityTableId + "'";
					objReader = DbFactory.GetDbReader(p_sConnectionString, sSQLQuery);
					if (p_iEntityId <= 0)
					{
						if (objReader != null)
						{
							if (objReader.Read())
							{
                                throw new RMAppException(Globalization.GetString("OrgHierarchy.UpdateCompanyInformation.AlreadyExist", m_iClientId));//sharishkumar Rmacloud
							}
						}
					}
					objEntity.CostCenterCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("CCCode")[0].InnerText);
					objEntity.Addr1 = objXmlElem.GetElementsByTagName("Addr1")[0].InnerText;
					objEntity.Addr2 = objXmlElem.GetElementsByTagName("Addr2")[0].InnerText;
                    objEntity.Addr3 = objXmlElem.GetElementsByTagName("Addr3")[0].InnerText;
                    objEntity.Addr4 = objXmlElem.GetElementsByTagName("Addr4")[0].InnerText;
					objEntity.City = objXmlElem.GetElementsByTagName("City")[0].InnerText;
					objEntity.CountryCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("CountryCode")[0].Attributes["codeid"].InnerText);
					objEntity.Abbreviation = objXmlElem.GetElementsByTagName("Abbreviation")[0].InnerText;
					//MITS_8517  Umesh  12/12/2006
					objEntity.Abbreviation = objEntity.Abbreviation;
					//MITS_8517  Umesh
					objEntity.StateId = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("StateId")[0].Attributes["codeid"].InnerText);
					objEntity.ZipCode = objXmlElem.GetElementsByTagName("ZipCode")[0].InnerText;
					objEntity.ParentEid = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("ParentEID")[0].InnerText);
					objEntity.TaxId = objXmlElem.GetElementsByTagName("TaxID")[0].InnerText;
					objEntity.Contact = objXmlElem.GetElementsByTagName("Contact")[0].InnerText;
					objEntity.Instructions.EntInstrText = objXmlElem.GetElementsByTagName("ClaimInstruction")[0].InnerText;
					objEntity.Instructions.EntityId = p_iEntityId;
					objEntity.EmailTypeCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("EmailTypeCode")[0].Attributes["codeid"].InnerText);
					objEntity.EmailAddress = objXmlElem.GetElementsByTagName("EmailAddress")[0].InnerText;
					objEntity.SexCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("SexCode")[0].InnerText);
					objEntity.BirthDate = objXmlElem.GetElementsByTagName("BirthDate")[0].InnerText;
					objEntity.Phone1 = objXmlElem.GetElementsByTagName("Phone1")[0].InnerText;
					objEntity.Phone2 = objXmlElem.GetElementsByTagName("Phone2")[0].InnerText;
					objEntity.FaxNumber = objXmlElem.GetElementsByTagName("FaxNumber")[0].InnerText;
					objEntity.DeletedFlag = Conversion.ConvertStrToBool
						(objXmlElem.GetElementsByTagName("DeletedFlag")[0].InnerText);
					objEntity.EffStartDate = objXmlElem.GetElementsByTagName("EffectiveStartDate")[0].InnerText;
					objEntity.EffEndDate = objXmlElem.GetElementsByTagName("EffectiveEndDate")[0].InnerText;
					objEntity.TriggerDateField = objXmlElem.GetElementsByTagName("TriggerDateField")[0].InnerText;
					objEntity.RMUserId = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("RMUSerID")[0].InnerText);
					objEntity.FreezePayments = Conversion.ConvertStrToBool
						(objXmlElem.GetElementsByTagName("FreezePayments")[0].InnerText);
					//paggarwal2 - start MITS 17730
					// Time Zone Code - R7 Time Zone Enhancement
					if (objXmlElem.GetElementsByTagName("TimeZoneCode") != null
						&& objXmlElem.GetElementsByTagName("TimeZoneCode").Count > 0
						&& objXmlElem.GetElementsByTagName("TimeZoneCode")[0].Attributes["codeid"] != null)
					{
						objEntity.TimeZoneCode = Conversion.ConvertStrToInteger
							(objXmlElem.GetElementsByTagName("TimeZoneCode")[0].Attributes["codeid"].InnerText);
					}
					// Time zone tracking settings - R7 Time Zone Enhancement
					if (objXmlElem.GetElementsByTagName("TimeZoneTracking") != null
						&& objXmlElem.GetElementsByTagName("TimeZoneTracking").Count > 0)
					{
						objEntity.TimeZoneTracking = Conversion.ConvertStrToBool
							(objXmlElem.GetElementsByTagName("TimeZoneTracking")[0].InnerText);
					}
					// Day Light Savings settings - R7 Time Zone Enhancement
                    //if (objXmlElem.GetElementsByTagName("DayLightSavings") != null
                    //    && objXmlElem.GetElementsByTagName("DayLightSavings").Count > 0)
                    //{
                    //    objEntity.DayLightSavings = Conversion.ConvertStrToBool
                    //        (objXmlElem.GetElementsByTagName("DayLightSavings")[0].InnerText);
                    //}
					//paggarwal2: end - MITS 17730
                    ///Daylight system checkbox not needed anymore. The daylght savings is handled bu code now. :Yatharth

					// Start Naresh 9/20/2007 MI Forms Changes
					//Arnab: MITS-10956 - Added condition to search the existance of "OrganizationType" node in the XML
					if (objXmlElem.GetElementsByTagName("OrganizationType") != null && objXmlElem.GetElementsByTagName("OrganizationType").Count != 0)
						objEntity.OrganizationType = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("OrganizationType")[0].Attributes["codeid"].InnerText);
					// End Naresh 9/20/2007 MI Forms Changes

					objEntity.Parent1099EID = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("Parent1099EID")[0].Attributes["codeid"].InnerText);
					objEntity.Entity1099Reportable = Conversion.ConvertStrToBool
						(objXmlElem.GetElementsByTagName("Report1099Flag")[0].InnerText);
					objEntity.Title = objXmlElem.GetElementsByTagName("Title")[0].InnerText;
					objEntity.DttmRcdAdded = objXmlElem.GetElementsByTagName("DateTimeAdded")[0].InnerText;

                    //Start: rsushilaggar MITS 21454 21-Jul-2010
                    objEntity.EntityApprovalStatusCode = objXmlElem.SelectSingleNode("//EntityApprovalStatusCode")!= null ?Int32.Parse(objXmlElem.SelectSingleNode("//EntityApprovalStatusCode").InnerText):0;
                    objEntity.RejectReasonText = objXmlElem.SelectSingleNode("//RejectReasonText")!= null?objXmlElem.SelectSingleNode("//RejectReasonText").InnerText:"";
                    objEntity.RejectReasonText_HTMLComments = objXmlElem.SelectSingleNode("//RejectReasonText_HTMLComments")!=null?objXmlElem.SelectSingleNode("//RejectReasonText_HTMLComments").InnerText:"";
                    //End: rsushilaggar


					//Add OperatingAs Information
					if (objXmlElem.GetElementsByTagName("OperationInfo") != null)
					{
						objOperationInfoList =
							objXmlElem.GetElementsByTagName("OperationInfo").Item(0).SelectNodes("Operating");
						foreach (EntityXOperatingAs objOperatingas in objEntity.EntityXOperatingAsList)
						{
							objEntity.EntityXOperatingAsList.Remove(objOperatingas.OperatingId);
						}

						objEntityXOperatingAs = new EntityXOperatingAs[objOperationInfoList.Count];
						for (int iInfoList = 0; iInfoList < objOperationInfoList.Count; iInfoList++)
						{
							objOperation = objXmlElem.GetElementsByTagName("OperationInfo").Item(0).
								SelectNodes("Operating")[iInfoList];

							objEntityXOperatingAs[iInfoList] = objEntity.EntityXOperatingAsList.AddNew();
							objEntityXOperatingAs[iInfoList].Initials =
								objOperation.SelectSingleNode("Initial").InnerText;
							objEntityXOperatingAs[iInfoList].OperatingAs =
								objOperation.SelectSingleNode("OperatingName").InnerText;
							objEntityXOperatingAs[iInfoList].OperatingId =
								Conversion.ConvertStrToInteger
								(objOperation.SelectSingleNode("OperatingID").InnerText);

							//Add EntityXOperatingAs to the Entity object
							objEntity.EntityXOperatingAsList.Add(objEntityXOperatingAs[iInfoList]);
						}
					}

					//Add Contact Information.
					if (objXmlElem.GetElementsByTagName("ContactInfo") != null)
					{
						objEntityXContactInfoList =
							objXmlElem.GetElementsByTagName("ContactInfo").Item(0).SelectNodes("Contact");
						int icontactcount = objEntity.EntityXContactInfoList.Count;
						foreach (EntityXContactInfo objContactinformation in objEntity.EntityXContactInfoList)
						{
							objEntity.EntityXContactInfoList.Remove(objContactinformation.ContactId);
						}

						objEntityXContactInfo = new EntityXContactInfo[objEntityXContactInfoList.Count];
						for (int iContactList = 0; iContactList < objEntityXContactInfoList.Count; iContactList++)
						{
							objContact = objXmlElem.GetElementsByTagName("ContactInfo").Item(0).SelectNodes("Contact")[iContactList];
							objEntityXContactInfo[iContactList] = objEntity.EntityXContactInfoList.AddNew();

							objEntityXContactInfo[iContactList].ContactName =
								objContact.SelectSingleNode("ContactName").InnerText;
							objEntityXContactInfo[iContactList].Title =
								objContact.SelectSingleNode("ContactTitle").InnerText;
							objEntityXContactInfo[iContactList].Initials =
								objContact.SelectSingleNode("ContactInitials").InnerText;
							objEntityXContactInfo[iContactList].Addr1 =
								objContact.SelectSingleNode("Addr1").InnerText;
							objEntityXContactInfo[iContactList].Addr2 =
								objContact.SelectSingleNode("Addr2").InnerText;
                            objEntityXContactInfo[iContactList].Addr3 =
                                objContact.SelectSingleNode("Addr3").InnerText;
                            objEntityXContactInfo[iContactList].Addr4 =
                                objContact.SelectSingleNode("Addr4").InnerText;
							objEntityXContactInfo[iContactList].City =
								objContact.SelectSingleNode("ContactCity").InnerText;
							objEntityXContactInfo[iContactList].State =
								Conversion.ConvertStrToInteger(objContact.SelectSingleNode("State").Attributes["codeid"].InnerText);
							objEntityXContactInfo[iContactList].ZipCode =
								objContact.SelectSingleNode("ZipCode").InnerText;
							objEntityXContactInfo[iContactList].Phone =
								objContact.SelectSingleNode("Phone").InnerText;
							objEntityXContactInfo[iContactList].Fax =
								objContact.SelectSingleNode("FaxNumber").InnerText;
							objEntityXContactInfo[iContactList].Email =
								objContact.SelectSingleNode("EMailAddress").InnerText;

							//Add EntityXContactInfo to the Entity object
							objEntity.EntityXContactInfoList.Add(objEntityXContactInfo[iContactList]);
						}
					}

					if (iEntityTableId >= 1005 && iEntityTableId <= 1012)// Check whether its OrgHierarchy Entity Entry or not
					{
						//Add Exposure information.

						if (objXmlElem.GetElementsByTagName("ExposureInfo") != null)
						{
							objEntityXExposureList = objXmlElem.SelectNodes("ExposureInfo");
							foreach (EntityXExposure objExp in objEntity.EntityXExposureList)
							{
								objEntity.EntityXExposureList.Remove(objExp.ExposureRowId);
							}
							objExposureInfo = objXmlElem.GetElementsByTagName("ExposureInfo").Item(0);

							objEntityExposure = new EntityXExposure[objEntityXExposureList.Count];
							objEntityExposure[0] = objEntity.EntityXExposureList.AddNew();
							objEntityExposure[0].StartDate = objExposureInfo.SelectSingleNode("StartDate").InnerText;
							objEntityExposure[0].EndDate = objExposureInfo.SelectSingleNode("EndDate").InnerText;
							objEntityExposure[0].NoOfEmployees =
								Conversion.ConvertStrToInteger(objExposureInfo.SelectSingleNode("NoOfEmployees").InnerText);
							objEntityExposure[0].NoOfWorkHours =
								Conversion.ConvertStrToInteger(objExposureInfo.SelectSingleNode("WorkHrs").InnerText);
							objEntityExposure[0].PayrollAmount =
								Conversion.ConvertStrToDouble(objExposureInfo.SelectSingleNode("PayRollAmt").InnerText);
							objEntityExposure[0].AssetValue =
								Conversion.ConvertStrToDouble(objExposureInfo.SelectSingleNode("AssetValue").InnerText);
							objEntityExposure[0].SquareFootage =
								Conversion.ConvertStrToInteger(objExposureInfo.SelectSingleNode("SquareFootage").InnerText);
							objEntityExposure[0].VehicleCount =
								Conversion.ConvertStrToInteger(objExposureInfo.SelectSingleNode("VehicleCount").InnerText);
							objEntityExposure[0].TotalRevenue =
								Conversion.ConvertStrToDouble(objExposureInfo.SelectSingleNode("TotalRevenue").InnerText);
							objEntityExposure[0].OtherBase =
								Conversion.ConvertStrToDouble(objExposureInfo.SelectSingleNode("OtherBase").InnerText);
							objEntityExposure[0].RiskMgmtOverhead =
								Conversion.ConvertStrToDouble(objExposureInfo.SelectSingleNode("RiskMgtOverHead").InnerText);
							objEntityExposure[0].UserGeneratedFlag =
								//Conversion.ConvertStrToBool (objExposureInfo.SelectSingleNode("UserGeneratedFlag").InnerText);
								Conversion.ConvertStrToBool(objExposureInfo.SelectSingleNode("UserGeneratedFlag").InnerText);

							//Add EntityXExposure to the Entity object

							objEntity.EntityXExposureList.Add(objEntityExposure[0]);

						}

						//Add Self Insured information

						if (objXmlElem.GetElementsByTagName("SelfInsuredInfo") != null)
						{
							objEntityXSelfInsuredList =
								objXmlElem.GetElementsByTagName("SelfInsuredInfo").Item(0).SelectNodes("SelfInsured");
							int iselfcount = objEntity.EntityXSelfInsuredList.Count;
							foreach (EntityXSelfInsured objSelf in objEntity.EntityXSelfInsuredList)
							{
								objEntity.EntityXSelfInsuredList.Remove(objSelf.SIRowId);
							}

							objEntityXSelfInsured = new EntityXSelfInsured[objEntityXSelfInsuredList.Count];

							for (int iInsuredList = 0; iInsuredList < objEntityXSelfInsuredList.Count; iInsuredList++)
							{
								objSelfInsured =
									objXmlElem.GetElementsByTagName("SelfInsuredInfo").Item(0).SelectNodes("SelfInsured")[iInsuredList];

								int iSelfRowID = Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("SIRowId").InnerText);

								objEntityXSelfInsured[iInsuredList] = objEntity.EntityXSelfInsuredList.AddNew();

								objEntityXSelfInsured[iInsuredList].Jurisdiction =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("Jurisdiction").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].DeletedFlag =
									Conversion.ConvertStrToBool(objSelfInsured.SelectSingleNode("DeletedFlag").InnerText);
								objEntityXSelfInsured[iInsuredList].LegalEntityId =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("LegalEntityId").InnerText);
								//Added by: Mohit for Bug No. 000078******************************************************
								objEntityXSelfInsured[iInsuredList].CertNameEid =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("CertNameEid").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].CertificateNumber =
									objSelfInsured.SelectSingleNode("CertificateNumber").InnerText;
								objEntityXSelfInsured[iInsuredList].EffectiveDate =
									objSelfInsured.SelectSingleNode("EffectiveDate").InnerText;
								objEntityXSelfInsured[iInsuredList].ExpirationDate =
									objSelfInsured.SelectSingleNode("ExpirationDate").InnerText;
								objEntityXSelfInsured[iInsuredList].Authorization =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("Authorization").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].Organization =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("Organization").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].DttmRcdAdded =
									objSelfInsured.SelectSingleNode("DttmRcdAdded").InnerText;
								objEntityXSelfInsured[iInsuredList].DttmRcdLastUpd =
									objSelfInsured.SelectSingleNode("DttmRcdLastUpd").InnerText;
								objEntityXSelfInsured[iInsuredList].UpdatedByUser =
									objSelfInsured.SelectSingleNode("UpdatedByUser").InnerText;
								objEntityXSelfInsured[iInsuredList].AddedByUser =
									objSelfInsured.SelectSingleNode("AddedByUser").InnerText;

								//Add EntityXSelfInsured to the Entity object
								objEntity.EntityXSelfInsuredList.Add(objEntityXSelfInsured[iInsuredList]);
							}
							//Add Client_Limits Data
							if (objXmlElem.GetElementsByTagName("ClientLimitsInfo") != null)
							{
								objClientLimitsList =
									objXmlElem.GetElementsByTagName("ClientLimitsInfo").Item(0).SelectNodes("ClientLimits");
								int clientcount = objEntity.ClientLimitsList.Count;
								foreach (ClientLimits objClients in objEntity.ClientLimitsList)
								{
									objEntity.ClientLimitsList.Remove(objClients.ClientLimitsRowId);
								}

								objClientLimits = new ClientLimits[objClientLimitsList.Count];

								for (int iLimitsList = 0; iLimitsList < objClientLimitsList.Count; iLimitsList++)
								{
									obj_ClientLimits =
										objXmlElem.GetElementsByTagName("ClientLimitsInfo").Item(0).SelectNodes("ClientLimits")[iLimitsList];
									int iLimitsID = Conversion.ConvertStrToInteger(obj_ClientLimits.SelectSingleNode("Client_RowId").InnerText);
									objClientLimits[iLimitsList] = objEntity.ClientLimitsList.AddNew();
									objClientLimits[iLimitsList].MaxAmount =
										Conversion.ConvertStrToInteger(obj_ClientLimits.SelectSingleNode("MAX_AMOUNT").InnerText);
									objClientLimits[iLimitsList].LobCode = Conversion.ConvertStrToInteger(obj_ClientLimits.SelectSingleNode("LOB_CODE").Attributes["codeid"].InnerText);
									objClientLimits[iLimitsList].ReserveTypeCode = Conversion.ConvertStrToInteger(obj_ClientLimits.SelectSingleNode("RESERVE_TYPE_CODE").Attributes["codeid"].InnerText);
									objClientLimits[iLimitsList].PaymentFlag = Conversion.ConvertStrToInteger(obj_ClientLimits.SelectSingleNode("PAYMENT_FLAG").InnerText);
									objClientLimits[iLimitsList].ClientEid = Conversion.ConvertStrToInteger(objEntity.EntityId.ToString());

									//Add ClientLimits to the Entity object
									objEntity.ClientLimitsList.Add(objClientLimits[iLimitsList]);
								}
							}
						}
						else
						{
							//Add Jurisdiction Information
							/*	if (objXmlElem.GetElementsByTagName ("JurisAndLicenInfo") != null)
								{
									objJurisAndLicenseCodesList = 
										objXmlElem.GetElementsByTagName("JurisAndLicenInfo").Item(0).SelectNodes("JurisAndLicen");
						
									for (int i=0; i < objJurisAndLicenseCodesList.Count ; i++)
									{
										objJurisAndLicenseCodes = objXmlElem.GetElementsByTagName("JurisAndLicenInfo").Item(0).
											SelectNodes("JurisAndLicen")[i];

										objEntity.JurisAndLicenseCodesList.Delete(Conversion.ConvertStrToInteger 
											(objJurisAndLicenseCodes.SelectSingleNode("TableRowId").InnerXml));  
									}

									objJurisAndLicense = new JurisAndLicenseCodes [objJurisAndLicenseCodesList.Count];
									for (int iInfoList=0; iInfoList < objJurisAndLicenseCodesList.Count ; iInfoList++)
									{
										objJurisAndLicenseCodes = objXmlElem.GetElementsByTagName("JurisAndLicenInfo").Item(0).SelectNodes("JurisAndLicen")[iInfoList];

										objJurisAndLicense [iInfoList] = objEntity.JurisAndLicenseCodesList.AddNew();
										objJurisAndLicense [iInfoList].Jurisdiction = Conversion.ConvertStrToInteger (objJurisAndLicenseCodes.SelectSingleNode("Jurisdiction").Attributes["codeid"].InnerXml);
										objJurisAndLicense [iInfoList].CodeLicenseNumber=objJurisAndLicenseCodes.SelectSingleNode("CodeLicenseNumber").InnerXml;
										objJurisAndLicense [iInfoList].EffectiveDate=objJurisAndLicenseCodes.SelectSingleNode("EffectiveDate").InnerXml;
										objJurisAndLicense [iInfoList].ExpirationDate=objJurisAndLicenseCodes.SelectSingleNode("ExpirationDate").InnerXml;
										objJurisAndLicense [iInfoList].GeneralClaims= Conversion.ConvertStrToInteger (objJurisAndLicenseCodes.SelectSingleNode("GeneralClaims").Attributes["codeid"].InnerXml);
										objJurisAndLicense [iInfoList].ShortTermDisability=Conversion.ConvertStrToInteger (objJurisAndLicenseCodes.SelectSingleNode("ShortTermDisability").Attributes["codeid"].InnerXml);
										objJurisAndLicense [iInfoList].VehicleAccident= Conversion.ConvertStrToInteger (objJurisAndLicenseCodes.SelectSingleNode("VehicleAccident").Attributes["codeid"].InnerXml);
										objJurisAndLicense [iInfoList].WorkersCompensation= Conversion.ConvertStrToInteger (objJurisAndLicenseCodes.SelectSingleNode("WorkersCompensation").Attributes["codeid"].InnerXml);
										objJurisAndLicense [iInfoList].AllLineOfBusiness= Conversion.ConvertStrToInteger (objJurisAndLicenseCodes.SelectSingleNode("AllLineOfBusiness").Attributes["codeid"].InnerXml);
										objJurisAndLicense [iInfoList].DttmRcdAdded=objJurisAndLicenseCodes.SelectSingleNode("DttmRcdAdded").InnerXml;
										objJurisAndLicense [iInfoList].DttmRcdLastUpd=objJurisAndLicenseCodes.SelectSingleNode("DttmRcdLastUpd").InnerXml;
										objJurisAndLicense [iInfoList].UpdatedByUser=objJurisAndLicenseCodes.SelectSingleNode("UpdatedByUser").InnerXml;
										objJurisAndLicense [iInfoList].AddedByUser=objJurisAndLicenseCodes.SelectSingleNode("AddedByUser").InnerXml;									
										objJurisAndLicense [iInfoList].DeletedFlag=Conversion.ConvertStrToBool (objJurisAndLicenseCodes.SelectSingleNode("DeletedFlag").InnerXml);

										//Add EntityXOperatingAs to the Entity object
										objEntity.JurisAndLicenseCodesList.Add (objJurisAndLicense [iInfoList]);
									}
								}*/
						}
					}

					//Defect #001586 - suplemental option is not available in the Organization Hierarchy.
					//Added code for saving supp information along with Org Maint XML.
					XmlNode objSuppNode = objXML.SelectSingleNode("//Supplementals");
					if (objSuppNode != null && objSuppNode.InnerXml != "")
					{
						objSuppXMlDoc.LoadXml(objSuppNode.OuterXml);
						objEntity.Supplementals.PopulateObject(objSuppXMlDoc);
					}
					objSuppNode = null;

					//Save Department information.

					objEntity.Save();

					//Set to true if Save operation is successful
					// JP 12-05-2005   Cleanup. Not used.    bRetVal = true;
					strRetVal = objEntity.EntityId.ToString();
				}
			}

			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("OrgHierarchy.UpdateCompanyInformation.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDMF != null) objDMF.Dispose();
				if (objReader != null)
				{
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
					objReader.Dispose();
				}
				if (objEntity != null) objEntity.Dispose();
				objXML = null;
				objCompanyInfoNode = null;
				objXmlElem = null;
				objOperationInfoList = null;
				objEntityXContactInfoList = null;
				objEntityXSelfInsuredList = null;
				objExposureInfo = null;
				objOperation = null;
				objContact = null;
				objSelfInsured = null;
				objEntityXOperatingAs = null;
				objEntityXContactInfo = null;
				objEntityXSelfInsured = null;
				objEntityExposure = null;
				objSuppXMlDoc = null;
			}
			return (strRetVal);
		}

		#endregion

		#region To get Specific Data from Table name provided as parameter
		/// Name		: GetSpecificData
		/// Author		: Rahul Sharma
		/// Date Created: 06/29/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		/// 
		///*************************************************************************
		/// <summary>
		///Get Specific data from Table Name provided
		/// </summary>
		/// <param name="p_sInputXml">XML string containing table information to be retrieved.</param>
		/// 
		/// <returns>True if department is added successfully else false</returns>
		public string GetSpecificData(string p_sInputXml)
		{
			XmlDocument objXML = null;
			// JP 12-05-2005   Cleanup. Not used.    XmlDocument objTemp = null;
			//	XmlDocument objInstance = new XmlDocument();
			DataSet objDataSet = null;
			DbConnection objConn = null;
			DbTransaction objTrans = null;
			XmlDocument objXmlDoc = new XmlDocument();
			XmlDocument tempDoc = new XmlDocument();
			XmlDocument objResultDoc = new XmlDocument();
			// JP 12-05-2005   Cleanup. Not used.    XmlElement objXmlElem=null;
			LocalCache objCache = null;
			string sSQL = "";
			string sLevel = "";
			string sTable = null;
			string sParam = null;
			string sAbbr = "";
			string sName = "";
			// JP 12-05-2005   Cleanup. Not used.    int iFlag=0;

			try
			{
				objDataSet = new DataSet();
				objXML = new XmlDocument();
				objXML.LoadXml(p_sInputXml);
				objCache = new LocalCache(m_sConnectionString,m_iClientId);
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				if (objConn.DatabaseType.ToString() == Constants.DB_ORACLE)
				{
					objTrans = objConn.BeginTransaction();
				}
				sParam = "(";
				sLevel = Conversion.ConvertObjToStr(objXML.SelectSingleNode("//rowid").InnerXml.ToString());
				sTable = Conversion.ConvertObjToStr(objXML.SelectSingleNode("//tablename").InnerXml.ToString());
				//objInstance.LoadXml(objXML.SelectSingleNode("//InnerDoc").InnerXml);
				string[] sChildRow = sLevel.Split('|');
				if (sChildRow.Length == 1)
				{
					sParam = sParam + sChildRow[0].ToString();
				}
				else
				{
					if (sChildRow.Length == 0)
					{
						sParam = sParam + sLevel;
					}
					else
					{
						// JP 12-05-2005   Cleanup. Not used.    iFlag=1;
						for (int i = 0; i < sChildRow.Length; i++)
						{
							if (sChildRow[i] != "")
								sParam = sParam + sChildRow[i] + ",";
						}
						sParam = sParam.Remove(sParam.Length - 1, 1);
					}
				}

				sParam = sParam + ")";
				XmlElement objtemp = objResultDoc.CreateElement("xml");
				if (sTable == "CLIENT_LIMITS")
				{
                    sSQL = "Select * from CLIENT_LIMITS where CLIENT_LIMITS_ROWID IN " + sParam;
					objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
					tempDoc.AppendChild(tempDoc.CreateElement("Client_RowId"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["CLIENT_LIMITS_ROWID"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Client_Eid"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["CLIENT_EID"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					//					tempDoc.AppendChild(tempDoc.CreateElement("Authorization"));
					//					tempDoc.DocumentElement.SetAttribute ("codeid", objDataSet.Tables[0].Rows[0]["SI_AUTHORIZATION"].ToString());				
					//					objCache.GetCodeInfo (int.Parse(objDataSet.Tables[0].Rows[0]["SI_AUTHORIZATION"].ToString()), ref sAbbr, ref sName);
					//					tempDoc.DocumentElement.InnerXml=sAbbr + " " + sName;
					tempDoc.AppendChild(tempDoc.CreateElement("LOB_CODE"));
					tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["LOB_CODE"].ToString());
					objCache.GetCodeInfo(int.Parse(objDataSet.Tables[0].Rows[0]["LOB_CODE"].ToString()), ref sAbbr, ref sName);
					tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("RESERVE_TYPE_CODE"));
					tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["RESERVE_TYPE_CODE"].ToString());
					objCache.GetCodeInfo(int.Parse(objDataSet.Tables[0].Rows[0]["RESERVE_TYPE_CODE"].ToString()), ref sAbbr, ref sName);
					tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("MAX_AMOUNT"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["MAX_AMOUNT"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("PAYMENT_FLAG"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["PAYMENT_FLAG"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Type_Of_Limit"));
					string typeoflimit = "";
					if (objDataSet.Tables[0].Rows[0]["PAYMENT_FLAG"].ToString() == "0")
					{
						typeoflimit = "Reserve";
					}
					else
					{
						typeoflimit = "Payment";
					}
					tempDoc.DocumentElement.InnerXml = typeoflimit;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					objResultDoc.AppendChild(objtemp);
				}else
				if (sTable == "ENT_X_CONTACTINFO")
				{
                    sSQL = "Select * from ENT_X_CONTACTINFO where CONTACT_ID=" + sParam;
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);

					tempDoc.AppendChild(tempDoc.CreateElement("ContactID"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["CONTACT_ID"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("ContactName"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["CONTACT_NAME"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("ContactTitle"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["TITLE"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("ContactInitials"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["INITIALS"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Addr1"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["ADDR1"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Addr2"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["ADDR2"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
                    tempDoc.AppendChild(tempDoc.CreateElement("Addr3"));
                    tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["ADDR3"].ToString();
                    objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
                    tempDoc = new XmlDocument();
                    tempDoc.AppendChild(tempDoc.CreateElement("Addr4"));
                    tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["ADDR4"].ToString();
                    objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
                    tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("ContactCity"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["CITY"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("State"));
					tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["STATE_ID"].ToString());
					objCache.GetStateInfo(int.Parse(objDataSet.Tables[0].Rows[0]["STATE_ID"].ToString()), ref sAbbr, ref sName);
					tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("ZipCode"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["ZIP_CODE"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Phone"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["PHONE1"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("FaxNumber"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["FAX_NUMBER"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("EMailAddress"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["EMAIL_ADDRESS"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();

					//	}		
					objResultDoc.AppendChild(objtemp);

				}else
				if (sTable == "ENT_X_OPERATINGAS")
				{
                    sSQL = "Select * from ENT_X_OPERATINGAS where OPERATING_ID=" + sParam;
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
					tempDoc.AppendChild(tempDoc.CreateElement("OperatingID"));
                    //tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["OPERATING_ID"].ToString();
                    tempDoc.DocumentElement.InnerXml = sParam;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Initial"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["INITIALS"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("OperatingName"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["OPERATING_AS"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					objResultDoc.AppendChild(objtemp);
				}
				//////				if(sTable=="ENTITY_EXPOSURE")
				//////				{
				//////					sSQL="Select * from "+sTable+" where EXPOSURE_ROW_ID="+sParam;
				//////					objDataSet = DbFactory.GetDataSet(m_sConnectionString,sSQL);
				//////					tempDoc.AppendChild(tempDoc.CreateElement("ExposureRowID"));
				//////					tempDoc.DocumentElement.InnerXml=objDataSet.Tables[0].Rows[0]["OPERATING_ID"].ToString();
				//////					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml ;
				//////					tempDoc=new XmlDocument();
				//////					tempDoc.AppendChild(tempDoc.CreateElement("Initial"));
				//////					tempDoc.DocumentElement.InnerXml=objDataSet.Tables[0].Rows[0]["INITIALS"].ToString();
				//////					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml ;
				//////					tempDoc=new XmlDocument();
				//////					tempDoc.AppendChild(tempDoc.CreateElement("OperatingName"));
				//////					tempDoc.DocumentElement.InnerXml=objDataSet.Tables[0].Rows[0]["OPERATING_AS"].ToString();
				//////					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml ;
				//////					tempDoc=new XmlDocument();
				//////					objResultDoc.AppendChild( objtemp );
				//////				}
				else if (sTable == "ENTITY_X_SELFINSUR")
				{
                    sSQL = "Select * from ENTITY_X_SELFINSUR where SI_ROW_ID=" + sParam;
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
					tempDoc.AppendChild(tempDoc.CreateElement("SIRowId"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["SI_ROW_ID"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("EntityId"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["ENTITY_ID"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					//					tempDoc.DocumentElement.SetAttribute ("codeid", objDataSet.Tables[0].Rows[0]["STATE_ID"].ToString());
					//					objCache.GetStateInfo (int.Parse(objDataSet.Tables[0].Rows[0]["STATE_ID"].ToString()), ref sAbbr, ref sName);
					//					tempDoc.DocumentElement.InnerXml=sAbbr + " " + sName;
					tempDoc.AppendChild(tempDoc.CreateElement("Jurisdiction"));
					tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["JURIS_ROW_ID"].ToString());
					objCache.GetStateInfo(int.Parse(objDataSet.Tables[0].Rows[0]["JURIS_ROW_ID"].ToString()), ref sAbbr, ref sName);
					tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("DeletedFlag"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["DELETED_FLAG"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("LegalEntityId"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["LEGAL_ENTITY_EID"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					//Changed by:Mohit for Bug No. 000078*********************
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("CertNameEid"));
					tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["CERT_NAME_EID"].ToString());
					tempDoc.DocumentElement.InnerXml = objCache.GetEntityLastFirstName(int.Parse(objDataSet.Tables[0].Rows[0]["CERT_NAME_EID"].ToString()));
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;

					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("CertificateNumber"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["CERT_NUMBER"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("EffectiveDate"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["EFFECTIVE_DATE"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("ExpirationDate"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["EXPIRATION_DATE"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Authorization"));
					tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["SI_AUTHORIZATION"].ToString());
					objCache.GetCodeInfo(int.Parse(objDataSet.Tables[0].Rows[0]["SI_AUTHORIZATION"].ToString()), ref sAbbr, ref sName);
					tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("Organization"));
					tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["SI_ORGANIZATION"].ToString());
					objCache.GetCodeInfo(int.Parse(objDataSet.Tables[0].Rows[0]["SI_ORGANIZATION"].ToString()), ref sAbbr, ref sName);
					tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("DttmRcdAdded"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["DTTM_RCD_ADDED"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("DttmRcdLastUpd"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["DTTM_RCD_LAST_UPD"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("UpdatedByUser"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["UPDATED_BY_USER"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("AddedByUser"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["ADDED_BY_USER"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					tempDoc.AppendChild(tempDoc.CreateElement("IsCertDiffFlag"));
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["IS_CERT_DIFF_FLAG"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();
					//sgoel6 Medicare 05/13/2009
					//tempDoc.AppendChild(tempDoc.CreateElement("LineOfBusCodeSelfInsured"));
					//tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString());
					//objCache.GetCodeInfo(int.Parse(objDataSet.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString()), ref sAbbr, ref sName);
					//if (int.Parse(objDataSet.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString()) == -3)
					//    tempDoc.DocumentElement.InnerXml = "All All";
					//else
					//    tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;
					//objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					//tempDoc = new XmlDocument();

					tempDoc.AppendChild(tempDoc.CreateElement("LobCode"));
					//tempDoc.DocumentElement.SetAttribute("codeid", objDataSet.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString());
					//objCache.GetCodeInfo(int.Parse(objDataSet.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString()), ref sAbbr, ref sName);
					//if (int.Parse(objDataSet.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString()) == -3)
					//    tempDoc.DocumentElement.InnerXml = "All";
					//else
					//    tempDoc.DocumentElement.InnerXml = sAbbr + " " + sName;                    
					tempDoc.DocumentElement.InnerXml = objDataSet.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString();
					objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
					tempDoc = new XmlDocument();




					objResultDoc.AppendChild(objtemp);
				}
				//Fill the dataset
				//	objDataSet = DbFactory.GetDataSet(m_sConnectionString,sSQL);			
				//	objXmlDoc.LoadXml(objDataSet.GetXml().ToString());
			}
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetSpecificData.GeneralError",m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDataSet != null)
				{
					objDataSet.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn.Dispose();
				}
				if (objTrans != null)
				{
					objTrans.Dispose();
				}
				if (objCache != null)
				{
					objCache.Dispose();
				}
				objXML = null;
				objXmlDoc = null;
				sSQL = null;
			}
			//	return (objXmlDoc.InnerXml);
			return (objResultDoc.InnerXml);

		}

		#endregion

		#region Add Company information
		/// Name		: AddCompanyInformation
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		/// 14-Mar-2005		   To implement Edit Company Info,				Nikhil Garg
		///					   1. Passed EntityId as function parameters
		///					   2. Added lines for MoveTo the particular
		///						  entity.
		///*************************************************************************
		/// <summary>
		/// Add the company information in the database.
		/// </summary>
		/// <param name="p_sCompanyInfo">XML string containing company information to be added.</param>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DSN name</param>
		/// <returns>True if department is added successfully else false</returns>
		public string AddCompanyInformation(int p_iEntityId, string p_sCompanyInfo, string p_sUserName,
			string p_sPassword, string p_sDsnName)
		{
			DataModelFactory objDMF = null;
			Entity objEntity = null;
			XmlDocument objXML = null;
			XmlNodeList objCompanyInfoNode = null;
			XmlElement objXmlElem = null;
			XmlNodeList objOperationInfoList = null;
			XmlNodeList objEntityXContactInfoList = null;
			XmlNodeList objEntityXSelfInsuredList = null;
			// JP 12-05-2005   Cleanup. Not used.    XmlNodeList objClientLimitsList=null;
			XmlNode objExposureInfo = null;
			XmlNode objOperation = null;
			XmlNode objContact = null;
			XmlNode objSelfInsured = null;
			// JP 12-05-2005   Cleanup. Not used.    XmlNode obj_ClientLimits=null;
			// JP 12-05-2005   Cleanup. Not used.    EntityXOperatingAs [] objEntityXOperatingAs  = null;
			EntityXContactInfo[] objEntityXContactInfo = null;
			EntityXSelfInsured[] objEntityXSelfInsured = null;
			// JP 12-05-2005   Cleanup. Not used.    EntityXExposure objEntityExposure = null;
			// JP 12-05-2005   Cleanup. Not used.    ClientLimits [] objClientLimits = null;


			// JP 12-05-2005   Cleanup. Not used.    bool bRetVal = false;
			string strRetVal = "";

			try
			{
				objXML = new XmlDocument();
				objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword,m_iClientId);//sharishkumar Rmacloud
				objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);

				//Load the company information in a DOM object for further processing.
				objXML.LoadXml(p_sCompanyInfo);
				objCompanyInfoNode = objXML.GetElementsByTagName("DepartmentInfo");

				//In case of invalid XML throw error
				if (objCompanyInfoNode == null)
                    throw new XmlOperationException(Globalization.GetString("OrgHierarchy.InvalidXML", m_iClientId));//sharishkumar Rmacloud

				for (int iCnt = 0; iCnt < objCompanyInfoNode.Count; iCnt++)
				{
					objXmlElem = (XmlElement)objCompanyInfoNode[iCnt];

					//Added by: Nikhil Garg		Dated: 14-Mar-2005	
					//Reason: Edit functionality was missing. So added the below 2 lines
					//		  Also passed Entity-Id in the function parameters.	
					//Move to the selected enity
					if (p_iEntityId > 0)
						objEntity.MoveTo(p_iEntityId);

					//Set the Enity columns
					objEntity.BusinessTypeCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("BusinessTypeCode")[0].Attributes["codeid"].InnerText);
					objEntity.County = objXmlElem.GetElementsByTagName("County")[0].InnerText;
					objEntity.LastName = objXmlElem.GetElementsByTagName("LastName")[0].InnerText;
					objEntity.NatureOfBusiness = objXmlElem.GetElementsByTagName("NatureOfBusiness")[0].InnerText;
					objEntity.SicCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("SicCode")[0].Attributes["codeid"].InnerText);
					objEntity.SicCodeDesc = objXmlElem.GetElementsByTagName("SicCodeDesc")[0].InnerText;
					objEntity.NaicsCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("NAICSCode")[0].Attributes["codeid"].InnerText);
					objEntity.WcFillingNumber = objXmlElem.GetElementsByTagName("WCFillingNumber")[0].InnerText;
					objEntity.EntityTableId = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("EntityTableId")[0].InnerText);
					objEntity.AlsoKnownAs = objXmlElem.GetElementsByTagName("AlsoKnownAs")[0].InnerText;
					objEntity.Abbreviation = objXmlElem.GetElementsByTagName("Abbreviation")[0].InnerText;
					objEntity.CostCenterCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("CCCode")[0].InnerText);
					objEntity.Addr1 = objXmlElem.GetElementsByTagName("Addr1")[0].InnerText;
					objEntity.Addr2 = objXmlElem.GetElementsByTagName("Addr2")[0].InnerText;
                    objEntity.Addr3 = objXmlElem.GetElementsByTagName("Addr3")[0].InnerText;
                    objEntity.Addr4 = objXmlElem.GetElementsByTagName("Addr4")[0].InnerText;
					objEntity.City = objXmlElem.GetElementsByTagName("City")[0].InnerText;
					objEntity.CountryCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("CountryCode")[0].Attributes["codeid"].InnerText);
					objEntity.Abbreviation = objXmlElem.GetElementsByTagName("Abbreviation")[0].InnerText;
					objEntity.StateId = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("StateId")[0].Attributes["codeid"].InnerText);
					objEntity.ZipCode = objXmlElem.GetElementsByTagName("ZipCode")[0].InnerText;
					objEntity.ParentEid = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("ParentEID")[0].InnerText);
					objEntity.TaxId = objXmlElem.GetElementsByTagName("TaxID")[0].InnerText;
					objEntity.Contact = objXmlElem.GetElementsByTagName("Contact")[0].InnerText;
					objEntity.Comments = objXmlElem.GetElementsByTagName("Comments")[0].InnerText;
					objEntity.EmailTypeCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("EmailTypeCode")[0].Attributes["codeid"].InnerText);
					objEntity.EmailAddress = objXmlElem.GetElementsByTagName("EmailAddress")[0].InnerText;
					objEntity.SexCode = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("SexCode")[0].InnerText);
					objEntity.BirthDate = objXmlElem.GetElementsByTagName("BirthDate")[0].InnerText;
					objEntity.Phone1 = objXmlElem.GetElementsByTagName("Phone1")[0].InnerText;
					objEntity.Phone2 = objXmlElem.GetElementsByTagName("Phone2")[0].InnerText;
					objEntity.FaxNumber = objXmlElem.GetElementsByTagName("FaxNumber")[0].InnerText;
					objEntity.DeletedFlag = Conversion.ConvertStrToBool
						(objXmlElem.GetElementsByTagName("DeletedFlag")[0].InnerText);
					objEntity.EffStartDate = objXmlElem.GetElementsByTagName("EffectiveStartDate")[0].InnerText;
					objEntity.EffEndDate = objXmlElem.GetElementsByTagName("EffectiveEndDate")[0].InnerText;
					objEntity.TriggerDateField = objXmlElem.GetElementsByTagName("TriggerDateField")[0].InnerText;
					objEntity.RMUserId = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("RMUSerID")[0].InnerText);
					objEntity.FreezePayments = Conversion.ConvertStrToBool
						(objXmlElem.GetElementsByTagName("FreezePayments")[0].InnerText);
					//paggarwal2: - start MITS 17730
					// Time Zone code : R7 Time Zone Enhancement
					if (objXmlElem.GetElementsByTagName("TimeZoneCode") != null
						&& objXmlElem.GetElementsByTagName("TimeZoneCode").Count > 0
						&& (objXmlElem.GetElementsByTagName("TimeZoneCode")[0].Attributes["codeid"] != null))
					{
						objEntity.TimeZoneCode = Conversion.ConvertStrToInteger
						   (objXmlElem.GetElementsByTagName("TimeZoneCode")[0].Attributes["codeid"].InnerText);
					}
					// Enabling/Disabling Time Zone tracking
					if (objXmlElem.GetElementsByTagName("TimeZoneTracking") != null
						&& objXmlElem.GetElementsByTagName("TimeZoneTracking").Count > 0)
					{
						objEntity.TimeZoneTracking = Conversion.ConvertStrToBool
							(objXmlElem.GetElementsByTagName("TimeZoneTracking")[0].InnerText);
					}
					// Enabling/Disabling Day Light Savings
                    //if (objXmlElem.GetElementsByTagName("DayLightSavings") != null
                    //    && objXmlElem.GetElementsByTagName("DayLightSavings").Count > 0)
                    //{
                    //    objEntity.DayLightSavings = Conversion.ConvertStrToBool
                    //        (objXmlElem.GetElementsByTagName("DayLightSavings")[0].InnerText);
                    //}
					//paggarwal2: end MITS 17730
                    //Daylight system checkbox not needed anymore. The daylght savings is handled bu code now. :Yatharth

					// Start Naresh 9/20/2007 MI Forms Changes
					//Arnab: MITS-10956 - Added condition to search the existance of "OrganizationType" node in the XML
					if (objXmlElem.GetElementsByTagName("OrganizationType") != null && objXmlElem.GetElementsByTagName("OrganizationType").Count != 0)
						objEntity.OrganizationType = Conversion.ConvertStrToInteger(objXmlElem.GetElementsByTagName("OrganizationType")[0].Attributes["codeid"].InnerText);
					// End Naresh 9/20/2007 MI Forms Changes

					objEntity.Parent1099EID = Conversion.ConvertStrToInteger
						(objXmlElem.GetElementsByTagName("Parent1099EID")[0].InnerText);
					objEntity.Entity1099Reportable = Conversion.ConvertStrToBool
						(objXmlElem.GetElementsByTagName("Report1099Flag")[0].InnerText);
					objEntity.Title = objXmlElem.GetElementsByTagName("Title")[0].InnerText;
					objEntity.DttmRcdAdded = objXmlElem.GetElementsByTagName("DateTimeAdded")[0].InnerText;

					//Add OperatingAs Information
					if (objXmlElem.GetElementsByTagName("OperationInfo") != null)
					{
						objOperationInfoList =
							objXmlElem.GetElementsByTagName("OperationInfo").Item(0).SelectNodes("Operating");

						//objEntityXOperatingAs = new EntityXOperatingAs [objOperationInfoList.Count];
						int iOperatingId = 0;
						for (int iInfoList = 0; iInfoList < objOperationInfoList.Count; iInfoList++)
						{
							try
							{
								objOperation = objXmlElem.GetElementsByTagName("OperationInfo").Item(0).SelectNodes("Operating")[iInfoList];

								//objEntity.EntityXOperatingAsList[iInfoList] = objEntity.EntityXOperatingAsList.AddNew();
								iOperatingId = Conversion.ConvertStrToInteger(objOperation.SelectSingleNode("OperatingID").InnerText);
								((EntityXOperatingAs)objEntity.EntityXOperatingAsList[iOperatingId]).Initials = objOperation.SelectSingleNode("Initial").InnerText;
								((EntityXOperatingAs)objEntity.EntityXOperatingAsList[iOperatingId]).OperatingAs = objOperation.SelectSingleNode("OperatingName").InnerText;

								//EntityXOperatingAs objEXO = new EntityXOperatingAs();
								//objEXO.OperatingId = iOperatingId;
								//
								//	objEntity.EntityXOperatingAsList.Delete() .Add(objEXO);  

								//((EntityXOperatingAs)objEntity.EntityXOperatingAsList[iOperatingId]).OperatingId=objOperation.SelectSingleNode("OperatingName").InnerText;
								//Add EntityXOperatingAs to the Entity object
								//objEntity.EntityXOperatingAsList.AddNew(); (objEntityXOperatingAs [iInfoList]);
							}
							catch (Exception ex)
							{
								string sExp = ex.Message;
							}
						}
					}

					//Add Contact Information
					if (objXmlElem.GetElementsByTagName("ContactInfo") != null)
					{
						objEntityXContactInfoList =
							objXmlElem.GetElementsByTagName("ContactInfo").Item(0).SelectNodes("Contact");
						objEntityXContactInfo = new EntityXContactInfo[objEntityXContactInfoList.Count];
						for (int iContactList = 0; iContactList < objEntityXContactInfoList.Count; iContactList++)
						{
							try
							{
								objContact = objXmlElem.GetElementsByTagName("ContactInfo").Item(0).SelectNodes("Contact")[iContactList];
								objEntityXContactInfo[iContactList] = objEntity.EntityXContactInfoList.AddNew();

								objEntityXContactInfo[iContactList].ContactName =
									objContact.SelectSingleNode("ContactName").InnerText;
								objEntityXContactInfo[iContactList].Title =
									objContact.SelectSingleNode("ContactTitle").InnerText;
								objEntityXContactInfo[iContactList].Initials =
									objContact.SelectSingleNode("ContactInitials").InnerText;
								objEntityXContactInfo[iContactList].Addr1 =
									objContact.SelectSingleNode("Addr1").InnerText;
								objEntityXContactInfo[iContactList].Addr2 =
									objContact.SelectSingleNode("Addr2").InnerText;
                                objEntityXContactInfo[iContactList].Addr3 =
                                    objContact.SelectSingleNode("Addr3").InnerText;
                                objEntityXContactInfo[iContactList].Addr4 =
                                    objContact.SelectSingleNode("Addr4").InnerText;
								objEntityXContactInfo[iContactList].City =
									objContact.SelectSingleNode("ContactCity").InnerText;
								objEntityXContactInfo[iContactList].State =
									Conversion.ConvertStrToInteger(objContact.SelectSingleNode("StateID").Attributes["codeid"].InnerText);
								objEntityXContactInfo[iContactList].ZipCode =
									objContact.SelectSingleNode("ZipCode").InnerText;
								objEntityXContactInfo[iContactList].Phone =
									objContact.SelectSingleNode("Phone").InnerText;
								objEntityXContactInfo[iContactList].Fax =
									objContact.SelectSingleNode("FaxNumber").InnerText;
								objEntityXContactInfo[iContactList].Email =
									objContact.SelectSingleNode("EMailAddress").InnerText;

								//Add EntityXContactInfo to the Entity object
								objEntity.EntityXContactInfoList.Add(objEntityXContactInfo[iContactList]);
							}
							catch (Exception Ex)
							{

							}
						}
					}

					//Add Exposure information.

					if (objXmlElem.GetElementsByTagName("ExposureInfo") != null)
					{
						try
						{
							//							objOperation = objXmlElem.GetElementsByTagName("OperationInfo").Item(0).SelectNodes("Operating")[iInfoList];
							//								   						
							//							//objEntity.EntityXOperatingAsList[iInfoList] = objEntity.EntityXOperatingAsList.AddNew();
							//							iOperatingId = Conversion.ConvertStrToInteger(objOperation.SelectSingleNode("OperatingID").InnerText);
							//							((EntityXOperatingAs)objEntity.EntityXOperatingAsList[iOperatingId]).Initial = objOperation.SelectSingleNode("Initial").InnerText;
							//							((EntityXOperatingAs)objEntity.EntityXOperatingAsList[iOperatingId]).OperatingAs = objOperation.SelectSingleNode("OperatingName").InnerText;

							int iExposureRowID = 0;
							objExposureInfo = objXmlElem.GetElementsByTagName("ExposureInfo")[0];
							iExposureRowID = Conversion.ConvertStrToInteger(objExposureInfo.SelectSingleNode("ExposureRowID").InnerText);
							//							
							//objEntityExposure = objEntity.EntityXExposureList.AddNew();
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).StartDate = objOperation.SelectSingleNode("StartDate").InnerText;
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).EndDate = objOperation.SelectSingleNode("EndDate").InnerText;
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).NoOfEmployees = Conversion.ConvertStrToInteger(objOperation.SelectSingleNode("NoOfEmployees").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).NoOfWorkHours = Conversion.ConvertStrToInteger(objOperation.SelectSingleNode("WorkHrs").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).PayrollAmount = Conversion.ConvertStrToDouble(objOperation.SelectSingleNode("PayRollAmt").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).AssetValue = Conversion.ConvertStrToDouble(objOperation.SelectSingleNode("AssetValue").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).SquareFootage = Conversion.ConvertStrToInteger(objOperation.SelectSingleNode("SquareFootage").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).VehicleCount = Conversion.ConvertStrToInteger(objOperation.SelectSingleNode("VehicleCount").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).TotalRevenue = Conversion.ConvertStrToDouble(objOperation.SelectSingleNode("TotalRevenue").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).OtherBase = Conversion.ConvertStrToDouble(objOperation.SelectSingleNode("OtherBase").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).RiskMgmtOverhead = Conversion.ConvertStrToDouble(objOperation.SelectSingleNode("RiskMgtOverHead").InnerText);
							((EntityXExposure)objEntity.EntityXExposureList[iExposureRowID]).UserGeneratedFlag = Conversion.ConvertStrToBool(objExposureInfo.SelectSingleNode("UserGeneratedFlag").InnerText);
							//////							objEntityExposure.StartDate = objExposureInfo.SelectSingleNode("StartDate").InnerText;
							//////							objEntityExposure.EndDate = objExposureInfo.SelectSingleNode("EndDate").InnerText;
							//////							objEntityExposure.NoOfEmployees = 
							//////								Conversion.ConvertStrToInteger (objExposureInfo.SelectSingleNode("NoOfEmployees").InnerText);
							//////							objEntityExposure.NoOfWorkHours = 
							//////								Conversion.ConvertStrToInteger (objExposureInfo.SelectSingleNode("WorkHrs").InnerText);
							//////							objEntityExposure.PayrollAmount = 
							//////								Conversion.ConvertStrToDouble (objExposureInfo.SelectSingleNode("PayRollAmt").InnerText);
							//////							objEntityExposure.AssetValue = 
							//////								Conversion.ConvertStrToDouble (objExposureInfo.SelectSingleNode("AssetValue").InnerText);
							//////							objEntityExposure.SquareFootage = 
							//////								Conversion.ConvertStrToInteger (objExposureInfo.SelectSingleNode("SquareFootage").InnerText);
							//////							objEntityExposure.VehicleCount = 
							//////								Conversion.ConvertStrToInteger (objExposureInfo.SelectSingleNode("VehicleCount").InnerText);
							//////							objEntityExposure.TotalRevenue = 
							//////								Conversion.ConvertStrToDouble (objExposureInfo.SelectSingleNode("TotalRevenue").InnerText);
							//////							objEntityExposure.OtherBase = 
							//////								Conversion.ConvertStrToDouble (objExposureInfo.SelectSingleNode("OtherBase").InnerText);
							//////							objEntityExposure.RiskMgmtOverhead = 
							//////								Conversion.ConvertStrToDouble (objExposureInfo.SelectSingleNode("RiskMgtOverHead").InnerText);
							//////							objEntityExposure.UserGeneratedFlag = 
							//////								Conversion.ConvertStrToBool (objExposureInfo.SelectSingleNode("UserGeneratedFlag").InnerText);

							//Add EntityXExposure to the Entity object
							//objEntity.EntityXExposureList.Add (objEntityExposure);
						}
						catch (Exception ex)
						{

						}
					}

					//Add Self Insured information

					if (objXmlElem.GetElementsByTagName("SelfInsuredInfo") != null)
					{

						objEntityXSelfInsuredList =
							objXmlElem.GetElementsByTagName("SelfInsuredInfo").Item(0).SelectNodes("SelfInsured");
						objEntityXSelfInsured = new EntityXSelfInsured[objEntityXSelfInsuredList.Count];
						for (int iInsuredList = 0; iInsuredList < objEntityXSelfInsuredList.Count; iInsuredList++)
						{
							try
							{
								objSelfInsured =
									objXmlElem.GetElementsByTagName("SelfInsuredInfo").Item(0).SelectNodes("SelfInsured")[iInsuredList];
								objEntityXSelfInsured[iInsuredList] = objEntity.EntityXSelfInsuredList.AddNew();

								objEntityXSelfInsured[iInsuredList].Jurisdiction =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("Jurisdiction").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].DeletedFlag =
									Conversion.ConvertStrToBool(objSelfInsured.SelectSingleNode("DeletedFlag").InnerText);
								objEntityXSelfInsured[iInsuredList].LegalEntityId =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("LegalEntityId").InnerText);
								//Changed by: Mohit Yadav for Bug No. 000078***********************
								objEntityXSelfInsured[iInsuredList].CertNameEid =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("CertNameEid").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].CertificateNumber =
									objSelfInsured.SelectSingleNode("CertificateNumber").InnerText;
								objEntityXSelfInsured[iInsuredList].EffectiveDate =
									objSelfInsured.SelectSingleNode("EffectiveDate").InnerText;
								objEntityXSelfInsured[iInsuredList].ExpirationDate =
									objSelfInsured.SelectSingleNode("ExpirationDate").InnerText;
								objEntityXSelfInsured[iInsuredList].Authorization =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("Authorization").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].Organization =
									Conversion.ConvertStrToInteger(objSelfInsured.SelectSingleNode("Organization").Attributes["codeid"].InnerText);
								objEntityXSelfInsured[iInsuredList].DttmRcdAdded =
									objSelfInsured.SelectSingleNode("DttmRcdAdded").InnerText;
								objEntityXSelfInsured[iInsuredList].DttmRcdLastUpd =
									objSelfInsured.SelectSingleNode("DttmRcdLastUpd").InnerText;
								objEntityXSelfInsured[iInsuredList].UpdatedByUser =
									objSelfInsured.SelectSingleNode("UpdatedByUser").InnerText;
								objEntityXSelfInsured[iInsuredList].AddedByUser =
									objSelfInsured.SelectSingleNode("AddedByUser").InnerText;

								//Add EntityXSelfInsured to the Entity object
								objEntity.EntityXSelfInsuredList.Add(objEntityXSelfInsured[iInsuredList]);
							}
							catch (Exception ex)
							{

							}
						}
					}
					//Save Department information.

					objEntity.Save();

					//Set to true if Save operation is successful
					// JP 12-05-2005   Cleanup. Not used.    bRetVal = true;
					strRetVal = objEntity.EntityId.ToString();
				}
			}

			catch (XmlOperationException p_objExp)
			{
				throw p_objExp;
			}

			catch (DataModelException p_objExp)
			{
				throw p_objExp;
			}

			catch (Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("OrgHierarchy.AddCompanyInformation.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDMF != null) objDMF.Dispose();
				if (objEntity != null) objEntity.Dispose();
				objXML = null;
				objCompanyInfoNode = null;
				objXmlElem = null;
				objOperationInfoList = null;
				objEntityXContactInfoList = null;
				objEntityXSelfInsuredList = null;
				objExposureInfo = null;
				objOperation = null;
				objContact = null;
				objSelfInsured = null;
				// JP 12-05-2005   Cleanup. Not used.    objEntityXOperatingAs  = null;
				objEntityXContactInfo = null;
				objEntityXSelfInsured = null;
				// JP 12-05-2005   Cleanup. Not used.    objEntityExposure = null;
			}
			//return (bRetVal);
			return (strRetVal);
		}
		#endregion

		#region Get Parent Lists
		/// Name		: GetParentList
		/// Author		: Rahul Shrama
		/// Date Created: 06/01/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Edits the company information for the give Entity Id.
		/// </summary>
		/// <param name="p_sInputXml">XML string to be populated with the Node information</param>
		/// <returns>XML string containing List of Parent Entity Ids information</returns>
		public string GetParentList(string p_sInputXml)
		{
			int iLevel = 0;
			XmlDocument objXML = null;
			DataSet objDataSet = null;
			DbConnection objConn = null;
			DbTransaction objTrans = null;
			XmlDocument objXmlDoc = new XmlDocument();
			XmlDocument tempDoc = new XmlDocument();
			XmlDocument objResultDoc = new XmlDocument();
			XmlNodeList objXMLNodeList = null;
			string sSQL="";
			string strSearch = "";  //MITS 13997 : Umesh
			bool bOracle = false;   //MITS 13997 : Umesh

			try
			{
				objDataSet = new DataSet();
				objXML = new XmlDocument();
				objXML.LoadXml(p_sInputXml);
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				if (objConn.DatabaseType.ToString() == Constants.DB_ORACLE)
				{
					objTrans = objConn.BeginTransaction();
					bOracle = true;                  //MITS 13997 : Umesh
				}
				iLevel=Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//child_eid").InnerXml.ToString());
				//MITS 13997 : Umesh
				if (objXML.SelectSingleNode("//strSearch") != null)
					strSearch = objXML.SelectSingleNode("//strSearch").InnerText.ToString();
				strSearch = strSearch.Trim();
				strSearch = strSearch.Replace("%", "");
				if(strSearch !="")
					strSearch = "%" + strSearch + "%";
				//MITS 13997 : End
				if(iLevel==4)
				{
					sSQL = "SELECT GLOSSARY_TEXT.TABLE_ID  TABLE_ID,GLOSSARY_TEXT.TABLE_NAME  TABLE_NAME"
                        + " FROM GLOSSARY,GLOSSARY_TEXT "
                        + "where GLOSSARY.TABLE_ID=GLOSSARY_TEXT.TABLE_ID and GLOSSARY_TYPE_CODE="
                        + iLevel
                        + " ORDER BY TABLE_NAME";
					sSQL.Remove(0, sSQL.Length);
					//Fill the dataset
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
					objXmlDoc.LoadXml(objDataSet.GetXml().ToString());
					objXMLNodeList = objXmlDoc.GetElementsByTagName("Table");
					XmlElement objtemp = objResultDoc.CreateElement("xml");
					for (int iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
					{
						tempDoc.AppendChild(tempDoc.CreateElement("option"));
						if ((Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["TABLE_ID"]) != null) || (Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["TABLE_ID"]) != ""))
							tempDoc.DocumentElement.SetAttribute("value", Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["TABLE_ID"]));

						if ((Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["TABLE_NAME"]) != null) || (Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["TABLE_NAME"]) != ""))
							tempDoc.DocumentElement.InnerText = objDataSet.Tables[0].Rows[iCnt]["TABLE_NAME"].ToString();
						else
							tempDoc.DocumentElement.InnerText = "NO_ENTITY";

						objtemp.InnerXml += tempDoc.DocumentElement.OuterXml;
						tempDoc = new XmlDocument();
					}
					objResultDoc.AppendChild(objtemp);

				}
				else
				{
					iLevel=iLevel-1;
					//MITS 13997 : Umesh
                    //Mits 26213: tmalhotra2 start
					if (strSearch != "")
					{
						if (!bOracle)
						{
                            sSQL = "Select A.ABBREVIATION, A.LAST_NAME,A.ENTITY_ID FROM ENTITY A WHERE ENTITY_TABLE_ID=" + iLevel + " AND A.ABBREVIATION + '-' + A.LAST_NAME LIKE '" + strSearch + "' AND DELETED_FLAG = 0 ORDER BY A.LAST_NAME";
						}
						else
						{
                            sSQL = "Select A.ABBREVIATION, A.LAST_NAME,A.ENTITY_ID FROM ENTITY A WHERE ENTITY_TABLE_ID=" + iLevel + " AND A.ABBREVIATION || '-' || A.LAST_NAME LIKE '" + strSearch + "' AND DELETED_FLAG = 0 ORDER BY A.LAST_NAME";
						}
					}
                    //mits 26213:tmalhotra2 end
					else
					sSQL="Select A.ABBREVIATION, A.LAST_NAME,A.ENTITY_ID FROM ENTITY A WHERE ENTITY_TABLE_ID="+iLevel+" ORDER BY A.LAST_NAME";
					sSQL.Remove(0, sSQL.Length);
					//Fill the dataset
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
			
					objXmlDoc.LoadXml(objDataSet.GetXml().ToString());
					objXMLNodeList = objXmlDoc.GetElementsByTagName("Table");
					XmlElement objtemp = objResultDoc.CreateElement( "xml" );
					StringBuilder sbTemp = new StringBuilder();
					for (int iCnt=0; iCnt<objDataSet.Tables[0].Rows.Count;iCnt++)
					{
						tempDoc.AppendChild (tempDoc.CreateElement ("option"));
						if ((Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
							tempDoc.DocumentElement.SetAttribute("value", Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

						if ((Conversion.ConvertObjToStr (objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
							tempDoc.DocumentElement.InnerText=JointAbbr (Conversion.ConvertObjToStr (objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]).Replace ("'", ""),Conversion.ConvertObjToStr (objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"]));
						else
							tempDoc.DocumentElement.InnerText="NO_ENTITY";
						sbTemp.Append(tempDoc.DocumentElement.OuterXml);
					
						//objtemp.InnerXml += tempDoc.DocumentElement.OuterXml ;
						tempDoc=new XmlDocument();
					}
					objtemp.InnerXml = sbTemp.ToString();
						sbTemp =null;
					objResultDoc.AppendChild( objtemp );
				}
				
			}
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetParentList.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				objXML = null;
				sSQL = null;
				if (objDataSet != null)
					objDataSet.Dispose();
				if (objConn != null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn.Dispose();
				}
				if (objTrans != null)
				{
					objTrans.Dispose();
				}
			}
			return (objResultDoc.InnerXml);

		}
		#endregion

		#region Edit Company information
		/// Name		: EditCompanyInformation
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 27-Feb-2006	   * Added 'p_iParentEid' as a parameter, which is used to set ParentEid in case of creating a new
		/// entity in org. hierarchy (TR Defect no. 2331) * Mihika Agrawal
		///************************************************************
		/// <summary>
		/// Edits the company information for the give Entity Id.
		/// </summary>
		/// <param name="p_iEntityId">Entity Id</param>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DSN name</param>
		/// <param name="p_sCompanyInfo">XML string to be populated with the Company information</param>
		/// <returns>XML string containing Company information</returns>
		public string EditCompanyInformation(int p_iEntityId, string p_sUserName, string p_sPassword,
			string p_sDsnName, string p_sCompanyInfo, int p_iParentEid)
		{
			int iLevel = 0;
			string sAbbr = "";
			string sName = "";
			string sParentAbbr = "";  //MITS 13997 : Umesh
			string sParentName = "";  //MITS 13997 : Umesh
			string sCodeDes = "";
			DataModelFactory objDMF = null;
			XmlDocument objXML = null;
			XmlElement objXmlElem = null;
			XmlNodeList objCompanyInfoNode = null;
			Entity objEntity = null;
			LocalCache objCache = null;
			XmlElement objSupplementalNode = null; //Anurag
			XmlElement objSuppDefElement = null; //Anurag

			try
			{
				objCache = new LocalCache(m_sConnectionString,m_iClientId);
				objXML = new XmlDocument();
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);//sharishkumar Rmacloud
				objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
				objXML.LoadXml(p_sCompanyInfo);
				objCompanyInfoNode = objXML.GetElementsByTagName("DepartmentInfo");

				if (objCompanyInfoNode == null)
                    throw new XmlOperationException(Globalization.GetString("OrgHierarchy.InvalidXML", m_iClientId));//sharishkumar Rmacloud


				for (int iCnt = 0; iCnt < objCompanyInfoNode.Count; iCnt++)
				{
					objXmlElem = (XmlElement)objCompanyInfoNode[iCnt];

					// Mihika, 27-Feb-2006 Defect# 2331
					// In case entity does not exist, populate its parent with the level selected.
					if (p_iEntityId <= 0)
					{
						objXmlElem.GetElementsByTagName("ParentEID")[0].InnerXml = p_iParentEid.ToString();
						//MITS 13997 : Umesh
						objCache.GetOrgInfo(p_iParentEid, ref sParentAbbr, ref sParentName);
						objXmlElem.GetElementsByTagName("ParentName")[0].InnerText = sParentAbbr + "-" + sParentName;
						//MITS 13997 : End
					}
					else
					//if (p_iEntityId > 0)
					{
						objEntity.MoveTo(p_iEntityId);

						//Fetch Client information
						objXmlElem.GetElementsByTagName("LastUpdate")[0].InnerText =

							Conversion.GetDBDateFormat(objEntity.DttmRcdLastUpd, "d");
						objXmlElem.GetElementsByTagName("UpdatedByUser")[0].InnerText = objEntity.UpdatedByUser;

						objCache.GetCodeInfo(objEntity.BusinessTypeCode, ref sAbbr, ref sName);
						objXmlElem.GetElementsByTagName("BusinessTypeCode")[0].InnerText = sAbbr + " " + sName;
						objXmlElem.GetElementsByTagName("BusinessTypeCode")[0].Attributes["codeid"].InnerText = objEntity.BusinessTypeCode.ToString();

						objXmlElem.GetElementsByTagName("County")[0].InnerText = objEntity.County;
						objXmlElem.GetElementsByTagName("NatureOfBusiness")[0].InnerText = objEntity.NatureOfBusiness;

						objCache.GetCodeInfo(objEntity.SicCode, ref sAbbr, ref sName);
						sCodeDes = sAbbr + " " + sName;
						objXmlElem.GetElementsByTagName("SicCode")[0].InnerText = sCodeDes.Replace("&amp;", "&");

						objXmlElem.GetElementsByTagName("SicCode")[0].Attributes["codeid"].InnerText = objEntity.SicCode.ToString();

						objXmlElem.GetElementsByTagName("SicCodeDesc")[0].InnerText = objEntity.SicCodeDesc;

						objCache.GetCodeInfo(objEntity.NaicsCode, ref sAbbr, ref sName);
						objXmlElem.GetElementsByTagName("NAICSCode")[0].Attributes["codeid"].InnerText = objEntity.NaicsCode.ToString();
						objXmlElem.GetElementsByTagName("NAICSCode")[0].InnerText = sAbbr + " " + sName;

						objXmlElem.GetElementsByTagName("WCFillingNumber")[0].InnerText = objEntity.WcFillingNumber;
						objXmlElem.GetElementsByTagName("EntityTableId")[0].InnerText = objEntity.EntityTableId.ToString();
						//Mukul 4/17/07 Handled "&" MITS 9260
						objXmlElem.GetElementsByTagName("LastName")[0].InnerText = objEntity.LastName;
						objXmlElem.GetElementsByTagName("LastNameSoundex")[0].InnerText = objEntity.LastNameSoundex;
						objXmlElem.GetElementsByTagName("FirstName")[0].InnerText = objEntity.FirstName;
						objXmlElem.GetElementsByTagName("AlsoKnownAs")[0].InnerText = objEntity.AlsoKnownAs;
						//Mukul 4/17/07 Handled "&" MITS 9260
						objXmlElem.GetElementsByTagName("Abbreviation")[0].InnerText = objEntity.Abbreviation;
						objXmlElem.GetElementsByTagName("CCCode")[0].InnerText = objEntity.CostCenterCode.ToString();
						objXmlElem.GetElementsByTagName("Addr1")[0].InnerText = objEntity.Addr1;
						objXmlElem.GetElementsByTagName("Addr2")[0].InnerText = objEntity.Addr2;
                        objXmlElem.GetElementsByTagName("Addr3")[0].InnerText = objEntity.Addr3;
                        objXmlElem.GetElementsByTagName("Addr4")[0].InnerText = objEntity.Addr4;
						objXmlElem.GetElementsByTagName("City")[0].InnerText = objEntity.City;

						objCache.GetCodeInfo(objEntity.CountryCode, ref sAbbr, ref sName);
						objXmlElem.GetElementsByTagName("CountryCode")[0].InnerText = sAbbr + " " + sName;
						objXmlElem.GetElementsByTagName("CountryCode")[0].Attributes["codeid"].InnerText = objEntity.CountryCode.ToString();

						objCache.GetStateInfo(objEntity.StateId, ref sAbbr, ref sName);
						objXmlElem.GetElementsByTagName("StateId")[0].InnerText = sAbbr + " " + sName;
						objXmlElem.GetElementsByTagName("StateId")[0].Attributes["codeid"].InnerText = objEntity.StateId.ToString();
						objXmlElem.GetElementsByTagName("ZipCode")[0].InnerText = objEntity.ZipCode;
						//MITS 13997 : Umesh
						objXmlElem.GetElementsByTagName("ParentEID")[0].InnerText = objEntity.ParentEid.ToString();
						objCache.GetOrgInfo(objEntity.ParentEid, ref sParentAbbr, ref sParentName);
						objXmlElem.GetElementsByTagName("ParentName")[0].InnerText = sParentAbbr + "-" + sParentName;
						//MITS 13997 : End
						objXmlElem.GetElementsByTagName("TaxID")[0].InnerText = objEntity.TaxId;
						objXmlElem.GetElementsByTagName("Contact")[0].InnerText = objEntity.Contact;
						objXmlElem.GetElementsByTagName("ClaimInstruction")[0].InnerText = objEntity.Instructions.EntInstrText;

						objCache.GetCodeInfo(objEntity.EmailTypeCode, ref sAbbr, ref sName);
						objXmlElem.GetElementsByTagName("EmailTypeCode")[0].InnerText = sAbbr + " " + sName;
						objXmlElem.GetElementsByTagName("EmailTypeCode")[0].Attributes["codeid"].InnerText = objEntity.EmailTypeCode.ToString();


						objXmlElem.GetElementsByTagName("EmailAddress")[0].InnerText = objEntity.EmailAddress;
						objXmlElem.GetElementsByTagName("SexCode")[0].InnerText = objEntity.SexCode.ToString();
						objXmlElem.GetElementsByTagName("BirthDate")[0].InnerText =
							Conversion.GetDBDateFormat(objEntity.BirthDate, "d");
						objXmlElem.GetElementsByTagName("Phone1")[0].InnerText = objEntity.Phone1;
						objXmlElem.GetElementsByTagName("Phone2")[0].InnerText = objEntity.Phone2;
						objXmlElem.GetElementsByTagName("FaxNumber")[0].InnerText = objEntity.FaxNumber;
						objXmlElem.GetElementsByTagName("DeletedFlag")[0].InnerText = objEntity.DeletedFlag.ToString();
						objXmlElem.GetElementsByTagName("EffectiveStartDate")[0].InnerText =
							Conversion.GetDBDateFormat(objEntity.EffStartDate, "d"); ;
						objXmlElem.GetElementsByTagName("EffectiveEndDate")[0].InnerText =
							Conversion.GetDBDateFormat(objEntity.EffEndDate, "d");
						objXmlElem.GetElementsByTagName("TriggerDateField")[0].InnerText = objEntity.TriggerDateField.ToString();
						objXmlElem.GetElementsByTagName("RMUSerID")[0].InnerText = objEntity.RMUserId.ToString();
						objXmlElem.GetElementsByTagName("FreezePayments")[0].InnerText = objEntity.FreezePayments.ToString();

						// For editing Time Zone Code : R7 Time Zone Enhacement
						//paggarwal2: start MITS 17730
						objCache.GetCodeInfo(objEntity.TimeZoneCode, ref sAbbr, ref sName);
						if (objXmlElem.GetElementsByTagName("TimeZoneCode") != null
							&& objXmlElem.GetElementsByTagName("TimeZoneCode").Count > 0
							&& objXmlElem.GetElementsByTagName("TimeZoneCode")[0].Attributes["codeid"] != null)
						{                            
							objXmlElem.GetElementsByTagName("TimeZoneCode")[0].Attributes["codeid"].InnerText = objEntity.TimeZoneCode.ToString();
							objXmlElem.GetElementsByTagName("TimeZoneCode")[0].InnerText = sAbbr + " " + sName;
						}
						// Edit Time Zone Tracking settings
						if (objXmlElem.GetElementsByTagName("TimeZoneTracking") != null
							&& objXmlElem.GetElementsByTagName("TimeZoneTracking").Count > 0)
						{
							objXmlElem.GetElementsByTagName("TimeZoneTracking")[0].InnerText = objEntity.TimeZoneTracking.ToString();
						}
						// Edit Day Light Savings settings
                        //if (objXmlElem.GetElementsByTagName("DayLightSavings") != null
                        //    && objXmlElem.GetElementsByTagName("DayLightSavings").Count > 0)
                        //{
                        //    objXmlElem.GetElementsByTagName("DayLightSavings")[0].InnerText = objEntity.DayLightSavings.ToString();
                        //}
						//paggarwal2: end MITS 17730
                        //Daylight system checkbox not needed anymore. The daylght savings is handled bu code now. :Yatharth
						// Start Naresh 9/20/2007 MI Forms Changes
						objCache.GetCodeInfo(objEntity.OrganizationType, ref sAbbr, ref sName);
						//Arnab: MITS-10956 - Added condition to search the existance of "OrganizationType" node in the XML
						if (objXmlElem.GetElementsByTagName("OrganizationType") != null && objXmlElem.GetElementsByTagName("OrganizationType").Count != 0)
						{
							objXmlElem.GetElementsByTagName("OrganizationType")[0].InnerText = sAbbr + " " + sName;
							objXmlElem.GetElementsByTagName("OrganizationType")[0].Attributes["codeid"].InnerText = objEntity.OrganizationType.ToString();
						}

						// End Naresh 9/20/2007 MI Forms Changes 
						objCache.GetCodeInfo(objEntity.Parent1099EID, ref sAbbr, ref sName);
						objXmlElem.GetElementsByTagName("Parent1099EID")[0].InnerText = sAbbr + " " + sName;
						objXmlElem.GetElementsByTagName("Parent1099EID")[0].Attributes["codeid"].InnerText = objEntity.Parent1099EID.ToString();
						objXmlElem.GetElementsByTagName("Report1099Flag")[0].InnerText =
							objEntity.Entity1099Reportable.ToString();
						objXmlElem.GetElementsByTagName("MiddleName")[0].InnerText = objEntity.MiddleName;
						objXmlElem.GetElementsByTagName("Title")[0].InnerText = objEntity.Title;
						objXmlElem.GetElementsByTagName("DateTimeAdded")[0].InnerText =
							Conversion.GetDBDateFormat(objEntity.DttmRcdAdded, "d");
						objXmlElem.GetElementsByTagName("AddedByUser")[0].InnerText = objEntity.AddedByUser;

                        //Start: rsushilaggar MITS 21454 21-Jul-2010
                        objCache.GetCodeInfo(objEntity.EntityApprovalStatusCode, ref sAbbr, ref sName);
                        objXmlElem.GetElementsByTagName("EntityApprovalStatusCode")[0].InnerText = sAbbr + " " + sName;
                        objXmlElem.GetElementsByTagName("EntityApprovalStatusCode")[0].Attributes["codeid"].InnerText = objEntity.EntityApprovalStatusCode.ToString();
                        objXmlElem.GetElementsByTagName("RejectReasonText")[0].InnerText = objEntity.RejectReasonText;
                        objXmlElem.GetElementsByTagName("RejectReasonText_HTMLComments")[0].InnerText = objEntity.RejectReasonText_HTMLComments;
                        //End: rsushilaggar

						//Fetch Contact information
						// Added to delete the empty node coming from blank instance
						XmlNode oRoot_contact = objXML.SelectSingleNode("DepartmentInfo/ContactInfo");

						XmlNode objelechid_contact = objXML.SelectSingleNode("DepartmentInfo/ContactInfo/Contact");
						try
						{
							oRoot_contact.RemoveChild(objelechid_contact);
						}
						catch
						{

						}
						//Deletion Ends
						foreach (EntityXContactInfo objEntityXContactInfo in objEntity.EntityXContactInfoList)
						{
							CreateXML(objXML, "Contact", "", "ContactInfo", iLevel);
							CreateXML(objXML, "ContactID", objEntityXContactInfo.ContactId.ToString(), "Contact", iLevel);
							CreateXML(objXML, "ContactName", objEntityXContactInfo.ContactName, "Contact", iLevel);
							CreateXML(objXML, "ContactTitle", objEntityXContactInfo.Title, "Contact", iLevel);
							CreateXML(objXML, "ContactInitials", objEntityXContactInfo.Initials, "Contact", iLevel);
							CreateXML(objXML, "Addr1", objEntityXContactInfo.Addr1, "Contact", iLevel);
							CreateXML(objXML, "Addr2", objEntityXContactInfo.Addr2, "Contact", iLevel);
                            CreateXML(objXML, "Addr3", objEntityXContactInfo.Addr3, "Contact", iLevel);
							CreateXML(objXML, "Addr4", objEntityXContactInfo.Addr4, "Contact", iLevel);
							CreateXML(objXML, "ContactCity", objEntityXContactInfo.City, "Contact", iLevel);
							objCache.GetStateInfo(objEntityXContactInfo.State, ref sAbbr, ref sName);
							CreateXML(objXML, "State", sAbbr + " " + sName, "Contact", iLevel, objEntityXContactInfo.State);
							CreateXML(objXML, "ZipCode", objEntityXContactInfo.ZipCode, "Contact", iLevel);
							CreateXML(objXML, "Phone", objEntityXContactInfo.Phone, "Contact", iLevel);
							CreateXML(objXML, "FaxNumber", objEntityXContactInfo.Fax, "Contact", iLevel);
							CreateXML(objXML, "EMailAddress", objEntityXContactInfo.Email, "Contact", iLevel);
							//Begin:Bug No. 0001260
							CreateXML(objXML, "StateCode", objEntityXContactInfo.State.ToString(), "Contact", iLevel);
							//End:Bug No. 0001260


							/*	objXmlElem.GetElementsByTagName("Contact")[0].InnerXml = "";
								objXmlElem.GetElementsByTagName("ContactID")[0].InnerXml = objEntityXContactInfo.ContactId.ToString();
								objXmlElem.GetElementsByTagName("ContactTitle")[0].InnerXml = objEntityXContactInfo.Title;
								objXmlElem.GetElementsByTagName("ContactName")[0].InnerXml = objEntityXContactInfo.ContactName;
								objXmlElem.GetElementsByTagName("ContactInitials")[0].InnerXml = objEntityXContactInfo.Initials;
								objXmlElem.GetElementsByTagName("Addr1")[1].InnerXml = objEntityXContactInfo.Addr1;
								objXmlElem.GetElementsByTagName("Addr2")[1].InnerXml = objEntityXContactInfo.Addr2;
								objXmlElem.GetElementsByTagName("ContactCity")[0].InnerXml = objEntityXContactInfo.City;
								objCache.GetStateInfo (objEntityXContactInfo.State, ref sAbbr, ref sName);
								objXmlElem.GetElementsByTagName("StateID")[0].InnerXml = sAbbr + " " + sName;
								objXmlElem.GetElementsByTagName("StateID")[0].Attributes["codeid"].InnerXml = objEntityXContactInfo.State.ToString();
								objXmlElem.GetElementsByTagName("ZipCode")[1].InnerXml = objEntityXContactInfo.ZipCode;
								objXmlElem.GetElementsByTagName("Phone")[0].InnerXml = objEntityXContactInfo.Phone;
								objXmlElem.GetElementsByTagName("FaxNumber")[1].InnerXml = objEntityXContactInfo.Fax;
								objXmlElem.GetElementsByTagName("EMailAddress")[0].InnerXml = objEntityXContactInfo.Email;
						
							*/
							iLevel++;
						}

						//Fetch Operating information
						iLevel = 0;
						XmlNode oRoot = objXML.SelectSingleNode("DepartmentInfo/OperationInfo");

						XmlNode objelechid = objXML.SelectSingleNode("DepartmentInfo/OperationInfo/Operating");
						try
						{
							oRoot.RemoveChild(objelechid);
						}
						catch 
						{

						}

						foreach (EntityXOperatingAs objEntityXOperatingAs in objEntity.EntityXOperatingAsList)
						{//Commented by Rahul on 30th May 2005 to avoid repeated tags.
							CreateXML(objXML, "Operating", "", "OperationInfo", iLevel);
							CreateXML(objXML, "Initial", objEntityXOperatingAs.Initials, "Operating", iLevel);
							CreateXML(objXML, "OperatingName", objEntityXOperatingAs.OperatingAs, "Operating", iLevel);
							CreateXML(objXML, "OperatingID", objEntityXOperatingAs.OperatingId.ToString(), "Operating", iLevel);
							/*objXmlElem.GetElementsByTagName("OperatingName")[0].InnerXml = objEntityXOperatingAs.OperatingAs;
							objXmlElem.GetElementsByTagName("Initial")[0].InnerXml = objEntityXOperatingAs.Initial;
							objXmlElem.GetElementsByTagName("OperatingID")[0].InnerXml = objEntityXOperatingAs.OperatingId.ToString();
							*/

							iLevel++;
						}

						if (int.Parse(objEntity.EntityTableId.ToString()) >= 1005 && int.Parse(objEntity.EntityTableId.ToString()) <= 1012)
						{
							//Fetch Self-Insured information
							XmlNode oRoot_selfInsured = objXML.SelectSingleNode("DepartmentInfo/SelfInsuredInfo");

							XmlNodeList objelechid_selfInsured = objXML.SelectNodes("DepartmentInfo/SelfInsuredInfo/SelfInsured");
							foreach (XmlNode objChild in objelechid_selfInsured)
							{
								try
								{
									oRoot_selfInsured.RemoveChild(objChild);
								}
								catch 
								{

								}
							}
							/*
							XmlNode objelechid_selfInsured=objXML.SelectSingleNode("DepartmentInfo/SelfInsuredInfo/SelfInsured");
							try
							{
								oRoot_selfInsured.RemoveChild(objelechid_selfInsured);
							}
							catch(Exception ex)
							{

							}
							*/
							iLevel = 0;
							foreach (EntityXSelfInsured objEntityXSelfInsured in objEntity.EntityXSelfInsuredList)
							{

								CreateXML(objXML, "SelfInsured", "", "SelfInsuredInfo", iLevel);
								CreateXML(objXML, "SIRowId", Conversion.ConvertObjToStr(objEntityXSelfInsured.SIRowId), "SelfInsured", iLevel);
								CreateXML(objXML, "EntityId", Conversion.ConvertObjToStr(objEntityXSelfInsured.EntityId), "SelfInsured", iLevel);
								objCache.GetStateInfo(objEntityXSelfInsured.Jurisdiction, ref sAbbr, ref sName);
								CreateXML(objXML, "Jurisdiction", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.Jurisdiction);
								CreateXML(objXML, "DeletedFlag", objEntityXSelfInsured.DeletedFlag.ToString(), "SelfInsured", iLevel);
								CreateXML(objXML, "LegalEntityId", objEntityXSelfInsured.LegalEntityId.ToString(), "SelfInsured", iLevel);
								//Changed by:Mohit Yadav for Bug No. 000078***************************************
								CreateXML(objXML, "CertNameEid", objCache.GetEntityLastFirstName(int.Parse(objEntityXSelfInsured.CertNameEid.ToString())), "SelfInsured", iLevel, objEntityXSelfInsured.CertNameEid);

								CreateXML(objXML, "CertificateNumber", objEntityXSelfInsured.CertificateNumber, "SelfInsured", iLevel);
								CreateXML(objXML, "EffectiveDate", Conversion.GetDBDateFormat
									(objEntityXSelfInsured.EffectiveDate, "d"), "SelfInsured", iLevel);
								CreateXML(objXML, "ExpirationDate", Conversion.GetDBDateFormat(objEntityXSelfInsured.ExpirationDate, "d"), "SelfInsured", iLevel);
								objCache.GetCodeInfo(objEntityXSelfInsured.Authorization, ref sAbbr, ref sName);
								CreateXML(objXML, "Authorization", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.Authorization);
								objCache.GetCodeInfo(objEntityXSelfInsured.Organization, ref sAbbr, ref sName);
								CreateXML(objXML, "Organization", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.Organization);
								CreateXML(objXML, "DttmRcdAdded", Conversion.GetDBDateFormat
									(objEntityXSelfInsured.DttmRcdAdded, "d"), "SelfInsured", iLevel);
								CreateXML(objXML, "DttmRcdLastUpd", Conversion.GetDBDateFormat
									(objEntityXSelfInsured.DttmRcdLastUpd, "d"), "SelfInsured", iLevel);
								CreateXML(objXML, "UpdatedByUser", objEntityXSelfInsured.UpdatedByUser, "SelfInsured", iLevel);
								CreateXML(objXML, "AddedByUser", objEntityXSelfInsured.AddedByUser, "SelfInsured", iLevel);
								iLevel++;
							}

							//Fetch Exposure information
							iLevel = 0;
							foreach (EntityXExposure objEntityXExposure in objEntity.EntityXExposureList)
							{
								/*
								CreateXML(objXML, "NoOfEmployees", objEntityXExposure.NoOfEmployees.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "WorkHrs",objEntityXExposure.NoOfWorkHours.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "PayRollAmt",objEntityXExposure.PayrollAmount.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "StartDate", Conversion.GetDBDateFormat 
									(objEntityXExposure.StartDate, "d"),"ExposureInfo",iLevel);
								CreateXML(objXML, "AssetValue",objEntityXExposure.AssetValue.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "SquareFootage", objEntityXExposure.SquareFootage.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "VehicleCount", objEntityXExposure.VehicleCount.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "TotalRevenue",objEntityXExposure.TotalRevenue.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "OtherBase", objEntityXExposure.OtherBase.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "RiskMgtOverHead",objEntityXExposure.RiskMgmtOverhead.ToString(),"ExposureInfo",iLevel);
								CreateXML(objXML, "UserGeneratedFlag",objEntityXExposure.UserGeneratedFlag.ToString(),"ExposureInfo",iLevel);
								*/
								objXmlElem.GetElementsByTagName("NoOfEmployees")[0].InnerXml = objEntityXExposure.NoOfEmployees.ToString();
								objXmlElem.GetElementsByTagName("WorkHrs")[0].InnerXml = objEntityXExposure.NoOfWorkHours.ToString();
								objXmlElem.GetElementsByTagName("PayRollAmt")[0].InnerXml = objEntityXExposure.PayrollAmount.ToString();
								objXmlElem.GetElementsByTagName("StartDate")[0].InnerXml = Conversion.GetDBDateFormat(objEntityXExposure.StartDate, "d");
								//pmittal5 MITS 12778 08/08/08
								objXmlElem.GetElementsByTagName("EndDate")[0].InnerXml = Conversion.GetDBDateFormat(objEntityXExposure.EndDate, "d");
								objXmlElem.GetElementsByTagName("AssetValue")[0].InnerXml = objEntityXExposure.AssetValue.ToString();
								objXmlElem.GetElementsByTagName("SquareFootage")[0].InnerXml = objEntityXExposure.SquareFootage.ToString();
								objXmlElem.GetElementsByTagName("VehicleCount")[0].InnerXml = objEntityXExposure.VehicleCount.ToString();
								objXmlElem.GetElementsByTagName("TotalRevenue")[0].InnerXml = objEntityXExposure.TotalRevenue.ToString();
								objXmlElem.GetElementsByTagName("OtherBase")[0].InnerXml = objEntityXExposure.OtherBase.ToString();
								objXmlElem.GetElementsByTagName("RiskMgtOverHead")[0].InnerXml = objEntityXExposure.RiskMgmtOverhead.ToString();
								objXmlElem.GetElementsByTagName("UserGeneratedFlag")[0].InnerXml = objEntityXExposure.UserGeneratedFlag.ToString();

								iLevel++;
							}
							//For Client Limits added on 13th June 2005 by Rahul Sharma.
							XmlNode oRoot_clientLimits = objXML.SelectSingleNode("DepartmentInfo/ClientLimitsInfo");

							XmlNode objelechid_clientLimits = objXML.SelectSingleNode("DepartmentInfo/ClientLimitsInfo/ClientLimits");
							try
							{
								oRoot_clientLimits.RemoveChild(objelechid_clientLimits);
							}
							catch 
							{

							}
							iLevel = 0;
							string strLimitType = null;
							foreach (ClientLimits objClientLimits in objEntity.ClientLimitsList)
							{
								//objXmlElem.GetElementsByTagName("NoOfEmployees")[0].InnerXml

								CreateXML(objXML, "ClientLimits", "", "ClientLimitsInfo", iLevel);
								CreateXML(objXML, "Client_RowId", objClientLimits.ClientLimitsRowId.ToString(), "ClientLimits", iLevel);
								CreateXML(objXML, "Client_Eid", objClientLimits.ClientEid.ToString(), "ClientLimits", iLevel);
								objCache.GetCodeInfo(objClientLimits.LobCode, ref sAbbr, ref sName);
								CreateXML(objXML, "LOB_CODE", sAbbr + "  " + sName, "ClientLimits", iLevel, int.Parse(objClientLimits.LobCode.ToString()));
								objCache.GetCodeInfo(objClientLimits.ReserveTypeCode, ref sAbbr, ref sName);
								CreateXML(objXML, "RESERVE_TYPE_CODE", sAbbr + "  " + sName, "ClientLimits", iLevel, int.Parse(objClientLimits.ReserveTypeCode.ToString()));
								CreateXML(objXML, "MAX_AMOUNT", objClientLimits.MaxAmount.ToString(), "ClientLimits", iLevel);
								CreateXML(objXML, "PAYMENT_FLAG", objClientLimits.PaymentFlag.ToString(), "ClientLimits", iLevel);
								//Begin:Bug No. 000310
								CreateXML(objXML, "LobCode", objClientLimits.LobCode.ToString(), "ClientLimits", iLevel);
								CreateXML(objXML, "ResTypeCode", objClientLimits.ReserveTypeCode.ToString(), "ClientLimits", iLevel);
								//End:Bug No. 000310
								if (objClientLimits.PaymentFlag == 0)
								{
									strLimitType = "Reserve";
								}
								else
								{
									strLimitType = "Payment";
								}
								CreateXML(objXML, "Type_Of_Limit", strLimitType, "ClientLimits", iLevel);
								strLimitType = "";
								iLevel++;
							}

						}
						else
						{
							/*XmlNode oRoot_juris=objXML.SelectSingleNode("DepartmentInfo/JurisAndLicenInfo");
					
							XmlNode objelechid_juris=objXML.SelectSingleNode("DepartmentInfo/JurisAndLicenInfo/JurisAndLicen");
							try
							{
								oRoot_juris.RemoveChild(objelechid_juris);
							}
							catch(Exception ex)
							{

							}
							iLevel = 0;
							foreach (JurisAndLicenseCodes objJurisAndLicenseCodes in objEntity.JurisAndLicenseCodesList)
							{
								CreateXML(objXML, "JurisAndLicen","","JurisAndLicenInfo",iLevel);
								CreateXML(objXML,"TableRowId",objJurisAndLicenseCodes.TableRowId.ToString(),"JurisAndLicen",iLevel);
								CreateXML(objXML,"EntityId",objJurisAndLicenseCodes.EntityId.ToString(),"JurisAndLicen",iLevel);
								objCache.GetStateInfo(Conversion.ConvertObjToInt(objJurisAndLicenseCodes.Jurisdiction),ref sAbbr,ref sName);, m_iClientId
								CreateXML(objXML,"Jurisdiction",sAbbr+"  " +sName,"JurisAndLicen",iLevel);
								CreateXML(objXML,"DeletedFlag",objJurisAndLicenseCodes.DeletedFlag.ToString(),"JurisAndLicen",iLevel);
								CreateXML(objXML,"CodeLicenseNumber",objJurisAndLicenseCodes.CodeLicenseNumber.ToString(),"JurisAndLicen",iLevel);
								CreateXML(objXML,"EffectiveDate",Conversion.GetDBDateFormat 
									(objJurisAndLicenseCodes.EffectiveDate, "d"),"JurisAndLicen",iLevel);
								CreateXML(objXML,"ExpirationDate",Conversion.GetDBDateFormat(objJurisAndLicenseCodes.ExpirationDate,"d"),"JurisAndLicen",iLevel);
								CreateXML(objXML,"DttmRcdAdded",objJurisAndLicenseCodes.DttmRcdAdded,"JurisAndLicen",iLevel);
								CreateXML(objXML,"DttmRcdLastUpd",objJurisAndLicenseCodes.DttmRcdLastUpd,"JurisAndLicen",iLevel);
								CreateXML(objXML,"UpdatedByUser",objJurisAndLicenseCodes.UpdatedByUser,"JurisAndLicen",iLevel);
								CreateXML(objXML,"AddedByUser",objJurisAndLicenseCodes.AddedByUser,"JurisAndLicen",iLevel);
								objCache.GetCodeInfo(objJurisAndLicenseCodes.GeneralClaims,ref sAbbr,ref sName);
								CreateXML(objXML,"GeneralClaims",sAbbr+ "  "+sName,"JurisAndLicen",iLevel);
								objCache.GetCodeInfo(objJurisAndLicenseCodes.ShortTermDisability,ref sAbbr,ref sName);
								CreateXML(objXML,"ShortTermDisability",sAbbr+ "  "+sName,"JurisAndLicen",iLevel);
								objCache.GetCodeInfo(objJurisAndLicenseCodes.VehicleAccident,ref sAbbr,ref sName);
								CreateXML(objXML,"VehicleAccident",sAbbr+ "  "+sName,"JurisAndLicen",iLevel);
								objCache.GetCodeInfo(objJurisAndLicenseCodes.WorkersCompensation,ref sAbbr,ref sName);
								CreateXML(objXML,"WorkersCompensation",sAbbr+ "  "+sName,"JurisAndLicen",iLevel);
								objCache.GetCodeInfo(objJurisAndLicenseCodes.AllLineOfBusiness,ref sAbbr,ref sName);
								CreateXML(objXML,"AllLineOfBusiness",sAbbr+ "  "+sName,"JurisAndLicen",iLevel);

								iLevel++;
							}*/
						}
					}
				}

				//Defect #001586 - suplemental option is not available in the Organization Hierarchy.
				//Added code for loading supp information along with Org Maint XML.
				objSupplementalNode = objXML.CreateElement("Supplementals");
				objSuppDefElement = objXML.CreateElement("SuppDef");

				if (objEntity.GetType().GetProperty("Supplementals") != null)
				{
					XmlDocument objSuppXMLDoc = new XmlDocument();

					Supplementals objSupp = (objEntity.GetProperty("Supplementals", new object[] { }) as Supplementals);
					if (objSupp != null)
					{
						objSupp.Refresh();
						objSuppXMLDoc.LoadXml(objSupp.SerializeObject());
						objSupplementalNode.InnerXml = objSuppXMLDoc.DocumentElement.FirstChild.InnerXml;
						objSupplementalNode.SetAttribute("tablename", objSuppXMLDoc.DocumentElement.FirstChild.Attributes["tablename"].Value);
						objSuppDefElement.InnerXml = objSupp.ViewXml.FirstChild.InnerXml;
						foreach (XmlNode objNde in objSuppDefElement.GetElementsByTagName("control"))
						{
							if (((XmlElement)objNde).HasAttribute("ref"))
							{
								((XmlElement)objNde).SetAttribute("ref", ((XmlElement)objNde).GetAttribute("ref").Replace("/Instance/*", "/Instance//DepartmentInfo"));
							}
						}
					}
				}

				CreateXML(objXML, "", objSupplementalNode.InnerXml, "Supplementals", 0);
				CreateXML(objXML, "", objSuppDefElement.InnerXml, "SuppDef", 0);

				p_sCompanyInfo = objXML.InnerXml;
			}
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.EditCompanyInformation.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDMF != null) objDMF.Dispose();
				if (objEntity != null) objEntity.Dispose();
				if (objCache != null) objCache.Dispose();
				objXML = null;
				objXmlElem = null;
				objCompanyInfoNode = null;
				objSupplementalNode = null; //Anurag
				objSuppDefElement = null; //Anurag

			}
			return (p_sCompanyInfo);
		}
		#endregion

		#region Creates XML
		/// Name		: CreateXML
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates the XML
		/// </summary>
		/// <param name="p_objXML">XML DOM to be processed.</param>
		/// <param name="p_sTagName">XML element to be added.</param>
		/// <param name="p_sTagValue">Value of the XML element.</param>
		/// <param name="p_sParentTag">Parent under which an element to be added.</param>
		/// <param name="p_iLevel">Level at which a particular element to be added.</param>
		private void CreateXML(XmlDocument p_objXML, string p_sTagName, string p_sTagValue, string p_sParentTag, int p_iLevel)
		{
			this.CreateXML(p_objXML, p_sTagName, p_sTagValue, p_sParentTag, p_iLevel, 0, false);
		}
		private void CreateXML(XmlDocument p_objXML, string p_sTagName, string p_sTagValue, string p_sParentTag, int p_iLevel, int p_iCodeId)
		{
			this.CreateXML(p_objXML, p_sTagName, p_sTagValue, p_sParentTag, p_iLevel, p_iCodeId, true);
		}
		private void CreateXML(XmlDocument p_objXML, string p_sTagName, string p_sTagValue, string p_sParentTag, int p_iLevel, int p_iCodeId, bool p_bDisplayCodeId)
		{
			XmlElement objMainElement = null;
			XmlElement objSubElement = null;
			XmlElement objChildElem = null;
			XmlNodeList objContactElement = null;
			XmlNodeList objOperatingElement = null;
			XmlNodeList objSelfInsuredElement = null;
			XmlDocument objXMLDoc = null;

			try
			{
				if (!(p_objXML.HasChildNodes))
				{
					objMainElement = p_objXML.CreateElement("ContactInfo");
					p_objXML.AppendChild(objMainElement);
				}
				else
					objMainElement = (XmlElement)(p_objXML.GetElementsByTagName("ContactInfo").Item(0));


				switch (p_sParentTag)
				{	//Contact
					case "ContactInfo":
						objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("ContactInfo").Item(0));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						objSubElement.AppendChild(objChildElem);
						break;

					case "Contact":
						objContactElement = p_objXML.GetElementsByTagName("ContactInfo").Item(0).SelectNodes("Contact");
						objSubElement = (XmlElement)(objContactElement.Item(p_iLevel));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						if (p_bDisplayCodeId)
						{
							if (p_sTagName == "State")
							{
								objChildElem.SetAttribute("codeid", p_iCodeId.ToString());
							}
						}
						objChildElem.InnerText = p_sTagValue;
						objSubElement.AppendChild(objChildElem);
						break;

					//Operating
					case "OperationInfo":
						objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("OperationInfo").Item(0));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						objSubElement.AppendChild(objChildElem);
						break;

					case "Operating":
						objOperatingElement = p_objXML.GetElementsByTagName("OperationInfo").Item(0).SelectNodes("Operating");
						objSubElement = (XmlElement)(objOperatingElement.Item(p_iLevel));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						objChildElem.InnerText = p_sTagValue;
						objSubElement.AppendChild(objChildElem);
						break;

					//Self insured
					case "SelfInsuredInfo":
						objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("SelfInsuredInfo").Item(0));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						objSubElement.AppendChild(objChildElem);
						break;

					case "SelfInsured":
						objSelfInsuredElement = p_objXML.GetElementsByTagName("SelfInsuredInfo").Item(0).SelectNodes("SelfInsured");
						objSubElement = (XmlElement)(objSelfInsuredElement.Item(p_iLevel));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						if (p_bDisplayCodeId)
						{
							if (p_sTagName == "Jurisdiction" || p_sTagName == "Authorization" || p_sTagName == "Organization" || p_sTagName == "CertNameEid")
							{
								objChildElem.SetAttribute("codeid", p_iCodeId.ToString());
							}
						}
						objChildElem.InnerText = p_sTagValue;
						objSubElement.AppendChild(objChildElem);
						break;

					//Exposure
					case "ExposureInfo":
						objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("ExposureInfo").Item(0));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						objChildElem.InnerXml = p_sTagValue;
						objSubElement.AppendChild(objChildElem);
						break;
					case "ClientLimitsInfo":
						objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("ClientLimitsInfo").Item(0));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						objSubElement.AppendChild(objChildElem);
						break;
					case "ClientLimits":
						objOperatingElement = p_objXML.GetElementsByTagName("ClientLimitsInfo").Item(0).SelectNodes("ClientLimits");
						objSubElement = (XmlElement)(objOperatingElement.Item(p_iLevel));
						objChildElem = p_objXML.CreateElement(p_sTagName);
						if (p_bDisplayCodeId)
						{
							if (p_sTagName == "LOB_CODE" || p_sTagName == "RESERVE_TYPE_CODE")
							{
								objChildElem.SetAttribute("codeid", p_iCodeId.ToString());
							}
						}
						objChildElem.InnerText = p_sTagValue;
						objSubElement.AppendChild(objChildElem);
						break;
					case "Supplementals":
						objXMLDoc = new XmlDocument();
						objXMLDoc.LoadXml("<Supplementals>" + p_sTagValue + "</Supplementals>");
						objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("Supplementals").Item(0));
						objSubElement.InnerXml = objXMLDoc.GetElementsByTagName("Supplementals").Item(0).InnerXml;
						break;
					case "SuppDef":
						objXMLDoc = new XmlDocument();
						objXMLDoc.LoadXml("<SuppDef>" + p_sTagValue + "</SuppDef>");
						objSubElement = (XmlElement)(p_objXML.GetElementsByTagName("SuppDef").Item(0));
						objSubElement.InnerXml = objXMLDoc.GetElementsByTagName("SuppDef").Item(0).InnerXml;
						break;
				}
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateXML.CreateError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				objMainElement = null;
				objSubElement = null;
				objChildElem = null;
				objContactElement = null;
				objOperatingElement = null;
				objSelfInsuredElement = null;
			}
		}
		#endregion

		#region Get Org Hierarchy XML
		/// Name		: GetOrgHierarchyXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the Organization Hierarchy information
		/// </summary>
		/// <param name="p_bExpand">Whether the Org Hierarchy node is expanded or not</param>
		/// <param name="p_lLevel">Level of the entities</param>
		/// <param name="p_lNode">Node id</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <param name="p_sParent">Parent of the current node</param>
		/// <param name="p_sTableName">Table name</param>
		/// <param name="p_lRowId">Row Id</param>
		/// <param name="p_sInputXml">XML to be populated with the Org Hierarchy data</param>
		/// <param name="p_iEntityEnh">Flag to check whether enhanced entity search needs to be shown or not</param>
		/// <returns>XML containing Org Hierarchy search result</returns>
		public string GetOrgHierarchyXml(bool p_bExpand, long p_lLevel, long p_lNode, string p_sNodCat,
			string p_sParent, string p_sTableName, long p_lRowId, string p_sInputXml, out int p_iEntityEnh, string p_sEventDate, string p_sClaimDate, string p_sPolicyDate)
		{
			//Deb:Commented below as they are not used
            //string sSql = "";
			//string sFieldName = "";
			string sOrgHierarchy = "";
			XmlDocument objXML = null;
			DataSet objDataSet = null;
			DbReader objReader = null;
			StringBuilder objBuildSQL = null;
			XmlNode objNod = null;
			try
			{
				m_lLevel = p_lLevel;
				objXML = new XmlDocument();
				objBuildSQL = new StringBuilder();
				objXML.LoadXml(p_sInputXml);
				p_iEntityEnh = 0;

				//TR1740 effective date trigger fails in case of new records
				m_sEventDate = p_sEventDate;
				m_sClaimDate = p_sClaimDate;
				m_sPolicyDate = p_sPolicyDate;//Added by Shivendu for MITS 17249

				//Set the value of the attribute in the element "control"
				objXML.SelectSingleNode("//control[@name='orgTree']").ParentNode["control"].
					SetAttribute("num_level", m_lLevel.ToString());

				objNod = objXML.SelectSingleNode("//chkFilter");
				if (objNod != null)
					m_sFilter = objXML.SelectSingleNode("//chkFilter").InnerText;

				//added by Nitin for Mits 12539 , inorder to display effective date checkbox as uncheck first time.
                //rsushilaggar MITS 18959 Date 04/05/2011 
                //if (m_sFilter == "")
                //{
                //    m_sFilter = "0";
                //}
				//ended by Nitin for Mits 12539
				//Fetch the ENH_HIERARCHY_LIST column value to find if Enhanced entity search is enabled or not
				objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT ENH_HIERARCHY_LIST FROM SYS_PARMS");

                if (objReader.Read())
                    //p_iEntityEnh = Conversion.ConvertObjToInt(objReader.GetValue("ENH_HIERARCHY_LIST"), m_iClientId);
                    //Deb: Performance Changes
                    p_iEntityEnh = Convert.ToInt32(objReader.GetValue("ENH_HIERARCHY_LIST"));
				objReader.Close();

				m_sTable = p_sTableName;
				m_lRowId = p_lRowId;
				//Mukul 4/18/07 Commented MITS 8733
				//if ((p_sTableName != "") && (p_sTableName != "nothing"))
				//{
				//	sFieldName = GetFieldName(m_sTable);
				//SetEffectiveDate(p_sTableName, p_lRowId);
				//}

				//objXML = RemoveSelection(objXML);
                //Deb: Performance Changes
                if (p_lRowId == 0)
                    m_sSystemDate = Conversion.GetDate(DateTime.Now.ToShortDateString());
                else
                {
                    SetEffectiveDate(p_sTableName, p_lRowId);
                    objXML = RemoveSelection(objXML);
                }
                //Deb: Performance Changes
				//if user has clicked on one of the org tree node
				if ((p_sNodCat != "") && (p_bExpand == true))
				{
					switch (p_sNodCat)
					{
						//fetch client information
						case CLIENT:
							//							objBuildSQL.Append ("SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID FROM ORG_HIERARCHY A ," + 
							//								"ENTITY C,ENTITY B WHERE A.CLIENT_EID=C.ENTITY_ID" +
							//								" AND A.COMPANY_EID= B.ENTITY_ID AND B.DELETED_FLAG=0 AND A.CLIENT_EID=" + p_lNode);

							//objBuildSQL.Append (@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//						FROM ORG_HIERARCHY A LEFT JOIN ENTITY B ON A.COMPANY_EID = B.ENTITY_ID 
							//						WHERE B.PARENT_EID=" + p_lNode +
							//						" AND B.DELETED_FLAG=0 AND A.CLIENT_EID=" + p_lNode);

							//PJS MITS 10894-12-06-2007 - Modified Above query to get desired result    
							//From the LEFT OUTER JOIN condition, every row of ENTITY will be joined to
							//all rows in ORG_HIERARCHY where A.COMPANY_EID = B.ENTITY_ID and A.CLIENT_EID=" + p_lNode +.
							//All rows of ENTITY that don't satisfy this condition are joined to a single row
							//of all NULL values to indicate that there's no matching row from ORG_HIERARCHY.
							//Earlier WHERE Clause was having this condition so only matching rows from ENTIY & ORG_HIERACHY were returned.


							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//						FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.COMPANY_EID  
							//                      AND A.CLIENT_EID=" + p_lNode +
							//						" WHERE B.PARENT_EID=" + p_lNode +
							//                      " AND B.DELETED_FLAG=0 ");

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID"
													+ " FROM  ENTITY B WHERE B.PARENT_EID=" + p_lNode +
												   " AND B.DELETED_FLAG=0 ");

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								objBuildSQL.Append(GetTriggerSQL("B", p_sTableName));
							}
							/*if ((p_sTableName != "") && p_sTableName != "nothing" && (m_sFilter=="1"))
							{

								objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + 
									m_sSystemDate + "' AND" +
									" B.EFF_END_DATE>='" + m_sSystemDate + "')");

								if (m_sClaimDate != "")
								{
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + 
										m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
								}
								
								if (m_sEventDate != "")
								{
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + 
										m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
								}

								if (m_sPolicyDate != "")
								{
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + 
										m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");
								}

								objBuildSQL.Append (" OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + 
									" OR B.TRIGGER_DATE_FIELD IS NULL");
								objBuildSQL.Append (")");
							}*/
							//End MITS 8733
							objBuildSQL.Append(" ORDER BY B.LAST_NAME");
							//Fill the dataset
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, objBuildSQL.ToString(), m_iClientId);
							objBuildSQL.Remove(0, objBuildSQL.Length);
							//Get the next node information
							sOrgHierarchy = GetCompanyXml(objDataSet, p_lNode, p_sParent, objXML, "C");
							break;

						//fetch company information
						case COMPANY:
							//							objBuildSQL.Append ("SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID FROM ORG_HIERARCHY A,ENTITY C,ENTITY B WHERE A.COMPANY_EID=C.ENTITY_ID" +
							//								" AND A.OPERATION_EID= B.ENTITY_ID AND B.DELETED_FLAG=0 AND A.COMPANY_EID=" + p_lNode);
							//objBuildSQL.Append (@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ORG_HIERARCHY A LEFT JOIN ENTITY B ON A.OPERATION_EID = B.ENTITY_ID 
							//					WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 AND A.COMPANY_EID=" + p_lNode);

							//PJS MITS 10894-12-06-2007 - Modified Above query to get desired result
							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.OPERATION_EID 
							//                    AND A.COMPANY_EID=" + p_lNode +
							//					" WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 ");

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID"
													+ " FROM  ENTITY B WHERE B.PARENT_EID=" + p_lNode +
													" AND B.DELETED_FLAG=0 ");

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								objBuildSQL.Append(GetTriggerSQL("B", p_sTableName));
							}
							/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
							{
								objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + m_sSystemDate + "' AND" +
									" B.EFF_END_DATE>='" + m_sSystemDate + "')");
								if (m_sClaimDate != "")
								{
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
								}
								if (m_sEventDate != "")
								{
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
								}
								if (m_sPolicyDate != "")
								{
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");
								}
								objBuildSQL.Append (" OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR B.TRIGGER_DATE_FIELD IS NULL");
								objBuildSQL.Append (")");
							}*/
							//End MITS 8733
							objBuildSQL.Append(" ORDER BY B.LAST_NAME");

							//Fill the dataset
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, objBuildSQL.ToString(), m_iClientId);
							objBuildSQL.Remove(0, objBuildSQL.Length);
							//Get the next node information
							sOrgHierarchy = GetOperationXml(objDataSet, p_lNode, p_sParent, objXML, "CO");
							break;

						//fetch operation information
						case OPERATION:
							//objBuildSQL.Append (@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//	FROM ORG_HIERARCHY A LEFT JOIN ENTITY B ON  A.REGION_EID = B.ENTITY_ID
							//	WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 AND A.OPERATION_EID=" + p_lNode);
							//PJS MITS 10894-12-06-2007 - Modified Above query to get desired result    
							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//	FROM  ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.REGION_EID
							//    AND A.OPERATION_EID=" + p_lNode +
							//	" WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 ");

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID"
													+" FROM  ENTITY B WHERE B.PARENT_EID=" + p_lNode +
													" AND B.DELETED_FLAG=0 ");

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								objBuildSQL.Append(GetTriggerSQL("B", p_sTableName));
							}
							/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
							{
								objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + m_sSystemDate + "' AND" +
									" B.EFF_END_DATE>='" + m_sSystemDate + "')");
								if (m_sClaimDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
								if (m_sEventDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
								if (m_sPolicyDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");

								objBuildSQL.Append (" OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR B.TRIGGER_DATE_FIELD IS NULL");
								objBuildSQL.Append (")");
							}*/
							//End MITS 8733
							objBuildSQL.Append(" ORDER BY B.LAST_NAME");

							//Fill the dataset
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, objBuildSQL.ToString(), m_iClientId);
							objBuildSQL.Remove(0, objBuildSQL.Length);
							//Get the next node information
							sOrgHierarchy = GetRegionXml(objDataSet, p_lNode, p_sParent, objXML, "O");
							break;

						//fetch region information
						case REGION:
							//objBuildSQL.Append (@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ORG_HIERARCHY A LEFT JOIN ENTITY B ON A.DIVISION_EID = B.ENTITY_ID 
							//					WHERE B.PARENT_EID=" + p_lNode +" AND B.DELETED_FLAG=0 AND A.REGION_EID=" + p_lNode);
							//PJS MITS 10894-12-06-2007 - Modified Above query to get desired result    
							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.DIVISION_EID
							//                    AND A.REGION_EID=" + p_lNode +
							//					" WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 ");

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
                            objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID,B.TAX_ID,B.FIRST_NAME"
													+" FROM  ENTITY B WHERE B.PARENT_EID=" + p_lNode +
													 " AND B.DELETED_FLAG=0 ");

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								objBuildSQL.Append(GetTriggerSQL("B", p_sTableName));
							}
							/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
							{
								objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + m_sSystemDate + "' AND" +
									" B.EFF_END_DATE>='" + m_sSystemDate + "')");
								if (m_sClaimDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
								if (m_sEventDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
								if (m_sPolicyDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");
							
								sSql = sSql + " OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR B.TRIGGER_DATE_FIELD IS NULL";
								sSql = sSql + ")";
							}*/
							//End MITS 8733
							objBuildSQL.Append(" ORDER BY B.LAST_NAME");

							//Fill the dataset
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, objBuildSQL.ToString(), m_iClientId);
							objBuildSQL.Remove(0, objBuildSQL.Length);
							//Get the next node information
							sOrgHierarchy = GetDivisionXml(objDataSet, p_lNode, p_sParent, objXML, "R");
							break;

						//fetch division information
						case DIVISION:
							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ORG_HIERARCHY A LEFT JOIN ENTITY B ON A.LOCATION_EID = B.ENTITY_ID 
							//					WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 AND A.DIVISION_EID=" + p_lNode);
							//PJS MITS 10894-12-06-2007 - Modified Above query to get desired result    
							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.LOCATION_EID
							//                    AND A.DIVISION_EID=" + p_lNode +
							//					" WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 ");

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
                            //skhare 7 
							objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID,B.TAX_ID,B.FIRST_NAME"
													+" FROM  ENTITY B WHERE B.PARENT_EID=" + p_lNode +
													" AND B.DELETED_FLAG=0 ");

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								objBuildSQL.Append(GetTriggerSQL("B", p_sTableName));
							}
							/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
							{
								objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + m_sSystemDate + "' AND" +
									" B.EFF_END_DATE>='" + m_sSystemDate + "')");
								if (m_sClaimDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
								if (m_sEventDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
								if (m_sPolicyDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");

								objBuildSQL.Append (" OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR B.TRIGGER_DATE_FIELD IS NULL");
								objBuildSQL.Append (")");
							}*/
							//End MITS 8733
							objBuildSQL.Append(" ORDER BY B.LAST_NAME");

							//Fill the dataset
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, objBuildSQL.ToString(), m_iClientId);
							objBuildSQL.Remove(0, objBuildSQL.Length);
							//Get the next node information
							sOrgHierarchy = GetLocationXml(objDataSet, p_lNode, p_sParent, objXML, "D");
							break;

						//fetch location information
						case LOCATION:
							//objBuildSQL.Append (@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ORG_HIERARCHY A LEFT JOIN ENTITY B ON A.FACILITY_EID = B.ENTITY_ID 
							//					WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 AND A.LOCATION_EID=" + p_lNode);
							//PJS MITS 10894-12-06-2007 - Modified Above query to get desired result    
							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.FACILITY_EID
							//                     AND A.LOCATION_EID=" + p_lNode +
							//					" WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 ");

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
                            //skhare7 r8
							objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID,B.TAX_ID,B.FIRST_NAME"
													+" FROM  ENTITY B WHERE B.PARENT_EID=" + p_lNode +
													" AND B.DELETED_FLAG=0 ");
							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								objBuildSQL.Append(GetTriggerSQL("B", p_sTableName));
							}
							/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
							{
								objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + m_sSystemDate + "' AND" +
									" B.EFF_END_DATE>='" + m_sSystemDate + "')");
								if (m_sClaimDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
								if (m_sEventDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
								if (m_sPolicyDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");
								objBuildSQL.Append (" OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR B.TRIGGER_DATE_FIELD IS NULL");
								objBuildSQL.Append (")");
							}*/
							//MITS 8733
							objBuildSQL.Append(" ORDER BY B.LAST_NAME");

							//Fill the dataset
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, objBuildSQL.ToString(), m_iClientId);
							objBuildSQL.Remove(0, objBuildSQL.Length);
							//Get the next node information
							sOrgHierarchy = GetFacilityXml(objDataSet, p_lNode, p_sParent, objXML, "L");
							break;

						//fetch facility information
						case FACILITY:
							//objBuildSQL.Append (@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ORG_HIERARCHY A LEFT JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID 
							//					WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 AND A.FACILITY_EID=" + p_lNode);
							//PJS MITS 10894-12-06-2007 - Modified Above query to get desired result    
							//objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID 
							//					FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.DEPARTMENT_EID 
							//                    AND A.FACILITY_EID=" + p_lNode +
							//					" WHERE B.PARENT_EID=" + p_lNode + " AND B.DELETED_FLAG=0 ");

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
                            //skhare7 
							objBuildSQL.Append(@"SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID,B.TAX_ID,B.FIRST_NAME"
													+ " FROM  ENTITY B WHERE B.PARENT_EID=" + p_lNode +
													" AND B.DELETED_FLAG=0 ");

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								objBuildSQL.Append(GetTriggerSQL("B", p_sTableName));
							}
							/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
							{
								objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + m_sSystemDate + "' AND" +
									" B.EFF_END_DATE>='" + m_sSystemDate + "')");
								
								if (m_sClaimDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
								if (m_sEventDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
								if (m_sPolicyDate != "")
									objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");
								objBuildSQL.Append (" OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR B.TRIGGER_DATE_FIELD IS NULL");
								objBuildSQL.Append (")");
							}*/
							//end MITS 8733
							objBuildSQL.Append(" ORDER BY B.LAST_NAME");

							//Fill the dataset
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, objBuildSQL.ToString(), m_iClientId);
							objBuildSQL.Remove(0, objBuildSQL.Length);
							//Get the next node information
							sOrgHierarchy = GetDepartmentXml(objDataSet, p_lNode, p_sParent, objXML, "F");
							break;
					}
				}

					//if user wants to see Org Tree for the first time
				else if ((p_sNodCat == "") && (p_bExpand == true))
				{
					//if Org preference XML is present
					if (GetOrgPreferenceXml(objXML) == 1)
					{
						string sXml = OpenToThisLevel(m_sNodCat, p_lLevel, objXML, p_sTableName, p_lRowId);

						XmlElement xmlTempEle = (XmlElement)objXML.SelectSingleNode("//OpenLevel");
						if (xmlTempEle == null)
							xmlTempEle = objXML.CreateElement("OpenLevel");
						xmlTempEle.InnerText = GetLevel(m_sNodCat).ToString();
						objXML.SelectSingleNode("//form").AppendChild(xmlTempEle);

						if (objNod != null)
							objNod.InnerText = m_sFilter;
						return objXML.OuterXml;
					}
					else
					{
						//objBuildSQL.Append ("SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID FROM ORG_HIERARCHY A,ENTITY B WHERE A.CLIENT_EID=B.ENTITY_ID AND B.DELETED_FLAG=0 AND A.CLIENT_EID<>0");
						//objBuildSQL.Append("SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID FROM ENTITY B WHERE DELETED_FLAG=0 AND ENTITY_TABLE_ID=1005");
						////Mukul 4/18/07 Added MITS 8733
						//if(m_sFilter=="1")
						//{
						//    objBuildSQL.Append(GetTriggerSQL("B",p_sTableName));
						//}
						/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
						{
							objBuildSQL.Append (" AND ((B.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND B.EFF_START_DATE<='" + m_sSystemDate + "' AND" +
								" B.EFF_END_DATE>='" + m_sSystemDate + "')");
							if (m_sClaimDate != "")
								objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND B.EFF_START_DATE<='" + m_sClaimDate + "' AND B.EFF_END_DATE>='" + m_sClaimDate + "') ");
							if (m_sEventDate != "")
								objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND B.EFF_START_DATE<='" + m_sEventDate + "' AND B.EFF_END_DATE>='" + m_sEventDate + "')");
							if (m_sPolicyDate != "")
								objBuildSQL.Append (" OR (B.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND B.EFF_START_DATE<='" + m_sPolicyDate + "' AND B.EFF_END_DATE>='" + m_sPolicyDate + "')");
							objBuildSQL.Append (" OR B.TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR B.TRIGGER_DATE_FIELD IS NULL");
							objBuildSQL.Append (")");
						}*/
						//End MITS 8733
						//objBuildSQL.Append (" ORDER BY B.LAST_NAME");

						////Fill the dataset
						//objDataSet = DbFactory.GetDataSet(m_sConnectionString,objBuildSQL.ToString());
						//objBuildSQL.Remove(0, objBuildSQL.Length);
						////Get the client information
						//sOrgHierarchy = GetClientXml (objXML,p_lNode, objDataSet);

						//Raman - Org Hierarchy Prf Imp
						//Opening tree to Client Level if default level is not set

						string sXml = OpenToThisLevel("C", 1, objXML, p_sTableName, p_lRowId);

						XmlElement xmlTempEle = (XmlElement)objXML.SelectSingleNode("//OpenLevel");
						if (xmlTempEle == null)
							xmlTempEle = objXML.CreateElement("OpenLevel");
						xmlTempEle.InnerText = GetLevel(m_sNodCat).ToString();
						objXML.SelectSingleNode("//form").AppendChild(xmlTempEle);

						if (objNod != null)
							objNod.InnerText = m_sFilter;
						return objXML.OuterXml;
					}
				}
				else
				{
					//Changed by: Nikhil Garg		Dated: 03/May/2005
					sOrgHierarchy = RemoveChildren(objXML, p_sNodCat, p_lNode, p_sParent);
					//p_sInputXml = RemoveChildren (objXML, p_sNodCat, p_lNode, p_sParent);
				}

				if (objNod != null)
					objNod.InnerText = m_sFilter;
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOrgHierarchyXml.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDataSet != null) objDataSet.Dispose();
				if (objReader != null) objReader.Dispose();
				objXML = null;
				objNod = null;
				objBuildSQL = null;
			}
			return (sOrgHierarchy);
		}
		public string GetOrgHierarchyXml(bool p_bExpand, long p_lLevel, long p_lNode, string p_sNodCat,
			string p_sParent, string p_sTableName, long p_lRowId, string p_sInputXml, out int p_iEntityEnh)
		{
			return GetOrgHierarchyXml(p_bExpand, p_lLevel, p_lNode, p_sNodCat, p_sParent, p_sTableName, p_lRowId, p_sInputXml, out p_iEntityEnh, "", "", "");//last value in method call added by Shivendu
		}
		#endregion

		#region Get specific Org Hierarchy
		/// Name		: GetSpecificOrgHierarchy
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the specific Organization Hierarchy information
		/// </summary>
		/// <param name="p_sNodCat">Node category</param>
		/// <param name="p_sInputXml">XML to be populated with the Org Hierarchy data</param>
		/// <param name="p_lLevel">Level of the entities</param>
		/// <param name="p_lNode">Current node id</param>
		/// <param name="p_sTreeNode">Tree node</param>
		/// <param name="p_sTableName">Table name</param>
		/// <param name="p_lRowId">Row Id</param>
		/// <returns>XML containing the Org Hierarchy data.</returns>
		public string GetSpecificOrgHierarchy(string p_sNodCat, string p_sInputXml, long p_lLevel, long p_lNode, string p_sTreeNode, string p_sTableName, long p_lRowId)
		{
			// JP 12-05-2005   Cleanup. Not used.    int iResult = 0;
			string sSql = "";
			string sRetXML = "";
			XmlDocument objXML = null;
			DataSet objDataSet = null;
			DbConnection objConn = null;
			DbTransaction objTrans = null;
			// JP 12-05-2005   Cleanup. Not used.    DbDataAdapter objAdap = null;

			try
			{
				objDataSet = new DataSet();
				objXML = new XmlDocument();
				objXML.LoadXml(p_sInputXml);
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				m_sDBType = objConn.DatabaseType.ToString();

				if (m_sDBType == Constants.DB_ORACLE)
				{
					objTrans = objConn.BeginTransaction();
				}

				m_lLevel = p_lLevel;
				objXML.SelectSingleNode("//control[@name='orgTree']").ParentNode["control"].SetAttribute("num_level", m_lLevel.ToString());

				m_sTable = p_sTableName;
				m_lRowId = p_lRowId;
				m_lSearchId = p_lNode;

				//Fetcj user preference XML from the database
				objXML = GetUserPreferenceXml(objXML);

				//if node categor is present
				if (p_sNodCat != "")
				{
					//Fetch the data corresponding to the given node category from database and generate corresponding XML
					switch (p_sNodCat.ToUpper().Trim())
					{
						case CLIENT:
							sSql = "SELECT DISTINCT H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A " +
								",ENTITY H WHERE A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

							objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
							sRetXML = GetXmlbyName(objXML, objDataSet, p_sNodCat);
							break;

						case COMPANY:
							sSql = "SELECT DISTINCT G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A " +
								",ENTITY G,ENTITY H WHERE A.COMPANY_EID=" +
								"G.ENTITY_ID AND A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

                            objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
							sRetXML = GetXmlbyName(objXML, objDataSet, p_sNodCat);
							break;

						case OPERATION:
							sSql = "SELECT DISTINCT F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB," +
								" G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A " +
								",ENTITY F,ENTITY G,ENTITY H" +
								" WHERE A.OPERATION_EID=F.ENTITY_ID AND A.COMPANY_EID=" +
								" G.ENTITY_ID AND A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

                            objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
							sRetXML = GetXmlbyName(objXML, objDataSet, p_sNodCat);
							break;

						case REGION:
							sSql = "SELECT DISTINCT E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB," +
								" G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A " +
								",ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE A.REGION_EID=E.ENTITY_ID AND" +
								" A.OPERATION_EID=F.ENTITY_ID AND A.COMPANY_EID=" +
								"G.ENTITY_ID AND A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

							objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
							sRetXML = GetXmlbyName(objXML, objDataSet, p_sNodCat);
							break;

						case DIVISION:
							sSql = "SELECT DISTINCT D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB,E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB," +
								" G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A " +
								",ENTITY D,ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE" +
								" A.DIVISION_EID=D.ENTITY_ID " +
								" AND A.REGION_EID=E.ENTITY_ID AND" +
								" A.OPERATION_EID=F.ENTITY_ID AND A.COMPANY_EID=" +
								"G.ENTITY_ID AND A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

							sRetXML = GenerateData(objXML, p_sNodCat, sSql, objConn);
							break;

						case LOCATION:
							sSql = "SELECT DISTINCT C.LAST_NAME L_NAME,C.ENTITY_ID LID,C.ABBREVIATION L_AB,D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB,E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB," +
								" G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A " +
								",ENTITY C,ENTITY D,ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE A.LOCATION_EID=" +
								" C.ENTITY_ID AND A.DIVISION_EID=D.ENTITY_ID AND" +
								" A.REGION_EID=E.ENTITY_ID AND" +
								" A.OPERATION_EID=F.ENTITY_ID AND A.COMPANY_EID=" +
								" G.ENTITY_ID AND A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0 AND C.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

							sRetXML = GenerateData(objXML, p_sNodCat, sSql, objConn);
							break;

						case FACILITY:
							sSql = "SELECT DISTINCT B.LAST_NAME F_NAME,B.ENTITY_ID FID,B.ABBREVIATION F_AB,C.LAST_NAME L_NAME,C.ENTITY_ID LID,C.ABBREVIATION L_AB,D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB,E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB," +
								" G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A,ENTITY B,ENTITY C,ENTITY D," +
								" ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE A.FACILITY_EID=B.ENTITY_ID AND A.LOCATION_EID=" +
								" C.ENTITY_ID AND A.DIVISION_EID=D.ENTITY_ID AND" +
								" A.REGION_EID=E.ENTITY_ID AND" +
								" A.OPERATION_EID=F.ENTITY_ID AND A.COMPANY_EID=" +
								" G.ENTITY_ID AND A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0 AND C.DELETED_FLAG=0 AND B.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

							sRetXML = GenerateData(objXML, p_sNodCat, sSql, objConn);
							break;

						case DEPARTMENT:
							sSql = "SELECT DISTINCT B1.LAST_NAME DT_NAME,B1.ENTITY_ID DTID,B1.ABBREVIATION DT_AB,B.LAST_NAME F_NAME,B.ENTITY_ID FID,B.ABBREVIATION F_AB,C.LAST_NAME L_NAME,C.ENTITY_ID LID,C.ABBREVIATION L_AB,D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB,E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB," +
								" G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ORG_HIERARCHY A,ENTITY B1,ENTITY B,ENTITY C,ENTITY D,ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE A.DEPARTMENT_EID=B1.ENTITY_ID AND" +
								" A.FACILITY_EID=B.ENTITY_ID AND A.LOCATION_EID=" +
								" C.ENTITY_ID AND A.DIVISION_EID=D.ENTITY_ID AND" +
								" A.REGION_EID=E.ENTITY_ID AND" +
								" A.OPERATION_EID=F.ENTITY_ID AND A.COMPANY_EID=" +
								" G.ENTITY_ID AND  A.CLIENT_EID=H.ENTITY_ID AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0 AND C.DELETED_FLAG=0 AND B.DELETED_FLAG=0 AND B1.DELETED_FLAG=0";

							if (p_lNode != 0)
								sSql = GetSql(sSql, p_sTreeNode, p_lNode);

							sRetXML = GenerateData(objXML, p_sNodCat, sSql, objConn);
							break;
					}
				}
				if (m_sDBType == Constants.DB_ORACLE)
				{
					objTrans.Commit();
				}
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
				if (objTrans != null) objTrans.Rollback();
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetSpecificOrgHierarchy.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objConn != null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn.Dispose();
				}
				if (objDataSet != null) objDataSet.Dispose();
				if (objTrans != null) objTrans.Dispose();
				// JP 12-05-2005   Cleanup. Not used.    objAdap = null;
				objXML = null;

			}
			return (sRetXML);
		}
		#endregion

		#region Remove children
		/// Name		: RemoveChildren
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Removes the invalid children from XML DOM
		/// </summary>
		/// <param name="p_objOrgInfo">XML DOM containing Org Hierarchy information</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <returns>Updated XML string with valid children</returns>
		private string RemoveChildren(XmlDocument p_objOrgInfo, string p_sNodCat, long p_lNode, string p_sParent)
		{
			string sNewNodename = "";
			XmlDocument objDomDoc = null;
			XmlElement objElm = null;
			XmlNodeList objXmlNodeList = null;

			try
			{
				p_objOrgInfo = RemoveError(p_objOrgInfo);
				objDomDoc = new XmlDocument();

				//Create node list with the given node id
				switch (p_sNodCat)
				{
					case CLIENT:
						objXmlNodeList = p_objOrgInfo.SelectNodes("//client[@id='" + p_lNode + "']");
						sNewNodename = "client";
						break;
					case COMPANY:
						objXmlNodeList = p_objOrgInfo.SelectNodes("//client/company[@id='" + p_lNode + "']");
						sNewNodename = "company";
						break;
					case OPERATION:
						objXmlNodeList = p_objOrgInfo.SelectNodes("//client/company/operation[@id='" + p_lNode + "']");
						sNewNodename = "operation";
						break;
					case REGION:
						objXmlNodeList = p_objOrgInfo.SelectNodes("//client/company/operation/region[@id='" + p_lNode + "']");
						sNewNodename = "region";
						break;
					case DIVISION:
						objXmlNodeList = p_objOrgInfo.SelectNodes("//client/company/operation/region/division[@id='" + p_lNode + "']");
						sNewNodename = "division";
						break;
					case LOCATION:
						objXmlNodeList = p_objOrgInfo.SelectNodes("//client/company/operation/region/division/location[@id='" + p_lNode + "']");
						sNewNodename = "location";
						break;
					case FACILITY:
						sNewNodename = "facility";
						objXmlNodeList = p_objOrgInfo.SelectNodes("//client/company/operation/region/division/location/facility[@id='" + p_lNode + "']");
						break;
				}

				//Remove children, which does not belong to valid parent node
				foreach (XmlElement objElem in objXmlNodeList)
				{
					if ((p_sParent != "0") && (p_sParent != ""))
					{
						if (objElem.GetAttribute("parent") == p_sParent)
						{
							//Changed by: Nikhil Garg		Dated: 03/May/2005
							objElm = objElem.OwnerDocument.CreateElement(sNewNodename);
							objElm.SetAttribute("id", objElem.GetAttribute("id"));
							objElm.SetAttribute("ischild", "1");
							objElm.SetAttribute("parent", objElem.GetAttribute("parent"));
							objElm.SetAttribute("name", objElem.GetAttribute("name"));
							objElm.SetAttribute("code", objElem.GetAttribute("code"));
							objElm.SetAttribute("child_name", objElem.GetAttribute("child_name"));

							//							objDomDoc.AppendChild (objDomDoc.CreateElement(sNewNodename));
							//							objDomDoc.DocumentElement.SetAttribute ("id", objElem.GetAttribute("id"));
							//							objDomDoc.DocumentElement.SetAttribute ("ischild", "1");
							//							objDomDoc.DocumentElement.SetAttribute ("parent", objElem.GetAttribute("parent"));
							//							objDomDoc.DocumentElement.SetAttribute ("name", objElem.GetAttribute("name"));
							//							objDomDoc.DocumentElement.SetAttribute ("code", objElem.GetAttribute("code"));
							//							objDomDoc.DocumentElement.SetAttribute ("child_name", objElem.GetAttribute("child_name"));
							//							objElm = objDomDoc.DocumentElement;
							objElem.ParentNode.InsertBefore(objElm, objElem);
							objElem.ParentNode.RemoveChild(objElem);
						}
					}
					else
					{
						//Changed by: Nikhil Garg		Dated: 03/May/2005
						objElm = objElem.OwnerDocument.CreateElement(sNewNodename);
						objElm.SetAttribute("id", objElem.GetAttribute("id"));
						objElm.SetAttribute("ischild", "1");
						objElm.SetAttribute("parent", "0");
						objElm.SetAttribute("name", objElem.GetAttribute("name"));
						objElm.SetAttribute("code", objElem.GetAttribute("code"));
						objElm.SetAttribute("child_name", objElem.GetAttribute("child_name"));

						//						objDomDoc.AppendChild (objDomDoc.CreateElement(sNewNodename));
						//						objDomDoc.DocumentElement.SetAttribute ("id", objElem.GetAttribute("id"));
						//						objDomDoc.DocumentElement.SetAttribute ("ischild", "1");
						//						objDomDoc.DocumentElement.SetAttribute ("parent", "0");
						//						objDomDoc.DocumentElement.SetAttribute ("name", objElem.GetAttribute("name"));
						//						objDomDoc.DocumentElement.SetAttribute ("code", objElem.GetAttribute("code"));
						//						objDomDoc.DocumentElement.SetAttribute ("child_name", objElem.GetAttribute("child_name"));
						//						objElm = objDomDoc.DocumentElement;

						objElem.ParentNode.InsertBefore(objElm, objElem);
						objElem.ParentNode.RemoveChild(objElem);
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.RemoveError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objDomDoc = null;
				objElm = null;
				objXmlNodeList = null;
			}
			return (p_objOrgInfo.InnerXml);
		}
		#endregion

		#region Get field name
		/// Name		: GetFieldName
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return the fields related to a table.
		/// </summary>
		/// <param name="p_sTname">Table name</param>
		/// <returns>Field name with the date</returns>
		private string GetFieldName(string p_sTname)
		{
			string sRetFieldName = "";
			try
			{
				switch (p_sTname.ToUpper())
				{
					case CLAIM:
						sRetFieldName = "('CLAIM.DATE_OF_CLAIM','EVENT.DATE_OF_EVENT','SYSTEM_DATE')";
						break;
					case EVENT:
						sRetFieldName = "('EVENT.DATE_OF_EVENT','SYSTEM_DATE')";
						break;
					case POLICY:
						sRetFieldName = "('POLICY.EFFECTIVE_DATE','SYSTEM_DATE')";
						break;
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetFieldName.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally { }
			return (sRetFieldName);
		}
		#endregion

		#region Set effective date
		/// Name		: SetEffectiveDate
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Sets the effective date
		/// </summary>
		/// <param name="p_sTableName">Table name</param>
		/// <param name="p_lRowId">Row Id</param>
		public void SetEffectiveDate(string p_sTableName, long p_lRowId)
		{
			string sSql = "";
			DbReader objReader = null;

			//Set effective date as per the table name
			try
			{
				switch (p_sTableName.ToUpper())
				{
					case EVENT:
						sSql = "SELECT DATE_OF_EVENT FROM EVENT WHERE EVENT_ID=" + p_lRowId;
						if (m_sEventDate == "")
						{
							objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

							if (objReader != null)
							{
								if (objReader.Read())
									m_sEventDate = Conversion.ConvertObjToStr(objReader.GetValue("date_of_event"));
							}
						}
						break;

					case POLICYENH://Case added by Shivendu for MITS 17249
						if (m_sPolicyDate == "")
						{
							sSql = "SELECT EFFECTIVE_DATE FROM POLICY_X_TERM_ENH WHERE POLICY_ID=" + p_lRowId + " ORDER BY TERM_ID DESC";
							objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

							if (objReader != null)
							{
								if (objReader.Read())
									m_sPolicyDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
							}
						}
						break;
					case POLICY:
						if (m_sPolicyDate == "")//If added by Shivendu for MITS 17249
						{
							sSql = "SELECT EFFECTIVE_DATE FROM POLICY WHERE POLICY_ID=" + p_lRowId;
							objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

							if (objReader != null)
							{
								if (objReader.Read())
									m_sPolicyDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
							}
						}
						break;

					case CLAIM:
					case "CLAIMGC":
					case "CLAIMWC":
					case "CLAIMVA":
					case "CLAIMDI":
						sSql = "SELECT A.DATE_OF_CLAIM,B.DATE_OF_EVENT FROM CLAIM A,EVENT B WHERE A.EVENT_ID=B.EVENT_ID AND A.CLAIM_ID=" + p_lRowId;
						if (m_sEventDate == "" && m_sClaimDate == "")
						{
							objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

							if (objReader != null)
							{
								if (objReader.Read())
								{
									m_sClaimDate = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CLAIM"));
									m_sEventDate = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_EVENT"));
								}
							}
						}
						break;
				}
				m_sSystemDate = Conversion.GetDate(DateTime.Now.ToShortDateString());
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.SetEffectiveDate.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objReader != null)
				{
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
					objReader.Dispose();
				}
			}
		}
		#endregion

		#region Get Company XML
		/// Name		: GetCompanyXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Company
		/// </summary>
		/// <param name="p_objDataSet">Data set containing Company data</param>
		/// <param name="p_lNode">Node id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_objXML">XML DOM to be populated with the Company data</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <returns>XML string containing Company information</returns>
		private string GetCompanyXml(DataSet p_objDataSet, long p_lNode, string p_sParent, XmlDocument p_objXML, string p_sNodCat)
		{
			XmlDocument objXMLDoc = null;
			XmlElement objElem = null;

			try
			{
				objXMLDoc = new XmlDocument();

				//Remove redundant nodes from the XML DOM, if there is any
				p_objXML = RemoveSibling(p_objXML, p_sParent, p_sNodCat, p_lNode);

				//Remove Error node, if there is any
				p_objXML = RemoveError(p_objXML);

				objElem = (XmlElement)p_objXML.SelectSingleNode("//client[@id='" + p_lNode + "']");

				//Set attributes
				objElem.SetAttribute("ischild", "0");
				objElem.SetAttribute("iscurrent", "1");

				for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
				{
					objXMLDoc.AppendChild(objXMLDoc.CreateElement("company"));

					if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
						objXMLDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

					if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
						objXMLDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
					else
						objXMLDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

					objXMLDoc.DocumentElement.SetAttribute("ischild", "1");
					objXMLDoc.DocumentElement.SetAttribute("child_name", "operation");
					objXMLDoc.DocumentElement.SetAttribute("level", "2");
					objXMLDoc.DocumentElement.SetAttribute("parent", p_lNode.ToString());
					objXMLDoc.DocumentElement.SetAttribute("code", "CO");
					objElem.AppendChild(p_objXML.ImportNode(objXMLDoc.DocumentElement, true));
					//Changed by: Nikhil Garg		Dated: 03/May/2005
					//Addd the below line
					objXMLDoc = new XmlDocument();
				}

			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetCompanyXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLDoc = null;
				objElem = null;
			}
			return (p_objXML.InnerXml);
		}
		#endregion

		#region Get Operation XML
		/// Name		: GetOperationXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Operation
		/// </summary>
		/// <param name="p_objDataSet">Data set containing Operation data</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_objXML">XML DOM to be populated with the Operation data</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <returns>XML string containing Operation information</returns>
		private string GetOperationXml(DataSet p_objDataSet, long p_lNode, string p_sParent, XmlDocument p_objXML, string p_sNodCat)
		{
			XmlDocument objXMLDoc = null;
			XmlNodeList objXMLNodeList = null;
			XmlElement objTempElement = null;

			try
			{
				objXMLDoc = new XmlDocument();

				//Remove redundant nodes from the XML DOM, if there is any
				p_objXML = RemoveSibling(p_objXML, p_sParent, p_sNodCat, p_lNode);

				//Remove Error node, if there is any
				p_objXML = RemoveError(p_objXML);

				objXMLNodeList = p_objXML.SelectNodes("//client/company[@id='" + p_lNode + "']");

				//Set attributes
				foreach (XmlElement objElem in objXMLNodeList)
				{
					if (objElem.GetAttribute("parent") == p_sParent)
					{
						objElem.SetAttribute("ischild", "0");
						objElem.SetAttribute("iscurrent", "1");

						for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
						{
							objXMLDoc.AppendChild(objXMLDoc.CreateElement("operation"));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
							else
								objXMLDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

							objXMLDoc.DocumentElement.SetAttribute("ischild", "1");
							objXMLDoc.DocumentElement.SetAttribute("child_name", "region");
							objXMLDoc.DocumentElement.SetAttribute("level", "3");
							objXMLDoc.DocumentElement.SetAttribute("parent", p_lNode.ToString());
							objXMLDoc.DocumentElement.SetAttribute("code", "O");
							objTempElement = objXMLDoc.DocumentElement;
							objElem.AppendChild(p_objXML.ImportNode(objXMLDoc.DocumentElement, true));
							//Changed by: Nikhil Garg		Dated: 03/May/2005
							//Addd the below line
							objXMLDoc = new XmlDocument();
						}
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOperationXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLDoc = null;
				objXMLNodeList = null;
				objTempElement = null;
			}
			return (p_objXML.InnerXml);
		}
		#endregion

		#region Get Region XML
		/// Name		: GetRegionXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Region
		/// </summary>
		/// <param name="p_objDataSet">Data set containing Region data</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_objXML">XML DOM to be populated with the Region data</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <returns>XML string containing Region information</returns>
		private string GetRegionXml(DataSet p_objDataSet, long p_lNode, string p_sParent, XmlDocument p_objXML, string p_sNodCat)
		{
			XmlDocument objXMLDoc = null;
			XmlNodeList objXMLNodeList = null;
			XmlElement objTempElement = null;

			try
			{
				objXMLDoc = new XmlDocument();

				//Remove redundant nodes from the XML DOM, if there is any
				p_objXML = RemoveSibling(p_objXML, p_sParent, p_sNodCat, p_lNode);

				//Remove Error node, if there is any
				p_objXML = RemoveError(p_objXML);

				objXMLNodeList = p_objXML.SelectNodes("//client/company/operation[@id='" + p_lNode + "']");

				//Set attributes
				foreach (XmlElement objElem in objXMLNodeList)
				{
					if (objElem.GetAttribute("parent") == p_sParent)
					{
						objElem.SetAttribute("ischild", "0");
						objElem.SetAttribute("iscurrent", "1");

						for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
						{
							objXMLDoc.AppendChild(objXMLDoc.CreateElement("region"));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
							else
								objXMLDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

							objXMLDoc.DocumentElement.SetAttribute("ischild", "1");
							objXMLDoc.DocumentElement.SetAttribute("child_name", "division");
							objXMLDoc.DocumentElement.SetAttribute("level", "4");
							objXMLDoc.DocumentElement.SetAttribute("parent", p_lNode.ToString());
							objXMLDoc.DocumentElement.SetAttribute("code", "R");
							objTempElement = objXMLDoc.DocumentElement;
							objElem.AppendChild(p_objXML.ImportNode(objXMLDoc.DocumentElement, true));
							//Changed by: Nikhil Garg		Dated: 03/May/2005
							//Addd the below line
							objXMLDoc = new XmlDocument();
						}
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetRegionXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLDoc = null;
				objXMLNodeList = null;
				objTempElement = null;
			}
			return (p_objXML.InnerXml);
		}
		#endregion

		#region Get Division XML
		/// Name		: GetDivisionXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Division
		/// </summary>
		/// <param name="p_objDataSet">Data set containing Division data</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_objXML">XML DOM to be populated with the Division data</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <returns>XML string containing Division information</returns>
		private string GetDivisionXml(DataSet p_objDataSet, long p_lNode, string p_sParent, XmlDocument p_objXML, string p_sNodCat)
		{
			XmlDocument objXMLDoc = null;
			XmlNodeList objXMLNodeList = null;
			XmlElement objTempElement = null;

			try
			{
				objXMLDoc = new XmlDocument();

				//Remove redundant nodes from the XML DOM, if there is any
				p_objXML = RemoveSibling(p_objXML, p_sParent, p_sNodCat, p_lNode);

				//Remove Error node, if there is any
				p_objXML = RemoveError(p_objXML);

				objXMLNodeList = p_objXML.SelectNodes("//client/company/operation/region[@id='" + p_lNode + "']");

				//Set attributes
				foreach (XmlElement objElem in objXMLNodeList)
				{
					if (objElem.GetAttribute("parent") == p_sParent)
					{
						objElem.SetAttribute("ischild", "0");
						objElem.SetAttribute("iscurrent", "1");

						for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
						{
							objXMLDoc.AppendChild(objXMLDoc.CreateElement("division"));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
							else
								objXMLDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

							objXMLDoc.DocumentElement.SetAttribute("ischild", "1");
							objXMLDoc.DocumentElement.SetAttribute("child_name", "location");
							objXMLDoc.DocumentElement.SetAttribute("level", "5");
							objXMLDoc.DocumentElement.SetAttribute("parent", p_lNode.ToString());
							objXMLDoc.DocumentElement.SetAttribute("code", "D");
							objTempElement = objXMLDoc.DocumentElement;
							objElem.AppendChild(p_objXML.ImportNode(objXMLDoc.DocumentElement, true));
							//Changed by: Nikhil Garg		Dated: 03/May/2005
							//Addd the below line
							objXMLDoc = new XmlDocument();
						}
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetDivisionXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLDoc = null;
				objXMLNodeList = null;
				objTempElement = null;
			}
			return (p_objXML.InnerXml);
		}
		#endregion

		#region Get Location XML
		/// Name		: GetLocationXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Location
		/// </summary>
		/// <param name="p_objDataSet">Data set containing Location data</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_objXML">XML DOM to be populated with the Location data</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <returns>XML string containing Location information</returns>
		private string GetLocationXml(DataSet p_objDataSet, long p_lNode, string p_sParent, XmlDocument p_objXML, string p_sNodCat)
		{
			XmlDocument objXMLDoc = null;
			XmlNodeList objXMLNodeList = null;
			XmlElement objTempElement = null;

			try
			{
				objXMLDoc = new XmlDocument();

				//Remove redundant nodes from the XML DOM, if there is any
				p_objXML = RemoveSibling(p_objXML, p_sParent, p_sNodCat, p_lNode);

				//Remove Error node, if there is any
				p_objXML = RemoveError(p_objXML);

				objXMLNodeList = p_objXML.SelectNodes("//client/company/operation/region/division[@id='" + p_lNode + "']");

				//Set attributes
				foreach (XmlElement objElem in objXMLNodeList)
				{
					if (objElem.GetAttribute("parent") == p_sParent)
					{
						objElem.SetAttribute("ischild", "0");
						objElem.SetAttribute("iscurrent", "1");

						for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
						{
							objXMLDoc.AppendChild(objXMLDoc.CreateElement("location"));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
							else
								objXMLDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

							objXMLDoc.DocumentElement.SetAttribute("ischild", "1");
							objXMLDoc.DocumentElement.SetAttribute("child_name", "facility");
							objXMLDoc.DocumentElement.SetAttribute("level", "6");
							objXMLDoc.DocumentElement.SetAttribute("parent", p_lNode.ToString());
							objXMLDoc.DocumentElement.SetAttribute("code", "L");
							objTempElement = objXMLDoc.DocumentElement;
							objElem.AppendChild(p_objXML.ImportNode(objXMLDoc.DocumentElement, true));
							//Changed by: Nikhil Garg		Dated: 03/May/2005
							//Addd the below line
							objXMLDoc = new XmlDocument();
						}
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetLocationXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLDoc = null;
				objXMLNodeList = null;
				objTempElement = null;
			}
			return (p_objXML.InnerXml);
		}
		#endregion

		#region Get Facility XML
		/// Name		: GetFacilityXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Facility
		/// </summary>
		/// <param name="p_objDataSet">Data set containing Facility data</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_objXML">XML DOM to be populated with the Facility data</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <returns>XML string containing Facility information</returns>
		private string GetFacilityXml(DataSet p_objDataSet, long p_lNode, string p_sParent, XmlDocument p_objXML, string p_sNodCat)
		{
			XmlDocument objXMLDoc = null;
			XmlNodeList objXMLNodeList = null;
			XmlElement objTempElement = null;

			try
			{
				objXMLDoc = new XmlDocument();

				//Remove redundant nodes from the XML DOM, if there is any
				p_objXML = RemoveSibling(p_objXML, p_sParent, p_sNodCat, p_lNode);

				//Remove Error node, if there is any
				p_objXML = RemoveError(p_objXML);

				objXMLNodeList = p_objXML.SelectNodes("//client/company/operation/region/division/location[@id='" + p_lNode + "']");

				//Set attributes
				foreach (XmlElement objElem in objXMLNodeList)
				{
					if (objElem.GetAttribute("parent") == p_sParent)
					{
						objElem.SetAttribute("ischild", "0");
						objElem.SetAttribute("iscurrent", "1");

						for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
						{
							objXMLDoc.AppendChild(objXMLDoc.CreateElement("facility"));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

							if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
								objXMLDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
							else
								objXMLDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

							objXMLDoc.DocumentElement.SetAttribute("ischild", "1");
							objXMLDoc.DocumentElement.SetAttribute("child_name", "department");
							objXMLDoc.DocumentElement.SetAttribute("level", "7");
							objXMLDoc.DocumentElement.SetAttribute("parent", p_lNode.ToString());
							objXMLDoc.DocumentElement.SetAttribute("code", "F");
							objTempElement = objXMLDoc.DocumentElement;
							objElem.AppendChild(p_objXML.ImportNode(objXMLDoc.DocumentElement, true));
							//Changed by: Nikhil Garg		Dated: 03/May/2005
							//Addd the below line
							objXMLDoc = new XmlDocument();
						}
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetFacilityXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLDoc = null;
				objXMLNodeList = null;
				objTempElement = null;
			}
			return (p_objXML.InnerXml);
		}
		#endregion

		#region Get Department XML
		/// Name		: GetDepartmentXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Department
		/// </summary>
		/// <param name="p_objDataSet">Data set containing Department data</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_objXML">XML DOM to be populated with the Department data</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <returns>XML string containing Department information</returns>
		private string GetDepartmentXml(DataSet p_objDataSet, long p_lNode, string p_sParent, XmlDocument p_objXML, string p_sNodCat)
		{
			XmlDocument objXMLDoc = null;
			XmlElement objElem = null;

			try
			{
				objXMLDoc = new XmlDocument();

				//Remove redundant nodes from the XML DOM, if there is any
				p_objXML = RemoveSibling(p_objXML, p_sParent, p_sNodCat, p_lNode);

				//Remove Error node, if there is any
				p_objXML = RemoveError(p_objXML);

				objElem = (XmlElement)p_objXML.SelectSingleNode("//client/company/operation/region/division/location/facility[@id='" + p_lNode + "']");

				//Set attributes
				objElem.SetAttribute("ischild", "0");
				objElem.SetAttribute("iscurrent", "1");

				for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
				{
					objXMLDoc.AppendChild(objXMLDoc.CreateElement("department"));

					if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
						objXMLDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

					if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
						objXMLDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
					else
						objXMLDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

					objXMLDoc.DocumentElement.SetAttribute("ischild", "");
					objXMLDoc.DocumentElement.SetAttribute("level", "8");
					//tkr mits 10267 
					objXMLDoc.DocumentElement.SetAttribute("parent", p_sParent);
					objElem.AppendChild(p_objXML.ImportNode(objXMLDoc.DocumentElement, true));
					//Changed by: Nikhil Garg		Dated: 03/May/2005
					//Addd the below line
					objXMLDoc = new XmlDocument();
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetDepartmentXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLDoc = null;
				objElem = null;
			}
			return (p_objXML.InnerXml);
		}
		#endregion

		#region Get Client XML
		/// Name		: GetClientXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches Organization Hierarchy data for Client
		/// </summary>
		/// <param name="p_objOrgInfo">XML DOM containing Org Hierarchy information</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_objDataSet">Data set containing Client data</param>
		/// <returns>XML string containing Client information</returns>
		private string GetClientXml(XmlDocument p_objOrgInfo, long p_lNode, DataSet p_objDataSet)
		{
			XmlNodeList objXMLNodeList = null;
			XmlDocument objDomDoc = null;
			XmlElement objElm = null;

			try
			{
				//Remove Error node, if there is any
				p_objOrgInfo = RemoveError(p_objOrgInfo);
				objDomDoc = new XmlDocument();
				objXMLNodeList = p_objOrgInfo.GetElementsByTagName("org");

				//Set attributes
				foreach (XmlElement objElem in objXMLNodeList)
				{
					for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
					{
						objDomDoc.AppendChild(objDomDoc.CreateElement("client"));

						if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]) != ""))
							objDomDoc.DocumentElement.SetAttribute("id", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));

						if ((Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != null) || (Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]) != ""))
							objDomDoc.DocumentElement.SetAttribute("name", JointAbbr(Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["LAST_NAME"]), Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"])));
						else
							objDomDoc.DocumentElement.SetAttribute("name", NO_ENTITY);

						objDomDoc.DocumentElement.SetAttribute("ischild", "1");
						objDomDoc.DocumentElement.SetAttribute("child_name", "company");
						objDomDoc.DocumentElement.SetAttribute("level", "1");
						objDomDoc.DocumentElement.SetAttribute("code", "C");
						objElm = objDomDoc.DocumentElement;
						objElem.AppendChild(p_objOrgInfo.ImportNode(objDomDoc.DocumentElement, true));
						//Changed by: Nikhil Garg		Dated: 03/May/2005
						//Addd the below line
						objDomDoc = new XmlDocument();
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetClientXml.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLNodeList = null;
				objDomDoc = null;
				objElm = null;
			}
			return (p_objOrgInfo.InnerXml);
		}
		#endregion

		#region Get Org Preference XML
		/// Name		: GetOrgPreferenceXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the preference XML containing user preference settings
		/// </summary>
		/// <param name="p_objXmlDoc">XML DOM containing Org Hierarchy XML</param>
		/// <returns>integer value indicating the user preference</returns>
		private int GetOrgPreferenceXml(XmlDocument p_objXmlDoc)
		{
			int iPrefXml = 0;
			string sSql = string.Empty;
			string sXml = string.Empty;
			XmlElement objElem = null;
			XmlDocument objPrefXml = null;
			XmlNodeList objNode = null;
			XmlElement objNewElem = null;
			DbReader objReader = null;
			DbConnection objConn = null;
			DbCommand objCommand = null;
			DbParameter objParam = null;

			try
			{
				objPrefXml = new XmlDocument();
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				objCommand = objConn.CreateCommand();
				objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//form/group/control[@name='dlevel']");

				//Set node category and user id depending upon the value present in passed XML DOM
				if (objElem != null)
				{
					if (m_bIsError == false)
					{
						m_sNodCat = objElem.InnerText;
						if (m_sNodCat == null)
							m_sNodCat = "";
						else
							objElem.InnerText = m_sNodCat;
					}
					if (objElem.GetAttribute("userid") != "")
						m_lUserId = Conversion.ConvertStrToLong(objElem.GetAttribute("userid"));
				}

				sSql = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + m_lUserId;
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
				if (objReader.Read())
				{
					sXml = Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
				}
				objReader.Close();

				if (sXml != string.Empty)
				{
					try
					{
						objPrefXml.LoadXml(sXml);
					}
					catch (Exception e)
					{
						//delete bad formatted xml string from database
						sSql = "DELETE FROM USER_PREF_XML WHERE USER_ID=" + m_lUserId;
						objCommand.Parameters.Clear();
						objCommand.CommandText = sSql;
						objCommand.ExecuteNonQuery();
						sXml = string.Empty;
					}
				}

				//if node category is not present then fetch the relevant information from the database
				if (m_sNodCat == string.Empty)
				{
					if (sXml != string.Empty)
					{
						objElem = (XmlElement)objPrefXml.SelectSingleNode("//OrgViewNet");
						if (objElem != null)
						{
							m_sNodCat = objElem.GetAttribute("vid");
							if (m_sFilter == "")
								m_sFilter = objElem.GetAttribute("filter");
							iPrefXml = 1;
						}

						if (m_sFilter != "")
						{
							//Update the filter flag to User pref
							objElem = (XmlElement)objPrefXml.SelectSingleNode("//OrgViewNet");
							if (objElem != null)
							{
								objElem.SetAttribute("filter", m_sFilter);
							}
							else
							{
								objElem = (XmlElement)objPrefXml.SelectSingleNode("setting");
								objNewElem = (XmlElement)objPrefXml.ImportNode(GetNewElement("OrgViewNet"), true);
								objNewElem.SetAttribute("vid", m_sNodCat);
								objNewElem.SetAttribute("filter", m_sFilter);
								objElem.AppendChild(objNewElem);
							}
							sXml = objPrefXml.InnerXml;

							//Update USER_PREF_XML table with the updated XML
							sSql = "UPDATE USER_PREF_XML SET PREF_XML=~PXML~ WHERE USER_ID=" + m_lUserId;
							objCommand.Parameters.Clear();
							objParam = objCommand.CreateParameter();
							objParam.Direction = ParameterDirection.Input;
							objParam.Value = sXml;
							objParam.ParameterName = "PXML";
							objParam.SourceColumn = "PREF_XML";
							objCommand.Parameters.Add(objParam);
							objCommand.CommandText = sSql;
							objCommand.ExecuteNonQuery();
						}

						//if node category is not present then give default as "C" i.e. Client
						if (m_sNodCat == "")
							m_sNodCat = "C";
					}
				}
				else
				{
					iPrefXml = 1;
					//if node category is present then update the XML DOM
					if (sXml != string.Empty)
					{
						objElem = (XmlElement)objPrefXml.SelectSingleNode("//OrgViewNet");
						if (objElem != null)
						{
							if (m_sFilter == "")
								m_sFilter = objElem.GetAttribute("filter");
							objElem.SetAttribute("vid", m_sNodCat);
							objElem.SetAttribute("filter", m_sFilter);
						}
						else
						{
							objElem = (XmlElement)objPrefXml.SelectSingleNode("setting");
							objNewElem = (XmlElement)objPrefXml.ImportNode(GetNewElement("OrgViewNet"), true);
							objNewElem.SetAttribute("vid", m_sNodCat);
							objNewElem.SetAttribute("filter", m_sFilter);
							objElem.AppendChild(objNewElem);
						}
						sXml = objPrefXml.InnerXml;

						//Update USER_PREF_XML table with the updated XML
						sSql = "UPDATE USER_PREF_XML SET PREF_XML=~PXML~ WHERE USER_ID=" + m_lUserId;
						objCommand.Parameters.Clear();
						objParam = objCommand.CreateParameter();
						objParam.Direction = ParameterDirection.Input;
						objParam.Value = sXml;
						objParam.ParameterName = "PXML";
						objParam.SourceColumn = "PREF_XML";
						objCommand.Parameters.Add(objParam);
						objCommand.CommandText = sSql;
						objCommand.ExecuteNonQuery();
					}
					else
					{
						//if user xml is not present in the database then insert a record for the same
						sXml = "<setting><OrgViewNet filter='1' vid='" + m_sNodCat + "'></OrgViewNet></setting>";
						sSql = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + m_lUserId + ", ~PXML~)";
						objCommand.Parameters.Clear();
						objParam = objCommand.CreateParameter();
						objParam.Direction = ParameterDirection.Input;
						objParam.Value = sXml;
						objParam.ParameterName = "PXML";
						objParam.SourceColumn = "PREF_XML";
						objCommand.Parameters.Add(objParam);
						objCommand.CommandText = sSql;
						objCommand.ExecuteNonQuery();
					}
				}

				//Set the "selected" attribute if node category is present
				if (m_sNodCat != string.Empty)
				{
					objNode = p_objXmlDoc.SelectNodes("//levellist/option");
					foreach (XmlElement objElemCnt in objNode)
					{
						if (objElemCnt.GetAttribute("value") == m_sNodCat)
							objElemCnt.SetAttribute("selected", "1");
						else
						{
							objElemCnt.RemoveAttribute("selected");
						}
					}
				}

                //Start rsushilaggar MITS 18959 Date 04/05/2011
                if (!string.IsNullOrEmpty(m_sFilter))
                {
                    XmlNode objNodeFilter = p_objXmlDoc.SelectSingleNode("//chkFilter");
                    if (objNodeFilter != null)
                        objNodeFilter.InnerText = m_sFilter;
                }
                //End rsushilaggar
			}
			//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOrgPreferenceXml.GeneralError",m_iClientId),
                    p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objReader != null)
				{
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn.Dispose();
				}
				objElem = null;
				objPrefXml = null;
				objNode = null;
				objNewElem = null;
			}
			return (iPrefXml);
		}
		#endregion

		#region Get new element
		/// Name		: GetNewElement
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creats a new XMLElement with the nodename passed as parameter
		/// </summary>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>XML element to be added in the DOM</returns>
		private XmlElement GetNewElement(string p_sNodeName)
		{
            //XmlDocument objXDoc;
            //XmlElement objXMLElement;

            //try
            //{
            //    objXDoc = new XmlDocument();
            //    objXMLElement = objXDoc.CreateElement(p_sNodeName);
            //    return objXMLElement;
            //}
            //catch (Exception p_objException)
            //{
            //    throw new RMAppException(Globalization.GetString("OrgHierarchy.GetNewElement.XMLError"), p_objException);
            //}
            //finally
            //{
            //    objXDoc = null;
            //    objXMLElement = null;
            //}

            

            //try
            //{

            m_objXDoc = new XmlDocument();            
            return m_objXDoc.CreateElement(p_sNodeName);

            //return objXMLElement;

            //}
            //catch (Exception p_objException)
            //{
            //    throw new RMAppException(Globalization.GetString("OrgHierarchy.GetNewElement.XMLError"), p_objException);
            //}
            //finally
            //{

            //objXDoc = null;
            //objXMLElement = null;

            //}

		}
		#endregion

		#region Open Org Hierarchy level

		/// Name		: OpenToThisLevel
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 09/26/2005	   * Changed the query and removed the case statement. Call is made to MakeXML method * Mihika Agrawal
		///************************************************************
		/// <summary>
		/// Opens the Organization Hierarchy level
		/// </summary>
		/// <param name="p_sNodCat">Node category</param>
		/// <param name="p_lLevel">Level of the current node</param>
		/// <param name="p_objOrgInfo">XML DOM containing Org Hierarchy information</param>
		/// <param name="p_sTableName">Table name</param>
		/// <param name="p_lRowId">Row Id</param>
		/// <returns>XML string containing Org Hierarchy level</returns>
		private string OpenToThisLevel(string p_sNodCat, long p_lLevel, XmlDocument p_objOrgInfo, string p_sTableName,
			long p_lRowId)
		{
			string sSql = "";
			string sRetXML = "";
			DataSet objDataSet = null;
			XmlElement objElem = null;
			DbConnection objConn = null;
			RMConfigurator objConfig = null;
			int iMax_Number_Item = 0;
			int iLevel = 0;
			bool bFirstTime = true;
			int iCount = 0;
            //Deb: Performance changes
            //string[] arrEntNames = { "CLIENT", "COMPANY", "OPERATION", "REGION", "DIVISION", "LOCATION", "FACILITY", "DEPARTMENT" };
            string[] arrTableEntId = { "1005", "1006", "1007", "1008", "1009", "1010", "1011", "1012" };
            //Deb: Performance changes
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				m_sDBType = objConn.DatabaseType.ToString();

				//Set class level variables
				m_lLevel = p_lLevel;
				m_sNodCat = p_sNodCat;
				m_sTable = p_sTableName;
				m_lRowId = p_lRowId;

				objElem = (XmlElement)p_objOrgInfo.SelectSingleNode("//org[@name='Organization Hierarchy']");
				objElem.InnerText = "Organization Data";
				objConfig = new RMConfigurator();
              
				iMax_Number_Item = Conversion.ConvertStrToInteger(m_OrgHierarchySettings["Maximum_Number_Item"].ToString());
				//If the current org level return too many entries, try the next higher level
				//while(bFirstTime || iCount > 3500)
				while (bFirstTime || iCount > iMax_Number_Item)
				{
					//Set the attributes for the given XML DOM
					objElem = (XmlElement)p_objOrgInfo.SelectSingleNode("//control[@name='orgTree']");
					if (objElem != null)
					{
						objElem.SetAttribute("num_level", p_lLevel.ToString());
						p_objOrgInfo.SelectSingleNode("//control[@name='orgTree']");
						objElem.SetAttribute("cmblevel", m_sNodCat);
					}

					objElem = (XmlElement)p_objOrgInfo.SelectSingleNode("//control[@name='dlevel']");
					if (objElem != null)
						objElem.InnerText = m_sNodCat;

					//Set effective date depending upon the table name
                    if (p_sTableName != "" && p_sTableName != "nothing")
                    {
                        //Deb: Performance Changes
                        if (p_lRowId != 0)
                        {
                            SetEffectiveDate(p_sTableName, p_lRowId);
                        }
                        else
                        {
                            m_sSystemDate = Conversion.GetDate(DateTime.Now.ToShortDateString());
                        }
                        //Deb: Performance Changes
                    }

					//if node category is present
					if (m_sNodCat != "")
					{
						if (m_sDBType == "DBMS_IS_ORACLE")
							// Start Naresh MITS 8458 Date 28/11/2006
							sSql = "SELECT ENTITY_ID, LAST_NAME LastName,ABBREVIATION || '-' || LAST_NAME  DISPLAY, PARENT_EID ,ENTITY_TABLE_ID , SYSTEM_TABLE_NAME ";
						// End Naresh MITS 8458 Date 28/11/2006
						else
							// Start Naresh MITS 8458 Date 28/11/2006
							sSql = "SELECT ENTITY_ID, LAST_NAME LastName,ABBREVIATION + '-' + LAST_NAME  DISPLAY, PARENT_EID ,ENTITY_TABLE_ID , SYSTEM_TABLE_NAME ";
						// End Naresh MITS 8458 Date 28/11/2006

						sSql += "FROM ENTITY, GLOSSARY " +
							"WHERE ENTITY_ID<>0 AND ENTITY.DELETED_FLAG<>-1 " +
							"AND ENTITY.ENTITY_TABLE_ID = GLOSSARY.TABLE_ID " +
							//"AND ENTITY_TABLE_ID IN ( SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME IN (";
                            //Deb: Performance Changes
                            "AND ENTITY_TABLE_ID IN (";
						iLevel = GetLevel(m_sNodCat);
						for (int iCnt = 0; iCnt < iLevel; iCnt++)
						{
							//sSql = sSql + "'" + arrEntNames[iCnt] + "'";
                            //Deb :Performance Changes
                            sSql = sSql + arrTableEntId[iCnt];
							if (iCnt != iLevel - 1)
								sSql = sSql + ",";
							else
								//sSql = sSql + "))";
                                //Deb : Performance changes
                                sSql = sSql + ")";
						}
						//pmittal5 Confidential Record
						if (IsBesEnabled)
						{
							string sCountEntityMap = string.Empty;
							sCountEntityMap = objConn.ExecuteScalar("SELECT COUNT(ENTITY_ID) FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND GROUP_MAP.DELETED_FLAG = 0 AND ORG_SECURITY_USER.USER_ID= " + m_iUserId).ToString();
							//For Admin Groups, only one row with Entity_Id = 0 is there in Entity_Map
							if(Conversion.ConvertStrToInteger(sCountEntityMap) > 1)
								sSql = sSql + " AND ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
						}
						//End
						//Mukul 4/18/07 Added MITS 8733
						if (m_sFilter == "1")
						{
							sSql = sSql + GetTriggerSQL("", p_sTableName);
						}
						/*if ((p_sTableName != "") && (p_sTableName != "nothing") && (m_sFilter=="1"))
						{
							string sFieldName = GetFieldName(m_sTable);
							sSql = sSql + " AND ((TRIGGER_DATE_FIELD='SYSTEM_DATE' AND EFF_START_DATE<='" + m_sSystemDate + "' AND EFF_END_DATE>='" + m_sSystemDate + "')";
			
							if(m_sClaimDate != "")
								sSql = sSql + " OR (TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND EFF_START_DATE<='" + m_sClaimDate  + "' AND EFF_END_DATE>='" + m_sClaimDate + "')";

							if(m_sEventDate != "")
								sSql = sSql + " OR (TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND EFF_START_DATE<='" + m_sEventDate + "' AND EFF_END_DATE>='" + m_sEventDate + "')";
			
							if(m_sPolicyDate != "")
								sSql = sSql + " OR (TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND EFF_START_DATE<='" + m_sPolicyDate + "' AND EFF_END_DATE>='" + m_sPolicyDate + "')";

							sSql = sSql + " OR TRIGGER_DATE_FIELD NOT IN " + sFieldName + " OR TRIGGER_DATE_FIELD IS NULL";
							sSql = sSql + ")";

						}*/
						//END MITS 8733
						objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql,m_iClientId);

						// See if too many items came back for display. If so, display a message and reset level to next level (which realistically will never have too many items).
						/*Commented By Rahul to let whole of the data to come as XML.*/
						/*Can be Uncommented if the tree is taking much time to load for heavy data.*/
						iCount = objDataSet.Tables[0].Rows.Count;
						//if ( iCount > 3500)
						if (iCount > iMax_Number_Item && iLevel > 1)
						{
							if (bFirstTime)
							{
								XmlElement objXMLChildEle = null;
								XmlElement objXMLErrorEle = null;
								objXMLErrorEle = (XmlElement)p_objOrgInfo.SelectSingleNode("//org[@name='Organization Hierarchy']");
								objXMLChildEle = (XmlElement)p_objOrgInfo.ImportNode(GetNewElement("error"), true);
								objXMLChildEle.SetAttribute("id", "0");
								objXMLChildEle.SetAttribute("name", "norecord");
                                objXMLChildEle.SetAttribute("desc", Globalization.GetString("OrgHierarchy.TooManyItems", m_iClientId));//sharishkumar Rmacloud
								objXMLChildEle.SetAttribute("value", "1");
								objXMLErrorEle.AppendChild(objXMLChildEle);
								m_bIsError = true;
							}
							iLevel = iLevel - 1;
							m_sNodCat = GetCatFromLevel(iLevel);
							objDataSet.Dispose();

							//Set the default level to the new level
							GetOrgPreferenceXml(p_objOrgInfo);
						}
						bFirstTime = false;
					}
				}
				sRetXML = MakeXML(p_objOrgInfo, objDataSet, m_sNodCat);
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.OpenToThisLevel.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objConn != null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn.Dispose();
				}
				if (objDataSet != null) objDataSet.Dispose();
				objElem = null;
				objConfig = null;
			}
			return (sRetXML);
		}
		#endregion

		#region Cloning Functionality

		/// Name		: LoadOrgList
		/// Author		: Raman Bhatia
		/// Date Created: 09/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads the various levels of organisation hierarchy between specified lower and upper levels
		/// </summary>
		/// <param name="p_lLower">Lower Level TABLE_ID</param>
		/// <param name="p_lUpper">Upper Level TABLE_ID</param>
		/// <returns>XML containing Org Hierarchy level</returns>
		public XmlDocument LoadOrgList(long p_lLower, long p_lUpper)
		{
			string sSQL = "";
			XmlDocument objXMLOut = null;
			XmlElement objXMLElement = null;
			XmlElement objChildElement = null;
			DbReader objReader = null;

			try
			{
				sSQL = "SELECT TABLE_ID, TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID BETWEEN " + p_lLower + " AND " + p_lUpper + " ORDER BY TABLE_ID";
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

				objXMLOut = new XmlDocument();
				objXMLElement = objXMLOut.CreateElement("OrganisationHierarchyLevels");

				if (objReader != null)
				{
					while (objReader.Read())
					{
						objChildElement = objXMLOut.CreateElement("TableName");
						objChildElement.InnerText = objReader.GetValue("TABLE_NAME").ToString();
						objChildElement.SetAttribute("TableId", objReader.GetValue("TABLE_ID").ToString());
						objXMLElement.AppendChild(objChildElement);
					}
				}
				objXMLOut.AppendChild(objXMLElement);
			}

			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.LoadOrgList.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objReader != null)
				{
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
					objReader.Dispose();
				}
				objChildElement = null;
				objXMLElement = null;
			}
			return (objXMLOut);
		}

		/// Name		: CreateClone
		/// Author		: Raman Bhatia
		/// Date Created: 09/22/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
        /// 10/19/2010   *   Implemening cloning by datamodel  *  Raman Bhatia  MITS 22695 
		/// <summary>
		/// Creates the Clones of any particular organisation levels
		/// </summary>
		/// <param name="p_bCloneMultipleLevels">CloneMultipleLevels Flag</param>
		/// <param name="p_lDesiredCloningLevel">Desired Cloning Level</param>
		/// <param name="p_lSelectedLevelId">Selected Level Entry ID</param>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DSN name</param>
		/// <param name="p_sConnectionString">Connection String</param>
		/// <param name="p_objCompanyInfoXML">XML string to be populated with the Company information</param>
		/// <returns>XML containing Org Hierarchy level</returns>
		public XmlDocument CreateClone(bool p_bCloneMultipleLevels, long p_lDesiredCloningLevel, long p_lSelectedLevelId, string p_sUserName, string p_sPassword, string p_sDsnName, string p_sConnectionString, XmlDocument p_objCompanyInfoXML)
		{
            /// 10/19/2010   *   Implemening cloning by datamodel  *  Raman Bhatia  MITS 22695
            
            //string sLastName = "";
            //string sFirstName = "";
            //string sAddress1 = "";
            //string sAddress2 = "";
            //string sCity = "";
            //string sCountry = "";
            //string sState = "";
            //string sStateId = "";
            //string sCountryId = "";
            //string sZip = "";
			long lNextTableID = 0;
			long lTableID = 0;
            //Start: rsushilaggar MITS 21454 21-Jul-2010
            //string sEntityApprovalStatusId = string.Empty;
            //string sRejectReasonText = string.Empty;
            //string sRejectReasonText_HTML = string.Empty;
            //End: rsushilaggar
			XmlDocument objXMLOut = null;
			//XmlDocument objCmpInfoXML = null;
			//XmlElement objXMLElement = null;
            DataModelFactory objDMF = null;
            Entity objEntity = null;
            string sSQL = string.Empty;

			try
			{
				//objCmpInfoXML = new XmlDocument();
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);//sharishkumar Rmacloud
                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                sSQL = String.Format("Select Entity_Table_Id from Entity where Entity_Id = '{0}'", p_lSelectedLevelId.ToString());
                lTableID = Conversion.ConvertObjToInt64(objEntity.Context.DbConn.ExecuteScalar(sSQL), m_iClientId); 
				// Mihika 27-Feb-2006 EditCompanyInformation() is changed to handle an extra parameter. Passing on a default value of 0.
				//objCmpInfoXML.InnerXml = EditCompanyInformation(Convert.ToInt32(p_lSelectedLevelId), p_sUserName, p_sPassword, p_sDsnName, p_objCompanyInfoXML.OuterXml, 0);

				//objXMLElement = (XmlElement)objCmpInfoXML.SelectSingleNode("//EntityTableId");
				//lTableID = Conversion.ConvertStrToLong(objXMLElement.InnerText);

				if (p_bCloneMultipleLevels == false)
				{
					p_lDesiredCloningLevel = lTableID + 1;
				}

				//sLastName = objCmpInfoXML.SelectSingleNode("//LastName").InnerText;
				//sFirstName = objCmpInfoXML.SelectSingleNode("//FirstName").InnerText;
				//sAddress1 = objCmpInfoXML.SelectSingleNode("//Addr1").InnerText;
                //sAddress2 = objCmpInfoXML.SelectSingleNode("//Addr2").InnerText;
                //sCity = objCmpInfoXML.SelectSingleNode("//City").InnerText;
                //sCountry = objCmpInfoXML.SelectSingleNode("//CountryCode").InnerText;
                //sState = objCmpInfoXML.SelectSingleNode("//StateId").InnerText;
                //sZip = objCmpInfoXML.SelectSingleNode("//ZipCode").InnerText;
                //sStateId = objCmpInfoXML.SelectSingleNode("//StateId/@codeid").InnerText;
                //sCountryId = objCmpInfoXML.SelectSingleNode("//CountryCode/@codeid").InnerText;
                //Start: rsushilaggar MITS 21454 21-Jul-2010
                //sEntityApprovalStatusId = objCmpInfoXML.SelectSingleNode("//EntityApprovalStatusCode/@codeid").InnerText;
                //sRejectReasonText = objCmpInfoXML.SelectSingleNode("//RejectReasonText").InnerText;
                //sRejectReasonText_HTML = objCmpInfoXML.SelectSingleNode("//RejectReasonText_HTMLComments").InnerText;
                //End: rsushilaggar

				for (int i = 1; i <= (p_lDesiredCloningLevel - lTableID); i++)
				{
					lNextTableID = lTableID + i;

					//filling data in our comanyinfo XML
                    //p_objCompanyInfoXML.SelectSingleNode("//LastName").InnerText = sLastName;
                    //p_objCompanyInfoXML.SelectSingleNode("//FirstName").InnerText = sFirstName;
                    //p_objCompanyInfoXML.SelectSingleNode("//Addr1").InnerText = sAddress1;
                    //p_objCompanyInfoXML.SelectSingleNode("//Addr2").InnerText = sAddress2;
                    //p_objCompanyInfoXML.SelectSingleNode("//City").InnerText = sCity;
                    //p_objCompanyInfoXML.SelectSingleNode("//CountryCode").InnerText = sCountry;
                    //p_objCompanyInfoXML.SelectSingleNode("//StateId").InnerText = sState;
                    //p_objCompanyInfoXML.SelectSingleNode("//ZipCode").InnerText = sZip;
                    //p_objCompanyInfoXML.SelectSingleNode("//StateId/@codeid").InnerText = sStateId;
                    //p_objCompanyInfoXML.SelectSingleNode("//CountryCode/@codeid").InnerText = sCountryId;

					//creating the cloned entity
                    //p_objCompanyInfoXML.SelectSingleNode("//EntityTableId").InnerText = lNextTableID.ToString();
                    //p_objCompanyInfoXML.SelectSingleNode("//ParentEID").InnerText = p_lSelectedLevelId.ToString();
                    //p_objCompanyInfoXML.SelectSingleNode("//Abbreviation").InnerText = p_lSelectedLevelId.ToString();

                    //Start: rsushilaggar MITS 21454 21-Jul-2010
                    //p_objCompanyInfoXML.SelectSingleNode("//EntityApprovalStatusCode").InnerText = sEntityApprovalStatusId;
                    //p_objCompanyInfoXML.SelectSingleNode("//RejectReasonText").InnerText = sRejectReasonText;
                    //p_objCompanyInfoXML.SelectSingleNode("//RejectReasonText_HTMLComments").InnerText = sRejectReasonText_HTML;
                    //End: rsushilaggar

					objXMLOut = new XmlDocument();
                    
                    if (p_lSelectedLevelId != 0)
                    {
                        objEntity.MoveTo(int.Parse((p_lSelectedLevelId.ToString())));
                        
                        objEntity.ParentEid = objEntity.EntityId;
                        objEntity.EntityTableId = int.Parse(lNextTableID.ToString());

                        foreach (ClientLimits objClientLimits in objEntity.ClientLimitsList)
                        {
                            objClientLimits.ClientEid = -1;
                            objClientLimits.ClientLimitsRowId = -1;
                        }

                        foreach (EntityXContactInfo objContactInfo in objEntity.EntityXContactInfoList)
                        {
                            objContactInfo.EntityId = -1;
                            objContactInfo.ContactId = -1;
                        }
                        
                        foreach (EntityXAddresses objAddressInfo in objEntity.EntityXAddressesList)
                        {
                            objAddressInfo.EntityId = -1;
                            objAddressInfo.AddressId = -1;
                        }
                        foreach (EntityXOperatingAs objOperatingInfo in objEntity.EntityXOperatingAsList)
                        {
                            objOperatingInfo.EntityId = -1;
                            objOperatingInfo.OperatingId = -1;
                        }

                        XmlDocument oDoc = new XmlDocument();
                        
                        string sSerialized = objEntity.Supplementals.SerializeObject();
                        oDoc.LoadXml(sSerialized);
                        
                        objEntity.EntityId = -1;
                        
                        objEntity.Save();

                        int iEntityId = objEntity.EntityId;

                        objEntity.MoveTo(iEntityId);

                        oDoc.SelectSingleNode("//ENTITY_ID").InnerText = iEntityId.ToString();
                                              
                        objEntity.Supplementals.PopulateObject(oDoc);

                        objEntity.Supplementals.DataChanged = true;
                        objEntity.Supplementals.Save();

                        p_lSelectedLevelId = objEntity.EntityId;
                        sSQL = String.Format("Update Entity set Abbreviation = '{0}' where Entity_Id = '{0}'" , objEntity.EntityId.ToString());
                        objEntity.Context.DbConn.ExecuteNonQuery(sSQL);
                        
                    }

					//p_lSelectedLevelId = Conversion.ConvertStrToLong(UpdateCompanyInformation(0, p_objCompanyInfoXML.OuterXml, p_sUserName, p_sPassword, p_sDsnName, p_sConnectionString));

					//modifying abbreviation
					//p_objCompanyInfoXML.SelectSingleNode("//Abbreviation").InnerText = p_lSelectedLevelId.ToString();
					//p_lSelectedLevelId = Conversion.ConvertStrToLong(UpdateCompanyInformation(Convert.ToInt32(p_lSelectedLevelId), p_objCompanyInfoXML.OuterXml, p_sUserName, p_sPassword, p_sDsnName, p_sConnectionString));
				}


			}

			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateClone.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
                objEntity.Dispose();
                objDMF.Dispose();
                
			}
			return (objXMLOut);
		}


		#endregion

		#region Remove sibling
		/// Name		: RemoveSibling
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Removes sibling from the XML DOM
		/// </summary>
		/// <param name="p_objXML">XML DOM containing Org Hierarchy information</param>
		/// <param name="p_sParent">Parent node</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <param name="p_lNode">Node Id</param>
		/// <returns>Valid XML DOM without any redundant node</returns>
		private XmlDocument RemoveSibling(XmlDocument p_objXML, string p_sParent, string p_sNodCat, long p_lNode)
		{
			XmlNodeList objXmlNodeList = null;

			try
			{
				switch (p_sNodCat)
				{
					case COMPANY:
						objXmlNodeList = p_objXML.SelectNodes("//client/company[@parent='" + p_sParent + "']");
						break;
					case OPERATION:
						objXmlNodeList = p_objXML.SelectNodes("//client/company/operation[@id='" + p_sParent + "']");
						break;
					case REGION:
						objXmlNodeList = p_objXML.SelectNodes("//client/company/operation/region[@id='" + p_sParent + "']");
						break;
					case DIVISION:
						objXmlNodeList = p_objXML.SelectNodes("//client/company/operation/region/division[@id='" + p_sParent + "']");
						break;
					case LOCATION:
						objXmlNodeList = p_objXML.SelectNodes("//client/company/operation/region/division/location[@id='" + p_sParent + "']");
						break;
					case FACILITY:
						objXmlNodeList = p_objXML.SelectNodes("//client/company/operation/region/division/location/facility[@id='" + p_sParent + "']");
						break;
				}

				//Remove the redundant node from the XML DOM
				if (objXmlNodeList != null)
				{
					foreach (XmlElement objElem in objXmlNodeList)
					{
						if (Conversion.ConvertStrToLong(objElem.GetAttribute("id")) != p_lNode)
						{
							objElem.ParentNode.RemoveChild(objElem);
						}
					}
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.RemoveError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objXmlNodeList = null;
			}
			return (p_objXML);
		}
		#endregion

		#region Remove Error
		/// Name		: RemoveError
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Removes the error elements from the XML DOM
		/// </summary>
		/// <param name="p_objXML">XML DOM to be validated</param>
		/// <returns>Valid XML DOM</returns>
		private XmlDocument RemoveError(XmlDocument p_objXML)
		{
			XmlElement objError = null;
			XmlElement objOrg = null;

			try
			{
				objOrg = (XmlElement)p_objXML.SelectSingleNode("//org[@name='Organization Hierarchy']");
				objError = (XmlElement)p_objXML.SelectSingleNode("//org/error[@value='1']");

				if (objError != null)
					objOrg.RemoveChild(objError);
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.RemoveError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objError = null;
				objOrg = null;
			}
			return (p_objXML);
		}
		#endregion

		#region Join abbreviation
		/// Name		: JointAbbr
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Concatenates the first & last name
		/// </summary>
		/// <param name="p_fName">First name</param>
		/// <param name="p_lName">Last name</param>
		/// <returns>Concatenated first and last name</returns>
		private string JointAbbr(string p_fName, string p_lName)
		{
			return (p_lName + "-" + p_fName);
		}
		#endregion

		//Raman 07/31/2009: MITS 17393: We really do not need any temperory table as there is only 1 query. All we were doing earlier 
		//was putting results of that query in a temperory table and fetching them from it!!
		//Commenting all temperory table code
		/*
		#region Create temporary table
		/// Name		: CreateTable
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates temp table as per the database
		/// </summary>
		/// <param name="p_sNode">Node name</param>
		/// <param name="p_sSql">SQL as per database</param>
		/// <param name="p_objConn">Connection object as temp table exists for a session only</param>
		/// <returns>Returns int value as per success/failure</returns>
		private int CreateTable(string p_sNode, string p_sSql, DbConnection p_objConn)
		{
			string sSQL = "";
			string sTemp = "";
			int iRetVal = 0;

			try
			{
				sTemp = TempTableName("TEMPORG");
				switch (p_sNode)
				{
					case CLIENT:
						sSQL = sSQL + "C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
					case COMPANY:
						sSQL = sSQL + "CO_NAME %VARCHAR%(50),COID %INT%,CO_AB %VARCHAR%(25),";
						sSQL = sSQL + "C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
					case OPERATION:
						sSQL = sSQL + "O_NAME %VARCHAR%(50),OID %INT%,O_AB %VARCHAR%(25)";
						sSQL = sSQL + ",CO_NAME %VARCHAR%(50),COID %INT%,CO_AB %VARCHAR%(25)";
						sSQL = sSQL + ",C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
					case REGION:
						sSQL = sSQL + "R_NAME %VARCHAR%(50),RID %INT%,R_AB %VARCHAR%(25)";
						sSQL = sSQL + ",O_NAME %VARCHAR%(50),OID %INT%,O_AB %VARCHAR%(25)";
						sSQL = sSQL + ",CO_NAME %VARCHAR%(50),COID %INT%,CO_AB %VARCHAR%(25)";
						sSQL = sSQL + ",C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
					case DIVISION:
						sSQL = sSQL + "D_NAME %VARCHAR%(50),DID %INT%,D_AB %VARCHAR%(25)";
						sSQL = sSQL + ",R_NAME %VARCHAR%(50),RID %INT%,R_AB %VARCHAR%(25)";
						sSQL = sSQL + ",O_NAME %VARCHAR%(50),OID %INT%,O_AB %VARCHAR%(25)";
						sSQL = sSQL + ",CO_NAME %VARCHAR%(50),COID %INT%,CO_AB %VARCHAR%(25)";
						sSQL = sSQL + ",C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
					case LOCATION:
						sSQL = sSQL + "L_NAME %VARCHAR%(50),LID %INT%,L_AB %VARCHAR%(25)";
						sSQL = sSQL + ",D_NAME %VARCHAR%(50),DID %INT%,D_AB %VARCHAR%(25)";
						sSQL = sSQL + ",R_NAME %VARCHAR%(50),RID %INT%,R_AB %VARCHAR%(25)";
						sSQL = sSQL + ",O_NAME %VARCHAR%(50),OID %INT%,O_AB %VARCHAR%(25)";
						sSQL = sSQL + ",CO_NAME %VARCHAR%(50),COID %INT%,CO_AB %VARCHAR%(25)";
						sSQL = sSQL + ",C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
					case FACILITY:
						sSQL = sSQL + "F_NAME %VARCHAR%(50),FID %INT%,F_AB %VARCHAR%(25)";
						sSQL = sSQL + ",L_NAME %VARCHAR%(50),LID %INT%,L_AB %VARCHAR%(25)";
						sSQL = sSQL + ",D_NAME %VARCHAR%(50),DID %INT%,D_AB %VARCHAR%(25)";
						sSQL = sSQL + ",R_NAME %VARCHAR%(50),RID %INT%,R_AB %VARCHAR%(25)";
						sSQL = sSQL + ",O_NAME %VARCHAR%(50),OID %INT%,O_AB %VARCHAR%(25)";
						sSQL = sSQL + ",CO_NAME %VARCHAR%(50),COID %INT%,CO_AB %VARCHAR%(25)";
						sSQL = sSQL + ",C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
					case DEPARTMENT:
						sSQL = sSQL + "DT_NAME %VARCHAR%(50),DTID %INT%,DT_AB %VARCHAR%(25)";
						sSQL = sSQL + ",F_NAME %VARCHAR%(50),FID %INT%,F_AB %VARCHAR%(25)";
						sSQL = sSQL + ",L_NAME %VARCHAR%(50),LID %INT%,L_AB %VARCHAR%(25)";
						sSQL = sSQL + ",D_NAME %VARCHAR%(50),DID %INT%,D_AB %VARCHAR%(25)";
						sSQL = sSQL + ",R_NAME %VARCHAR%(50),RID %INT%,R_AB %VARCHAR%(25)";
						sSQL = sSQL + ",O_NAME %VARCHAR%(50),OID %INT%,O_AB %VARCHAR%(25)";
						sSQL = sSQL + ",CO_NAME %VARCHAR%(50),COID %INT%,CO_AB %VARCHAR%(25)";
						sSQL = sSQL + ",C_NAME %VARCHAR%(50),CID %INT%,C_AB %VARCHAR%(25)";
						break;
				}

				switch (m_sDBType)
				{
					case Constants.DB_ACCESS:
						iRetVal = 0;
						break;
					case Constants.DB_SQLSRVR:
						sSQL = "CREATE TABLE #" + sTemp + "(" + sSQL.Replace("%INT%", "INT").Replace("%VARCHAR%", "VARCHAR") + ")";
						p_objConn.ExecuteNonQuery(sSQL);
						sSQL = "INSERT INTO #" + sTemp + " " + p_sSql;
						p_objConn.ExecuteNonQuery(sSQL);
						m_sSql = "SELECT * FROM #" + sTemp;
						iRetVal = 1;
						break;
					case Constants.DB_SYBASE:
						sSQL = "CREATE TABLE #" + sTemp + "(" + sSQL.Replace("%INT%", "INT").Replace("%VARCHAR%", "VARCHAR") + ")";
						p_objConn.ExecuteNonQuery(sSQL);
						sSQL = "INSERT INTO #" + sTemp + " " + p_sSql;
						p_objConn.ExecuteNonQuery(sSQL);
						m_sSql = "SELECT * FROM #" + sTemp + "";
						iRetVal = 1;
						break;
					case Constants.DB_INFORMIX:
						sSQL = "CREATE TEMP TABLE " + sTemp + "(" + sSQL.Replace("%INT%", "INTEGER").Replace("%VARCHAR%", "VARCHAR") + ")";
						p_objConn.ExecuteNonQuery(sSQL);
						sSQL = "INSERT INTO " + sTemp + " " + p_sSql;
						p_objConn.ExecuteNonQuery(sSQL);
						m_sSql = "SELECT * FROM " + sTemp + "";
						iRetVal = 1;
						break;
					case Constants.DB_ORACLE:
						sSQL = "CREATE GLOBAL TEMPORARY TABLE " + sTemp + "(" + sSQL.Replace("%INT%", "NUMBER(10)").Replace("%VARCHAR%", "VARCHAR2") + ")";
						p_objConn.ExecuteNonQuery(sSQL);
						sSQL = "INSERT INTO " + sTemp + " " + p_sSql;
						p_objConn.ExecuteNonQuery(sSQL);
						m_sSql = "SELECT * FROM " + sTemp + "";
						iRetVal = 1;
						break;
					case Constants.DB_ODBC:
						iRetVal = 0;
						break;
					case Constants.DB_DB2:
						sSQL = "DECLARE GLOBAL TEMPORARY TABLE " + sTemp + "(" + sSQL.Replace("%INT%", "INTEGER").Replace("%VARCHAR%", "VARCHAR") + ")";
						p_objConn.ExecuteNonQuery(sSQL);
						sSQL = "INSERT INTO SESSION." + sTemp + " " + p_sSql;
						p_objConn.ExecuteNonQuery(sSQL);
						m_sSql = "SELECT * FROM SESSION." + sTemp + "";
						iRetVal = 1;
						break;
				}
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}

			catch (Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateTable.GeneralError"), p_objExp);
			}
			return (iRetVal);
		}
		#endregion
		*/
		//Raman 07/31/2009: MITS 17393: We really do not need any temperory table as there is only 1 query. All we were doing earlier 
		//was putting results of that query in a temperory table and fetching them from it!!
		//Commenting all temperory table code
		/*
		#region Drops temporary table
		/// Name		: DropTempTable
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Drops the temp table
		/// </summary>
		/// <param name="p_sTname">Table name</param>
		/// <param name="p_objConn">Connection object as temp table exists for a session only</param>
		/// <returns>Returns int value as per success/failure</returns>
		private int DropTempTable(string p_sTname, DbConnection p_objConn)
		{
			string sSQL = "";
			int iRetVal = 0;
			string sTemp = "";
			int iRet = 0;
			try
			{
				sTemp = TempTableName(p_sTname);

				switch (m_sDBType)
				{
					case Constants.DB_ACCESS:
						iRetVal = 1;
						break;
					case Constants.DB_SQLSRVR:
						sSQL = "DROP TABLE #" + sTemp;
						break;
					case Constants.DB_SYBASE:
						sSQL = "DROP TABLE #" + sTemp;
						break;
					case Constants.DB_INFORMIX:
						sSQL = "DROP TEMP TABLE " + sTemp;
						break;
					case Constants.DB_ORACLE:
						sSQL = "DROP TABLE " + sTemp;
						break;
					case Constants.DB_ODBC:
						iRetVal = 1;
						break;
					case Constants.DB_DB2:
						sSQL = "DROP TABLE " + sTemp;
						break;
				}
				iRet = p_objConn.ExecuteNonQuery(sSQL);
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}

			catch (Exception p_objExp)
			{
				//Raman 07/31/2009: If temporary table does not exist for the given connection session then do nothing 
				//else in case of any other error throw the exception.
				if (p_objExp is System.Data.SqlClient.SqlException || iRet == -1) { }
				else if (m_sDBType == Constants.DB_ORACLE && p_objExp.Message.IndexOf("942") != -1) { }
				else
					throw new RMAppException(Globalization.GetString("OrgHierarchy.DropTempTable.GeneralError"), p_objExp);
			}
			finally { }
			return (iRetVal);
		}
		#endregion
		*/
		//Raman 07/31/2009: MITS 17393: We really do not need any temperory table as there is only 1 query. All we were doing earlier 
		//was putting results of that query in a temperory table and fetching them from it!!
		//Commenting all temperory table code
		/*
		#region Temporary table name
		/// Name		: TempTableName
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Generates the temporary table name
		/// </summary>
		/// <param name="p_sName">Table name</param>
		/// <returns>Temporary table name</returns>
		private string TempTableName(string p_sName)
		{
			return (p_sName + Thread.CurrentThread.ManagedThreadId.ToString());
		}
		#endregion
		*/
		#region Remove Selection
		/// Name		: RemoveSelection
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Removes redundant elements from the XML DOM
		/// </summary>
		/// <param name="p_objXML">XML DOM containing Org Hierarchy information</param>
		/// <returns>Valid XML DOM with proper "isCurrent" attribute</returns>
		private XmlDocument RemoveSelection(XmlDocument p_objXML)
		{
			XmlNodeList objXmlNodeList = null;
			try
			{
                //objXmlNodeList = p_objXML.SelectNodes("//client[@iscurrent='1']");
                //foreach (XmlElement objElem in objXmlNodeList)
                //    objElem.SetAttribute("iscurrent", "0");

                //objXmlNodeList = p_objXML.SelectNodes("//company[@iscurrent='1']");
                //foreach (XmlElement objElem in objXmlNodeList)
                //    objElem.SetAttribute("iscurrent", "0");

                //objXmlNodeList = p_objXML.SelectNodes("//operation[@iscurrent='1']");
                //foreach (XmlElement objElem in objXmlNodeList)
                //    objElem.SetAttribute("iscurrent", "0");

                //objXmlNodeList = p_objXML.SelectNodes("//region[@iscurrent='1']");
                //foreach (XmlElement objElem in objXmlNodeList)
                //    objElem.SetAttribute("iscurrent", "0");

                //objXmlNodeList = p_objXML.SelectNodes("//division[@iscurrent='1']");
                //foreach (XmlElement objElem in objXmlNodeList)
                //    objElem.SetAttribute("iscurrent", "0");

                //objXmlNodeList = p_objXML.SelectNodes("//location[@iscurrent='1']");
                //foreach (XmlElement objElem in objXmlNodeList)
                //    objElem.SetAttribute("iscurrent", "0");

                //objXmlNodeList = p_objXML.SelectNodes("//facility[@iscurrent='1']");
                //foreach (XmlElement objElem in objXmlNodeList)
                //    objElem.SetAttribute("iscurrent", "0");

                
                //Deb : performance improvements
                objXmlNodeList = p_objXML.SelectNodes("//@iscurrent[.='1']");
                foreach (XmlAttribute objElem in objXmlNodeList)
                {
                   objElem.Value = "0";
                }
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.RemoveError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
                objXmlNodeList = null;
			}
			return (p_objXML);
		}
		#endregion

		#region Append Trigger SQL
		/// Name		: AppendDateTriggerSql
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Appends Date Trigger SQL
		/// </summary>
		/// <param name="p_sSql">SQL to be modified</param>
		/// <param name="p_sNodcat">Node category</param>
		/// <returns>Updated SQL string</returns>
		public string AppendDateTriggerSql(string p_sSql, string p_sNodcat)
		{
			//Mukul 4/19/07 MITS 8733 NO need to get field names
			//string sFieldName = "";
			string sSql = "";
			try
			{
				//sFieldName = GetFieldName(m_sTable);
				switch (p_sNodcat)
				{
					case CLIENT:
						sSql = sSql + GetTriggerSQL("H", m_sTable);
						break;
					case COMPANY:
						sSql = GetTriggerSQL("G", m_sTable)
						    + GetTriggerSQL("H", m_sTable);
						break;
					case OPERATION:
						sSql = GetTriggerSQL("F", m_sTable)
						    + GetTriggerSQL("G", m_sTable)
						    + GetTriggerSQL("H", m_sTable);
						break;
					case REGION:
						sSql = GetTriggerSQL("E", m_sTable)
						    + GetTriggerSQL("F", m_sTable)
						    + GetTriggerSQL("G", m_sTable)
						    + GetTriggerSQL("H", m_sTable);
						break;
					case DIVISION:
						sSql = GetTriggerSQL("D", m_sTable)
						    + GetTriggerSQL("E", m_sTable)
						    + GetTriggerSQL("F", m_sTable)
						    + GetTriggerSQL("G", m_sTable)
						    + GetTriggerSQL("H", m_sTable);
						break;
					case LOCATION:
						sSql = GetTriggerSQL("C", m_sTable)
						 + GetTriggerSQL("D", m_sTable)
						 + GetTriggerSQL("E", m_sTable)
						 + GetTriggerSQL("F", m_sTable)
						 + GetTriggerSQL("G", m_sTable)
						 + GetTriggerSQL("H", m_sTable);
						break;
					case FACILITY:
						sSql = GetTriggerSQL("B", m_sTable)
						 + GetTriggerSQL("C", m_sTable)
						 + GetTriggerSQL("D", m_sTable)
						 + GetTriggerSQL("E", m_sTable)
						 + GetTriggerSQL("F", m_sTable)
						 + GetTriggerSQL("G", m_sTable)
						 + GetTriggerSQL("H", m_sTable);
						break;
					case DEPARTMENT:
					case ALL:
						sSql = GetTriggerSQL("B1", m_sTable)
						    + GetTriggerSQL("B", m_sTable)
						    + GetTriggerSQL("C", m_sTable)
						    + GetTriggerSQL("D", m_sTable)
						    + GetTriggerSQL("E", m_sTable)
						    + GetTriggerSQL("F", m_sTable)
						    + GetTriggerSQL("G", m_sTable)
						    + GetTriggerSQL("H", m_sTable);
						break;
				}
				p_sSql = p_sSql + sSql;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.AppendDateTriggerSql.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			return p_sSql;
		}
		#endregion

		#region Get Trigger SQL
		/// Name		: GetTriggerSQL
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Builds SQL queries with Trigger date field
		/// </summary>
		/// <param name="p_sCode">Code</param>
		/// <param name="p_sFieldName">Field name</param>
		/// <returns>Modified SQL string</returns>
		//private string GetTriggerSQL(string p_sCode,string p_sFieldName)
		private string GetTriggerSQL(string p_sCode, string p_sTname)
		{
			string sSQL = "";
			try
			{
				//Mukul 4/19/07 MITS 8733 This function is generalized now.			
				if (p_sCode != "")
					p_sCode = p_sCode + ".";

				switch (p_sTname.ToUpper())
				{
					case "":
					case "NOTHING":
						//sgoel6 03/31/2009
						sSQL = " AND ((" + p_sCode + "TRIGGER_DATE_FIELD IS NULL OR " + p_sCode + "TRIGGER_DATE_FIELD = 'NULL') "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "') "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "') "
						    + " OR " + p_sCode + "TRIGGER_DATE_FIELD NOT IN ('SYSTEM_DATE'))";
						break;
					case CLAIM: case "CLAIMGC":  case "CLAIMWC":  case "CLAIMVA":  case "CLAIMDI":
						//sgoel6 03/31/2009
                        sSQL = " AND ((" + p_sCode + "TRIGGER_DATE_FIELD IS NULL OR " + p_sCode + "TRIGGER_DATE_FIELD = 'NULL') "
                            //vkumar258 RMA-15360 Start:
                            + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL)"
                            + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE<='" + m_sEventDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL)"
                            + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sEventDate + "')"
                            + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE<='" + m_sEventDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sEventDate + "')"
                            //vkumar258 RMA-15360 End.
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND " + p_sCode + "EFF_START_DATE<='" + m_sClaimDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sClaimDate + "') "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND " + p_sCode + "EFF_START_DATE<='" + m_sClaimDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sClaimDate + "') "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "') "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "'))";
						break;
					case EVENT:
						//sgoel6 03/31/2009
						sSQL = " AND ((" + p_sCode + "TRIGGER_DATE_FIELD IS NULL OR " + p_sCode + "TRIGGER_DATE_FIELD = 'NULL') "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL)"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE<='" + m_sEventDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL)"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sEventDate + "')"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + "EFF_START_DATE<='" + m_sEventDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sEventDate + "')"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL)"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL)"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "')"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "'))";
						break;
					case POLICYENH://Case added by Shivendu for MITS 17249
					case POLICY:
						//sgoel6 03/31/2009

						//Modified the query. Putting LIKE instead of = as while inserting 
						//POLICY.EFFECTIVE_DATE,SYSTEM_DATE is added in the database.Modified by Shivendu for MITS 17249

						//sSQL = " AND ((" + p_sCode + "TRIGGER_DATE_FIELD IS NULL OR " + p_sCode + "TRIGGER_DATE_FIELD = 'NULL') ";
						//sSQL += " OR (" + p_sCode + "TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL) ";
						//sSQL += " OR (" + p_sCode + "TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sPolicyDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL) ";
						//sSQL += " OR (" + p_sCode + "TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sPolicyDate + "')";
						//sSQL += " OR (" + p_sCode + "TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sPolicyDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sPolicyDate + "')";
						sSQL = " AND ((" + p_sCode + "TRIGGER_DATE_FIELD IS NULL OR " + p_sCode + "TRIGGER_DATE_FIELD = 'NULL') "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD LIKE '%POLICY.EFFECTIVE_DATE%' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD LIKE '%POLICY.EFFECTIVE_DATE%' AND " + p_sCode + "EFF_START_DATE<='" + m_sPolicyDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL) "
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD LIKE '%POLICY.EFFECTIVE_DATE%' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sPolicyDate + "')"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD LIKE '%POLICY.EFFECTIVE_DATE%' AND " + p_sCode + "EFF_START_DATE<='" + m_sPolicyDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sPolicyDate + "')"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE IS NULL)"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE IS NULL)"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE IS NULL AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "')"
						    + " OR (" + p_sCode + "TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + "EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + "EFF_END_DATE>='" + m_sSystemDate + "'))";
						break;
				}
				/*sSQL = " AND ((" + p_sCode + ".TRIGGER_DATE_FIELD='SYSTEM_DATE' AND " + p_sCode + ".EFF_START_DATE<='" + m_sSystemDate + "' AND " + p_sCode + ".EFF_END_DATE>='" + m_sSystemDate + "')";
			
				if(m_sClaimDate != "")
					sSQL = sSQL + " OR (" + p_sCode + ".TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND " + p_sCode + ".EFF_START_DATE<='" + m_sClaimDate  + "' AND " + p_sCode + ".EFF_END_DATE>='" + m_sClaimDate + "')";

				if(m_sEventDate != "")
					sSQL = sSQL + " OR (" + p_sCode + ".TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND " + p_sCode + ".EFF_START_DATE<='" + m_sEventDate + "' AND " + p_sCode + ".EFF_END_DATE>='" + m_sEventDate + "')";
			
				if(m_sPolicyDate != "")
					sSQL = sSQL + " OR (" + p_sCode + ".TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND " + p_sCode + ".EFF_START_DATE<='" + m_sPolicyDate + "' AND " + p_sCode + ".EFF_END_DATE>='" + m_sPolicyDate + "')";

				sSQL = sSQL + " OR " + p_sCode + ".TRIGGER_DATE_FIELD NOT IN " + p_sFieldName + " OR " + p_sCode + ".TRIGGER_DATE_FIELD IS NULL";
				sSQL = sSQL + ")";*/
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.SqlError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			return sSQL;
		}
		#endregion

		#region XML by Name
		/// Name		: GetXmlbyName
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded function which calls the main function
		/// </summary>
		/// <param name="p_objXml">XML DOM containing Org Hierarchy data</param>
		/// <param name="objDataSet">Data set from which Org Hierarchy information to be extracted</param>
		/// <param name="p_sNodcat">Node caregory</param>
		/// <returns>XML string containing Org Hierarchy data</returns>
		private string GetXmlbyName(XmlDocument p_objXml, DataSet objDataSet, string p_sNodcat)
		{
			return GetXmlbyName(p_objXml, objDataSet, p_sNodcat, "");
		}
		#endregion

		#region Overloaded XML by Name
		/// Name		: GetXmlbyName
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Sets the XML DOM element with the Org Hierarchy data
		/// </summary>
		/// <param name="p_objXml">XML DOM containing Org Hierarchy data</param>
		/// <param name="p_objDataSet">Data set from which Org Hierarchy information to be extracted</param>
		/// <param name="p_sNodcat">Node caregory</param>
		/// <param name="p_sTname">Table name</param>
		/// <returns>XML string containing Org Hierarchy data</returns>
		private string GetXmlbyName(XmlDocument p_objXml, DataSet p_objDataSet, string p_sNodcat, string p_sTname)
		{
			int iNodeCount = 0;
			int iResult = 0;
			int iEnh = 0;
			long lClient = 0;
			long lDepartment = 0;
			long lFacility = 0;
			long lLocation = 0;
			long lDivision = 0;
			long lRegion = 0;
			long lOperation = 0;
			long lCompany = 0;
			long lClientId = 0;
			long lCompanyId = 0;
			long lOperationId = 0;
			long lRegionId = 0;
			long lDivisionId = 0;
			long lLocationId = 0;
			long lFacilityId = 0;
			long lDepartmentId = 0;
			string sErrorCheckXML = "";
			string sLevel = "";
			string sClientName = "";
			string sCompanyName = "";
			string sOperationName = "";
			string sRegionName = "";
			string sDivisionName = "";
			string sLocationName = "";
			string sFacilityName = "";
			string sDepartmentName = "";
			string sRetXml = "";
			XmlDocument objXML = null;
			XmlElement objXMLParentEle = null;
			XmlElement objXMLChildEle = null;
			XmlElement objXMLNodeEle = null;
			XmlElement objXMLErrorEle = null;
			int iMax_Number_Item = 0;
			RMConfigurator objConfig = null;

			try
			{
				objXML = p_objXml;
				sErrorCheckXML = objXML.InnerXml;

				objConfig = new RMConfigurator();
				iMax_Number_Item = Conversion.ConvertStrToInteger(m_OrgHierarchySettings["Maximum_Number_Item"].ToString());

				//Select  Organization Hierarchy node
				objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//org[@name='Organization Hierarchy']");
				m_bIsError = false;

				//if data is present in the dataset
				if (p_objDataSet.Tables[0].Rows.Count > 0)
				{
					//loop through all the rows present in dataset
					for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
					{
						#region "Switch"
						switch (p_sNodcat)
						{
							//Set client node attributes
							case CLIENT:
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());
								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;
									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "1");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");

									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}
								break;

							//Set company node attributes
							case COMPANY:

								//Client
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());

								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;

									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "0");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");

									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}

								//Company
								lCompanyId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["COID"].ToString());

								if (lCompanyId != lCompany || lCompanyId == 0)
								{
									lCompany = lCompanyId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client[@id='" + lClient + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("company"), true);

									sCompanyName = p_objDataSet.Tables[0].Rows[iCnt]["CO_NAME"].ToString();

									if (sCompanyName != null && sCompanyName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sCompanyName, p_objDataSet.Tables[0].Rows[iCnt]["CO_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lClient.ToString());
									objXMLNodeEle.SetAttribute("id", lCompany.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lCompany)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "operation");
									objXMLNodeEle.SetAttribute("ischild", "1");
									objXMLNodeEle.SetAttribute("code", "CO");
									objXMLNodeEle.SetAttribute("isSelect", "1");
									objXMLNodeEle.SetAttribute("level", "2");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}
								break;

							//Set operation node attributes
							case OPERATION:
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());

								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;
									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "0");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");

									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}

								lCompanyId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["COID"].ToString());

								if (lCompanyId != lCompany || lCompanyId == 0)
								{
									lCompany = lCompanyId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client[@id='" + lClient + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("company"), true);

									sCompanyName = p_objDataSet.Tables[0].Rows[iCnt]["CO_NAME"].ToString();

									if (sCompanyName != null && sCompanyName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sCompanyName, p_objDataSet.Tables[0].Rows[iCnt]["CO_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lClient.ToString());
									objXMLNodeEle.SetAttribute("id", lCompany.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lCompany)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "operation");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "CO");
									objXMLNodeEle.SetAttribute("level", "2");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lOperationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["OID"].ToString());

								if (lOperationId != lOperation || lOperationId == 0)
								{
									lOperation = lOperationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company[@id='" + lCompany + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("operation"), true);

									sOperationName = p_objDataSet.Tables[0].Rows[iCnt]["O_NAME"].ToString();

									if (sOperationName != null && sOperationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sOperationName, p_objDataSet.Tables[0].Rows[iCnt]["O_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lCompany.ToString());
									objXMLNodeEle.SetAttribute("id", lOperation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lOperation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "region");
									objXMLNodeEle.SetAttribute("ischild", "1");
									objXMLNodeEle.SetAttribute("code", "O");
									objXMLNodeEle.SetAttribute("isSelect", "1");
									objXMLNodeEle.SetAttribute("level", "3");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}
								break;

							//Set region node attributes
							case REGION:
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());

								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;
									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "0");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");

									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}

								lCompanyId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["COID"].ToString());

								if (lCompanyId != lCompany || lCompanyId == 0)
								{
									lCompany = lCompanyId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client[@id='" + lClient + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("company"), true);

									sCompanyName = p_objDataSet.Tables[0].Rows[iCnt]["CO_NAME"].ToString();

									if (sCompanyName != null && sCompanyName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sCompanyName, p_objDataSet.Tables[0].Rows[iCnt]["CO_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lClient.ToString());
									objXMLNodeEle.SetAttribute("id", lCompany.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lCompany)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "operation");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "CO");
									objXMLNodeEle.SetAttribute("level", "2");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lOperationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["OID"].ToString());

								if (lOperationId != lOperation || lOperationId == 0)
								{
									lOperation = lOperationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company[@id='" + lCompany + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("operation"), true);

									sOperationName = p_objDataSet.Tables[0].Rows[iCnt]["O_NAME"].ToString();

									if (sOperationName != null && sOperationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sOperationName, p_objDataSet.Tables[0].Rows[iCnt]["O_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lCompany.ToString());
									objXMLNodeEle.SetAttribute("id", lOperation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lOperation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "region");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "O");
									objXMLNodeEle.SetAttribute("level", "3");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lRegionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["RID"].ToString());

								if (lRegionId != lRegion || lRegionId == 0)
								{
									lRegion = lRegionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation[@id='" + lOperation + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("region"), true);

									sRegionName = p_objDataSet.Tables[0].Rows[iCnt]["R_NAME"].ToString();

									if (sRegionName != null && sRegionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sRegionName, p_objDataSet.Tables[0].Rows[iCnt]["R_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lOperation.ToString());
									objXMLNodeEle.SetAttribute("id", lRegion.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lRegion)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "division");
									objXMLNodeEle.SetAttribute("ischild", "1");
									objXMLNodeEle.SetAttribute("code", "R");
									objXMLNodeEle.SetAttribute("isSelect", "1");
									objXMLNodeEle.SetAttribute("level", "4");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}
								break;

							//Set division node attributes
							case DIVISION:
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());
								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;
									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "0");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");

									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}

								lCompanyId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["COID"].ToString());

								if (lCompanyId != lCompany || lCompanyId == 0)
								{
									lCompany = lCompanyId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client[@id='" + lClient + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("company"), true);

									sCompanyName = p_objDataSet.Tables[0].Rows[iCnt]["CO_NAME"].ToString();
									if (sCompanyName != null && sCompanyName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sCompanyName, p_objDataSet.Tables[0].Rows[iCnt]["CO_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lClient.ToString());
									objXMLNodeEle.SetAttribute("id", lCompany.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lCompany)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "operation");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "CO");
									objXMLNodeEle.SetAttribute("level", "2");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lOperationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["OID"].ToString());

								if (lOperationId != lOperation || lOperationId == 0)
								{
									lOperation = lOperationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company[@id='" + lCompany + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("operation"), true);

									sOperationName = p_objDataSet.Tables[0].Rows[iCnt]["O_NAME"].ToString();

									if (sOperationName != null && sOperationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sOperationName, p_objDataSet.Tables[0].Rows[iCnt]["O_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lCompany.ToString());
									objXMLNodeEle.SetAttribute("id", lOperation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lOperation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "region");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "O");
									objXMLNodeEle.SetAttribute("level", "3");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lRegionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["RID"].ToString());

								if (lRegionId != lRegion || lRegionId == 0)
								{
									lRegion = lRegionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation[@id='" + lOperation + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("region"), true);

									sRegionName = p_objDataSet.Tables[0].Rows[iCnt]["R_NAME"].ToString();

									if (sRegionName != null && sRegionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sRegionName, p_objDataSet.Tables[0].Rows[iCnt]["R_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lOperation.ToString());
									objXMLNodeEle.SetAttribute("id", lRegion.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lRegion)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "division");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "R");
									objXMLNodeEle.SetAttribute("level", "4");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lDivisionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["DID"].ToString());

								if (lDivisionId != lDivision || lDivisionId == 0)
								{
									lDivision = lDivisionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region[@id='" + lRegion + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("division"), true);

									sDivisionName = p_objDataSet.Tables[0].Rows[iCnt]["D_NAME"].ToString();

									if (sDivisionName != null && sDivisionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sDivisionName, p_objDataSet.Tables[0].Rows[iCnt]["D_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lRegion.ToString());
									objXMLNodeEle.SetAttribute("id", lDivision.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lDivision)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "location");
									objXMLNodeEle.SetAttribute("ischild", "1");
									objXMLNodeEle.SetAttribute("code", "D");
									objXMLNodeEle.SetAttribute("isSelect", "1");
									objXMLNodeEle.SetAttribute("level", "5");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}
								break;

							//Set location node attributes
							case LOCATION:
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());
								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;
									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "0");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");

									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}

								lCompanyId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["COID"].ToString());

								if (lCompanyId != lCompany || lCompanyId == 0)
								{
									lCompany = lCompanyId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client[@id='" + lClient + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("company"), true);

									sCompanyName = p_objDataSet.Tables[0].Rows[iCnt]["CO_NAME"].ToString();

									if (sCompanyName != null && sCompanyName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sCompanyName, p_objDataSet.Tables[0].Rows[iCnt]["CO_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lClient.ToString());
									objXMLNodeEle.SetAttribute("id", lCompany.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lCompany)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "operation");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "CO");
									objXMLNodeEle.SetAttribute("level", "2");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lOperationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["OID"].ToString());

								if (lOperationId != lOperation || lOperationId == 0)
								{
									lOperation = lOperationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company[@id='" + lCompany + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("operation"), true);

									sOperationName = p_objDataSet.Tables[0].Rows[iCnt]["O_NAME"].ToString();

									if (sOperationName != null && sOperationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sOperationName, p_objDataSet.Tables[0].Rows[iCnt]["O_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lCompany.ToString());
									objXMLNodeEle.SetAttribute("id", lOperation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lOperation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "region");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "O");
									objXMLNodeEle.SetAttribute("level", "3");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lRegionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["RID"].ToString());

								if (lRegionId != lRegion || lRegionId == 0)
								{
									lRegion = lRegionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation[@id='" + lOperation + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("region"), true);

									sRegionName = p_objDataSet.Tables[0].Rows[iCnt]["R_NAME"].ToString();

									if (sRegionName != null && sRegionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sRegionName, p_objDataSet.Tables[0].Rows[iCnt]["R_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lOperation.ToString());
									objXMLNodeEle.SetAttribute("id", lRegion.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lRegion)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "division");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "R");
									objXMLNodeEle.SetAttribute("level", "4");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lDivisionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["DID"].ToString());

								if (lDivisionId != lDivision || lDivisionId == 0)
								{
									lDivision = lDivisionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region[@id='" + lRegion + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("division"), true);

									sDivisionName = p_objDataSet.Tables[0].Rows[iCnt]["D_NAME"].ToString();

									if (sDivisionName != null && sDivisionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sDivisionName, p_objDataSet.Tables[0].Rows[iCnt]["D_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lRegion.ToString());
									objXMLNodeEle.SetAttribute("id", lDivision.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lDivision)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "location");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "D");
									objXMLNodeEle.SetAttribute("level", "5");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lLocationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["LID"].ToString());

								if (lLocationId != lLocation || lLocationId == 0)
								{
									lLocation = lLocationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region/division[@id='" + lDivision + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("location"), true);

									sLocationName = p_objDataSet.Tables[0].Rows[iCnt]["L_NAME"].ToString();

									if (sLocationName != null && sLocationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sLocationName, p_objDataSet.Tables[0].Rows[iCnt]["L_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lDivision.ToString());
									objXMLNodeEle.SetAttribute("id", lLocation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lLocation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "facility");
									objXMLNodeEle.SetAttribute("ischild", "1");
									objXMLNodeEle.SetAttribute("code", "L");
									objXMLNodeEle.SetAttribute("isSelect", "1");
									objXMLNodeEle.SetAttribute("level", "6");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}
								break;

							//Set facility node attributes
							case FACILITY:
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());
								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;
									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "0");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");

									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}

								lCompanyId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["COID"].ToString());

								if (lCompanyId != lCompany || lCompanyId == 0)
								{
									lCompany = lCompanyId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client[@id='" + lClient + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("company"), true);

									sCompanyName = p_objDataSet.Tables[0].Rows[iCnt]["CO_NAME"].ToString();

									if (sCompanyName != null && sCompanyName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sCompanyName, p_objDataSet.Tables[0].Rows[iCnt]["CO_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lClient.ToString());
									objXMLNodeEle.SetAttribute("id", lCompany.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lCompany)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "operation");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "CO");
									objXMLNodeEle.SetAttribute("level", "2");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lOperationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["OID"].ToString());

								if (lOperationId != lOperation || lOperationId == 0)
								{
									lOperation = lOperationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company[@id='" + lCompany + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("operation"), true);

									sOperationName = p_objDataSet.Tables[0].Rows[iCnt]["O_NAME"].ToString();

									if (sOperationName != null && sOperationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sOperationName, p_objDataSet.Tables[0].Rows[iCnt]["O_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lCompany.ToString());
									objXMLNodeEle.SetAttribute("id", lOperation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lOperation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "region");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "O");
									objXMLNodeEle.SetAttribute("level", "3");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lRegionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["RID"].ToString());

								if (lRegionId != lRegion || lRegionId == 0)
								{
									lRegion = lRegionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation[@id='" + lOperation + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("region"), true);

									sRegionName = p_objDataSet.Tables[0].Rows[iCnt]["R_NAME"].ToString();

									if (sRegionName != null && sRegionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sRegionName, p_objDataSet.Tables[0].Rows[iCnt]["R_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lOperation.ToString());
									objXMLNodeEle.SetAttribute("id", lRegion.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lRegion)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "division");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "R");
									objXMLNodeEle.SetAttribute("level", "4");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lDivisionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["DID"].ToString());

								if (lDivisionId != lDivision || lDivisionId == 0)
								{
									lDivision = lDivisionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region[@id='" + lRegion + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("division"), true);

									sDivisionName = p_objDataSet.Tables[0].Rows[iCnt]["D_NAME"].ToString();

									if (sDivisionName != null && sDivisionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sDivisionName, p_objDataSet.Tables[0].Rows[iCnt]["D_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lRegion.ToString());
									objXMLNodeEle.SetAttribute("id", lDivision.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lDivision)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "location");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "D");
									objXMLNodeEle.SetAttribute("level", "5");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lLocationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["LID"].ToString());

								if (lLocationId != lLocation || lLocationId == 0)
								{
									lLocation = lLocationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region/division[@id='" + lDivision + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("location"), true);

									sLocationName = p_objDataSet.Tables[0].Rows[iCnt]["L_NAME"].ToString();

									if (sLocationName != null && sLocationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sLocationName, p_objDataSet.Tables[0].Rows[iCnt]["L_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lDivision.ToString());
									objXMLNodeEle.SetAttribute("id", lLocation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lLocation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "facility");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "L");
									objXMLNodeEle.SetAttribute("level", "6");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lFacilityId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["FID"].ToString());

								if (lFacilityId != lFacility || lFacilityId == 0)
								{
									lFacility = lFacilityId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region/division/location[@id='" + lLocation + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("facility"), true);

									sFacilityName = p_objDataSet.Tables[0].Rows[iCnt]["F_NAME"].ToString();

									if (sFacilityName != null && sFacilityName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sFacilityName, p_objDataSet.Tables[0].Rows[iCnt]["F_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lLocation.ToString());
									objXMLNodeEle.SetAttribute("id", lFacility.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lFacility)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "department");
									objXMLNodeEle.SetAttribute("ischild", "1");
									objXMLNodeEle.SetAttribute("code", "F");
									objXMLNodeEle.SetAttribute("isSelect", "1");
									objXMLNodeEle.SetAttribute("level", "7");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}
								break;

							//Set department node attributes
							case DEPARTMENT:
							case ALL:
								lClientId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["CID"].ToString());
								if (lClientId != lClient || lClientId == 0)
								{
									lClient = lClientId;
									objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("client"), true);

									sClientName = p_objDataSet.Tables[0].Rows[iCnt]["C_NAME"].ToString();

									if (sClientName != null && sClientName != "")
										objXMLChildEle.SetAttribute("name", JointAbbr(sClientName, p_objDataSet.Tables[0].Rows[iCnt]["C_AB"].ToString()));
									else
										objXMLChildEle.SetAttribute("name", NO_ENTITY);

									objXMLChildEle.SetAttribute("parent", "0");
									objXMLChildEle.SetAttribute("ischild", "0");
									objXMLChildEle.SetAttribute("id", lClient.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lClient)
										objXMLChildEle.SetAttribute("iscurrent", "1");
									objXMLChildEle.SetAttribute("child_name", "company");
									objXMLChildEle.SetAttribute("code", "C");
									objXMLChildEle.SetAttribute("level", "1");
									objXMLParentEle.AppendChild(objXMLChildEle);
									iNodeCount = iNodeCount + 1;
								}

								lCompanyId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["COID"].ToString());

								if (lCompanyId != lCompany || lCompanyId == 0)
								{
									lCompany = lCompanyId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client[@id='" + lClient + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("company"), true);

									sCompanyName = p_objDataSet.Tables[0].Rows[iCnt]["CO_NAME"].ToString();

									if (sCompanyName != null && sCompanyName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sCompanyName, p_objDataSet.Tables[0].Rows[iCnt]["CO_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lClient.ToString());
									objXMLNodeEle.SetAttribute("id", lCompany.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lCompany)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "operation");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "CO");
									objXMLNodeEle.SetAttribute("level", "2");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lOperationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["OID"].ToString());

								if (lOperationId != lOperation || lOperationId == 0)
								{
									lOperation = lOperationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company[@id='" + lCompany + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("operation"), true);

									sOperationName = p_objDataSet.Tables[0].Rows[iCnt]["O_NAME"].ToString();

									if (sOperationName != null && sOperationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sOperationName, p_objDataSet.Tables[0].Rows[iCnt]["O_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lCompany.ToString());
									objXMLNodeEle.SetAttribute("id", lOperation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lOperation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "region");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "O");
									objXMLNodeEle.SetAttribute("level", "3");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lRegionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["RID"].ToString());

								if (lRegionId != lRegion || lRegionId == 0)
								{
									lRegion = lRegionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation[@id='" + lOperation + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("region"), true);

									sRegionName = p_objDataSet.Tables[0].Rows[iCnt]["R_NAME"].ToString();

									if (sRegionName != null && sRegionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sRegionName, p_objDataSet.Tables[0].Rows[iCnt]["R_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lOperation.ToString());
									objXMLNodeEle.SetAttribute("id", lRegion.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lRegion)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "division");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "R");
									objXMLNodeEle.SetAttribute("level", "4");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lDivisionId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["DID"].ToString());

								if (lDivisionId != lDivision || lDivisionId == 0)
								{
									lDivision = lDivisionId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region[@id='" + lRegion + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("division"), true);

									sDivisionName = p_objDataSet.Tables[0].Rows[iCnt]["D_NAME"].ToString();

									if (sDivisionName != null && sDivisionName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sDivisionName, p_objDataSet.Tables[0].Rows[iCnt]["D_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lRegion.ToString());
									objXMLNodeEle.SetAttribute("id", lDivision.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lDivision)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "location");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "D");
									objXMLNodeEle.SetAttribute("level", "5");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lLocationId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["LID"].ToString());

								if (lLocationId != lLocation || lLocationId == 0)
								{
									lLocation = lLocationId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region/division[@id='" + lDivision + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("location"), true);

									sLocationName = p_objDataSet.Tables[0].Rows[iCnt]["L_NAME"].ToString();

									if (sLocationName != null && sLocationName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sLocationName, p_objDataSet.Tables[0].Rows[iCnt]["L_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lDivision.ToString());
									objXMLNodeEle.SetAttribute("id", lLocation.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lLocation)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "facility");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "L");
									objXMLNodeEle.SetAttribute("level", "6");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lFacilityId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["FID"].ToString());

								if (lFacilityId != lFacility || lFacilityId == 0)
								{
									lFacility = lFacilityId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region/division/location[@id='" + lLocation + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("facility"), true);

									sFacilityName = p_objDataSet.Tables[0].Rows[iCnt]["F_NAME"].ToString();

									if (sFacilityName != null && sFacilityName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sFacilityName, p_objDataSet.Tables[0].Rows[iCnt]["F_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);

									objXMLNodeEle.SetAttribute("parent", lLocation.ToString());
									objXMLNodeEle.SetAttribute("id", lFacility.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lFacility)
										objXMLNodeEle.SetAttribute("iscurrent", "1");

									objXMLNodeEle.SetAttribute("child_name", "department");
									objXMLNodeEle.SetAttribute("ischild", "0");
									objXMLNodeEle.SetAttribute("code", "F");
									objXMLNodeEle.SetAttribute("level", "7");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}

								lDepartmentId = Conversion.ConvertStrToLong(p_objDataSet.Tables[0].Rows[iCnt]["DTID"].ToString());

								if (lDepartmentId != lDepartment || lDepartmentId == 0)
								{
									lDepartment = lDepartmentId;
									objXMLChildEle = (XmlElement)objXMLParentEle.SelectSingleNode("//client/company/operation/region/division/location/facility[@id='" + lFacility + "']");
									objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("department"), true);

									sDepartmentName = p_objDataSet.Tables[0].Rows[iCnt]["DT_NAME"].ToString();

									if (sDepartmentName != null && sDepartmentName != "")
										objXMLNodeEle.SetAttribute("name", JointAbbr(sDepartmentName, p_objDataSet.Tables[0].Rows[iCnt]["DT_AB"].ToString()));
									else
										objXMLNodeEle.SetAttribute("name", NO_ENTITY);
									//parag Start MITS 11080 
									objXMLNodeEle.SetAttribute("parent", lFacilityId.ToString());
									//parag End MITS 11080 
									objXMLNodeEle.SetAttribute("id", lDepartment.ToString());

									if (m_lSearchId != 0 && m_lSearchId == lDepartment)
										objXMLNodeEle.SetAttribute("iscurrent", "1");
									objXMLNodeEle.SetAttribute("isSelect", "1");
									objXMLNodeEle.SetAttribute("ischild", "");
									objXMLNodeEle.SetAttribute("level", "8");
									objXMLChildEle.AppendChild(objXMLNodeEle);
									iNodeCount = iNodeCount + 1;
								}
								break;
						}
						#endregion
						//if Org tree contains node more than 3500 then add result too large attribute
						//if (iNodeCount > 3500)
						if (iNodeCount > iMax_Number_Item)
						{
							objXML.LoadXml(sErrorCheckXML);
							objXMLErrorEle = (XmlElement)objXML.SelectSingleNode("//org[@name='Organization Hierarchy']");
							objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("error"), true);
							objXMLChildEle.SetAttribute("id", "0");
							objXMLChildEle.SetAttribute("name", "norecord");
							sLevel = GetLevelName(p_sNodcat);
							//The following error text is as per the existing VB component. 
							//This error message might get updated in future.
                            objXMLChildEle.SetAttribute("desc", Globalization.GetString("OrgHierarchy.TooManyItems", m_iClientId));//sharishkumar Rmacloud
							objXMLChildEle.SetAttribute("value", "1");
							objXMLErrorEle.AppendChild(objXMLChildEle);
							m_bIsError = true;
							m_sNodCat = "C";
							iResult = GetOrgPreferenceXml(objXML);
							sRetXml = OpenToThisLevel("C", m_lLevel, objXML, "", 0);
							return (sRetXml);
						}
					}
					XmlElement xmlTempEle = (XmlElement)objXML.SelectSingleNode("//OpenLevel");
					if (xmlTempEle == null)
						xmlTempEle = objXML.CreateElement("OpenLevel");
					xmlTempEle.InnerText = GetLevel(p_sNodcat).ToString();
					objXML.SelectSingleNode("//form").AppendChild(xmlTempEle);
				}
				else
				{
					string sRetOrgInfo = "";
					string sOrgInfo = "";
					sOrgInfo = objXML.InnerXml;
					sRetOrgInfo = GetOrgHierarchyXml(true, m_lLevel, 0, "", "", m_sTable, m_lRowId, sOrgInfo, out iEnh);
					objXML.LoadXml(sRetOrgInfo);
					objXMLChildEle = (XmlElement)objXML.ImportNode(GetNewElement("error"), true);
					objXMLChildEle.SetAttribute("id", "0");
					objXMLChildEle.SetAttribute("name", "norecord");
					objXMLChildEle.SetAttribute("desc", "Search Not Found");
					objXMLChildEle.SetAttribute("value", "1");
					objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//org[@name='Organization Hierarchy']");
					objXMLParentEle.AppendChild(objXMLChildEle);
					XmlElement xmlTempEle = (XmlElement)objXML.SelectSingleNode("//OpenLevel");
					if (xmlTempEle == null)
						xmlTempEle = objXML.CreateElement("OpenLevel");
					if (m_sNodCat != String.Empty)
						xmlTempEle.InnerText = GetLevel(m_sNodCat).ToString();

					objXML.SelectSingleNode("//form").AppendChild(xmlTempEle);
				}
				sRetXml = objXML.InnerXml;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetXmlbyName.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				objXML = null;
				objXMLParentEle = null;
				objXMLChildEle = null;
				objXMLNodeEle = null;
				objXMLErrorEle = null;
			}
			return (sRetXml);
		}
		#endregion

		#region Org Hierarchy by level
		/// Name		: GetOrgHierarchybyLevel
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the Organization Hierarchy information by level
		/// </summary>
		/// <param name="p_sSearch">Search string</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <param name="p_sInputXml">XML containing Org Hierarchy data</param>
		/// <param name="p_lLevel">Level of the current node</param>
		/// <param name="p_lNode">Node id</param>
		/// <param name="p_sTreeNode">Tree node</param>
		/// <param name="p_sTableName">Table name</param>
		/// <param name="p_lRowId">Row Id</param>
		/// <returns>XML string containing Org Hierarchy data for the given search criteria</returns>
		public string GetOrgHierarchybyLevel(string p_sSearch, string p_sNodCat, string p_sInputXml,
			long p_lLevel, long p_lNode, string p_sTreeNode, string p_sTableName, long p_lRowId,string p_sCity, long p_lState, string p_sZip, long p_lEntityId)
			//MITS 17157 Pankaj 7/15/09 Added entity id
		{
			bool bCaseInSens = false;
			// JP 12-05-2005   Cleanup. Not used.    int iResult = 0;
			string sSql = "";
			string sSearch = "";
			string sCity = "";  //MITS 16771- Pankaj 5/29/09 New parms of city, state, zip
			long lState = 0;
			string sZip = "";
			string sRetXML = "";
			DbConnection objConn = null;
			XmlDocument objXMLDOM = null;
			DataSet objDataSet = null;
			XmlElement objXMLElement = null;
			DbTransaction objTrans = null;
			// JP 12-05-2005   Cleanup. Not used.    DbDataAdapter objAdap = null;
			//sgoel6 MITS 14875 04/08/2009
			string sOperator = "=";
            string sSqlAdmin = String.Empty;
            bool bAdmin = false;

			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				m_sDBType = objConn.DatabaseType.ToString();

                // Ash - cloud, load orghierarchy settings from DB

				if (m_sDBType == Constants.DB_ORACLE)
				{
					objTrans = objConn.BeginTransaction();
				}

				objXMLDOM = new XmlDocument();
				objXMLDOM.LoadXml(p_sInputXml);

				sSearch = p_sSearch.Replace("'", "''");
				//MITS 16771- Pankaj 5/29/09
				sCity = p_sCity.Replace("'", "''");
				lState = p_lState;
				sZip = p_sZip.Replace("'", "''");

				//sgoel6 MITS 14875 04/08/2009
				//E.g. of OH Search
				//Search Text: ABC   | OH search will find data with value = ABC
				//Search Text: *ABC  | OH search will find data with value LIKE %ABC
				//Search Text: ABC*	 | OH search will find data with value LIKE ABC%
				//Search Text: *ABC* | OH search will find data with value LIKE %ABC%
				sOperator = "=";
				if (sSearch.IndexOf("*", 0) > -1)
				{
					sOperator = "LIKE";
					sSearch = sSearch.Replace("*", "%").Replace("%%", "%");
				}
				m_lLevel = p_lLevel;

				//Set attributes
				objXMLElement = (XmlElement)objXMLDOM.SelectSingleNode("//control[@name='orgTree']");
				objXMLElement.SetAttribute("num_level", m_lLevel.ToString());
				objXMLElement.SetAttribute("cmblevel", p_sNodCat);
				objXMLElement.SetAttribute("srchstr", p_sSearch);

				objXMLDOM = GetUserPreferenceXml(objXMLDOM);

				m_sTable = p_sTableName;
				m_lRowId = p_lRowId;

				//Raman 04/17/2009
				bool bchkFilter = false;
				XmlElement oEle = (XmlElement) objXMLDOM.SelectSingleNode("//chkFilter");
				if (oEle != null && oEle.InnerText == "1")
				{
					bchkFilter = true;
				}

				//Done by: Nikhil Garg		Dated: 03-May-2005
				//Added p_sTableName != "nothing" in the condition
				if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
					SetEffectiveDate(p_sTableName, p_lRowId);

                if (IsBesEnabled)
                {
                    sSqlAdmin = "SELECT GROUP_ENTITIES FROM ORG_SECURITY INNER JOIN GROUP_MAP ON ORG_SECURITY.GROUP_ID = GROUP_MAP.SUPER_GROUP_ID INNER JOIN ORG_SECURITY_USER ON GROUP_MAP.GROUP_ID = ORG_SECURITY_USER.GROUP_ID WHERE USER_ID = " + m_iUserId;
                    using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSqlAdmin))
                    {
                        while (oDbReader.Read())
                        {
                            bAdmin = oDbReader["GROUP_ENTITIES"].ToString() == "<ALL>";
                        }
                    }
                }

				//if node category is present
				if (p_sNodCat != "")
				{
					//look for case sensitiveness in case of Oracle database
					if (m_sDBType == Constants.DB_ORACLE)
					{
						bCaseInSens = IsCaseInSensitveSearch();
					}

					switch (p_sNodCat.ToUpper())
					{
						case CLIENT:
							sSql = "SELECT DISTINCT H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ENTITY H WHERE H.ENTITY_TABLE_ID=1005 AND H.DELETED_FLAG=0 AND H.ENTITY_ID>0";

							if (p_lEntityId != 0) //MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND H.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (H.ABBREVIATION LIKE '%" + sSearch + "%' OR H.LAST_NAME LIKE '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (H.ABBREVIATION " + sOperator + " '" + sSearch + "' OR H.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(H.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(H.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
								sSql = sSql + " AND (H.ABBREVIATION " + sOperator + " '" + sSearch + "' OR H.LAST_NAME " + sOperator + " '" + sSearch + "')";
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(H.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(H.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(H.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(H.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(H.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(H.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(H.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(H.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.CLIENT_EID = H.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}

							//Raman 04/13/2009: Trigger SQL should be appended in all scenarios
							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}

							//pmittal5 Confidential Record


                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND H.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSql = sSql + " ORDER BY H.ENTITY_ID";

							//Fill the dataset & get updated XML for the given node category
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
							sRetXML = GetXmlbyName(objXMLDOM, objDataSet, p_sNodCat);
							break;

						case COMPANY:
							sSql = "SELECT DISTINCT G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME," +
								"H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ENTITY G,ENTITY H WHERE G.ENTITY_TABLE_ID=1006 AND " +
								"G.PARENT_EID=H.ENTITY_ID AND G.ENTITY_ID>0 AND H.ENTITY_ID>0 AND " +
								"H.DELETED_FLAG=0 AND G.DELETED_FLAG=0";

							if (p_lEntityId != 0)//MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND G.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (G.ABBREVIATION " LIKE " '%" + sSearch + "%' OR G.LAST_NAME " LIKE " '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (G.ABBREVIATION " + sOperator + " '" + sSearch + "' OR G.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(G.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(G.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
                                sSql = sSql + " AND (G.ABBREVIATION " + sOperator + " '" + sSearch + "' OR G.LAST_NAME " + sOperator + " '" + sSearch + "')";
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(G.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(G.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(G.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(G.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(G.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(G.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(G.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(G.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.COMPANY_EID = G.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}

							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}
							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND G.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End

							sSql = sSql + " ORDER BY H.ENTITY_ID,G.ENTITY_ID";

							//Fill the dataset & get updated XML for the given node category
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
							sRetXML = GetXmlbyName(objXMLDOM, objDataSet, p_sNodCat);
							break;

						case OPERATION:
							sSql = "SELECT DISTINCT F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB,G.LAST_NAME CO_NAME," +
								"G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB " +
								"FROM ENTITY F,ENTITY G,ENTITY H WHERE F.ENTITY_TABLE_ID=1007 AND F.PARENT_EID=G.ENTITY_ID AND " +
								"G.PARENT_EID=H.ENTITY_ID AND H.ENTITY_ID>0 AND G.ENTITY_ID>0 AND " +
								"F.ENTITY_ID>0 AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 ";

							if (p_lEntityId != 0)//MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND F.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (F.ABBREVIATION LIKE '%" + sSearch + "%' OR F.LAST_NAME LIKE '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (F.ABBREVIATION " + sOperator + " '" + sSearch + "' OR F.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(F.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(F.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
                                sSql = sSql + " AND (F.ABBREVIATION " + sOperator + " '" + sSearch + "' OR F.LAST_NAME " + sOperator + " '" + sSearch + "')";
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(F.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(F.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(F.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(F.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(F.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(F.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(F.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(F.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.OPERATION_EID = F.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}

							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}
							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND F.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End

							sSql = sSql + " ORDER BY H.ENTITY_ID,G.ENTITY_ID,F.ENTITY_ID";

							//Fill the dataset & get updated XML for the given node category
							objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
							sRetXML = GetXmlbyName(objXMLDOM, objDataSet, p_sNodCat);
							break;

						case REGION:
							sSql = "SELECT DISTINCT E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB," +
									"F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB, " +
									"G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB, " +
									"H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB " +
									"FROM ENTITY E,ENTITY F,ENTITY G,ENTITY H " +
									"WHERE E.ENTITY_TABLE_ID=1008 AND E.PARENT_EID=F.ENTITY_ID  AND " +
									"F.PARENT_EID=G.ENTITY_ID  AND G.PARENT_EID=H.ENTITY_ID  AND " +
									"H.ENTITY_ID>0 AND G.ENTITY_ID>0 AND F.ENTITY_ID>0 " +
									"AND H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 " +
									"AND E.DELETED_FLAG=0 ";

							if (p_lEntityId != 0)//MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND E.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (E.ABBREVIATION LIKE '%" + sSearch + "%' OR E.LAST_NAME LIKE '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (E.ABBREVIATION " + sOperator + " '" + sSearch + "' OR E.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(E.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(E.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
                                sSql = sSql + " AND (E.ABBREVIATION " + sOperator + " '" + sSearch + "' OR E.LAST_NAME " + sOperator + " '" + sSearch + "')";
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(E.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(E.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(E.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(E.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(E.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(E.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(E.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(E.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.REGION_EID = E.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}

							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}

							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND E.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSql = sSql + " ORDER BY H.ENTITY_ID,G.ENTITY_ID,F.ENTITY_ID,E.ENTITY_ID";

							sRetXML = GenerateData(objXMLDOM, p_sNodCat, sSql, objConn);
							break;

						case DIVISION:
							sSql = "SELECT DISTINCT D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB,E.LAST_NAME R_NAME,E.ENTITY_ID RID, " +
									"E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB,G.LAST_NAME CO_NAME,G.ENTITY_ID COID, " +
									"G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB " +
									"FROM ENTITY D,ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE " +
									"D.ENTITY_TABLE_ID=1009 AND D.PARENT_EID=E.ENTITY_ID AND E.PARENT_EID=F.ENTITY_ID AND " +
									"F.PARENT_EID=G.ENTITY_ID AND G.PARENT_EID=H.ENTITY_ID AND D.ENTITY_ID>0 AND " +
									"E.ENTITY_ID>0 AND F.ENTITY_ID>0 AND G.ENTITY_ID>0 AND H.ENTITY_ID>0 AND H.DELETED_FLAG=0 " +
									"AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0 ";

							if (p_lEntityId != 0)//MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND D.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (D.ABBREVIATION LIKE '%" + sSearch + "%' OR D.LAST_NAME LIKE '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (D.ABBREVIATION " + sOperator + " '" + sSearch + "' OR D.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(D.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(D.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
                                sSql = sSql + " AND (D.ABBREVIATION " + sOperator + " '" + sSearch + "' OR D.LAST_NAME " + sOperator + " '" + sSearch + "')";//Deb MITS 31355
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(D.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(D.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(D.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(D.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(D.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(D.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(D.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(D.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.DIVISION_EID = D.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}

							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}
							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND D.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End

							sSql = sSql + " ORDER BY H.ENTITY_ID,G.ENTITY_ID,F.ENTITY_ID,E.ENTITY_ID,D.ENTITY_ID";

							sRetXML = GenerateData(objXMLDOM, p_sNodCat, sSql, objConn);
							break;

						case LOCATION:
							sSql = "SELECT DISTINCT C.LAST_NAME L_NAME,C.ENTITY_ID LID,C.ABBREVIATION L_AB,D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB," +
								"E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB, G.LAST_NAME CO_NAME," +
								"G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ENTITY C,ENTITY D," +
								"ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE C.ENTITY_TABLE_ID=1010 AND C.PARENT_EID=D.ENTITY_ID AND " +
								"D.PARENT_EID=E.ENTITY_ID AND E.PARENT_EID=F.ENTITY_ID AND F.PARENT_EID=G.ENTITY_ID AND " +
								"G.PARENT_EID=H.ENTITY_ID AND H.ENTITY_ID>0 AND G.ENTITY_ID>0 AND F.ENTITY_ID>0 AND " +
								"E.ENTITY_ID>0 AND D.ENTITY_ID>0 AND C.ENTITY_ID>0 AND H.DELETED_FLAG=0 AND " +
								"G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0 AND C.DELETED_FLAG=0";

							if (p_lEntityId != 0)//MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND C.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (C.ABBREVIATION LIKE '%" + sSearch + "%' OR C.LAST_NAME LIKE '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
                                //sSql = sSql + " AND (C.ABBREVIATION " + sOperator + " '" + sSearch + "' OR C.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(C.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(C.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
                                sSql = sSql + " AND (C.ABBREVIATION " + sOperator + " '" + sSearch + "' OR C.LAST_NAME " + sOperator + " '" + sSearch + "')";
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(C.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(C.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(C.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(C.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(C.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(C.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(C.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(C.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.LOCATION_EID = C.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}

							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}

							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND C.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSql = sSql + " ORDER BY H.ENTITY_ID,G.ENTITY_ID,F.ENTITY_ID,E.ENTITY_ID,D.ENTITY_ID,C.ENTITY_ID";

							sRetXML = GenerateData(objXMLDOM, p_sNodCat, sSql, objConn);
							break;

						case FACILITY:
							sSql = "SELECT DISTINCT B.LAST_NAME F_NAME,B.ENTITY_ID FID,B.ABBREVIATION F_AB,C.LAST_NAME L_NAME,C.ENTITY_ID LID,C.ABBREVIATION C_AB," +
								"D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB,E.LAST_NAME R_NAME,E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME," +
								"F.ENTITY_ID OID,F.ABBREVIATION O_AB,G.LAST_NAME CO_NAME,G.ENTITY_ID COID,G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID," +
								"H.ABBREVIATION L_AB FROM ENTITY B,ENTITY C,ENTITY D,ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE " +
								"B.ENTITY_TABLE_ID=1011 AND B.PARENT_EID=C.ENTITY_ID AND C.PARENT_EID=D.ENTITY_ID AND D.PARENT_EID=E.ENTITY_ID AND " +
								"E.PARENT_EID=F.ENTITY_ID AND F.PARENT_EID=G.ENTITY_ID AND G.PARENT_EID=H.ENTITY_ID AND H.ENTITY_ID>0 AND " +
								"G.ENTITY_ID>0 AND F.ENTITY_ID>0 AND E.ENTITY_ID>0 AND D.ENTITY_ID>0 AND C.ENTITY_ID>0 AND B.ENTITY_ID>0 AND H.DELETED_FLAG=0" +
								" AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0 AND C.DELETED_FLAG=0 AND B.DELETED_FLAG=0";

							if (p_lEntityId != 0)//MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND B.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (B.ABBREVIATION LIKE '%" + sSearch + "%' OR B.LAST_NAME LIKE '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (B.ABBREVIATION " + sOperator + " '" + sSearch + "' OR B.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(B.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(B.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
                                sSql = sSql + " AND (B.ABBREVIATION " + sOperator + " '" + sSearch + "' OR B.LAST_NAME " + sOperator + " '" + sSearch + "')";
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(B.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(B.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(B.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(B.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(B.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(B.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(B.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(B.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.FACILITY_EID = B.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}
							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}
							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End

							sSql = sSql + " ORDER BY H.ENTITY_ID,G.ENTITY_ID,F.ENTITY_ID,E.ENTITY_ID,D.ENTITY_ID,C.ENTITY_ID,B.ENTITY_ID";

							sRetXML = GenerateData(objXMLDOM, p_sNodCat, sSql, objConn);
							break;

						case DEPARTMENT:
							sSql = "SELECT DISTINCT B1.LAST_NAME DT_NAME,B1.ENTITY_ID DTID,B1.ABBREVIATION DT_AB,B.LAST_NAME F_NAME,B.ENTITY_ID FID,B.ABBREVIATION F_AB," +
								"C.LAST_NAME L_NAME,C.ENTITY_ID LID,C.ABBREVIATION L_AB,D.LAST_NAME D_NAME,D.ENTITY_ID DID,D.ABBREVIATION D_AB,E.LAST_NAME R_NAME," +
								"E.ENTITY_ID RID,E.ABBREVIATION R_AB,F.LAST_NAME O_NAME,F.ENTITY_ID OID,F.ABBREVIATION O_AB,G.LAST_NAME CO_NAME,G.ENTITY_ID COID," +
								"G.ABBREVIATION CO_AB,H.LAST_NAME C_NAME,H.ENTITY_ID CID,H.ABBREVIATION C_AB FROM ENTITY B1,ENTITY B,ENTITY C," +
								"ENTITY D,ENTITY E,ENTITY F,ENTITY G,ENTITY H WHERE B1.ENTITY_TABLE_ID=1012 AND B1.PARENT_EID=B.ENTITY_ID AND " +
								"B.PARENT_EID= C.ENTITY_ID AND C.PARENT_EID=D.ENTITY_ID AND D.PARENT_EID=E.ENTITY_ID AND E.PARENT_EID=F.ENTITY_ID AND " +
								"F.PARENT_EID= G.ENTITY_ID AND G.PARENT_EID=H.ENTITY_ID AND H.ENTITY_ID>0 AND G.ENTITY_ID>0 AND F.ENTITY_ID>0 AND E.ENTITY_ID>0 AND " +
								"D.ENTITY_ID>0 AND C.ENTITY_ID>0 AND B.ENTITY_ID>0 AND B1.ENTITY_ID>0 AND  H.DELETED_FLAG=0 AND G.DELETED_FLAG=0 AND F.DELETED_FLAG=0 " +
								"AND E.DELETED_FLAG=0 AND D.DELETED_FLAG=0 AND C.DELETED_FLAG=0 AND B.DELETED_FLAG=0 AND B1.DELETED_FLAG=0";

							if (p_lEntityId != 0)//MITS 17157 Pankaj 7/15/09
								sSql = sSql + " AND B1.ENTITY_ID = " + p_lEntityId; 

							if (sSearch != "")
							if (bCaseInSens == false)
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (B1.ABBREVIATION LIKE '%" + sSearch + "%' OR B1.LAST_NAME LIKE '%" + sSearch + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (B1.ABBREVIATION " + sOperator + " '" + sSearch + "' OR B1.LAST_NAME " + sOperator + " '" + sSearch + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (LTRIM(RTRIM(B1.ABBREVIATION)) " + sOperator + " '" + sSearch + "' OR LTRIM(RTRIM(B1.LAST_NAME)) " + sOperator + " '" + sSearch + "')";
                                sSql = sSql + " AND (B1.ABBREVIATION " + sOperator + " '" + sSearch + "' OR B1.LAST_NAME " + sOperator + " '" + sSearch + "')";
							else
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql + " AND (UPPER(B1.ABBREVIATION) LIKE '%" + sSearch.ToUpper() + "%' OR UPPER(B1.LAST_NAME) LIKE '%" + sSearch.ToUpper() + "%')";
								//MGaba2: MITS 17231:search does not trim the spaces before comparing
								//sSql = sSql + " AND (UPPER(B1.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(B1.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                                //sSql = sSql + " AND (UPPER(LTRIM(RTRIM(B1.ABBREVIATION))) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LTRIM(RTRIM(B1.LAST_NAME))) " + sOperator + " '" + sSearch.ToUpper() + "')";
                                sSql = sSql + " AND (UPPER(B1.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(B1.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";
							//MITS 16771- Pankaj 5/29/09
							sSql = sSql + AddAdditionalFilter(sCity, lState, sZip, p_sNodCat.ToUpper(), bCaseInSens);

							if (p_lNode != 0)
							{
								//sgoel6 MITS 14875 04/08/2009
								//sSql = sSql.Replace("FROM", "FROM ORG_HIERARCHY A,");
								//sSql = sSql.Replace("WHERE", "WHERE A.DEPARTMENT_EID = B1.ENTITY_ID AND");
								sSql = GetSqlForLevel(sSql, p_sTreeNode, p_lNode);
							}

							//if (p_sTableName != "" & p_sTableName != null & p_sTableName != "nothing")
							if (bchkFilter)
							{
								sSql = AppendDateTriggerSql(sSql, p_sNodCat);
							}
							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSql = sSql + " AND B1.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End

							sSql = sSql + " ORDER BY H.ENTITY_ID,G.ENTITY_ID,F.ENTITY_ID,E.ENTITY_ID,D.ENTITY_ID,C.ENTITY_ID,B.ENTITY_ID,B1.ENTITY_ID";

							sRetXML = GenerateData(objXMLDOM, p_sNodCat, sSql, objConn);
							break;

					}
				}
				if (m_sDBType == Constants.DB_ORACLE)
				{
					objTrans.Commit();
				}
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
				if (objTrans != null) objTrans.Dispose();
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOrgHierarchybyLevel.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objConn != null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn.Dispose();
				}
				if (objTrans != null) objTrans.Dispose();
				if (objDataSet != null) objDataSet.Dispose();
				objXMLDOM = null;
				objXMLElement = null;
				// JP 12-05-2005   Cleanup. Not used.    objAdap = null;
			}
			return (sRetXML);
		}
		#endregion

		#region Overloaded Get SQL
		/// Name		: GetSql
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Builds the SQL queries
		/// </summary>
		/// <param name="p_sSql">String containing SQL queries</param>
		/// <param name="p_sTreeNode">Tree node</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sAliasName">Alias name</param>
		/// <returns>Modified SQL string</returns>
		private string GetSql(string p_sSql, string p_sTreeNode, long p_lNode, string p_sAliasName)
		{
			string sTableName = "";

			try
			{
				if (p_sAliasName == "")
					sTableName = "A";
				else
					sTableName = p_sAliasName;

				switch (p_sTreeNode)
				{
					case CLIENT:
						p_sSql = p_sSql + " AND " + sTableName + ".CLIENT_EID=" + p_lNode;
						break;
					case COMPANY:
						p_sSql = p_sSql + " AND " + sTableName + ".COMPANY_EID=" + p_lNode;
						break;
					case OPERATION:
						p_sSql = p_sSql + " AND " + sTableName + ".OPERATION_EID=" + p_lNode;
						break;
					case REGION:
						p_sSql = p_sSql + " AND " + sTableName + ".REGION_EID=" + p_lNode;
						break;
					case DIVISION:
						p_sSql = p_sSql + " AND " + sTableName + ".DIVISION_EID=" + p_lNode;
						break;
					case LOCATION:
						p_sSql = p_sSql + " AND " + sTableName + ".LOCATION_EID=" + p_lNode;
						break;
					case FACILITY:
						p_sSql = p_sSql + " AND " + sTableName + ".FACILITY_EID=" + p_lNode;
						break;
					case DEPARTMENT:
						p_sSql = p_sSql + " AND " + sTableName + ".DEPARTMENT_EID=" + p_lNode;
						break;
				}
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.SqlError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			return p_sSql;
		}

		private string GetSqlForLevel(string p_sSql, string p_sTreeNode, long p_lNode)
		{
			return (GetSqlForLevel(p_sSql, p_sTreeNode, p_lNode, ""));
		}
		private string GetSqlForLevel(string p_sSql, string p_sTreeNode, long p_lNode, string p_sAliasName)
		{
			string sTableName = "";

			try
			{
				if (p_sAliasName == "")
					sTableName = "A";
				else
					sTableName = p_sAliasName;

				switch (p_sTreeNode)
				{
					case CLIENT:
						p_sSql = p_sSql + " AND H.ENTITY_ID=" + p_lNode;
						break;
					case COMPANY:
						p_sSql = p_sSql + " AND G.ENTITY_ID=" + p_lNode;
						break;
					case OPERATION:
						p_sSql = p_sSql + " AND F.ENTITY_ID=" + p_lNode;
						break;
					case REGION:
						p_sSql = p_sSql + " AND E.ENTITY_ID=" + p_lNode;
						break;
					case DIVISION:
						p_sSql = p_sSql + " AND D.ENTITY_ID=" + p_lNode;
						break;
					case LOCATION:
						p_sSql = p_sSql + " AND C.ENTITY_ID=" + p_lNode;
						break;
					case FACILITY:
						p_sSql = p_sSql + " AND B.ENTITY_ID=" + p_lNode;
						break;
					case DEPARTMENT:
						p_sSql = p_sSql + " AND B1.ENTITY_ID=" + p_lNode;
						break;
				}
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.SqlError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			return p_sSql;
		}
		#endregion

		#region Get SQL
		/// Name		: GetSql
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method
		/// </summary>
		/// <param name="p_sSql">String containing SQL queries</param>
		/// <param name="p_sTreeNode">Tree node</param>
		/// <param name="p_lNode">Node Id</param>
		/// <returns>Modified SQL string</returns>
		private string GetSql(string p_sSql, string p_sTreeNode, long p_lNode)
		{
			return GetSql(p_sSql, p_sTreeNode, p_lNode, "");
		}
		#endregion

		#region Get User Preference XML
		/// Name		: GetUserPreferenceXml
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the user preference XML from database
		/// </summary>
		/// <param name="p_objXMLDOM">XML DOM containing Org Hierarchy data</param>
		/// <returns>XML DOM containing user preference data</returns>
		private XmlDocument GetUserPreferenceXml(XmlDocument p_objXMLDOM)
		{
			string sXMLText = "";
			string sSQL = "";
			XmlDocument objPrefXMLDOM = null;
			XmlElement objXMLElement = null;
			XmlNodeList objXMLNodeList = null;
			DbReader objReader = null;

			try
			{
				objXMLElement = (XmlElement)p_objXMLDOM.SelectSingleNode("//form/group/control[@name='dlevel']");
				if (objXMLElement != null)
				{
					if (objXMLElement.GetAttribute("userid") != "")
						m_lUserId = Common.Conversion.ConvertStrToLong(objXMLElement.GetAttribute("userid"));
				}

				//Fetch user preference XML from the database
				sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + m_lUserId;
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

				if (objReader != null)
				{
					if (objReader.Read())
					{
						sXMLText = objReader.GetString("PREF_XML");
						objReader.Close();
					}

					//Fetch node category
					if (sXMLText != "")
					{
						objPrefXMLDOM = new XmlDocument();
						objPrefXMLDOM.LoadXml(sXMLText);
						objXMLElement = (XmlElement)objPrefXMLDOM.SelectSingleNode("//OrgViewNet");
						if (objXMLElement != null)
						{
							m_sNodCat = objXMLElement.GetAttribute("vid");
							m_sFilter = objXMLElement.GetAttribute("filter");
						}
					}
				}

				//if node category is not present then give default as "C" i.e. Client
				if (m_sNodCat == "")
					m_sNodCat = "C";

				//Set "selected" atribute
				objXMLNodeList = p_objXMLDOM.SelectNodes("//levellist/option");
				foreach (XmlElement objXMLEle in objXMLNodeList)
				{
					if (objXMLEle.GetAttribute("value") == m_sNodCat)
						objXMLEle.SetAttribute("selected", "1");
				}
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetUserPreferenceXml.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objReader != null)
				{
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
					objReader.Dispose();
				}
				objPrefXMLDOM = null;
				objXMLElement = null;
				objXMLNodeList = null;
			}
			return p_objXMLDOM;
		}
		#endregion

		#region Case Sensitiveness
		/// Name		: IsCaseInSensitveSearch
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Finds the case sensitiveness for Oracle database
		/// </summary>
		/// <returns>boolean value indicating case sensitiveness</returns>
		private bool IsCaseInSensitveSearch()
		{
			int iResult;
			string sSQL = "";
			DbReader objReader = null;

			try
			{
				sSQL = "SELECT * FROM SYS_PARMS";
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

				if (objReader != null)
				{
					if (objReader.Read())
					{
						iResult = objReader.GetInt("ORACLE_CASE_INS");
						objReader.Close();
						if (iResult == 1 || iResult == -1)
							return true;
					}
				}
			}
			//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.IsCaseInSensitveSearch.DataError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}

			finally
			{
				if (objReader != null)
				{
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
					objReader.Dispose();
				}
			}
			return false;
		}
		#endregion

		#region Get level name
		/// Name		: GetLevelName
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the level of the Org Hierarchy
		/// </summary>
		/// <param name="p_sLevel">Level</param>
		/// <returns>Level depending upon the given parameter</returns>
		private string GetLevelName(string p_sLevel)
		{
			string sRetVal = "";
			try
			{
				switch (p_sLevel)
				{
					case CLIENT:
						sRetVal = "[CLIENT]";
						break;
					case COMPANY:
						sRetVal = "[COMPANY]";
						break;
					case OPERATION:
						sRetVal = "[OPERATION]";
						break;
					case REGION:
						sRetVal = "[REGION]";
						break;
					case DIVISION:
						sRetVal = "[DIVISION]";
						break;
					case LOCATION:
						sRetVal = "[LOCATION]";
						break;
					case FACILITY:
						sRetVal = "[FACILITY]";
						break;
					case DEPARTMENT:
						sRetVal = "[DEPARTMENT]";
						break;
				}
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetLevelName.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally { }
			return (sRetVal);
		}
		#endregion

		#region Get Organization with address
		/// Name		: GetOrganizationsWithAddr
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the Organization Hierarchy with address
		/// </summary>
		/// <param name="p_sName">Name of the node</param>
		/// <param name="p_sNodCat">Node category</param>
		/// <param name="p_lLevel">Level of the current node</param>
		/// <param name="p_lNode">Node Id</param>
		/// <param name="p_sTreeNode">Tree node</param>
		/// <param name="p_iSearchAddr">Indicates whether address check box is clicked or not</param>
		/// <param name="p_iScreenType">Indicates screen type: popup or maintenance.</param>
		/// <returns>XML string containing Org Hierarchy information</returns>
		public string GetOrganizationsWithAddr(string p_sName, string p_sNodCat, long p_lLevel, long p_lNode,
			string p_sTreeNode, int p_iSearchAddr, int p_iScreenType)
		{
			bool blnCaseInSens = false;
			string sSql = "";
			string sOp = "";
			string sRetXML = "";
			string sSearchOn = "";
			int iLevel = 0;
			DbConnection objConn = null;
			StringBuilder objStringBuild = null;
			DataSet objDataSet = null;

			try
			{
				m_lLevel = p_lLevel;

				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				m_sDBType = objConn.DatabaseType.ToString();
				objConn.Close();

				//check for Oracle case sensitiveness
				if (m_sDBType == Constants.DB_ORACLE)
				{
					blnCaseInSens = IsCaseInSensitveSearch();
				}
				objStringBuild = new StringBuilder();
				objStringBuild.Append("SELECT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME  E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME  P_LAST_NAME,"
                    + " E.ADDR1,E.ADDR2,E.ADDR3,E.ADDR4,E.CITY,S.STATE_ID,E.ZIP_CODE"
				    + " FROM ENTITY  E, ENTITY  P,STATES  S"
				    + "");
				if (objStringBuild.Length > 0)
					sSql = objStringBuild.ToString();

				if (p_lNode != 0)
					sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");


				//Generate SQL queries on the basis of the given node id
				//////				switch (p_sNodCat.ToUpper())
				//////				{
				//////					case CLIENT :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE  E.ENTITY_TABLE_ID=1005 AND E.ENTITY_ID=A.CLIENT_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////
				//////					case COMPANY :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE  E.ENTITY_TABLE_ID=1006 AND E.ENTITY_ID=A.COMPANY_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////
				//////					case OPERATION :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE  E.ENTITY_TABLE_ID=1007 AND E.ENTITY_ID=A.OPERATION_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////
				//////					case REGION :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE  E.ENTITY_TABLE_ID=1008 AND E.ENTITY_ID=A.REGION_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////					case DIVISION :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE  E.ENTITY_TABLE_ID=1009 AND E.ENTITY_ID=A.DIVISION_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////					case LOCATION :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE  E.ENTITY_TABLE_ID=1010 AND E.ENTITY_ID=A.LOCATION_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////					case FACILITY :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE E.ENTITY_TABLE_ID=1011 AND E.ENTITY_ID=A.FACILITY_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////					case DEPARTMENT :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE E.ENTITY_TABLE_ID=1012 AND E.ENTITY_ID=A.DEPARTMENT_EID");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;
				//////					case ALL :
				//////						objStringBuild = new StringBuilder ();
				//////						//Changed By: Nikhil Garg    Dated: 10-Mar-2005.   Change: Gave Alias to Column Name 
				//////						objStringBuild.Append ("SELECT DISTINCT E.ENTITY_ID,E.ABBREVIATION,E.LAST_NAME AS E_LAST_NAME,E.ENTITY_TABLE_ID,P.LAST_NAME AS P_LAST_NAME,");
				//////						objStringBuild.Append ("E.ADDR1,E.ADDR2,E.CITY,S.STATE_ID,E.ZIP_CODE");
				//////						objStringBuild.Append (" FROM ENTITY E, ENTITY P,STATES S,ORG_HIERARCHY A");
				//////						objStringBuild.Append (" WHERE E.ENTITY_TABLE_ID BETWEEN 1005 AND 1012");
				//////						objStringBuild.Append (" AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID");
				//////
				//////						if (objStringBuild.Length > 0)
				//////							sSql = objStringBuild.ToString ();
				//////
				//////						if (p_lNode != 0)
				//////							sSql = GetSql(sSql, p_sTreeNode, p_lNode, "");
				//////						break;

				//////				}
				switch (p_sNodCat.ToUpper())
				{
					case CLIENT:
						iLevel = 1005;
						break;
					case COMPANY:
						iLevel = 1006;
						break;
					case OPERATION:
						iLevel = 1007;
						break;
					case REGION:
						iLevel = 1008;
						break;
					case DIVISION:
						iLevel = 1009;
						break;
					case LOCATION:
						iLevel = 1010;
						break;
					case FACILITY:
						iLevel = 1011;
						break;
					case DEPARTMENT:
						iLevel = 1012;
						break;
					case ALL:
						iLevel = 0;
						break;
				}
				sSearchOn = p_sName;
				if (sSearchOn != "")
				{
					if (sSearchOn.IndexOf("*") != -1)
					{
						sOp = "LIKE";
						sSearchOn = sSearchOn.Replace("*", "%");
					}
					else
					{
						sOp = "LIKE";
						sSearchOn = sSearchOn + "%";
					}

					sSearchOn = sOp + " '" + sSearchOn.Replace("'", "''") + "'";

					if (blnCaseInSens == false)
						sSql = sSql + " WHERE (E.ABBREVIATION " + sSearchOn + " OR E.LAST_NAME " + sSearchOn;
					else
						sSql = sSql + " WHERE (UPPER(E.ABBREVIATION) " + sSearchOn.ToUpper() + " OR UPPER(E.LAST_NAME) " + sSearchOn.ToUpper();

					if (p_iSearchAddr == 1)
					{
						if (blnCaseInSens == false)
                            sSql = sSql + " OR E.ADDR1 " + sSearchOn + " OR E.ADDR2 " + sSearchOn + " OR E.ADDR3 " + sSearchOn + " OR E.ADDR4 " + sSearchOn + " OR E.CITY " + sSearchOn + " OR S.STATE_ID " + sSearchOn + " OR E.ZIP_CODE " + sSearchOn + ") AND ";
						else
                            sSql = sSql + " OR UPPER(E.ADDR1) " + sSearchOn.ToUpper() + " OR UPPER(E.ADDR2) " + sSearchOn.ToUpper() + " OR UPPER(E.ADDR3) " + sSearchOn.ToUpper() + " OR UPPER(E.ADDR4) " + sSearchOn.ToUpper() + " OR UPPER(E.CITY) " + sSearchOn.ToUpper() + " OR UPPER(S.STATE_ID) " + sSearchOn.ToUpper() + " OR E.ZIP_CODE " + sSearchOn.ToUpper() + ") AND ";
					}
					else
						sSql = sSql + ") AND ";

					//					If cboOrgLevel.ListIndex <> -1 Then
					//        lLevel = cboOrgLevel.ItemData(cboOrgLevel.ListIndex)
					//        If lLevel >= 1005 And lLevel <= 1012 Then
					//            sSQL = sSQL & " AND E.ENTITY_TABLE_ID = " & lLevel
					//        Else
					//            sSQL = sSQL & " AND E.ENTITY_TABLE_ID BETWEEN 1005 AND 1012"
					//        End If
					//    End If
					//    
					//    ' ... put in join criteria
					//    sSQL = sSQL & " AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID"
					//    
					//    If chkSelBranchOnly And lSelItemEID <> 0 Then
					//        Set parentOrg = orgCache.Orgs(lSelItemEID)
					//        If Not parentOrg Is Nothing Then
					//            sSQL = sSQL & " AND OH." & sGetSystemTableName(parentOrg.tableid) & "_EID = "
					//            
					//        End If
					//    End If
					//    
					//    sSQL = sSQL & " ORDER BY E.LAST_NAME,E.ABBREVIATION"
					//    

				}
				else
					sSql = sSql + " WHERE ";
				if (iLevel >= 1005 && iLevel <= 1012)
					sSql = sSql + " E.ENTITY_TABLE_ID = " + iLevel;
				else
					sSql = sSql + " E.ENTITY_TABLE_ID BETWEEN 1005 AND 1012";
				sSql = sSql + " AND E.PARENT_EID = P.ENTITY_ID AND E.STATE_ID = S.STATE_ROW_ID";


				sSql = sSql + " ORDER BY E.LAST_NAME,E.ABBREVIATION";
				objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSql, m_iClientId);
				sRetXML = MakeXmlForSearch(objDataSet, p_iScreenType);
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOrganizationsWithAddr.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
				if (objDataSet != null) objDataSet = null;
				objStringBuild = null;
			}
			return (sRetXML);
		}
		#endregion

		#region Make XML for search
		/// Name		: MakeXmlForSearch
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Makes XML string with the search results
		/// </summary>
		/// <param name="p_objDataSet">Data set containing search results</param>
		/// <returns>XML string containing search results</returns>
		private string MakeXmlForSearch(DataSet p_objDataSet, int iScreenType)
		{
			int iTableId = 0;
			string sSearchin = "";
			string sMakeXmlForSearch = "";
			XmlDocument objDom = null;
			XmlElement objElm = null;
			XmlNode objNode = null;
			string sFormTag = string.Empty;

			try
			{
				objDom = new XmlDocument();
				if (iScreenType == 0)
					sFormTag = "windowstatus_new()";
				else
					sFormTag = "windowstatus_new()";
				objDom.LoadXml("<form><body1  req_func='yes' func='" + sFormTag + "'></body1><group name='orghier'><control type='orgsearch' name='orgentities'><search></search></control></group></form>");
				objNode = objDom.SelectSingleNode("//search");

				if (p_objDataSet.Tables[0].Rows.Count > 0)
				{
					for (int iCnt = 0; iCnt < p_objDataSet.Tables[0].Rows.Count; iCnt++)
					{
						//changed by: Nikhil Garg		Dated: 10-Mar-2005    Change: New Element has to be from the same document context 
						objElm = (XmlElement)objDom.ImportNode(GetNewElement("result"), true);
						//objElm = GetNewElement("result");
						//changed by: Nikhil Garg		Dated: 10-Mar-2005    Change:Used Alias column for accessing Last name
						objElm.SetAttribute("cName", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["E_LAST_NAME"]));
						objElm.SetAttribute("pName", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["P_LAST_NAME"]));
						objElm.SetAttribute("eid", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_ID"]));
						objElm.SetAttribute("abr", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ABBREVIATION"]));
						objElm.SetAttribute("city", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["CITY"]));
                        objElm.SetAttribute("addr", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ADDR1"] + " " + p_objDataSet.Tables[0].Rows[iCnt]["ADDR2"] + " " + p_objDataSet.Tables[0].Rows[iCnt]["ADDR3"] + " " + p_objDataSet.Tables[0].Rows[iCnt]["ADDR4"]));
						objElm.SetAttribute("state", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["STATE_ID"]));
						objElm.SetAttribute("zip", Conversion.ConvertObjToStr(p_objDataSet.Tables[0].Rows[iCnt]["ZIP_CODE"]));

						//Changed by: Nikhil Garg		Dated: 10-Mar-2005    Change:changed TableId to Int instead of Long 
						iTableId = (int)p_objDataSet.Tables[0].Rows[iCnt]["ENTITY_TABLE_ID"];

						switch (iTableId)
						{
							case 1005:
								sSearchin = "C";
								break;
							case 1006:
								sSearchin = "CO";
								break;
							case 1007:
								sSearchin = "O";
								break;
							case 1008:
								sSearchin = "R";
								break;
							case 1009:
								sSearchin = "D";
								break;
							case 1010:
								sSearchin = "L";
								break;
							case 1011:
								sSearchin = "F";
								break;
							case 1012:
								sSearchin = "DT";
								break;
						}
						objElm.SetAttribute("level", m_lLevel.ToString());
						objElm.SetAttribute("searchin", sSearchin);
						objNode.AppendChild(objElm);
					}
				}
				else
				{
					objElm = (XmlElement)objDom.SelectSingleNode("//group[@name='orghier']");

					if (objElm != null)
						objElm.ParentNode.RemoveChild(objElm);
				}
				sMakeXmlForSearch = objDom.InnerXml;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.MakeXmlForSearch.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				objDom = null;
				objElm = null;
				objNode = null;
			}
			return (sMakeXmlForSearch);
		}
		#endregion

		#region Flat Search
		/// Name		: GetEnitySearch
		/// Author		: Sumeet Rathod
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the results of entity search
		/// </summary>
		/// <param name="p_lEntityId">Entity Id</param>
		/// <param name="p_sLookUp">Look up string</param>
		/// <returns>XML containing entity search results</returns>
		public string GetEnitySearch(long p_lEntityId, string p_sLookUp, string p_sCity, long p_lState, string p_sZip)
		{
			string sSQL = "";
			string sGetEntitySearch = "";
			DataSet objDataSet = null;
			bool bCaseInSens = false;
			DbConnection objConn = null;
			//sgoel6 | MITS 14875 | 04/08/2009
			string sOperator = "=";
			string sSearch = p_sLookUp;
            string sSqlAdmin = String.Empty;
            bool bAdmin = false;

			try
			{
				//sgoel6 | MITS 14875 | 04/08/2009
				if (sSearch.IndexOf("*", 0) > -1)
				{
					sOperator = "LIKE";
					sSearch = sSearch.Replace("*", "%").Replace("%%", "%");
				}
				sSearch = sSearch.Replace("'", "''");
				p_sCity = p_sCity.Replace("'", "''");
				p_sZip = p_sZip.Replace("'", "''");

				//MITS 16771- Pankaj 5/29/09
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				if (objConn.DatabaseType.ToString() == Constants.DB_ORACLE)
				{
					bCaseInSens = IsCaseInSensitveSearch();
				}
				objConn.Close();
				objConn.Dispose();

                if (IsBesEnabled)
                {
                    sSqlAdmin = "SELECT GROUP_ENTITIES FROM ORG_SECURITY INNER JOIN GROUP_MAP ON ORG_SECURITY.GROUP_ID = GROUP_MAP.SUPER_GROUP_ID INNER JOIN ORG_SECURITY_USER ON GROUP_MAP.GROUP_ID = ORG_SECURITY_USER.GROUP_ID WHERE USER_ID = " + m_iUserId;
                    using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSqlAdmin))
                    {
                        while (oDbReader.Read())
                        {
                            bAdmin = oDbReader["GROUP_ENTITIES"].ToString() == "<ALL>";
                        }
                    }
                }
				
				//If user has selected specific level like Region, Client etc.
				//MCIC Changes 01/25/08. Mihika-Deleted flag check in the queries below
				if (p_lEntityId != 0)
				{
					//sgoel6 | MITS 14875 | 03/27/2009
					//if (p_sLookUp != "")
					if (sSearch != "")
					//MITS 12112 Raman Bhatia 
					//List view results should also take into account OracleCaseInsensitive setting in utilities    
					{
						//look for case sensitiveness in case of Oracle database
						objConn = DbFactory.GetDbConnection(m_sConnectionString);
						objConn.Open();
						if (objConn.DatabaseType.ToString() == Constants.DB_ORACLE)
						{
							bCaseInSens = IsCaseInSensitveSearch();
						}
						objConn.Close();
						objConn.Dispose();
						if (!bCaseInSens)
						{
							//abansal23: on 05/14/2009 for Group Association
							if(p_lEntityId != 1012)
							sSQL = "SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=" + p_lEntityId + " AND DELETED_FLAG = 0  AND (ABBREVIATION " + sOperator + " '" + sSearch + "' OR LAST_NAME " + sOperator + " '" + sSearch + "')"; // ORDER BY ABBREVIATION";
							else
							sSQL = "SELECT A.*,B.ABBREVIATION,B.LAST_NAME,B.ENTITY_ID FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE B.ENTITY_TABLE_ID=" + p_lEntityId + " AND B.DELETED_FLAG = 0  AND (B.ABBREVIATION " + sOperator + " '" + sSearch + "' OR B.LAST_NAME " + sOperator + " '" + sSearch + "')"; // ORDER BY B.ABBREVIATION";
						}
						else
						{
							//abansal23: on 05/14/2009 for Group Association
							if (p_lEntityId != 1012)
							sSQL = "SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=" + p_lEntityId + " AND DELETED_FLAG = 0  AND (UPPER(ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";// ORDER BY ABBREVIATION";
							else
							sSQL = "SELECT A.*,B.ABBREVIATION,B.LAST_NAME,B.ENTITY_ID FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE B.ENTITY_TABLE_ID=" + p_lEntityId + " AND B.DELETED_FLAG = 0  AND (UPPER(B.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(B.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')";// ORDER BY B.ABBREVIATION";
						}
					}

					else
					{
						//abansal23: on 05/14/2009 for Group Association
						if (p_lEntityId != 1012)
							sSQL = "SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=" + p_lEntityId + " AND DELETED_FLAG = 0"; //ORDER BY ABBREVIATION";
						else
							sSQL = "SELECT A.*,B.ABBREVIATION,B.LAST_NAME,B.ENTITY_ID FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE B.ENTITY_TABLE_ID=" + p_lEntityId + " AND B.DELETED_FLAG = 0"; //ORDER BY B.ABBREVIATION";
					}
				}
				//If user has selected "ALL" option
				else
				{
					if (!bCaseInSens)
					{
						//abansal23: on 05/14/2009 for Group Association
						//sSQL = "SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND DELETED_FLAG = 0 AND (ABBREVIATION " + sOperator + " '" + sSearch + "' OR LAST_NAME " + sOperator + " '" + sSearch + "') ORDER BY ABBREVIATION";
						sSQL = "SELECT A.*,B.ABBREVIATION,B.LAST_NAME,B.ENTITY_ID FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE B.ENTITY_TABLE_ID=1012 AND B.DELETED_FLAG = 0 AND (B.ABBREVIATION " + sOperator + " '" + sSearch + "' OR B.LAST_NAME " + sOperator + " '" + sSearch + "')"; // ORDER BY B.ABBREVIATION";
					}
					else
					{
						//sSQL = "SELECT ENTITY_ID,ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID=1012 AND DELETED_FLAG = 0 AND (UPPER(ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "') ORDER BY ABBREVIATION";
						sSQL = "SELECT A.*,B.ABBREVIATION,B.LAST_NAME,B.ENTITY_ID FROM ORG_HIERARCHY A INNER JOIN ENTITY B ON A.DEPARTMENT_EID = B.ENTITY_ID WHERE B.ENTITY_TABLE_ID=1012 AND B.DELETED_FLAG = 0 AND (UPPER(B.ABBREVIATION) " + sOperator + " '" + sSearch.ToUpper() + "' OR UPPER(B.LAST_NAME) " + sOperator + " '" + sSearch.ToUpper() + "')"; // ORDER BY B.ABBREVIATION";
					}
				}

				//MITS 16771- Pankaj 5/29/09
				sSQL = sSQL + AddAdditionalFilter(p_sCity, p_lState, p_sZip, "LIST_VIEW", bCaseInSens);
				//added by Nitin on 06/05/2009 in order to merge with prev version
				if (sSQL.Contains("B.ABBREVIATION"))
				{
					//pmittal5 Confidential Record
                    if (IsBesEnabled && !bAdmin)
						sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
					//End
					sSQL = sSQL + " ORDER BY B.ABBREVIATION";
				}
				else
				{
					//pmittal5 Confidential Record
                    if (IsBesEnabled && !bAdmin)
						sSQL = sSQL + " AND ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
					//End
					sSQL = sSQL + " ORDER BY ABBREVIATION";
				}

				objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);

				if (objDataSet != null)
					sGetEntitySearch = objDataSet.GetXml();
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetEntitySearch.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDataSet != null) objDataSet.Dispose();
				if (objConn != null)
				{
					objConn.Dispose();
					objConn = null;
				}
			}
			return (sGetEntitySearch);
		}
		#endregion

		#region Generate Data
		/// <summary>
		/// Generate the data from the dataset for the corresponding module i.e. Operation, Region etc.
		/// </summary>
		/// <param name="p_objOrgInfo">XML DOM containing Organization Hierarchy information to be modified</param>
		/// <param name="p_sNodCat">Node Category</param>
		/// <param name="p_sSQL">SQL query</param>
		/// <param name="p_objConn">Connection object as temporary table gets generated for the same session</param>
		/// <returns>Generates & drops temporary tables and creates XML string for Organization Hierarchy</returns>
		private string GenerateData(XmlDocument p_objOrgInfo, string p_sNodCat, string p_sSQL, DbConnection p_objConn)
		{
			DataSet objDataSet = null;
			DbDataAdapter objAdap = null;
			int iResult = 0;
			string sRetVal = "";
			try
			{
				//Raman 07/31/2009: MITS 17393: We really do not need any temperory table as there is only 1 query. All we were doing earlier 
				//was putting results of that query in a temperory table and fetching them from it!!
				//Commenting all temperory table code
				
				
				//Create temporary table
				//iResult = CreateTable(p_sNodCat.ToUpper(), p_sSQL, p_objConn);

				//if temporary table is created
				//if (iResult == 1)
				//    p_sSQL = m_sSql;
				//if temporary table is not created


				//Fill the dataset
				objDataSet = new DataSet();
				objAdap = DbFactory.GetDataAdapter(p_objConn, p_sSQL);
				objAdap.Fill(objDataSet);

				//Drop temporary table
				//if (iResult == 1)
				//    iResult = DropTempTable("TEMPORG", p_objConn);

				//Get updated XML for the given node category
				sRetVal = GetXmlbyName(p_objOrgInfo, objDataSet, p_sNodCat);
			}

				//Riskmaster.Db is throwing following exception so need to catch the same
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GenerateData.GeneralError", m_iClientId), p_objExp);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDataSet != null) objDataSet.Dispose();
				objAdap = null;
			}
			return (sRetVal);
		}
		#endregion

		#region Make XML
		/// Name		: MakeXML
		/// Author		: Mihika Agrawal
		/// Date Created: 09/26/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded function which calls the main function
		/// </summary>
		/// <param name="p_objXml">XML DOM containing Org Hierarchy data</param>
		/// <param name="objDataSet">Data set from which Org Hierarchy information to be extracted</param>
		/// <param name="p_sNodcat">Node caregory</param>
		/// <returns>XML string containing Org Hierarchy data</returns>
		private string MakeXML(XmlDocument p_objXML, DataSet p_objDtSet, string p_sNodeCat)
		{
			return MakeXML(p_objXML, p_objDtSet, p_sNodeCat, "");
		}

		#endregion

		#region Overloaded Make XML
		/// Name		: MakeXML
		/// Author		: Mihika Agrawal
		/// Date Created: 09/26/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Sets the XML DOM element with the Org Hierarchy data
		/// </summary>
		/// <param name="p_objXml">XML DOM containing Org Hierarchy data</param>
		/// <param name="p_objDataSet">Data set from which Org Hierarchy information to be extracted</param>
		/// <param name="p_sNodcat">Node caregory</param>
		/// <param name="p_sTname">Table name</param>
		/// <returns>XML string containing Org Hierarchy data</returns>
		private string MakeXML(XmlDocument p_objXML, DataSet p_objDtSet, string p_sNodeCat, string p_sTableName)
		{
            //string sErrorCheckXML = string.Empty;
			string sRetXml = string.Empty;

            string[] arrEntTypeUp = { "CLIENT", "COMPANY", "OPERATION", "REGION", "DIVISION", "LOCATION", "FACILITY", "DEPARTMENT" };
            string[] arrEntTypeLw = { "client", "company", "operation", "region", "division", "location", "facility", "department" };

			string sExpr = string.Empty;
			string sSort = string.Empty;
            string sParentEid = string.Empty;

			DataRow[] foundRows = new DataRow[1];

			int iLevel = 0;
            //int iEnh = 0;
			int iNextLevel = 0;

            XmlElement objXMLParentEle = null;
			XmlElement objXMLChildEle = null;

            XmlElement objXMLSearchEle = null;
            //XmlElement objXMLErrorEle = null;

			try
			{
                //sErrorCheckXML = p_objXML.InnerXml;

				//Select  Organization Hierarchy node
				objXMLParentEle = (XmlElement)p_objXML.SelectSingleNode("//org[@name='Organization Hierarchy']");
				m_bIsError = false;

                //rsolanki2: Performance improvements
                objXMLSearchEle = (XmlElement)p_objXML.SelectSingleNode("//org[@name='Organization Hierarchy']");

				//if data is present in the dataset
				if (p_objDtSet.Tables[0].Rows.Count > 0)
				{
					iLevel = GetLevel(p_sNodeCat);
					sSort = "DISPLAY ASC";
					for (int iCnt = 0; iCnt < iLevel; iCnt++)
					{
						// where clause for the data table
						sExpr = "SYSTEM_TABLE_NAME = '" + arrEntTypeUp[iCnt] + "'";

                        //objXMLParentEle = (XmlElement)p_objXML.SelectSingleNode("/form/group/control[@name='orgTree']/org[@name='Organization Hierarchy']/client/company/operation/region/division/location/facility[@id='"

						foundRows = p_objDtSet.Tables[0].Select(sExpr, sSort, DataViewRowState.CurrentRows);
						foreach (DataRow row in foundRows)
						{
                            //if (iCnt != 0)
                            //    objXMLParentEle = (XmlElement)p_objXML.SelectSingleNode("//" 
                            //        + arrEntType[iCnt - 1].ToLower() 
                            //        + "[@id='" 
                            //        + row["PARENT_EID"].ToString() 
                            //        + "']");
                            
                            sParentEid = row["PARENT_EID"].ToString();

                            //rsolanki2 : Org Hierarchy Performance improvements 
                            // Optimizing xpath to search at the same org level instead of a Global "//" search.
                            if (iCnt == 7)
                            {
                                objXMLParentEle = (XmlElement)objXMLSearchEle.SelectSingleNode("./client/company/operation/region/division/location/facility[@id='"
                                     + sParentEid
                                     + "']");
                            }
                            else if (iCnt == 6)
                            {
                                objXMLParentEle = (XmlElement)objXMLSearchEle.SelectSingleNode("./client/company/operation/region/division/location[@id='"
                                     + sParentEid
                                     + "']");
                            }
                            else if (iCnt == 5)
                            {
                                objXMLParentEle = (XmlElement)objXMLSearchEle.SelectSingleNode("./client/company/operation/region/division[@id='"
                                     + sParentEid
                                     + "']");
                            }
                            else if (iCnt == 4)
                            {
                                objXMLParentEle = (XmlElement)objXMLSearchEle.SelectSingleNode("./client/company/operation/region[@id='"
                                     + sParentEid
                                     + "']");
                            }
                            else if (iCnt == 3)
                            {
                                objXMLParentEle = (XmlElement)objXMLSearchEle.SelectSingleNode("./client/company/operation[@id='"
                                     + sParentEid
                                     + "']");
                            }
                            else if (iCnt == 2)
                            {
                                objXMLParentEle = (XmlElement)objXMLSearchEle.SelectSingleNode("./client/company[@id='"
                                     + sParentEid
                                     + "']");
                            }
                            else if (iCnt == 1)
                            {
                                objXMLParentEle = (XmlElement)objXMLSearchEle.SelectSingleNode("./client[@id='"
                                     + sParentEid
                                     + "']");
                            }
                            

							if (objXMLParentEle != null)
							{
								objXMLChildEle = (XmlElement)p_objXML.ImportNode(GetNewElement(arrEntTypeLw[iCnt]), true);
								if (row["DISPLAY"].ToString() != "")
									objXMLChildEle.SetAttribute("name", row["DISPLAY"].ToString());
								else
									objXMLChildEle.SetAttribute("name", NO_ENTITY);
								// Start Naresh MITS 8458 Date 28/11/2006
								if (row["LastName"].ToString() != "")
									objXMLChildEle.SetAttribute("lastname", row["LastName"].ToString());
								else
									objXMLChildEle.SetAttribute("lastname", NO_ENTITY);
								// End Naresh MITS 8458 Date 28/11/2006
                                //objXMLChildEle.SetAttribute("parent", row["PARENT_EID"].ToString());
                                objXMLChildEle.SetAttribute("parent", sParentEid);

								if (iCnt == iLevel - 1)
								{
									objXMLChildEle.SetAttribute("ischild", "1");
									objXMLChildEle.SetAttribute("isSelect", "1");
								}
								else
									objXMLChildEle.SetAttribute("ischild", "0");

								if (iCnt != 7)
									objXMLChildEle.SetAttribute("child_name", arrEntTypeLw[iCnt + 1]);

								objXMLChildEle.SetAttribute("code", GetCode(arrEntTypeUp[iCnt]));
								objXMLChildEle.SetAttribute("id", row["ENTITY_ID"].ToString());
								iNextLevel = iCnt + 1;
								objXMLChildEle.SetAttribute("level", iNextLevel.ToString());

                                if (m_lSearchId != 0 && m_lSearchId == Conversion.ConvertObjToInt64(row["ENTITY_ID"], m_iClientId))
									objXMLChildEle.SetAttribute("iscurrent", "1");

								objXMLParentEle.AppendChild(objXMLChildEle);
							}
						}
					}
				}
				//if Org tree contains node more than 3500 then add result too large attribute
				/*Commented By Rahul on 03/01/2006. This code is never executed as this condition is checked in OpenThisLevel().*/
				//				else if (p_objDtSet.Tables[0].Rows.Count > 3500)
				//				{
				//					p_objXML.LoadXml(sErrorCheckXML);
				//					objXMLErrorEle = (XmlElement)p_objXML.SelectSingleNode("//org[@name='Organization Hierarchy']");
				//					objXMLChildEle = (XmlElement)p_objXML.ImportNode(GetNewElement("error"),true);
				//					objXMLChildEle.SetAttribute("id", "0");
				//					objXMLChildEle.SetAttribute("name", "norecord");
				//					//The following error text is as per the existing VB component. 
				//					//This error message might get updated in future.
				//					objXMLChildEle.SetAttribute("desc", Globalization.GetString("OrgHierarchy.TooManyItems"));
				//					objXMLChildEle.SetAttribute("value", "1");
				//					objXMLErrorEle.AppendChild(objXMLChildEle);
				//					m_bIsError = true;
				//					m_sNodCat = "C";
				//					sRetXml = OpenToThisLevel("C", m_lLevel, p_objXML,"",0);
				//					return (sRetXml);	
				//				}
				// if data not present in the dataset
				else
				{
					//MITS 16867 The following code causes endless loop, so comment out.
					//string sRetOrgInfo = "";
					//string sOrgInfo = "";
					//sOrgInfo = p_objXML.InnerXml;
					//sRetOrgInfo = GetOrgHierarchyXml(true, m_lLevel, 0, "", "", m_sTable, m_lRowId, sOrgInfo, out iEnh);
					//p_objXML.LoadXml(sRetOrgInfo);
					objXMLChildEle = (XmlElement)p_objXML.ImportNode(GetNewElement("error"), true);
					objXMLChildEle.SetAttribute("id", "0");
					objXMLChildEle.SetAttribute("name", "norecord");
					objXMLChildEle.SetAttribute("desc", "Search Not Found");
					objXMLChildEle.SetAttribute("value", "1");
					objXMLParentEle.AppendChild(objXMLChildEle);
				}
				sRetXml = p_objXML.InnerXml;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.MakeXML.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				objXMLParentEle = null;
				objXMLChildEle = null;
                //objXMLErrorEle = null;
                objXMLSearchEle = null;
			}
			return sRetXml;
		}
		#endregion

		#region Get Level
		/// Name		: GetLevel
		/// Author		: Mihika Agrawal
		/// Date Created: 09/26/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the level of the Org Hierarchy
		/// </summary>
		/// <param name="p_sNodCat">Level</param>
		/// <returns>Level depending upon the given parameter</returns>
		private static int GetLevel(string p_sNodCat)
		{
			switch (p_sNodCat.ToUpper())
			{
				case CLIENT:
					return 1;
				case COMPANY:
					return 2;
				case OPERATION:
					return 3;
				case REGION:
					return 4;
				case DIVISION:
					return 5;
				case LOCATION:
					return 6;
				case FACILITY:
					return 7;
				case DEPARTMENT:
					return 8;
				default:
					return 1;
			}
		}
		#endregion

		/// <summary>
		/// Get Level category code form level number
		/// </summary>
		/// <param name="p_iLevel"></param>
		/// <returns></returns>
		private string GetCatFromLevel(long p_iLevel)
		{
			switch (p_iLevel)
			{
				case 1:
					return CLIENT;
				case 2:
					return COMPANY;
				case 3:
					return OPERATION;
				case 4:
					return REGION;
				case 5:
					return DIVISION;
				case 6:
					return LOCATION;
				case 7:
					return FACILITY;
				case 8:
					return DEPARTMENT;
				default:
					return CLIENT;
			}
		}

		#region Get Code
		/// Name		: GetCode
		/// Author		: Mihika Agrawal
		/// Date Created: 09/26/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the code of the Org Hierarchy
		/// </summary>
		/// <param name="p_sNodCat">Level</param>
		/// <returns>Code depending upon the given parameter</returns>
		private static string GetCode(string p_sNodCat)
		{
			switch (p_sNodCat.ToUpper())
			{
                case "CLIENT":
                    return "C";
                case "COMPANY":
                    return "CO";
                case "OPERATION":
                    return "O";
                case "REGION":
                    return "R";
                case "DIVISION":
                    return "D";
                case "LOCATION":
                    return "L";
                case "FACILITY":
                    return "F";
                case "DEPARTMENT":
                    return "DT";
                default:
                    return "";
			}
		}
		#endregion

		//Changed by Gagan for MITS 8599 : Start

		# region Print Org Hierarchy Report


		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_bIsLandscape">Landscape or Portrait</param>
		/// <param name="p_Left">Left</param>
		/// <param name="p_Right">Right</param>
		/// <param name="p_Top">Top</param>
		/// <param name="p_Bottom">Bottom</param>
		private void StartDoc(bool p_bIsLandscape, int p_Left, int p_Right, int p_Top, int p_Bottom)
		{
			try
			{
				m_objPrintDoc = new C1PrintDocument();
				m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
				// m_objPrintDoc.PageSettings.PrinterSettings.PrinterName = RMConfigurator.Value("DefaultPrinterName").ToString();

				m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape;
				m_objPrintDoc.PageSettings.Margins.Left = p_Left;
				m_objPrintDoc.PageSettings.Margins.Right = p_Right;
				m_objPrintDoc.PageSettings.Margins.Top = p_Top;
				m_objPrintDoc.PageSettings.Margins.Bottom = p_Bottom;

				m_objPrintDoc.StartDoc();

				// Causes access to default printer     objRect = m_objPrintDoc.PageSettings.Bounds;
				m_dblPageHeight = 11 * 1440;
				m_dblPageWidth = 8.5 * 1440;
				if (p_bIsLandscape)
					m_dblPageHeight += 700;
				else
					m_dblPageWidth += 700;

				m_objFont = new Font("Arial", 10);
				m_objText = new RenderText(m_objPrintDoc);
				m_objText.Style.Font = m_objFont;
				m_objText.Style.WordWrap = false;
				m_objText.Width = m_dblPageWidth;
			}
			catch (RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.StartDoc.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}


		/// <summary>
		/// Set font bold
		/// </summary>
		/// <param name="p_bBold">Bool flag</param>
		private void SetFontBold(bool p_bBold)
		{
			try
			{
				if (p_bBold)
					m_objFont = new Font(m_objFont.Name, m_objFont.Size, FontStyle.Bold);
				else
					m_objFont = new Font(m_objFont.Name, m_objFont.Size);

				m_objText.Style.Font = m_objFont;

			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.SetFontBold.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}

		}



		/// <summary>
		/// Get text width.
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Width</returns>
		private double GetTextWidth(string p_sText)
		{
			try
			{
				m_objText.Text = p_sText + "";
				return (m_objText.BoundWidth);
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetTextWidth.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}

		/// <summary>
		/// Get Text Height
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Height</returns>
		private double GetTextHeight(string p_sText)
		{
			try
			{
				m_objText.Text = p_sText + "";
				return (m_objText.BoundHeight);
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.GetTextHeight.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}




		/// <summary>
		/// Creates a new page in a report
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		/// <param name="p_bFirstPage">First page or not</param>		
		private void CreateNewPage(string p_sDSN, bool p_bFirstPage)
		{
			SysParms objSysSetting = null;

			RenderText m_objText = null;
			try
			{
				if (!p_bFirstPage)
				{
					m_objPrintDoc.NewPage();
				}
				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 11700);

				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 500, Color.Black, 1, Color.LightGray);
				m_objPrintDoc.RenderDirectRectangle(0, 11300, 15600, 11900, Color.Black, 1, Color.LightGray);
				m_objText = new RenderText(m_objPrintDoc);
				m_objText.Text = " ";
				CurrentX = m_objText.BoundWidth;


                objSysSetting = new SysParms(p_sDSN, m_iClientId);
				m_objText.Text = " Organizational Hierarchy" + "                                                    " + objSysSetting.SysSettings.ClientName +
					"                                                    " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "      ";
				objSysSetting = null;
				CurrentX = 0; //(ClientWidth - m_objText.BoundWidth);

				m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left);
				m_objText = new RenderText(m_objPrintDoc);

                m_objText.Text = Globalization.GetString("OrgHierarchy.CreateReport.Page", m_iClientId) + " [@@PageNo@@]";//sharishkumar Rmacloud
				CurrentX = (PageWidth - m_objText.BoundWidth) / 2;
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center);

                m_objText.Text = String.Format("{0} - {1}  ", Globalization.GetString("OrgHierarchy.CreateOrgHierarchyReport.ConfidentialData", m_iClientId), m_MessageSettings["DEF_RMCAPTION"]);//sharishkumar Rmacloud
				CurrentX = (ClientWidth - m_objText.BoundWidth);
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold | FontStyle.Italic), Color.Black, AlignHorzEnum.Right);
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateReport.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
			finally
			{
				objSysSetting = null;
				m_objText = null;
			}
		}


		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintText(string p_sPrintText)
		{
			try
			{
				m_objText.Text = p_sPrintText + "\r";
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.PrintText.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}


		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintTextNoCr(string p_sPrintText)
		{
			try
			{
				m_objText.Text = p_sPrintText + "";
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.PrintTextNoCr.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}


		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		private void Save(string p_sPath)
		{
			try
			{
				m_objPrintDoc.ExportToPDF(p_sPath, false);
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.Save.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}


		/// <summary>
		/// Creates a record in a report
		/// </summary>
		/// <param name="p_objData">DataRow object containing a row data</param>
		/// <param name="p_dblCurrentX">Current X position</param>
		/// <param name="p_dblCurrentY">Current Y position</param>
		/// <param name="p_arrColAlign">Columns alignment</param>
		/// <param name="p_dblLeftMargin">Left margin</param>
		/// <param name="p_dblLineHeight">Line height</param>
		/// <param name="p_arrColWidth">Column width</param>
		/// <param name="p_dblCharWidth">Character width</param>
		/// <param name="p_bLeadingLine">Leading Line Flag</param>
		/// <param name="p_bCalTotal">Calculate Total Flag</param>
		/// <param name="p_dblTotalAll">Over all Total</param>
		/// <param name="p_dblTotalPay">Total Payments</param>
		/// <param name="p_dblTotalCollect">Total Collections</param>
		/// <param name="p_dblTotalVoid">Total Voids</param>
		private void CreateRow(int iCounter, double p_dblCurrentX,
			ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
			double[] p_arrColWidth, double p_dblCharWidth)
		{
			string sData = "";
			int level = 0;
            StringBuilder sb = new StringBuilder();

			try
			{
				CurrentX = p_dblCurrentX;
				CurrentY = p_dblCurrentY;

				level = int.Parse(arrListLevel[iCounter].ToString());

				while (level != 0)
				{
                    //sData = sData + "    ";
                    sb.Append(sData).Append("    ");
					level--;
				}

                //sData += arrListName[iCounter];
                sb.Append( arrListName[iCounter]);

                //PrintText(sData);
                PrintText(sb.ToString());

				p_dblCurrentX = p_dblLeftMargin;
				p_dblCurrentY += p_dblLineHeight;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateRow.Error", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
		}


		/// <summary>
		/// Call the end event.
		/// </summary>
		private void EndDoc()
		{
			try
			{
				m_objPrintDoc.EndDoc();
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.EndDoc.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}


		/// <summary>
		/// Creates Report Footer
		/// </summary>
		/// <param name="p_dCurrentX">X Position</param>
		/// <param name="p_dCurrentY">Y Position</param>
		/// <param name="p_dHeight">Height</param>
		/// <param name="p_dblSpace">Space between 2 words</param>
		private void CreateFooter(double p_dCurrentX, double p_dCurrentY, double p_dHeight, double p_dblSpace)
		{
			try
			{
				m_objText.Style.Font = new Font("Arial", 7f, FontStyle.Underline | FontStyle.Bold);
				m_objText.Text = m_MessageSettings["DEF_NOTE"].ToString();
				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objPrintDoc.RenderDirect(p_dCurrentX, p_dCurrentY, m_objText);

				m_objText.Style.Font = new Font("Arial", 7f, FontStyle.Bold);
				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_RMPROPRIETARY"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_NAME"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_RMCOPYRIGHT"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateFooter.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
		}




		/// <summary>
		/// 
		/// </summary>
		/// <param name="p_objXmlDoc"></param>
		/// <param name="p_objMemory"></param>
		/// <returns></returns>
		public int CreateOrgHierarchyReport(XmlDocument p_objXmlDoc, out MemoryStream p_objMemory)
		{
			#region local variables
			double dblCharWidth = 0.0;
			double dblLineHeight = 0.0;
			double dblLeftMargin = 10.0;
			double dblRightMargin = 10.0;
			double dblTopMargin = 10.0;
			double dblPageWidth = 0.0;
			double dblPageHeight = 0.0;
			double dblBottomHeader = 0.0;
			double dblCurrentY = 0.0;
			double dblCurrentX = 0.0;
			double iCounter = 0.0;
			double[] arrColWidth = null;
			int iReturnValue = 0;

			string sHeader = string.Empty;
			string sFile = string.Empty;
			string sSQL = string.Empty;
			string sNumChecks = string.Empty;
			string[] arrColAlign = null;
			string[] arrCol = null;
			int iEntityEnh = 0;
			XmlNode objNode = null;
			string sInputXML = "";
			#endregion
			p_objMemory = null;

			try
			{                
                //Obtain the names and levels from org hierarchy XML				
				arrListName = new ArrayList();
				arrListLevel = new ArrayList();

				//Raman 10/13/2008: If XML hasn't arrived from UI which would be the case in R5 then we should get XML from Database
				if (p_objXmlDoc.SelectSingleNode("//InputXml") != null)
				{
					sInputXML = p_objXmlDoc.SelectSingleNode("//InputXml").InnerXml;
					p_objXmlDoc.LoadXml(GetOrgHierarchyXml(true, 0, 0, "", "", "", 0, sInputXML, out iEntityEnh));
				}

				objNode = p_objXmlDoc.SelectSingleNode("//org");
				RecursiveRead(objNode);

				arrColAlign = new string[1];
				arrColWidth = new double[1];
				arrCol = new string[1];

				StartDoc(true, 10, 10, 10, 10);
				SetFontBold(true);
				ClientWidth = 15400;

				dblCharWidth = GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ") / 40;
				dblLineHeight = GetTextHeight("W");
				dblLeftMargin = dblCharWidth * 2;
				dblRightMargin = dblCharWidth * 2;

				dblTopMargin = dblLineHeight * 5;
				dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin;
				dblPageHeight = PageHeight;
				dblBottomHeader = dblPageHeight - (5 * dblLineHeight);


				arrColWidth[0] = dblCharWidth * 20;// Org Hierarachy


				dblCurrentX = dblLeftMargin;
				dblCurrentY = 900;

				CreateNewPage(m_sConnectionString, true);

				arrColAlign[0] = LEFT_ALIGN; //Org Hierarchy

				CurrentX = dblLeftMargin;
				CurrentY = dblCurrentY;

				SetFontBold(false);
				dblCurrentY = 1200;
				iCounter = 0;

				for (int i = 0; i < arrListName.Count; i++)
				{
					CreateRow(i, dblLeftMargin, ref dblCurrentY, arrColAlign,
						dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth);
					iCounter++;
					if (iCounter == NO_OF_ROWS)
					{
						CreateNewPage(m_sConnectionString, false);
						iCounter = 0;
						dblCurrentY = 900;
					}
				}

				// Printing the Summary
				if ((iCounter + 3) > NO_OF_ROWS)
				{
					CreateNewPage(m_sConnectionString, false);
					dblCurrentY = 900;
					iCounter = 0;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight;
				}

				SetFontBold(true);
				CurrentX = dblLeftMargin;
				CurrentY = dblCurrentY;

				// Printing the Footer
				if ((iCounter + 7) > NO_OF_ROWS)
				{
					CreateNewPage(m_sConnectionString, false);
					dblCurrentY = 900;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight * 2;
				}

				CreateFooter(dblLeftMargin, dblCurrentY, dblLineHeight, 250);
				EndDoc();
				sFile = TempFile;
				Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);

				File.Delete(sFile);
				iReturnValue = 1;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateReport.Error", m_iClientId), p_objException);
			}
			finally
			{
				arrColAlign = null;
				arrColWidth = null;
				m_objPrintDoc = null;
				m_objFont = null;
				m_objText = null;
			}
			return iReturnValue;
		}

		/// <summary>
		/// Raman Bhatia: Gets the children for a particular node.
		/// </summary>
		/// <param name="p_objDocIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut"></param>
		/// <returns></returns>
		public XmlDocument GetChildNodes(XmlDocument p_objDocIn)
		{
			string sSQL = string.Empty;
            string sSqlAdmin = string.Empty;
			string sCodeID = "";
			string sChildCodeID = "";
			int iFunctionID = 0;
			string sFunctionIDs = string.Empty;
			string sFunctionNames = string.Empty;
			string sFunctionName = string.Empty;
			XmlDocument p_objXmlOut = null;
			Dictionary<int, string> dicFunctionIDs = new Dictionary<int, string>();
			Dictionary<int, string> dicFunctionNames = new Dictionary<int, string>();
			int iParentID = 0;
			int iMaximum_Children_Per_Iteration = 0;
            bool bAdmin = false;
			try
			{  
				iMaximum_Children_Per_Iteration = Conversion.ConvertStrToInteger(m_OrgHierarchySettings["Maximum_Children_Per_Iteration"].ToString());
				XmlNode oParentId = p_objDocIn.SelectSingleNode("//parentFuncId");
				if (oParentId != null)
				{
					string sParentID = oParentId.InnerText;
					if (string.IsNullOrEmpty(sParentID))
						iParentID = 0;
					else
						iParentID = int.Parse(sParentID);
				}
				XmlNode oCodeId = p_objDocIn.SelectSingleNode("//codeid");
				if (oCodeId != null)
				{
					sCodeID = oCodeId.InnerText;
				}
				XmlNode ochkFilter = p_objDocIn.SelectSingleNode("//chkFilter");
				if (ochkFilter != null)
				{
					m_sFilter = ochkFilter.InnerText;
				}
				XmlNode objNod = p_objDocIn.SelectSingleNode("//tablename");
				if (objNod != null)
					m_sTable = p_objDocIn.SelectSingleNode("//tablename").InnerText;
				objNod = p_objDocIn.SelectSingleNode("//RowId");
				if (objNod != null)
					m_lRowId = Conversion.ConvertStrToLong(p_objDocIn.SelectSingleNode("//RowId").InnerText);
				//Start by Shivendu to get the evetdate and claimdate for MITS 17249
				objNod = p_objDocIn.SelectSingleNode("//ClaimDate");
				if (objNod != null)
					m_sClaimDate = p_objDocIn.SelectSingleNode("//ClaimDate").InnerText;
				objNod = p_objDocIn.SelectSingleNode("//EventDate");
				if (objNod != null)
					m_sEventDate = p_objDocIn.SelectSingleNode("//EventDate").InnerText;
				objNod = p_objDocIn.SelectSingleNode("//PolicyDate");
				if (objNod != null)
					m_sPolicyDate = p_objDocIn.SelectSingleNode("//PolicyDate").InnerText;
				//End by Shivendu to get the evetdate and claimdate for MITS 17249
				SetEffectiveDate(m_sTable, m_lRowId);

				p_objDocIn = RemoveSelection(p_objDocIn);

                if (IsBesEnabled)
                {
                    sSqlAdmin = "SELECT GROUP_ENTITIES FROM ORG_SECURITY INNER JOIN GROUP_MAP ON ORG_SECURITY.GROUP_ID = GROUP_MAP.SUPER_GROUP_ID INNER JOIN ORG_SECURITY_USER ON GROUP_MAP.GROUP_ID = ORG_SECURITY_USER.GROUP_ID WHERE USER_ID = " + m_iUserId;
                  
                    using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSqlAdmin))
                    {
                        while (oDbReader.Read())
                        {
                            bAdmin = oDbReader["GROUP_ENTITIES"].ToString() == "<ALL>";
                        }
                    }
                }

				//if user has clicked on one of the org tree node
				if (iParentID != 0)
				{
					switch (sCodeID)
					{
						//fetch client information
						case CLIENT:
							//							objBuildSQL.Append ("SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID FROM ORG_HIERARCHY A ," + 
							//								"ENTITY C,ENTITY B WHERE A.CLIENT_EID=C.ENTITY_ID" +
							//								" AND A.COMPANY_EID= B.ENTITY_ID AND B.DELETED_FLAG=0 AND A.CLIENT_EID=" + p_lNode);

							//sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
							//                    + "FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.COMPANY_EID AND A.CLIENT_EID=" + iParentID
							//+"WHERE B.PARENT_EID=" + iParentID
							//+ " AND B.DELETED_FLAG=0"; 

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
									+ "FROM  ENTITY B WHERE B.PARENT_EID= " + iParentID
									+ " AND B.DELETED_FLAG=0";

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								sSQL = sSQL + GetTriggerSQL("B", m_sTable);
							}

							//End MITS 8733
							//sSQL = sSQL + " ORDER BY B.LAST_NAME"; //pmittal5 Mits 19292 - Sort on the basis of Abbreviation
							//pmittal5 Confidential Record
                                if(IsBesEnabled && !bAdmin)
								    sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSQL = sSQL + " ORDER BY B.ABBREVIATION";
							sChildCodeID = COMPANY;
							break;

						//fetch company information
						case COMPANY:
							//sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
							//        + "FROM ENTITY B LEFT JOIN  ORG_HIERARCHY A ON B.ENTITY_ID = A.OPERATION_EID AND A.COMPANY_EID=" + iParentID
							//+"WHERE B.PARENT_EID=" + iParentID + " AND B.DELETED_FLAG=0";

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
									+ "FROM  ENTITY B WHERE B.PARENT_EID= " + iParentID
									+ " AND B.DELETED_FLAG=0";

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								sSQL = sSQL + GetTriggerSQL("B", m_sTable);
							}

							//End MITS 8733
							//sSQL = sSQL + " ORDER BY B.LAST_NAME"; //pmittal5 Mits 19292 - Sort on the basis of Abbreviation
							//pmittal5 Confidential Record
                            if (IsBesEnabled && !bAdmin)
								sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSQL = sSQL + " ORDER BY B.ABBREVIATION";
							sChildCodeID = OPERATION;
							break;

						//                        //fetch operation information
						case OPERATION:
							//sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
							//    + "FROM  ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.REGION_EID AND A.OPERATION_EID=" + iParentID
							//    + "WHERE B.PARENT_EID=" + iParentID + " AND B.DELETED_FLAG=0" ;

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
									+ "FROM  ENTITY B WHERE B.PARENT_EID= " + iParentID
									+ " AND B.DELETED_FLAG=0";

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								sSQL = sSQL + GetTriggerSQL("B", m_sTable);
							}

							//End MITS 8733

							//pmittal5 Confidential Record
							if (IsBesEnabled && !bAdmin)
								sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End

							//sSQL += " ORDER BY B.LAST_NAME";  //pmittal5 Mits 19292 - Sort on the basis of Abbreviation
							sSQL += " ORDER BY B.ABBREVIATION";
							sChildCodeID = REGION;
							break;

						//                        //fetch region information
						case REGION:
							//sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
							//        + "FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.DIVISION_EID AND A.REGION_EID=" + iParentID
							//+ "WHERE B.PARENT_EID=" + iParentID + " AND B.DELETED_FLAG=0"; 

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
									+ "FROM  ENTITY B WHERE B.PARENT_EID= " + iParentID
									+ " AND B.DELETED_FLAG=0";

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								sSQL = sSQL + GetTriggerSQL("B", m_sTable);
							}
							//End MITS 8733

							//pmittal5 Confidential Record
							if (IsBesEnabled && !bAdmin)
								sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSQL += " ORDER BY B.ABBREVIATION";
							sChildCodeID = DIVISION;
							break;

						//                        //fetch division information
						case DIVISION:
							//sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
							//    + "FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.LOCATION_EID AND A.DIVISION_EID=" + iParentID
							//+"WHERE B.PARENT_EID=" + iParentID + " AND B.DELETED_FLAG=0";

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
									+ "FROM  ENTITY B WHERE B.PARENT_EID= " + iParentID
									+ " AND B.DELETED_FLAG=0";

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								sSQL = sSQL + GetTriggerSQL("B", m_sTable);
							}
							//End MITS 8733

							//pmittal5 Confidential Record
							if (IsBesEnabled && !bAdmin)
								sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSQL += " ORDER BY B.ABBREVIATION";

							sChildCodeID = LOCATION;
							break;
						//fetch location information
						case LOCATION:
							//sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
							//    + " FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.FACILITY_EID AND A.LOCATION_EID=" + iParentID
							//+"WHERE B.PARENT_EID=" + iParentID + " AND B.DELETED_FLAG=0"; 

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
									+ "FROM  ENTITY B WHERE B.PARENT_EID= " + iParentID
									+ " AND B.DELETED_FLAG=0";

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								sSQL = sSQL + GetTriggerSQL("B", m_sTable);
							}
							//End MITS 8733

							//pmittal5 Confidential Record
							if (IsBesEnabled && !bAdmin)
								sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSQL += " ORDER BY B.ABBREVIATION";

							sChildCodeID = FACILITY;
							break;

						//fetch facility information
						case FACILITY:
							//sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
							//    + "FROM ENTITY B LEFT JOIN ORG_HIERARCHY A ON B.ENTITY_ID = A.DEPARTMENT_EID AND A.FACILITY_EID=" + iParentID
							//    + "WHERE B.PARENT_EID=" + iParentID + " AND B.DELETED_FLAG=0"; 

							//PJS MITS 11728 03-05-08 - modified above query built by looping entity table
							sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID "
									+ "FROM  ENTITY B WHERE B.PARENT_EID= " + iParentID
									+ " AND B.DELETED_FLAG=0";

							//Mukul 4/18/07 Added MITS 8733
							if (m_sFilter == "1")
							{
								sSQL = sSQL + GetTriggerSQL("B", m_sTable);
							}
							//End MITS 8733

							//pmittal5 Confidential Record
							if (IsBesEnabled && !bAdmin)
								sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
							//End
							sSQL += " ORDER BY B.ABBREVIATION";
							break;
					}

				}
				else
				{
					sSQL = "SELECT DISTINCT B.LAST_NAME,B.ABBREVIATION,B.ENTITY_ID FROM ENTITY B WHERE B.DELETED_FLAG=0 AND B.ENTITY_TABLE_ID=1005";

					//Mukul 4/18/07 Added MITS 8733
					if (m_sFilter == "1")
					{
						sSQL = sSQL + GetTriggerSQL("B", m_sTable);
					}

					//End MITS 8733

					//pmittal5 Confidential Record
					if (IsBesEnabled && !bAdmin)
						sSQL = sSQL + " AND B.ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_MAP,ORG_SECURITY_USER,GROUP_MAP WHERE ORG_SECURITY_USER.GROUP_ID=GROUP_MAP.GROUP_ID AND GROUP_MAP.SUPER_GROUP_ID = ENTITY_MAP.GROUP_ID AND ORG_SECURITY_USER.USER_ID= " + m_iUserId + ")";
					//End
					sSQL += " ORDER BY B.ABBREVIATION";
					sChildCodeID = CLIENT;
				}

				using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
				{
					while (oDbReader.Read())
					{
						if (sFunctionIDs.Length > 0)
							sFunctionIDs += ",";
						iFunctionID = oDbReader.GetInt(2);

						sFunctionIDs += iFunctionID.ToString();
						if (oDbReader.GetValue(1) != DBNull.Value)
							sFunctionName = oDbReader.GetString(1) + '-' + oDbReader.GetString(0);
						else
							sFunctionName = NO_ENTITY;
						int iCount = 0;
						if (sCodeID == FACILITY)
							iCount = 0;
						else
							iCount = 1;
						int a = GetLevel(sCodeID);
						dicFunctionIDs.Add(iFunctionID, iFunctionID.ToString() + m_sInsideIdDelimiter + iCount.ToString() + m_sInsideIdDelimiter + sChildCodeID);
						dicFunctionNames.Add(iFunctionID, sFunctionName);
					}
				}

				/*
				//check if the child function permission is set
				sSQL = "SELECT FUNC_ID FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + sGroupID +
					" AND FUNC_ID IN (" + sFunctionIDs + ")";
				using (DbReader oDbReader = DbFactory.GetDbReader(sRMConnectionString, sSQL))
				{
					while (oDbReader.Read())
					{
						iFunctionID = oDbReader.GetInt("FUNC_ID");
						dicFunctionIDs[iFunctionID] += m_sInsideIdDelimiter + "1";
					}
				}
				*/
				sFunctionIDs = string.Empty;
				sFunctionNames = string.Empty;

				if (dicFunctionIDs.Count < iMaximum_Children_Per_Iteration)
				{
					foreach (KeyValuePair<int, string> kvp in dicFunctionIDs)
					{
						sFunctionIDs += m_sFuncIdDelimiter + kvp.Value;
						sFunctionNames += m_sNameDelimiter + dicFunctionNames[kvp.Key];
					}
					if (sFunctionIDs.Length > 0)
						sFunctionIDs = sFunctionIDs.Substring(m_sFuncIdDelimiter.Length);
					if (sFunctionNames.Length > 0)
						sFunctionNames = sFunctionNames.Substring(m_sNameDelimiter.Length);

					XmlNode oRetNode = p_objDocIn.SelectSingleNode("//RetVal");
					if (oRetNode != null)
					{
						if (sFunctionIDs.Length == 0)
							oRetNode.InnerText = string.Empty;
						else
						{
							//if(sFunctionNames.Contains("&")) sFunctionNames = sFunctionNames.Replace('&' , ' ');
							oRetNode.InnerText = sFunctionIDs + m_sNameDelimiter + m_sNameDelimiter + sFunctionNames;
						}
					}
				}
				else
				{
					XmlNode oNode = p_objDocIn.SelectSingleNode("//childnodecounter");
					if (oNode != null && oNode.InnerText != String.Empty)
					{
						int iCounterBegin = (Conversion.ConvertStrToInteger(oNode.InnerText) - 1) * iMaximum_Children_Per_Iteration;
						int iCounterEnd = (Conversion.ConvertStrToInteger(oNode.InnerText)) * iMaximum_Children_Per_Iteration;

						if (dicFunctionIDs.Count < iCounterEnd)
						{
							iCounterEnd = dicFunctionIDs.Count;
						}
						//int iCounter = 1;
						int[] iCurrentKeySet = new int[dicFunctionIDs.Count];
						dicFunctionIDs.Keys.CopyTo(iCurrentKeySet , 0);
						for (int i = iCounterBegin; i < iCounterEnd; i++)
						{
							sFunctionIDs += m_sFuncIdDelimiter + dicFunctionIDs[iCurrentKeySet[i]];
							sFunctionNames += m_sNameDelimiter + dicFunctionNames[iCurrentKeySet[i]];
						}

						//foreach (KeyValuePair<int, string> kvp in dicFunctionIDs)
						//{
						//    if (iCounter > iCounterBegin && iCounter <= iCounterEnd)
						//    {
						//        sFunctionIDs += m_sFuncIdDelimiter + kvp.Value;
						//        sFunctionNames += m_sNameDelimiter + dicFunctionNames[kvp.Key];
						//    }
						//    iCounter = iCounter + 1;
						//}
						if (sFunctionIDs.Length > 0)
							sFunctionIDs = sFunctionIDs.Substring(m_sFuncIdDelimiter.Length);
						if (sFunctionNames.Length > 0)
							sFunctionNames = sFunctionNames.Substring(m_sNameDelimiter.Length);

						XmlNode oRetNode = p_objDocIn.SelectSingleNode("//RetVal");
						if (oRetNode != null)
						{
							if (sFunctionIDs.Length == 0)
								oRetNode.InnerText = string.Empty;
							else
							{
								//if(sFunctionNames.Contains("&")) sFunctionNames = sFunctionNames.Replace('&' , ' ');
								oRetNode.InnerText = sFunctionIDs + m_sNameDelimiter + m_sNameDelimiter + sFunctionNames;
							}
						}

					}
					else
					{
						XmlNode oRetNode = p_objDocIn.SelectSingleNode("//RetVal");
						if (oRetNode != null)
						{
							((XmlElement)oRetNode).SetAttribute("count", ((dicFunctionIDs.Count) / iMaximum_Children_Per_Iteration).ToString());
						}
					}
				}
				p_objXmlOut = p_objDocIn;
			}
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("OrgHierarchy.GetChildNodes.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{

			}
			return p_objXmlOut;
		}
		public void AppendAllChildNodes(ref XmlDocument p_objXML, XmlElement p_objParentEle, XmlElement p_objChildEle, bool p_bPopulated)
		{
			//If the child is first child of the parent then put it inside a fresh "ul" tag  else put it under the same "ul" tag
			XmlElement objTempEle = null;
			if (p_objParentEle.ChildNodes.Count >= 2)
			{
				if (p_objParentEle.FirstChild.NodeType == XmlNodeType.Element)
				{
					p_objParentEle.FirstChild.AppendChild(p_objChildEle);
				}
				else
				{
					p_objParentEle.ChildNodes[1].AppendChild(p_objChildEle);
				}
			}
			else
			{
				objTempEle = p_objXML.CreateElement("ul");
				objTempEle.AppendChild(p_objChildEle);
				p_objParentEle.AppendChild(objTempEle);
			}

			//If the child is of lowest level then we need to place a "ul" tag for "+" sign

			if ((p_bPopulated == false) && (m_sTempNodCat != DEPARTMENT))
			{
				objTempEle = p_objXML.CreateElement("ul");
				p_objChildEle.AppendChild(objTempEle);
			}
			if (m_sTempNodCat == DEPARTMENT)
			{
				p_objChildEle.SetAttribute("populated", "true");
			}
			else
			{
				p_objChildEle.SetAttribute("populated", p_bPopulated.ToString().ToLower());
			}
		}


		/// <summary>
		/// Recursively Read xml nodes
		/// </summary>
		/// <param name="objNode"></param>
		public void RecursiveRead(XmlNode objNode)
		{
			XmlNodeList objNodeList;
			int iLevel;
			string sName;

			try
			{
				objNodeList = objNode.ChildNodes;
				foreach (XmlNode objTmpNode in objNodeList)
				{
					if (objTmpNode.Attributes == null)
					{
						continue;
					}
					arrListName.Add(objTmpNode.Attributes["name"].Value);
					sName = objTmpNode.Name;
					iLevel = 1;

					switch (sName)
					{
						case CLIENT_NAME: iLevel = 1;
							break;
						case COMPANY_NAME: iLevel = 2;
							break;
						case OPERATION_NAME: iLevel = 3;
							break;
						case REGION_NAME: iLevel = 4;
							break;
						case DIVISION_NAME: iLevel = 5;
							break;
						case LOCATION_NAME: iLevel = 6;
							break;
						case FACILITY_NAME: iLevel = 7;
							break;
						case DEPARTMENT_NAME: iLevel = 8;
							break;
						default: iLevel = 1;
							break;
					}

					//arrListLevel.Add(objTmpNode.Attributes["level"].Value);         
					arrListLevel.Add(iLevel);

					if (objTmpNode.HasChildNodes)
						RecursiveRead(objTmpNode);
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateReport.Error", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				objNodeList = null;
			}
		}

		# endregion

		//Changed by Gagan for MITS 8599 : End

		#region Self Insured Methods
		/// <summary>
		/// Gets SelfInsured Data
		/// </summary>
		/// <param name="p_iEntityId"></param>
		/// <param name="p_sSelfInsuredInfo"></param>
		/// <param name="p_sUserName"></param>
		/// <param name="p_sPassword"></param>
		/// <param name="p_sDsnName"></param>
		public XmlDocument GetEntityXSelfInsuredData(int p_iEntityId, string p_sSelfInsuredInfo, string p_sUserName, string p_sPassword, string p_sDsnName)
		{
			DataModelFactory objDMF = null;
			Entity objEntity = null;
			XmlDocument objXML = null;
			int iLevel = 0;
			string sAbbr = "";
			string sName = "";
			LocalCache objCache = null;
			try
			{
				objCache = new LocalCache(m_sConnectionString,m_iClientId);
				objXML = new XmlDocument();
				objXML.LoadXml(p_sSelfInsuredInfo);
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);//sharishkumar Rmacloud
				objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
				if (p_iEntityId > 0)
					objEntity.MoveTo(p_iEntityId);
				else
					throw new RMAppException(Globalization.GetString
						("OrgHierarchy.EditCompanyInformation.InvalidId",m_iClientId));//sharishkumar Rmacloud

				iLevel = 0;
				foreach (EntityXSelfInsured objEntityXSelfInsured in objEntity.EntityXSelfInsuredList)
				{

					if (objEntityXSelfInsured.DeletedFlag.ToString().ToLower() == "true")
					{
						continue;
					}
					CreateXML(objXML, "SelfInsured", "", "SelfInsuredInfo", iLevel);
					CreateXML(objXML, "SIRowId", Conversion.ConvertObjToStr(objEntityXSelfInsured.SIRowId), "SelfInsured", iLevel);
					CreateXML(objXML, "EntityId", Conversion.ConvertObjToStr(objEntityXSelfInsured.EntityId), "SelfInsured", iLevel);
					objCache.GetStateInfo(objEntityXSelfInsured.Jurisdiction, ref sAbbr, ref sName);
					CreateXML(objXML, "Jurisdiction", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.Jurisdiction);
					CreateXML(objXML, "DeletedFlag", objEntityXSelfInsured.DeletedFlag.ToString(), "SelfInsured", iLevel);
					CreateXML(objXML, "LegalEntityId", objEntityXSelfInsured.LegalEntityId.ToString(), "SelfInsured", iLevel);
					//Changed by:Mohit Yadav for Bug No. 000078***************************************
					//CreateXML(objXML, "CertNameEid", objCache.GetEntityLastFirstName(int.Parse(objEntityXSelfInsured.CertNameEid.ToString())), "SelfInsured", iLevel, objEntityXSelfInsured.CertNameEid);

					CreateXML(objXML, "CertificateNumber", objEntityXSelfInsured.CertificateNumber, "SelfInsured", iLevel);
					CreateXML(objXML, "EffectiveDate", Conversion.GetDBDateFormat
						(objEntityXSelfInsured.EffectiveDate, "d"), "SelfInsured", iLevel);
					CreateXML(objXML, "ExpirationDate", Conversion.GetDBDateFormat(objEntityXSelfInsured.ExpirationDate, "d"), "SelfInsured", iLevel);
					objCache.GetCodeInfo(objEntityXSelfInsured.Authorization, ref sAbbr, ref sName);
					CreateXML(objXML, "Authorization", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.Authorization);
					objCache.GetCodeInfo(objEntityXSelfInsured.Organization, ref sAbbr, ref sName);
					CreateXML(objXML, "Organization", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.Organization);
					CreateXML(objXML, "DttmRcdAdded", Conversion.GetDBDateFormat
						(objEntityXSelfInsured.DttmRcdAdded, "d"), "SelfInsured", iLevel);
					CreateXML(objXML, "DttmRcdLastUpd", Conversion.GetDBDateFormat
						(objEntityXSelfInsured.DttmRcdLastUpd, "d"), "SelfInsured", iLevel);
					CreateXML(objXML, "UpdatedByUser", objEntityXSelfInsured.UpdatedByUser, "SelfInsured", iLevel);
					CreateXML(objXML, "AddedByUser", objEntityXSelfInsured.AddedByUser, "SelfInsured", iLevel);
					//sgoel6 Medicare 05/13/2009
					//objCache.GetCodeInfo(objEntityXSelfInsured.LineOfBusCode, ref sAbbr, ref sName);
					//if (objEntityXSelfInsured.LineOfBusCode == -3)
					//    CreateXML(objXML, "LineOfBusCodeSelfInsured", "All", "SelfInsured", iLevel, objEntityXSelfInsured.LineOfBusCode);
					//else
					//    CreateXML(objXML, "LineOfBusCodeSelfInsured", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.LineOfBusCode);
					objCache.GetCodeInfo(objEntityXSelfInsured.LineOfBusCode, ref sAbbr, ref sName);
					if (objEntityXSelfInsured.LineOfBusCode == -3)
						CreateXML(objXML, "LobCode", "All", "SelfInsured", iLevel, objEntityXSelfInsured.LineOfBusCode);
					else
						CreateXML(objXML, "LobCode", sAbbr + " " + sName, "SelfInsured", iLevel, objEntityXSelfInsured.LineOfBusCode);
					if(objEntityXSelfInsured.IsCertDiffFlag.ToString() == "True")
					{
						CreateXML(objXML, "IsCertDiffFlag", "Yes", "SelfInsured", iLevel);
					} // if
					else
					{
						CreateXML(objXML, "IsCertDiffFlag", "No", "SelfInsured", iLevel);
					} // else

					CreateXML(objXML, "CertNameEid", objCache.GetEntityLastFirstName(int.Parse(objEntityXSelfInsured.CertNameEid.ToString())), "SelfInsured", iLevel, objEntityXSelfInsured.CertNameEid);

					iLevel++;
				}

			}
			catch (RMAppException p_objExp)
			{
				throw p_objExp;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("OrgHierarchy.GetEntitySearch.GeneralError", m_iClientId), p_objException);//sharishkumar Rmacloud
			}
			finally
			{
				if (objDMF != null)
				{
					objDMF.Dispose();
				}
				if (objEntity != null)
				{
					objEntity.Dispose();
				}
				if (objCache != null)
				{
					objCache.Dispose();
				}

			}
			return objXML;
		}
		/// <summary>
		/// Deletes the Self Insured Details
		/// </summary>
		/// <param name="iId">EntityId/RowId</param>
		/// <param name="connectionString"></param>
		/// <param name="IsEntityDeletion">Entity Deletion is true/false</param>
		/// <returns>True/False</returns>
		public bool DeleteEntityXSelfInsuredData(int iId, string connectionString)
		{
			bool bReturnValue = false;
			string sSQL = "";
			DbReader objReader = null;
			
			try
			{

				sSQL = "UPDATE ENTITY_X_SELFINSUR SET DELETED_FLAG = -1 where SI_ROW_ID = " + iId;

				objReader = DbFactory.GetDbReader(connectionString, sSQL);
				if (objReader.Read())
				{
                    throw new RMAppException(Globalization.GetString("OrgHierarchy.DeleteCompanyInformation.GeneralError", m_iClientId));//sharishkumar Rmacloud
				}
				objReader.Close();
				bReturnValue = true;

			}
			catch (RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.DeleteCompanyInformation.GeneralError", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Dispose();
				}


			}
			return bReturnValue;
		}
		/// <summary>
		/// Saves Self Insured Details
		/// </summary>
		/// <param name="p_sInputXml"></param>
		/// <param name="p_sUserName"></param>
		/// <param name="p_sPassword"></param>
		/// <param name="p_sDsnName"></param>
		/// <returns>True/False</returns>
		public bool SaveEntityXSelfInsuredData(string p_sInputXml, string p_sUserName, string p_sPassword, string p_sDsnName)
		{
			bool bReturnValue = false;
			XmlDocument objXML = new XmlDocument();
			string sRowId = string.Empty;
			EntityXSelfInsured objEntityXSelfInsured = null;
			DataModelFactory objDMF = null;
			int iEntityId;
			

			try
			{
                objDMF = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);//sharishkumar Rmacloud
				objXML.LoadXml(p_sInputXml);
				sRowId = objXML.SelectSingleNode("//rowid").InnerText;
				iEntityId = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//EntityId").InnerText);

				int iRowId = Conversion.ConvertStrToInteger(sRowId);
				objEntityXSelfInsured = (EntityXSelfInsured)objDMF.GetDataModelObject("EntityXSelfInsured", false);
				if (iRowId > 0)
					objEntityXSelfInsured.MoveTo(iRowId);
				
				objEntityXSelfInsured.SIRowId = iRowId;
				objEntityXSelfInsured.EntityId = iEntityId;
				objEntityXSelfInsured.Jurisdiction = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//Jurisdiction").Attributes["codeid"].InnerText);
				objEntityXSelfInsured.DeletedFlag = Conversion.ConvertStrToBool(objXML.SelectSingleNode("//DeletedFlag").InnerText);
				objEntityXSelfInsured.LegalEntityId = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//LegalEntityId").InnerText);
				objEntityXSelfInsured.CertNameEid = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//CertNameEid").Attributes["codeid"].InnerText);
				objEntityXSelfInsured.CertificateNumber = objXML.SelectSingleNode("//CertificateNumber").InnerText;
				objEntityXSelfInsured.EffectiveDate = objXML.SelectSingleNode("//EffectiveDate").InnerText;
				objEntityXSelfInsured.ExpirationDate = objXML.SelectSingleNode("//ExpirationDate").InnerText;
				objEntityXSelfInsured.Authorization = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//Authorization").Attributes["codeid"].InnerText);
				objEntityXSelfInsured.Organization = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//Organization").Attributes["codeid"].InnerText);
				objEntityXSelfInsured.DttmRcdAdded = objXML.SelectSingleNode("//DttmRcdAdded").InnerText;
				objEntityXSelfInsured.DttmRcdLastUpd = objXML.SelectSingleNode("//DttmRcdLastUpd").InnerText;
				objEntityXSelfInsured.UpdatedByUser = objXML.SelectSingleNode("//UpdatedByUser").InnerText;
				objEntityXSelfInsured.AddedByUser = objXML.SelectSingleNode("//AddedByUser").InnerText;
				//sgoel6 Medicare 05/13/2009
				//objEntityXSelfInsured.LineOfBusCode = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//LineOfBusCodeSelfInsured").Attributes["codeid"].InnerText);
				objEntityXSelfInsured.IsCertDiffFlag = Conversion.ConvertStrToBool(objXML.SelectSingleNode("//IsCertDiffFlag").InnerText);
				objEntityXSelfInsured.LineOfBusCode = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//LobCode").InnerText);

				objEntityXSelfInsured.Save();
	 
				bReturnValue = true;           

			}

			catch (RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.Save.Error", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
			finally
			{
				if (objEntityXSelfInsured != null)
				{
					objEntityXSelfInsured.Dispose();
				}


			}
			return bReturnValue;
		}
		#endregion Self Insured Methods

		//MITS 16771- Pankaj 5/29/09
		/// <summary>
		/// Adds filter clause to the main search query
		/// </summary>
		/// <param name="p_sCity">City Filter</param>
		/// <param name="p_lState">State Filter</param>
		/// <param name="p_sZip">Zip Filter</param>
		/// <param name="p_sLevel">Level</param>
		/// <param name="p_bCaseInSens">Case Ins</param>
		/// <returns>Filter clause</returns>
		public string AddAdditionalFilter(string p_sCity, long p_lState, string p_sZip, string p_sLevel, bool p_bCaseInSens)
		{
			string sSQL = "";
			string sOperator = "=";
			try
			{
				if (p_sCity.Trim() != "")
				{
					if (p_sCity.IndexOf("*", 0) > -1)
					{
						sOperator = "LIKE ";
						p_sCity = p_sCity.Replace("*", "%").Replace("%%", "%");
					}
					else
						sOperator = "=";

					if (p_bCaseInSens == false)
                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                        //sSQL = " AND LTRIM(RTRIM(*.CITY)) " + sOperator + "'" + p_sCity + "'";
						sSQL = " AND *.CITY " + sOperator + "'" + p_sCity + "'";
					else
                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                        //sSQL = " AND UPPER(LTRIM(RTRIM(*.CITY))) " + sOperator + "'" + p_sCity.ToUpper() + "'";
						sSQL = " AND UPPER(*.CITY) " + sOperator + "'" + p_sCity.ToUpper() + "'";
				}

				if (p_sZip.Trim() != "")
				{
					if (p_sZip.IndexOf("*", 0) > -1)
					{
						sOperator = "LIKE ";
						p_sZip = p_sZip.Replace("*", "%").Replace("%%", "%");
					}
					else
						sOperator = "=";

					if (p_bCaseInSens == false)
                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                        //sSQL = sSQL + " AND LTRIM(RTRIM(*.ZIP_CODE)) " + sOperator + "'" + p_sZip + "'";
						sSQL = sSQL + " AND *.ZIP_CODE " + sOperator + "'" + p_sZip + "'";
					else
                        //MITS 24313 Remove LTRIM/RTRIM because it prevent using index in the database query
                        //sSQL = sSQL + " AND UPPER(LTRIM(RTRIM(*.ZIP_CODE))) " + sOperator + "'" + p_sZip.ToUpper() + "'";
						sSQL = sSQL + " AND UPPER(*.ZIP_CODE) " + sOperator + "'" + p_sZip.ToUpper() + "'";
				}

				if (p_lState > 0)
					sSQL = sSQL + " AND *.STATE_ID =" + p_lState;

				switch (p_sLevel)
				{
					case "LIST_VIEW":
						sSQL = sSQL.Replace("*.", "");
						break;
					case CLIENT:
						sSQL = sSQL.Replace("*", "H");
						break;
					case COMPANY:
						sSQL = sSQL.Replace("*", "G");
						break;
					case OPERATION:
						sSQL = sSQL.Replace("*", "F");
						break;
					case REGION:
						sSQL = sSQL.Replace("*", "E");
						break;
					case DIVISION:
						sSQL = sSQL.Replace("*", "D");
						break;
					case LOCATION:
						sSQL = sSQL.Replace("*", "C");
						break;
					case FACILITY:
						sSQL = sSQL.Replace("*", "B");
						break;
					case DEPARTMENT:
						sSQL = sSQL.Replace("*", "B1");
						break;
				}
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgHierarchy.AddAdditionalFilter.GeneralError", m_iClientId), p_objEx);//sharishkumar Rmacloud
			}
			return sSQL;
		}

		
	}
}
