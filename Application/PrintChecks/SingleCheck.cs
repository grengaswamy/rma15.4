﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Xml ;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.Settings ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;

namespace Riskmaster.Application.PrintChecks
{
	/**************************************************************
	 * $File		: SingleCheck.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 23/08/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: Class exposes methods for Single Check. 
	 * $Source		:  	
	**************************************************************/
	public class SingleCheck:IDisposable
	{
		#region Variable Declaration 
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store UserId
		/// </summary>
		private int m_iUserId = 0 ;
		/// <summary>
		/// Private variable to store User GroupId
		/// </summary>
		private int m_iUserGroupId = 0 ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
        /// <summary>
        /// ClientID for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable to store the instance of XmlDocument object
		/// </summary>
		private XmlDocument m_objDocument = null ;
		#endregion 

		#region Constructors & destructor
		public SingleCheck(string p_sDsnName , string p_sUserName , string p_sPassword , int p_iUserId , int p_iUserGroupId, int p_iClientId )
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_iUserGroupId = p_iUserGroupId ;
            m_iClientId = p_iClientId;
			m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword, m_iClientId );	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
			Functions.m_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() ;
		}

        ~SingleCheck()
        {            
            Dispose();
		}
		#endregion 

		#region Public Function.
		/// Name		: LoadSingleCheck
		/// Author		: Vaibhav Kaushik
		/// Date Created: 23/08/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// LoadSingleCheck returns the OnLoad XML string for the Single Check PopUp Dialog.
		/// </summary>
		/// <param name="p_iTransId">The TransId of the Check.</param>
		/// <param name="p_dblTransTotal">Total Trans Amount.</param>
		/// <param name="sLangCode">Language Code.</param>
		/// <returns>Returns the XML document.</returns>
        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        public XmlDocument LoadSingleCheck(int p_iTransId, double p_dblTransTotal, string p_sLangCode, int p_iDistributionType) //JIRA:438 START: ajohari2
        // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
		{
			LobSettings objLobSettings = null ;
			ColLobSettings objColLobSettings = null ;
			CCacheFunctions objCCacheFunctions = null ;
			SysSettings objSysSettings = null ;
			Functions objFunctions = null ;			
			Funds objFunds = null ;
			DbReader objReader = null ;
			XmlElement objRootNode = null ;
			XmlElement objMassageNode = null ;
            XmlElement objTempNode = null;

			string sSQL = string.Empty ;
			int iTemp = 0 ;
			//int iNextCheckNumber = 0 ;
            long lNextCheckNumber = 0;    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
			int iAccountId = 0 ;
			int iReleaseCode = 0 ;
			double dblAmountLimit = 0.0 ;
			bool bFreezePayments = false ;
            bool bPostDate = false;
            bool bIsEFTAccount = false;
            int iExportToFile = 0;
			try
			{
				objFunctions = new Functions( m_objDataModelFactory );
				objCCacheFunctions = new CCacheFunctions( m_sConnectionString, m_iClientId );
				objColLobSettings = new ColLobSettings( m_sConnectionString, m_iClientId );
				objSysSettings = new SysSettings( m_sConnectionString, m_iClientId ); //Ash - cloud
				objLobSettings = new LobSettings();
					
				objFunds = ( Funds ) m_objDataModelFactory.GetDataModelObject( "Funds" , false );
				objFunds.MoveTo( p_iTransId );
				Functions.StartDocument( ref m_objDocument , ref objRootNode , ref objMassageNode , "LoadSingleCheck" , m_iClientId);
				
				// Check User Privileges
				try
				{
					objLobSettings = objColLobSettings[objFunds.LineOfBusCode] ;
				}
				catch
				{
					objLobSettings = null ;
				}
				if( objLobSettings != null )
				{
					if( objLobSettings.PrtChkLmtFlag )
					{
						// limits are activated now check  if greater than skip
						if( objFunctions.GetMaxPrintAmount( objFunds.LineOfBusCode , ref dblAmountLimit , m_iUserId , m_iUserGroupId,m_iClientId ) )
						{
							if( p_dblTransTotal > dblAmountLimit )
							{
                                Functions.CreateAndSetElement(objMassageNode, "Error", CommonFunctions.FilterBusinessMessage(Globalization.GetString("SingleCheck.LoadSingleCheck.AmountExceed", m_iClientId), p_sLangCode), m_iClientId);//dvatsa-cloud
								return( m_objDocument );
							}
						}
					}	
				}
                if (!Functions.IsPrinterSelected(m_sConnectionString,m_iClientId))
                {
                    Functions.CreateAndSetElement(objRootNode, "IsPrinterSelected", "false", m_iClientId);
                }
                else
                {
                    Functions.CreateAndSetElement(objRootNode, "IsPrinterSelected", "true", m_iClientId);
                }
				// No negative or zero checks should be printed.
				if( p_dblTransTotal <= 0 )
				{
                    // changed by nadim for  13685
                    if (!(Functions.AllowZeroPayments(m_sConnectionString, m_iClientId) && p_dblTransTotal == 0))
                    {
                        Functions.CreateAndSetElement(objMassageNode, "Error", CommonFunctions.FilterBusinessMessage(Globalization.GetString("SingleCheck.LoadSingleCheck.ZeroAmount", m_iClientId), p_sLangCode), m_iClientId);//dvatsa-cloud
					return( m_objDocument );
                    }
                    // changed by nadim for  13685
				}
				
				// Check for the STATUS CODE.
				iReleaseCode = objCCacheFunctions.GetCodeIDWithShort( "R" , "CHECK_STATUS" );
				if( objFunds.StatusCode != iReleaseCode || objFunds.VoidFlag || p_dblTransTotal == 0 )
				{
                    // changed by nadim for  13685
                    if (!(Functions.AllowZeroPayments(m_sConnectionString, m_iClientId) && p_dblTransTotal == 0))
                    {
                        Functions.CreateAndSetElement(objMassageNode, "Error", CommonFunctions.FilterBusinessMessage(Globalization.GetString("SingleCheck.LoadSingleCheck.CantPrintMsg", m_iClientId), p_sLangCode), m_iClientId);//dvatsa-cloud
					return( m_objDocument );
                    }
                    // changed by nadim for  13685
				}
				
				// Confirm that Check has not printed already.
				if( objFunds.StatusCode == 1054 )
				{
                    Functions.CreateAndSetElement(objMassageNode, "Error", CommonFunctions.FilterBusinessMessage(Globalization.GetString("SingleCheck.LoadSingleCheck.CheckAlreadyPrinted", m_iClientId), p_sLangCode), m_iClientId);//dvatsa-cloud
					return( m_objDocument );
				}

				// Check for the Freeze Payments.
				sSQL = "SELECT FREEZE_PAYMENTS FROM ENTITY WHERE ENTITY_ID = " + objFunds.PayeeEid ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader != null )
				{
					if( objReader.Read() )
					{
						bFreezePayments = objReader.GetBoolean( "FREEZE_PAYMENTS" );
					}
					objReader.Close();
				}
				if( bFreezePayments )
				{
                    Functions.CreateAndSetElement(objMassageNode, "Error", CommonFunctions.FilterBusinessMessage(Globalization.GetString("SingleCheck.LoadSingleCheck.FreezePayments", m_iClientId), p_sLangCode), m_iClientId);//dvatsa-cloud
					return( m_objDocument );
				}
                using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                {
                    if (p_iDistributionType != objCache.GetCodeId("MAL", "DISTRIBUTION_TYPE"))
                    {
                        iExportToFile = GetPrintOptions(p_iDistributionType);
                        // npadhy JIRA 6418 Ends

                        if (iExportToFile == -1)
                        {
                            string sShortCode = string.Empty;
                            string sCodeDesc = string.Empty;
                            string sCodeDetail = string.Empty;


                            objCache.GetCodeInfo(p_iDistributionType, ref sShortCode, ref sCodeDesc, Convert.ToInt32(p_sLangCode));

                            sCodeDetail = string.Format("{0} {1}", sShortCode, sCodeDesc);

                            Functions.CreateAndSetElement(objMassageNode, "Error", string.Format(CommonFunctions.FilterBusinessMessage(Globalization.GetString("SingleCheck.LoadSingleCheck.MissingDstrbtnType", m_iClientId), p_sLangCode), sCodeDetail), m_iClientId);
                        }
                    }
                }

				// At this point, all permissions are grated. Now render the screen layout info.

				if( !objSysSettings.UseFundsSubAcc ) 
				{
					iAccountId = objFunds.AccountId ;
                    if (!objFunctions.MasterBankAcct(objFunds.AccountId, ref iTemp, ref lNextCheckNumber, m_iClientId)) //Ash - cloud
					{
                        sSQL = "SELECT NEXT_CHECK_NUMBER,IS_EFT_ACCOUNT FROM ACCOUNT WHERE ACCOUNT_ID = " + objFunds.AccountId;
						objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
						if( objReader != null )
						{
							if( objReader.Read() )
							{
                                //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
								//iNextCheckNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( "NEXT_CHECK_NUMBER" ), m_iClientId );
                                lNextCheckNumber = Common.Conversion.ConvertObjToInt64(objReader.GetValue("NEXT_CHECK_NUMBER"), m_iClientId);
                                bIsEFTAccount = Common.Conversion.ConvertObjToBool(objReader.GetValue("IS_EFT_ACCOUNT"), m_iClientId);   
							}
							objReader.Close();
						}
						
					}
				}
				else
				{
                    sSQL = "SELECT NEXT_CHECK_NUMBER,ACCOUNT.ACCOUNT_ID,ACCOUNT.IS_EFT_ACCOUNT FROM ACCOUNT,BANK_ACC_SUB WHERE ACCOUNT.ACCOUNT_ID = BANK_ACC_SUB.ACCOUNT_ID AND SUB_ROW_ID = " + objFunds.SubAccountId;
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader != null )
					{
						if( objReader.Read() )
						{
                            //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
							//iNextCheckNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( "NEXT_CHECK_NUMBER" ), m_iClientId );
                            lNextCheckNumber = Common.Conversion.ConvertObjToInt64(objReader.GetValue("NEXT_CHECK_NUMBER"), m_iClientId);
                            iAccountId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId);
                            bIsEFTAccount = Common.Conversion.ConvertObjToBool(objReader.GetValue("IS_EFT_ACCOUNT"), m_iClientId);   
						}
						objReader.Close();
					}

				}
				
                Functions.CreateAndSetElement(objRootNode, "AccountNumber", iAccountId.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "NextCheckNumber", lNextCheckNumber.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "TransId", objFunds.TransId.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "CheckDate", System.DateTime.Now.ToString("d"), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "IsEFTAccount", bIsEFTAccount.ToString().ToLower(), m_iClientId);
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //Functions.CreateAndSetElement(objRootNode, "PrintEFTPayment", p_bPrintEFTPayment.ToString().ToLower(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "DistributionType", p_iDistributionType.ToString().ToLower(), m_iClientId);
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 End:
                //MITS 12301 Raman Bhatia
                //Allow Post Dated Checks checkbox needs to be implemented
                //Check Date should be disabled in such a scenario

                
                sSQL = "SELECT ALLOW_POST_DATE FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bPostDate = objReader.GetBoolean("ALLOW_POST_DATE");
                    }
                    objReader.Close();
                }
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("CheckDate");
                if (objTempNode != null)
                {
                    objTempNode.SetAttribute("disabled", bPostDate.ToString());
                }

                
                objFunctions.LoadListWithStocks( objRootNode , iAccountId, m_iClientId );

                

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("TODO", m_iClientId), p_sLangCode), p_objEx);//dvatsa-cloud
			}
			finally
			{
                objFunctions = null;
				objCCacheFunctions = null ;
				objColLobSettings = null ;
				objSysSettings = null ;
				objLobSettings = null ;
				if( objReader != null )
				{
					//objReader.Close();
					objReader = null ;
				}
				if( objFunds != null )
				{
					objFunds.Dispose();
					objFunds = null ;
				}
				objRootNode = null ;
				objMassageNode = null ;
			}
			return( m_objDocument );
		}

		#endregion 

        #region Private Function
        private int GetPrintOptions(int p_iDistributionType)
        {
            using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
            {
                return (objCache.GetPrintOptions(p_iDistributionType));
            }
        }
        #endregion

        #region Dispose
        private bool _isDisposed = false;
		/// Name		: Dispose
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// UnInitialize the objects. 
		/// </summary>
		public void Dispose()
		{
            if (!_isDisposed)
            {
                _isDisposed = true;
                try
                {
                    if (m_objDataModelFactory != null)
                    {
                        m_objDataModelFactory.Dispose();
                        m_objDataModelFactory = null;
                    }
                    GC.SuppressFinalize(this);
                }
                catch
                {
                    // Avoid raising any exception here.				
                }
            }
		}
		#endregion 
	}
}
