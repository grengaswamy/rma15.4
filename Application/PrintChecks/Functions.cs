﻿using System;
using System.Xml ;
using System.Text ;
using System.Collections;
using System.Collections.Generic;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.Settings ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;

namespace Riskmaster.Application.PrintChecks
{	
	/**************************************************************
	 * $File		: Functions.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/05/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: Exposes common methods used in component. 
	 * $Source		:  	
	**************************************************************/
	public class Functions
	{ 
		#region Variable Declaration 
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;		
		/// <summary>
		/// Private variable to store the DataBase Type
		/// </summary>
		internal static string m_sDBType = "";

		#endregion 

		#region Constructors
		/// Name		: Functions
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_objDataModelFactory">Data Model Factory Object</param>
		internal Functions( DataModelFactory p_objDataModelFactory )
		{
			m_objDataModelFactory = p_objDataModelFactory ;	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString ;						
		}

		#endregion 

		#region Get Configuration File Options
		/// Name		: GetOptionValue
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the value from config file
		/// </summary>
		/// <param name="p_sKey">key representing XML element</param>
		/// <returns>text value</returns>
		internal static string GetOptionValue(string p_sKey, Hashtable objLoadOptions, int p_iClientId)
		{
			string bRet = "";
			try
			{
				
				if (objLoadOptions.ContainsKey(p_sKey))
				{
					bRet = (string)objLoadOptions[p_sKey] ;
				}
				else
				{
					bRet = null ;
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Functions.GetOptionValue.Error",p_iClientId),p_objException);
			}
			finally
			{
				objLoadOptions = null;
			}
			return (bRet);
		}

        /// <summary>
        /// Rahul Aggarwal  Merged check stub text functionality from safeway to R7
        /// Get the value from config file
        /// </summary>
        /// <param name="p_sKey">key representing XML element</param>
        /// <returns>text value</returns>
        internal static string GetOptionValue(string p_sKey, string sConnString, int p_iClientId) //Ash - cloud
        {
            Hashtable objLoadOptions = null;
            string bRet = "";
            try
            {
                objLoadOptions = new Hashtable();
                objLoadOptions = LoadOptions(sConnString, p_iClientId); // Ash - cloud

                if (objLoadOptions.ContainsKey(p_sKey))
                {
                    bRet = (string)objLoadOptions[p_sKey];
                }
                else
                {
                    bRet = null;
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Functions.GetOptionValue.Error", p_iClientId), p_objException);
            }
            finally
            {
                objLoadOptions = null;
            }
            return (bRet);
        }
		
		/// Name		: GetOption
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the options from config file
		/// </summary>
		/// <param name="p_sKey">key representing XML element</param>
		/// <returns>option value</returns>
        internal static bool GetOption(string p_sKey, Hashtable objLoadOptions, int p_iClientId)
		{
			bool bRet = false;
			try
			{
				
				if (objLoadOptions.ContainsKey(p_sKey))
				{
					bRet = true;
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Functions.GetOption.Error", p_iClientId), p_objException);
			}
			finally
			{
				objLoadOptions = null;
			}
			return (bRet);
		}

        /// <summary>
        /// Rahul Aggarwal  Merged check stub text functionality from safeway to R7
        /// Get the options from config file
        /// </summary>
        /// <param name="p_sKey">key representing XML element</param>
        /// <returns>option value</returns>
        internal static bool GetOption(string p_sKey, string sConnString, int p_iClientId) // Ash - cloud
        {
            Hashtable objLoadOptions = null;
            bool bRet = false;
            try
            {
                objLoadOptions = new Hashtable();
                objLoadOptions = LoadOptions(sConnString, p_iClientId); //Ash - cloud

                if (objLoadOptions.ContainsKey(p_sKey))
                {
                    bRet = true;
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Functions.GetOption.Error", p_iClientId), p_objException);
            }
            finally
            {
                objLoadOptions = null;
            }
            return (bRet);
        }		
		
        /// <summary>
        /// Gets the configuration setting to determine if zero payments are allowed
        /// </summary>
        /// <returns>boolean indicating whether or not to allow zero payments</returns>
        // public static bool AllowZeroPayments()
        public static bool AllowZeroPayments(string p_sConnString, int p_iClientId) //Ash - cloud
        {
            Hashtable objConfig = RMConfigurationManager.GetDictionarySectionSettings("Payments", p_sConnString,p_iClientId);
            
            bool blnAllowZeroPayments = Convert.ToBoolean(objConfig["AllowZeroPayment"].ToString());

            //Clean up 
            objConfig = null;

            return blnAllowZeroPayments;
        } // method: AllowZeroPayments


		
		/*
		 * SAMPLE CONFIG FILE XML
		 * 
			<PrintChecks>
				<PrintCheck_ClaimInfoOnCheck>true</PrintCheck_ClaimInfoOnCheck>
				<PrintCheck_DriverInfoOnCheck>true</PrintCheck_DriverInfoOnCheck>
				<PrintCheck_PageHeight>15840</PrintCheck_PageHeight>
				<PrintCheck_ApplicationDirectory>c:\vaibhav</PrintCheck_ApplicationDirectory>
				<PrintCheck_UseTransCodeDesc>true</PrintCheck_UseTransCodeDesc>
				<PrintCheck_PdfSavePath>c:\vaibhav\pdf</PrintCheck_PdfSavePath>
			</PrintChecks>
		*/
		/// Name		: LoadOptions
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Load the options from the config file.
		/// </summary>
		/// <param name="p_objLoadOptions">Hash table containing the config keys & corresponding values</param>
		internal static Hashtable LoadOptions(string sConnString, int iClientId) //Ash - cloud
		{
			Hashtable p_objLoadOptions = RMConfigurationManager.GetDictionarySectionSettings("PrintChecks", sConnString, iClientId); //Ash - cloud

            List<string> arrKeys = new List<string>();

            arrKeys.Add(CheckConstants.APPLICATION_PATH);
            arrKeys.Add(CheckConstants.EXPORT_TO_FILE_PATH);
            arrKeys.Add(CheckConstants.PDF_SAVE_PATH);
            arrKeys.Add(CheckConstants.EOB_PDF_SAVE_PATH);

            foreach (string item in arrKeys)
            {
                if (!p_objLoadOptions.ContainsKey(item))
                {
                    //Add additional configuration options for the Hashtable
                    p_objLoadOptions.Add(item, RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, CheckConstants.PRINT_CHECKS_DIR));
                }//if
            }//foreach

            return p_objLoadOptions;
			
		}
		#endregion

		#region Common XML functions
		/// Name		: StartDocument
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the XML document
		/// </summary>
		/// <param name="objXmlDocument">XML document</param>
		/// <param name="p_objRootNode">Root Node</param>
		/// <param name="p_objMessagesNode">Messages Node</param>
		/// <param name="p_sRootNodeName">Root Node Name</param>
		internal static void StartDocument( ref XmlDocument objXmlDocument , ref XmlElement p_objRootNode, ref XmlElement p_objMessagesNode , string p_sRootNodeName, int p_iClientId )
		{
			try
			{
				objXmlDocument = new XmlDocument();
				p_objRootNode = objXmlDocument.CreateElement( p_sRootNodeName );
				objXmlDocument.AppendChild( p_objRootNode );
				Functions.CreateElement( p_objRootNode , CheckConstants.MESSAGES_NODE , ref p_objMessagesNode,p_iClientId );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.StartDocument.InitDocError", p_iClientId) , p_objEx );				
			}
		}

		/// Name		: CreateElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Node Name</param>		
		internal static void CreateElement( XmlElement p_objParentNode , string p_sNodeName, int p_iClientId )
		{
			XmlElement objChildNode = null ;
			try
			{
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.CreateElement.NewNodeError", p_iClientId), p_objEx);
			}
			finally
			{
				objChildNode = null ;
			}

		}	
	
		/// Name		: CreateElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Child Node Name</param>
		/// <param name="p_objChildNode">Child Node</param>
		internal static void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode, int p_iClientId )
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.CreateElement.NewNodeError", p_iClientId) , p_objEx );
			}

		}
		
		/// Name		: CreateAndSetElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sText">Text</param>		
		internal static void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText, int p_iClientId )
		{
			try
			{
				XmlElement objChildNode = null ;
				Functions.CreateAndSetElement( p_objParentNode , p_sNodeName , p_sText , ref objChildNode ,p_iClientId);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.CreateAndSetElement.NewNodeError",p_iClientId) , p_objEx );
			}
		}
		/// Name		: CreateAndSetElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sText">Text</param>
		/// <param name="p_objChildNode">Child Node</param>
		internal static void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode, int p_iClientId )
		{
			try
			{
				CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode, p_iClientId );
				p_objChildNode.InnerText = p_sText ;			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.CreateAndSetElement.NewNodeError",p_iClientId) , p_objEx );
			}
		}

		/// Name		: GetValue
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get text value of the node.
		/// </summary>
		/// <param name="p_objDocument">Input XML document.</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>Text value of the node</returns>
		internal static string GetValue( XmlDocument p_objDocument , string p_sNodeName, int p_iClientId )
		{
			XmlElement objNode = null ;
			string sValue = "" ;

			try
			{
				objNode = ( XmlElement ) p_objDocument.SelectSingleNode( "//" + p_sNodeName );

				if( objNode != null )
					sValue = objNode.InnerText ;
			}
			catch(Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.GetValue.Error", p_iClientId) , p_objEx );
			}
			finally
			{
				objNode = null ;
			}
			return( sValue );

		}

		/// Name		: GetList
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get list form XML document.
		/// </summary>
		/// <param name="p_objDocument">Input XML document</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>Array List</returns>
		internal static ArrayList GetList( XmlDocument p_objDocument , string p_sNodeName, int p_iClientId)
		{
			XmlNodeList objNodeList = null ;
			ArrayList arrlstReturn = null ;
			string sValue = "" ;

			try
			{
				arrlstReturn = new ArrayList();

				objNodeList = p_objDocument.SelectNodes( "//" + p_sNodeName );
				foreach( XmlNode objTempNode in objNodeList )
				{
					sValue = ((XmlElement)objTempNode).InnerText ;
					arrlstReturn.Add( sValue );
				}
				return( arrlstReturn );
			}
			catch(Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.GetList.XMLInput",p_iClientId) , p_objEx );
			}
			finally
			{
				objNodeList = null ;
				arrlstReturn = null ;
			}			
		}		

		/// Name		: GetList
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get list form XML document.
		/// </summary>
		/// <param name="p_objDocument">Input XML document</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sSeprator" >Seprator string </param>
		/// <returns>Array List</returns>
		internal static ArrayList GetList( XmlDocument p_objDocument , string p_sNodeName, string p_sSeprator, int p_iClientId )
		{
			XmlNode objNode = null ;
			ArrayList arrlstReturn = null ;
			string sTemp = "" ;			

			try
			{
				arrlstReturn = new ArrayList();

				objNode = p_objDocument.SelectSingleNode( "//" + p_sNodeName );
				
				if( objNode != null )
				{
					sTemp = objNode.InnerText ;

					foreach( string sValue in sTemp.Split( p_sSeprator.ToCharArray() ) )
					{
						arrlstReturn.Add( sValue );
					}
				}
				return( arrlstReturn );
			}
			catch(Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.GetList.XMLInput",p_iClientId) , p_objEx );
			}
			finally
			{
				objNode = null ;
				arrlstReturn = null ;
			}			
		}		
		#endregion 

		#region Misc.	
		/// Name		: ReplaceArguments
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Replace the arguments in the strings.
		/// </summary>
		/// <param name="p_sInput">Input string</param>
		/// <param name="p_arrArguments">Array of parameters.</param>
		/// <returns></returns>
		internal static string ReplaceArguments( string p_sInput ,  int p_iClientId, params string[] p_arrArguments )
		{
			int iIndex = 0 ;
			string sSearchTo = "%%" ;	
			string sTemp = "" ;
            //changes for ML MITS 31020
            string []sSeparator= {"|^|"};
            if (p_sInput.Contains("|^|"))
            {
              p_sInput = p_sInput.Split(sSeparator, StringSplitOptions.None)[2];
            }
            //changes for ML MITS 31020
			sTemp = p_sInput ;
			try
			{
				for( iIndex = 0 ; iIndex < p_arrArguments.Length ; iIndex++ )
				{				
					if( sTemp.IndexOf( sSearchTo + ( iIndex + 1 ).ToString() ) == -1 )
                        throw new RMAppException(Functions.ReplaceArguments(Globalization.GetString("Functions.ReplaceArguments.IndexNotFound", p_iClientId), p_iClientId, iIndex.ToString())); 						
					else
						sTemp = sTemp.Replace( sSearchTo + ( iIndex + 1 ).ToString() , p_arrArguments[iIndex] );
				}				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.ReplaceArguments.Error",p_iClientId) , p_objEx );				
			}
			
			return( sTemp );	
		}		

		/// Name		: InRecordSet
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Check the existence of a column-field in the recordset.
		/// </summary>
		/// <param name="p_objReader">Reader Object</param>
		/// <param name="p_sColumnName">Column Name</param>
		/// <returns>Return true, if exist</returns>
		internal static bool InRecordSet( DbReader p_objReader , string p_sColumnName, int p_iClientId )
		{
			int iIndexI = 0 ;
			try
			{
				if( p_objReader != null )
					for( iIndexI = 0 ; iIndexI < p_objReader.FieldCount ; iIndexI++ )
						if( p_objReader.GetName( iIndexI ).ToUpper() == p_sColumnName.ToUpper() )
							return( true );	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.InRecordSet.Error",p_iClientId) , p_objEx );				
			}
			return( false );												
		}
		
		/// Name		: GetUniqueFileName
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the unique file name.
		/// </summary>
		/// <returns>Returns the unique file name</returns>
		internal static string GetUniqueFileName(int p_iClientId)
		{
			string sTempPath = "" ;
			
			try
			{
                // akaushik5 Changed for MITS 38045 Starts
                //sTempPath = "Check" + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
                //    + System.AppDomain.GetCurrentThreadId() + ".PDF" ; 			
                sTempPath = string.Format("Check{0}{1}.PDF", System.DateTime.Now.ToString("yyyyMMddHHmmssffffff"), System.AppDomain.GetCurrentThreadId());
                // akaushik5 Changed for MITS 38045 Ends
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.GetUniqueFileName.Error",p_iClientId) , p_objEx );				
			}
			return sTempPath ;
		}

        internal static string GetUniqueFileNameForHoldPayment(int p_iClientId)
        {
            string sTempPath = "";

            try
            {
                // akaushik5 Changed for MITS 38045 Starts
                //sTempPath = "Check" + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
                //    + System.AppDomain.GetCurrentThreadId() + ".PDF" ; 			
                sTempPath = string.Format("HoldPayment{0}{1}.PDF", System.DateTime.Now.ToString("yyyyMMddHHmmssffffff"), System.AppDomain.GetCurrentThreadId());
                // akaushik5 Changed for MITS 38045 Ends
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.GetUniqueFileName.Error", p_iClientId), p_objEx);
            }
            return sTempPath;
        }


		/// Name		: CheckInSelectionList
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return True, if check is in selection list.
		/// </summary>
		/// <param name="p_iTransId">TransId</param>
		/// <param name="p_bCheckTypeAuto">Is Auto Check</param>
		/// <param name="p_arrlstSelectedCheck">Selected Pre Checks</param>
		/// <param name="p_arrlstSelectedAutoCheck">Selected Auto Checks</param>
		/// <returns>true/false</returns>
		internal static bool CheckInSelectionList( int p_iTransId , bool p_bCheckTypeAuto , ArrayList p_arrlstSelectedCheck , 
			ArrayList p_arrlstSelectedAutoCheck, int p_iClientId )
		{
			try
			{
				if( p_bCheckTypeAuto ) 
				{
					if( p_arrlstSelectedAutoCheck.Contains( p_iTransId.ToString() ) )
						return true ;					
				}
				else
				{
					if( p_arrlstSelectedCheck.Contains( p_iTransId.ToString() ) )
						return true ;
				}
			}		
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.CheckInSelectionList.Error",p_iClientId) , p_objEx );
			}
			return false ;
		}
		
		
		/// Name		: ReturnSortOrder
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return UI Sort order string.
		/// </summary>
		/// <param name="p_sOrderByField">Sort Order</param>
		/// <returns>UI Sort Order</returns>
		internal static string ReturnSortOrder( string p_sOrderByField, int p_iClientId )
		{
			string sOrderByField = string.Empty ;
			
			try
			{
				switch( p_sOrderByField.Trim() )
				{
					case "FUNDS.AMOUNT" :
						sOrderByField = "Check Total" ;
						break ;
					case "FUNDS.CLAIM_NUMBER" :
						sOrderByField = "Claim Number" ;
						break ;
					case "FUNDS.CTL_NUMBER" :
						sOrderByField = "Control Number" ;
						break ;
					case "FUNDS.LAST_NAME,FUNDS.FIRST_NAME" :
						sOrderByField = "Payee Name" ;
						break ;
					case "FUNDS.TRANS_DATE" :
						sOrderByField = "Transaction Date" ;
						break ;
					case "FUNDS.TRANS_NUMBER" :
						sOrderByField = "Check Number" ;
						break ;
					case "BANK_ACC_SUB" :
						sOrderByField = "Sub Bank Account" ;
						break ;
					case "PAYER_LEVEL" :
						sOrderByField = "Payer Level" ;
						break ;
					case "ADJUSTER_EID" :
						sOrderByField = "Current Adjuster" ;
						break ;
                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                    case "ORG_LEVEL":
                        sOrderByField = "Org. Hierarchy";
                        break;
                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
					default :
						sOrderByField = "(None)" ;
						break;
				}
				return( sOrderByField );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.ReturnSortOrder.Error",p_iClientId) , p_objEx );
			}			
		}

		/// Name		: GetSortOrder
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return AL Sort order string.
		/// </summary>
		/// <param name="p_sOrderByField">Sort Order</param>
		/// <returns>AL Sort Order</returns>		
        internal static string GetSortOrder(string p_sOrderByField, int p_iClientId)
		{
			string sOrderByField = string.Empty ;
			
			try
			{
				switch( p_sOrderByField.Trim() )
				{
					case "Check Total" :
						sOrderByField = "FUNDS.AMOUNT" ;
						break ;
					case "Claim Number" :
						sOrderByField = "FUNDS.CLAIM_NUMBER" ;
						break ;
					case "Control Number" :
						sOrderByField = "FUNDS.CTL_NUMBER" ;
						break ;
					case "Payee Name" :
						sOrderByField = "FUNDS.LAST_NAME,FUNDS.FIRST_NAME" ;
						break ;
					case "Transaction Date" :
						sOrderByField = "FUNDS.TRANS_DATE" ;
						break ;
					case "Check Number" :
						sOrderByField = "FUNDS.TRANS_NUMBER" ;
						break ;
					case "Sub Bank Account" :
						sOrderByField = "BANK_ACC_SUB" ;
						break ;
					case "Payer Level" :
						sOrderByField = "PAYER_LEVEL" ;
						break ;
					case "Current Adjuster" :
						sOrderByField = "ADJUSTER_EID" ;
						break ;
                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                    case "Org Hierarchy": //rsushilaggar MITS 21396 Date 09/29/2010
                    case "Org. Hierarchy": // Bharani - MITS : 36616
                        sOrderByField = "ORG_LEVEL";
                        break;
                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
					default :
						sOrderByField = "" ;
						break;
				}
				return( sOrderByField );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.GetSortOrder.Error",p_iClientId) , p_objEx );
			}			
		}
        /// Name		: IsCanadianFormat
        /// Author		: Raman Bhatia
        /// Date Created: 04/07/2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Returns true if zipcode is in canadian format.
        /// </summary>
        /// <param name="p_sOrderByField">Sort Order</param>
        /// <returns>AL Sort Order</returns>		
        
        internal static bool IsCanadianFormat( string p_sZipCode )
        {
            try
            {
                if (p_sZipCode.Length >= 6)
                {
                    if ((Common.Utilities.IsNumeric(p_sZipCode.Substring(0, 1)) == true) ||
                     (Common.Utilities.IsNumeric(p_sZipCode.Substring(1, 1)) == false) ||
                     (Common.Utilities.IsNumeric(p_sZipCode.Substring(2, 1)) == true))   
                    {
                        return false;
                    }
                    
                    return true;
                }
                return false;
                
                
            }
            catch (Exception p_objEx)
            {
                return false;
            }
        }
        public static bool IsPrinterSelected(string p_sConnectionString, int p_iClientId)
        {

            DbReader objReader = null;
            string sPrinterName = "";
            string sDirectToPrinter = "";
            try
            {
                string sSQL = "SELECT DIRECT_TO_PRINTER , PRINTER_NAME FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(p_sConnectionString, sSQL);

                if (objReader.Read())
                {
                    sDirectToPrinter = objReader.GetValue(0).ToString();
                    sPrinterName = objReader.GetValue(1).ToString();
                }
                objReader.Dispose();
                if (sDirectToPrinter == "-1")
                {
                    if (sPrinterName.Trim() == "")
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckManager.SetPaymentParameterValues.Error",p_iClientId), p_objEx);
            }
            finally
            {

                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return true;
        }
        public static bool IsDirectToPrinter(string p_sConnectionString)
        {
            DbReader objReader = null;
            string sDirectPrinter = "";
            try
            {
                string sSQL = "SELECT DIRECT_TO_PRINTER FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(p_sConnectionString, sSQL);

                while (objReader.Read())
                {
                     sDirectPrinter = objReader.GetValue(0).ToString();
                }
                objReader.Dispose();
                if (sDirectPrinter == "-1")
                    return true;
            }
            catch (Exception p_objEx)
            {
                return false;
            }
            return false;
        }
        public static bool IsLocalPrinter(string p_sConnectionString)
        {
            DbReader objReader = null;
            string sLocalPrinter = "";
            try
            {
                string sSQL = "SELECT LOCAL_PRINTER FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(p_sConnectionString, sSQL);

                while (objReader.Read())
                {
                    sLocalPrinter = objReader.GetValue(0).ToString();
                }
                objReader.Dispose();
                if (sLocalPrinter == "-1")
                    return true;
            }
            catch (Exception p_objEx)
            {
                return false;
            }
            return false;
        }
		#endregion 

		#region No Static Functions ( Worked with database ).
		/// Name		: MasterBankAcct
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the Master Bank Account Id.
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_iMasterBankAcctId">Master Bank Account Id</param>
		/// <param name="p_iNextCheckNum">Next Check Number </param>
		/// <returns>return true, if Master Bank Account Found</returns>
 //pmittal5 Mits 14500 02/24/09 - Changed type of p_lNextCheckNum from Int to Long 
		internal bool MasterBankAcct( int p_iAccountId , ref int p_iMasterBankAcctId, ref long p_lNextCheckNum, int p_iClientId ) //ash - cloud
		{
			//
			// MasterBankAcct is True only if
			// 1) bUseFundsMastAcc,   
			// 2) Not bUseFundsSubAccounts,
			// 3) there is an ACCOUNT WHERE ACCOUNT_ID= p_iAccountId   and
			// 4) there is an ACCOUNT WHERE ACCOUNT_ID= p_iMasterBankAcctId
			//

			SysSettings objSysSettings = null;
			Account objAccount = null ;

			bool bReturnValue = false ;
			bool bAccountFound = false ;

			try
			{
                objSysSettings = new SysSettings(m_sConnectionString, p_iClientId); //Ash - cloud
				if( objSysSettings.UseMastBankAcct && ( ! objSysSettings.UseFundsSubAcc ) )
				{
					objAccount = ( Account ) m_objDataModelFactory.GetDataModelObject( "Account" , false );
					try
					{
						objAccount.MoveTo( p_iAccountId );
						bAccountFound = true ;
					}
					catch
					{
						bAccountFound = false ;
					}
				
					if( bAccountFound )
					{
						p_iMasterBankAcctId = objAccount.MastBankAcctId ;
					}
					
					// Find Next check number for master account
					try
					{
                        //rkulavil : RMA-9279 starts
                        if (p_iMasterBankAcctId > 0)
                        {
                            objAccount.MoveTo(p_iMasterBankAcctId);
                            bAccountFound = true;
                        }
                        else
                        {
                            bAccountFound = false;
                        }
                        //rkulavil : RMA-9279 ends
					}
					catch
					{
						bAccountFound = false ;
					}
				
					if( bAccountFound )
					{
						p_lNextCheckNum = objAccount.NextCheckNumber ;
						bReturnValue = true ;
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.MasterBankAcct.Error",p_iClientId) , p_objEx );				
			}
			finally
			{
				objSysSettings = null;
				if( objAccount != null )
				{
					objAccount.Dispose();
					objAccount = null ;
				}
			}
			return( bReturnValue );
		}

		/// Name		: AccountName
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Account Name
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <returns>Account Name string</returns>
		internal string AccountName( int p_iAccountId, int p_iClientId )
		{
			Account objAccount = null ;
			string sAcctName = "" ;
			bool bAccountFound = false ;

			try
			{
				objAccount = ( Account ) m_objDataModelFactory.GetDataModelObject( "Account" , false );
				try
				{
					objAccount.MoveTo( p_iAccountId );
					bAccountFound = true ;
				}
				catch
				{
					bAccountFound = false ;
				}
				if( bAccountFound )
					sAcctName = objAccount.AccountName.Trim() ;
				else
					sAcctName = Globalization.GetString("Functions.AccountName.NotKnown",p_iClientId) ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.AccountName.Error",p_iClientId) , p_objEx );				
			}
			finally
			{
				if( objAccount != null )
				{
					objAccount.Dispose();
					objAccount = null ;
				}				
			}
			return( sAcctName );
		}
		
		/// Name		: OracleFunctions
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Execute the Oracle functions.
		/// </summary>
		/// <param name="p_iIndex">IndexId of the function to execute.</param>
// JP 05.15.2006   Removed use of Oracle functions. No longer needed in 9i, 10g.
//		internal void OracleFunctions( int p_iIndex )
//		{
//			DbConnection objConn = null ;
//			StringBuilder sbSQL = null;
//
//			string sVbCrlf = "\r\n" ; 
//			
//			try
//			{
//				sbSQL = new StringBuilder();
//
//				switch( p_iIndex )
//				{
//					case 1 :
//						sbSQL.Append( " create or replace function getFrzFlag (lclaimid IN NUMBER) RETURN NUMBER IS frzFlag NUMBER;" );
//						sbSQL.Append( sVbCrlf + "BEGIN" );
//						sbSQL.Append( sVbCrlf + "SELECT PAYMNT_FROZEN_FLAG INTO frzFlag FROM CLAIM WHERE CLAIM.CLAIM_ID = lclaimid;" );
//						sbSQL.Append( sVbCrlf + "return(frzFlag);" );
//						sbSQL.Append( sVbCrlf + "EXCEPTION" );
//						sbSQL.Append( sVbCrlf + "WHEN OTHERS" );
//						sbSQL.Append( sVbCrlf + "THEN return(0);" );
//						sbSQL.Append( sVbCrlf + "END;" );
//						break;
//					case 2 :
//						sbSQL.Append( "create or replace function getClaimType (lclaimid IN NUMBER) RETURN NUMBER IS claimType NUMBER;" );
//						sbSQL.Append( sVbCrlf + "BEGIN" );
//						sbSQL.Append( sVbCrlf + "SELECT CLAIM_TYPE_CODE INTO claimType FROM CLAIM WHERE CLAIM.CLAIM_ID = lclaimid;" );
//						sbSQL.Append( sVbCrlf + "return(claimType);" );
//						sbSQL.Append( sVbCrlf + "EXCEPTION" );
//						sbSQL.Append( sVbCrlf + "WHEN OTHERS" );
//						sbSQL.Append( sVbCrlf + "THEN return(0);" );
//						sbSQL.Append( sVbCrlf + "END;" );
//						break;
//					case 3 :
//						sbSQL.Append( "create or replace function getLOBCode (lclaimid IN NUMBER) RETURN NUMBER IS lobCode NUMBER;" );
//						sbSQL.Append( sVbCrlf + "BEGIN" );
//						sbSQL.Append( sVbCrlf + "SELECT LINE_OF_BUS_CODE INTO lobCode FROM CLAIM WHERE CLAIM.CLAIM_ID = lclaimid;" );
//						sbSQL.Append( sVbCrlf + "return(lobCode);" );
//						sbSQL.Append( sVbCrlf + "EXCEPTION" );
//						sbSQL.Append( sVbCrlf + "WHEN OTHERS" );
//						sbSQL.Append( sVbCrlf + "THEN return(0);" );
//						sbSQL.Append( sVbCrlf + "END;" );
//						break ;
//					case 4 :
//						sbSQL.Append( "create or replace function getFrzPayee (lPayeeEID IN NUMBER) RETURN NUMBER IS frzPayee NUMBER;" );
//						sbSQL.Append( sVbCrlf + "BEGIN" );
//						sbSQL.Append( sVbCrlf + "SELECT FREEZE_PAYMENTS INTO frzPayee FROM ENTITY WHERE ENTITY.ENTITY_ID = lPayeeEID;" );
//						sbSQL.Append( sVbCrlf + "return(frzPayee);" );
//						sbSQL.Append( sVbCrlf + "EXCEPTION" );
//						sbSQL.Append( sVbCrlf + "WHEN OTHERS" );
//						sbSQL.Append( sVbCrlf + "THEN return(0);" );
//						sbSQL.Append( sVbCrlf + "END;" );
//						break ;
//				}
//
//				objConn = DbFactory.GetDbConnection( m_sConnectionString );
//				objConn.Open();
//				objConn.ExecuteNonQuery( sbSQL.ToString() );
//				objConn.Close();
//			}
//			catch( RMAppException p_objEx )
//			{
//				throw p_objEx ;
//			}
//			catch( Exception p_objEx )
//			{
//				throw new RMAppException(Globalization.GetString("Functions.OracleFunctions.Error") , p_objEx );
//			}
//			finally
//			{
//				if ( objConn != null )
//				{
//					objConn.Close();
//					objConn.Dispose();					
//				}
//				sbSQL = null ;
//			}
//		}
		/// Name		: GetPayerLevel
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the Payer Level.
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <returns>Payer Level string </returns>
		internal string GetPayerLevel( int p_iAccountId, int p_iClientId )
		{
			DbReader objReader = null ;
			string sPayerLevel = "" ;
			int iPayerLevel = 0 ;
			string sSQL = "" ;

			try
			{
				sSQL = " SELECT PAYER_LEVEL FROM CHECK_STOCK WHERE ACCOUNT_ID = " + p_iAccountId ;
				objReader = DbFactory.GetDbReader( m_sConnectionString ,sSQL );
				if( objReader != null )
				{
					if( objReader.Read() )
					{
						iPayerLevel = Common.Conversion.ConvertObjToInt( objReader.GetValue( "PAYER_LEVEL" ),p_iClientId);
					}
//					objReader.Close();
				}
			
				switch( iPayerLevel )
				{
					case 1005 :  // client
						sPayerLevel = "CLIENT_EID" ;
						break;
					case 1006 :  // company
						sPayerLevel = "COMPANY_EID" ;
						break;
					case 1007 :  // operation
						sPayerLevel = "OPERATION_EID" ;
						break;
					case 1008 :  // region
						sPayerLevel = "REGION_EID" ;
						break;
					case 1009 :  // division
						sPayerLevel = "DIVISION_EID" ;
						break;
					case 1010 :  // location
						sPayerLevel = "LOCATION_EID" ;
						break;
					case 1011 :  // facility
						sPayerLevel = "FACILITY_EID" ;
						break;
					case 1012 :  // department
						sPayerLevel = "DEPARTMENT_EID" ;
						break;
					default :
						sPayerLevel = "" ;
						break ;
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.GetPayerLevel.Error",p_iClientId) , p_objEx );				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( sPayerLevel  );
		}

		/// Name		: GetMaxPrintAmount
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Max Print Amount
		/// </summary>
		/// <param name="p_iLob">Lob Code</param>
		/// <param name="p_dblAmount">Amount</param>
		/// <param name="p_iUserId">User Id</param>
		/// <param name="p_iUserGroupId">User Group Id</param>
		/// <returns>Max Pay Amount Flag</returns>
		internal bool GetMaxPrintAmount( int p_iLob , ref double p_dblAmount , int p_iUserId , int p_iUserGroupId , int p_iClientId)
		{
			DbReader objReader = null ;		

			bool bGetMaxPrintAmount = false ;
			string sSQL = "" ;

			try
			{
				p_dblAmount = 0 ;

				sSQL = " SELECT MAX(MAX_AMOUNT) FROM PRT_CHECK_LIMITS WHERE USER_ID = " + p_iUserId  
					+  " AND LINE_OF_BUS_CODE=" + p_iLob ;

				if( p_iUserGroupId > 0 )
					sSQL += " OR GROUP_ID = " + p_iUserGroupId ;

				sSQL += " GROUP BY LINE_OF_BUS_CODE" ;

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
			
				if( objReader != null )
				{
					if( objReader.Read() )
					{
						p_dblAmount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( 0 ) ));
						bGetMaxPrintAmount = true ;
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.GetMaxPrintAmount.Error",p_iClientId) , p_objEx );
			}
			finally
			{				
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( bGetMaxPrintAmount ) ;
		}
		

		/// Name		: LoadListWithStocks
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get list of stocks for AccountId.
		/// </summary>
		/// <param name="p_objRootNode">Root Node of the output XML.</param>
		/// <param name="p_iAccountId">Account Id</param>
		/// <returns>true, if at least one stock exist.</returns>
		internal void LoadListWithStocks( XmlElement p_objRootNode , int p_iAccountId, int p_iClientId )
		{
			DbReader objReader = null ;
			XmlElement objStocksNode = null ;
			XmlElement objStockNode = null ;

			string sSQL = "" ;
			int iCount = 0;
			
			try
			{
				sSQL = " SELECT STOCK_NAME,STOCK_ID FROM CHECK_STOCK WHERE ACCOUNT_ID = " + p_iAccountId ;
                Functions.CreateElement(p_objRootNode, "Stocks", ref objStocksNode,p_iClientId);
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader != null )
				{
					while( objReader.Read() )
					{
						iCount++;
                        Functions.CreateElement(objStocksNode, "Stock", ref objStockNode, p_iClientId);
						objStockNode.SetAttribute( "Name" , objReader.GetString("STOCK_NAME" ) );
						objStockNode.SetAttribute( "Id" , Conversion.ConvertObjToInt(objReader.GetValue("STOCK_ID" ), p_iClientId).ToString() );
					}
//					objReader.Close();
				}
				objStocksNode.SetAttribute( "Count" , iCount.ToString() );				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Functions.LoadListWithStocks.Error",p_iClientId) , p_objEx );				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				objStocksNode = null ;
				objStockNode = null ;
			}			
		}
		
		#endregion 
        /// <summary>
        /// To get the uniqueFileName for EFT
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_sAccountName"></param>
        /// <param name="p_sAccountnumber"></param>
        /// <returns></returns>
        internal static string GetUniqueFileNameForEft(string p_sAccountName, string p_sAccountnumber, int p_iClientId)
        {
            string sTempPath = "";

            try
            {
                sTempPath = p_sAccountName + p_sAccountnumber + System.DateTime.Now.ToString("yyyyMMddHHmmss")
                     + ".csv";
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.GetUniqueFileName.Error", p_iClientId), p_objEx);
            }
            return sTempPath;
        }
        /// <summary>
        /// To check if the bank account is an eft bank account.And create a node for the 
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_objRootNode"></param>
        /// <param name="p_iAccountId"></param>
        internal void IsEFTAccount(XmlElement p_objRootNode, int p_iAccountId, int p_iClientId)
        {
            DbReader objReader = null;
            XmlElement objEFTNode = null;
            string sSQL = "";
            try
            {
                
                Functions.CreateElement(p_objRootNode, "IsEFTAccount", ref objEFTNode,p_iClientId);
                if (Functions.IsEFTBank(p_iAccountId, m_sConnectionString, p_iClientId))
                            objEFTNode.InnerText = "true";
                        else
                            objEFTNode.InnerText = "false"; 
               
                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Functions.LoadListWithStocks.Error",p_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objEFTNode = null;
                
            }
        }

        internal static bool IsEFTBank(int p_iAccountId, string p_sConnectionString, int p_iClientId)
        {
            bool bReturn = false;
            string sSQL = string.Empty;
            try
            {
                sSQL = "SELECT IS_EFT_ACCOUNT FROM ACCOUNT WHERE ACCOUNT_ID = " + p_iAccountId;
                using (DbReader objReader = DbFactory.GetDbReader(p_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        bReturn = Conversion.ConvertObjToBool(objReader.GetValue("IS_EFT_ACCOUNT"), p_iClientId);
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            return bReturn;
        }
       
	}
}
