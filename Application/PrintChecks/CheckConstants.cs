using System;

namespace Riskmaster.Application.PrintChecks
{
	/// <summary>
	/// Defines constants used through the project
	/// </summary>
	internal class CheckConstants
	{
		#region Constants
		/// <summary>
		/// Constant for claim info configuration entry flag
		/// </summary>
		internal const string PRINT_CLAIM_INFO = "ClaimInfoOnCheck" ;
		/// <summary>
		/// Constant for driver name configuration entry flag
		/// </summary>
		internal const string PRINT_DRIVER_NAME = "DriverInfoOnCheck" ;
		/// <summary>
		/// Constant for page length configuration entry
		/// </summary>
		internal const string PRINT_PAGE_HEIGHT = "PageHeight" ;		
		/// <summary>
		/// Constant for application path configuration entry 
		/// </summary>
		internal const string APPLICATION_PATH = "ApplicationDirectory" ;
		/// <summary>
		/// Constant for Export To File path configuration entry 
		/// </summary>
		internal const string EXPORT_TO_FILE_PATH = "ExportToFilePath" ;		
		/// <summary>
		/// Constant for trans code configuration entry flag
		/// </summary>
		internal const string USE_TRANSCODE_DESC = "UseTransCodeDesc" ;
		/// <summary>
		/// Constant for PDF save folder path entry in configuration file.
		/// </summary>
		internal const string PDF_SAVE_PATH = "PdfSavePath" ;
		/// <summary>
		/// Constant for Eob PDF save folder path entry in configuration file.
		/// </summary>
		internal const string EOB_PDF_SAVE_PATH = "EobPdfSavePath" ;
		/// <summary>
		/// Constant for left align
		/// </summary>
		internal const string LEFT_ALIGN = "1" ;
		/// <summary>
		/// Constant for right align
		/// </summary>
		internal const string RIGHT_ALIGN = "2" ;
		/// <summary>
		/// Constant for center align
		/// </summary>
		internal const string CENTER_ALIGN = "3" ;	
		/// <summary>
		/// Constant for Messages Node Name
		/// </summary>
		internal const string MESSAGES_NODE = "Messages" ;
		/// <summary>
		/// Constant for Message Node Name
		/// </summary>
		internal const string MESSAGE_NODE = "Message" ;

        internal const string PRINT_CHECKS_DIR = "PrintChecks";
	
		#endregion 
	}
}
