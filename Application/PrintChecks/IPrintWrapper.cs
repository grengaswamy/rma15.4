using System;
using System.Collections.Generic;
using System.Text;

namespace PDFComponent
{
 


    interface IPrintWrapper
    {
        double CurrentX
        {
            get;
            set;
        }

        double CurrentY
        {
            get;
            set;
        }

        double PageHeight
        {
            get;
            set;
        }

        double PageWidth
        {
            get;
            set;
        }

        object Document
        {
            get;
            set;
        }

        object NewPage
        {
            get;
        }

        bool IsLandscape
        {
            get;
            set;
        }

        string FontName
        {
            get;
            set;
        }

        double FontSize
        {
            get;
            set;
        }

        int FontStyle
        {
            get;
            set;
        }

        float TopMargin
        {
            get;
            set;
        }

        float BottomMargin
        {
            get;
            set;
        }

        float LeftMargin
        {
            get;
            set;
        }

        float RightMargin
        {
            get;
            set;
        }


        void EndDoc();

        double GetTextHeight();

        double GetTextWidth(string strWidth);

        void PrintImage(System.IO.MemoryStream objMS);

        void PrintLine(double dblFromX, double dblFromY, double dblToX, double dblToY);

        void PrintText(string strPrintText);

        void PrintTextEndAt(string strPrintText);

        void PrintTextNoCr(string strPrintText);

        void Save(string strPDFPath, string strPDFPassword);

        void Save(string strPDFPath);

        void SetFont(string strFontName, double dblFontSize);

        void SetFontBold(bool blnIsBold);

        void StartDoc(bool blnIsLandscape);
    }
} // namespace