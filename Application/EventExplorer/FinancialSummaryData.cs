using System;

namespace Riskmaster.Application.EventExplorer
{
	/// <summary>
	/// Author  :   Vaibhav Kaushik
	/// Dated   :   1 Oct 2004 
	/// Purpose :   Represents Financial Summary Data.  
	/// </summary>	
	internal class FinancialSummaryData
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable for Code ID.
		/// </summary>
		private int m_iCodeID = 0 ;
		/// <summary>
		/// Private variable for Short Code.
		/// </summary>
		private string m_sShortCode = "" ;
		/// <summary>
		/// Private variable for Code Desc.
		/// </summary>
		private string m_sCodeDesc = "" ;		
		#endregion
		
		#region Constructors
		/// <summary>
		/// Default Constructor
		/// </summary>
		internal FinancialSummaryData()
		{						
		}
		#endregion

		#region Properties
		/// <summary>
		/// Read/Write property for CodeID.
		/// </summary>
		internal int CodeID 
		{
			get
			{
				return m_iCodeID;
			}
			set
			{
				m_iCodeID = value ;
			}
		}
		/// <summary>
		/// Read/Write property for ShortCode.
		/// </summary>
		internal string ShortCode 
		{
			get
			{
				return m_sShortCode;
			}
			set
			{
				m_sShortCode = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CodeDesc.
		/// </summary>
		internal string CodeDesc 
		{
			get
			{
				return m_sCodeDesc;
			}
			set
			{
				m_sCodeDesc = value ;
			}
		}
		#endregion
	}
}
