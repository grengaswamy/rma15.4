﻿
using System;
using System.Xml;
using System.Text ;
using System.Collections ;
using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Application.SupportScreens;
using Riskmaster.Application.ProgressNotes;
using Riskmaster.Security.Authentication;
using Riskmaster.Settings;
using System.Globalization;
using System.Threading;
using Riskmaster.Security;

namespace Riskmaster.Application.EventExplorer
{	
	///************************************************************** 
	///* $File		: EventExplorer.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 1 Oct 2004 
	///* $Author	: Vaibahv Kaushik
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************	
	/// <summary>
	/// To Generate RiskMaster Quick Summary Report.
	/// </summary>
	public class EventExplorerManager
	{	
		#region Constants
		private const string COMMA_STRING = "," ;
		private const string NAME_EQUAL = "name=" ;
		private const string EVENT_ID_EQUAL = "eventid=" ;
		private const string CLAIM_ID_EQUAL = "claimid=" ;
        private const string POLICY_ID_EQUAL = "policyid=";  //ijain4 11/09/2015
		private const string REQUESTED_INFO_ID_EQUAL = "subid=" ;
		private const string REQUESTED_INFO_CHILD_ID_EQUAL = "subid2=" ;
		private const string CLASS_EQUAL = "class=" ;
        private const string PARENTTABLENAME_EQUAL = "parenttable="; 
        private const string PARENTROWID_EQUAL = "parentrowid=";
		private const string LANGUAGE_CODE = "CODES_TEXT.LANGUAGE_CODE = 1033" ;
		private const string NODE = "node" ;
		private const string KEY = "key" ;
		private const string TEXT = "text" ;
		private const string EVENT_SUMMARY_NODE = "eventsummary" ;	
		private const string EVENT_NODE = "event" ;
		private const string EVENT_FINANCIALS_NODE = "eventfinancials" ;
		private const string EVENT_DIARIES_NODE = "eventdiaries" ;
		private const string EVENT_DETAILS_NODE = "eventdetails" ;
		private const string EVENT_FOLLOWUP_NODE = "eventfollowup" ;
		private const string EVENT_ACTIONS_NODE = "eventactions" ;
		private const string EVENT_OUTCOMES_NODE = "eventoutcomes" ;
		private const string EVENT_LOCATION_NODE = "eventlocation" ;
		private const string EVENT_OSHA_NODE = "eventosha" ;
		private const string EVENT_PERSONS_INVOLVED_NODE = "eventpersonsinvolved" ;
        private const string CLAIM_PERSONS_INVOLVED_NODE = "claimpersoninvolved";
        private const string POLICY_PERSONS_INVOLVED_NODE = "policypersoninvolved"; 
        private const string EVENT_PI_CLASS_NODE = "eventpiclass"; 
        private const string CLAIM_PI_CLASS_NODE = "claimpiclass";
        private const string POLICY_PI_CLASS_NODE = "policypiclass";
		private const string PI_NODE = "pi" ;
		private const string CLAIM_NODE = "claim" ;
		private const string NOTES_NODE = "notes" ;
        //Changed by Gagan for MITS 7658 : Start
        private const string CLAIM_ENHANCED_NOTES_NODE = "claimenhancednotes";
        private const string EVENT_ENHANCED_NOTES_NODE = "eventenhancednotes";        
        //Changed by Gagan for MITS 7658 : End
		private const string DIARIES_NODE = "diaries" ;
		private const string CURRENT_RESERVES_NODE = "currentreserves" ;
		private const string PAYMENT_HIST_NODE = "paymenthist" ;
        private const string CLAIMANT_PAYMENT_HIST_NODE = "claimantpaymenthist";//parijat :21784
		private const string BY_PAYEE_NODE = "bypayee" ;
        private const string CLAIMANT_BY_PAYEE_NODE = "claimantbypayee";//Parijat:21784
		private const string BY_RESERVE_NODE = "byreserve" ;
        private const string CLAIMANT_BY_RESERVE_NODE = "claimantbyreserve";//Parijat:21784
		private const string BY_BENINFIT_NODE = "bybenefit" ;
        private const string CLAIMANT_BY_BENINFIT_NODE = "claimantbybenefit";//Parijat:21784
		private const string BY_TRANS_TYPE_NODE = "bytranstype" ;
        private const string CLAIMANT_BY_TRANS_TYPE_NODE = "claimantbytranstype";//Parijat:21784
		private const string FUTURE_PAYMENTS_NODE = "futurepayments" ;
		private const string DEFENDANTS_NODE = "defendants" ;
		private const string DEFENDANT_NODE = "defendant" ;
		private const string ADJUSTERS_NODE = "adjusters" ;
		private const string ADJUSTER_NODE = "adjuster" ;
		private const string ATTACHED_POLICY_NODE = "attachedpolicy" ;
        //MITS # 29792 - 20121009 - bsharma33 - BR Id # 5.1.19 - added POLICY_COVERAGE_NODE
        private const string POLICY_COVERAGE_NODE = "policycoverage";
		private const string CLAIMANTS_NODE = "claimants" ;
		private const string CLAIMANT_NODE = "claimant" ;
		private const string CLAIMANT_FINANCIALS_NODE ="claimantfinancials" ;
        private const string CLAIMANT_ENHANCED_NOTES_NODE = "claimantenhancednotes";//Added by gbindra MITS#34104
		private const string UNITS_NODE = "units" ;

		private const string UNIT_NODE = "unit" ;
		private const string LITIGATIONS_NODE = "litigations" ;
		private const string LIT_DOCKET_NODE = "litdocket" ;
		private const string LIT_ATTORNEY_NODE = "litattorney" ;
		private const string LIT_JUDGE_NODE = "litjudge" ;
		private const string LIT_EXPERT_NODE = "litexpert" ;
		private const string PAYEE_INFO_NODE = "payeeinfo" ;
        // Ayush, MITS: 18291, Date: 01/12/10, Start:
        private const string CLAIM_PROPERTY_NODE = "claimpropertynode";
        private const string CLAIM_PROPERTY_COPE_NODE = "claimpropertycopenode";
        private const string CLAIM_PROPERTY_OP_COPE_NODE = "claimpropertyopcopenode";
        private const string CLAIM_PROPERTY_SCHD_NODE = "claimpropertyschdnode";
        // Ayush, MITS: 18291, Date: 01/12/10, End
		private const int GENERAL_CLAIM_BUS_TYPE_CODE = 241 ;
		private const int VEHICLE_CLAIM_BUS_TYPE_CODE = 242 ;
		private const int WORKERS_CLAIM_BUS_TYPE_CODE = 243 ;
		private const int SHORT_TERM_CLAIM_BUS_TYPE_CODE = 844 ;
        private const string sNoSort = "NoSort";//Added by Shivendu for MITS 18098
        private const string defaultSort = "DateEntered";//Parijat :19724 - Sorting has always to be by default by -"Activity Date"
        // Ayush, MITS: 18291, Date: 01/11/10, Start:
        private const int PROPERTY_CLAIM_BUS_TYPE_CODE = 845;
        // Ayush, MITS: 18291, Date: 01/11/10, End.
        //skhare7 R8 enhancement
        private const string SUBROGATIONS_NODE = "subrogations";
        private const string SUB_CLAIMNO_NODE = "subclaimno";
        private const string SUB_POLICYNO_NODE = "subpolicyno";
        private const string SUB_STATUS_NODE = "substatus";
        private const string SUB_TYPE_NODE = "subtype";
        private const string SUB_COMP_NODE = "subcomp";
        private const string SUB_SPECIALIST_NODE = "subspecialist";
        private const string SUB_ADJUS_NODE = "subadjuster";
        private const string SUB_ADVPARTY_NODE = "subadvparty";
        private const string SUB_STATUSDATE_NODE = "substatusdate";
        //arbitrations
        private const string ARBITRATIONS_NODE = "arbitrations";
        private const string ARB_CLAIMNO_NODE = "arbclaimno";
        private const string ARB_STATUS_NODE = "arbstatus";
        private const string ARB_TYPE_NODE = "arbtype";
        private const string ARB_DATEFILED_NODE = "arbdatefiled";
        //Liability
        private const string LIABILITY_NODE = "liability";
        private const string LIABILITY_TYPE_NODE = "arbclaimno";
        //Property Loss
        private const string PROPERTYLOSS_NODE = "propertyloss";
        private const string PROPERTYLOSS_TYPE_NODE = "propertylosstype";
        private const string PROPERTYLOSS_ESTDAMAGE_NODE = "estimateddamage";
        //salvage
        private const string SALVAGEPROP_NODE = "salvageprop";
        private const string SALVAGEUNIT_NODE = "salvageunit";
      
			
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store Dsn Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;						
		/// <summary>
		/// Private variable to store the instance of XmlDocument object
		/// </summary>
		private XmlDocument m_objDocument = null ;
		/// <summary>
		/// Private variable to store the instance of data node
		/// </summary>
		private XmlElement m_objData = null ;
		/// <summary>
		/// Private variable to store the instance of header node
		/// </summary>
		private XmlElement m_objHeader = null ;
		/// <summary>
		/// Private variable to store the instance of column node
		/// </summary>
		private XmlElement m_objCol = null ;
		/// <summary>
		/// Private variable to store the instance of rows node
		/// </summary>
		private XmlElement m_objRows = null ;
		/// <summary>
		/// Private variable to store the instance of row node
		/// </summary>
		private XmlElement m_objRow = null ;

		private static string m_sDBType = "";
        //Aman ML Change
        /// <summary>
        /// Private variable to store the language code
        /// </summary>
        private int m_iLangCode = 0;
        /// <summary>
        /// ClientID for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;

        public UserLogin m_UserLogin;
        /// <summary>
        /// Private variable to store person Involved Parent Table Name
        /// </summary>
        private string m_sParentTableName = string.Empty;
        //Aman ML Change
        #endregion 

		#region Constructors
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public EventExplorerManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;	//ash -cloud	
		}		
		

		#endregion 
        //Aman ML Change
        #region Properties
        public int LanguageCode
        {
            get 
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion
        //Aman ML Change
        #region GetData Functions
        /// <summary>
		/// Get the Query Data
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <param name="sKey">The Key</param>
        /// <param name="p_iClaimId">Claim Id</param>
		/// <returns>Required information in form of Xml Document.</returns>
        public XmlDocument GetData(int p_iEventID, string p_sKey, int p_iClaimId)  //tmalhotra2 MITS-29787 Re# 5.1.13 
		{
			XmlDocument objXmlDocument = null ;

			string sNodeName = "" ;
			string sClassName = "" ;
            int iClaimID = p_iClaimId;  //ijain4 11/09/2015
			int iRequestedInfoID = 0 ;
			int iRequestedInfoChildID = 0 ;
            string sParentTableName= "";
            int iParentRowId= 0;
			try
			{				
				m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName , m_sUserName , m_sPassword ,m_iClientId);	//sonali
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() ;

                SplitKey(p_sKey, ref sNodeName,  ref sClassName, ref iClaimID, ref iRequestedInfoID, ref iRequestedInfoChildID, ref sParentTableName, ref iParentRowId); 
                //tmalhotra2 MITS-29787 Re# 5.1.13
				switch( sNodeName )
				{
					case "" :
                    case EVENT_NODE:
                        objXmlDocument = GetEventData(p_iEventID, p_iClaimId);   //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_FINANCIALS_NODE:
                        objXmlDocument = GetCurrentFinancials("Event", p_iEventID, p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_DIARIES_NODE:
                        objXmlDocument = GetDiaries("Event", p_iEventID, p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_DETAILS_NODE:
                        objXmlDocument = GetEventDetails(p_iEventID, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_FOLLOWUP_NODE:
                        objXmlDocument = GetEventFollowUp(p_iEventID, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_ACTIONS_NODE:
                    case EVENT_OUTCOMES_NODE:
                        objXmlDocument = GetEventActionsOutcomes(p_iEventID, sNodeName, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_LOCATION_NODE:
                        objXmlDocument = GetEventLocation(p_iEventID, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_OSHA_NODE:
                        objXmlDocument = GetEventOsha(p_iEventID, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case EVENT_PERSONS_INVOLVED_NODE:
                        objXmlDocument = GetEventPersonInvolved(p_iEventID, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    // case PI_CLASS_NODE: 
                    case EVENT_PI_CLASS_NODE:
                    case CLAIM_PI_CLASS_NODE:
                    case POLICY_PI_CLASS_NODE:
                      //  objXmlDocument = GetEventPIClass(p_iEventID, sNodeName, sClassName, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13                        
                        objXmlDocument = GetPersonInvolvedNodeDetails(p_iEventID, sNodeName, sClassName,sParentTableName, iParentRowId); 
                        break;
                    case PI_NODE:
                        objXmlDocument = GetEventPI(p_iEventID, sNodeName, sClassName, iRequestedInfoID, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case CLAIM_NODE:
                        objXmlDocument = GetClaimDetails(iClaimID);
                        break;
                    //Changed by Gagan for MITS 7658 : Start
                    case EVENT_ENHANCED_NOTES_NODE:
                        objXmlDocument = GetEventEnhancedNotes(p_iEventID, p_iClaimId); //tmalhotra2 MITS-29787 Re# 5.1.13
                        break;
                    case CLAIM_ENHANCED_NOTES_NODE:
                        objXmlDocument = GetClaimEnhancedNotes(p_iEventID, iClaimID);
                        break;
                    //Changed by Gagan for MITS 7658 : End
                    case NOTES_NODE:
                        objXmlDocument = GetClaimNotes(iClaimID);
                        break;
                    case DIARIES_NODE:
                        objXmlDocument = GetDiaries("Claim", iClaimID, p_iClaimId);
                        break;
                    case CURRENT_RESERVES_NODE:
                        objXmlDocument = GetCurrentFinancials("Claim", iClaimID, p_iClaimId);
                        break;
					case PAYMENT_HIST_NODE :
						objXmlDocument = GetClaimPaymentHistory( iClaimID ) ;
						break ;
                    case CLAIMANT_PAYMENT_HIST_NODE://Parijat 21784
                        objXmlDocument = GetClaimPaymentHistory(iClaimID, iRequestedInfoID);
						break ;
					case BY_PAYEE_NODE :
						objXmlDocument = GetClaimByPayee( iClaimID ) ;
						break ;
                    case CLAIMANT_BY_PAYEE_NODE://parijat 21784
                        objXmlDocument = GetClaimByPayee(iClaimID, iRequestedInfoID);
                        break;
					case BY_RESERVE_NODE :
						objXmlDocument = GetClaimByReserve( iClaimID ) ;
						break ;
                    case CLAIMANT_BY_RESERVE_NODE://parijat 21784
                        objXmlDocument = GetClaimByReserve(iClaimID, iRequestedInfoID);
                        break;
					case BY_BENINFIT_NODE :
						objXmlDocument = GetClaimByBenefit( iClaimID ) ;
						break ;
                    case CLAIMANT_BY_BENINFIT_NODE://parijat 21784
                        objXmlDocument = GetClaimByBenefit(iClaimID, iRequestedInfoID);
                        break;
					case BY_TRANS_TYPE_NODE :
						objXmlDocument = GetClaimByTransType( iClaimID ) ;
						break ;
                    case CLAIMANT_BY_TRANS_TYPE_NODE://parijat 21784
                        objXmlDocument = GetClaimByTransType(iClaimID, iRequestedInfoID);
                        break;
					case FUTURE_PAYMENTS_NODE :
						objXmlDocument = GetClaimFuturePayments( iClaimID ) ;
						break ;
					case CLAIMANTS_NODE :
					case DEFENDANTS_NODE :
                    case CLAIM_PERSONS_INVOLVED_NODE: // pgupta215  JIra  RMA-15677 
                    case POLICY_PERSONS_INVOLVED_NODE:                
					case ADJUSTERS_NODE :
						objXmlDocument = GetClaimPartiesInvolved( p_iClaimId , sNodeName, sParentTableName, iParentRowId) ; //11/12/2015 ijain4
						break ;
					case UNITS_NODE :
						objXmlDocument = GetClaimPropertiesInvolved( iClaimID ) ;
						break ;
					case CLAIMANT_NODE : 
						objXmlDocument = GetClaimClaimant( iRequestedInfoID ) ;
						break ;
                    // Manish added claim id parameter to the function
					case DEFENDANT_NODE :
                        objXmlDocument = GetClaimDefendant(iRequestedInfoID, p_iClaimId);
						break ;
					case ADJUSTER_NODE :
						objXmlDocument = GetClaimAdjuster( iClaimID , iRequestedInfoID );
						break ;
                        // Manish added claim id parameter to the function
					case UNIT_NODE :
                        objXmlDocument = GetClaimUnitInvolved(iClaimID , iRequestedInfoID);
						break ;
					case CLAIMANT_FINANCIALS_NODE :
						objXmlDocument = GetCurrentFinancials( "Claimant" , iClaimID , iRequestedInfoID, p_iClaimId);
						break ;
					case ATTACHED_POLICY_NODE :
						objXmlDocument = GetClaimAttachedPolicy( iClaimID , iRequestedInfoID );
						break ;
                    //MITS # 29792 - 20121009 - bsharma33 - BR Id # 5.1.19 - start
                    case POLICY_COVERAGE_NODE:
                        objXmlDocument = GetPolicyCoverages(iClaimID);
                        break;
                    //MITS # 29792 - 20121009 - bsharma33 - BR Id # 5.1.19 - end
					case LITIGATIONS_NODE :
						objXmlDocument = GetClaimLitigations( iClaimID );
						break ;
					case LIT_DOCKET_NODE :
						objXmlDocument = GetClaimLitDocket( iClaimID , iRequestedInfoID );
						break ;
					case LIT_ATTORNEY_NODE :
						objXmlDocument = GetClaimLitAttorney( iClaimID , iRequestedInfoID );
						break ;
					case LIT_JUDGE_NODE :
						objXmlDocument = GetClaimLitJudge( iClaimID , iRequestedInfoID );
						break ;
					case LIT_EXPERT_NODE :
						objXmlDocument = GetClaimLitExpert( iClaimID , iRequestedInfoID , iRequestedInfoChildID );
						break ;
                    // Ayush, MITS: 18291, Date: 01/12/10, Start:
                    case CLAIM_PROPERTY_NODE:
                        objXmlDocument = GetClaimPropertyData(iClaimID, sNodeName, iRequestedInfoID);
                        break;
                    case CLAIM_PROPERTY_COPE_NODE:
                        objXmlDocument = GetClaimPropertyData(iClaimID, sNodeName, iRequestedInfoID);
                        break;
                    case CLAIM_PROPERTY_OP_COPE_NODE:
                        objXmlDocument = GetClaimPropertyData(iClaimID, sNodeName, iRequestedInfoID);
                        break;
                    case CLAIM_PROPERTY_SCHD_NODE:
                        objXmlDocument = GetClaimPropertyData(iClaimID, sNodeName, iRequestedInfoID);
                        break;
                    // Ayush, MITS: 18291, Date: 01/12/10, End
                        //skhare7 R8 enahncement

                    case SUBROGATIONS_NODE:
                        objXmlDocument = GetClaimSubrogations(iClaimID);
                        break;
                    case SUB_CLAIMNO_NODE:
                    case SUB_POLICYNO_NODE:
                    case SUB_STATUSDATE_NODE:
                    case SUB_STATUS_NODE:
                    case SUB_TYPE_NODE:
                        objXmlDocument = GetClaimSubrogationDetails(iClaimID, iRequestedInfoID);
                        break;
                    case ARBITRATIONS_NODE:
                        objXmlDocument = GetClaimArbitrations(iClaimID);
                        break;
                    case ARB_STATUS_NODE:
                    case ARB_TYPE_NODE:
                    case ARB_DATEFILED_NODE:
                        objXmlDocument = GetClaimArbitrationDetails(iClaimID, iRequestedInfoID);
                        break;
                    case LIABILITY_NODE:
                        objXmlDocument = GetClaimLiability(iClaimID);
                        break;
                    case LIABILITY_TYPE_NODE:
                        objXmlDocument = GetClaimLiabilityDetails(iClaimID, iRequestedInfoID);
                        break;
                    case PROPERTYLOSS_NODE:
                        objXmlDocument = GetClaimPropertyLoss(iClaimID);
                        break;
                    case PROPERTYLOSS_TYPE_NODE:
                        objXmlDocument = GetClaimPropertyLossDetails(iClaimID, iRequestedInfoID);
                        break;
                         
                          case SALVAGEPROP_NODE:
                         objXmlDocument = GetClaimPropertyLossSalavegDetails(iClaimID);
                        break;
                          case SALVAGEUNIT_NODE:
                        objXmlDocument = GetClaimUnitSalavageDetails(iClaimID);
                        break;

                    // tmalhotra2 Req. # 5.1.13 end
					/*Added by gbindra MITS#34104*/
                    case CLAIMANT_ENHANCED_NOTES_NODE:
                        objXmlDocument = GetClaimantEnhancedNotes(p_iEventID, iClaimID, iRequestedInfoID);
                        break;
                    /*Added by gbindra MITS#34104 END*/
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetData.DataError",m_iClientId) , p_objEx );//sonali
			}	
			finally
			{
				CleanUp();
			}
			return( objXmlDocument );
		}		
		/// <summary>
		/// Get the data for an Event.
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
        /// <param name="p_iClaimID">Claimant ID</param>
		/// <returns>Event data in form of a Xmldocument</returns>
        private XmlDocument GetEventData(int p_iEventID, int p_iClaimID) //tmalhotra2 MITS-29787 Re# 5.1.13
        {
            Event objEvent = null;
            //Claimant objClaimant = null;
            LocalCache objLocalCache = null;
            //tmalhotra2 MITS-29787 Re# 5.1.13 - TODO: Remove comment if claimant name required is required here
            //string ClaimantName = GetClaimants(p_iClaimID);
			string[] arrEventDataColumn = { 
											  "Claim Number" 
											  ,"Status" 
											  ,"Claim Date"
											  ,"Policy Number" 
											  ,"Policy Dates"
                                              //MITS # 29792 - 20121009 - bsharma33 - BR Id # 5.1.19
                                              ,"Claimant" 
										  } ;

			string sShortCode = "" ;
			string sCodeDesc =  "" ;
			
			try
			{
				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );//sonali

				StartDocument( "Event Details - " + "Claim for Event: " + objEvent.EventNumber , arrEventDataColumn );

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

				foreach( Claim objClaim in objEvent.ClaimList )
				{				 					
					objLocalCache.GetCodeInfo( objClaim.ClaimStatusCode ,ref sShortCode , ref sCodeDesc );

					arrEventDataColumn[0] = objClaim.ClaimNumber ;
					arrEventDataColumn[1] = sShortCode + " " + sCodeDesc ;
					arrEventDataColumn[1] = arrEventDataColumn[1].Trim();
					arrEventDataColumn[2] = Conversion.GetDBDateFormat( objClaim.DateOfClaim , "d" );
					arrEventDataColumn[3] = objClaim.PrimaryPolicy.PolicyNumber ;
					arrEventDataColumn[4] = Conversion.GetDBDateFormat( objClaim.PrimaryPolicy.EffectiveDate , "d" ) + " - " + Conversion.GetDBDateFormat( objClaim.PrimaryPolicy.ExpirationDate , "d" ) ;
                    arrEventDataColumn[5] = GetClaimants(objClaim.ClaimId);
                    AddRow( arrEventDataColumn );
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventData.DataError",m_iClientId) , p_objEx );//sonali
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
				arrEventDataColumn = null ;
			}
			return( m_objDocument );
		}
        //tmalhotra2 MITS-29787 Re# 5.1.13 Req. # 5.1.13 start
		/// <summary>
		/// Get the data for Current Financials
		/// </summary>
		/// <param name="p_sType">Node Type</param>
		/// <param name="p_iEventID">Event ID</param>
        private XmlDocument GetCurrentFinancials(string p_sType, int p_iID, int p_iClaimId)
		{
            return (GetCurrentFinancials(p_sType, p_iID, 0, p_iClaimId)); //tmalhotra2 MITS-29787 Re# 5.1.13
		}
		/// <summary>
		/// Get the data for Current Financials
		/// </summary>
		/// <param name="p_sType">Node Type</param>
		/// <param name="p_iEventID">Event ID</param>
        /// <param name="p_iClaimantID">Claimant ID</param>
		/// <returns>Return the Current Financial information in form of Xmldocument</returns>
        private XmlDocument GetCurrentFinancials(string p_sType, int p_iID, int p_iClaimantID, int p_iClaimId) //tmalhotra2 MITS-29787 Re# 5.1.13
		{
			Event objEvent = null ;
			Claim objClaim = null ;
			FinancialSummary objFinancialSummary = null ;
			FinancialSummaryItemList objFinancialSummaryItemList = null ;

			string[] arrCurrentFinancialsColumn = {  "Type" , "Outstanding" , "Payments" , "Collections" , "Incurred" } ;

			bool bFinancials = false ;
			string sTitle = "" ;
            //string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13
			double dblPaidTotal = 0.0 ;
			double dblCollectedTotal = 0.0 ;
			double dblIncurredTotal = 0.0 ;
			double dblOutstandingTotal = 0.0 ;
			
			try
			{
				objFinancialSummary = new FinancialSummary( m_sConnectionString, m_iClientId );

                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iID, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
				switch( p_sType )
				{
					case "Event" :
						// Get the reference of an event having event_id p_iEventID
						objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
						if( p_iID > 0 )
							objEvent.MoveTo( p_iID );
						else
							throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );//sonali
					
						sTitle = "Current Financials for Event - " + objEvent.EventNumber ;//MITS 34830 srajindersin 1/7/2014
					
						if( objEvent.ClaimList.Count > 0 )
						{
							StartDocument( sTitle , arrCurrentFinancialsColumn );
							foreach( Claim objClaimTemp in objEvent.ClaimList )
							{
								objFinancialSummaryItemList = objFinancialSummary.GetListForClaim( objClaimTemp.ClaimId );
                                CreateCurrentFinancialDoc(objFinancialSummaryItemList, ref bFinancials, ref dblPaidTotal, ref dblCollectedTotal, ref dblIncurredTotal, ref dblOutstandingTotal, objClaimTemp.ClaimId);
							}						
						}					
						break;

					case "Claim" :
                         string ClaimantName = GetClaimants(p_iID);  //tmalhotra2 MITS-29787 Re# 5.1.13
						// Get the reference of an claim having claim_id p_iClaimID
						objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );				
						if( p_iID > 0 )
							objClaim.MoveTo( p_iID );
						else
							throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );

                        //MITS 34830 srajindersin 1/7/2014
						sTitle = "Current Financials for Claim - " + objClaim.ClaimNumber + " * "  + ClaimantName;  //tmalhotra2 MITS-29787 Re# 5.1.13
						StartDocument( sTitle , arrCurrentFinancialsColumn );
					
						objFinancialSummaryItemList = objFinancialSummary.GetListForClaim( p_iID );
                        CreateCurrentFinancialDoc(objFinancialSummaryItemList, ref bFinancials, ref dblPaidTotal, ref dblCollectedTotal, ref dblIncurredTotal, ref dblOutstandingTotal, p_iID);
						break;
					case "Claimant" :
						sTitle = "Current Financials for Claimant" ;
						StartDocument( sTitle , arrCurrentFinancialsColumn );
					
						objFinancialSummaryItemList = objFinancialSummary.GetListForClaimant( p_iID , p_iClaimantID );
                        CreateCurrentFinancialDoc(objFinancialSummaryItemList, ref bFinancials, ref dblPaidTotal, ref dblCollectedTotal, ref dblIncurredTotal, ref dblOutstandingTotal, p_iID);
					
						break;
				}	
				// if financials are there.
				if( bFinancials )
				{
					arrCurrentFinancialsColumn[0] = "---------------" ; 
					arrCurrentFinancialsColumn[1] = "---------------" ;
					arrCurrentFinancialsColumn[2] = "---------------" ;
					arrCurrentFinancialsColumn[3] = "---------------" ;
					arrCurrentFinancialsColumn[4] = "---------------" ;
					AddRow( arrCurrentFinancialsColumn );

					arrCurrentFinancialsColumn[0] = "Total" ;
					arrCurrentFinancialsColumn[1] = string.Format("{0:C}" , dblOutstandingTotal ) ;
					arrCurrentFinancialsColumn[2] = string.Format("{0:C}" , dblPaidTotal ) ;
					arrCurrentFinancialsColumn[3] = string.Format("{0:C}" , dblCollectedTotal ) ;
					arrCurrentFinancialsColumn[4] = string.Format("{0:C}" , dblIncurredTotal ) ;
					AddRow( arrCurrentFinancialsColumn );
				}
					// if there is no financials.
				else  
				{
					arrCurrentFinancialsColumn[0] = "" ; 
					arrCurrentFinancialsColumn[1] = "" ;
					arrCurrentFinancialsColumn[2] = "" ;
					arrCurrentFinancialsColumn[3] = "" ;
					arrCurrentFinancialsColumn[4] = "" ;
					StartDocument( sTitle , arrCurrentFinancialsColumn );

					arrCurrentFinancialsColumn[0] = "No Financial Activity for selected item" ;
					AddRow( arrCurrentFinancialsColumn );
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}			
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetCurrentFinancials.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}				
				objFinancialSummary = null ;
				objFinancialSummaryItemList = null ;
				arrCurrentFinancialsColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Create XML doc for a particular Financial Summary ItemList
		/// </summary>
		/// <param name="p_objFinancialSummaryItemList">Financial Summary Item List </param>
		/// <param name="p_bFinancials">Flag for checking at least on Financial</param>
		/// <param name="p_dblPaidTotal">Paid Total</param>
		/// <param name="p_dblCollectedTotal">Collected Total</param>
		/// <param name="p_dblIncurredTotal">Incurred Total</param>
		/// <param name="p_dblOutstandingTotal">Outstanding Total</param>	
		private void CreateCurrentFinancialDoc( FinancialSummaryItemList p_objFinancialSummaryItemList , ref bool p_bFinancials , ref double p_dblPaidTotal , ref double p_dblCollectedTotal , ref double p_dblIncurredTotal , ref double p_dblOutstandingTotal,int p_Claimid )
		{
			string[] arrCurrentFinancialsColumn = { "" , "" , "" , "" , "" };
			int iLastFinancialTypeID = 0 ;
			string sDescription = "" ;
			
			try
			{
				foreach( FinancialSummaryItem objFinancialSummaryItem in p_objFinancialSummaryItemList )
				{
					if( objFinancialSummaryItem.ReserveExists )
					{
						p_bFinancials = true ;
						if( iLastFinancialTypeID != 0 )
						{
							if( iLastFinancialTypeID == objFinancialSummaryItem.FinancialTypeId )
								sDescription = "--------------------" ;
							else
								sDescription = objFinancialSummaryItem.FinancialTypeDesc ;
						}
						else
						{
							sDescription = objFinancialSummaryItem.FinancialTypeDesc ;
						}
						iLastFinancialTypeID = objFinancialSummaryItem.FinancialTypeId ;

                        //Manish for multi currency
                        string culture = CommonFunctions.GetCulture(p_Claimid, CommonFunctions.NavFormType.None, m_sConnectionString);
                        Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
									
						arrCurrentFinancialsColumn[0] = sDescription ;
						arrCurrentFinancialsColumn[1] = string.Format("{0:C}" , objFinancialSummaryItem.OutstandingTotal ); 			
						arrCurrentFinancialsColumn[2] = string.Format("{0:C}" , objFinancialSummaryItem.PaymentsTotal ); 			
						arrCurrentFinancialsColumn[3] = string.Format("{0:C}" , objFinancialSummaryItem.CollectionsTotal ); 			
						arrCurrentFinancialsColumn[4] = string.Format("{0:C}" , objFinancialSummaryItem.IncurredTotal ); 
						AddRow( arrCurrentFinancialsColumn );
									
						p_dblPaidTotal += objFinancialSummaryItem.PaymentsTotal  ;
						p_dblCollectedTotal += objFinancialSummaryItem.CollectionsTotal ;
						p_dblIncurredTotal += objFinancialSummaryItem.IncurredTotal ;
						p_dblOutstandingTotal += objFinancialSummaryItem.OutstandingTotal ;
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.CreateCurrentFinancialDoc.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				arrCurrentFinancialsColumn = null ;
			}
		}

		/// <summary>
		/// Get the data for diaries.
		/// </summary>
		/// <param name="p_sType">The type of diary ( either "Event" or "Claim" )</param>
		/// <param name="iID">ID of Type </param>
		/// <returns>Diary data in form of a Xmldocument</returns>
        private XmlDocument GetDiaries(string p_sType, int p_iID, int p_iClaimId)
		{
			Event objEvent = null ;
			Claim objClaim = null ;
			DbReader objReader = null ;
			StringBuilder sbSQL ;
            //string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13

			string sTitle = "" ;			
			string[] arrDiariesColumn = {
											"Activity" 
											,"Status"
											,"Priority" 
											,"Assigned User"
											,"Date Created" 
											,"Due Date"
											,"Notes"
										};
			try
			{
				switch( p_sType )
				{
					case "Event" :
						// Get the reference of an event having event_id p_iID
						objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
						if( p_iID > 0 )
							objEvent.MoveTo( p_iID );
						else
							throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );//sonali

						sTitle = "Diary(s) for " + p_sType + " - " + objEvent.EventNumber ;//MITS 34830 srajindersin 1/7/2014
						break ;
					case "Claim"  :
						// Get the reference of a claim having claim_id p_iID
                        string ClaimantName = GetClaimants(p_iID);  //tmalhotra2 MITS-29787 Re# 5.1.13
						objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
						if( p_iID > 0 )
							objClaim.MoveTo( p_iID );
						else
							throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );

                        sTitle = "Diary(s) for " + p_sType + " - " + objClaim.ClaimNumber + " * " + ClaimantName;//MITS 34830 srajindersin 1/7/2014
						break ;
				}

				StartDocument( sTitle , arrDiariesColumn );				
				
				sbSQL = new StringBuilder() ;	

				sbSQL.Append ( "SELECT * FROM WPA_DIARY_ENTRY WHERE ATTACH_RECORDID = " + p_iID + " AND ATTACH_TABLE='" ) ;
				sbSQL.Append ( p_sType.ToUpper() + "'" + " AND DIARY_DELETED=0 AND DIARY_VOID=0" ) ;

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );

				if ( objReader != null )
				{
					while( objReader.Read() )
					{
						arrDiariesColumn[0] = objReader.GetString( "ENTRY_NAME" ) ;
					
						if( objReader.GetBoolean( "STATUS_OPEN" ) )
							arrDiariesColumn[1] = "Open" ;
						else
							arrDiariesColumn[1] = "Closed" ;
					
						switch( objReader.GetInt( "PRIORITY" ) )
						{
							case 1 :
								arrDiariesColumn[2] = "Optional" ;
								break ;
							case 2 :
								arrDiariesColumn[2] = "Important" ;
								break ;
							case 3 :
								arrDiariesColumn[2] = "Required" ;
								break ;
						}
					
						arrDiariesColumn[3] = objReader.GetString( "ASSIGNED_USER" );
						arrDiariesColumn[4] = Conversion.GetDBDateFormat( objReader.GetString( "CREATE_DATE" ) , "d" );
						arrDiariesColumn[5] = Conversion.GetDBDateFormat( objReader.GetString( "COMPLETE_DATE" ) , "d" );
						arrDiariesColumn[6] = objReader.GetString( "ENTRY_NOTES" );

						AddRow( arrDiariesColumn );
					}
				}				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetDiaries.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				sbSQL = null ;
				arrDiariesColumn = null ;

				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( m_objDocument );

		}
		/// <summary>
		/// Get the data for event details
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <returns>Event detail data in form of a Xmldocument</returns>
        private XmlDocument GetEventDetails(int p_iEventID, int p_iClaimId)
		{
			Event objEvent = null ;
			Entity objEntity = null ;
			Entity objEntityReporter = null ;
			
			string[] arrEventDetailsColumn = { "" ,"" };
           // string ClaimantName = GetClaimants(p_iClaimId);  //MITS 35322 Prashant-Commented,as variable is not used in the current block
			string sDeptLastName = "" ;
			string sDeptInvolveLastName = null ;
			string sLastFirstName = "" ;
			bool bEntityFound = false ;
			bool bReporterFound = false ;	


			try
			{
				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );				
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );

				StartDocument( "Event Details - " + objEvent.EventNumber , arrEventDetailsColumn );
							
				objEntity  = ( Entity )m_objDataModelFactory.GetDataModelObject( "Entity" , false );
				
				try
				{
					objEntity.MoveTo( objEvent.DeptEid );
					bEntityFound = true ;
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sDeptLastName = objEntity.LastName ;
				
				try
				{
					objEntity.MoveTo( objEvent.DeptInvolvedEid );
					bEntityFound = true ;
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sDeptInvolveLastName = objEntity.LastName ;
								
				arrEventDetailsColumn[0] = "Department:" ;
				arrEventDetailsColumn[1] = sDeptLastName ;
				AddRow( arrEventDetailsColumn );

				arrEventDetailsColumn[0] = "Event Date:" ;
				arrEventDetailsColumn[1] = Conversion.GetDBDateFormat( objEvent.DateOfEvent , "d") ;
				AddRow( arrEventDetailsColumn );

				arrEventDetailsColumn[0] = "Date Reported:" ;
				arrEventDetailsColumn[1] = Conversion.GetDBDateFormat( objEvent.DateReported , "d") ;
				AddRow( arrEventDetailsColumn );

				arrEventDetailsColumn[0] = "Event Status:" ;
				arrEventDetailsColumn[1] = GetCodeDescription( objEvent.EventStatusCode );
				AddRow( arrEventDetailsColumn );

				arrEventDetailsColumn[0] = "Event Type:" ;
				arrEventDetailsColumn[1] = GetCodeDescription( objEvent.EventTypeCode );
				AddRow( arrEventDetailsColumn );

				arrEventDetailsColumn[0] = "Event Indicator:" ;
				arrEventDetailsColumn[1] = GetCodeDescription( objEvent.EventIndCode );
				AddRow( arrEventDetailsColumn );

				arrEventDetailsColumn[0] = "Department Involved:" ;
				arrEventDetailsColumn[1] = sDeptInvolveLastName ;
				AddRow( arrEventDetailsColumn );

				arrEventDetailsColumn[0] = "Cause Code:" ;
				arrEventDetailsColumn[1] = GetCodeDescription( objEvent.CauseCode );
				AddRow( arrEventDetailsColumn );
				
				try
				{
					objEntityReporter = objEvent.ReporterEntity ;
					bReporterFound = true ;					
				}
				catch
				{
					bReporterFound = false ;
				}
				if( bReporterFound )
					sLastFirstName = objEntityReporter.GetLastFirstName(); 
				else
					sLastFirstName = "" ;
				
				arrEventDetailsColumn[0] = "Reported By:" ;
				arrEventDetailsColumn[1] = sLastFirstName ;
				AddRow( arrEventDetailsColumn );
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventDetails.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				if( objEntityReporter != null )
				{
					objEntityReporter.Dispose();
					objEntityReporter = null ;
				}
				arrEventDetailsColumn = null ;
			}
			return( m_objDocument );
		}

		/// <summary>
		/// Get the data for event follow up
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <returns>Event FollowUp data in form of a Xmldocument</returns>
        private XmlDocument GetEventFollowUp(int p_iEventID, int p_iClaimId)
		{
			Event objEvent = null ;
			
			string[] arrEventFollowUpColumn = { "" , "" };
            //string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13
			try
			{
				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );

                StartDocument("Follow Up Details - " + objEvent.EventNumber, arrEventFollowUpColumn);//MITS 34830 srajindersin 1/7/2014

				arrEventFollowUpColumn[0] = "Follow Up Date:" ;
				arrEventFollowUpColumn[1] = Conversion.GetDBDateFormat( objEvent.DateToFollowUp , "d") ;
				AddRow( arrEventFollowUpColumn ) ;

				arrEventFollowUpColumn[0] = "Date Physician Advised:" ;
				arrEventFollowUpColumn[1] = Conversion.GetDBDateFormat( objEvent.DatePhysAdvised , "d") ;
				AddRow( arrEventFollowUpColumn ) ;
			    
				arrEventFollowUpColumn[0] = "Date Carrier Notified:" ;
				arrEventFollowUpColumn[1] = Conversion.GetDBDateFormat( objEvent.DateCarrierNotif , "d") ;
				AddRow( arrEventFollowUpColumn ) ;

				arrEventFollowUpColumn[0] = "Treatment Given:" ;
				arrEventFollowUpColumn[1] = objEvent.TreatmentGiven ? "Yes" : "No" ;			 
				AddRow( arrEventFollowUpColumn ) ;

				arrEventFollowUpColumn[0] = "Release Signed:" ;
				arrEventFollowUpColumn[1] = objEvent.ReleaseSigned ? "Yes" : "No" ;
				AddRow( arrEventFollowUpColumn ) ;
			    
				arrEventFollowUpColumn[0] = "Department Head Advised:" ;
				arrEventFollowUpColumn[1] = objEvent.DeptHeadAdvised ? "Yes" : "No" ;
				AddRow( arrEventFollowUpColumn ) ;

				arrEventFollowUpColumn[0] = "Physician Notes:" ;
				arrEventFollowUpColumn[1] = objEvent.PhysNotes ;
				AddRow( arrEventFollowUpColumn ) ;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventFollowUp.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				arrEventFollowUpColumn = null ;															
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for Event Actions OR Event Outcomes
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>Event Actions or Event Outcomes data in form of a Xmldocument</returns>
        private XmlDocument GetEventActionsOutcomes(int p_iEventID, string p_sNodeName, int p_iClaimId)
		{
			Event objEvent = null ;
			DbConnection objConnection  = null ;
			
			string sCaption = "" ;
			string[] arrEventActionOutcomesColumn = { "" } ;
			string sSQL = "" ;
            string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13
			switch( p_sNodeName )
			{
				case EVENT_ACTIONS_NODE :
					sCaption = "Actions" ;
					break;
				case EVENT_OUTCOMES_NODE :
					sCaption = "Outcomes" ;
					break;
			}
			
			try
			{
				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId));
			
				arrEventActionOutcomesColumn[0] = sCaption ;
				StartDocument( sCaption + " for Event - " + objEvent.EventNumber , arrEventActionOutcomesColumn );
				
				objConnection = DbFactory.GetDbConnection( m_sConnectionString );

				switch( p_sNodeName )
				{
					case EVENT_ACTIONS_NODE :
						try
						{							
							objConnection.Open();
							foreach( System.Collections.DictionaryEntry objCodeID in objEvent.EventXAction )
							{	
								if (objConnection.DatabaseType.Equals(eDatabaseType.DBMS_IS_ORACLE))
								{
									sSQL = "SELECT CODES.SHORT_CODE || '  ' || CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.CODE_ID = " + objCodeID.Value + " AND " + LANGUAGE_CODE ;
								}
								else
								{
									sSQL = "SELECT CODES.SHORT_CODE + '  ' + CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.CODE_ID = " + objCodeID.Value + " AND " + LANGUAGE_CODE ;
								}
								arrEventActionOutcomesColumn[0] = FetchValueFromDatabase( sSQL , objConnection );
								AddRow( arrEventActionOutcomesColumn  );
							}							
						}
						catch( DataModelException p_objEx )
						{
							throw p_objEx ;
						}
						catch( RMAppException p_objEx )
						{
							throw p_objEx ;
						}
						catch( Exception p_objEx )
						{
							throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventActionsOutcomes.EventActionsDataError",m_iClientId) , p_objEx );
						}
						finally
						{
							objConnection.Close();
							objConnection.Dispose();
						}
						break;
					case EVENT_OUTCOMES_NODE :
						try
						{
							objConnection.Open();

							foreach( System.Collections.DictionaryEntry objCodeID in objEvent.EventXOutcome )
							{
								if (objConnection.DatabaseType.Equals(eDatabaseType.DBMS_IS_ORACLE))
								{
									sSQL = "SELECT CODES.SHORT_CODE || '  ' || CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.CODE_ID = " + objCodeID.Value + " AND " + LANGUAGE_CODE ;
								}
								else
								{
									sSQL = "SELECT CODES.SHORT_CODE + '  ' + CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.CODE_ID = " + objCodeID.Value + " AND " + LANGUAGE_CODE ;
								}
								arrEventActionOutcomesColumn[0] = FetchValueFromDatabase( sSQL , objConnection );
								AddRow( arrEventActionOutcomesColumn  );
							}
						}
						catch( DataModelException p_objEx )
						{
							throw p_objEx ;
						}
						catch( RMAppException p_objEx )
						{
							throw p_objEx ;
						}
						catch( Exception p_objEx )
						{
							throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventActionsOutcomes.EventOutcomesDataError",m_iClientId) , p_objEx );
						}
						finally
						{
							objConnection.Close();
							objConnection.Dispose();
						}
						break;
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventActionsOutcomes.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if ( objConnection != null )
				{
					objConnection.Close();
					objConnection.Dispose();					
				}
				arrEventActionOutcomesColumn = null ;		
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for Event Locations
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <returns>Event Locations data in form of a Xmldocument</returns>
        private XmlDocument GetEventLocation(int p_iEventID, int p_iClaimId)
		{
			Event objEvent = null ;
			State objState = null ;
			
			string[] arrEventLocationColumn = { "" , "" } ;
            //string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13

			string sStateName = "" ;
			bool bStateFound = false ;

			try
			{
				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );

				StartDocument( "Location Details - " + objEvent.EventNumber , arrEventLocationColumn );
				
				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );
				try
				{
					objState.MoveTo( objEvent.StateId );
					bStateFound = true ;
				}
				catch
				{
					bStateFound = false ;
				}
				if( bStateFound )
					sStateName = objState.StateName ;

				arrEventLocationColumn[0] = "Location Type:" ;
				arrEventLocationColumn[1] = GetCodeDescription( objEvent.LocationTypeCode );
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "Primary Location" ;
				arrEventLocationColumn[1] = GetCodeDescription( objEvent.PrimaryLocCode );
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "On Premise" ;
				arrEventLocationColumn[1] = objEvent.OnPremiseFlag ? "Yes" : "No" ;
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "Location Address 1:" ;
				arrEventLocationColumn[1] = objEvent.Addr1 ;
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "Location Address 2:" ;
				arrEventLocationColumn[1] = objEvent.Addr2 ;
				AddRow( arrEventLocationColumn ) ;

                arrEventLocationColumn[0] = "Location Address 3:";
                arrEventLocationColumn[1] = objEvent.Addr3;
                AddRow(arrEventLocationColumn);

                arrEventLocationColumn[0] = "Location Address 4:";
                arrEventLocationColumn[1] = objEvent.Addr4;
                AddRow(arrEventLocationColumn);

				arrEventLocationColumn[0] = "City:" ;
				arrEventLocationColumn[1] = objEvent.City ;
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "State:" ;
				arrEventLocationColumn[1] = sStateName ;
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "Zipcode:" ;
				arrEventLocationColumn[1] = objEvent.ZipCode ;
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "County of Injury:" ;
				arrEventLocationColumn[1] = objEvent.CountyOfInjury ;
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "Country:" ;
				arrEventLocationColumn[1] = GetCodeDescription( objEvent.CountryCode );
				AddRow( arrEventLocationColumn ) ;

				arrEventLocationColumn[0] = "Location Desc:" ;
				arrEventLocationColumn[1] = objEvent.LocationAreaDesc ;
				AddRow( arrEventLocationColumn ) ;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventLocation.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				arrEventLocationColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for Event OSHA
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <returns>Event OSHA data in form of a Xmldocument</returns>
        private XmlDocument GetEventOsha(int p_iEventID, int p_iClaimId)
		{
			Event objEvent = null ;
			
			string[] arrEventOshaColumn = { "" , "" } ;
            //string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13

			try
			{
				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );				
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );

				StartDocument( "OSHA Details - " + objEvent.EventNumber , arrEventOshaColumn );

				arrEventOshaColumn[0] = "Recordable:" ;
				arrEventOshaColumn[1] = objEvent.EventOsha.RecordableFlag ? "Yes" : "No" ;
				AddRow( arrEventOshaColumn );

				arrEventOshaColumn[0] = "Activity When Injured:" ;
				// Should be string returning 
				arrEventOshaColumn[1] = objEvent.EventOsha.ActivityWhenInj.ToString() ;
				AddRow( arrEventOshaColumn );
			    
				arrEventOshaColumn[0] = "How Accident Occurred:" ;
				arrEventOshaColumn[1] = objEvent.EventOsha.HowAccOccurred ;
				AddRow( arrEventOshaColumn );
			    
				arrEventOshaColumn[0] = "What Injured Person:" ;
				arrEventOshaColumn[1] = objEvent.EventOsha.ObjSubstThatInj ;
				AddRow( arrEventOshaColumn );

				arrEventOshaColumn[0] = "Safeguards Provided:" ;
				//Start - ANURAG/Defect#000429 - The property is returning codeid so added code to get code desc
				arrEventOshaColumn[1] =  GetCodeDescription(objEvent.EventOsha.SafegProvided); //Convert.ToBoolean(objEvent.EventOsha.SafegProvided) ?  "Yes" : "No" ;
				//End
				AddRow( arrEventOshaColumn );

				arrEventOshaColumn[0] = "Safeguards Used:" ;
				//Start - ANURAG/Defect#000429 - The property is returning codeid so added code to get code desc
				arrEventOshaColumn[1] = GetCodeDescription(objEvent.EventOsha.SafegNotUsed);//Convert.ToBoolean(objEvent.EventOsha.SafegNotUsed) ? "No" : "Yes" ;
				//END
				AddRow( arrEventOshaColumn );

				arrEventOshaColumn[0] = "Rules Followed:" ;
				//Start - ANURAG/Defect#000429 - The property is returning codeid so added code to get code desc
				arrEventOshaColumn[1] = GetCodeDescription(objEvent.EventOsha.RulesNotFollowed);//Convert.ToBoolean(objEvent.EventOsha.RulesNotFollowed) ? "No" : "Yes" ;
				//END
				AddRow( arrEventOshaColumn );
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventOsha.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}		
				arrEventOshaColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for Person Involved
		/// </summary>
		/// <param name="p_iEventID">event ID</param>
		/// <returns>Person Involved data in form of a Xmldocument</returns>
        private XmlDocument GetEventPersonInvolved(int p_iEventID, int p_iClaimId)
		{
			Event objEvent = null ;
			Entity objEntity = null ;
            //string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13
			string[] arrEventPersonInvolvedColumn = { "Name" , "Role" , "Phone" , "Address" } ;
			string sClassName = "" ;
			string sNameType = "" ;					
			HipaaLog objHippa=null;
			try
			{
				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );				
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );
			
				StartDocument( "Party(s) involved with Event - " + objEvent.EventNumber , arrEventPersonInvolvedColumn );

				foreach( PersonInvolved objPersonInvolved in objEvent.PiList )
				{															
					sClassName =  GetItemString( CountItems( "." , objPersonInvolved.GetType().ToString() ) - 1 , objPersonInvolved.GetType().ToString() , "." );
					sNameType = GetNameType( sClassName );					
					
					objEntity = objPersonInvolved.PiEntity ;										
					if( objEntity != null )
					{
						arrEventPersonInvolvedColumn[0] = objEntity.GetLastFirstName() ;
						arrEventPersonInvolvedColumn[1] = sNameType ;
						arrEventPersonInvolvedColumn[2] = objEntity.Phone1 ;
						arrEventPersonInvolvedColumn[3] = GetDisplayAddress( objEntity ) ;

						AddRow( arrEventPersonInvolvedColumn );
						if (sNameType=="Patient" || sNameType=="PiPatient")
						{
							objHippa=new HipaaLog(m_sConnectionString, m_iClientId);
							objHippa.LogHippaInfo(objPersonInvolved.PiEid,p_iEventID.ToString(),"0","Event Occurrence/Quick Summary","VW",m_sUserName);	
						}
						
					}					
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventPersonInvolved.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				arrEventPersonInvolvedColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for Person Involved of a type p_sClassName
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sClassName">Class Name</param>
        /// <param name="p_sParentTableName">PI ParentTableName</param>
        /// <param name="p_iParentRowId">PI ParentRowId</param>
		/// <returns>Person Involved data in form of a Xmldocument</returns>
        private XmlDocument GetPersonInvolvedNodeDetails(int p_iEventID, string p_sNodeName, string p_sClassName, string p_sParentTableName, int p_iParentRowId) 
        {
            Event objEvent = null;
            Claim objClaim = null;
            Entity objEntity = null;
            Policy objPolicy = null; 
            string[] arrPIClassColumn = { "Name", "Phone", "Address" };
            string sClassName = "";
            string sNameType = "";
            sNameType = GetNameType(p_sClassName);
            try
            {
                // pgupta215  JIra  RMA-15677  start
                if (p_iParentRowId == 0)
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidParentRowID", m_iClientId));
                switch (p_sNodeName)
                {
                    case CLAIM_PI_CLASS_NODE:
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        if (p_iParentRowId > 0)
                            objClaim.MoveTo(p_iParentRowId);
                        else
                            throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidParentRowID", m_iClientId));
                        StartDocument(sNameType + "(s) involved in Claim - " + objClaim.ClaimNumber, arrPIClassColumn);
                        foreach (PersonInvolved objPersonInvolved in objClaim.PiList)
                        {
                            sClassName = GetItemString(CountItems(".", objPersonInvolved.GetType().ToString()) - 1, objPersonInvolved.GetType().ToString(), ".");
                            if (sClassName.ToLower() == p_sClassName.ToLower())
                            {
                                objEntity = objPersonInvolved.PiEntity;
                                if (objEntity != null)
                                {
                                    arrPIClassColumn[0] = objEntity.GetLastFirstName();
                                    arrPIClassColumn[1] = objEntity.Phone1;
                                    arrPIClassColumn[2] = GetDisplayAddress(objEntity);
                                    AddRow(arrPIClassColumn);
                                }
                            }
                        }
                        break;
                    case POLICY_PI_CLASS_NODE:
                    objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);  
                        if ( p_iParentRowId> 0)
                            objPolicy.MoveTo(p_iParentRowId);
                        else
                            throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidParentRowID", m_iClientId));
                        StartDocument(sNameType + "(s) involved in Policy - " + objPolicy.PolicyNumber + " "+ objPolicy.PolicyName, arrPIClassColumn);
                        foreach (PersonInvolved objPersonInvolved in objPolicy.PiList)
                        {
                            sClassName = GetItemString(CountItems(".", objPersonInvolved.GetType().ToString()) - 1, objPersonInvolved.GetType().ToString(), ".");
                            if (sClassName.ToLower() == p_sClassName.ToLower())
                            {
                                objEntity = objPersonInvolved.PiEntity;
                                if (objEntity != null)
                                {
                                    arrPIClassColumn[0] = objEntity.GetLastFirstName();
                                    arrPIClassColumn[1] = objEntity.Phone1;
                                    arrPIClassColumn[2] = GetDisplayAddress(objEntity);
                                    AddRow(arrPIClassColumn);
                                }
                            }
                        }
                        break;
                    case EVENT_PI_CLASS_NODE:
                        // Get the reference of an event having event_id p_iEventID
                        objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                        if (p_iEventID > 0)
                            objEvent.MoveTo(p_iEventID);
                        else
                            throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidParentRowID", m_iClientId));
                        StartDocument(sNameType + "(s) involved in Event - " + objEvent.EventNumber, arrPIClassColumn);
                        foreach (PersonInvolved objPersonInvolved in objEvent.PiList)
                        {
                            sClassName = GetItemString(CountItems(".", objPersonInvolved.GetType().ToString()) - 1, objPersonInvolved.GetType().ToString(), ".");
                            if (sClassName.ToLower() == p_sClassName.ToLower())
                            {
                                objEntity = objPersonInvolved.PiEntity;
                                if (objEntity != null)
                                {
                                    arrPIClassColumn[0] = objEntity.GetLastFirstName();
                                    arrPIClassColumn[1] = objEntity.Phone1;
                                    arrPIClassColumn[2] = GetDisplayAddress(objEntity);
                                    AddRow(arrPIClassColumn);
                                }
                            }
                        }
                        break;
                }
                // JIRA RMA-15677  End
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventPIClass.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objEvent != null)
                {
                    objEvent.Dispose();
                    objEvent = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                //   arrEventPIClassColumn = null;
                // pgupta215  JIra  RMA-15677  start
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                    objPolicy = null;
                }
                //arrClaimPIClassColumn = null;
                arrPIClassColumn = null;
                // pgupta215  JIra  RMA-15677  End
            }
            return (m_objDocument);
        }
		/// <summary>
		/// Get the data for Person Involved of a type p_sClassName and having ID equal to p_iRequestedInfoID
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sClassName">Class Name</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID</param>
		/// <returns>Person Involved data in form of a Xmldocument</returns>
        private XmlDocument GetEventPI(int p_iEventID, string p_sNodeName, string p_sClassName, int p_iRequestedInfoID, int p_iClaimId)
		{
			Entity objEntity = null ;
			State objState = null ;
		
			string[] arrEventPIColumn = { "" , "" }; 
			string sSQL = "" ;
			
			//string sStateName = "" ;
			string sNameType = "" ;
			string sEmployeeNumber = "" ;
			//bool bStateFound = false ;
            //Added Rakhi for R7:Add Emp Data Elements
            SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId); //sonali
            LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId);
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sCodeDesc = string.Empty;
            string[][] arrAddrColumn = new string [8][];
            //string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13

            int iCount = 0;
            //Added Rakhi for R7:Add Emp Data Elements
			
			try
			{
				// Get the reference of an entity having entity_id p_iRequestedInfoID
				objEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
				if( p_iRequestedInfoID > 0 )
					objEntity.MoveTo( p_iRequestedInfoID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventPI.InvalidEntityID",m_iClientId) );				
			
				sNameType = GetNameType( p_sClassName );
				StartDocument( sNameType + " Details - " + objEntity.GetLastFirstName() , arrEventPIColumn );

				arrEventPIColumn[0] = "Role (PI Type):" ;
				arrEventPIColumn[1] = sNameType ;
                AddRow(arrEventPIColumn,false);
		                
				arrEventPIColumn[0] = "Name:" ;
				arrEventPIColumn[1] = objEntity.GetLastFirstName() ;
				AddRow( arrEventPIColumn,false);

                foreach (AddressXPhoneInfo objAddressXPhoneInfo in objEntity.AddressXPhoneInfoList)
                {
                    sCodeDesc = objCache.GetCodeDesc(objAddressXPhoneInfo.PhoneCode);
                    arrEventPIColumn[0] = sCodeDesc + " Phone:";
                    arrEventPIColumn[1] = objAddressXPhoneInfo.PhoneNo;
                    AddRow(arrEventPIColumn,false);
                    
                }

                #region Code Commented Rakhi as we are now using Cache object to get StateInfo
                //objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
                //try
                //{
                //    objState.MoveTo( objEntity.StateId );
                //    bStateFound = true ;
                //}
                //catch
                //{
                //    bStateFound = false ;
                //}
                //if( bStateFound )
                //    sStateName = objState.StateId + "   "  + objState.StateName ;
#endregion

				if( sNameType == "Employee" )
				{				
					sSQL = "SELECT EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_EID = " + p_iRequestedInfoID ;
					sEmployeeNumber = FetchValueFromDatabase( sSQL );					

					arrEventPIColumn[0] = "Employee Number:" ;
					arrEventPIColumn[1] = sEmployeeNumber ;
					AddRow( arrEventPIColumn,false);
		            
					arrEventPIColumn[0] = "Social Security #:" ;
					arrEventPIColumn[1] = objEntity.TaxId.ToString() ;
					AddRow( arrEventPIColumn,false) ;
		            
					arrEventPIColumn[0] = "Birth Date:" ;
					arrEventPIColumn[1] = Conversion.GetDBDateFormat( objEntity.BirthDate , "d") ;
					AddRow( arrEventPIColumn,false);
		            
					arrEventPIColumn[0] = "Sex:" ;
					arrEventPIColumn[1] = GetCodeDescription( objEntity.SexCode );
					AddRow( arrEventPIColumn,false) ;
				}				
                if (objSysSettings.UseMultipleAddresses)
                {
                    
                    foreach (EntityXAddresses objEntityXAddressesInfo in objEntity.EntityXAddressesList)
                    {
                        iCount++;
						//RMA 14593 JIRA RMA-8753  START
                        arrEventPIColumn[0] = "Street Address:";
                        arrEventPIColumn[1] = objEntityXAddressesInfo.Address.Addr1;
                        arrAddrColumn[0] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };

                        arrEventPIColumn[0] = "Address Line 2:";
                        arrEventPIColumn[1] = objEntityXAddressesInfo.Address.Addr2;
                        arrAddrColumn[1] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };

                        arrEventPIColumn[0] = "Address Line 3:";
                        arrEventPIColumn[1] = objEntityXAddressesInfo.Address.Addr3;
                        arrAddrColumn[2] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };

                        arrEventPIColumn[0] = "Address Line 4:";
                        arrEventPIColumn[1] = objEntityXAddressesInfo.Address.Addr4;
                        arrAddrColumn[3] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };

                        arrEventPIColumn[0] = "City:";
                        arrEventPIColumn[1] = objEntityXAddressesInfo.Address.City;
                        arrAddrColumn[4] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };

                        objCache.GetStateInfo(objEntityXAddressesInfo.Address.State, ref sStateCode, ref sStateDesc);

                        arrEventPIColumn[0] = "State:";
                        arrEventPIColumn[1] = sStateCode + "   " + sStateDesc;
                        arrAddrColumn[5] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };


                        arrEventPIColumn[0] = "ZipCode:";
                        arrEventPIColumn[1] = objEntityXAddressesInfo.Address.ZipCode;
                        arrAddrColumn[6] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };
						//JIRA RMA-8753 END

                        arrEventPIColumn[0] = "Fax Number:";
                        arrEventPIColumn[1] = objEntityXAddressesInfo.Fax;
                        arrAddrColumn[7] = new string[2] { arrEventPIColumn[0], arrEventPIColumn[1] };

                        AddSubRows("Address" + " " + iCount.ToString(), arrAddrColumn);
                    }
                }
                else
                {

                    arrEventPIColumn[0] = "Street Address:";
                    arrEventPIColumn[1] = objEntity.Addr1;
                    AddRow(arrEventPIColumn,false);

                    arrEventPIColumn[0] = "Address Line 2:";
                    arrEventPIColumn[1] = objEntity.Addr2;
                    AddRow(arrEventPIColumn,false);

                    arrEventPIColumn[0] = "Address Line 3:";
                    arrEventPIColumn[1] = objEntity.Addr3;
                    AddRow(arrEventPIColumn, false);

                    arrEventPIColumn[0] = "Address Line 4:";
                    arrEventPIColumn[1] = objEntity.Addr4;
                    AddRow(arrEventPIColumn, false);

                    arrEventPIColumn[0] = "City:";
                    arrEventPIColumn[1] = objEntity.City;
                    AddRow(arrEventPIColumn,false);

                    objCache.GetStateInfo(objEntity.StateId, ref sStateCode, ref sStateDesc);

                    arrEventPIColumn[0] = "State:";
                    //arrEventPIColumn[1] = sStateName;
                    arrEventPIColumn[1] = sStateCode + "   " + sStateDesc;
                    AddRow(arrEventPIColumn,false);

                    arrEventPIColumn[0] = "ZipCode:";
                    arrEventPIColumn[1] = objEntity.ZipCode;
                    AddRow(arrEventPIColumn,false);

                    #region Commented Rakhi as we now have multiple phone number types
                    //arrEventPIColumn[0] = "Home Phone:";
                    //arrEventPIColumn[1] = objEntity.Phone1;
                    //AddRow(arrEventPIColumn);

                    //arrEventPIColumn[0] = "Work Phone:";
                    //arrEventPIColumn[1] = objEntity.Phone2;
                    //AddRow(arrEventPIColumn);
                    #endregion

                    arrEventPIColumn[0] = "Fax Number:";
                    arrEventPIColumn[1] = objEntity.FaxNumber;
                    AddRow(arrEventPIColumn,false);

                    //arrEventPIColumn[0] = "E-mail Address:"  ;
                    //arrEventPIColumn[1] = objEntity.EmailAddress ;
                    //AddRow( arrEventPIColumn );				
                }
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventPI.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				arrEventPIColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for claim
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim data in form of a Xmldocument</returns>
		private XmlDocument GetClaimDetails( int p_iClaimID )
		{
			Event objEvent = null ;
			Claim objClaim = null ;
			Entity objEntity = null ;
			State objState = null ;
            DbReader objReader = null; //averma62
            string sbSQL = string.Empty; //averma62
			string[] arrClaimDetailsColumn = { "" , "" } ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			string sDeptLastName = "" ;
			string sStateName = "" ;			
			bool bEntityFound = false ;
			bool bStateFound = false ;
            string sInsuredClaimDeptLastName = string.Empty; //Added by Nikhil on 09/19/2014

			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );

				// Get the reference of an event having event_id objClaim.EventId
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( objClaim.EventId > 0 )
					objEvent.MoveTo( objClaim.EventId );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID",m_iClientId) );

                StartDocument("Claim Details - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimDetailsColumn);  //tmalhotra2 MITS-29787 Re# 5.1.13
				
				objEntity  = ( Entity )m_objDataModelFactory.GetDataModelObject( "Entity" , false );
				try
				{
					objEntity.MoveTo( objEvent.DeptEid );
					bEntityFound = true ;
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sDeptLastName = objEntity.LastName ;
				
				if( objClaim.LineOfBusCode == WORKERS_CLAIM_BUS_TYPE_CODE || objClaim.LineOfBusCode == SHORT_TERM_CLAIM_BUS_TYPE_CODE )
				{
					objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );
					try
					{
						objState.MoveTo( objClaim.FilingStateId );
						bStateFound = true ;
					}
					catch
					{
						bStateFound = false ;
					}
					if( bStateFound )
						sStateName = objState.StateId + "   "  + objState.StateName;
				}

				arrClaimDetailsColumn[0] = "Department:" ;
				arrClaimDetailsColumn[1] = sDeptLastName ;
				AddRow( arrClaimDetailsColumn ) ;

				arrClaimDetailsColumn[0] = "Event Date:" ;
				arrClaimDetailsColumn[1] = Conversion.GetDBDateFormat( objEvent.DateOfEvent , "d") ;
				AddRow( arrClaimDetailsColumn ) ;

				arrClaimDetailsColumn[0] = "Date Reported:" ;
				arrClaimDetailsColumn[1] = Conversion.GetDBDateFormat( objEvent.DateReported , "d") ;
				AddRow( arrClaimDetailsColumn ) ;

				arrClaimDetailsColumn[0] = "Claim Date:" ;
				arrClaimDetailsColumn[1] = Conversion.GetDBDateFormat( objClaim.DateOfClaim , "d" ) ;
				AddRow( arrClaimDetailsColumn ) ;

				arrClaimDetailsColumn[0] = "Claim Type:" ;
				arrClaimDetailsColumn[1] = GetCodeDescription( objClaim.ClaimTypeCode );
				AddRow( arrClaimDetailsColumn ) ;

				arrClaimDetailsColumn[0] = "Claim Status:" ;
				arrClaimDetailsColumn[1] = GetCodeDescription( objClaim.ClaimStatusCode );
				AddRow( arrClaimDetailsColumn ) ;

				if ( GetShortCode( objClaim.ClaimStatusCode )== "C" )
				{
					arrClaimDetailsColumn[0] = "Date/Time Closed:" ;
					arrClaimDetailsColumn[1] = Conversion.GetDBDateFormat( objClaim.DttmClosed , "d" ) ;
					AddRow( arrClaimDetailsColumn ) ;
				}			
                //averma62 Catstrophe Enhancement MITS 28528 --start

                arrClaimDetailsColumn[0] = "Catastrophe Type:";
                arrClaimDetailsColumn[1] = GetCodeDescription(objClaim.CatastropheCode);
                AddRow(arrClaimDetailsColumn);

                sbSQL = "SELECT CAT_NUMBER FROM CATASTROPHE WHERE CATASTROPHE_ROW_ID = " + objClaim.CatastropheRowId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader.Read())
                {
                    arrClaimDetailsColumn[0] = "Catastrophe Number:";
                    arrClaimDetailsColumn[1] = Conversion.ConvertObjToStr(objReader.GetValue("CAT_NUMBER"));
                    AddRow(arrClaimDetailsColumn); 
                }
                

                //averma62 Catstrophe Enhancement MITS 28528 --end

				arrClaimDetailsColumn[0] = "Service Code:" ;
				arrClaimDetailsColumn[1] = GetCodeDescription( objClaim.ServiceCode );
				AddRow( arrClaimDetailsColumn ) ;

				arrClaimDetailsColumn[0] = "File Number:" ;
				arrClaimDetailsColumn[1] = objClaim.FileNumber ;
				AddRow( arrClaimDetailsColumn ) ;

				arrClaimDetailsColumn[0] = "Estimated Collection:" ;
				arrClaimDetailsColumn[1] = objClaim.EstCollection.ToString() ;
				AddRow( arrClaimDetailsColumn ) ;

				if( objClaim.LineOfBusCode == WORKERS_CLAIM_BUS_TYPE_CODE || objClaim.LineOfBusCode == SHORT_TERM_CLAIM_BUS_TYPE_CODE )
				{
					//corrected the spelling by Nitin for R4-R5 merging mits no 12707
                    //arrClaimDetailsColumn[0] = "Jusisdiction:" ;
                    arrClaimDetailsColumn[0] = "Jurisdiction:";
					arrClaimDetailsColumn[1] = sStateName ;
					AddRow( arrClaimDetailsColumn ) ;
				}			

				arrClaimDetailsColumn[0] = "Payments Frozen:" ;
				arrClaimDetailsColumn[1] = objClaim.PaymntFrozenFlag ? "Yes" : "No" ;
				AddRow( arrClaimDetailsColumn ) ;
                //Start - Added by Nikhil on 09/19/2014
                if (objClaim.Context.InternalSettings.SysSettings.UseInsuredClaimDept == -1)
                {
                    sInsuredClaimDeptLastName = objClaim.Context.DbConnLookup.ExecuteString("SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + objClaim.InsuredClaimDeptEid);
                    arrClaimDetailsColumn[0] = "Insured Department (Claim Level):";
                    arrClaimDetailsColumn[1] = sInsuredClaimDeptLastName;
                    AddRow(arrClaimDetailsColumn);

                }
                //End - Added by Nikhil on 09/19/2014
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimDetails.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
				arrClaimDetailsColumn = null ;
			}
			return( m_objDocument );
		}
        private XmlDocument GetClaimPaymentHistory(int p_iClaimID)//parijat :21784
        {
            return (GetClaimPaymentHistory(p_iClaimID, 0));
        }
		/// <summary>
		/// Get the data for Claim  Payment History
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim Payment History data in form of a Xmldocument</returns>
        private XmlDocument GetClaimPaymentHistory(int p_iClaimID, int p_iClaimantID)
		{
			Claim objClaim = null ;
			DbReader objReader = null ;

            //rupal:start, add first & final column in payment history if carrier claim is on
            string[] arrClaimPaymentHistoryColumn;
            SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//sonali
            
            //Manish for multi currency
            string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

            if (objSysSettings.MultiCovgPerClm != 0)
            {//Changed by amitosh for new columns in payment history grid
                arrClaimPaymentHistoryColumn = new string[]{
														"Trans Date" , "Control #" , "Check #" , "Type" , 
														"Cleared" , "Void" , "Payee" , "Check Amount" , 
														"From/To Date" , "Invoice #" , "Trans Type" , 
														"Split Amount" , "User","Combined Payment" ,
                                                         "First & Final" ,"Policy Name","Coverage Type","Unit","LossType","Invoice Amount",
													};
            }
            else
            {
                arrClaimPaymentHistoryColumn = new string[]{
														"Trans Date" , "Control #" , "Check #" , "Type" , 
														"Cleared" , "Void" , "Payee" , "Check Amount" , 
														"From/To Date" , "Invoice #" , "Trans Type" , 
														"Split Amount" , "User" ,"Combined Payment" ,"Invoice Amount",
													};
            }
            //rupal:end
            //end Amitosh
			StringBuilder sbSQL ;	
		
			string sName = "" ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13
			int iLastTransID = 0 ;			
			int iTransID = 0 ;			
			double dblAmount = 0.0 ;
			double dblTotalAll = 0.0 ;
			double dblTotalVoid = 0.0 ;
			double dblTotalPay = 0.0 ;
			double dblTotalCollect = 0.0 ;
			double dblSplitAmount = 0.0 ;
            double dblIAmount = 0.0;
			bool bPayment = false ;
			bool bCleared = false ;
			bool bVoid = false ;
            string sPayeesName = string.Empty;  //averma62 MITS -27477
            //rupal
            bool bFirstFinal = false;
            //skhare7 R8 
            bool bCombPayment = false;

			const string  DITTO = "\"" ;
			
			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );
                
                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Payment History for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimPaymentHistoryColumn);  //tmalhotra2 MITS-29787 Re# 5.1.13
				
				sbSQL = new StringBuilder() ;
                //rupal, added FUNDS_TRANS_SPLIT.IS_FIRST_FINAL in select clause
                if (objSysSettings.MultiCovgPerClm != 0)
                {
				sbSQL.Append( "SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.TRANS_NUMBER,"  );
                sbSQL.Append(" FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,FUNDS_TRANS_SPLIT.INVOICE_AMOUNT | FIAMT,");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE,");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.ADDED_BY_USER, CODES_TEXT.CODE_DESC,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,LOSSCODE.CODE_DESC LOSS_CODE ");
                    //RUpal:start,policy interface change
                    //sbSQL.Append(" , CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT VIN FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)   WHEN 'E' THEN (SELECT EMPLOYEE_DETAILS FROM WC_POLICY_EMPLOYEE)  END UNIT_TYPE");
                    sbSQL.Append(" , CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT VIN FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT ENTITY.LAST_NAME FROM ENTITY,OTHER_UNIT WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT_TYPE");
                    //rupal:end
                    sbSQL.Append(" ,FUNDS.COMBINED_PAY_FLAG");
                    sbSQL.Append(" ,POL.CODE_DESC COVERAGETYPE,POLICY.POLICY_NAME ");
                    sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT,CODES_TEXT,FUNDS,RESERVE_CURRENT,COVERAGE_X_LOSS,CODES_TEXT LOSSCODE  ");
                    sbSQL.Append(" ,POLICY_X_CVG_TYPE,CODES_TEXT POL,POLICY, POLICY_X_UNIT");
                 
                    sbSQL.Append(" WHERE FUNDS.CLAIM_ID = " + p_iClaimID);
                    sbSQL.Append(" AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                    sbSQL.Append(" AND CODES_TEXT.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");
                    sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RC_ROW_ID= RESERVE_CURRENT.RC_ROW_ID");
                    sbSQL.Append(" AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID");
                    sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID ");
                    sbSQL.Append("   AND LOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE  ");

                    sbSQL.Append("  AND POL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE  ");
                    sbSQL.Append("  AND   POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                    sbSQL.Append(" AND POLICY.POLICY_ID =POLICY_X_UNIT.POLICY_ID");   
                    sbSQL.Append(" AND " + LANGUAGE_CODE);
                }
                else
                {
                    sbSQL.Append("SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.TRANS_NUMBER,FUNDS.COMBINED_PAY_FLAG ,");
                    sbSQL.Append(" FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,FUNDS_TRANS_SPLIT.INVOICE_AMOUNT | FIAMT,");
				sbSQL.Append( " FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE," );
                sbSQL.Append(" FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.ADDED_BY_USER, CODES_TEXT.CODE_DESC,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL");
				sbSQL.Append( " FROM FUNDS, FUNDS_TRANS_SPLIT, CODES_TEXT " ); 
				sbSQL.Append( " WHERE FUNDS.CLAIM_ID = " + p_iClaimID ) ;
				sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID" );
				sbSQL.Append( " AND CODES_TEXT.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE" );
				sbSQL.Append( " AND " + LANGUAGE_CODE );
                }
                if (p_iClaimantID != 0)//parijat 21784
                {
                    sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantID);
                }
				sbSQL.Append( " ORDER BY FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID" );
				
				if ( m_sDBType == Constants.DB_ACCESS )
					sbSQL = sbSQL.Replace( "|" , " AS " ) ;
				else
					sbSQL = sbSQL.Replace( "|" , "  " ) ;			

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );

				if( objReader != null )
				{
					while( objReader.Read() )
					{						
						iTransID = objReader.GetInt( "TRANS_ID" );
						if( iTransID != iLastTransID )    //' skip duplicates (caused by inner join splits table)
						{
							iLastTransID = iTransID ;


                            //Start averma62 MITS -27477
                            string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + iTransID;
                            if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            {
                                sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                                sEntSQL = sEntSQL.Replace("+", "||");

                            }
                            using (DbReader objeReader = DbFactory.GetDbReader(m_sConnectionString, sEntSQL))
                            {
                                while (objeReader.Read())
                                {
                                    if (sPayeesName == string.Empty)
                                    {
                                        sPayeesName = objeReader.GetString("PAYEES_NAME");
                                    }
                                    else
                                    {
                                        sPayeesName = sPayeesName + ", " + objeReader.GetString("PAYEES_NAME");
                                    }

                                }
                            }
                            //End averma62 MITS -27477


							bVoid = objReader.GetBoolean("VOID_FLAG") ;
							bCleared = objReader.GetBoolean("CLEARED_FLAG") ;
							bPayment = objReader.GetBoolean("PAYMENT_FLAG") ;

							arrClaimPaymentHistoryColumn[0] = Conversion.GetDBDateFormat( objReader.GetString("TRANS_DATE"), "d") ;
							arrClaimPaymentHistoryColumn[1] = objReader.GetString("CTL_NUMBER") ;
							arrClaimPaymentHistoryColumn[2] = objReader["TRANS_NUMBER"].ToString() ;
							arrClaimPaymentHistoryColumn[3] = bPayment ? "Payment" : "Collection" ;
							arrClaimPaymentHistoryColumn[4] = bCleared ? "Yes" : "No" ;
							arrClaimPaymentHistoryColumn[5] = bVoid ? "Yes" : "No" ;

                            //Start averma62 MITS - 27477
                            /* 
                            sName = objReader.GetString( "FIRST_NAME" ) ;
                            if( sName.Trim().Length > 0 ) 
                                sName += " " ;
                            arrClaimPaymentHistoryColumn[6] = sName + objReader.GetString( "LAST_NAME" ) ;
                             */
                            arrClaimPaymentHistoryColumn[6] = sPayeesName; 
                            sPayeesName = string.Empty;
                            //End averma62 MITS - 27477

							dblAmount = objReader.GetDouble( "FAMT" );
							if(  ! bPayment )
								dblAmount = -dblAmount ;						
							dblTotalAll += dblAmount ;
							if( bVoid )
								dblTotalVoid += dblAmount ;
							if( bPayment )
								dblTotalPay += dblAmount ;						
							else
								// Changed Total Collections to positive. 
								dblTotalCollect -= dblAmount ;
							arrClaimPaymentHistoryColumn[7] = string.Format("{0:C}" , dblAmount ); 

							arrClaimPaymentHistoryColumn[8] = Conversion.GetDBDateFormat( objReader.GetString( "FROM_DATE" ) , "d" )
								+ " - " + Conversion.GetDBDateFormat( objReader.GetString( "TO_DATE" ) , "d" ) ;


							arrClaimPaymentHistoryColumn[9] = objReader.GetString( "INVOICE_NUMBER" );
							arrClaimPaymentHistoryColumn[10] = objReader.GetString( "CODE_DESC" ); 

							dblSplitAmount = objReader.GetDouble( "FTSAMT" );
							if( !bPayment ) 
								dblSplitAmount = - dblSplitAmount ;
                            dblIAmount = objReader.GetDouble("FIAMT");
                            if (!bPayment)
                                dblIAmount = -dblIAmount;
						
							arrClaimPaymentHistoryColumn[11] = string.Format("{0:C}" , dblSplitAmount ); 
                            
							arrClaimPaymentHistoryColumn[12] = objReader.GetString( "ADDED_BY_USER" ) ;
                            //skhare7 R8
                            bCombPayment = objReader.GetBoolean("COMBINED_PAY_FLAG");
                            arrClaimPaymentHistoryColumn[13] = bCombPayment ? "Yes" : "No";
                            //rupal:start
                            //add first & final column in payment history if carrier claim is on
                            if (objSysSettings.MultiCovgPerClm != 0)
                            {
                                bFirstFinal = objReader.GetBoolean("IS_FIRST_FINAL");
                                arrClaimPaymentHistoryColumn[14] = bFirstFinal ? "Yes" : "No";
                                //skhare7 r8 
                                 arrClaimPaymentHistoryColumn[15]= objReader.GetString("POLICY_NAME");
                                 arrClaimPaymentHistoryColumn[16]=objReader.GetString("COVERAGETYPE");
                                 arrClaimPaymentHistoryColumn[17] = objReader.GetString("UNIT_TYPE");
                                 arrClaimPaymentHistoryColumn[18] = objReader.GetString("LOSS_CODE");
                                 arrClaimPaymentHistoryColumn[19] = string.Format("{0:C}", dblIAmount); //srajindersin MITS 36456
                            }
                            //AddRow(arrClaimPaymentHistoryColumn);
                            AddRow(arrClaimPaymentHistoryColumn, false);
                            //rupal:end

							
						}
						else
						{
							arrClaimPaymentHistoryColumn[0] = DITTO ;
							arrClaimPaymentHistoryColumn[1] = DITTO ;
							arrClaimPaymentHistoryColumn[2] = DITTO ;
							arrClaimPaymentHistoryColumn[3] = DITTO ;
							arrClaimPaymentHistoryColumn[4] = DITTO ;
							arrClaimPaymentHistoryColumn[5] = DITTO ;
							arrClaimPaymentHistoryColumn[6] = DITTO ;
							arrClaimPaymentHistoryColumn[7] = DITTO ;
						
							arrClaimPaymentHistoryColumn[8] = Conversion.GetDBDateFormat( objReader.GetString( "FROM_DATE" ) , "d" )
								+ " - " + Conversion.GetDBDateFormat( objReader.GetString( "TO_DATE" ) , "d" ) ;
						
							arrClaimPaymentHistoryColumn[9] = objReader.GetString( "INVOICE_NUMBER" );
							arrClaimPaymentHistoryColumn[10] = objReader.GetString( "CODE_DESC" );
						
							dblSplitAmount = objReader.GetDouble( "FTSAMT" );
							if( !bPayment ) 
								dblSplitAmount = - dblSplitAmount ;
                            dblIAmount = objReader.GetDouble("FIAMT");
                            if (!bPayment)
                                dblIAmount = -dblIAmount;
						
							arrClaimPaymentHistoryColumn[11] = string.Format("{0:C}" , dblSplitAmount );
                            //arrClaimPaymentHistoryColumn[19] = string.Format("{0:C}", dblIAmount); //jira 5489, this line should be under carrier claim on block,
							arrClaimPaymentHistoryColumn[12] = DITTO ;

                            bCombPayment = objReader.GetBoolean("COMBINED_PAY_FLAG");
                            arrClaimPaymentHistoryColumn[13] = bCombPayment ? "Yes" : "No";
                            //rupal:start
                            //add first & final column in payment history if carrier claim is on
                            if (objSysSettings.MultiCovgPerClm != 0)
                            {
                                bFirstFinal = objReader.GetBoolean("IS_FIRST_FINAL");
                                arrClaimPaymentHistoryColumn[14] = bFirstFinal ? "Yes" : "No";
                                arrClaimPaymentHistoryColumn[15] = objReader.GetString("POLICY_NAME");
                                arrClaimPaymentHistoryColumn[16] = objReader.GetString("COVERAGETYPE");
                                arrClaimPaymentHistoryColumn[17] = objReader.GetString("UNIT_TYPE");
                                arrClaimPaymentHistoryColumn[18] = objReader.GetString("LOSS_CODE");
                                arrClaimPaymentHistoryColumn[19] = string.Format("{0:C}", dblIAmount); //jira 5489
                            }

                            //AddRow(arrClaimPaymentHistoryColumn);
                            AddRow(arrClaimPaymentHistoryColumn, false);
                            //rupal:end							
						}
					}
				}
			
                //rupal
			    //for( int iIndex= 0 ; iIndex < 13 ; iIndex++ )
                for (int iIndex = 0; iIndex < arrClaimPaymentHistoryColumn.Length; iIndex++)
					arrClaimPaymentHistoryColumn[iIndex] = "" ;
				AddRow( arrClaimPaymentHistoryColumn );

				arrClaimPaymentHistoryColumn[0] = "Overall Total:" ;
				arrClaimPaymentHistoryColumn[1] = string.Format("{0:C}" , dblTotalAll );
				AddRow( arrClaimPaymentHistoryColumn ) ;

				arrClaimPaymentHistoryColumn[0] = "Total Paid:" ;
				arrClaimPaymentHistoryColumn[1] = string.Format("{0:C}" , dblTotalPay ); 
				AddRow( arrClaimPaymentHistoryColumn );

				arrClaimPaymentHistoryColumn[0] = "Total Collections:" ;
				arrClaimPaymentHistoryColumn[1] = string.Format("{0:C}" , dblTotalCollect ); 
				AddRow( arrClaimPaymentHistoryColumn );

				arrClaimPaymentHistoryColumn[0] = "Total Voids:" ;
				arrClaimPaymentHistoryColumn[1] = string.Format("{0:C}" , dblTotalVoid ); 
				AddRow( arrClaimPaymentHistoryColumn );

				arrClaimPaymentHistoryColumn[0] = "Payments Less Voids:" ;
				arrClaimPaymentHistoryColumn[1] = string.Format("{0:C}" , dblTotalPay - dblTotalVoid ); 
				AddRow( arrClaimPaymentHistoryColumn );
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimPaymentHistory.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				sbSQL = null ;
				arrClaimPaymentHistoryColumn = null ;

				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( m_objDocument );

		}

        private XmlDocument GetClaimByPayee(int p_iClaimID)//parijat :21784
        {
            return (GetClaimByPayee(p_iClaimID, 0));
        }
		/// <summary>
		/// Get the data for Claim by Payee
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim by Payee data in form of a Xmldocument</returns>
        private XmlDocument GetClaimByPayee(int p_iClaimID, int p_iClaimantID)//parijat :21784--definition change
		{
			Claim objClaim = null ;
			Entity objEntity = null ;
			State objState = null ;
			DbReader objReaderPayee = null ;
			DbReader objReaderAmount = null ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13
			string[] arrClaimByPayeeColumn = 
													{
														"Payee Name" , "Tax ID" , "Payee Street Address" ,
														"City" , "State" , "Zip Code" , "Amount" 
													};

			
			string sSQL = "" ;
			int iPayeeID = 0 ; 
			int iLastPayeeID = 0 ;
			string sName ="" ;
			int iStateID = 0 ;
			bool bEntityFound = false ;
			bool bStateFound = false ;

			try
			{
                //Manish for multi currency 
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
               Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );				
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );

                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Payment History By Payee for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimByPayeeColumn);  //tmalhotra2 MITS-29787 Re# 5.1.13
                if (p_iClaimantID != 0)//parijat 21784
                {
                    sSQL = "SELECT PAYEE_EID, FIRST_NAME, LAST_NAME, ADDR1, ADDR2, ADDR3, ADDR4 CITY, STATE_ID, ZIP_CODE "
                        + " FROM FUNDS WHERE CLAIMANT_EID = " + p_iClaimantID + " AND PAYMENT_FLAG <> 0 AND VOID_FLAG=0 "
                        + " ORDER BY LAST_NAME,FIRST_NAME";
                }
                else
                {
                    sSQL = "SELECT PAYEE_EID, FIRST_NAME, LAST_NAME, ADDR1, ADDR2, ADDR3, ADDR4, CITY, STATE_ID, ZIP_CODE "
                        + " FROM FUNDS WHERE CLAIM_ID = " + p_iClaimID + " AND PAYMENT_FLAG <> 0 AND VOID_FLAG=0 "
                        + " ORDER BY LAST_NAME,FIRST_NAME";
                }

				objReaderPayee = DbFactory.GetDbReader( m_sConnectionString , sSQL );			
				if( objReaderPayee != null )
				{
					objEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
					objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );
								
					while( objReaderPayee.Read() )
					{
						iPayeeID = objReaderPayee.GetInt( "PAYEE_EID" );
						if( iLastPayeeID != iPayeeID )
						{
							sName = objReaderPayee.GetString( "FIRST_NAME" ) ;
							if( sName.Trim().Length > 0 )
								sName += " " ;
							arrClaimByPayeeColumn[0] = sName + objReaderPayee.GetString( "LAST_NAME" );
							
							try
							{
								objEntity.MoveTo( iPayeeID );
								bEntityFound = true ;
							}
							catch
							{
								bEntityFound = false ;
							}
							if( bEntityFound )								
								arrClaimByPayeeColumn[1] = objEntity.TaxId.ToString() ;

                            arrClaimByPayeeColumn[2] = objReaderPayee.GetString("ADDR1") + " " + objReaderPayee.GetString("ADDR2") + " " + objReaderPayee.GetString("ADDR3") + " " + objReaderPayee.GetString("ADDR4");
							arrClaimByPayeeColumn[3] = objReaderPayee.GetString( "CITY" ) ;
							iStateID = objReaderPayee.GetInt( "STATE_ID" );

							try
							{
								objState.MoveTo( iStateID );
								bStateFound = true ;
							}
							catch
							{
								bStateFound = false ;
							}
							if( bStateFound )
								arrClaimByPayeeColumn[4] = objState.StateId.ToString() ;

							arrClaimByPayeeColumn[5] = objReaderPayee.GetString( "ZIP_CODE" );

                            sSQL = "SELECT SUM(AMOUNT)|SUMOFAMOUNT,PAYEE_EID"
                                + " FROM FUNDS WHERE CLAIM_ID = " + p_iClaimID + " AND PAYMENT_FLAG <> 0 AND VOID_FLAG=0 "
                                + " AND PAYEE_EID=" + iPayeeID + " ";
                            if (p_iClaimantID != 0)//parijat 21784
                            {
                                sSQL = sSQL + " AND FUNDS.CLAIMANT_EID = " + p_iClaimantID + " ";
                            }
                            sSQL=sSQL+ " GROUP BY PAYEE_EID "//parijat 21784
								+ " ORDER BY PAYEE_EID" ;	

							if (m_sDBType == Constants.DB_ACCESS  )
								sSQL = sSQL.Replace( "|" , " AS " ) ;
							else
								sSQL = sSQL.Replace( "|" , "  " ) ;
						
							try
							{
								objReaderAmount = DbFactory.GetDbReader( m_sConnectionString , sSQL );

								if( objReaderAmount != null )
								{
									if( objReaderAmount.Read() )
									{
										arrClaimByPayeeColumn[6] = string.Format("{0:C}" , objReaderAmount.GetDouble( "SUMOFAMOUNT" ) ); 
									}
								}
							}
							catch( RMAppException p_objEx )
							{
								throw p_objEx ;
							}
							catch( Exception p_objEx )
							{
								throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimByPayee.SumOfAmountDataError",m_iClientId) , p_objEx );
							}
							finally
							{															
								if( objReaderAmount != null )
								{
									objReaderAmount.Close();
									objReaderAmount.Dispose();
								}
							}
							AddRow( arrClaimByPayeeColumn , iPayeeID , p_iClaimID ) ;
						} // end if
						iLastPayeeID = iPayeeID ;
					} // end while
				} // end if 
			} //end try
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimByPayee.DataError",m_iClientId) , p_objEx );
			}
			finally
			{				
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				arrClaimByPayeeColumn = null ;

				if( objReaderPayee != null )
				{
					objReaderPayee.Close();
					objReaderPayee.Dispose();
				}
			}
			return( m_objDocument );
		}

        private XmlDocument GetClaimByReserve(int p_iClaimID)//parijat :21784
        {
            return (GetClaimByReserve(p_iClaimID, 0));
        }
		/// <summary>
		/// Get the data for Claim by Reserve
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim by Reserve data in form of a Xmldocument</returns>
        private XmlDocument GetClaimByReserve(int p_iClaimID, int p_iClaimantID)//parijat :21784
		{
			Claim objClaim = null ;
			
			DbReader objReader = null ;			

			string[] arrClaimByReserveColumn = 
											{
												"Reserve" , "Control #" , "Payee" , "From/To Date" ,
												"Trans DAte" , "Check #" , "Check Date" , "Check Amount" ,
												"Trans Type" , "Split Amount" , "Invoice #" , "Type" , "Cleared"
											};
			string sSQL = "" ;		
			string sName = "" ;
			int iLastReserve = 0 ;			
			int iTransID = 0 ;
			bool bFirstReserve = false ;
			bool bPayment = false ;
			bool bCleared = false ;
			bool bVoid = false ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			double dblAmount = 0.0 ;
			double dblSplitAmount = 0.0 ;
            double dblIAmount = 0.0;
            SysSettings objSettings = null;
			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
                objSettings = new SysSettings(m_sConnectionString,m_iClientId);
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );

                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Payment History by Reserve Type for Claim  - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimByReserveColumn);

                if (objSettings.MultiCovgPerClm != 0)
                {
                    sSQL = "SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.TRANS_NUMBER,"
                       + " FUNDS.CLEARED_FLAG, FUNDS.AMOUNT  |  FAMT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,"
                       + " FUNDS_TRANS_SPLIT.AMOUNT  |  FTSAMT,FUNDS_TRANS_SPLIT.INVOICE_AMOUNT  |  FIAMT, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,"
                       + " FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, FUNDS.ADDED_BY_USER,"
                       + " FUNDS.DATE_OF_CHECK,RESERVE_CURRENT.RESERVE_TYPE_CODE,"
                       + " CODES_TEXT.CODE_DESC  |  TRANSTYPECODE, CODES_TEXT_1.CODE_DESC  |  RESERVETYPECODE"
                       + " FROM FUNDS, FUNDS_TRANS_SPLIT,RESERVE_CURRENT, CODES_TEXT, CODES_TEXT  |  CODES_TEXT_1"
                       + " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS_TRANS_SPLIT.RC_ROW_ID=RESERVE_CURRENT.RC_ROW_ID"
                       + " AND FUNDS.CLAIM_ID=" + p_iClaimID
                       + " AND CODES_TEXT.CODE_ID=FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE"
                       + " AND CODES_TEXT_1.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE"
                       + " AND " + LANGUAGE_CODE;

                    if (p_iClaimantID != 0)//parijat 21784
                    {
                        sSQL += " AND RESERVE_CURRENT.CLAIMANT_EID = " + p_iClaimantID;
                    }

                    sSQL = sSQL + " ORDER BY RESERVE_CURRENT.RESERVE_TYPE_CODE, FUNDS.TRANS_ID";//parijat 21784
                }
                else
                {
                    sSQL = "SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.TRANS_NUMBER,"
                    + " FUNDS.CLEARED_FLAG, FUNDS.AMOUNT  |  FAMT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,"
                    + " FUNDS_TRANS_SPLIT.AMOUNT  |  FTSAMT,FUNDS_TRANS_SPLIT.INVOICE_AMOUNT  |  FIAMT, FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,"
                    + " FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, FUNDS.ADDED_BY_USER,"
                    + " FUNDS.DATE_OF_CHECK,FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE,"
                    + " CODES_TEXT.CODE_DESC  |  TRANSTYPECODE, CODES_TEXT_1.CODE_DESC  |  RESERVETYPECODE"
                    + " FROM FUNDS, FUNDS_TRANS_SPLIT, CODES_TEXT, CODES_TEXT  |  CODES_TEXT_1"
                    + " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
                    + " AND FUNDS.CLAIM_ID=" + p_iClaimID
                    + " AND CODES_TEXT.CODE_ID=FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE"
                    + " AND CODES_TEXT_1.CODE_ID=FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE"
                    + " AND " + LANGUAGE_CODE;
                
                if (p_iClaimantID != 0)//parijat 21784
                {
                    sSQL+= " AND FUNDS.CLAIMANT_EID = " + p_iClaimantID;
                }
					
               sSQL= sSQL+ " ORDER BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS.TRANS_ID" ;//parijat 21784
                }
				if ( m_sDBType == Constants.DB_ACCESS  )
					sSQL = sSQL.Replace( "|" , " AS " ) ;
				else
					sSQL = sSQL.Replace( "|" , "  " ) ;	

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
			
				if( objReader != null )
				{
					while( objReader.Read() )
					{
						if( iLastReserve != objReader.GetInt( "RESERVE_TYPE_CODE" ) && bFirstReserve )
						{
							bFirstReserve = true ;
							for( int iIndex = 0 ; iIndex < 13 ; iIndex++ )
								arrClaimByReserveColumn[iIndex] = "" ;
							AddRow( arrClaimByReserveColumn );
							iLastReserve = objReader.GetInt( "RESERVE_TYPE_CODE" );
						}
						bFirstReserve = true ;
						iLastReserve = objReader.GetInt( "RESERVE_TYPE_CODE" );

						iTransID = objReader.GetInt( "TRANS_ID" );
						bVoid = objReader.GetBoolean("VOID_FLAG") ;
						bCleared = objReader.GetBoolean("CLEARED_FLAG") ;
						bPayment = objReader.GetBoolean("PAYMENT_FLAG") ;

						arrClaimByReserveColumn[0] = objReader.GetString( "RESERVETYPECODE" );
						arrClaimByReserveColumn[1] = objReader.GetString( "CTL_NUMBER" );

						sName = objReader.GetString( "FIRST_NAME" ) ;
						if( sName.Trim().Length > 0 ) 
							sName += " " ;
						arrClaimByReserveColumn[2] = sName + objReader.GetString( "LAST_NAME" ) ;

						arrClaimByReserveColumn[3] = Conversion.GetDBDateFormat( objReader.GetString( "FROM_DATE" ) , "d" )
							+ " - " + Conversion.GetDBDateFormat( objReader.GetString( "TO_DATE" ) , "d" ) ;

						arrClaimByReserveColumn[4] = Conversion.GetDBDateFormat( objReader.GetString( "TRANS_DATE" ) , "d" ) ;
						arrClaimByReserveColumn[5] = objReader["TRANS_NUMBER"].ToString();
						arrClaimByReserveColumn[6] = Conversion.GetDBDateFormat( objReader.GetString( "DATE_OF_CHECK" ) , "d" ) ;
						
						dblAmount = objReader.GetDouble( "FAMT" ) ;
						if( ! bPayment )
							dblAmount = - dblAmount ;
						
						arrClaimByReserveColumn[7] = string.Format("{0:C}" , dblAmount ); 
						arrClaimByReserveColumn[8] = objReader.GetString( "TRANSTYPECODE" ) ;
						
						dblSplitAmount = objReader.GetDouble( "FTSAMT" ) ;
						if( ! bPayment )
							dblSplitAmount = - dblSplitAmount ;

                        dblIAmount = objReader.GetDouble("FIAMT");
                        if (!bPayment)
                            dblIAmount = -dblIAmount;
						arrClaimByReserveColumn[9] = string.Format("{0:C}" , dblSplitAmount ); 
                        //arrClaimByReserveColumn[13] = string.Format("{0:C}", dblIAmount); srajindersin MITS 26453
						arrClaimByReserveColumn[10] = objReader.GetString( "INVOICE_NUMBER" ) ;
						arrClaimByReserveColumn[11] = bPayment ? "Payment" : "Collection" ;
						arrClaimByReserveColumn[12] = bCleared ? "Yes" : "No" ;

						AddRow( arrClaimByReserveColumn );
					}
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimByReserve.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				arrClaimByReserveColumn = null ;

				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( m_objDocument );
		}

        private XmlDocument GetClaimByTransType(int p_iClaimID)//parijat :21784
        {
            return (GetClaimByTransType(p_iClaimID, 0));
        }
		/// <summary>
		/// Get the data for Claim by TransType
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim by TransType data in form of a Xmldocument</returns>
        private XmlDocument GetClaimByTransType(int p_iClaimID, int p_iClaimantID)//parijat 21784- definition changed
		{
			Claim objClaim = null ;
			DbReader objReader = null ;			

			string[] arrClaimByTransTypeColumn = 
											{
												"Transaction Type" , "Amount" , "Transaction Date" , "Amount Type"
											};		
	
			int iLastTransTypeCode = 0 ;
			bool bFirstTransType = false ;
			bool bPayment = false ;
			double dblAmount = 0.0 ;
			string sSQL = "" ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );

                StartDocument("Payment History by Transaction Type for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimByTransTypeColumn);
				
				sSQL = "SELECT FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, FUNDS_TRANS_SPLIT.AMOUNT | TRANSAMT, CODES_TEXT.CODE_DESC,"
					+ " FUNDS.TRANS_DATE, FUNDS.PAYMENT_FLAG, FUNDS.COLLECTION_FLAG, FUNDS_TRANS_SPLIT.INVOICE_AMOUNT "
					+ " FROM FUNDS, FUNDS_TRANS_SPLIT, CODES_TEXT "
					+ " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
					+ " AND FUNDS.CLAIM_ID = " + p_iClaimID
					+ " AND FUNDS.VOID_FLAG = 0"
					+ " AND FUNDS_TRANS_SPLIT.AMOUNT <> 0"
					+ " AND CODES_TEXT.CODE_ID=FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE"
                    + " AND " + LANGUAGE_CODE;

                if (p_iClaimantID != 0)//parijat 21784
                {
                    sSQL += " AND FUNDS.CLAIMANT_EID = " + p_iClaimantID;
                }
                sSQL += " ORDER BY FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, FUNDS.TRANS_DATE DESC";

				if ( m_sDBType == Constants.DB_ACCESS  )
					sSQL = sSQL.Replace( "|" , " AS " ) ;
				else
					sSQL = sSQL.Replace( "|" , "  " ) ;			

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );			

				if( objReader != null )
				{
					while( objReader.Read() )
					{
						if( iLastTransTypeCode != objReader.GetInt( "TRANS_TYPE_CODE" ) && bFirstTransType )
						{
							bFirstTransType = true ;
							for( int iIndex = 0 ; iIndex < 4 ; iIndex++ )
								arrClaimByTransTypeColumn[iIndex] = "" ;
							AddRow( arrClaimByTransTypeColumn );
							iLastTransTypeCode = objReader.GetInt( "TRANS_TYPE_CODE" );
						}
						bFirstTransType = true ;
						iLastTransTypeCode = objReader.GetInt( "TRANS_TYPE_CODE" );

						bPayment = objReader.GetBoolean( "PAYMENT_FLAG" );
						arrClaimByTransTypeColumn[0] = objReader.GetString( "CODE_DESC" );
						dblAmount = objReader.GetDouble( "TRANSAMT" );
						if( ! bPayment )
							dblAmount = -dblAmount ;

						arrClaimByTransTypeColumn[1] = string.Format("{0:C}" , dblAmount );
						arrClaimByTransTypeColumn[2] = Conversion.GetDBDateFormat( objReader.GetString("TRANS_DATE" ) , "d" );
						arrClaimByTransTypeColumn[3] = bPayment ? "Payment" : "Collection" ;

						AddRow( arrClaimByTransTypeColumn );
					}	
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimByTransType.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				arrClaimByTransTypeColumn = null ;

				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( m_objDocument );

		}

		/// <summary>
		/// Get the data for Claim by FuturePayments
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim by FuturePayments data in form of a Xmldocument</returns>
		private XmlDocument GetClaimFuturePayments( int p_iClaimID )
		{
			Claim objClaim = null ;
			DbReader objReader = null ;			

			string[] arrClaimFuturePaymentsColumn = 
											{
												"Print Date" , "Control #" , "Payee" , "Amount"
											};					
			string sSQL = "" ;
			string sName = "" ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
					throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID",m_iClientId) );

                StartDocument("Future Auto Payments for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimFuturePaymentsColumn);
								
				sSQL = "SELECT FUNDS_AUTO.PRINT_DATE,FUNDS_AUTO.CTL_NUMBER,FUNDS_AUTO.FIRST_NAME,FUNDS_AUTO.LAST_NAME,FUNDS_AUTO.AMOUNT "
					+ "FROM FUNDS_AUTO WHERE CLAIM_ID =" + p_iClaimID ;

				if ( m_sDBType == Constants.DB_ACCESS  )
					sSQL = sSQL.Replace( "|" , " AS " ) ;
				else
					sSQL = sSQL.Replace( "|" , "  " ) ;			

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );			

				if( objReader != null )
				{
					while( objReader.Read() )
					{
						arrClaimFuturePaymentsColumn[0] = Conversion.GetDBDateFormat( objReader.GetString("PRINT_DATE" ) , "d" );
						arrClaimFuturePaymentsColumn[1] = objReader.GetString( "CTL_NUMBER" );
						sName = objReader.GetString( "FIRST_NAME" ) ;
						if( sName.Trim().Length > 0 ) 
							sName += " " ;
						arrClaimFuturePaymentsColumn[2] = sName + objReader.GetString( "LAST_NAME" ) ;
						arrClaimFuturePaymentsColumn[3] = string.Format("{0:C}" , objReader.GetDouble( "AMOUNT" ) );
						
						AddRow( arrClaimFuturePaymentsColumn );
					}	
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimFuturePayments.DataError",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				arrClaimFuturePaymentsColumn = null ;

				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for Parties Involved
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>Parties Involved data in form of a Xmldocument</returns>
        private XmlDocument GetClaimPartiesInvolved(int p_iClaimID, string p_sNodeName, string p_sParentTableName, int p_iParentRowId) // ijain4 11/12/2015
        {
           // p_iPolicyId = 1038;
            Policy objPolicy = null;
            Claim objClaim = null;
            Entity objEntity = null;
            string sClassName = "";
            string sNameType = "";
            HipaaLog objHippa = null;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

            //string[] arrClaimPartiesInvolvedColumn = 
            //                                {
            //                                    "Name" , "Role" , "Phone" , "Address" 
            //                                };					
            //JIRA RMA-10751 ajohari2: Start
            string[] arrClaimPartiesInvolvedColumn = null;
            if (p_sNodeName.Equals("adjusters"))
            {
                arrClaimPartiesInvolvedColumn = new string[] {
												"Name" , "Role" , "Type", "Phone" , "Address" 
											    };
            }
            else
            {
                arrClaimPartiesInvolvedColumn = new string[] {
												"Name" , "Role" , "Phone" , "Address" 
											    };
            }
            //JIRA RMA-10751 ajohari2: End

			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
                switch (p_sParentTableName.ToUpper())
                {
                    case "POLICY":
                         objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);  // ijain4 : 11/10/2015
                        if (p_iParentRowId > 0)
                            objPolicy.MoveTo(p_iParentRowId);
                        else
                             throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidPolicyID", m_iClientId));
                        break;
                    default:
                        if (p_iClaimID > 0)
                            objClaim.MoveTo(p_iClaimID);
                        else
                            throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
				
                        break;
                }
				
				switch( p_sNodeName )
				{
					case "claimants" :
                        StartDocument("Claimants for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimPartiesInvolvedColumn);
						
						foreach( Claimant objClaimant in objClaim.ClaimantList )
						{
							if( objClaimant.ClaimantEid == 0 )
								break ;
							arrClaimPartiesInvolvedColumn[0] = objClaimant.ClaimantEntity.GetLastFirstName() ;
							arrClaimPartiesInvolvedColumn[1] = objClaimant.PrimaryClmntFlag ? "Claimant - Primary" : "Claimant" ;
							arrClaimPartiesInvolvedColumn[2] = objClaimant.ClaimantEntity.Phone1 ;
							arrClaimPartiesInvolvedColumn[3] = GetDisplayAddress( objClaimant.ClaimantEntity );

							AddRow( arrClaimPartiesInvolvedColumn );
						}
						break ;
					case "defendants" :
                        StartDocument("Defendants for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimPartiesInvolvedColumn);
						
						foreach( Defendant objDefendant in objClaim.DefendantList )
						{
							if( objDefendant.DefendantEid == 0 )
								break ;
							arrClaimPartiesInvolvedColumn[0] = objDefendant.DefendantEntity.GetLastFirstName();
							arrClaimPartiesInvolvedColumn[1] = "Defendant" ;
							arrClaimPartiesInvolvedColumn[2] = objDefendant.DefendantEntity.Phone1 ;
							arrClaimPartiesInvolvedColumn[3] = GetDisplayAddress( objDefendant.DefendantEntity ) ;

							AddRow( arrClaimPartiesInvolvedColumn );
						}
						break ;
					case "adjusters" :
                        StartDocument("Adjusters involved with Claim - " + objClaim.ClaimNumber + " * "  + ClaimantName, arrClaimPartiesInvolvedColumn);
						foreach( ClaimAdjuster objClaimAdjuster in objClaim.AdjusterList )
						{
							if( objClaimAdjuster.AdjusterEid == 0 )
								break ;
							arrClaimPartiesInvolvedColumn[0] = objClaimAdjuster.AdjusterEntity.GetLastFirstName(); 
							arrClaimPartiesInvolvedColumn[1] = objClaimAdjuster.CurrentAdjFlag ? "Adjuster - Current" : "Adjuster" ;
                            //JIRA RMA-10751 ajohari2: Start
                            arrClaimPartiesInvolvedColumn[2] = GetCodeDescription(objClaimAdjuster.AdjusterType);//JIRA RMA-10751 ajohari2
                            arrClaimPartiesInvolvedColumn[3] = objClaimAdjuster.AdjusterEntity.Phone1;
                            arrClaimPartiesInvolvedColumn[4] = GetDisplayAddress(objClaimAdjuster.AdjusterEntity);
                            //JIRA RMA-10751 ajohari2: End

							AddRow( arrClaimPartiesInvolvedColumn );
						}
						break ;
                    // pgupta215  JIra  RMA-15677  start
                    case "claimpersoninvolved":
                        StartDocument("Party(s) involved with Claim - " + objClaim.ClaimNumber, arrClaimPartiesInvolvedColumn);
                            foreach (PersonInvolved objPersonInvolved in objClaim.PiList)
                        {
                            sClassName = GetItemString(CountItems(".", objPersonInvolved.GetType().ToString()) - 1, objPersonInvolved.GetType().ToString(), ".");
                            sNameType = GetNameType(sClassName);
                            objEntity = objPersonInvolved.PiEntity;
                            if (objEntity != null)
                            {
                                arrClaimPartiesInvolvedColumn[0] = objEntity.GetLastFirstName();
                                arrClaimPartiesInvolvedColumn[1] = sNameType;
                                arrClaimPartiesInvolvedColumn[2] = objEntity.Phone1;
                                arrClaimPartiesInvolvedColumn[3] = GetDisplayAddress(objEntity);

                                AddRow(arrClaimPartiesInvolvedColumn);
                                if (sNameType == "Patient" || sNameType == "PiPatient")
                                {
                                    objHippa = new HipaaLog(m_sConnectionString, m_iClientId);
                                    objHippa.LogHippaInfo(objPersonInvolved.PiEid, p_iClaimID.ToString(), "0", "Event Occurrence/Quick Summary", "VW", m_sUserName);
                                }
                            }
                        }
                        break;
                    case "policypersoninvolved":
                       
                        StartDocument("Party(s) involved with Policy - " + objPolicy.PolicyNumber+ " " +objPolicy.PolicyName, arrClaimPartiesInvolvedColumn);
                        foreach (PersonInvolved objPersonInvolved in objPolicy.PiList)
                        {
                            sClassName = GetItemString(CountItems(".", objPersonInvolved.GetType().ToString()) - 1, objPersonInvolved.GetType().ToString(), ".");
                            sNameType = GetNameType(sClassName);
                            objEntity = objPersonInvolved.PiEntity;
                            if (objEntity != null)
                            {
                                arrClaimPartiesInvolvedColumn[0] = objEntity.GetLastFirstName();
                                arrClaimPartiesInvolvedColumn[1] = sNameType;
                                arrClaimPartiesInvolvedColumn[2] = objEntity.Phone1;
                                arrClaimPartiesInvolvedColumn[3] = GetDisplayAddress(objEntity);

                                AddRow(arrClaimPartiesInvolvedColumn);
                                if (sNameType == "Patient" || sNameType == "PiPatient")
                                {
                                    objHippa = new HipaaLog(m_sConnectionString, m_iClientId);
                                    objHippa.LogHippaInfo(objPersonInvolved.PiEid, p_iClaimID.ToString(), "0", "Event Occurrence/Quick Summary", "VW", m_sUserName);
                                }
                            }
                        }
                        break;
                    // pgupta215  JIra  RMA-15677  END					
				} 											
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimPartiesInvolved.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}	
				arrClaimPartiesInvolvedColumn = null ;														
			}
			return( m_objDocument );
		}
		
		/// <summary>
		/// Get the data for Properties Involved
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Properties Involved data in form of a Xmldocument</returns>
		private XmlDocument GetClaimPropertiesInvolved( int p_iClaimID )
		{
			Claim objClaim = null ;
			Vehicle objVehicle = null ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			string[] arrClaimPropertiesInvolvedColumn = 
											{
												"ID" , "Description" , "Insurance Coverage"
											};					
			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                StartDocument("Property(s) involved with Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimPropertiesInvolvedColumn);

				foreach( UnitXClaim objUnitXClaim in objClaim.UnitList )
				{
					if( objUnitXClaim.UnitId == 0 )
						break ;

					objVehicle = objUnitXClaim.Vehicle ;
					
					arrClaimPropertiesInvolvedColumn[0] = objUnitXClaim.UnitId.ToString(); 
					arrClaimPropertiesInvolvedColumn[1] = VehicleDescForDisplay( objVehicle );
					arrClaimPropertiesInvolvedColumn[2] = objVehicle.InsuranceCoverage ;

					AddRow( arrClaimPropertiesInvolvedColumn );
				}																						
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimPropertiesInvolved.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objVehicle != null )
				{
					objVehicle.Dispose();
					objVehicle = null ;
				}
				arrClaimPropertiesInvolvedColumn = null ;		
			}
			return( m_objDocument );
		}

		/// <summary>
		/// Get the data for Claimant
		/// </summary>
		/// <param name="p_iRequestedInfoID">Requested Info ID as Claiment ID </param>
		/// <returns>Claimant data in form of a Xmldocument</returns>
		private XmlDocument GetClaimClaimant( int p_iRequestedInfoID )
		{
			Claimant objClaimant = null ;
			State objState = null ;
			Entity objEntity = null ;
			Entity objEntityAttorney = null ;

			string[] arrClaimClaimantColumn = { "" , "" } ;
			
			string sStateName = "" ;
			string sEntityName = "" ;
			bool bEntityFound = false ;
			bool bStateFound = false ;
			bool bAttorneyFound = false ;
			string sLastFirstName = "" ;

			try
			{
				// Get the reference of an claiment having Claimant id  p_iRequestedInfoID				
				objClaimant = ( Claimant )m_objDataModelFactory.GetDataModelObject( "Claimant" , false );
				if( p_iRequestedInfoID > 0)
					objClaimant.MoveTo( p_iRequestedInfoID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimClaimant.InvalidClaimantID", m_iClientId));
				
				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
				try
				{
					objState.MoveTo( objClaimant.ClaimantEntity.StateId );
					bStateFound = true ;
				}
				catch
				{
					bStateFound = false ;
				}
				if( bStateFound )
					sStateName = objState.StateId + "   " + objState.StateName ;

				objEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
				try
				{
					objEntity.MoveTo( objClaimant.InsurerEid );
					bEntityFound = true ;
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sEntityName = objEntity.FirstName + " " + objEntity.LastName ;				

				arrClaimClaimantColumn[0] = "Claimant:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.GetLastFirstName() ;
				StartDocument( "Claimant Details - " + objClaimant.ClaimantEntity.GetLastFirstName() , arrClaimClaimantColumn );

				arrClaimClaimantColumn[0] = "Claimant Number:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantNumber.ToString() ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Claimant Type:" ;
				arrClaimClaimantColumn[1] = GetCodeDescription( objClaimant.ClaimantTypeCode ) ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Primary Claimant:" ;
				arrClaimClaimantColumn[1] = objClaimant.PrimaryClmntFlag ? "Yes" : "No" ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Street Address:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.Addr1 ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Address Line 2:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.Addr2 ;
				AddRow( arrClaimClaimantColumn ) ;

                arrClaimClaimantColumn[0] = "Address Line 3:";
                arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.Addr3;
                AddRow(arrClaimClaimantColumn);

                arrClaimClaimantColumn[0] = "Address Line 4:";
                arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.Addr4;
                AddRow(arrClaimClaimantColumn);

				arrClaimClaimantColumn[0] = "City:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.City ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "State:" ;
				arrClaimClaimantColumn[1] = sStateName ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Zip Code:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.ZipCode ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Home Phone:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.Phone1 ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Work Phone:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.Phone2 ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Fax Number:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.FaxNumber ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "E-mail Address:" ;
				arrClaimClaimantColumn[1] = objClaimant.ClaimantEntity.EmailAddress ;
				AddRow( arrClaimClaimantColumn ) ;

				try
				{
					objEntityAttorney = objClaimant.AttorneyEntity ;
					bAttorneyFound = true ;					
				}
				catch
				{
					bAttorneyFound = false ;
				}
				if( bAttorneyFound )
					sLastFirstName = objEntityAttorney.GetLastFirstName(); 
				else
					sLastFirstName = "" ;

				arrClaimClaimantColumn[0] = "Claimant Attorney:" ;
				arrClaimClaimantColumn[1] = sLastFirstName ;
				AddRow( arrClaimClaimantColumn ) ;
				

				arrClaimClaimantColumn[0] = "Insurer:" ;
				arrClaimClaimantColumn[1] = sEntityName.Trim() ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Sex:" ;
				arrClaimClaimantColumn[1] = GetCodeDescription( objClaimant.ClaimantEntity.SexCode );
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "Date of Birth:" ;
				arrClaimClaimantColumn[1] = Conversion.GetDBDateFormat( objClaimant.ClaimantEntity.BirthDate , "d" ) ;
				AddRow( arrClaimClaimantColumn ) ;

				arrClaimClaimantColumn[0] = "SOL Date:" ;
				arrClaimClaimantColumn[1] = Conversion.GetDBDateFormat( objClaimant.SolDate , "d" ) ;
				AddRow( arrClaimClaimantColumn ) ;

                //JIRA RMA-9685 ajohari2  : Start
                arrClaimClaimantColumn[0] = "Status:";
                arrClaimClaimantColumn[1] = GetCodeDescription(objClaimant.ClaimantStatusCode);
                AddRow(arrClaimClaimantColumn);
                //JIRA RMA-9685 ajohari2  : End
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimClaimant.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objClaimant != null )
				{
					objClaimant.Dispose();
					objClaimant = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				if( objEntityAttorney != null )
				{
					objEntityAttorney.Dispose();
					objEntityAttorney = null ;
				}
				arrClaimClaimantColumn = null ;
			}
			return( m_objDocument );
		}

		/// <summary>
		/// Get the data for Defendant
		/// </summary>
		/// <param name="p_iRequestedInfoID">Requested Info ID as Defendent ID</param>
		/// <returns>Defendant data in form of a Xmldocument</returns>
        private XmlDocument GetClaimDefendant(int p_iRequestedInfoID, int p_iClaimID)
		{
			Defendant objDefendant = null ;
			State objState = null ;
			Entity objEntity = null ;
			Entity objEntityAttorney = null ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			bool bEntityFound = false ;
			bool bStateFound = false ;
			bool bAttorneyFound = false ;

			string[] arrClaimDefendantColumn = { "" , "" } ;
			
			string sStateName = "" ;
			string sEntityName = "" ;
			string sLastFirstName = "" ;

			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				// Get the reference of an defendent having defendent id  p_iRequestedInfoID				
				objDefendant = ( Defendant )m_objDataModelFactory.GetDataModelObject( "Defendant" , false );
				if( p_iRequestedInfoID > 0)
					objDefendant.MoveTo( p_iRequestedInfoID );	
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimDefendant.InvalidDefendantID", m_iClientId));
			
				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
				try
				{
					objState.MoveTo( objDefendant.DefendantEntity.StateId );
					bStateFound = true ;
				}
				catch
				{
					bStateFound = false ;
				}
				if( bStateFound )
					sStateName = objState.StateId + "   " + objState.StateName ;

				objEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
				try
				{
					objEntity.MoveTo( objDefendant.InsurerEid );
					bEntityFound = true ;
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sEntityName = objEntity.FirstName + " " + objEntity.LastName ;				

				arrClaimDefendantColumn[0] = "" ;
				arrClaimDefendantColumn[1] = "" ;
                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Defendant Details - " + objDefendant.DefendantEntity.GetLastFirstName(), arrClaimDefendantColumn);

				arrClaimDefendantColumn[0] = "Name:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.GetLastFirstName() ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Defendant Type:" ;
				arrClaimDefendantColumn[1] = GetCodeDescription( objDefendant.DfndntTypeCode );
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Street Address:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.Addr1 ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Address Line 2:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.Addr2 ;
				AddRow( arrClaimDefendantColumn );

                arrClaimDefendantColumn[0] = "Address Line 3:";
                arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.Addr3;
                AddRow(arrClaimDefendantColumn);

                arrClaimDefendantColumn[0] = "Address Line 4:";
                arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.Addr4;
                AddRow(arrClaimDefendantColumn);

				arrClaimDefendantColumn[0] = "City:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.City ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "State:" ;
				arrClaimDefendantColumn[1] = sStateName ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Zip Code:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.ZipCode ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Home Phone:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.Phone1  ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Work Phone:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.Phone2 ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Fax Number:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.FaxNumber ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "E-mail Address:" ;
				arrClaimDefendantColumn[1] = objDefendant.DefendantEntity.EmailAddress ;
				AddRow( arrClaimDefendantColumn );

				try
				{
					objEntityAttorney = objDefendant.AttorneyEntity ;
					bAttorneyFound = true ;					
				}
				catch
				{
					bAttorneyFound = false ;
				}
				if( bAttorneyFound )
					sLastFirstName = objEntityAttorney.GetLastFirstName(); 
				else
					sLastFirstName = "" ;

				arrClaimDefendantColumn[0] = "Defendant Attorney:" ;
				arrClaimDefendantColumn[1] = sLastFirstName ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Insurer:" ;
				arrClaimDefendantColumn[1] = sEntityName ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Policy Number:" ;
				arrClaimDefendantColumn[1] = objDefendant.PolicyNumber ;
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Policy Type:" ;
				arrClaimDefendantColumn[1] = GetCodeDescription( objDefendant.PolicyTypeCode );
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Policy Limit:" ;
				arrClaimDefendantColumn[1] = string.Format("{0:C}" , objDefendant.PolicyLimit ); 
				AddRow( arrClaimDefendantColumn );

				arrClaimDefendantColumn[0] = "Policy Expiration Date:" ;
				arrClaimDefendantColumn[1] = Conversion.GetDBDateFormat( objDefendant.PolExpireDate , "d" ) ;
				AddRow( arrClaimDefendantColumn );
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimDefendant.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objDefendant != null )
				{
					objDefendant.Dispose();
					objDefendant = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				if( objEntityAttorney != null )
				{
					objEntityAttorney.Dispose();
					objEntityAttorney = null ;
				}
				arrClaimDefendantColumn = null ;
			}
			return( m_objDocument );
		}

		/// <summary>
		/// Get the data for Adjuster
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID as Adjuster ID</param>
		/// <returns>Adjuster data in form of a Xmldocument</returns>
		private XmlDocument GetClaimAdjuster( int p_iClaimID , int p_iRequestedInfoID )
		{
			Claim objClaim = null ;
			ClaimAdjuster objClaimAdjuster = null ;
            bool bSortCompleted = false;//Asif Mits 14413
            int iIndex = 0;//Asif Mits 14413
            ArrayList arrlstClaimAdjuster = null;//Asif Mits 14413
            AdjustDatedText objAdjusterDatedtxt = null;//Asif Mits 14413
            AdjustDatedText objNextAdjusterDatedtxt = null;//Asif Mits 14413

			string[] arrClaimAdjusterColumn = { "Date" , "Time" , "Type" , "Entered By" , "Comment"	} ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if ( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );	
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

				objClaimAdjuster = ( ClaimAdjuster ) m_objDataModelFactory.GetDataModelObject( "ClaimAdjuster" , false );
				if ( p_iRequestedInfoID > 0 )
					objClaimAdjuster.MoveTo( p_iRequestedInfoID );	
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimAdjuster.InvalidClaimAdjusterID", m_iClientId));

                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Adjuster Dated Text for Claim - " + objClaim.ClaimNumber + " and Adjuster: " + objClaimAdjuster.AdjusterEntity.GetLastFirstName() + " * " + ClaimantName, arrClaimAdjusterColumn);
                arrlstClaimAdjuster = new ArrayList();//Asif	
				foreach( AdjustDatedText objAdjustDatedText in objClaimAdjuster.AdjustDatedTextList )
				{
					if( objAdjustDatedText.AdjDttextRowId == 0)
						break;						
               //Mits 14413:Asif Start
                    //arrClaimAdjusterColumn[0] = Conversion.GetDBDateFormat( objAdjustDatedText.DateEntered , "d" );
                    //arrClaimAdjusterColumn[1] = Conversion.GetDBTimeFormat( objAdjustDatedText.TimeEntered , "t" );
                    //arrClaimAdjusterColumn[2] = GetCodeDescription( objAdjustDatedText.TextTypeCode ) ;
                    //arrClaimAdjusterColumn[3] = objAdjustDatedText.EnteredByUser ;
                    //arrClaimAdjusterColumn[4] = objAdjustDatedText.DatedText ;
                    //AddRow( arrClaimAdjusterColumn );
                    arrlstClaimAdjuster.Add(objAdjustDatedText);   
				}
                if (arrlstClaimAdjuster.Count > 0)
                {
                    bSortCompleted = false;
                    while (!bSortCompleted)
                    {
                        bSortCompleted = true;
                        for (iIndex = 0; iIndex < arrlstClaimAdjuster.Count - 1; iIndex++)
                        {
                            objAdjusterDatedtxt = (AdjustDatedText)arrlstClaimAdjuster[iIndex];
                            objNextAdjusterDatedtxt = (AdjustDatedText)arrlstClaimAdjuster[iIndex + 1];
                            if (Conversion.ConvertStrToLong(objAdjusterDatedtxt.DateEntered.Trim()) < Conversion.ConvertStrToLong(objNextAdjusterDatedtxt.DateEntered.Trim()))
                            {
                                bSortCompleted = false;
                                arrlstClaimAdjuster.RemoveAt(iIndex);
                                arrlstClaimAdjuster.Insert(iIndex, objNextAdjusterDatedtxt);
                                arrlstClaimAdjuster.RemoveAt(iIndex + 1);
                                arrlstClaimAdjuster.Insert(iIndex + 1, objAdjusterDatedtxt);
                            }
                                //Time Sorting Started
                            else if (Conversion.ConvertStrToLong(objAdjusterDatedtxt.DateEntered.Trim()) == Conversion.ConvertStrToLong(objNextAdjusterDatedtxt.DateEntered.Trim()))
                             {
                                 if (Conversion.ConvertStrToLong(objAdjusterDatedtxt.TimeEntered.Trim()) < Conversion.ConvertStrToLong(objNextAdjusterDatedtxt.TimeEntered.Trim()))
                                 {
                                     bSortCompleted = false;
                                     arrlstClaimAdjuster.RemoveAt(iIndex);
                                     arrlstClaimAdjuster.Insert(iIndex, objNextAdjusterDatedtxt);
                                     arrlstClaimAdjuster.RemoveAt(iIndex + 1);
                                     arrlstClaimAdjuster.Insert(iIndex + 1, objAdjusterDatedtxt);
                                 }
                             }
                            //Time Sorting Ends
                        }
                    }
                    
                    // Sort completed
                  
                    for (iIndex = 0; iIndex < arrlstClaimAdjuster.Count; iIndex++)
                    {
                        objAdjusterDatedtxt = null;
                        objAdjusterDatedtxt = (AdjustDatedText)arrlstClaimAdjuster[iIndex];
                        arrClaimAdjusterColumn[0] = Conversion.GetDBDateFormat(objAdjusterDatedtxt.DateEntered, "d");
                        arrClaimAdjusterColumn[1] = Conversion.GetDBTimeFormat(objAdjusterDatedtxt.TimeEntered, "t");
                        arrClaimAdjusterColumn[2] = GetCodeDescription(objAdjusterDatedtxt.TextTypeCode);
                        arrClaimAdjusterColumn[3] = objAdjusterDatedtxt.EnteredByUser;
                        arrClaimAdjusterColumn[4] = objAdjusterDatedtxt.DatedText;
                        AddRow(arrClaimAdjusterColumn);
                    }
                }
                //Mits 14413:Asif End
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimAdjuster.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objClaimAdjuster != null )
				{
					objClaimAdjuster.Dispose();
					objClaimAdjuster = null ;
				}
				arrClaimAdjusterColumn = null ;			
			}
			return( m_objDocument );
		}

		/// <summary>
		/// Get the data for Unit Involved
		/// </summary>
		/// <param name="p_iRequestedInfoID">Sub ID1 as Unit ID</param>
		/// <returns>Unit Involved data in form of a Xmldocument</returns>
		private XmlDocument GetClaimUnitInvolved( int p_claimid,int p_iRequestedInfoID )
		{
			Vehicle objVehicle = null ;
						
			string[] arrClaimUnitInvolveColumn = { "" , "" };
			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_claimid, CommonFunctions.NavFormType.None, m_sConnectionString);
                 Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				objVehicle = ( Vehicle ) m_objDataModelFactory.GetDataModelObject( "Vehicle" , false );
				if( p_iRequestedInfoID > 0 )
					objVehicle.MoveTo( p_iRequestedInfoID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimUnitInvolved.InvalidVehicleID", m_iClientId));

				StartDocument( "Unit Details - " + VehicleDescForDisplay( objVehicle ) , arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Deductible" ;
				arrClaimUnitInvolveColumn[1] = objVehicle.Deductible.ToString() ;
				AddRow( arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Disposal Date" ;
				arrClaimUnitInvolveColumn[1] = Common.Conversion.GetDBDateFormat( objVehicle.DisposalDate , "d" ) ;
				AddRow( arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Gross Weight" ;
				arrClaimUnitInvolveColumn[1] = objVehicle.GrossWeight.ToString() ;
				AddRow( arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Last Service Date" ;
				arrClaimUnitInvolveColumn[1] = Common.Conversion.GetDBDateFormat( objVehicle.LastServiceDate , "d" ) ;
				AddRow( arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Lease Amount" ;
				arrClaimUnitInvolveColumn[1] = string.Format("{0:C}" , objVehicle.LeaseAmount );
				AddRow( arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Original Cost" ;
				arrClaimUnitInvolveColumn[1] = string.Format("{0:C}" , objVehicle.OriginalCost );
				AddRow( arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Purchase Date" ;
				arrClaimUnitInvolveColumn[1] = Common.Conversion.GetDBDateFormat( objVehicle.PurchaseDate , "d" ) ;
				AddRow( arrClaimUnitInvolveColumn );

				arrClaimUnitInvolveColumn[0] = "Type of Service" ;
				arrClaimUnitInvolveColumn[1] = objVehicle.TypeOfService ;
				AddRow( arrClaimUnitInvolveColumn );
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimUnitInvolved.DataError", m_iClientId), p_objEx);
			}
			finally
			{				
				if( objVehicle != null )
				{
					objVehicle.Dispose();
					objVehicle = null ;
				}
				arrClaimUnitInvolveColumn = null ;
			}
			return( m_objDocument );

		}

        //MITS # 29792 - 20121009 - bsharma33 - BR Id # 5.1.19
        /// <summary>
        /// Get Policy Coverages 
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// 
        /// <returns>Policy coverage data in form of a Xmldocument</returns>
        private XmlDocument GetPolicyCoverages(int p_iClaimID)
        {
            Claim objClaim = null;
            string ClaimantName = GetClaimants(p_iClaimID);
            string[] arrPolicyCoverages = 
											{
												"Coverage Type" , "Claim Limit" 
											};
            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Policy Coverage(s) for Claim - " + objClaim.ClaimNumber + (ClaimantName == string.Empty ? string.Empty : " * " + ClaimantName), arrPolicyCoverages);

                foreach (PolicyXCvgType objPolicyCoverage in objClaim.PrimaryPolicy.PolicyXCvgTypeList)
                {
                    arrPolicyCoverages[0] = objPolicyCoverage.GetCodeString("COVERAGE_TYPE", objPolicyCoverage.CoverageTypeCode, false);
                    //start:Added by nitin goel, Correct the statement to populate amount in correct format.
                    //arrPolicyCoverages[1] = Convert.ToString(objPolicyCoverage.ClaimLimit);
                    arrPolicyCoverages[1] = string.Format("{0:C}", objPolicyCoverage.ClaimLimit);
                    //end:Added by nitin goel
                    AddRow(arrPolicyCoverages);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetPolicyCoverages.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                arrPolicyCoverages = null;
            }
            return (m_objDocument);
        }
		/// <summary>
		/// Get the data for Policy 
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID as Policy ID</param>
		/// <returns>Policy data in form of a Xmldocument</returns>
		private XmlDocument GetClaimAttachedPolicy( int p_iClaimID , int p_iRequestedInfoID )
		{
			Claim objClaim = null ;
			Entity objEntity = null ;

			bool bEntityFound = false ;
			string sLastFirstName = "" ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13		
			string[] arrClaimAttachedPolicyColumn = 
											{
												"Policy Number" , "Policy Name" , "Policy Dates" , 
												"Insurer" , "Broker" 
											};					
			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if ( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                StartDocument("Policy(s) attached to Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimAttachedPolicyColumn);						
				
				arrClaimAttachedPolicyColumn[0] = objClaim.PrimaryPolicy.PolicyNumber ;
				arrClaimAttachedPolicyColumn[1] = objClaim.PrimaryPolicy.PolicyName ;
				arrClaimAttachedPolicyColumn[2] = Conversion.GetDBDateFormat( objClaim.PrimaryPolicy.EffectiveDate , "d" ) + " - " + Conversion.GetDBDateFormat( objClaim.PrimaryPolicy.ExpirationDate , "d" ) ;
				
				try
				{
					objEntity = objClaim.PrimaryPolicy.InsurerEntity ;
					bEntityFound = true ;					
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sLastFirstName = objEntity.GetLastFirstName(); 
				else
					sLastFirstName = "" ;

				arrClaimAttachedPolicyColumn[3] = sLastFirstName ;

				try
				{
					objEntity = objClaim.PrimaryPolicy.BrokerEntity ;
					bEntityFound = true ;					
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sLastFirstName = objEntity.GetLastFirstName(); 
				else
					sLastFirstName = "" ;
				arrClaimAttachedPolicyColumn[4] = sLastFirstName ;

				AddRow( arrClaimAttachedPolicyColumn );
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimAttachedPolicy.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				arrClaimAttachedPolicyColumn = null ;													
			}
			return( m_objDocument );
		}

		/// <summary>
		/// Get the data for Litigation
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Litigation data in form of a Xmldocument</returns>
		private XmlDocument GetClaimLitigations( int p_iClaimID )
		{
			Claim objClaim = null ;
			State objState = null;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13		
		
			string[] arrClaimLitigationsColumn = 
											{
												"Docket Number" , "Suit Date" , "Court Date" , "Venue State"
											};					
			bool bStateFound = false ;

			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if ( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                StartDocument("Litigation(s) involved with Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimLitigationsColumn);
				
				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
					
				foreach( ClaimXLitigation objClaimXLitigation in objClaim.LitigationList )
				{
					arrClaimLitigationsColumn[0] = objClaimXLitigation.DocketNumber ;
					arrClaimLitigationsColumn[1] = Conversion.GetDBDateFormat( objClaimXLitigation.SuitDate , "d" );
					arrClaimLitigationsColumn[2] = Conversion.GetDBDateFormat( objClaimXLitigation.CourtDate , "d" );
					
					try
					{
						objState.MoveTo( objClaimXLitigation.VenueStateId );
						bStateFound = true ;
					}
					catch
					{
						bStateFound = false ;
					}
					if( bStateFound )
						arrClaimLitigationsColumn[3] = objState.StateId + "   " + objState.StateName ;

					AddRow( arrClaimLitigationsColumn );
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimLitigations.DataError", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				arrClaimLitigationsColumn = null ;					
			}
			return( m_objDocument );
		}
		
		/// <summary>
		/// Get the data for LitDocket
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID as LitDocket ID</param>
		/// <returns>LitDocket data in form of a Xmldocument</returns>
		private XmlDocument GetClaimLitDocket( int p_iClaimID , int p_iRequestedInfoID )
		{
			Claim objClaim = null ;
			ClaimXLitigation objClaimXLitigation = null ;
			State objState = null ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13
		
			string[] arrClaimLitDocketColumn = { "" , "" };					

			string sStateName = "" ;
			bool bStateFound = false ;

			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if ( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

				objClaimXLitigation = ( ClaimXLitigation )m_objDataModelFactory.GetDataModelObject( "ClaimXLitigation" , false );
				if( p_iRequestedInfoID > 0 )
					objClaimXLitigation.MoveTo( p_iRequestedInfoID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXLitigationID", m_iClientId));
								
				arrClaimLitDocketColumn[0] = "Docket Number" ;
				arrClaimLitDocketColumn[1] = objClaimXLitigation.DocketNumber ;
                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Docket for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimLitDocketColumn);

				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
				try
				{
					objState.MoveTo( objClaimXLitigation.VenueStateId );
					bStateFound = true ;
				}
				catch
				{
					bStateFound = false ;
				}
				if( bStateFound )
					sStateName = objState.StateId + "   " + objState.StateName ;				

				arrClaimLitDocketColumn[0] = "Suit Date:" ;
				arrClaimLitDocketColumn[1] = Conversion.GetDBDateFormat( objClaimXLitigation.SuitDate , "d") ;
				AddRow( arrClaimLitDocketColumn );

				arrClaimLitDocketColumn[0] = "Court Date:" ;
				arrClaimLitDocketColumn[1] = Conversion.GetDBDateFormat( objClaimXLitigation.CourtDate , "d") ;
				AddRow( arrClaimLitDocketColumn );

				arrClaimLitDocketColumn[0] = "Venue State:" ;
				arrClaimLitDocketColumn[1] = sStateName ;
				AddRow( arrClaimLitDocketColumn );

				arrClaimLitDocketColumn[0] = "Litigation Type:" ;
				arrClaimLitDocketColumn[1] = GetCodeDescription( objClaimXLitigation.LitTypeCode );
				AddRow( arrClaimLitDocketColumn );

				arrClaimLitDocketColumn[0] = "Litigation Status:" ;
				arrClaimLitDocketColumn[1] = GetCodeDescription( objClaimXLitigation.LitStatusCode );
				AddRow( arrClaimLitDocketColumn );

				arrClaimLitDocketColumn[0] = "County:" ;
				arrClaimLitDocketColumn[1] =  objClaimXLitigation.County ;
				AddRow( arrClaimLitDocketColumn );

				arrClaimLitDocketColumn[0] = "Demand Allegations:" ;
				arrClaimLitDocketColumn[1] = objClaimXLitigation.DemandAllegations ;
				AddRow( arrClaimLitDocketColumn );

			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimLitDocket.DataError", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objClaimXLitigation != null )
				{
					objClaimXLitigation.Dispose();
					objClaimXLitigation = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				arrClaimLitDocketColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for LitAttorney
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID as LitAttorney ID</param>
		/// <returns>LitAttorney data in form of a Xmldocument</returns>
		private XmlDocument GetClaimLitAttorney( int p_iClaimID , int p_iRequestedInfoID )
		{
			Claim objClaim = null ;
			ClaimXLitigation objClaimXLitigation = null ;
			State objState = null ;
			Entity objEntity = null ;
					
			string[] arrClaimLitAttorneyColumn = { "" , "" };					
			
			string sStateName = "" ;
			string sEntityName = "" ;

			bool bStateFound = false ;
			bool bEntityFound = false ;

			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if ( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

				objClaimXLitigation = ( ClaimXLitigation )m_objDataModelFactory.GetDataModelObject( "ClaimXLitigation" , false );
				if( p_iRequestedInfoID > 0 )
					objClaimXLitigation.MoveTo( p_iRequestedInfoID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXLitigationID", m_iClientId));
								
				arrClaimLitAttorneyColumn[0] = "Attorney" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.GetLastFirstName() ;
                //MITS 34830 srajindersin 1/7/2014
                StartDocument( "Attorney for Claim - " + objClaim.ClaimNumber , arrClaimLitAttorneyColumn ) ;

				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
				try
				{
					objState.MoveTo( objClaimXLitigation.CoAttorneyEntity.StateId );
					bStateFound = true ;
				}
				catch
				{
					bStateFound = false ;
				}
				if( bStateFound )
					sStateName = objState.StateId + "   " + objState.StateName ;

				objEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
				try
				{
					objEntity.MoveTo( objClaimXLitigation.CoAttorneyEntity.ParentEid );	
					bEntityFound = true ;
				}
				catch
				{
					bEntityFound = false ;
				}
				if( bEntityFound )
					sEntityName = objEntity.FirstName + " " + objEntity.LastName ;				
				
				arrClaimLitAttorneyColumn[0] = "Street Address:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.Addr1 ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "Address Line 2:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.Addr2 ;
				AddRow(  arrClaimLitAttorneyColumn );

                arrClaimLitAttorneyColumn[0] = "Address Line 3:";
                arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.Addr3;
                AddRow(arrClaimLitAttorneyColumn);

                arrClaimLitAttorneyColumn[0] = "Address Line 4:";
                arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.Addr4;
                AddRow(arrClaimLitAttorneyColumn);

				arrClaimLitAttorneyColumn[0] = "City:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.City ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "State:" ;
				arrClaimLitAttorneyColumn[1] = sStateName ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "Zip Code:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.ZipCode ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "Country:" ;
				arrClaimLitAttorneyColumn[1] = GetCodeDescription( objClaimXLitigation.CoAttorneyEntity.CountryCode );
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "Phone:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.Phone1 ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "Alt. Phone:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.Phone2 ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "Fax Number:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.FaxNumber ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "E-mail Address:" ;
				arrClaimLitAttorneyColumn[1] = objClaimXLitigation.CoAttorneyEntity.EmailAddress ;
				AddRow(  arrClaimLitAttorneyColumn );

				arrClaimLitAttorneyColumn[0] = "Attorney Firm:" ;
				arrClaimLitAttorneyColumn[1] = sEntityName.Trim();
				AddRow(  arrClaimLitAttorneyColumn );
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimLitAttorney.DataError", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}				
				if( objClaimXLitigation != null )
				{
					objClaimXLitigation.Dispose();
					objClaimXLitigation = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				arrClaimLitAttorneyColumn = null ;
			}
			return( m_objDocument );
		}

		/// <summary>
		/// Get the data for LitJudge
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID as LitJudge ID</param>
		/// <returns>LitJudge data in form of a Xmldocument</returns>
		private XmlDocument GetClaimLitJudge( int p_iClaimID , int p_iRequestedInfoID  )
		{			
			ClaimXLitigation objClaimXLitigation = null ;
			State objState = null ;
					
			string[] arrClaimLitJudgeColumn = { "" , "" };					

			string sStateName = "" ;
			bool bStateFound = false ;
			
			try
			{
				objClaimXLitigation = ( ClaimXLitigation )m_objDataModelFactory.GetDataModelObject( "ClaimXLitigation" , false );
				if( p_iRequestedInfoID > 0 ) 
					objClaimXLitigation.MoveTo( p_iRequestedInfoID );				
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXLitigationID", m_iClientId));
								
				arrClaimLitJudgeColumn[0] = "Judge" ;
				arrClaimLitJudgeColumn[1] =  objClaimXLitigation.JudgeEntity.GetLastFirstName() ;
                StartDocument("Judge Details - " + objClaimXLitigation.JudgeEntity.GetLastFirstName(), arrClaimLitJudgeColumn);//MITS 34830 srajindersin 1/7/2014

				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
				try
				{
					objState.MoveTo( objClaimXLitigation.JudgeEntity.StateId );
					bStateFound = true ;
				}
				catch
				{
					bStateFound = false ;
				}
				if( bStateFound )
					sStateName = objState.StateId + "   " + objState.StateName ;				
				
				arrClaimLitJudgeColumn[0] = "Street Address:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.Addr1 ;
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "Address Line 2:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.Addr2 ;
				AddRow(  arrClaimLitJudgeColumn );

                arrClaimLitJudgeColumn[0] = "Address Line 3:";
                arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.Addr3;
                AddRow(arrClaimLitJudgeColumn);

                arrClaimLitJudgeColumn[0] = "Address Line 4:";
                arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.Addr4;
                AddRow(arrClaimLitJudgeColumn);


				arrClaimLitJudgeColumn[0] = "City:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.City ;
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "State:" ;
				arrClaimLitJudgeColumn[1] = sStateName ;
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "Zip Code:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.ZipCode ;
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "Country:" ;
				arrClaimLitJudgeColumn[1] = GetCodeDescription( objClaimXLitigation.JudgeEntity.CountryCode );
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "Work Phone:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.Phone1 ;
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "Alternate Phone:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.Phone2 ;
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "Fax Number:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.FaxNumber ;
				AddRow(  arrClaimLitJudgeColumn );

				arrClaimLitJudgeColumn[0] = "E-mail Address:" ;
				arrClaimLitJudgeColumn[1] = objClaimXLitigation.JudgeEntity.EmailAddress ;
				AddRow(  arrClaimLitJudgeColumn );				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimLitJudge.DataError", m_iClientId), p_objEx);				
			}
			finally
			{				
				if( objClaimXLitigation != null )
				{
					objClaimXLitigation.Dispose();
					objClaimXLitigation = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				arrClaimLitJudgeColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for LitExpert
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID as Litigation ID</param>
		/// <param name="p_iRequestedInfoChildID">Requested Info Child ID as Expert ID</param>
		/// <returns>LitExpert data in form of a Xmldocument</returns>
		private XmlDocument GetClaimLitExpert( int p_iClaimID , int p_iRequestedInfoID , int p_iRequestedInfoChildID )
		{
			Claim objClaim = null ;
			ClaimXLitigation objClaimXLitigation = null ;
			Expert objExpert = null ;
			State objState = null ;
					
			string[] arrClaimLitExpertColumn = { "" , "" };		
			
			string sStateName = "" ;
			bool bStateFound = false ;

			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if ( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

				objClaimXLitigation = ( ClaimXLitigation )m_objDataModelFactory.GetDataModelObject( "ClaimXLitigation" , false );
				if( p_iRequestedInfoID > 0 )	
					objClaimXLitigation.MoveTo( p_iRequestedInfoID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXLitigationID", m_iClientId));				

				foreach( Expert objExpertTemp in objClaimXLitigation.ExpertList )
				{
					if( objExpertTemp.ExpertEid == p_iRequestedInfoChildID )
					{
						objExpert = objExpertTemp ;
						break ;
					}
					if( objExpertTemp.ExpertEid == 0 )
						break ;
				}
				
				if( objExpert != null )
				{
					arrClaimLitExpertColumn[0] = "Expert Witness" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.GetLastFirstName()  ;
                    StartDocument("Witness for Claim - " + objClaim.ClaimNumber, arrClaimLitExpertColumn);//MITS 34830 srajindersin 1/7/2014
				
					objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );				
					try
					{
						objState.MoveTo( objExpert.ExpertEntity.StateId );
						bStateFound = true ;
					}
					catch
					{
						bStateFound = false ;
					}
					if( bStateFound )
						sStateName = objState.StateId + "   " + objState.StateName ;

					arrClaimLitExpertColumn[0] = "Witness Type:" ;
					arrClaimLitExpertColumn[1] = GetCodeDescription( objExpert.ExpertType ); 
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Specialty:" ;
					arrClaimLitExpertColumn[1] = objExpert.Specialty ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Rate:" ;
					arrClaimLitExpertColumn[1] = string.Format("{0:C}" , objExpert.Rate );   
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Street Address:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.Addr1 ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Address Line 2:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.Addr2 ;
					AddRow( arrClaimLitExpertColumn );

                    arrClaimLitExpertColumn[0] = "Address Line 3:";
                    arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.Addr3;
                    AddRow(arrClaimLitExpertColumn);

                    arrClaimLitExpertColumn[0] = "Address Line 4:";
                    arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.Addr4;
                    AddRow(arrClaimLitExpertColumn);

					arrClaimLitExpertColumn[0] = "City:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.City ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "State:" ;
					arrClaimLitExpertColumn[1] = sStateName ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Zip Code:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.ZipCode ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Country:" ;
					arrClaimLitExpertColumn[1] = GetCodeDescription( objExpert.ExpertEntity.CountryCode ); 
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Phone:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.Phone1 ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Alt. Phone:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.Phone2 ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "Fax Number:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.FaxNumber ;
					AddRow( arrClaimLitExpertColumn );

					arrClaimLitExpertColumn[0] = "E-mail Address:" ;
					arrClaimLitExpertColumn[1] = objExpert.ExpertEntity.EmailAddress ;
					AddRow( arrClaimLitExpertColumn );	
				}
				else
				{
					arrClaimLitExpertColumn[0] = "Expert Witness" ;
					arrClaimLitExpertColumn[1] = "Error, no expert witness attached to this claim with id " + p_iRequestedInfoChildID.ToString() ;
					StartDocument( "Witness for claim: " + objClaim.ClaimNumber , arrClaimLitExpertColumn );	
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimLitExpert.DataError", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}				
				if( objClaimXLitigation != null )
				{
					objClaimXLitigation.Dispose();
					objClaimXLitigation = null ;
				}
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
				if( objExpert != null )
				{
					objExpert.Dispose();
					objExpert = null ;
				}
				arrClaimLitExpertColumn = null ;
			}
			return( m_objDocument );
		}
        //skhare7 R8 enhancement
        /// <summary>
        /// Get the data for subrogation
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <returns>Litigation data in form of a Xmldocument</returns>
        private XmlDocument GetClaimSubrogations(int p_iClaimID)
        {
            Claim objClaim = null;
            Entity objEntity = null;
        
            string[] arrClaimSubrogationsColumn = 
											{
												"Subrogation Type" , "Status Date" , "Status" , "Specialist"
											};
       

            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                StartDocument("Subrogation(s) involved with Claim - " + objClaim.ClaimNumber, arrClaimSubrogationsColumn);
               
               	objEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );				
					
				

                foreach (ClaimXSubrogation objClaimXSubrogation in objClaim.SubrogationList)
                {
                    arrClaimSubrogationsColumn[0] = GetCodeDescription(objClaimXSubrogation.SubTypeCode); ;
                    arrClaimSubrogationsColumn[1] = Conversion.GetDBDateFormat(objClaimXSubrogation.StatusDate, "d");
                    arrClaimSubrogationsColumn[2]  = GetCodeDescription(objClaimXSubrogation.SubStatusCode);

                    if (objClaimXSubrogation.SubSpecialistEid > 0) 
                        objEntity.MoveTo(objClaimXSubrogation.SubSpecialistEid);
					
                   
                        arrClaimSubrogationsColumn[3] = objEntity.GetLastFirstName();
                    AddRow(arrClaimSubrogationsColumn);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimSubrogation.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                arrClaimSubrogationsColumn = null;
            }
            return (m_objDocument);
        }
		
        /// <summary>
        /// Get the data for Subrogation Details
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iRequestedInfoID">Requested Info ID as LitDocket ID</param>
        /// <returns>LitDocket data in form of a Xmldocument</returns>
        private XmlDocument GetClaimSubrogationDetails(int p_iClaimID, int p_iRequestedInfoID)
        {
            Claim objClaim = null;
            ClaimXSubrogation objClaimXsubrogation = null;
            Entity objEntity = null;

            string[] arrClaimSubDetailsColumn = { "", "" };

            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);	
                objClaimXsubrogation = (ClaimXSubrogation)m_objDataModelFactory.GetDataModelObject("ClaimXSubrogation", false);
                if (p_iRequestedInfoID > 0)
                    objClaimXsubrogation.MoveTo(p_iRequestedInfoID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXSubrogationID", m_iClientId));

                arrClaimSubDetailsColumn[0] = "Claim Number";
                arrClaimSubDetailsColumn[1] = objClaimXsubrogation.SubAdverseClaimNum;
                StartDocument("Subrogation Adverse Claim Number for Claim - " + objClaim.ClaimNumber, arrClaimSubDetailsColumn);//MITS 34830 srajindersin 1/7/2014

              

                arrClaimSubDetailsColumn[0] = "Type:";
                arrClaimSubDetailsColumn[1] = GetCodeDescription(objClaimXsubrogation.SubTypeCode);
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Status:";
                arrClaimSubDetailsColumn[1] = GetCodeDescription(objClaimXsubrogation.SubStatusCode);
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Status Date:";
                arrClaimSubDetailsColumn[1] = Conversion.GetDBDateFormat(objClaimXsubrogation.StatusDate, "d");
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Status Description:";
                arrClaimSubDetailsColumn[1] = GetCodeDescription(objClaimXsubrogation.SubStatusDescCode);
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Claim Number:";
                arrClaimSubDetailsColumn[1] = (objClaimXsubrogation.SubAdverseClaimNum);
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Policy Number:";
                arrClaimSubDetailsColumn[1] = objClaimXsubrogation.SubAdversePolicyNum;
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Amount Paid:";
                arrClaimSubDetailsColumn[1] = objClaimXsubrogation.AmountPaid.ToString(); ;
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Statute of Limitation Date:";
                arrClaimSubDetailsColumn[1] = Conversion.GetDBDateFormat(objClaimXsubrogation.StatuteLimitationDate, "d");
                AddRow(arrClaimSubDetailsColumn);
              

                arrClaimSubDetailsColumn[0] = "Specialist Name";
                if (objClaimXsubrogation.SubSpecialistEid > 0)
                {
                    objEntity.MoveTo(objClaimXsubrogation.SubSpecialistEid);
                    arrClaimSubDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimSubDetailsColumn[1] = string.Empty;

                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Adverse Party:";
                if (objClaimXsubrogation.SubAdversePartyEid > 0)
                {
                    objEntity.MoveTo(objClaimXsubrogation.SubAdversePartyEid);
                    arrClaimSubDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimSubDetailsColumn[1] = string.Empty;
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Insurance Company:";
                if (objClaimXsubrogation.SubAdverseInsCoEid > 0)
                {
                    objEntity.MoveTo(objClaimXsubrogation.SubAdverseInsCoEid);
                    arrClaimSubDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimSubDetailsColumn[1] = string.Empty;
                AddRow(arrClaimSubDetailsColumn);

                arrClaimSubDetailsColumn[0] = "Subrogation Adjuster:";
                if (objClaimXsubrogation.SubAdvAdjusterEid > 0)
                {
                    objEntity.MoveTo(objClaimXsubrogation.SubAdvAdjusterEid);
                    arrClaimSubDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimSubDetailsColumn[1] = string.Empty;
                AddRow(arrClaimSubDetailsColumn);

               
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimSubrogation.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objClaimXsubrogation != null)
                {
                    objClaimXsubrogation.Dispose();
                    objClaimXsubrogation = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
               
                arrClaimSubDetailsColumn = null;
            }
            return (m_objDocument);
        }
        //skhare7 R8 enhancement
        /// <summary>
        /// Get the data for subrogation
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <returns>Arbitrations data in form of a Xmldocument</returns>
        private XmlDocument GetClaimArbitrations(int p_iClaimID)
        {
            Claim objClaim = null;
           

            string[] arrClaimArbitrationsColumn = 
											{
												"Arbitration Type" ,"Date Filed", "Status" , "Arbitration Party"
											};


            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                StartDocument("Arbitration(s) involved with Claim - " + objClaim.ClaimNumber, arrClaimArbitrationsColumn);

              


                foreach (ClaimXArbitration objClaimXArbitration in objClaim.ArbitrationList)
                {
                    arrClaimArbitrationsColumn[0] = GetCodeDescription(objClaimXArbitration.ArbTypeCode); ;
                    arrClaimArbitrationsColumn[1] = Conversion.GetDBDateFormat(objClaimXArbitration.DateFiled, "d");
                    arrClaimArbitrationsColumn[2] = GetCodeDescription(objClaimXArbitration.ArbStatusCode);

                   // if (objClaimXArbitration.ArbPartyCode > 0)
                      //  objEntity.MoveTo(objClaimXArbitration.SubSpecialistEid);


                    arrClaimArbitrationsColumn[3] = GetCodeDescription(objClaimXArbitration.ArbPartyCode);
                    AddRow(arrClaimArbitrationsColumn);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimArbitrations.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                
                arrClaimArbitrationsColumn = null;
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get the data for Arbitrations Details
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iRequestedInfoID">Requested Info ID as LitDocket ID</param>
        /// <returns>LitDocket data in form of a Xmldocument</returns>
        private XmlDocument GetClaimArbitrationDetails(int p_iClaimID, int p_iRequestedInfoID)
        {
            Claim objClaim = null;
            ClaimXArbitration objClaimXArbitration = null;
            Entity objEntity = null;

            string[] arrClaimArbDetailsColumn = { "", "" };

            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                objClaimXArbitration = (ClaimXArbitration)m_objDataModelFactory.GetDataModelObject("ClaimXArbitration", false);
                if (p_iRequestedInfoID > 0)
                    objClaimXArbitration.MoveTo(p_iRequestedInfoID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXArbitrationID", m_iClientId));

                arrClaimArbDetailsColumn[0] = "Claim Number";
                arrClaimArbDetailsColumn[1] = objClaimXArbitration.ArbAdversClaimNum;
                StartDocument("Adverse Claim Number for Claim - " + objClaim.ClaimNumber, arrClaimArbDetailsColumn);//MITS 34830 srajindersin 1/7/2014



                arrClaimArbDetailsColumn[0] = "Type:";
                arrClaimArbDetailsColumn[1] = GetCodeDescription(objClaimXArbitration.ArbPartyCode);
                AddRow(arrClaimArbDetailsColumn);

                arrClaimArbDetailsColumn[0] = "Status:";
                arrClaimArbDetailsColumn[1] = GetCodeDescription(objClaimXArbitration.ArbStatusCode);
                AddRow(arrClaimArbDetailsColumn);


                if (objClaimXArbitration.DateFiled != string.Empty)
                {
                    arrClaimArbDetailsColumn[0] = "Filed Date:";
                    arrClaimArbDetailsColumn[1] = Conversion.GetDBDateFormat(objClaimXArbitration.DateFiled, "d");
                    AddRow(arrClaimArbDetailsColumn);
                }

                arrClaimArbDetailsColumn[0] = "Claim Number:";
                arrClaimArbDetailsColumn[1] = (objClaimXArbitration.ArbAdversClaimNum);
                AddRow(arrClaimArbDetailsColumn);

                if (objClaimXArbitration.DateHearing != string.Empty)
                {
                    arrClaimArbDetailsColumn[0] = "Hearing Date:";
                    arrClaimArbDetailsColumn[1] = Conversion.GetDBDateFormat(objClaimXArbitration.DateHearing, "d");
                    AddRow(arrClaimArbDetailsColumn);
                }

                arrClaimArbDetailsColumn[0] = "Adverse Party:";
                if (objClaimXArbitration.ArbAdversePartyEid > 0)
                {
                    objEntity.MoveTo(objClaimXArbitration.ArbAdversePartyEid);
                    arrClaimArbDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimArbDetailsColumn[1] = string.Empty;
                AddRow(arrClaimArbDetailsColumn);

                arrClaimArbDetailsColumn[0] = "Insurance Company:";
                if (objClaimXArbitration.ArbAdverseCoEid > 0)
                {
                    objEntity.MoveTo(objClaimXArbitration.ArbAdverseCoEid);
                    arrClaimArbDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimArbDetailsColumn[1] = string.Empty;
                AddRow(arrClaimArbDetailsColumn);

                arrClaimArbDetailsColumn[0] = "Adjuster:";
                if (objClaimXArbitration.ArbAdvAdjusterEid > 0)
                {
                    objEntity.MoveTo(objClaimXArbitration.ArbAdvAdjusterEid);
                    arrClaimArbDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimArbDetailsColumn[1] = string.Empty;
                AddRow(arrClaimArbDetailsColumn);


            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimArbitrations.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objClaimXArbitration != null)
                {
                    objClaimXArbitration.Dispose();
                    objClaimXArbitration = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }

                arrClaimArbDetailsColumn = null;
            }
            return (m_objDocument);
        }

        //skhare7 R8 enhancement
        /// <summary>
        /// Get the data for subrogation
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <returns>Arbitrations data in form of a Xmldocument</returns>
        private XmlDocument GetClaimLiability(int p_iClaimID)
        {
            Claim objClaim = null;

            Entity objEntity = null;
            string[] arrClaimLiabilytColumn = 
											{
												"Liability Type" ,"Damage Type" , "Party Affected"
											};


            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);		
                StartDocument("Liability(s) involved with Claim - " + objClaim.ClaimNumber, arrClaimLiabilytColumn);




                foreach (ClaimXLiabilityLoss objClaimXLiability in objClaim.LiabilityLossList)
                {
                    arrClaimLiabilytColumn[0] = GetCodeDescription(objClaimXLiability.LiabilityType);
                    arrClaimLiabilytColumn[1] = GetCodeDescription(objClaimXLiability.DamageType);
                    if (objClaimXLiability.PartyAffected > 0)
                    {
                        objEntity.MoveTo(objClaimXLiability.PartyAffected);
                        arrClaimLiabilytColumn[2] = objEntity.GetLastFirstName();
                    }
                  

                    AddRow(arrClaimLiabilytColumn);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimLiability.DataError",m_iClientId), p_objEx);//sharishkumar Rmacloud
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                arrClaimLiabilytColumn = null;
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get the data for Liability Loss Details
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iRequestedInfoID">Requested Info ID as LitDocket ID</param>
        /// <returns>LitDocket data in form of a Xmldocument</returns>
        private XmlDocument GetClaimLiabilityDetails(int p_iClaimID, int p_iRequestedInfoID)
        {
            Claim objClaim = null;
            ClaimXLiabilityLoss objClaimXLiablity = null;
            Entity objEntity = null;

            string[] arrClaimLiabDetailsColumn = { "", "" };

            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                objClaimXLiablity = (ClaimXLiabilityLoss)m_objDataModelFactory.GetDataModelObject("ClaimXLiabilityLoss", false);
                if (p_iRequestedInfoID > 0)
                    objClaimXLiablity.MoveTo(p_iRequestedInfoID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXLiabilityLossID", m_iClientId));

                arrClaimLiabDetailsColumn[0] = "Liablity Type";
                arrClaimLiabDetailsColumn[1] = GetCodeDescription(objClaimXLiablity.LiabilityType);
                StartDocument("Laiblity Loss Type for Claim - " + arrClaimLiabDetailsColumn[1], arrClaimLiabDetailsColumn);



                arrClaimLiabDetailsColumn[0] = "Damage Type:";
                arrClaimLiabDetailsColumn[1] = GetCodeDescription(objClaimXLiablity.DamageType);
                AddRow(arrClaimLiabDetailsColumn);

                arrClaimLiabDetailsColumn[0] = "Policy Number:";
                arrClaimLiabDetailsColumn[1] = objClaimXLiablity.PolicyNumber;
                AddRow(arrClaimLiabDetailsColumn);




                arrClaimLiabDetailsColumn[0] = "Affected Party:";
                if (objClaimXLiablity.PartyAffected > 0)
                {
                    objEntity.MoveTo(objClaimXLiablity.PartyAffected);
                    arrClaimLiabDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimLiabDetailsColumn[1] = string.Empty;
                AddRow(arrClaimLiabDetailsColumn);

                arrClaimLiabDetailsColumn[0] = "Company:";
                if (objClaimXLiablity.Company > 0)
                {
                    objEntity.MoveTo(objClaimXLiablity.Company);
                    arrClaimLiabDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimLiabDetailsColumn[1] = string.Empty;
                AddRow(arrClaimLiabDetailsColumn);

                arrClaimLiabDetailsColumn[0] = "Policy Holder:";
                if (objClaimXLiablity.PolicyHolder > 0)
                {
                    objEntity.MoveTo(objClaimXLiablity.PolicyHolder);
                    arrClaimLiabDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimLiabDetailsColumn[1] = string.Empty;
                AddRow(arrClaimLiabDetailsColumn);


            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimLiability.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objClaimXLiablity != null)
                {
                    objClaimXLiablity.Dispose();
                    objClaimXLiablity = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }

                arrClaimLiabDetailsColumn = null;
            }
            return (m_objDocument);
        }

        //skhare7 R8 enhancement
        /// <summary>
        /// Get the data for subrogation
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <returns>Property Loss data in form of a Xmldocument</returns>
        private XmlDocument GetClaimPropertyLoss(int p_iClaimID)
        {
            Claim objClaim = null;

            Entity objEntity = null;
            string[] arrClaimPropertytColumn = 
											{
												"Property Type" ,"Estimated Damage" , "Owner", "Date Reported"
											};


            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                StartDocument("Property Loss(s) involved with Claim - " + objClaim.ClaimNumber, arrClaimPropertytColumn);




                foreach (ClaimXPropertyLoss objClaimXProperty in objClaim.PropertyLossList)
                {
                    arrClaimPropertytColumn[0] = GetCodeDescription(objClaimXProperty.PropertyType);
                    arrClaimPropertytColumn[1] = objClaimXProperty.EstDamage.ToString();
                    if (objClaimXProperty.Owner > 0)
                    {
                        objEntity.MoveTo(objClaimXProperty.Owner);
                        arrClaimPropertytColumn[2] = objEntity.GetLastFirstName();
                    }
                    if (objClaimXProperty.Date !=string.Empty)
                    {

                        arrClaimPropertytColumn[3] = Conversion.GetDBDateFormat(objClaimXProperty.Date, "d");
                    }


                    AddRow(arrClaimPropertytColumn);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimPropertyLoss.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                arrClaimPropertytColumn = null;
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get the data for Property Loss Details
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iRequestedInfoID">Requested Info ID</param>
        /// <returns>Xmldocument</returns>
        private XmlDocument GetClaimPropertyLossDetails(int p_iClaimID, int p_iRequestedInfoID)
        {
            Claim objClaim = null;
            ClaimXPropertyLoss objClaimXProperty = null;
            Entity objEntity = null;

            string[] arrClaimPropDetailsColumn = { "", "" };

            try
            {
                // Get the reference of an claim having claim_id p_iClaimID
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                objClaimXProperty = (ClaimXPropertyLoss)m_objDataModelFactory.GetDataModelObject("ClaimXPropertyLoss", false);
                if (p_iRequestedInfoID > 0)
                    objClaimXProperty.MoveTo(p_iRequestedInfoID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimXPropertyLossID", m_iClientId));

                arrClaimPropDetailsColumn[0] = "Property Type";
                arrClaimPropDetailsColumn[1] = GetCodeDescription(objClaimXProperty.PropertyType);
                StartDocument("Property Type for Claim - " + arrClaimPropDetailsColumn[1], arrClaimPropDetailsColumn);



                arrClaimPropDetailsColumn[0] = "Estimated Damage:";
                arrClaimPropDetailsColumn[1] = (objClaimXProperty.EstDamage.ToString());
                AddRow(arrClaimPropDetailsColumn);

                arrClaimPropDetailsColumn[0] = "Policy Number:";
                arrClaimPropDetailsColumn[1] = objClaimXProperty.PolicyNumber;
                AddRow(arrClaimPropDetailsColumn);


                arrClaimPropDetailsColumn[0] = "Location Of Theft:";
                arrClaimPropDetailsColumn[1] = GetCodeDescription(objClaimXProperty.LocOfTheft);
                AddRow(arrClaimPropDetailsColumn);


                arrClaimPropDetailsColumn[0] = "Owner:";
                if (objClaimXProperty.Owner > 0)
                {
                    objEntity.MoveTo(objClaimXProperty.Owner);
                    arrClaimPropDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimPropDetailsColumn[1] = string.Empty;
                AddRow(arrClaimPropDetailsColumn);

                arrClaimPropDetailsColumn[0] = "Company:";
                if (objClaimXProperty.Company > 0)
                {
                    objEntity.MoveTo(objClaimXProperty.Company);
                    arrClaimPropDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimPropDetailsColumn[1] = string.Empty;
                AddRow(arrClaimPropDetailsColumn);

                arrClaimPropDetailsColumn[0] = "Policy Holder:";
                if (objClaimXProperty.PolicyHolder > 0)
                {
                    objEntity.MoveTo(objClaimXProperty.PolicyHolder);
                    arrClaimPropDetailsColumn[1] = objEntity.GetLastFirstName();
                }
                else
                    arrClaimPropDetailsColumn[1] = string.Empty;
                AddRow(arrClaimPropDetailsColumn);


            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimPropertyLoss.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objClaimXProperty != null)
                {
                    objClaimXProperty.Dispose();
                    objClaimXProperty = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }

                arrClaimPropDetailsColumn = null;
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get the data for Property Loss Salvage Details
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iRequestedInfoID">Requested Info ID</param>
        /// <returns>Xmldocument</returns>
        private XmlDocument GetClaimPropertyLossSalavegDetails(int p_iClaimID)
        {
           
            Entity objEntity = null;
            string sSql = string.Empty;
            bool bFlag = false;
            DbReader objReader = null; 
            string[] arrClaimPropSalvageColumn = { "", "" };

            try
            {
                sSql = "SELECT CONTROLNUMBER,SALVAGE_TYPE,SALVAGE_STATUS,CLOSE_DATE,CUTOFF_DATE,BUYER_EID,SOLD_DATE,SALE_PRICE,STOCKNUMBER,CLAIM_X_PROPERTYLOSS.ROW_ID FROM SALVAGE,CLAIM_X_PROPERTYLOSS " +
                " WHERE CLAIM_ID=" + p_iClaimID + " AND PARENT_NAME='ClaimXPropertyLoss' AND CLAIM_X_PROPERTYLOSS.ROW_ID=SALVAGE.PARENT_ID";
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                objReader = DbFactory.GetDbReader( m_sConnectionString , sSql );

                if (objReader != null)
                {
                    if (objReader.Read())
                    {

                        arrClaimPropSalvageColumn[0] = "Control Number:";
                        arrClaimPropSalvageColumn[1] = (objReader.GetValue("CONTROLNUMBER").ToString());

                        StartDocument("Salvage For Control Number - " + arrClaimPropSalvageColumn[1], arrClaimPropSalvageColumn);

                        arrClaimPropSalvageColumn[0] = "Salvage Type";
                        arrClaimPropSalvageColumn[1] = GetCodeDescription(Conversion.CastToType<int>(objReader.GetValue("SALVAGE_TYPE").ToString(), out bFlag));

                        AddRow(arrClaimPropSalvageColumn);

                        arrClaimPropSalvageColumn[0] = "Salvage Status";
                        arrClaimPropSalvageColumn[1] = GetCodeDescription(Conversion.CastToType<int>(objReader.GetValue("SALVAGE_STATUS").ToString(), out bFlag));

                        AddRow(arrClaimPropSalvageColumn);

                        arrClaimPropSalvageColumn[0] = "Stock Number:";
                        arrClaimPropSalvageColumn[1] = (objReader.GetValue("STOCKNUMBER").ToString());
                        AddRow(arrClaimPropSalvageColumn);


                        arrClaimPropSalvageColumn[0] = "Close Date:";
                        arrClaimPropSalvageColumn[1] = Conversion.GetDBDateFormat(objReader.GetValue("CLOSE_DATE").ToString(), "d");
                        AddRow(arrClaimPropSalvageColumn);

                        arrClaimPropSalvageColumn[0] = "Cut Off Date:";
                        arrClaimPropSalvageColumn[1] = Conversion.GetDBDateFormat(objReader.GetValue("CUTOFF_DATE").ToString(), "d");
                        AddRow(arrClaimPropSalvageColumn);

                        arrClaimPropSalvageColumn[0] = "Buyer:";
                        if (Conversion.CastToType<int>(objReader.GetValue("BUYER_EID").ToString(), out bFlag) > 0)
                        {
                            objEntity.MoveTo(Conversion.CastToType<int>(objReader.GetValue("BUYER_EID").ToString(), out bFlag));
                            arrClaimPropSalvageColumn[1] = objEntity.GetLastFirstName();
                        }
                        else
                            arrClaimPropSalvageColumn[1] = string.Empty;
                        AddRow(arrClaimPropSalvageColumn);

                        arrClaimPropSalvageColumn[0] = "Sold Date:";
                        arrClaimPropSalvageColumn[1] = Conversion.GetDBDateFormat(objReader.GetValue("SOLD_DATE").ToString(), "d");
                        AddRow(arrClaimPropSalvageColumn);

                        arrClaimPropSalvageColumn[0] = "Sale Price:";
                        arrClaimPropSalvageColumn[1] = objReader.GetValue("SALE_PRICE").ToString();
                        AddRow(arrClaimPropSalvageColumn);


                    }
                }

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetPropertySalvage.DataError", m_iClientId), p_objEx);
            }
            finally
            {
               
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }

                arrClaimPropSalvageColumn = null;
            }
            return (m_objDocument);
        }
        /// <summary>
        /// Get the data for unit Loss Salvage Details
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
      
        /// <returns>Xmldocument</returns>
        private XmlDocument GetClaimUnitSalavageDetails(int p_iClaimID)
        {
            bool bFlag = false;
            Entity objEntity = null;
            string sSql = string.Empty;
            DbReader objReader = null;
            string[] arrClaimUnitSalvageColumn = { "", "" };
            //Changed By agupta298 - PMC GAP08 - NMVTIS Reporting - RMA-4694
            bool bIsTheftRecovered = false;
            SysSettings objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
            //Changed By agupta298 - PMC GAP08 - NMVTIS Reporting - RMA-4694

            try
            {
                //Changed By agupta298 - PMC GAP08 - NMVTIS Reporting - RMA-4694
                //sSql = "SELECT CONTROLNUMBER,SALVAGE_TYPE,SALVAGE_STATUS,CLOSE_DATE,CUTOFF_DATE,BUYER_EID,SOLD_DATE,SALE_PRICE,STOCKNUMBER,UNIT_X_CLAIM.UNIT_ID FROM SALVAGE,UNIT_X_CLAIM " +
                sSql = "SELECT CONTROLNUMBER,SALVAGE_TYPE,SALVAGE_STATUS,CLOSE_DATE,CUTOFF_DATE,BUYER_EID,SOLD_DATE,SALE_PRICE,STOCKNUMBER,UNIT_X_CLAIM.UNIT_ID,MILEAGE,TITLE_NUMBER,STATE_BRAND_CODE,THEFT_RECOVERED,SALVAGE_DSGND_DATE,SALVAGE_REASON,SALVAGE_SUBROGATION FROM SALVAGE,UNIT_X_CLAIM " +
             " WHERE CLAIM_ID=" + p_iClaimID + " AND PARENT_NAME='UnitXClaim' AND UNIT_X_CLAIM.UNIT_ROW_ID=SALVAGE.PARENT_ID";
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {

                        arrClaimUnitSalvageColumn[0] = "Control Number:";
                        arrClaimUnitSalvageColumn[1] = (objReader.GetValue("CONTROLNUMBER").ToString());
                      
                        StartDocument("Salvage For Control Number - " + arrClaimUnitSalvageColumn[1], arrClaimUnitSalvageColumn);

                        arrClaimUnitSalvageColumn[0] = "Salvage Type";
                        arrClaimUnitSalvageColumn[1] = GetCodeDescription(Conversion.CastToType<int>(objReader.GetValue("SALVAGE_TYPE").ToString(), out bFlag));

                        AddRow(arrClaimUnitSalvageColumn);

                        arrClaimUnitSalvageColumn[0] = "Salvage Status";
                        arrClaimUnitSalvageColumn[1] = GetCodeDescription(Conversion.CastToType<int>(objReader.GetValue("SALVAGE_STATUS").ToString(), out bFlag));

                        AddRow(arrClaimUnitSalvageColumn);

                        arrClaimUnitSalvageColumn[0] = "Stock Number:";
                        arrClaimUnitSalvageColumn[1] = (objReader.GetValue("STOCKNUMBER").ToString());
                        AddRow(arrClaimUnitSalvageColumn);


                        arrClaimUnitSalvageColumn[0] = "Close Date:";
                        arrClaimUnitSalvageColumn[1] = Conversion.GetDBDateFormat(objReader.GetValue("CLOSE_DATE").ToString(), "d");
                        AddRow(arrClaimUnitSalvageColumn);

                        arrClaimUnitSalvageColumn[0] = "Cut Off Date:";
                        arrClaimUnitSalvageColumn[1] = Conversion.GetDBDateFormat(objReader.GetValue("CUTOFF_DATE").ToString(), "d");
                        AddRow(arrClaimUnitSalvageColumn);

                        arrClaimUnitSalvageColumn[0] = "Buyer:";
                        if (Conversion.CastToType<int>(objReader.GetValue("BUYER_EID").ToString(),out bFlag) > 0)
                        {
                            objEntity.MoveTo(Conversion.CastToType<int>(objReader.GetValue("BUYER_EID").ToString(),out bFlag));
                            arrClaimUnitSalvageColumn[1] = objEntity.GetLastFirstName();
                        }
                        else
                            arrClaimUnitSalvageColumn[1] = string.Empty;
                        AddRow(arrClaimUnitSalvageColumn);

                        arrClaimUnitSalvageColumn[0] = "Sold Date:";
                        arrClaimUnitSalvageColumn[1] = Conversion.GetDBDateFormat(objReader.GetValue("SOLD_DATE").ToString(), "d");
                        AddRow(arrClaimUnitSalvageColumn);

                        arrClaimUnitSalvageColumn[0] = "Sale Price:";
                        arrClaimUnitSalvageColumn[1] = objReader.GetValue("SALE_PRICE").ToString();
                        AddRow(arrClaimUnitSalvageColumn);

                        //Added By agupta298 - PMC GAP08 - NMVTIS Reporting - Start - RMA-4694
                        if (objSysSettings.UseNMVTISReqFields)
                        {
                            arrClaimUnitSalvageColumn[0] = "Mileage/Odometer Reading:";
                            arrClaimUnitSalvageColumn[1] = Convert.ToString(objReader.GetValue("MILEAGE"));
                            AddRow(arrClaimUnitSalvageColumn);

                            arrClaimUnitSalvageColumn[0] = "Title Number:";
                            arrClaimUnitSalvageColumn[1] = Convert.ToString(objReader.GetValue("TITLE_NUMBER"));
                            AddRow(arrClaimUnitSalvageColumn);

                            arrClaimUnitSalvageColumn[0] = "State Brand:";
                            arrClaimUnitSalvageColumn[1] = GetCodeDescription(Conversion.CastToType<int>(objReader.GetValue("STATE_BRAND_CODE").ToString(), out bFlag));
                            AddRow(arrClaimUnitSalvageColumn);

                            arrClaimUnitSalvageColumn[0] = "Recovered Theft:";
                            bIsTheftRecovered = objReader.GetBoolean("THEFT_RECOVERED");
                            arrClaimUnitSalvageColumn[1] = bIsTheftRecovered ? "Yes" : "No";
                            AddRow(arrClaimUnitSalvageColumn);

                            arrClaimUnitSalvageColumn[0] = "Date Designated Salvage:";
                            arrClaimUnitSalvageColumn[1] = Conversion.GetDBDateFormat(objReader.GetValue("SALVAGE_DSGND_DATE").ToString(), "d");
                            AddRow(arrClaimUnitSalvageColumn);

                            arrClaimUnitSalvageColumn[0] = "Reason for Determination:";
                            arrClaimUnitSalvageColumn[1] = Convert.ToString(objReader.GetValue("SALVAGE_REASON"));
                            AddRow(arrClaimUnitSalvageColumn);

                            arrClaimUnitSalvageColumn[0] = "Salvage/Subrogation:";
                            arrClaimUnitSalvageColumn[1] = Convert.ToString(objReader.GetValue("SALVAGE_SUBROGATION"));
                            AddRow(arrClaimUnitSalvageColumn);
                        }
                        //Added By agupta298 - PMC GAP08 - NMVTIS Reporting - End - RMA-4694
                    }
                }

            

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.UnitSalvage.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }

                arrClaimUnitSalvageColumn = null;
                objSysSettings = null;
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get the data for Claimant's enhanced notes
        /// Added by gbindra MITS#34104
        /// </summary>
        /// <param name="p_iEventId">Event ID</param>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iClaimantID">Claimant Row ID</param>
        /// <returns>Xmldocument</returns>
        private XmlDocument GetClaimantEnhancedNotes(int p_iEventId, int p_iClaimID, int p_iClaimantID)
        {
            ProgressNotesManager objProgressNotes = null;
            XmlDocument objDocument = null;
            XmlNode objMainNode = null;
            XmlElement objTempNode = null;
            int flag = 0;
            string[] arrEnhancedNotesColumn = { 
											  "Activity Date" 
											  ,"Attached To" 
											  ,"Note Type"
											  ,"Note Text" 
											  ,"Entered By"
                                              ,"Subject"
										  };
            try
            {
                StartDocument("Enhanced Notes for Claimant.", arrEnhancedNotesColumn);
                objProgressNotes = new ProgressNotesManager(this.m_sDsnName, this.m_sUserName, this.m_sPassword, m_iClientId);
                objProgressNotes.m_UserLogin = this.m_UserLogin;
                objDocument = objProgressNotes.OnLoad(p_iEventId, p_iClaimID, false, "", -1, defaultSort, "DESC", false, 0, "", "", "", p_iClaimantID);
                objMainNode = objDocument.SelectSingleNode("//ProgressNotes");

                if (objMainNode != null)
                {
                    foreach (XmlNode objNode in objMainNode.ChildNodes)
                    {
                        objTempNode = (XmlElement)objNode.SelectSingleNode("DateEntered");
                        if (objTempNode != null)
                        {
                            arrEnhancedNotesColumn[0] = objTempNode.InnerText;

                        }
                        else
                        {
                            flag++;
                        }
                        objTempNode = (XmlElement)objNode.SelectSingleNode("Claimant");
                        if (objTempNode != null)
                        {
                            arrEnhancedNotesColumn[1] = "CLAIMANT: " + objTempNode.InnerText; ;
                        }
                        else
                        {
                            flag++;
                        }
                        objTempNode = (XmlElement)objNode.SelectSingleNode("NoteTypeCode");
                        if (objTempNode != null)
                        {
                            arrEnhancedNotesColumn[2] = objTempNode.InnerText; ;
                        }
                        else
                        {
                            flag++;
                        }
                        objTempNode = (XmlElement)objNode.SelectSingleNode("NoteMemo");
                        if (objTempNode != null)
                        {
                            arrEnhancedNotesColumn[3] = objTempNode.InnerText;
                        }
                        else
                        {
                            flag++;
                        }
                        objTempNode = (XmlElement)objNode.SelectSingleNode("EnteredByName");
                        if (objTempNode != null)
                        {
                            arrEnhancedNotesColumn[4] = objTempNode.InnerText; ;
                        }
                        else
                        {
                            flag++;
                        }

                        objTempNode = (XmlElement)objNode.SelectSingleNode("Subject");
                        if (objTempNode != null)
                        {
                            arrEnhancedNotesColumn[5] = objTempNode.InnerText; ;
                        }
                        else
                        {
                            flag++;
                        }
                        if (flag != 6)
                        {
                            AddRow(arrEnhancedNotesColumn);
                        }

                        flag = 0;
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimantNotes.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                arrEnhancedNotesColumn = null;
                if (objProgressNotes != null)
                {
                    objProgressNotes.Dispose();
                    objProgressNotes = null;
                }
            }

            return (m_objDocument);			
        }
        
        private XmlDocument GetClaimByBenefit(int p_iClaimID)//parijat :21784
        {
            return (GetClaimByBenefit(p_iClaimID, 0));
        }
		/// <summary>
		/// Get the data for ClaimByBenefit
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim By Benefits in form of a Xmldocument</returns>
        private XmlDocument GetClaimByBenefit(int p_iClaimID, int p_iClaimantID)//parijat :21784
		{
			Claim objClaim = null ;
			ArrayList arrlstPayment = null ;
			Payment objPayment = null ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

			string[] arrClaimByBenefitColumn = 
											{
												"Benefit Type" , "Total " , "From" , "To" , "Paid " , "Ave. Wkly. Rate"
											};						
			int iIndex = 0 ;

			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

				arrlstPayment = new ArrayList();

				CreatePaymentArray( objClaim.ClaimId , objClaim.FilingStateId , arrlstPayment,p_iClaimantID );

                //MITS 34830 srajindersin 1/7/2014
                StartDocument("Payment history, Benefit Summary for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimByBenefitColumn);

				for( iIndex = 0 ; iIndex < arrlstPayment.Count ; iIndex++ )
				{
					objPayment = (Payment)arrlstPayment[iIndex] ;

					if( objPayment.Paid == 0.0  )
					{
						arrClaimByBenefitColumn[0] = "-" + objPayment.CodeDesc.Trim() ;
						arrClaimByBenefitColumn[1] = "" ;
					}
					else
					{
						arrClaimByBenefitColumn[0] = objPayment.CodeDesc.Trim() ;
						arrClaimByBenefitColumn[1] = string.Format("{0:C}" , objPayment.Paid ); 
					}
					arrClaimByBenefitColumn[2] = Conversion.GetDBDateFormat( objPayment.FromDate , "d" );
					arrClaimByBenefitColumn[3] = Conversion.GetDBDateFormat( objPayment.ToDate , "d" );

					if( objPayment.Amount == 0.0 )
						arrClaimByBenefitColumn[4] = "" ;
					else
						arrClaimByBenefitColumn[4] = string.Format("{0:C}" , objPayment.Amount ); 

					if( objPayment.AvgRate == 0.0 )
						arrClaimByBenefitColumn[5] = "" ;
					else
					{
                        //rsushilaggar MITS 24562 Date 08/30/2011
                        //if( objPayment.LumSum )
                        //    arrClaimByBenefitColumn[5] = "Lump Sum" ;
                        //else
							arrClaimByBenefitColumn[5] = string.Format("{0:C}" , objPayment.AvgRate ); 
					}

					AddRow( arrClaimByBenefitColumn );
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimByBenefit.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}		
				arrlstPayment = null ;
				objPayment = null;
				arrClaimByBenefitColumn = null ;
			}
			return( m_objDocument );
		}
		/// <summary>
		/// Get the data for Claim Notes
		/// </summary>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Claim Notes in form of a Xmldocument</returns>
		private XmlDocument GetClaimNotes( int p_iClaimID )
		{
			Claim objClaim = null ;
			Event objEvent = null ;
			Entity objEntity = null ;
			ArrayList arrlstClaimNotes = null ;
			ClaimNotes objClaimNotes = null ;
			ClaimNotes objLastClaimNotes = null ;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13

            DbReader objReader = null;  // Added by csingh7 R6 Claim Comment Enhancement
            string sClaimComment = "";
            string sEventComment = "";
			string sSQL = "" ;	
		          
			int iIndex = 0 ;
			bool bSortCompleted = false ;
			bool bEntityFound = false ;
			string sLastFirstName = "" ;
			
            //MITS - 10226 - Changed the name of header from "Date" to "Date Entered" for better user understandability
			//string[] arrClaimNotesColumn = { "Type" , "Date" , "User" , "Comments" } ;						
            string[] arrClaimNotesColumn = { "Type", "Date Entered", "User", "Comments" };

			try
			{
				// Get the reference of an claim having claim_id p_iClaimID
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimID > 0 )
					objClaim.MoveTo( p_iClaimID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                StartDocument("Notes for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrClaimNotesColumn);	 //tmalhotra2 MITS-29787 Re# 5.1.13					
			
				arrlstClaimNotes = new ArrayList() ;

				foreach( ClaimAdjuster objClaimAdjuster in objClaim.AdjusterList )
				{                
					if( objClaimAdjuster.AdjRowId == 0 )
						break;
					foreach( AdjustDatedText objAdjustDatedText in objClaimAdjuster.AdjustDatedTextList )
					{
						if( objAdjustDatedText.AdjDttextRowId == 0 )
							break;	
					
						objClaimNotes = new ClaimNotes();
		
						objClaimNotes.HeaderNote = "Adjuster Notes" ;
						objClaimNotes.DateEntered = objAdjustDatedText.DateEntered ;
					    //Raman 02/10/2010: We need to get the full name instead of loginname
                        string sFullName;
                        if (PublicFunctions.GetFullNameFromLoginName(objAdjustDatedText.EnteredByUser, out sFullName, m_iClientId))
                        {
                            objClaimNotes.EnteredByUser = sFullName;
                        }
                        else
                        {
                            objClaimNotes.EnteredByUser = objAdjustDatedText.EnteredByUser;
                        }
                        //Arnab: MITS-10226 - Added new objClaimNotes.LastModified Property                        
                        objClaimNotes.DttmLastUpdated = objAdjustDatedText.DttmRcdLastUpd;
					    //MITS-10226 End
						try
						{
							objEntity = objClaimAdjuster.AdjusterEntity ;
							bEntityFound = true ;					
						}
						catch
						{
							bEntityFound = false ;
						}
						if( bEntityFound )
							sLastFirstName = objEntity.GetLastFirstName(); 
						else
							sLastFirstName = "" ;

						objClaimNotes.LastFirstName = sLastFirstName + " - " + objAdjustDatedText.DatedText ;																	
						objClaimNotes.LastFirstName = objClaimNotes.LastFirstName.Replace( "\r\n" , " " );
						arrlstClaimNotes.Add( objClaimNotes );	
						
						objClaimNotes = null ;
					}
				}
				if( arrlstClaimNotes.Count > 0 )
				{
					bSortCompleted = false ;

                    //Arnab: MITS-10226 - Modified sorting Last Update date based, Commented the old codes
					//sort on DateEntered   --  Previous Comment, Not to be referred now

                    //sort on Date Last Updated
					while( !bSortCompleted )
					{
						bSortCompleted = true ;
						
						for( iIndex = 0 ; iIndex < arrlstClaimNotes.Count - 1  ; iIndex++ )
						{
							objLastClaimNotes = (ClaimNotes)arrlstClaimNotes[ iIndex ] ;
							objClaimNotes= (ClaimNotes)arrlstClaimNotes[ iIndex + 1 ] ;
                            //Arnab: MITS-10226 - Modified the If condition to be Last Update Date based							
                            //if (Conversion.ConvertStrToLong(objLastClaimNotes.DttmRcdAdded.Trim()) > Conversion.ConvertStrToLong(objClaimNotes.DttmRcdAdded.Trim()))
                            if (Conversion.ConvertStrToLong(objLastClaimNotes.DttmLastUpdated.Trim()) < Conversion.ConvertStrToLong(objClaimNotes.DttmLastUpdated.Trim()))
							{
								bSortCompleted = false ;		
								arrlstClaimNotes.RemoveAt( iIndex );
								arrlstClaimNotes.Insert( iIndex , objClaimNotes );

								arrlstClaimNotes.RemoveAt( iIndex + 1 );
								arrlstClaimNotes.Insert( iIndex + 1 , objLastClaimNotes );											
							} 
						}
					}
					// Sort completed
                    //MITS-10226 - End
					for( iIndex = 0 ; iIndex < arrlstClaimNotes.Count ; iIndex++ )
					{
						objClaimNotes = (ClaimNotes)arrlstClaimNotes[iIndex] ;
						arrClaimNotesColumn[0] = objClaimNotes.HeaderNote.Trim() ;
						arrClaimNotesColumn[1] = Conversion.GetDBDateFormat(objClaimNotes.DateEntered.Trim() , "d" );
						arrClaimNotesColumn[2] = objClaimNotes.EnteredByUser ;
						arrClaimNotesColumn[3] = objClaimNotes.LastFirstName ;  
						arrClaimNotesColumn[3] = arrClaimNotesColumn[3].Replace( "\r\n" , " " );
										
						AddRow( arrClaimNotesColumn );
					}
				}
    
                //Added by csingh7 : R6 Claim Comment Enhancement : fetching claim comments
                //Modified by mdhamija : MITS 28062
                //sSQL = "SELECT  COMMENTS FROM COMMENTS_TEXT WHERE ATTACH_RECORDID = " + p_iClaimID + " AND ATTACH_TABLE = 'CLAIM'";
                sSQL = "SELECT  HTMLCOMMENTS FROM COMMENTS_TEXT WHERE ATTACH_RECORDID = " + p_iClaimID + " AND ATTACH_TABLE = 'CLAIM'";				
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
			
				if( objReader != null )
				{
					while( objReader.Read() )
					{
                        //sClaimComment = objReader.GetString("COMMENTS");//mdhamija : MITS 28062
                        sClaimComment = objReader.GetString("HTMLCOMMENTS");						
					}
				}
                if (sClaimComment.Trim().Length > 0)
                {
                    arrClaimNotesColumn[0] = "Claim Comments";
                    arrClaimNotesColumn[1] = "";
                    arrClaimNotesColumn[2] = "";
                    arrClaimNotesColumn[3] = sClaimComment.Replace("\r\n", " ");
                    AddRow(arrClaimNotesColumn);
                }

				// Get the reference of an event having event_id p_ieventid
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( objClaim.EventId > 0 )
					objEvent.MoveTo( objClaim.EventId );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID", m_iClientId));

                //Added by csingh7 : R6 Claim Comment Enhancement : fetching event comments
                ////Modified by mdhamija : MITS 28062
                //sSQL = "SELECT  COMMENTS FROM COMMENTS_TEXT WHERE ATTACH_RECORDID = " + objClaim.EventId + " AND ATTACH_TABLE = 'EVENT'";
                sSQL = "SELECT HTMLCOMMENTS FROM COMMENTS_TEXT WHERE ATTACH_RECORDID = " + objClaim.EventId + " AND ATTACH_TABLE = 'EVENT'"; 
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        //sEventComment = objReader.GetString("COMMENTS");mdhamija : MITS 28062
                        sEventComment = objReader.GetString("HTMLCOMMENTS");
                    }
                }
                if (sEventComment.Trim().Length > 0)
                {
                    arrClaimNotesColumn[0] = "Event Comments";
                    arrClaimNotesColumn[1] = "";
                    arrClaimNotesColumn[2] = "";
                    arrClaimNotesColumn[3] = sEventComment.Replace("\r\n", " ");
                    AddRow(arrClaimNotesColumn);
                }			

				foreach( EventXDatedText objEventXDatedText in objEvent.EventXDatedTextList )
				{
					if( objEventXDatedText.EvDtRowId == 0 )
						break;
					arrClaimNotesColumn[0] = "Event Dated Text" ;
					arrClaimNotesColumn[1] = Conversion.GetDBDateFormat( objEventXDatedText.DateEntered , "d" );
					arrClaimNotesColumn[2] = objEventXDatedText.EnteredByUser ;
					arrClaimNotesColumn[3] = objEventXDatedText.DatedText.Replace( "\r\n" , " " ); 

					AddRow( arrClaimNotesColumn );
				}
			
				if( objEvent.EventDescription.Trim().Length > 0 ) 
				{
					arrClaimNotesColumn[0] = "Event Description" ;
					arrClaimNotesColumn[1] = "" ;
					arrClaimNotesColumn[2] = "" ;
					arrClaimNotesColumn[3] = objEvent.EventDescription.Replace( "\r\n" , " " );
					AddRow( arrClaimNotesColumn ) ;
				}

				if( objEvent.LocationAreaDesc.Trim().Length > 0 ) 
				{
					arrClaimNotesColumn[0] = "Location Descripton" ;
					arrClaimNotesColumn[1] = "" ;
					arrClaimNotesColumn[2] = "" ;
					arrClaimNotesColumn[3] = objEvent.LocationAreaDesc.Replace( "\r\n" , " " );
					AddRow( arrClaimNotesColumn ) ;
				}

				if( objEvent.PhysNotes.Trim().Length > 0 ) 
				{
					arrClaimNotesColumn[0] = "Physician Notes" ;
					arrClaimNotesColumn[1] = "" ;
					arrClaimNotesColumn[2] = "" ;
					arrClaimNotesColumn[3] = objEvent.PhysNotes.Replace( "\r\n" , " " );
					AddRow( arrClaimNotesColumn ) ;
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimNotes.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
				arrlstClaimNotes = null ;	
				objLastClaimNotes = null ;
				objClaimNotes = null ;
				arrClaimNotesColumn = null ;
			}
			return( m_objDocument );
		}

        //Changed for MITS 7658 by Gagan : Start

        /// <summary>
		/// Get the data for Enhanced Notes for Claim
		/// </summary>
        /// <param name="p_iEventID">Event ID</param>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <returns>Enhanced Notes in form of a Xmldocument</returns>
        private XmlDocument GetClaimEnhancedNotes(int p_iEventId, int p_iClaimID)
        {
            Claim objClaim = null;
            ProgressNotesManager objProgressNotes = null;
            XmlDocument objDocument = null;            
            XmlNode objMainNode = null;
            XmlElement objTempNode = null;
            //MITS 15480: ybhaskar: Added flag for the case if there is no value in any control...
            int flag = 0;
            string ClaimantName = GetClaimants(p_iClaimID);  //tmalhotra2 MITS-29787 Re# 5.1.13
			string[] arrEnhancedNotesColumn = { 
											  "Activity Date" 
											  ,"Attached To" 
											  ,"Note Type"
											  ,"Note Text" 
											  ,"Entered By"
                                              ,"Subject"
										  } ;            
            try
            {
                //bkumar33 has changed
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimID > 0)
                    objClaim.MoveTo(p_iClaimID);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));

                StartDocument("Enhanced Notes for Claim - " + objClaim.ClaimNumber + " * " + ClaimantName, arrEnhancedNotesColumn);
                //StartDocument("Enhanced Notes for Claim - " + p_iEventId, arrEnhancedNotesColumn); objClaim.Dispose();
                //end



                objProgressNotes = new ProgressNotesManager(this.m_sDsnName, this.m_sUserName, this.m_sPassword, m_iClientId);
                objProgressNotes.m_UserLogin = this.m_UserLogin; //Aman ML Change
                objDocument = objProgressNotes.OnLoad(p_iEventId, p_iClaimID, false, "", -1, defaultSort, "DESC", false, 0, "", "", "", -1);//Modified by Shivendu for MITS 18098 // mkaran2 - MITS 27038//-1 added for claimant id -gbindra MITS#34104
                //Modified by Parijat :19724: Since quick summary didn't use paging and hence all the records were not being shown there.
                objMainNode = objDocument.SelectSingleNode("//ProgressNotes");

                foreach(XmlNode objNode in objMainNode.ChildNodes)
				{

                    //Activity Date
                    objTempNode = (XmlElement)objNode.SelectSingleNode("DateEntered");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[0] = objTempNode.InnerText;
                        
                    }
                    else
                    {
                        flag++;
                    }
                    //Attahced To
                    objTempNode = (XmlElement)objNode.SelectSingleNode("ClaimNumber");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[1] = "CLAIM: " + objTempNode.InnerText; ;
                        
                    }
                    else
                    {
                        flag++;
                    }
                    //Note Type
                    objTempNode = (XmlElement)objNode.SelectSingleNode("NoteTypeCode");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[2] = objTempNode.InnerText; ;
                        
                    }
                    else
                    {
                        flag++;
                    }
                    //Note Text
                    objTempNode = (XmlElement)objNode.SelectSingleNode("NoteMemo");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[3] = objTempNode.InnerText;
                        
                    }
                    else
                    {
                        flag++;
                    }
                    //Entered By
                    objTempNode = (XmlElement)objNode.SelectSingleNode("EnteredByName");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[4] = objTempNode.InnerText; ;
                        
                    }
                    else
                    {
                        flag++;
                    }

                    //zmohammad MITS 31046
                    objTempNode = (XmlElement)objNode.SelectSingleNode("Subject");                    
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[5] = objTempNode.InnerText; ;

                    }
                    else
                    {
                        flag++;
                    }
                    if (flag != 6)
                        AddRow(arrEnhancedNotesColumn);

                    flag = 0;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimNotes.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                arrEnhancedNotesColumn = null;
                if (objProgressNotes != null)
                {
                    objProgressNotes.Dispose();
                    objProgressNotes = null;
                }
            }

            return (m_objDocument);			
        }

		// Ayush, MITS: 18291, Date: 01/12/10 Start
        /// <summary>
        /// Get the data for Properties Involved
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <returns>Properties Claim data in form of a Xmldocument</returns>
        private XmlDocument GetClaimPropertyData(int p_iClaimID, string p_sNodeName, int p_iRequestedInfoID)
        {
            Claim objClaim = null;
            PropertyUnit objProp = null;
            string sTemp = string.Empty;
            string sTempAbbre = string.Empty;
            string[] arrClaimPropertyDataColumn = 
											{
												" " , " "
											};
            string[] arrClaimPropertySchdDataColumn = 
											{
												"Schedule Name " , "  Amount"
											};
            try
            {
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimID, CommonFunctions.NavFormType.None, m_sConnectionString);
                 Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

                // Get the reference of a claim having claim_id p_iClaimID
                using (objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false))
                {
                    if (p_iClaimID > 0)
                    {
                        objClaim.MoveTo(p_iClaimID);
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidClaimID", m_iClientId));
                    }
                    using (objProp = (PropertyUnit)m_objDataModelFactory.GetDataModelObject("PropertyUnit", false))
                    {
                        if (p_iRequestedInfoID > 0)
                        {
                            objProp.MoveTo(p_iRequestedInfoID);
                        }
                        else
                        {
                            throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidPropertyID", m_iClientId));
                        }

                        if (p_sNodeName == CLAIM_PROPERTY_NODE)
                        {
                            StartDocument("Property Information for Claim - " + objClaim.ClaimNumber, arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Pin:";
                            arrClaimPropertyDataColumn[1] = objProp.Pin;
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Description:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.Description;
                            AddRow(arrClaimPropertyDataColumn);
                            arrClaimPropertyDataColumn[0] = "Address 1:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.Addr1;
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Address 2:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.Addr2;
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Address 3:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.Addr3;
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Address 4:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.Addr4;
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "City:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.City;
                            AddRow(arrClaimPropertyDataColumn);

                            objClaim.Context.LocalCache.GetStateInfo(objClaim.PropertyClaim.StateId, ref sTempAbbre, ref sTemp);

                            arrClaimPropertyDataColumn[0] = "State:";
                            arrClaimPropertyDataColumn[1] = sTemp;
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Zipcode:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.ZipCode;
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Territory Code:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.TerritoryCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Category Code:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.CategoryCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Appraised Value:";
                            arrClaimPropertyDataColumn[1] = string.Format("{0:C}", objClaim.PropertyClaim.AppraisedValue);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Appraised Date:";
                            arrClaimPropertyDataColumn[1] = Conversion.GetDBDateFormat(objClaim.PropertyClaim.AppraisedDate, "d");
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Appraisal Source:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.AppraisalSourceCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Land Value:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.LandValue.ToString();
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Replacement Value:";
                            arrClaimPropertyDataColumn[1] = string.Format("{0:C}", objClaim.PropertyClaim.ReplacementValue);
                            AddRow(arrClaimPropertyDataColumn);
                        }
                        else if (p_sNodeName == CLAIM_PROPERTY_COPE_NODE)
                        {
                            StartDocument("Property COPE Data for Claim - " + objClaim.ClaimNumber, arrClaimPropertyDataColumn);
                            arrClaimPropertyDataColumn[0] = "Class of Construction:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.ClassOfConstruction);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Wall Construction:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.WallConstructionCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Roof Construction:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.RoofConstructionCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Heating System:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.HeatingSysCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Cooling System:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.CoolingSysCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Fire Alarm:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.FireAlarmCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Sprinklers:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.SprinklersCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Entry Alarm:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.EntryAlarmCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Roof Anchoring:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.RoofAnchoringCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Strength of Glass:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.GlassStrengthCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Year of Construction:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.YearOfConstruction.ToString();
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Square Footage:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.SquareFootage.ToString();
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Avg. Story Height:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.AvgStoryHeight.ToString();
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "No. of Stories:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.NoOfStories.ToString();
                            AddRow(arrClaimPropertyDataColumn);
                        }
                        else if (p_sNodeName == CLAIM_PROPERTY_OP_COPE_NODE)
                        {
                            StartDocument("Property Optional COPE Data for Claim - " + objClaim.ClaimNumber, arrClaimPropertyDataColumn);
                            arrClaimPropertyDataColumn[0] = "GPS Altitude:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.GPSAltitude.ToString();
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "GPS Latitude:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.GPSLatitude.ToString();
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "GPS Longitude:";
                            arrClaimPropertyDataColumn[1] = objClaim.PropertyClaim.GPSLongitude.ToString();
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Plot Plans:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.PlotPlansCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "Flood Zone:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.FloodZoneCertCode);
                            AddRow(arrClaimPropertyDataColumn);

                            arrClaimPropertyDataColumn[0] = "EarthQuake Zone:";
                            arrClaimPropertyDataColumn[1] = GetCodeDescription(objClaim.PropertyClaim.EarthquakeZoneCode);
                            AddRow(arrClaimPropertyDataColumn);
                        }
                        else if (p_sNodeName == CLAIM_PROPERTY_SCHD_NODE)
                        {
                            StartDocument("Property Schedule for Claim - " + objClaim.ClaimNumber, arrClaimPropertySchdDataColumn);
                            foreach (PolicyXExpEnh objExp in objClaim.PrimaryPolicyEnh.PolicyXExposureEnhList)
                            {
                                foreach (PolicyXUar objPolicyXUar in objExp.PolicyXUarList)
                                {
                                    //Start:Nitin Goel - 02/17/2010 MITS#18230
                                    //if (objPolicyXPschedEnh.PropertyID == objClaim.PropertyClaim.PropertyID)
                                    if (objPolicyXUar.UarId == objClaim.PropertyClaim.PropertyID)
                                    {
                                        foreach (PolicyXPschedEnh objPolicyXPschedEnh in objPolicyXUar.PolicyXPschedEnhList)
                                        {
                                            //arrClaimPropertySchdDataColumn[0] = objPolicyXPschedEnh.ScheduleName;
                                            arrClaimPropertySchdDataColumn[0] = objPolicyXPschedEnh.Name;
                                            //End:Nitin Goel - 02/17/2010 MITS#18230
                                            arrClaimPropertySchdDataColumn[1] = string.Format("{0:C}", objPolicyXPschedEnh.Amount);
                                            AddRow(arrClaimPropertySchdDataColumn);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //end using
                }
                //end using
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimPropertyData.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                arrClaimPropertyDataColumn = null;
                arrClaimPropertySchdDataColumn = null;
            }
            return (m_objDocument);
        }

        // Ayush, MITS: 18291, Date: 01/12/10 : End


        /// <summary>
		/// Get the data for Enhanced Notes for an Event
		/// </summary>
        /// <param name="p_iEventID">Event ID</param>		
		/// <returns>Enhanced Notes in form of a Xmldocument</returns>
        private XmlDocument GetEventEnhancedNotes(int p_iEventId, int p_iClaimId)
        {               
            ProgressNotesManager objProgressNotes = null;
            Event objEvent = null;
            XmlDocument objDocument = null;            
            XmlNode objMainNode = null;
            XmlElement objTempNode = null;
            //MITS 15480: ybhaskar: Added flag for the case if there is no value in any control...
            int flag = 0;
           // string ClaimantName = GetClaimants(p_iClaimId);  //tmalhotra2 MITS-29787 Re# 5.1.13       MITS 35322 Commented as the variable is not used in the current block     
			string[] arrEnhancedNotesColumn = { 
											  "Activity Date" 
											  ,"Attached To" 
											  ,"Note Type"
											  ,"Note Text" 
											  ,"Entered By"
                                              ,"Subject"
										  } ;            
            try
            {     
                //bkumar33 has changed for cosmetic Mits
                //StartDocument("Enhanced Notes for Event - " + p_iEventId , arrEnhancedNotesColumn);

                objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                if (p_iEventId > 0)
                    objEvent.MoveTo(p_iEventId);
                else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID", m_iClientId));

                StartDocument("Enhanced Notes for Event - " + objEvent.EventNumber, arrEnhancedNotesColumn);
                objEvent.Dispose();
                //end

                objProgressNotes = new ProgressNotesManager(this.m_sDsnName, this.m_sUserName, this.m_sPassword, m_iClientId);
                objProgressNotes.m_UserLogin = this.m_UserLogin; //Aman ML Change
                objDocument = objProgressNotes.OnLoad(p_iEventId, 0, false, "", -1, defaultSort, "DESC", false, 0, "", "", "", -1);//Modified by Shivendu for MITS 18098 // mkaran2 - MITS 27038 //-1 added for claimant id so as claimant's notes are not picked -by gbindra MITS#34104
                //Modified by Parijat :19724: Since quick summary didn't use paging and hence all the records were not being shown there.
                objMainNode = objDocument.SelectSingleNode("//ProgressNotes");

                foreach (XmlNode objNode in objMainNode.ChildNodes)
                {

                    //Activity Date
                    objTempNode = (XmlElement)objNode.SelectSingleNode("DateEntered");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[0] = objTempNode.InnerText;
                    }
                    else
                    {
                        flag++;
                    }
                    //Attahced To
                    objTempNode = (XmlElement)objNode.SelectSingleNode("EventNumber");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[1] = "EVENT: " + objTempNode.InnerText; ;
                    }
                    else
                    {
                        flag++;
                    }
                    //Note Type
                    objTempNode = (XmlElement)objNode.SelectSingleNode("NoteTypeCode");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[2] = objTempNode.InnerText;
                    }
                    else
                    {
                        flag++;
                    }
 
                    //Note Text
                    objTempNode = (XmlElement)objNode.SelectSingleNode("NoteMemo");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[3] = objTempNode.InnerText; ;
                    }
                    else
                    {
                        flag++;
                    }
                    //Entered By
                    objTempNode = (XmlElement)objNode.SelectSingleNode("EnteredByName");
                    //ybhaskar: MITS 15480: Added null check and if else
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[4] = objTempNode.InnerText; ;
                    }
                    else
                    {
                        flag++;
                    }
                    //zmohammad MITS 31046
                    objTempNode = (XmlElement)objNode.SelectSingleNode("Subject");
                    if (objTempNode != null)
                    {
                        arrEnhancedNotesColumn[5] = objTempNode.InnerText; ;
                    }
                    else
                    {
                        flag++;
                    }
                    if(flag!=6)
                        AddRow(arrEnhancedNotesColumn);

                    flag = 0;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetClaimNotes.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                arrEnhancedNotesColumn = null;
                if (objProgressNotes != null)
                {
                    objProgressNotes.Dispose();
                    objProgressNotes = null;
                }
            }

            return (m_objDocument);			
        }


        //Changed for MITS 7658 by Gagan : End


		/// <summary>
		/// Get Transaction Benefit string
		/// </summary>
		/// <param name="p_iUTIL32_CBO_ID">UTIL32_CBO_ID</param>
		/// <param name="p_iStateID">State ID</param>
		/// <returns>Tansaction Benefit string</returns>
		private string GetTransactionTypesByBenefit( int p_iUTIL32_CBO_ID , int p_iStateID  )
		{
			DbReader objReader = null ;

			string sTransactionTypesByBenefit = "" ;
			string sSQL = "" ;	
			bool bFirstInstance = true ;			

			try
			{
				sSQL = "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES WHERE STATE_ROW_ID = " + p_iStateID ;
				if( p_iUTIL32_CBO_ID != -1 )
					sSQL += " AND UTIL32_CBO_ID = " + p_iUTIL32_CBO_ID ;
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
			
				if( objReader != null )
				{
					while( objReader.Read() )
					{
						if( ! bFirstInstance )
							sTransactionTypesByBenefit += " , " ;							
						else
							bFirstInstance = false ;

						sTransactionTypesByBenefit += objReader.GetInt( "CODE_ID" ).ToString() ;
						
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetTransactionTypesByBenefit.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( sTransactionTypesByBenefit );
		}
		/// <summary>
		/// Get the data arraylist for claim benefit.
		/// </summary>
		/// <param name="p_iClaimID">Claim Id</param>
		/// <param name="p_iClaimFilingStateID">Filling State Id </param>
		/// <param name="p_arrPaymentArray">Array List</param>		
        private void CreatePaymentArray(int p_iClaimID, int p_iClaimFilingStateID, ArrayList p_arrlstPayment, int p_iClaimantID)
		{
			DbReader objReader = null ;
			Payment objPayment = null ;
			Payment objLastPayment = null ;
			
			const double dblLumpSum = 1000.0 ;

			string sListOfSupplementalTransTypeCodes = "" ;
			string sListOfAllTransTypeCodes = "" ;
			string sSQL = "" ;
			string sOldTransType = "" ;

			DateTime datFromDate  ;
			DateTime datToDate ;
			
			int p_iArrayRow = -1;
			int iIndex = 0 ;
			int iOldTransTypeID = 0 ;
			int iDateDifference = 0 ;
			bool bSortCompleted = false ;

			try
			{
				sListOfSupplementalTransTypeCodes = GetTransactionTypesByBenefit( 5 , p_iClaimFilingStateID ) ;
				sListOfAllTransTypeCodes = GetTransactionTypesByBenefit( -1 , p_iClaimFilingStateID );
			
				sSQL = "SELECT FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE" 
					+ ",FUNDS_TRANS_SPLIT.AMOUNT" 
					+ ",FUNDS_TRANS_SPLIT.FROM_DATE" 
					+ ",FUNDS_TRANS_SPLIT.TO_DATE" 
                    + ",FUNDS.PAYMENT_FLAG" //rsushilaggar MITS 23334
					+ ",CODES_TEXT.CODE_DESC" 
					+ " FROM FUNDS, FUNDS_TRANS_SPLIT, CODES_TEXT" 
					+ " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID" 
					+ " AND VOID_FLAG = 0"
					+ " AND FUNDS.CLAIM_ID = " +p_iClaimID 
					+ " AND FUNDS_TRANS_SPLIT.AMOUNT <> 0" 
					+ " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID" ;

				if( sListOfAllTransTypeCodes != "" )
					sSQL += " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" + sListOfAllTransTypeCodes + ")" ;
                if (p_iClaimantID != 0)//parijat 21784
                {
                    sSQL += " AND FUNDS.CLAIMANT_EID = " + p_iClaimantID;
                }
				
                sSQL += " ORDER BY TO_DATE,TRANS_TYPE_CODE" ;

				if ( m_sDBType == Constants.DB_ACCESS  )
					sSQL = sSQL.Replace( "|" , " AS " ) ;
				else
					sSQL = sSQL.Replace( "|" , "  " ) ;	
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
			
				if( objReader != null )
				{
					if( objReader.Read() )
					{
						p_iArrayRow++;
						iOldTransTypeID = objReader.GetInt("TRANS_TYPE_CODE" );
						sOldTransType = objReader.GetString( "CODE_DESC" ) ;
					
						objPayment = new Payment();
						objPayment.TransTypeCode = objReader.GetInt("TRANS_TYPE_CODE" );
						objPayment.Amount = objReader.GetDouble( "AMOUNT" ) ;
                        //Start rsushilaggar MITS 23334  Date 01/19/2011
                        if (!objReader.GetBoolean("PAYMENT_FLAG"))
                            objPayment.Amount = -objPayment.Amount;
                        //End rsushilaggar
						objPayment.FromDate = objReader.GetString( "FROM_DATE" );
						objPayment.ToDate = objReader.GetString( "TO_DATE" );
						objPayment.CodeDesc = objReader.GetString( "CODE_DESC" );	
						p_arrlstPayment.Insert( p_iArrayRow , objPayment );
					}
					while( objReader.Read() )
					{
						objLastPayment = ( Payment )p_arrlstPayment[ p_iArrayRow ] ;
						if( iOldTransTypeID == objReader.GetInt("TRANS_TYPE_CODE" ) && objLastPayment.ToDate != ""  && objLastPayment.FromDate != "" )
						{
							iDateDifference = 0 ;
							if( objReader.GetString( "TO_DATE").Trim() != "" )
							{
								datFromDate = Convert.ToDateTime( Conversion.GetDBDateFormat( objLastPayment.ToDate , "d" ) );
								datToDate = Convert.ToDateTime( Conversion.GetDBDateFormat( objReader.GetString( "TO_DATE").Trim() , "d" ) ) ;
								iDateDifference = ((TimeSpan)datFromDate.Subtract( datToDate ) ).Days ;								
							}
							if( iDateDifference == 1 || iDateDifference == -1 )
							{							
								objLastPayment.Amount += objReader.GetDouble( "AMOUNT" );
                                //Start rsushilaggar MITS 23334  Date 01/19/2011
                                if (!objReader.GetBoolean("PAYMENT_FLAG"))
                                    objPayment.Amount = -objPayment.Amount;
                                //End rsushilaggar
								objLastPayment.ToDate = objReader.GetString( "TO_DATE" );								
							}						
							else
							{
								//'transaction id is the same, from and to dates donot flow unbroken
								p_iArrayRow++ ;
								iOldTransTypeID = objReader.GetInt("TRANS_TYPE_CODE" ) ;
								sOldTransType = objReader.GetString( "CODE_DESC" ) ;

								objPayment = new Payment();
								objPayment.TransTypeCode = objReader.GetInt("TRANS_TYPE_CODE" );
								objPayment.Amount = objReader.GetDouble( "AMOUNT" ) ;
                                //Start rsushilaggar MITS 23334  Date 01/19/2011
                                if (!objReader.GetBoolean("PAYMENT_FLAG"))
                                    objPayment.Amount = -objPayment.Amount;
                                //End rsushilaggar
								objPayment.FromDate = objReader.GetString( "FROM_DATE" );
								objPayment.ToDate = objReader.GetString( "TO_DATE" );
								objPayment.CodeDesc = objReader.GetString( "CODE_DESC" );	
								p_arrlstPayment.Insert( p_iArrayRow , objPayment );
							}
						}
						else
						{
							p_iArrayRow++ ;
							iOldTransTypeID = objReader.GetInt("TRANS_TYPE_CODE" ) ;
							sOldTransType = objReader.GetString( "CODE_DESC" ) ;

							objPayment = new Payment();
							objPayment.TransTypeCode = objReader.GetInt("TRANS_TYPE_CODE" );
							objPayment.Amount = objReader.GetDouble( "AMOUNT" ) ;
                            //Start rsushilaggar MITS 23334  Date 01/19/2011
                            if (!objReader.GetBoolean("PAYMENT_FLAG"))
                                objPayment.Amount = -objPayment.Amount;
                            //End rsushilaggar
							objPayment.FromDate = objReader.GetString( "FROM_DATE" );
							objPayment.ToDate = objReader.GetString( "TO_DATE" );
							objPayment.CodeDesc = objReader.GetString( "CODE_DESC" );	
							p_arrlstPayment.Insert( p_iArrayRow , objPayment );

						}
					}
				}
			
				for( iIndex = 0 ; iIndex <= p_iArrayRow ; iIndex++ )
				{
					objLastPayment = ( Payment )p_arrlstPayment[ iIndex ] ;
					
					if( objLastPayment.ToDate != "" && objLastPayment.FromDate != "" )
					{						
						datFromDate = Convert.ToDateTime( Conversion.GetDBDateFormat( objLastPayment.FromDate , "d" ) );
						datToDate = Convert.ToDateTime( Conversion.GetDBDateFormat( objLastPayment.ToDate , "d" ) ) ;

						iDateDifference = ((TimeSpan)datFromDate.Subtract( datToDate ) ).Days ;

						if( iDateDifference < 0 )
							iDateDifference = -iDateDifference ;
						iDateDifference = iDateDifference + 1 ;

						objLastPayment.AvgRate = ( objLastPayment.Amount / iDateDifference ) *7  ;
                        //rsushilaggar MITS 24562 Date 08/30/2011
                        //if( iDateDifference == 1 && objLastPayment.AvgRate > dblLumpSum  )
                        //{
                        //    objLastPayment.LumSum = true ;		//Lump sum
                        //}
					}
				}
			
                //rsushilaggar MITS 23334 Date 01/21/2011 
                // This query gets the data from the table funds. In this query. the sum of  all the payments is subtract from the sum of collection for each transaction type.
                sSQL = "SELECT TRANS_TYPE_CODE,SUM(S_AMOUNT) | S_AMOUNT, CODE_DESC"
                        + " FROM (SELECT FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,"
                        + " CASE FUNDS.PAYMENT_FLAG WHEN 0 THEN -FUNDS_TRANS_SPLIT.AMOUNT  ELSE FUNDS_TRANS_SPLIT.AMOUNT  END AS S_AMOUNT,"
                        + " CODES_TEXT.CODE_DESC FROM FUNDS, FUNDS_TRANS_SPLIT, CODES_TEXT"
                        + " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
                        + " AND VOID_FLAG = 0 AND FUNDS.CLAIM_ID =" + p_iClaimID
                        + " AND FUNDS_TRANS_SPLIT.AMOUNT <> 0"
                        + " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID";
                
				if( sListOfAllTransTypeCodes != "" )
					sSQL += " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" + sListOfAllTransTypeCodes + ")" ;

                if (p_iClaimantID != 0)//parijat 21784
                {
                    sSQL += " AND FUNDS.CLAIMANT_EID = " + p_iClaimantID;
                }

                //rsushilaggar MITS 23334 Date 01/21/2010
                //sSQL += " GROUP BY TRANS_TYPE_CODE, CODE_DESC, PAYMENT_FLAG";
                sSQL += " ) ALLRECORDS GROUP BY TRANS_TYPE_CODE,CODE_DESC";
			
				if ( m_sDBType == Constants.DB_ACCESS  )
					sSQL = sSQL.Replace( "|" , " AS " ) ;
				else
					sSQL = sSQL.Replace( "|" , "  " ) ;

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL.ToString() );

                if( objReader != null )
				{
					while( objReader.Read() )
					{
                        p_iArrayRow++;
						objPayment = new Payment();
						objPayment.TransTypeCode = objReader.GetInt( "TRANS_TYPE_CODE" ) ;
						objPayment.Amount = 0.0 ;
						objPayment.FromDate = "" ;
						objPayment.ToDate = "" ;
						objPayment.AvgRate = 0.0 ;
						objPayment.Paid = objReader.GetDouble( "S_AMOUNT") ;
						objPayment.CodeDesc = objReader.GetString( "CODE_DESC" );
                        p_arrlstPayment.Insert(p_iArrayRow, objPayment);
					}
				}
                
                bSortCompleted = false;
				//sort on FROM_DATE
				while( !bSortCompleted )
				{
					bSortCompleted = true ;
					for( iIndex = 0 ; iIndex < p_iArrayRow  ; iIndex++ )
					{
						objLastPayment = (Payment)p_arrlstPayment[ iIndex ] ;
						objPayment= (Payment)p_arrlstPayment[ iIndex + 1 ] ;
						
						if( Conversion.ConvertStrToLong( objLastPayment.FromDate) > Conversion.ConvertStrToLong( objPayment.FromDate ) )  
						{
							bSortCompleted = false ;														
							p_arrlstPayment.RemoveAt( iIndex );
							p_arrlstPayment.Insert( iIndex , objPayment );

							p_arrlstPayment.RemoveAt( iIndex + 1 );
							p_arrlstPayment.Insert( iIndex + 1 , objLastPayment );											
						} 
					}
				}

				bSortCompleted = false ;
				//sort on code desc, 
				while( !bSortCompleted )
				{
					bSortCompleted = true ;
					for( iIndex = 0 ; iIndex < p_iArrayRow ; iIndex++ )
					{
						objLastPayment = (Payment)p_arrlstPayment[ iIndex ] ;
						objPayment= (Payment)p_arrlstPayment[ iIndex + 1 ] ;

						if( string.Compare( objLastPayment.CodeDesc.Trim() ,objPayment.CodeDesc.Trim() ) > 0 )
						{
							bSortCompleted = false ;		
							p_arrlstPayment.RemoveAt( iIndex );
							p_arrlstPayment.Insert( iIndex , objPayment );

							p_arrlstPayment.RemoveAt( iIndex + 1 );
							p_arrlstPayment.Insert( iIndex + 1 , objLastPayment );
						}
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePaymentArray.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				objLastPayment = null ;
				objPayment = null ;
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}		
		
		/// <summary>
		/// Execute the SQL queries.
		/// </summary>
		/// <param name="p_arrSQL">Array of SQL query strings</param>
		/// <param name="p_arrFetchValues">Array of results strings</param>			
		private void FetchValuesFromDatabase( string[] p_arrSQL , string[] p_arrFetchValues )
		{
			DbReader objReader = null ;

			int iIndex = 0 ;
						
			for( iIndex = 0 ; iIndex < p_arrSQL.GetUpperBound(0) + 1 ; iIndex++ )
			{
				if( p_arrSQL[iIndex].Trim().Length > 0 )
				{
					try
					{
						objReader = DbFactory.GetDbReader( m_sConnectionString , p_arrSQL[iIndex] ) ;
						if( objReader != null )
							if( objReader.Read() )
								p_arrFetchValues[iIndex] = objReader.GetString( 0 ) ;							
					}
					catch( RMAppException p_objEx )
					{
						throw p_objEx ;
					}
					catch( Exception p_objEx )
					{
                        throw new RMAppException(Globalization.GetString("EventExplorerManager.FetchValuesFromDatabase.DataError", m_iClientId), p_objEx);
					}
					finally
					{
						if( objReader != null )
						{
							objReader.Close();
							objReader.Dispose();
						}
					}
				}			
			}			
		}

		/// <summary>
		/// Execute the SQL and return the result string.
		/// </summary>
		/// <param name="p_arrSQL">SQL string</param>
		/// <returns>Return the result string</returns>
		private string FetchValueFromDatabase( string p_sSQL )
		{
			DbReader objReader = null ;
			string sFetchValue = "" ;
			
			if( p_sSQL.Trim().Length > 0 )
			{
				try
				{
					objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL.Trim() ) ;
					if( objReader != null )
						if( objReader.Read() )
							sFetchValue = objReader.GetString( 0 ) ;							
				}
				catch( RMAppException p_objEx )
				{
					throw p_objEx ;
				}
				catch( Exception p_objEx )
				{
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.FetchValueFromDatabase.DataError", m_iClientId), p_objEx);
				}
				finally
				{
					if( objReader != null )
					{
						objReader.Close();
						objReader.Dispose();
					}
				}				
			}	
			return( sFetchValue );	
		}
		/// <summary>
		/// Execute the SQL and return the result string.
		/// </summary>
		/// <param name="p_arrSQL">SQL string</param>
		/// <param name="p_objConnection">Connection string</param>
		/// <returns>Return the result string</returns>
		private string FetchValueFromDatabase( string p_sSQL , DbConnection p_objConnection )
		{
			DbReader objReader = null ;
			string sFetchValue = "" ;
			
			if( p_sSQL.Trim().Length > 0 )
			{
				try
				{
					objReader = p_objConnection.ExecuteReader( p_sSQL.Trim() ) ;
					if( objReader != null )
						if( objReader.Read() )
							sFetchValue = objReader.GetString( 0 ) ;							
				}
				catch( RMAppException p_objEx )
				{
					throw p_objEx ;
				}
				catch( Exception p_objEx )
				{
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.FetchValueFromDatabase.DataError", m_iClientId), p_objEx);
				}
				finally
				{
					if( objReader != null )
					{
						objReader.Close();
						objReader.Dispose();
					}
				}				
			}	
			return( sFetchValue );	
		}
		/// <summary>
		/// Create the XmlDocument and insert the Header Row.
		/// </summary>
		/// <param name="p_sTitle">Title of the Row</param>
		/// <param name="p_arrHeader">Header Data Array</param>
		private void StartDocument( string p_sTitle , string[] p_arrHeader )
		{
			int iIndex = 0 ;			
			try
			{
				m_objDocument = new XmlDocument();
				// Create "data" and "header" node
				m_objData = m_objDocument.CreateElement( "data" );
				m_objHeader = m_objDocument.CreateElement( "header" );
				m_objData.SetAttribute( "type", "list" );
				m_objData.SetAttribute( "title", p_sTitle );
				// Append "header" node to "data" node
				m_objData.AppendChild( m_objHeader );
				// Append "data" node to document root
				m_objDocument.AppendChild( m_objData );
				// Add each header column to "header" node 
				for( iIndex = 0 ; iIndex < p_arrHeader.GetUpperBound(0) + 1 ; iIndex++ )
				{
					m_objCol = m_objDocument.CreateElement( "col" );
					m_objCol.InnerText = p_arrHeader[iIndex] ;
					m_objHeader.AppendChild( m_objCol );
				}
				// Create "rows" node
				m_objRows = m_objDocument.CreateElement( "rows" );
				// Append "rows" node to "data" node
				m_objData.AppendChild( m_objRows );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.StartDocument.DataError", m_iClientId), p_objEx);
			}

		}
        #region Added Rakhi for R7:Add Emp Data Elements
        private void AddSubRows(string p_sTitle, string[][] p_arrDataRows)
        {
             XmlElement m_objInnerRow = null ;
             XmlElement m_objOuterRow = null;
            try
            {
                m_objOuterRow = m_objDocument.CreateElement("rows");
                m_objOuterRow.SetAttribute("title", p_sTitle + ":");
                for (int iIndex = 0; iIndex < p_arrDataRows.Length; iIndex++)
                {
                    m_objInnerRow = m_objDocument.CreateElement("row");
                    m_objOuterRow.AppendChild(m_objInnerRow);
                    for (int i = 0; i < p_arrDataRows[iIndex].Length; i++)
                    {
                        m_objCol = m_objDocument.CreateElement("col");
                        m_objInnerRow.AppendChild(m_objCol);
                        m_objCol.InnerText = p_arrDataRows[iIndex][i];
                    }
                }
                m_objRows.AppendChild(m_objOuterRow);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.AddRow.DataError", m_iClientId), p_objEx);
            }
        }
        #endregion

		/// <summary>
		/// Insert a row in the XmlDocument.
		/// </summary>
		/// <param name="p_arrDataColumn">Array of Data Column</param>
		/// <param name="p_iRowID">Row ID</param>
		/// <param name="p_iID">The Node ID</param>
		private void AddRow( string[] p_arrDataColumn , int p_iRowID , int p_iID,bool bSort) //Added parameter bSort to prevent sort in case of addresses and Phone Numbers
		{					
			int iIndex = 0 ;
			try
			{
				// Append "row" node to "rows" node
				m_objRow = m_objDocument.CreateElement( "row" );
                if(bSort)
                    m_objRow.SetAttribute("ClaimNumber", p_arrDataColumn[0].ToString());//abansal23: MITS 17595 Claims not sorted by Claim Number in Quick Summary
				m_objRows.AppendChild( m_objRow );

				for( iIndex = 0 ; iIndex < p_arrDataColumn.GetUpperBound(0) + 1 ; iIndex++ )
				{
					m_objCol = m_objDocument.CreateElement("col" );
					if( p_iRowID != 0 && p_iID != 0 && iIndex == 0 )
					{
						m_objCol.SetAttribute( "rid", p_iRowID.ToString() );
						m_objCol.SetAttribute( "id", p_iID.ToString() );
					}
					m_objRow.AppendChild( m_objCol );
					m_objCol.InnerText = p_arrDataColumn[iIndex] ;
				}
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.AddRow.DataError", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// Insert a row in the XmlDocument.
		/// </summary>
		/// <param name="p_arrDataColumn">Array of Data Column</param>
		private void AddRow( string[] p_arrDataColumn )
		{
            AddRow(p_arrDataColumn, 0, 0, true); //Added Rakhi:sending bSort=true by default
		}
        //Added Rakhi to prevent Claim Number Sort getting attached in case of Address Details
        private void AddRow(string[] p_arrDataColumn,bool bSort) 
        {
            AddRow(p_arrDataColumn, 0, 0,bSort);
        }

        private void AddRow(string[] p_arrDataColumn, int p_iRowID, int p_iID) //Signature Changed to 4 parameters now
        {
            AddRow(p_arrDataColumn, p_iRowID, p_iID, true); //sending bSort=true by default
        }
        //Added Rakhi to prevent Claim Number Sort getting attached in case of Address Details

		/// <summary>
		/// Create the Address string.
		/// </summary>
		/// <param name="p_objEntity">Entity</param>
		/// <returns>Address string</returns>
		private string GetDisplayAddress( Entity p_objEntity )
		{
			State objState = null ;

			string sAddress  = "" ;
			string sStateName = "" ;
			bool bStateFound = false ;

			try
			{
				objState = ( State ) m_objDataModelFactory.GetDataModelObject( "State" , false );
				try
				{
					objState.MoveTo( p_objEntity.StateId );
					bStateFound = true ;
				}
				catch
				{
					bStateFound = false ;
				}
				if( bStateFound )
					sStateName = objState.StateName ;				

				sAddress = p_objEntity.Addr1 ;
				if( p_objEntity.Addr2 != "" && p_objEntity.Addr2 != " " )
					sAddress += " " + p_objEntity.Addr2 ;
                if (p_objEntity.Addr3.Trim() != "")
                    sAddress += " " + p_objEntity.Addr3;
                if (p_objEntity.Addr4.Trim() != "")
                    sAddress += " " + p_objEntity.Addr4;

				sAddress += " " + p_objEntity.City + " " + sStateName + " " + p_objEntity.ZipCode ;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetDisplayAddress.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				if( objState != null )
				{
					objState.Dispose();
					objState = null ;
				}
			}
			return( sAddress );
		}
		/// <summary>
		/// Get the Node Name , Class Name , Claim Id , Node Id  and Sub Id form the unique key string
		/// </summary>
		/// <param name="p_sKey">Key string </param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sClassName">Class Name</param>
		/// <param name="p_iClaimID">Claim ID</param>
		/// <param name="p_iRequestedInfoID">Requested Info ID</param>
		/// <param name="p_iRequestedInfoChildID">Requested Info Child ID</param>
        public void SplitKey(string p_sKey, ref string p_sNodeName, ref string p_sClassName, ref int p_iClaimID, ref int p_iRequestedInfoID, ref int p_iRequestedInfoChildID, ref string p_sParentTableName, ref int p_iParentRowId)  //ijain4 11/09/2015
		{
			int iCountItems = 0 ;
            string sClaimID = string.Empty;
			string sItem = "" ;
			string sRequestedInfoID = "" ;
			string sRequestedInfoChildID = "" ;

			try
			{				
				iCountItems = CountItems( COMMA_STRING , p_sKey );

				for( int iIndexItems = 0 ; iIndexItems < iCountItems ; iIndexItems++ )
				{
					int iComma = 0 ;
					int iHeaderPosition = 0;
					sItem = GetItemString( iIndexItems , p_sKey , COMMA_STRING );
				
					iHeaderPosition = sItem.IndexOf( NAME_EQUAL ) ;
					if( iHeaderPosition != -1 )
					{
						p_sNodeName = sItem.Remove( 0 , iHeaderPosition + NAME_EQUAL.Length );
						iComma = p_sNodeName.IndexOf( COMMA_STRING );
						if( iComma != -1 ) 
							p_sNodeName = p_sNodeName.Split( COMMA_STRING.ToCharArray())[0];
						continue;
					}
                    iHeaderPosition = sItem.IndexOf(CLAIM_ID_EQUAL);
                    if (iHeaderPosition != -1)
                    {
                        sClaimID = p_iClaimID.ToString();
                        sClaimID = sItem.Remove(0, iHeaderPosition + CLAIM_ID_EQUAL.Length);
                        iComma = sClaimID.IndexOf(COMMA_STRING);
                        if (iComma != -1)
                            sClaimID = sClaimID.Split(COMMA_STRING.ToCharArray())[0];
                        p_iClaimID = Common.Conversion.ConvertStrToInteger(sClaimID);
                        continue;
                    }

					iHeaderPosition = sItem.IndexOf( REQUESTED_INFO_ID_EQUAL ) ;
					if( iHeaderPosition != -1 )
					{
						sRequestedInfoID = sItem.Remove( 0 , iHeaderPosition + REQUESTED_INFO_ID_EQUAL.Length );
						iComma = sRequestedInfoID.IndexOf( COMMA_STRING );
						if( iComma != -1 ) 
							sRequestedInfoID = sRequestedInfoID.Split( COMMA_STRING.ToCharArray())[0];
						p_iRequestedInfoID = Common.Conversion.ConvertStrToInteger( sRequestedInfoID );
						continue;
					}

					iHeaderPosition = sItem.IndexOf( REQUESTED_INFO_CHILD_ID_EQUAL ) ;
					if( iHeaderPosition != -1 )
					{
						sRequestedInfoChildID = sItem.Remove( 0 , iHeaderPosition + REQUESTED_INFO_CHILD_ID_EQUAL.Length );
						iComma = sRequestedInfoChildID.IndexOf( COMMA_STRING );
						if( iComma != -1 ) 
							sRequestedInfoChildID = sRequestedInfoChildID.Split( COMMA_STRING.ToCharArray())[0];
						p_iRequestedInfoChildID = Common.Conversion.ConvertStrToInteger( sRequestedInfoChildID );
						continue;
					}

					iHeaderPosition = sItem.IndexOf( CLASS_EQUAL ) ;
					if( iHeaderPosition != -1 )
					{
						p_sClassName = sItem.Remove( 0 , iHeaderPosition + CLASS_EQUAL.Length );
						iComma = p_sClassName.IndexOf( COMMA_STRING );
						if( iComma != -1 ) 
							p_sClassName = p_sClassName.Split( COMMA_STRING.ToCharArray())[0];
						continue;
					}
                    //pgupta215  JIra  RMA-15677 
                    iHeaderPosition = sItem.IndexOf(PARENTTABLENAME_EQUAL);
                    if (iHeaderPosition != -1)
                    {
                        p_sParentTableName = (sItem.Remove(0, iHeaderPosition + PARENTTABLENAME_EQUAL.Length));
                        iComma = p_sClassName.IndexOf(COMMA_STRING);
                        if (iComma != -1)
                            p_sParentTableName = p_sClassName.Split(COMMA_STRING.ToCharArray())[0];
                        continue;
                    }
                    iHeaderPosition = sItem.IndexOf(PARENTROWID_EQUAL);
                    if (iHeaderPosition != -1)
                    {
                        p_iParentRowId = Common.Conversion.ConvertStrToInteger(sItem.Remove(0, iHeaderPosition + PARENTROWID_EQUAL.Length));
                        iComma = p_sClassName.IndexOf(COMMA_STRING);
                        if (iComma != -1)
                            p_iParentRowId = Common.Conversion.ConvertStrToInteger(p_sClassName.Split(COMMA_STRING.ToCharArray())[0]);
                        continue;
                    }
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.SplitKey.DataError", m_iClientId), p_objEx);
			}
		}

		/// <summary>
		/// Get the Vehicle Description string.
		/// </summary>
		/// <param name="p_objVehicle">Vehicle</param>
		/// <returns>Vehicle Description as a string </returns>
		private string VehicleDescForDisplay( Vehicle p_objVehicle )
		{
			string sVehicleDesc = "" ;
			try
			{
				if( p_objVehicle.VehicleYear != 0 )
					sVehicleDesc = p_objVehicle.VehicleYear.ToString() + " " ;

				sVehicleDesc += p_objVehicle.VehicleMake ;

				if( p_objVehicle.VehicleModel != "" )
					sVehicleDesc += " " + p_objVehicle.VehicleModel ;
			
				sVehicleDesc += " " + p_objVehicle.Vin ;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.VehicleDescForDisplay.DataError", m_iClientId), p_objEx);
			}
			return( sVehicleDesc );			
		}

		/// <summary>
		/// Get the type of the Person Involved
		/// </summary>
		/// <param name="p_sClassName">Class Name</param>
		/// <returns>Type of person involved as a string</returns>
		private string GetNameType( string p_sClassName )
		{
			switch( p_sClassName.ToLower() )
			{
				case "piemployee" :
					return( "Employee" );
				case "piwitness" :
					return( "Witness" );
				case "piother" :
					return( "Other Person" );
				case "patient" :
					return( "Patient" );
				case "piphysician" :
					return( "Physician" );
				case "pimedicalstaff" :
					return( "Medical Staff" );
				case "pipatient" :
					return( "Patient" );
                case "pidriver":  //Aman Driver Enhancement
                    return ("Driver");
			}
			return( "" );
		}
		/// <summary>
		/// Get a particular segment in a string 
		/// </summary>
		/// <param name="p_iSegment">Segment number</param>
		/// <param name="p_sIn">Input string </param>
		/// <param name="p_sDelimiter">Delimiter string</param>
		/// <returns>Segment string</returns>
		private string GetItemString( int p_iSegment , string p_sIn , string p_sDelimiter )
		{
			string[] arrItem = p_sIn.Split( p_sDelimiter.ToCharArray() );
			if( p_iSegment < 0 || p_iSegment > arrItem.GetUpperBound(0) )
                throw new IndexOutOfRangeException(Globalization.GetString("EventExplorerManager.GetItemString.IndexOutofRange", m_iClientId));			 
			return( arrItem[p_iSegment] );
		}

		/// <summary>
		/// Get the number of segments in a string
		/// </summary>
		/// <param name="p_sDelimiter">Delimiter string </param>
		/// <param name="p_sKey">Input Key</param>
		/// <returns>Number of segments</returns>
		private int CountItems( string p_sDelimiter , string p_sKey )
		{
			string[] arrSegment = p_sKey.Split( p_sDelimiter.ToCharArray() );
			return( arrSegment.GetUpperBound(0) + 1 );
		}
		
		/// <summary>
		/// Un Initialize the data model factory object. 
		/// </summary>
		private void CleanUp()
		{
			try
			{
				m_objDataModelFactory.UnInitialize();
				m_objCol = null ;
				m_objData = null ;
				m_objDocument = null ;
				m_objHeader = null ;
				m_objRow = null ;
				m_objRows = null ;
				m_sConnectionString = "" ;
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}

		/// <summary>
		/// Get the Code Description corresponding to the Code ID
		/// </summary>
		/// <param name="p_iCodeID">Code ID</param>
		/// <returns>Return the Code Description</returns>
		private string GetCodeDescription( int p_iCodeID )
		{
			string sSQL = "" ;
			string sCodeDesc = "" ;
			try
			{
				sSQL = "SELECT CODES_TEXT.CODE_DESC FROM CODES_TEXT WHERE CODES_TEXT.CODE_ID = " + p_iCodeID + " AND " + LANGUAGE_CODE ;
				sCodeDesc =  FetchValueFromDatabase( sSQL ) ;					
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetCodeDescription.DataError", m_iClientId), p_objEx);
			}
			return( sCodeDesc );
		}
		/// <summary>
		/// Get the Short Code corresponding to the Code ID
		/// </summary>
		/// <param name="p_iCodeID">Code ID</param>
		/// <returns>Return the Short Code</returns>
		private string GetShortCode( int p_iCodeID )
		{
			string sSQL = "" ;
			string sShortCode = "" ;			
			try
			{
				sSQL = "SELECT CODES.SHORT_CODE FROM CODES, CODES_TEXT WHERE CODES.RELATED_CODE_ID = CODES_TEXT.CODE_ID AND CODES.CODE_ID=" + p_iCodeID + " AND " + LANGUAGE_CODE ;
				sShortCode = FetchValueFromDatabase( sSQL ) ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetShortCode.DataError", m_iClientId), p_objEx);
			}
			return( sShortCode );
		}

		#endregion 
		public string GetLobString(string p_sLob,int p_iClaim)
		{
			string sLobString="";
			if (p_sLob.Trim()=="0")
			{
				return "Event Occurrence/Quick Summary";
			}
			switch(p_sLob)
			{
				case "241":
					sLobString="General Claims/Quick Summary";
					break;
				case "844":
					sLobString="Non-Occupational Claims/Quick Summary";
					break;
				case "243":
					sLobString="Workers' Compensation/Quick Summary";
					break;
				case "242":
                    sLobString="Vehicle Accident /Quick Summary";
					break;
                // Ayush, MITS: 18291, Date: 01/11/10, Start:
                case "845":
                    sLobString = "Property Claims/Quick Summary";
                    break;
                // Ayush, MITS: 18291, Date: 01/11/10, End
			}
			return sLobString;
		}
		#region EventExplorer Tree
		/// <summary>
		/// Generate the event summary Xml document 
		/// </summary>
		/// <param name="p_iEventID">Event ID</param>
		/// <returns>Xml Document</returns>
		public XmlDocument GetEventSummaryXml( int p_iEventID ,int p_iClaimId,string p_sLob)
		{			
			XmlDocument objEventSummaryDoc = null ;
			XmlElement objEventSummaryNode = null ;
			Event objEvent = null ;
			XmlElement objEventNode = null ;
			XmlElement objFollowUpNode = null ;
			XmlElement objPersonsInvolvedNode = null ;		
			XmlElement objClaimNode = null ;
			
			XmlElement objPiPatientNode = null ;
            XmlElement objPiDriverNode = null; //Aman Driver Enh
			XmlElement objPiWitnessNode = null ;
			XmlElement objPiOtherNode = null ;
			XmlElement objPiEmployeeNode = null ;
			XmlElement objPiPhysicianNode = null ;
			XmlElement objPiMedicalStaffNode = null ;

			PiPatient objPiPatient = null ;
            PiDriver objPiDriver = null;  //Aman Driver Enhancement
			Patient objPatient = null ;
			PiWitness objPiWitness = null ;
			PiOther objPiOther = null ;
			PiEmployee objPiEmployee = null ;
			PiPhysician objPiPhysician = null ;
			PiMedicalStaff objPiMedicalStaff = null ;

			LocalCache objLocalCache = null ;
			
			string sTypeOfPerson = "" ;
			string sClaimTypeDiscription = "" ;
			string sTemp = "" ;
			string sTempAbbre = "" ;										
			//MITS # 32341 - 20132404 - ksirigudi- start
            ArrayList arrlstClaim = new ArrayList();
            bool bSortCompleted;
            Claim objClaimtxt = null;
            Claim objNextClaimtxt = null;
			//MITS # 32341 - 20132404 - ksirigudi- End						
			
			HipaaLog objHippa=null;
			try
			{
				m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword,m_iClientId );	//sonali
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() ;

				// Initialize a new XML document and create "eventsummary" node
				objEventSummaryDoc = new XmlDocument();	
				objEventSummaryNode = objEventSummaryDoc.CreateElement( EVENT_SUMMARY_NODE );
				objEventSummaryDoc.AppendChild( objEventSummaryNode );

				// Get the reference of an event having event_id p_iEventID
				objEvent = ( Event ) m_objDataModelFactory.GetDataModelObject( "Event" , false );
				if( p_iEventID > 0 )
					objEvent.MoveTo( p_iEventID );
				else
                    throw new RMAppException(Globalization.GetString("EventExplorerManager.InvalidEventID", m_iClientId));
							
				// Create event node			
				objEventNode = objEventSummaryDoc.CreateElement( NODE );
				objEventSummaryNode.AppendChild( objEventNode );

				objEventNode.SetAttribute( KEY , "name=" + EVENT_NODE + ",eventid=" + p_iEventID );
				objEventNode.SetAttribute( TEXT , "Event - " + objEvent.EventNumber + " (Event Date: " + Conversion.GetDBDateFormat(objEvent.DateOfEvent , "d") + ")" );

				// Create Current Financials node
				CreateElement( objEventNode , "name=" + EVENT_FINANCIALS_NODE + ",eventid=" + p_iEventID , "Current Financials" );

				// Create Diaries node
				CreateElement( objEventNode , "name=" + EVENT_DIARIES_NODE + ",eventid=" + p_iEventID , "Diaries" );

				// Create Event Details node
				CreateElement( objEventNode , "name=" + EVENT_DETAILS_NODE+ ",eventid=" + p_iEventID , "Event Details" );

				// Create Follow up node				
				CreateElement( objEventNode , "name=" + EVENT_FOLLOWUP_NODE+ ",eventid=" + p_iEventID , "Follow Up" , ref objFollowUpNode );

				// Create "Actions" Node as a child of "Follow Up" node
				if( objEvent.EventXAction.Count > 0 )
					CreateElement( objFollowUpNode , "name=" + EVENT_ACTIONS_NODE + ",eventid=" + p_iEventID , "Actions");

				// Create "Outcomes" Node as a child of "Follow Up" node
				if( objEvent.EventXOutcome.Count > 0 )
					CreateElement( objFollowUpNode , "name=" + EVENT_OUTCOMES_NODE + ",eventid=" + p_iEventID , "Outcomes" );

				// Create Location Info node
				CreateElement( objEventNode , "name=" + EVENT_LOCATION_NODE + ",eventid=" + p_iEventID , "Location Info" );

				// Create OSHA Information node
				CreateElement( objEventNode , "name=" + EVENT_OSHA_NODE +",eventid=" + p_iEventID , "OSHA Information" );

                //Changed by Gagan for MITS 7658 : Start
                // Create Event Enhanced Notes node
                CreateElement(objEventNode, "name=" + EVENT_ENHANCED_NOTES_NODE + ",eventid=" + p_iEventID, "Event Enhanced Notes");
                //Changed by Gagan for MITS 7658 : End

				if( objEvent.PiList.Count > 0 )
				{
					// Create "Persons Involved" node
                    m_sParentTableName = "EVENT";
                    CreateElement(objEventNode, "name=" + EVENT_PERSONS_INVOLVED_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID + ",eventid=" + p_iEventID, "Persons Involved", ref objPersonsInvolvedNode);									
					
					foreach( object objPersonInvolved in objEvent.PiList )
					{															
						sTypeOfPerson =  GetItemString( CountItems( "." , objPersonInvolved.GetType().ToString() ) - 1 , objPersonInvolved.GetType().ToString() , "." );
						switch( sTypeOfPerson ) 
						{
							case "PiPatient" : //"PiPatient" :
								objPiPatient = objPersonInvolved as PiPatient ;
								if( objPiPatientNode == null )
									CreateElement( objPersonsInvolvedNode , "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID + ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson , "Patient" , ref objPiPatientNode );
								CreateElement( objPiPatientNode , "name=" + PI_NODE + ",eventid=" +p_iEventID + ",subid=" + objPiPatient.PiEid + ",class=" + sTypeOfPerson , objPiPatient.PiEntity.GetLastFirstName() );
								objHippa=new HipaaLog(m_sConnectionString, m_iClientId);
								if (p_sLob=="0")
								{
									objHippa.LogHippaInfo(objPiPatient.PiEid,p_iEventID.ToString(),"0",GetLobString(p_sLob,p_iClaimId),"VW",m_sUserName);
								}
								else
								{
									objHippa.LogHippaInfo(objPiPatient.PiEid,p_iEventID.ToString(),p_iClaimId.ToString(),GetLobString(p_sLob,p_iClaimId),"VW",m_sUserName);
								}
								break;
							case "Patient" : // "Patient" :
								objPatient = objPersonInvolved as Patient ;
								if( objPiPatientNode == null )
                                    CreateElement(objPersonsInvolvedNode, "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID + ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson, "Patient", ref objPiPatientNode);
								CreateElement( objPiPatientNode , "name=" + PI_NODE + ",eventid=" + p_iEventID + ",subid=" + objPatient.PatientEid + ",class=" + sTypeOfPerson , objPatient.PatientEntity.GetLastFirstName() );
								objHippa=new HipaaLog(m_sConnectionString, m_iClientId);
								if (p_sLob=="0")
								{
									objHippa.LogHippaInfo(objPatient.PatientEid,p_iClaimId.ToString(),"0",GetLobString(p_sLob,p_iClaimId),"VW",m_sUserName);
								}
								else
								{
									objHippa.LogHippaInfo(objPatient.PatientEid,p_iClaimId.ToString(),p_iClaimId.ToString(),GetLobString(p_sLob,p_iClaimId),"VW",m_sUserName);
								}
								
								break;
                            case "PiDriver": //"PiDriver" : Aman Driver Enhancement
                                objPiDriver = objPersonInvolved as PiDriver;
                                if (objPiDriverNode == null)
                                    CreateElement(objPersonsInvolvedNode, "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID + ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson, "Driver", ref objPiDriverNode);
                                CreateElement(objPiDriverNode, "name=" + PI_NODE + ",eventid=" + p_iEventID + ",subid=" + objPiDriver.PiEid + ",class=" + sTypeOfPerson, objPiDriver.PiEntity.GetLastFirstName());
                                break;                           
							case "PiWitness" :  // "PiWitness" :
								objPiWitness = objPersonInvolved as PiWitness ;
								if( objPiWitnessNode == null )
                                    CreateElement(objPersonsInvolvedNode, "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID +  ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson, "Witness", ref objPiWitnessNode);
								CreateElement( objPiWitnessNode , "name=" + PI_NODE + ",eventid=" + p_iEventID + ",subid=" + objPiWitness.PiEid + ",class=" + sTypeOfPerson , objPiWitness.PiEntity.GetLastFirstName() );
								break;
							case "PiOther" :  // "PiOther" :
								objPiOther = objPersonInvolved as PiOther ;
								if( objPiOtherNode == null )
                                    CreateElement(objPersonsInvolvedNode, "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID + ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson, "Other Person", ref objPiOtherNode);
								CreateElement( objPiOtherNode , "name=" + PI_NODE + ",eventid=" + p_iEventID + ",subid=" + objPiOther.PiEid + ",class=" + sTypeOfPerson , objPiOther.PiEntity.GetLastFirstName() );
								break;
							case "PiEmployee" :  // "PiEmployee" :
								objPiEmployee = objPersonInvolved as PiEmployee ;
								if( objPiEmployeeNode == null )
                                    CreateElement(objPersonsInvolvedNode, "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID +  ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson, "Employee", ref objPiEmployeeNode);
								CreateElement( objPiEmployeeNode , "name=" + PI_NODE + ",eventid=" + p_iEventID + ",subid=" + objPiEmployee.PiEid + ",class=" + sTypeOfPerson , objPiEmployee.PiEntity.GetLastFirstName() );
								break;								
							case "PiPhysician" :  // "Physician" :
								objPiPhysician = objPersonInvolved as PiPhysician ;
								if( objPiPhysicianNode == null )
                                    CreateElement(objPersonsInvolvedNode, "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID + ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson, "Physician", ref objPiPhysicianNode);
								CreateElement( objPiPhysicianNode , "name=" + PI_NODE + ",eventid=" + p_iEventID + ",subid=" + objPiPhysician.Physician.PhysEid + ",class=" + sTypeOfPerson , objPiPhysician.Physician.PhysEntity.GetLastFirstName() );
								break;								 
							case "PiMedicalStaff" :  // "PiMedicalStaff" :
								objPiMedicalStaff = objPersonInvolved as PiMedicalStaff ;
								if( objPiMedicalStaffNode == null )
                                    CreateElement(objPersonsInvolvedNode, "name=" + EVENT_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_iEventID + ",eventid=" + p_iEventID + ",class=" + sTypeOfPerson, "Medical Staff", ref objPiMedicalStaffNode);
								CreateElement( objPiMedicalStaffNode , "name=" + PI_NODE + ",eventid=" + p_iEventID + ",subid=" + objPiMedicalStaff.MedicalStaff.StaffEid + ",class=" + sTypeOfPerson , objPiMedicalStaff.MedicalStaff.StaffEntity.GetLastFirstName() );
								break;
						}					
					}
				}
				//MITS # 32341 - 20132404 - ksirigudi- start
				foreach( Claim objClaim in objEvent.ClaimList )
                {
                    arrlstClaim.Add(objClaim);
                }

                if (arrlstClaim.Count > 0)
                {
                    bSortCompleted = false;
                    while (!bSortCompleted)
                    {
                        bSortCompleted = true;
                        for (int iIndex = 0; iIndex < arrlstClaim.Count - 1; iIndex++)
                        {
                            objClaimtxt = (Claim)arrlstClaim[iIndex];
                            objNextClaimtxt = (Claim)arrlstClaim[iIndex + 1];
                            if (String.Compare(objClaimtxt.ClaimNumber.ToString(), objNextClaimtxt.ClaimNumber.ToString()) > 0)
                            {
                                bSortCompleted = false;
                                arrlstClaim.RemoveAt(iIndex);
                                arrlstClaim.Insert(iIndex, objNextClaimtxt);
                                arrlstClaim.RemoveAt(iIndex + 1);
                                arrlstClaim.Insert(iIndex + 1, objClaimtxt);
                            }                     
                        }
                    }
                }
                //END MITS # 32341 - 20132404 - ksirigudi


				// For each Claim, Create a claim node
                //MITS # 32341 - 20132404 - ksirigudi
                //foreach (Claim objClaim in objEvent.ClaimList)
				foreach (Claim objClaim in arrlstClaim)
				{				
					sClaimTypeDiscription = "" ;
					switch( objClaim.LineOfBusCode )
					{
						case GENERAL_CLAIM_BUS_TYPE_CODE :
							sClaimTypeDiscription = "General Claim" ;
							break;
						case VEHICLE_CLAIM_BUS_TYPE_CODE :
							sClaimTypeDiscription = "Vehicle Accident Claim" ;
							break;
						case WORKERS_CLAIM_BUS_TYPE_CODE :
							sClaimTypeDiscription = "Workers' Comp. Claim" ;
							break;
						case SHORT_TERM_CLAIM_BUS_TYPE_CODE :
							sClaimTypeDiscription = "Short Term Dis. Claim" ;
							break;
                        // Ayush, MITS: 18291, Date: 01/11/10, Start:
                        case PROPERTY_CLAIM_BUS_TYPE_CODE:
                            sClaimTypeDiscription = "Property Claim";
                            break;
                        // Ayush, MITS: 18291, Date: 01/11/10, End 
					}
					sClaimTypeDiscription += " Nr. " + objClaim.ClaimNumber ;
					if( objClaim.LineOfBusCode == WORKERS_CLAIM_BUS_TYPE_CODE || objClaim.LineOfBusCode == SHORT_TERM_CLAIM_BUS_TYPE_CODE  )
					{
						sTemp = "" ;
						sTempAbbre = "" ;
                        objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
						objLocalCache.GetStateInfo( objClaim.FilingStateId , ref sTempAbbre , ref sTemp );
						sClaimTypeDiscription += " (Jurisdiction: " + sTemp  + ")" ;
					}
					
					CreateElement( objEventNode , "name=" + CLAIM_NODE + ",eventid=" + p_iEventID + ",claimid=" + objClaim.ClaimId , sClaimTypeDiscription , ref objClaimNode );
					CreateClaimSummary( objClaimNode , objClaim , p_iEventID );
				}
				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetEventSummaryXml.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				objEventSummaryNode = null ;
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				objEventNode = null ;
				objFollowUpNode = null ;
				objPersonsInvolvedNode = null ;		
				objClaimNode = null ;
			
				objPiPatientNode = null ;
				objPiWitnessNode = null ;
				objPiOtherNode = null ;
				objPiEmployeeNode = null ;
				objPiPhysicianNode = null ;
				objPiMedicalStaffNode = null ;
				
				if( objPiPatient != null )
				{
					objPiPatient.Dispose();
					objPiPatient = null ;
				}
				if( objPatient != null )
				{
					objPatient.Dispose();
					objPatient = null ;
				}
                if (objPiDriver != null)  //Aman Driver Enh
                {
                    objPiDriver.Dispose();
                    objPiDriver = null;
                }
				if( objPiWitness != null )
				{
					objPiWitness.Dispose();
					objPiWitness = null ;
				}
				if( objPiOther != null )
				{
					objPiOther.Dispose();
					objPiOther = null ;
				}
				if( objPiEmployee != null )
				{
					objPiEmployee.Dispose();
					objPiEmployee = null ;
				}
				if( objPiPhysician != null )
				{
					objPiPhysician.Dispose();
					objPiPhysician = null ;
				}
				if( objPiMedicalStaff != null )
				{
					objPiMedicalStaff.Dispose();
					objPiMedicalStaff = null ;
				}
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
				
				CleanUp();
			}
			return( objEventSummaryDoc );

		}
		/// <summary>
		/// Generate Xml for a claim
		/// </summary>
		/// <param name="p_objClaimNode">Claim Node</param>
		/// <param name="p_objClaim">Claim</param>
		/// <param name="p_iEventID">Event ID</param>
		private void CreateClaimSummary( XmlElement p_objClaimNode , Claim p_objClaim , int p_iEventID )
		{
			XmlElement objCurrentFinancials = null ;
			XmlElement objPaymentHistory = null ;
            string sSql= string.Empty;
            DbReader objPolicyReader=null;
            int iPolicyId = 0;
            int iUseAdvClaim = p_objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm;

            //Changed by Gagan for MITS 7658 : Start
            int iUseClaimProgressNotes = 0;
            //Changed by Gagan for MITS 7658 : End
            XmlElement objPolicy = null; //MITS # 29792 - 20121009 - bsharma33 - BR Id # 5.1.19
			
			try
			{
                //Start:Added by Nitin Goel,03/15/2010 MITS:18230
                if (p_objClaim.LineOfBusCode == PROPERTY_CLAIM_BUS_TYPE_CODE)
                //End:Nitin Goel,03/15/2010 MITS:18230
                {
                    // Ayush, MITS: 18291, Date: 01/12/10, Start:
                    // Create Property claim Data Node
                    CreatePropertyData(p_objClaimNode, p_objClaim, p_iEventID);
                    // Ayush, MITS: 18291, Date: 01/12/10, End
                    // Create "Claim Notes" node
                }
				CreateElement( p_objClaimNode , "name=" + NOTES_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Claim Notes" );

                //Changed by Gagan for MITS 7658 : Start
                iUseClaimProgressNotes = p_objClaim.Context.DbConnLookup.ExecuteInt(
                        "SELECT USE_CLAIM_PROGRESS_NOTES FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + p_objClaim.LineOfBusCode.ToString());
                if (iUseClaimProgressNotes != 0)              
                    CreateElement(p_objClaimNode, "name=" + CLAIM_ENHANCED_NOTES_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Claim Enhanced Notes");
                //Changed by Gagan for MITS 7658 : End
			
				// Create "Diaries" node
				CreateElement( p_objClaimNode , "name=" + DIARIES_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Diaries" );                

				// Create "Current Financials" node
				CreateElement( p_objClaimNode , "name=" + CURRENT_RESERVES_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Current Financials" , ref objCurrentFinancials );

				// Create "Payment History" node as a child of "Current Financials" node
				CreateElement( objCurrentFinancials , "name=" + PAYMENT_HIST_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Payment History" , ref objPaymentHistory );

				// Create "By Payee" node as a child of "Payment History" node
				CreateElement( objPaymentHistory , "name=" + BY_PAYEE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "By Payee" );

				//Create "By Reserve Type" node as a child of "Payment History" node
				CreateElement( objPaymentHistory , "name=" + BY_RESERVE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "By Reserve Type" );
			
				//Create "By Benefit, Summary" node as a child of "Payment History" node
				CreateElement( objPaymentHistory , "name=" + BY_BENINFIT_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "By Benefit, Summary" );

				//Create "By Transaction Type" node as a child of "Payment History" node
				CreateElement( objPaymentHistory , "name=" + BY_TRANS_TYPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "By Transaction Type" );

				// Create "Future Auto Payments" node as a child of "Current Financials" node
				CreateElement( objCurrentFinancials , "name=" + FUTURE_PAYMENTS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Future Auto Payments" );
			
				// Create Parties Involved node
				CreatePartiesInvolved( p_objClaimNode , p_objClaim , p_iEventID );

                // Create Persons Involved node for claim
                CreateClaimPersonInvolved(p_objClaimNode, p_objClaim, p_iEventID); // pgupta215 Jira RMA-15677  
             
				// Create Properties Involved node
				CreatePropertiesInvolved( p_objClaimNode , p_objClaim , p_iEventID );
			
            
                // Create "Attached Policy" node
                  if (p_objClaim.PrimaryPolicy.PolicyId != 0)
                   {
                    Policy objCurrentPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);  // ijain4 : 11/10/2015
                    if (iUseAdvClaim == 0)
                        sSql = "SELECT PRIMARY_POLICY_ID,CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + p_objClaim.ClaimId;
                    else
                        sSql = "SELECT CLAIM_X_POLICY.POLICY_ID AS PRIMARY_POLICY_ID,CLAIM.CLAIM_TYPE_CODE FROM CLAIM_X_POLICY,CLAIM WHERE CLAIM_X_POLICY.CLAIM_ID = CLAIM.CLAIM_ID AND CLAIM_X_POLICY.CLAIM_ID =" + p_objClaim.ClaimId;
                        objPolicyReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                        using(DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                        {
                            while (objReader.Read())
                            {
                                iPolicyId = Conversion.ConvertObjToInt(objReader.GetValue("PRIMARY_POLICY_ID"), m_iClientId);
                                objCurrentPolicy.MoveTo(iPolicyId);
                                //  CreateElement(p_objClaimNode, "name=" + ATTACHED_POLICY_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + p_objClaim.PrimaryPolicy.PolicyId, "Attached Policy - " + p_objClaim.PrimaryPolicy.PolicyNumber + " " + p_objClaim.PrimaryPolicy.PolicyName, ref objPolicy);
                                CreateElement(p_objClaimNode, "name=" + ATTACHED_POLICY_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objCurrentPolicy.PolicyId, "Attached Policy - " + objCurrentPolicy.PolicyNumber + " " + objCurrentPolicy.PolicyName, ref objPolicy);

                                //MITS # 29792 - 20121009 - bsharma33 - BR Id # 5.1.19
                                //if (p_objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0) :TODO in R8, uncomment the check for carrier claim
                                //{ //create internal node of policy for policy coverages
                                CreateElement(objPolicy, "name=" + POLICY_COVERAGE_NODE + ",eventid=" + p_objClaim.EventId + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objCurrentPolicy.PolicyId, "Policy Coverage(s)");
                                //}
                                CreatePolicyPersonInvolved(objPolicy, objCurrentPolicy); // pgupta215 Jira RMA-15677  
                            }
	                     }
                    }
				// Create Litigation node
				CreateLitigations( p_objClaimNode , p_objClaim , p_iEventID );
                //skhare7
                CreateSubrogations(p_objClaimNode, p_objClaim, p_iEventID);
                CreateArbitrations(p_objClaimNode, p_objClaim, p_iEventID);
                CreateLiability(p_objClaimNode, p_objClaim, p_iEventID);
                CreatePropertyLoss(p_objClaimNode, p_objClaim, p_iEventID);
                
               
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreateClaimSummary.DataError", m_iClientId), p_objEx);
			}		
			finally
			{
				objCurrentFinancials = null ;
				objPaymentHistory = null ;
			}
		}
		/// <summary>
		/// Generate Xml for Claimants, Defendants and Adjusters.
		/// </summary>
		/// <param name="p_objClaimNode">Claim Node</param>
		/// <param name="p_objClaim">Claim</param>
		/// <param name="p_iEventID">Event ID</param>
		private void CreatePartiesInvolved( XmlElement p_objClaimNode , Claim p_objClaim , int p_iEventID )
		{
			XmlElement objClaimantNode = null ;
			XmlElement objClaimantLastFirstName = null ;

			XmlElement objDefendantNode = null ;
			XmlElement objAdjusterNode = null ;
            XmlElement objCurrentFinancials1 = null;//Parijat
            XmlElement objPaymentHistory1 = null;
						
			bool IsFirstClaimant = true ;	
			bool IsFirstDefendant = true ;		
			bool isFirstAdjuster = true ;
			try
			{									
				// Add each claimant as a child node to "Claimant" header node
				foreach( Claimant objClaimant in p_objClaim.ClaimantList )
				{
					if( objClaimant.ClaimantEid == 0 )
						break;
					if( IsFirstClaimant )
					{
						CreateElement( p_objClaimNode , "name=" + CLAIMANTS_NODE+ ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Claimant" , ref objClaimantNode );
						IsFirstClaimant = false ;
					}				
					objClaimantLastFirstName = null ;
					// Add each claimant as a child node to "Claimant" header node
					CreateElement( objClaimantNode , "name=" + CLAIMANT_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantRowId , objClaimant.ClaimantEntity.GetLastFirstName() , ref objClaimantLastFirstName ) ;
                    //parijat 21784
                    /*Added by gbindra MITS#34104 Create "Claimant Enhanced Notes" child node to claimant node*/
                    CreateElement(objClaimantLastFirstName, "name=" + CLAIMANT_ENHANCED_NOTES_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantRowId, "Claimant Enhanced Notes");
                    /*Added by gbindra MITS#34104*/
                    // Add "Current Financials" as a child node to claimant node
                   if( p_objClaim.LineOfBusCode != WORKERS_CLAIM_BUS_TYPE_CODE && p_objClaim.LineOfBusCode != SHORT_TERM_CLAIM_BUS_TYPE_CODE )
						CreateElement( objClaimantLastFirstName , "name=" + CLAIMANT_FINANCIALS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantEid , "Current Financials",ref objCurrentFinancials1 );
                    //parijat 21784
                    // Create "Payment History" node as a child of "Current Financials" node under claimant
                   if (p_objClaim.LineOfBusCode != WORKERS_CLAIM_BUS_TYPE_CODE && p_objClaim.LineOfBusCode != SHORT_TERM_CLAIM_BUS_TYPE_CODE)
                    CreateElement(objCurrentFinancials1, "name=" + CLAIMANT_PAYMENT_HIST_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantEid, "Payment History", ref objPaymentHistory1);
                   
                    //--------------------------------------------------------------------------------------------------------
                    // Create "By Payee" node as a child of "Payment History" node
                   if (objPaymentHistory1 != null)
                   {
                       CreateElement(objPaymentHistory1, "name=" + CLAIMANT_BY_PAYEE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantEid, "By Payee");

                       //Create "By Reserve Type" node as a child of "Payment History" node
                       CreateElement(objPaymentHistory1, "name=" + CLAIMANT_BY_RESERVE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantEid, "By Reserve Type");

                       //Create "By Benefit, Summary" node as a child of "Payment History" node
                       CreateElement(objPaymentHistory1, "name=" + CLAIMANT_BY_BENINFIT_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantEid, "By Benefit, Summary");

                       //Create "By Transaction Type" node as a child of "Payment History" node
                       CreateElement(objPaymentHistory1, "name=" + CLAIMANT_BY_TRANS_TYPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimant.ClaimantEid, "By Transaction Type");
                   }
                    //parijat
				}
				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePartiesInvolved.ClaimantDataError", m_iClientId), p_objEx);
			}
			finally
			{
				objClaimantNode = null ;
				objClaimantLastFirstName = null ;
			}

			try
			{
				// Add "Defendant" Header node
				foreach( Defendant objDefendant in p_objClaim.DefendantList )
				{
					if( objDefendant.DefendantEid == 0 )
						break;
					if( IsFirstDefendant )
					{
						CreateElement( p_objClaimNode , "name=" + DEFENDANTS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Defendant" , ref objDefendantNode ) ;
						IsFirstDefendant = false ;
					}
					CreateElement( objDefendantNode , "name=" + DEFENDANT_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objDefendant.DefendantRowId , objDefendant.DefendantEntity.GetLastFirstName() );
				}
				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePartiesInvolved.DefendantDataError", m_iClientId), p_objEx);
			}
			finally
			{
				objDefendantNode = null ;
			}

			try
			{
				// Add "Adjuster" Header node				
				// Add each Adjuster as a child node to "Adjuster" header node
				foreach( ClaimAdjuster objClaimAdjuster in p_objClaim.AdjusterList )
				{
					if( objClaimAdjuster.AdjusterEid == 0 )
						break ;
					if( isFirstAdjuster )
					{
						CreateElement( p_objClaimNode , "name=" + ADJUSTERS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Adjuster" , ref objAdjusterNode );					
						isFirstAdjuster = false ;
					}
					CreateElement( objAdjusterNode , "name=" + ADJUSTER_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimAdjuster.AdjRowId , objClaimAdjuster.AdjusterEntity.GetLastFirstName() );
				}
				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePartiesInvolved.AdjusterDataError", m_iClientId), p_objEx);
			}
			finally
			{
				objAdjusterNode = null ;
			}
		}

		/// <summary>
		/// Generate Xml for Unit Properties.
		/// </summary>
		/// <param name="p_objClaimNode">Claim Node</param>
		/// <param name="p_objClaim">Claim</param>
		/// <param name="p_iEventID">Event ID</param>
		private void CreatePropertiesInvolved( XmlElement p_objClaimNode , Claim p_objClaim , int p_iEventID )
		{
			XmlElement objInvolvedPropertyNode = null ;
            //skhare7 
            XmlElement objUnitSalvageNode = null;
            string sSql=  string.Empty;
            DbReader objReader = null;
           
			try
			{
				// Add "Involved Property(s)" Header node
                if (p_objClaim.UnitList.Count > 0)
                {

                    CreateElement(p_objClaimNode, "name=" + UNITS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Unit Loss(s)", ref objInvolvedPropertyNode);
                    // Add each Adjuster as a child node to "Adjuster" header node
                    foreach (UnitXClaim objUnitXClaim in p_objClaim.UnitList)
                    {
                       
                        CreateElement(objInvolvedPropertyNode, "name=" + UNIT_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objUnitXClaim.Vehicle.UnitId, objUnitXClaim.Vehicle.VehicleYear + " " + objUnitXClaim.Vehicle.VehicleMake + " " + objUnitXClaim.Vehicle.VehicleModel + " " + objUnitXClaim.Vehicle.Vin, ref objUnitSalvageNode);
                        if (objUnitXClaim.UnitRowId > 0)
                        {
                            sSql = "SELECT SALVAGE_ROW_ID,SALVAGE_TYPE,PARENT_NAME ,CONTROLNUMBER FROM SALVAGE WHERE PARENT_ID=" + objUnitXClaim.UnitRowId + " AND PARENT_NAME='UnitXClaim'";
                        
                        objReader = DbFactory.GetDbReader( m_sConnectionString , sSql );
                         if(objReader!=null)
                        {   
                             if(objReader.Read())
                             {
                        
                               if (objReader.GetValue("PARENT_NAME").ToString().CompareTo("UnitXClaim") == 0)
                                {
                                    CreateElement(objUnitSalvageNode, "name=" + SALVAGEUNIT_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Salvage:" + objReader.GetValue("CONTROLNUMBER").ToString());
                                }


                            }
                        
                       }
                    
                   }
                 }
                }
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePropertiesInvolved.DataError", m_iClientId), p_objEx);
			}			
			finally
			{
				 objInvolvedPropertyNode = null ;					
			}
		}

        /// <summary>
        /// Generate Xml for PErson Involved for claims- pgupta215 Jira RMA-15677 Start
        /// </summary>
        /// <param name="p_objClaimNode"></param>
        /// <param name="p_objClaim"></param>
        /// <param name="p_iEventID"></param>
        private void CreateClaimPersonInvolved(XmlElement p_objClaimNode, Claim p_objClaim, int p_iEventID)
        {
            XmlElement objInvolvedPropertyNode = null;
            string sSql = string.Empty;
            string sTypeOfPerson = "";
            XmlElement objPiPatientNode = null;
            XmlElement objPiDriverNode = null;
            XmlElement objPiWitnessNode = null;
            XmlElement objPiOtherNode = null;
            XmlElement objPiEmployeeNode = null;
            XmlElement objPiPhysicianNode = null;
            XmlElement objPiMedicalStaffNode = null;

            PiPatient objPiPatient = null;
            PiDriver objPiDriver = null;
            Patient objPatient = null;
            PiWitness objPiWitness = null;
            PiOther objPiOther = null;
            PiEmployee objPiEmployee = null;
            PiPhysician objPiPhysician = null;
            PiMedicalStaff objPiMedicalStaff = null;
            m_sParentTableName = "CLAIM";
            try
            {
                if (p_objClaim.PiList.Count > 0)
                {
                    // Create "Persons Involved" node
                    CreateElement(p_objClaimNode, "name=" + CLAIM_PERSONS_INVOLVED_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId + ",claimid=" + p_objClaim.ClaimId, "Persons Involved", ref objInvolvedPropertyNode);
                    foreach (object objPersonInvolved in p_objClaim.PiList)
                    {
                        sTypeOfPerson = GetItemString(CountItems(".", objPersonInvolved.GetType().ToString()) - 1, objPersonInvolved.GetType().ToString(), ".");
                        switch (sTypeOfPerson)
                        {
                            case "PiPatient":
                                objPiPatient = objPersonInvolved as PiPatient;
                                if (objPiPatientNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId + ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Patient", ref objPiPatientNode);
                                CreateElement(objPiPatientNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPiPatient.PiEid + ",class=" + sTypeOfPerson, objPiPatient.PiEntity.GetLastFirstName());
                                //objHippa = new HipaaLog(m_sConnectionString, m_iClientId);
                                //if (p_sLob == "0")
                                //{
                                //    objHippa.LogHippaInfo(objPiPatient.PiEid, p_iEventID.ToString(), "0", GetLobString(p_sLob, p_iClaimId), "VW", m_sUserName);
                                //}
                                //else
                                //{
                                //    objHippa.LogHippaInfo(objPiPatient.PiEid, p_iEventID.ToString(), p_iClaimId.ToString(), GetLobString(p_sLob, p_iClaimId), "VW", m_sUserName);
                                //}
                                break;
                            case "Patient":
                                objPatient = objPersonInvolved as Patient;
                                if (objPiPatientNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId +  ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Patient", ref objPiPatientNode);
                                CreateElement(objPiPatientNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPatient.PatientEid + ",class=" + sTypeOfPerson, objPatient.PatientEntity.GetLastFirstName());
                                //objHippa = new HipaaLog(m_sConnectionString, m_iClientId);
                                //if (p_sLob == "0")
                                //{
                                //    objHippa.LogHippaInfo(objPatient.PatientEid, p_iClaimId.ToString(), "0", GetLobString(p_sLob, p_iClaimId), "VW", m_sUserName);
                                //}
                                //else
                                //{
                                //    objHippa.LogHippaInfo(objPatient.PatientEid, p_iClaimId.ToString(), p_iClaimId.ToString(), GetLobString(p_sLob, p_iClaimId), "VW", m_sUserName);
                                //}

                                break;
                            case "PiDriver":
                                objPiDriver = objPersonInvolved as PiDriver;
                                if (objPiDriverNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId +  ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Driver", ref objPiDriverNode);
                                CreateElement(objPiDriverNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPiDriver.PiEid + ",class=" + sTypeOfPerson, objPiDriver.PiEntity.GetLastFirstName());
                                break;
                            case "PiWitness":
                                objPiWitness = objPersonInvolved as PiWitness;
                                if (objPiWitnessNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId + ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Witness", ref objPiWitnessNode);
                                CreateElement(objPiWitnessNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPiWitness.PiEid + ",class=" + sTypeOfPerson, objPiWitness.PiEntity.GetLastFirstName());
                                break;
                            case "PiOther":
                                objPiOther = objPersonInvolved as PiOther;
                                if (objPiOtherNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId + ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Other Person", ref objPiOtherNode);
                                CreateElement(objPiOtherNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPiOther.PiEid + ",class=" + sTypeOfPerson, objPiOther.PiEntity.GetLastFirstName());
                                break;
                            case "PiEmployee":
                                objPiEmployee = objPersonInvolved as PiEmployee;
                                if (objPiEmployeeNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId + ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Employee", ref objPiEmployeeNode);
                                CreateElement(objPiEmployeeNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPiEmployee.PiEid + ",class=" + sTypeOfPerson, objPiEmployee.PiEntity.GetLastFirstName());
                                break;
                            case "PiPhysician":
                                objPiPhysician = objPersonInvolved as PiPhysician;
                                if (objPiPhysicianNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId + ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Physician", ref objPiPhysicianNode);
                                CreateElement(objPiPhysicianNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPiPhysician.Physician.PhysEid + ",class=" + sTypeOfPerson, objPiPhysician.Physician.PhysEntity.GetLastFirstName());
                                break;
                            case "PiMedicalStaff":
                                objPiMedicalStaff = objPersonInvolved as PiMedicalStaff;
                                if (objPiMedicalStaffNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + CLAIM_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objClaim.ClaimId + ",claimid=" + p_objClaim.ClaimId + ",class=" + sTypeOfPerson, "Medical Staff", ref objPiMedicalStaffNode);
                                CreateElement(objPiMedicalStaffNode, "name=" + PI_NODE + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objPiMedicalStaff.MedicalStaff.StaffEid + ",class=" + sTypeOfPerson, objPiMedicalStaff.MedicalStaff.StaffEntity.GetLastFirstName());
                                break;
                        }
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePropertiesInvolved.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                objInvolvedPropertyNode = null;
            }
        }
        //  ppgupta215  JIra  RMA-15677  END
        /// <summary>
        /// Generate Xml for PErson Involved for policy
        /// </summary>
        /// <param name="p_objPolicyNode"></param>
        /// <param name="p_objPolicy"></param>
        private void CreatePolicyPersonInvolved(XmlElement p_objPolicyNode, Policy p_objPolicy)
        {
            XmlElement objInvolvedPropertyNode = null;
            string sSql = string.Empty;
            string sTypeOfPerson = "";
            XmlElement objPiPatientNode = null;
            XmlElement objPiDriverNode = null;
            XmlElement objPiWitnessNode = null;
            XmlElement objPiOtherNode = null;
            XmlElement objPiEmployeeNode = null;
            XmlElement objPiPhysicianNode = null;
            XmlElement objPiMedicalStaffNode = null;

            PiPatient objPiPatient = null;
            PiDriver objPiDriver = null;
            Patient objPatient = null;
            PiWitness objPiWitness = null;
            PiOther objPiOther = null;
            PiEmployee objPiEmployee = null;
            PiPhysician objPiPhysician = null;
            PiMedicalStaff objPiMedicalStaff = null;
            m_sParentTableName="POLICY";
            try
            {
                if (p_objPolicy.PiList.Count > 0)
                {
                    // Create "Persons Involved" node
                    CreateElement(p_objPolicyNode, "name=" + POLICY_PERSONS_INVOLVED_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId  +",policyid=" + p_objPolicy.PolicyId, "Persons Involved", ref objInvolvedPropertyNode); //ijain4 11/12/2015
                    foreach (object objPersonInvolved in p_objPolicy.PiList)
                    {
                        sTypeOfPerson = GetItemString(CountItems(".", objPersonInvolved.GetType().ToString()) - 1, objPersonInvolved.GetType().ToString(), ".");
                        switch (sTypeOfPerson)
                        {
                            case "PiPatient":
                                objPiPatient = objPersonInvolved as PiPatient;
                                if (objPiPatientNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Patient", ref objPiPatientNode);
                                CreateElement(objPiPatientNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPiPatient.PiEid + ",class=" + sTypeOfPerson, objPiPatient.PiEntity.GetLastFirstName());
                                break;
                            case "Patient":
                                objPatient = objPersonInvolved as Patient;
                                if (objPiPatientNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Patient", ref objPiPatientNode);
                                CreateElement(objPiPatientNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPatient.PatientEid + ",class=" + sTypeOfPerson, objPatient.PatientEntity.GetLastFirstName());
                                break;
                            case "PiDriver":
                                objPiDriver = objPersonInvolved as PiDriver;
                                if (objPiDriverNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Driver", ref objPiDriverNode);
                                CreateElement(objPiDriverNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPiDriver.PiEid + ",class=" + sTypeOfPerson, objPiDriver.PiEntity.GetLastFirstName());
                                break;
                            case "PiWitness":
                                objPiWitness = objPersonInvolved as PiWitness;
                                if (objPiWitnessNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Witness", ref objPiWitnessNode);
                                CreateElement(objPiWitnessNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPiWitness.PiEid + ",class=" + sTypeOfPerson, objPiWitness.PiEntity.GetLastFirstName());
                                break;
                            case "PiOther":
                                objPiOther = objPersonInvolved as PiOther;
                                if (objPiOtherNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Other Person", ref objPiOtherNode);
                                CreateElement(objPiOtherNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPiOther.PiEid + ",class=" + sTypeOfPerson, objPiOther.PiEntity.GetLastFirstName());
                                break;
                            case "PiEmployee":
                                objPiEmployee = objPersonInvolved as PiEmployee;
                                if (objPiEmployeeNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Employee", ref objPiEmployeeNode);
                                CreateElement(objPiEmployeeNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPiEmployee.PiEid + ",class=" + sTypeOfPerson, objPiEmployee.PiEntity.GetLastFirstName());
                                break;
                            case "PiPhysician":
                                objPiPhysician = objPersonInvolved as PiPhysician;
                                if (objPiPhysicianNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Physician", ref objPiPhysicianNode);
                                CreateElement(objPiPhysicianNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPiPhysician.Physician.PhysEid + ",class=" + sTypeOfPerson, objPiPhysician.Physician.PhysEntity.GetLastFirstName());
                                break;
                            case "PiMedicalStaff":
                                objPiMedicalStaff = objPersonInvolved as PiMedicalStaff;
                                if (objPiMedicalStaffNode == null)
                                    CreateElement(objInvolvedPropertyNode, "name=" + POLICY_PI_CLASS_NODE + ",parenttable=" + m_sParentTableName + ",parentrowid=" + p_objPolicy.PolicyId + ",policyid=" + p_objPolicy.PolicyId + ",class=" + sTypeOfPerson, "Medical Staff", ref objPiMedicalStaffNode);
                                CreateElement(objPiMedicalStaffNode, "name=" + PI_NODE + ",policyid=" + p_objPolicy.PolicyId + ",subid=" + objPiMedicalStaff.MedicalStaff.StaffEid + ",class=" + sTypeOfPerson, objPiMedicalStaff.MedicalStaff.StaffEntity.GetLastFirstName());
                                break;
                        }
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePropertiesInvolved.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                objInvolvedPropertyNode = null;
            }
        }
        //  ppgupta215  JIra  RMA-15677  END
        /// <summary>
		/// Generate Xml for Litigations.
		/// </summary>
		/// <param name="p_objClaimNode">Claim Node</param>
		/// <param name="p_objClaim">Claim</param>
		/// <param name="p_iEventID">Event ID</param>
		private void CreateLitigations( XmlElement p_objClaimNode , Claim p_objClaim , int p_iEventID )
		{
			XmlElement objLitigationNode = null ;
			XmlElement objDocketNumberNode = null ;
						
			try
			{
				// Add "Litigation(s)" Header node
				if( p_objClaim.LitigationList.Count > 0 )
				{					
					CreateElement( p_objClaimNode , "name=" + LITIGATIONS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId , "Litigation(s)" , ref objLitigationNode );
					
					// Add each Litigation as a child node to "Litigation" header node
					foreach( ClaimXLitigation objClaimXLitigation in p_objClaim.LitigationList )
					{
						if( objClaimXLitigation.LitigationRowId == 0 )
							break;
						objDocketNumberNode = null ;
						
						// Add each "Docket Number" as a child node to "Litigation" header node
						CreateElement( objLitigationNode , "name=" + LIT_DOCKET_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXLitigation.LitigationRowId , "Docket Number: " + objClaimXLitigation.DocketNumber , ref objDocketNumberNode );
						
						// Add "Attorney" as a child node to "Docket Number" node						 						
						if ( objClaimXLitigation.CoAttorneyEid != 0 )   
							CreateElement( objDocketNumberNode , "name=" + LIT_ATTORNEY_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXLitigation.LitigationRowId , "Attorney - " + objClaimXLitigation.CoAttorneyEntity.GetLastFirstName() );
						
						//Add "Judge" as a child node to "Docket Number" node
						if( objClaimXLitigation.JudgeEid != 0 )
							if( objClaimXLitigation.JudgeEntity.LastName != "" )
								CreateElement( objDocketNumberNode , "name=" + LIT_JUDGE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXLitigation.LitigationRowId , "Judge - " + objClaimXLitigation.JudgeEntity.GetLastFirstName() );
						
						// Add each "Expert" node as a child node to "Docket Number" node
						foreach( Expert objExpert in objClaimXLitigation.ExpertList )
						{	
							if( objExpert.ExpertEid == 0 )
								break;
							CreateElement( objDocketNumberNode , "name=" + LIT_EXPERT_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXLitigation.LitigationRowId + ",subid2=" + objExpert.ExpertEid , "Expert Witness - " + objExpert.ExpertEntity.GetLastFirstName() );
						}
					}					
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreateLitigations.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				objLitigationNode = null ;
				objDocketNumberNode = null ;
			}
		}

        /// <summary>
        /// Generate Xml for Subrogations.//skhare7 R8 enhancement
        /// </summary>
        /// <param name="p_objClaimNode">Claim Node</param>
        /// <param name="p_objClaim">Claim</param>
        /// <param name="p_iEventID">Event ID</param>
        private void CreateSubrogations(XmlElement p_objClaimNode, Claim p_objClaim, int p_iEventID)
        {
            XmlElement objSubrogationNode = null;
            XmlElement objClaimNumberNode = null;
            Entity objEntity = null;


            try
            {
                // Add "Litigation(s)" Header node
                if (p_objClaim.SubrogationList.Count > 0)
                {
                    objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                   
                    CreateElement(p_objClaimNode, "name=" + SUBROGATIONS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Subrogations(s)", ref objSubrogationNode);

                    // Add each subrogation as a child node to "subrogation" header node
                    foreach (ClaimXSubrogation objClaimXSubrogation in p_objClaim.SubrogationList)
                    {
                        if (objClaimXSubrogation.SubrogationRowId == 0)
                            break;
                        objClaimNumberNode = null;

                      
                        CreateElement(objSubrogationNode, "name=" + SUB_CLAIMNO_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXSubrogation.SubrogationRowId, "Claim Number: " + objClaimXSubrogation.SubAdverseClaimNum, ref objClaimNumberNode);

                     			 						
                       
                            CreateElement(objClaimNumberNode, "name=" + SUB_POLICYNO_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXSubrogation.SubrogationRowId, "Policy Number - " + objClaimXSubrogation.SubAdversePolicyNum.ToString());

                            CreateElement(objClaimNumberNode, "name=" + SUB_TYPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXSubrogation.SubrogationRowId, "Subrogation Type - " + GetCodeDescription(objClaimXSubrogation.SubTypeCode));
                            CreateElement(objClaimNumberNode, "name=" + SUB_STATUS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXSubrogation.SubrogationRowId, "Status - " + GetCodeDescription(objClaimXSubrogation.SubStatusCode));

                      
                      
                         if (objClaimXSubrogation.StatusDate != string.Empty)
                         {

                             CreateElement(objClaimNumberNode, "name=" + SUB_STATUSDATE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXSubrogation.SubrogationRowId, "Status Date - " +Conversion.GetDBDateFormat(objClaimXSubrogation.StatusDate, "d"));
                         }
                      
                     
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreateLitigations.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                objSubrogationNode = null;
                objClaimNumberNode = null;
            }
        }

        //skhare7 R8 Arbitrations
        /// <summary>
        /// Generate Xml for Arbitrations.//skhare7 R8 enhancement
        /// </summary>
        /// <param name="p_objClaimNode">Claim Node</param>
        /// <param name="p_objClaim">Claim</param>
        /// <param name="p_iEventID">Event ID</param>
        private void CreateArbitrations(XmlElement p_objClaimNode, Claim p_objClaim, int p_iEventID)
        {
            XmlElement objArbitrationNode = null;
            XmlElement objArbTypeNode = null;
            string sArbtypeCode=string.Empty;
            string sArbStatusCode = string.Empty;
           

            try
            {
                // Add "Litigation(s)" Header node
                if (p_objClaim.ArbitrationList.Count > 0)
                {


                    CreateElement(p_objClaimNode, "name=" + ARBITRATIONS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Arbitration(s)", ref objArbitrationNode);

                    // Add each subrogation as a child node to "subrogation" header node
                    foreach (ClaimXArbitration objClaimXArbitration in p_objClaim.ArbitrationList)
                    {
                        if (objClaimXArbitration.ArbitrationRowId == 0)
                            break;
                        objArbTypeNode = null;
                        //Mgaba2:MITS 29283
                        //if (objClaimXArbitration.DateFiled != string.Empty)
                        //{

                            CreateElement(objArbitrationNode, "name=" + ARB_DATEFILED_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXArbitration.ArbitrationRowId, "Filed Date - " + Conversion.GetDBDateFormat(objClaimXArbitration.DateFiled, "d"), ref objArbTypeNode);
                        //}
                        sArbtypeCode = GetCodeDescription(objClaimXArbitration.ArbTypeCode);
                        if(sArbtypeCode!=string.Empty)

                            CreateElement(objArbTypeNode, "name=" + ARB_TYPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXArbitration.ArbitrationRowId, "Arbitration Type: " + sArbtypeCode);
                        //Mgaba2:MITS 29283:Missing assignment
                        sArbStatusCode = GetCodeDescription(objClaimXArbitration.ArbStatusCode);

                       if(sArbStatusCode!=string.Empty)
                           CreateElement(objArbTypeNode, "name=" + ARB_STATUS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXArbitration.ArbitrationRowId, "Status - " + sArbStatusCode);


                       


                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreateArbitrations.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                objArbTypeNode = null;
                objArbitrationNode = null;
            }
        }
        //skhare7 R8 Arbitrations
        /// <summary>
        /// Generate Xml for Arbitrations.//skhare7 R8 enhancement
        /// </summary>
        /// <param name="p_objClaimNode">Claim Node</param>
        /// <param name="p_objClaim">Claim</param>
        /// <param name="p_iEventID">Event ID</param>
        private void CreateLiability(XmlElement p_objClaimNode, Claim p_objClaim, int p_iEventID)
        {
            XmlElement objLiablityNode = null;
           
            string sLiabtypeCode = string.Empty;
            string sArbStatusCode = string.Empty;


            try
            {
                // Add "Litigation(s)" Header node
                if (p_objClaim.LiabilityLossList.Count > 0)
                {


                    CreateElement(p_objClaimNode, "name=" + LIABILITY_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Liability Loss(s)", ref objLiablityNode);

                    // Add each subrogation as a child node to "subrogation" header node
                    foreach (ClaimXLiabilityLoss objClaimXLiability in p_objClaim.LiabilityLossList)
                    {
                        if (objClaimXLiability.RowId == 0)
                            break;
                       

                        sLiabtypeCode = GetCodeDescription(objClaimXLiability.LiabilityType);
                        if (sLiabtypeCode != string.Empty)

                            CreateElement(objLiablityNode, "name=" + LIABILITY_TYPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXLiability.RowId, "Liability Type: " + sLiabtypeCode);

                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreateLiability.DataError", m_iClientId), p_objEx);
            }
            finally
            {
              //  objLiabilityTypeNode = null;
                objLiablityNode = null;
            }
        }
        //skhare7 R8 PropertyLoss
        /// <summary>
        /// Generate Xml for PropertyLoss.//skhare7 R8 enhancement
        /// </summary>
        /// <param name="p_objClaimNode">Claim Node</param>
        /// <param name="p_objClaim">Claim</param>
        /// <param name="p_iEventID">Event ID</param>
        private void CreatePropertyLoss(XmlElement p_objClaimNode, Claim p_objClaim, int p_iEventID)
        {
            XmlElement objPropLossNode = null;
             XmlElement objPropLossSalvageNode = null;
            

            string sPropLosstypeCode = string.Empty;

           
            string sSql = string.Empty;
            DbReader objReader = null;
           

            try
            {
                // Add "Litigation(s)" Header node
                if (p_objClaim.PropertyLossList.Count > 0)
                {


                    CreateElement(p_objClaimNode, "name=" + PROPERTYLOSS_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Property Loss(s)", ref objPropLossNode);

                    // Add each subrogation as a child node to "subrogation" header node
                    foreach (ClaimXPropertyLoss objClaimXPropLoss in p_objClaim.PropertyLossList)
                    {
                        if (objClaimXPropLoss.RowId == 0)
                            break;


                        sPropLosstypeCode = GetCodeDescription(objClaimXPropLoss.PropertyType);
                        //MGaba2:MITS 29283
                        //if (sPropLosstypeCode != string.Empty)
                        CreateElement(objPropLossNode, "name=" + PROPERTYLOSS_TYPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + objClaimXPropLoss.RowId, "Property Type: " + sPropLosstypeCode, ref objPropLossSalvageNode);


                        if (objClaimXPropLoss.RowId > 0)
                        {
                            sSql = "SELECT SALVAGE_ROW_ID,CONTROLNUMBER,PARENT_NAME FROM SALVAGE WHERE PARENT_ID=" + objClaimXPropLoss.RowId + " AND PARENT_NAME='ClaimXPropertyLoss'";

                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                            if (objReader != null)
                            {
                                if (objReader.Read())
                                {

                                    if (objReader.GetValue("PARENT_NAME").ToString().CompareTo("ClaimXPropertyLoss") == 0)
                                    {
                                        CreateElement(objPropLossSalvageNode, "name=" + SALVAGEPROP_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId, "Salvage:" + objReader.GetValue("CONTROLNUMBER").ToString());
                                    }


                                }

                            }

                        }
                     
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePropertyLoss.DataError", m_iClientId), p_objEx);
            }
            finally
            {

                objPropLossNode = null;
            }
        }

       
        // Ayush, MITS: 18291, Date: 01/12/10: Start
        /// <summary>
        /// Generate Xml for Property Claims.
        /// </summary>
        /// <param name="p_objClaimNode">Claim Node</param>
        /// <param name="p_objClaim">Claim</param>
        /// <param name="p_iEventID">Event ID</param>
        private void CreatePropertyData(XmlElement p_objClaimNode, Claim p_objClaim, int p_iEventID)
        {
            XmlElement objPropertyDataNode = null;

            try
            {
                //Start:Added by Nitin Goel,03/15/2010 MITS:18230
                //// Add "Property Claim" Header nodes
                //if ((p_objClaim.PropertyClaim.PropertyID > 0) && (p_objClaim.LineOfBusCode == PROPERTY_CLAIM_BUS_TYPE_CODE))
                if (p_objClaim.PropertyClaim.PropertyID > 0)
                //End:Nitin Goel,03/15/2010 MITS:18230
                {
                        CreateElement(p_objClaimNode, "name=" + CLAIM_PROPERTY_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId+ ",subid=" + p_objClaim.PropertyClaim.PropertyID, "Property Info", ref objPropertyDataNode);
                        CreateElement(objPropertyDataNode, "name=" + CLAIM_PROPERTY_COPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + p_objClaim.PropertyClaim.PropertyID, "COPE Data");
                        CreateElement(objPropertyDataNode, "name=" + CLAIM_PROPERTY_OP_COPE_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + p_objClaim.PropertyClaim.PropertyID, "Optional Cope Data");
                        CreateElement(objPropertyDataNode, "name=" + CLAIM_PROPERTY_SCHD_NODE + ",eventid=" + p_iEventID + ",claimid=" + p_objClaim.ClaimId + ",subid=" + p_objClaim.PropertyClaim.PropertyID, "Schedule");
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreatePropertyData.DataError", m_iClientId), p_objEx);
            }
            finally
            {
                objPropertyDataNode = null;
            }
        }

        // Ayush, MITS: 18291, Date: 01/12/10, End


		/// <summary>
		/// Append a new child node in the parent node. 
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sKey">Attribute Key</param>
		/// <param name="p_sText">Attribute Text</param>
		private void CreateElement( XmlElement p_objParentNode , string	p_sKey , string p_sText )
		{
			XmlElement objChildNode = null ;			
			CreateElement( p_objParentNode , p_sKey , p_sText , ref objChildNode ) ;			
		}

		/// <summary>
		/// Append a new child node in the parent node. Also maintain the reference to child node.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sKey">Attribute Key</param>
		/// <param name="p_sText">Attribute Text</param>
		/// <param name="p_objChildNode">Reference to Child Node</param>
		private void CreateElement( XmlElement p_objParentNode , string	p_sKey , string p_sText , ref XmlElement p_objChildNode )
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( "node" ) ;
				p_objParentNode.AppendChild( p_objChildNode );
				p_objChildNode.SetAttribute( KEY , p_sKey );
				p_objChildNode.SetAttribute( TEXT , p_sText );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.CreateElement.Error", m_iClientId), p_objEx);
			}

		}
			
		#endregion 

		#region Get Payee Details
		/// Name		: GetPayeeDetails
		/// Author		: Vaibhav Kaushik
		/// Date Created: 04/07/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// GetPayeeDetails returns the Payee details.
		/// </summary>
		/// <param name="p_iClaimId"> Claim Id</param>
		/// <param name="p_iPayeeId"> Payee Id</param>
		/// <param name="p_sSortCol"> Sort Column</param>
		/// <param name="p_bAscending">Ascending Flag</param>
		/// <returns> XmlDocument contains Payee details</returns>
		public XmlDocument GetPayeeDetails( int p_iClaimId , int p_iPayeeId , string p_sSortCol , bool p_bAscending )
		{
			StringBuilder sbSQL = null ;
			XmlDocument objPayeeInfoDoc = null ;
			XmlElement objRootNode = null ;
			XmlElement objRowNode = null ;
			XmlElement objTempNode = null ;
			DbReader objReader = null ;

			const string  DITTO = "\"" ;

			int iTransId = 0 ;
			int iLastTransId = 0 ;	
            //int iTransNumber = 0 ;
            long iTransNumber = 0;       //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database
			double dblAmount = 0.0 ;
			double dblTotalAll = 0.0 ;
			double dblSplitAmount = 0.0 ;
            double dblIAmount = 0.0;
			bool bPayment = false ;			
			string sSortCol = string.Empty ;
			string sCtlNumber = string.Empty ;
			string sTransDate = string.Empty ;
			string sAmount = string.Empty ;
			string sTransNumber = string.Empty ;
			string sDateRange = string.Empty ;
			string sInvoiceNumber = string.Empty ;
			string sCodeDesc = string.Empty ;

			try
			{
				m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword ,m_iClientId);	//sonali
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() ;

                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(p_iClaimId, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

				sbSQL = new StringBuilder();
				sbSQL.Append( " SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, " );
				sbSQL.Append( " FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, " );
				sbSQL.Append( " FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT," );
				sbSQL.Append( " FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE, " );
				sbSQL.Append( " FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, " );
				sbSQL.Append( " FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.CHECK_MEMO, FUNDS.STATUS_CODE" );
				sbSQL.Append( " FROM FUNDS, FUNDS_TRANS_SPLIT,CODES_TEXT WHERE FUNDS.CLAIM_ID = " + p_iClaimId );
				sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.PAYMENT_FLAG<>0 AND FUNDS.VOID_FLAG=0 " );

				if( p_iPayeeId > 0  )
					sbSQL.Append( " AND FUNDS.PAYEE_EID = " + p_iPayeeId );
			
				sbSQL.Append( " AND CODES_TEXT.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE " );

				if( p_sSortCol == "" )
				{
					sbSQL.Append( " ORDER BY FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID " );
					sSortCol = "FUNDS.TRANS_DATE" ;
				}
				else
				{
					sSortCol = p_sSortCol ;
					if( p_bAscending )
						sbSQL.Append( " ORDER BY " + sSortCol + " ASC, FUNDS.TRANS_ID" );
					else
						sbSQL.Append( " ORDER BY " + sSortCol + " DESC, FUNDS.TRANS_ID");
				}

				if( m_sDBType == Constants.DB_ACCESS )
					sbSQL = sbSQL.Replace( "|", " AS " );
				else
					sbSQL = sbSQL.Replace( "|", " " );
	
				// Initialize a new XML document and create PAYEE_INFO_NODE node
				objPayeeInfoDoc = new XmlDocument();	
				objRootNode = objPayeeInfoDoc.CreateElement( PAYEE_INFO_NODE );
				objPayeeInfoDoc.AppendChild( objRootNode );

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );

				if( objReader != null )
				{
					while( objReader.Read() )
					{					
						// skip duplicates caused by inner join with splits table
                        iTransId = Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
						if( iTransId != iLastTransId )
						{
							iLastTransId = iTransId ;
                            bPayment = Common.Conversion.ConvertObjToBool(objReader.GetValue("PAYMENT_FLAG"), m_iClientId);						
							sCtlNumber = objReader.GetString( "CTL_NUMBER" );				
                            //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database
							//iTransNumber =	Common.Conversion.ConvertObjToInt( objReader.GetValue( "TRANS_NUMBER" ), m_iClientId );						
                            iTransNumber = Common.Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_NUMBER"), m_iClientId);						
							sTransDate = Conversion.GetDBDateFormat( objReader.GetString( "TRANS_DATE" ) , "d" );
							dblAmount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr( objReader.GetValue( "FAMT" ) ));
							if( !bPayment )
								dblAmount = -dblAmount ;           
							dblTotalAll = dblTotalAll + dblAmount ;
										
							sTransNumber = iTransNumber.ToString();
							sAmount = string.Format( "{0:C}" , dblAmount ) ;
						}
						else
						{
							sCtlNumber = DITTO ;
							sTransNumber = DITTO ;
							sTransDate = DITTO ;
							sAmount = DITTO ;
						}
						sDateRange = Conversion.GetDBDateFormat( objReader.GetString( "FROM_DATE" ) , "d" )
							+ " - "	+ Conversion.GetDBDateFormat( objReader.GetString( "TO_DATE" ) , "d" );

						sInvoiceNumber = objReader.GetString( "INVOICE_NUMBER" );
						sCodeDesc = objReader.GetString( "CODE_DESC" );
						dblSplitAmount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr( objReader.GetValue( "FTSAMT" ) ));
                        dblIAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue("FAMT"))); //JIRA RMA-410

						if( !bPayment )
							dblSplitAmount = - dblSplitAmount ;	
                        if (!bPayment)
                            dblIAmount = -dblIAmount;	

						objRowNode = objPayeeInfoDoc.CreateElement( "row" );						

						objTempNode = objPayeeInfoDoc.CreateElement( "CtlNumber" );
						objTempNode.InnerText = sCtlNumber ;
						objRowNode.AppendChild( objTempNode );

						objTempNode = objPayeeInfoDoc.CreateElement( "TransNumber" );
						objTempNode.InnerText = sTransNumber ;
						objRowNode.AppendChild( objTempNode );

						objTempNode = objPayeeInfoDoc.CreateElement( "TransDate" );
						objTempNode.InnerText = sTransDate	 ;
						objRowNode.AppendChild( objTempNode );

						objTempNode = objPayeeInfoDoc.CreateElement( "Amount" );
						objTempNode.InnerText = sAmount ; 
						objRowNode.AppendChild( objTempNode );

						objTempNode = objPayeeInfoDoc.CreateElement( "Date" );
						objTempNode.InnerText = sDateRange ;
						objRowNode.AppendChild( objTempNode );

						objTempNode = objPayeeInfoDoc.CreateElement( "InvoiceNumber" );
						objTempNode.InnerText = sInvoiceNumber ;
						objRowNode.AppendChild( objTempNode );

						objTempNode = objPayeeInfoDoc.CreateElement( "CodeDesc" );
						objTempNode.InnerText = sCodeDesc ;
						objRowNode.AppendChild( objTempNode );
                        objTempNode = objPayeeInfoDoc.CreateElement("InvoiceAmount");
                        objTempNode.InnerText = string.Format("{0:C}", dblIAmount);
						objRowNode.AppendChild( objTempNode );

						objTempNode = objPayeeInfoDoc.CreateElement( "SplitAmount" );
						objTempNode.InnerText = string.Format( "{0:C}" , dblSplitAmount ); 
						objRowNode.AppendChild( objTempNode );

						objRootNode.AppendChild( objRowNode );
					}
				}

				objTempNode = objPayeeInfoDoc.CreateElement( "TotalAll" );
				objTempNode.InnerText = string.Format( "{0:C}" , dblTotalAll ); 
				objRootNode.AppendChild( objTempNode );		
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EventExplorerManager.GetPayeeDetails.DataError", m_iClientId), p_objEx);
			}
			finally
			{
				sbSQL = null ;
				objRootNode = null ;
				objRowNode = null ;
				objTempNode = null ;
				if( objReader != null )
				{
					objReader.Dispose();
					objReader = null ;
				}								
				CleanUp();
			}
			return( objPayeeInfoDoc );
		}
		#endregion 
		
        #region Get Claimant Name
        //tmalhotra2 MITS-29787 Re# 5.1.13
        /// Name		: GetClaimants
        /// Author		: Tushar Malhotra
        /// Date Created: 04/07/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// GetClaimans returns the Primary Claimant/defualt Claimants name.
        /// </summary>
        /// <param name="p_iClaimId"> Claim Id</param>
        /// <returns> XmlDocument contains Payee details</returns>
        private string GetClaimants(int p_iClaimId)
        {
            Claim objClaim = null;
            string sClaim_PrimaryClaimant = string.Empty;
            // akaushik5 Added for MITS 37330 Starts
            if (p_iClaimId.Equals(default(int)))
            {
                return string.Empty;
            }
            // akaushik5 Added for MITS 37330 Ends
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                sClaim_PrimaryClaimant = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
            
            return sClaim_PrimaryClaimant;
        }

        #endregion 
	}
}
