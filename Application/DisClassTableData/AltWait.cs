using System;
using System.Xml;
using System.Data;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.AltWaitPeriods
{
	/**************************************************************
		 * $File		: AltWait.cs
		 * $Revision	: 1.0.0.0
		 * $Date		: 08/27/2005
		 * $Author		: Mihika Agrawal
		 * $Comment		: 
		 * $Source		:  	
		**************************************************************/

	/// <summary>
	/// AltWait class
	/// </summary>
	public class AltWait : IDisposable
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
        private int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public AltWait(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			this.Initialize();
		}
		#endregion
        private bool _isDisposed = false;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                GC.SuppressFinalize(this);
            }
        }

		#region Public Methods

		/// Name		: GetListAltWait
		/// Author		: Mihika Agrawal
		/// Date Created: 08/27/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the list of the Alternate Waiting Periods for a Plan Class
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document
		///		The structure of the input XML :
		///		<Document>
		///			<AltWaitPeriods>
		///				<ClassRowId />
		///			</AltWaitPeriods>
		///		</Document>
		/// </param>
		/// <returns>XML Document with data. Structure:
		///		<Document>
		///			<AltWaitPeriods>
		///				<ClassRowId />
		///				<List>
		///					<Row>
		///						<WaitRowId/>
		///						<Quantity/>
		///						<Period/>
		///						<DisabilityType/>
		///					</Row>
		///				</List>
		///			</AltWaitPeriods>
		///		</Document>
		/// </returns>
		public XmlDocument GetListAltWait(XmlDocument p_objXmlDoc)
		{
			XmlElement objLstRow = null;
			XmlElement objRowTxt = null;
			XmlNode objNode = null;
			LocalCache objCache = null;

			string sClassRowId = string.Empty;
			string sSQL = string.Empty;

			DisabilityClass objDisClass = null;
			
			try
			{
				objCache = new LocalCache(m_sConnectionString,m_iClientId);

				sClassRowId = p_objXmlDoc.SelectSingleNode("//ClassRowId").InnerText;

				objDisClass = (DataModel.DisabilityClass)m_objDataModelFactory.GetDataModelObject("DisabilityClass", false);
				objDisClass.MoveTo(Conversion.ConvertStrToInteger(sClassRowId));

				objNode = p_objXmlDoc.SelectSingleNode("//AltWaitPeriods/List");  

				foreach (DisClassWait objWait in objDisClass.WaitList)
				{
					objLstRow = p_objXmlDoc.CreateElement("listrow");

					// Wait Row ID
					objRowTxt = p_objXmlDoc.CreateElement("WaitRowId");
					objRowTxt.InnerText = objWait.WaitRowId.ToString();
					objLstRow.AppendChild(objRowTxt);

					// Quantity
					objRowTxt = p_objXmlDoc.CreateElement("Quantity");
					objRowTxt.InnerText = objWait.DisWaitPrd.ToString();
					objLstRow.AppendChild(objRowTxt);

					// Period
					objRowTxt = p_objXmlDoc.CreateElement("Period");
					objRowTxt.InnerText = objCache.GetCodeDesc(objWait.DisClndrWrkCode)
						+ " - " + objCache.GetCodeDesc(objWait.DisPrdType) ;
					objLstRow.AppendChild(objRowTxt);

					// Disablity Type
					objRowTxt = p_objXmlDoc.CreateElement("DisablityType");
					objRowTxt.InnerText = objCache.GetCodeDesc(objWait.DisTypeCode);
					objLstRow.AppendChild(objRowTxt);
					
					objNode.AppendChild((XmlNode)objLstRow);
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("AltWait.GetListAltWait.Error",m_iClientId), p_objEx);
			}
			finally
			{
				if(objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				if (objDisClass != null)
				{
					objDisClass.Dispose();
					objDisClass = null;
				}
				objLstRow = null;
				objRowTxt = null;
				objNode = null;
			}
			return p_objXmlDoc;
		}


		/// Name		: GetRecordAltWait
		/// Author		: Mihika Agrawal
		/// Date Created: 08/27/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the a particular record in Alternate Waiting Period for a Plan Class for editing
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document
		///		The structure of the input XML :
		///		<Document>
		///			<AltWaitPeriods>
		///				<ClassRowId />
		///				<DisWaitRowId />
		///			</AltWaitPeriods>
		///		</Document>
		/// </param>
		/// <returns>XML Document with data. Structure:
		///		<Document>
		///			<AltWaitPeriods>
		///				<ClassRowId />
		///				<DisWaitRowId />
		///				<DisWaitPrd />
		///				<DisClndrWrkCode />
		///				<DisPrdType/>
		///				<DisTypeCode/>
		///			</AltWaitPeriods>
		///		</Document>
		/// </returns>
		public XmlDocument GetRecordAltWait(XmlDocument p_objXmlDoc)
		{
			string sClassRowId = string.Empty;
			string sDisWaitRowId = string.Empty;
			string sSQL = string.Empty;

			DisabilityClass objDisClass = null;
			LocalCache objCache = null;
			
			try
			{
				sClassRowId = p_objXmlDoc.SelectSingleNode("//control[@name='hdnclassid']").InnerText;
				sDisWaitRowId = p_objXmlDoc.SelectSingleNode("//control[@name='DisWaitRowId']").InnerText;

				if (sClassRowId != "" && sDisWaitRowId != "")
				{
					objDisClass = (DataModel.DisabilityClass)m_objDataModelFactory.GetDataModelObject("DisabilityClass", false);
					objDisClass.MoveTo(Conversion.ConvertStrToInteger(sClassRowId));

                    objCache = new LocalCache(m_sConnectionString, m_iClientId);

					foreach (DisClassWait objWait in objDisClass.WaitList)
					{
						if (objWait.WaitRowId == Conversion.ConvertStrToInteger(sDisWaitRowId))
						{
							// Disability Wait Period
							p_objXmlDoc.SelectSingleNode("//control[@name='DisWaitPrd']").InnerText = objWait.DisWaitPrd.ToString();

							// Disability Calender Work Code
							p_objXmlDoc.SelectSingleNode("//control[@name='DisClndrWrkCode']").InnerText 
								= objCache.GetShortCode(objWait.DisClndrWrkCode) + " " + objCache.GetCodeDesc(objWait.DisClndrWrkCode);
							p_objXmlDoc.SelectSingleNode("//control[@name='DisClndrWrkCode']").Attributes.GetNamedItem("codeid").InnerText 
								= objWait.DisClndrWrkCode.ToString();

							// Disability Period Type
							p_objXmlDoc.SelectSingleNode("//control[@name='DisPrdType']").InnerText 
								= objCache.GetShortCode(objWait.DisPrdType) + " " + objCache.GetCodeDesc(objWait.DisPrdType);
							p_objXmlDoc.SelectSingleNode("//control[@name='DisPrdType']").Attributes.GetNamedItem("codeid").InnerText 
								= objWait.DisPrdType.ToString();

							// Disability Type Code
                            if (objWait.DisTypeCode != 0)
                            {
                                p_objXmlDoc.SelectSingleNode("//control[@name='DisTypeCode']").InnerText
                                    = objCache.GetShortCode(objWait.DisTypeCode) + " " + objCache.GetCodeDesc(objWait.DisTypeCode);
                            }
                            else
                            {
                                p_objXmlDoc.SelectSingleNode("//control[@name='DisTypeCode']").InnerText = "";
                            }
						    p_objXmlDoc.SelectSingleNode("//control[@name='DisTypeCode']").Attributes.GetNamedItem("codeid").InnerText 
								= objWait.DisTypeCode.ToString();
						}
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AltWait.GetRecordAltWait.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objDisClass != null)
				{
					objDisClass.Dispose();
					objDisClass = null;
				}
				if (objCache != null)
				{
					objCache.Dispose();
					objCache = null;
				}
			}
			return p_objXmlDoc;
		}


		/// Name		: Save
		/// Author		: Mihika Agrawal
		/// Date Created: 08/27/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Inserts/Updates a record depending upon the value of DisWaitRowId(zero or non zero)
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document
		///		The structure of the input XML :
		///		<Document>
		///			<AltWaitPeriods>
		///				<ClassRowId />
		///				<DisWaitRowId />
		///				<DisWaitPrd />
		///				<DisClndrWrkCode />
		///				<DisPrdType/>
		///				<DisTypeCode/>
		///			</AltWaitPeriods>
		///		</Document>
		/// </param>
		public void Save(XmlDocument p_objXmlDoc)
		{
			string sDisWaitRowId = string.Empty;
			string sClassId = string.Empty;

			DisabilityClass objDisClass = null;
//			DisClassWait objDisClassWait = null;
			XmlElement objElm = null;

			try
			{
				sDisWaitRowId = p_objXmlDoc.SelectSingleNode("//control[@name='DisWaitRowId']").InnerText;
				sClassId = p_objXmlDoc.SelectSingleNode("//control[@name='hdnclassid']").InnerText;

				objDisClass = (DataModel.DisabilityClass)m_objDataModelFactory.GetDataModelObject("DisabilityClass", false);
				objDisClass.MoveTo(Conversion.ConvertStrToInteger(sClassId));

				if (sDisWaitRowId == "" || sDisWaitRowId == "0")
				{
					DisClassWait objDisClassWait = objDisClass.WaitList.AddNew();
					//sDisWaitRowId = Utilities.GetNextUID(m_sConnectionString, "DIS_CLASS_WAIT").ToString();
                    sDisWaitRowId = "-1";

					objDisClassWait.WaitRowId = Conversion.ConvertStrToInteger(sDisWaitRowId);
					objDisClassWait.DisWaitPrd = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//control[@name='DisWaitPrd']").InnerText);

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DisClndrWrkCode']");
					objDisClassWait.DisClndrWrkCode = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DisPrdType']");
					objDisClassWait.DisPrdType = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DisTypeCode']");
					objDisClassWait.DisTypeCode = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));

					objDisClass.WaitList.Add(objDisClassWait);
					objDisClass.Save();
				}
				else
				{
					foreach(DisClassWait objDisClassWait in objDisClass.WaitList)
					{
						if (objDisClassWait.WaitRowId == Conversion.ConvertStrToInteger(sDisWaitRowId))
						{
							objDisClassWait.DisWaitPrd = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//control[@name='DisWaitPrd']").InnerText);

							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DisClndrWrkCode']");
							objDisClassWait.DisClndrWrkCode = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));

							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DisPrdType']");
							objDisClassWait.DisPrdType = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));

							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DisTypeCode']");
							objDisClassWait.DisTypeCode = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));

							objDisClass.Save();
						}
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AltWait.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objDisClass != null)
				{
					objDisClass.Dispose();
					objDisClass = null;
				}
			}
		}


		/// Name		: Delete
		/// Author		: Mihika Agrawal
		/// Date Created: 08/27/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delets a record for the 'Alternate Waiting Period' corresponding to the input row id
		/// </summary>
		/// <param name="p_DisWaitRowId">Row Id of the record to be deleted</param>
		/// <param name="p_iClassRowId">Clas id of the Disability class</param>
		public void Delete(int p_iDisWaitRowId, int p_iClassRowId)
		{
			DisabilityClass objDisClass = null;
			
			try
			{
				objDisClass = (DataModel.DisabilityClass)m_objDataModelFactory.GetDataModelObject("DisabilityClass", false);
				objDisClass.MoveTo(p_iClassRowId);
                DisClassWait objWait = (DataModel.DisClassWait)m_objDataModelFactory.GetDataModelObject("DisClassWait", false);
                objWait.MoveTo(p_iDisWaitRowId);
                objWait.Delete();
                objDisClass.WaitList.Delete(p_iDisWaitRowId);
				objDisClass.Save();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AltWait.Delete.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objDisClass != null)
				{
					objDisClass.Dispose();
					objDisClass = null;
				}
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("AltWait.Initialize.Error", m_iClientId), p_objEx);				
			}			
		}

		#endregion
	}
}
