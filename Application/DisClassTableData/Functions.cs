using System;
using Riskmaster.DataModel ;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.AltWaitPeriods
{
	/// <summary>
	/// Summary description for Functions.
	/// </summary>
	public class Functions
	{
		#region Constructors
		
		#region Variable Declaration 
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;		
		/// <summary>
		/// Private variable to store the DataBase Type
		/// </summary>
		internal static string m_sDBType = "";

		#endregion 

		internal Functions( DataModelFactory p_objDataModelFactory )
		{
			m_objDataModelFactory = p_objDataModelFactory ;	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString ;						
		}

		#endregion 

		#region Common XML functions

        internal static void StartDocument(ref XmlDocument objXmlDocument, ref XmlElement p_objRootNode, string p_sRootNodeName, int p_iClientId)
		{
			try
			{
				objXmlDocument = new XmlDocument();
				p_objRootNode = objXmlDocument.CreateElement( p_sRootNodeName );
				objXmlDocument.AppendChild( p_objRootNode );				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", p_iClientId), p_objEx);				
			}
		}


        internal static void CreateElement(XmlElement p_objParentNode, string p_sNodeName, int p_iClientId)
		{
			XmlElement objChildNode = null ;
			try
			{
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("TODO",p_iClientId) , p_objEx );
			}
			finally
			{
				objChildNode = null ;
			}

		}


        internal static void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode, int p_iClientId)
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("TODO",p_iClientId) , p_objEx );
			}

		}


        internal static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, int p_iClientId)
		{
			try
			{
				XmlElement objChildNode = null ;
                Functions.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode, p_iClientId);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("TODO",p_iClientId) , p_objEx );
			}
		}


        internal static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode, int p_iClientId)
		{
			try
			{
				CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode,p_iClientId );
				p_objChildNode.InnerText = p_sText ;			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("TODO",p_iClientId) , p_objEx );
			}
		}
			
		#endregion 

	}
}
