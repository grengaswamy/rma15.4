using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.SupportScreens
{
	///************************************************************** 
	///* $File				: DupClaimList.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 01-05-2005
	///* $Author			: Mihika Agrawal
	///***************************************************************	
	/// <summary>	
	///	This class lists the possible Duplicate Claims for a new claim
	/// </summary>
	public class DupClaimList
	{
		#region Constructor
		// Default Constructor
		public DupClaimList()
		{			
		}

		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		public DupClaimList(string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientID = p_iClientId;//rkaur27
		}
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;

        private int m_iClientID = 0;//rkaur27
		#endregion

		#region Public Methods
		/// <summary>
		///		This function creates a list of the duplicate claims
		/// </summary>
		/// <param name="p_objXmlDoc">XML Document containing the claim ids of the duplicate claims</param>
		/// <returns>XML Document containing list of duplicate claims</returns>
		public XmlDocument GetDupClaimList(XmlDocument p_objXmlDoc)
		{
			XmlElement objRootElm = null;
			XmlElement objRowElm = null;

			Claim objClaim = null;
			Event objEvent = null;
			DbReader objReader = null;

			string sSQL = "";			
			string sClaimIds = string.Empty;
			string [] arrCodes = {""};
			DataModelFactory objDataModelFactory = null;
            string sPolicyName = string.Empty;//RMA-12047
			try
			{
				sClaimIds = p_objXmlDoc.SelectSingleNode("//Dupes/ClaimIds").InnerText;
                sPolicyName = p_objXmlDoc.SelectSingleNode("//Dupes/PolicyName").InnerText;
				if(sClaimIds.Trim() != "")
				{
					arrCodes = new string[1];
					if(sClaimIds.IndexOf(",") > 0)
						arrCodes = sClaimIds.Split(',');
					else
						arrCodes[0] = sClaimIds;

					this.Initialize();
					objRootElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Dupes");

					objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword , m_iClientID);//rkaur27	
					
					objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);

					for(int i = 0; i < arrCodes.Length; i++)  
					{
						int iClaimantRowId = 0;
						objClaim.MoveTo(Conversion.ConvertStrToInteger(arrCodes[i].Trim()));
						objEvent = objClaim.Parent as Event;

						sSQL = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIM_ID = " + Conversion.ConvertStrToLong(arrCodes[i].Trim());
						objReader = DbFactory.GetDbReader(objDataModelFactory.Context.DbConn.ConnectionString,sSQL);
						
						objRowElm = p_objXmlDoc.CreateElement("claim");

						// Claim ID
						objRowElm.SetAttribute("claimid", objClaim.ClaimId.ToString());

						// Claim Number
						objRowElm.SetAttribute("ClaimNumber", objClaim.ClaimNumber);

						// Claim Status
						objRowElm.SetAttribute("ClaimStatus", objDataModelFactory.Context.LocalCache.GetCodeDesc(objClaim.ClaimStatusCode));
						
						//-- ABhateja, 07.18.2006, START
						//-- Gaps, MITS 5979
						
						// Claimant
						if(objReader.Read())
						{
							iClaimantRowId = objReader.GetInt("CLAIMANT_ROW_ID");
							objRowElm.SetAttribute("Claimant", (objClaim.ClaimantList[iClaimantRowId].ClaimantEntity.FirstName + " " + objClaim.ClaimantList[iClaimantRowId].ClaimantEntity.LastName));
						}
						else
							objRowElm.SetAttribute("Claimant", "[none]");
						
						//-- ABhateja, 07.18.2006, END
						//-- Gaps, MITS 5979

						// Event Number 
						objRowElm.SetAttribute("EventNumber", objEvent.EventNumber);

						// Event Description
						objRowElm.SetAttribute("EventDesc", objEvent.EventDescription);

						// Claim Date
						objRowElm.SetAttribute("ClaimDate", Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString());
						//RMA-12047
                        objRowElm.SetAttribute("EventDate", Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString());

                        objRowElm.SetAttribute("PolicyName", sPolicyName);
						//RMA-12047
						objRootElm.AppendChild(objRowElm);
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("DupClaimList.GetDupClaimList.Error", m_iClientID), p_objEx);//rkaur27
			}
			finally
			{
				if (objClaim != null)
				{
					objClaim.Dispose();
					objClaim = null;
				}
				if (objEvent != null)
				{
					objEvent.Dispose();
					objEvent = null;
				}
				if (objDataModelFactory != null)
				{
					objDataModelFactory.UnInitialize();
					objDataModelFactory = null;
				}
                //Added by Shivendu
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();

                }
				objRootElm = null;
				objRowElm = null;
			}
			return p_objXmlDoc;
		}
		#endregion

		#region Private Methods

		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
//			try
//			{
//				m_objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword );	
//				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
//			}
//			catch(DataModelException p_objEx)
//			{
//				throw p_objEx ;
//			}
//			catch( RMAppException p_objEx )
//			{
//				throw p_objEx ;
//			}
//			catch( Exception p_objEx )
//			{
//				throw new RMAppException(Globalization.GetString("DupClaimList.Initialize.Error") , p_objEx );				
//			}			
		}

		#endregion
	}
}
