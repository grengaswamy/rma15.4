using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.SupportScreens
{
    //RMA-8753 nshah28 start

	/// <summary>	
	///	This class lists the possible Duplicate Address
	/// </summary>
	public class DupAddressList
	{
		#region Constructor
		//Default constructor
        public DupAddressList()
        {

        }

		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public DupAddressList(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientID = p_iClientId;//rkaur27
		}
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;

        private int m_iClientID = 0;//rkaur27
		#endregion

		#region Public Methods
		/// <summary>
		///		This function creates a list of the duplicate Address
		/// </summary>
		/// <param name="p_objXmlDoc">XML Document containing the address ids of the duplicate address</param>
		/// <returns>XML Document containing list of duplicate address</returns>
		public XmlDocument GetDupAddressList(XmlDocument p_objXmlDoc)
		{
			XmlElement objRootElm = null;
			XmlElement objRowElm = null;
            XmlElement objAddressElm = null;

			Address objAddress = null;
			Event objEvent = null;
			DbReader objReader = null;

            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;

			string sSQL = "";			
			string sAddressId = string.Empty;
			string [] arrCodes = {""};
			DataModelFactory objDataModelFactory = null;
            XmlElement objTmpElement = null;
            XmlElement objArgElement = null;

            try
            {
                sAddressId = p_objXmlDoc.SelectSingleNode("//Dupes/AddressId").InnerText;
                if (sAddressId.Trim() != "")
                {
                    arrCodes = new string[1];
                    if (sAddressId.IndexOf(",") > 0)
                        arrCodes = sAddressId.Split(',');
                    else
                        arrCodes[0] = sAddressId;

                    //this.Initialize();
                   // objRootElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Dupes");
                    if (p_objXmlDoc.SelectSingleNode("//Dupes") != null)
                    {
                        p_objXmlDoc.DocumentElement.RemoveChild(p_objXmlDoc.SelectSingleNode("//Dupes"));
                    }

                    objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientID);//rkaur27	

                    objAddress = (Address)objDataModelFactory.GetDataModelObject("Address", false);

                    for (int i = 0; i < arrCodes.Length; i++)
                    {
                        //int iClaimantRowId = 0;
                        objAddress.MoveTo(Conversion.ConvertStrToInteger(arrCodes[i].Trim()));
                        objEvent = objAddress.Parent as Event;

                        objRootElm = p_objXmlDoc.CreateElement("Dupes");
                        p_objXmlDoc.DocumentElement.AppendChild(objRootElm);

                        objArgElement = p_objXmlDoc.CreateElement("AddressId");
                        objArgElement.InnerText = Conversion.ConvertObjToStr(objAddress.AddressId);
                        objRootElm.AppendChild(objArgElement);

                        //Addr1
                          objArgElement = p_objXmlDoc.CreateElement("Addr1");
                          objArgElement.InnerText = objAddress.Addr1;
                          objRootElm.AppendChild(objArgElement);

                          objArgElement = p_objXmlDoc.CreateElement("Addr2");
                          objArgElement.InnerText = objAddress.Addr2;
                          objRootElm.AppendChild(objArgElement);

                          objArgElement = p_objXmlDoc.CreateElement("Addr3");
                          objArgElement.InnerText = objAddress.Addr3;
                          objRootElm.AppendChild(objArgElement);

                          objArgElement = p_objXmlDoc.CreateElement("Addr4");
                          objArgElement.InnerText = objAddress.Addr4;
                          objRootElm.AppendChild(objArgElement);

                          objArgElement = p_objXmlDoc.CreateElement("City");
                          objArgElement.InnerText = objAddress.City;
                          objRootElm.AppendChild(objArgElement);

                          objArgElement = p_objXmlDoc.CreateElement("County");
                          objArgElement.InnerText = objAddress.County;
                          objRootElm.AppendChild(objArgElement);

                          objArgElement = p_objXmlDoc.CreateElement("ZipCode");
                          objArgElement.InnerText = objAddress.ZipCode;
                          objRootElm.AppendChild(objArgElement);

                          objArgElement = p_objXmlDoc.CreateElement("Country");
                          objArgElement.InnerText = objDataModelFactory.Context.LocalCache.GetCodeDesc(objAddress.Country);
                          objRootElm.AppendChild(objArgElement);

                          objDataModelFactory.Context.LocalCache.GetStateInfo(objAddress.State, ref sStateCode, ref sStateDesc);
                          objArgElement = p_objXmlDoc.CreateElement("State");
                          objArgElement.InnerText = sStateDesc;
                          objRootElm.AppendChild(objArgElement);

                    }


                }
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DupAddressList.GetDupAddressList.Error", m_iClientID), p_objEx);
            }
			finally
			{
                if (objAddress != null)
				{
                    objAddress.Dispose();
                    objAddress = null;
				}
				if (objEvent != null)
				{
					objEvent.Dispose();
					objEvent = null;
				}
				if (objDataModelFactory != null)
				{
					objDataModelFactory.UnInitialize();
					objDataModelFactory = null;
				}
                
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();

                }
				objRootElm = null;
				objRowElm = null;
                objAddressElm = null;
			}
			return p_objXmlDoc;
		}
		#endregion

		
	}
}
