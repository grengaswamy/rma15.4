﻿using System;
using System.Collections;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Data;

namespace Riskmaster.Application.SupportScreens
{
    /// <summary>
    /// Author  :   Gurpreet Singh Bindra
    /// Dated   :   15th, Apr 2014
    /// Purpose :   MITS#35365 to handle the person involved MDI menu.
    /// </summary>
    /// <returns></returns>
    public class PersonInvMenu
    {
        #region Variable Declaration
        string m_ConnectionString = string.Empty;
        int m_UserId = 0;
        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;

        private struct PersonInvMenuConfig
        {
            internal int iAddExistingEmployee;
            internal int iAddNewEmployee;
            internal int iAddExistingMedicalStaff;
            internal int iAddNewMedicalStaff;
            internal int iAddExistingOtherPerson;
            internal int iAddNewOtherPerson;
            internal int iAddExistingDriver;
            internal int iAddNewDriver;
            internal int iAddExistingPatient;
            internal int iAddNewPatient;
            internal int iAddExistingPhysician;
            internal int iAddNewPhysician;
            internal int iAddExistingWitness;
            internal int iAddNewWitness;

            internal string GetPersonInvSettingStructValue(string sFieldName)
            {
                string sValue = string.Empty;

                switch (sFieldName)
                {
                    case "AddExistingEmployee":
                        sValue = Conversion.ConvertObjToStr(this.iAddExistingEmployee);
                        break;
                    case "AddNewEmployee":
                        sValue = Conversion.ConvertObjToStr(this.iAddNewEmployee);
                        break;
                    case "AddExistingMedicalStaff":
                        sValue = Conversion.ConvertObjToStr(this.iAddExistingMedicalStaff);
                        break;
                    case "AddNewMedicalStaff":
                        sValue = Conversion.ConvertObjToStr(this.iAddNewMedicalStaff);
                        break;
                    case "AddExistingOtherPerson":
                        sValue = Conversion.ConvertObjToStr(this.iAddExistingOtherPerson);
                        break;
                    case "AddNewOtherPerson":
                        sValue = Conversion.ConvertObjToStr(this.iAddNewOtherPerson);
                        break;
                    case "AddExistingDriver":
                        sValue = Conversion.ConvertObjToStr(this.iAddExistingDriver);
                        break;
                    case "AddNewDriver":
                        sValue = Conversion.ConvertObjToStr(this.iAddNewDriver);
                        break;
                    case "AddExistingPatient":
                        sValue = Conversion.ConvertObjToStr(this.iAddExistingPatient);
                        break;
                    case "AddNewPatient":
                        sValue = Conversion.ConvertObjToStr(this.iAddNewPatient);
                        break;
                    case "AddExistingPhysician":
                        sValue = Conversion.ConvertObjToStr(this.iAddExistingPhysician);
                        break;
                    case "AddNewPhysician":
                        sValue = Conversion.ConvertObjToStr(this.iAddNewPhysician);
                        break;
                    case "AddExistingWitness":
                        sValue = Conversion.ConvertObjToStr(this.iAddExistingWitness);
                        break;
                    case "AddNewWitness":
                        sValue = Conversion.ConvertObjToStr(this.iAddNewWitness);
                        break;
                }

                return sValue;
            }
        }

        private PersonInvMenuConfig m_PersonInvMenuConfig;

        private Hashtable arrSystemFields;

        #endregion

        #region constructor

        /// <summary>
        /// Base constructor to create hashtable
        /// </summary>
        public PersonInvMenu()
        {
            arrSystemFields = new Hashtable();
            arrSystemFields.Add("ADD_EXIS_EMP", "AddExistingEmployee");
            arrSystemFields.Add("ADD_NEW_EMP", "AddNewEmployee");
            arrSystemFields.Add("ADD_EXIS_MEDSTAFF", "AddExistingMedicalStaff");
            arrSystemFields.Add("ADD_NEW_MEDSTAFF", "AddNewMedicalStaff");
            arrSystemFields.Add("ADD_EXIS_OTHPRSN", "AddExistingOtherPerson");
            arrSystemFields.Add("ADD_NEW_OTHPRSN", "AddNewOtherPerson");
            arrSystemFields.Add("ADD_EXIS_DRIVER", "AddExistingDriver");
            arrSystemFields.Add("ADD_NEW_DRIVER", "AddNewDriver");
            arrSystemFields.Add("ADD_EXIS_PATIENT", "AddExistingPatient");
            arrSystemFields.Add("ADD_NEW_PATIENT", "AddNewPatient");
            arrSystemFields.Add("ADD_EXIS_PHYSICIAN", "AddExistingPhysician");
            arrSystemFields.Add("ADD_NEW_PHYSICIAN", "AddNewPhysician");
            arrSystemFields.Add("ADD_EXIS_WITNESS", "AddExistingWitness");
            arrSystemFields.Add("ADD_NEW_WITNESS", "AddNewWitness");
        }

        /// <summary>
        /// Constructor to call base constructor and also assign connection string.
        /// </summary>
        /// <param name="sConnectionString">ConnectionString</param>
        public PersonInvMenu(string sConnectionString,int p_iClientId) : this()
        {
            m_ConnectionString = sConnectionString;
            m_iClientId = p_iClientId;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get Value of the Node.
        /// </summary>
        /// <param name="p_objDocument">Input document.</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <returns>Node Inner Text.</returns>
        private int GetValue(XmlDocument p_objDocument, string p_sNodeName)
        {
            XmlElement objNode = null;
            int iValue = default(int);
            bool sValue = false;
            bool bSuccess = false;

            try
            {
                objNode = (XmlElement)p_objDocument.SelectSingleNode("//" + p_sNodeName);

                if (objNode != null)
                    sValue = Common.Conversion.CastToType<bool>(objNode.InnerText, out bSuccess);
                
                iValue = bSuccess ? (sValue ? -1 : 0) : 0;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PersonInvMenu.GetValue.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
            return (iValue);
        }

        /// <summary>
        /// saves the user Prference to db
        /// </summary>
        /// <param name="p_objPersInvMenuConfig"></param>
        private void SaveUserPreferToDb(PersonInvMenuConfig p_objPersInvMenuConfig)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            SysSettings objSettings = null;
            string sSQL = string.Empty;
            int iInternalRowId = 1;

            try
            {
                objSettings = new SysSettings(m_ConnectionString, m_iClientId);
                //Try to delete the old one.
                sSQL = "DELETE FROM PI_CONFIG_MENU ";
                objConn = DbFactory.GetDbConnection(m_ConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.Close();
                objWriter = DbFactory.GetDbWriter(m_ConnectionString);
                foreach (string sParamValue in arrSystemFields.Keys)
                {
                    objWriter.Tables.Add("PI_CONFIG_MENU");
                    objWriter.Fields.Add("PI_CONFIG_PARM_NAME", sParamValue);
                    objWriter.Fields.Add("STR_PARM_VALUE", p_objPersInvMenuConfig.GetPersonInvSettingStructValue(arrSystemFields[sParamValue].ToString()));
                    objWriter.Fields.Add("ROW_ID", iInternalRowId++);

                    objWriter.Execute();
                    objWriter.Reset(true);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PersonInvMenu.SaveUserPreferToDb.Err", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
                
                p_objPersInvMenuConfig = default(PersonInvMenuConfig);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///  This method is used to update the user selected values in the DB
        /// </summary>
        /// <param name="p_objXmlIn"> The input XML containing all the setting details.</param>
        /// <returns></returns>
        public XmlDocument SetPersonInvMenuConfig(XmlDocument p_objXmlIn)
        {
            bool bSuccess = false;
            XmlDocument objXmlDoc = new XmlDocument();
            XmlElement objTempNode = null;
            m_PersonInvMenuConfig = default(PersonInvMenuConfig);
            
            try
            {
                m_PersonInvMenuConfig.iAddExistingEmployee = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddExistingEmployee");
                m_PersonInvMenuConfig.iAddNewEmployee = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddNewEmployee");
                m_PersonInvMenuConfig.iAddExistingMedicalStaff = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddExistingMedicalStaff");
                m_PersonInvMenuConfig.iAddNewMedicalStaff = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddNewMedicalStaff");
                m_PersonInvMenuConfig.iAddExistingOtherPerson = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddExistingOtherPerson");
                m_PersonInvMenuConfig.iAddNewOtherPerson = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddNewOtherPerson");
                m_PersonInvMenuConfig.iAddExistingDriver = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddExistingDriver");
                m_PersonInvMenuConfig.iAddNewDriver = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddNewDriver");
                m_PersonInvMenuConfig.iAddExistingPatient = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddExistingPatient");
                m_PersonInvMenuConfig.iAddNewPatient = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddNewPatient");
                m_PersonInvMenuConfig.iAddExistingPhysician = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddExistingPhysician");
                m_PersonInvMenuConfig.iAddNewPhysician = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddNewPhysician");
                m_PersonInvMenuConfig.iAddExistingWitness = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddExistingWitness");
                m_PersonInvMenuConfig.iAddNewWitness = this.GetValue(p_objXmlIn, "PersonInvMenuConfig/AddNewWitness");

                SaveUserPreferToDb(m_PersonInvMenuConfig);

                objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("/PersonInvMenuConfig");
                if (objTempNode != null)
                {
                    objTempNode = objXmlDoc.CreateElement("Success");
                    objXmlDoc.AppendChild(objTempNode);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PersonInvMenu.SetPersonInvMenuConfig.Err", m_iClientId), p_objException); 
            }
            finally
            {
                if (objTempNode != null)
                {
                    objTempNode = null;
                }
            }

            return objXmlDoc;
        }

        /// <summary>
        /// This function returns the Person involved settings saved in the DB to display on the screen.
        /// </summary>
        /// <param name="p_objXmlIn"> Input XML</param>
        /// <returns> XML document with all the settings.</returns>
        public XmlDocument GetPersonInvMenuConfig(XmlDocument p_objXmlIn)
        {
            XmlElement objRootNode = null;
            string sKey = string.Empty;
            string sValue = string.Empty;
            DataTable objDataTable = null;
            DbReader objDBrdr = null;
            
            try
            {
                using (objDBrdr = DbFactory.GetDbReader(m_ConnectionString, "SELECT * FROM PI_CONFIG_MENU "))
                {
                    objDataTable = objDBrdr.GetSchemaTable();
                    DataRow[] objDataRows = null;

                    if (objDBrdr != null)
                    {
                        while (objDBrdr.Read())
                        {
                            objDataRows = objDataTable.Select("ColumnName='PI_CONFIG_PARM_NAME'");
                            if (objDataRows.GetLength(0) == 1)
                                sKey = objDBrdr.GetString("PI_CONFIG_PARM_NAME");

                            objDataRows = objDataTable.Select("ColumnName='STR_PARM_VALUE'");
                            if (objDataRows.GetLength(0) == 1)
                                sValue = Conversion.ConvertObjToStr(objDBrdr.GetInt32("STR_PARM_VALUE"));

                            objRootNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PersonInvMenuConfig/" + arrSystemFields[sKey].ToString());
                            if (objRootNode != null)
                            {
                                objRootNode.InnerText = (string.Compare(sValue, "-1") == 0) ? "True" : "False";
                            }
                        }

                        objDBrdr.Close();
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PersonInvMenu.GetPersonInvMenuConfig.Err", m_iClientId), p_objException);
            }
            finally
            {
                objRootNode = null;
                if (objDataTable != null)
                    objDataTable.Dispose();
            }

            return p_objXmlIn;

        }

        #endregion
    }
}
