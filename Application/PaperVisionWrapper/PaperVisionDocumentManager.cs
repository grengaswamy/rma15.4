﻿
using System;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using Riskmaster.Security;
using Riskmaster.Models;
using System.IO;
using System.Xml;
using System.Data;
using System.Collections;


namespace Riskmaster.Application.PaperVisionWrapper
{
    ///************************************************************** 
    ///* $File		: PaperVisionDocumentManager.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 26-May-2009
    ///* $Author	: Animesh Sahai
    ///* $Comment	: 
    ///* $Source	: 
    /// <summary>	
    /// Manages all operations related to documents, attachments for papervision 
    /// </summary>
    public class PaperVisionDocumentManager
    {
        #region Constants
        /// <summary>
        /// Function IDs for User Document related security permission
        /// </summary>
        private const int USERDOC_SID = 50;
        private const int USERDOC_VIEW_SID = 51;
        private const int USERDOC_CREATE_SID = 80;
        private const int USERDOC_MODIFY_SID = 81;
        private const int USERDOC_DELETE_SID = 82;
        private const int USERDOC_VIEWPLAY_SID = 83;
        private const int USERDOC_PRINT_SID = 84;
        private const int USERDOC_DETATCH_SID = 85;
        private const int USERDOC_EXPORT_SID = 86;
        private const int USERDOC_TRANSFER_SID = 87;
        private const int USERDOC_COPY_SID = 88;

        private const int ATTDOC_ATTACHTO_SID = 8;
        private const int ATTDOC_DELETE_SID = 9;

        private const string FUNCNAME_COPY = "copy";
        private const string FUNCNAME_CREATE = "create";
        private const string FUNCNAME_DELETE = "delete";
        private const string FUNCNAME_DETATCH = "detach";
        private const string FUNCNAME_EXPORT = "export";
        private const string FUNCNAME_MODIFY = "modify";
        private const string FUNCNAME_PRINT = "print";
        private const string FUNCNAME_TRANSFER = "transfer";
        private const string FUNCNAME_VIEW = "view";
        private const string FUNCNAME_VIEWPLAY = "viewplay";
        private string m_sFormName = string.Empty;
        #endregion

        #region Member Variables
        /// <summary>		
        /// Database connection string
        /// </summary>
        private string m_sConnectionString;

        /// <summary>		
        /// User Login Name
        /// </summary>
        private string m_sUserLoginName;

        /// <summary>		
        /// Security Id
        /// </summary>
        private long m_lSecurityId;

        /// <summary>		
        /// Table name to which documents is attached.
        /// </summary>
        private string m_sTableName;

        /// <summary>		
        /// Destination Storage Path. For database storage
        /// it is the connection string, and for file storage
        /// it is the folder path.
        /// </summary>
        private string m_sDestinationStoragePath;

        /// <summary>		
        /// m_userLogin: Passed from Adapter and used to check permission 
        /// </summary>
        protected UserLogin m_userLogin;

        private int m_iStorageId;
        private int m_iClientId = 0;
        #endregion

        #region Properties
        /* All string properties are set to string.Empty if a
		 * null value is assigned to them. This ensures a more robust
		 * component free of NullValueExceptions generated inadvertantly
		 * if the client sets a string property to null. 
		 * */
        /// <summary>
        /// Database Connection String
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return m_sConnectionString;
            }
            set
            {
                if (value != null)
                {
                    m_sConnectionString = value;
                }
                else
                {
                    m_sConnectionString = string.Empty;
                }
            }
        }

        /// <summary>
        /// User Login Name
        /// </summary>
        public string UserLoginName
        {
            get
            {
                return m_sUserLoginName;
            }
            set
            {
                if (value != null)
                {
                    m_sUserLoginName = value;
                }
                else
                {
                    m_sUserLoginName = string.Empty;
                }
            }
        }

        /// <summary>
        /// Security Id
        /// </summary>
        public long SecurityId
        {
            get
            {
                return m_lSecurityId;
            }
            set
            {
                m_lSecurityId = value;
            }
        }

        /// <summary>
        /// Table name to which document is attached.
        /// </summary>
        public string TableName
        {
            get
            {
                return m_sTableName;
            }
            set
            {
                if (value != null)
                {
                    m_sTableName = value;
                }
                else
                {
                    m_sTableName = string.Empty;
                }
            }
        }
        /// <summary>
        /// This will track updating the stored document in database server. 
        /// </summary>
        public int StorageId
        {
            get
            {
                return m_iStorageId;
            }
            set
            {
                m_iStorageId = value;
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor: 
        /// </summary>
        /// <param name="oLogin"> Riskmaster.Security.UserLogin object</param>

        public PaperVisionDocumentManager(UserLogin oLogin,int p_iClientId)
        {
            m_sConnectionString = string.Empty;
            m_sUserLoginName = string.Empty;
            m_lSecurityId = 0;
            m_sTableName = string.Empty;
            m_userLogin = oLogin;
            m_iClientId = p_iClientId;

        }

        /// <summary>
        /// Constructor: 
        /// </summary>
        /// <param name="oLogin"> Riskmaster.Security.UserLogin object</param>
        /// <param name="p_lpsid"> Security Id</param>
        public PaperVisionDocumentManager(UserLogin oLogin, long p_lpsid,int p_iClientId)
        {
            m_sConnectionString = string.Empty;
            m_sUserLoginName = string.Empty;
            m_lSecurityId = p_lpsid;
            m_sTableName = string.Empty;
            m_userLogin = oLogin;
            m_iClientId = p_iClientId;
        }

        #endregion

        /// <summary>
        /// Enum for display order (order by clause)
        /// </summary>
        private enum DOCSORTORDER : int
        {
            NONE,
            ASC_NAME,
            ASC_SUBJECT,
            ASC_TYPE,
            ASC_CLASS,
            ASC_CATEGORY,
            ASC_CREATE_DATE,
            DEC_NAME,
            DEC_SUBJECT,
            DEC_TYPE,
            DEC_CLASS,
            DEC_CATEGORY,
            DEC_CREATE_DATE
        }

        #region Public Functions

        /// Name			: GetAttachments
        /// Author			: Animesh Sahai 
        /// Date Created	: 26-May-2009
        /// <summary>
        /// Get data for attachments in form of XML
        /// Uses TableName and ConnectionString property values.
        /// </summary>
        /// <param name="p_sFormName">
        ///		Form name set as an attribute in root element of output XML
        /// </param>
        /// <param name="p_lRecordId">
        ///		Specific record id to which the document is attached. 
        ///	</param>
        /// <param name="p_sAttachmentsXML">
        ///		Out parameter. Returns attachments data in form of XML
        ///	</param>
        /// <returns>0 - Success</returns> 	
        public int GetAttachmentsNew(string sUserName, string sPassword, int lSortId, string p_sFormName, long p_lRecordId, PVWrapper objPaperVision,
            out DocumentList p_objDocumentList, long p_lPageNumber)
        {
            int iReturnValue = 0;
            DataSet objDataSet = null;
            DataTable objDocumentTable = null;
            bool blnPVLoginSucceed;
            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;
            XmlElement objXmlElement = null;

            XmlDocument objXml = null;
            XmlNode objSessionDSN = null;
            string sSQL = string.Empty;
            int iSortOrder = 0;
            LocalCache objCache = null;
            DbReader objDbReader = null;
            string strClaimNumber = string.Empty;
            DataTable objPVDocTable = null;
            //Animesh Inserted MITS 20256 07/04/2010
            string strPVSortOrder = string.Empty;
            DataTable objFinalTable = null;
            //Animesh Insertion ends 07/04/2010

            long lDocRecordCount = 0;
            long lTotalRecordCount = 0;
            long lTotalPageCount = 0;
            long lPageSize = 10;
            long lPageStart = 0;
            long lPageEnd = 0;

            try
            {
                RaiseInitializationException();
                blnPVLoginSucceed = false;
                sSQL = "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = " + p_lRecordId;
                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                if (objDbReader.Read())
                {
                    strClaimNumber = Conversion.ConvertObjToStr(objDbReader.GetValue("CLAIM_NUMBER"));
                }
                objDbReader.Close();
                objDbReader = null;
                //Mona:Papervision : Added Filename
                sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT_NAME,"
                    + "DOCUMENT_FILENAME, "
                    + " DOCUMENT.ISAT_PV,DOCUMENT.PV_DOCID,DOCUMENT.KEYWORDS, "
                    + " DOCUMENT.CREATE_DATE,DOCUMENT.NOTES,"
                    + " DOCUMENT.USER_ID, (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID =DOCUMENT.DOCUMENT_CLASS) AS DOCUMENT_CLASS,"
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT.DOCUMENT_CATEGORY) AS DOCUMENT_CATEGORY,"
                    + " DOCUMENT.DOCUMENT_SUBJECT, "
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT.DOCUMENT_TYPE) AS DOCUMENT_TYPE, DOCUMENT.DOC_INTERNAL_TYPE, DOCUMENT.DOCUMENT_FILEPATH "
                    + " FROM DOCUMENT ,DOCUMENT_ATTACH ";
                if (this.TableName.Length == 0)
                {
                    sSQL = sSQL + " WHERE DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID AND ISAT_PV = -1 ";
                }
                else if (p_lRecordId == 0)
                {
                    sSQL = sSQL + " WHERE UPPER(DOCUMENT_ATTACH.TABLE_NAME)=UPPER('" + this.TableName + "')"
                    + " AND DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID AND ISAT_PV = -1 ";
                }
                else
                {
                    sSQL = sSQL + " WHERE UPPER(DOCUMENT_ATTACH.TABLE_NAME)=UPPER('" + this.TableName + "')"
                    + " AND DOCUMENT_ATTACH.RECORD_ID=" + p_lRecordId.ToString()
                    + " AND DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID AND ISAT_PV = -1 ";
                }
                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_NAME ASC";
                        //Animesh Insertion ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.ASC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_SUBJECT;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_SUBJECT ASC";
                        //Animesh Insertion Ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.ASC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_TYPE;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_TYPE ASC";
                        //Aniemsh Insertion ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.ASC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CLASS;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_CLASS ASC";
                        //Animesh Insertion Ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.ASC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CATEGORY;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_CATEGORY ASC";
                        //Animesh Insertion Ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.ASC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "CREATE_DATE ASC";
                        //Animesh Insertion Ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_NAME;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_NAME DESC";
                        //Animesh Insertion Ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.DEC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_SUBJECT;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_SUBJECT DESC";
                        //Animesh insertion ends 07/04/2010
                        break;
                    case (int)DOCSORTORDER.DEC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_TYPE;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_TYPE DESC";
                        //Animesh  insertion Ends
                        break;
                    case (int)DOCSORTORDER.DEC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CLASS;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_CLASS DESC";
                        //Animesh Insertion Ends
                        break;
                    case (int)DOCSORTORDER.DEC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CATEGORY;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "DOCUMENT_CATEGORY DESC";
                        //Animesh Insertion ends
                        break;
                    case (int)DOCSORTORDER.DEC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CREATE_DATE;
                        //Animesh Inserted MITS 20256 07/04/2010
                        strPVSortOrder = "CREATE_DATE DESC";
                        //Animesh Insertion Ends
                        break;
                    default:
                        //Bharani - MITS : 31951 - For maintaining uniformity in sorting during page load
                        //sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";                        
                        //iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        //lSortId = (int)DOCSORTORDER.ASC_NAME;
                        //strPVSortOrder = "DOCUMENT_NAME ASC";
                        sSQL = sSQL + " ORDER BY CREATE_DATE ASC";
                        lSortId = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        iSortOrder = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        strPVSortOrder = "CREATE_DATE ASC";
                        //Bharani - MITS : 31951 - For maintaining uniformity in sorting during page load
                        //Animesh Inserted MITS 20256 07/04/2010                        
                        
                        //Animesh Insertion ends
                        break;
                }
                //p_objDocumentList = new DocumentList();
                objDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL,m_iClientId);
                objDocumentTable = objDataSet.Tables[0];

                //Add by kuladeep for mits:26519 Start
                blnPVLoginSucceed = objPaperVision.ValidateRequiredVariables();
                if (blnPVLoginSucceed)
                    blnPVLoginSucceed = objPaperVision.LoginToPV(sUserName, sPassword);
                else
                    throw new RMAppException(Globalization.GetString("PaperVisionManager.ReqdParameters.Exception",m_iClientId));

                if (blnPVLoginSucceed)
                    objPVDocTable = objPaperVision.Search("CLAIM ID", strClaimNumber, strPVSortOrder);
                if (objPVDocTable.Rows.Count > 0)
                {
                    lDocRecordCount = objPVDocTable.Rows.Count;
                }            
                //getting the Document record count
                //if (objDataSet.Tables.Count > 0)
                //{
                //    lDocRecordCount = objDocumentTable.Rows.Count;
                //}
                //Add by kuladeep for mits:26519 End

                //getting the total record count
                lTotalRecordCount = lDocRecordCount;

                //getting the page count to be added to the attribute of the data element
                lTotalPageCount = lTotalRecordCount / lPageSize;
                if (lTotalRecordCount > lTotalPageCount * lPageSize)
                {
                    lTotalPageCount = lTotalPageCount + 1;
                }

                if (lTotalPageCount == 0)
                {
                    lTotalPageCount = 1;
                }

                p_objDocumentList = new DocumentList();
                p_objDocumentList.Name = "Attachments";
                p_objDocumentList.Pid = p_lRecordId.ToString();
                p_objDocumentList.Sid = this.SecurityId.ToString();
                p_objDocumentList.Documents = new List<DocumentType>();
                //check for permission, get psid from the request if it is available
                m_sFormName = p_sFormName;
                CheckDocumentListPermissions(ref p_objDocumentList, Convert.ToInt32(m_lSecurityId));

                if (p_lPageNumber > 1)
                {
                    p_objDocumentList.Previouspage = (p_lPageNumber - 1).ToString();
                    p_objDocumentList.Firstpage = "1";
                }
                if (p_lPageNumber < lTotalPageCount)
                {
                    if (p_lPageNumber == 0) { p_lPageNumber = 1; }
                    p_objDocumentList.Nextpage = ((int)p_lPageNumber + 1).ToString();
                    p_objDocumentList.Lastpage = lTotalPageCount.ToString();
                }
                if (p_lPageNumber > lTotalPageCount)
                {
                    p_objDocumentList.Pagenumber = lTotalPageCount.ToString();
                    p_lPageNumber = lTotalPageCount;
                }
                else
                {
                    p_objDocumentList.Pagenumber = p_lPageNumber.ToString();
                }
                p_objDocumentList.Pagecount = lTotalPageCount.ToString();
                p_objDocumentList.Orderby = iSortOrder.ToString();

                //objDocumentList.Folders.Add(objFolder);
                //getting the loop starting point and end point
                if (p_lPageNumber == 1)
                    lPageStart = 1;
                else
                    lPageStart = ((p_lPageNumber - 1) * lPageSize) + 1;

                lPageEnd = lPageStart + lPageSize - 1;
                if (lPageEnd > lTotalRecordCount)
                    lPageEnd = lTotalRecordCount;

                long lIndex = lPageStart;
                int iDocIndex = 0;


                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                //Comment by kuladeep for mits:26519 Start
                //blnPVLoginSucceed = objPaperVision.ValidateRequiredVariables();
                //if (blnPVLoginSucceed)
                //    blnPVLoginSucceed = objPaperVision.LoginToPV(sUserName, sPassword);
                //else
                //    throw new RMAppException(Globalization.GetString("PaperVisionManager.ReqdParameters.Exception"));
                //Comment by kuladeep for mits:26519 End

                /*Bharani - MITS : 31951 - Change 1 - Start
                if (blnPVLoginSucceed)
                {
                    //objPVDocTable = objPaperVision.Search("CLAIM ID", strClaimNumber, strPVSortOrder); //Comment by kuladeep for mits:26519 Start
                    if (objPVDocTable != null)
                    {
                        while (lIndex <= lPageEnd)
                        {
                            iDocIndex = (int)(lIndex - 1);

                            StringBuilder strFilterExp = new StringBuilder();
                            //DocumentType objDocument = new DocumentType();
                            DataView objView = new DataView(objDocumentTable);
                            strFilterExp.Append("PV_DOCID=");
                            strFilterExp.Append(Convert.ToString(objPVDocTable.Rows[iDocIndex]["DOCID"]));
                            objView.RowFilter = strFilterExp.ToString();
                            objView.RowStateFilter = DataViewRowState.CurrentRows;
                            DocumentType objDocument = new DocumentType();
                            if (objView.Count > 0)
                            {
                                objDocument.Title = objView[0].Row["DOCUMENT_NAME"].ToString();
                                objDocument.Keywords =  objView[0].Row["KEYWORDS"].ToString();
                                objDocument.CreateDate = Conversion.GetDBDateFormat(Convert.ToString(objView[0].Row["CREATE_DATE"].ToString()), "d");
                                objDocument.FileName =  objView[0].Row["DOCUMENT_FILENAME"].ToString();
                                objDocument.UserName = objView[0].Row["USER_ID"].ToString();
                                objDocument.Subject = objView[0].Row["DOCUMENT_SUBJECT"].ToString();
                                objDocument.DocumentsType = objView[0].Row["DOCUMENT_TYPE"].ToString();
                                objDocument.DocumentClass = objView[0].Row["DOCUMENT_CLASS"].ToString();
                                objDocument.DocumentCategory = objView[0].Row["DOCUMENT_CATEGORY"].ToString();
                                objDocument.Notes = objView[0].Row["NOTES"].ToString();
                                objDocument.Pid = objView[0].Row["DOCUMENT_ID"].ToString();
                                objDocument.UserId = objView[0].Row["USER_ID"].ToString();

                            }
                            else
                            {
                                int iNewDocID = 0;
                                PaperVisionDocument objPVDoc = new PaperVisionDocument();
                                objPVDoc.ConnectionString = this.ConnectionString;
                                objPVDoc.Title = objPVDocTable.Rows[iDocIndex]["DOCUMENT DESC"].ToString();
                                objPVDoc.IsAtPaperVision = -1;
                                //objPVDoc.CreateDate = Conversion.GetDate(objRow["DOCUMENT DATE"].ToString()); //Animesh Modified the code //MITS 19654
                                objPVDoc.CreateDate = Conversion.GetDBDateFormat(Conversion.GetDate(objPVDocTable.Rows[iDocIndex]["DOCUMENT DATE"].ToString()), "MM/dd/yyyy");
                                objPVDoc.Type = Convert.ToInt64(objCache.GetCodeId(objPVDocTable.Rows[iDocIndex]["DOCUMENT TYPE"].ToString(), "DOCUMENT_TYPE"));
                                objPVDoc.AttachRecordId = p_lRecordId;
                                objPVDoc.AttachTable = "CLAIM";
                                objPVDoc.DocumentId = 0;
                                objPVDoc.PVDocumentId = Convert.ToInt64(objPVDocTable.Rows[iDocIndex]["DOCID"].ToString());
                                objPVDoc.Notes = "Document uploaded through PaperVision.";
                                //Animesh Inserted MITS 20256 08/04/2010
                                objPVDoc.FileName = "File uploaded through Papervision";
                                //Animesh Insertion Ends
                                objPVDoc.UserLoginName = m_userLogin.LoginName;
                                iNewDocID = objPVDoc.Update();


                                objDocument.Title = objPVDocTable.Rows[iDocIndex]["DOCUMENT DESC"].ToString();
                                objDocument.Keywords = "";
                                objDocument.CreateDate = Conversion.GetDate(objPVDocTable.Rows[iDocIndex]["DOCUMENT DATE"].ToString());
                                objDocument.FileName = ""; //Mona:PaperVision
                                objDocument.UserName = "";
                                objDocument.Subject = "";
                                objDocument.DocumentsType = objCache.GetCodeDesc(objCache.GetCodeId(objPVDocTable.Rows[iDocIndex]["DOCUMENT TYPE"].ToString(), "DOCUMENT_TYPE"));
                                objDocument.DocumentClass = "";
                                objDocument.DocumentCategory = "";
                                objDocument.Notes = "Document Loaded through PaperVision.";
                                objDocument.DocumentId = Convert.ToInt32(objPVDoc.DocumentId);
                                objDocument.UserId = m_userLogin.LoginName;
                            }

                            p_objDocumentList.Documents.Add(objDocument);
                            strFilterExp = null;
                            objView.Dispose();
                            lIndex = lIndex + 1;
                        }


                        p_objDocumentList.Email_allowed = "0";
                    }  
                }
                Bharani - MITS : 31951 - Change 1 - End */
                //Bharani - MITS : 31951 - Change 2 - Start
                DataTable obNewDt = objDocumentTable.Clone();
                if (blnPVLoginSucceed)
                {
                    //objPVDocTable = objPaperVision.Search("CLAIM ID", strClaimNumber, strPVSortOrder); //Comment by kuladeep for mits:26519 Start
                    if (objPVDocTable != null)
                    {
                        while (lIndex <= lPageEnd)
                        {
                            iDocIndex = (int)(lIndex - 1);

                            StringBuilder strFilterExp = new StringBuilder();
                            DataView objView = new DataView(objDocumentTable);
                            strFilterExp.Append("PV_DOCID=");
                            strFilterExp.Append(Convert.ToString(objPVDocTable.Rows[iDocIndex]["DOCID"]));
                            objView.RowFilter = strFilterExp.ToString();
                            objView.RowStateFilter = DataViewRowState.CurrentRows;
                            if (objView.Count > 0)
                            {
                                IEnumerator viewEnumerator = objView.GetEnumerator();
                                while (viewEnumerator.MoveNext())
                                {                                    
                                    DataRowView drv = (DataRowView)viewEnumerator.Current;
                                    DataRow dr = obNewDt.NewRow();
                                    foreach (DataColumn col in obNewDt.Columns)
                                    {
                                        dr[col.ToString()] = drv[col.ToString()];
                                    }
                                    obNewDt.Rows.Add(dr);
                                    break;
                                }
                            }
                            else
                            {
                                #region else part of previous code
                                DocumentType objDocument = new DocumentType();
                                int iNewDocID = 0;
                                PaperVisionDocument objPVDoc = new PaperVisionDocument(m_iClientId);
                                objPVDoc.ConnectionString = this.ConnectionString;
                                objPVDoc.Title = objPVDocTable.Rows[iDocIndex]["DOCUMENT DESC"].ToString();
                                objPVDoc.IsAtPaperVision = -1;
                                //objPVDoc.CreateDate = Conversion.GetDate(objRow["DOCUMENT DATE"].ToString()); //Animesh Modified the code //MITS 19654
                                objPVDoc.CreateDate = Conversion.GetDBDateFormat(Conversion.GetDate(objPVDocTable.Rows[iDocIndex]["DOCUMENT DATE"].ToString()), "MM/dd/yyyy");
                                objPVDoc.Type = Convert.ToInt64(objCache.GetCodeId(objPVDocTable.Rows[iDocIndex]["DOCUMENT TYPE"].ToString(), "DOCUMENT_TYPE"));
                                objPVDoc.AttachRecordId = p_lRecordId;
                                objPVDoc.AttachTable = "CLAIM";
                                objPVDoc.DocumentId = 0;
                                objPVDoc.PVDocumentId = Convert.ToInt64(objPVDocTable.Rows[iDocIndex]["DOCID"].ToString());
                                objPVDoc.Notes = "Document uploaded through PaperVision.";
                                //Animesh Inserted MITS 20256 08/04/2010
                                objPVDoc.FileName = "File uploaded through Papervision";
                                //Animesh Insertion Ends
                                objPVDoc.UserLoginName = m_userLogin.LoginName;
                                iNewDocID = objPVDoc.Update();


                                objDocument.Title = objPVDocTable.Rows[iDocIndex]["DOCUMENT DESC"].ToString();
                                objDocument.Keywords = "";
                                objDocument.CreateDate = Conversion.GetDate(objPVDocTable.Rows[iDocIndex]["DOCUMENT DATE"].ToString());
                                objDocument.FileName = ""; //Mona:PaperVision
                                objDocument.UserName = "";
                                objDocument.Subject = "";
                                objDocument.DocumentsType = objCache.GetCodeDesc(objCache.GetCodeId(objPVDocTable.Rows[iDocIndex]["DOCUMENT TYPE"].ToString(), "DOCUMENT_TYPE"));
                                objDocument.DocumentClass = "";
                                objDocument.DocumentCategory = "";
                                objDocument.Notes = "Document Loaded through PaperVision.";
                                objDocument.DocumentId = Convert.ToInt32(objPVDoc.DocumentId);
                                objDocument.UserId = m_userLogin.LoginName;
                                p_objDocumentList.Documents.Add(objDocument);
                                #endregion
                            }
                            lIndex = lIndex + 1;
                            strFilterExp = null;
                        }
                        DataView objview2 = obNewDt.DefaultView;
                        objview2.Sort = strPVSortOrder;
                        foreach (DataRowView dv in objview2)
                        {
                            DocumentType objDocument = new DocumentType();
                            objDocument.Title = dv["DOCUMENT_NAME"].ToString();
                            objDocument.Keywords = dv["KEYWORDS"].ToString();
                            objDocument.CreateDate = Conversion.GetDBDateFormat(Convert.ToString(dv["CREATE_DATE"].ToString()), "d");
                            objDocument.FileName = dv["DOCUMENT_FILENAME"].ToString();
                            objDocument.UserName = dv["USER_ID"].ToString();
                            objDocument.Subject = dv["DOCUMENT_SUBJECT"].ToString();
                            objDocument.DocumentsType = dv["DOCUMENT_TYPE"].ToString();
                            objDocument.DocumentClass = dv["DOCUMENT_CLASS"].ToString();
                            objDocument.DocumentCategory = dv["DOCUMENT_CATEGORY"].ToString();
                            objDocument.Notes = dv["NOTES"].ToString();
                            objDocument.Pid = dv["DOCUMENT_ID"].ToString();
                            objDocument.UserId = dv["USER_ID"].ToString();
                            p_objDocumentList.Documents.Add(objDocument);
                        }

                    }
                    p_objDocumentList.Email_allowed = "0";
                }//Bharani - MITS : 31951 - Change 2 - End 
                else
                {
                    //throw new RMAppException(Globalization.GetString("PaperVisionManager.ReqdParameters.Exception"));
                    throw new RMAppException(Globalization.GetString("PaperVisionManager.Login.Exception",m_iClientId));
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisonManager.GetAttachments.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PaperVision.GetAttachments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                    objXml = null;
                    objSessionDSN = null;
                }
                //Animesh Inserted MITS 20256 Date 09/04/2010
                if (objFinalTable != null)
                {
                    objFinalTable.Dispose();
                    objFinalTable = null;
                }
                //Animesh Insertion Ends
                if (objDocumentTable != null)
                {
                    objDocumentTable.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objXmlElement = null;
                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
                if (objPaperVision.SessionID != "")
                {
                    objPaperVision.Logoff();
                }
            }
            return iReturnValue;
        }

        /// Name			: AddDocument
        /// Author			: Animesh Sahai 
        /// Date Created	: 26-May-2009
        /// <summary>
        /// Add new document in the store.
        /// </summary>
        /// <param name="p_sDocumentXML">
        ///		Document properties in Xml format.
        ///	</param>
        /// <param name="p_objDocumentFile">
        ///		Document file.
        ///	</param>
        /// <param name="p_lDocumentID">
        ///		Out parameter. ID of the newly added document.
        ///	</param>
        ///	<param name="iPSID">parent security ID</param>
        /// <returns>0 - Success</returns> 								
        public int AddDocument(string p_sDocumentXML, MemoryStream p_objDocumentFile, int iPSID, PVWrapper objPaperVision,
            out long p_lDocumentID)
        {
            int iReturnValue = 0;
            int iSecurityID = 0;
            PaperVisionDocument objDocument = null;
            p_lDocumentID = 0;
            XmlDocument objDoc = new XmlDocument();
            try
            {
                RaiseInitializationException();
                if (p_sDocumentXML != "")
                {
                    objDoc.LoadXml(p_sDocumentXML);
                }
                objDocument = new PaperVisionDocument(m_iClientId);
                objDocument.ConnectionString = this.ConnectionString;
                objDocument.CreateNewDocument(p_sDocumentXML, p_objDocumentFile, m_userLogin.LoginName, m_userLogin.Password, objPaperVision);
                p_lDocumentID = objDocument.DocumentId;
                objDocument.Update();
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.AddDocument.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.AddDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
            }

            return iReturnValue;
        }


        /// Name			: Delete
        /// Author			: Animesh Sahai 
        /// Date Created	: 26-May-2009
        /// <summary>
        /// Deletes the specified documents, folders and all the child folders
        /// along with the documents inside recursively .
        /// </summary>
        /// <param name="p_sDocumentIds">
        ///		Comma separated list of DocumentIds.
        ///	</param>
        /// <param name="p_sFolderIds">
        ///		Comma separated list of FolderIds.
        ///	</param>
        ///	<param name="iPSID">parent security ID</param>
        /// <returns>0 - Success</returns> 										
        public int Delete(string p_sDocumentIds, int iPSID, PVWrapper objPaperVision)
        {
            int iReturnValue = 0;
            //int iSecurityID = 0;
            string sDelimiter = ",";
            string[] arrsDocumentIds = null;

            // Stores 'where' clause for DocumentIds passed through p_sDocumentIds
            StringBuilder sbDocumentsWhereClause = null;
            string sWhereClause = null;
            string sbDeleteIds = string.Empty;
            string sSQL = null;
            DataSet objDataSet = null;
            DataTable objDocumentTable = null;
            DbConnection objDbConnection = null;
            DbTransaction objDbTransaction = null;

            //FileStorageManager objFileStorageManager = null;
            DbConnection objCon = null;
            string sOraCaseIns = string.Empty;
            string strPVDocIDs = string.Empty;
            bool blnPvSuccess;
            try
            {
                RaiseInitializationException();
                blnPvSuccess = false;
                objCon = DbFactory.GetDbConnection(this.ConnectionString);
                objCon.Open();
                if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                {
                    sOraCaseIns = " UPPER(USER_ID) ";
                }
                else
                {
                    sOraCaseIns = "USER_ID";
                }
                if (p_sDocumentIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("DocumentManager.Delete.InvalidIDs", m_iClientId));
                }
                sbDocumentsWhereClause = new StringBuilder();
                if (p_sDocumentIds.Length > 0)
                {
                    arrsDocumentIds = p_sDocumentIds.Split(sDelimiter.ToCharArray());
                    foreach (string sDocumentId in arrsDocumentIds)
                    {
                        if (sbDocumentsWhereClause.Length == 0)
                        {
                            sbDocumentsWhereClause.Append(" DOCUMENT_ID=" + sDocumentId);
                        }
                        else
                        {
                            sbDocumentsWhereClause.Append(" OR DOCUMENT_ID=" + sDocumentId);
                        }
                    }
                }

                if (sbDocumentsWhereClause.Length > 0)
                {
                    sWhereClause = sbDocumentsWhereClause.ToString();
                }

                sSQL = "SELECT DOCUMENT_ID,DOCUMENT_FILENAME, PV_DOCID "
                    + " FROM DOCUMENT "
                    + " WHERE (" + sWhereClause + ") ";

                objDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocumentTable = objDataSet.Tables[0];
                //    iDeletedFileIndex = 0;
                foreach (DataRow objRow in objDocumentTable.Rows)
                {
                    if (strPVDocIDs.Trim() == "")
                    {
                        strPVDocIDs = Conversion.ConvertObjToStr(objRow["PV_DOCID"]);
                        sbDeleteIds = Conversion.ConvertObjToStr(objRow["DOCUMENT_ID"]);
                    }
                    else
                    {
                        strPVDocIDs = strPVDocIDs + "," + Conversion.ConvertObjToStr(objRow["PV_DOCID"]);
                        sbDeleteIds = sbDeleteIds + "," + Conversion.ConvertObjToStr(objRow["DOCUMENT_ID"]);
                    }
                }

                objDbConnection = DbFactory.GetDbConnection(this.ConnectionString);
                objDbConnection.Open();
                objDbTransaction = objDbConnection.BeginTransaction();

                if (strPVDocIDs.Length > 0)
                {
                    blnPvSuccess = objPaperVision.DeleteDocFromPaperVision(strPVDocIDs, m_userLogin.LoginName, m_userLogin.Password);
                }
                if (blnPvSuccess)
                {
                    if (sbDeleteIds.Length > 0)
                    {
                        // Delete documents permanently
                        sSQL = "DELETE FROM DOCUMENT "
                            + " WHERE DOCUMENT_ID IN (" + sbDeleteIds.ToString() + ")";

                        objDbConnection.ExecuteNonQuery(sSQL, objDbTransaction);
                    }
                    if (sbDocumentsWhereClause.Length > 0)
                    {
                        // Remove attachments
                        sSQL = "DELETE FROM DOCUMENT_ATTACH "
                            + " WHERE " + sbDocumentsWhereClause.ToString();

                        objDbConnection.ExecuteNonQuery(sSQL, objDbTransaction);
                    }
                    objDbTransaction.Commit();
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("PaperVisionManager.Login.Exception", m_iClientId));
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("PaperVisionManager.Delete.Exception", m_iClientId), p_objException);
            }
            finally
            {
                sbDocumentsWhereClause = null;
                sbDeleteIds = null;
                if (objDocumentTable != null)
                    objDocumentTable.Dispose();
                if (objDbTransaction != null)
                {
                    objDbTransaction.Dispose();
                    objDbTransaction = null;
                }
                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
            }

            return iReturnValue;
        }


        /// <summary>
        /// Check permissions for add new/move/email/delete/copy related to document list
        /// operations. 
        /// </summary>
        /// <param name="oDataElement">data XML element in the output</param>
        /// <param name="iPSID">parent security ID</param>
        private void CheckDocumentListPermissions(ref DocumentList oDocList, int iPSID)
        {
            int iSecurityID = 0;
            try
            {

                oDocList.Psid = iPSID.ToString();
                //Check for User Document Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                {
                    oDocList.Create_allowed = "1";
                    oDocList.Createfolder_allowed = "0";
                }
                else
                {
                    oDocList.Copy_allowed = "0";
                    oDocList.Createfolder_allowed = "0";
                }
                //Check for Attachment Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                {

                    oDocList.Att_create_allowed = "1";

                }
                else
                {
                    oDocList.Att_create_allowed = "0";
                }
                //Check for User Document Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Delete_allowed = "1";
                else
                    oDocList.Delete_allowed = "0";

                //Check for Attachment Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDocList.Att_delete_allowed = "1";
                else
                    oDocList.Att_delete_allowed = "0";
                oDocList.Transfer_allowed = "0";
                oDocList.Att_transfer_allowed = "0";
                oDocList.Copy_allowed = "0";
                oDocList.Att_copy_allowed = "0";
                oDocList.Email_allowed = "0";
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.CheckDocListPermissions.Error", m_iClientId), p_objException);
            }
        }

        private int GetSecurityID(int iPSID, string sFunctionName)
        {
            int iSecurityID = 0;
            sFunctionName = sFunctionName.ToLower();
            string sOraCaseIns = "";
            using (DbConnection objCon = DbFactory.GetDbConnection(this.ConnectionString))
            {
                objCon.Open();
                if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                {
                    sOraCaseIns = " UPPER(GLOSSARY.SYSTEM_TABLE_NAME) ";
                }
                else
                {
                    sOraCaseIns = "GLOSSARY.SYSTEM_TABLE_NAME";
                }
                if (iPSID == 13000)
                {
                    string sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE = 468 AND " + sOraCaseIns + "='" + m_sFormName.ToUpper() + "'";
                    DbReader objReader = objCon.ExecuteReader(sSQL);
                    if (objReader.Read())
                        iPSID = 5000000 + Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId) * 20;
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
            }

            if (iPSID != USERDOC_SID)
            {
                switch (sFunctionName)
                {
                    case FUNCNAME_CREATE:
                        iSecurityID = iPSID + ATTDOC_ATTACHTO_SID;
                        break;
                    case FUNCNAME_DELETE:
                        iSecurityID = iPSID + ATTDOC_DELETE_SID;
                        break;
                    case FUNCNAME_TRANSFER:
                        iSecurityID = iPSID + ATTDOC_ATTACHTO_SID;
                        break;
                    case FUNCNAME_COPY:
                        iSecurityID = iPSID + ATTDOC_ATTACHTO_SID;
                        break;
                    default:
                        iSecurityID = iPSID;
                        break;
                }
            }
            else if (iPSID == USERDOC_SID)
            {
                switch (sFunctionName)
                {
                    case FUNCNAME_COPY:
                        iSecurityID = USERDOC_COPY_SID;
                        break;
                    case FUNCNAME_CREATE:
                        iSecurityID = USERDOC_CREATE_SID;
                        break;
                    case FUNCNAME_DELETE:
                        iSecurityID = USERDOC_DELETE_SID;
                        break;
                    case FUNCNAME_DETATCH:
                        iSecurityID = USERDOC_DETATCH_SID;
                        break;
                    case FUNCNAME_EXPORT:
                        iSecurityID = USERDOC_EXPORT_SID;
                        break;
                    case FUNCNAME_MODIFY:
                        iSecurityID = USERDOC_MODIFY_SID;
                        break;
                    case FUNCNAME_PRINT:
                        iSecurityID = USERDOC_PRINT_SID;
                        break;
                    case FUNCNAME_TRANSFER:
                        iSecurityID = USERDOC_TRANSFER_SID;
                        break;
                    case FUNCNAME_VIEW:
                        iSecurityID = USERDOC_VIEW_SID;
                        break;
                    case FUNCNAME_VIEWPLAY:
                        iSecurityID = USERDOC_VIEWPLAY_SID;
                        break;
                }
            }

            return iSecurityID;
        }

        /// Name			: RaiseInitializationException
        /// Author			: Animesh Sahai
        /// Date Created	: 28-May-2009
        /// <summary>
        /// Raises InitializationException if required properties
        /// for DocumentManager object are not set.
        /// </summary>
        private void RaiseInitializationException()
        {
            if (this.ConnectionString.Length == 0)
            {
                throw new InitializationException(Globalization.GetString("PaperVision.ConnectionStringNotSet", m_iClientId));
            }

            if (this.UserLoginName.Length == 0)
            {
                throw new InitializationException(Globalization.GetString("PaperVisionManager.UserLoginNameNotSet", m_iClientId));
            }
        }

        /// Name			: GetBaseSecurityIDForTable
        /// Author			: Animesh Sahai
        /// Date Created	: 29-May-2009
        /// <summary>
        /// DocumentManager.GetBaseSecurityIDForTable of Application 
        /// layer gets Base Securiy ID for particular table(i.e. Admin Tracking tables).
        /// </summary>
        ///
        /// <param name="iPSID">An current base security ID.</param>
        /// <param name="sTableName">An current system table name.</param>
        /// Structure of the output XML is:
        /// <returns>return Integer new base ID for a specified table or same ID in case of failure.</returns>
        ///	<returns>A XML with user details.</returns>	
        public int GetBaseSecurityIDForTable(int iPSID, string sTableName)
        {
            if (iPSID == 13000) //This is for Administrative Tracking Tables
            {
                DbReader objReader = null;
                try
                {
                    string sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE = 468 AND GLOSSARY.SYSTEM_TABLE_NAME='" + sTableName.ToUpper() + "'";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader.Read())
                        iPSID = 5000000 + Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId) * 20;
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("PaperVisionManagement.GetBaseSecurityIDForTable.Error", m_iClientId), p_objException);
                }
                finally
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                return iPSID;
            }
            else
                return iPSID;
        }

        /// Name			: GetDocumentDetails
        /// Author			: Animesh Sahai
        /// Date Created	: 04-June-2009
        /// Retrieves document details in Xml format

        public int GetDocumentDetails(string strUserName, string strPassword, PVWrapper objPaperVision, long p_lDocumentId, int iPSID, string p_sScreenFlag, out string p_sDocumentXML)
        {
            int iReturnValue = 0;
            PaperVisionDocument objDocument = null;
            XmlDocument oXmlDocument = null;
            XmlElement oDataElement = null;
            XmlNode oDocumentNode = null;
            XmlNode oNode = null;
            XmlDocument objXml = null;
            XmlNode objSessionDSN = null;
            bool blnPVLoginSucceed;
            string strUrl = string.Empty;
            try
            {
                blnPVLoginSucceed = false;
                RaiseInitializationException();
                objDocument = new PaperVisionDocument(m_iClientId);
                objDocument.DocumentId = p_lDocumentId;
                objDocument.ConnectionString = this.ConnectionString;
                objDocument.UserLoginName = this.UserLoginName;
                objDocument.Load();

                blnPVLoginSucceed = objPaperVision.ValidateRequiredVariables();
                if (blnPVLoginSucceed)
                {
                    blnPVLoginSucceed = objPaperVision.LoginToPV(strUserName, strPassword);
                    if (blnPVLoginSucceed)
                    {
                        strUrl = objPaperVision.PVShowDocLinkUrl + "?SessionID=" + objPaperVision.SessionID + "&EntID=" + objPaperVision.PVEntityID + "&ProjID=" + objPaperVision.PVProjID + "&DocID=" + Conversion.ConvertObjToInt64(objDocument.PVDocumentId, m_iClientId) + "@" + objPaperVision.PVLoginUrl;
                        objDocument.GetDocumentXml(out p_sDocumentXML);
                        oXmlDocument = new XmlDocument();
                        oXmlDocument.LoadXml(p_sDocumentXML);
                        oDataElement = (XmlElement)oXmlDocument.SelectSingleNode("//data");
                        CheckDocPermissions(ref oDataElement, iPSID);
                        //Add psid to the response
                        oDocumentNode = oXmlDocument.SelectSingleNode("//Document");
                        oNode = oXmlDocument.CreateElement("", "psid", "");
                        oNode.InnerText = iPSID.ToString();
                        oDocumentNode.AppendChild(oNode);
                        //Add move_allowed attribute
                        XmlNode objDataNode = oXmlDocument.SelectSingleNode("//data");
                        XmlElement objDataElement = (XmlElement)objDataNode;
                        objDataElement.SetAttribute("move_allowed", "0");
                        oDocumentNode = oXmlDocument.SelectSingleNode("//PapervisionUrl");
                        oDocumentNode.InnerText = strUrl;
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("PaperVisionManager.Login.Exception", m_iClientId));
                    }
                    p_sDocumentXML = oXmlDocument.OuterXml;
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("PaperVisionManager.ReqdParameters.Exception", m_iClientId));
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.GetDocumentDetails.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
                objXml = null;
                objSessionDSN = null;
            }
            return iReturnValue;
        }
        #endregion
        #region Check Document Permission
        /// <summary>
        /// Check permissions for add new/move/email/delete/copy related to a single document
        /// operations. 
        /// </summary>
        /// <param name="oDataElement">data XML element in the output</param>
        /// <param name="iPSID">parent security ID</param>
        private void CheckDocPermissions(ref XmlElement oDataElement, int iPSID)
        {
            int iSecurityID = 0;
            try
            {
                //Check for view permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_VIEW);
                oDataElement.SetAttribute("view_allowed", "", "0");
                //Check for Add New permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_CREATE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("create_allowed", "", "1");
                else
                    oDataElement.SetAttribute("create_allowed", "", "0");
                //oDataElement.SetAttribute("edit_allowed", "", "0"); Animesh Commented ICRMP PCR 5 MITS 20256
                //********************************************
                //Check for edit permission
                //Animesh Inserted for MITS 20256 Date: 08/04/2010
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_MODIFY);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("edit_allowed", "", "1");
                else
                    oDataElement.SetAttribute("edit_allowed", "", "0");
                //Animesh Insertion Ends Date: 08/04/2010
                //********************************************
                //Check for Delete permission
                iSecurityID = GetSecurityID(iPSID, FUNCNAME_DELETE);
                if (m_userLogin.IsAllowedEx(iSecurityID) || (iSecurityID == 0))
                    oDataElement.SetAttribute("delete_allowed", "", "1");
                else
                    oDataElement.SetAttribute("delete_allowed", "", "0");
                oDataElement.SetAttribute("transfer_allowed", "", "0");
                oDataElement.SetAttribute("copy_allowed", "", "0");
                oDataElement.SetAttribute("download_allowed", "", "0");
                oDataElement.SetAttribute("email_allowed", "", "0");
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManagement.CheckDocPermissions.Error", m_iClientId), p_objException);
            }
        }
        //***************************************************
        /// Name			: GetDocuments
        /// Author			: Animesh Sahai 
        /// Date Created	: 15/06/2009
        /// <summary>
        /// Returns information about documents and folders present
        /// in a particular folder, in Xml format. 
        /// Uses UserLoginName and ConnectionString property values. 
        /// </summary>
        ///  <param name="lSortId">
        ///		Sort order of the records
        ///	</param>
        ///	<param name="p_lPageNumber">
        ///		Page Number of the requested page based on the Total no. of information/Page Size.
        ///	</param>
        /// <param name="p_sDocumentsXML">
        ///		Out parameter. Returns information about documents in Xml
        ///		format
        ///	</param>		
        /// <returns>0 - Success</returns>
        public int GetDocumentsNew(int lSortId, long p_lPageNumber, out DocumentList objDocumentList)
        {
            int iReturnValue = 0;
            DataSet objDocumentsDataSet = null;
            DataTable objDocumentTable = null;
            DbReader objDbReader = null;
            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objFolderElement = null;
            XmlElement objXmlElement = null;
            XmlDocument objXml = null;
            XmlNode objSessionDSN = null;
            string sSQL = string.Empty;
            long lDocRecordCount = 0;
            long lTotalRecordCount = 0;
            long lPageSize = 10;
            long lTotalPageCount = 0;
            long lPageStart = 0;
            long lPageEnd = 0;
            int iSortOrder = 0;
            LocalCache objCache = null;
            DbConnection objCon = null;
            string sOraCaseIns = string.Empty;
            try
            {
                RaiseInitializationException();
                objCon = DbFactory.GetDbConnection(this.ConnectionString);
                objCon.Open();
                if (objCon.DatabaseType.ToString() == Riskmaster.Common.Constants.DB_ORACLE)
                {
                    sOraCaseIns = " UPPER(USER_ID) ";
                }
                else
                {
                    sOraCaseIns = "USER_ID";
                }
                // Get all documents
                sSQL = "SELECT DOCUMENT_ID,DOCUMENT_NAME,DOCUMENT_FILENAME,KEYWORDS, "
                    + " CREATE_DATE,NOTES,USER_ID, (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_CLASS) AS DOCUMENT_CLASS,"
                    + " (SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_CATEGORY) AS DOCUMENT_CATEGORY,"
                    + " DOCUMENT_SUBJECT, "
                    + "(SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = DOCUMENT_TYPE) AS DOCUMENT_TYPE, "
                    + " FOLDER_ID, DOC_INTERNAL_TYPE, PV_DOCID,ISAT_PV"
                    + " FROM DOCUMENT "
                    + " WHERE ISAT_PV= -1 "
                    + " AND " + sOraCaseIns + "='" + this.UserLoginName.ToUpper() + "'";
                switch (lSortId)
                {
                    case (int)DOCSORTORDER.ASC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                    case (int)DOCSORTORDER.ASC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.ASC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_TYPE;
                        break;
                    case (int)DOCSORTORDER.ASC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CLASS;
                        break;
                    case (int)DOCSORTORDER.ASC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.ASC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE ASC";
                        iSortOrder = (int)DOCSORTORDER.ASC_CREATE_DATE;
                        break;
                    case (int)DOCSORTORDER.DEC_NAME:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_NAME;
                        break;
                    case (int)DOCSORTORDER.DEC_SUBJECT:
                        sSQL = sSQL + " ORDER BY DOCUMENT_SUBJECT DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_SUBJECT;
                        break;
                    case (int)DOCSORTORDER.DEC_TYPE:
                        sSQL = sSQL + " ORDER BY DOCUMENT_TYPE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_TYPE;
                        break;
                    case (int)DOCSORTORDER.DEC_CLASS:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CLASS DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CLASS;
                        break;
                    case (int)DOCSORTORDER.DEC_CATEGORY:
                        sSQL = sSQL + " ORDER BY DOCUMENT_CATEGORY DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CATEGORY;
                        break;
                    case (int)DOCSORTORDER.DEC_CREATE_DATE:
                        sSQL = sSQL + " ORDER BY CREATE_DATE DESC";
                        iSortOrder = (int)DOCSORTORDER.DEC_CREATE_DATE;
                        break;
                    default:
                        sSQL = sSQL + " ORDER BY DOCUMENT_NAME ASC";
                        lSortId = (int)DOCSORTORDER.ASC_NAME;
                        iSortOrder = (int)DOCSORTORDER.ASC_NAME;
                        break;
                }
                objDocumentsDataSet = DbFactory.GetDataSet(this.ConnectionString, sSQL, m_iClientId);
                objDocumentTable = objDocumentsDataSet.Tables[0];
                //getting the Document record count
                if (objDocumentsDataSet.Tables.Count > 0)
                {
                    lDocRecordCount = objDocumentTable.Rows.Count;
                }
                lTotalRecordCount = lDocRecordCount;
                //getting the page count to be added to the attribute of the data element
                lTotalPageCount = lTotalRecordCount / lPageSize;
                if (lTotalRecordCount > lTotalPageCount * lPageSize)
                {
                    lTotalPageCount = lTotalPageCount + 1;
                }
                if (lTotalPageCount == 0)
                {
                    lTotalPageCount = 1;
                }
                objDocumentList = new DocumentList();
                objDocumentList.Name = "User Documents";
                objDocumentList.Pid = "1";
                objDocumentList.Sid = this.SecurityId.ToString();
                CheckDocumentListPermissions(ref objDocumentList, USERDOC_SID);
                if (p_lPageNumber > 1)
                {
                    objDocumentList.Previouspage = (p_lPageNumber - 1).ToString();
                    objDocumentList.Firstpage = "1";
                }
                if (p_lPageNumber < lTotalPageCount)
                {
                    if (p_lPageNumber == 0) { p_lPageNumber = 1; }
                    objDocumentList.Nextpage = ((int)p_lPageNumber + 1).ToString();
                    objDocumentList.Lastpage = lTotalPageCount.ToString();
                }
                if (p_lPageNumber > lTotalPageCount)
                {
                    objDocumentList.Pagenumber = lTotalPageCount.ToString();
                    p_lPageNumber = lTotalPageCount;
                }
                else
                {
                    objDocumentList.Pagenumber = p_lPageNumber.ToString();
                }
                Folder objFolder = new Folder();
                objFolder.FolderID = "-2";
                objFolder.FolderName = "Attachments";
                objDocumentList.TopFolder = objFolder;

                objDocumentList.Pagecount = lTotalPageCount.ToString();
                objDocumentList.Orderby = iSortOrder.ToString();
                if (p_lPageNumber == 1)
                    lPageStart = 1;
                else
                    lPageStart = ((p_lPageNumber - 1) * lPageSize) + 1;
                lPageEnd = lPageStart + lPageSize - 1;
                if (lPageEnd > lTotalRecordCount)
                    lPageEnd = lTotalRecordCount;
                long lIndex = lPageStart;
                int iDocIndex = 0;
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                iDocIndex = 0;
                objDocumentList.Documents = new System.Collections.Generic.List<DocumentType>();
                while (lIndex <= lPageEnd)
                {
                    iDocIndex = (int)(lIndex - 1);
                    DocumentType objDocument = new DocumentType();
                    objDocument.Title = objDocumentTable.Rows[iDocIndex]["DOCUMENT_NAME"].ToString();
                    objDocument.Keywords = objDocumentTable.Rows[iDocIndex]["KEYWORDS"].ToString();
                    objDocument.CreateDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDocumentTable.Rows[iDocIndex]["CREATE_DATE"].ToString()), "d");
                    objDocument.FileName = objDocumentTable.Rows[iDocIndex]["DOCUMENT_FILENAME"].ToString();
                    objDocument.UserName = objDocumentTable.Rows[iDocIndex]["USER_ID"].ToString();
                    objDocument.Subject = objDocumentTable.Rows[iDocIndex]["DOCUMENT_SUBJECT"].ToString();
                    objDocument.DocumentsType = objDocumentTable.Rows[iDocIndex]["DOCUMENT_TYPE"].ToString();
                    objDocument.DocumentClass = objDocumentTable.Rows[iDocIndex]["DOCUMENT_CLASS"].ToString();
                    objDocument.DocumentCategory = objDocumentTable.Rows[iDocIndex]["DOCUMENT_CATEGORY"].ToString();
                    objDocument.Notes = objDocumentTable.Rows[iDocIndex]["NOTES"].ToString();
                    objDocument.Pid = objDocumentTable.Rows[iDocIndex]["DOCUMENT_ID"].ToString();
                    objDocument.UserId = objDocumentTable.Rows[iDocIndex]["USER_ID"].ToString();
                    objDocumentList.Documents.Add(objDocument);
                    lIndex = lIndex + 1;
                }
                objDocumentList.Move_allowed = "0";
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.GetDocuments.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.GetDocuments.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }
                if (objDocumentsDataSet != null)
                {
                    objDocumentsDataSet.Dispose();
                    objDocumentsDataSet = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objDocumentTable = null;
                objXmlElement = null;
                objFolderElement = null;
                objRootElement = null;
                objXmlDocument = null;
                objXml = null;
                objSessionDSN = null;
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
            }

            return iReturnValue;
        }

        /// <summary>
        /// Method to read the file from the temporary location and to create the Byte Array.
        /// </summary>
        /// <param name="p_sFileName"></param>
        /// <param name="fileBuffer"></param>
        /// <returns></returns>
        public int GetFileBuffer(string p_sFileName, ref byte[] fileBuffer)
        {
            FileStream fileStream;
            BinaryReader binaryRdr;
            int iRetCd = 42;

            if (File.Exists(p_sFileName))
            {
                try
                {
                    // Open existing for read only
                    fileStream = File.OpenRead(p_sFileName);
                    binaryRdr = new BinaryReader(fileStream);
                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(fileStream.Length));

                    binaryRdr.Close();
                    fileStream.Close();
                    iRetCd = 0;
                }
                catch (IOException e)
                {
                    // As necessary we can add more specific responses 
                    // to the system exceptions.
                    string strMessage = "There was an IO Error retrieving file [" +
                        p_sFileName + "].";

                }
                catch (Exception e)
                {
                    string strMessage = "There was an Error reading the file [" +
                        p_sFileName + "].";

                }
            }
            else
            {
                string strMessage = "The file was not found at [" +
                    p_sFileName + "].";

            }

            return iRetCd;
        }

        /// Name			: AddDocument
        /// Author			: Animesh Sahai 
        /// Date Created	: 26-May-2009
        /// <summary>
        /// Add new document in the store.
        /// </summary>
        /// <param name="p_sDocumentXML">
        ///		Document properties in Xml format.
        ///	</param>
        /// <param name="p_objDocumentFile">
        ///		Document file.
        ///	</param>
        /// <param name="p_lDocumentID">
        ///		Out parameter. ID of the newly added document.
        ///	</param>
        ///	<param name="iPSID">parent security ID</param>
        /// <returns>0 - Success</returns> 								
        public int AddDocument(string strName, string strSubject, string strFileName, string strAttachTable,
            string strAttachRecID, MemoryStream p_objDocumentFile, int iPSID, PVWrapper objPaperVision,
            out long p_lDocumentID)
        {
            int iReturnValue = 0;
            int iSecurityID = 0;
            PaperVisionDocument objDocument = null;
            p_lDocumentID = 0;
            XmlDocument objDoc = new XmlDocument();
            XmlDocument objXmlDocument = null;
            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;
            try
            {
                RaiseInitializationException();
                //*************************************************************
                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("data");
                objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                objRootElement.AppendChild(objXmlElement);
                objXmlNode = objXmlDocument.CreateElement("DocumentId");
                objXmlNode.InnerText = "0";
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                objXmlNode.InnerText = System.DateTime.Now.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("Category");
                objXmlNode.InnerText = "0";
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("Name");
                objXmlNode.InnerText = strName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("Class");
                objXmlNode.InnerText = "0";
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("Subject");
                objXmlNode.InnerText = strSubject;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("Type");
                objXmlNode.InnerText = "0";
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("Notes");
                objXmlNode.InnerText = "";
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                objXmlNode.InnerText = this.m_userLogin.LoginName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("FileName");
                objXmlNode.InnerText = strFileName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("Keywords");
                objXmlNode.InnerText = "";
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                objXmlNode.InnerText = strAttachTable;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                objXmlNode.InnerText = strAttachRecID;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
                //*************************************************************
                if (objXmlDocument.OuterXml != "")
                {
                    objDoc.LoadXml(objXmlDocument.OuterXml);
                }
                objDocument = new PaperVisionDocument(m_iClientId);
                objDocument.ConnectionString = this.ConnectionString;
                objDocument.CreateNewDocument(objXmlDocument.OuterXml, p_objDocumentFile, m_userLogin.LoginName, m_userLogin.Password, objPaperVision);
                p_lDocumentID = objDocument.DocumentId;
                objDocument.Update();
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (InvalidValueException p_objInvalidValueException)
            {
                throw p_objInvalidValueException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXMLException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.AddDocument.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PaperVisionManager.AddDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                objDocument = null;
            }

            return iReturnValue;
        }


        //***************************************************
        #endregion


    }
}
