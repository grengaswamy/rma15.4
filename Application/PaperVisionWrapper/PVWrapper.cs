﻿//****************************************************************
//Author : Animesh Sahai
//Description: Class to act as a wrapper to communicate with the PaperVision Web Site.
//Date: 5/20/2009
//****************************************************************
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Data;
using System.Xml;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.ExceptionTypes;
using System.Security.Principal;
using System.Security.Permissions;
namespace Riskmaster.Application.PaperVisionWrapper
{
    public class PVWrapper
    {
        #region Class Variables
        string strPVServerAddress = string.Empty;
        string strPVLoginUrl = string.Empty;
        string strPVShowDocLinkUrl = string.Empty;
        string strPVEntityID = string.Empty;
        string strPVProjID = string.Empty;
        string strSessionID = string.Empty;
        //Animesh Inserted MITS 18345
        //string strMappedDriveName = string.Empty;
        string strSharedNetworkLocationPath = string.Empty;
        string strDomainName = string.Empty;
        string strUserID = string.Empty;
        string strPassword = string.Empty;
        //Animesh Insertion Ends
        //Animesh Inserted MITS 18345
        const int i_CHUNK_SIZE = 5242880; //5 MB chunk size
        const int i_MAX_FILE_SIZE = 52428800; //50 MB max file size to be uploaded in single write call.
        //Animesh Insertion Ends
        private int m_iClientId = 0;
        #endregion

        #region Class Properties
        public string SessionID
        {
            get { return strSessionID; }
            set { strSessionID = value; }
        }
        public string PVServerAddress
        {
            get { return strPVServerAddress; }
            set { strPVServerAddress = value; }
        }
        public string PVLoginUrl
        {
            get { return strPVLoginUrl; }
            set { strPVLoginUrl = value; }
        }
        public string PVShowDocLinkUrl
        {
            get { return strPVShowDocLinkUrl; }
            set { strPVShowDocLinkUrl = value; }
        }
        public string PVEntityID
        {
            get { return strPVEntityID; }
            set { strPVEntityID = value; }
        }
        public string PVProjID
        {
            get { return strPVProjID; }
            set { strPVProjID = value; }
        }
        //Animesh Inserted MITS 18345
        public string SharedNetworkLocationPath
        {
            get { return strSharedNetworkLocationPath; }
            set { strSharedNetworkLocationPath = value; }
        }
        public string DomainName
        {
            get { return strDomainName; }
            set { strDomainName = value; }
        }
        public string UserID
        {
            get { return strUserID; }
            set { strUserID = value; }
        }
        public string Password
        {
            get { return strPassword; }
            set { strPassword = value; }
        }
        //Animesh Insertion ends
        #endregion

        #region UtilityFunctions
        protected string ConvertFileToBase64(string fileName)
        {
            string ReturnValue = "";
            if (File.Exists(fileName) == true)
            {
                using (FileStream BinaryFile = new FileStream(fileName, FileMode.Open))
                {
                    BinaryReader BinRead = new BinaryReader(BinaryFile);
                    byte[] BinBytes = BinRead.ReadBytes((int)BinaryFile.Length);
                    ReturnValue = Convert.ToBase64String(BinBytes);
                    BinaryFile.Close();
                }
            }
            return ReturnValue;
        }

        protected string FixIt(string s)
        {
            s = s.Replace("<BR>", "$$br$$");
            s = s.Replace("<", "&lt;");
            s = s.Replace(">", "&gt;");
            s = s.Replace("$$br$$", "&lt;BR&gt;<BR>\r\n");
            s = s.Replace("\r\n", "<BR>\r\n");

            return s;
        }
        protected string XMLChop(string sXML, string sStart, string sEnd)
        {
            try
            {
                string sChopped = sXML;
                sChopped = sChopped.Substring(sChopped.IndexOf(sStart) + sStart.Length);
                sChopped = sChopped.Substring(0, sChopped.IndexOf(sEnd));
                return sChopped;
            }
            catch
            {
                return "";
            }
        }
        #endregion

        #region PaperVision XML Methods
        protected DataTable ConvertSearchToDataTable(string sXMLResults)
        {
            DataTable dt = new DataTable("SearchResults");
            DataRow drow;
            DataColumn dc;
            XmlDocument xmldoc;
            XmlNodeList xmlnlist;
            Hashtable objHash;
            try
            {
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(sXMLResults);
                objHash = new Hashtable();

                xmlnlist = xmldoc.SelectNodes("/PVDM_ExecuteQuery_Project/SEARCHRESULTS/PROJECT/PVDM_ProjectAttributes/PROJECTATTRIBUTES/DOCUMENT_FIELDS/FIELD");
                if (xmlnlist == null)
                    return dt;
                if (xmlnlist.Count < 1)
                    return dt;
                foreach (XmlNode xmln in xmlnlist)
                {
                    objHash.Add(Conversion.ConvertObjToStr(XMLAttrVal(xmln, "FIELDNUM")), Conversion.ConvertObjToStr(xmln.ChildNodes.Item(0).InnerText));
                }

                xmlnlist = xmldoc.SelectNodes("/PVDM_ExecuteQuery_Project/SEARCHRESULTS/DOCS/DOC");
                if (xmlnlist == null)
                    return dt;
                if (xmlnlist.Count < 1)
                    return dt;
                int i;
                dc = new DataColumn("DOCID");
                dc.DataType = Type.GetType("System.String");
                dt.Columns.Add(dc);
                for (i = 0; i <= xmlnlist.Item(0).ChildNodes.Count - 1; i++)
                {
                    if (xmlnlist.Item(0).ChildNodes[i].Name.Contains("DOCINDEX"))
                    {
                        string strID = Conversion.ConvertObjToStr(xmlnlist.Item(0).ChildNodes[i].Name.Substring(Conversion.ConvertObjToInt(xmlnlist.Item(0).ChildNodes[i].Name.IndexOf("DOCINDEX"), m_iClientId) + 8));
                        if (objHash.ContainsKey(strID))
                            dc = new DataColumn(Conversion.ConvertObjToStr(objHash[strID]));
                        else
                            dc = new DataColumn(xmlnlist.Item(0).ChildNodes[i].Name);
                    }
                    else
                    {
                        dc = new DataColumn(xmlnlist.Item(0).ChildNodes[i].Name);
                    }
                    dc.DataType = Type.GetType("System.String");
                    dt.Columns.Add(dc);
                }
                dc = new DataColumn("DOCVIEWLINK");
                dc.DataType = Type.GetType("System.String");
                dt.Columns.Add(dc);
                foreach (XmlNode xmln in xmlnlist)
                {
                    drow = dt.NewRow();
                    drow["DOCID"] = XMLAttrVal(xmln, "DOCID");
                    for (i = 0; i <= xmln.ChildNodes.Count - 1; i++)
                    {
                        if (xmln.ChildNodes.Item(i).Name.Contains("DOCINDEX"))
                        {
                            string strID = Conversion.ConvertObjToStr(xmlnlist.Item(0).ChildNodes[i].Name.Substring(Conversion.ConvertObjToInt(xmlnlist.Item(0).ChildNodes[i].Name.IndexOf("DOCINDEX"), m_iClientId) + 8));
                            if (objHash.ContainsKey(strID))
                            {
                                drow[Conversion.ConvertObjToStr(objHash[strID])] = xmln.ChildNodes.Item(i).InnerText.Replace("%26", "&");
                            }
                            else
                            {
                                drow[xmln.ChildNodes.Item(i).Name] = xmln.ChildNodes.Item(i).InnerText.Replace("%26", "&");
                            }
                        }
                        else
                        {
                            drow[xmln.ChildNodes.Item(i).Name] = xmln.ChildNodes.Item(i).InnerText.Replace("%26", "&");
                        }
                    }
                    //drow["DOCVIEWLINK"] = "http://icrmpback/ShowDoc.ASP?SessionID=" + this.strSessionID  + "&EntID=" + this.PVEntityID  + "&ProjID=" + this.PVProjID + "&DocID=" + XMLAttrVal(xmln, "DOCID");
                    drow["DOCVIEWLINK"] = "";
                    dt.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return dt;
        }
        protected int CountNodes(XmlNodeList xmlnlist)
        {
            int i = 0;

            try
            {
                foreach (XmlNode xmln in xmlnlist)
                    i = i + 1;
            }
            catch (Exception ex)
            {
                //Errors.ErrorHandler(ex, true, true);
            }

            return i;
        }
        protected string XMLAttrVal(XmlNode xmln, string sChildName)
        {
            try
            {
                return xmln.Attributes[sChildName].Value;
            }
            catch (Exception ex)
            {
                //Errors.ErrorHandler(ex, true, true);
                return "[not found: " + sChildName + "]";
            }
        }
        protected string XMLChildName(XmlNode xmln, int iChildNum)
        {
            try
            {
                if (iChildNum > xmln.ChildNodes.Count - 1)
                    return "[error: out of range]";

                if (xmln.ChildNodes.Item(iChildNum) != null)
                    return xmln.ChildNodes.Item(iChildNum).Name;

                return "[not found: " + iChildNum.ToString() + "]";
            }
            catch (Exception ex)
            {
                //Errors.ErrorHandler(ex, true, true);
                return "[error: " + iChildNum.ToString() + "]";
            }
        }
        protected string XMLChildVal(string xml, string sStartPath, string sChildName)
        {
            XmlDocument xmldoc;
            XmlNodeList xmlnlist;

            try
            {
                xmldoc = new XmlDocument();
                xmldoc.LoadXml(xml);
                xmlnlist = xmldoc.SelectNodes(sStartPath);

                foreach (XmlNode xmln in xmlnlist)
                    return XMLChildVal(xmln, sChildName);

                return "[not found]";
            }
            catch (Exception ex)
            {
                //Errors.ErrorHandler(ex, true, true);
                return "[error]";
            }
        }
        protected string XMLChildVal(XmlNode xmln, int iChildNum)
        {
            try
            {
                if (iChildNum > xmln.ChildNodes.Count - 1)
                    return "[error: out of range]";

                if (xmln.ChildNodes.Item(iChildNum) != null)
                    return xmln.ChildNodes.Item(iChildNum).InnerText;

                return "[not found: " + iChildNum.ToString() + "]";
            }
            catch (Exception ex)
            {
                //Errors.ErrorHandler(ex, true, true);
                return "[error: " + iChildNum.ToString() + "]";
            }
        }
        protected string XMLChildVal(XmlNode xmln, string sChildName)
        {
            int n;

            try
            {
                for (n = 0; n <= xmln.ChildNodes.Count - 1; n++)
                    if (xmln.ChildNodes.Item(n) != null)
                        if (xmln.ChildNodes.Item(n).Name == sChildName)
                            return xmln.ChildNodes.Item(n).InnerText;

                return "[not found: " + sChildName + "]";
            }
            catch (Exception ex)
            {
                //Errors.ErrorHandler(ex, true, true);
                return "[error: " + sChildName + "]";
            }
        }
        #endregion

        #region "Private internal PaperVision methods"
        /// <summary>
        /// Funtion to create the header of the Web request.
        /// </summary>
        /// <param name="sFunctionName"></param>
        /// <returns></returns>
        protected string FunctionStart(string sFunctionName)
        {
            string sXML;

            sXML = "<?xml version=\"1.0\" standalone=\"yes\" ?>\r\n";
            sXML += "<MicroNetWrapper>";
            sXML += "	<FUNCTION>";
            sXML += "		<NAME>" + sFunctionName + "</NAME>";
            sXML += "		<PARAMETERS>";
            sXML += "			<ENTITYID>" + this.PVEntityID + "</ENTITYID>";
            sXML += "			<SESSIONID>" + this.SessionID + "</SESSIONID>";

            return sXML;
        }
        /// <summary>
        /// Funtion to add the parameters and their values to the web request.
        /// </summary>
        /// <param name="sParameterName"></param>
        /// <param name="sParameterValue"></param>
        /// <returns></returns>
        protected string FunctionParameter(string sParameterName, string sParameterValue)
        {
            return "<" + sParameterName.ToUpper() + ">" + sParameterValue + "</" + sParameterName.ToUpper() + ">";
        }
        /// <summary>
        /// FunctionEnd: To create the trailer of the Web Request. 
        /// </summary>
        /// <returns></returns>
        protected string FunctionEnd()
        {
            string sXML;

            sXML = "		</PARAMETERS>";
            sXML += "	</FUNCTION>";
            sXML += "</MicroNetWrapper>";

            return sXML;
        }
        #endregion

        #region PaperVision Main Methods
        /// <summary>
        /// Author: Animesh Sahai
        /// Date : 5/21/2009
        /// Method to vaidate the existing session.
        /// </summary>
        /// <returns>Boolean value.</returns>
        protected bool ValidateSession()
        {
            string sResult;
            string sXML;
            if (SessionID.Trim() != "")
            {
                sXML = "<?xml version=\"1.0\" standalone=\"yes\" ?>\r\n";
                sXML += "<MicroNetWrapper>";
                sXML += "	<FUNCTION>";
                sXML += "		<NAME>ValidateSession</NAME>";
                sXML += "		<PARAMETERS>";
                sXML += "			<ENTITYID>" + PVEntityID.Trim() + "</ENTITYID>";
                sXML += "			<SESSIONID>" + SessionID.Trim() + "</SESSIONID>";
                sXML += "		</PARAMETERS>";
                sXML += "	</FUNCTION>";
                sXML += "</MicroNetWrapper>";
                sResult = POST(sXML, PVLoginUrl);
                return sResult.Contains("<GOODSESSION>1</GOODSESSION>");
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Author: Animesh Sahai
        /// Date : 5/21/2009
        /// Method to Validate the required fields necessary to PaperVison
        /// Enhancement
        /// </summary>
        /// <returns>Boolean value.</returns>
        public bool ValidateRequiredVariables()
        {
            if (PVProjID.Trim() == "" || PVEntityID.Trim() == "" || PVShowDocLinkUrl.Trim() == ""
                || PVLoginUrl.Trim() == "" || PVServerAddress.Trim() == ""
                || SharedNetworkLocationPath.Trim() == "" || DomainName.Trim() == ""
                || UserID.Trim() == "" || Password.Trim() == "")
                return false;
            else
                return true;
        }

        /// <summary>
        /// Author: Animesh Sahai
        /// Date : 5/21/2009
        /// Method to overload the genuine POST method.
        /// </summary>
        /// <param name="sURLParams">Web request parameters.</param>
        /// <returns>The Output of the Web request.</returns>
        protected string POST(string sURLParams)
        {
            return this.POST(sURLParams, this.PVLoginUrl);
        }

        /// <summary>
        /// Author: Animesh Sahai
        /// Date: 5/21/2009
        /// Method to raise a Web request to the specified URL and attaching the 
        /// required parameters to the Web request.
        /// </summary>
        /// <param name="sURLParams">Web request parameters</param>
        /// <param name="sHost">Web request Url</param>
        /// <returns>The Output of the Web request.</returns>
        protected string POST(string sURLParams, string sHost)
        {
            WebRequest objReq;
            StreamWriter objWriter;
            sURLParams = sURLParams.Replace("\t", "");
            try
            {
                objReq = WebRequest.Create(sHost);
                objReq.Timeout = 900000; // fifteen minutes
                objReq.Credentials = CredentialCache.DefaultCredentials;
                objReq.ContentType = "application/x-www-form-urlencoded";
                objReq.ContentLength = sURLParams.Length;
                System.Net.IWebProxy iwp = System.Net.WebRequest.GetSystemWebProxy();
                objReq.Proxy = iwp;
                objReq.Method = "POST";
                objWriter = new StreamWriter(objReq.GetRequestStream());
            }
            catch // (Exception ex)
            {
                return "";
            }
            objWriter.Write(sURLParams);
            objWriter.Flush();
            objWriter.Close();
            objWriter = null;
            try
            {
                WebResponse objResp = objReq.GetResponse();
                StreamReader objSRead = new StreamReader(objResp.GetResponseStream());
                string cContent = objSRead.ReadToEnd();
                objSRead.Close();
                objSRead.Dispose();
                objSRead = null;
                objResp = null;
                return cContent;
            }
            catch (Exception ex)
            {
                return "";
            }
            finally
            {
                if (objWriter != null)
                {
                    objWriter.Close();
                    objWriter.Dispose();
                    objWriter = null;
                }
                objReq = null;
            }

        }

        /// <summary>
        /// Author: Animesh Sahai 
        /// Date: 5/21/2009
        /// Method to retrieve the Error from the returned XML.
        /// </summary>
        /// <param name="sXMLResults">Returns XML from the Papervision</param>
        protected string ShowError(string sXMLResults, string strFunction)
        {
            string sError = "";
            string sPathFinder;
            XmlDocument xmldoc2 = new XmlDocument();
            xmldoc2.LoadXml(sXMLResults);
            sPathFinder = "/PVDM_" + strFunction + "/ERROR";
            if (CountNodes(xmldoc2.SelectNodes(sPathFinder)) < 1)
            {
                sPathFinder = "/MicroNetWrapper/ERROR";
                if (CountNodes(xmldoc2.SelectNodes(sPathFinder)) < 1)
                {
                    sPathFinder = "/PVDM_HTTPHandler/ERROR";
                }
            }
            try
            {
                if (XMLChildVal(sXMLResults, sPathFinder, "NUMBER").Contains("[") == false)
                {
                    sError += "PAPERVISION ERROR RETURNED!: ";
                    sError += "Description: " + XMLChildVal(sXMLResults, sPathFinder, "DESCRIPTION");
                }
                return sError;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        /// <summary>
        /// Author: Animesh Sahai.
        /// Method to add a Document to the PaperVision server
        /// </summary>
        /// <param name="sDocDate"></param>
        /// <param name="sDocType"></param>
        /// <param name="sClaimNum"></param>
        /// <param name="strEventID"></param>
        /// <param name="strEventDate"></param>
        /// <param name="sFileContents"></param>
        /// <param name="sFileName"></param>
        /// <param name="sOrigFileName"></param>
        /// <param name="sUserID"></param>
        /// <param name="sPwd"></param>
        /// <param name="strEntityName"></param>
        /// <param name="lPVDocID"></param>
        /// <returns></returns>
        //public bool AddDocToPaperVision(string sDocDate, string sDocType, string sClaimNum,string strEventID,string strEventDate, byte[] sFileContents, string sFileName, string sUserID, string sPwd, out long lPVDocID) 
        //public bool AddDocToPaperVision(string sDocDate, string sDocType, string sClaimNum,string strEventID,string strEventDate, byte[] sFileContents, string sFileName, string sUserID, string sPwd, string strEntityName, out long lPVDocID) //Defination modified MITS 18055
        public bool AddDocToPaperVision(string sDocDate, string sDocType, string sClaimNum, string strEventID, string strEventDate, byte[] sFileContents, string sFileName, string sOrigFileName, string sUserID, string sPwd, string strEntityName, out long lPVDocID,int p_iClientId) //Defination modified to save the orginal filename on the server as well - Animesh 
        {
            bool blnReturn;
            string strDocCols = string.Empty;
            string strDocData = string.Empty;
            string sXML = string.Empty;
            string sResult = string.Empty;
            string strTempFileName = string.Empty;
            string strStorageLoc = string.Empty;
            bool blnFileWritten; //MITS 18345 Animesh Inserted 12/20/2009
            int iFileSize = 0;
            int iOffset = 0;
            int iChunkSize = 0;
            FileStream outFileStream = null;
            try
            {
                //***********************
                lPVDocID = 0;
                //Animesh Inserted MITS 18345 12/20/2009
                blnFileWritten = false;
                strTempFileName = "Doc_" + DateTime.Now.Ticks.ToString() + "_" + sOrigFileName;
                strSharedNetworkLocationPath = strSharedNetworkLocationPath.Trim();
                strUserID = strUserID.Trim();
                strPassword = strPassword.Trim();
                strDomainName = strDomainName.Trim();
                if (strSharedNetworkLocationPath.EndsWith("\\"))
                    strSharedNetworkLocationPath = strSharedNetworkLocationPath.Substring(0, strSharedNetworkLocationPath.Length - 1);
                using (UNCAccessWithCredentials objUNC = new UNCAccessWithCredentials())
                {
                    if (objUNC.NetUseWithCredentials(strSharedNetworkLocationPath, strUserID, strDomainName, strPassword))
                    {
                        try
                        {
                            iFileSize = sFileContents.Length;
                            iChunkSize = i_CHUNK_SIZE;//Animesh Setting ChunkSize to 5MB to write file in chunks if its size is too big.
                            if (iFileSize > i_MAX_FILE_SIZE) //Maximum file size to be uploaded on the network in one go is 64 MB. Currently limiting it to 50MB 
                            {
                                outFileStream = new FileStream(strSharedNetworkLocationPath + "\\" + strTempFileName, FileMode.Create, FileAccess.Write);
                                while (iFileSize > 0)
                                {
                                    outFileStream.Write(sFileContents, iOffset, iChunkSize);
                                    iFileSize = iFileSize - iChunkSize;
                                    iOffset = iOffset + iChunkSize;
                                    if (iFileSize < iChunkSize)
                                    {
                                        iChunkSize = iFileSize;
                                    }
                                }
                            }
                            else
                            {
                                File.WriteAllBytes(strSharedNetworkLocationPath + "\\" + strTempFileName, sFileContents);
                            }
                            blnFileWritten = true;
                        }
                        catch
                        {
                            throw new RMAppException(Globalization.GetString("PVWrapper.AddDocToPaperVision.FileWriteException",p_iClientId));
                        }
                        finally
                        {
                            if (outFileStream != null)
                            {
                                outFileStream.Close();
                                outFileStream.Dispose();
                            }

                        }
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("PVWrapper.AddDocToPaperVision.DriveNotExistsException",p_iClientId));
                    }
                    // When it reaches the end of the using block, the class deletes the connection.
                }
                blnReturn = false;
                if (blnFileWritten) //Animesh Modified MITS 18345 12/20/2009
                {
                    //*************************
                    lPVDocID = 0;
                    blnReturn = LoginToPV(sUserID, sPwd);
                    if (blnReturn)
                    {
                        blnReturn = ValidateRequiredVariables();
                        if (blnReturn)
                        {
                            strDocCols = "DOCUMENT DATE|DOCUMENT TYPE|DOCUMENT DESC|UPLOAD DATE|CLAIM ID|ENTITY ID|EVENT DATE|ENTITY NAME"; //Animesh Modified MITS 18055
                            strDocData = sDocDate + "|" + sDocType + "|" + sFileName + "|" + sDocDate + "|" + sClaimNum + "|" + strEventID + "|" + strEventDate + "|" + strEntityName; //Animesh Modified MITS 18055
                            sXML = FunctionStart("AttachNewDocToProject");
                            sXML += FunctionParameter("FieldNames", strDocCols);
                            sXML += FunctionParameter("FieldValues", strDocData);
                            //sXML += FunctionParameter("OriginalFileName", sFileName); //Animesh Modifed to save the original filename instead of the title.
                            sXML += FunctionParameter("OriginalFileName", sOrigFileName);
                            //Animesh Inserted MITS 18345 12/22/2009
                            sXML += FunctionParameter("SavedFile", strSharedNetworkLocationPath + "\\" + strTempFileName);
                            //Animesh Insertion ends
                            //sXML += "<INFILEDATA dt:dt=\"bin.base64\" xmlns:dt=\"urn:schemas-microsoft-com:datatypes\">" + Convert.ToBase64String(sFileContents) + "</INFILEDATA>"; //Animesh Commented MITS 18345
                            sXML += FunctionParameter("ProjID", this.PVProjID);
                            sXML += FunctionEnd();
                            sResult = POST(sXML, this.PVLoginUrl);
                            if (sResult.Contains("<ERROR><NUMBER>") || sResult.Trim() == "")
                                blnReturn = false;
                            else
                            {
                                lPVDocID = Conversion.ConvertObjToInt64(XMLChop(sResult, "<NEWDOCID>", "</NEWDOCID>"), m_iClientId);
                                blnReturn = true;
                            }
                        }
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("PaperVisionManager.AddDocument.Exception",p_iClientId));
                    }

                }

                return blnReturn;
            }
            catch (RMAppException p_objExc)
            {
                throw p_objExc;
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                if (this.SessionID.Trim() != "")
                {
                    this.Logoff();
                }
            }
        }

        /// <summary>
        /// Author: Animesh Sahai.
        /// Mehtod to authenticate the user on the paervision server.
        /// </summary>
        /// <param name="sUserID"></param>
        /// <param name="sPwd"></param>
        /// <returns></returns>
        public bool LoginToPV(string sUserID, string sPwd)
        {
            string sXML = string.Empty;
            string sResult = string.Empty;
            try
            {
                sXML = "<?xml version=\"1.0\" standalone=\"yes\" ?>\r\n";
                sXML += "<MicroNetWrapper>";
                sXML += "	<FUNCTION>";
                sXML += "		<NAME>LoginUser</NAME>";
                sXML += "		<PARAMETERS>";
                sXML += "			<ENTITYID>" + this.PVEntityID + "</ENTITYID>";
                sXML += "			<USERNAME>" + sUserID + "</USERNAME>";
                sXML += "			<PASSWORD>" + sPwd + "</PASSWORD>";
                sXML += "		</PARAMETERS>";
                sXML += "	</FUNCTION>";
                sXML += "</MicroNetWrapper>";

                sResult = POST(sXML, this.PVLoginUrl);
                if (sResult.Contains("<ERROR><NUMBER>") || sResult.Trim() == "")
                {
                    return false;
                }
                this.SessionID = XMLChop(sResult, "<SESSIONID>", "</SESSIONID>");
                if (this.SessionID == "")
                {
                    return false;
                }
                sXML = "<?xml version=\"1.0\" standalone=\"yes\" ?>\r\n";
                sXML += "<MicroNetWrapper>";
                sXML += "	<FUNCTION>";
                sXML += "		<NAME>LoadSystemAccess</NAME>";
                sXML += "		<PARAMETERS>";
                sXML += "			<ENTITYID>" + this.PVEntityID + "</ENTITYID>";
                sXML += "			<SESSIONID>" + SessionID + "</SESSIONID>";
                sXML += "		</PARAMETERS>";
                sXML += "	</FUNCTION>";
                sXML += "</MicroNetWrapper>";
                sResult = POST(sXML, this.PVLoginUrl);
                if (sResult.Contains("<ERROR><NUMBER>") || sResult.Trim() == "")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Author: Animesh Sahai.
        /// Method to Logoff from the PaperVision Server.
        /// </summary>
        public void Logoff()
        {
            try
            {
                if (ValidateSession() == true)
                {
                    string sXML = "";
                    string sResult;
                    sXML = FunctionStart("KillSession");
                    sXML += FunctionParameter("ProjID", this.PVProjID);
                    sXML += FunctionEnd();
                    sResult = POST(sXML, this.PVLoginUrl);
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Author: Animesh Sahai 
        /// Date 06/15/2009
        /// Method:DeleteDocFromPaperVision - To delete the list of documents from the PaperVision Server.
        /// </summary>
        /// <param name="strPVDocID"></param>
        /// <param name="sUserID"></param>
        /// <param name="sPwd"></param>
        /// <returns></returns>
        public bool DeleteDocFromPaperVision(string strPVDocID, string sUserID, string sPwd)
        {
            bool blnReturn;
            string sXML = string.Empty;
            string sResult = string.Empty;
            try
            {
                blnReturn = false;
                blnReturn = LoginToPV(sUserID, sPwd);
                if (blnReturn)
                {
                    blnReturn = ValidateRequiredVariables();
                    if (blnReturn)
                    {
                        sXML = FunctionStart("DeleteDocs_Project");
                        sXML += FunctionParameter("DocIDList", strPVDocID);
                        sXML += FunctionParameter("ProjID", this.PVProjID);
                        sXML += FunctionEnd();
                        sResult = POST(sXML, this.PVLoginUrl);
                        if (sResult.Contains("<ERROR><NUMBER>"))
                            blnReturn = false;
                        else
                        {
                            blnReturn = true;
                        }
                    }
                    else
                    {
                        blnReturn = false;
                    }
                }
                if (this.SessionID.Trim() != "")
                {
                    this.Logoff();
                }

                return blnReturn;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Author: Animesh Sahai 
        /// Date: 06/19/2009
        /// Method to search the papervision server to get the list of document availabale at the Papervision server. 
        /// </summary>
        /// <param name="sFields"></param>
        /// <param name="sCriteria"></param>
        /// <param name="sSortBy"></param>
        /// <param name="sProjID"></param>
        /// <returns></returns>
        public DataTable Search(string sFields, string sCriteria, string sSortBy)
        {
            string sXML = "";
            string sResult;
            try
            {
                sXML = FunctionStart("SubmitSearchCriteria_Project");
                sXML += FunctionParameter("FieldNames", sFields);
                if (sCriteria != null)
                {
                    sXML += FunctionParameter("FromFields", sCriteria);
                    sXML += FunctionParameter("ToFields", sCriteria);
                }
                sXML += FunctionParameter("SortBy", sSortBy);
                sXML += FunctionParameter("SearchType", "OR");
                sXML += FunctionParameter("ProjID", this.PVProjID);
                sXML += FunctionEnd();
                sResult = this.POST(sXML);
                if (sResult.Contains("<ERROR><NUMBER>"))
                {
                    return null;
                }
                sXML = this.FunctionStart("ExecuteQuery_Project");
                sXML += this.FunctionParameter("StartingDocID", "1");
                sXML += this.FunctionParameter("FormatValues", "False");
                sXML += this.FunctionParameter("ProjID", this.PVProjID);
                sXML += this.FunctionEnd();
                sResult = this.POST(sXML);
                if (sResult.Contains("<ERROR><NUMBER>"))
                {
                    return null;
                }
                DataTable dt = ConvertSearchToDataTable(sResult);
                return dt;
            }
            catch
            {
                return null;
            }
        }


        #endregion
    }
}
