using System;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.IO;


namespace Riskmaster.Application.PrintEOB
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   18 January 2004
	///Purpose :   This class fetches the Print Check Stub Details
	/// </summary>
	public class PrintCheckStub
	{
		#region "Constructor"
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		public PrintCheckStub(string p_sUserName , string p_sPassword, string p_sDsnName, string p_sDsnId, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_sDsnId = p_sDsnId;
            m_iClientId = p_iClientId;//rkaur27
			this.Initialize();
			 
		}		
		#endregion

		#region "Member Variables"
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = string.Empty;

		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = string.Empty;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = string.Empty;

		/// <summary>
		/// Private variable to store Connection String
		/// </summary>
		private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Stores the object of common functions class
        /// </summary>
        private Functions m_objFunctions = null;
        /// <summary>
        /// Private variable to store DSN Id
        /// </summary>
        private string m_sDsnId = string.Empty;
		#endregion

        private int m_iClientId = 0;//rkaur27
		#region "Public Functions"
		
		#region "PrintDetail(int p_iTransId, out string p_sPDFSaveFilePath)"
		/// Name		: PrintDetail
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// To be able to print the check stub information to match up with the EOBs when they printed them out individually.
		/// This will only called from the Funds form and it should turned on from utilities.
		/// </summary>
		/// <param name="p_iTransId">Funds Trans ID</param>
		/// <param name="p_sPDFSaveFilePath">Pdf path</param>
        public bool PrintDetail(int p_iTransId, out string p_sPDFSaveFilePath, out string p_sSaveFileName)
		{
			DataModelFactory objDataModelFactory = null;
			PrintWrapper objPrintWrapper = null;
			Funds objFunds = null;
			LocalCache objLocalCache = null;
			Claim objClaim = null;
			Entity objEntity = null;
			Event objEvent = null;
			string sClaimantFullName = string.Empty;
			string sEntityFullName = string.Empty; 
			string sCheckDate = string.Empty;
			int iDescX = 0;
			int iFromX = 0;
			int iToX = 0;
			int iInvNumX = 0;
			int iInvAmtX = 0;
			int iAmountX = 0;
			double dAvgHeight = 0.0;
			double dAvgWidth = 0.0;
			double dCurrX = 0.0;
			double dCurrY = 0.0;
			string sTemp = string.Empty;
			string sDestFolderPath = "";

			bool bStubDetail = false ;
			try
			{
				p_sPDFSaveFilePath = string.Empty ;
                p_sSaveFileName = string.Empty ;
				objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword,m_iClientId );//sharishkumar Jira 835
				m_sConnectionString = objDataModelFactory.Context.DbConn.ConnectionString;

				// Vaibhav : Read the Stub Detail flag: START 
				string sSQL = "SELECT PRINT_STUB_DETAIL FROM CHECK_OPTIONS" ;
				DbReader objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );	
		
				if( objReader != null )
					if( objReader.Read() )
						bStubDetail = objReader.GetBoolean( "PRINT_STUB_DETAIL" );						
				objReader.Close();	
				objReader = null ;
				// Stub Detail flag : END.

				if( !bStubDetail )
					return false;


				p_sPDFSaveFilePath = "CheckStub_" + p_iTransId + "_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
                sDestFolderPath = Functions.GetSavePath();

				if( ! Directory.Exists( sDestFolderPath ) )
					Directory.CreateDirectory( sDestFolderPath );
                p_sSaveFileName = p_sPDFSaveFilePath;
				p_sPDFSaveFilePath = sDestFolderPath + @"\" + p_sPDFSaveFilePath;

				objLocalCache = new LocalCache(m_sConnectionString,m_iClientId); 				
				objFunds = (Funds)objDataModelFactory.GetDataModelObject("Funds",false);
				objFunds.MoveTo(p_iTransId);

				//Set the X values for printing the transaction details
				iDescX = 500;
                //zalam mits:-10813 03/31/2008 Start
				//iFromX = 4175;                
				//iToX = 5600;                
				//iInvNumX = 6840;                
				//iInvAmtX = 8015;                
				//iAmountX = 10850;
                
                iFromX = 3800;
                iToX = 4835;
                iInvNumX = 5800;
                iInvAmtX = 8500;
                iAmountX = 10350;
                //zalam mits:-10813 03/31/2008 End
				
				objPrintWrapper = new PrintWrapper(m_iClientId);				
				objPrintWrapper.StartDoc( false );
				dAvgHeight = objPrintWrapper.GetTextHeight("W");
				dAvgWidth = objPrintWrapper.PageWidth;
				
				dCurrX = 500;
				dCurrY = 900;

				objPrintWrapper.SetFont("Arial",9);

				//Print the header
				objPrintWrapper.CurrentX = iDescX; 
				objPrintWrapper.CurrentY = dCurrY + dAvgHeight;
				objPrintWrapper.PrintText("Description");
				objPrintWrapper.CurrentX = iFromX; 
				objPrintWrapper.PrintText("From Date");
				objPrintWrapper.CurrentX = iToX; 
				objPrintWrapper.PrintText("To Date");
				objPrintWrapper.CurrentX = iInvNumX; 
				objPrintWrapper.PrintText("Invoice #");
				objPrintWrapper.CurrentX = iInvAmtX; 
				objPrintWrapper.PrintText("Invoice Amount");
				objPrintWrapper.CurrentX = iAmountX; 
				objPrintWrapper.PrintText("Amount");

				dCurrY = dCurrY + dAvgHeight;
				objPrintWrapper.PrintLine(500, dCurrY + (dAvgHeight - 10), 11550, dCurrY + (dAvgHeight - 10));
				dCurrY = dCurrY - 10;
				foreach(FundsTransSplit objFundsTransSplit in objFunds.TransSplitList)
				{
					dCurrY = dCurrY + dAvgHeight;
					objPrintWrapper.CurrentX = iDescX; 
					objPrintWrapper.CurrentY = dCurrY;
					objPrintWrapper.PrintText(objLocalCache.GetCodeDesc(objFundsTransSplit.TransTypeCode));
					objPrintWrapper.CurrentX = iFromX; 
					if(objFundsTransSplit.FromDate != "")
						objPrintWrapper.PrintText(Conversion.ToDate(objFundsTransSplit.FromDate).ToShortDateString());
					else
						objPrintWrapper.PrintText(objFundsTransSplit.FromDate);			
					objPrintWrapper.CurrentX = iToX; 
					if(objFundsTransSplit.ToDate != "")
						objPrintWrapper.PrintText(Conversion.ToDate(objFundsTransSplit.ToDate).ToShortDateString());
					else
						objPrintWrapper.PrintText(objFundsTransSplit.ToDate);
					objPrintWrapper.CurrentX = iInvNumX; 
					objPrintWrapper.PrintText(objFundsTransSplit.InvoiceNumber);
					objPrintWrapper.CurrentX = iInvAmtX; 
					objPrintWrapper.PrintText(string.Format("{0:C}",objFundsTransSplit.InvoiceAmount));
					sTemp = string.Format("{0:C}",objFundsTransSplit.Amount);
					objPrintWrapper.CurrentX = iAmountX - (objFundsTransSplit.InvoiceAmount.ToString().Length); 
					objPrintWrapper.PrintText(sTemp);
				}
				
				//now we print the stub info 
				objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim",false);
				
				objClaim.MoveTo(objFunds.ClaimId);
				foreach(Claimant objClaimant in objClaim.ClaimantList)
				{
					if(objFunds.ClaimId == objClaimant.ClaimId)   
						sClaimantFullName = objClaimant.ClaimantEntity.FirstName == string.Empty ? objClaimant.ClaimantEntity.LastName : objClaimant.ClaimantEntity.LastName + ", " + objClaimant.ClaimantEntity.FirstName; 
				}

				sEntityFullName = objFunds.PayeeEntity.FirstName == string.Empty ? objFunds.PayeeEntity.LastName : objFunds.PayeeEntity.LastName + ", " + objFunds.PayeeEntity.FirstName;

				objEvent = (Event)objDataModelFactory.GetDataModelObject("Event",false);
				objEvent.MoveTo(objClaim.EventId);

				objPrintWrapper.SetFont("Arial",9);

				dCurrY += dAvgHeight;
				objPrintWrapper.CurrentX = dCurrX;
				objPrintWrapper.CurrentY = dCurrY;
				objPrintWrapper.PrintText("");

				dCurrY += dAvgHeight;
				objPrintWrapper.CurrentX = dCurrX;
				objPrintWrapper.CurrentY = dCurrY;

				objPrintWrapper.PrintText("Claim Number: " + objClaim.ClaimNumber + "     Claimant: " + sClaimantFullName + "     Payee: " + sEntityFullName);  
				dCurrY += dAvgHeight;
				objPrintWrapper.CurrentX = dCurrX;
				objPrintWrapper.CurrentY = dCurrY;
				objPrintWrapper.PrintText("Check Number: " + objFunds.TransNumber + "     Total Check Amount: " + string.Format("{0:C}",objFunds.Amount) + "     Event Date: " + Conversion.GetDate(objEvent.DateOfEvent) + "     Department: " + GetDeptName(objEvent.EventId,objClaim.ClaimId, "DEPARTMENT"));
				dCurrY += dAvgHeight;
				objPrintWrapper.CurrentX = dCurrX;
				objPrintWrapper.CurrentY = dCurrY;
				if(objFunds.StatusCode == 1054)
					sCheckDate = Conversion.GetDate(objFunds.DateOfCheck);
				else
					sCheckDate = "Not Printed";
				objPrintWrapper.PrintText("Event Number: " +  objEvent.EventNumber + "     Control Number: " + objFunds.CtlNumber + "     Payee Tax ID: " + string.Format("{0:###-##-####}",objFunds.PayeeEntity.TaxId) + "     Date of Check: " + sCheckDate); 

				if(objClaim.CurrentAdjuster.CurrentAdjFlag)
				{
					try
					{
                        //zmohammad JIRA-5349 / MITs 37361 : Object hasnt been initialised. Fixed here.
                        objEntity = (Entity)objDataModelFactory.GetDataModelObject("Entity", false);
						objEntity.MoveTo(objClaim.CurrentAdjuster.AdjusterEid);
						dCurrY += dAvgHeight;
						objPrintWrapper.CurrentX = dCurrX;
						objPrintWrapper.CurrentY = dCurrY;
						if(objEntity.FirstName != string.Empty)
							objPrintWrapper.PrintText("Adjuster Name and Phone: " + objEntity.FirstName + " " + objEntity.LastName + string.Format("{0:(###) ###-####}",objEntity.Phone1));
						else
							objPrintWrapper.PrintText("Adjuster Name and Phone: " + objEntity.LastName + string.Format("{0:(###) ###-####}",objEntity.Phone1));
					}
					catch{}
				}

				objPrintWrapper.EndDoc();
				objPrintWrapper.Save(p_sPDFSaveFilePath);
                //abisht
                //Deb : MITS 32174
                if (m_objFunctions.m_bDirectToPrinter == true && Functions.IsPrinterSelected(m_sConnectionString,m_iClientId))
                    //m_objFunctions.PrintCheckJob(p_sPDFSaveFilePath);
                    m_objFunctions.PrintCheckJobNEW(p_sPDFSaveFilePath);
				return true ;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("PrintCheckStub.PrintDetail.ErrorPrint",m_iClientId) , p_objEx );	//sharishkumar Jira 835			
			}
			finally
			{

				if(objDataModelFactory != null)
					objDataModelFactory.Dispose();
				if(objPrintWrapper != null)
					objPrintWrapper = null;
				if(objFunds != null)
					objFunds.Dispose();
				if(objLocalCache != null)
					objLocalCache.Dispose();
				if(objClaim != null)
					objClaim.Dispose();
				if(objEntity != null)
					objEntity.Dispose();
				if(objEvent != null)
					objEvent.Dispose();
                // akaushik5 Added for MITS 35846 Starts
                if (this.m_objFunctions != null)
                {
                    this.m_objFunctions.Dispose();
                }
                // akaushik5 Added for MITS 35846 Ends
			}
		}

		#endregion
		
		#endregion

		#region "Private Functions"

        #region "Initialize"
        /// Name		: Initialize
        /// Author		: Alok Singh Bisht
        /// Date Created	: 08 Apr 2009		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Initialize;
        /// </summary>
        private void Initialize()
        {
            DataModelFactory objDataModelFactory = null;
            try
            {
                objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//sharishkumar Jira 835
                m_sConnectionString = objDataModelFactory.Context.DbConn.ConnectionString;
                m_objFunctions = new Functions(m_sDsnId, m_sUserName, m_sConnectionString,m_sPassword,m_sDsnName, m_iClientId);//rkaur27
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintEOBAR.Initialize.ErrorInit", m_iClientId), p_objEx);//sharishkumar Jira 835
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
            } // finally

        }
        #endregion

		#region "GetDeptName(long p_lTmpEventID, long p_lTmpClaimID,string p_sOrgLevel)"
		/// Name		: GetDeptName
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the department name.
		/// </summary>
		/// <param name="p_lTmpEventID">Event Id</param>
		/// <param name="p_lTmpClaimID">Claim Id</param>
		/// <param name="p_sOrgLevel">Organizational level</param>
		/// <returns>Name of the department</returns>
		private string GetDeptName(long p_lTmpEventID, long p_lTmpClaimID,string p_sOrgLevel)
		{
			string sSQL = "";
			string sOrgLevelName = "";			
			string sLN = "";
			string sFN = "";
			long lTmpDeptID = 0;
			long lOrgLevel = 0;
			DbReader objReader = null;

			try
			{
				sSQL = "SELECT DIARY_ORG_LEVEL FROM SYS_PARMS";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
				
				if (objReader.Read())
				{
					lOrgLevel = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("DIARY_ORG_LEVEL")));
					objReader.Close();
				}
				else
					lOrgLevel = 1012;
    
				//Get the Name of the org Hier level
				if (p_lTmpEventID != 0)
					sOrgLevelName = GetDiaryOrgName(false, p_lTmpEventID, lOrgLevel, p_sOrgLevel);
				else if (p_lTmpClaimID != 0)
					sOrgLevelName = GetDiaryOrgName(true, p_lTmpClaimID, lOrgLevel, p_sOrgLevel);

				if (sOrgLevelName == "")
				{
					if (p_lTmpEventID != 0)
					{
						sSQL = "SELECT DEPT_EID FROM EVENT WHERE EVENT_ID = " + p_lTmpEventID;
						objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
						
						if (objReader.Read())
						{
							lTmpDeptID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("DEPT_EID")));
							objReader.Close();
						}
					}
					else if (p_lTmpClaimID != 0)
					{
						sSQL = "SELECT DEPT_EID FROM CLAIM, EVENT WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM.CLAIM_ID = " + p_lTmpClaimID;
						objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
						
						if (objReader.Read())
						{
							lTmpDeptID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("DEPT_EID")));
							objReader.Close();
						}
					}
            
					sSQL = "SELECT LAST_NAME, FIRST_NAME FROM ENTITY WHERE ENTITY.ENTITY_ID = " + lTmpDeptID;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
					
					if (objReader.Read())
					{
						sFN = objReader.GetString("FIRST_NAME");
						sLN = objReader.GetString("LAST_NAME");

						if (sFN.Trim().Length > 0)
							sOrgLevelName = sFN + " " + sLN;
						else
							sOrgLevelName = sLN;

					}
					p_sOrgLevel = "Department";
				}
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintCheckStub.GetDeptName.Error", m_iClientId), p_objException);//sharishkumar Jira 835
			}
			finally 
			{
				if (objReader != null) 
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return(sOrgLevelName);
		}
		#endregion

		#region "GetDiaryOrgName(bool p_bClaimID, long p_lThisID, long p_lThisOrgLevel, string p_sOrgLevel)"
		/// Name		: GetDiaryOrgName
		/// Author		: Anurag Agarwal
		/// Date Created	: 13 Jan 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the name of the Organization
		/// </summary>
		/// <param name="p_bClaimID">Claim Id</param>
		/// <param name="p_lThisID">Name of the Event or Claim Id</param>
		/// <param name="p_lThisOrgLevel">Organizational level for the given Claim or Event</param>
		/// <param name="p_sOrgLevel">Organizational level</param>
		/// <returns>Name of the Organization</returns>
		private string GetDiaryOrgName(bool p_bClaimID, long p_lThisID, long p_lThisOrgLevel, string p_sOrgLevel)
		{
			string sTmp = "";
			string sSQL = "";			
			string sLN = "";
			string sFN = "";
			long lThisEID = 0;
			DbReader objReader = null;
		
			try
			{
				switch (p_lThisOrgLevel)
				{
					case 1005:   //client
						p_sOrgLevel = "CLIENT_EID";
						break;
					case 1006:   //company
						p_sOrgLevel = "COMPANY_EID";
						break;
					case 1007:   //operation 
						p_sOrgLevel = "OPERATION_EID";
						break;
					case 1008:   //region
						p_sOrgLevel = "REGION_EID";
						break;
					case 1009:   //division
						p_sOrgLevel = "DIVISION_EID";
						break;
					case 1010:   //location
						p_sOrgLevel = "LOCATION_EID";
						break;
					case 1011:   //facility
						p_sOrgLevel = "FACILITY_EID";
						break;
					case 1012:   //department
						p_sOrgLevel = "DEPARTMENT_EID";
						break;
					default:
						p_sOrgLevel = "";
						break;
				}

				lThisEID = 0;
				sTmp = "";
	    
				if (p_sOrgLevel != "")
				{
					if (p_bClaimID)
					{
						sSQL = "SELECT " + p_sOrgLevel + ", EVENT.DEPT_EID FROM CLAIM, EVENT, ORG_HIERARCHY WHERE CLAIM_ID = " + p_lThisID + " AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID";
						objReader = DbFactory.GetDbReader (m_sConnectionString,sSQL);
					
						if (objReader.Read())
						{
							lThisEID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue(0)));
							objReader.Close();
						}
						else
						{
							sSQL = "SELECT " + p_sOrgLevel + ", EVENT.DEPT_EID FROM EVENT, ORG_HIERARCHY WHERE EVENT_ID = " + p_lThisID + " AND ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID";
							objReader = DbFactory.GetDbReader (m_sConnectionString,sSQL);
	                    
							if (objReader.Read())
							{
								lThisEID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue(0)));
								objReader.Close();
							}
						}

						if (lThisEID != 0)
						{
							sSQL = "SELECT * FROM ENTITY WHERE ENTITY_ID = " + lThisEID;
							objReader = DbFactory.GetDbReader (m_sConnectionString,sSQL);
						
							if (objReader.Read())
							{
								sFN = objReader.GetString ("FIRST_NAME");
								sLN = objReader.GetString ("LAST_NAME");
								objReader.Close();

								if (sFN.Trim().Length > 0)
									sTmp = sFN + " " + sLN;
								else
									sTmp = sLN;
							}
						}
	        
						if (p_sOrgLevel != "") 
							p_sOrgLevel = p_sOrgLevel.Substring(0,p_sOrgLevel.Length -4);
							
					}
				}
			}

			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintCheckStub.GetDiaryOrgName.Error", m_iClientId), p_objException);//sharishkumar Jira 835
			}

			finally 
			{
				if (objReader != null) 
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return (sTmp);
		}
		#endregion

		#endregion
	}
}
