﻿
using System;
using System.Text;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using System.IO;
using Riskmaster.Settings;
using System.Configuration;


namespace Riskmaster.Application.VSSInterface
{
    /**************************************************************
     * $File		: VSSImportProcess.cs
     * $Revision	: 1.0.0.0
     * $Date		: 12/10/2012
     * $Author		: Anshul Verma
     * $Comment		: VSSImportProcess class has functions for importing data from VSS to RMX
     * $Source		:  	
    **************************************************************/
    /// <summary>
	/// VSSImportProcess class has functions for importing data from VSS to RMX
	/// </summary>
	internal class VSSImportProcess : IImportProcess
	{

		#region Variables Declaration
        /// <summary>
        /// Gets and sets the Riskmaster Database Connection String
        /// </summary>
        public string RiskmasterConnectionString
        {
            get;
            set;
        } // property RiskmasterConnectionString

		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Password
		/// </summary>
		private string m_sPassword="";
		/// <summary>
		/// DSN Name
		/// </summary>
		private string m_sDsnName="";

        private string logging = string.Empty;
        private string vSSExportUrl = string.Empty;
        private string vSSExportService = string.Empty;

        private int m_iClientId = 0;

        #region public properties
        public string Logging
        {
            get
            {
                return logging;
            }
            set
            {
                logging = value;
            }
        }

        public string VSSExportUrl
        {
            get
            {
                return vSSExportUrl;
            }
            set
            {
                vSSExportUrl = value;
            }
        }

        public string VSSExportService
        {
            get
            {
                return vSSExportService;
            }
            set
            {
                vSSExportService = value;
            }
        }
        #endregion

        UserLogin UserLoginInstance
        {
            get;
            set;
        }//property UserLoginInstance

        /// <summary>
        /// Writer to aid in HTML creation for VSS , MCM integration
        /// </summary>
        StreamWriter writer = null;
        private double dExchangeRateCl2Bs;
        private double dClaimAmt;
       
        private const int RMB_FUNDS_TRANSACT = 9650;
        private const int RMO_ALLOW_PAYMENT_CLOSED_CLM = 37;

		#endregion

		#region Constructor
        /// <summary>
        /// Overloaded Class constructor
        /// </summary>
        /// 
        /// <param name="p_sUserName">string containing the user name to log into the Riskmaster security database</param>
        /// <param name="p_sPassword">string containing the password to log into the Riskmaster security database</param>
        /// <param name="p_sDsnName">string containing the DSN Name selection</param>
        public VSSImportProcess(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
		{
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();
            RMConfigurationManager.GetVSSExportSettings();
            //Set the connection string for the Riskmaster Database
            this.RiskmasterConnectionString = VSSCommonFunctions.GetRiskmasterConnectionString(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
            this.UserLoginInstance = VSSCommonFunctions.GetUserLoginInstance(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
		}
		
		#endregion

		#region Public Execute Method
		/// Name		: Execute
		/// Author		: Manoj Agrawal/ Anshul Verma
		/// Date Created: 08/29/2006 - 12/10/2012
		/// <summary>		
		/// This method decides which type of import to call and then call appropriate method. This method is called from Web Service.
		/// </summary>
		public XmlDocument Execute(XmlDocument p_objXmlIn)
		{
			XmlNamespaceManager nsmgr = null;
			XmlDocument p_objXmlOut = null;
			DbConnection objConn = null;

			try
			{
				if(!p_objXmlIn.OuterXml.Equals(""))
				{
					nsmgr = new XmlNamespaceManager(p_objXmlIn.NameTable); 
					nsmgr.AddNamespace("csc", p_objXmlIn.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                    if (p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/ClaimsParty/ItemIdInfo/OtherIdentifier/OtherIdTypeCd",nsmgr) != null)
					{
                        if (p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/ClaimsParty/ItemIdInfo/OtherIdentifier/OtherIdTypeCd").InnerText.Equals("csc:AssignmentID"))
                            p_objXmlOut = VSSAssignmentImport(p_objXmlIn);

                        else if(p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/ClaimsParty/ClaimsPartyInfo/ClaimsPartyRoleCd",nsmgr)!=null)
                        {
                            if (p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/ClaimsParty/ClaimsPartyInfo/ClaimsPartyRoleCd").InnerText.Equals("csc:VendorAccount"))
                             p_objXmlOut = VSSVendorAccInfoImport(p_objXmlIn);
                        }

                        else if (p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/ClaimsPayment", nsmgr) != null)
                        {
                            p_objXmlOut = PaymentInvoiceImport(p_objXmlIn);
                        }
					}
					
					else if(p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/FileAttachmentInfo", nsmgr) != null)
					{
						p_objXmlOut = DocumentImport(p_objXmlIn);
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("VSSImportProcess.Execute.Error", m_iClientId), p_objEx);
			}
			finally
			{
				nsmgr = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                }
			}

			return p_objXmlOut;
		}
		#endregion

        #region Private Vss Assignment Import Method
		/// Name		: Vss Assignment Import
		/// Author		: Anshul Verma
		/// Date Created: 12/18/2012
		/// <summary>		
		/// This method is used to import Vss and Assignment information from VSS
		/// </summary>

        private XmlDocument VSSAssignmentImport(XmlDocument objVSSImportXml)
        {
            DataModelFactory objDMF=null;
			LocalCache objCache = null;
			DbReader objReaderTemp = null;
            Entity objEntity = null;
			DbConnection objConn=null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

			string sMainRqUID = "";

			string sRqUID = "";
			int iReqId = 0;
			int iEventId = 0;
            int iEntityId = 0;
			int iExistingVssAssRowId = 0;
            int iExistEntityId = 0;
            int iNewVssAssRowId=0;
			int iAssignmentId = 0;
            int iVssId=0;
            int iAccountId = 0;
			string sClaimNumber = "";
			string sAssignmentName= "";		
            string sAssignmentDueDate="";
            string sAccountNumber="";
            string sCompanyName="";
            string sTaxIdNumber="";
            string sAdjuster="";
            string sManager="";
            string sUnit="";
            string sAddr1="";
            string sAddr2="";
            string sCity="";
            string sStateId="";
            string sPostalCode="";
            string sPhone="";
            int iClaimId=0;
			bool bResponse = false;		//this is set to false when error - this is not used
			bool bError = false;		//this is set to true when error
            SysSettings objSettings = null;

            try
            {
                //logging2 - enter initial records in loggin tables				
                if (VSSExportSection.Logging.ToUpper().Equals("YES"))
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
				}
                VSSCommonFunctions.iLogDXRequest(iReqId, "VSSASS", "TORM", "VSS", "STRT", objVSSImportXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				objCache=new LocalCache(this.RiskmasterConnectionString,m_iClientId);

				nsmgr = new XmlNamespaceManager(objVSSImportXml.NameTable);
				nsmgr.AddNamespace("csc", objVSSImportXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				if(objVSSImportXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
					sMainRqUID = objVSSImportXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;

               foreach (XmlNode objXmlNode in objVSSImportXml.SelectNodes("//ClaimsSubsequentRptSubmitRq", nsmgr))
				{
                    objEntity = null;
					iExistingVssAssRowId = 0;
                    iExistEntityId = 0;
                    iNewVssAssRowId=0;
					iAssignmentId = 0;
                    iVssId=0;
                    iClaimId=0;
                    iEntityId = 0;
                    iAccountId = 0;

                    sRqUID = "";
                    sClaimNumber = "";
                    sAdjuster = "";
                    sAddr1 = "";
                    sAddr2 = "";
                    sCompanyName = "";
                    sStateId = "";
                    sManager = "";

					bResponse = false;
					bError = false;

					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;

					if(bError == false)
					{
                        //Assignment Id
						iAssignmentId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:AssignmentID", "OtherId"));
                        //Vss Id
                        iVssId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:VendorID", "OtherId"));
                        //mandatory - Claim Number
                        sClaimNumber = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:ClaimNum", "OtherId");
						//Assignment Name
                        sAssignmentName = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/AssignmentInfo/AssignmentName", nsmgr);
						//Assignment Due Date
                        sAssignmentDueDate = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/AssignmentInfo/AssignmentDueDt", nsmgr);
						//Assignment Account Number
                        sAccountNumber = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/AccountIdentity/AccountNo", nsmgr);
                        //AccountI Id
                        iAccountId = Common.Conversion.ConvertStrToInteger(VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/AccountIdentity/AccountId", nsmgr));
                        //Company Name - Vendor Name
                        sCompanyName = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/NameInfo/CommlName/CommercialName", nsmgr);
                        //Address1
                        sAddr1 = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Addr/Addr1", nsmgr);
                        //Address2
                        sAddr2 = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Addr/Addr2", nsmgr);
                        //City
                        sCity = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Addr/City", nsmgr);
                        //State id
                        sStateId = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Addr/StateProvCd", nsmgr);
                        //Postal Code
                        sPostalCode = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Addr/Zip", nsmgr);
                        //Phone
                        sPhone = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber", nsmgr);
                        // Tax Id Number
                        sTaxIdNumber = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/NameInfo/TaxIdentity/TaxId", nsmgr);
                        //Manager Name
                        sManager = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/ClaimRoles/csc:Manager/csc:LastName", nsmgr) + " " + VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/ClaimRoles/csc:Manager/csc:FirstName ", nsmgr);
                        //Adjuster Name
                        sAdjuster = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/ClaimRoles/csc:Adjuster/csc:LastName", nsmgr) + " " + VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/ClaimRoles/csc:Adjuster/csc:FirstName ", nsmgr);

						if(sClaimNumber.Equals(""))
						{
							AddError(ref objResponse, "VSSAssignmentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Claim Number}");
							bResponse = false;		
							bError = true;
						}
						else if(sAssignmentName.Equals(""))
						{
							AddError(ref objResponse, "VSSAssignmentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Assignment Name}");
							bResponse = false;		
							bError = true;
						}
                        else if(sAccountNumber.Equals(""))
						{
							AddError(ref objResponse, "VSSAssignmentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Account Number}");
							bResponse = false;		
							bError = true;
						}
					}
                   
					if(bError == false)
					{
						//Claim number exists in rmA domain
						iClaimId = GetRowIdFromOtherField("CLAIM_ID", "CLAIM", "CLAIM_NUMBER", "'" + sClaimNumber + "'");

                        if (iClaimId == 0)
						{
							AddError(ref objResponse, "VSSAssignmentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
							bResponse = false;		
							bError = true;
						}
					}

					if(bError == false)		//everything is ok and there is no error till this point. create document in RMX.
					{
						//logging3 - begin update
                        VSSCommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);
                        //either VSS assignment already exists and we need to update that or create new one. decide this based on assignment id and vss id.
                        objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT ROW_ID,VENDOR_EID FROM CLAIM_X_VSSASSIGNMENT WHERE ASSIGNMENT_ID = '" + iAssignmentId + "'");
							
                        if (objReaderTemp.Read())
							{
								iExistingVssAssRowId =Common.Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                iExistEntityId = Common.Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);
							}
							objReaderTemp.Close();

                            if (iExistingVssAssRowId == 0)
							{
                                //create/update entity with provided Firm information
                                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                                objEntity.EntityTableId = objCache.GetTableId("VENDOR");

                                objEntity.LastName = sCompanyName;
                                objEntity.Addr1 = sAddr1;
                                objEntity.Addr2 = sAddr2;
                                objEntity.City = sCity;
                                objEntity.StateId = objCache.GetCodeId(sStateId, "STATES"); ;
                                objEntity.ZipCode = sPostalCode;
                                objEntity.TaxId = sTaxIdNumber;
                                objEntity.Phone1 = sPhone;
                                //  Set the entity approval status "APPROVED" by default for all the new entities that are coming from VSS. 
                                objEntity.EntityApprovalStatusCode = objCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                                objEntity.Save();
                                //assign newly created Entity id to iEntityId
                                iEntityId = objEntity.EntityId;
                                objEntity.Dispose();
                                objEntity = null;

                                if (iEntityId == 0)
                                {
                                    AddError(ref objResponse, "VSSAssignmentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value: {Entity not found.}");
                                    bResponse = false;
                                    bError = true;
                                }

                                //set the added by field value VSSINF if new record was added.
                                SetField("ENTITY", "ADDED_BY_USER", "VSSINF", "ENTITY_ID", iEntityId.ToString());

								//document does not exist so create new one
                                iNewVssAssRowId = Utilities.GetNextUID(this.RiskmasterConnectionString, "CLAIM_X_VSSASSIGNMENT", m_iClientId);

								objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
								objConn.Open();
                                //create the VSS assignment entry in CLAIM_X_VSSASSIGNMENT table.
                                objConn.ExecuteNonQuery("INSERT INTO CLAIM_X_VSSASSIGNMENT (ROW_ID, ASSIGNMENT_ID, VENDOR_EID, ASSIGNMENT_NAME, ASSIGNMENT_DUE_DATE, ACCOUNT_NUMBER,ADJUSTER,MANAGER,CLAIM_ID) VALUES (" + iNewVssAssRowId + ", '" + iAssignmentId + "','" + iEntityId + "', '" + sAssignmentName + "', '" + sAssignmentDueDate + "', '" + sAccountNumber + "','" + sAdjuster + "', '" + sManager + "', '" + iClaimId + "')");
								objConn.Close();
								//objConn=null;
							}
							else
							{
                                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                                objEntity.MoveTo(iExistEntityId);
                                objEntity.LastName = sCompanyName;
                                objEntity.Addr1 = sAddr1;
                                objEntity.Addr2 = sAddr2;
                                objEntity.City = sCity;
                                objEntity.StateId = objCache.GetCodeId(sStateId, "STATES");
                                objEntity.ZipCode = sPostalCode;
                                objEntity.TaxId = sTaxIdNumber;
                                objEntity.Phone1 = sPhone; 
                                objEntity.EntityApprovalStatusCode = objCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                                objEntity.Save();
                                objEntity.Dispose();
                                objEntity = null;
                                //document already exists so update that
								objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
								objConn.Open();

                                //update the VSS assignment entry in CLAIM_X_VSSASSIGNMENT table.
                                objConn.ExecuteNonQuery("UPDATE CLAIM_X_VSSASSIGNMENT SET ASSIGNMENT_NAME = '" + sAssignmentName + "', ASSIGNMENT_DUE_DATE = '" + sAssignmentDueDate + "', ACCOUNT_NUMBER = '" + sAccountNumber + "',ADJUSTER='" + sAdjuster + "', MANAGER='" + sManager + "', CLAIM_ID= '" + iClaimId + "'   WHERE ROW_ID = " + iExistingVssAssRowId);
								objConn.Close();
							}
						}
						//logging4 - end update
                    VSSCommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "VSSAssignmentResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;
                }//foreach
			}//try	
            catch (Exception p_objException)
                {
                    AddError(ref objResponse, "VSSAssignmentResponse", sRqUID, "FAILED", "V110", "Data push failed: Unknown Error: " + p_objException.Message);
                    bResponse = false;
                    bError = true;
                    //logging5 - log exception
                    VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                }
            finally
            {
                nsmgr = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objReaderTemp != null)
                {
                    objReaderTemp.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
                if (objSettings != null)
                {
                    objSettings = null;
                }
            }

            //logging6 - begin response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

            objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
            if (objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
                objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

            foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
            {
                objOrg.InnerText = m_sDsnName;
            }

            //logging7 - end response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

            //logging8 - final entry
            if (bError == false)
            {
                VSSCommonFunctions.iLogDXRequest(iReqId, "VSSASS", "TORM", "VSS", "CMPD", objVSSImportXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
            }
            else
            {
                VSSCommonFunctions.iLogDXRequest(iReqId, "VSSASS", "TORM", "VSS", "FAIL", objVSSImportXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
            }

            return objResponse;
        }

        #endregion

        #region Private Vss Vendor Profile Information Import Method
        /// Name		: Vss Vendor Profile Import
        /// Author		: Anshul Verma
        /// Date Created: 01/07/2013
        /// <summary>		
        /// This method is used to import Vss Vendor profile information to rmA
        /// </summary>

        private XmlDocument VSSVendorAccInfoImport(XmlDocument objVSSVendorProfileXml)
        {
            DataModelFactory objDMF = null;
            LocalCache objCache = null;
            DbReader objReaderTemp = null;
            Entity objEntity = null;
            DbConnection objConn = null;

            XmlDocument objResponse = null;
            XmlNamespaceManager nsmgr = null;

            string sMainRqUID = "";

            string sRqUID = "";
            int iReqId = 0;
            int iEventId = 0;
            int iEntityId = 0;
            int iExistEntityId = 0;
            int iExistingVssAssRowId = 0;
            int iNewVssAssRowId=0;
            int iVssId = 0;
            int iAccountId = 0;
            string sAccountNumber = "";
            string sCompanyName = "";
            string sTaxIdNumber = "";
            string sAddr1 = "";
            string sAddr2 = "";
            string sCity = "";
            string sStateId = "";
            string sPostalCode = "";
            string sPhone = "";

            bool bResponse = false;		//this is set to false when error - this is not used
            bool bError = false;		//this is set to true when error
            SysSettings objSettings = null;

            try
            {
                //logging2 - enter initial records in loggin tables				
                if (VSSExportSection.Logging.ToUpper().Equals("YES"))
                {
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
                }
                VSSCommonFunctions.iLogDXRequest(iReqId, "VENACC", "TORM", "VSS", "STRT", objVSSVendorProfileXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
                objCache = new LocalCache(this.RiskmasterConnectionString, m_iClientId);

                nsmgr = new XmlNamespaceManager(objVSSVendorProfileXml.NameTable);
                nsmgr.AddNamespace("csc", objVSSVendorProfileXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                if (objVSSVendorProfileXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
                    sMainRqUID = objVSSVendorProfileXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;

                foreach (XmlNode objXmlNode in objVSSVendorProfileXml.SelectNodes("//ClaimsSubsequentRptSubmitRq", nsmgr))
                {
                    objEntity = null;
                    iExistEntityId = 0;
                    iNewVssAssRowId = 0;
                    iAccountId = 0;
                    sRqUID = "";
                    iEntityId = 0;
                    bResponse = false;
                    bError = false;

                    if (objXmlNode.SelectSingleNode("RqUID") != null)
                        sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
                    else
                        sRqUID = sMainRqUID;

                    if (bError == false)
                    {
                        //VENDDOR ID
                       // iVssId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:VendorId", "OtherId"));
                        //Account Id
                        iAccountId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:AccountId", "OtherId"));
                        //Assignment Account Number
                        sAccountNumber = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:AccountNumber", "OtherId");
                        //Company Name - Vendor Name
                        sCompanyName = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/csc:NameInfo/csc:CompanyName", nsmgr);
                        //Address1
                        sAddr1 = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Communications/AddressInfo/Address1", nsmgr);
                        //Address2
                        sAddr2 = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Communications/AddressInfo/Address2", nsmgr);
                        //City
                        sCity = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Communications/AddressInfo/City", nsmgr);
                        //State id
                        sStateId = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Communications/AddressInfo/State", nsmgr);
                        //Postal Code
                        sPostalCode = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Communications/AddressInfo/Zip", nsmgr);
                        //Phone
                        sPhone = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber", nsmgr);
                        // Tax Id Number
                        sTaxIdNumber = VSSCommonFunctions.GetValue(objXmlNode, "ClaimsParty/GeneralPartyInfo/tax_id_number", nsmgr);

                        if (iAccountId.Equals(""))
                        {
                            AddError(ref objResponse, "VSSVendorAccInfoResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Account Id}");
                            bResponse = false;
                            bError = true;
                        }
                        else if (sCompanyName.Equals(""))
                        {
                            AddError(ref objResponse, "VSSVendorAccInfoResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Company Name}");
                            bResponse = false;
                            bError = true;
                        }
                        else if (sAccountNumber.Equals(""))
                        {
                            AddError(ref objResponse, "VSSVendorAccInfoResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Account Number}");
                            bResponse = false;
                            bError = true;
                        }
                    }

                    if (bError == false)		//everything is ok and there is no error till this point. create document in RMX.
                    {
                        //logging3 - begin update
                        VSSCommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);
                        //either VSS assignment already exists and we need to update that or create new one. decide this based on assignment id and vss id.
                        objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT ROW_ID,VENDOR_EID FROM CLAIM_X_VSSASSIGNMENT WHERE ACCOUNT_ID='" + iAccountId + "' AND ACCOUNT_NUMBER='"+sAccountNumber+"' ");

                        if (objReaderTemp.Read())
                        {
                            iExistingVssAssRowId = Common.Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            iExistEntityId = Common.Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);
                        }
                        objReaderTemp.Close();

                        if (iExistEntityId == 0 && iExistingVssAssRowId==0)
                        {
                            //create/update entity with provided Firm information
                            objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                            objEntity.EntityTableId = objCache.GetTableId("VENDOR");

                            objEntity.LastName = sCompanyName;
                            objEntity.Addr1 = sAddr1;
                            objEntity.Addr2 = sAddr2;
                            objEntity.City = sCity;
                            objEntity.StateId = objCache.GetCodeId(sStateId, "STATES");
                            objEntity.ZipCode = sPostalCode;
                            objEntity.TaxId = sTaxIdNumber;
                            objEntity.Phone1 = sPhone;
                            //  Set the entity approval status "APPROVED" by default for all the new entities that are coming from VSS. 
                            objEntity.EntityApprovalStatusCode = objCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                            objEntity.Save();
                            //assign newly created Entity id to iEntityId
                            iEntityId = objEntity.EntityId;
                            objEntity.Dispose();
                            objEntity = null;

                            if (iEntityId == 0)
                            {
                                AddError(ref objResponse, "VSSVendorAccInfoResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value: {Entity not found.}");
                                bResponse = false;
                                bError = true;
                            }

                            //set the added by field value VSSINF if new record was added.
                            SetField("ENTITY", "ADDED_BY_USER", "VSSINF", "ENTITY_ID", iEntityId.ToString());

                            //document does not exist so create new one
                            iNewVssAssRowId = Utilities.GetNextUID(this.RiskmasterConnectionString, "CLAIM_X_VSSASSIGNMENT", m_iClientId);

                            objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                            objConn.Open();
                            //create the VSS assignment entry in CLAIM_X_VSSASSIGNMENT table.
                            objConn.ExecuteNonQuery("INSERT INTO CLAIM_X_VSSASSIGNMENT (ROW_ID, VENDOR_EID, ACCOUNT_NUMBER, ACCOUNT_ID) VALUES (" + iNewVssAssRowId + ", '" + iEntityId + "', '" + sAccountNumber + "', '"+iAccountId+"')");
                            objConn.Close();
                            //objConn=null;
                        }
                        else
                        {
                            objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                            objEntity.MoveTo(iExistEntityId);
                            objEntity.LastName = sCompanyName;
                            objEntity.Addr1 = sAddr1;
                            objEntity.Addr2 = sAddr2;
                            objEntity.City = sCity;
                            objEntity.StateId = objCache.GetCodeId(sStateId, "STATES");
                            objEntity.ZipCode = sPostalCode;
                            objEntity.TaxId = sTaxIdNumber;
                            objEntity.Phone1 = sPhone;
                            objEntity.EntityApprovalStatusCode = objCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                            objEntity.Save();
                            objEntity.Dispose();
                            objEntity = null;
                            //document already exists so update that
                            objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                            objConn.Open();

                            //update the VSS assignment entry in CLAIM_X_VSSASSIGNMENT table.
                            objConn.ExecuteNonQuery("UPDATE CLAIM_X_VSSASSIGNMENT SET ACCOUNT_NUMBER = '" + sAccountNumber + "' WHERE ACCOUNT_ID = " + iAccountId);
                            objConn.Close();
                        }
                    }
                    //logging4 - end update
                    VSSCommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

                    AddError(ref objResponse, "VSSVendorAccInfoResponse", sRqUID, "Success", "0", "Success");
                    bResponse = true;
                    bError = false;
                }//foreach
            }//try	
            catch (Exception p_objException)
            {
                AddError(ref objResponse, "VSSVendorAccInfoResponse", sRqUID, "FAILED", "V110", "Data push failed: Unknown Error: " + p_objException.Message);
                bResponse = false;
                bError = true;
                //logging5 - log exception
                VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
            }
            finally
            {
                nsmgr = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objReaderTemp != null)
                {
                    objReaderTemp.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
                if (objSettings != null)
                {
                    objSettings = null;
                }
                if (objEntity != null)
                {
                    objEntity = null;
                }
            }

            //logging6 - begin response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

            objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
            if (objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
                objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

            foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
            {
                objOrg.InnerText = m_sDsnName;
            }

            //logging7 - end response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

            //logging8 - final entry
            if (bError == false)
            {
                VSSCommonFunctions.iLogDXRequest(iReqId, "VSSASS", "TORM", "VSS", "CMPD", objVSSVendorProfileXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
            }
            else
            {
                VSSCommonFunctions.iLogDXRequest(iReqId, "VSSASS", "TORM", "VSS", "FAIL", objVSSVendorProfileXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
            }

            return objResponse;
        }
        #endregion

		#region Private Payment/Invoice Import Method
        /// Name		: PaymentInvoice Import
		/// Author		: Anshul Verma
		/// Date Created: 19/12/2012
		/// <summary>		
		/// This method is used to import Payment from VSS
		/// </summary>
		private XmlDocument PaymentInvoiceImport(XmlDocument objPaymentInvoiceXml)
		{
			DataModelFactory objDMF=null;
			Funds objFunds = null;
			FundsTransSplit objFundsTransSplit = null;
            ReserveCurrent objReserveCurr = null;
			Entity objEntity = null;
			LocalCache objCache = null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

            
            int iWCLOBCode = 243;
            int iDILOBCode = 844;
            //gagnihotri MITS 12437 10/30/2008
            int iGCLOBCode = 241;
            int iVALOBCode = 242;           //gagnihotri MITS 14376 02/12/2009
            int iReserveTracking = 0;

			string sMainRqUID = "";

			//Funds record
            int iRcRowId = 0;
            int iPolicyCovRowId = 0;;
            int iVssAssignmentId = 0;
            int iAccountId = 0;
            int iSubAccountId=0;
            string sLossCode = string.Empty;   //Disability code or loss code
            string sDisabilityCat = string.Empty;
            string sTransTypeCode = string.Empty;
            string sResTypecode = string.Empty;

			int iTransId = 0;
			string sRqUID = "";
			int iEntityId = 0;
			int iInvoiceId = 0;

			string sClaimNumber = "";
			int iClaimId = 0;
            int iLOBCode = 0;
            int iClaimStatusCode=0;
            int iClaimantEid = 0;
			double dInvoiceAmt = 0;
			double dFinalInvoiceAmt = 0;
			string sInvoiceNum = "";
			string sInvoiceDt = "";

			bool bResponse = false;		//this is set to false when error - this is not used
			bool bError = false;		//this is set to true when error

			//logging1
			int iReqId = 0;
			int iEventId = 0;		
            string sSQLTemp = "";
          
            //Multicurrency
            SysParms objSysSetting = null;
            int iClaimCurrCode = 0;
            double dExcgB2C = 0.0;    //skhare7 MITS 27671
            FundsXPayee objFundsXPayee = null; 
			try
			{
                objSysSetting = new SysParms(this.RiskmasterConnectionString, m_iClientId);
				//logging2 - enter initial records in loggin tables

                if (VSSExportSection.Logging.ToUpper().Equals("YES"))
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
				}
                VSSCommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "VSS", "STRT", objPaymentInvoiceXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
                objCache = new LocalCache(this.RiskmasterConnectionString, m_iClientId);

                nsmgr = new XmlNamespaceManager(objPaymentInvoiceXml.NameTable);
                nsmgr.AddNamespace("csc", objPaymentInvoiceXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                if (objPaymentInvoiceXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
                    sMainRqUID = objPaymentInvoiceXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;
                
				//following for loop will iterate through all the payments coming in XML
                foreach (XmlNode objXmlNode in objPaymentInvoiceXml.SelectNodes("//ClaimsSubsequentRptSubmitRq"))
				{
					objFunds = null;
					objFundsTransSplit = null;
					objEntity = null;

                    iRcRowId = 0;
					iTransId = 0;
					sRqUID = "";
					iEntityId = 0;
					iInvoiceId = 0;
					sClaimNumber = "";
					iClaimId = 0;
					dInvoiceAmt = 0;
					dFinalInvoiceAmt = 0;
                    iAccountId =0;
                    iSubAccountId=0;
					sInvoiceNum = "";
					sInvoiceDt = "";

					bResponse = false;
					bError = false;
					
	
                    bool allowPaymentToClosedClaims = false;//Added by bsharma33 for MITS 26860

					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;

                    iVssAssignmentId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:AssignmentID", "OtherId"));

                    iRcRowId = Common.Conversion.ConvertStrToInteger(objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/ReserveID", nsmgr).InnerText);

					iInvoiceId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:InvoiceId", "OtherId"));

                    iPolicyCovRowId = Common.Conversion.ConvertStrToInteger(objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/PolicyCoverageRowId", nsmgr).InnerText);

                    if (!string.IsNullOrEmpty(objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/LossCd", nsmgr).InnerText))
                    {
                        sLossCode = objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/LossCd", nsmgr).InnerText;
                    }
                    else
                    {
                        sLossCode = objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/DisabilityCd", nsmgr).InnerText;
                    }
                    
                    sDisabilityCat = objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/DisabilityCat", nsmgr).InnerText;

                    sClaimNumber = objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/ClaimNumber", nsmgr).InnerText;

                    sTransTypeCode = objXmlNode.SelectSingleNode("//ClaimsParty/ClaimPartyInfo/TransactionTypeCode", nsmgr).InnerText;
                    sResTypecode = objCache.GetRelatedShortCode(objCache.GetCodeId(sTransTypeCode, "TRANS_TYPES"));

                    using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                    {
                        if (objReaderTemp.Read())
                        {
                            iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            iLOBCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);
                            iClaimCurrCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(2), m_iClientId);
                        }
                    }

                    if (objXmlNode.SelectSingleNode("//csc:OrigInvoiceAmt/Amt", nsmgr) != null)
                        dInvoiceAmt = Common.Conversion.ConvertStrToDouble(objXmlNode.SelectSingleNode("//csc:OrigInvoiceAmt/Amt", nsmgr).InnerText);

                    if (objXmlNode.SelectSingleNode("//csc:FinalPaymentAmt/Amt", nsmgr) != null)
                        dFinalInvoiceAmt = Common.Conversion.ConvertStrToDouble(objXmlNode.SelectSingleNode("//csc:FinalPaymentAmt/Amt", nsmgr).InnerText);

                    objSysSetting = new SysParms(this.RiskmasterConnectionString, m_iClientId);

                    dClaimAmt = dFinalInvoiceAmt;

                    if (iClaimCurrCode != 0 && (objSysSetting.SysSettings.UseMultiCurrency == -1))
                    {
                        double dExchangeRateC2B = Common.CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, this.RiskmasterConnectionString, m_iClientId);
                        
                        dInvoiceAmt = dExchangeRateC2B * dInvoiceAmt;
                    
                        dFinalInvoiceAmt = dFinalInvoiceAmt * dExchangeRateC2B;

                        dExcgB2C = Common.CommonFunctions.ExchangeRateSrc2Dest(Common.CommonFunctions.GetBaseCurrencyCode(this.RiskmasterConnectionString), iClaimCurrCode, this.RiskmasterConnectionString, m_iClientId);
                    }

                    sInvoiceNum = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:VendorInvNum", "OtherId");

					sInvoiceDt = ReadTagValue(objXmlNode, "EventInfo", "csc:InvoiceDate", "EventDt");

                    if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                    {
                        dExchangeRateCl2Bs = Common.CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, this.RiskmasterConnectionString, m_iClientId);
                    }

                    if (iVssAssignmentId.Equals(0))
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Assignment Id for the payment}");
                        bResponse = false;
                        bError = true;
                    }

                    if (iRcRowId.Equals(0))
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Reserve Id}");
                        bResponse = false;
                        bError = true;
                    }
					else if(sClaimNumber.Equals(""))
					{
						AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Claim Number}");
						bResponse = false;		
						bError = true;
					}
					else if(dFinalInvoiceAmt == 0)
					{
						AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value: {Final Invoice Amt: zero}");
						bResponse = false;		
						bError = true;
					}
                    else if (objCache.GetCodeId(sResTypecode, "RESERVE_TYPE") <= 0)//check if Reserve type is valid
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value:  {Reserve Type: " + sResTypecode + "}");
                        bResponse = false;
                        bError = true;
                    }
                    else if(objCache.GetCodeId(sTransTypeCode, "TRANS_TYPES") <= 0)//check if Trans type is valid
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value:  {Transaction Type: " + sTransTypeCode + "}");
                            bResponse = false;
                            bError = true;
                    }

                    if (bError == false)
                    {
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT ACCOUNT_ID,SUB_ACCOUNT_ID FROM CLAIM_X_VSSASSIGNMENT WHERE ASSIGNMENT_ID = " + iVssAssignmentId + ""))
                        {
                            if (objReaderTemp.Read())
                            {
                                iAccountId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                iSubAccountId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);

                                if (iAccountId.Equals(0))
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V111", "Payment: Account Id is not setup in rmA for this Vendor..");
                                    bResponse = false;
                                    bError = true;
                                }
                            }
                        }
                    }

					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//check if claim exists
						iClaimId = 0;
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE,CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                        {
						    if(objReaderTemp.Read())
						    {
							    iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                iLOBCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);
                                iClaimStatusCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(2), m_iClientId);
                                if (objCache.GetRelatedShortCode(iClaimStatusCode) == "C")
                                {
                                    if (!this.UserLoginInstance.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_PAYMENT_CLOSED_CLM))
                                    {
                                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V111", "Payment: Not allowed on closed claims.");
                                        bResponse = false;
                                        bError = true;
                                    }
                                    else
                                    {
                                        allowPaymentToClosedClaims = true;
                                    }
                                }
						    }
						}

						if(iClaimId == 0)
						{
							AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
							bResponse = false;		
							bError = true;
						}
					}

					//check for insufficient reserve
					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						double dAmount = 0;
						double dblBalance = 0;
						int iReserveType = 0;
						int iLob = 0;

                        objReserveCurr = (ReserveCurrent)objDMF.GetDataModelObject("ReserveCurrent", false);
                        objReserveCurr.MoveTo(iRcRowId);
	
						//in following loop we will put amount based on Reserve types. because Insufficient Reserves would be calculated based on Reserve types.
						//note: in arrTransType we keep short code of Trans Type. 
						//note: in arrReserveType we keep code id of Reserve Type
									

                        if (allowPaymentToClosedClaims == false) //if allowPaymentToClosedClaims = true no need to check funds reserve, will be handled automatically in Funds.cs save() function.
                        {
                             dAmount = objReserveCurr.ReserveAmount;
                             iReserveType = objCache.GetCodeId(sResTypecode, "RESERVE_TYPE");
                             iLob = iLOBCode;
                             dblBalance = objReserveCurr.BalanceAmount;
                             if (((objReserveCurr.ReserveAmount - objReserveCurr.BalanceAmount) < 0.009) && dFinalInvoiceAmt < 0)
                             {
                                //reserves are insufficient. raise error.
                                AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V109", "Payment: Reserve not met.");
                                bResponse = false;
                                bError = true;
                             }
                        }
					}
                    if (dFinalInvoiceAmt<=0) 
                    {
                        //Collection amount is greater than Paid Total. raise error.
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V109", "Payment: Payment amount should be greater than 0");
                        bResponse = false;
                        bError = true;
                        break;
                    }

                   if (dFinalInvoiceAmt>objReserveCurr.BalanceAmount)
                   {
                       //Collection amount is greater than Paid Total. raise error.
                       AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V109", "Payment: Inadequate Reserve Balance in RISKMASTER");
                       bResponse = false;
                       bError = true;
                       break;
                   }

					//check if payee exists. if yes, update that. if no, create new.
					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//check if payee exists
						iEntityId = 0;
                        sSQLTemp = "SELECT VENDOR_EID FROM CLAIM_X_VSSASSIGNMENT WHERE ASSIGNMENT_ID = " + iVssAssignmentId;
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQLTemp))
                        {
                            if (objReaderTemp.Read())
                            {
                                iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            }
                            else
                            {
                                AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V114", "Data push failed:{Payee for Assignment id: " + iVssAssignmentId + " not found.}");
                                bResponse = false;
                                bError = true;
                            }
                        }
					}

                    if (objCache.GetShortCode(objReserveCurr.ReserveTypeCode) != sResTypecode)
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V111", "Payment: Reserve sent from VSS not matched to the rmA reserve type");
                        bResponse = false;
                        bError = true;
                        break;
                    }

					//if everything is ok and there is no error till this point
					//then we can create payment if bError == false
					if(bError == false)		
					{
						iTransId = 0;

						objFunds = (Funds)objDMF.GetDataModelObject("Funds", false);
						//logging3 - begin update
                        VSSCommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

						objFunds.ClaimId = iClaimId;
						objFunds.ClaimNumber = sClaimNumber;

                        //Set claimant_Eid for WC/DI claim
                        iClaimantEid = objReserveCurr.ClaimantEid;
                        if (iClaimantEid > 0)
                        {
                            objFunds.ClaimantEid = iClaimantEid;
                        }

                        objFunds.AccountId = iAccountId;
                        objFunds.SubAccountId = iSubAccountId;
						objFunds.Amount = dFinalInvoiceAmt;
                        objFunds.TransDate = sInvoiceDt;
                        objFunds.DateOfCheck = sInvoiceDt;		//as per Jeff Blanchard we should update this with Trans Date, this is the way we are doing in RM. when check will be printed then this date will be overwritten.
                        objFunds.ClaimCurrencyAmount = dClaimAmt;
                        objFunds.PmtCurrencyAmount = dClaimAmt;
                        if (dFinalInvoiceAmt < 0)
                        {
                            objFunds.CollectionFlag = true;
                            objFunds.PaymentFlag = false;
                            objFunds.Amount = objFunds.Amount * (-1);
                        }
                        else
                        {
                            objFunds.PaymentFlag = true;
                            objFunds.CollectionFlag = false;
                        }

                        if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                        {
                            objFunds.BaseToClaimCurRate = dExcgB2C;
                            objFunds.ClaimCurrencyType = iClaimCurrCode;
                            objFunds.PmtCurrencyType = iClaimCurrCode;
                            objFunds.PmtToBaseCurRate = dExchangeRateCl2Bs;
                            objFunds.PmtToClaimCurRate = 1.0;
                            objFunds.BaseToPmtCurRate = dExcgB2C;
                        }
                        else
                        {
                            objFunds.BaseToClaimCurRate = 1.0; 
                            objFunds.PmtToBaseCurRate = 1.0; 
                            objFunds.PmtToClaimCurRate = 1.0;
                            objFunds.BaseToPmtCurRate = 1.0;
                        }

                        objFunds.StatusCode = objCache.GetCodeId("R", "CHECK_STATUS");	//released status
						objFunds.PayeeTypeCode = objCache.GetCodeId("O", "PAYEE_TYPE");	//Other payee

						objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
						objEntity.MoveTo(iEntityId);

						objFunds.PayeeEid = iEntityId;
						objFunds.FirstName = objEntity.FirstName;
						objFunds.LastName = objEntity.LastName;
						objFunds.Addr1 = objEntity.Addr1;
						objFunds.Addr2 = objEntity.Addr2;
						objFunds.City = objEntity.City;
						objFunds.StateId = objEntity.StateId;
						objFunds.CountryCode = objEntity.CountryCode;
						objFunds.ZipCode = objEntity.ZipCode;
                  
						objEntity.Dispose();
						objEntity = null;
						
                        // Create an split corresponding to VSS Payment
                        objFundsTransSplit = (FundsTransSplit)objDMF.GetDataModelObject("FundsTransSplit", false);

                        objFundsTransSplit.InvoiceNumber = sInvoiceNum;
                        objFundsTransSplit.InvoiceDate = sInvoiceDt;
                        objFundsTransSplit.RCRowId = iRcRowId;
                        objFundsTransSplit.CoverageId = objReserveCurr.CoverageId;
                        objFundsTransSplit.TransTypeCode = objCache.GetCodeId(sTransTypeCode, "TRANS_TYPES");
                        objFundsTransSplit.ReserveTypeCode = objCache.GetRelatedCodeId(objCache.GetCodeId(sTransTypeCode, "TRANS_TYPES"));
                        objFundsTransSplit.Amount = dFinalInvoiceAmt;

                        if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                        {
                            objFundsTransSplit.ClaimCurrencyAmount = objFundsTransSplit.Amount * dExcgB2C;
                            objFundsTransSplit.PmtCurrencyAmount = objFundsTransSplit.Amount * dExcgB2C;
                        }
                        else
                        {
                            objFundsTransSplit.ClaimCurrencyAmount = objFundsTransSplit.Amount;
                            objFundsTransSplit.PmtCurrencyAmount = objFundsTransSplit.Amount;
                        }
                        if (dFinalInvoiceAmt < 0)
                            objFundsTransSplit.Amount = objFundsTransSplit.Amount * (-1);

                        objFunds.TransSplitList.Add(objFundsTransSplit);

                        objFundsTransSplit.Dispose();
                        objFundsTransSplit = null;

                        objFunds.PayToTheOrderOf = objFunds.FirstName + " " + objFunds.LastName;

                        objFundsXPayee = (FundsXPayee)objDMF.GetDataModelObject("FundsXPayee", false);
                        objFundsXPayee.PayeeEid = iEntityId;
                        objFundsXPayee.Payee1099Flag = true;
                        objFundsXPayee.PayeeTypeCode = objFunds.PayeeTypeCode;
                        objFunds.FundsXPayeeList.Add(objFundsXPayee);

						objFunds.Save();		//Reservers are also updated here

						iTransId = objFunds.TransId;
                       
						objFunds.Dispose();
						objFunds = null;

						//set the added by field value VSSINF if new record was added.
						SetField("FUNDS", "ADDED_BY_USER", "VSSINF", "TRANS_ID", iTransId.ToString());
                        SetField("FUNDS_TRANS_SPLIT", "ADDED_BY_USER", "VSSINF", "TRANS_ID", iTransId.ToString());

						//logging4 - end update
                        VSSCommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "PaymentResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;

					}	//if(bError == false)
				}		//foreach
			}			//try
			catch(Exception p_objException)
			{
				AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V110", "Data push failed: Unknown Error: " + p_objException.Message);
				bResponse = false;		
				bError = true;
				//logging5 - log exception
                VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
			}
			finally
			{
				objEntity = null;
				objFundsTransSplit = null;
				objFunds = null;
                objReserveCurr = null;
				nsmgr = null;

				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}

                if (objFundsXPayee != null)
                {
                    objFundsXPayee.Dispose();
                    objFundsXPayee = null;
                }
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
                objSysSetting = null;
			}

			//logging6 - begin response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
                VSSCommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "VSS", "CMPD", objPaymentInvoiceXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
                VSSCommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "VSS", "FAIL", objResponse.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}
		
		#endregion

		#region Private Document Import Method
		/// Name		: DocumentImport
		/// Author		: Anshul Verma
		/// Date Created: 18/12/2012
		/// <summary>		
		/// This method is used to import Document data from VSS
		/// </summary>
		private XmlDocument DocumentImport(XmlDocument objDocumentXml)
		{
			DataModelFactory objDMF=null;

			LocalCache objCache = null;
			DbReader objReaderTemp = null;
			DbConnection objConn=null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

			string sMainRqUID = "";

			string sRqUID = "";

			int iNewDocumentId = 0;
			int iExistingDocumentId = 0;
			string sDocManId = "";
			string sEnvId = "";
			string sClaimNumber = "";
			int iClaimNumber = 0;
			string sAttachmentDesc = "";		//subject
            string sAttachmentFilename = "";	//not the file name, just name given to document
            string sWebsiteURL = "";
			string sLastUpdateDtm = "";			//contains date in YYYYMMDD format
			string sCRUD = "";					//to find out if document is to create (C) or delete (D)
			string sKeyWord = "";
            string sDocTitle = "";

			string sTemp = "";
            double dTemp = 0;
			int iTemp = 0;

			bool bResponse = false;		//this is set to false when error - this is not used
			bool bError = false;		//this is set to true when error

			//logging1
			int iReqId = 0;
			int iEventId = 0;			

            Boolean bSuccess = false;
            string sTempAcrosoftUserId = m_sUserName;
            string sTempAcrosoftPassword = m_sPassword;

            //Raman Bhatia -- MCM Updates 09/07/2008
            //We need to move VSS document links to MCM server...
            Acrosoft objAcrosoft = null;
            DbReader objReader = null;
            SysSettings objSettings = null;
            int iReturnId = 0;
            string sSessionId = "";
            string sTempPath = "";
            bool bUseAcrosoftInterface = false;
            string sRMXEventNumber = "";
            string sRMXClaimNumber = "";
            string sAppExcpXml = "";
            //Handling the filenames, as Acrosoft do not allow more than 60 char
            int iFilenameLength = 0;
            int iPos = 0;
            string sExt = "";
			try
			{
				//logging2 - enter initial records in loggin tables				
                if (VSSExportSection.Logging.ToUpper().Equals("YES"))
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
				}
                VSSCommonFunctions.iLogDXRequest(iReqId, "DOCM", "TORM", "VSS", "STRT", objDocumentXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				//objCache = objDMF.Context.LocalCache;
                objCache = new LocalCache(this.RiskmasterConnectionString, m_iClientId);

				nsmgr = new XmlNamespaceManager(objDocumentXml.NameTable);
				nsmgr.AddNamespace("csc", objDocumentXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				if(objDocumentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
					sMainRqUID = objDocumentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;
                
				//Raman Bhatia -- MCM Updates 09/07/2008
                //We need to move VSS document links to MCM server...

                objSettings = new SysSettings(this.RiskmasterConnectionString, m_iClientId);
                if (objSettings.UseAcrosoftInterface)
                {
                   bUseAcrosoftInterface = true;
                   sTempPath = RMConfigurator.TempPath;

                   if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                   {
                       if (AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName] != null
                           && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName].RmxUser))
                       {
                           sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName].AcrosoftUserId;
                           sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName].AcrosoftPassword;
                       }
                       else
                       {
                           sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                           sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                       }
                   }                  
                }
				//following for loop will iterate through all the Documents coming in XML
				foreach (XmlNode objXmlNode in objDocumentXml.SelectNodes("//ClaimsSubsequentRptSubmitRq", nsmgr))
				{

					iNewDocumentId = 0;
					iExistingDocumentId = 0;
					sRqUID = "";
					iClaimNumber = 0;
					sRMXClaimNumber = "0";
                    sRMXEventNumber = "0";
					sAttachmentDesc = "";		//subject
                    sAttachmentFilename = "";	//not the file name, just name given to document
					sWebsiteURL = "";
					sLastUpdateDtm = "";			//contains date in YYYYMMDD format
					sCRUD = "";					//to find out if document is to create (C) or delete (D)
					sKeyWord = "";

                    sDocManId = "";
                    sEnvId = "";
                    sClaimNumber = "";

					sTemp = "";
					dTemp = 0;
					iTemp = 0;
                    //Handling the filenames, as Acrosoft do not allow more than 60 char
                    iFilenameLength = 0;
                    iPos = 0;
                    sExt = "";

					bResponse = false;
					bError = false;

					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;


					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//to find out if document is to create (C) or delete (D)
						//sCRUD = VSSCommonFunctions.GetValue(objXmlNode, "//csc:CRUD", nsmgr);
                        sCRUD = "C"; //As of now only document create is being implemented for VSS for taking this by default "C=create"
					}


					if(bError == false)
					{
						if(!sCRUD.Equals("C") && !sCRUD.Equals("D"))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value: {CRUD: " + sCRUD + "}");
							bResponse = false;		
							bError = true;
						}
					}

					if(bError == false)
					{
                        //Start - Inner loop for other id field
                        foreach (XmlNode objchildXmlNode in objXmlNode.SelectNodes("./FileAttachmentInfo/ItemIdInfo/OtherIdentifier", nsmgr))
                     //   foreach (XmlNode objchildXmlNode in  objXmlNode.SelectSingleNode("//ItemIdInfo", nsmgr))
                        {
                            if (objchildXmlNode.ChildNodes[0].InnerText.Equals("csc:DocManId"))
                            {
                                sDocManId = objchildXmlNode.ChildNodes[1].InnerText;
                            }

                            if (objchildXmlNode.ChildNodes[0].InnerText.Equals("csc:EnvId"))
                            {
                                sEnvId = objchildXmlNode.ChildNodes[1].InnerText;
                            }

                            //mandatory
                            if (objchildXmlNode.ChildNodes[0].InnerText.Equals("csc:ClaimId"))
                            {
                                sClaimNumber = objchildXmlNode.ChildNodes[1].InnerText;
                            }
                        }

                        //End - Inner loop for other id field 

						//subject
						sAttachmentDesc = VSSCommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/AttachmentDesc", nsmgr);

						//title. mandatory. not the file name, just name given to document.
						sAttachmentFilename = VSSCommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/AttachmentFilename", nsmgr);
						
						//link to VSS document. mandatory
						sWebsiteURL = VSSCommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/WebsiteURL", nsmgr);

						//contains date in YYYYMMDD format
						sLastUpdateDtm = VSSCommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/csc:LastUpdateDtm", nsmgr);
						if(!sLastUpdateDtm.Equals(""))
							sLastUpdateDtm = sLastUpdateDtm + "000000";
						else
							sLastUpdateDtm = Conversion.GetDateTime(DateTime.Now.ToString());
					
						if(sDocManId.Equals(""))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {DocManId}");
							bResponse = false;		
							bError = true;
						}
						else if(sEnvId.Equals(""))
						{
                            AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {EnvId}");
							bResponse = false;		
							bError = true;
						}
						else if(sAttachmentFilename.Equals(""))
						{
                            AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Attachment File name}");
							bResponse = false;		
							bError = true;
						}
						else if(sWebsiteURL.Equals(""))
						{
                            AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {WebsiteURL}");
							bResponse = false;		
							bError = true;
						}
						else if(sClaimNumber.Equals(""))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Claim Number}");
							bResponse = false;		
							bError = true;
						}
					}

                    //Handling the filenames, as Acrosoft do not allow more than 60 char
                    if (bError == false)
                    {
                        iFilenameLength = sAttachmentFilename.Length;
                        if (iFilenameLength > 55)   //.html will be added to it later
                        {
                            iPos = sAttachmentFilename.LastIndexOf('.');
                            sExt = sAttachmentFilename.Substring(iPos);
                            sAttachmentFilename = sAttachmentFilename.Substring(0, (55 - sExt.Length)).Trim();  //leaving the margin for .docx
                            sAttachmentFilename = sAttachmentFilename + sExt;
                        }                        
                    }
					if(bError == false)
					{
						//document is attached to claim
						iClaimNumber = GetRowIdFromOtherField("CLAIM_ID", "CLAIM", "CLAIM_NUMBER", "'" + sClaimNumber + "'");

						if(iClaimNumber == 0)
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
							bResponse = false;		
							bError = true;
						}
					}


					if(bError == false)		//everything is ok and there is no error till this point. create document in RMX.
					{
						//logging3 - begin update
                        VSSCommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

						sKeyWord = "DocManId=" + sDocManId + ";sEnvId=" + sEnvId;

						if(sCRUD.Equals("D"))
						{
							objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
							objConn.Open();
							objConn.ExecuteNonQuery("UPDATE DOCUMENT SET FOLDER_ID = -1 WHERE KEYWORDS = '" + sKeyWord + "'");
                            //Should not be shown in attachments anymore
                            objConn.ExecuteNonQuery("DELETE FROM DOCUMENT_ATTACH WHERE DOCUMENT_ID IN (SELECT DOCUMENT_ID FROM DOCUMENT WHERE KEYWORDS = '" + sKeyWord + "')");
							objConn.Close();

                            if (bUseAcrosoftInterface)
                            {
                                //VSS documents are not working with MCM.. they need to be uploaded to MCM too
                                objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                                objConn.Open();
                                string sSQL = "Select event_number , claim_number from claim where claim_id = " + iClaimNumber.ToString();
                                objReader = objConn.ExecuteReader(sSQL);
                                if (objReader.Read())
                                {
                                    sRMXEventNumber = Conversion.ConvertObjToStr(objReader.GetValue("event_number"));
                                    sRMXClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("claim_number"));
                                }
                                objConn.Close();

                                //Authenticating Acrosoft credentials

                                objAcrosoft = new Acrosoft(m_iClientId);   //dvatsa-cloud                                                             
                               
                                iReturnId = objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml);
                                if (iReturnId == 0)
                                {
                                    //Set Search Data
                                    objAcrosoft.GetGlobalSearchCriteria = "<ASNDXSearchInput><ClaimNbr>" + sRMXClaimNumber +
                                        "</ClaimNbr><EventNbr>" + sRMXEventNumber + "</EventNbr></ASNDXSearchInput>";

                                    objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword, (sAttachmentFilename + ".html"), string.Empty,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, out sAppExcpXml);
                                }
                            }
						}
						else if(sCRUD.Equals("C"))
						{
							//either document already exists and we need to update that or create new one. decide this based on ManId and EnvId.

							objReaderTemp=DbFactory.GetDbReader(this.RiskmasterConnectionString,"SELECT DOCUMENT_ID FROM DOCUMENT WHERE KEYWORDS = '" + sKeyWord + "'");
							if (objReaderTemp.Read())
							{
								iExistingDocumentId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
							}
							objReaderTemp.Close();

                            //We need to move VSS document links to MCM server...

                            if(bUseAcrosoftInterface)
                            {
                               //Generating the dummy HTML containing the link to VSS document
                               //for MCM consumption
                                sDocTitle = sAttachmentFilename;
                                GenerateHTMLForVSS(sTempPath, sWebsiteURL , sDocTitle);
                                objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud

                                objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                                objConn.Open();
                                string sSQL = "Select event_number , claim_number from claim where claim_id = " + iClaimNumber.ToString();
                                objReader = objConn.ExecuteReader(sSQL);
                                if (objReader.Read())
                                {
                                    sRMXEventNumber = Conversion.ConvertObjToStr(objReader.GetValue("event_number"));
                                    sRMXClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("claim_number"));
                                }
                                objConn.Close();

                            }
							if(iExistingDocumentId == 0)
							{
								//document does not exist so create new one
                                iNewDocumentId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DOCUMENT", m_iClientId);

								objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
								objConn.Open();
								//create the document entry in DOCUMENT table and attach info (to claim) in DOCUMENT_ATTACH table
								
								//default values
								/*ARCHIVE_LEVEL = 0
								DOCUMENT_CATEGORY = 0
								DOCUMENT_CLASS = 0
								DOCUMENT_TYPE = 0
								SECURITY_LEVEL = 0
								DOCUMENT_EXPDTTM = 0
								DOCUMENT_ACTIVE = 0*/

								objConn.ExecuteNonQuery("INSERT INTO DOCUMENT (DOCUMENT_ID, CREATE_DATE, DOCUMENT_NAME, DOCUMENT_SUBJECT, USER_ID, DOC_INTERNAL_TYPE, DOCUMENT_FILENAME, DOCUMENT_FILEPATH, FOLDER_ID, KEYWORDS, ARCHIVE_LEVEL, DOCUMENT_CATEGORY, DOCUMENT_CLASS, DOCUMENT_TYPE, SECURITY_LEVEL, DOCUMENT_EXPDTTM, DOCUMENT_ACTIVE) VALUES (" + iNewDocumentId + ", '" + sLastUpdateDtm + "', '" + sAttachmentFilename.PadRight(32, ' ').Substring(0, 32).Trim().Replace("'", "''") + "', '" + sAttachmentDesc.PadRight(50, ' ').Substring(0, 50).Trim().Replace("'", "''") + "', '" + m_sUserName + "', 3, '" + sAttachmentFilename.PadRight(255, ' ').Substring(0, 255).Trim().Replace("'", "''") + "', '" + sWebsiteURL.Replace("'", "''") + "', 0, '" + sKeyWord + "', 0, 0, 0, 0, 0, 0, 0)");
								objConn.ExecuteNonQuery("INSERT INTO DOCUMENT_ATTACH (TABLE_NAME, RECORD_ID, DOCUMENT_ID) VALUES ('CLAIM', " + iClaimNumber + ", " + iNewDocumentId + ")");
								objConn.Close();
								//objConn=null;
                                if (bUseAcrosoftInterface)
                                {
                                    //VSS documents are not working with MCM.. they need to be uploaded to MCM too
                                    
                                    //Authenticating Acrosoft credentials
                                   
                                    iReturnId = objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml);

                                    if (iReturnId == 0)
                                    {
                                        //creating attachment folder
                                        objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            AcrosoftSection.AcrosoftAttachmentsTypeKey, sRMXEventNumber, sRMXClaimNumber,
                                            AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                                        //Posting HTML to Acrosoft..Raman Bhatia
                                        iReturnId = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            (sTempPath + "\\" + sAttachmentFilename + ".html"), sAttachmentFilename, "", sRMXEventNumber, sRMXClaimNumber,
                                            AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, string.Empty, string.Empty, out sAppExcpXml);
                                    }
                                }
							}
							else
							{
								//document already exists so update that
								objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
								objConn.Open();

								//update the document entry in DOCUMENT table and attach info (to claim) in DOCUMENT_ATTACH table
								objConn.ExecuteNonQuery("UPDATE DOCUMENT SET DOCUMENT_NAME = '" + sAttachmentFilename.PadRight(32, ' ').Substring(0, 32).Trim().Replace("'", "''") + "', DOCUMENT_SUBJECT = '" + sAttachmentDesc.PadRight(50, ' ').Substring(0, 50).Trim().Replace("'", "''") + "', DOCUMENT_FILENAME = '" + sAttachmentFilename.PadRight(255, ' ').Substring(0, 255).Trim().Replace("'", "''") + "', DOCUMENT_FILEPATH = '" + sWebsiteURL.Replace("'", "''") + "' WHERE DOCUMENT_ID = " + iExistingDocumentId);
								objConn.ExecuteNonQuery("UPDATE DOCUMENT_ATTACH SET RECORD_ID = " + iClaimNumber + " WHERE DOCUMENT_ID = " + iExistingDocumentId);
								objConn.Close();
                                if (bUseAcrosoftInterface)
                                {
                                    //VSS documents are not working with MCM.. they need to be uploaded to MCM too
                                    //Authenticating Acrosoft credentials

                                    iReturnId = objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml);
                                    if (iReturnId == 0)
                                    {
                                        //Set Search Data
                                        objAcrosoft.GetGlobalSearchCriteria = "<ASNDXSearchInput><ClaimNbr>" + sRMXClaimNumber + 
                                            "</ClaimNbr><EventNbr>" + sRMXEventNumber + "</EventNbr></ASNDXSearchInput>";

                                        //Deleting old html
                                        objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword, 
                                            (sAttachmentFilename + ".html"), "", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", out sAppExcpXml);

                                        //creating attachment folder
                                        objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            AcrosoftSection.AcrosoftAttachmentsTypeKey, sRMXEventNumber, sRMXClaimNumber,
                                            AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                                        //Posting HTML to Acrosoft..Raman Bhatia
                                        iReturnId = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            (sTempPath + "\\" + sAttachmentFilename + ".html"), sAttachmentFilename, "", sRMXEventNumber, 
                                            sRMXClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", "", out sAppExcpXml);
                                    }
                                }
							}
						}
						//logging4 - end update
                        VSSCommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "DocumentResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;
					}
				}		//foreach
			}			//try
			catch(Exception p_objException)
			{
				AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "V110", "Data push failed: Unknown Error: " + p_objException.Message);
				bResponse = false;		
				bError = true;

				//logging5 - log exception
                VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
			}
			finally
			{
				nsmgr = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objReaderTemp != null)
                {
                    objReaderTemp.Dispose();
                }
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
				
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
                if (objSettings != null)
                {
                    objSettings = null;
                }
                if (objAcrosoft != null)
                {
                    objAcrosoft = null;
                }
			}

			//logging6 - begin response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            VSSCommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
                VSSCommonFunctions.iLogDXRequest(iReqId, "DOCM", "TORM", "VSS", "CMPD", objDocumentXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
                VSSCommonFunctions.iLogDXRequest(iReqId, "DOCM", "TORM", "VSS", "FAIL", objDocumentXml.OuterXml, this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}
		
		#endregion

		#region Private Common Methods

		private void AddError(ref XmlDocument p_objResponse, string sResponseType, string sRqUID, string sStatusCode, string sErrorCode, string sStatusDesc)
		{
			bool bExist = false;
			XmlNode objErrorNode = null;
			string sErrorNode = "";
			string sErrorNodeParent = "";
            string sResponseXMLPath = "";

			if (p_objResponse == null)
			{
				p_objResponse = new XmlDocument();
                switch (sResponseType)
                {
                    case "VSSAssignmentResponse":
                        sResponseXMLPath = "VSSInterface/VendorAcceptanceStatusRs.xml";
                        break;

                    case "VSSVendorAccInfoResponse":
                        sResponseXMLPath = "VSSInterface/VendorAccountInfoStatusRs.xml";
                        break;

                    case "PaymentResponse":
                        sResponseXMLPath = "VSSInterface/PaymentResponse.xml";
                        break;

                    case "DocumentResponse":
                        sResponseXMLPath = "VSSInterface/DocumentResponse.xml";
                        break;

                    default:
                        sResponseXMLPath = "";
                        break;
                }
                p_objResponse.Load(Path.Combine(RMConfigurator.AppFilesPath, sResponseXMLPath));
				if(p_objResponse.GetElementsByTagName("ServerDt").Count > 0)
					p_objResponse.GetElementsByTagName("ServerDt").Item(0).InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
			}

			switch(sResponseType)
			{
                case "VSSAssignmentResponse":
					sErrorNode = "ClaimsSubsequentRptSubmitRs";
					sErrorNodeParent = "ClaimsSvcRs";
					break;

                case "VSSVendorAccInfoResponse":
                    sErrorNode = "ClaimsSubsequentRptSubmitRs";
                    sErrorNodeParent = "ClaimsSvcRs";
					break;

				case "PaymentResponse":
					sErrorNode = "ClaimsSubsequentRptSubmitRs";
					sErrorNodeParent = "ClaimsSvcRs";
					break;

				case "DocumentResponse":
					sErrorNode = "ClaimsSubsequentRptSubmitRs";
					sErrorNodeParent = "ClaimsSvcRs";
					break;
				default:
					sErrorNode = "";
					sErrorNodeParent = "";
					break;
			}

			foreach (XmlNode objXmlNode in p_objResponse.GetElementsByTagName("RqUID"))
			{
				if(objXmlNode.InnerText.Equals(sRqUID))
				{
					bExist = true;		//if error has been put for same RqUID then don't put again
					break;
				}
			}

			if(bExist == false && !sErrorNode.Equals(""))
			{
                if (p_objResponse.GetElementsByTagName(sErrorNode).Item(0) != null)
                {
				objErrorNode = p_objResponse.GetElementsByTagName(sErrorNode).Item(0).Clone();

				if(objErrorNode.SelectNodes("//RqUID").Count > 0)
					objErrorNode.SelectNodes("//RqUID").Item(0).InnerText = sRqUID;

				if(objErrorNode.SelectNodes("//TransactionResponseDt").Count > 0)
					objErrorNode.SelectNodes("//TransactionResponseDt").Item(0).InnerText = Conversion.GetDateTime(DateTime.Now.ToString());

				if(objErrorNode.SelectNodes("//MsgStatusCd").Count > 0)
					objErrorNode.SelectNodes("//MsgStatusCd").Item(0).InnerText = sStatusCode;

				if(objErrorNode.SelectNodes("//MsgErrorCd").Count > 0)
					objErrorNode.SelectNodes("//MsgErrorCd").Item(0).InnerText = sErrorCode;

				if(objErrorNode.SelectNodes("//MsgStatusDesc").Count > 0)
					objErrorNode.SelectNodes("//MsgStatusDesc").Item(0).InnerText = sStatusDesc;

				p_objResponse.SelectSingleNode("/ACORD/" + sErrorNodeParent).AppendChild(objErrorNode);

				objErrorNode = null;
			}
			}
		}

		
		private int GetRowIdFromOtherField(string sSelectField, string sTableName, string sFieldName, string sFieldValue)
		{
			DbConnection oCn = null;
			DbReader objDbReader = null;
			int iRowId =0;
			try
			{
				oCn = Db.DbFactory.GetDbConnection(this.RiskmasterConnectionString);
				oCn.Open(); 
				
				objDbReader = oCn.ExecuteReader("SELECT " + sSelectField + " FROM " + sTableName + " WHERE " + sFieldName + " = " + sFieldValue);
				if(!objDbReader.Read())
				{	
					objDbReader.Close();
					return 0;					
				}
				iRowId = objDbReader.GetInt(0);
				return iRowId;
			}
			catch(Exception p_oException)
			{
				throw new Exception(p_oException.Message, p_oException);
			}
			finally
			{
				if(oCn != null)
				{
					oCn.Close();
					oCn.Dispose();
				}
				if(objDbReader != null)
				{
					if(!objDbReader.IsClosed)
						objDbReader.Close();
					objDbReader = null;
				}
			}
		}
	

		/// Name		: ReadTagValue
		/// Author		: Manoj Agrawal
		/// Date Created: 08/29/2006
		/// ************************************************************
		/// <summary>		
		/// ReadTagValue() function works for following type of structure. It will read sValue from &lt;OtherId&gt; tag if we pass "OtherIdentifier" in sParent and "csc:SupervisorUserId" in sType and "OtherId" in sTag.
		///	 &lt;OtherIdentifier&gt;
		///		&lt;OtherIdTypeCd&gt;csc:SupervisorUserId&lt;/OtherIdTypeCd&gt;
		///		&lt;OtherId&gt;SupervisorName&lt;/OtherId&gt; 
		///	 &lt;/OtherIdentifier&gt;
		/// </summary>
		private string ReadTagValue(XmlNode objNode, string sParent, string sType, string sTag)
		{
			string sValue = "";

			try
			{
				for (int i = 0; i < objNode.SelectNodes("//" + sParent).Count; i++)
				{
					if(objNode.SelectNodes("//" + sParent).Item(i).HasChildNodes == true)
					{
						for (int j = 0; j < objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Count; j++)
						{
							if(objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Item(j).InnerText.Equals(sType))
							{
								goto readvalue;
							}
						}

						continue;

					readvalue:
						for (int j = 0; j < objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Count; j++)
						{
							if(objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Item(j).Name.Equals(sTag))
							{
								sValue = objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Item(j).InnerText;
								return VSSCommonFunctions.AddAmparsand(sValue);
							}
						}
					}	//if
				}		//for

				return VSSCommonFunctions.AddAmparsand(sValue);
			}			//try
			catch(Exception objException)
			{
				return VSSCommonFunctions.AddAmparsand(sValue);
			}
		}


		private void SetField(string sTable, string sSetField, string sSetValue, string sWhereField, string sWhereValue)
		{
          //  SetField("ENTITY", "ADDED_BY_USER", "VSSINF", "ENTITY_ID", iEntityId.ToString());
			DbConnection objConn=null;

			try
			{
				objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
				objConn.Open();
				objConn.ExecuteNonQuery("UPDATE " + sTable + " SET " + sSetField + " = '" + sSetValue.Replace("'", "''") + "' WHERE " + sWhereField + " = " + sWhereValue.Replace("'", "''"));
				objConn.Close();
				objConn=null;
			}
			catch(Exception objEx)
			{			
			}
			finally
			{
				if(objConn != null)
				{
					objConn.Close();
					objConn=null;
				}
			}
		}
        /// Name		: GenerateHTMLForVSS
        /// Author		: Raman Bhatia / Anshul Verma
        /// Date Created: 07/03/2008  / 10/01/2013
        /// ************************************************************
        /// <summary>		
        /// GenerateHTMLForVSS() function generates dummy HTML for opening VSS documents in MCM
        /// </summary>
        private bool GenerateHTMLForVSS(string filepath , string url , string filename)
        {
            FileStream outputfile = null;
            string sCompleteFilePath = "";
            try
            {
                sCompleteFilePath = filepath + "\\" + filename + ".html";
                if (File.Exists(sCompleteFilePath))
                {
                    File.Delete(sCompleteFilePath);
                }
                outputfile = new FileStream(sCompleteFilePath, FileMode.OpenOrCreate, FileAccess.Write);

                writer = new StreamWriter(outputfile);

                 DoWrite("<HTML>");
                 DoWrite("<script>");
                 DoWrite("function autoChange()");
                 DoWrite("{");
                 DoWrite("var timeID = setTimeout(location.href='" + url + "', 5000)");
                 DoWrite("}");
                 DoWrite("</script>");
                 DoWrite("<BODY onLoad='autoChange()'>");
                 DoWrite("Loading VSS document..");
                 DoWrite("Your browser will automatically jump there.<BR>");
                 DoWrite("If it does not then please click <A HREF='" + url + "'>Open</A> to view this document</H4>");
                 DoWrite("</BODY>");
                 DoWrite("</HTML>");

                writer.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void DoWrite(String line)
        {
            writer.WriteLine(line);
            writer.Flush();
        }

        private void AddTagValue(XmlNode objNode, string sParent, string sType, string sTag, string sValue, XmlNamespaceManager nsmgr)
        {
            try
            {
                for (int i = 0; i < objNode.SelectNodes("//" + sParent, nsmgr).Count; i++)
                {
                    if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).HasChildNodes == true)
                    {
                        for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
                        {
                            if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText.Equals(sType))
                            {
                                goto setvalue;
                            }
                        }

                        continue;

                    setvalue:
                        for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
                        {
                            if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).Name.Equals(sTag))
                            {
                                objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText = sValue;
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
        }

        /// <summary>
        /// Rahul Aggarwal
		/// Closes a passed DbReader
        /// Date Created: 05/21/2010	
        /// MITS 20073/20036/20938
		/// </summary>
		/// <param name="p_objReader">DbReader</param>
		private void CloseReader(ref DbReader p_objReader)
		{
			if (p_objReader!=null)
			{
				if (!p_objReader.IsClosed)
				{
					p_objReader.Close();
				}
				p_objReader=null;
			}
		}
		#endregion
	}
}
