using System;
using System.Xml;

	/**************************************************************
	 * $File		: ProcessFactory.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 22/09/2005
	 * $Author		: Parag Sarin
	 * $Comment		: ProcessFactory class
	 * $Source		:  	
	**************************************************************/

namespace Riskmaster.Application.VSSInterface
{

	public class ExportProcessFactory:IExportProcessFactory
	{

        private string m_sUser = string.Empty, m_sPwd = string.Empty, m_sDsnName = string.Empty;

        private int m_iClientId = 0;

        #region IProcess Members

        /// <summary>
        /// Gets and sets the Riskmaster Database Connection String
        /// </summary>
        public string RiskmasterConnectionString
        {
            get;
            set;
        } // property RiskmasterConnectionString

        #endregion


		//private IClaimProcess objClaim=null;
		private IExportProcess objExport=null;

		//public ClaimProcessFactory(string p_sDSN,string p_sUserName,string p_sPassword,string p_sDsnName)
		public ExportProcessFactory(string p_sUserName,string p_sPassword,string p_sDsnName, int p_iClientId)
		{
			m_sUser=p_sUserName;
			m_sPwd=p_sPassword;
			m_sDsnName=p_sDsnName;
            m_iClientId = p_iClientId;

            this.RiskmasterConnectionString = VSSCommonFunctions.GetRiskmasterConnectionString(m_sUser, m_sPwd, m_sDsnName, m_iClientId);
		}

		//public IClaimProcess CreateClaimProcess()
        //{
		//	objClaim=new LSSClaimProcess(m_sDSN,m_sUser,m_sPwd,m_sDsnName);
		//	return objClaim;
		//}
		public IExportProcess CreateExportProcess()
		{
            objExport = new VSSExportProcess(m_sUser, m_sPwd, m_sDsnName, m_iClientId);
			return objExport;
		}



    }


	//Manoj - added new class for Import
	public class ImportProcessFactory:IImportProcessFactory
	{
		private string m_sDSN="";
		private string m_sUser="";
		private string m_sPwd="";
		private string m_sDsnName="";
        private int m_iClientId = 0;

		private IImportProcess objImport = null;

        public ImportProcessFactory(string p_sDSN, string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
		{
			m_sDSN=p_sDSN;
			m_sUser=p_sUserName;
			m_sPwd=p_sPassword;
			m_sDsnName=p_sDsnName;

            m_iClientId = p_iClientId;
		}

		public IImportProcess CreateImportProcess()
		{
            objImport = new VSSImportProcess(m_sUser, m_sPwd, m_sDsnName, m_iClientId);
			return objImport;
		}
	}

}
