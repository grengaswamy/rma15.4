﻿
using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Collections.Specialized;
using Riskmaster.Security.Encryption;
using System.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Riskmaster.Application.VSSInterface.VSSExportService;

namespace Riskmaster.Application.VSSInterface
{
    /**************************************************************
     * $File		: VSSExportProcess.cs
     * $Revision	: 1.0.0.0
     * $Date		: 12/10/2012
     * $Author		: Anshul Verma
     * $Comment		: VSSExportProcess class has functions to export Claim, Reserve data from RMX to VSS
     * $Source		:  	

     **************************************************************/
    /// <summary>
	/// VSSExportProcess class has functions to export Claims, Reserve from RMX to VSS
	/// </summary>

    internal class VSSExportProcess : IExportProcess
    {
        #region Variables Declaration

        /// <summary>
        /// Gets and sets the Riskmaster Database Connection String
        /// </summary>
        public string RiskmasterConnectionString
        {
            get;
            set;
        } // property RiskmasterConnectionString


        /// <summary>
        /// User name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// DSN Name
        /// </summary>
        private string m_sDsnName = "";

        internal string m_strAppFilesPath = string.Empty;

        private VSSDataExchangeServiceClient objVSSDataExchange = null;
        /// <summary>
        /// Gets and sets the AppFiles Path
        /// </summary>
        internal string AppFilesPath
        {
            get { return m_strAppFilesPath; }
            set { m_strAppFilesPath = value; }
        } // property AppFilesPath

        UserLogin UserLoginInstance
        {
            get;
            set;
        }//property UserLoginInstance



        string sReserveExportXmlPath = string.Empty;
        string sPaymentVoidExportXmlPath = string.Empty;
        string sPassUpdateExportXmlPath = string.Empty;
        string sCurentAdjusterExportXmlPath = string.Empty;//asharma326 For RMA-VSS Adjuster
        private int m_iClientId = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// Overloaded Class constructor
        /// </summary>
        /// 
        /// <param name="p_sUserName">string containing the user name to log into the Riskmaster security database</param>
        /// <param name="p_sPassword">string containing the password to log into the Riskmaster security database</param>
        /// <param name="p_sDsnName">string containing the DSN Name selection</param>
        public VSSExportProcess(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            RMConfigurationManager.GetVSSExportSettings();
            //Initialize the VSS Path values
            InitializeVSSValues();
            m_iClientId = p_iClientId;
            //Set the connection string for the Riskmaster Database
            this.RiskmasterConnectionString = VSSCommonFunctions.GetRiskmasterConnectionString(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
            this.UserLoginInstance = VSSCommonFunctions.GetUserLoginInstance(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>

        //<ReserveExportXml>%AppFilesPath%\VSSInterface\AdminExport.xml</ReserveExportXml>

        public void InitializeVSSValues()
        {
            this.AppFilesPath = RMConfigurator.AppFilesPath;
            sReserveExportXmlPath = Path.Combine(this.AppFilesPath, @"VSSInterface\ClaimsSubsequentRptSubmitRq.xml");
            sPaymentVoidExportXmlPath = Path.Combine(this.AppFilesPath, @"VSSInterface\VoidPayCheckAddRq.xml");
            sPassUpdateExportXmlPath = Path.Combine(this.AppFilesPath, @"VSSInterface\PasswordUpdateRq.xml");
            sCurentAdjusterExportXmlPath = Path.Combine(this.AppFilesPath, @"VSSInterface\ClaimCurrentAdjuster.xml");//Asharma326 For claim Adjuster RMA-VSS

        } // method: InitializeVSSValues

        #region Reserve Export function
        /// Name		: ReserveExport 
        /// Author		: Anshul Verma
        /// Date Created: 12/10/2012
        /// ************************************************************
        /// <summary>		
        /// This method is used to export reserve - claimant - claim data from RMX to VSS
        /// </summary>
        public bool ReserveExport(int p_iClaimId, int p_iClaimantId, int p_iReserveId)
        {
            //Variable for claim node child
            int iClaimantId = 0;
            int iClaimantEid = 0;
            string sClaimNumber = "";
            //Variable for claimant node child
            string sOfficePhone = "";
            string sRqUID = "";
            string sMainRqUID = "";
            string sFax = "";
            DataModelFactory objDMF = null;
            DbReader objReader = null;
            DbConnection objConn = null;

            UserLogin objUserLogin = null;
            Claim objClaim = null;
            Claimant objClaimant = null;
            Entity objEntity = null;
            ReserveCurrent objReserveCurr = null;
            CvgXLoss objCvgXLoss = null;
            PolicyXCvgType objPolicyXCvgType = null;
            PolicyXUnit objPolicyXUnit = null;
            Policy objPolicy = null;

            LocalCache objCache = null;
            XmlDocument objReserveXml = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode objReserveNode = null;

            int iReqId = 0;
            int iEventId = 0;
            bool bSuccess = false;
            string sPackType = "RES";
            string sResponse=string.Empty, sReqId = string.Empty;
            string sStatusDesc = string.Empty, sStatusCd = string.Empty, sStatusNode = "VSSWebServiceInfo", sErrorCd = string.Empty;
            string sXml = string.Empty;
            XmlNode objReqIdNode = null;
            XmlNode objStatusCdNode = null;
            XmlNode objErrorCdNode = null;
            XmlNode objStatusDescNode = null;
            XmlDocument objDoc = null;
            XmlDocument objResponseDoc = null;

            Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(this.RiskmasterConnectionString, m_iClientId); 
            try
            {
                if (!File.Exists(sReserveExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("VSS.LoadFile.FileNotFound", m_iClientId)));

                objReserveXml = new XmlDocument();
                try
                {
                    objReserveXml.Load(sReserveExportXmlPath);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("VSSExportProcess.ReserveExportXmlNotFound", m_iClientId), p_objException);
                }

                objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                objDMF = new DataModelFactory(objUserLogin, m_iClientId);

                objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString, m_iClientId);

                sMainRqUID = Guid.NewGuid().ToString();						// this will generate a unique Req ID each time Reserve Data Exchange 
                sRqUID = sMainRqUID;			                            //set the default value for sRqUID same as sMainRqUID
                foreach (XmlNode objRqNode in objReserveXml.GetElementsByTagName("RqUID"))
                {
                    objRqNode.InnerText = sMainRqUID;
                }

                foreach (XmlNode objOrg in objReserveXml.GetElementsByTagName("Org"))
                {
                    objOrg.InnerText = m_sDsnName;
                }

                nsmgr = new XmlNamespaceManager(objReserveXml.NameTable);
                nsmgr.AddNamespace("csc", objReserveXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                objReserveCurr = (ReserveCurrent)objDMF.GetDataModelObject("ReserveCurrent", false);
                objReserveCurr.MoveTo(p_iReserveId);

                if (!objReserveCurr.ClaimantEid.Equals(0))
                {
                    objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIMANT_ROW_ID, CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + objReserveCurr.ClaimId + " AND CLAIMANT_EID = " + objReserveCurr.ClaimantEid);
                    if (objReader.Read())
                    {
                        iClaimantId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        iClaimantEid = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);
                    }
                    objReader.Close();
                }
                else
                {
                    objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIMANT_ROW_ID, CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + objReserveCurr.ClaimId + " AND PRIMARY_CLMNT_FLAG =" + -1);
                    if (objReader.Read())
                    {
                        iClaimantId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        iClaimantEid = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);
                    }
                    objReader.Close();
                }
                
                objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                objClaim.MoveTo(objReserveCurr.ClaimId);

                objClaimant = (Claimant)objDMF.GetDataModelObject("Claimant", false);
                objClaimant.MoveTo(iClaimantId);

                objReserveNode = objReserveXml.GetElementsByTagName("ClaimsSubsequentRptSubmitRq").Item(0).Clone();

                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                objEntity.MoveTo(iClaimantEid);

                sClaimNumber = objClaim.ClaimNumber;

                if (objReserveNode.SelectNodes("RqUID").Count > 0)
                {
                    sRqUID = Guid.NewGuid().ToString();
                    objReserveNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
                }

                if (objReserveNode.SelectNodes("//TransactionRequestDt",nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//TransactionRequestDt", nsmgr).Item(0).InnerText = Conversion.GetDate(DateTime.Now.ToString());
                }

                if (!p_iClaimId.Equals(0))
                {
                    AddTagValue(objReserveNode, "OtherIdentifier", "ClaimId", "OtherId", p_iClaimId.ToString(), nsmgr);
                }

                if (!sClaimNumber.Equals(""))
                {
                    AddTagValue(objReserveNode, "OtherIdentifier", "ClaimNumber", "OtherId", sClaimNumber, nsmgr);
                }

                AddTagValue(objReserveNode, "OtherIdentifier", "EventId", "OtherId",objClaim.EventId.ToString(), nsmgr);

                if (!iClaimantId.Equals(0))
                {
                    AddTagValue(objReserveNode, "OtherIdentifier", "ClaimantId", "OtherId", iClaimantId.ToString(), nsmgr);
                }

                if (objReserveNode.SelectNodes("//NonCarrierFlag", nsmgr).Count > 0)
                {
                    if (objSettings.MultiCovgPerClm.ToString().Equals("-1"))
                    {
                        objReserveNode.SelectNodes("//NonCarrierFlag", nsmgr).Item(0).InnerText = "T"; // -1:True,0:False
                    }
                    else
                    {
                        objReserveNode.SelectNodes("//NonCarrierFlag", nsmgr).Item(0).InnerText = "F"; // -1:True,0:False
                    }
                }

                if (objReserveNode.SelectNodes("//IncidentClaimTypeCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//IncidentClaimTypeCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objClaim.PolicyLOBCode);
                }

                if (objReserveNode.SelectNodes("//EventNumber", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//EventNumber", nsmgr).Item(0).InnerText = objClaim.EventNumber;
                }

                objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT DATE_OF_EVENT FROM EVENT WHERE EVENT_ID = " + objClaim.EventId);
                if (objReader.Read())
                {
                    if (objReserveNode.SelectNodes("//EventDt", nsmgr).Count > 0)
                    {
                        objReserveNode.SelectNodes("//EventDt", nsmgr).Item(0).InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
                //Start Ashish
                if (objReserveNode.SelectNodes("//LossDt", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//LossDt", nsmgr).Item(0).InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                }
                objReader.Close();
                //End Ashish

                if (objReserveNode.SelectNodes("//IncidentClaimTypeDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//IncidentClaimTypeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objClaim.PolicyLOBCode);
                }

                if (objReserveNode.SelectNodes("//ClaimTypeCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimTypeCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objClaim.ClaimTypeCode);
                }

                if (objReserveNode.SelectNodes("//ClaimTypeDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimTypeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objClaim.ClaimTypeCode);
                }

                if (objReserveNode.SelectNodes("//ClaimStatusCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimStatusCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objClaim.ClaimStatusCode);
                }

                if (objReserveNode.SelectNodes("//ClaimStatusDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimStatusDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objClaim.ClaimStatusCode);
                }

                objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CAT_NUMBER FROM CATASTROPHE WHERE CAT_CODE = " + objClaim.CatastropheCode);
                if (objReader.Read())
                {
                    if (objReserveNode.SelectNodes("//CatastropheNumber", nsmgr).Count > 0)
                    {
                        objReserveNode.SelectNodes("//CatastropheNumber", nsmgr).Item(0).InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
                objReader.Close();

                if (objReserveNode.SelectNodes("//ReportedDt", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ReportedDt", nsmgr).Item(0).InnerText =objClaim.DateRptdToRm;
                }

                if (objReserveNode.SelectNodes("//DateOfClaim", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//DateOfClaim", nsmgr).Item(0).InnerText = objClaim.DateOfClaim;
                }

                if (objReserveNode.SelectNodes("//ClaimsPartyRoleCd",nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsPartyRoleCd", nsmgr).Item(0).InnerText = "Claimant";
                }

                if (objReserveNode.SelectNodes("//ClaimantTypeCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimantTypeCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objClaimant.ClaimantTypeCode);
                }

                if (objReserveNode.SelectNodes("//ClaimantTypeDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimantTypeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objClaimant.ClaimantTypeCode);
                }

                if (objReserveNode.SelectNodes("//SOLDate", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//SOLDate", nsmgr).Item(0).InnerText = objClaimant.SolDate;
                }

                if (objReserveNode.SelectNodes("//InjuryDescription", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//InjuryDescription", nsmgr).Item(0).InnerText = objClaimant.InjuryDescription;
                }

                if (objReserveNode.SelectNodes("//DamageDescription", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//DamageDescription", nsmgr).Item(0).InnerText = objClaimant.DamageDescription;
                }

                sOfficePhone = objEntity.Phone1;

                if (!sOfficePhone.Equals(""))
                {
                    AddTagValue(objReserveNode, "ClaimsParty/GeneralPartyInfo/Communications/PhoneInfo", "Office", "PhoneNumber", sOfficePhone, nsmgr);
                }
                //Start averma62 
                if (!objEntity.Phone2.Equals(""))
                {
                    AddTagValue(objReserveNode, "ClaimsParty/GeneralPartyInfo/Communications/PhoneInfo", "Home	Phone", "PhoneNumber", objEntity.Phone2, nsmgr);
                }
                //End averma62
                sFax = objEntity.FaxNumber;

                if (!sFax.Equals(""))
                {
                    AddTagValue(objReserveNode, "ClaimsParty/GeneralPartyInfo/Communications/PhoneInfo", "Fax", "PhoneNumber", sFax, nsmgr);
                }

                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong", nsmgr).Item(0).InnerText = objEntity.LastName;
                }
                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong", nsmgr).Item(0).InnerText = objEntity.FirstName;
                }
                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong", nsmgr).Item(0).InnerText = objEntity.MiddleName;
                }
                //Start - averma62
                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/ClaimantTitle", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/NameInfo/PersonNameLong/ClaimantTitle", nsmgr).Item(0).InnerText = objEntity.Title;
                }
                //End - averma62

                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/Addr1", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/Addr1", nsmgr).Item(0).InnerText = objEntity.Addr1;
                }

                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/Addr2", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/Addr2",nsmgr).Item(0).InnerText = objEntity.Addr2;
                }

                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/City", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/City", nsmgr).Item(0).InnerText = objEntity.City;
                }

                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/StateProvCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/StateProvCd", nsmgr).Item(0).InnerText = objCache.GetStateCode(objEntity.StateId);
                }

                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/PostalCode", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/PostalCode", nsmgr).Item(0).InnerText = objEntity.ZipCode;
                }

                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/CountryCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/CountryCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objEntity.CountryCode);
                }
                //Start Ashish
                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/CountryCdDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Addr/CountryCdDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objEntity.CountryCode);
                }
                //End Ashish

                if (objReserveNode.SelectNodes("//BirthDt", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//BirthDt", nsmgr).Item(0).InnerText = objEntity.BirthDate;
                }
                if (objReserveNode.SelectNodes("//GenderCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//GenderCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objEntity.SexCode);
                }
                //Asharma326 MITS 33302  Starts
                if (objClaim.AdjusterList.Count == 0)
                {
                    if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Count > 0)
                    {
                        objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Item(0).InnerText = objUserLogin.objUser.Email;
                    }
                }
                else
                {
                    ClaimAdjuster objClaimCurrentAdjuster = ((ClaimAdjusterList)objClaim.AdjusterList).GetCurrentAdjuster();
                    if (objClaimCurrentAdjuster.CurrentAdjFlag)
                    {
                        if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Item(0).InnerText = objClaimCurrentAdjuster.AdjusterEntity.EmailAddress;
                        }
                    }
                    else
                    {
                        if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Item(0).InnerText = objUserLogin.objUser.Email;
                        }
                    }
                } 
                //Asharma326 MITS 33302  Ends
                //Start - averma62
                if (objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/ClaimantEmailAddr", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ClaimsParty/GeneralPartyInfo/Communications/EmailInfo/ClaimantEmailAddr", nsmgr).Item(0).InnerText = objEntity.EmailAddress;
                }
                //End - averma62

                objCvgXLoss = (CvgXLoss)objDMF.GetDataModelObject("CvgXLoss", false);
                objCvgXLoss.MoveTo(objReserveCurr.CoverageLossId);

                objPolicyXCvgType = (PolicyXCvgType)objDMF.GetDataModelObject("PolicyXCvgType", false);
                objPolicyXCvgType.MoveTo(objCvgXLoss.PolCvgRowId);

                objPolicyXUnit = (PolicyXUnit)objDMF.GetDataModelObject("PolicyXUnit", false);
                objPolicyXUnit.MoveTo(objPolicyXCvgType.PolicyUnitRowId);

                if (!p_iReserveId.Equals(0))            //Reserve Id Table - RESERVE_CURRENT
                {
                    AddTagValue(objReserveNode, "OtherIdentifier", "RcRowId", "OtherId", p_iReserveId.ToString(), nsmgr);
                }

                if (objReserveNode.SelectNodes("//ReservePositionTypeCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ReservePositionTypeCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objReserveCurr.ReserveTypeCode);
                }

                if (objReserveNode.SelectNodes("//ReservePositionTypeDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ReservePositionTypeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objReserveCurr.ReserveTypeCode);
                }

                if (objReserveNode.SelectNodes("//ReserveAmt", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//ReserveAmt", nsmgr).Item(0).InnerText = objReserveCurr.BalanceAmount.ToString();
                }

                if (!objCvgXLoss.PolCvgRowId.Equals(0)) //Coverage id related to reserve Table - COVERAGE_X_LOSS
                {
                    AddTagValue(objReserveNode, "OtherIdentifier", "CoverageXUnitId", "OtherId", objCvgXLoss.PolCvgRowId.ToString(), nsmgr);
                }

                //Start Loss Type

                //asharma326 no need of sending coverage loss now they want claim loss MITS 33302 
                if (objReserveNode.SelectNodes("//TypeLossCauseCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//TypeLossCauseCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objCvgXLoss.LossCode) ;
                }
                if (objReserveNode.SelectNodes("//TypeLossCauseCdDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//TypeLossCauseCdDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objCvgXLoss.LossCode);
                }
                //asharma326 no need of sending coverage loss now they want claim loss MITS 33302 Ends


                if (objReserveNode.SelectNodes("//LossCauseCdDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//LossCauseCdDesc", nsmgr).Item(0).InnerText = objClaim.LossDescription;
                    //objReserveNode.SelectNodes("//LossCauseCdDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objCvgXLoss.LossCode);
                }
                //End Loss Type
                
                //   OR

                //Start Disablity Type
                if (objReserveNode.SelectNodes("//DisabilityCat", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//DisabilityCat", nsmgr).Item(0).InnerText = objCache.GetShortCode(objCvgXLoss.DisablityCategory);
                }

                if (objReserveNode.SelectNodes("//DisabilityCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//DisabilityCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objCvgXLoss.LossCode);
                }

                if (objReserveNode.SelectNodes("//DisabilityDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//DisabilityDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objCvgXLoss.LossCode);
                }
                //End Disablity Type

                if (objReserveNode.SelectNodes("//CoverageCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//CoverageCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objPolicyXCvgType.CoverageTypeCode);
                }

                if (objReserveNode.SelectNodes("//CoverageDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//CoverageDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objPolicyXCvgType.CoverageTypeCode);
                }

                if (objReserveNode.SelectNodes("//Limit", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//Limit", nsmgr).Item(0).InnerText = objPolicyXCvgType.Limit.ToString();
                }

                if (!objPolicyXUnit.PolicyUnitRowId.Equals(0)) //Coverage id related to reserve Table - POLICY_X_UNIT
                {
                    AddTagValue(objReserveNode, "OtherIdentifier", "PolicyXUnitId", "OtherId", objPolicyXUnit.PolicyUnitRowId.ToString(), nsmgr);
                }

                if (objReserveNode.SelectNodes("//UnitType", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//UnitType", nsmgr).Item(0).InnerText = objPolicyXUnit.UnitType;
                }

                switch (objPolicyXUnit.UnitType)
                {
                    case "V":
                        Vehicle objVehicle=null;

                        objVehicle = (Vehicle)objDMF.GetDataModelObject("Vehicle", false);
                        objVehicle.MoveTo(objPolicyXUnit.UnitId);

                        if (objReserveNode.SelectNodes("//Vin", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//Vin", nsmgr).Item(0).InnerText = objVehicle.Vin;
                        }
                        if (objReserveNode.SelectNodes("//LeasedVehInd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LeasedVehInd", nsmgr).Item(0).InnerText = objVehicle.LeaseFlag.ToString(); //0: False, -1: Ture
                        }
                        if (objReserveNode.SelectNodes("//LeasedDt", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LeasedDt", nsmgr).Item(0).InnerText = objVehicle.LeaseExpireDate;
                        }
                        if (objReserveNode.SelectNodes("//Manufacturer", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//Manufacturer", nsmgr).Item(0).InnerText = objVehicle.VehicleMake;
                        }
                       
                        if (objReserveNode.SelectNodes("//Model", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//Model", nsmgr).Item(0).InnerText = objVehicle.VehicleModel;
                        }
                        if (objReserveNode.SelectNodes("//ModelYear", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ModelYear", nsmgr).Item(0).InnerText = objVehicle.VehicleYear.ToString();
                        }
                       
                        if (objReserveNode.SelectNodes("//GrossVehOrCombinedWeight", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//GrossVehOrCombinedWeight", nsmgr).Item(0).InnerText = objVehicle.GrossWeight.ToString();
                        }
                        if (objReserveNode.SelectNodes("//LicensePlateNumber", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LicensePlateNumber", nsmgr).Item(0).InnerText = objVehicle.LicenseNumber;
                        }
                        if (objReserveNode.SelectNodes("//PurchaseDt", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PurchaseDt", nsmgr).Item(0).InnerText = objVehicle.PurchaseDate;
                        }
                        if (objReserveNode.SelectNodes("//CostNewAmt", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//CostNewAmt", nsmgr).Item(0).InnerText = objVehicle.OriginalCost.ToString();
                        }
                        if (objReserveNode.SelectNodes("//UnitTypeCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//UnitTypeCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objVehicle.UnitTypeCode);
                        }
                        if (objReserveNode.SelectNodes("//UnitTypeCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//UnitTypeCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objVehicle.UnitTypeCode);
                        }
                        if (objReserveNode.SelectNodes("//DisposalDate", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//DisposalDate", nsmgr).Item(0).InnerText = objVehicle.DisposalDate;
                        }
                        if (objReserveNode.SelectNodes("//LeaseAmount", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LeaseAmount", nsmgr).Item(0).InnerText = objVehicle.LeaseAmount.ToString();
                        }
                        if (objReserveNode.SelectNodes("//LeaseExpireDate", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LeaseExpireDate", nsmgr).Item(0).InnerText = objVehicle.LeaseExpireDate;
                        }
                        if (objReserveNode.SelectNodes("//LeaseNumber", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LeaseNumber", nsmgr).Item(0).InnerText = objVehicle.LeaseNumber;
                        }
                        if (objReserveNode.SelectNodes("//LeaseTerm", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LeaseTerm", nsmgr).Item(0).InnerText = objVehicle.LeaseTerm.ToString();
                        }
                        if (objReserveNode.SelectNodes("//LicenseRnwlDate", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LicenseRnwlDate", nsmgr).Item(0).InnerText = objVehicle.LicenseRnwlDate;
                        }
                        if (objReserveNode.SelectNodes("//LeasingCoEid", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LeasingCoEid", nsmgr).Item(0).InnerText = objVehicle.LeasingCoEid.ToString();
                        }
                        if (objReserveNode.SelectNodes("//LastServiceDate", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LastServiceDate", nsmgr).Item(0).InnerText = objVehicle.LastServiceDate;
                        }
                        if (objReserveNode.SelectNodes("//TypeOfService", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//TypeOfService", nsmgr).Item(0).InnerText = objVehicle.TypeOfService;
                        }
                        //Start Averma62
                        if (objReserveNode.SelectNodes("//InsuranceCoverage", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//InsuranceCoverage", nsmgr).Item(0).InnerText = objVehicle.InsuranceCoverage;
                        }

                        if (objReserveNode.SelectNodes("//HomeDeptEid", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//HomeDeptEid", nsmgr).Item(0).InnerText = objVehicle.HomeDeptEid.ToString();
                        }
                        //End Averma62
                        objVehicle = null;
                        break;

                    case "P":
                        PropertyUnit objProperty = null;
                        objProperty = (PropertyUnit)objDMF.GetDataModelObject("PropertyUnit", false);
                        objProperty.MoveTo(objPolicyXUnit.UnitId);
                        if (objReserveNode.SelectNodes("//Pin", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//Pin", nsmgr).Item(0).InnerText = objProperty.Pin;
                        }
                        if (objReserveNode.SelectNodes("//ClassOfConstruction", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ClassOfConstruction", nsmgr).Item(0).InnerText = objProperty.ClassOfConstruction.ToString();
                        }
                        if (objReserveNode.SelectNodes("//YearOfConstruction", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//YearOfConstruction", nsmgr).Item(0).InnerText = objProperty.YearOfConstruction.ToString();
                        }
                        if (objReserveNode.SelectNodes("//WallConstructionCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//WallConstructionCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.WallConstructionCode);
                        }
                        if (objReserveNode.SelectNodes("//WallConstructionCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//WallConstructionCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.WallConstructionCode);
                        }
                        if (objReserveNode.SelectNodes("//RoofConstructionCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//RoofConstructionCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.RoofConstructionCode);
                        }
                        if (objReserveNode.SelectNodes("//RoofConstructionCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//RoofConstructionCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.RoofConstructionCode);
                        }
                        if (objReserveNode.SelectNodes("//SquareFootage", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SquareFootage", nsmgr).Item(0).InnerText = objProperty.SquareFootage.ToString();
                        }
                        if (objReserveNode.SelectNodes("//NoOfStories", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//NoOfStories", nsmgr).Item(0).InnerText = objProperty.NoOfStories.ToString();
                        }
                        if (objReserveNode.SelectNodes("//AvgStoryHeight", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//AvgStoryHeight", nsmgr).Item(0).InnerText = objProperty.AvgStoryHeight.ToString();
                        }
                        if (objReserveNode.SelectNodes("//HeatingSysCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//HeatingSysCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.HeatingSysCode);
                        }
                        if (objReserveNode.SelectNodes("//HeatingSysCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//HeatingSysCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.HeatingSysCode);
                        }
                        if (objReserveNode.SelectNodes("//CoolingSysCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//CoolingSysCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.CoolingSysCode);
                        }
                        if (objReserveNode.SelectNodes("//CoolingSysDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//CoolingSysDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.CoolingSysCode);
                        }
                        if (objReserveNode.SelectNodes("//FireAlarmCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//FireAlarmCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.FireAlarmCode);
                        }
                        if (objReserveNode.SelectNodes("//FireAlarmCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//FireAlarmCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.FireAlarmCode);
                        }
                        if (objReserveNode.SelectNodes("//SprinklersCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SprinklersCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.SprinklersCode);
                        }
                        if (objReserveNode.SelectNodes("//SprinklersCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SprinklersCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.SprinklersCode);
                        }
                        if (objReserveNode.SelectNodes("//EntryAlarmCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//EntryAlarmCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.EntryAlarmCode);
                        }
                        if (objReserveNode.SelectNodes("//EntryAlarmCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//EntryAlarmCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.EntryAlarmCode);
                        }
                        if (objReserveNode.SelectNodes("//PlotPlansCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PlotPlansCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.PlotPlansCode);
                        }
                        if (objReserveNode.SelectNodes("//PlotPlansCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PlotPlansCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.PlotPlansCode);
                        }
                        if (objReserveNode.SelectNodes("//FloodZoneCertCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//FloodZoneCertCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.FloodZoneCertCode);
                        }
                        if (objReserveNode.SelectNodes("//FloodZoneCertCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//FloodZoneCertCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.FloodZoneCertCode);
                        }
                        if (objReserveNode.SelectNodes("//GPSLatitude", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//GPSLatitude", nsmgr).Item(0).InnerText = objProperty.GPSLongitude.ToString();
                        }
                        if (objReserveNode.SelectNodes("//EarthquakeZoneCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//EarthquakeZoneCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.EarthquakeZoneCode);
                        }
                        if (objReserveNode.SelectNodes("//EarthquakeZoneCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//EarthquakeZoneCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.EarthquakeZoneCode);
                        }
                        if (objReserveNode.SelectNodes("//GPSLongitude", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//GPSLongitude", nsmgr).Item(0).InnerText = objProperty.GPSLatitude.ToString();
                        }
                        if (objReserveNode.SelectNodes("//RoofAnchoringCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//RoofAnchoringCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.RoofAnchoringCode);
                        }
                        if (objReserveNode.SelectNodes("//RoofAnchoringCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//RoofAnchoringCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.RoofAnchoringCode);
                        }
                        if (objReserveNode.SelectNodes("//GPSAltitude", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//GPSAltitude", nsmgr).Item(0).InnerText = objProperty.GPSAltitude.ToString();
                        }
                        if (objReserveNode.SelectNodes("//GlassStrengthCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//GlassStrengthCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.GlassStrengthCode);
                        }
                        if (objReserveNode.SelectNodes("//GlassStrengthCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//GlassStrengthCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.GlassStrengthCode);
                        }
                        if (objReserveNode.SelectNodes("//AppraisedValue", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//AppraisedValue", nsmgr).Item(0).InnerText = objProperty.AppraisedValue.ToString();
                        }
                        if (objReserveNode.SelectNodes("//ReplacementValue", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ReplacementValue", nsmgr).Item(0).InnerText = objProperty.ReplacementValue.ToString();
                        }
                        if (objReserveNode.SelectNodes("//AppraisedDate", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//AppraisedDate", nsmgr).Item(0).InnerText = objProperty.AppraisedDate;
                        }
                        if (objReserveNode.SelectNodes("//LandValue", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//LandValue", nsmgr).Item(0).InnerText = objProperty.LandValue.ToString();
                        }
                        if (objReserveNode.SelectNodes("//TerritoryCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//TerritoryCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.TerritoryCode);
                        }
                        if (objReserveNode.SelectNodes("//TerritoryCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//TerritoryCodeDesc", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.TerritoryCode);
                        }
                        if (objReserveNode.SelectNodes("//CategoryCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//CategoryCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.AppraisalSourceCode);
                        }
                        if (objReserveNode.SelectNodes("//CategoryCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//CategoryCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.CategoryCode);
                        }
                        if (objReserveNode.SelectNodes("//AppraisalSourceCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//AppraisalSourceCode", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.AppraisalSourceCode);
                        }
                        if (objReserveNode.SelectNodes("//AppraisalSourceCodeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//AppraisalSourceCodeDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.AppraisalSourceCode);
                        }

                        if (objReserveNode.SelectNodes("//PropertyInfo/Addr/Addr1", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PropertyInfo/Addr/Addr1", nsmgr).Item(0).InnerText = objProperty.Addr1;
                        }
                      
                        if (objReserveNode.SelectNodes("//PropertyInfo/Addr/Addr2", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PropertyInfo/Addr/Addr2", nsmgr).Item(0).InnerText = objProperty.Addr2;
                        }

                        if (objReserveNode.SelectNodes("//PropertyInfo/Addr/City", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PropertyInfo/Addr/City", nsmgr).Item(0).InnerText = objProperty.City;
                        }

                        if (objReserveNode.SelectNodes("//PropertyInfo/Addr/StateProvCd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PropertyInfo/Addr/StateProvCd", nsmgr).Item(0).InnerText = objCache.GetStateCode(objProperty.StateId);
                        }

                        if (objReserveNode.SelectNodes("//PropertyInfo/Addr/PostalCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PropertyInfo/Addr/PostalCode", nsmgr).Item(0).InnerText = objProperty.ZipCode;
                        }

                        if (objReserveNode.SelectNodes("//PropertyInfo/Addr/CountryCd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PropertyInfo/Addr/CountryCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objProperty.CountryCode);
                        }
                        //Start Ashish
                        if (objReserveNode.SelectNodes("//PropertyInfo/Addr/CountryCdDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PropertyInfo/Addr/CountryCdDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objProperty.CountryCode);
                        }
                        //End Ashish
                        objProperty = null;
                        break;

                    case "S":
                        SiteUnit objSite = null;
                        objSite = (SiteUnit)objDMF.GetDataModelObject("SiteUnit", false);
                        objSite.MoveTo(objPolicyXUnit.UnitId);
                        if (objReserveNode.SelectNodes("//SiteNumber", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteNumber", nsmgr).Item(0).InnerText = objSite.SiteNumber;
                        }
                        if (objReserveNode.SelectNodes("//Name", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//Name", nsmgr).Item(0).InnerText = objSite.Name;
                        }
                        if (objReserveNode.SelectNodes("//TaxLocation", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//TaxLocation", nsmgr).Item(0).InnerText = objSite.TaxLocation;
                        }
                        if (objReserveNode.SelectNodes("//UnEmploymentNumber", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//UnEmploymentNumber", nsmgr).Item(0).InnerText = objSite.UnemployementNumber;
                        }
                        if (objReserveNode.SelectNodes("//NumberOfEmployees", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//NumberOfEmployees", nsmgr).Item(0).InnerText = objSite.NoOfEmp.ToString();
                        }
                        if (objReserveNode.SelectNodes("//FEIN", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//FEIN", nsmgr).Item(0).InnerText = objSite.FEIN;
                        }
                        if (objReserveNode.SelectNodes("//SIC", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SIC", nsmgr).Item(0).InnerText = objSite.SIC;
                        }

                       if (objReserveNode.SelectNodes("//SiteInfo/Addr/Addr1", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteInfo/Addr/Addr1", nsmgr).Item(0).InnerText = objSite.Adsress1;
                        }
                      
                        if (objReserveNode.SelectNodes("//SiteInfo/Addr/Addr2", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteInfo/Addr/Addr2", nsmgr).Item(0).InnerText = objSite.Adsress2;
                        }

                        if (objReserveNode.SelectNodes("//SiteInfo/Addr/City", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteInfo/Addr/City", nsmgr).Item(0).InnerText = objSite.City;
                        }

                        if (objReserveNode.SelectNodes("//SiteInfo/Addr/StateProvCd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteInfo/Addr/StateProvCd", nsmgr).Item(0).InnerText = objCache.GetStateCode(objSite.StateId);
                        }

                        if (objReserveNode.SelectNodes("//SiteInfo/Addr/PostalCode", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteInfo/Addr/PostalCode", nsmgr).Item(0).InnerText = objSite.ZipCode;
                        }

                        if (objReserveNode.SelectNodes("//SiteInfo/Addr/CountryCd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteInfo/Addr/CountryCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objSite.CountryId);
                        }

                        if (objReserveNode.SelectNodes("//SiteInfo/Addr/CountryCdDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//SiteInfo/Addr/CountryCdDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objSite.CountryId);
                        }
                        
                        objSite = null;
                        break;
                }

                objPolicy = (Policy)objDMF.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(objPolicyXUnit.PolicyId);
                if (!objPolicyXUnit.PolicyId.Equals(0))                         //Policy id related to reserve Table - POLICY
                {
                    AddTagValue(objReserveNode, "OtherIdentifier", "PolicyId", "OtherId", objPolicy.PolicyId.ToString(), nsmgr);
                }

                if (objReserveNode.SelectNodes("//PolicyName", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//PolicyName", nsmgr).Item(0).InnerText = objPolicy.PolicyName;
                }
                if (objReserveNode.SelectNodes("//PolicyNumber", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//PolicyNumber", nsmgr).Item(0).InnerText = objPolicy.PolicyNumber;
                }
                if (objReserveNode.SelectNodes("//PolicyStatusCd", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//PolicyStatusCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(objPolicy.PolicyStatusCode);
                }
                if (objReserveNode.SelectNodes("//PolicyStatusDesc", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//PolicyStatusDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objPolicy.PolicyStatusCode);
                }
                if (objReserveNode.SelectNodes("//PolicyEffDt", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//PolicyEffDt", nsmgr).Item(0).InnerText = objPolicy.EffectiveDate;
                }
                if (objReserveNode.SelectNodes("//PolicySymbol", nsmgr).Count > 0)
                {
                    objReserveNode.SelectNodes("//PolicySymbol", nsmgr).Item(0).InnerText = objPolicy.PolicySymbol;
                }
                if (objReserveNode.SelectNodes("//InsurerId", nsmgr).Count > 0)
                {
                    objEntity.MoveTo(objClaimant.InsurerEid);
                    string name = "";
                    if (!string.IsNullOrEmpty(objEntity.FirstName))
                        name = objEntity.LastName + ", " + objEntity.FirstName;
                    else
                        name = objEntity.LastName;
                    objReserveNode.SelectNodes("//InsurerId", nsmgr).Item(0).InnerText = name;
                }

                if (objPolicy.InsurerEid > 0)
                {
                    objEntity.MoveTo(objPolicy.InsurerEid);

                    if (objReserveNode.SelectNodes("//MiscParty/GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong", nsmgr).Count > 0)
                    {
                        objReserveNode.SelectNodes("//MiscParty/GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong", nsmgr).Item(0).InnerText = objEntity.LastName;
                    }
                    if (objReserveNode.SelectNodes("//MiscParty/GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong", nsmgr).Count > 0)
                    {
                        objReserveNode.SelectNodes("//MiscParty/GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong", nsmgr).Item(0).InnerText = objEntity.FirstName;
                    }
                    if (objReserveNode.SelectNodes("//MiscParty/GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong", nsmgr).Count > 0)
                    {
                        objReserveNode.SelectNodes("//MiscParty/GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong", nsmgr).Item(0).InnerText = objEntity.MiddleName;
                    }
                }

                objReserveXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objReserveNode);
                objDMF.Dispose();

                objReserveXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objReserveXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq").Item(0));

                if(p_iReserveId > 0)
                    sXml = objReserveXml.OuterXml;
                else
                    sXml = "";			// if there is no Reserve to export then don't call web service

                //Logging and other methods

                if (!string.IsNullOrEmpty(sXml.Trim()))
                {
                    if (VSSExportSection.Logging.ToUpper().Equals("YES"))
                    {
                        iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                        iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
                    }
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "STRT", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                    //logging3 - begin getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BGET", this.RiskmasterConnectionString, m_iClientId);

                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXml);

                    //logging4 - end getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EGET", this.RiskmasterConnectionString, m_iClientId);

                    if (objDoc != null)
                    {
                        //Instantiate the service only if document information exists
                        objVSSDataExchange = new VSSDataExchangeServiceClient();
                        EndpointAddress oAdd = new EndpointAddress(VSSExportSection.VSSExportUrl);
                        objVSSDataExchange.Endpoint.Address = oAdd;

                        //logging7 - begin webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BWEB", this.RiskmasterConnectionString, m_iClientId);

                        sResponse = objVSSDataExchange.Execute(objDoc.OuterXml);
                        //Close the opened service
                        objVSSDataExchange.Close();

                        //logging8 - end webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EWEB", this.RiskmasterConnectionString, m_iClientId);

                        if (!string.IsNullOrEmpty(sResponse.Trim()))
                        {
                            objResponseDoc = new XmlDocument();
                            objResponseDoc.LoadXml(sResponse);

                            foreach (XmlNode objResponseNode in objResponseDoc.GetElementsByTagName(sStatusNode))
                            {
                                objStatusCdNode = objResponseNode.SelectSingleNode("//VSSWebServiceInfo/WasSuccessful");
                                objErrorCdNode = objResponseNode.SelectSingleNode("//VSSWebServiceInfo/WasSuccessful");
                                objStatusDescNode = objResponseNode.SelectSingleNode("//VSSWebServiceInfo/ServiceMessage");
                                objReqIdNode = objResponseNode.SelectSingleNode("ReqID");

                                if (objStatusCdNode != null)
                                    sStatusCd = objStatusCdNode.InnerText;

                                if (objErrorCdNode != null)
                                    sErrorCd = objErrorCdNode.InnerText;

                                if (objStatusDescNode != null)
                                    sStatusDesc = objStatusDescNode.InnerText;

                                if (objReqIdNode != null)
                                    sReqId = objReqIdNode.InnerText;

                                //logging9 - final entry
                                if (objStatusCdNode != null && objStatusCdNode.InnerText.ToUpper().Equals("TRUE"))
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "CMPD", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
                                }
                                else
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "FAIL", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
                                }
                                objReqIdNode = null;
                                objStatusCdNode = null;
                                objErrorCdNode = null;
                                objStatusDescNode = null;
                            }	//foreach

                            objResponseDoc = null;
                        }
                    }

                    objDoc = null;
                } //if block end
            
            }
            catch (FaultException p_objException)
            {
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            catch (Exception p_objException)
            {
                //logging10 - log exception
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
           
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }

                if (objEntity != null)
                {
                    objEntity.Dispose();
                }

                if (objClaim != null)
                {
                    objClaim.Dispose();
                }

                if (objClaimant != null)
                {
                    objClaimant.Dispose();
                }

                if (objCvgXLoss != null)
                {
                    objCvgXLoss.Dispose();
                }

                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                }

                if (objPolicyXCvgType != null)
                {
                    objPolicyXCvgType.Dispose();
                }

                if (objPolicyXUnit != null)
                {
                    objPolicyXUnit.Dispose();
                }

                if (objReserveNode != null)
                {
                    objReserveNode = null;
                }

                if (objSettings != null)
                {
                    objSettings = null;
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
               
                if (objConn != null)
                {
                    if (objConn.State == ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn = null;
                }
                objUserLogin = null;
                nsmgr = null;
                objReserveXml = null;
            }
            return true;
        }
        #endregion

        #region Check Status Export Function
        /// Name		: Check Status Export 
        /// Author		: Anshul Verma
        /// Date Created: 12/26/2012
        /// ************************************************************
        /// <summary>		
        /// This method is used to export check status from RMX to VSS
        /// </summary>
        ///

        public bool CheckStatusExport(int p_iTransid)
        {
            string sClaimNumber = "";
            string sInvoiceNumber = "";
            string sRqUID = "";
            string sMainRqUID = "";
            DataModelFactory objDMF = null;
            UserLogin objUserLogin = null;
            Claim objClaim = null;
            Funds objFund = null;
            LocalCache objCache = null;
            XmlDocument objCheckStatusXml = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode objCheckStatusNode = null;

            //logging1
            int iReqId = 0;
            int iEventId = 0;
            bool bSuccess = false;
            string sPackType = "CHKST";
            string sResponse = string.Empty, sReqId = string.Empty;
            string sStatusDesc = string.Empty, sStatusCd = string.Empty, sStatusNode = "PasswordAssignRs", sErrorCd = string.Empty;
            string sXml = string.Empty;
            XmlNode objReqIdNode = null;
            XmlNode objStatusCdNode = null;
            XmlNode objErrorCdNode = null;
            XmlNode objStatusDescNode = null;
            XmlDocument objDoc = null;
            XmlDocument objResponseDoc = null;
            try
            {
                if (!File.Exists(sPaymentVoidExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("VSS.LoadFile.FileNotFound", m_iClientId)));

                objCheckStatusXml = new XmlDocument();
                try
                {
                    objCheckStatusXml.Load(sPaymentVoidExportXmlPath);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("VSSExportProcess.CheckStatusExportXmlNotFound", m_iClientId), p_objException);
                }

                objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDsnName,0);
                objDMF = new DataModelFactory(objUserLogin, m_iClientId);

                objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString,m_iClientId);
                sMainRqUID = Guid.NewGuid().ToString();						// this will generate a unique Req ID each time Reserve Data Exchange 
                sRqUID = sMainRqUID;			                            //set the default value for sRqUID same as sMainRqUID
                foreach (XmlNode objRqNode in objCheckStatusXml.GetElementsByTagName("RqUID"))
                {
                    objRqNode.InnerText = sMainRqUID;
                }

                foreach (XmlNode objOrg in objCheckStatusXml.GetElementsByTagName("Org"))
                {
                    objOrg.InnerText =m_sDsnName;
                }

                nsmgr = new XmlNamespaceManager(objCheckStatusXml.NameTable);
                nsmgr.AddNamespace("csc", objCheckStatusXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                objFund = (Funds)objDMF.GetDataModelObject("Funds", false);
                objFund.MoveTo(p_iTransid);
                objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                objClaim.MoveTo(objFund.ClaimId);
                sClaimNumber = objClaim.ClaimNumber;

                objCheckStatusNode = objCheckStatusXml.GetElementsByTagName("ClaimsSubsequentRptSubmitRq").Item(0).Clone();

                if (objCheckStatusNode.SelectNodes("RqUID").Count > 0)
                {
                    sRqUID = Guid.NewGuid().ToString();
                    objCheckStatusNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
                }

                if (objCheckStatusNode.SelectNodes("//TransactionRequestDt").Count > 0)
                {
                    objCheckStatusNode.SelectNodes("//TransactionRequestDt").Item(0).InnerText = Conversion.GetDate(DateTime.Now.ToString());
                }

                foreach (FundsTransSplit obj in objFund.TransSplitList)
                {
                    if (string.IsNullOrEmpty(sInvoiceNumber))
                    {
                        sInvoiceNumber = obj.InvoiceNumber;
                    }
                    else
                    {
                        sInvoiceNumber = sInvoiceNumber + "," + obj.InvoiceNumber;
                    }
                }
                if (!p_iTransid.Equals(0))
                {
                    AddTagValue(objCheckStatusNode, "OtherIdentifier", "csc:TransId", "OtherId", p_iTransid.ToString(), nsmgr);
                }

                if (!objFund.ClaimId.Equals(0))
                {
                    AddTagValue(objCheckStatusNode, "OtherIdentifier", "csc:ClaimId", "OtherId", objFund.ClaimId.ToString(), nsmgr);
                }

                if (!string.IsNullOrEmpty(sClaimNumber))
                {
                    AddTagValue(objCheckStatusNode, "OtherIdentifier", "csc:ClaimNumber", "OtherId", sClaimNumber, nsmgr);
                }

                if (!string.IsNullOrEmpty(sInvoiceNumber))
                {
                    AddTagValue(objCheckStatusNode, "OtherIdentifier", "csc:VendorInvNum", "OtherId", sInvoiceNumber, nsmgr);
                }

                if (!string.IsNullOrEmpty(objFund.CtlNumber))
                {
                    AddTagValue(objCheckStatusNode, "OtherIdentifier", "csc:ControlNumber", "OtherId", objFund.CtlNumber, nsmgr);
                }

                if (objCheckStatusNode.SelectNodes("//csc:CheckStatus", nsmgr).Count > 0)
                {
                    objCheckStatusNode.SelectNodes("//csc:CheckStatus", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objFund.StatusCode);
                }

                if (objCheckStatusNode.SelectNodes("//csc:DateOfCheck", nsmgr).Count > 0)
                {
                    objCheckStatusNode.SelectNodes("//csc:DateOfCheck", nsmgr).Item(0).InnerText = objFund.DateOfCheck;
                }

                if (objCheckStatusNode.SelectNodes("//csc:TransDate", nsmgr).Count > 0)
                {
                    objCheckStatusNode.SelectNodes("//csc:TransDate", nsmgr).Item(0).InnerText = objFund.TransDate;
                }

                if (objCheckStatusNode.SelectNodes("//csc:VoidPaymentFlag", nsmgr).Count > 0)
                {
                    objCheckStatusNode.SelectNodes("//csc:VoidPaymentFlag", nsmgr).Item(0).InnerText = "F";
                }

                objCheckStatusXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objCheckStatusNode);
                objDMF.Dispose();

                objCheckStatusXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objCheckStatusXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq").Item(0));

                if (p_iTransid > 0)
                    sXml = objCheckStatusXml.OuterXml;
                else
                    sXml = "";			// if there is no Reserve to export then don't call web service
                //Logging and other methods

                if (!string.IsNullOrEmpty(sXml.Trim()))
                {
                    if (VSSExportSection.Logging.ToUpper().Equals("YES"))
                    {
                        iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                        iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
                    }
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "STRT", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                    //logging3 - begin getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BGET", this.RiskmasterConnectionString, m_iClientId);

                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXml);

                    //logging4 - end getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EGET", this.RiskmasterConnectionString, m_iClientId);

                    if (objDoc != null)
                    {
                        //Instantiate the service only if document information exists
                        objVSSDataExchange = new VSSDataExchangeServiceClient();

                        EndpointAddress oAdd = new EndpointAddress(VSSExportSection.VSSExportUrl);
                        objVSSDataExchange.Endpoint.Address = oAdd;

                        //logging7 - begin webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BWEB", this.RiskmasterConnectionString, m_iClientId);

                        sResponse = objVSSDataExchange.Execute(objDoc.OuterXml);

                        //Close the opened service
                        objVSSDataExchange.Close();

                        //logging8 - end webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EWEB", this.RiskmasterConnectionString, m_iClientId);

                        if (!string.IsNullOrEmpty(sResponse.Trim()))
                        {
                            objResponseDoc = new XmlDocument();
                            objResponseDoc.LoadXml(sResponse);

                            foreach (XmlNode objResponseNode in objResponseDoc.GetElementsByTagName(sStatusNode))
                            {
                                objStatusCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusCd");
                                objErrorCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgErrorCd");
                                objStatusDescNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusDesc");
                                objReqIdNode = objResponseNode.SelectSingleNode("RqUID");

                                if (objStatusCdNode != null)
                                    sStatusCd = objStatusCdNode.InnerText;

                                if (objErrorCdNode != null)
                                    sErrorCd = objErrorCdNode.InnerText;

                                if (objStatusDescNode != null)
                                    sStatusDesc = objStatusDescNode.InnerText;

                                if (objReqIdNode != null)
                                    sReqId = objReqIdNode.InnerText;

                                //logging9 - final entry
                                if (objStatusCdNode != null && objStatusCdNode.InnerText.Equals("Success"))
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "CMPD", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
                                }
                                else
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "FAIL", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
                                }

                                objReqIdNode = null;
                                objStatusCdNode = null;
                                objErrorCdNode = null;
                                objStatusDescNode = null;
                            }	//foreach

                            objResponseDoc = null;
                        }
                    }

                    objDoc = null;

                } //if block end

            }
            catch (FaultException p_objException)
            {
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            catch (Exception p_objException)
            {
                //logging10 - log exception
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
                if (objFund != null)
                {
                    objFund.Dispose();
                }
                if (objCheckStatusNode != null)
                {
                    objCheckStatusNode = null;
                }
                objUserLogin = null;
                nsmgr = null;
                objCheckStatusXml = null;
            }
            return true;
        }
        #endregion

        #region PaymentVoid Export Function
        /// Name		: Payment Void Export 
        /// Author		: Anshul Verma
        /// Date Created: 12/26/2012
        /// ************************************************************
        /// <summary>		
        /// This method is used to export paymnet void data from RMX to VSS
        /// </summary>
        /// 
        public bool PaymentVoidExport(int p_iTransid)
        {
            string sClaimNumber = "";
            string sInvoiceNumber = "";
            string sRqUID = "";
            string sMainRqUID = "";
            DataModelFactory objDMF = null;
            UserLogin objUserLogin = null;
            Claim objClaim = null;
            Funds objFund = null;
            LocalCache objCache = null;
            XmlDocument objPaymentVoidXml = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode objPaymentVoidNode = null;
            //logging1
            int iReqId = 0;
            int iEventId = 0;
            bool bSuccess = false;
            string sPackType = "PAYVO";
            string sResponse = string.Empty, sReqId = string.Empty;
            string sStatusDesc = string.Empty, sStatusCd = string.Empty, sStatusNode = "PasswordAssignRs", sErrorCd = string.Empty;
            string sXml = string.Empty;
            XmlNode objReqIdNode = null;
            XmlNode objStatusCdNode = null;
            XmlNode objErrorCdNode = null;
            XmlNode objStatusDescNode = null;
            XmlDocument objDoc = null;
            XmlDocument objResponseDoc = null;

            try
            {
                if (!File.Exists(sPaymentVoidExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("VSS.LoadFile.FileNotFound", m_iClientId)));

                objPaymentVoidXml = new XmlDocument();
                try
                {
                    objPaymentVoidXml.Load(sPaymentVoidExportXmlPath);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("VSSExportProcess.PaymentVoidExportXmlNotFound", m_iClientId), p_objException);
                }

                objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDsnName,0);
                objDMF = new DataModelFactory(objUserLogin, m_iClientId);

                objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString,m_iClientId);
                sMainRqUID = Guid.NewGuid().ToString();						// this will generate a unique Req ID each time Reserve Data Exchange 
                sRqUID = sMainRqUID;			                            //set the default value for sRqUID same as sMainRqUID
                foreach (XmlNode objRqNode in objPaymentVoidXml.GetElementsByTagName("RqUID"))
                {
                    objRqNode.InnerText = sMainRqUID;
                }

                foreach (XmlNode objOrg in objPaymentVoidXml.GetElementsByTagName("Org"))
                {
                    objOrg.InnerText = m_sDsnName;
                }

                nsmgr = new XmlNamespaceManager(objPaymentVoidXml.NameTable);
                nsmgr.AddNamespace("csc", objPaymentVoidXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                objFund = (Funds)objDMF.GetDataModelObject("Funds", false);
                objFund.MoveTo(p_iTransid);
                objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                objClaim.MoveTo(objFund.ClaimId);
                sClaimNumber = objClaim.ClaimNumber;

                objPaymentVoidNode = objPaymentVoidXml.GetElementsByTagName("ClaimsSubsequentRptSubmitRq").Item(0).Clone();

                if (objPaymentVoidNode.SelectNodes("RqUID").Count > 0)
                {
                    sRqUID = Guid.NewGuid().ToString();
                    objPaymentVoidNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
                }

                if (objPaymentVoidNode.SelectNodes("//TransactionRequestDt").Count > 0)
                {
                    objPaymentVoidNode.SelectNodes("//TransactionRequestDt").Item(0).InnerText = Conversion.GetDate(DateTime.Now.ToString()); ;
                }

                foreach (FundsTransSplit obj in objFund.TransSplitList)
                {
                      sInvoiceNumber = obj.InvoiceNumber;
                }
                if (!p_iTransid.Equals(0))
                {
                    AddTagValue(objPaymentVoidNode, "OtherIdentifier", "csc:TransId", "OtherId", p_iTransid.ToString(), nsmgr);
                }

                if (!objFund.ClaimId.Equals(0))
                {
                    AddTagValue(objPaymentVoidNode, "OtherIdentifier", "csc:ClaimId", "OtherId", objFund.ClaimId.ToString(), nsmgr);
                }

                if (!string.IsNullOrEmpty(sClaimNumber))
                {
                    AddTagValue(objPaymentVoidNode, "OtherIdentifier", "csc:ClaimNumber", "OtherId", sClaimNumber, nsmgr);
                }

                if (!string.IsNullOrEmpty(sInvoiceNumber))
                {
                    AddTagValue(objPaymentVoidNode, "OtherIdentifier", "csc:VendorInvNum", "OtherId", sInvoiceNumber, nsmgr);
                }

                if (!string.IsNullOrEmpty(objFund.CtlNumber))
                {
                    AddTagValue(objPaymentVoidNode, "OtherIdentifier", "csc:ControlNumber", "OtherId", objFund.CtlNumber, nsmgr);
                }

                if (objPaymentVoidNode.SelectNodes("//csc:VoidDate",nsmgr).Count > 0)
                {
                    objPaymentVoidNode.SelectNodes("//csc:VoidDate",nsmgr).Item(0).InnerText = objFund.VoidDate;
                }

                if (objPaymentVoidNode.SelectNodes("//csc:VoidReason",nsmgr).Count > 0)
                {
                    objPaymentVoidNode.SelectNodes("//csc:VoidReason",nsmgr).Item(0).InnerText = objFund.Reason;
                }

                if (objPaymentVoidNode.SelectNodes("//csc:VoidPaymentFlag", nsmgr).Count > 0)
                {
                    objPaymentVoidNode.SelectNodes("//csc:VoidPaymentFlag", nsmgr).Item(0).InnerText = "T";
                }
               
                if (objPaymentVoidNode.SelectNodes("//csc:TransDate",nsmgr).Count > 0)
                {
                    objPaymentVoidNode.SelectNodes("//csc:TransDate", nsmgr).Item(0).InnerText = objFund.TransDate;
                }

                //Asharma326 
                if (objPaymentVoidNode.SelectNodes("//csc:VoidAdjusterEmailAddress", nsmgr).Count > 0)
                {
                    objPaymentVoidNode.SelectNodes("//csc:VoidAdjusterEmailAddress", nsmgr).Item(0).InnerText = objUserLogin.objUser.Email; 
                }
                //Asharma326 

                objPaymentVoidXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objPaymentVoidNode);
                objDMF.Dispose();

                objPaymentVoidXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objPaymentVoidXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq").Item(0));

                if (p_iTransid > 0)
                    sXml = objPaymentVoidXml.OuterXml;
                else
                    sXml = "";			// if there is no Reserve to export then don't call web service
                //Logging and other methods

                if (!string.IsNullOrEmpty(sXml.Trim()))
                {
                    if (VSSExportSection.Logging.ToUpper().Equals("YES"))
                    {
                        iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                        iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
                    }
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "STRT", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                    //logging3 - begin getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BGET", this.RiskmasterConnectionString, m_iClientId);

                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXml);

                    //logging4 - end getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EGET", this.RiskmasterConnectionString, m_iClientId);

                    if (objDoc != null)
                    {
                        //Instantiate the service only if document information exists
                        objVSSDataExchange = new VSSDataExchangeServiceClient();

                        EndpointAddress oAdd = new EndpointAddress(VSSExportSection.VSSExportUrl);
                        objVSSDataExchange.Endpoint.Address = oAdd;

                        //logging7 - begin webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BWEB", this.RiskmasterConnectionString, m_iClientId);

                        sResponse = objVSSDataExchange.Execute(objDoc.OuterXml);

                        //Close the opened service
                        objVSSDataExchange.Close();

                        //logging8 - end webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EWEB", this.RiskmasterConnectionString, m_iClientId);

                        if (!string.IsNullOrEmpty(sResponse.Trim()))
                        {
                            objResponseDoc = new XmlDocument();
                            objResponseDoc.LoadXml(sResponse);

                            foreach (XmlNode objResponseNode in objResponseDoc.GetElementsByTagName(sStatusNode))
                            {
                                objStatusCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusCd");
                                objErrorCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgErrorCd");
                                objStatusDescNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusDesc");
                                objReqIdNode = objResponseNode.SelectSingleNode("RqUID");

                                if (objStatusCdNode != null)
                                    sStatusCd = objStatusCdNode.InnerText;

                                if (objErrorCdNode != null)
                                    sErrorCd = objErrorCdNode.InnerText;

                                if (objStatusDescNode != null)
                                    sStatusDesc = objStatusDescNode.InnerText;

                                if (objReqIdNode != null)
                                    sReqId = objReqIdNode.InnerText;

                                //logging9 - final entry
                                if (objStatusCdNode != null && objStatusCdNode.InnerText.Equals("Success"))
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "CMPD", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
                                }
                                else
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "FAIL", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
                                }

                                objReqIdNode = null;
                                objStatusCdNode = null;
                                objErrorCdNode = null;
                                objStatusDescNode = null;
                            }	//foreach
                            objResponseDoc = null;
                        }
                    }

                    objDoc = null;

                } //if block end
            }
            catch (FaultException p_objException)
            {
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            catch (Exception p_objException)
            {
                //logging10 - log exception
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
                if (objFund != null)
                {
                    objFund.Dispose();
                }
                if (objPaymentVoidNode != null)
                {
                    objPaymentVoidNode = null;
                }
                objUserLogin = null;
                nsmgr = null;
                objPaymentVoidXml = null;
            }
            return true;
        }
        #endregion

        #region Password Update Export Function
        /// Name		: Password Update Export 
        /// Author		: Anshul Verma
        /// Date Created: 12/26/2012
        /// ************************************************************
        /// <summary>		
        /// This method is used to export password update data from RMX to VSS
        /// </summary>
        /// 
        public bool PassUpdateExport(string p_sUserId, string p_sPassword, string p_sEmailId, string p_sDsn)
        {
            string sRqUID = "";
            string sMainRqUID = "";
            DataModelFactory objDMF = null;
            UserLogin objUserLogin = null;
            LocalCache objCache = null;
            XmlDocument objPassUpdateXml = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode objPassUpdateNode = null;

            int iReqId = 0;
            int iEventId = 0;
            bool bSuccess = false;
            string sPackType = "PROFUP";
            string sResponse = string.Empty, sReqId = string.Empty;
            string sStatusDesc = string.Empty, sStatusCd = string.Empty, sStatusNode = "PasswordAssignRs", sErrorCd = string.Empty;
            string sXml = string.Empty;
            XmlNode objReqIdNode = null;
            XmlNode objStatusCdNode = null;
            XmlNode objErrorCdNode = null;
            XmlNode objStatusDescNode = null;
            XmlDocument objDoc = null;
            XmlDocument objResponseDoc = null;

            try
            {
                if (!File.Exists(sPassUpdateExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("VSS.LoadFile.FileNotFound", m_iClientId)));
                objPassUpdateXml = new XmlDocument();
                try
                {
                    objPassUpdateXml.Load(sPassUpdateExportXmlPath);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("VSSExportProcess.PassUpdateExportXmlNotFound", m_iClientId), p_objException);
                }

                objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                objDMF = new DataModelFactory(objUserLogin, m_iClientId);

                objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString,m_iClientId);
                sMainRqUID = Guid.NewGuid().ToString();						// this will generate a unique Req ID each time Reserve Data Exchange 
                sRqUID = sMainRqUID;			                            //set the default value for sRqUID same as sMainRqUID
                foreach (XmlNode objRqNode in objPassUpdateXml.GetElementsByTagName("RqUID"))
                {
                    objRqNode.InnerText = sMainRqUID;
                }

                foreach (XmlNode objOrg in objPassUpdateXml.GetElementsByTagName("Org"))
                {
                    objOrg.InnerText = m_sDsnName;
                }

                nsmgr = new XmlNamespaceManager(objPassUpdateXml.NameTable);
                nsmgr.AddNamespace("csc", objPassUpdateXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                objPassUpdateNode = objPassUpdateXml.GetElementsByTagName("PasswordAssignRq").Item(0).Clone();

                if (objPassUpdateNode.SelectNodes("RqUID").Count > 0)
                {
                    sRqUID = Guid.NewGuid().ToString();
                    objPassUpdateNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
                }

                if (objPassUpdateNode.SelectNodes("//TransactionRequestDt").Count > 0)
                {
                    objPassUpdateNode.SelectNodes("//TransactionRequestDt").Item(0).InnerText = Conversion.GetDate(DateTime.Now.ToString());
                }

                if (objPassUpdateNode.SelectNodes("//rmAUserId").Count > 0)
                {
                    objPassUpdateNode.SelectNodes("//rmAUserId").Item(0).InnerText = p_sUserId;
                }

                if (objPassUpdateNode.SelectNodes("//rmAEncryptPswd").Count > 0)
                {
                    objPassUpdateNode.SelectNodes("//rmAEncryptPswd").Item(0).InnerText = RMCryptography.EncryptString(p_sPassword);
                }

                if (objPassUpdateNode.SelectNodes("//rmADSN").Count > 0)
                {
                    objPassUpdateNode.SelectNodes("//rmADSN").Item(0).InnerText = p_sDsn;
                }

                if (objPassUpdateNode.SelectNodes("//EmailId").Count > 0)
                {
                    objPassUpdateNode.SelectNodes("//EmailId").Item(0).InnerText = p_sEmailId;
                }

                objPassUpdateXml.SelectSingleNode("/ACORD/BaseSvcRq").AppendChild(objPassUpdateNode);
                objDMF.Dispose();

                objPassUpdateXml.SelectSingleNode("/ACORD/BaseSvcRq").RemoveChild(objPassUpdateXml.SelectNodes("/ACORD/BaseSvcRq/PasswordAssignRq").Item(0));

                if (!string.IsNullOrEmpty(p_sUserId))
                    sXml = objPassUpdateXml.OuterXml;
                else
                    sXml = "";
                //Logging and other methods

                if (!string.IsNullOrEmpty(sXml.Trim()))
                {
                    if (VSSExportSection.Logging.ToUpper().Equals("YES"))
                    {
                        iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                        iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
                    }
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "STRT", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDate(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                    //logging3 - begin getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BGET", this.RiskmasterConnectionString, m_iClientId);

                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXml);

                    //logging4 - end getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EGET", this.RiskmasterConnectionString, m_iClientId);

                    if (objDoc != null)
                    {
                        //Instantiate the service only if document information exists
                        objVSSDataExchange = new VSSDataExchangeServiceClient();

                        EndpointAddress oAdd = new EndpointAddress(VSSExportSection.VSSExportUrl);
                        objVSSDataExchange.Endpoint.Address = oAdd;

                        //logging7 - begin webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BWEB", this.RiskmasterConnectionString, m_iClientId);

                        sResponse = objVSSDataExchange.Execute(objDoc.OuterXml);

                        //Close the opened service
                        objVSSDataExchange.Close();

                        //logging8 - end webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EWEB", this.RiskmasterConnectionString, m_iClientId);

                        if (!string.IsNullOrEmpty(sResponse.Trim()))
                        {
                            objResponseDoc = new XmlDocument();
                            objResponseDoc.LoadXml(sResponse);

                            foreach (XmlNode objResponseNode in objResponseDoc.GetElementsByTagName(sStatusNode))
                            {
                                objStatusCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusCd");
                                objErrorCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgErrorCd");
                                objStatusDescNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusDesc");
                                objReqIdNode = objResponseNode.SelectSingleNode("RqUID");

                                if (objStatusCdNode != null)
                                    sStatusCd = objStatusCdNode.InnerText;

                                if (objErrorCdNode != null)
                                    sErrorCd = objErrorCdNode.InnerText;

                                if (objStatusDescNode != null)
                                    sStatusDesc = objStatusDescNode.InnerText;

                                if (objReqIdNode != null)
                                    sReqId = objReqIdNode.InnerText;

                                //logging9 - final entry
                                if (objStatusCdNode != null && objStatusCdNode.InnerText.Equals("Success"))
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "CMPD", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
                                }
                                else
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "FAIL", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
                                }

                                objReqIdNode = null;
                                objStatusCdNode = null;
                                objErrorCdNode = null;
                                objStatusDescNode = null;
                            }	//foreach

                            objResponseDoc = null;
                        }
                    }

                    objDoc = null;
                } //if block end

            }
            catch (FaultException p_objException)
            {
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            catch (Exception p_objException)
            {
                //logging10 - log exception
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            finally
            {
            }
            return true;
        }
        #endregion

        #region Private Functions
        /// Name		: AddTagValue
        /// Author		: Manoj Agrawal / Anshul Verma
        /// Date Created: 08/23/2006
        /// ************************************************************
        /// <summary>		
        /// AddTagValue() function works for following type of structure. It will put sValue under &lt;OtherId&gt; tag if we pass "OtherIdentifier" in sParent and "csc:SupervisorUserId" in sType and "OtherId" in sTag.
        ///	 &lt;OtherIdentifier&gt;
        ///		&lt;OtherIdTypeCd&gt;csc:SupervisorUserId&lt;/OtherIdTypeCd&gt;
        ///		&lt;OtherId&gt;SupervisorName&lt;/OtherId&gt; 
        ///	 &lt;/OtherIdentifier&gt;
        /// </summary>
        private void AddTagValue(XmlNode objNode, string sParent, string sType, string sTag, string sValue, XmlNamespaceManager nsmgr)
        {
            try
            {
                for (int i = 0; i < objNode.SelectNodes("//" + sParent, nsmgr).Count; i++)
                {
                    if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).HasChildNodes == true)
                    {
                        for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
                        {
                            if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText.Equals(sType))
                            {
                                goto setvalue;
                            }
                        }

                        continue;

                    setvalue:
                        for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
                        {
                            if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).Name.Equals(sTag))
                            {
                                objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText = sValue;
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
        }


        /// Name		: RemoveTag
        /// Author		: Manoj Agrawal / Anshul Verma
        /// Date Created: 08/23/2006
        /// ************************************************************
        /// <summary>		
        /// RemoveTag() function works for following type of structure. It will remove &lt;OtherIdTypeCd&gt; and &lt;OtherId&gt; tags if we pass "OtherIdentifier" in sParent and "csc:SupervisorUserId" in sTag.
        ///	 &lt;OtherIdentifier&gt;
        ///		&lt;OtherIdTypeCd&gt;csc:SupervisorUserId&lt;/OtherIdTypeCd&gt;
        ///		&lt;OtherId&gt;SupervisorName&lt;/OtherId&gt; 
        ///	 &lt;/OtherIdentifier&gt;
        /// </summary>		
        private void RemoveTag(XmlNode objNode, string sParent, string sTag, XmlNamespaceManager nsmgr)
        {
            int x = -1;

            try
            {
                for (int i = 0; i < objNode.SelectNodes("//" + sParent, nsmgr).Count; i++)
                {
                    if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).HasChildNodes == true)
                    {
                        for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
                        {
                            if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText.Equals(sTag))
                            {
                                x = i;
                                goto removenode;
                            }
                        }
                    }
                }

                return;

            removenode:
                objNode.SelectNodes("//" + sParent, nsmgr).Item(x).ParentNode.RemoveChild(objNode.SelectNodes("//" + sParent, nsmgr).Item(x));
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
        }
        #endregion

        #region Current Adjuster Export Function
        /// <summary>
        /// Asharma326 for exporting Current Adjuster to VSS
        /// </summary>
        /// <param name="m_iClaimId">current claim id</param>
        /// <param name="m_iAdjusterEid">current adjuster id</param>
        /// <returns></returns>
        internal bool AdjusterExport(int m_iClaimId, int m_iAdjusterEid, int p_iClientId)//rkaur27 : Cloud Change
        {
            Entity objClaimAdjuster = null;
            DataModelFactory objDMF = null;
            UserLogin objUserLogin = null;
            XmlDocument objCurrentAdjusterxml = null;
            Claim objClaim = null;
            string sMainRqUID = "";
            string sRqUID = "";
            XmlNamespaceManager nsmgr;
            XmlNode objAdjusterNode = null;
            string sXml = string.Empty;
            int iReqId = 0;
            DbReader objReader = null;
            int iEventId = 0;
            string sPackType = "ADJCURR";
            XmlDocument objDoc = null;
            string sResponse = string.Empty, sReqId = string.Empty;
            XmlDocument objResponseDoc = null;
            string sStatusDesc = string.Empty, sStatusCd = string.Empty, sStatusNode = "PasswordAssignRs", sErrorCd = string.Empty;//VSS MITS for Logging
            XmlNode objReqIdNode = null;
            XmlNode objStatusCdNode = null;
            XmlNode objErrorCdNode = null;
            XmlNode objStatusDescNode = null;
            try
            {

                if (!File.Exists(sCurentAdjusterExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("VSS.LoadFile.FileNotFound", m_iClientId)));

                objCurrentAdjusterxml = new XmlDocument();
                try
                {
                    objCurrentAdjusterxml.Load(sCurentAdjusterExportXmlPath);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("VSSExportProcess.CurrentAdjusterXmlNotFound", m_iClientId), p_objException);
                }
                objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                objDMF = new DataModelFactory(objUserLogin, m_iClientId);
                
                objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                objClaim.MoveTo(m_iClaimId);

                objClaimAdjuster = (Entity)objDMF.GetDataModelObject("Entity", false);
                objClaimAdjuster.MoveTo(m_iAdjusterEid);

                sMainRqUID = Guid.NewGuid().ToString();						// this will generate a unique Req ID each time Reserve Data Exchange 
                sRqUID = sMainRqUID;			                            //set the default value for sRqUID same as sMainRqUID
                foreach (XmlNode objRqNode in objCurrentAdjusterxml.GetElementsByTagName("RqUID"))
                {
                    objRqNode.InnerText = sMainRqUID;
                }

                foreach (XmlNode objOrg in objCurrentAdjusterxml.GetElementsByTagName("Org"))
                {
                    objOrg.InnerText = m_sDsnName;
                }

                nsmgr = new XmlNamespaceManager(objCurrentAdjusterxml.NameTable);
                nsmgr.AddNamespace("csc", objCurrentAdjusterxml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                objAdjusterNode = objCurrentAdjusterxml.GetElementsByTagName("ClaimsSubsequentRptSubmitRq").Item(0).Clone();

                if (!objClaimAdjuster.RMUserId.Equals(""))
                {
                    
                    objReader = DbFactory.GetDbReader(Security.SecurityDatabase.GetSecurityDsn(p_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE [USER_ID] = " + objClaimAdjuster.RMUserId + " AND DSNID= " +objUserLogin.objRiskmasterDatabase.DataSourceId);
                    string rmaUserID = string.Empty; ;
                    if (objReader.Read())
                    {
                        rmaUserID = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                    objReader.Close();
                    AddTagValue(objAdjusterNode, "OtherIdentifier", "AdjusterId", "OtherId", rmaUserID, nsmgr);
                }

                if (!m_iClaimId.Equals(0))
                {
                    AddTagValue(objAdjusterNode, "OtherIdentifier", "ClaimId", "OtherId", m_iClaimId.ToString(), nsmgr);
                }
                if (!objClaim.ClaimNumber.Equals(""))
                {
                    AddTagValue(objAdjusterNode, "OtherIdentifier", "ClaimNumber", "OtherId", objClaim.ClaimNumber.ToString(), nsmgr);
                }
                if (!objClaimAdjuster.Phone1.Equals(""))
                {
                    AddTagValue(objAdjusterNode, "AdjusterParty/GeneralPartyInfo/Communications/PhoneInfo", "Office", "PhoneNumber", objClaimAdjuster.Phone1, nsmgr);
                }
                if (!objClaimAdjuster.Phone2.Equals(""))
                {
                    AddTagValue(objAdjusterNode, "AdjusterParty/GeneralPartyInfo/Communications/PhoneInfo", "Home	Phone", "PhoneNumber", objClaimAdjuster.Phone2, nsmgr);
                }
                if (!objClaimAdjuster.FaxNumber.Equals(""))
                {
                    AddTagValue(objAdjusterNode, "AdjusterParty/GeneralPartyInfo/Communications/PhoneInfo", "Fax", "PhoneNumber", objClaimAdjuster.FaxNumber, nsmgr);
                }
                if (objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong", nsmgr).Count > 0)
                {
                    objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong", nsmgr).Item(0).InnerText = objClaimAdjuster.LastName;
                }
                if (objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong", nsmgr).Count > 0)
                {
                    objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong", nsmgr).Item(0).InnerText = objClaimAdjuster.FirstName;
                }
                if (objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong", nsmgr).Count > 0)
                {
                    objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong", nsmgr).Item(0).InnerText = objClaimAdjuster.MiddleName;
                }
                if (objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Count > 0)
                {
                    objAdjusterNode.SelectNodes("//AdjusterParty/GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr).Item(0).InnerText = objClaimAdjuster.EmailAddress;
                }

                objCurrentAdjusterxml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objAdjusterNode);
                objDMF.Dispose();

                objCurrentAdjusterxml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objCurrentAdjusterxml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq").Item(0));

                if (m_iAdjusterEid > 0)
                    sXml = objCurrentAdjusterxml.OuterXml;
                else
                    sXml = "";			// if there is no Current Adjuster then don't call web service
                
                #region "DB Logging And Web Service Call"
                

                if (!string.IsNullOrEmpty(sXml.Trim()))
                {
                    if (VSSExportSection.Logging.ToUpper().Equals("YES"))
                    {
                        iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST", m_iClientId);
                        iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "VSS_DX_REQUEST_EVENT", m_iClientId);
                    }
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "STRT", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                    //logging3 - begin getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BGET", this.RiskmasterConnectionString, m_iClientId);

                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXml);

                    //logging4 - end getdata
                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EGET", this.RiskmasterConnectionString, m_iClientId);

                    if (objDoc != null)
                    {
                        //Instantiate the service only if document information exists
                        objVSSDataExchange = new VSSDataExchangeServiceClient();
                        EndpointAddress oAdd = new EndpointAddress(VSSExportSection.VSSExportUrl);
                        objVSSDataExchange.Endpoint.Address = oAdd;

                        //logging7 - begin webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "BWEB", this.RiskmasterConnectionString, m_iClientId);

                        sResponse = objVSSDataExchange.Execute(objDoc.OuterXml);
                        //Close the opened service
                        objVSSDataExchange.Close();

                        //logging8 - end webservice call
                        Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "EWEB", this.RiskmasterConnectionString, m_iClientId);

                        if (!string.IsNullOrEmpty(sResponse.Trim()))
                        {
                            objResponseDoc = new XmlDocument();
                            objResponseDoc.LoadXml(sResponse);

                            foreach (XmlNode objResponseNode in objResponseDoc.GetElementsByTagName(sStatusNode))
                            {
                                objStatusCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusCd");
                                objErrorCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgErrorCd");
                                objStatusDescNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusDesc");
                                objReqIdNode = objResponseNode.SelectSingleNode("ReqID");

                                if (objStatusCdNode != null)
                                    sStatusCd = objStatusCdNode.InnerText;

                                if (objErrorCdNode != null)
                                    sErrorCd = objErrorCdNode.InnerText;

                                if (objStatusDescNode != null)
                                    sStatusDesc = objStatusDescNode.InnerText;

                                if (objReqIdNode != null)
                                    sReqId = objReqIdNode.InnerText;

                                //logging9 - final entry
                                if (objStatusCdNode != null && objStatusCdNode.InnerText.Equals("Success"))
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "CMPD", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
                                }
                                else
                                {
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "rmA", "FAIL", sXml.Trim(), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString, m_iClientId);
                                    Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
                                }
                                objReqIdNode = null;
                                objStatusCdNode = null;
                                objErrorCdNode = null;
                                objStatusDescNode = null;
                            }	//foreach

                            objResponseDoc = null;
                        }
                    }

                    objDoc = null;
                } //if block end
                #endregion

            }
            catch (FaultException p_objException)
            {
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
            catch (Exception p_objException)
            {
                //logging10 - log exception
                Riskmaster.Application.VSSInterface.VSSCommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }

            finally
            {

                if (objClaimAdjuster != null)
                {
                    objClaimAdjuster.Dispose();
                }

                if (objClaim != null)
                {
                    objClaim.Dispose();
                }

                if (objAdjusterNode != null)
                {
                    objAdjusterNode = null;
                }

                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }

                objUserLogin = null;
                nsmgr = null;
                objCurrentAdjusterxml = null;
            }
            return true;
        }
        #endregion

    }
}
