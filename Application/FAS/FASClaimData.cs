﻿using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Collections.Specialized;
using Riskmaster.Security.Encryption;
using System.Globalization;
using System.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace Riskmaster.Application.FAS
{
    ///************************************************************** 
    ///* $File		: FASClaimData.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 24-June-2013
    ///* $Author	: Anshul Verma
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    ///<summary>
    ///Author  :   Anshul Verma
    ///Dated   :   24-June-2013
    ///Purpose :   To generate claim data for FAS.
    /// </summary>

    public class FASClaimData
    {
         #region Variable Declarations

        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// ClientId for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId=0;
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        string sReserveExportXmlPath = string.Empty;

        internal string m_strAppFilesPath = string.Empty;
        private string m_sEmailId = string.Empty;
        private string m_sUserDsn = string.Empty;
        private string m_sUserId = string.Empty;
        private string m_sUserPassword = string.Empty;
        private DataModelFactory m_objDataModelFactory = null;

        #endregion

        #region Constructor		
		
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>

        public FASClaimData()
        {
            this.Initialize();
        }
        
        internal string AppFilesPath
        {
            get { return m_strAppFilesPath; }
            set { m_strAppFilesPath = value; }
        } // property AppFilesPath

        public FASClaimData(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;//Ash - cloud
			this.Initialize();
		}

        #endregion
        /// <summary>
        /// Initialize objects
        /// </summary>
        private void Initialize()
        {
            try
            {
                using (DataModelFactory dataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId))
                {
                    m_sConnectionString = dataModelFactory.Context.DbConn.ConnectionString;                    
                }
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);
                this.AppFilesPath = RMConfigurator.AppFilesPath;
                sReserveExportXmlPath = Path.Combine(this.AppFilesPath, @"FAS\ClaimsSubsequentRptSubmitRq.xml");
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FASClaimData.Initialize.ErrorInit",m_iClientId), p_objEx);
            }
        }

        private bool _isDisposed = false;
        /// <summary>
        /// Un Initialize the data model factory object. 
        /// </summary>
        public void Dispose()
        {
            //tkr nothing to do, but left method in case is being called somewhere
            if (!_isDisposed)
            {
                _isDisposed = true;
            }
            GC.SuppressFinalize(this);
        }

        #region Get Claim Status Codes
        public XmlDocument GetClaimStatus(XmlDocument p_objDoc)
        {
            string sSQL=string.Empty;
            XmlDocument objXmlDocument = null;
            XmlElement objElement = null;
            XmlNode objNode = null;
            StringBuilder sbSql = null;
            DataSet objDS = null;
            SysSettings objSysSetting = null;
            try
            {
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud
                objXmlDocument = new XmlDocument();
                objXmlDocument = p_objDoc;
                //sbSql = new StringBuilder();

                objNode = objXmlDocument.SelectSingleNode("//control[@name='lstAvailableLossComponents']");
                

                sbSql = new StringBuilder();
                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                sbSql.Append(" AND CODES.TABLE_ID=1002");
                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ORDER BY LOWER(CODES.SHORT_CODE) "); //asharma326 MITS 35079 Sorting

                objDS = DbFactory.GetDataSet(m_sConnectionString, sbSql.ToString(), m_iClientId);
                foreach (DataRow dr in objDS.Tables[0].Rows)
                {
                    objElement = objXmlDocument.CreateElement("option");
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                    objElement.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                   
                    if(objNode!=null)objNode.AppendChild(objElement);
                }

                objNode = objXmlDocument.SelectSingleNode("//control[@name='lstAvailableCTypeComponents']");
                sbSql = new StringBuilder();
                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                sbSql.Append(" AND CODES.TABLE_ID=1023");
                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ORDER BY LOWER(CODES.SHORT_CODE) "); //asharma326 MITS 35079 Sorting

                objDS = null;
                objDS = DbFactory.GetDataSet(m_sConnectionString, sbSql.ToString(), m_iClientId);
                foreach (DataRow dr in objDS.Tables[0].Rows)
                {
                    objElement = objXmlDocument.CreateElement("option");
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                    objElement.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();

                    if (objNode != null) objNode.AppendChild(objElement);
                }


                objNode = objXmlDocument.SelectSingleNode("//control[@name='FASServer']");
                objNode.InnerText = objSysSetting.FASServer;

                objNode = objXmlDocument.SelectSingleNode("//control[@name='FASUserId']");
                objNode.InnerText = objSysSetting.FASUserId;

                objNode = objXmlDocument.SelectSingleNode("//control[@name='FASPassword']");
                objNode.InnerText = RMCryptography.EncryptString(objSysSetting.FASPassword);


                objNode = objXmlDocument.SelectSingleNode("//control[@name='FASFolder']");
                objNode.InnerText = objSysSetting.FASFolder;


                //Asharma326 MITS 32386 Starts
                objNode = objXmlDocument.SelectSingleNode("//control[@name='FASFileLocation']");
                objNode.InnerText = objSysSetting.FASFileLocation;
                objNode = objXmlDocument.SelectSingleNode("//control[@name='FASSharedLocation']");
                objNode.InnerText = objSysSetting.FASSharedLocation;
                objNode = objXmlDocument.SelectSingleNode("//control[@name='RMALocation']");
                objNode.InnerText = RMConfigurator.BasePath + @"\userdata"; 
                //Asharma326 MITS 32386 Ends
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objElement != null)
                {
                    objElement = null;
                }

                if (objNode != null)
                {
                    objNode = null;
                }

                if (sbSql != null)
                {
                    sbSql = null;
                }

                if (objDS != null)
                {
                    objDS = null;
                }
            }
            return objXmlDocument;
                
        }
        #endregion 
    }
}
