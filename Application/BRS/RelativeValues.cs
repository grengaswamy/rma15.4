using System;
using System.Collections;

using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using Riskmaster.Common; 
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{
	/**************************************************************
	 * $File		: RelativeValues.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/04/2004
	 * $Author		: Navneet Sota
	 * $Comment		: This class contains the collection of RelativeValue objects
	 * $Source		: Riskmaster.Application.BRS
	**************************************************************/
	internal class RelativeValues:CollectionBase
	{
		#region Variable Declarations	

		/// <summary>
		/// RelativeValue Object
		/// </summary>
		private RelativeValue m_objRelativeValue;
        private int m_iClientId = 0;
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}
		# endregion
		
		#region Constructor
		/// Name		: RelativeValues
		/// Author		: Navneet Sota
		/// Date Created: 01/03/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
        internal RelativeValues(int p_iClientId)
		{
            m_iClientId = p_iClientId;
        }
		
		#endregion

		#region RelativeValue class
		/// <summary>
		/// Class containing RelativeValue variables.
		/// </summary>
		internal class RelativeValue
		{
			internal string m_sCPT="";
			internal string m_sByReport="";
			internal string m_sStarred="";
			internal string m_sQuestion="";
			internal string m_sAssistSurg="";
			internal string m_sAnesBr="";
			internal string m_sAnesNa="";
			internal string m_sGlblCode=""; // Medicare/Tx Follow up comes with Alpha codes. 
			internal string m_sSpecialty="";
            internal string m_sUniqueSub="";
            internal string m_sIndicator="";
            internal string m_sDesc="";
            internal string m_sSubCode="";
            internal string m_sNotCovered="";
            internal string m_sPOS="";
			internal string m_sTOS="";
			internal string m_sMODS="";

            internal double m_dFactor=0;
			internal double m_dAmount=0;
			internal double m_dProfComponent=0;
            internal double m_dAnesthesiaVu=0;
            
			internal int m_iTypeOfService=0;
			internal int m_iFollup=0;
			internal int m_iMatches=0;   //the number of fields that match the POS/TOS/MOD criteria passed in            
		}//end class

		#endregion

		#region Properties
			/// <summary>
			/// Internal property for Relative Value class
			/// </summary>
			internal RelativeValue RelativeValueObject
			{
				set
				{
					m_objRelativeValue = value;
				}
				get
				{
					if (m_objRelativeValue == null)
						m_objRelativeValue = new RelativeValue();
					return m_objRelativeValue;
				}
			}
		#endregion

		#region Internal Methods

		#region Add
		/// Name			: Add
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds RelativeValue Object to the collection
		/// </summary>
		internal void Add()
		{
			try
			{
				this.List.Add(m_objRelativeValue);
			}
			catch (RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ModifierValues.Add.DataErr", m_iClientId), p_objException);
			}
		}
		#endregion

		#region RelativeValue
		/// Name			: RelativeValue
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the RelativeValue object for that index
		/// </summary>
		internal RelativeValue this[int p_iIndex]
		{        
			get
			{
				return (RelativeValue)this.List[p_iIndex];
			}        
		}

		#endregion

		#region Return Number of items in Collection
		/// Name		: NumberofObjects
		/// Author		: Navneet Sota
		/// Date Created: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the number of RelativeValue objects in collection
		/// </summary>
		internal int NumberofObjects
		{        
			get
			{
				return this.List.Count;
			}        
		}

		#endregion

		#region Remove RelativeValue object
		/// Name			: RemoveObject
		/// Author			: Navneet Sota
		/// Date Created	: 01/03/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Remove RelativeValue obect from collection
		/// </summary>
		internal void RemoveObject(int p_iIndex)
		{        
			this.List.RemoveAt(p_iIndex);
		}

		#endregion

		#endregion

	}//End Class
}//End Namespace
