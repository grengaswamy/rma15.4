﻿
using System;
using System.Text;
using System.Data; 
using System.Xml;
//abisht BRS Issue
using System.IO;
using Riskmaster.Security;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;

using System.Collections;

using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.Application.BRS
{
    /// <summary>
    /// A public class available to outside world. It exposes various methods for performing
    /// various functions related to Bill Review System. 
    /// </summary>
	public class Manager:IDisposable
	{
		#region Constants declaration
		
		/// <summary>
		/// KeepOnNext value for BRS User preference
		/// </summary>
		private const string KEEPONNEXT = "KeepOnNext";

		#endregion

		#region Variable Declarations	

		/// <summary>
		/// Private variable to store the instance of Common class 
		/// </summary>
		private Common m_objCommon ;

		/// <summary>
		/// Private variable to store the instance of Request object
		/// </summary>
		private Request m_objRequest  ;

		/// <summary>
		/// Private variable to store the instance of Bill-Item object
		/// </summary>
		private BillItem m_objBillItem  ;

      

		/// <summary>
		/// Private variable to store the instance of Funds object
		/// </summary>
		private Funds m_objFunds ;
      
		/// <summary>
		/// Private variable to store the instance of ProviderContracts object
		/// </summary>
		private ProviderContracts m_objProviderContracts ;
        
		private DataModelFactory m_objDataModelFact;

        /// <summary>
        /// DataModelFactor object
        /// </summary>
		public DataModelFactory DataModelFactory
		{
			get
			{
				return m_objDataModelFact;
			}
			set
			{
				m_objDataModelFact=value;

			}
		}

        /// <summary>
        /// Provider Contract for the payee
        /// </summary>
		public ProviderContracts ProviderContracts
		{
			get
			{
				return m_objProviderContracts;
			}
			set
			{
				m_objProviderContracts=value;

			}
		}
		private bool m_bInvokedThroughFunds=false;
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;
        //public string m_sUniqueId = null;
        
        //private Hashtable m_objHashtable = null;

        /// <summary>
        /// BusinessAdaptorErrors object property
        /// </summary>
		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}

        private int m_iClientId = 0; //rkaur27 

        //UserLogin m_oUserLogin = null;//vkumar258 ML Changes
        UserLogin UserLogin
        {
            get
            {
                return this.m_objFunds.Context.RMUser;
            }
        }
		#endregion
		
		#region Constructor
		/// Name		: Manager
		/// Author		: Navneet Sota
		/// Date Created: 1/12/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_sDsnName">DSN Name</param>
		/// <param name="p_sUserName">User Name for DSN</param>
		/// <param name="p_sPassword">Password for DSN</param>
		public Manager(string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)//rkaur27
		{
			m_objCommon = new Common(m_iClientId);

			//Initialize the global Variables
			if(m_objCommon.g_objXMLDoc == null)
				m_objCommon.g_objXMLDoc = new XmlDocument();
            
			m_objCommon.g_sDsnName = p_sDsnName;
			m_objCommon.g_sUserName = p_sUserName;
			m_objCommon.g_sPassword = p_sPassword;
            m_iClientId = p_iClientId;//rkaur27
			this.Initialize();
			m_objFunds = (Funds)m_objCommon.g_objDataModelFactory.GetDataModelObject("Funds",false);
		}

        /// <summary>
        /// Added for integration with Funds, to have the same datamodel factory between funds and BRS modules
        /// else will have problems keeping track of it seperately for Funds and BRS.
        /// Ref datamodel object will be passed from Funds app layer and hence will be shared. 
        /// </summary>
        /// <param name="p_sDsnName"></param>
        /// <param name="p_sUserName"></param>
        /// <param name="p_sPassword"></param>
        /// <param name="p_objDataModel"></param>
        public Manager(string p_sDsnName, string p_sUserName, string p_sPassword, ref DataModelFactory p_objDataModel, int p_iClientId)//rkaur27
		{
            
			if(p_objDataModel!=null)
			{
				m_bInvokedThroughFunds = true;
			}
			m_objCommon = new Common(m_iClientId);
			//Initialize the global Variables
			if(m_objCommon.g_objXMLDoc == null)
				m_objCommon.g_objXMLDoc = new XmlDocument();
            
			m_objCommon.g_sDsnName = p_sDsnName;
			m_objCommon.g_sUserName = p_sUserName;
			m_objCommon.g_sPassword = p_sPassword;

			m_objCommon.g_objDataModelFactory=p_objDataModel;
	    	m_objCommon.g_sConnectionString = m_objCommon.g_objDataModelFactory.Context.DbConn.ConnectionString;
            m_iClientId = p_iClientId;//rkaur27
			m_objFunds = (Funds)m_objCommon.g_objDataModelFactory.GetDataModelObject("Funds",false);
		}

		/// Name		: Manager
		/// Author		: Navneet Sota
		/// Date Created: 3/9/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Overloaded Constructor for the class
		/// </summary>
		/// <param name="p_objUserLogin">User Login Object</param>
        public Manager(UserLogin p_objUserLogin, int p_iClientId)//rkaur27
		{
			m_objCommon = new Common(p_iClientId);
			//Initialize the global Variables
			if(m_objCommon.g_objXMLDoc == null)
				m_objCommon.g_objXMLDoc = new XmlDocument();
            
			m_objCommon.g_sDsnName = p_objUserLogin.objRiskmasterDatabase.DataSourceName;
			m_objCommon.g_sUserName = p_objUserLogin.LoginName;
			m_objCommon.g_sPassword = p_objUserLogin.Password;
            m_iClientId = p_iClientId;//rkaur27
			this.Initialize();

			m_objFunds = (Funds)m_objCommon.g_objDataModelFactory.GetDataModelObject("Funds",false);
            //m_oUserLogin = p_objUserLogin;
		}

        /// <summary>
        /// property for Common object
        /// </summary>
		public Common Common
		{
			get
			{
              return m_objCommon;
			}
			set
			{
               m_objCommon=value;
			}
		}
		#endregion

		#region Public Methods
        /// <summary>
        /// Put value from XML document to BillItem object
        /// </summary>
        /// <param name="p_objXMLDoc"></param>
		public void FillBRSItem(ref XmlDocument p_objXMLDoc)
		{
			BillItem objItem=null;
			FillBRSItem(ref p_objXMLDoc,ref objItem);
		}
		#region public FillBRSItem (ref p_objXMLDoc)
		/// Name		: FillBRSItem
		/// Author		: Navneet Sota
		/// Date Created: 01/19/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Populates the global Bill-Item object and fills the passed in XmlDoc also.
		/// </summary>
		/// <param name="p_objXMLDoc">The XML Document for the BRS Form</param>
        /// <param name="p_objItem"></param>
		public void FillBRSItem(ref XmlDocument p_objXMLDoc,ref BillItem p_objItem)
		{
			//Provider Contract Override class object
			ProviderContractOverride objProviderContractOverride = null;
           
			//Xml variable
			XmlElement objErrors;

			//int variables
			int iCalcType = 0;
			int iRedType = 0;
           
			//bool variable
			bool bContractExists = false;

			string sLOBCode = "";
            //Mona: R8.2 : BRS in Carrier claim
            bool bSuccess = false;
			try
			{
				if(p_objXMLDoc != null)
				{
					objErrors = (XmlElement)p_objXMLDoc.CreateElement(Common.ERR_ERRORS);
					//Assign the passed in XML to Global variable so that it is available to 
					//whole component
					m_objCommon.g_objXMLDoc = p_objXMLDoc;
                    m_objBillItem=p_objItem;
					if(m_objBillItem == null)
						m_objBillItem = new BillItem(m_objCommon, m_iClientId);

					m_objBillItem.PayeeId = m_objCommon.Val(m_objCommon.GetValueOf("providereid", p_objXMLDoc));
					m_objBillItem.DiagnosisCodes.ClearAll();

					sLOBCode = m_objCommon.Val(m_objCommon.GetValueOf("lineofbusinesscode", p_objXMLDoc)).ToString();
					if(Conversion.IsNumeric(sLOBCode) && Int32.Parse(sLOBCode) != 0) 
						m_objBillItem.LineOfBusinessCode = Int32.Parse(sLOBCode);

					foreach(string sCodeId in p_objXMLDoc.SelectSingleNode("//diagnosislist").Attributes["codeid"].Value.ToString().Split(' '))
						if(Conversion.IsNumeric(sCodeId) && Int32.Parse(sCodeId) != 0) 
							m_objBillItem.DiagnosisCodes.Add(Conversion.IsNumeric(sCodeId) ? Int32.Parse(sCodeId): 0);
                //MITS 32423 : praveen ICD10 Changes
                    m_objBillItem.DiagnosisCodesicd10.ClearAll();
                    foreach(string sCodeId in p_objXMLDoc.SelectSingleNode("//diagnosislisticd10").Attributes["codeid"].Value.ToString().Split(' '))
						if(Conversion.IsNumeric(sCodeId) && Int32.Parse(sCodeId) != 0) 
							m_objBillItem.DiagnosisCodesicd10.Add(Conversion.IsNumeric(sCodeId) ? Int32.Parse(sCodeId): 0);
                //MITS 32423 : praveen ICD10 Changes
					m_objBillItem.EOBCodes.ClearAll();

					foreach(string sCodeId in p_objXMLDoc.SelectSingleNode("//eoblist").Attributes["codeid"].Value.ToString().Split(' '))
						if(Conversion.IsNumeric(sCodeId) && Int32.Parse(sCodeId) != 0) 
							m_objBillItem.EOBCodes.Add(Conversion.IsNumeric(sCodeId) ? Int32.Parse(sCodeId): 0);

					m_objBillItem.ModValues.Clear();

					foreach(string sCodeId in p_objXMLDoc.SelectSingleNode("//modifierlist").Attributes["codeid"].Value.ToString().Split(' '))
						if(Conversion.IsNumeric(sCodeId) && Int32.Parse(sCodeId) != 0) 
							m_objBillItem.ModValues.Add(Conversion.IsNumeric(sCodeId) ? Int32.Parse(sCodeId): 0,"");

                    foreach (string sCodeId in p_objXMLDoc.SelectSingleNode("//modifierlist").Attributes["codeid"].Value.ToString().Split(' '))
                    {
                        if (Conversion.IsNumeric(sCodeId) && Int32.Parse(sCodeId) != 0)
                        {
                            m_objBillItem.ModifierCode = Conversion.ConvertObjToInt(sCodeId, m_iClientId);
                            break;
                        }
                    }

                    m_objBillItem.FeeTable = m_objCommon.GetValueOf("feesched", p_objXMLDoc);
					m_objBillItem.ZipCode = m_objCommon.GetValueOf("zip", p_objXMLDoc);
                    m_objBillItem.FeeTableState = m_objCommon.GetValueOf("statecode", p_objXMLDoc);

					//The first entity fee schedule may be user selectable even when a contract is in play.
					m_objBillItem.EntFeeSchdId = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("feesched", p_objXMLDoc));
 
					iCalcType = Conversion.ConvertStrToInteger(m_objCommon.GetSingleString("CALC_TYPE", "FEE_TABLES", "TABLE_ID=" + m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXMLDoc))));
					if(iCalcType == Common.FREEPHARM || iCalcType == Common.FREEGENERIC || 
						iCalcType == Common.FREEHOSP ||iCalcType == Common.FLHOSP)
					{
                        m_objBillItem.CPT = m_objCommon.GetValueOf("billingcode_cid", p_objXMLDoc) + " - " + p_objXMLDoc.SelectSingleNode("//billingcode").InnerText;
                        if (m_objBillItem.CPT.Trim().Equals(" - "))
                        {
                            m_objBillItem.CPT = "";
                        }
                        //m_objBillItem.CPT = m_objCommon.GetValueOf("billingcode", p_objXMLDoc);
					}
					else
					{
                        m_objBillItem.CPT = m_objCommon.GetValueOf("billingcode", p_objXMLDoc);
					}
					m_objBillItem.TOS = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("typeofservice", p_objXMLDoc));
					m_objBillItem.POS = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("placeofservice", p_objXMLDoc));
					m_objBillItem.TrnType = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("TransTypeCode", p_objXMLDoc));
					m_objBillItem.UnitsBilled = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("unitsbillednum", p_objXMLDoc));
					m_objBillItem.UnitsBilledType = m_objCommon.Val(m_objCommon.GetValueOf("unitsbilledtype", p_objXMLDoc));
                    //Mona: R8.2 : BRS in Carrier claim
                    
                   // m_objBillItem.CoverageId = Conversion.CastToType<int>(m_objCommon.GetValueOf("CoverageId", p_objXMLDoc), out bSuccess);
                    m_objBillItem.IsFirstFinal = Conversion.CastToType<bool>(m_objCommon.GetValueOf("IsFirstFinal", p_objXMLDoc), out bSuccess);                

                    //Mona: R8.2 : BRS in Carrier claim

					m_objBillItem.AmountBilled = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amountbilled", p_objXMLDoc));
					m_objBillItem.AmountAllowed = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amountallowed", p_objXMLDoc));
					//Mukul Added 4/10/07 MITS 9160
					m_objBillItem.StartDate = Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXMLDoc));//vkumar258 ML Changes
					m_objBillItem.EndDate = Conversion.GetDate(m_objCommon.GetValueOf("ToDate", p_objXMLDoc));//vkumar258 ML Changes

                    //Kiran Added 02/26/2013 MITS 31444 Start
                    Double AmountReduced = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amountreduced", p_objXMLDoc));
                    Double BilledAmount = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amountbilled", p_objXMLDoc));
                    Double AllowedAmount = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amountallowed", p_objXMLDoc));
                    Double TotalAmount = Math.Min(BilledAmount, AllowedAmount);
                    //caggarwal4 Merged Issue RMA-9115 Starts
                    Double percentile = 0;
                    if (TotalAmount != 0)
                    {
                        percentile = (AmountReduced * 100) / TotalAmount;
                    }
                    //caggarwal4 ends
					//m_objBillItem.Percentile = m_objCommon.GetValueOf("percentile", p_objXMLDoc);
                    m_objBillItem.Percentile = percentile.ToString();
                    // Kiran End

					m_objBillItem.InvoiceNumber = m_objCommon.GetValueOf("invoicenumber", p_objXMLDoc);
					m_objBillItem.InvoicedBy = m_objCommon.GetValueOf("invoicedby", p_objXMLDoc);
					m_objBillItem.InvoiceDate = m_objCommon.GetValueOf("invoicedate", p_objXMLDoc);
					m_objBillItem.GLAccount = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("glaccountcode", p_objXMLDoc));

                    //BRS FL Merge : Umesh
                    m_objBillItem.BillType = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("billtype", p_objXMLDoc));
                    m_objBillItem.PhyPharmNDC = m_objCommon.GetValueOf("phyPharmNDC_cid", p_objXMLDoc);
                    m_objBillItem.PhyPharmNDCDes = m_objCommon.GetValueOf("phyPharmNDC_Des", p_objXMLDoc);
                    
                    m_objBillItem.RevenueCode = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("RevenueCode", p_objXMLDoc));
                    //srajindersin MITS 29012 dt-09/26/2012
                    m_objBillItem.DenyLineItem = Conversion.ConvertStrToBool(m_objCommon.GetValueOf("DenyLineItem", p_objXMLDoc));
                    m_objBillItem.PhyEid = Conversion.ConvertStrToInteger(p_objXMLDoc.SelectSingleNode("//PhysicianLookUp/@codeid ").InnerText);
                    //MITS 11338 : Umesh 
                    //MITS 23814 :skhare7
                    if (p_objXMLDoc.SelectSingleNode("//PhysicianLookUp/@PhyLookupchanged ") != null)
                    {
                        if (p_objXMLDoc.SelectSingleNode("//PhysicianLookUp/@PhyLookupchanged ").InnerText == "Y")
                        {
                            m_objBillItem.FLLicense = GetPhysicianLicenseNum(m_objBillItem.PhyEid);
                      
                        }
                        else
                            m_objBillItem.FLLicense = m_objCommon.GetValueOf("FLLicense", p_objXMLDoc);
                        p_objXMLDoc.SelectSingleNode("//PhysicianLookUp/@PhyLookupchanged ").InnerText = "N";
                    }
                    else
                     m_objBillItem.FLLicense=   m_objCommon.GetValueOf("FLLicense", p_objXMLDoc);
                    //MITS 23814 :skhare7 End
                  //  m_objBillItem.FLLicense = GetPhysicianLicenseNum(m_objBillItem.PhyEid);
                   // if (m_objBillItem.FLLicense.Trim() == "")
                   //m_objBillItem.FLLicense = m_objCommon.GetValueOf("FLLicense", p_objXMLDoc);
                        

                    //skhare7:MIS 23814 End
                    //MITS 11338 End
                    m_objBillItem.DiagRefNo = m_objCommon.GetValueOf("RefNo", p_objXMLDoc);
                    //BRS FL Merge End

                    m_objBillItem.SplitRowId = Conversion.ConvertStrToInteger(p_objXMLDoc.SelectSingleNode("//SplitRowId").InnerText);

					if(Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("contractexists", p_objXMLDoc)) != 0)
						m_objBillItem.ContractExists = true;
					else
						m_objBillItem.ContractExists = false;

					if(m_objBillItem.ContractExists)
					{
						bContractExists = FindContract(m_objBillItem.PayeeId, m_objBillItem.StartDate);
						if(bContractExists)
						{
                            m_objBillItem.SchdAmount = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("ovrschedamount", p_objXMLDoc));
							m_objBillItem.ContractAmount = m_objProviderContracts.Amount; 
							m_objBillItem.ContractDesc = m_objProviderContracts.Comments;
							m_objBillItem.ContractBegin = m_objProviderContracts.StartDate;
							m_objBillItem.ContractEnd = m_objProviderContracts.EndDate;
                            //m_objBillItem.Discount = Conversion.ConvertStrToDouble(Conversion.ConvertBoolToInt(m_objProviderContracts.DiscOnSchdFlag).ToString());
                            //m_objBillItem.Discount2nd = Conversion.ConvertStrToDouble(Conversion.ConvertBoolToInt(m_objProviderContracts.Disc2ndSchdFlag).ToString());
                            //m_objBillItem.DiscountBill = Conversion.ConvertStrToDouble(Conversion.ConvertBoolToInt(m_objProviderContracts.DiscOnBillFlag).ToString());
                            //MITS 17229 : Umesh
                            m_objBillItem.Discount = m_objProviderContracts.Discount;
                            m_objBillItem.Discount2nd = m_objProviderContracts.Discount2nd;
                            m_objBillItem.DiscountBill = m_objProviderContracts.DiscountBill;
							m_objBillItem.FirstRate = m_objProviderContracts.FirstRate;
							m_objBillItem.SecondRate = m_objProviderContracts.SecondRate;
							m_objBillItem.LastRate = m_objProviderContracts.LastRate;
							m_objBillItem.RateChange = m_objProviderContracts.RateChg;
							m_objBillItem.SecondRateChg = m_objProviderContracts.RateChg2nd;
						}//end if(bContractExists)
                    }//end if(m_objBillItem.ContractExists)
                    else
                    {
                        m_objBillItem.SchdAmount = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("sched_amt", p_objXMLDoc));
                    }
                    m_objBillItem.EntFeeSchdUsed = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("feeschedused", p_objXMLDoc));

					//The second entity fee schedule is always explicitly set by the contract.
					m_objBillItem.EntFeeSchdId2 = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("feesched2", p_objXMLDoc));
                    //m_objBillItem.PerDiemAmount = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("ovr_perdiemamount", p_objXMLDoc));
                    //m_objBillItem.StopLossAmt = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("ovr_perdiemstoplossamount", p_objXMLDoc));
                    //m_objBillItem.StopLossFlag = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("stoplossflag", p_objXMLDoc));
                    //MITS 17229 : Umesh
                    m_objBillItem.PerDiemAmount = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("overrideperdiemamount", p_objXMLDoc));
                    m_objBillItem.StopLossAmt = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("ovrperdiemstoplossamount", p_objXMLDoc));
                    m_objBillItem.StopLossFlag = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("stoplossflag", p_objXMLDoc));

					m_objBillItem.OverrideType = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("overrideoption", p_objXMLDoc));
					m_objBillItem.AmountToPay = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amounttopay", p_objXMLDoc));
					m_objBillItem.RedAmount = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amountreduced", p_objXMLDoc));
					m_objBillItem.RedType = iRedType;
					m_objBillItem.AmountSaved = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("amountsaved", p_objXMLDoc));
					m_objBillItem.FollowUpMessage = "";//m_objCommon.GetValueOf("folluplabel", p_objXMLDoc);

                    m_objBillItem.ToothNo = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(m_objCommon.GetValueOf("toothno", p_objXMLDoc)), m_iClientId);
					m_objBillItem.SurfaceText = m_objCommon.GetValueOf("surfacetext", p_objXMLDoc);
					m_objBillItem.PrescriptionNo = m_objCommon.GetValueOf("prescripno", p_objXMLDoc);
					m_objBillItem.DrugName = m_objCommon.GetValueOf("drugname", p_objXMLDoc);
                    m_objBillItem.PrescriptionDate = Conversion.GetUIDate(m_objCommon.GetValueOf("prescripdate", p_objXMLDoc),UserLogin.objUser.NlsCode.ToString(),m_iClientId);//vkumar258 ML Changes
                    m_objBillItem.PrescriptionCertification = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("prescripcert", p_objXMLDoc));
                    m_objBillItem.MedQuantity = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("medquantity", p_objXMLDoc));
                    m_objBillItem.MedDaysSupply = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("meddaysupply", p_objXMLDoc));
                    m_objBillItem.PrescriptionIndicator = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("prescripind", p_objXMLDoc));
                    m_objBillItem.PurchasedIndicator = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("purchasedind", p_objXMLDoc));
                    m_objBillItem.SupplyChargeFlag = Conversion.ConvertStrToBool(m_objCommon.GetValueOf("supplychargeflag", p_objXMLDoc));
                    m_objBillItem.UsualCharge = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("usualcharge", p_objXMLDoc));
                    m_objBillItem.Dispensed = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("dispensed", p_objXMLDoc));
                    m_objBillItem.HcpcsCode = m_objCommon.GetValueOf("hcpcscode", p_objXMLDoc);

					//Fee Table Type
					m_objBillItem.FeeDataSource = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("calctype", p_objXMLDoc));              
                    m_objBillItem.OverrideType = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("overrideoption", p_objXMLDoc));

					if(m_objBillItem.OverrideType != (int)Common.BillOverrideType.APPLY_UNKNOWN 
						&& m_objBillItem.OverrideType != (int)Common.BillOverrideType.DO_NOT_OVERRIDE)
					{
						m_objBillItem.AmountAllowed = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("overrideamount", p_objXMLDoc));
                        //MITS 17229 : Umesh
                        m_objBillItem.EntFeeSchdAmt = Conversion.ConvertStrToDouble(m_objCommon.GetValueOf("ovrschedamount", p_objXMLDoc));
						//Try to make a new override object to match the original
						if(m_objRequest != null)
						{
							objProviderContractOverride = new ProviderContractOverride(m_objCommon, m_iClientId);
							//Set Minimum required fields of the Override object.
							objProviderContractOverride.m_dBilledAmt = m_objBillItem.AmountBilled;
							objProviderContractOverride.m_dOverrideAmount = m_objBillItem.AmountAllowed;
							objProviderContractOverride.OverrideOption = (Common.BillOverrideType)m_objBillItem.OverrideType;
                            //MITS 17229 : Umesh
                            objProviderContractOverride.m_dPerDiemAmt = m_objBillItem.PerDiemAmount;
                            objProviderContractOverride.m_dPerDiemAmtWithStopLoss = m_objBillItem.StopLossAmt;
                            objProviderContractOverride.m_dSchedAmt = m_objBillItem.EntFeeSchdAmt;
							m_objRequest.AddParam(objProviderContractOverride, "OVERRIDE");
						}//end if(m_objRequest != null)
					}//end if

                    //values related to specialty codes
                    m_objBillItem.SpecialtyCode = m_objCommon.GetValueOf("Specialty", p_objXMLDoc);
                    m_objBillItem.SpecialtyCodeFactor = m_objCommon.GetValueOf("CmboSpecCdsAmount", p_objXMLDoc);

                    //For Free Entry Pharmacy, set NDC value for billing code which is required in
                    //EDI extract
                    if (m_objBillItem.FeeDataSource == Common.FREEPHARM)
                    {
                        m_objBillItem.CPT = string.Format("{0} - {1}", m_objBillItem.PhyPharmNDC, m_objBillItem.PhyPharmNDCDes);
                    }

					if(objErrors.HasChildNodes)
                        p_objXMLDoc.SelectSingleNode("//option").InsertBefore(objErrors, p_objXMLDoc.SelectSingleNode("//option").FirstChild);
				}//end if(p_objXMLDoc != null)
			}//end main try
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.FillBRSItem.GenericError", m_iClientId), p_objException);
			}
			finally
			{
				objProviderContractOverride = null;
				objErrors = null;
			}
		}// end FillBRSItem
       
		#endregion
		public void  FillBRSDOM(ref XmlDocument p_objXMLDoc)
		{
			BillItem objBill=null;
			FillBRSDOM(ref p_objXMLDoc,ref objBill);
		}
		#region public FillBRSDOM (ref p_objXMLDoc)

		/// Name		: FillBRSDOM
		/// Author		: Navneet Sota
		/// Date Created: 1/19/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Fill BRS XML From Database Values unless objItem is specified - in which case, use it.
		/// </summary>
		/// <param name="p_objXMLDoc">The XML string for the BRS Form</param>
        /// <param name="p_objBillItem">BillItem object</param>
		public void FillBRSDOM(ref XmlDocument p_objXMLDoc, ref BillItem p_objBillItem)
		{
			//int Variables
			int iTransId =0;
			int iSplitRowId = 0;
			int iProviderEid = 0;
			int iFeeSchedule = 0;
			int iFeeSchedule2 = 0;
			//determined by lFeeSchedule1 for purposes of free entry of billing code and showing 
			//dental, pharmacy, and hospital fields
			int iCalcType = 0; 
			int iTemp = 0;

			//Objects Variables
			FundsTransSplit objTransSplit = null;
			FeeSchedule objFeeSchedule = null;
			ProviderContractOverride objProviderContractOverride = null;
			Riskmaster.Settings.CCacheFunctions objCacheSettings = null;

			//XML Variables
			XmlElement objErrorsEle;
			XmlElement objAlertsEle;
			XmlElement objXMLFieldEle;
			XmlElement objTempXMLFieldEle;
			XmlNode objXMLNode;
			XmlNodeList objNodeList;
			XmlAttribute oAttr = null;

			//string Variables
			string sFieldName = "";
			string sTemp = "";
			string sTemp2 = "";
            string sResubmitMode = "";     //BRS FL Merge

			//boolean Variables
		    bool bContractExists = false;
			bool bIsSavedSplit = false;
			bool bUseItem = false;
			bool bIsReadonly = false;
            //pmahli BRS 11/7/2007  - Start
            // falg to capture value of resubmit button
            bool bResubmitReadonly = false;
            //pmahli 11/7/2007 - End

			try
			{
                if (p_objXMLDoc == null)
                {
                    return;
                }

				if(p_objBillItem!=null)
					m_objBillItem = p_objBillItem;

				if(m_objRequest	!= null)
				{
					if(m_objRequest.GetParam("OVERRIDE")!= null)
						objProviderContractOverride = (ProviderContractOverride)m_objRequest.GetParam("OVERRIDE");
				}

                //Need to get values for provider
                if (objProviderContractOverride == null)
                {
                    if (m_objBillItem.OverrideType != (int)Common.BillOverrideType.APPLY_UNKNOWN
                            && m_objBillItem.OverrideType != (int)Common.BillOverrideType.DO_NOT_OVERRIDE)
                    {
                        objProviderContractOverride = new ProviderContractOverride(m_objCommon, m_iClientId);
                        objProviderContractOverride.m_dBilledAmt = m_objBillItem.AmountBilled;
                        objProviderContractOverride.m_dOverrideAmount = m_objBillItem.AmountAllowed;
                        objProviderContractOverride.OverrideOption = (Common.BillOverrideType)m_objBillItem.OverrideType;
                        objProviderContractOverride.m_dPerDiemAmt = m_objBillItem.PerDiemAmount;
                        objProviderContractOverride.m_dPerDiemAmtWithStopLoss = m_objBillItem.StopLossAmt;
                        objProviderContractOverride.m_dSchedAmt = m_objBillItem.SchdAmount;
                    }
                }

				objErrorsEle = p_objXMLDoc.CreateElement(Common.ERR_ERRORS);
				objAlertsEle = p_objXMLDoc.CreateElement(Common.ALERTS);

				//Assign the passed in XML to Global variable
				m_objCommon.g_objXMLDoc = p_objXMLDoc;

				//Fetch Parameters from XML
				iTransId = m_objCommon.Val(m_objCommon.GetValueOf("transid", p_objXMLDoc));
				iSplitRowId = m_objCommon.Val(m_objCommon.GetValueOf("SplitRowId", p_objXMLDoc));
				iProviderEid = m_objCommon.Val(m_objCommon.GetValueOf("providereid", p_objXMLDoc));
               
                //Get fee table list
                GetFeeSchedules(ref p_objXMLDoc, iProviderEid,
                                    Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXMLDoc)),
                                    Conversion.GetDate(m_objCommon.GetValueOf("ToDate", p_objXMLDoc)),
                                    iTransId, iSplitRowId);//vkumar258 ML Changes

                //if lFeeSchedule2 > 0 this means a contract must exist
                iFeeSchedule = m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXMLDoc));
                iFeeSchedule2 = m_objCommon.Val(m_objCommon.GetValueOf("feesched2", p_objXMLDoc));


                if (iSplitRowId > 0)
					bIsSavedSplit = true;

				if(m_objBillItem != null) //Get Data from existing BillItem
				{
					objTransSplit = m_objBillItem.objSplit;
					bUseItem = true;				
				}//end if(m_objBillItem != null)
				else if(bIsSavedSplit) //Get Data from DB Trans object.
				{
                    objTransSplit = m_objFunds.TransSplitList[iSplitRowId];
					m_objBillItem = new BillItem(m_objCommon, m_iClientId);
					m_objBillItem.objSplit = objTransSplit;
				}//if(bIsSavedSplit)
				else //Get Data from an Empty DB Trans object.
				{
                    objTransSplit = m_objFunds.TransSplitList.Add(iSplitRowId);
					m_objBillItem = new BillItem(m_objCommon, m_iClientId);
					m_objBillItem.objSplit = objTransSplit;
				}//end if(m_objBillItem == null)

				//There is a group of data that are "write once,  read-only" even during an edit.
				//This includes the date range and the fee schedule selection.
				//To respect this, we will only over-write these values when:
				//1.) The Dom is being filled by a bill-item.
				//    (This BillItem will have these correct values from its creation\population.)
				//OR
				//2.) The XML Does not contain a current value.
				if(m_objCommon.GetValueOf("FromDate", p_objXMLDoc) == "" || bUseItem)
				{
                    m_objCommon.SetValueOf("FromDate", (m_objBillItem.StartDate == "" ? "" : Conversion.GetUIDate(m_objBillItem.StartDate, UserLogin.objUser.NlsCode.ToString(), m_iClientId)), ref p_objXMLDoc);//vkumar258 ML Changes
                    m_objCommon.SetValueOf("ToDate", (m_objBillItem.StartDate == "" ? "" : Conversion.GetUIDate(m_objBillItem.EndDate, UserLogin.objUser.NlsCode.ToString(), m_iClientId)), ref p_objXMLDoc);//vkumar258 ML Changes
				}

				//Contract members of any stored Brsinvoicedetail are ignored.
				//fromdate/todate could have changed; this makes historic contract information irrelevent
				//Therefore, we find out if a new contract is applicable and stuff that boolean value
				//(rather than the original) back into the xml for the rest of this processing.
				if(m_objProviderContracts==null)
				m_objProviderContracts=(ProviderContracts)m_objCommon.g_objDataModelFactory.GetDataModelObject("ProviderContracts",false);
				if (!bUseItem)
				{
					bContractExists = FindContract(iProviderEid, Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXMLDoc)));//vkumar258 ML Changes
					objXMLFieldEle = (XmlElement)p_objXMLDoc.SelectSingleNode("//contractexists");
                    m_objCommon.SetValue(ref objXMLFieldEle, Conversion.ConvertBoolToInt(bContractExists, m_iClientId));
				}
				else
				{
					bContractExists = m_objBillItem.ContractExists;
                    if (!bContractExists)
                    {
                        bContractExists = FindContract(iProviderEid, Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXMLDoc)));//vkumar258 ML Changes
                        m_objBillItem.ContractExists = bContractExists;
                    }
					if(!bContractExists)
						m_objProviderContracts = null;
				}//if (!bUseItem)

				//check for untenable situations:
				//if iFeeSchedule2 must be a contract
				if(iFeeSchedule2 > 0 && !bContractExists)
				{
					sTemp = "Error, 2nd fee schedule = " + iFeeSchedule2;
					sTemp =  sTemp + " but no contract found for provider id=" + iProviderEid + ", from date="; 
					sTemp =  sTemp + Conversion.GetUIDate(m_objCommon.GetValueOf("FromDate", p_objXMLDoc),UserLogin.objUser.NlsCode.ToString(),m_iClientId) + ", to date=" ;
					sTemp =  sTemp + Conversion.GetUIDate(m_objCommon.GetValueOf("ToDate", p_objXMLDoc),UserLogin.objUser.NlsCode.ToString(),m_iClientId);//vkumar258 ML Changes

					throw new RMAppException(sTemp); 
				}

				//Get a FeeSchedule object for the feeschedule1 desc,calc type,percentile fields
				//calc type is determined by feeschedule1.
				//calc type determines free entry of billingcode and appearance/removal of dental/hospital/pharmacy fields
				//calc type determines appearance/removal of percentile combo box
				objFeeSchedule = new FeeSchedule(m_objCommon, m_iClientId);
				if(iFeeSchedule > 0)
				{
					objFeeSchedule.MoveToRecord(iFeeSchedule);
					iCalcType = (int)objFeeSchedule.ScheduleType;
				}
				else
				{
					iCalcType = -1;
					if(objTransSplit!=null)
					{
						if(objTransSplit.BrsInvoiceDetail.TableCode > 0)
						{
							iFeeSchedule = objTransSplit.BrsInvoiceDetail.TableCode;
							objFeeSchedule.MoveToRecord(iFeeSchedule);
							iCalcType = (int)objFeeSchedule.ScheduleType;
						}
						else
						{
                           iCalcType = -1;
						}
					}
					else
					{
						iCalcType = -1;
					}
				}

				//Fill percentile combo box based on calctype
				switch (iCalcType)
				{
					case Common.UCR94:
					case Common.UCR95:
					case Common.OPT:
					case Common.HCPCS:
					case Common.DENT:
						objXMLFieldEle = (XmlElement)p_objXMLDoc.SelectSingleNode("//percentile");
                        if (iSplitRowId == 0) //new record, use default percentile
							GetPercentiles(ref objXMLFieldEle, objFeeSchedule.UCRPercentile, objFeeSchedule.DefaultPercentile);
						else //editing a record, the value will be set below
							GetPercentiles(ref objXMLFieldEle, objFeeSchedule.UCRPercentile, 0);
						break;
				}//end switch

				//change modifierlist to code vs. codelist based on calctype
				//in brsworld a combination combobox/listbox only looks at mod 
				//code in combo box when doing calc.
				//if(iCalcType == (int)Common.UCR94 || iCalcType == (int)Common.UCR95 
				//	|| iCalcType == (int)Common.ANES)
				//{
					//objXMLFieldEle = (XmlElement)p_objXMLDoc.SelectSingleNode("//modifierlist");
					//objXMLFieldEle.SetAttribute("type", "code");
				//}

                //capturing value of resubmit button 
                if (p_objXMLDoc.DocumentElement.Attributes["resubmitmode"] != null)
                {
                    sResubmitMode = p_objXMLDoc.DocumentElement.Attributes["resubmitmode"].Value;
                    if (sResubmitMode == "true")
                    {
                        bResubmitReadonly = true;
                    }
                }

                if (p_objXMLDoc.DocumentElement.Attributes["brsmode"] != null)
                {
                    if (p_objXMLDoc.DocumentElement.Attributes["brsmode"].Value == "view")
                    {
                        bIsReadonly = true;
                        if (sResubmitMode != "true")
                            m_objCommon.RemoveNamedNode("btnReturn");
                    }
                }

                objNodeList = p_objXMLDoc.SelectSingleNode("//option").ChildNodes;
				for(int iCnt = 0; iCnt <= objNodeList.Count - 1; iCnt++)
				{
					objTempXMLFieldEle = (XmlElement)objNodeList.Item(iCnt);

                    sFieldName = objTempXMLFieldEle.Name;
					switch (sFieldName)
					{
						case "contractexists":
                            m_objCommon.SetValue(ref objTempXMLFieldEle, Conversion.ConvertBoolToInt(bContractExists, m_iClientId));
							break;
						case "feescheddesc":
							//if editing an existing split user is either using the feeschedule 
							//from the existing split OR chose a different one OR contract exists and was readonly
							//if new user chose a fee schedule
							objTempXMLFieldEle.InnerText = objFeeSchedule.Name;
							if(bContractExists)
								objTempXMLFieldEle.SetAttribute("title", "Contract Table 1");
							break;
						case "feescheddesc2":
							//only appears if contract exists AND user checked "Other"
							if(iFeeSchedule2 > 0)
								objTempXMLFieldEle.InnerText = m_objCommon.GetSingleString("USER_TABLE_NAME", "FEE_TABLES", "TABLE_ID = " + iFeeSchedule2);
							break;
						case "zip":
							if(!bIsSavedSplit)
							{
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTempXMLFieldEle.InnerText);
								//SetReadonly(ref objTempXMLFieldEle, bIsReadonly);
							}
							break;
						case "calctype":
							m_objCommon.SetValue(ref objTempXMLFieldEle, iCalcType);
							break;
						case "unitsbilledtype":
							if(!bIsSavedSplit && m_objCommon.Val(objTempXMLFieldEle.InnerText) == 1)
								m_objCommon.SetValue(ref objTempXMLFieldEle, m_objCommon.Val(objTempXMLFieldEle.InnerText).ToString());
							
							//Remove non-selected one
							if( bIsReadonly )
							{
								XmlNodeList objUnitsBilledList = null;
								XmlNode objUnitsBilledNode = null;
								string sUnitsBilledType = "";
								sUnitsBilledType = objTempXMLFieldEle.InnerText;
								objUnitsBilledList = p_objXMLDoc.SelectNodes("//unitsbilled");
								if( objUnitsBilledList != null )
								{
									for(int iUnitBilled=0; iUnitBilled< objUnitsBilledList.Count; iUnitBilled++)
									{
										objUnitsBilledNode = objUnitsBilledList[iUnitBilled];
										if( objUnitsBilledNode.Attributes.GetNamedItem("value").InnerText == sUnitsBilledType )
										{
											oAttr = (XmlAttribute)objUnitsBilledNode.Attributes.GetNamedItem("label");
											if( oAttr != null )
												objUnitsBilledNode.InnerText = "   " + oAttr.InnerText;
										}
									}
								}
							}
							break;
						case "unitsbilled":
							break;
						case "amounttopay":
							if(!bIsSavedSplit)
								m_objCommon.SetValue(ref objTempXMLFieldEle, 0);
							break;
						case "glaccountcode":
							FillComboWithCodes("GL_ACCOUNTS", ref objTempXMLFieldEle,true); 
							break;
						case "lineofbusinesscode":
							m_objCommon.SetValue(ref objTempXMLFieldEle, objTempXMLFieldEle.InnerText);
							break;
						case "folluplabel":
							m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.FollowUpMessage);
							break;
					}//end switch (sFieldName)

					if(bContractExists)
					{
						//show contract info read-only all
						//with all objProviderContract fields--remove the node if not applicable
						switch (sFieldName)
						{
							case "providername":
								objTempXMLFieldEle.InnerText = m_objCommon.GetSingleString("LAST_NAME", "ENTITY", "ENTITY_ID=" + iProviderEid);
								break;
							case "contr_startdate":
								sTemp = m_objProviderContracts.StartDate;
								if(sTemp != "")
									objTempXMLFieldEle.InnerText = Conversion.ToDate(sTemp).ToString();
								break;
							case "contr_enddate":
								sTemp = m_objProviderContracts.EndDate;
								if(sTemp != "")
									objTempXMLFieldEle.InnerText = Conversion.ToDate(sTemp).ToString();
								break;
							case "contr_comments":
								objTempXMLFieldEle.InnerText = m_objProviderContracts.Comments;
								break;
							case "contr_amount":
                                if (m_objProviderContracts.Amount > 0)
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:C}", m_objProviderContracts.Amount));
                                else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_firstrate":
								if(m_objProviderContracts.FirstRate > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:C}",m_objProviderContracts.FirstRate));
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_firstratechanged":
								if(m_objProviderContracts.RateChg > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, m_objProviderContracts.RateChg);
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_secondrate":
								if(m_objProviderContracts.SecondRate > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:C}",m_objProviderContracts.SecondRate));
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_secondratechanged":
								if(m_objProviderContracts.RateChg2nd > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, m_objProviderContracts.RateChg2nd);
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_lastrate":
								if(m_objProviderContracts.LastRate > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:C}",m_objProviderContracts.LastRate));
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_dpercentage":
								if(m_objProviderContracts.Discount> 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.0000}",m_objProviderContracts.Discount));
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_dpercentage2":
								if(m_objProviderContracts.Discount2nd > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.0000}",m_objProviderContracts.Discount2nd));
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
							case "contr_dpercentagebill":
								if(m_objProviderContracts.DiscountBill > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.0000}",m_objProviderContracts.DiscountBill));
								else
                                    m_objCommon.RemoveNamedNode(sFieldName);
								break;
						}//end switch
					}//end if(bContractExists)
					//else
					//for an existing BillItem or one retrieved from DB
                    if (iSplitRowId != 0 || bUseItem)
					{
						//if editing a transsplit grab the values and fill dom
						switch (sFieldName)
						{
							case "zip":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.ZipCode);
								break;
							case "placeofservice":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.PlaceOfSerCode);
								break;
							case "typeofservice":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.TypeOfSerCode);
								break;
                            case "CmboSpecCds":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.SpecialtyCodeList);
                                break;
							case "specialty_desc":
								//if iCalcType is idbBasic and the length of the cpt code is 6, 
								//then the cpt code is a specialty short code
								//cannot fill if user changed fee schedule, cpt no longer applies
								if(iCalcType == Common.BASIC && 
									objTransSplit.BrsInvoiceDetail.TableCode == iFeeSchedule && 
									objTransSplit.BrsInvoiceDetail.BillingCodeText == "6")
								{
									sTemp = objTransSplit.BrsInvoiceDetail.BillingCodeText.Trim();
									objCacheSettings = new Riskmaster.Settings.CCacheFunctions(m_objCommon.g_sConnectionString, m_iClientId);

									///TODO: Function to be added in LocalCache
									iTemp = objCacheSettings.GetCodeIDWithShort(sTemp, "SPECIALTY_CODES");
									objCacheSettings.GetCodeInfo(iTemp, ref sTemp, ref sTemp2);
									m_objCommon.SetValue(ref objTempXMLFieldEle, sTemp2);
								}
								else
									m_objCommon.SetValue(ref objTempXMLFieldEle, "");
								break;
							case "TransTypeCode":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.TransTypeCode);
								break;
							case "ReserveTypeCode":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.ReserveTypeCode);
								break;
							case "billingcode":
								if(m_objBillItem.CPT.Trim().Length > 0)
								{
									switch (iCalcType)
									{
										case Common.FREEPHARM:
										case Common.FREEHOSP:
										case Common.FREEGENERIC:
                                        case Common.FLHOSP:
											((XmlElement)p_objXMLDoc.SelectSingleNode("//billingcode_cid")).InnerText=m_objBillItem.CPT;
											break;
										default:
											((XmlElement)p_objXMLDoc.SelectSingleNode("//billingcode")).InnerText=m_objBillItem.objSplit.BrsInvoiceDetail.BillingCodeText;
											//grab the desc for the cpt code
											//sTemp = string.Format( "{0: 0000}" , m_objCommon.Val(objTransSplit.BrsInvoiceDetail.BillingCodeText).ToString());
										
											break;
									}//end switch
								}
								//cannot fill if user changed fee schedule, cpt no longer applies
								//if(m_objBillItem.CPT.Length > 0)
								//	objTempXMLFieldEle.InnerText = m_objBillItem.CPT;
								if( objTransSplit.BrsInvoiceDetail.TableCode == iFeeSchedule && iFeeSchedule!=0)
								{
                                    string sBillingCodeIdDesc = objTransSplit.BrsInvoiceDetail.BillingCodeText;
                                    string sBillingCodeId = string.Empty;
                                    string sBillingCodeDesc = string.Empty;
									switch (iCalcType)
									{
										case Common.FREEPHARM:
										case Common.FREEHOSP:
										case Common.FREEGENERIC:
                                        case Common.FLHOSP:
                                            iTemp = sBillingCodeIdDesc.IndexOf(" - ");
                                            if (iTemp >= 0)
                                            {
                                                sBillingCodeId = sBillingCodeIdDesc.Substring(0, iTemp).Trim();
                                                sBillingCodeDesc = sBillingCodeIdDesc.Substring(iTemp + 3).Trim();
                                            }
                                            ((XmlElement)p_objXMLDoc.SelectSingleNode("//billingcode_cid")).InnerText = sBillingCodeId;
                                            objXMLNode = p_objXMLDoc.SelectSingleNode("//billingcode");
                                            objXMLNode.InnerText = sBillingCodeDesc;
											break;
										default:
											//grab the desc for the cpt code
											//sTemp = string.Format( "{0: 0000}" , m_objCommon.Val(objTransSplit.BrsInvoiceDetail.BillingCodeText).ToString());
											//sTemp =  m_objCommon.Val(objTransSplit.BrsInvoiceDetail.BillingCodeText).ToString("00000");
											//Mukul Changed MITS 8667 earlier it was taking 5 characters which was actually removing specialty code
                                            if (sBillingCodeIdDesc.IndexOf(" - ") > 0)
											{
                                                sBillingCodeId = sBillingCodeIdDesc.Substring(0, sBillingCodeIdDesc.IndexOf(" - "));
                                                sBillingCodeDesc = sBillingCodeIdDesc.Substring(sBillingCodeIdDesc.IndexOf(" - ") + 3);
											}
											else
											{
                                                if (sBillingCodeIdDesc.Trim().Length >= 5)
                                                {
                                                    sBillingCodeId = sBillingCodeIdDesc.Substring(0, 5);
                                                }
                                                else
                                                {
                                                    sBillingCodeId = sBillingCodeIdDesc.Trim();
                                                }
											}

                                            //MITS 19249 Don't try to get description for existing payment because the fee schedule file may no longer
                                            //exist.
                                            if (!string.IsNullOrEmpty(sBillingCodeId) && objTransSplit.SplitRowId <= 0)
                                            {
                                                sBillingCodeDesc = objFeeSchedule.GetBillingCodeDesc(sBillingCodeId);
                                            }

                                            sBillingCodeIdDesc = sBillingCodeId + " - " + sBillingCodeDesc;    //display [cpt] - [desc]
                                            if (sBillingCodeId == "00000")
                                                sBillingCodeIdDesc = string.Empty;
                                            if (string.IsNullOrEmpty(sBillingCodeId.Trim()) && string.IsNullOrEmpty(sBillingCodeIdDesc))
											{
                                                break;
                                            }
                                            ((XmlElement)p_objXMLDoc.SelectSingleNode("//billingcode")).SetAttribute("codeid", sBillingCodeId.Trim());
											objXMLNode = p_objXMLDoc.SelectSingleNode("//billingcode");
                                            if (sBillingCodeIdDesc.Trim() == "-")
                                                sBillingCodeIdDesc = string.Empty;
                                            objXMLNode.InnerText = sBillingCodeIdDesc;
											break;
									}//end switch
									
								}//end if
								break;

                            case "billingcode_cid":
                                break;

                                //brs fl merge    : Umesh
                            case "billtype":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.BillType);
                                break;

                            case "FLLicense":
                          
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.FlLicense);

                                break;
                            case "RevenueCode":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.RevenueCode);
                                break;
                            //srajindersin MITS 29012 dt-09/26/2012
                            case "DenyLineItem":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.DenyLineItem);
                                break;
                            case "phyPharmNDC_cid":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.PhyPharmNdc);
                                break;
                            case "phyPharmNDC_Des":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.PhyPharmNdcDes);
                                break;
                            case "PhysicianLookUp":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.PhyEid);
                                break;
                                //End brs fl merge
							case "modifierlist":
								//switch (iCalcType)
								//{
								//	case Common.UCR94:
								//	case Common.UCR95:
								//	case Common.ANES:
										//only one modifier code, stored in .ModifierCode
										//changed node type to code vs. codelist above in this case
										//Tanuj-> Need to check this functionality with Brian, as no where in the library this(objTransSplit.BrsInvoiceDetail.ModifierCode) value is getting set.
										//m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.ModifierCode);
								//		m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.ModifierList);
								//		break;
								//	default:
										//a code list
										m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.ModifierList);
								//		break;
								//}//end switch
								//SetReadonly(ref objTempXMLFieldEle, bIsReadonly);
								break;
							case "diagnosislist":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.DiagnosisList);
								break;
						//MITS 32423 : praveen ICD10 Changes
                            case "diagnosislisticd10":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.DiagnosisListicd10);
                                break;
						//MITS 32423 : praveen ICD10 Changes
                            case "RefNo":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.DiagRefNo);
                                break;
							case "amountreducedpercent":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.Percentile); //Kiran MITS 31444 
								break;
							case "eoblist":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.EOBList);
								break;
							case "glaccountcode":
								if(objTransSplit.GlAccountCode <= 0)
									FillComboWithCodes("GL_ACCOUNTS", ref objTempXMLFieldEle, true);
								else
									FillComboWithCodes("GL_ACCOUNTS", ref objTempXMLFieldEle, true, "", objTransSplit.GlAccountCode);
								break;
							case "unitsbillednum":
								if(objTransSplit.BrsInvoiceDetail.UnitsBilledNum==0 && m_objBillItem.UnitsBilled==0)
								{
									m_objCommon.SetValue(ref objTempXMLFieldEle, 1);
								}
								else
								{
									m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.UnitsBilledNum);
								}
								break;
							case "unitsbilledtype":
								if(objTransSplit.BrsInvoiceDetail.UnitsBilledType!=1 && objTransSplit.BrsInvoiceDetail.UnitsBilledType>0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.UnitsBilledType);
								else
									m_objCommon.SetValue(ref objTempXMLFieldEle, 1);
								break;
							case "invoicedby":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.InvoicedBy);
								break;
							case "invoicenumber":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.InvoiceNumber);
								break;
							case "invoicedate":
                                //mbahl3
                                if (!string.IsNullOrEmpty(objTransSplit.InvoiceDate))
                                {
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, Conversion.ToDate(objTransSplit.InvoiceDate).ToShortDateString());
                                }
                                else
                                {
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.InvoiceDate);
                                }
                                

								break;
							case "percentile":
								if(objTransSplit.BrsInvoiceDetail.Percentile.Length > 0)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.Percentile);
								break;
								//pharmacy only
							case "prescripno":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.PrescripNo);
								break;
							case "drugname":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.DrugName);
								break;
							case "prescripdate":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.PrescripDate);
								break;
                            case "prescripcert":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.Certification);
                                break;
                            case "medquantity":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.MedsQuantity);
                                break;
                            case "meddaysupply":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.DaysSupplied);
                                break;
                            case "prescripind":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.RxInd);
                                break;
                            case "supplychargeflag":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.MedSupplyFlag);
                                break;
                            case "usualcharge":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.RxUsualCharge);
                                break;
                            case "purchasedind":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.PurchasedInd);
                                break;
                            case "dispensed":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.Dispensed);
                                break;
                            case "hcpcscode":
                                m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.HcpcsCode);
                                break;
								//dental only
							case "toothno":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.ToothNumber);
								break;
							case "surfacetext":
								m_objCommon.SetValue(ref objTempXMLFieldEle, objTransSplit.BrsInvoiceDetail.SurfaceText);
								break;

								//calculation
							case "amountbilled":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.AmountBilled));
								break;
							case "amountsaved":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.AmountSaved));
								break;
							case "amountreduced":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.AmountReduced));
								break;
							case "amounttopay":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.AmountToPay));
								break;
							case "Amount":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.AmountToPay));
								break;
							case "invoiceamount":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.AmountBilled));
								break;
							//Mukul Added 4/10/07 MITS 9160
							case "sched_amt":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.ScheduledAmount));
								break;
								//values/captions that might have been set during calculation
							case "folluptextlabel":
								if(m_objBillItem.FollowUpMessage.Length > 0)
									objTempXMLFieldEle.InnerText = m_objBillItem.FollowUpMessage;
								break;
								//values set by calc algorithms, needed to store in brsinvoicedetail on save
							case "studyid":
								m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.FeeTableStudy);
								break;
							case "revision":
								m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.FeeDataRevision);
								break;
							case "statecode":
								m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.FeeTableState);
								break;
							case "stoplossflag":
                                //if(objProviderContractOverride != null)
                                //    m_objCommon.SetValue(ref objTempXMLFieldEle, m_objCommon.Val(objProviderContractOverride.m_dPerDiemAmtWithStopLoss.ToString()));
                                //else
									m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.StopLossFlag);
								break;
							case "overrideoption":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, (int)objProviderContractOverride.OverrideOption);
								else
									m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.OverrideType);
								break;
							case "amountallowed":
								m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objTransSplit.BrsInvoiceDetail.AmountAllowed));
								if(objProviderContractOverride != null)
								{
									if(objProviderContractOverride.OverrideOption != Common.BillOverrideType.DO_NOT_OVERRIDE &&
										objProviderContractOverride.OverrideOption != Common.BillOverrideType.APPLY_UNKNOWN )
										m_objCommon.SetValue(ref objTempXMLFieldEle, string.Format("{0:0.00}",objProviderContractOverride.m_dOverrideAmount));
								}
								break;

								//Missing Param Details Section
							case "override1":
								if(objProviderContractOverride != null && Conversion.ConvertStrToDouble(objTempXMLFieldEle.Attributes["value"].Value) == objProviderContractOverride.DefaultOverrideOption)
									m_objCommon.g_objXMLDoc.SelectSingleNode("//override").InnerText=objProviderContractOverride.DefaultOverrideOption.ToString();
								break;
							case "ovr_billedamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dBilledAmt);
								break;
							case "ovr_discbilledamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dBilledAmtWithDisc);
								break;
							case "ovr_contractamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dContractAmt);
                                else
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.ContractAmount);
								break;
							case "ovr_disccontractamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dContractAmtWithDisc);
								break;
							case "ovr_schedamount":
								if(objProviderContractOverride != null)
								{
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.MinAmt(objProviderContractOverride.m_dSchedAmt, objProviderContractOverride.m_dSched2Amt));
									if(objProviderContractOverride.MinAmt(objProviderContractOverride.m_dSchedAmt, objProviderContractOverride.m_dSched2Amt) == objProviderContractOverride.m_dSchedAmt)
									{
										m_objCommon.SetValueOf("ovr_schedamountalt", objProviderContractOverride.m_dSched2Amt,ref p_objXMLDoc);
										m_objCommon.SetValueOf("feeschedused", 1,ref p_objXMLDoc);
									}
									else
									{
										m_objCommon.SetValueOf("ovr_schedamountalt", objProviderContractOverride.m_dSchedAmt,ref p_objXMLDoc);
										m_objCommon.SetValueOf("feeschedused", 2,ref p_objXMLDoc);
									}
								}
								break;
							case "ovr_discschedamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.MinAmt(objProviderContractOverride.m_dSchedAmtWithDisc,objProviderContractOverride.m_dSched2AmtWithDisc));
								break;
							case "ovr_perdiemamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dPerDiemAmt);
								break;
							case "ovr_perdiemstoplossamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dPerDiemAmtWithStopLoss);
								break;
							case "overrideamount":
								if(objProviderContractOverride != null)
									m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dOverrideAmount);
								else if(m_objBillItem.OverrideType != (int)Common.BillOverrideType.APPLY_UNKNOWN && 
									m_objBillItem.OverrideType != (int)Common.BillOverrideType.DO_NOT_OVERRIDE)
									m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.AmountAllowed);
								break;
                            //MITS 17229 : Umesh
                            case "overrideperdiemamount":
                                if (objProviderContractOverride != null)
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dPerDiemAmt);
                                else
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.PerDiemAmount);
                                break;
                            case "ovrperdiemstoplossamount":
                                if (objProviderContractOverride != null)
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.m_dPerDiemAmtWithStopLoss);
                                else
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.StopLossAmt);
                                break;
                            case "ovrschedamount":
                                if (objProviderContractOverride != null)
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, objProviderContractOverride.MinAmt(objProviderContractOverride.m_dSchedAmt, objProviderContractOverride.m_dSched2Amt));
                                else
                                    m_objCommon.SetValue(ref objTempXMLFieldEle, m_objBillItem.EntFeeSchdAmt);
                                break;
                            //MITS 17229 : end
						}//end switch
                    }//end if(iSplitRowId != 0 || bUseItem)
				}//end foreach(objXMLFieldEle in objNodeList)
				
				FilterBRSDom((Common.eSteps)m_objCommon.Val(m_objCommon.GetValueOf("step", p_objXMLDoc)), ref p_objXMLDoc);

                //Get all the nodes to be hidden in the screen and put into the XML
                StringBuilder sbNodesToBeDeleted = new StringBuilder();
                foreach (string sNodeName in m_objCommon.m_KilledNodeList)
                {
                    if (sNodeName.Length > 0)
                        sbNodesToBeDeleted.Append("|");
                    sbNodesToBeDeleted.Append(sNodeName);
                }
                XmlElement oKillNodeElement = (XmlElement)p_objXMLDoc.SelectSingleNode("//option/SysKilledNodes");
                if (oKillNodeElement == null)
                {
                    oKillNodeElement = p_objXMLDoc.CreateElement("SysKilledNodes");
                    XmlNode oSplitDataNode = p_objXMLDoc.SelectSingleNode("//option");
                    if (oSplitDataNode != null)
                        oSplitDataNode.AppendChild((XmlNode)oKillNodeElement);
                }
                oKillNodeElement.InnerText = sbNodesToBeDeleted.ToString();

                //Get the readonly nodes
                string sReadonlyNodes = string.Empty;
                if (bIsReadonly)
                {
                    if (bResubmitReadonly)
                    {
                        sReadonlyNodes = m_objCommon.GetSectionNodeList("resubmitreadonly");
                    }
                    else
                    {
                        sReadonlyNodes = m_objCommon.GetSectionNodeList("readonly");
                    }
                    XmlElement oReadonlyNodeElement = (XmlElement)p_objXMLDoc.SelectSingleNode("//option/SysReadOnlyNodes");
                    if (oReadonlyNodeElement == null)
                    {
                        oReadonlyNodeElement = p_objXMLDoc.CreateElement("SysReadOnlyNodes");
                        XmlNode oSplitDataNode = p_objXMLDoc.SelectSingleNode("//option");
                        if (oSplitDataNode != null)
                            oSplitDataNode.AppendChild((XmlNode)oReadonlyNodeElement);
                    }
                    oReadonlyNodeElement.InnerText = sReadonlyNodes;
                }

				if(objAlertsEle.HasChildNodes)
                    p_objXMLDoc.GetElementsByTagName("option").Item(0).AppendChild(objAlertsEle);
				if(objErrorsEle.HasChildNodes)
                    p_objXMLDoc.GetElementsByTagName("option").Item(0).AppendChild(objErrorsEle);
			}//end Main Try
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.FillBRSDOM.GenericError", m_iClientId), p_objException);
			}
			finally
			{
                if (objTransSplit != null)
                    objTransSplit.Dispose();                            
				objCacheSettings = null;
				objFeeSchedule = null;
				objAlertsEle = null;
				objProviderContractOverride = null;
				objTransSplit = null;
				objErrorsEle = null;
				objNodeList = null;
				objTempXMLFieldEle = null;
				objXMLNode = null;
			}
		}// end FillBRSDOM

		#endregion

		#region public WebReviewBill (ref p_objXMLDoc)
		/// Name		: WebReviewBill
		/// Author		: Navneet Sota
		/// Date Created: 12/14/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Used for calculation methods implemented by the library
		/// </summary>
		/// <returns>
		/// 0 to indicate non-supported calc type.
		/// Return non-zero to indicate success.
		/// </returns>
		/// <param name="p_objXmlDoc">The input XML</param>
        /// <param name="p_iCount"></param>
        /// <param name="p_iFLType"></param>
        /// <param name="p_objHashtable"></param>
        public int WebReviewBill(ref XmlDocument p_objXmlDoc, int p_iFLType, int p_iCount, Hashtable p_objHashtable)
		{
			//int Variables
			int iReturnValue = 0;

			//string Variables
			string sTemp = "";
			string sTemp2 = "";
			
			//Object Variables
			BillItem objItem = null;
			try
			{	
				if(m_objRequest == null)
                    m_objRequest = new Request(m_objCommon, m_iClientId);
				m_objRequest.Errors=m_objCommon.Errors;

                int iStep = Conversion.ConvertObjToInt(p_objXmlDoc.SelectSingleNode("//step").InnerText, m_iClientId);
                string sCmd = p_objXmlDoc.SelectSingleNode("//cmd").InnerText;
				m_objRequest.Step=iStep;
                m_objRequest.Command = sCmd;

				FillBRSItem(ref p_objXmlDoc);

				objItem = new BillItem(m_objCommon, m_iClientId);
				if(m_objBillItem != null)
				{
					m_objRequest.BillItemObject = m_objBillItem;
					objItem = m_objBillItem;
				}
                
				//Populate any Request Specific XML.
				//Kick Off Calculation Logic
                ReviewBill(p_iFLType, p_iCount, p_objHashtable,ref p_objXmlDoc);
				objItem.AmountAllowed = objItem.SchdAmount;

				foreach(DictionaryEntry objEntry in m_objRequest.MissingParams)
				{
					sTemp = objEntry.Value.ToString().ToUpper();
					switch (sTemp)
					{
						case "OVERRIDE":
							//Dump objOverride into string and pass to client side script.
							m_objCommon.SetValueOf("step","7",ref p_objXmlDoc);
							break;
						case "ICCPT":
							//Dump ICCPT LIST to string and pass to client side script
							for(int iTemp1 = 0; iTemp1 < m_objRequest.GetParamICCPTList("ICCPTLIST").Count - 1; iTemp1++)
							{
								sTemp2 = sTemp2 + Conversion.ConvertObjToStr(m_objRequest.GetParamICCPTList("ICCPTLIST")[iTemp1]) + ",";
							}
							m_objCommon.SetValueOf("missing", "ICCPT", ref p_objXmlDoc);
							m_objCommon.SetValueOf("param", sTemp2, ref p_objXmlDoc);
							break;
						case "SPECIALTYSHORTCODE":
							//No additional info to pass to client side script, 
							//just ask for specialty shortcode.
							m_objCommon.SetValueOf("missing", "SPECIALTYSHORTCODE", ref p_objXmlDoc);
							m_objCommon.SetValueOf("param", "", ref p_objXmlDoc);
							break;
						case "PROCEDURECASE":
							//No additional info to pass to client side script, 
							//just ask for procedurecase.
							m_objCommon.SetValueOf("missing", "PROCEDURECASE", ref p_objXmlDoc);
							m_objCommon.SetValueOf("param", "", ref p_objXmlDoc);
							break;
					}//end switch
				}//end for(iCnt = 0; iCnt < m_objRequest.MissingParams.Count - 1; iCnt++)

				FillBRSDOM(ref p_objXmlDoc);

			}//end try
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BRS.WebReviewBill.GenericError", m_iClientId), p_objException);
			}
			finally
			{
				objItem = null;
			}

			return iReturnValue;
		}// end ReviewBill

		#endregion

        /// <summary>
        /// function for bill review
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public int WebReviewBill(ref XmlDocument p_objXmlDoc)
        {
            int iFlType = 0;
            int iCount = 0;
            Hashtable objHashTable = null;
            int iReturnValue = WebReviewBill(ref p_objXmlDoc, iFlType, iCount, objHashTable);
            return iReturnValue;
        }

		#region public FetchKeepOnNextList (p_sUserLoginName)
		/// Name		: FetchKeepOnNextList
		/// Author		: Navneet Sota
		/// Date Created: 1/12/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This function is used for fetching the KeepOnNextList constant 
		/// value from either database or configuration file
		/// </summary>
		/// <remarks>
		/// If the KeepOnNextList value is not defined for a particular user in database then
		/// corresponding value is updated in DB in XML format.
		/// </remarks>
		/// <param name="p_sUserLoginName">User Login</param>
		public string FetchKeepOnNextList(string p_sUserLoginName)
		{
			//String Variables
			string sReturnValue = "";
			string sTemp = "";

			//String Object
			StringBuilder sbSQL = null;

			long lUserID = 0;
			int iRowID = 0;

			//XML Object variables
			XmlDocument objXmlDoc = null;
			XmlElement objXmlElement;

			//Database objects
			DataSet objDataSet=null;
			DbConnection objConn = null;
			DbCommand objCommand = null;
            DbParameter objParam = null;                    //Umesh
			try
			{
				lUserID = m_objCommon.GetSingleLong("USER_ID", "USER_DETAILS_TABLE", "LOGIN_NAME ='" + p_sUserLoginName + "'");

				sbSQL = new StringBuilder();
				objXmlDoc = new XmlDocument();
				
				//Attempt Get List from USER_PREF_XML
				//user_id of -1 is system default setting, order results such that 
				//it is only used if personal setting not found.

				//Table[0]
				sbSQL.Append(" SELECT USER_ID, PREF_XML FROM USER_PREF_XML ");
				sbSQL.Append(" WHERE USER_ID IN (-1,").Append(lUserID).Append(") ");
				sbSQL.Append(" ORDER BY USER_ID DESC ");
				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);
				
				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				{
					iRowID = Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["USER_ID"].ToString());
					sTemp = objDataSet.Tables[0].Rows[0]["PREF_XML"].ToString();
				
					//Parse Out the KeepOnNext value if Available
					if(sTemp != "")
					{
						try
						{
							objXmlDoc.LoadXml(sTemp);
						}
						catch(Exception p_objXmlException)
						{
                            throw new RMAppException(Globalization.GetString("Manager.FetchKeepOnNextList.XmlLoadError", m_iClientId), p_objXmlException);
						}
						if(objXmlDoc != null)
						{
							objXmlElement = (XmlElement)objXmlDoc.SelectSingleNode("//BRS");
							if(objXmlElement != null)
							{
								sTemp = objXmlElement.GetAttribute("KeepOnNext");
								//If value for KeepOnNext is found in DB then return it from here itself
								//no need for doing through rest of the logic below.
								if(sTemp != "")
									sReturnValue = sTemp;
							}//end if(objXmlElement != null)					
						}//end if(objXmlDoc != null)
					}//end if(sTemp != "")
				}//end if (objDataSet.Tables[0] != null)
				
				//if KeepOnNext value is not there in DB
				if(sTemp == "")
				{
					//Attempt Get KeepOnNext value from RMConfigurator
                    sReturnValue = RMConfigurationManager.GetNameValueSectionSettings("BillingReviewSystem",m_objCommon.g_sConnectionString, m_iClientId)["KeepOnNext"];
					
					//Stash Value Into XML for next time.
					if(sReturnValue != null && sReturnValue != "")
					{
						//New XML altogether
						if(objXmlDoc.OuterXml == "")
							objXmlDoc.LoadXml("<option><BRS KeepOnNext='" + sReturnValue + "' /></option>");
						else
						{
							objXmlElement = objXmlDoc.CreateElement("BRS");
							objXmlElement.SetAttribute("KeepOnNext",sTemp);

							objXmlDoc.AppendChild(objXmlElement);
						}

						// Save XML to DataBase for future reference
						sbSQL.Remove(0,sbSQL.Length);

						// Execute the Update transaction.
						objConn = DbFactory.GetDbConnection(m_objCommon.g_sConnectionString);
						objConn.Open();

						objCommand = objConn.CreateCommand();
                        // insert/update using SQL command  using parameter : Umesh
                        objParam = objCommand.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = objXmlDoc.OuterXml;
                        objParam.ParameterName = "XML";
                        objParam.SourceColumn = "PREF_XML";
                        objCommand.Parameters.Add(objParam);

                        sbSQL.Append(" UPDATE USER_PREF_XML SET PREF_XML=~XML~");
                        sbSQL.Append(" WHERE USER_ID = ").Append(iRowID);
						objCommand.CommandText=sbSQL.ToString();
						objCommand.ExecuteNonQuery();
					}//end if(sReturnValue != null && sReturnValue != "")
					else //No list defined
						sReturnValue = "";
				}//end if(sTemp == "")
			}//end try
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.FetchKeepOnNextList.GenericError", m_iClientId), p_objException);
			}
			finally
			{
				sbSQL = null;

				if (objDataSet != null)
					objDataSet.Dispose();

				objCommand = null;
				if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
				objXmlDoc = null;
			}// End finally

			//Return the Result to calling function
			return sReturnValue;
		}// end FetchKeepOnNextList

		#endregion

        #region Check MDB File
        /// Name		: CheckMDBFile
        /// Author		: Alok Singh Bisht(abisht)
        /// Date Created: 2/22/2008
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This function is used for fetching the full path of the MDb for 
        /// given fee schedule selected and then verifying if mdb is present for the 
        /// location specified
        /// </summary>
        /// <remarks>
        /// </remarks>
        public bool CheckMDBFile(ref XmlDocument p_objXMLDoc)
        {
            string sOdbcName = string.Empty;
            string sDbPath = string.Empty;
            //Xml variable
            XmlElement objErrors;
            try
            {
                if (p_objXMLDoc != null)
                {
                    objErrors = (XmlElement)p_objXMLDoc.CreateElement(Common.ERR_ERRORS);
                    //Assign the passed in XML to Global variable so that it is available to 
                    //whole component
                    m_objCommon.g_objXMLDoc = p_objXMLDoc;

                    //ODBC Name.BRS Issue
                    if (m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXMLDoc)) > 0)
                    {
                        sOdbcName = m_objCommon.GetSingleString("ODBC_NAME", "FEE_TABLES", "TABLE_ID=" + m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXMLDoc)));
                        if (sOdbcName != "")
                            sDbPath = m_objCommon.GetSingleString("DB_PATH", "BRS_FEESCHD", "FEESCHD_NAME= '" + sOdbcName + "'");

                        if (sDbPath.ToLower().EndsWith(".mdb"))
                        {
                            if (sDbPath != "" && !File.Exists(sDbPath))
                            {
                                m_objCommon.AddAlert(Globalization.GetString("Manager.GetFeeSchedule.MissingMDBFile", m_iClientId));
                                m_objCommon.HandleXMLErrorMessage(ref m_objCommon.g_objXMLDoc, ref objErrors, "BRS.Manager.CheckMDBFile");
                            }
                        }
                    }
                    if (objErrors.HasChildNodes)
                    {
                        p_objXMLDoc.SelectSingleNode("//option").InsertBefore(objErrors, p_objXMLDoc.SelectSingleNode("//option").FirstChild);
                        return false;
                    }
                    return true;
                }
                return true;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Manager.FillBRSItem.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                objErrors = null;
            }
        }
        #endregion

        #region public int GetFLType(Hashtable p_objHashtable)
        /// <summary>
        ///Get the  FLtype for set of bill items
        /// </summary>
        public int GetFLType(Hashtable p_objHashtable)
        {
            string splacecode = null;
            string sBillItems = "";

            int iRevCodeTemp;
            int iFLType = 0;
            int iCountOfBillItems = p_objHashtable.Count;

            XmlDocument objSessionBillItem = null;
            LocalCache objCache = null;

            try
            {
                objSessionBillItem = new XmlDocument();
                objCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClientId);
                sBillItems = p_objHashtable[iCountOfBillItems].ToString();
                objSessionBillItem.LoadXml(sBillItems);
                splacecode = objCache.GetShortCode(Conversion.ConvertStrToInteger(objSessionBillItem.SelectSingleNode("//placeofservice/@codeid").InnerText.Trim()));
                if (objCache != null)
                    objCache.Dispose();
                switch (splacecode)
                {
                    case "24": //8 ambulatory center
                        iFLType = 8;
                        break;
                    case "51":
                    case "52":
                    case "53":
                    case "54":
                    case "55":
                    case "56":
                    case "61":
                    case "62":
                        iFLType = 5;
                        break;
                }

                //for each line item go through
                bool bSurgical = false;
                bool bInpatient = false;

                if (iFLType == 0)
                {
                    foreach (DictionaryEntry Item in p_objHashtable)
                    {
                        sBillItems = Item.Value.ToString();
                        objSessionBillItem.LoadXml(sBillItems);
                        //check revenue code
                        iRevCodeTemp = Conversion.ConvertStrToInteger(objSessionBillItem.SelectSingleNode("//RevenueCode").InnerText);

                        switch (iRevCodeTemp)
                        {
                            case 360:
                            case 361:
                            case 362:
                            case 367:
                            case 369:
                                bSurgical = true;
                                break;
                        }

                        if (iRevCodeTemp >= 120 && iRevCodeTemp <= 169)
                            bInpatient = true;
                    }
                    if (bInpatient)
                    {
                        if (bSurgical)
                            iFLType = 1;
                        else
                            iFLType = 2;
                    }
                    else
                        iFLType = 6;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Manager.GetFLType.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
                objSessionBillItem = null;
            }

            return iFLType;
        }
        # endregion

		#endregion

		#region Private Methods

		#region "Initialize"
		/// Name		: Initialize
		/// Author		: Navneet Sota
		/// Date Created	: 1/12/2005	
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string;
		/// </summary>
		private void Initialize()
		{
			try
			{
				if(m_objDataModelFact==null)
				{
					m_objCommon.g_objDataModelFactory = new DataModelFactory(m_objCommon.g_sDsnName , m_objCommon.g_sUserName , m_objCommon.g_sPassword, m_iClientId);//rkaur27	
				}
				else
				{
                   m_objCommon.g_objDataModelFactory=m_objDataModelFact;
				}
				m_objCommon.g_sConnectionString = m_objCommon.g_objDataModelFactory.Context.DbConn.ConnectionString;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Manager.Initialize.ErrorInit", m_iClientId) , p_objEx );//rkaur27				
			}			
		}	
		#endregion

		#region private FilterBRSDom(p_enumStepID, ref p_objXmlDocument)
		/// Name		: FilterBRSDom
		/// Author		: Navneet Sota
		/// Date Created: 1/20/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Screens out inappropriate GUI elements based on the process step, 
		/// the current data selections and the type of Fee Schedule
		/// </summary>
		/// <param name="p_enumStepID"> Step-ID</param>
		/// <param name="p_objXmlDocument">XML Doc</param>
		/// <remarks>		
		/// Relies on a fully populated Dom Document.The dom may or may not 
		/// possess all nodes, but data MUST be present already.
		/// </remarks>
		private void FilterBRSDom(Common.eSteps p_enumStepID, ref XmlDocument p_objXmlDocument)
		{
			int iCalcType = 0;
            int ifeesched;
            int iBillTypeCode = 0;

            string sState = "";
            string sSQL = "";
            string sBillTypeShortCode = "";

            //PJS 11/29/2007- MITS 10814 - defined objNode
            try
            {
			    if(p_enumStepID != Common.eSteps.Preload)
			    {
				    //Remove Groups & Nodes based on Input Selections
                    if (!Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(m_objCommon.GetValueOf("contractexists", p_objXmlDocument)), m_iClientId))
                    {
                        m_objCommon.RemoveNamedNode("contract");
                        m_objCommon.RemoveNamedNode("overridegroup");
                    }//end if(contractexists)
                    else if (p_enumStepID != Common.eSteps.Override)
                        m_objCommon.RemoveNamedNode("overridegroup");

				    //No First Fee Schedule
                    //if(m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXmlDocument)) <= 0)
                    //{
                    //    m_objCommon.RemoveNamedNode("feescheddesc");
                    //}

                    ////Get fee table list, move to FillBRSDOM function
                    //GetFeeSchedules(ref p_objXmlDocument, m_objCommon.Val(m_objCommon.GetValueOf("providereid", p_objXmlDocument)),
                    //                    Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXmlDocument)),
                    //                    Conversion.GetDate(m_objCommon.GetValueOf("ToDate", p_objXmlDocument)),
                    //                    m_objCommon.Val(m_objCommon.GetValueOf("transid", p_objXmlDocument)),
                    //                    m_objCommon.Val(m_objCommon.GetValueOf("SplitRowId", p_objXmlDocument)));

				    //No Second Fee Schedule
                    if (m_objCommon.Val(m_objCommon.GetValueOf("feesched2", p_objXmlDocument)) <= 0)
                        m_objCommon.RemoveNamedNode("feescheddesc2");

				    //No Specialty Description Label
                    if (m_objCommon.Val(m_objCommon.GetValueOf("specialty_desc", p_objXmlDocument)) <= 0)
                        m_objCommon.RemoveNamedNode("specialty_desc");

				    if(m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXmlDocument)) <= 0)
                        iCalcType = -1;
                    else
					    iCalcType = Conversion.ConvertStrToInteger(m_objCommon.GetSingleString("CALC_TYPE", "FEE_TABLES", "TABLE_ID=" + m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXmlDocument))));

                    using (LocalCache objCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClientId))
                    {
                        ifeesched = m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXmlDocument));
                        sSQL = "SELECT ODBC_NAME,STATE FROM FEE_TABLES WHERE TABLE_ID = " + ifeesched;
                        using (DbReader objReader = DbFactory.GetDbReader(m_objCommon.g_sConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                sState = objCache.GetStateCode(objReader.GetInt("STATE"));
                            }
                        }
                        XmlElement objFieldElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//statecode");
                        m_objCommon.SetValue(ref objFieldElement, sState);

                        if (sState != "FL")
                            m_objCommon.RemoveNamedNode("FLLicense");

                        iBillTypeCode = Conversion.ConvertStrToInteger(m_objCommon.GetValueOf("billtype", p_objXmlDocument));
                        if (iBillTypeCode != 0)
                            sBillTypeShortCode = objCache.GetShortCode(iBillTypeCode);
                    }
				    //Remove Groups & Nodes based on Calctype
				    switch(iCalcType)
				    {
					    case Common.UCR94:
					    case Common.UCR95:
					    case Common.OPT:
					    case Common.HCPCS:
					    case Common.DENT:
						    break;
					    default:
                            m_objCommon.RemoveNamedNode("percentile");
						    break;
				    }
    				
				    switch (iCalcType)
				    {
					    case Common.DENT: //Remove billingcodefreeentry,pharmacy
                            //caggarwal4 Merged Issue RMA-9115 starts
                             m_objCommon.RemoveNamedNode("btnCalculate");
                            m_objCommon.RemoveNamedNode("billingcodesearch");
                            m_objCommon.RemoveNamedNode("pharmacy");
                            m_objCommon.RemoveNamedNode("RevenueCode");
                            m_objCommon.RemoveNamedNode("billingcode_cid");
                            m_objCommon.RemoveNamedNode("billingcode");
                            m_objCommon.RemoveNamedNode("DenyLineItem");//srajindersin MITS 29012 dt-09/26/2012
                            m_objCommon.RemoveNamedNode("billingcode_text");
                            //caggarwal4 ends
						    break;
                        case Common.FREEPHARM: //Remove billingcodesearch,dental and rename billingcode_cid to "NDC Code and delete description "
                            m_objCommon.RemoveNamedNode("billingcodesearch");
                            m_objCommon.RemoveNamedNode("dental");
                            m_objCommon.RemoveNamedNode("RevenueCode");
                            m_objCommon.RemoveNamedNode("billingcode_cid");
                            m_objCommon.RemoveNamedNode("billingcode");
                            m_objCommon.RemoveNamedNode("DenyLineItem");//srajindersin MITS 29012 dt-09/26/2012

                            //XmlNode n = p_objXmlDocument.SelectSingleNode("//billingcode_cid");
                            //if (n.Attributes["title"] != null)
                            //{
                            //    n.Attributes["title"].Value = Globalization.GetString("Manager.NDCCode");
                            //}
                            //PJS 11/29/2007- MITS 10814 - Remove Description in case of NDC - Free Pharmacy
                            m_objCommon.RemoveNamedNode("billingcode_text");

                            //PJS 11/29/2007- MITS 10814 -End
                            break;
                            case Common.FREEGENERIC: //Remove billingcodesearch,pharmacy,dental and RevenueCode
                                m_objCommon.RemoveNamedNode("billingcodesearch");
                                m_objCommon.RemoveNamedNode("dental");
                                m_objCommon.RemoveNamedNode("pharmacy");
                                m_objCommon.RemoveNamedNode("RevenueCode");
                                m_objCommon.RemoveNamedNode("DenyLineItem");//srajindersin MITS 29012 dt-09/26/2012
                                break;
                            case Common.FREEHOSP: //Remove billingcodesearch,pharmacy,dental
                                m_objCommon.RemoveNamedNode("billingcodesearch");
                                m_objCommon.RemoveNamedNode("dental");
                                m_objCommon.RemoveNamedNode("pharmacy");
                                break;
                            case Common.FLHOSP: //BRS FL Merge   : Umesh
                                m_objCommon.RemoveNamedNode("btnCalculate");
                                m_objCommon.RemoveNamedNode("billingcodesearch");
                                m_objCommon.RemoveNamedNode("dental");
                                m_objCommon.RemoveNamedNode("pharmacy");
                                //MITS : 12421
                                m_objCommon.RemoveNamedNode("amountreducedpercent");
                                m_objCommon.RemoveNamedNode("amountreduced");
                                break;
                            case Common.WC00 :
                                m_objCommon.RemoveNamedNode("billingcodefreeentry");
                                m_objCommon.RemoveNamedNode("dental");
                                if(sBillTypeShortCode!="RX")
                                    m_objCommon.RemoveNamedNode("pharmacy");
                                m_objCommon.RemoveNamedNode("RevenueCode");
                                m_objCommon.RemoveNamedNode("DenyLineItem");//srajindersin MITS 29012 dt-09/26/2012
                                break;
					    default: //Remove billingcodefreeentry,pharmacy,dental
                                m_objCommon.RemoveNamedNode("billingcodefreeentry");
                                m_objCommon.RemoveNamedNode("RevenueCode");
                                m_objCommon.RemoveNamedNode("pharmacy");
                                m_objCommon.RemoveNamedNode("dental");
                                m_objCommon.RemoveNamedNode("DenyLineItem");//srajindersin MITS 29012 dt-09/26/2012
						    break;
				    }//end switch (iCalcType)

                    //If Not(Common.FREEPHARM OR (Common.WC00 AND BillType = "RX")), hide all drug realted field
                    if(!(iCalcType==Common.FREEPHARM ||(iCalcType==Common.WC00 && string.Compare(sBillTypeShortCode, "RX", true)==0 )))
                    {
                        m_objCommon.RemoveNamedNode("prescripno");
                        m_objCommon.RemoveNamedNode("prescripdate");
                        m_objCommon.RemoveNamedNode("prescripcert");
                        m_objCommon.RemoveNamedNode("drugname");
                        m_objCommon.RemoveNamedNode("medquantity");
                        m_objCommon.RemoveNamedNode("meddaysupply");
                        m_objCommon.RemoveNamedNode("prescripind");
                        m_objCommon.RemoveNamedNode("purchasedind");
                        m_objCommon.RemoveNamedNode("supplychargeflag");
                        m_objCommon.RemoveNamedNode("usualcharge");
                        m_objCommon.RemoveNamedNode("dispensed");
                        m_objCommon.RemoveNamedNode("hcpcscode");
                    }

				    GenerateStepDom(ref p_objXmlDocument,p_enumStepID);
                }//end if(p_enumStepID != Common.eSteps.Preload)
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(p_objException.Message, p_objException);
            }
            finally
            {
            
            }
		}// end FilterBRSDom

		#endregion

		#region private ReviewBill ()
		/// Name		: ReviewBill
		/// Author		: Navneet Sota
		/// Date Created: 12/14/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Request is altered
		/// </summary>
        private double ReviewBill(int p_iFLType, int p_iCount, Hashtable p_objHashtable, ref XmlDocument p_objXmlDocument)
		{
			double dReturnValue = 0;

			//objects
			FeeSchedule objFeeSchedule = null;
			ProviderContractOverride objProviderContractOverride = null;
			try
			{
				objFeeSchedule = new FeeSchedule(m_objCommon, m_iClientId);
				//Find out if this Request involves a contract.
				m_objRequest.BillItemObject.ContractExists = 
					FindContract(m_objRequest.BillItemObject.PayeeId, m_objRequest.BillItemObject.objSplit.FromDate);
                
				if(m_objRequest.BillItemObject.ContractExists)
				{
					m_objRequest.ProviderContract=this.m_objProviderContracts;
				}
				if(m_objRequest.BillItemObject.ContractExists)
				{
					// There Is a Contract - Populate a ProviderContractOverride object
					objProviderContractOverride = m_objCommon.ProviderContractReviewBill(ref m_objRequest);
					if(objProviderContractOverride == null)
					{
						throw new RMAppException("An applicable contract exists but the override logic could not be completed.");
						//m_objRequest.Errors.Add(new Exception("An applicable contract exists but the override logic could not be completed."),BusinessAdaptorErrorType.Error);
					}	//m_objRequest.Errors.Add("","An applicable contract exists but the override logic could not be completed.");
					else
						ApplyManualReduction(ref m_objRequest);
				}
				else //No Contract
				{
					//Set Up Base fee schedule from the request.
					objFeeSchedule.MoveToRecord(m_objRequest.BillItemObject.EntFeeSchdId);
				
					//Ask Fee Schedule for an Amount value.
                    dReturnValue = objFeeSchedule.GetScheduledAmount(m_objRequest, p_iFLType, p_iCount, p_objHashtable);

					if(m_objRequest.MissingParams.Count > 0)
						return dReturnValue; 
				
					m_objRequest.BillItemObject.SchdAmount = dReturnValue;
					ApplyManualReduction(ref m_objRequest);
				}//end if(m_objRequest.BillItemObject.ContractExists)
			}//end try
            catch (Exception p_objException)
			{
                throw new RMAppException(p_objException.Message, p_objException);
			}
			finally
			{
				objFeeSchedule = null;
				objProviderContractOverride = null;
			}

			return dReturnValue;
		}// end ReviewBill

		#endregion
        private double ReviewBill()
        {
            XmlDocument p_objXmlDoc = null;
            int iFLType = 0;
            int iCount = 0;
            Hashtable objHashtable = null;
            double dReturnValue = ReviewBill(iFLType, iCount, objHashtable,ref p_objXmlDoc);
            return dReturnValue;
        }

		#region private ApplyManualReduction (ref p_objRequest)
		/// Name		: ApplyManualReduction
		/// Author		: Navneet Sota
		/// Date Created: 1/12/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Manual Reduction to be applied on the amount to be paid.
		/// </summary>
		/// <remarks>		
		/// Amount to pay is computed as follows
		/// min(SchdAmount, Amount Billed) - Reduction Amount
		/// if Amount to Pay is negative in the above calculation then it is set to 0
		/// </remarks>
		/// <param name="p_objRequest">The Request class object</param>
		private void ApplyManualReduction(ref Request p_objRequest)
		{
			//double dRedAmountDollars = 0.0;
            int iCalcType = 0;
			//Objects
			ProviderContractOverride objOverride = new ProviderContractOverride(m_objCommon, m_iClientId);
			Settings.CCacheFunctions objCacheSettings = null;
			Settings.BrsSettings objBrsSettings = null;
			try
			{
                iCalcType = Conversion.ConvertStrToInteger(m_objCommon.GetSingleString("CALC_TYPE", "FEE_TABLES", "TABLE_ID=" + p_objRequest.BillItemObject.EntFeeSchdId));
                //MITS 13164 : Umesh
                //zmohammad MITS 29012 Start
                //p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.SchdAmount;
                p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.DenyLineItem ? 0 : p_objRequest.BillItemObject.SchdAmount;
				
                if((p_objRequest.BillItemObject.AmountBilled < p_objRequest.BillItemObject.SchdAmount) && iCalcType != Common.FLHOSP)
				{
					//p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.AmountBilled;
                    p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.DenyLineItem ? 0 : p_objRequest.BillItemObject.AmountBilled;

					if(p_objRequest.GetParam("OVERRIDE") != null)
					{
						objCacheSettings = new Riskmaster.Settings.CCacheFunctions(m_objCommon.g_sConnectionString, m_iClientId);
						objBrsSettings = new Riskmaster.Settings.BrsSettings(m_objCommon.g_sConnectionString,m_iClientId);
						if(objBrsSettings != null && objBrsSettings.GenEOBCodeFlag >= 1)
						{
							//TODO: Function to be added in LocalCache
                            DataSimpleList oEOBList = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.EOBList;
                            oEOBList.Remove(objCacheSettings.GetCodeIDWithShort(objOverride.OverrideEOBCode, "EOB_CODES"));
							objOverride.OverrideOption = Common.BillOverrideType.DO_NOT_OVERRIDE;
                            int iEOBCodeId = objCacheSettings.GetCodeIDWithShort(objOverride.OverrideEOBCode, "EOB_CODES");
                            oEOBList.Remove(iEOBCodeId);
                            oEOBList.Add(iEOBCodeId);
						}//end if(objBrsSettings != null && objBrsSettings.GenEOBCodeFlag)
					}//end if(p_objRequest.GetParam("OVERRIDE") != null)
				}//end if(p_objRequest.BillItemObject.AmountBilled < p_objRequest.BillItemObject.SchdAmount)
				else
					//Mukul Added 3/27/07 MITS 8695 Manual Override case is handled here
					if (p_objRequest.Step==(int)Riskmaster.Application.BRS.Common.eSteps.Calculate && p_objRequest.Command!="complete") 
						//p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.SchdAmount;
                        p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.DenyLineItem ? 0 : p_objRequest.BillItemObject.SchdAmount;
                    //MITS 12421 :Umesh
					if(p_objRequest.Step==(int)Riskmaster.Application.BRS.Common.eSteps.Calculate  &&  iCalcType == Common.FLHOSP)
                        //p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.SchdAmount;
                        p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.DenyLineItem ? 0 : p_objRequest.BillItemObject.SchdAmount;
				//Mukul Commented as now it allowed manual override so it may reduce double of that amount so relying on javascript calculation.

                //MITS 13164 : Umeshweb
                if (p_objRequest.BillItemObject.RedAmount != 0)
                   // p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.AmountToPay - p_objRequest.BillItemObject.RedAmount;
                    p_objRequest.BillItemObject.AmountToPay = p_objRequest.BillItemObject.DenyLineItem ? 0 : p_objRequest.BillItemObject.AmountToPay - p_objRequest.BillItemObject.RedAmount;
				if (p_objRequest.BillItemObject.AmountToPay < 0)
				{
					p_objRequest.BillItemObject.AmountToPay = 0;
				}
                //MITS 13164 : End 
                //zmohammad MITS 29012 End
				p_objRequest.BillItemObject.AmountSaved =
                     p_objRequest.BillItemObject.AmountBilled - Convert.ToDouble(string.Format("{0:0.00}", p_objRequest.BillItemObject.AmountToPay));

			}//end try
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.ApplyManualReduction.GenericError", m_iClientId), p_objException);
			}
			finally
			{
				objBrsSettings = null;
				objCacheSettings = null;
				objOverride = null;
			}
		}// end ApplyManualReduction

		#endregion

		#region private GenerateStepDom(ref p_objXmlDocument,p_StepID)
		/// Name		: GenerateStepDom
		/// Author		: Navneet Sota
		/// Date Created: 1/18/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <param name="p_objXmlDocument">XML Document</param>
		/// <param name="p_StepID">e-StepId</param>
		private void GenerateStepDom(ref XmlDocument p_objXmlDocument, Common.eSteps p_StepID)
		{
			//XML Object variables
			XmlElement objXmlElement;
			XmlElement objTempXmlElement;
			XmlElement objErrors =null;
			XmlElement objAlerts = null;
			
			string sTemp = "";
            string sSubtitle1="";
            string sSubtitle2="";
            string sSubtitle3 = "";
			try
			{
				objErrors = p_objXmlDocument.CreateElement(Common.ERR_ERRORS);
				objAlerts = p_objXmlDocument.CreateElement(Common.ALERTS);

                objXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//option");
                sSubtitle1 = p_objXmlDocument.SelectSingleNode("//CurrentBillItemIndex").InnerText;
                sSubtitle2 = p_objXmlDocument.SelectSingleNode("//TotalBillItem").InnerText;
                sSubtitle3 = "(Item " + sSubtitle1 + " of " + sSubtitle2 + " )";

				sTemp = ",,mode,step,transid,splitid,claimid,providereid,fromdate,todate,feesched,feesched2,feeschedused,calctype,contractexists,inputneeded,";
                sTemp = sTemp + "stoplossflag,studyid,revision,statecode,overrideoption,overrideamount,overrideperdiemamount,ovrperdiemstoplossamount,ovrschedamount,idx,cmd,lineofbusinesscode,";
                sTemp = sTemp + "UniqueId0,BillItemUniqueId,NextBillItem,CurrentBillItemIndex,TotalBillItem,CreateNewSession,AddCurrentBillItem,createspace,resubmitmode,";
                sTemp = sTemp + "sysCmdQueue,sys_formidname,sys_formpidname,sys_formpform,sys_ex,sys_notreqnew,SysFormName,overrideoption,error,errors,alert,alerts,sessionposition,hdAction,plitrowidCmboSpecCds,CmboSpecCdsAmount";

				if(objXmlElement != null)
				{
					switch (p_StepID)
					{
						case Common.eSteps.FeeSchedule: //fee schedule combobox(es) or textbox(es)
							GetFeeSchedules(ref p_objXmlDocument, m_objCommon.Val(m_objCommon.GetValueOf("providereid", p_objXmlDocument)),
                                Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXmlDocument)),
                                Conversion.GetDate(m_objCommon.GetValueOf("ToDate", p_objXmlDocument)),
								m_objCommon.Val(m_objCommon.GetValueOf("transid", p_objXmlDocument)), 
								m_objCommon.Val(m_objCommon.GetValueOf("SplitRowId", p_objXmlDocument)));//vkumar258 ML Changes
							break;
						case Common.eSteps.Override:
                            //RemoveChildBranchesFiltered (sTemp);
							objXmlElement.SetAttribute("subtitle", ":- The provider has a prenegotiated contract amount, discount, per diem rate, and/or schedule. Select the desired option and click the 'Continue' button.");
							break;
                        case Common.eSteps.LoadBRSXML:
                            int iCalcType = 0;            
                            iCalcType = Conversion.ConvertStrToInteger(m_objCommon.GetSingleString("CALC_TYPE", "FEE_TABLES", "TABLE_ID=" + m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXmlDocument))));
                            if (iCalcType == Common.FLHOSP)
                            {
                                objTempXmlElement = (XmlElement)objXmlElement.SelectSingleNode("//amountbilled");
                                if (objTempXmlElement != null)
                                {
                                    objTempXmlElement.RemoveAttribute("lookupbutton");
                                    objTempXmlElement.RemoveAttribute("functionname");
                                }
                            }
                            objXmlElement.SetAttribute("subtitle", sSubtitle3);
                            //abisht BRS Issue.
                            bool bContractExists = FindContract(m_objCommon.Val(m_objCommon.GetValueOf("providereid", p_objXmlDocument)),
                                Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXmlDocument)));//vkumar258 ML Changes
                            if (bContractExists)
                            {
                                if (m_objProviderContracts != null)
                                {
                                    int iFeeSchedule = m_objProviderContracts.FeeTableId;
                                    int iFeeSchedule2 = m_objProviderContracts.FeeTableId2nd;
                                    if (iFeeSchedule < 0 && iFeeSchedule2 < 0 && iFeeSchedule > -100)
                                    {
                                        m_objCommon.RemoveNamedNode("modifierlist");
                                        m_objCommon.RemoveNamedNode("billingcodesearch");
                                    }
                                }//end if(m_objProviderContracts != null)
                            }
                            break;
                        case Common.eSteps.Calculate:
                            //abisht BRS Issue
                            bool bContractExist = FindContract(m_objCommon.Val(m_objCommon.GetValueOf("providereid", p_objXmlDocument)),
                                Conversion.GetDate(m_objCommon.GetValueOf("FromDate", p_objXmlDocument)));//vkumar258 ML Changes
                            if (bContractExist)
                            {
                                if (m_objProviderContracts != null)
                                {
                                    int iFeeSchedule = m_objProviderContracts.FeeTableId;
                                    int iFeeSchedule2 = m_objProviderContracts.FeeTableId2nd;
                                    if (iFeeSchedule < 0 && iFeeSchedule2 < 0 && iFeeSchedule > -100)
                                    {
                                        m_objCommon.RemoveNamedNode("modifierlist");
                                        m_objCommon.RemoveNamedNode("billingcodesearch");
                                    }
                                }//end if(m_objProviderContracts != null)
                            }

                            objXmlElement.SetAttribute("subtitle", sSubtitle3);
                            break;
                        default:
                            
                            objXmlElement.SetAttribute("subtitle", sSubtitle3);

                            break;
					}//end switch

					if(objAlerts.HasChildNodes)
                        p_objXmlDocument.GetElementsByTagName("option").Item(0).AppendChild(objAlerts);
					if(objErrors.HasChildNodes)
                        p_objXmlDocument.GetElementsByTagName("option").Item(0).AppendChild(objErrors);
				}//end if(objXMLNode != null)				
			}//End of Main Try Block
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.GeneratStepDom.GenericError", m_iClientId), p_objException);
			}
			finally
			{
				objAlerts = null;
				objErrors = null;
				objTempXmlElement = null;
				objXmlElement = null;
			}
		}// end GenerateStepDom

		#endregion

		#region private RemoveChildBranchesFiltered (p_objXMLNode, p_sFilterList)
		/// Name		: RemoveChildBranchesFiltered
		/// Author		: Navneet Sota
		/// Date Created: 1/12/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>	
		/// Removes filtered Child nodes from the passed in XML Node
		/// </summary>
		/// <param name="p_sFilterList">The list of nodes that needs to be removed</param>
		private void RemoveChildBranchesFiltered(string p_sFilterList)
		{

			string[] arrFilter = null;
            arrFilter = p_sFilterList.Split(',');
            foreach (string sFieldName in arrFilter)
            {
                if (!m_objCommon.m_KilledNodeList.Contains(sFieldName))
                {
                    m_objCommon.m_KilledNodeList.Add(sFieldName);
                }
            }
		}// end function RemoveChildBranchesFiltered()
        #endregion

        #region private GetFeeSchedules(p_objXMLNode,p_iEntityId,p_sFromDate,p_sToDate,p_iTransId,p_iTransSplitId)
        /// Name		: GetFeeSchedules
		/// Author		: Navneet Sota
		/// Date Created: 1/12/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Gets the fee schedule from database and stuff them into passed in XML Doc
		/// </summary>
		/// <param name="p_objXMLDoc">XML Document to be changed</param>
		/// <param name="p_iEntityId">Enitity ID</param>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_iTransId">Trans ID</param>
		/// <param name="p_iTransSplitId">Trans Split Id</param>
		private void GetFeeSchedules(ref XmlDocument p_objXMLDoc, int p_iEntityId, string p_sFromDate, 
			string p_sToDate, int p_iTransId, int p_iTransSplitRowId)
		{
			//String Object
			StringBuilder sbSQL = null;
            //abisht BRS Issue.If no table comes out for the provided fee schedule ID then need to do default handling for that.
            string sSQL = string.Empty;
			XmlElement objXmlElement;
			XmlElement objErrors =null;
			XmlElement objAlerts = null;
			XmlElement objAppendTarget = null;
			
			//Database objects
			DataSet objDataSet=null;

			bool bContractExists = false;
			bool bFound = false;

			int iFeeSchedule = 0;
			int iFeeSchedule2 = 0;
			int iCnt = 0;

			string sSubTitle = "";
			string sTemp = "";
            
			int iPreservedFeeScheduleId=0;
			try
			{
                //If there is no from/to date, we can assume the initial adding item step
                if (string.IsNullOrEmpty(p_sFromDate) && string.IsNullOrEmpty(p_sToDate))
                    return;

				objErrors = p_objXMLDoc.CreateElement(Common.ERR_ERRORS);
				objAlerts = p_objXMLDoc.CreateElement(Common.ALERTS);

				if(m_objCommon.g_objXMLDoc == null)
					m_objCommon.g_objXMLDoc = new XmlDocument();
				m_objCommon.g_objXMLDoc = p_objXMLDoc;

                objAppendTarget = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//option");

                //objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feescheddesc");
                //objXmlElement.InnerText = string.Empty;

				//Remove nodes
				//"feesched" & "feesched2" are already present in the brs.xml as an id control,
				//Get rid of them to avoid a duplicate name conflict when adding the display items!
				/*
				 *Before removing the following nodes, need to keep track of ref's else Orbeon will complain.-Tanuj
				 * */
                
				iPreservedFeeScheduleId = m_objCommon.Val(m_objCommon.GetValueOf("feesched", p_objXMLDoc));
				//m_objCommon.RemoveNamedNode("feesched");
				//m_objCommon.RemoveNamedNode("feesched2");
				//m_objCommon.RemoveNamedNode("feescheddesc");
				//m_objCommon.RemoveNamedNode("feescheddesc2");

				//This reset if combo box is filled
				sSubTitle = "Step 2.  Choose 'Next' to continue.";

				//Get the provider contract, if any
                bContractExists = FindContract(p_iEntityId, p_sFromDate);
				if(bContractExists)
				{
					if(m_objProviderContracts != null)
					{
						iFeeSchedule = m_objProviderContracts.FeeTableId;
						iFeeSchedule2 = m_objProviderContracts.FeeTableId2nd;				
					}//end if(m_objProviderContracts != null)
				}
				else
				{
					//if editing an existing transsplit and no contract, grab the fee schedule from it
                    if (p_iTransId != 0 && p_iTransSplitRowId > 0)
					{
						//FillBRSItem(ref p_objXMLDoc);
						m_objFunds.MoveTo(p_iTransId);
                        FundsTransSplit objSplit = m_objFunds.TransSplitList[p_iTransSplitRowId];
						if(objSplit!=null)
							iFeeSchedule= objSplit.BrsInvoiceDetail.TableCode;
					}
					else
					{
						/*Tanuj-> Bill item was in session and user want to edit that.
						 * i.e this item not yet make it to database*/
						//Keep the original FeeSchedule value even it's not saved to db yet.
						//if(p_iTransId!=0 && p_iTransSplitId<=0)
						//{
                           iFeeSchedule = iPreservedFeeScheduleId;
						//}
					}
				}

				sbSQL = new StringBuilder();

                //objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feesched");
                //if (bContractExists)
                //    objXmlElement.SetAttribute("title", "Contract Table 1:");
                //else
                //    objXmlElement.SetAttribute("title", "Table:");

				//create the combo box if no contract OR if contract and lFeeSchedule is a state (<= -100) 
				//OR if fee schedule = 0
                // abisht BRS Issue.Changed the condition to make it work with the RMWorld contracts as well.
				if(!bContractExists || (bContractExists && iFeeSchedule <= -100) 
                    || (bContractExists && iFeeSchedule == 0))
				{
                    //Table[0] fee schedule SQL
                    sbSQL.Append(" SELECT TABLE_ID  ID,USER_TABLE_NAME  DESCRIPTION FROM FEE_TABLES ");
                    sbSQL.Append("WHERE (('").Append(p_sFromDate).Append("' >= START_DATE AND '");
                    sbSQL.Append(p_sFromDate).Append("' <= END_DATE) OR ");
                    if (p_sToDate != "")
                    {
                        sbSQL.Append(" ('").Append(p_sFromDate).Append("' >= START_DATE AND '");
                        sbSQL.Append(p_sToDate).Append("' <= END_DATE) OR ");
                    }
                    sbSQL.Append(" ('").Append(p_sFromDate).Append("' >= START_DATE ");

                    if (p_sToDate != "")
                    {
                        sbSQL.Append(" AND '").Append(p_sToDate).Append("' <= END_DATE OR ");
                        sbSQL.Append(" ('").Append(p_sFromDate).Append("' >= START_DATE) ");
                    }
                    sbSQL.Append(" AND END_DATE = '00000000')) ");

                    //Support for auto select by state on contracts  (Millenium)
                    if (bContractExists && iFeeSchedule <= -100)
                        sbSQL.Append(" AND STATE = ").Append(-1 * (iFeeSchedule + 100));

                    sbSQL.Append(" ORDER BY PRIORITY, DESCRIPTION");
                    //abisht BRS Issue.Assigning value from string builder to string variable so that the SQL in the string builder is not lost.
                    sSQL = sbSQL.ToString();

					sSubTitle = "Step 2.  Select a fee table and choose 'Next' to continue.";
                    objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feesched");
                    
                    //Try to keep the user selected fee schedue to be selected.
                    int iSelectedFeeSchedule = 0;
                    if (iPreservedFeeScheduleId > 0 && iFeeSchedule < -100)
                        iSelectedFeeSchedule = iPreservedFeeScheduleId;
                    else
                        iSelectedFeeSchedule = iFeeSchedule;

                    if (iFeeSchedule != -1)
                    {
                        iCnt = FillComboWithCodes("", ref objXmlElement, false, sbSQL.ToString(), iSelectedFeeSchedule);
                        //If there is no fee table selected yet, set to the first one in the list
                        if (iCnt > 0 && iSelectedFeeSchedule ==0)
                        {
                            XmlNode oFeeTableNode = objXmlElement.SelectNodes("./option")[0];
                            objXmlElement.SetAttribute("codeid", oFeeTableNode.Attributes["value"].Value);
                        }
                    }
                    else
                    {
                        iCnt = FillComboWithCodes("", ref objXmlElement, true, sbSQL.ToString(), iSelectedFeeSchedule);
                    }

                    if (!(bContractExists) && p_iTransId != 0 && p_iTransSplitRowId != 0
						&& (objXmlElement.Attributes["codeid"].Value.Trim()!= ""))
				    	bFound = true;

					if(iCnt == 0)
					{
                        m_objCommon.AddAlert(Globalization.GetString("Manager.GetFeeSchedule.Alert1", m_iClientId));
						m_objCommon.HandleXMLErrorMessage(ref m_objCommon.g_objXMLDoc, ref objErrors, "BRS.Manager.FillBillingCodeDom");
					}
                    else if (!bFound && !bContractExists && p_iTransId != 0 && p_iTransSplitRowId != 0)
					{
                        m_objCommon.AddAlert(Globalization.GetString("Manager.GetFeeSchedule.Alert2", m_iClientId));
						m_objCommon.HandleXMLErrorMessage(ref m_objCommon.g_objXMLDoc, ref objErrors, "BRS.Manager.FillBillingCodeDom");
					}

					objAppendTarget.AppendChild(objXmlElement);

                    string sSelectedFeeTableId=objXmlElement.Attributes["codeid"].Value.Trim();
					if(sSelectedFeeTableId == "" )
					   objXmlElement.Attributes["codeid"].Value="";

				}//end if(!bContractExists||(bContractExists && iFeeSchedule <= -100)||(bContractExists || iFeeSchedule = 0))
				

                if (bContractExists && iFeeSchedule > -100 && iFeeSchedule != -1)
                // abisht BRS Issue.Changed the condition to make it work with the RMWorld contracts as well.
				{
                    //abisht BRS Issue.Code move to make a combobox if there is no table in the database for the given iFeeSchedule.
                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append(" SELECT USER_TABLE_NAME FROM FEE_TABLES WHERE TABLE_ID = ").Append(iFeeSchedule);
                    objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString, sbSQL.ToString(), m_iClientId);
                    //abisht added an if-else condition so that if the table is not present in the database a default handling is done in the else condition.
                    if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
                    {
                        string sFeeTableName = string.Empty;
                        if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
                            sFeeTableName = objDataSet.Tables[0].Rows[0]["USER_TABLE_NAME"].ToString();

                        //objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feescheddesc");
                        //objXmlElement.InnerText = sFeeTableName;
                        objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feesched");
                        objXmlElement.SetAttribute("codeid", iFeeSchedule.ToString());
                        objXmlElement.InnerXml = string.Format("<option value='{0}'>{1}</option>", iFeeSchedule, sFeeTableName);
                    }
                    else
                    {
                        sSubTitle = "Step 2.  Select a fee table and choose 'Next' to continue.";
                        objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("feesched");
                        //objXmlElement.SetAttribute("type", "combobox");
                        iCnt = FillComboWithCodes("", ref objXmlElement, false, sSQL, iFeeSchedule);

                        if (!(bContractExists) && p_iTransId != 0 && p_iTransSplitRowId != 0
                            && (objXmlElement.Attributes["codeid"].Value.Trim() != ""))
                            bFound = true;

                        if (iCnt == 0)
                        {
                            m_objCommon.AddAlert(Globalization.GetString("Manager.GetFeeSchedule.Alert1", m_iClientId));
                            m_objCommon.HandleXMLErrorMessage(ref m_objCommon.g_objXMLDoc, ref objErrors, "BRS.Manager.FillBillingCodeDom");
                        }
                        else if (!bFound && !bContractExists && p_iTransId != 0 && p_iTransSplitRowId != 0)
                        {
                            m_objCommon.AddAlert(Globalization.GetString("Manager.GetFeeSchedule.Alert2", m_iClientId));
                            m_objCommon.HandleXMLErrorMessage(ref m_objCommon.g_objXMLDoc, ref objErrors, "BRS.Manager.FillBillingCodeDom");
                        }

                        string sSelectedFeeTableId = objXmlElement.Attributes["codeid"].Value.Trim();
                        if (sSelectedFeeTableId == "")
                            objXmlElement.Attributes["codeid"].Value = "";


                        //using feesched combo box to select & display, 
                        //however later we expect this field to hold the chosen table name.
                        //objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feescheddesc");
                    }
				}//end if(bContractExists && iFeeSchedule > -100 && iFeeSchedule != 0)

				//Create id node for feesched2, is always 0 if no contract, 
				//but need existence of node for javascript functions in asp pages
                objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feesched2");
				objXmlElement.InnerText = iFeeSchedule2.ToString();

				if(bContractExists && iFeeSchedule2 > 0)
				{
					//create text label for feesched2 description
					sTemp = m_objCommon.GetSingleString("USER_TABLE_NAME", "FEE_TABLES", "TABLE_ID=" + iFeeSchedule2);

                    objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.SelectSingleNode("//feescheddesc2");
					objXmlElement.SetAttribute("title", "Contract Table 2:");
					objXmlElement.InnerText = sTemp;
				}//end if(bContractExists && iFeeSchedule2 > 0)
                string sSubtitle1 = objXmlElement.SelectSingleNode("//CurrentBillItemIndex").InnerText;
                string sSubtitle2 = objXmlElement.SelectSingleNode("//TotalBillItem").InnerText;
                string sSubtitle3 = "(Item " + sSubtitle1 + " of " + sSubtitle2 + " )";
                objXmlElement = (XmlElement)m_objCommon.g_objXMLDoc.GetElementsByTagName("option").Item(0);
				objXmlElement.SetAttribute("subtitle", sSubTitle + sSubtitle3);

				if(objAlerts.HasChildNodes)
                    m_objCommon.g_objXMLDoc.GetElementsByTagName("option").Item(0).AppendChild(objAlerts);
				if(objErrors.HasChildNodes)
                    m_objCommon.g_objXMLDoc.GetElementsByTagName("option").Item(0).AppendChild(objErrors);

			}//End of Main Try Block
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.GetFeeSchedules.GenericError", m_iClientId), p_objException);
			}
			finally
			{
				sbSQL = null;
				if (objDataSet != null)
					objDataSet.Dispose();

				//objXmlDoc = null;
				objAlerts = null;
				objAppendTarget = null;
				objErrors = null;
				//objTempXmlElement = null;
				objXmlElement = null;
			}
		}// end GetFeeSchedules

		#endregion

		#region private FindContract (p_iEntityID, p_sFromDate)
		/// Name		: FindContract
		/// Author		: Navneet Sota
		/// Date Created: 1/12/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Finds the Contract ID from database based on passed in entity-id
		/// </summary>
		/// <param name="p_iEntityID">Entity ID</param>
		/// <param name="p_sFromDate">From Date</param>
		public bool FindContract(int p_iEntityID, string p_sFromDate)
		{
			bool bReturnValue = false;
			int iProviderContRowId = 0;

			//String Object
			StringBuilder sbSQL = null;
			
			//Database objects
			DataSet objDataSet=null;

			try
			{
				if(p_iEntityID != 0)
				{
					sbSQL = new StringBuilder();
					
					//Table[0]
					sbSQL.Append(" SELECT PRVD_CONT_ROW_ID FROM PROVIDER_CONTRACTS ");
					sbSQL.Append(" WHERE PROVIDER_EID = ").Append(p_iEntityID);
					sbSQL.Append(" AND '").Append(p_sFromDate).Append("' > START_DATE");
					sbSQL.Append(" AND (END_DATE IS NULL OR END_DATE  = '')");
		
					objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);				
					if (objDataSet.Tables[0].Rows.Count == 0)
					{
						sbSQL.Remove(0,sbSQL.Length);
						//Table[0]
						sbSQL.Append(" SELECT PRVD_CONT_ROW_ID FROM PROVIDER_CONTRACTS ");
						sbSQL.Append(" WHERE PROVIDER_EID = ").Append(p_iEntityID);
						sbSQL.Append(" AND '").Append(p_sFromDate);
						sbSQL.Append(" ' BETWEEN START_DATE AND END_DATE");

						objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);				
					}

					if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
					{
						iProviderContRowId = Conversion.ConvertStrToInteger(objDataSet.Tables[0].Rows[0]["PRVD_CONT_ROW_ID"].ToString());	
						if(iProviderContRowId != 0)
						{
							bReturnValue = true;
							m_objProviderContracts = (ProviderContracts)m_objCommon.g_objDataModelFactory.GetDataModelObject("ProviderContracts",false);                        
							m_objProviderContracts.MoveTo(iProviderContRowId);
						}//end if(iProviderContRowId != 0)
					}//end if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				}//end if(p_iEntityID != 0)
			}//end try
            catch (Exception p_objException)
			{
                throw new RMAppException(p_objException.Message, p_objException);
			}
			finally
			{
				if(objDataSet != null)
				{
					objDataSet.Dispose();
					objDataSet = null;
				}
				sbSQL = null;
			}
			return bReturnValue;
		}// end FindContract

		#endregion

		#region private FillComboWithCodes(p_sCodeTable,p_objXMLElement,p_bBlank,p_sSQLOverride,p_iSelected)
		/// Name		: FillComboWithCodes
		/// Author		: Navneet Sota
		/// Date Created: 1/18/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Fills the XML for Combo box values and Id's.
		/// </summary>
		/// <param name="p_sCodeTable">Code-Table</param>
		/// <param name="p_objXMLElement">XML Element to be filled</param>
		/// <param name="p_bBlank">Flag to indicate if a empty option is required in combo or not</param>
		/// <param name="p_sSQLOverride">SQL to be used in filling the combo box</param>
		/// <param name="p_iSelected">Value to be selected by default in combo-box</param>
		private int FillComboWithCodes(string p_sCodeTable, ref XmlElement p_objXMLElement, bool p_bBlank,
			string p_sSQLOverride, int p_iSelected)
		{
			//Return value
			int iReturnValue = 0;
			int iValue = 0;
			int iCnt = 0;

			//String Object
			StringBuilder sbSQL = null;

			//XML Object variables
			XmlElement objXmlElement;
			
			//Database objects
			DataSet objDataSet=null;
			try
			{
				m_objCommon.RemoveChildNodes(ref p_objXMLElement);
				p_objXMLElement.SetAttribute("codeid","");
                
				sbSQL = new StringBuilder();

				if(p_sSQLOverride != "")
					sbSQL.Append(p_sSQLOverride);
				else
				{
					p_objXMLElement.SetAttribute("codetable", p_sCodeTable);
					switch(p_sCodeTable.ToUpper())
					{
					    
						case "STATES":
							sbSQL.Append(" SELECT STATES.STATE_ROW_ID  ID,");
							
							if(DbFactory.IsOracleDatabase(m_objCommon.g_sConnectionString))
									{
										sbSQL.Append(" STATES.STATE_ID || ' - ' || STATES.STATE_NAME  DESCRIPTION");
									}
									else
									{
										sbSQL.Append(" STATES.STATE_ID + ' - ' + STATES.STATE_NAME  DESCRIPTION");
							}
							
							sbSQL.Append(" FROM STATES WHERE STATE_ROW_ID > 0 ORDER BY STATES.STATE_ID ");
							break;
						default:
							 sbSQL.Append(" SELECT CODES.CODE_ID   ID,");
							
                            if (DbFactory.IsOracleDatabase(m_objCommon.g_sConnectionString))
								{
									sbSQL.Append(" CODES.SHORT_CODE  || ' - ' ||  CODES_TEXT.CODE_DESC  DESCRIPTION");
								}
								else
								{
									sbSQL.Append(" CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC  DESCRIPTION");
							}
							
							sbSQL.Append(" FROM CODES, CODES_TEXT, GLOSSARY ");
							sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID ");
							sbSQL.Append(" AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
							sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = '").Append(p_sCodeTable).Append("'");
							sbSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL )");
							break;
					}//end switch
				}//end if(p_sSQLOverride != "")

				if(p_bBlank)
				{
					//append a blank option with value 0
					objXmlElement = m_objCommon.g_objXMLDoc.CreateElement("option");
					objXmlElement.SetAttribute("value", "0");
					p_objXMLElement.AppendChild(objXmlElement);
				}//end if(p_bBlank)

				objDataSet = DbFactory.GetDataSet(m_objCommon.g_sConnectionString,sbSQL.ToString(), m_iClientId);
				if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
				{
					foreach(DataRow objRow in objDataSet.Tables[0].Rows)
					{
						objXmlElement = m_objCommon.g_objXMLDoc.CreateElement("option");
						iValue = Conversion.ConvertStrToInteger(objRow["ID"].ToString());
						objXmlElement.SetAttribute("value", iValue.ToString());

						if(iValue == p_iSelected)
						{
							//objXmlElement.SetAttribute("selected", "1");
							objXmlElement.SetAttribute("codeid",p_iSelected.ToString());
						}
						objXmlElement.InnerText = objRow["DESCRIPTION"].ToString();

						p_objXMLElement.AppendChild(objXmlElement);
						iCnt++;
					}//end for

					iReturnValue = iCnt;
				}//end if (objDataSet.Tables[0] != null)

				//Combo field values at the top control level.
				p_objXMLElement.SetAttribute("codeid", p_iSelected.ToString());
			}//End of Main Try Block
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.FillComboWithCodes.GenericError", m_iClientId), p_objException);
			}
			finally
			{
				sbSQL = null;
				if (objDataSet != null)
					objDataSet.Dispose();
				objXmlElement = null;
			}

			//Return the value to the calling function
			return iReturnValue;
		}// end FillComboWithCodes
		#endregion
		
		#region private FillComboWithCodes(p_sCodeTable,p_objXMLElement,p_bBlank)
		/// Name		: FillComboWithCodes
		/// Author		: Navneet Sota
		/// Date Created: 1/18/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Overloaded function
		/// </summary>
		/// <param name="p_sCodeTable">Code-Table</param>
		/// <param name="p_objXMLElement">XML Element to be filled</param>
		/// <param name="p_bBlank">Flag to indicate if a empty option is required in combo or not</param>
		private int FillComboWithCodes(string p_sCodeTable, ref XmlElement p_objXMLElement, bool p_bBlank)
		{
			return FillComboWithCodes(p_sCodeTable,ref p_objXMLElement,p_bBlank,"",0);
		}// end FillComboWithCodes

		#endregion

		#region private GetPercentiles(ref p_objXMLElem, p_iPercentile, p_iSelected)
		/// Name		: GetPercentiles
		/// Author		: Navneet Sota
		/// Date Created: 1/27/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Get all available percentiles
		/// </summary>
		/// <param name="p_objXMLElem">The passed in XML</param>
		/// <param name="p_iPercentile">Type of percentile to be added</param>
		/// <param name="p_iSelected">Percentile to be selected</param>
		private void GetPercentiles(ref XmlElement p_objXMLElem,int p_iPercentile, int p_iSelected)
		{
			m_objCommon.RemoveChildNodes(ref p_objXMLElem);
			p_objXMLElem.SetAttribute("codeid","");
			switch(p_iPercentile)
			{
				case 1: //Standard
					m_objCommon.AddOption(ref p_objXMLElem, 50, "50th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 60, "60th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 70, "70th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 75, "75th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 80, "80th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 85, "85th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 90, "90th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 95, "95th Percentile", p_iSelected);
					break;
				case 2:  //Expanded
					m_objCommon.AddOption(ref p_objXMLElem, 25, "25th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 30, "30th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 40, "40th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 50, "50th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 60, "60th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 70, "70th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 80, "80th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 90, "90th Percentile", p_iSelected);
					break; 
				case 3:  //Combined
					m_objCommon.AddOption(ref p_objXMLElem, 25, "25th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 30, "30th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 40, "40th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 50, "50th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 60, "60th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 70, "70th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 75, "75th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 80, "80th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 85, "85th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 90, "90th Percentile", p_iSelected);
					m_objCommon.AddOption(ref p_objXMLElem, 95, "95th Percentile", p_iSelected);
					break;
			}//end switch
			
		}// end FillComboWithCodes


		private void CleanXml(XmlDocument p_objXml)
		{
			XmlNodeList objNodLst = p_objXml.SelectNodes("//displaycolumn");
			foreach(XmlNode objNod in objNodLst)
			{
				if(objNod.Attributes.Count==0)
                    if(!objNod.HasChildNodes)
						objNod.ParentNode.RemoveChild(objNod);  
			} 

			objNodLst = p_objXml.SelectNodes("//group");
			foreach(XmlNode objNod in objNodLst)
			{
				if(!objNod.HasChildNodes)
					objNod.ParentNode.RemoveChild(objNod);  
			} 
		}

		#endregion

        #region private string GetPhysicianLicenseNum(int iPhyEid)
        /// <summary>
        /// Get the physician license number 
        /// </summary>
        /// <param name="iPhyEid"></param>
        private string GetPhysicianLicenseNum(int iPhyEid)
        {
            string PhyLicenseNumber = "";
            string sSQL = "";
            DbReader objDbReader = null;
            try
            {
                sSQL = "SELECT LIC_NUM FROM PHYSICIAN WHERE PHYS_EID=" + iPhyEid;
                objDbReader = DbFactory.GetDbReader(m_objCommon.g_sConnectionString, sSQL);
                if (objDbReader.Read())
                    PhyLicenseNumber = objDbReader.GetString("LIC_NUM");
                return PhyLicenseNumber;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(p_objException.Message, p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
            }
        }
        # endregion

        # endregion

		#region IDisposable Members

        /// <summary>
        /// Release resources
        /// </summary>
		public void Dispose()
		{
            if (m_objFunds != null)
            {
                m_objFunds.Dispose();
            }

			if(m_objBillItem!=null)
			{
				m_objBillItem.Dispose();
				m_objBillItem = null ;
			}
			if(!m_bInvokedThroughFunds)
			{
				if(this.DataModelFactory!=null)
				{
					this.DataModelFactory.Dispose();
					this.DataModelFactory=null;
				}
			
				if(m_objCommon!=null)
				{
					if(m_objCommon.g_objDataModelFactory!=null)
					{
						m_objCommon.g_objDataModelFactory.UnInitialize();
						m_objCommon.g_objDataModelFactory=null;
					}
				}
				if(m_objRequest!=null)
				{
					m_objRequest.Dispose();
					m_objRequest=null;
				}
			}
		}

		#endregion
	}// End class
} // End namespace
