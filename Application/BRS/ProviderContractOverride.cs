using System;

using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{
	/**************************************************************
	 * $File		: ProviderContractOverride.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/03/2004
	 * $Author		: Navneet Sota
	 * $Comment		: This class represents collection of certain variables which are filled using 
	 *				  some of the defined helper methods.
	 * $Source		: Riskmaster.Application.BRS
	**************************************************************/
	internal class ProviderContractOverride
	{
	
		#region Variable Declarations	

		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon = null;

		/// <summary>
		/// Represents the Billed Amount
		/// </summary>
		internal double m_dBilledAmt = 0.0;

		/// <summary>
		/// Represents the Billed Amount With Discount
		/// </summary>
		internal double m_dBilledAmtWithDisc = 0.0;

		/// <summary>
		/// Represents the Per Diem Amount
		/// </summary>
		internal double m_dPerDiemAmt = 0.0;
		
		/// <summary>
		/// Represents the Per Diem Amount With Stop Loss
		/// </summary>
		internal double m_dPerDiemAmtWithStopLoss = 0.0;
		
		/// <summary>
		/// Represents the Contract Amount
		/// </summary>
		internal double m_dContractAmt  = 0.0;
		
		/// <summary>
		/// Represents the Contract Amount With Discount
		/// </summary>
		internal double m_dContractAmtWithDisc = 0.0;
		
		/// <summary>
		/// Represents the Sched Amount
		/// </summary>
		internal double m_dSchedAmt = 0.0;
		
		/// <summary>
		/// Represents the Scheduled Amount With Discount
		/// </summary>
		internal double m_dSchedAmtWithDisc = 0.0;
		
		/// <summary>
		/// Represents the Scheduled 2 Amount
		/// </summary>
		internal double m_dSched2Amt = 0.0;
		
		/// <summary>
		/// Represents the Scheduled 2 Amount With Discount
		/// </summary>
		internal double m_dSched2AmtWithDisc = 0.0;
		
		/// <summary>
		/// Represents the Override Amount
		/// </summary>
		internal double m_dOverrideAmount = 0.0;

		/// <summary>
		/// Represents the enum value of BillOverrideType
		/// </summary>
		private Common.BillOverrideType m_enumBillOverrideType;
		
		/// <summary>
		/// Holds the provide contract object.
		/// </summary>
		private ProviderContracts m_objParent;
		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}
        private int m_iClientId = 0;//rkaur27
		# endregion
		
		#region Constructor
		/// Name		: ProviderContractOverride
		/// Author		: Navneet Sota
		/// Date Created: 01/03/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
		internal ProviderContractOverride(Common p_objCommon, int p_iClientId)
		{
			m_objCommon = p_objCommon;
			this.OverrideOption=Common.BillOverrideType.APPLY_UNKNOWN;
            m_iClientId = p_iClientId;//rkaur27
		}
		
		#endregion

		#region Properties

		/// <summary>
		/// Internal property for Bill Override Option
		/// </summary>
		internal Common.BillOverrideType OverrideOption
		{
			set
			{
				m_enumBillOverrideType = value;
			}
			get
			{
				return m_enumBillOverrideType;
			}	
		}

		/// <summary>
		/// Internal property for Default Override Option
		/// </summary>
		internal int DefaultOverrideOption
		{
			get
			{
				double dCurMin = 0.0;
				int iReturnValue = 0;
				dCurMin = this.DefaultOverrideAmount;

				if(dCurMin == this.m_dBilledAmt)
					iReturnValue = (int)Common.BillOverrideType.DO_NOT_OVERRIDE;
				if (dCurMin == this.m_dBilledAmtWithDisc) 
					iReturnValue = (int)Common.BillOverrideType.APPLY_DISC_ON_SCHD;
				if (dCurMin == this.m_dContractAmt) 
					iReturnValue = (int)Common.BillOverrideType.APPLY_CONTRACT_AMOUNT;
				if (dCurMin == this.m_dContractAmtWithDisc) 
					iReturnValue = (int)Common.BillOverrideType.APPLY_DISC_ON_CONTRACT;
				if (dCurMin == this.MinAmt(this.m_dSchedAmt, this.m_dSched2Amt)) 
					iReturnValue = (int)Common.BillOverrideType.APPLY_ENT_FEE_SCHD;
				if (dCurMin == this.MinAmt(this.m_dSchedAmtWithDisc, this.m_dSched2AmtWithDisc)) 
					iReturnValue = (int)Common.BillOverrideType.APPLY_DISC_ON_ENT_FEE;
				if (dCurMin == this.m_dPerDiemAmt) 
					iReturnValue = (int)Common.BillOverrideType.APPLY_PER_DIEM;
				if (dCurMin == this.m_dPerDiemAmtWithStopLoss) 
					iReturnValue = (int)Common.BillOverrideType.APPLY_PER_DIEM_STOP_LOSS;

				return iReturnValue;
			}	
		}

		/// <summary>
		/// Internal property for Default Override Amount
		/// </summary>
		internal double DefaultOverrideAmount
		{
			get
			{
				double dCurMin = 0.0;
				dCurMin  = this.m_dBilledAmt;

				dCurMin  = MinAmt(dCurMin , this.m_dBilledAmt);
				dCurMin  = MinAmt(dCurMin , this.m_dBilledAmtWithDisc);
				dCurMin  = MinAmt(dCurMin , this.m_dContractAmt);
				dCurMin  = MinAmt(dCurMin , this.m_dContractAmtWithDisc);
				dCurMin  = MinAmt(dCurMin , MinAmt(this.m_dSchedAmt, this.m_dSched2Amt));
				dCurMin  = MinAmt(dCurMin , MinAmt(this.m_dSchedAmtWithDisc, this.m_dSched2AmtWithDisc));
				dCurMin  = MinAmt(dCurMin , this.m_dPerDiemAmt);
				dCurMin  = MinAmt(dCurMin , this.m_dPerDiemAmtWithStopLoss);

				return dCurMin;
			}	
		}

		/// <summary>
		/// Internal property for Override EOB Code
		/// </summary>
		internal string OverrideEOBCode
		{
			get
			{
				string sReturnValue = "";
				switch (this.OverrideOption)
				{
					case Common.BillOverrideType.DO_NOT_OVERRIDE:
						sReturnValue = "ON-AB";
						break;
			        case Common.BillOverrideType.APPLY_DISC_ON_SCHD:
						sReturnValue = "O-DAB";
						break;
					case Common.BillOverrideType.APPLY_CONTRACT_AMOUNT:
						sReturnValue = "O-CA";
						break;
					case Common.BillOverrideType.APPLY_DISC_ON_CONTRACT:
						sReturnValue = "O-DCA";
						break;
					case Common.BillOverrideType.APPLY_PER_DIEM:
						sReturnValue = "O-PDR";
						break;
					case Common.BillOverrideType.APPLY_PER_DIEM_STOP_LOSS:
						if (this.m_dPerDiemAmt == this.m_dPerDiemAmtWithStopLoss)
							sReturnValue = "O-SL";
						else
							sReturnValue = "O-PDR";
						break;
					case Common.BillOverrideType.APPLY_ENT_FEE_SCHD:
						sReturnValue = "O-ES";
						break;
			        case Common.BillOverrideType.APPLY_DISC_ON_ENT_FEE:
						sReturnValue = "O-DES";
						break;
				}//end Switch
				
				return sReturnValue;

			}//get	
		}//Property OverrideEOBCode

		internal ProviderContracts Parent
		{
			get
			{
                return  m_objParent;
			}
			set
			{
				m_objParent=value;
			}
		}
		#endregion
	
		#region internal Methods

		#region internal Populate(p_objRequest)
		/// Name			: Populate
		/// Author			: Navneet Sota
		/// Date Created	: 1/3/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Populates the ProviderContractOverride variables
		/// </summary>
		/// <param name="p_objRequest">The object for Request class</param>
		internal void Populate(Request p_objRequest)
		{
			//double variable
			double dAmtBilled = 0.0;
			
			//objects
			ProviderContracts objProviderContracts = null;
			FeeSchedule objFeeSchedule = null;

            try
            {
                dAmtBilled = p_objRequest.BillItemObject.AmountBilled;

                //	objProviderContracts = (ProviderContracts)m_objCommon.g_objDataModelFactory.GetDataModelObject("ProviderContracts",false);
                objProviderContracts = p_objRequest.ProviderContract;
                if (dAmtBilled != 0 && objProviderContracts != null)
                {
                    //Instantiate and load the objects with correponding values
                    //objProviderContracts.MoveFirst();

                    objFeeSchedule = new FeeSchedule(m_objCommon, m_iClientId);
                    //objFeeSchedule.MoveFirst();

                    //Get First FeeSchedule Amount from Contract if any.
                    if (objProviderContracts.FeeTableId > 0)
                    {
                        objFeeSchedule.MoveToRecord(objProviderContracts.FeeTableId);
                        //Ask Fee Schedule for an Amount value.
                        m_dSchedAmt = objFeeSchedule.GetScheduledAmount(p_objRequest);
                    }//end if(objProviderContracts.FeeTableId > 0)
                    else if (objProviderContracts.FeeTableId <= -100)
                    {
                        //tkr MITS 9978 when auto select on state contracts the fee table id is -1 * (StateId + 100).
                        //the real fee table id in this case is the BillItemObject.EntFeeSchdId
                        objFeeSchedule.MoveToRecord(p_objRequest.BillItemObject.EntFeeSchdId);
                        m_dSchedAmt = objFeeSchedule.GetScheduledAmount(p_objRequest);
                    }
                    if (objProviderContracts.FeeTableId2nd > 0)
                    {
                        objFeeSchedule.MoveToRecord(objProviderContracts.FeeTableId2nd);
                        //Ask Fee Schedule for an Amount value.
                        m_dSched2Amt = objFeeSchedule.GetScheduledAmount(p_objRequest);
                    }//end if(objProviderContracts.FeeTableId > 0)
                    else if (objProviderContracts.FeeTableId2nd <= -100)
                    {
                        //tkr MITS 9978 when auto select on state contracts the fee table id is -1 * (StateId + 100).
                        //the real fee table id in this case is the BillItemObject.EntFeeSchdId
                        objFeeSchedule.MoveToRecord(p_objRequest.BillItemObject.EntFeeSchdId2);
                        m_dSched2Amt = objFeeSchedule.GetScheduledAmount(p_objRequest);
                    }
                    if (m_objCommon.g_hstErrors.Count <= 0 && m_objCommon.g_hstMissingParams.Count <= 0)
                    {
                        m_dBilledAmt = dAmtBilled;
                        m_dBilledAmtWithDisc = dAmtBilled * (1 - objProviderContracts.DiscountBill / 100);
                        m_dContractAmt = objProviderContracts.Amount;
                        m_dContractAmtWithDisc = objProviderContracts.Amount * (1 - objProviderContracts.DiscountBill / 100);
                        m_dPerDiemAmt = GetPerDiem(p_objRequest.BillItemObject.UnitsBilled, objProviderContracts);

                        if (m_dPerDiemAmt > m_dBilledAmtWithDisc)
                            m_dPerDiemAmtWithStopLoss = m_dPerDiemAmt;
                        else
                            m_dPerDiemAmtWithStopLoss = m_dBilledAmtWithDisc;

                        m_dSchedAmtWithDisc = m_dSchedAmt * (1 - objProviderContracts.Discount / 100);
                        m_dSched2AmtWithDisc = m_dSched2Amt * (1 - objProviderContracts.Discount2nd / 100);
                    }//end if(Common.g_hstErrors.Count <= 0 && Common.g_hstMissingParams.Count <= 0)
                }//end if(dAmtBilled != 0 && objProviderContracts != null)
            }
            catch (RMAppException p_objAppException)
            {
                throw p_objAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProviderContractOverride.Populate.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                objProviderContracts = null;
                objFeeSchedule = null;
            }
		}
		#endregion

		#region internal MinAmt(p_dAmount1, p_dAmount2)
		/// Name			: MinAmt
		/// Author			: Navneet Sota
		/// Date Created	: 1/3/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This function picks minimum between fee schedule results omitting zero or negative amts.
		/// </summary>
		/// <remarks>		
		/// </remarks>
		/// <param name="p_dAmountFirst">The first Value</param>
		/// <param name="p_dAmountSecond">The Second Value</param>
		internal double MinAmt(double p_dAmountFirst, double p_dAmountSecond)
		{
			//return value
			double dReturnValue = 0.0;

			if(p_dAmountFirst <= 0 && p_dAmountSecond <= 0)
				dReturnValue = 0.0;
			else if(p_dAmountFirst <= 0)
				dReturnValue = p_dAmountSecond;
			else if(p_dAmountSecond <= 0)
				dReturnValue = p_dAmountFirst;
			else if(p_dAmountFirst <= p_dAmountSecond)
				dReturnValue = p_dAmountFirst;
			else
				dReturnValue = p_dAmountSecond;

			return dReturnValue;
		}
		#endregion

		#endregion

		#region Private Methods

		#region private GetPerDiem(p_dTotalDays, p_objProviderContracts)
		/// Name			: GetPerDiem
		/// Author			: Navneet Sota
		/// Date Created	: 1/3/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Calculates the PerDiem amount
		/// </summary>
		/// <param name="p_dTotalDays">The Total no of days</param>
		/// <param name="p_objProviderContracts">Provider Contracts Object</param>
		private double GetPerDiem(double p_dTotalDays, ProviderContracts p_objProviderContracts)
		{
			//double variables
			double dReturnValue = 0.0;
			double dRate = 0.0;
			double dCurrentDay = 0.0;

			//Simplistic PerDiem Calculation
			for(dCurrentDay = 0; dCurrentDay < p_dTotalDays; dCurrentDay++)
			{
				if(dCurrentDay >= p_objProviderContracts.RateChg2nd)
					dRate = p_objProviderContracts.LastRate;
				else if(dCurrentDay >= p_objProviderContracts.RateChg)
					dRate = p_objProviderContracts.SecondRate;
				else
					dRate = p_objProviderContracts.FirstRate;

				dReturnValue = dReturnValue + dRate;
			}//end for

			//Return the result to calling function
			return dReturnValue;

		}//End GetPerDiem

		#endregion

		#endregion

	}//End Class
}//End Namespace
