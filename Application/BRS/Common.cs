﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.BRS
{
    /// <summary>
    /// This class contains various common methods that are needed by different classes
    /// </summary>
	public class Common
	{
		#region Constant Declaration

		//Integer Constants
		internal const int APPLY_UNKNOWN = 1; 
		internal const int PERCENT = 1   ;	//Reduction Type
		internal const int UCR94 = 0;			//Ingenix/Medicode Table Type (CALC_TYPE)
		internal const int UCR95 = 1;

		internal const int OPT = 3;
		internal const int HCPCS = 4;
		internal const int ANES = 7;
		internal const int DENT = 18;    //caggarwal4 Merged for issue RMA-9115
		internal const int BASIC = 12;		//"Basic User Entered Schedule" Millennium custom dcm
		internal const int WC00 = 13;			//"Workers' Compensation 2000" ingenix/Medicode Table Type dcm
		internal const int FREEPHARM = 15;	//"Free Entry Pharmacy" To allow Pharmacy Fee Entry without a Table.
		internal const int FREEHOSP = 16;		//"Free Entry Hospital" To allow Hospital Fee Entry without a Table.
		internal const int FREEGENERIC = 17;	//"Generic Free Entry" To allow Fee Entry without a Table. 
        internal const int FLHOSP = 20;        //"Florida Only Hosspital"  BRS FL Merge   : Umesh
 	    
		//error handling
		internal const string ERR_ERRORS = "errors";
		internal const string ERR_ERROR = "error";
		internal const string ERR_NUMBER = "number";
		internal const string ERR_SOURCE = "source";
		internal const string ALERTS = "alerts";

		//private constants
		private const string CODE_FLDTYPE = "code" ;
		private const string CODEDETAIL_FLDTYPE = "codewithdetail" ;
		private const string COMBOBOX_FLDTYPE = "combobox" ;
		private const string EIDLOOKUP_FLDTYPE = "eidlookup" ;
		private const string ATT_CODEID = "codeid" ;
		private const string CURRENCY_FLDTYPE = "currency";
        private const string PHYLOOKUP_FLDTYPE = "physicianlookup";

		//Type of fields
		internal const string CODELIST_FLDTYPE = "codelist";
		internal const string ENTITYLIST_FLDTYPE = "entitylist";
		internal const string ATT_NAME = "name";
		internal const string RADIO_FLDTYPE = "radio";
		internal const string TEXT_CONTROL = "text";
		internal const string BOOL_FLDTYPE = "checkbox";
		internal const string DATE_FLDTYPE = "date";
		internal const string INVOICE_DETAIL = "invDetail";
		internal const string RATE_TABLE = "ratetable";
		internal const string TEXTANDLIST = "textandlist";
		internal const string MEMO_FLDTYPE = "memo";
		internal const string TEXTML_FLDTYPE = "textml";
		internal const string ATT_ID = "id";
		internal const string SSN_FLDTYPE = "ssn";
		internal const string ZIP_FLDTYPE = "zip";
		internal const string TAXID_FLDTYPE = "taxid";
		internal const string ID_CONTROL = "id";
		internal const string TEXT_LABEL = "textlable";
		internal const string PHONE_FLDTYPE = "phone";
		internal const string TIME_FLDTYPE = "time";
		internal const string DATEBSCRIPT_FLDTYPE = "datebuttonscript";

		/// <summary>
		/// DSN value for Access Database DSN
		/// </summary>
		internal const string DSNFORACCESS = "DsnForAccess";
		internal const int MAXRECORDS = 500;

		#endregion

		#region Variable Declarations	
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		internal string g_sConnectionString;

		/// <summary>
		/// Represents the ODBC Name for Access Database, used for fee tables
		/// </summary>
		internal string g_sODBCName = "";

		
		/// <summary>
		/// Represents the global XML Document to be used across component.
		/// </summary>
		internal XmlDocument g_objXMLDoc;
		
		/// <summary>
		/// Represents the global variable to be used for DSN name.
		/// </summary>
		internal string g_sDsnName;

		/// <summary>
		/// Represents the global variable to be used for User name.
		/// </summary>
		internal string g_sUserName ;

		/// <summary>
		/// Represents the global variable to be used for Password.
		/// </summary>
		internal string  g_sPassword ;

		/// <summary>
		/// Represents the global variable to be used for DataModelFactory object.
		/// </summary>
		internal DataModelFactory g_objDataModelFactory  ;		
		
		/// <summary>
		/// Represents the module variable to be used for accumulating alerts.
		/// </summary>
		internal Hashtable g_hstAlerts = new Hashtable();

		/// <summary>
		/// Represents the collection of Miissing Params.
		/// </summary>
		internal Hashtable g_hstMissingParams = null;

		/// <summary>
		/// Represents the collection of Errors.
		/// </summary>
		internal Hashtable g_hstErrors = null;

		/// <summary>
		/// Represents the Module variable for Common.LocalCache Class.
		/// </summary>
		private LocalCache m_objLocalCache = null;

        /// <summary>
        /// list of nodes to be hidden in the screen
        /// </summary>
        internal List<string> m_KilledNodeList = new List<string>();

        internal Dictionary<string, string> m_SectionNodes = null;

		/// <summary>
		/// Tanuj->Should contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

        /// <summary>
        /// BusinessAdaptorErrors
        /// </summary>
		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
                m_objErrors=value;
			}
		}
			
        /// <summary>
        /// LocalCache used for code lookup
        /// </summary>
		public LocalCache LocalCache
		{
			get
			{
				if(m_objLocalCache==null)
					return new LocalCache(g_sConnectionString,m_iClientId);
				    return m_objLocalCache;
			}
		}

		#endregion
        private int m_iClientId = 0;//rkaur27
		#region Enumerations

		#region UnitsBilledType enum

		/// <summary>
		/// Enum containing Units Billed Types.
		/// </summary>
		internal enum UnitsBilledType:int
		{
			ubtUnits = 1,
			ubtMINUTES = 2,
			ubtOther = 3
		};

		#endregion

		#region eSteps Enum

		/// <summary>
		/// Enum containing eSteps.
		/// </summary>
		public enum eSteps:int
		{
			Preload = -1,
			Dates = 0,
			FeeSchedule = 1,
			LoadBRSXML = 2,
			Calculate = 3,
			Specialty = 4,
			ProcedureCase = 5,
			ICCPT = 6,
			Override = 7,
			ManualReduction = 8,
            AddNextBillItem = 9,
            PreviousBillItem = 10,
		};

		#endregion

		
		#region BillOverrideType Enum
		/// <summary>
		/// Enum containing Bill Override Types.
		/// </summary>
		internal enum BillOverrideType:int
		{
			APPLY_UNKNOWN = -1,
			DO_NOT_OVERRIDE = 0,
			APPLY_DISC_ON_SCHD = 1,
			APPLY_CONTRACT_AMOUNT = 2,
			APPLY_DISC_ON_CONTRACT = 3,
			APPLY_PER_DIEM = 4,
			APPLY_PER_DIEM_STOP_LOSS = 5,
			APPLY_ENT_FEE_SCHD = 6,
			APPLY_DISC_ON_ENT_FEE = 7,
		};

		#endregion

		#endregion

		#region Constructor
		/// Name		: Common
		/// Author		: Navneet Sota
		/// Date Created: 1/11/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		public Common(int p_iClientId)
		{
            m_iClientId = p_iClientId;//rkaur27
        }
		
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sUser">login user name</param>
        /// <param name="p_sPwd">login user password</param>
        /// <param name="p_sDsn">data source name</param>
		public Common(string p_sUser, string p_sPwd, string p_sDsn, int p_iClientId)
		{
			g_sUserName = p_sUser;
			g_sPassword = p_sPwd;
			g_sDsnName = p_sDsn;
            m_iClientId = p_iClientId;//rkaur27
		}

        /// <summary>
        /// Create the dictionary to store section to child nodes
        /// </summary>
        internal string GetSectionNodeList(string sSectionName)
        {
            string sNodeList = sSectionName;

            try
            {
                //If 1st time calling this function, try to initialize m_SectionNodes dictionary.
                if (m_SectionNodes == null)
                {
                    m_SectionNodes = new Dictionary<string, string>();
                    string sFilePath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"BRS\instance.xml");
                    if (!string.IsNullOrEmpty(sFilePath))
                    {
                        int index = sFilePath.LastIndexOf("\\");
                        if (index > 0)
                            sFilePath = sFilePath.Substring(0, index+1);
                        sFilePath += "SectionNodes.xml";
                        XElement oDocElement = XElement.Load(sFilePath);
                        IEnumerable<XElement> oSections = oDocElement.Elements("section");
                        foreach (XElement oSectionElement in oSections)
                        {
                            string sSectionNodes = string.Empty;
                            foreach (XElement oControlElement in oSectionElement.Nodes())
                            {
                                if (!string.IsNullOrEmpty(sSectionNodes))
                                    sSectionNodes += "|";
                                sSectionNodes += oControlElement.Attribute("name").Value;
                            }
                            m_SectionNodes.Add(oSectionElement.Attribute("name").Value, sSectionNodes);

                            //For readonly resubmit nodes
                            if (oSectionElement.Attribute("name").Value == "readonly")
                            {
                                IEnumerable<XElement> oResubmitReadonlyNodes = oSectionElement.Elements("control").Where(e => (e.Attribute("resubmit") == null));
                                sSectionNodes = string.Empty;
                                foreach (XElement oControlElement in oResubmitReadonlyNodes)
                                {
                                    if (!string.IsNullOrEmpty(sSectionNodes))
                                        sSectionNodes += "|";
                                    sSectionNodes += oControlElement.Attribute("name").Value;
                                }
                                m_SectionNodes.Add("resubmitreadonly", sSectionNodes);
                            }
                        }

                        //Add a viewonly-resubmit node
                        IEnumerable<XElement> oResubmitSection = oDocElement.Elements("section");
                    }
                }

                if (m_SectionNodes.ContainsKey(sSectionName))
                    sNodeList = m_SectionNodes[sSectionName];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return sNodeList;
        }

		#endregion

		#region internal Functions

		#region Val(p_sInput)
		/// Name		: Val
		/// Author		: Navneet Sota
		/// Date Created: 01/19/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Converts the input string to integer value, by scrapping all the sunsequent string characters.
		/// If the first character is non-numeric then -1 is returned.
		/// </summary>
		/// <param name="p_sInput">Field Name</param>
		internal int Val(string p_sInput)
		{
			int iReturnValue = 0;
			int iCnt = 0;

			string sTemp = "";

			if(p_sInput == null || p_sInput == "")
				return 0;

			for(iCnt=0;iCnt<p_sInput.Length;iCnt++)
			{
				if(Conversion.IsNumeric(p_sInput.Substring(iCnt,1)))
					sTemp = sTemp + p_sInput.Substring(iCnt,1);
				else
				{
					//If the first character is non-numeric then -1 is returned.
					if(iCnt == 0)
                        sTemp = "0";
					//exit for
					break;
				}
			}//end for
			iReturnValue = Conversion.ConvertStrToInteger(sTemp);

			return iReturnValue;
		}//end function
		#endregion

		#region internal HandleSubCode(p_objRequest, p_sSubCode)
		/// Name		: HandleSubCode
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>	
		/// Adds the alert message to the global alerts collection.	
		/// </summary>
		/// <param name="p_objRequest"></param>
		/// <param name="p_sSubCode"></param>
		internal void HandleSubCode(Request p_objRequest, string p_sSubCode)
		{
			string sMsg = "";			
			string sTemp = "";

			if(p_sSubCode != "")
			{
				sTemp = p_sSubCode.Trim();
				if(p_sSubCode.Length>=2)
				sTemp = p_sSubCode.Substring(0,2).ToUpper();
				else
                sTemp = p_sSubCode.Substring(0,1).ToUpper();
				switch (sTemp)
				{
					case "A":
                        sMsg = Globalization.GetString("Common.HandleSubCode.A", m_iClientId);
						break;
					case "BR":
                        sMsg = Globalization.GetString("Common.HandleSubCode.BR", m_iClientId);
						break;
					case "D":
                        sMsg = Globalization.GetString("Common.HandleSubCode.D", m_iClientId);
						break;
					case "GA":
                        sMsg = Globalization.GetString("Common.HandleSubCode.GA", m_iClientId);
						break;
					case "I":
                        sMsg = Globalization.GetString("Common.HandleSubCode.I", m_iClientId);
						break;
					case "NA":
                        sMsg = Globalization.GetString("Common.HandleSubCode.NA", m_iClientId);
						break;
					case "NF":
                        sMsg = Globalization.GetString("Common.HandleSubCode.NF", m_iClientId);
						break;
					case "NR":
                        sMsg = Globalization.GetString("Common.HandleSubCode.NR", m_iClientId);
						break;
					case "PO":
                        sMsg = Globalization.GetString("Common.HandleSubCode.PO", m_iClientId);
						break;
					case "PS":
                        sMsg = Globalization.GetString("Common.HandleSubCode.PS", m_iClientId);
						break;
					case "R":
                        sMsg = Globalization.GetString("Common.HandleSubCode.R", m_iClientId);
						break;
					case "RV":
                        sMsg = Globalization.GetString("Common.HandleSubCode.RV", m_iClientId);
						break;
					case "AM":
                        sMsg = Globalization.GetString("Common.HandleSubCode.AM", m_iClientId);
						break;
					case "NT":
                        sMsg = Globalization.GetString("Common.HandleSubCode.NT", m_iClientId);
						break;
					case "SC":
                        sMsg = Globalization.GetString("Common.HandleSubCode.SC", m_iClientId);
						break;
					case "DS":
                        sMsg = Globalization.GetString("Common.HandleSubCode.DS", m_iClientId);
						break;
					case "VQ":
                        sMsg = Globalization.GetString("Common.HandleSubCode.VQ", m_iClientId);
						break;
					case "RNE":
                        sMsg = Globalization.GetString("Common.HandleSubCode.RNE", m_iClientId);
						break;
					case "M":
						if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum == 1)
                            sMsg = Globalization.GetString("Common.HandleSubCode.M", m_iClientId);
						break;
					case "H":
						if(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum == 1)
                            sMsg = Globalization.GetString("Common.HandleSubCode.H", m_iClientId);
						break;
				}//end switch

				if(sMsg != "")
					AddAlert(sMsg);
			}//end if(p_sSubCode != "")
		}//End HandleSubCode

		#endregion

		#region internal GetModifierValue(p_iFeeSchedule, p_iModifierCode)
		/// Name		: GetModifierValue
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Gets the Modifier value from database based on passed in values
		/// </summary>
		/// <param name="p_iFeeSchedule"> Fee Schedule</param>
		/// <param name="p_iModifierCode">Modifier Code</param>		
		internal double GetModifierValue(int p_iFeeSchedule, int p_iModifierCode)
		{
			double dReturnValue = 1;
			StringBuilder sbSQL = null;

			//Database objects
			DataSet objDataSet=null;

            try
            {
                if (p_iModifierCode != 0)
                {
                    sbSQL = new StringBuilder();

                    sbSQL.Append(" SELECT MODIFIER_VALUE FROM BRS_MOD_VALUES WHERE ");
                    sbSQL.Append(" FEETABLE_ID = " + p_iFeeSchedule);
                    sbSQL.Append(" AND MODIFIER_CODE = " + p_iModifierCode);

                    objDataSet = DbFactory.GetDataSet(g_sConnectionString, sbSQL.ToString(), m_iClientId);
                    if (objDataSet.Tables[0] != null && objDataSet.Tables[0].Rows.Count > 0)
                    {
                        dReturnValue = Conversion.ConvertStrToDouble(objDataSet.Tables[0].Rows[0]["MODIFIER_VALUE"].ToString());
                        dReturnValue = dReturnValue / 100;
                    }
                }//end if(p_iModifierCode != 0)
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(p_objException.Message, p_objException);
            }
            finally
            {
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
            }

			return dReturnValue;
		}//End GetModifierValue

		#endregion

		#region internal GetDbReader(ref p_objReader, p_sSQL)
		/// Name		: GetDbReader
		/// Author		: Navneet Sota
		/// Date Created: 2/10/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment						*    Author
		/// 2/22/2005		   Added the Parameter p_objReader		 Navneet 
		/// ************************************************************
		/// <summary>
		/// Returns the DBReader for the SQL passed, if there is any record else returns null
		/// </summary>
		/// <param name="p_objReader">DbReader object to be filled with the corresponding SQL values</param>
		/// <param name="p_sSQL">SQL to be executed</param>
		internal bool GetDbReader(ref DbReader p_objReader, string p_sSQL)
		{
			//string variable
            string sODBCFullFilePath = string.Empty, sODBCConnectionString = string.Empty;

            try
            {
                if (p_objReader != null)
                    p_objReader.Close();

                if (p_sSQL == null)
                    return false;

                if (string.IsNullOrEmpty(g_sODBCName))
                {
                    AddAlert(Globalization.GetString("Common.Error.DSN",m_iClientId));
                    return false;
                }

                //Get the physical path of MDB file from database
                sODBCFullFilePath = GetSingleString("DB_PATH", "BRS_FEESCHD", "FEESCHD_NAME='" + g_sODBCName + "'");

                //Construct the ODBC value for Access DB, for accessing Fee Tables.
                if (sODBCFullFilePath.ToLower().EndsWith(".mdb"))
                {
                    sODBCConnectionString = GetBRSConnectionString(sODBCFullFilePath);
                }
                else
                {
                    sODBCConnectionString = sODBCFullFilePath;
                }
                p_objReader = DbFactory.GetDbReader(sODBCConnectionString, p_sSQL);
                
                if (p_objReader != null)
                    return true;
                else
                    return false;

            }//end try
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Common.GetDbReader.GeneralError", m_iClientId), p_objException);
            }
            
		}//end GetDbReader

        /// <summary>
        /// Gets the BRS connection string using the Access ODBC Driver
        /// </summary>
        /// <param name="sODBCFullFilePath">string containing the file path to the Access Database</param>
        /// <returns>string containing the Access ODBC Connection String</returns>
        private static string GetBRSConnectionString(string sODBCFullFilePath)
        {
            string sODBCConnectionString = string.Empty;
            sODBCConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;", sODBCFullFilePath);
            return sODBCConnectionString;
        }
		#endregion

		#region internal GetSingleLong(p_sFieldName, p_sTableName, p_sCriteria)
		/// Name		: GetSingleLong
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Creates a dynamic query and executes it against the database and returns result as a Long value
		/// </summary>
		/// <param name="p_sFieldName">Field Name</param>
		/// <param name="p_sTableName">Table Name</param>
		/// <param name="p_sCriteria">Criteria i.e. where clause</param>
		internal long GetSingleLong(string p_sFieldName, string p_sTableName, string p_sCriteria)
		{
			long lGetSingleLong=0;
			string sSql="";
			DbReader objReader=null;
			try
			{	
				if (p_sCriteria.Trim().Length==0) 
					p_sCriteria="";
				else
					p_sCriteria=" WHERE " + p_sCriteria;

				sSql="SELECT " + p_sFieldName + " FROM " + p_sTableName+p_sCriteria;
				try
				{
					objReader=DbFactory.GetDbReader(g_sConnectionString,sSql);
				}
				catch
				{
					lGetSingleLong = 0;
					objReader = null;
				}

				if (objReader != null)
				{
					if(objReader.Read())
					{
						if(objReader.GetValue(0) is System.DBNull)
							lGetSingleLong=0;
						else
							lGetSingleLong=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue(0)));
					}//end if(objReader.Read())
				}//end if (objReader != null)
			}//end try
			catch(Exception p_objException)
			{
				lGetSingleLong = 0;
                throw new RMAppException(Globalization.GetString("Common.GetSingleLong.GeneralError", m_iClientId), p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return (lGetSingleLong);
		}//end GetSingleLong()
		#endregion

		#region public GetSingleString(p_sFieldName, p_sTableName, p_sCriteria)
		/// Name		: GetSingleString
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Creates a dynamic query and executes it against the database and returns result as a String value
		/// </summary>
		/// <param name="p_sFieldName">Field Name</param>
		/// <param name="p_sTableName">Table Name</param>
		/// <param name="p_sCriteria">Criteria i.e. Where clause</param>
		public string GetSingleString(string p_sFieldName, string p_sTableName, string p_sCriteria)
		{
			string sReturnValue="";
			string sSql="";
			DbReader objReader=null;
			try
			{	
				if (p_sCriteria.Trim().Length==0) 
					p_sCriteria="";
				else
					p_sCriteria=" WHERE " + p_sCriteria;

				sSql="SELECT " + p_sFieldName + " FROM " + p_sTableName+p_sCriteria;
				try
				{
					objReader=DbFactory.GetDbReader(g_sConnectionString,sSql);
				}
				catch
				{
					sReturnValue = "";
					objReader = null;
				}

				if (objReader != null)
					if(objReader.Read())
					{
						if(objReader.GetValue(0) is System.DBNull)
						{
							sReturnValue="";
						}
						else
						{
							sReturnValue=Conversion.ConvertObjToStr(objReader.GetValue(0));
						}
					}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Common.GetSingleString.GeneralError", m_iClientId), p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return (sReturnValue);
		}//end GetSingleString()
		#endregion

		#region internal AddOption(ref p_objXMLElem,p_iValue, p_sValue, p_iSelected)
		/// Name		: AddOption
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Adds the Option tag to passed in XML
		/// </summary>
		/// <param name="p_objXMLElem">XML Node to which Options needs to be added</param>
		/// <param name="p_iValue">Value of the Options tag</param>
		/// <param name="p_sValue">Display text fro Options tag</param>
		/// <param name="p_iSelected">Flag to indicate which option is selected</param>
		internal void AddOption(ref XmlElement p_objXMLElem,int p_iValue, string p_sValue, int p_iSelected)
		{
			XmlElement objXMLOptionEle;
			try
			{	
			
				objXMLOptionEle = (XmlElement)g_objXMLDoc.CreateElement("option");
				objXMLOptionEle.SetAttribute("value", p_iValue.ToString());
				if(p_iValue == p_iSelected)
					p_objXMLElem.SetAttribute("codeid",p_iSelected.ToString());//objXMLOptionEle.SetAttribute("selected", "1");
				
				objXMLOptionEle.InnerText = p_sValue;

				p_objXMLElem.AppendChild(objXMLOptionEle);
			}//end try
			catch(Exception p_objXMLException)
			{
				throw new RMAppException(p_objXMLException.Message, p_objXMLException);
			}
		}//end AddOption
		#endregion

		#region internal RemoveNamedNode(p_sFieldName, p_objXMLDoc)
		/// Name		: RemoveNamedNode
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Removes the passed node name from passed XML 
		/// </summary>
		/// <param name="p_sFieldName">Field Name</param>
		internal void RemoveNamedNode(string p_sFieldName)
		{
            if (!m_KilledNodeList.Contains(p_sFieldName))
                m_KilledNodeList.Add(GetSectionNodeList(p_sFieldName));
		}//end RemoveNamedNode
		#endregion

        internal void AddNamedNode(string p_sFieldName)
        {
            if (m_KilledNodeList.Contains(p_sFieldName))
                m_KilledNodeList.Remove(p_sFieldName);
        }

		#region internal RemoveChildNode(p_sFieldName, p_objXMLDoc)
		/// Name		: RemoveChildNode
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Removes all the chid nodes from passed XML Node Element
		/// </summary>
		/// <param name="p_objXMLElement">XML Node from which the child node needs to be removed</param>
		internal void RemoveChildNode(ref XmlElement p_objXMLElement)
		{
			XmlNode objXMLNode;
			int iCnt = 0;
            try
            {
                if (p_objXMLElement != null && p_objXMLElement.HasChildNodes)
                {
                    for (iCnt = 0; iCnt <= p_objXMLElement.ChildNodes.Count - 1; iCnt++)
                    {
                        objXMLNode = p_objXMLElement.ChildNodes[iCnt];
                        objXMLNode.ParentNode.RemoveChild(objXMLNode);
                    }
                }
            }//end try
            catch (Exception p_objXMLException)
            {
                throw new RMAppException(p_objXMLException.Message, p_objXMLException);
            }
            finally
            {
                objXMLNode = null;
            }
		}//end RemoveChildNode
		#endregion

        /// <summary>
        /// Add the remove node to the KilledNodes List
        /// </summary>
        /// <param name="p_objXMLDoc"></param>
        /// <param name="sNodeName"></param>
        public void AddKilledNodes(ref XmlDocument p_objXMLDoc, string sNodeName)
        {
            XmlElement oKillNodeElement = (XmlElement)p_objXMLDoc.SelectSingleNode("//option/KilledNodes");
            if (oKillNodeElement == null)
            {
                oKillNodeElement = p_objXMLDoc.CreateElement("KilledNodes");
                XmlNode oSplitDataNode = p_objXMLDoc.SelectSingleNode("//option");
                if( oSplitDataNode != null )
                    oSplitDataNode.AppendChild((XmlNode)oKillNodeElement);
            }

            string sKilledNodes = oKillNodeElement.InnerText;
            if (sKilledNodes.IndexOf(sNodeName + "|") < 0)
                sKilledNodes += sNodeName + "|";
        }

		internal void RemoveChildNodes(ref XmlElement p_objNode)
		{
			if(p_objNode==null)return;

			XmlNodeList objChildList=p_objNode.ChildNodes;
			int iChildCount=p_objNode.ChildNodes.Count;
			try
			{
				if(p_objNode != null)
				{
					for(int iTemp=iChildCount-1; iTemp>=0;iTemp--)
					{
						p_objNode.RemoveChild(objChildList[iTemp]);
					}
				}
			}
			catch(Exception P_objErr)
			{
				throw new RMAppException(P_objErr.Message, P_objErr);
			}
			finally
			{
				objChildList=null;
			}
		}
		internal void AppendAttribute(ref XmlElement p_objElement, string p_sAttributeName,string p_sAttributeVal)
		{
			p_objElement.SetAttribute(p_sAttributeName,p_sAttributeVal);
		}
		#region internal GetValueOf(p_sFieldName, p_objXMLNode)
		/// Name		: GetValueOf
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Get Value of control from XML element
		/// </summary>
		/// <param name="p_sFieldName">Field Name</param>
		/// <param name="p_objXMLNode">XML Node</param>
		/// <returns>Value string</returns>
		public string GetValueOf(string p_sFieldName , XmlDocument p_objXMLNode )
		{
			XmlElement objTargetNode = null ;
			string sValue = "" ;				
			try
			{				
				objTargetNode = ( XmlElement )p_objXMLNode.SelectSingleNode( "//" + p_sFieldName);
				if( objTargetNode != null )
				{
					//if(objTargetNode.GetAttribute( "type" ) == "radio") 
					//	objTargetNode = (XmlElement)p_objXMLNode.SelectSingleNode("//control[@name='" + p_sFieldName + "' and @checked]") ;

					if( objTargetNode != null )
						sValue = GetValue( objTargetNode );
				}
			}//end try
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Common.GetValueOf.GenericError", m_iClientId), p_objEx);				
			}
			finally
			{
				objTargetNode = null ;
			}
			return( sValue );
		}//end GetValueOf
		
		#endregion

		#region SetValueOf(p_sFieldName, p_objVariant, ref p_objXMLDoc)
		/// Name		: SetValueOf
		/// Author		: Navneet
		/// Date Created	: 01/21/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Sets the value of the nodes in a XMLElement passed as input
		/// </summary>
		/// <param name="p_sFieldName">The filed for which the value needs to be set</param>
		/// <param name="p_objXMLDoc">Containing the node whose value to be set</param>
		/// <param name="p_objVariant">Variant object which will depend on parameter passd</param>
		/// 
		/*
		 * Making some of the methods public so that they can be used through funds while implementing BRS splits.Tanuj
		 * */
		public void SetValueOf(string p_sFieldName, object p_objVariant, ref XmlDocument p_objXMLDoc)
		{
			XmlElement objXMLElement = null;

			objXMLElement = (XmlElement)p_objXMLDoc.SelectSingleNode("//" + p_sFieldName);
			if(objXMLElement != null)
				SetValue(ref objXMLElement,p_objVariant);

		}//end SetValue
		#endregion

		#region SetValue(XmlElement p_objXMLElement, object p_objVariant)
		/// Name		: SetValue
		/// Author		: Navneet
		/// Date Created	: 01/20/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Sets the value of the nodes in a XMLElement passed as input
		/// </summary>
		/// <param name="p_objXMLElement">Containing the node whose value to be set</param>
		/// <param name="p_objVariant">Variant object which will depend on parameter passd</param>
		internal void SetValue(ref XmlElement p_objXMLElement, object p_objVariant)
		{
			//string variables
			string sNodeType = "";
			string sName = "";
			string sTemp1 = "";
			string sTemp2 = "";

			//objects
			DataSimpleList objDSList = null;
			Entity objEntity = null;
			IEnumerator objEnum = null;
			//Xml variables
			XmlElement objOptionXMLEle = null;

            try
            {
                if (m_objLocalCache == null)
                    m_objLocalCache = new LocalCache(g_sConnectionString, m_iClientId);

                sNodeType = p_objXMLElement.GetAttribute("type");
                sName = p_objXMLElement.GetAttribute("name");

                switch (sNodeType)
                {
                    case CODE_FLDTYPE:
                    case CODEDETAIL_FLDTYPE:
                        sName = p_objXMLElement.GetAttribute("codetable");
                        if (sName.ToLower() == "states")
                        {
                            if (Val(Conversion.ConvertObjToStr(p_objVariant)) > 0)
                            {
                                m_objLocalCache.GetStateInfo(Conversion.ConvertObjToInt(p_objVariant, m_iClientId),
                                    ref sTemp1, ref sTemp2);
                            }
                        }//end if(sName.ToLower() == "states")
                        else
                        {
                            if (Val(Conversion.ConvertObjToStr(p_objVariant)) > 0)
                            {
                                m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(p_objVariant, m_iClientId), ref sTemp1, ref sTemp2);

                            }
                        }
                        p_objXMLElement.InnerText = sTemp1.Trim() + " " + sTemp2.Trim();
                        p_objXMLElement.SetAttribute(ATT_CODEID, Conversion.ConvertObjToStr(p_objVariant));
                        break;
                    //					case RADIO_FLDTYPE:
                    //						if(Conversion.ConvertObjToStr(p_objVariant) != "")
                    //							p_objXMLElement.SetAttribute("value",Conversion.ConvertObjToStr(p_objVariant));
                    //						else
                    //							p_objXMLElement.RemoveAttribute("checked");
                    //						break;
                    case TEXT_LABEL:
                        p_objXMLElement.InnerText = Conversion.ConvertObjToStr(p_objVariant);
                        break;
                    case BOOL_FLDTYPE:
                        if (p_objVariant != null)
                            p_objXMLElement.InnerText = "1";
                        else
                            p_objXMLElement.InnerText = "0";
                        break;
                    case PHONE_FLDTYPE:
                        p_objXMLElement.InnerText = Conversion.ToPhoneNumber(Conversion.ConvertObjToStr(p_objVariant));
                        break;
                    case SSN_FLDTYPE:
                        sTemp1 = Conversion.ConvertObjToStr(p_objVariant);
                        p_objXMLElement.InnerText = sTemp1;
                        if (Conversion.IsNumeric(sTemp1) && sTemp1.Length == 9)
                            p_objXMLElement.InnerText = sTemp1.Substring(0, 3) + "-" + sTemp1.Substring(3, 2) + "-" + sTemp1.Substring(5);
                        break;
                    case TAXID_FLDTYPE:
                        sTemp1 = Conversion.ConvertObjToStr(p_objVariant);
                        p_objXMLElement.InnerText = sTemp1;
                        if (Conversion.IsNumeric(sTemp1) && sTemp1.Length == 9)
                            p_objXMLElement.InnerText = sTemp1.Substring(0, 2) + "-" + sTemp1.Substring(2);
                        break;
                    case ZIP_FLDTYPE:
                        sTemp1 = Conversion.ConvertObjToStr(p_objVariant);
                        p_objXMLElement.InnerText = sTemp1;
                        if (Conversion.IsNumeric(sTemp1) && sTemp1.Length >= 5)
                        {
                            sTemp1 = System.Text.RegularExpressions.Regex.Replace(sTemp1, "\\D", "");
                            p_objXMLElement.InnerText = sTemp1.Substring(0, 5);
                            if (sTemp1.Length > 5)
                                p_objXMLElement.InnerText = p_objXMLElement.InnerText + "-" + sTemp1.Substring(5);
                        }
                        break;
                    case DATE_FLDTYPE:
                    case DATEBSCRIPT_FLDTYPE:
                        p_objXMLElement.InnerText = "";
                        sTemp1 = Conversion.ConvertObjToStr(p_objVariant);
                        if (sTemp1 != "" && sTemp1.Length == 8)
                            p_objXMLElement.InnerText = Conversion.GetDate(sTemp1);
                        break;
                    case TIME_FLDTYPE:
                        sTemp1 = Conversion.ConvertObjToStr(p_objVariant);
                        p_objXMLElement.InnerText = "";
                        if (sTemp1 != "" && sTemp1.Length == 6)
                            p_objXMLElement.InnerText = Conversion.GetTime(sTemp1);
                        break;
                    case CODELIST_FLDTYPE:
                        sNodeType = p_objXMLElement.GetAttribute("codetable");
                        string sCodeIdList = "";
                        RemoveChildNodes(ref p_objXMLElement);
                        if (p_objVariant.GetType().Name == "DataSimpleList")
                        {
                            objDSList = (DataSimpleList)p_objVariant;
                            //							for(int iCnt = 0; iCnt < objDSList.Count; iCnt++)
                            //							{
                            //								m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(objDSList[iCnt]), ref sTemp1, ref sTemp2);			
                            //								objOptionXMLEle = p_objXMLElement.OwnerDocument.CreateElement("option");
                            //								objOptionXMLEle.SetAttribute("value", objDSList[iCnt].ToString());
                            //								objOptionXMLEle.InnerText = sTemp1.Trim() + " " + sTemp2.Trim();
                            //								p_objXMLElement.AppendChild(objOptionXMLEle);
                            //							}//end for

                            objEnum = objDSList.GetEnumerator();
                            while (objEnum.MoveNext())
                            {
                                m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(((DictionaryEntry)objEnum.Current).Value, m_iClientId), ref sTemp1, ref sTemp2);
                                objOptionXMLEle = p_objXMLElement.OwnerDocument.CreateElement("Item");
                                objOptionXMLEle.SetAttribute("value", ((DictionaryEntry)objEnum.Current).Value.ToString());
                                objOptionXMLEle.InnerText = sTemp1.Trim() + " " + sTemp2.Trim();
                                p_objXMLElement.AppendChild(objOptionXMLEle);
                                sCodeIdList = sCodeIdList + " " + ((DictionaryEntry)objEnum.Current).Value.ToString();

                            }
                            sCodeIdList = sCodeIdList.Trim();
                            p_objXMLElement.SetAttribute("codeid", sCodeIdList);
                        }//end if
                        else
                        {
                            m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(p_objVariant, m_iClientId), ref sTemp1, ref sTemp2);
                            objOptionXMLEle = p_objXMLElement.OwnerDocument.CreateElement("option");
                            objOptionXMLEle.SetAttribute("value", p_objVariant.ToString());
                            objOptionXMLEle.InnerText = sTemp1.Trim() + " " + sTemp2.Trim();
                            p_objXMLElement.AppendChild(objOptionXMLEle);
                        }

                        break;
                    case ENTITYLIST_FLDTYPE:
                        RemoveChildNode(ref p_objXMLElement);
                        objDSList = (DataSimpleList)p_objVariant;
                        for (int iCnt = 0; iCnt < objDSList.Count; iCnt++)
                        {
                            objEntity = (Entity)g_objDataModelFactory.GetDataModelObject("Entity", false);
                            sTemp1 = Conversion.ConvertObjToStr(objDSList[iCnt]);
                            if (Conversion.IsNumeric(sTemp1))
                                sTemp2 = objEntity.GetLastFirstName();

                            objOptionXMLEle = p_objXMLElement.OwnerDocument.CreateElement("option");
                            objOptionXMLEle.SetAttribute("value", objDSList[iCnt].ToString());
                            objOptionXMLEle.InnerText = sTemp2.Trim();
                            p_objXMLElement.AppendChild(objOptionXMLEle);
                        }//end for
                        break;
                    case EIDLOOKUP_FLDTYPE:
                    case PHYLOOKUP_FLDTYPE :
                        objEntity = (Entity)g_objDataModelFactory.GetDataModelObject("Entity", false);
                        sTemp1 = Conversion.ConvertObjToStr(p_objVariant);
                        if (Conversion.IsNumeric(sTemp1) && Val(sTemp1) > 0)
							sTemp2 = objEntity.GetLastFirstName( Conversion.ConvertStrToInteger(sTemp1));

                        p_objXMLElement.SetAttribute(ATT_CODEID, sTemp1);
                        p_objXMLElement.InnerText = sTemp2.Trim();
                        break;
                    case COMBOBOX_FLDTYPE:
                        sTemp1 = Conversion.ConvertObjToStr(p_objVariant);
                        p_objXMLElement.SetAttribute(ATT_CODEID, sTemp1);
                        objOptionXMLEle = (XmlElement)p_objXMLElement.SelectSingleNode("option[@value='" + sTemp1 + "']");
                        if (objOptionXMLEle != null)
                            objOptionXMLEle.SetAttribute("selected", "1");
                        break;
                    case CURRENCY_FLDTYPE:
                        p_objXMLElement.InnerText = string.Format("{0:C}", Conversion.ConvertObjToStr(p_objVariant));
                        break;
                    default:
                        p_objXMLElement.InnerText = Conversion.ConvertObjToStr(p_objVariant);
                        break;
                }//end switch
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Common.SetValue.GenericError", m_iClientId), p_objException);
            }
            finally
            {
                if (objDSList != null)
                {
                    objDSList.Dispose();
                    objDSList = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                objEnum = null;             
                objOptionXMLEle = null;
                if (m_objLocalCache != null)
                {
                    m_objLocalCache.Dispose();
                    m_objLocalCache = null;
                }
            }
		}//end SetValue
		#endregion

		#region internal AlertsToXML(p_hstAlerts,ref p_objXmlDoc)
		/// Name			: AlertsToXML
		/// Author			: Navneet
		/// Date Created	: 01/20/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		///	Adds the collection of Alerts to XML Doc
		/// </summary>
		/// <param name="p_hstAlerts">Collection of Alerts</param>
		/// <param name="p_objXmlDoc">XML Doc to which alerts needs to be added</param>
		internal void AlertsToXML(Hashtable p_hstAlerts,ref XmlDocument p_objXmlDoc)
		{
			XmlElement xmlAlerts;
			XmlElement xmlAlert;
            try
            {
                if (p_hstAlerts.Count > 0)
                {
                    xmlAlerts = (XmlElement)p_objXmlDoc.CreateElement("alerts");
                    for (int iCnt = 1; iCnt <= p_hstAlerts.Count; iCnt++)
                    {
                        xmlAlert = (XmlElement)p_objXmlDoc.CreateElement(Common.ERR_ERROR);
                        xmlAlert.InnerText = p_hstAlerts[iCnt].ToString();
                        xmlAlerts.AppendChild(xmlAlert);
                    }
                    p_objXmlDoc.DocumentElement.AppendChild(xmlAlerts);
                }
            }//end try
            catch (Exception p_objXMLException)
            {
                throw new RMAppException(p_objXMLException.Message, p_objXMLException);
            }
            finally
            {
                xmlAlerts = null;
                xmlAlert = null;
            }
		}//end AlertsToXML
		#endregion

		#region TBD internal HandleXMLErrorMessage(ref p_objXmlDoc, ref p_objXmlErrors, p_sModule)
		/// Name			: HandleXMLErrorMessage
		/// Author			: Navneet
		/// Date Created	: 01/20/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		///	
		/// </summary>
		/// <param name="p_objXmlDoc">XML Document</param>
		/// <param name="p_objXmlErrors">XmlErrors element</param>
		/// <param name="p_sModule">Module</param>
		internal void HandleXMLErrorMessage(ref XmlDocument p_objXmlDoc, ref XmlElement p_objXmlErrors, string p_sModule)
		{
			XmlElement xmlErr;
            try
            {
                xmlErr = (XmlElement)p_objXmlDoc.CreateElement(Common.ERR_ERROR);
                p_objXmlErrors.AppendChild(xmlErr);
            }//end try
            catch (Exception p_objXMLException)
            {
                throw new RMAppException(p_objXMLException.Message, p_objXMLException);
            }
            finally
            {
                xmlErr = null;
            }
		}//end HandleXMLErrorMessage
#endregion

		#region internal ProviderContractReviewBill (ref p_objRequest)
		/// Name		: ProviderContractReviewBill
		/// Author		: Navneet Sota
		/// Date Created: 2/1/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// Request is altered and passed back
		/// </summary>
		/// <param name="p_objRequest">The Request class object</param>
		/// <returns> ProviderContractOverride class object</returns>
		internal ProviderContractOverride ProviderContractReviewBill(ref Request p_objRequest)
		{
			//Objects
			ProviderContractOverride objReturnValue = null;
			Settings.BrsSettings objBrsSettings = null;
			Riskmaster.Settings.CCacheFunctions objSettings = null;

			string sTemp = "";
			try
			{
				if(p_objRequest != null)
				{
					if(p_objRequest.GetParam("OVERRIDE") != null)
						objReturnValue = (ProviderContractOverride)p_objRequest.GetParam("OVERRIDE");
					else
						objReturnValue = new ProviderContractOverride(this, m_iClientId);

					if(objReturnValue.OverrideOption != Common.BillOverrideType.APPLY_UNKNOWN)
					{
						//Request included a fully populated override object then just use it.
						objBrsSettings = new Settings.BrsSettings(g_sConnectionString, m_iClientId);
						if(m_objLocalCache == null)
                            m_objLocalCache = new LocalCache(g_sConnectionString, m_iClientId);
						objSettings = new Riskmaster.Settings.CCacheFunctions(g_sConnectionString, m_iClientId);
						if (objBrsSettings.GenEOBCodeFlag != 0)
						{
							//Fill in the Currently appropriate Override EOB.
							if(p_objRequest.BillItemObject.EOBCodes.Count != 0)
							{								
								IEnumerator objEnum = p_objRequest.BillItemObject.EOBCodes.GetEnumerator();
								int iTemp=0;
								string[] arrTemp=new string[p_objRequest.BillItemObject.EOBCodes.Count];
								try
								{
									while(objEnum.MoveNext())
									{
										arrTemp[iTemp]=((DictionaryEntry)objEnum.Current).Value.ToString();
										iTemp++;
									}
									foreach(string sVal in arrTemp)
									{
                                        sTemp = m_objLocalCache.GetShortCode(Conversion.ConvertObjToInt(sVal, m_iClientId));
										if(sTemp != null && sTemp != "" && sTemp.Substring(0,1).ToUpper() == "O")
                                            p_objRequest.BillItemObject.EOBCodes.Remove(Conversion.ConvertObjToInt(Conversion.ConvertObjToInt(sVal, m_iClientId), m_iClientId));
									}
								}
								catch(Exception p_objErr)
								{
									throw new RMAppException(p_objErr.Message, p_objErr);
								}
								finally
								{
                                  arrTemp=null;
								  objEnum=null;
								}

								
							}//end if(p_objRequest.BillItemObject.EOBCodes.Count != 0)

							//TODO: Function to be added in LocalCache
							p_objRequest.BillItemObject.EOBCodes.Add(objSettings.GetCodeIDWithShort(objReturnValue.OverrideEOBCode, "EOB_CODES"));
						}//if (objBrsSettings.GenEOBCodeFlag != 0)
						p_objRequest.BillItemObject.SchdAmount = objReturnValue.m_dOverrideAmount;
					
						if(p_objRequest.GetMissingParam("OVERRIDE") != null &&
							p_objRequest.GetMissingParam("OVERRIDE").ToString() != "")
							p_objRequest.MissingParams.Remove("OVERRIDE");
					}//end if(objReturnValue.OverrideOption != APPLY_UNKNOWN)
					else
					{
						//Calculate and return possible fresh Override amounts waiting for user override type 
						//selection.
						objReturnValue = new ProviderContractOverride(this, m_iClientId);

						objReturnValue.Populate(p_objRequest);
						if(Conversion.ConvertObjToStr(p_objRequest.GetMissingParam("OVERRIDE")) != "")
							p_objRequest.MissingParams.Remove("OVERRIDE");
					
						//Get rid of any existing one
						p_objRequest.Params.Remove("OVERRIDE");
						//Slide in the fresh one.
						p_objRequest.AddParam(objReturnValue,"OVERRIDE");
						p_objRequest.AddMissingParam("OVERRIDE");

						//Handle Populating the BillItem with updated information inside this Library.
						//Clients just Update the screen from this fully current BillItem object.
						
					}//end if(objReturnValue.OverrideOption == APPLY_UNKNOWN)
				}//end if(p_objRequest != null)
				p_objRequest.BillItemObject.OverrideType = (int)objReturnValue.OverrideOption;
				p_objRequest.BillItemObject.AmountAllowed = objReturnValue.m_dOverrideAmount;
				//Return the value to the calling function
				return objReturnValue;
			}//end try
			catch(Exception p_objException)
			{
				throw new RMAppException(p_objException.Message, p_objException);
			}
			finally
			{
                if (m_objLocalCache != null)
                {
                    m_objLocalCache.Dispose();
                    m_objLocalCache = null;
                }
				objBrsSettings = null;
                objReturnValue = null;
                objSettings = null;
			}
		}// end ProviderContractReviewBill

		#endregion

		#region internal AddAlert(p_sInputAlert)
		/// Name			: AddAlert
		/// Author			: Navneet
		/// Date Created	: 01/02/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Adds the Alert value in global hashtable.
		/// </summary>
		/// <param name="p_sInputAlert"></param>
		internal void AddAlert(string p_sInputAlert)
		{
			this.Errors.Add("",p_sInputAlert,BusinessAdaptorErrorType.Message);
		}
        internal void AddError(string p_sInputError)
        {
            this.Errors.Add("", p_sInputError, BusinessAdaptorErrorType.Error);
        }
		internal void AddToError(Exception p_objErr)
		{
			this.Errors.Add(p_objErr,BusinessAdaptorErrorType.Error);
		}
		#endregion

		#endregion

		#region private Functions

		#region private string GetValue(p_objXMLNode)
		/// Name		: GetValue
		/// Author		: Navneet Sota
		/// Date Created: 1/5/2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Get Node Value
		/// </summary>
		/// <param name="p_objXMLNode">XML Node</param>
		/// <returns>Value string</returns>
		private string GetValue( XmlElement p_objXMLNode )
		{
			string sType = "" ;
			string sName = "" ;
			string sReturnValue = "" ;			

			sType = p_objXMLNode.GetAttribute( "type" );
			sName = p_objXMLNode.GetAttribute( "name" );
			if( sType == CODE_FLDTYPE || sType == CODEDETAIL_FLDTYPE || sType == COMBOBOX_FLDTYPE || sType == EIDLOOKUP_FLDTYPE )
			{
				sReturnValue = p_objXMLNode.GetAttribute( ATT_CODEID );
			}
			else
			{
				sReturnValue = p_objXMLNode.InnerText ;
			}							

			return( sReturnValue );
		}//end GetValue
		#endregion

		#endregion
		/// <summary>
		/// Used for filling billing codes.
		/// </summary>
		/// <param name="p_objDoc"></param>
		/// <param name="p_sRMConnStr"></param>
		/// <returns></returns>
		/// Incomin document structure
		public XmlDocument FillBillingCodeDom(ref XmlDocument p_objDoc, string p_sRMConnStr)
		{
			XmlElement objCodesElements=null;
			int iFeeScheduleId=0,iRequestedPageNumber=0,iRecCount1=0,iRecCount2=0,iTotalRecordCount=0
				,iPageCount=0,iCounter = 500;
			string sFind=string.Empty;
			try
			{
				sFind=p_objDoc.SelectSingleNode("//BillingCodesParams//Find").InnerText.Trim();
                //UMESH Searc and Lookup issue

				objCodesElements = p_objDoc.CreateElement("codes");
				iFeeScheduleId = Convert.ToInt32(p_objDoc.SelectSingleNode("//BillingCodesParams//FeeScheduleId").InnerText);
                iTotalRecordCount = Conversion.ConvertObjToInt(p_objDoc.SelectSingleNode("//BillingCodesParams//TotalRecordCount").InnerText.Trim(), m_iClientId);

				if(Convert.ToInt32(p_objDoc.SelectSingleNode("//BillingCodesParams//RequestedPageNumber").InnerText)==0)
				{
					iRequestedPageNumber=1;
					p_objDoc.SelectSingleNode("//BillingCodesParams//RequestedPageNumber").InnerText=iRequestedPageNumber.ToString();
				}
				else
				{
					iRequestedPageNumber=Convert.ToInt32(p_objDoc.SelectSingleNode("//BillingCodesParams//RequestedPageNumber").InnerText);
				}
                //MITS 24732 If first fee table ID is 0, try to get the second fee table
                if (iFeeScheduleId > 0)
                {
                    GetBillingCodes(ref p_objDoc, ref objCodesElements, sFind, iFeeScheduleId, ref iTotalRecordCount, iRequestedPageNumber, ref iRecCount1, p_sRMConnStr, false);
                }

				if(Conversion.ConvertStrToInteger(p_objDoc.SelectSingleNode("//FeeScheduleId2").InnerText)>0)
				{
                     // GetBillingCodes(ref p_objDoc,ref objCodesElements,sCPT,sDesc,Conversion.ConvertStrToInteger(p_objDoc.SelectSingleNode("//FeeScheduleId2").InnerText),ref iTotalRecordCount,iRequestedPageNumber,ref iRecCount2,p_sRMConnStr,true);
                    GetBillingCodes(ref p_objDoc, ref objCodesElements, sFind, Conversion.ConvertStrToInteger(p_objDoc.SelectSingleNode("//FeeScheduleId2").InnerText), ref iTotalRecordCount, iRequestedPageNumber, ref iRecCount2, p_sRMConnStr, true);
				}
				if(Conversion.ConvertStrToInteger(p_objDoc.SelectSingleNode("//BillingCodesParams//TotalRecordCount").InnerText)==0)
				{
                  iTotalRecordCount=iRecCount1+iRecCount2;
				  p_objDoc.SelectSingleNode("//BillingCodesParams//TotalRecordCount").InnerText=iTotalRecordCount.ToString();
				}
				
				iPageCount = Convert.ToInt32(p_objDoc.SelectSingleNode("//PageCount").InnerText);
				if(iPageCount==0)
				{
					iPageCount = iTotalRecordCount/iCounter;
					if((iTotalRecordCount % iCounter)>0)
						iPageCount++;
					if(iPageCount==0)
					{
                       iPageCount=1;
					}
					p_objDoc.SelectSingleNode("//PageCount").InnerText = iPageCount.ToString();
				}

				if(Convert.ToInt32(p_objDoc.SelectSingleNode("//PageCount").InnerText)>1)
				{
					if(iRequestedPageNumber==1)
					{
						objCodesElements.RemoveAttribute("previouspage");
					}
					else
					{
						objCodesElements.SetAttribute("previouspage",Conversion.ConvertObjToStr(iRequestedPageNumber-1));
					}

					if(iRequestedPageNumber>1)
					{
						objCodesElements.SetAttribute("firstpage","1");
					}
					else
					{
						objCodesElements.RemoveAttribute("firstpage");
					}

					if(iPageCount==iRequestedPageNumber)
					{
						objCodesElements.RemoveAttribute("nextpage");
					}
					else
					{
						objCodesElements.SetAttribute("nextpage",Conversion.ConvertObjToStr(iRequestedPageNumber+1));
					}
					if(iRequestedPageNumber<iPageCount)
					{
						objCodesElements.SetAttribute("lastpage",iPageCount.ToString());
					}
					else
					{
						objCodesElements.RemoveAttribute("lastpage");
					}
						objCodesElements.SetAttribute("thispage",iRequestedPageNumber.ToString());
				}
				objCodesElements.SetAttribute("pagecount",iPageCount.ToString());
				objCodesElements.SetAttribute("feesched", iFeeScheduleId.ToString());
				objCodesElements.SetAttribute("feesched2",p_objDoc.SelectSingleNode("//FeeScheduleId2").InnerText);
				objCodesElements.SetAttribute("find", sFind);
				if(objCodesElements.SelectSingleNode("//code")==null)
				{
					objCodesElements.SetAttribute("nocodes", "true");
				}
				else
				{
                   objCodesElements.SetAttribute("nocodes", "false");
				}
                p_objDoc.DocumentElement.AppendChild(objCodesElements);
			}
			catch(Exception p_objErr)
			{
                throw new StringException(Globalization.GetString("Common.FillBillingCodeDom.Error", m_iClientId), p_objErr);
			}
			finally
			{
                objCodesElements = null;
			}
			return p_objDoc;
		}

        /// <summary>
        /// Get billing codes from fee table
        /// </summary>
        /// <param name="p_objDoc"></param>
        /// <param name="p_objCodesNode"></param>
        /// <param name="p_sFind"></param>
        /// <param name="p_iFeeSheduleId"></param>
        /// <param name="p_iTotalRecordCount"></param>
        /// <param name="p_iRequestedPageNumber"></param>
        /// <param name="p_iRecordCount"></param>
        /// <param name="p_sConnStr"></param>
        /// <param name="p_bIsFeeSchedule2nd"></param>
        public void GetBillingCodes(ref XmlDocument p_objDoc, ref XmlElement p_objCodesNode, string p_sFind, int p_iFeeSheduleId, ref int p_iTotalRecordCount, int p_iRequestedPageNumber, ref int p_iRecordCount, string p_sConnStr, bool p_bIsFeeSchedule2nd)
		{
			int iCounter = Common.MAXRECORDS;
			FeeSchedule objFeeSchedule = null;
			
			DbReader objReader=null;
			int iCalcType = 0;
			string sODBCName = "";
            string sCptColName =  "";
            string sDescColName = "";
            string sCptTableName = "";
			string sTempSql = "";
			string sPath = "";
			string sDsn = "";
			string sCountSQL = "";
			int iStartReadingAt = 0;
			int iTemp = 0;
			string sSQL = "";
			Common objCommon= new Common(m_iClientId);
			objCommon.g_sConnectionString=p_sConnStr;
			try
			{
				objFeeSchedule=new FeeSchedule(objCommon, m_iClientId);
				objFeeSchedule.MoveToRecord(p_iFeeSheduleId);
				sTempSql="SELECT CALC_TYPE,ODBC_NAME FROM FEE_TABLES WHERE TABLE_ID = " + p_iFeeSheduleId.ToString();
                 
				objReader=DbFactory.GetDbReader(p_sConnStr,sTempSql);
				if(objReader.Read())
				{
					iCalcType=objReader.GetInt16(0);
					if(iCalcType!=(int)FeeScheduleType.BASIC && iCalcType!=(int)FeeScheduleType.EXTENDED)
					{
						sODBCName=objReader.GetString(1);
					}
				}
				objReader.Close();
                
				sCptColName = objFeeSchedule.CPTCodeColumnName;
				sDescColName = objFeeSchedule.CPTDescriptionColumnName;
				sCptTableName = objFeeSchedule.CPTTableName;
    
				sSQL = "SELECT DISTINCT " + sCptColName + "," + sDescColName + " FROM " + sCptTableName;

				if(iCalcType!=(int)FeeScheduleType.BASIC && iCalcType!=(int)FeeScheduleType.EXTENDED)
				{
					if(sODBCName==null || sODBCName.Trim()=="")
					{
						throw new RMAppException("Error, missing ODBC_NAME, TABLE_ID=" + p_iFeeSheduleId.ToString());
					}
					sTempSql="SELECT DB_PATH FROM BRS_FEESCHD WHERE FEESCHD_NAME = '" + sODBCName + "'";

					objReader=DbFactory.GetDbReader(p_sConnStr , sTempSql);
					if(objReader.Read())
					{
                      sPath=objReader.GetString(0);
					}
					if(sPath.Trim()=="")
					{
						throw new RMAppException("Error, missing DB_PATH, ODBC_NAME= " + sODBCName + ", TABLE_ID=" + p_iFeeSheduleId.ToString());
					}

                    if (sPath.ToLower().EndsWith(".mdb"))
                    {
                        if (!File.Exists(sPath))
                        {
                            throw new RMAppException("MDB file does not exist");
                        }
                        sDsn = GetBRSConnectionString(sPath);
                        sCountSQL = "SELECT COUNT(*) FROM (SELECT DISTINCT " + sCptColName + "," + sDescColName + " FROM " + sCptTableName + ")";
                    }
                    else
                    {
                        sDsn = sPath;
                        sCountSQL = "SELECT COUNT(DISTINCT " + sCptColName + "+" + sDescColName + ") FROM " + sCptTableName;
                    }
				}
				else
				{
                   sCountSQL = "SELECT COUNT(DISTINCT " + sCptColName + ") FROM " + sCptTableName;
				   sDsn=p_sConnStr;
				}
                //  Umesh search and lookup

                if (p_sFind.Trim().Length > 0)
                {
                    sSQL = sSQL + " WHERE ";
                    sCountSQL = sCountSQL + " WHERE ";

                    if (!DbFactory.IsOracleDatabase(sDsn))
                    {
                        sSQL += string.Format("{0} + ' ' + {1} LIKE '%{2}%' OR {0} + ' - ' + {1} LIKE '%{2}%' OR {0} LIKE '%{2}%'", sCptColName, sDescColName, p_sFind.Replace("'", "''"));
                        sCountSQL += string.Format("{0} + ' ' + {1} LIKE '%{2}%' OR {0} + ' - ' + {1} LIKE '%{2}%' OR {0} LIKE '%{2}%'", sCptColName, sDescColName, p_sFind.Replace("'", "''"));
                    }
                    else
                    {
                        sSQL += string.Format("{0} || ' ' || {1} LIKE '%{2}%' OR {0} || ' - ' || {1} LIKE '%{2}%' OR {0} LIKE '%{2}%'", sCptColName, sDescColName, p_sFind.Replace("'", "''"));
                        sCountSQL += string.Format("{0} || ' ' || {1} LIKE '%{2}%' OR {0} || ' - ' || {1} LIKE '%{2}%' OR {0} LIKE '%{2}%'", sCptColName, sDescColName, p_sFind.Replace("'", "''"));
                    }
                }
                //End search and lookup
				else
				{
					if(iCalcType== (int)FeeScheduleType.BASIC || iCalcType== (int)FeeScheduleType.EXTENDED)
					{
						sSQL = sSQL + " WHERE TABLE_ID = " + p_iFeeSheduleId.ToString()  ;
                        sCountSQL = sCountSQL + " WHERE TABLE_ID = " + p_iFeeSheduleId.ToString();
					}
				}
				if(p_iTotalRecordCount==0 || p_bIsFeeSchedule2nd)
				{
					objReader=DbFactory.GetDbReader(sDsn,sCountSQL.Trim());
					if(objReader.Read())
					{
						p_iRecordCount= objReader.GetInt(0);
					}
				}
				
                if(objReader!=null && !(objReader.IsClosed))
					objReader.Close();
				if(p_objCodesNode.HasAttribute("fullpage"))
				{
					if(p_objCodesNode.Attributes["fullpage"].Value=="true")
					{
						return;
					}
				}
				if(p_iRequestedPageNumber==1)
				{
					iStartReadingAt=1;
				}
				else
				{
					if(p_bIsFeeSchedule2nd)
					{
                          SetStartAt(p_iTotalRecordCount,p_iRecordCount,p_iRequestedPageNumber,ref iStartReadingAt,ref iCounter);
					}
					else
					{
						iStartReadingAt=(((p_iRequestedPageNumber-1)*iCounter)+1);
					}
				}

				objReader = DbFactory.GetDbReader(sDsn, sSQL);
                
				if(iStartReadingAt>1)
				{
                 iTemp=1;

					do
					{
                        objReader.Read();
                        iTemp++;
					}while(iTemp<iStartReadingAt);

				}
				iTemp=0;
                
				while(objReader.Read() && iTemp<=iCounter)
				{
                  XmlElement objCode = p_objDoc.CreateElement("code");
				
                  objCode.SetAttribute("codedesc", Conversion.ConvertObjToStr(objReader.GetValue(1)));

				  objCode.SetAttribute("id",objReader.GetString(0));
				  objCode.SetAttribute("shortcode",objReader.GetString(0));
                  p_objCodesNode.AppendChild(objCode);
				  objCode=null;
                  iTemp++;
				}
				if(iTemp>=Common.MAXRECORDS && objReader.Read())
				{
					p_objCodesNode.SetAttribute("fullpage","true");
				}
				objReader.Close();
			}
			catch(Exception p_objErr)
			{
                throw new StringException(Globalization.GetString("Common.GetBillingCodes.Error", m_iClientId), p_objErr);
			}
			finally
			{
				if(objReader!=null)
				{
					if(!objReader.IsClosed)
					{
						objReader.Close();
					}
					objReader=null;
				}

                objFeeSchedule = null;
                objCommon = null;
			}
			//return objCodesElements;
		}

        /// <summary>
        /// Check if any contract exist for the provider
        /// </summary>
        /// <param name="p_iEntityId"></param>
        /// <returns></returns>
		public bool AnyContractExits(int p_iEntityId)
		{
			string sSql="";
			DbReader objReader=null;
			try
			{
				sSql = "SELECT Count(*) FROM PROVIDER_CONTRACTS";
				sSql = sSql + " WHERE PROVIDER_EID = " + p_iEntityId.ToString();
				objReader = DbFactory.GetDbReader(g_sConnectionString,sSql);
				if(objReader.Read())
				{
					return true;
				}
			}
			catch(Exception p_objErr)
			{
				throw new RMAppException(p_objErr.Message, p_objErr);
			}
			finally
			{
				if(objReader!=null)
				{
                    objReader.Close();
					objReader.Dispose();
					objReader=null;
				}
			}
			return false;
		}
		
		private void SetStartAt(int p_iTotalRecordCount, int p_iRecordCount, int p_iPageNumber ,ref int p_iStartAt ,ref int p_iCounterMax )
		{
			int iRec1=0,iSlopOver=0,iFirstPage=0;

			p_iCounterMax=Common.MAXRECORDS;

			iRec1=p_iTotalRecordCount-p_iRecordCount;
			iSlopOver=iRec1%p_iCounterMax;
			iFirstPage=iRec1/p_iCounterMax;

			if(iRec1%p_iCounterMax==0)
			{
				iSlopOver++;
			}

			if(p_iPageNumber==iFirstPage && iSlopOver + p_iRecordCount <= p_iCounterMax)
			{
				p_iStartAt=1;
			}
			else
			{
				if(p_iPageNumber==iFirstPage)
				{
					p_iStartAt=1;
					p_iCounterMax=Common.MAXRECORDS-iSlopOver;
					return;
				}
				else
				{
					if(iSlopOver>0)
					{
						iFirstPage++;
					}
					p_iStartAt=(Common.MAXRECORDS-iSlopOver)+(Common.MAXRECORDS * ((p_iPageNumber - 1) - iFirstPage));
				}
			}
				
		}
	}//end Class
}//end Namespace
