﻿
using System;
using System.Text;
using System.Data;
using System.Globalization;
using System.Collections;
using System.Linq;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.Application.BRS
{
	/**************************************************************
	 * $File		: FloridaHospital.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 10/11/2007
	 * $Author		: Umesh Kusvaha
	 * $Comment		: This class 
	 * $Source		: Riskmaster.Application.BRS
	**************************************************************/
	internal class FloridaHospital
	{
		#region Variable Declarations	

		/// <summary>
		/// Represents the Module variable for Common Class.
		/// </summary>
		private Common m_objCommon = null;

		/// <summary>
		/// Represents the Module variable for Common.LocalCache Class.
		/// </summary>
		private LocalCache m_objLocalCache = null;
        private string m_sZipCode = null;
        private string m_sCPT = null;
        private string m_sTOS = null;
        private string m_sPOS = null;
        
        private double m_dUnitsBilled = 0;
        private int m_iUnitsBilledType = 0;
        private string m_sStateCode = null;
        private double m_dSchdAmount =0;
        private long m_lFeeTableID = 0 ;
        
        private string m_sStudy = null;     //' StudyID
        private string m_sRevision = null;  //  ' ProdCode new to Medicode 2000
        
        public bool bNotCovered = false;  // Valid only after Calculate is called.   
        private string m_sFLLicense = null;
        private int m_iClientId = 0;
		/// <summary>
		/// contain errors/alerts for BRS module, will be set by BRS adaptor and errors/alerts will be displayed onto UI. 
		/// </summary>
		private BusinessAdaptorErrors m_objErrors;

		public BusinessAdaptorErrors Errors
		{
			get
			{
				return m_objErrors;
			}
			set
			{
				m_objErrors=value;
			}
		}
		# endregion

        #region Properties
        public string Study
		{
			set
			{
				m_sStudy = value;
			}
			get
			{
				return m_sStudy;
			}	
		}

        public string Revision
		{
			set
			{
				m_sRevision = value;
			}
			get
			{
				return m_sRevision;
			}	
		}


        public long FeeTableID
		{
			set
			{
				m_lFeeTableID = value;
			}
			get
			{
				return m_lFeeTableID;
			}	
		}

        public double SchdAmount
		{
			set
			{
				 m_dSchdAmount = value;
			}
			get
			{
				return m_dSchdAmount;
			}	
		}

        public string StateCode
		{
			set
			{
				 m_sStateCode = value;
			}
			get
			{
				return m_sStateCode;
			}	
		}

        public string FLLicense
		{
			set
			{
				 m_sFLLicense = value;
			}
			get
			{
				return m_sFLLicense;
			}	
		}

        public int UnitsBilledType
        {
            set
            {
                m_iUnitsBilledType = value;
            }
            get
            {
                return m_iUnitsBilledType;
            }
        }

        public double UnitsBilled
        {
            set
            {
                m_dUnitsBilled = value;
            }
            get
            {
                return m_dUnitsBilled;
            }
        }


        public string POS
        {
            set
            {
                m_sPOS = value;
            }
            get
            {
                return m_sPOS;
            }
        }

        public string TOS
        {
            set
            {
                m_sTOS = value;
            }
            get
            {
                return m_sTOS;
            }
        }

        public string CPT
        {
            set
            {
                m_sCPT = value;
            }
            get
            {
                return m_sCPT;
            }
        }

        public string ZipCode
        {
            set
            {
                m_sZipCode = value;
            }
            get
            {
                return m_sZipCode;
            }
        }

        public bool NotCovered
        {
            set
            {
                bNotCovered = value;
            }
            get
            {
                return bNotCovered;
            }
        }
        # endregion

		#region ModCode Structure

		/// <summary>
		/// Structure containing variables for ModCode.
		/// </summary>
		internal struct ModCode
		{
			internal string sMod;
			internal int iModCode;
			internal double dPercent;
			internal double dPercentMax;
		}
		#endregion
		
		#region Constructor
		/// Name		: FloridaHospital
		/// Author		: Umesh Kusvaha
		/// Date Created: 10/11/2007
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_objCommon">Common Class object instantiated by Manager </param>
        internal FloridaHospital(Common p_objCommon, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_objCommon = p_objCommon;
            m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString, m_iClientId);
		}
		
		#endregion

		#region Internal Methods

        #region internal double CalcFlHospital(FeeSchedule p_objFeeSchedule, Request p_objRequest, ref string p_sStudyId, ref string p_sStateCode, ref string p_sRevision)
        
        /// <summary>
        /// Calculates the amount allowed for Florida Only Hospital
        /// </summary>
        /// <param name="p_objFeeSchedule">Fee Schedule Object</param>
        /// <param name="p_objRequest">Request Object</param>
        /// <param name="p_sStateCode">State Code</param>
        /// <param name="p_sStudyId">Study-Id</param>
        /// <param name="p_sRevision">Revision</param>
		
        internal double CalcFlHospital(FeeSchedule p_objFeeSchedule, Request p_objRequest, 
			ref string p_sStudyId, ref string p_sStateCode, ref string p_sRevision)
        {
            //int variables
            
            int iFeeSchedule = 0;
            int iCnt = 0;
            int iTemp = 0;

            //double variables
            double dReturnValue = -1;
            double dMaxSchdAmount = -1;
            double dFactor = 0.0;
            double dUnitsBilled = 0.0;
            double dBaseSchdAmount = 0.0;
            double dTemp = 0.0;
            double dMaxRlv = 0.0;
            double dTotMod = 0.0;

            //string variables
            string sMods = "";
            string sStateId = "";
            string sShortCode = "";
            string sCodeDesc = "";
            string sEffDate = "";
            string sFeeFlag = "";
            string sBaseDesc = "";
            string sError = "";
            string sERRZipCode = "";

           

            //Collections
            ModCode[] arrModCodes = null;
            DataModel.DataSimpleList objDSList = null;
            

            //objects
            StringBuilder sbSQL = null;
            RelativeValues.RelativeValue objRelativeValue = null;
            Riskmaster.Settings.CCacheFunctions objCacheSettings = null;
            DataSet objDataSet = null;
            DbConnection objConn = null;
            DbReader objDbReader = null;
            try
            {
                iFeeSchedule = p_objFeeSchedule.FeeScheduleId;

                objDSList = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList;

                
                objCacheSettings = new Riskmaster.Settings.CCacheFunctions(m_objCommon.g_sConnectionString,m_iClientId);

                if (m_objLocalCache == null)
                    m_objLocalCache = new LocalCache(m_objCommon.g_sConnectionString,m_iClientId);
                m_objLocalCache.GetStateInfo(p_objFeeSchedule.StateId, ref p_sStateCode, ref sCodeDesc);
                p_sStateCode = m_objLocalCache.GetStateCode(p_objFeeSchedule.StateId);

                sbSQL = new StringBuilder();
                sbSQL.Append(" SELECT FEEFLAG,EFFDATE,STUDYID,PRODCODE ");
                sbSQL.Append(" FROM WRKMAS ");
                sbSQL.Append(" WHERE STATE = '").Append(p_sStateCode).Append("'");
                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {
                    if (objDbReader.Read())
                    {
                        sFeeFlag = Conversion.ConvertObjToStr(objDbReader.GetValue("FEEFLAG"));
                        sEffDate = Conversion.ConvertObjToStr(objDbReader.GetValue("EFFDATE"));
                        p_sStudyId = Conversion.ConvertObjToStr(objDbReader.GetValue("STUDYID"));
                        p_sRevision = Conversion.ConvertObjToStr(objDbReader.GetValue("PRODCODE"));
                    }
                    else
                    {
                        m_objCommon.AddAlert(Globalization.GetString("Common.Constants.ErrState", m_iClientId));
                        //Exit Function
                        return dReturnValue;
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append(" SELECT STATEID FROM WRKZIP WHERE STUDYID = '").Append(p_sStudyId).Append("' ");
                sbSQL.Append(" AND '").Append(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ZipCode);
                sbSQL.Append("' BETWEEN BEGZIP AND ENDZIP ");
                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {
                    if (objDbReader.Read())
                    {
                        sStateId = objDbReader.GetValue("STATEID").ToString();
                    }
                    else
                    {
                        sERRZipCode = Globalization.GetString("Common.Constants.ErrZipCode", m_iClientId);
                        sERRZipCode = sERRZipCode + GetZipErrorMessage(p_objFeeSchedule, p_objRequest, p_sStudyId);
                        m_objCommon.AddAlert(sERRZipCode);
                        //Exit Function
                        return dReturnValue;
                    }
                }

                if(objDbReader!=null)
                objDbReader.Close();

                //loop through modifier codes collection:
                //	1.  create array of ModCodes for use below
                //	2.  create the sMods list of short codes used in SQL statements
                //	3.  calculate NV dblMaxSchdAmount
                if (objDSList.Count > 0)
                {
                    arrModCodes = new ModCode[objDSList.Count];
                    
                    iCnt = 0;
                    foreach (DictionaryEntry objEntry in p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList)
                    {

                        m_objLocalCache.GetCodeInfo(Conversion.ConvertObjToInt(objEntry.Value, m_iClientId), ref sShortCode, ref sCodeDesc);
                        if(iCnt==0)
                            sMods = "'" + sShortCode + "'";
                        else
                            sMods = sMods + ", '" + sShortCode + "'";

                        arrModCodes[iCnt].iModCode = Conversion.ConvertObjToInt(objEntry.Value, m_iClientId);
                        arrModCodes[iCnt].sMod = sShortCode;
                        

                        iCnt++;
                    }//end foreach
                    
                }//end if(objDSList.Count > 0)


                //Get relative value object for studyid/state
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append(" SELECT RVUFEE,POS,TOS,MODIFIER,SPECIALTY,UNIQUESUB,INDICATOR,DESC, ");
                sbSQL.Append(" FOLLUP,SUBCODE,BYREPORT,STARRED,QUESTION,ASSISTSURG,NOTCOVERED ");
                sbSQL.Append(" FROM WRKUVA WHERE STUDYID = '").Append(p_sStudyId).Append("'");
                sbSQL.Append(" AND STATEID = '").Append(sStateId).Append("'");
                sbSQL.Append(" AND [PROC] = '").Append(p_objRequest.CPT).Append("'");
                sbSQL.Append(" ORDER BY UNIQUESUB ");

                bool temp = (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()));
               
                if(temp)
                    {
                      objRelativeValue = GetRelativeValuesFlHospital(p_objFeeSchedule, p_objRequest, p_sStudyId, p_sStateCode, sStateId, sMods, ref objDbReader,ref sError);
                            //MITS 11339 : Umesh
                          if (objRelativeValue == null && sError!="")
                            {
                                //m_objCommon.AddAlert(sError);
                                m_objCommon.AddError(sError);
                                //Exit Function
                                return dReturnValue;
                            }
                            //MITS 11339 : End
                           else
                            {
                        UnitsBilled = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.UnitsBilledNum;
                        if (objRelativeValue.m_sNotCovered.ToUpper() == "NC")
                            NotCovered = true;
                        else
                            NotCovered = false;
                        sBaseDesc = objRelativeValue.m_sDesc;
                        dFactor = 1;
                        dUnitsBilled = UnitsBilled;
                        dBaseSchdAmount = objRelativeValue.m_dAmount * dFactor * dUnitsBilled;
                        dReturnValue = dBaseSchdAmount;






                        dMaxRlv = -1;
                        dTemp = -1;
                        dTotMod = 1;
                        if (objDSList.Count > 0)
                        {
                            
                            GetModifierPercentsFLHosp(p_objFeeSchedule, p_objRequest.CPT, ref arrModCodes, p_sStudyId, sStateId, sMods);
                            iCnt = 0;
                            
                            foreach (DictionaryEntry objEntry in p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ModifierList)
                            {
                                for (iTemp = 0; iTemp <= arrModCodes.Length - 1; iTemp++)
                                {
                                    if (arrModCodes[iTemp].iModCode == Conversion.ConvertObjToInt(objEntry.Value, m_iClientId))
                                        break;
                                }

                                p_objFeeSchedule.GetMaxRLVAndModValue(Conversion.ConvertObjToInt(objEntry.Value, m_iClientId), p_objRequest.CPT, ref arrModCodes[iTemp].dPercentMax, ref dTemp);

                                if (dMaxRlv < dTemp)
                                    dMaxRlv = dTemp;

                                sbSQL.Remove(0, sbSQL.Length);
                                sbSQL.Append("SELECT PERCENT,MODIFIER,BEGPROC,ENDPROC FROM WRKMOD WHERE");
                                sbSQL.Append(" STUDYID = '" + p_sStudyId.ToString() + "'");
                                sbSQL.Append(" AND STATEID = '" + sStateId + "'");
                                sbSQL.Append(" AND MODIFIER = '" + arrModCodes[iTemp].sMod + "'");
                                sbSQL.Append(" AND UNIT IS NULL");
                                sbSQL.Append(" ORDER BY MODIFIER");


                                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                                {
                                    string sTempBEGCPT = "";
                                    string sTempENDCPT = "";
                                    if (objDbReader.Read())
                                    {
                                        sTempBEGCPT = objDbReader.GetValue("BEGPROC").ToString();
                                        sTempENDCPT = objDbReader.GetValue("ENDPROC").ToString();
                                        
                                        if (arrModCodes[iTemp].sMod.ToLower().Trim() == objDbReader.GetValue("MODIFIER").ToString().ToLower().Trim())
                                        {
                                            if (sTempBEGCPT != "")
                                            {
                                                if ((m_objCommon.Val(p_objRequest.CPT) >= m_objCommon.Val(sTempBEGCPT)) &&
                                                    (m_objCommon.Val(p_objRequest.CPT) <= m_objCommon.Val(sTempENDCPT)))
                                                {
                                                    dTemp = 1;
                                                }

                                            }
                                        }

                                        if (arrModCodes[iTemp].dPercent > 0)
                                            dTotMod = dTotMod * arrModCodes[iTemp].dPercent;
                                        else if (arrModCodes[iTemp].dPercentMax > 0)
                                            dTotMod = dTotMod * arrModCodes[iTemp].dPercentMax;

                                    }
                                    else
                                    {
                                        if (dMaxRlv < 0)
                                        {
                                            dTemp = -1;
                                        }
                                        if (arrModCodes[iTemp].dPercentMax > 0)
                                            dTotMod = dTotMod * arrModCodes[iTemp].dPercentMax;
                                    }
                                }



                                //If the max rlv for this mod code not found add an alert
                                if (dTemp == -1)
                                    m_objCommon.AddAlert("Modifier code " + arrModCodes[iTemp].sMod + " value not found for CPT " + p_objRequest.CPT + " and will not reduce the amount.");

                                //Apply the modifier percent for this mod code if it exists in WRKMOD,
                                //otherwise use BRS_MOD_VALUES.MODIFIER_VALUE (fetched in GetMaxRLVAndModValue above)
                                //if(arrModCodes[iTemp].dPercent > 0)
                                //	dTotMod = dTotMod * arrModCodes[iTemp].dPercent;
                                //else if(arrModCodes[iTemp].dPercentMax >0)
                                //	dTotMod = dTotMod * arrModCodes[iTemp].dPercentMax;

                                dTemp = -1;
                            }//for(iCnt = 0 ; arrModCodes.Length - 1; iCnt++)
                            //Reset scheduled amount to the lowest BRS_MOD_VALUES.MAX_RLV if max_rlv is less than amount
                            //	if(dMaxRlv >= 0 && dMaxRlv < objRelativeValue.m_dAmount)
                            //	{
                            //		objRelativeValue.m_dAmount = dMaxRlv;
                            dReturnValue = dReturnValue * dTotMod;
                            //	}
                        }//end if(objDSList.Count > 0)

                        if (dMaxSchdAmount != -1 && dReturnValue > dMaxSchdAmount)
                            dReturnValue = dMaxSchdAmount;
                        }
                    }
                else
                    {
                        NotCovered = false;
                        dBaseSchdAmount = 0.0;
                        dReturnValue = dBaseSchdAmount;

                    }

            }
            catch (RMAppException p_objRmAEx)
            {
                throw p_objRmAEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FloridaHospital.CalcFLHospital.GenericErr", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                    objConn.Dispose();
                objConn = null;

                if (objDataSet != null)
                    objDataSet.Dispose();
                objDataSet = null;

                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                objDbReader = null;
                if (m_objLocalCache != null)
                    m_objLocalCache.Dispose();

                sbSQL = null;
                objCacheSettings = null;
                objDSList = null;
                objRelativeValue = null;
            }

            //Return value to the calling function
            return dReturnValue;

        }//end function

        #endregion

        #endregion

        #region Private Methods

        #region GetZipErrorMessage(p_objFeeSchedule,p_objRequest,p_sStudyId)
        
        /// <summary>
        /// Gets the error message based on zip code
        /// </summary>
        /// <param name="p_objFeeSchedule">Fee Schedule object</param>
        /// <param name="p_objRequest">Request class object</param>
        /// <param name="p_sStudyId">Study Id</param>
        private string GetZipErrorMessage(FeeSchedule p_objFeeSchedule, Request p_objRequest, string p_sStudyId)
        {
            //string Variables
            string sReturnValue = "";
            string sSqlStudy = "";

            //objects
            StringBuilder sbSQL = null;
            DbReader objDbReader = null;
            try
            {
                if ((int)p_objFeeSchedule.ScheduleType == Common.WC00)
                    sSqlStudy = "STUDYID";
                else
                    sSqlStudy = "STUDY";

                sbSQL = new StringBuilder();

                //Table[0]
                sbSQL.Append(" SELECT MIN(BEGZIP) AS MINZIP, MAX(ENDZIP) AS MAXZIP ");
                sbSQL.Append(" FROM WRKZIP WHERE ").Append(sSqlStudy).Append(" = '").Append(p_sStudyId).Append("'");

                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {
                    if (objDbReader.Read())
                    {
                        sReturnValue = "(Valid Zip Codes From ";
                        sReturnValue = sReturnValue + (objDbReader.GetValue("MINZIP").ToString());
                        sReturnValue = sReturnValue + " To " + (objDbReader.GetValue("MAXZIP").ToString());
                        sReturnValue = sReturnValue + ").";
                    }
                }

                objDbReader = null;
                //Table[1]
                //sbSQL.Append(" \r\n ");
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append(" SELECT Count(*) FROM PROVIDER_CONTRACTS ");
                sbSQL.Append(" WHERE PROVIDER_EID = ").Append(p_objRequest.BillItemObject.PayeeId);


                //				if(m_objCommon.GetDbReader(ref objDbReader,sbSQL.ToString()) && p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ContractExists > 0)
                //                        sReturnValue = sReturnValue + Globalization.GetString("CalculateWC.GetZipErrorMessage.Alert1");
                if ((p_objRequest.Common.AnyContractExits(p_objRequest.BillItemObject.PayeeId)) && p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.ContractExists > 0)
                    sReturnValue = sReturnValue + Globalization.GetString("CalculateWC.GetZipErrorMessage.Alert1", m_iClientId);


            }//end try
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                sbSQL = null;
                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader.Close();
                    objDbReader = null;
                }
            }
            //Return the result to calling function
            return sReturnValue;
        }//end function

        #endregion

        #region GetModifierPercentsFLHosp(p_objFeeSchedule, p_sCPT,arrModCode,p_sStudyId, p_sStateId, p_sMode)
        /// Name			: GetModifierPercents for FL Hospital
        /// Author			: Umesh Kusvaha
        /// Date Created	: 11/11/2007		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Gets the Modifier Percentage
        /// </summary>
        /// <param name="p_objFeeSchedule">Fee Schedule class object</param>
        /// <param name="p_sCPT">CPT</param>
        /// <param name="arrModCode">Mod Code Array</param>
        /// <param name="p_sMode">Mode</param>
        /// <param name="p_sStateId">State-Id</param>
        /// <param name="p_sStudyId">Study-Id</param>
        private void GetModifierPercentsFLHosp(FeeSchedule p_objFeeSchedule, string p_sCPT, ref ModCode[] arrModCode,
            string p_sStudyId, string p_sStateId, string p_sMode)
        {
            int iCnt = 0;
            double dPercent = 0.0;

            //string variable
            string sTempBEGCPT = "";
            string sTempENDCPT = "";
            string sMod = "";

            //objects
            StringBuilder sbSQL = null;
            DbReader objDbReader = null;

            sbSQL = new StringBuilder();
            try
            {

                //Table[0]
                sbSQL.Append(" SELECT PERCENT,MODIFIER,BEGPROC,ENDPROC FROM WRKMOD WHERE ");
                sbSQL.Append(" STUDYID = '").Append(p_sStudyId).Append("'");
                sbSQL.Append(" AND STATEID = '").Append(p_sStateId).Append("'");
                sbSQL.Append(" AND MODIFIER IN (").Append(p_sMode).Append(")");
                sbSQL.Append(" AND UNIT IS NULL ORDER BY MODIFIER ");

                if (m_objCommon.GetDbReader(ref objDbReader, sbSQL.ToString()))
                {
                    while (objDbReader.Read())
                    {
                        dPercent = -1;
                        sTempBEGCPT = objDbReader.GetValue("BEGPROC").ToString();
                        sTempENDCPT = objDbReader.GetValue("ENDPROC").ToString();
                        sMod = objDbReader.GetValue("MODIFIER").ToString();
                        if (sTempBEGCPT != "")
                        {
                            if ((m_objCommon.Val(p_sCPT) >= m_objCommon.Val(sTempBEGCPT)) &&
                                (m_objCommon.Val(p_sCPT) <= m_objCommon.Val(sTempENDCPT)))
                                dPercent = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENT").ToString()) / 100;
                        }//if(sTempBEGCPT != "")
                        else
                            dPercent = Conversion.ConvertStrToDouble(objDbReader.GetValue("PERCENT").ToString()) / 100;

                        if (dPercent != -1)
                        {
                            for (iCnt = 0; iCnt <= arrModCode.Length - 1; iCnt++)
                            {
                                if (arrModCode[iCnt].sMod.Trim().ToLower() == sMod.Trim().ToLower())
                                    arrModCode[iCnt].dPercent = dPercent;
                            }
                        }//end if(dPercent != -1)
                    }
                }//end if(objDbReader != null)
            }
            catch (RMAppException p_objRmAEx)
            {
                throw p_objRmAEx;
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                    sbSQL = null;
                }

            }
        }//end function
        #endregion


        #region GetRelativeValuesFlHospital(p_objFeeSchedule,p_objRequest,p_sStudyId, p_sStateId, p_sCalcMods)

        private RelativeValues.RelativeValue GetRelativeValuesFlHospital(FeeSchedule p_objFeeSchedule, Request p_objRequest, string p_sStudyId,
            string p_sStateCode, string p_sStateId, string p_sCalcMods,ref DbReader p_objDbReader, ref string p_sError)
        {
            //Return Variable
            Hashtable hstReturnValue = null;

            //string variables
            string sCalcPos = "";
            string sCalcTos = "";
            string sPos = "";
            string sTos = "";
            string sMod = "";
            string sPrevPOS = "";
            string sPrevTOS = "";
            string sPrevMOD = "";
            //int variables
            int iMatch = 0;
            int iMaxMatch = 0;
            int iCnt = 0;
            int iFinalMatch = 0;
            bool bPosMatch = false;
            bool bTosMatch = false;
            bool bModMatch = false;
            //objects
            RelativeValues.RelativeValue objRelativeValue = null;
            StringBuilder sbSQL = null;
            DbReader objDbReader1 = null;
            bool bFirst = true;

            try
            {
                hstReturnValue = new Hashtable();
                sbSQL = new StringBuilder();

                //fill short codes from g_objCalculate code values
                //these short codes are filled by CCalculate because are referred to in great number of places
                int iCalcTos = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode;
                int iCalcPos = p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode;
                if (iCalcTos == 0)
                    sCalcTos = "";
                else
                    sCalcTos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.TypeOfSerCode);

                if (iCalcPos == 0)
                    sCalcPos = "";
                else
                    sCalcPos = m_objLocalCache.GetShortCode(p_objRequest.BillItemObject.objSplit.BrsInvoiceDetail.PlaceOfSerCode);

                //highest number of matches on the 3 criteria
                iMaxMatch = 0;

                while (p_objDbReader.Read())
                {
                    iMatch = 0;
                    sPos = p_objDbReader.GetValue("POS").ToString();
                    sTos = p_objDbReader.GetValue("TOS").ToString();
                    sMod = p_objDbReader.GetValue("MODIFIER").ToString();
                    bPosMatch = false;
                    bTosMatch = false;
                    bModMatch = false;

                    if (sPos == sCalcPos)
                    {
                        iMatch = iMatch + 1;
                        bPosMatch = true;
                    }
                    else
                    {
                        if (sPos.Trim() == "")
                        {
                            iMatch = iMatch + 1;
                            bPosMatch = true; 

                        }
                    }

                    if (sTos == sCalcTos)
                    {
                        iMatch = iMatch + 1;
                        bTosMatch = true;
                    }
                    else
                    {
                        if (sTos.Trim() == "")
                        {
                            iMatch = iMatch + 1;
                            bTosMatch = true;
                        }
                    }

                    if ((p_sCalcMods.Trim() == "" && sMod.Trim() == "") || (p_sCalcMods.IndexOf("'" + sMod + "'", 0) != -1))
                    {

                        iMatch = iMatch + 1;
                        bModMatch = true;
                    }
                    else
                    {
                        if (sMod.Trim() == "")
                        {
                            bModMatch = true;
                        }
                    }

                    if (iMaxMatch < iMatch)
                        iMaxMatch = iMatch;
                    if (bPosMatch == true && bTosMatch == true && bModMatch == true && iMatch == iMaxMatch && iMatch > 0)
                    {
                        objRelativeValue = new RelativeValues.RelativeValue();
                        objRelativeValue.m_iMatches = iMatch;
                        objRelativeValue.m_sPOS = sPos;
                        objRelativeValue.m_sTOS = sTos;
                        objRelativeValue.m_sMODS = sMod;
                        objRelativeValue.m_dAmount = Conversion.ConvertStrToDouble(p_objDbReader.GetValue("RVUFEE").ToString());
                        objRelativeValue.m_sSpecialty = p_objDbReader.GetValue("SPECIALTY").ToString();
                        objRelativeValue.m_sUniqueSub = p_objDbReader.GetValue("UNIQUESUB").ToString();
                        objRelativeValue.m_sIndicator = p_objDbReader.GetValue("INDICATOR").ToString();
                        objRelativeValue.m_sDesc = p_objDbReader.GetValue("DESC").ToString();
                        objRelativeValue.m_sSubCode = p_objDbReader.GetValue("SUBCODE").ToString();
                        objRelativeValue.m_sByReport = p_objDbReader.GetValue("BYREPORT").ToString();
                        objRelativeValue.m_sStarred = p_objDbReader.GetValue("STARRED").ToString();
                        objRelativeValue.m_sQuestion = p_objDbReader.GetValue("QUESTION").ToString();
                        objRelativeValue.m_sAssistSurg = p_objDbReader.GetValue("ASSISTSURG").ToString();
                        objRelativeValue.m_sNotCovered = p_objDbReader.GetValue("NOTCOVERED").ToString();

                        if (Conversion.IsNumeric(p_objDbReader.GetValue("FOLLUP").ToString()))
                            objRelativeValue.m_iFollup =
                                Conversion.ConvertStrToInteger(p_objDbReader.GetValue("FOLLUP").ToString());
                        else
                            objRelativeValue.m_sGlblCode = p_objDbReader.GetValue("FOLLUP").ToString();

                        //Add to collection
                        if (bFirst || (sPrevPOS == sPos && sPrevTOS == sTos && sPrevMOD == sMod))
                        {
                            hstReturnValue.Add(iCnt, objRelativeValue);
                            objRelativeValue = null;
                        }
                        else
                        {
                            hstReturnValue.Clear();
                        }

                        bFirst = false;
                        sPrevPOS = sPos;
                        sPrevTOS = sTos;
                        sPrevMOD = sMod;
                        iCnt++;
                        iFinalMatch = iMatch;
                    }
                }

                if (hstReturnValue.Count > 0)
                {
                    objRelativeValue = new RelativeValues.RelativeValue();
                    int maxKey = hstReturnValue.Keys.Cast<int>().OrderByDescending(x => x).First();
                    objRelativeValue = (RelativeValues.RelativeValue)hstReturnValue[maxKey];
                }
                else
                {
                    objRelativeValue = null;
                    p_sError = Globalization.GetString("Common.Constants.ErrTos",m_iClientId);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException("Error while getting Relative Valus.", p_objException);
            }
            finally
            {
                
                if (objDbReader1 != null)
                {
                    objDbReader1.Close();
                    objDbReader1.Dispose();
                }
            }

            //Return the result to calling function
            return objRelativeValue;
        }

        # endregion

		# endregion

		#region Destructor
        ~FloridaHospital()
		{
			m_objCommon = null;
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
                m_objLocalCache = null;
            }
		}
		# endregion

	}//End Class
}//End Namespace
