using System;
using System.IO;
using C1.C1Report;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.Sentinel
{	
	/// <summary>	
	///	This class contains methods that will generate the Sentinel Report in PDF Format.		
	/// </summary>
	internal class PrintReport
	{
		
		#region Member Variables		
		/// <summary>		
		/// Sentinel Report
		/// </summary>
		private C1Report m_objSentinelReport = null;
        private int m_iClientId=0;

		#endregion
		
		#region Constructor
		/// Name			: PrintReport
		/// Author			: Neelima Dabral
		/// Date Created	: 03-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sReportTemplateFile">
		///		File name for the report template.
		/// </param>	
		internal PrintReport(string p_sReportTemplateFile, int p_iClientId)
		{
			m_objSentinelReport = new C1Report();						
			m_objSentinelReport.Clear();			
			m_objSentinelReport.Load(p_sReportTemplateFile,"EVENT_SENTINEL_REPORT");			
			m_objSentinelReport.Layout.MarginLeft=100;
			m_objSentinelReport.Layout.MarginRight=0;
			m_objSentinelReport.Layout.MarginBottom=0;
			m_objSentinelReport.Layout.MarginTop=0;
			m_objSentinelReport.Layout.Orientation = OrientationEnum.Portrait;
            m_iClientId = p_iClientId;
		}

		#endregion

		#region Methods

		/// Name			: SetFont
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// This method will set the font for the report.		
		/// </summary>
		/// <param name="p_sFont">Font name</param>	
		internal void SetFont(string p_sFont)
		{
			try
			{			
				m_objSentinelReport.Font.Name = p_sFont;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(Globalization.GetString("PrintReport.SetFont.ErrorSet", m_iClientId),p_objException);
			}
		}

		/// Name			: RenderToFile
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method will render the PDF report to a specified file.
		/// </summary>
		/// <param name="p_sReportFile">Filename</param>					
		internal void RenderToFile(string p_sReportFile)
		{
			try
			{
				m_objSentinelReport.RenderToFile(p_sReportFile,FileFormatEnum.PDF);
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("PrintReport.RenderToFile.Error", m_iClientId), p_objException);
			}
			finally
			{
				m_objSentinelReport = null;
			}
		}

		/// Name			: RenderToStream
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will render the PDF report to memory stream.
		/// </summary>
		/// <returns>Memory Stream</returns>
		internal MemoryStream RenderToStream()
		{
			MemoryStream objPDFMemoryStream=null;
			try
			{
				objPDFMemoryStream=new MemoryStream();
				m_objSentinelReport.RenderToStream(objPDFMemoryStream,FileFormatEnum.PDF);
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("PrintReport.RenderToStream.Error", m_iClientId), p_objException);
			}	
			finally
			{
				m_objSentinelReport = null;
			}
			return objPDFMemoryStream;
		}

		/// Name			: PrintData
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will populate the specified report field with specified data.
		/// </summary>		
		/// <param name="p_sFieldName">Name of the field to be populated</param>
		/// <param name="p_sFieldData">Data with which the specific field will be populated</param>
		internal void PrintData(string p_sFieldName,string p_sFieldData)
		{
			FieldCollection  objField=null;	
			try
			{
				objField = m_objSentinelReport.Fields;
				objField[p_sFieldName].Text = p_sFieldData;
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("PrintReport.PrintData.Error", m_iClientId), p_objException);
			}				
			finally
			{
				objField=null;	
			}
		}
		
		/// Name			: PrintImage
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will print the specified image in the report.
		/// </summary>		
		/// <param name="p_sFieldName">Name of the field which that contain the picture</param>
		/// <param name="p_sImageFile">
		///		Name of the file contains the image to be printed
		///		It will be the fully qualified path of the image file.
		/// </param>
		internal void PrintImage(string p_sFieldName,string p_sImageFile)
		{
			FieldCollection  objField=null;	
			try
			{
				objField = m_objSentinelReport.Fields;
				objField[p_sFieldName].Picture = p_sImageFile;
				objField[p_sFieldName].PictureAlign =PictureAlignEnum.Stretch;
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("PrintReport.PrintImage.Error", m_iClientId), p_objException);
			}				
			finally
			{
				objField=null;	
			}
		}

		/// Name			: ShowField
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will hide or show a specified field in the report.
		/// </summary>		
		/// <param name="p_sFieldName">Name of the field to show or hide</param>
		/// <param name="p_bHideField">True (Show field) / False (Hide field)</param>
		internal void ShowField(string p_sFieldName,bool p_bHideField)
		{
			FieldCollection  objField=null;	
			try
			{
				objField = m_objSentinelReport.Fields;
				objField[p_sFieldName].Visible = p_bHideField;
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("PrintReport.ShowField.Error", m_iClientId), p_objException);
			}				
			finally
			{
				objField=null;	
			}
		}

		#endregion
	}
}
