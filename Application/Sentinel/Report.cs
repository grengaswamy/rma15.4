using System;
using System.IO;
using System.Text;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;


namespace Riskmaster.Application.Sentinel
{
	///************************************************************** 
	///* $File				: Report.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 03-Dec-2004
	///* $Author			: Neelima Dabral
	///***************************************************************	
	
	/// <summary>	
	///	This class contains methods to fetch the data related to a specific event and event sentinel 
	///	and then generate the Root Cause Analysis Report for that event.	
	/// </summary>
	
	public class Report
	{
		#region Member Variables
		
		/// <summary>
		/// Database Connection string
		/// </summary>
		private string m_sConnectionString = "";

		/// <summary>
		/// Event Id for which the �Root Cause Analysis Report� has to be generated
		/// </summary>
		private int m_iEventId =0;

		/// <summary>		
		/// Root Cause Analysis report
		/// </summary>
		private PrintReport	m_objSentinelReport=null;	

		/// <summary>
		/// Name of the XML file that will be template for the report to be generated,
		/// It will contain the fully qualified path along with the file name.
		/// </summary>
		private string m_sReportTemplateFile = "";

		/// <summary>
		/// Name of the file containing the logo of JCAHO that will be displayed on the report,
		/// It will contain the fully qualified path along with the file name.
		/// </summary>
		private string m_sJCAHOImgFile  = "";

        /// <summary>
        /// Language Code 
        /// </summary>
        private string m_sLangCode = "";

        /// <summary>
        /// ClientId for cloud environment
        /// </summary>
        private int m_iClientId=0;

		#endregion

		#region Properties

		/// <summary>
		/// It accesses the value of the file name for the report template.
		/// </summary>
		public string TemplateFileName{get{return m_sReportTemplateFile;}set{m_sReportTemplateFile = value;}}

		/// <summary>
		/// It accesses the value of the file name for the JCAHO image.
		/// </summary>
		public string JCAHOImagePath{get{return m_sJCAHOImgFile;}set{m_sJCAHOImgFile = value;}}

        public string LangCode { get { return m_sLangCode; } set { m_sLangCode = value; } }
		#endregion

		#region Constructor

		/// Name			: Report
		/// Author			: Neelima Dabral
		/// Date Created	: 03-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnectionString">
		///		Database Connection string
		/// </param>	
		public Report(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString =  p_sConnectionString;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Methods

		/// Name			: GetRootCauseAnalysisReport
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will call methods to generate the Root Cause Analysis
		///		Report for the specified Event. It will return the binary stream for the report generated.
		/// </summary>
		/// <param name="p_iEventId">
		///		Event Id for which report will be generated
		/// </param>
		/// <param name="p_objSentinelReport">
		///		Binary Stream corresponding to the report.
		/// </param>		
        public void GetRootCauseAnalysisReport(int p_iEventId, out MemoryStream p_objSentinelReport)
		{
			string			sSentinelDesc="";
			int				iReportLevel=0;
			int				iDeptEid=0;
			string			sEventDate="";
			try
			{
				m_iEventId =  p_iEventId ;
				InitializeReport();		
				PrintRootAnalysis(ref sSentinelDesc, ref iReportLevel,ref iDeptEid,ref sEventDate);
				PrintJCAHO(sSentinelDesc,iReportLevel,iDeptEid,sEventDate);
				p_objSentinelReport = 	m_objSentinelReport.RenderToStream();			
			}				
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
					(CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.GetRootCauseAnalysisReport.Error", m_iClientId),m_sLangCode),p_objException);
			}
			finally
			{
				m_objSentinelReport = null;			
				sSentinelDesc = null;
			}			
		}
		
		/// Name			: GetRootCauseAnalysisReport
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will call methods to generate the Root Cause Analysis
		///		Report for the specified Event. 
		///		It will copy the contents of report to the specified file.
		/// </summary>
		/// <param name="p_iEventId">
		///		Event Id for which report will be generated
		/// </param>
		///<param name="p_sFilName">
		///		Name of the file to which the generated Root Cause Analysis reports needs to be copied.
		///</param>				
		public void GetRootCauseAnalysisReport(int p_iEventId, string p_sFilName)
		{
			string			sSentinelDesc="";
			int				iReportLevel=0;
			int				iDeptEid=0;
			string			sEventDate="";

			try
			{
				m_iEventId =  p_iEventId ;
				InitializeReport();						
				PrintRootAnalysis(ref sSentinelDesc, ref iReportLevel,ref iDeptEid,ref sEventDate);
				PrintJCAHO(sSentinelDesc,iReportLevel,iDeptEid ,sEventDate);
				m_objSentinelReport.RenderToFile(p_sFilName);			
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.GetRootCauseAnalysisReport.Error", m_iClientId), m_sLangCode), p_objException);
			}
			finally
			{
				m_objSentinelReport = null;				
				sSentinelDesc = null;
			}			
		}
		
		/// Name			: InitializeReport
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will initialize the report properties like Font etc.		
		/// </summary>		
		private void InitializeReport()
		{
			try
			{
				if ((m_sConnectionString == null) || (m_sConnectionString.Trim() == ""))				
					throw new InvalidValueException
                        (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.InvalidConnString", m_iClientId), m_sLangCode));
				
				if ((m_sReportTemplateFile == null) || (m_sReportTemplateFile.Trim() == ""))				
					throw new InvalidValueException
                        (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.InvalidTemplateFileName", m_iClientId), m_sLangCode));
				
				if ((m_sJCAHOImgFile  == null) || (m_sJCAHOImgFile.Trim() == ""))
					throw new InvalidValueException
                        (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.InvalidJCAHOImg", m_iClientId), m_sLangCode));
				
				if (!File.Exists(m_sReportTemplateFile ))
					throw new FileInputOutputException
                        (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.ReportTemplateNotFound", m_iClientId), m_sLangCode));

                m_objSentinelReport = new PrintReport(m_sReportTemplateFile, m_iClientId);
				m_objSentinelReport.SetFont("Arial");								
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.InitializeReport.Error", m_iClientId), m_sLangCode), p_objException);
			}			
		}


		/// Name			: PrintRootAnalysis
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will print the data related to Root Cause Analysis of the specified event. 		
		/// </summary>
		/// <param name="p_sSentinelDesc">Sentinel Description</param>
		/// <param name="p_iReportLevel">Organization level (Client/Company etc)</param>
		/// <param name="p_iDeptEid">Department EID where event occured</param>
		/// <param name="p_sEventDate">Date Of Event</param>
		private void PrintRootAnalysis(ref string p_sSentinelDesc,ref int p_iReportLevel,ref int p_iDeptEid,
																		ref string p_sEventDate)
		{
			
			string			sCompanyName = "";
			string			sEventNumber = "";			
			string			sDateReported = "";
			string			sTimeReported = "";									
			string			sDamageCons  = "";		
			string			sDiffChanged  = "";		
			int				iProcFollowed = 0;
			int				iAvoidWorse =  0;
			int				iStaffKnowHandle  = 0;
			int				iHappenedBefore  = 0;
			string			sPrevFix  = "";			
			string			sUnusualRep  = "";
			string			sComments = "";
			string			sPrevent="";
			StringBuilder	sbDataString=null;			
			StringBuilder	sbSql=null;
			DbReader		objReader = null;
			try
			{
				#region Fetching the Event related data to be shown in the report
				sbSql = new StringBuilder();
				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT CLIENT_NAME FROM SYS_PARMS");				
				objReader = DbFactory.GetDbReader(m_sConnectionString ,sbSql.ToString());
				if(objReader.Read())
					sCompanyName = objReader.GetString("CLIENT_NAME");
				objReader.Close();

				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT EVENT_NUMBER, DATE_OF_EVENT, DATE_REPORTED, TIME_REPORTED,");				
				sbSql.Append(" DEPT_EID FROM EVENT WHERE EVENT_ID=" + m_iEventId);
				objReader = DbFactory.GetDbReader(m_sConnectionString ,sbSql.ToString());
				if(objReader.Read())
				{
					sEventNumber = objReader.GetString("EVENT_NUMBER").Trim();					
					p_sEventDate = objReader.GetString("DATE_OF_EVENT").Trim();
					sDateReported = objReader.GetString("DATE_REPORTED").Trim();
					sTimeReported = objReader.GetString("TIME_REPORTED").Trim();
					if (!( objReader.GetValue("DEPT_EID") is System.DBNull))
						p_iDeptEid	  = Conversion.ConvertObjToInt(objReader.GetValue("DEPT_EID"), m_iClientId);					
					
				}
				objReader.Close();
				#endregion

				#region Printing the Event related data to the report and fetching the Sentinel related data

				//Print Page Header
                //Changed by Gagan for MITS 18945 : start
                m_objSentinelReport.PrintData("PageHeader1", CommonFunctions.FilterBusinessMessage(Globalization.GetString("PageHeader1", m_iClientId), m_sLangCode));
                //Changed by Gagan for MITS 18945 : end

				m_objSentinelReport.PrintData("PageHeader2",sCompanyName);
				//m_objSentinelReport.PrintData("PageHeader3",System.DateTime.Now.ToString("d"));
                m_objSentinelReport.PrintData("PageHeader3", Conversion.GetUIDate(System.DateTime.Now.ToString(), m_sLangCode, m_iClientId));
				//Print Page Footer				
                m_objSentinelReport.PrintData("ConfidentialData", CommonFunctions.FilterBusinessMessage(Globalization.GetString("PageFooter", m_iClientId), m_sLangCode));				

                sbDataString = new StringBuilder();

				sbDataString.Remove(0,sbDataString.Length);
                sbDataString.Append(" " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("EventNumber", m_iClientId), m_sLangCode) + " ");
                sbDataString.Append(sEventNumber + "  " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("EventDate", m_iClientId), m_sLangCode));
                sbDataString.Append(" " + Conversion.GetUIDate(p_sEventDate, m_sLangCode, m_iClientId));
                //sbDataString.Append(" " +VBFunctions.VBMid(p_sEventDate,5,2));
                //sbDataString.Append("/" + VBFunctions.VBRight(p_sEventDate,2));
                //sbDataString.Append("/" + VBFunctions.VBLeft(p_sEventDate,4)); 

				m_objSentinelReport.PrintData("EventDetail",sbDataString.ToString());
				
				sbSql.Remove(0,sbSql.Length);
				sbSql.Append("SELECT SENT_EVENT_DESC, DAMAGE_CONS, DIFF_CHANGED, PREVENT, PROC_FOLLOWED,");				
				sbSql.Append("AVOID_WORSE, STAFF_KNOW_HANDLE, HAPPENED_BEFORE, PREVIOUS_FIX, UNUSUAL_REPORT,");
				sbSql.Append("COMMENTS,REPORT_LEVEL FROM EVENT_SENTINEL WHERE EVENT_ID=" + m_iEventId);				
				objReader = DbFactory.GetDbReader(m_sConnectionString ,sbSql.ToString());
				if(objReader.Read())
				{
					p_sSentinelDesc = objReader.GetString("SENT_EVENT_DESC").Trim();					
					sDamageCons = objReader.GetString("DAMAGE_CONS").Trim();
					sDiffChanged = objReader.GetString("DIFF_CHANGED").Trim();
					sPrevent = objReader.GetString("PREVENT").Trim();
					if (!( objReader.GetValue("PROC_FOLLOWED") is System.DBNull))
						iProcFollowed = Conversion.ConvertObjToInt(objReader.GetValue("PROC_FOLLOWED"), m_iClientId);

					if (!( objReader.GetValue("AVOID_WORSE") is System.DBNull))
						iAvoidWorse = Conversion.ConvertObjToInt(objReader.GetValue("AVOID_WORSE"), m_iClientId);

					if (!( objReader.GetValue("STAFF_KNOW_HANDLE") is System.DBNull))
						iStaffKnowHandle = Conversion.ConvertObjToInt(objReader.GetValue("STAFF_KNOW_HANDLE"), m_iClientId);

					if (!( objReader.GetValue("HAPPENED_BEFORE") is System.DBNull))
						iHappenedBefore = Conversion.ConvertObjToInt(objReader.GetValue("HAPPENED_BEFORE"), m_iClientId);
					sPrevFix = objReader.GetString("PREVIOUS_FIX").Trim();					
					sUnusualRep = objReader.GetString("UNUSUAL_REPORT").Trim();
					sComments = objReader.GetString("COMMENTS").Trim();
					if (!( objReader.GetValue("REPORT_LEVEL") is System.DBNull))
						p_iReportLevel = Common.Conversion.ConvertObjToInt(objReader.GetValue("REPORT_LEVEL"), m_iClientId);
				}					
				objReader.Close();

                m_objSentinelReport.PrintData("LblInfo1", CommonFunctions.FilterBusinessMessage(Globalization.GetString("EventDesc", m_iClientId), m_sLangCode));				
				m_objSentinelReport.PrintData("Info1",PrintMemoString(p_sSentinelDesc,100));

                m_objSentinelReport.PrintData("LblInfo2", CommonFunctions.FilterBusinessMessage(Globalization.GetString("DamageCons", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("Info2",PrintMemoString(sDamageCons,100));

                m_objSentinelReport.PrintData("LblInfo3", CommonFunctions.FilterBusinessMessage(Globalization.GetString("DiffChanged", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("Info3",PrintMemoString(sDiffChanged,100));

                m_objSentinelReport.PrintData("LblInfo4", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Prevent", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("Info4",PrintMemoString(sPrevent,100));
				
				if(iProcFollowed != 0)
                    m_objSentinelReport.PrintData("Info5", CommonFunctions.FilterBusinessMessage(Globalization.GetString("ProcFollowed", m_iClientId), m_sLangCode));					
				else
                    m_objSentinelReport.PrintData("Info5", CommonFunctions.FilterBusinessMessage(Globalization.GetString("ProcNotFollowed", m_iClientId), m_sLangCode));									
				
				if(iAvoidWorse != 0)
                    m_objSentinelReport.PrintData("Info6", CommonFunctions.FilterBusinessMessage(Globalization.GetString("WorseAvoided", m_iClientId), m_sLangCode));					
				else
                    m_objSentinelReport.PrintData("Info6", CommonFunctions.FilterBusinessMessage(Globalization.GetString("WorseNotAvoided", m_iClientId), m_sLangCode));				
			
				if(iStaffKnowHandle != 0)
                    m_objSentinelReport.PrintData("Info7", CommonFunctions.FilterBusinessMessage(Globalization.GetString("StaffKnowHandled", m_iClientId), m_sLangCode));					
				else
                    m_objSentinelReport.PrintData("Info7", CommonFunctions.FilterBusinessMessage(Globalization.GetString("StaffKnowNotHandled", m_iClientId), m_sLangCode));
				
				if(iHappenedBefore != 0)
				{
                    m_objSentinelReport.PrintData("Info8", CommonFunctions.FilterBusinessMessage(Globalization.GetString("HappenedBefore", m_iClientId), m_sLangCode));
                    m_objSentinelReport.PrintData("LblInfo8b", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Changes", m_iClientId), m_sLangCode));					
					m_objSentinelReport.PrintData("Info8b",PrintMemoString(sPrevFix,100));

					sbDataString.Remove(0,sbDataString.Length);
                    sbDataString.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ReportedOn", m_iClientId), m_sLangCode)); 								
                    //sbDataString.Append(" " + VBFunctions.VBMid(sDateReported,5,2));
                    //sbDataString.Append("/" + VBFunctions.VBRight(sDateReported,2));
                    //sbDataString.Append("/" + VBFunctions.VBLeft(sDateReported,4));
                    sbDataString.Append(" " + Conversion.GetUIDate(sDateReported, m_sLangCode,m_iClientId));
                    sbDataString.Append(" " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("At", m_iClientId), m_sLangCode) + " "); 								
					sbDataString.Append(Common.Conversion.GetTimeAMPM(sTimeReported));

					m_objSentinelReport.PrintData("Info9",sbDataString.ToString());

                    m_objSentinelReport.PrintData("LblInfo10", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Unusual", m_iClientId), m_sLangCode));
					m_objSentinelReport.PrintData("Info10",PrintMemoString(sUnusualRep,100));

                    m_objSentinelReport.PrintData("LblInfo11", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Comments", m_iClientId), m_sLangCode));
					m_objSentinelReport.PrintData("Info11",PrintMemoString(sComments,100));

					m_objSentinelReport.ShowField("Info9Hid",false);
					m_objSentinelReport.ShowField("LblInfo10Hid",false);
					m_objSentinelReport.ShowField("Info10Hid",false);
					m_objSentinelReport.ShowField("LblInfo11Hid",false);
					m_objSentinelReport.ShowField("Info11Hid",false);
					
				}
				else
				{
                    m_objSentinelReport.PrintData("Info8", CommonFunctions.FilterBusinessMessage(Globalization.GetString("NotHappenedBefore", m_iClientId), m_sLangCode));
					sbDataString.Remove(0,sbDataString.Length);
                    sbDataString.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ReportedOn", m_iClientId), m_sLangCode)); 								
                    //sbDataString.Append(" " + VBFunctions.VBMid(sDateReported,5,2));
                    //sbDataString.Append("/" + VBFunctions.VBRight(sDateReported,2));
                    //sbDataString.Append("/" + VBFunctions.VBLeft(sDateReported,4));
                    sbDataString.Append(" " + Conversion.GetUIDate(sDateReported, m_sLangCode, m_iClientId));
                    sbDataString.Append(" " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("At", m_iClientId), m_sLangCode) + " "); 								
					sbDataString.Append(Common.Conversion.GetTimeAMPM(sTimeReported));

					m_objSentinelReport.PrintData("Info9Hid",sbDataString.ToString());

                    m_objSentinelReport.PrintData("LblInfo10Hid", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Unusual", m_iClientId), m_sLangCode));
					m_objSentinelReport.PrintData("Info10Hid",PrintMemoString(sUnusualRep,100));

                    m_objSentinelReport.PrintData("LblInfo11Hid", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Comments", m_iClientId), m_sLangCode));
					m_objSentinelReport.PrintData("Info11Hid",PrintMemoString(sComments,100));
					
					m_objSentinelReport.ShowField("LblInfo8b",false);
					m_objSentinelReport.ShowField("Info8b",false);

					m_objSentinelReport.ShowField("Info9",false);
					m_objSentinelReport.ShowField("LblInfo10",false);
					m_objSentinelReport.ShowField("Info10",false);
					m_objSentinelReport.ShowField("LblInfo11",false);
					m_objSentinelReport.ShowField("Info11",false);					
				}		
				#endregion

			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.PrintRootAnalysis.Error", m_iClientId), m_sLangCode), p_objException);
			}
			finally
			{
				if (objReader != null)				
					objReader.Dispose();				
				sbSql = null;
				sbDataString = null;
			}
		}
		/// Name			: PrintJCAHO
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		///<summary>
		///		This method will print the information related to JCAHO.
		///</summary>		
		///<param name="p_sSentinelDesc">Sentinel Description</param>
		///<param name="p_iReportLevel">Organization level (Client/Company etc)</param>
		///<param name="p_iDeptEid">Department EID where event occured</param>
		///<param name="p_sEventDate">Date Of Event</param>
		private void PrintJCAHO(string p_sSentinelDesc,int p_iReportLevel,int p_iDeptEid,
																string p_sEventDate)
		{
			StringBuilder	sbSql=null;
			DbReader		objReader = null;
			StringBuilder	sbDataString = null;
			string			sCompanyName = "";
			string			sCompanyAddr1 = "";
			string			sCompanyAddr2 = "";
            string          sCompanyAddr3 = "";
            string          sCompanyAddr4 = "";
			string			sCompanyCity = "";
			string			sCompanyState = "";
			string			sCompanyZip = "";	
			string			sEntityTableId = "";
			try
			{
			
				if (!File.Exists(m_sJCAHOImgFile))
					throw new FileInputOutputException
                        (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.PrintJCAHO.InvalidJCAHOImgLocation", m_iClientId), m_sLangCode));

				m_objSentinelReport.PrintImage("JCAHOLogo",m_sJCAHOImgFile);								

				#region Fetching the data to be shown in the report
				sbSql = new StringBuilder();
				sbSql.Remove(0,sbSql.Length);
                sbSql.Append("SELECT LAST_NAME,ADDR1,ADDR2,ADDR3,ADDR4,CITY,STATES.STATE_ID,ZIP_CODE FROM ENTITY,");
				sbSql.Append(" ORG_HIERARCHY, STATES WHERE ENTITY.STATE_ID=STATES.STATE_ROW_ID AND ");
				sbSql.Append(" ORG_HIERARCHY.DEPARTMENT_EID = " + p_iDeptEid);
				sbSql.Append(" AND ENTITY.ENTITY_ID = ORG_HIERARCHY.");			
				
				sEntityTableId = Common.Conversion.EntityTableIdToOrgTableName(p_iReportLevel);
				if(sEntityTableId.Trim() == "")
					throw new InvalidValueException
                        (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.PrintJCAHO.InvalidReportLevel", m_iClientId), m_sLangCode));

				sbSql.Append(sEntityTableId);
				sbSql.Append("_EID");
				objReader = DbFactory.GetDbReader(m_sConnectionString ,sbSql.ToString());
				if(objReader.Read())
				{
					sCompanyName = objReader.GetString("LAST_NAME").Trim();					
					sCompanyAddr1 = objReader.GetString("ADDR1").Trim();					
					sCompanyAddr2 = objReader.GetString("ADDR2").Trim();
                    sCompanyAddr3 = objReader.GetString("ADDR3").Trim();
                    sCompanyAddr4 = objReader.GetString("ADDR4").Trim();
					sCompanyCity = objReader.GetString("CITY").Trim();					
					sCompanyState = objReader.GetString("STATE_ID").Trim();					
					sCompanyZip = objReader.GetString("ZIP_CODE").Trim();						
				}
				objReader.Close();				
				#endregion

				#region Print JCAHO related details 
				
				sbDataString = new StringBuilder();
                m_objSentinelReport.PrintData("LblJCAHO", CommonFunctions.FilterBusinessMessage(Globalization.GetString("JCAHOUseOnly", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("LblCase", CommonFunctions.FilterBusinessMessage(Globalization.GetString("CaseNo", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("LblHCO", CommonFunctions.FilterBusinessMessage(Globalization.GetString("HCONo", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("JCAHOHead1", CommonFunctions.FilterBusinessMessage(Globalization.GetString("AccreditedOrganization", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("JCAHOHead2", CommonFunctions.FilterBusinessMessage(Globalization.GetString("SelfReportedSentinelEvent", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("NameOfOrg",sCompanyName);
                m_objSentinelReport.PrintData("Address", sCompanyAddr1 + " " + sCompanyAddr2 + " " + sCompanyAddr3 + " " + sCompanyAddr4);
				m_objSentinelReport.PrintData("LblNameOfOrg",
                                            CommonFunctions.FilterBusinessMessage(Globalization.GetString("NameAccreditedOrganization", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("LblAddress", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Address", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("City",sCompanyCity);
				m_objSentinelReport.PrintData("State",sCompanyState);
				m_objSentinelReport.PrintData("Zipcode",sCompanyZip);

                m_objSentinelReport.PrintData("LblCity", CommonFunctions.FilterBusinessMessage(Globalization.GetString("City", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("LblState", CommonFunctions.FilterBusinessMessage(Globalization.GetString("State", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("LblZipcode", CommonFunctions.FilterBusinessMessage(Globalization.GetString("ZipCode", m_iClientId), m_sLangCode));

                m_objSentinelReport.PrintData("DateOfIncident", CommonFunctions.FilterBusinessMessage(Globalization.GetString("DateOfIncident", m_iClientId), m_sLangCode));
				sbDataString.Remove(0,sbDataString.Length);				
                //sbDataString.Append(" " + VBFunctions.VBMid(p_sEventDate,5,2));
                //sbDataString.Append("/" + VBFunctions.VBRight(p_sEventDate,2));
                //sbDataString.Append("/" + VBFunctions.VBLeft(p_sEventDate,4));
                sbDataString.Append(" " + Conversion.GetUIDate(p_sEventDate,m_sLangCode,m_iClientId));
				m_objSentinelReport.PrintData("DateOfIncident1",sbDataString.ToString());

                m_objSentinelReport.PrintData("SummaryOfIncident", CommonFunctions.FilterBusinessMessage(Globalization.GetString("IncidentSummary", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("IncidentText", CommonFunctions.FilterBusinessMessage(Globalization.GetString("IncidentSummaryDesc", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("IncidentSummary",PrintMemoString(p_sSentinelDesc,200));
				m_objSentinelReport.PrintData("HasAnalysisStarted",
                                CommonFunctions.FilterBusinessMessage(Globalization.GetString("RootCauseAnalysisStarted", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("Yes", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Yes", m_iClientId), m_sLangCode));
				//m_objSentinelReport.PrintData("TodayDate",System.DateTime.Now.ToString("d"));
                m_objSentinelReport.PrintData("TodayDate", Conversion.GetUIDate(System.DateTime.Now.ToString(), m_sLangCode, m_iClientId));
                m_objSentinelReport.PrintData("Signature", CommonFunctions.FilterBusinessMessage(Globalization.GetString("OfficerName", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("Title", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Title", m_iClientId), m_sLangCode));
                m_objSentinelReport.PrintData("Date", CommonFunctions.FilterBusinessMessage(Globalization.GetString("Date", m_iClientId), m_sLangCode));

                m_objSentinelReport.PrintData("JCAHODesc1", CommonFunctions.FilterBusinessMessage(Globalization.GetString("ContactDetails", m_iClientId), m_sLangCode));

				m_objSentinelReport.PrintData("FooterLabel1",
                                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("One_Ranaissance_Boulevard", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("FooterLabel4",
                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("Member_Organizations", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("FooterLabel7",
                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("American_Dental_ Association", m_iClientId), m_sLangCode));									

				m_objSentinelReport.PrintData("FooterLabel2",
                                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("Oakbrook_Terrace", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("FooterLabel5",
                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("American_College_of_Physicians", m_iClientId), m_sLangCode));
				m_objSentinelReport.PrintData("FooterLabel8",
                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("American_Hospital_Association", m_iClientId), m_sLangCode));

				m_objSentinelReport.PrintData("FooterLabel3","(630)792-5800");
				m_objSentinelReport.PrintData("FooterLabel6",
                                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("American_College_of_Surgeons", m_iClientId), m_sLangCode));				
				m_objSentinelReport.PrintData("FooterLabel9",
                                    CommonFunctions.FilterBusinessMessage(Globalization.GetString("American_Medical_Association", m_iClientId), m_sLangCode));

				#endregion
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.PrintJCAHO.Error", m_iClientId), m_sLangCode), p_objException);
			}
			finally
			{				
				sbSql=null;
				sbDataString = null;
				if (objReader != null)
					objReader.Dispose();
			}
		}

		/// Name			: PrintMemoString
		/// Author			: Neelima Dabral
		/// Date Created	: 3-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This method will print the Memo string.
		/// </summary>	
		/// <param name="p_sMemoString">Memo string</param>		
		/// <param name="p_iLength">Number of characters in a single line</param>
		/// <returns>The memo string to be printed</returns>
		private string PrintMemoString(string p_sMemoString,int p_iLength)
		{
			string	sTemp="";
			string	sToPrint="";
			int		iPtr = 0;
			string	sConcat="";
			bool	bFlag=false;
			int		iNextLinePos=0;		
			string	sFinalString="";			
			try
			{
				do
				{
                    iPtr = VBFunctions.VBInStr(p_sMemoString, " ");

                    //Changed by Gagan for MITS 18945 : start
					if (iPtr == 0)
                        iPtr = p_sMemoString.Length;// +1;
                    //Changed by Gagan for MITS 18945 : end
					sTemp = VBFunctions.VBLeft(p_sMemoString,iPtr);
					sConcat= sToPrint + sTemp;
					if(sConcat.Length > p_iLength)
					{
						bFlag = false;
						iNextLinePos =VBFunctions.VBInStr(sToPrint,Constants.VBCr);
						if (iNextLinePos > 0)
						{
							sTemp = VBFunctions.VBMid(sToPrint,iNextLinePos+2) + " " + sTemp;
							sToPrint = VBFunctions.VBLeft(sToPrint,iNextLinePos-1);
							if (VBFunctions.VBLeft(sTemp,1) == Constants.VBCr)
								bFlag = true;
						}						
						sFinalString = sFinalString  + sToPrint.Replace("\n"," ");						
						sFinalString = sFinalString + "\n";
						if(bFlag)						
						{
							sFinalString = sFinalString  + " ";							
						}
						sToPrint = sTemp;
					}
					else
					{
						sToPrint = sToPrint + sTemp;
					}
					p_sMemoString = VBFunctions.VBMid(p_sMemoString,iPtr +1);
				}while (p_sMemoString != "");

				if(sToPrint != "")
				{
					iNextLinePos = VBFunctions.VBInStr(sToPrint,Constants.VBCr);
					while (iNextLinePos > 0)
					{
						bFlag = false;
						sTemp = VBFunctions.VBLeft(sToPrint,iNextLinePos-1);
						sToPrint = VBFunctions.VBMid(sToPrint,iNextLinePos+2);
						if(VBFunctions.VBLeft(sToPrint,1) ==
								Constants.VBCr)
							bFlag = true;	
						sFinalString = sFinalString  + sTemp.Replace("\n"," ");					

						iNextLinePos = VBFunctions.VBInStr(sToPrint,Constants.VBCr);
						if (bFlag)				
						{
							sFinalString = sFinalString  + " ";
						}
					}							
					sFinalString = sFinalString  + sToPrint.Replace("\n"," ");				
				}
			}				
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (CommonFunctions.FilterBusinessMessage(Globalization.GetString("Report.PrintMemoString.ErrorPrint", m_iClientId), m_sLangCode), p_objException);
			}		
			return sFinalString;
		}
		
		#endregion
	}

}
