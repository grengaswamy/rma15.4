﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using System.Collections;
using System.Net.Mail; //Needed for SMTP mailing
using System.IO;
using Riskmaster.Security.Encryption;
using System.Xml.Linq; //Bharani - MITS : 33816.
using System.Data;

namespace Riskmaster.Application.SortMaster
{
    public class SortMaster
    {
        private string m_sConnectString = string.Empty;
        private string m_sDSNID = string.Empty;
        private string m_sGroupId = string.Empty;
        private string m_sAdminRights = string.Empty;
        //private string m_sConnectionStrSM = string.Empty;
        private string m_sConnectionStrTM = string.Empty;
        private string m_sUserID = string.Empty;
        private string m_sUserName = string.Empty;
        private string m_sUserEmailId = string.Empty;
        private string m_sDatabaseName = string.Empty;
        private string m_sPassword = string.Empty;
        private int m_iClientId = 0; //mbahl3 Jira[RMACLOUD-124]

       
        
        /// <summary>
        ///constructor
        /// </summary>
        public SortMaster(UserLogin objUserLogin,  int p_iClientId) //mbahl3 Jira[RMACLOUD-124]
        {
            m_sConnectString = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sDSNID = Convert.ToString(objUserLogin.objRiskmasterDatabase.DataSourceId);
            m_sGroupId = Convert.ToString(objUserLogin.GroupId);
             m_iClientId = p_iClientId; //mbahl3 Jira[RMACLOUD-124]
            m_sConnectionStrTM = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", m_iClientId);  //mbahl3 Jira[RMACLOUD-124]
             m_sUserID = Convert.ToString(objUserLogin.UserId);
            m_sUserName = objUserLogin.LoginName;
            m_sUserEmailId = objUserLogin.objUser.Email;
            m_sDatabaseName = objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sPassword = objUserLogin.Password;
            if (objUserLogin.IsAllowedEx(100000))
                m_sAdminRights = "1";
            else
                m_sAdminRights = "0";

        }

        # region Getusers
        ///<Summary>
        /// Gets the users to be displayed while posting OSHA Reports
        ///</Summary>
        public bool GetUsers(ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            XmlNode objXmlnode = null;
            bool m_sDBType;
            try
            {
                objElement = p_objXmlDocOut.CreateElement("UserDetails");
                p_objXmlDocOut.AppendChild(objElement);
                objElement = null;

                if (m_sAdminRights == "1")
                {
                    m_sDBType = DbFactory.IsOracleDatabase(SecurityDatabase.GetSecurityDsn(m_iClientId));
                    if (m_sDBType)
                    {
                        sSQL = sSQL = @"SELECT USER_TABLE.USER_ID,(USER_TABLE.FIRST_NAME || USER_TABLE.LAST_NAME || '(' || USER_DETAILS_TABLE.LOGIN_NAME || ')' )AS LOGINNAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + m_sDSNID + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";
                    }
                    else
                    {
                        sSQL = @"SELECT USER_TABLE.USER_ID,(USER_TABLE.FIRST_NAME+' '+USER_TABLE.LAST_NAME+'('+USER_DETAILS_TABLE.LOGIN_NAME+')')AS LOGINNAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + m_sDSNID + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";
                    }
                    using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL))//dvatsa-added GetSecurityDsn(m_iclientid)
                    {
                        while (objReader.Read())
                        {
                            //objElement = p_objXmlDocOut.CreateElement("UserDetails");
                            //p_objXmlDocOut.AppendChild(objElement);
                            //objElement = null;

                            objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails");
                            objElement = p_objXmlDocOut.CreateElement("User");
                            objXmlnode.AppendChild(objElement);
                            objElement = null;
                            objXmlnode = null;

                            objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails/User");
                            objElement = p_objXmlDocOut.CreateElement("option");
                            objElement.InnerText = objReader.GetValue("LOGINNAME").ToString();
                            objElement.SetAttribute("value", objReader.GetValue("USER_ID").ToString());
                            objXmlnode.AppendChild(objElement);
                            objElement = null;
                        }

                    }
                }
                else
                {
                    sSQL = @"SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID=" + m_sGroupId;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL)) //should be using main DB
                    {
                        while (objReader.Read())
                        {
                            // akaushik5 Changed for MITS 37212 Starts
                            // String ssSql = @"SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME 
                            // FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + objReader.GetValue("USER_ID").ToString() + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";
                            //using (DbReader objRdr = DbFactory.GetDbReader(SecurityDatabase.Dsn, ssSql))
                            StringBuilder ssSql = new StringBuilder();
                            ssSql.Append("SELECT DISTINCT USER_TABLE.USER_ID, USER_TABLE.LAST_NAME, USER_TABLE.FIRST_NAME, USER_DETAILS_TABLE.LOGIN_NAME")
                                .Append(" FROM USER_TABLE JOIN USER_DETAILS_TABLE ON USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID")
                                .AppendFormat(" WHERE USER_TABLE.USER_ID = {0}", objReader.GetValue("USER_ID").ToString())
                                .Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");

                            using (DbReader objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), ssSql.ToString()))
                            // akaushik5 Changed for MITS 37212 Ends
                            {
                                while (objRdr.Read())
                                {
                                    objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails");
                                    objElement = p_objXmlDocOut.CreateElement("User");
                                    objXmlnode.AppendChild(objElement);
                                    objElement = null;
                                    objXmlnode = null;

                                    objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails/User");
                                    objElement = p_objXmlDocOut.CreateElement("option");
                                    // akaushik5 Changed for MITS 37212 Ends
                                    //objElement.InnerText = objReader.GetValue("LOGINNAME").ToString();
                                    objElement.InnerText = string.Format("{0} {1}({2})", objRdr.GetValue("FIRST_NAME").ToString(), objRdr.GetValue("LAST_NAME").ToString(), objRdr.GetValue("LOGIN_NAME").ToString());
                                    // akaushik5 Changed for MITS 37212 Ends
                                    objElement.SetAttribute("value", objReader.GetValue("USER_ID").ToString());
                                    objXmlnode.AppendChild(objElement);
                                    objElement = null;
                                }
                            }
                        }
                    }
                }
                objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails");
                objElement = p_objXmlDocOut.CreateElement("SMPath");
                objElement.InnerText = RMConfigurationManager.GetAppSetting("SMPath").ToString();
                objXmlnode.AppendChild(objElement);

            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetUsers() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
                objXmlnode = null;
            }
            return true;

        }
        #endregion

        #region save
        ///<Summary>
        /// Save osha reports - posting
        ///</Summary>
        public bool PostReports(XmlDocument p_objInputXMLDoc)
        {
            XmlElement objSMXMLEle = null;
            string sReportName = string.Empty;
            string sReportDesc = string.Empty;
            string sReportXML = string.Empty;
            string sReportPrivacyCase = string.Empty;
            string sNextUID = string.Empty;
            string sUserIds = string.Empty;
            DbConnection objCon = null;
            DbCommand objCmd = null;
            DbParameter objParamXml = null;
            string sSQL = string.Empty;
            string[] sUserIDs;
            try
            {
                sNextUID = GetNextUID("TM_REPORTS", m_sConnectionStrTM).ToString();
                objSMXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Report");

                sReportName = objSMXMLEle.GetElementsByTagName("ReportName").Item(0).InnerText;
                sReportDesc = objSMXMLEle.GetElementsByTagName("ReportDesc").Item(0).InnerText;
                sReportXML = objSMXMLEle.GetElementsByTagName("ReportXML").Item(0).InnerXml;
                sReportPrivacyCase = objSMXMLEle.GetElementsByTagName("PrivacyCase").Item(0).InnerText;
                sUserIds = objSMXMLEle.SelectSingleNode("UserIDs").InnerText;
                if (sUserIds.Contains(","))
                {
                    sUserIDs = sUserIds.Split(',');
                }
                else
                {
                    string[] strArr = new string[] { sUserIds };
                    sUserIDs = strArr;
                }
                

                objCon = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objCon.Open();
                objCmd = objCon.CreateCommand();
                objParamXml = objCmd.CreateParameter();
                objParamXml.Direction = ParameterDirection.Input;
                objParamXml.Value = sReportXML;
                objParamXml.ParameterName = "XML";
                objCmd.Parameters.Add(objParamXml);  
                sSQL = @"INSERT INTO TM_REPORTS (REPORT_ID,REPORT_NAME,REPORT_DESC,REPORT_XML,PRIVACY_CASE)VALUES(" +
                        sNextUID + ",' " + sReportName + "','" + sReportDesc + "', ~XML~ ," + sReportPrivacyCase + ")";

                objCmd.CommandText = sSQL;
                objCmd.ExecuteNonQuery();

                for (int j = 0; j < sUserIDs.Length; j++)
                {
                    sSQL = string.Empty;
                    sSQL = @"INSERT INTO TM_REPORTS_PERM (REPORT_ID,USER_ID)VALUES(" +
                            sNextUID + ", " + sUserIDs[j] + ")";

                    objCon.ExecuteNonQuery(sSQL);
                }

                return true;

            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in PostReports() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();

                if (objSMXMLEle != null)
                    objSMXMLEle = null;
            }
        }
        #endregion


        #region next UID
        ///<Summary>
        /// Get Next UID
        ///</Summary>
        private int GetNextUID(string p_sTableName, string connectionstring)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            DbReader objReader = null;
            int iOrigUID = 0;
            int iNextUID = 0;
            int iCollisionRetryCount = 0;
            int iErrRetryCount = 0;
            int iRows = 0;

            const int COLLISION_RETRY_COUNT = 1000;
            const int ERROR_RETRY_COUNT = 5;
            try
            {
                do
                {
                    sSQL = "SELECT NEXT_ID FROM TM_REP_IDS WHERE TABLE_NAME = '" + p_sTableName + "'";

                    objConn = DbFactory.GetDbConnection(connectionstring);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (!objReader.Read())
                        {
                            objReader.Close();
                            objReader = null;
                            throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.NoSuchTable", m_iClientId));
                        }

                        iNextUID = objReader.GetInt("NEXT_ID");
                        objReader.Close();
                        objReader = null;

                        // Compute next id
                        iOrigUID = iNextUID;
                        if (iOrigUID != 0)
                            iNextUID++;
                        else
                            iNextUID = 2;

                        // try to reserve id (searched update)
                        sSQL = "UPDATE TM_REP_IDS SET NEXT_ID = " + iNextUID + " WHERE TABLE_NAME = '" + p_sTableName + "'";

                        // only add searched clause if no chance of a null originally
                        // in row (only if no records ever saved against table)   
                        if (iOrigUID != 0)
                            sSQL += " AND NEXT_ID = " + iOrigUID;

                        // Try update
                        try
                        {
                            iRows = objConn.ExecuteNonQuery(sSQL);
                        }
                        catch (Exception e)
                        {
                            iErrRetryCount++;
                            if (iErrRetryCount >= 5)
                            {
                                objConn.Close();
                                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.ErrorModifyingDB", m_iClientId), e);
                            }
                        }
                        // if success, return
                        if (iRows == 1)
                        {
                            objConn.Close();
                            return iNextUID - 1;
                        }
                        else // collided with another user - try again (up to 1000 times)
                            iCollisionRetryCount++;

                    }

                } while ((iErrRetryCount < ERROR_RETRY_COUNT) && (iCollisionRetryCount < COLLISION_RETRY_COUNT));

                if (iCollisionRetryCount >= COLLISION_RETRY_COUNT)
                {
                    objConn.Close();
                    throw new RMAppException
                        (Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.CollisionTimeout", m_iClientId));
                }
                objConn.Close();
                //shouldn't get here under normal conditions.
                return 0;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.Error", m_iClientId), p_objEx);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
        }

        #endregion

        private int GetNextTMUID(string p_sTableName)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            DbReader objReader = null;
            int iOrigUID = 0;
            int iNextUID = 0;
            int iCollisionRetryCount = 0;
            int iErrRetryCount = 0;
            int iRows = 0;

            const int COLLISION_RETRY_COUNT = 1000;
            const int ERROR_RETRY_COUNT = 5;
            try
            {
                do
                {
                    sSQL = "SELECT NEXT_ID FROM TM_IDS WHERE TABLE_NAME = '" + p_sTableName + "'";

                    objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (!objReader.Read())
                        {
                            objReader.Close();
                            objReader = null;
                            throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.NoSuchTable", m_iClientId));
                        }

                        iNextUID = objReader.GetInt("NEXT_ID");
                        objReader.Close();
                        objReader = null;

                        // Compute next id
                        iOrigUID = iNextUID;
                        if (iOrigUID != 0)
                            iNextUID++;
                        else
                            iNextUID = 2;

                        // try to reserve id (searched update)
                        sSQL = "UPDATE TM_IDS SET NEXT_ID = " + iNextUID + " WHERE TABLE_NAME = '" + p_sTableName + "'";

                        // only add searched clause if no chance of a null originally
                        // in row (only if no records ever saved against table)   
                        if (iOrigUID != 0)
                            sSQL += " AND NEXT_ID = " + iOrigUID;

                        // Try update
                        try
                        {
                            iRows = objConn.ExecuteNonQuery(sSQL);
                        }
                        catch (Exception e)
                        {
                            iErrRetryCount++;
                            if (iErrRetryCount >= 5)
                            {
                                objConn.Close();
                                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.ErrorModifyingDB", m_iClientId), e);
                            }
                        }
                        // if success, return
                        if (iRows == 1)
                        {
                            objConn.Close();
                            return iNextUID - 1;
                        }
                        else // collided with another user - try again (up to 1000 times)
                            iCollisionRetryCount++;

                    }

                } while ((iErrRetryCount < ERROR_RETRY_COUNT) && (iCollisionRetryCount < COLLISION_RETRY_COUNT));

                if (iCollisionRetryCount >= COLLISION_RETRY_COUNT)
                {
                    objConn.Close();
                    throw new RMAppException
                        (Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.CollisionTimeout", m_iClientId));
                }
                objConn.Close();
                //shouldn't get here under normal conditions.
                return 0;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.Error", m_iClientId), p_objEx);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
        }



        #region next Doc UID
        ///<Summary>
        /// Get Next Document Table UID
        ///</Summary>
        private int GetNextDocUID(string p_sTableName, string connectionstring)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            DbReader objReader = null;
            int iOrigUID = 0;
            int iNextUID = 0;
            int iCollisionRetryCount = 0;
            int iErrRetryCount = 0;
            int iRows = 0;

            const int COLLISION_RETRY_COUNT = 1000;
            const int ERROR_RETRY_COUNT = 5;
            try
            {
                do
                {
                    sSQL = "SELECT TABLE_ID,NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "'";

                    objConn = DbFactory.GetDbConnection(connectionstring);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (!objReader.Read())
                        {
                            objReader.Close();
                            objReader = null;
                            throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.NoSuchTable", m_iClientId));
                        }

                        iNextUID = objReader.GetInt("NEXT_UNIQUE_ID");
                        objReader.Close();
                        objReader = null;

                        // Compute next id
                        iOrigUID = iNextUID;
                        if (iOrigUID != 0)
                            iNextUID++;
                        else
                            iNextUID = 2;

                        // try to reserve id (searched update)
                        sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + iNextUID + " WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "'";

                        // only add searched clause if no chance of a null originally
                        // in row (only if no records ever saved against table)   
                        if (iOrigUID != 0)
                            sSQL += " AND NEXT_UNIQUE_ID = " + iOrigUID;

                        // Try update
                        try
                        {
                            iRows = objConn.ExecuteNonQuery(sSQL);
                        }
                        catch (Exception e)
                        {
                            iErrRetryCount++;
                            if (iErrRetryCount >= 5)
                            {
                                objConn.Close();
                                throw new RMAppException("Error in GetNextDocUID() in SortMaster :", e);
                            }
                        }
                        // if success, return
                        if (iRows == 1)
                        {
                            objConn.Close();
                            return iNextUID - 1;
                        }
                        else // collided with another user - try again (up to 1000 times)
                            iCollisionRetryCount++;

                    }

                } while ((iErrRetryCount < ERROR_RETRY_COUNT) && (iCollisionRetryCount < COLLISION_RETRY_COUNT));

                if (iCollisionRetryCount >= COLLISION_RETRY_COUNT)
                {
                    objConn.Close();
                    throw new RMAppException
                        (Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.CollisionTimeout", m_iClientId));
                }
                objConn.Close();
                //shouldn't get here under normal conditions.
                return 0;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("Error in GetNextDocUID() in SortMaster : ", p_objEx);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
        }

        #endregion


        #region schedule report
        ///<Summary>
        /// Schedule OSHA Reports - monthly
        ///</Summary>
        public void ScheduleReports(XmlDocument p_objXmlDoc)
        {
            ScheduleDetails objSchedule;
            DbReader objReader = null;
            DateTime dttm;
            DateTime dttmNow;
            string sModulename = string.Empty;
            try
            {

                objSchedule = new ScheduleDetails();
                if (p_objXmlDoc.SelectSingleNode("//ScheduleIdSM") != null)
                {
                    objSchedule.ScheduleId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ScheduleIdSM").InnerText);
                }
                if (p_objXmlDoc.SelectSingleNode("//ReportId") != null)
                {
                    objSchedule.ReportId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ReportId").InnerText);
                }
                if (p_objXmlDoc.SelectSingleNode("//LastRunDTTM") != null)
                {
                    objSchedule.LastRunDTTM = p_objXmlDoc.SelectSingleNode("//LastRunDTTM").InnerText;
                }
                else
                    objSchedule.LastRunDTTM = "NULL";

                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.ScheduleTypeIdSM = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ScheduleTypeIdSM").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//Mon_Run").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//Mon_Run").InnerText == "True")
                    objSchedule.Mon = 1;
                else
                    objSchedule.Mon = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Mon_Run").InnerText);
                if (p_objXmlDoc.SelectSingleNode("//Tue_Run").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//Tue_Run").InnerText == "True")
                    objSchedule.Tue = 1;
                else
                    objSchedule.Tue = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Tue_Run").InnerText);
                //objSchedule.Tue = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Tuesday").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//Wed_Run").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//Wed_Run").InnerText == "True")
                    objSchedule.Wed = 1;
                else
                    objSchedule.Wed = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Wed_Run").InnerText);
                //objSchedule.Wed = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Wednesday").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//Thu_Run").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//Thu_Run").InnerText == "True")
                    objSchedule.Thu = 1;
                else
                    objSchedule.Thu = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Thu_Run").InnerText);
                //objSchedule.Thu = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Thursday").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//Fri_Run").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//Fri_Run").InnerText == "True")
                    objSchedule.Fri = 1;
                else
                    objSchedule.Fri = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Fri_Run").InnerText);
                //objSchedule.Fri = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Friday").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//Sat_Run").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//Sat_Run").InnerText == "True")
                    objSchedule.Sat = 1;
                else
                    objSchedule.Sat = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Sat_Run").InnerText);
                //objSchedule.Sat = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Saturday").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//Sun_Run").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//Sun_Run").InnerText == "True")
                    objSchedule.Sun = 1;
                else
                    objSchedule.Sun = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Sun_Run").InnerText);
                //objSchedule.Sun = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Sunday").InnerText);
                objSchedule.DayOfMonth = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//DayOfMonth").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//January").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//January").InnerText == "True")
                    objSchedule.Jan = 1;
                else
                    objSchedule.Jan = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//January").InnerText);
                //objSchedule.Jan = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//January").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//February").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//February").InnerText == "True")
                    objSchedule.Feb = 1;
                else
                    objSchedule.Feb = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//February").InnerText);
                //objSchedule.Feb = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//February").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//March").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//March").InnerText == "True")
                    objSchedule.Mar = 1;
                else
                    objSchedule.Mar = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//March").InnerText);
                //objSchedule.Mar = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//March").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//April").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//April").InnerText == "True")
                    objSchedule.Apr = 1;
                else
                    objSchedule.Apr = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//April").InnerText);
                //objSchedule.Apr = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//April").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//May").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//May").InnerText == "True")
                    objSchedule.May = 1;
                else
                    objSchedule.May = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//May").InnerText);
                //objSchedule.May = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//May").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//June").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//June").InnerText == "True")
                    objSchedule.Jun = 1;
                else
                    objSchedule.Jun = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//June").InnerText);
                //objSchedule.Jun = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//June").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//July").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//July").InnerText == "True")
                    objSchedule.Jul = 1;
                else
                    objSchedule.Jul = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//July").InnerText);
                //objSchedule.Jul = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//July").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//August").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//August").InnerText == "True")
                    objSchedule.Aug = 1;
                else
                    objSchedule.Aug = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//August").InnerText);
                //objSchedule.Aug = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//August").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//September").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//September").InnerText == "True")
                    objSchedule.Sep = 1;
                else
                    objSchedule.Sep = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//September").InnerText);
                //objSchedule.Sep = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//September").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//October").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//October").InnerText == "True")
                    objSchedule.Oct = 1;
                else
                    objSchedule.Oct = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//October").InnerText);
                //objSchedule.Oct = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//October").InnerText);
                if (p_objXmlDoc.SelectSingleNode("//November").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//November").InnerText == "True")
                    objSchedule.Nov = 1;
                else
                    objSchedule.Nov = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//November").InnerText);
                //objSchedule.Nov = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//November").InnerText);

                if (p_objXmlDoc.SelectSingleNode("//December").InnerText == "true" || p_objXmlDoc.SelectSingleNode("//December").InnerText == "True")
                    objSchedule.Dec = 1;
                else
                    objSchedule.Dec = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//December").InnerText);
                //objSchedule.Dec = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//December").InnerText);

                //dttmNow = DateTime.Now;
                //if (objSchedule.ScheduleTypeIdSM == 2)
                //{
                //    if (dttmNow.Month > Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Month").InnerText))
                //    {
                //        dttm = new DateTime(dttmNow.Year + 1, Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Month").InnerText), Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//DayOfMonth").InnerText));
                //    }
                //    else
                //    {
                //        dttm = new DateTime(dttmNow.Year, Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Month").InnerText), Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//DayOfMonth").InnerText));
                //    }
                //}
                //else
                //{
                string ScheduleDT = p_objXmlDoc.SelectSingleNode("//Date").InnerText;
                dttm = DateTime.Parse(p_objXmlDoc.SelectSingleNode("//Date").InnerText);
                //}

                objSchedule.StartDate = Conversion.GetDate(dttm.ToShortDateString());
                objSchedule.NextRunDT = Conversion.GetDate(dttm.ToShortDateString());
                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                string StartDateTime = Conversion.GetDateTime(dttm.ToShortDateString() + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);

                objSchedule.JobName = p_objXmlDoc.SelectSingleNode("//ReportName").InnerText + "-" + m_sUserName + "-" + StartDateTime;
                objSchedule.JobDesc = "Report " + p_objXmlDoc.SelectSingleNode("//ReportName").InnerText + " submitted on " + Conversion.ToDate(Conversion.GetDate(DateTime.Now.Date.ToString())).ToShortDateString() + " at " + p_objXmlDoc.SelectSingleNode("//Time").InnerText + " by user " + m_sUserName;
                //objSchedule.DSN = m_sConnectString;
                objSchedule.OutputType = p_objXmlDoc.SelectSingleNode("//OutputType").InnerText;
                objSchedule.OutputPath = p_objXmlDoc.SelectSingleNode("//OutputPath").InnerText;
                objSchedule.OutputPathURL = p_objXmlDoc.SelectSingleNode("//OutputPathURL").InnerText;

                objSchedule.ReportXML = p_objXmlDoc.SelectSingleNode("//ReportXML").InnerXml;
                objSchedule.NotificationType = p_objXmlDoc.SelectSingleNode("//NotificationType").InnerText;
                objSchedule.NotifyEmail = p_objXmlDoc.SelectSingleNode("//NotifyEmail").InnerText;

                if (p_objXmlDoc.SelectSingleNode("//NotifyMsg").InnerText != "")
                    objSchedule.NotifyMsg = "'" + p_objXmlDoc.SelectSingleNode("//NotifyMsg").InnerText + "'";
                else
                    objSchedule.NotifyMsg = "NULL";

                if (p_objXmlDoc.SelectSingleNode("//OutputOptions").InnerText != "")
                    objSchedule.OutputOptions = "'" + p_objXmlDoc.SelectSingleNode("//OutputOptions").InnerText + "'";
                else
                    objSchedule.OutputOptions = "NULL";

                objSchedule.UserId = m_sUserID;

                if (objSchedule.ScheduleId.ToString() == "" || objSchedule.ScheduleId <= 0)
                {
                    SaveSettings(objSchedule);
                    UserLogin objuserLogin = new UserLogin(m_sUserName, m_sPassword,m_sDatabaseName,m_iClientId);
                    TaskManager objTaskmanager = new TaskManager(objuserLogin, m_iClientId);//rkaur27
                    if (objSchedule.ScheduleTypeIdSM == 2)
                    {
                        objTaskmanager.SaveMonthlySettings(p_objXmlDoc);
                    }
                    if (objSchedule.ScheduleTypeIdSM == 1)
                    {
                        objTaskmanager.SaveWeeklySettings(p_objXmlDoc);
                    }
                }
                else // to update values 
                {
                    UpdateSettings(objSchedule);
                    int UserID = Convert.ToInt32(m_sUserID);
                    UserLogin objuserLogin = new UserLogin(UserID, m_iClientId); //asharma329
                    TaskManager objTaskmanager = new TaskManager(objuserLogin, m_iClientId);//rkaur27
                    if (objSchedule.ScheduleTypeIdSM != null && (objSchedule.ScheduleTypeIdSM == 2))
                    {
                        objTaskmanager.SaveMonthlySettings(p_objXmlDoc);
                    }
                    if (objSchedule.ScheduleTypeIdSM != null && (objSchedule.ScheduleTypeIdSM == 1))
                    {
                        objTaskmanager.SaveWeeklySettings(p_objXmlDoc);
                    }
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveMonthlySettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
        }
        #endregion

        #region RunReport
        ///<Summary>
        /// inserts records to be processed directly into Jobs table
        ///</Summary>
        public void RunReport(XmlDocument p_objXmlDoc)
        {
            JobDetails objJob;
            DateTime dttm;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            DbParameter objParamXml = null;
            string sSQL = string.Empty;
            string Config = string.Empty;
            string TaskTypeId = string.Empty;
            string sTM_Job_Id = string.Empty;
            string sNextJobId = string.Empty;
            string sNextTMScheduleID = string.Empty;
            try
            {
                sNextJobId = GetNextUID("TM_REP_JOBS", m_sConnectionStrTM).ToString();
                sNextTMScheduleID = GetNextTMUID("TM_SCHEDULE").ToString();
                objJob = new JobDetails();
                if (p_objXmlDoc.SelectSingleNode("//JobPriority") != null)
                {
                    objJob.JobPriority = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//JobPriority").InnerText);
                }
                dttm = DateTime.Now;
                if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//Time").InnerText))
                {
                    p_objXmlDoc.SelectSingleNode("//Time").InnerText = DateTime.Now.ToString("hh:mm:ss tt");
                }
                string StartDateTime = Conversion.GetDateTime(dttm.ToShortDateString() + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objJob.JobName = p_objXmlDoc.SelectSingleNode("//ReportName").InnerText + "-" + m_sUserName + "-" + StartDateTime;
                objJob.JobDesc = "Report " + p_objXmlDoc.SelectSingleNode("//ReportName").InnerText + " submitted on " + Conversion.ToDate(Conversion.GetDate(DateTime.Now.Date.ToString())).ToShortDateString() + " at " + p_objXmlDoc.SelectSingleNode("//Time").InnerText + " by user " + m_sUserName;
                //objJob.DSN = m_sConnectString;
                objJob.OutputType = p_objXmlDoc.SelectSingleNode("//OutputType").InnerText;
                objJob.OutputPath = p_objXmlDoc.SelectSingleNode("//OutputPath").InnerText;
                objJob.OutputPathURL = p_objXmlDoc.SelectSingleNode("//OutputPathURL").InnerText + "&JobID=" + sNextJobId;

                if (p_objXmlDoc.SelectSingleNode("//OutputOptions").InnerText != "")
                    objJob.OutputOptions = "'" + p_objXmlDoc.SelectSingleNode("//OutputOptions").InnerText + "'";
                else
                    objJob.OutputOptions = "NULL";

                objJob.ReportXML = p_objXmlDoc.SelectSingleNode("//ReportXML").InnerXml;

                if ((p_objXmlDoc.SelectSingleNode("//Time") != null) && (p_objXmlDoc.SelectSingleNode("//Date") != null))
                {
                    objJob.StartDateTime = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//Date").InnerText) + Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                }

                objJob.NotificationType = p_objXmlDoc.SelectSingleNode("//NotificationType").InnerText;
                objJob.NotifyEmail = p_objXmlDoc.SelectSingleNode("//NotifyEmail").InnerText;

                if (p_objXmlDoc.SelectSingleNode("//NotifyMsg").InnerText != "")
                    objJob.NotifyMsg = "'" + p_objXmlDoc.SelectSingleNode("//NotifyMsg").InnerText + "'";
                else
                    objJob.NotifyMsg = "NULL";

                objJob.Assigned = 0;//0 if specific time else -1 for immediately
                objJob.AssignedTo = "NULL";
                objJob.AssignedDTTM = "NULL";
                objJob.Complete = 0;
                objJob.CompleteDTTM = "NULL";
                objJob.Archive = 0;
                objJob.ErrorFlag = 0;
                objJob.UserId = m_sUserID;

                sSQL = "SELECT NEXT_ID FROM TM_IDS WHERE TABLE_NAME = 'TM_JOBS'";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objRdr.Read())
                    {
                        sTM_Job_Id = objRdr.GetValue("NEXT_ID").ToString();
                    }
                }

                objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                objParamXml = objCommand.CreateParameter();
                objParamXml.Direction = ParameterDirection.Input;
                objParamXml.Value = objJob.ReportXML.ToString();
                objParamXml.ParameterName = "XML";
                objCommand.Parameters.Add(objParamXml);  

                sSQL = "INSERT INTO TM_REP_JOBS (JOB_ID, JOB_PRIORITY, JOB_NAME, JOB_DESC, OUTPUT_TYPE, OUTPUT_PATH, OUTPUT_PATH_URL, "
                    + "OUTPUT_OPTIONS, REPORT_XML, START_DTTM, NOTIFICATION_TYPE, NOTIFY_EMAIL, NOTIFY_MSG, ASSIGNED, ASSIGNED_TO, ASSIGNED_DTTM, COMPLETE, COMPLETE_DTTM, USER_ID ,ARCHIVED, ERROR_FLAG,TM_JOBS_ID,TM_SCHEDULE_ID) VALUES(" +
                sNextJobId + ", " +
                objJob.JobPriority.ToString() + ", '" +
                objJob.JobName.ToString() + "', '" +
                objJob.JobDesc.ToString() + "', '" +
                    //objJob.DSN.ToString() + "', '" +
                objJob.OutputType + "', '" +
                objJob.OutputPath.ToString() + "', '" +
                objJob.OutputPathURL + "', " +
                objJob.OutputOptions + ", ~XML~ , '" +
                objJob.StartDateTime + "', '" +
                objJob.NotificationType + "', '" +
                objJob.NotifyEmail + "', " +
                objJob.NotifyMsg + ", " +
                objJob.Assigned.ToString() + ", " +
                objJob.AssignedTo.ToString() + ", " +
                objJob.AssignedDTTM.ToString() + ", " +
                objJob.Complete.ToString() + ", " +
                objJob.CompleteDTTM.ToString() + ", " +
                objJob.UserId.ToString() + ", " +
                objJob.Archive.ToString() + ", " +
                objJob.ErrorFlag.ToString() + ", " +
                 sTM_Job_Id + ", " +
                sNextTMScheduleID + ")"; 

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();

                sSQL = "SELECT TASK_TYPE_ID,CONFIG FROM TM_TASK_TYPE WHERE NAME='OSHA Reports'";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objRdr.Read())
                    {
                        Config = objRdr.GetValue("CONFIG").ToString();
                        TaskTypeId = objRdr.GetValue("TASK_TYPE_ID").ToString();
                    }
                }
                XmlDocument objXmlDoc = null;
                XmlElement objTmpElement = null;
                XmlElement objArgElement = null;

                objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(Config);
                objTmpElement = objXmlDoc.CreateElement("Args");
                objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                objArgElement = objXmlDoc.CreateElement("arg");

                object oSMTP = DbFactory.ExecuteScalar(m_sConnectString, "SELECT SMTP_SERVER FROM SYS_PARMS");
                objArgElement.InnerText = oSMTP.ToString();
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.InnerText = RMConfigurationManager.GetAppSetting("SMDataPath");
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "connectionstring");
                objArgElement.InnerText = RMCryptography.EncryptString(m_sConnectionStrTM);
                objTmpElement.AppendChild(objArgElement);

                

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "connectionstring");
                objArgElement.InnerText = RMCryptography.EncryptString(m_sConnectString);
                objTmpElement.AppendChild(objArgElement);

                sSQL = "INSERT INTO TM_JOBS (JOB_ID, TASK_TYPE_ID, JOB_STATE_ID, START_DTTM, TARGET_HOST, CONFIG,  OPTIONSET_ID, JOB_NAME,TM_USER,TM_DSN) VALUES(" +
                GetNextTMUID("TM_JOBS").ToString() + ", '" +
                TaskTypeId + "', 1,'" +
                objJob.StartDateTime + "','" +
                System.Environment.MachineName + "', '" +
                objXmlDoc.OuterXml + "','0','OSHA Reports','" + m_sUserName + " ','" + m_sDatabaseName + "')";

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();


                sSQL = "INSERT INTO TM_SCHEDULE (SCHEDULE_ID, SCHEDULE_STATE, SCHEDULE_TYPE_ID, TASK_TYPE_ID, "
                    + "INTERVAL_TYPE_ID, INTERVAL, TRIGGER_DIRECTORY, TIME_TO_RUN, DAY_OF_MON_TO_RUN, NEXT_RUN_DTTM, "
                    + "FINAL_RUN_DTTM, CONFIG, MON_RUN, TUE_RUN, WED_RUN, THU_RUN, FRI_RUN, SAT_RUN, SUN_RUN, JAN_RUN, "
                    + "FEB_RUN, MAR_RUN, APR_RUN, MAY_RUN, JUN_RUN, JUL_RUN, AUG_RUN, SEP_RUN, OCT_RUN, NOV_RUN, DEC_RUN, TASK_NAME, OPTIONSET_ID,TM_USER,TM_DSN) VALUES(" +
                sNextTMScheduleID + ", " +
                "1, " +
                "1, '" + TaskTypeId +
                "', " +
                "0, " +
                "0, '','" +
                Conversion.GetTime(DateTime.Now.ToString()) + "', " +
                "0,'"+Conversion.GetDateTime(p_objXmlDoc.SelectSingleNode("//Date").InnerText + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText)+"','','" +
                objXmlDoc.OuterXml + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'OSHA Reports',0,'" + m_sUserName + " ','" + m_sDatabaseName + "')";

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();

                //Bharani - MITS : 33816 - Start

                //if (!(string.IsNullOrEmpty(objJob.NotificationType.ToString())) && objJob.NotificationType.ToString().ToUpper() != "NONE")
                //{
                //    MailMessage objMailMessage = new MailMessage();
                //    objMailMessage.From = new System.Net.Mail.MailAddress(m_sUserEmailId);
                //    string[] sCCList = objJob.NotifyEmail.Split('-');
                //    foreach(string cc in sCCList)
                //        objMailMessage.CC.Add(new System.Net.Mail.MailAddress(cc));
                //    objMailMessage.Subject = "Sortmaster Report Mail";
                //    objMailMessage.Body = "Sortmaster Report Mail" + objJob.NotifyMsg;
                //    if (File.Exists(objJob.OutputPath))
                //    {
                //        Attachment objAttachment = new Attachment(objJob.OutputPath);
                //        objMailMessage.Attachments.Add(objAttachment);
                //    }
                //    Mailer.SendMail(objMailMessage);
                //}

                //Bharani - MITS : 33816 - End


            }
            catch (RMAppException ex)
            {
                throw new RMAppException("Error in RunReport() in SortMaster : ", ex);
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveMonthlySettings.Error", m_iClientId), objException);
            }
        }
        #endregion

        #region SaveSetting
        /// <summary>
        /// This method saves the values entered in all the schedule types to db.
        /// </summary>
        /// <param name="p_objSchedule">A structure which is filled according to the schedule type and passed to
        /// this method to save it to db.</param>
        public void SaveSettings(ScheduleDetails p_objSchedule)
        {
            //DbReader objReader = null;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            DbParameter objParamXml = null;
            string sSQL = "";
            string sTM_Schedule_Id = string.Empty;
            try
            {
                sSQL = "SELECT NEXT_ID FROM TM_IDS WHERE TABLE_NAME = 'TM_SCHEDULE'";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objRdr.Read())
                    {
                        sTM_Schedule_Id = objRdr.GetValue("NEXT_ID").ToString();
                    }
                }

                objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                objParamXml = objCommand.CreateParameter();
                objParamXml.Direction = ParameterDirection.Input;
                objParamXml.Value = p_objSchedule.ReportXML.ToString();
                objParamXml.ParameterName = "XML";
                objCommand.Parameters.Add(objParamXml); 

                sSQL = "INSERT INTO TM_REP_SCHEDULE (SCHEDULE_ID, REPORT_ID, LAST_RUN_DTTM, NEXT_RUN_DATE, SCHEDULE_TYPE, START_TIME, START_DATE, "
                    + "MON_RUN, TUE_RUN, WED_RUN, THU_RUN, FRI_RUN, SAT_RUN, SUN_RUN, DAYOFMONTH_RUN, JAN_RUN, "
                    + "FEB_RUN, MAR_RUN, APR_RUN, MAY_RUN, JUN_RUN, JUL_RUN, AUG_RUN, SEP_RUN, OCT_RUN, NOV_RUN, DEC_RUN, JOB_NAME, JOB_DESC,OUTPUT_TYPE, "
                + "OUTPUT_PATH, OUTPUT_PATH_URL, OUTPUT_OPTIONS, REPORT_XML, NOTIFICATION_TYPE, NOTIFY_EMAIL, NOTIFY_MSG, USER_ID,TM_SCHEDULE_ID) VALUES(" +
                GetNextUID("TM_REP_SCHEDULE", m_sConnectionStrTM).ToString() + ", " +
                p_objSchedule.ReportId.ToString() + ", '" +
                p_objSchedule.LastRunDTTM.ToString() + "', '" +
                p_objSchedule.NextRunDT.ToString() + "', " +
                p_objSchedule.ScheduleTypeIdSM.ToString() + ", '" +
                p_objSchedule.TimeToRun + "', '" +
                p_objSchedule.StartDate.ToString() + "', " +
                p_objSchedule.Mon.ToString() + ", " +
                p_objSchedule.Tue.ToString() + ", " +
                p_objSchedule.Wed.ToString() + ", " +
                p_objSchedule.Thu.ToString() + ", " +
                p_objSchedule.Fri.ToString() + ", " +
                p_objSchedule.Sat.ToString() + ", " +
                p_objSchedule.Sun.ToString() + ", " +
                p_objSchedule.DayOfMonth.ToString() + ", " +
                p_objSchedule.Jan.ToString() + ", " +
                p_objSchedule.Feb.ToString() + ", " +
                p_objSchedule.Mar.ToString() + ", " +
                p_objSchedule.Apr.ToString() + ", " +
                p_objSchedule.May.ToString() + ", " +
                p_objSchedule.Jun.ToString() + ", " +
                p_objSchedule.Jul.ToString() + ", " +
                p_objSchedule.Aug.ToString() + ", " +
                p_objSchedule.Sep.ToString() + ", " +
                p_objSchedule.Oct.ToString() + ", " +
                p_objSchedule.Nov.ToString() + ", " +
                p_objSchedule.Dec.ToString() + ", '" +
                p_objSchedule.JobName.ToString() + "', '" +
                p_objSchedule.JobDesc.ToString() + "', '" +
                    //p_objSchedule.DSN.ToString() + "', '" +
                p_objSchedule.OutputType.ToString() + "', '" +
                p_objSchedule.OutputPath.ToString() + "', '" +
                p_objSchedule.OutputPathURL.ToString() + "', " +
                p_objSchedule.OutputOptions.ToString() + ", ~XML~ , '" +
                p_objSchedule.NotificationType.ToString() + "', '" +
                p_objSchedule.NotifyEmail.ToString() + "', " +
                p_objSchedule.NotifyMsg.ToString() + ", " +
                p_objSchedule.UserId.ToString() + ", " +
                 sTM_Schedule_Id + ")";

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();
            }
            catch (RMAppException ex)
            {
                throw new RMAppException("Error in SaveSettings() in SortMaster : ", ex);
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveSettings.Error", m_iClientId), objException);
            }
            finally
            {
                //if (objReader != null)
                //    objReader.Dispose();
                if (objConn != null)
                    objConn.Dispose();
            }
        }

        #endregion

        #region UpdateSetting
        /// <summary>
        /// This method updates the edited values to db.
        /// </summary>
        /// <param name="p_objSchedule">A structure which is filled according to the schedule type and passed to
        /// this method to save it to db.</param>
        private void UpdateSettings(ScheduleDetails p_objSchedule)
        {
            DbReader objReader = null;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string sSQL = "";
            //DbCommand objCmd = null;//rkaur27 : RMA -7227
            DbParameter objParamXml = null;
            try
            {

                objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                objParamXml = objCommand.CreateParameter();//rkaur27 : RMA -7227
                objParamXml.Direction = ParameterDirection.Input;
                objParamXml.Value = p_objSchedule.ReportXML.ToString();
                objParamXml.ParameterName = "XML";
                objCommand.Parameters.Add(objParamXml); //rkaur27 : RMA -7227 

                sSQL = "UPDATE TM_REP_SCHEDULE " +
                    "SET REPORT_ID = " + p_objSchedule.ReportId.ToString() + ", " +
                    "LAST_RUN_DTTM = '" + p_objSchedule.LastRunDTTM.ToString() + "', " +
                    "NEXT_RUN_DATE = '" + p_objSchedule.NextRunDT.ToString() + "', " +
                    "SCHEDULE_TYPE = " + p_objSchedule.ScheduleTypeIdSM.ToString() + ", " +
                    "START_TIME = '" + p_objSchedule.TimeToRun.ToString() + "', " +
                    "START_DATE = '" + p_objSchedule.StartDate.ToString() + "', " +
                    "MON_RUN = " + p_objSchedule.Mon.ToString() + ", " +
                    "TUE_RUN = " + p_objSchedule.Tue.ToString() + ", " +
                    "WED_RUN = " + p_objSchedule.Wed.ToString() + ", " +
                    "THU_RUN = " + p_objSchedule.Thu.ToString() + ", " +
                    "FRI_RUN = " + p_objSchedule.Fri.ToString() + ", " +
                    "SAT_RUN = " + p_objSchedule.Sat.ToString() + ", " +
                    "SUN_RUN = " + p_objSchedule.Sun.ToString() + ", " +
                    "DAYOFMONTH_RUN= " + p_objSchedule.DayOfMonth.ToString() + ", " +
                    "JAN_RUN = " + p_objSchedule.Jan.ToString() + ", " +
                    "FEB_RUN = " + p_objSchedule.Feb.ToString() + ", " +
                    "MAR_RUN = " + p_objSchedule.Mar.ToString() + ", " +
                    "APR_RUN = " + p_objSchedule.Apr.ToString() + ", " +
                    "MAY_RUN = " + p_objSchedule.May.ToString() + ", " +
                    "JUN_RUN = " + p_objSchedule.Jun.ToString() + ", " +
                    "JUL_RUN = " + p_objSchedule.Jul.ToString() + ", " +
                    "AUG_RUN = " + p_objSchedule.Aug.ToString() + ", " +
                    "SEP_RUN = " + p_objSchedule.Sep.ToString() + ", " +
                    "OCT_RUN = " + p_objSchedule.Oct.ToString() + ", " +
                    "NOV_RUN = " + p_objSchedule.Nov.ToString() + ", " +
                    "DEC_RUN = " + p_objSchedule.Dec.ToString() + ", " +
                    "JOB_NAME = '" + p_objSchedule.JobName.ToString() + "', " +
                    "JOB_DESC = '" + p_objSchedule.JobDesc.ToString() + "', " +
                    //"DSN = '" + p_objSchedule.DSN.ToString() + "', " +
                    "OUTPUT_TYPE = '" + p_objSchedule.OutputType.ToString() + "', " +
                    "OUTPUT_PATH = '" + p_objSchedule.OutputPath.ToString() + "', " +
                    "OUTPUT_PATH_URL = '" + p_objSchedule.OutputPathURL.ToString() + "', " +
                    "OUTPUT_OPTIONS = " + p_objSchedule.OutputOptions.ToString() + ", " +
                    "REPORT_XML = ~XML~, " +
                    "NOTIFICATION_TYPE = '" + p_objSchedule.NotificationType.ToString() + "', " +
                    "NOTIFY_EMAIL = '" + p_objSchedule.NotifyEmail.ToString() + "', " +
                    "NOTIFY_MSG = " + p_objSchedule.NotifyMsg.ToString() + ", " +
                    "USER_ID = " + p_objSchedule.UserId.ToString() + " " +
                    "WHERE SCHEDULE_ID = " + p_objSchedule.ScheduleId.ToString();

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();
            }
            catch (RMAppException ex)
            {
                throw new RMAppException("Error in UpdateSettings() in SortMaster : ", ex);
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.UpdateSettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objConn != null)
                    objConn.Dispose();
            }
        }

        #endregion

        # region GetReportDetails
        ///<Summary>
        /// Gets the details of OSHA reports
        ///</Summary>
        public bool GetReportDetails(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            XmlElement objTmpElement = null;
            XmlElement objSMXMLEle = null;
            string sMenuRequestType = string.Empty;

            try
            {
                if (p_objXmlDocIn.SelectSingleNode("//MenuRequestType") != null)
                {
                    objSMXMLEle = (XmlElement)p_objXmlDocIn.SelectSingleNode("//MenuRequestType");
                    sMenuRequestType = objSMXMLEle.InnerText;
                }

                if (sMenuRequestType.Equals("adminavailablereports"))
                {

                    sSQL = @"select  TM_REPORTS.REPORT_ID AS REPORTID ,TM_REPORTS.REPORT_NAME AS REPORTNAME,TM_REPORTS.REPORT_DESC AS REPORTDESC, TM_REPORTS.PRIVACY_CASE AS PRIVACYCASE FROM TM_REPORTS 
                            Join TM_REPORTS_PERM on TM_REPORTS.REPORT_ID=TM_REPORTS_PERM.REPORT_ID ORDER BY TM_REPORTS.REPORT_NAME";
                }
                else
                {
                    sSQL = @"select  TM_REPORTS.REPORT_ID AS REPORTID ,TM_REPORTS.REPORT_NAME AS REPORTNAME,TM_REPORTS.REPORT_DESC AS REPORTDESC, TM_REPORTS.PRIVACY_CASE AS PRIVACYCASE FROM TM_REPORTS 
                            Join TM_REPORTS_PERM on TM_REPORTS.REPORT_ID=TM_REPORTS_PERM.REPORT_ID WHERE TM_REPORTS_PERM.USER_ID =" + m_sUserID + "ORDER BY TM_REPORTS.REPORT_NAME";
                }

                objElement = p_objXmlDocOut.CreateElement("ReportDetails");
                p_objXmlDocOut.AppendChild(objElement);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {
                        objTmpElement = p_objXmlDocOut.CreateElement("Report");
                        objTmpElement.SetAttribute("ReportId", Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        objTmpElement.SetAttribute("ReportName", Conversion.ConvertObjToStr(objReader.GetValue(1)));
                        objTmpElement.SetAttribute("ReportDesc", Conversion.ConvertObjToStr(objReader.GetValue(2)));
                        objTmpElement.SetAttribute("PrivacyCase", Conversion.ConvertObjToStr(objReader.GetValue(3)));
                        objElement.AppendChild(objTmpElement);
                    }
                }

                sSQL = @"select TASK_TYPE_ID FROM TM_TASK_TYPE WHERE NAME='OSHA Reports'";
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {
                        objTmpElement = p_objXmlDocOut.CreateElement("OSHATaskTypeID");
                        objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        objElement.AppendChild(objTmpElement);
                    }
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetReportDetails() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
                //objXmlnode = null;
            }
            return true;

        }
        #endregion


        # region DeleteReports
        ///<Summary>
        /// Deletes OSHA reports
        ///</Summary>
        public bool DeleteReports(XmlDocument p_objXmlIn)
        {
            string sSQL = string.Empty;
            string DeleteAll = string.Empty;
            string ReportIds = string.Empty;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string count = string.Empty;
            string sTMScheduleIDs = string.Empty;
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                if (p_objXmlIn.SelectSingleNode("//Report/DeleteAllReport") != null)
                {
                    DeleteAll = p_objXmlIn.SelectSingleNode("//Report/DeleteAllReport").InnerText;
                }
                if (p_objXmlIn.SelectSingleNode("//Report/ReportIDs") != null)
                {
                    ReportIds = p_objXmlIn.SelectSingleNode("//Report/ReportIDs").InnerText;
                }
                if ((m_sAdminRights == "1") && DeleteAll == "1")
                {
                    sSQL = "DELETE FROM TM_REPORTS_PERM WHERE REPORT_ID IN (" + ReportIds + ")";
                    objCommand.CommandText = sSQL;
                    objCommand.ExecuteNonQuery();
                    count = "0";
                }
                else
                {
                    sSQL = "DELETE FROM TM_REPORTS_PERM WHERE REPORT_ID IN (" + ReportIds + ") AND USER_ID=" + m_sUserID;
                    objCommand.CommandText = sSQL;
                    objCommand.ExecuteNonQuery();

                    sSQL = "SELECT COUNT(*) FROM TM_REPORTS_PERM WHERE REPORT_ID IN (" + ReportIds + ")";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objReader.Read())
                        {
                            count = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                    }
                }
                if (count == "0")
                {

                    sSQL = "DELETE FROM TM_REPORTS WHERE REPORT_ID IN (" + ReportIds + ")";
                    objCommand.CommandText = sSQL;
                    objCommand.ExecuteNonQuery();

                    sSQL = @"select TM_SCHEDULE_ID FROM TM_REP_SCHEDULE WHERE REPORT_ID IN (" + ReportIds + ")";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objReader.Read())
                        {
                            if (sTMScheduleIDs == string.Empty)
                                sTMScheduleIDs = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            else
                                sTMScheduleIDs = sTMScheduleIDs + "," + Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                    }

                    if (sTMScheduleIDs != string.Empty)
                    {
                        sSQL = "DELETE FROM TM_SCHEDULE WHERE SCHEDULE_ID IN (" + sTMScheduleIDs + ")";
                        objCommand.CommandText = sSQL;
                        objCommand.ExecuteNonQuery();
                    }

                    sSQL = "DELETE FROM TM_REP_SCHEDULE WHERE REPORT_ID IN (" + ReportIds + ")";
                    objCommand.CommandText = sSQL;
                    objCommand.ExecuteNonQuery();
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in DeleteReports() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                // objElement = null;
                //objXmlnode = null;
            }
            return true;

        }
        #endregion

        # region GetXMLByReportID
        ///<Summary>
        /// Gets the XML based on ReportId
        ///</Summary>
        public bool GetXMLByReportID(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            XmlElement objSMXMLEle = null;
            p_objXmlDocOut = new XmlDocument();
            // akaushik5 Commneted for MITS 38389 Starts
            //string SMNetServer = "";
            // akaushik5 Commneted for MITS 38389 Ends
            string SMPath = "";
            string outputPath = string.Empty;
            String ReportValue = "";
            int ReportID = 0;
            string sMenuRequestType = string.Empty;
            try
            {
                ReportValue = p_objXmlDocIn.SelectSingleNode("//ReportID").InnerText;
                ReportID = Convert.ToInt32(ReportValue);

                if (p_objXmlDocIn.SelectSingleNode("//MenuRequestType") != null)
                {
                    objSMXMLEle = (XmlElement)p_objXmlDocIn.SelectSingleNode("//MenuRequestType");
                    sMenuRequestType = objSMXMLEle.InnerText;
                }

                if (sMenuRequestType.Equals("adminavailablereports"))
                {
                    sSQL = "SELECT TMR.REPORT_XML FROM TM_REPORTS TMR JOIN TM_REPORTS_PERM TMRP ON TMR.REPORT_ID=TMRP.REPORT_ID WHERE TMR.REPORT_ID =" + ReportID;
                }
                else
                {
                    sSQL = "SELECT TMR.REPORT_XML FROM TM_REPORTS TMR JOIN TM_REPORTS_PERM TMRP ON TMR.REPORT_ID=TMRP.REPORT_ID WHERE TMRP.USER_ID =" + m_sUserID + "AND TMR.REPORT_ID =" + ReportID;
                }



                objElement = p_objXmlDocOut.CreateElement("ReportXML");
                p_objXmlDocOut.AppendChild(objElement);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {
                        objElement = p_objXmlDocOut.CreateElement("RptXML");
                        objElement.InnerText = objReader.GetValue(0).ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);
                    }
                }

                // akaushik5 Commneted for MITS 38389 Starts
                //SMNetServer = RMConfigurationManager.GetAppSetting("SMNetServer");
                //objElement = p_objXmlDocOut.CreateElement("SMNetServer");
                //objElement.InnerText = SMNetServer;
                //p_objXmlDocOut.FirstChild.AppendChild(objElement);
                //objElement = null;
                // akaushik5 Commneted for MITS 38389 Ends

                SMPath = RMConfigurationManager.GetAppSetting("SMPath").ToString();
                objElement = p_objXmlDocOut.CreateElement("SMPath");
                objElement.InnerText = SMPath;
                p_objXmlDocOut.FirstChild.AppendChild(objElement);
                objElement = null;

                objElement = p_objXmlDocOut.CreateElement("EmailID");
                objElement.InnerText = m_sUserEmailId;
                p_objXmlDocOut.FirstChild.AppendChild(objElement);
                objElement = null;

                outputPath = RMConfigurationManager.GetAppSetting("SMDataPath");
                objElement = p_objXmlDocOut.CreateElement("SMDataPath");
                objElement.InnerText = outputPath;
                p_objXmlDocOut.FirstChild.AppendChild(objElement);
                objElement = null;

            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetXMLByReportID() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
                //objXmlnode = null;
            }
            return true;

        }
        #endregion

        # region GetJobDetails
        ///<Summary>
        /// Gets the details of OSHA Jobs
        ///</Summary>
        public bool GetJobDetails(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            XmlElement objTmpElement = null;
            string sMenuRequestType = string.Empty;
            XmlElement objSMXMLEle = null;
            string User = string.Empty;
            Dictionary<string, string> userDetails = new Dictionary<string, string>();
            string userId = string.Empty;
            try
            {
                if (p_objXmlDocIn.SelectSingleNode("//MenuRequestType") != null)
                {
                    objSMXMLEle = (XmlElement)p_objXmlDocIn.SelectSingleNode("//MenuRequestType");
                    sMenuRequestType = objSMXMLEle.InnerText;
                }
                if (sMenuRequestType.Equals("adminreportqueue"))
                {
                    sSQL = @"SELECT TM_REP_JOBS.JOB_ID,JOB_NAME,JOB_DESC,OUTPUT_PATH_URL,START_DTTM,ASSIGNED,ASSIGNED_DTTM,COMPLETE,COMPLETE_DTTM,ERROR_FLAG,OUTPUT_TYPE,OUTPUT_PATH,USER_ID,TM_REP_JOB_LOG.MSG FROM TM_REP_JOBS " +
                        "LEFT OUTER JOIN TM_REP_JOB_LOG ON TM_REP_JOB_LOG.JOB_ID=TM_REP_JOBS.JOB_ID WHERE (ARCHIVED = 0 OR ARCHIVED IS NULL) ORDER BY TM_REP_JOBS.JOB_ID DESC";
                }
                else
                {
                    sSQL = @"SELECT TM_REP_JOBS.JOB_ID,JOB_NAME,JOB_DESC,OUTPUT_PATH_URL,START_DTTM,ASSIGNED,ASSIGNED_DTTM,COMPLETE,COMPLETE_DTTM,ERROR_FLAG,OUTPUT_TYPE,OUTPUT_PATH,USER_ID,TM_REP_JOB_LOG.MSG FROM TM_REP_JOBS " +
                            "LEFT OUTER JOIN TM_REP_JOB_LOG ON TM_REP_JOB_LOG.JOB_ID=TM_REP_JOBS.JOB_ID WHERE TM_REP_JOBS.USER_ID = " + m_sUserID +
                            " AND (ARCHIVED = 0 OR ARCHIVED IS NULL) ORDER BY TM_REP_JOBS.JOB_ID DESC";
                }

                objElement = p_objXmlDocOut.CreateElement("JobDetails");
                p_objXmlDocOut.AppendChild(objElement);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {
                        objTmpElement = p_objXmlDocOut.CreateElement("Job");
                        objTmpElement.SetAttribute("JobId", Conversion.ConvertObjToStr(objReader.GetValue("JOB_ID")));
                        objTmpElement.SetAttribute("JobName", Conversion.ConvertObjToStr(objReader.GetValue("JOB_NAME")));
                        objTmpElement.SetAttribute("JobDesc", Conversion.ConvertObjToStr(objReader.GetValue("JOB_DESC")));
                        objTmpElement.SetAttribute("OutputPathURL", Conversion.ConvertObjToStr(objReader.GetValue("OUTPUT_PATH_URL")));

                        if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader.GetValue("START_DTTM"))))
                        {
                            objTmpElement.SetAttribute("StartDTTM", string.Empty);
                        }
                        else
                        {
                            objTmpElement.SetAttribute("StartDTTM", Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("START_DTTM"))).ToString());
                        }

                        objTmpElement.SetAttribute("Assigned", Conversion.ConvertObjToStr(objReader.GetValue("ASSIGNED")));
                        objTmpElement.SetAttribute("AssignedDTTM", Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("ASSIGNED_DTTM"))).ToString());
                        objTmpElement.SetAttribute("Complete", Conversion.ConvertObjToStr(objReader.GetValue("COMPLETE")));
                        objTmpElement.SetAttribute("CompleteDTTM", Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("COMPLETE_DTTM"))).ToString());
                        objTmpElement.SetAttribute("ErrorFlag", Conversion.ConvertObjToStr(objReader.GetValue("ERROR_FLAG")));
                        objTmpElement.SetAttribute("OutputType", Conversion.ConvertObjToStr(objReader.GetValue("OUTPUT_TYPE")));
                        objTmpElement.SetAttribute("OutputPath", Conversion.ConvertObjToStr(objReader.GetValue("OUTPUT_PATH")));

                        if (Conversion.ConvertObjToStr(objReader.GetValue("COMPLETE")) == "0")
                        {
                            objTmpElement.SetAttribute("ProgressMsg", Conversion.ConvertObjToStr(objReader.GetValue("MSG")));
                        }
                        else
                        {
                            objTmpElement.SetAttribute("CompleteMsg", string.Empty);
                        }

                        if (Conversion.ConvertObjToStr(objReader.GetValue("ERROR_FLAG")) == "-1")
                        {
                            objTmpElement.SetAttribute("ErrorMsg", Conversion.ConvertObjToStr(objReader.GetValue("MSG")));
                        }
                        else
                        {
                            objTmpElement.SetAttribute("ErrorMsg", string.Empty);
                        }

                        if (sMenuRequestType.Equals("adminreportqueue"))
                        {
                            userId = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));
                            if (!string.IsNullOrEmpty(userId))
                            {
                                User = string.Empty;

                                if (userDetails.ContainsKey(userId))
                                {
                                    User = userDetails[userId];
                                }
                                else
                                {
                                    sSQL = @"SELECT DISTINCT USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME " +
                                            "FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID and USER_TABLE.USER_ID = '" + Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")) + "'ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ";
                                    using (DbReader objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL))
                                    {
                                        if (objRdr.Read())
                                        {
                                            User = Conversion.ConvertObjToStr(objRdr.GetValue(2)) + " (" + Conversion.ConvertObjToStr(objRdr.GetValue(1)) + " " + Conversion.ConvertObjToStr(objRdr.GetValue(0)) + ")";
                                            userDetails.Add(userId, User);
                                        }
                                    }
                                }

                                objTmpElement.SetAttribute("User", User);
                            }
                        }
                        objElement.AppendChild(objTmpElement);

                    }
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetJobDetails() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
            }
            return true;

        }
        #endregion

        # region GetScheduleDetails
        ///<Summary>
        /// Gets the details of OSHA reports which are scheduled
        ///</Summary>
        public bool GetScheduleDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            XmlElement objTmpElement = null;
            string OrderBy = string.Empty;
            string sMenuRequestType = string.Empty;
            XmlElement objSMXMLEle = null;

            try
            {

                if (p_objXmlIn.SelectSingleNode("//MenuRequestType") != null)
                {
                    objSMXMLEle = (XmlElement)p_objXmlIn.SelectSingleNode("//MenuRequestType");
                    sMenuRequestType = objSMXMLEle.InnerText;
                }
                if (p_objXmlIn.SelectSingleNode("//OrderBy") != null)
                {
                    OrderBy = p_objXmlIn.SelectSingleNode("//OrderBy").InnerText;
                }
                if (sMenuRequestType.Equals("adminreportqueue"))
                {
                    sSQL = @"SELECT SCHEDULE_ID, TM_REP_SCHEDULE.REPORT_ID, TM_REPORTS.REPORT_NAME, SCHEDULE_TYPE, LAST_RUN_DTTM, NEXT_RUN_DATE, START_TIME FROM TM_REP_SCHEDULE,TM_REPORTS WHERE TM_REP_SCHEDULE.REPORT_ID=TM_REPORTS.REPORT_ID";
                }
                else
                {
                    sSQL = @"SELECT SCHEDULE_ID, TM_REP_SCHEDULE.REPORT_ID, TM_REPORTS.REPORT_NAME, SCHEDULE_TYPE, LAST_RUN_DTTM, NEXT_RUN_DATE, START_TIME FROM TM_REP_SCHEDULE,TM_REPORTS WHERE TM_REP_SCHEDULE.REPORT_ID=TM_REPORTS.REPORT_ID AND TM_REP_SCHEDULE.USER_ID=" + m_sUserID;
                }

                if (OrderBy == string.Empty)
                    sSQL += " ORDER BY TM_REPORTS.REPORT_NAME";
                else
                    sSQL += " ORDER BY " + OrderBy;

                objElement = p_objXmlDocOut.CreateElement("ScheduleDetails");
                p_objXmlDocOut.AppendChild(objElement);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {
                        objTmpElement = p_objXmlDocOut.CreateElement("Schedule");
                        objTmpElement.SetAttribute("ScheduleId", Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        objTmpElement.SetAttribute("ReportId", Conversion.ConvertObjToStr(objReader.GetValue(1)));
                        objTmpElement.SetAttribute("ReportName", Conversion.ConvertObjToStr(objReader.GetValue(2)));
                        objTmpElement.SetAttribute("ScheduleType", Conversion.ConvertObjToStr(objReader.GetValue(3)));
                        if (objReader.GetValue(4) != string.Empty)
                        {
                            objTmpElement.SetAttribute("LastRunDTTM", Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue(4))).ToString());
                        }
                        else
                        {
                            objTmpElement.SetAttribute("LastRunDTTM", string.Empty);
                        }
                        objTmpElement.SetAttribute("NextRunDT", Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue(5)) + Conversion.ConvertObjToStr(objReader.GetValue(6))).ToString());
                        objElement.AppendChild(objTmpElement);
                    }
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetScheduleDetails() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
            }
            return true;

        }
        #endregion

        # region GetReportsForSchedule
        ///<Summary>
        /// Gets the posted reports
        ///</Summary>
        public bool GetReportsForSchedule(ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            XmlElement objTmpElement = null;
            try
            {
                sSQL = @"SELECT JOB_ID,JOB_NAME,JOB_DESC,OUTPUT_PATH_URL,START_DTTM,ASSIGNED,ASSIGNED_DTTM,COMPLETE,COMPLETE_DTTM,ERROR_FLAG FROM TM_REP_JOBS WHERE TM_REP_JOBS.USER_ID = " + m_sUserID +
                            " AND (ARCHIVED = 0 OR ARCHIVED IS NULL) ORDER BY JOB_ID DESC";

                objElement = p_objXmlDocOut.CreateElement("JobDetails");
                p_objXmlDocOut.AppendChild(objElement);


            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetReportsForSchedule() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
            }
            return true;

        }
        #endregion

        # region DeleteScheduledReports
        ///<Summary>
        /// Deletes scheduled OSHA reports
        ///</Summary>
        public bool DeleteScheduledReports(XmlDocument p_objXmlIn)
        {
            string sSQL = string.Empty;
            string DeleteAll = string.Empty;
            string ScheduleIds = string.Empty;
            string sTMScheduleIDs = string.Empty;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string count = string.Empty;
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objConn.Open();
                objCommand = objConn.CreateCommand();

                if (p_objXmlIn.SelectSingleNode("//Schedule/ScheduleIds") != null)
                {
                    ScheduleIds = p_objXmlIn.SelectSingleNode("//Schedule/ScheduleIds").InnerText;
                }

                if (ScheduleIds != string.Empty)
                {
                    sSQL = @"select TM_SCHEDULE_ID FROM TM_REP_SCHEDULE WHERE SCHEDULE_ID IN (" + ScheduleIds + ")";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objReader.Read())
                        {
                            if (sTMScheduleIDs == string.Empty)
                                sTMScheduleIDs = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            else
                                sTMScheduleIDs = sTMScheduleIDs + "," + Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                    }
                    if (sTMScheduleIDs != string.Empty)
                    {
                        sSQL = "DELETE FROM TM_SCHEDULE WHERE SCHEDULE_ID IN (" + sTMScheduleIDs + ")";
                        objCommand.CommandText = sSQL;
                        objCommand.ExecuteNonQuery();
                    }

                    sSQL = "DELETE FROM TM_REP_SCHEDULE WHERE SCHEDULE_ID IN (" + ScheduleIds + ")";
                    objCommand.CommandText = sSQL;
                    objCommand.ExecuteNonQuery();
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in DeleteScheduleReports() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                // objElement = null;
                //objXmlnode = null;
            }
            return true;

        }
        #endregion

        # region DeleteJobs
        ///<Summary>
        /// Deletes  OSHA Jobs
        ///</Summary>
        public bool DeleteJobs(XmlDocument p_objXmlIn)
        {
            string sSQL = string.Empty;
            string DeleteAll = string.Empty;
            string JobIds = string.Empty;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string count = string.Empty;
            string sTMJobIDs = string.Empty;
            string sTMScheduleIDs = string.Empty;
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objConn.Open();
                objCommand = objConn.CreateCommand();

                if (p_objXmlIn.SelectSingleNode("//Job/JobIds") != null)
                {
                    JobIds = p_objXmlIn.SelectSingleNode("//Job/JobIds").InnerText;
                }

                if (JobIds != string.Empty)
                {
                    sSQL = "SELECT OUTPUT_PATH FROM TM_REP_JOBS WHERE COMPLETE <> 0 AND JOB_ID IN (" + JobIds + ") AND USER_ID = " + m_sUserID;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objReader.Read())
                        {
                            string FileName = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            if (File.Exists(FileName))
                                System.IO.File.Delete(FileName);
                        }
                    }

                    sSQL = @"select TM_JOBS_ID FROM TM_REP_JOBS WHERE JOB_ID IN (" + JobIds + ")";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objReader.Read())
                        {
                            if (sTMJobIDs == string.Empty)
                                sTMJobIDs = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            else
                                sTMJobIDs = sTMJobIDs + "," + Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                    }
                    if (sTMJobIDs != string.Empty)
                    {

                        sSQL = "DELETE FROM TM_JOBS WHERE JOB_ID IN (" + sTMJobIDs + ")";
                        objCommand.CommandText = sSQL;
                        objCommand.ExecuteNonQuery();
                    }
                    sSQL = @"select TM_SCHEDULE_ID FROM TM_REP_JOBS WHERE JOB_ID IN (" + JobIds + ")";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objReader.Read())
                        {
                            string TMScheduleIDs = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            if (TMScheduleIDs != string.Empty)
                            {
                                if (sTMScheduleIDs == string.Empty)
                                    sTMScheduleIDs = TMScheduleIDs;
                                else
                                    sTMScheduleIDs = sTMScheduleIDs + "," + TMScheduleIDs;
                            }
                        }
                    }
                    if (sTMScheduleIDs != string.Empty)
                    {

                        sSQL = "DELETE FROM TM_SCHEDULE WHERE SCHEDULE_ID IN (" + sTMScheduleIDs + ")";
                        objCommand.CommandText = sSQL;
                        objCommand.ExecuteNonQuery();
                    }
                    sSQL = "DELETE FROM TM_REP_JOBS WHERE JOB_ID IN (" + JobIds + ") AND USER_ID = " + m_sUserID;

                    objCommand.CommandText = sSQL;
                    objCommand.ExecuteNonQuery();
                }


            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in DeleteJobs() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                // objElement = null;
                //objXmlnode = null;
            }
            return true;

        }
        #endregion

        #region sendMail
        public bool sendMail(XmlDocument p_objXmlIn)
        {
            MailMessage objMailMessage = null;
            Attachment objAttachment = null;
            string m_sSecurityDSN = string.Empty, sSQL = string.Empty, sFromAddress = string.Empty;
            string sSubject = string.Empty;
            string sBody = string.Empty;
            string JobIds = string.Empty;
            string UserIds = string.Empty;
            string[] sPairs;
            string[] sUserIDs;
            string EmailTo = string.Empty;
            string EmailCC = string.Empty;
            try
            {
                sSubject = "Sortmaster Report Mail";
                sBody = "Report(s) in this message has been sent to you by " + m_sUserName + " (" + m_sUserEmailId + ")";
                sBody = sSubject + p_objXmlIn.SelectSingleNode("//Mail/OptionalMessage").InnerText;
                EmailCC = p_objXmlIn.SelectSingleNode("//Mail/EmailCC").InnerText;

                JobIds = p_objXmlIn.SelectSingleNode("//Mail/JobIds").InnerText;
                if (JobIds.Contains(","))
                {
                    sPairs = JobIds.Split(',');
                }
                else
                {
                    string[] strArr = new string[] { JobIds };
                    sPairs = strArr;
                }

                for (int i = 0; i < sPairs.Length; i++)
                {
                    string OutputPath = string.Empty;

                    sSQL = "SELECT OUTPUT_PATH FROM TM_REP_JOBS WHERE JOB_ID=" + sPairs[i];

                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objReader.Read())
                        {
                            OutputPath = Conversion.ConvertObjToStr(objReader.GetValue(0));

                        }
                    }

                    UserIds = p_objXmlIn.SelectSingleNode("//Mail/UserID").InnerText;
                    if (UserIds.Contains(","))
                    {
                        sUserIDs = UserIds.Split(',');
                    }
                    else
                    {
                        string[] strArr = new string[] { UserIds };
                        sUserIDs = strArr;
                    }
                    EmailTo = EmailCC;
                    for (int j = 0; j < sUserIDs.Length; j++)
                    {
                        if (sUserIDs[j] != string.Empty)
                        {
                            sSQL = @"SELECT USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + sUserIDs[j] +
                                " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID =" + m_sDSNID;

                            using (DbReader objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL))//dvatsa-cloud
                            {
                                while (objRdr.Read())
                                {
                                    if (EmailTo == string.Empty)
                                    {
                                        EmailTo = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                                    }
                                    else
                                    {
                                        EmailTo = EmailTo + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                                    }

                                }
                            }
                        }
                    }
                    objMailMessage = new MailMessage();
                    objMailMessage.From = new System.Net.Mail.MailAddress(m_sUserEmailId);
                    objMailMessage.CC.Add(new System.Net.Mail.MailAddress(EmailTo));
                    objMailMessage.Subject = sSubject;
                    objMailMessage.Body = sBody;
                    if (File.Exists(OutputPath))
                    {
                        objAttachment = new Attachment(OutputPath);
                        objMailMessage.Attachments.Add(objAttachment);
                    }
                    Mailer.SendMail(objMailMessage, m_iClientId);

                }
                return true;
            }
            catch (SmtpException ex)
            {
                throw new RMAppException("Error in sendMail() in SortMaster : ", ex);
            }
            finally
            {
                if (objMailMessage != null)
                    objMailMessage.Dispose();
                if (objAttachment != null)
                    objAttachment.Dispose();
            }
        }
        #endregion

        #region GetUsersForMail
        public bool GetUsersForMail(ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            bool IsUserListByGroup = false;
            string ssSQL = string.Empty;
            XmlNode objXmlnode = null;
            string UserID = string.Empty;
            try
            {
                sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USER_BY_GROUP_FLG'";
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objReader.Read())
                    {
                        IsUserListByGroup = Conversion.ConvertObjToBool(objReader.GetValue(0), m_iClientId);
                    }
                }


                objElement = p_objXmlDocOut.CreateElement("UserDetails");
                p_objXmlDocOut.AppendChild(objElement);
                objElement = null;

                if (!IsUserListByGroup)
                {
                    sSQL = "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + m_sDSNID + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";
                    using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sSQL))
                    {
                        while (objReader.Read())
                        {
                            if ((Conversion.ConvertObjToStr(objReader.GetValue(3)) != null && (Conversion.ConvertObjToStr(objReader.GetValue(3)) != string.Empty)))
                            {
                                objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails");
                                objElement = p_objXmlDocOut.CreateElement("User");
                                objXmlnode.AppendChild(objElement);
                                objElement = null;
                                objXmlnode = null;

                                objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails/User");
                                objElement = p_objXmlDocOut.CreateElement("option");
                                objElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(2)) + Conversion.ConvertObjToStr(objReader.GetValue(1)) + "(" + Conversion.ConvertObjToStr(objReader.GetValue(3)) + ")";
                                objElement.SetAttribute("value", objReader.GetValue(0).ToString());
                                objXmlnode.AppendChild(objElement);
                                objElement = null;

                                //objTmpElement = p_objXmlDocOut.CreateElement("User");
                                //objTmpElement.SetAttribute("DataText", Conversion.ConvertObjToStr(objReader.GetValue(2)) + Conversion.ConvertObjToStr(objReader.GetValue(1)) + "(" + Conversion.ConvertObjToStr(objReader.GetValue(3)) + ")");
                                //objTmpElement.SetAttribute("DataValue",Conversion.ConvertObjToStr(objReader.GetValue(0)));
                                //objElement.AppendChild(objTmpElement);
                            }
                        }
                    }
                }
                else
                {
                    sSQL = "SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID=" + m_sGroupId;
                    using (DbReader objRead = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        while (objRead.Read())
                        {
                            UserID = Conversion.ConvertObjToStr(objRead.GetValue(0));

                            ssSQL = "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + UserID + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";
                            using (DbReader objRdr = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), ssSQL))
                            {
                                while (objRdr.Read())
                                {
                                    if ((Conversion.ConvertObjToStr(objRdr.GetValue(3)) != null && (Conversion.ConvertObjToStr(objRdr.GetValue(3)) != string.Empty)))
                                    {
                                        objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails");
                                        objElement = p_objXmlDocOut.CreateElement("User");
                                        objXmlnode.AppendChild(objElement);
                                        objElement = null;
                                        objXmlnode = null;

                                        objXmlnode = p_objXmlDocOut.SelectSingleNode("/UserDetails/User");
                                        objElement = p_objXmlDocOut.CreateElement("option");
                                        objElement.InnerText = Conversion.ConvertObjToStr(objRdr.GetValue(2)) + Conversion.ConvertObjToStr(objRdr.GetValue(1)) + "(" + Conversion.ConvertObjToStr(objRdr.GetValue(3)) + ")";
                                        objElement.SetAttribute("value", objRdr.GetValue(0).ToString());
                                        objXmlnode.AppendChild(objElement);
                                        objElement = null;

                                        //objTmpElement = p_objXmlDocOut.CreateElement("User");
                                        //objTmpElement.SetAttribute("DataText", Conversion.ConvertObjToStr(objReader.GetValue(2)) + Conversion.ConvertObjToStr(objReader.GetValue(1)) + "(" + Conversion.ConvertObjToStr(objReader.GetValue(3)) + ")");
                                        //objTmpElement.SetAttribute("DataValue",Conversion.ConvertObjToStr(objReader.GetValue(0)));
                                        //objElement.AppendChild(objTmpElement);
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetUsersForMail() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
                objXmlnode = null;
            }
            return true;

        }
        #endregion

        # region GetScheduleDetailsSM
        ///<Summary>
        /// Gets the Schedule Details based on ScheduleId
        ///</Summary>
        public bool GetScheduleDetailsSM(int ScheduleID, ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            string SMPath = "";
            // akaushik5 Commneted for MITS 38389 Starts
            //string SMNetServer = "";
            // akaushik5 Commneted for MITS 38389 Ends
            string outputPath = string.Empty;
            p_objXmlDocOut = new XmlDocument();

            try
            {
                sSQL = "SELECT * FROM TM_REP_SCHEDULE WHERE SCHEDULE_ID =" + ScheduleID;

                objElement = p_objXmlDocOut.CreateElement("ScheduleDetails");
                p_objXmlDocOut.AppendChild(objElement);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {
                        objElement = p_objXmlDocOut.CreateElement("ScheduleType");
                        objElement.InnerText = objReader["SCHEDULE_TYPE"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("LastRunDTTM");
                        objElement.InnerText = objReader["LAST_RUN_DTTM"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("NextRunDate");
                        objElement.InnerText = objReader["NEXT_RUN_DATE"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("NextRunTime");
                        objElement.InnerText = objReader["START_TIME"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("MonRun");
                        objElement.InnerText = objReader["MON_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("TueRun");
                        objElement.InnerText = objReader["TUE_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("WedRun");
                        objElement.InnerText = objReader["WED_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("ThuRun");
                        objElement.InnerText = objReader["THU_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("FriRun");
                        objElement.InnerText = objReader["FRI_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("SatRun");
                        objElement.InnerText = objReader["SAT_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("SunRun");
                        objElement.InnerText = objReader["SUN_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("JanRun");
                        objElement.InnerText = objReader["JAN_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("FebRun");
                        objElement.InnerText = objReader["FEB_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("MarRun");
                        objElement.InnerText = objReader["MAR_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("AprRun");
                        objElement.InnerText = objReader["APR_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("MayRun");
                        objElement.InnerText = objReader["MAY_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("JunRun");
                        objElement.InnerText = objReader["JUN_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("JulRun");
                        objElement.InnerText = objReader["JUL_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("AugRun");
                        objElement.InnerText = objReader["AUG_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("SepRun");
                        objElement.InnerText = objReader["SEP_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("OctRun");
                        objElement.InnerText = objReader["OCT_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("NovRun");
                        objElement.InnerText = objReader["NOV_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("DecRun");
                        objElement.InnerText = objReader["DEC_RUN"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("OutputType");
                        objElement.InnerText = objReader["OUTPUT_TYPE"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("ReportXML");
                        objElement.InnerText = objReader["REPORT_XML"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("NotificationType");
                        objElement.InnerText = objReader["NOTIFICATION_TYPE"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("NotifyEmail");
                        objElement.InnerText = objReader["NOTIFY_EMAIL"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);

                        objElement = p_objXmlDocOut.CreateElement("TMScheduleID");
                        objElement.InnerText = objReader["TM_SCHEDULE_ID"].ToString();
                        p_objXmlDocOut.FirstChild.AppendChild(objElement);
                    }
                }

                // akaushik5 Commneted for MITS 38389 Starts
                //SMNetServer = RMConfigurationManager.GetAppSetting("SMNetServer");
                //objElement = p_objXmlDocOut.CreateElement("SMNetServer");
                //objElement.InnerText = SMNetServer;
                //p_objXmlDocOut.FirstChild.AppendChild(objElement);
                //objElement = null;
                // akaushik5 Commneted for MITS 38389 Ends

                SMPath = RMConfigurationManager.GetAppSetting("SMPath").ToString();
                objElement = p_objXmlDocOut.CreateElement("SMPath");
                objElement.InnerText = SMPath;
                p_objXmlDocOut.FirstChild.AppendChild(objElement);
                objElement = null;

                outputPath = RMConfigurationManager.GetAppSetting("SMDataPath");
                objElement = p_objXmlDocOut.CreateElement("SMDataPath");
                objElement.InnerText = outputPath;
                p_objXmlDocOut.FirstChild.AppendChild(objElement);
                objElement = null;
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetScheduleDetailsSM() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
            }
            return true;

        }
        #endregion

        # region ArchiveJob
        ///<Summary>
        /// Updates the data of OSHA Jobs
        ///</Summary>
        public bool ArchiveJob(XmlDocument p_objInputXMLDoc)
        {
            DbReader objReader = null;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            string[] sJobIDs = null;
            string sJobIds = string.Empty;
            XmlElement objSMXMLEle = null;
            string OutputPath = string.Empty;
            string ssSQL = string.Empty;
            string lDocumentId = string.Empty;
            string[] sFolderIDs = null;
            string sFolderIds = string.Empty;
            string[] sFolderNames = null;
            string sTargetFolderName = string.Empty;
            try
            {
                objSMXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Job");
                sJobIds = objSMXMLEle.SelectSingleNode("JobIDs").InnerText;
                sFolderIds = objSMXMLEle.SelectSingleNode("FolderIDs").InnerText;
                sTargetFolderName = "";
                if (sJobIds.Contains(","))
                {
                    sJobIDs = sJobIds.Split(',');
                }
                else
                {
                    string[] strArr = new string[] { sJobIds };
                    sJobIDs = strArr;
                }

                if (sFolderIds.Contains(","))
                {
                    sFolderIDs = sFolderIds.Split(',');
                }
                else
                {
                    string[] strArr = new string[] { sFolderIds };
                    sFolderIDs = strArr;
                }

                if (sTargetFolderName.Contains(","))
                {
                    sFolderNames = sTargetFolderName.Split(',');
                }
                else
                {
                    string[] strArr = new string[] { sTargetFolderName };
                    sFolderNames = strArr;
                }

                for (int j = 0; j < sJobIDs.Length; j++)
                {

                    sSQL = @"SELECT OUTPUT_PATH,JOB_NAME,JOB_DESC FROM TM_REP_JOBS WHERE TM_REP_JOBS.JOB_ID in (" + sJobIDs[j] +
                           ") AND (ARCHIVED = 0 OR ARCHIVED IS NULL) ORDER BY JOB_ID DESC";
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            string currentDTTM = Conversion.GetDateTime(DateTime.Now.ToShortDateString());
                            string JobName = Conversion.ConvertObjToStr(objRdr.GetValue(1)).Trim();
                            string JobDesc = Conversion.ConvertObjToStr(objRdr.GetValue(2));
                            OutputPath = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                            OutputPath = OutputPath.Substring(OutputPath.LastIndexOf("\\") + 1);
                            if (JobName.Length > 31) // mkaran2 : MITS 33046 : file name path 
                                JobName = JobName.Substring(0, 31);
                            for (int i = 0; i < sFolderIDs.Length; i++)
                            {
                                lDocumentId = (GetNextDocUID("DOCUMENT", m_sConnectString)).ToString();

                                ssSQL = @"INSERT INTO DOCUMENT (DOCUMENT_ID,ARCHIVE_LEVEL,CREATE_DATE,DOCUMENT_CATEGORY,DOCUMENT_NAME,DOCUMENT_CLASS,DOCUMENT_SUBJECT,DOCUMENT_TYPE,NOTES,SECURITY_LEVEL,USER_ID,
                                    DOC_INTERNAL_TYPE,DOCUMENT_CREAT_APP,DOCUMENT_EXPDTTM,DOCUMENT_ACTIVE,DOCUMENT_FILENAME,DOCUMENT_FILEPATH,FOLDER_ID,KEYWORDS) VALUES (" + lDocumentId + ",0,'" + currentDTTM + "',0,'" + JobName + "',0,'',0,'" + JobDesc + "',0,'" + m_sUserName + "',2,'',0,0,'" + OutputPath + "','','" + sFolderIDs[i] + "',NULL)";
                                objConn = DbFactory.GetDbConnection(m_sConnectString);
                                objConn.Open();
                                objConn.ExecuteNonQuery(ssSQL);
                            }
                        }
                    }
                }

                objConn = DbFactory.GetDbConnection(m_sConnectionStrTM);
                objConn.Open();
                objCommand = objConn.CreateCommand();

                sSQL = @"UPDATE TM_REP_JOBS SET TM_REP_JOBS.ARCHIVED = " + -1 + " WHERE TM_REP_JOBS.JOB_ID  in (" + sJobIds + ")";

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in ArchiveJob() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objConn != null)
                    objConn.Dispose();
            }
            return true;

        }
        #endregion

        # region GetJobMessage
        ///<Summary>
        /// Gets the message from Job_Log Table
        ///</Summary>
        public bool GetJobMessage(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            string JobId = string.Empty;
            string MessageType = string.Empty;
            try
            {

                if (p_objXmlIn.SelectSingleNode("//JobID") != null)
                {
                    JobId = p_objXmlIn.SelectSingleNode("//JobID").InnerText;
                }
                if (p_objXmlIn.SelectSingleNode("//MessageType") != null)
                {
                    MessageType = p_objXmlIn.SelectSingleNode("//MessageType").InnerText;
                }

                sSQL = @"SELECT MSG FROM TM_REP_JOB_LOG WHERE JOB_ID = " + JobId + " AND MSG_TYPE ='" + MessageType + "' ORDER BY LOG_ID DESC";

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {

                        objElement = p_objXmlDocOut.CreateElement("JobMessage");
                        objElement.InnerText = objReader.GetValue(0).ToString();
                        p_objXmlDocOut.AppendChild(objElement);
                        objElement = null;
                    }
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetJobMessage() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
            }
            return true;

        }
        #endregion

        # region GetFoldersForArchive
        ///<Summary>
        /// Gets the folder name  from Archiving
        ///</Summary>
        public bool GetFoldersForArchive(ref XmlDocument p_objXmlDocOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlDocOut = new XmlDocument();
            XmlNode objXmlnode = null;
            try
            {
                objElement = p_objXmlDocOut.CreateElement("FolderDetails");
                p_objXmlDocOut.AppendChild(objElement);
                objElement = null;


                sSQL = "SELECT FOLDER_ID,PARENT_ID,FOLDER_NAME FROM DOC_FOLDERS WHERE USER_ID='" + m_sUserName + "' ORDER BY PARENT_ID,FOLDER_NAME";
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objReader.Read())
                    {
                        //objElement = p_objXmlDocOut.CreateElement("UserDetails");
                        //p_objXmlDocOut.AppendChild(objElement);
                        //objElement = null;

                        objXmlnode = p_objXmlDocOut.SelectSingleNode("/FolderDetails");
                        objElement = p_objXmlDocOut.CreateElement("Folder");
                        objXmlnode.AppendChild(objElement);
                        objElement = null;
                        objXmlnode = null;

                        objXmlnode = p_objXmlDocOut.SelectSingleNode("/FolderDetails/Folder");
                        objElement = p_objXmlDocOut.CreateElement("option");
                        objElement.InnerText = objReader.GetValue("FOLDER_NAME").ToString();
                        objElement.SetAttribute("value", objReader.GetValue("FOLDER_ID").ToString());
                        objXmlnode.AppendChild(objElement);
                        objElement = null;
                    }
                }
            }
            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetFoldersForArchive() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
                objXmlnode = null;
            }
            return true;


        }
        #endregion

        # region GetFoldersForArchive
        ///<Summary>
        /// Gets the output path by JobID
        ///</Summary>
        public bool GetOutputPathByJobID(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut)
        {
            string sSQL = string.Empty;
            XmlElement objElement = null;
            p_objXmlOut = new XmlDocument();
            XmlElement objTmpElement = null;
            string JobID = string.Empty;
            try
            {
                if (p_objXmlIn.SelectSingleNode("//JobID") != null)
                {
                    JobID = p_objXmlIn.SelectSingleNode("//JobID").InnerText;
                }

                sSQL = "SELECT OUTPUT_PATH FROM TM_REP_JOBS WHERE JOB_ID =" + JobID;

                objElement = p_objXmlOut.CreateElement("OutputDetails");
                p_objXmlOut.AppendChild(objElement);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionStrTM, sSQL))
                {
                    while (objReader.Read())
                    {
                        objTmpElement = p_objXmlOut.CreateElement("Output");
                        objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        objElement.AppendChild(objTmpElement);
                    }
                }
            }

            catch (Exception p_objExcepion)
            {
                throw new RMAppException("Error in GetOutputPathByJobID() in SortMaster : ", p_objExcepion);
            }
            finally
            {
                objElement = null;
                objTmpElement = null;
            }
            return true;
        }
        #endregion

        #region ScheduleDetails Class
        /// <summary>
        /// ScheduleDetails Class
        /// </summary>
        public struct ScheduleDetails
        {
            private int m_iScheduleId;
            private int m_sReportId;
            private string m_sLastRunDTTM;
            private string m_sNextRunDT;
            private int m_iScheduleTypeIdSM;
            private string m_sStartTime;
            private string m_sStartDate;
            private int m_bMon;
            private int m_bTue;
            private int m_bWed;
            private int m_bThu;
            private int m_bFri;
            private int m_bSat;
            private int m_bSun;
            private int m_iDayOfMonth;
            private int m_bJan;
            private int m_bFeb;
            private int m_bMar;
            private int m_bApr;
            private int m_bMay;
            private int m_bJun;
            private int m_bJul;
            private int m_bAug;
            private int m_bSep;
            private int m_bOct;
            private int m_bNov;
            private int m_bDec;
            private string m_sJobName;
            private string m_sJobDesc;
            //private string m_sDSN;
            private string m_sOutputType;
            private string m_sOutputPath;
            private string m_sOutputPathURL;
            private string m_sOutputOptions;
            private string m_sReportXML;
            private string m_sNotificationType;
            private string m_sNotifyEmail;
            private string m_sNotifyMsg;
            private string m_sUserId;

            public int ScheduleId { get { return m_iScheduleId; } set { m_iScheduleId = value; } }
            public string TimeToRun { get { return m_sStartTime; } set { m_sStartTime = value; } }
            public int DayOfMonth { get { return m_iDayOfMonth; } set { m_iDayOfMonth = value; } }
            public int Mon { get { return m_bMon; } set { m_bMon = value; } }
            public int Tue { get { return m_bTue; } set { m_bTue = value; } }
            public int Wed { get { return m_bWed; } set { m_bWed = value; } }
            public int Thu { get { return m_bThu; } set { m_bThu = value; } }
            public int Fri { get { return m_bFri; } set { m_bFri = value; } }
            public int Sat { get { return m_bSat; } set { m_bSat = value; } }
            public int Sun { get { return m_bSun; } set { m_bSun = value; } }
            public int Jan { get { return m_bJan; } set { m_bJan = value; } }
            public int Feb { get { return m_bFeb; } set { m_bFeb = value; } }
            public int Mar { get { return m_bMar; } set { m_bMar = value; } }
            public int Apr { get { return m_bApr; } set { m_bApr = value; } }
            public int May { get { return m_bMay; } set { m_bMay = value; } }
            public int Jun { get { return m_bJun; } set { m_bJun = value; } }
            public int Jul { get { return m_bJul; } set { m_bJul = value; } }
            public int Aug { get { return m_bAug; } set { m_bAug = value; } }
            public int Sep { get { return m_bSep; } set { m_bSep = value; } }
            public int Oct { get { return m_bOct; } set { m_bOct = value; } }
            public int Nov { get { return m_bNov; } set { m_bNov = value; } }
            public int Dec { get { return m_bDec; } set { m_bDec = value; } }
            public int ReportId { get { return m_sReportId; } set { m_sReportId = value; } }
            public string LastRunDTTM { get { return m_sLastRunDTTM; } set { m_sLastRunDTTM = value; } }
            public string NextRunDT { get { return m_sNextRunDT; } set { m_sNextRunDT = value; } }
            public string StartDate { get { return m_sStartDate; } set { m_sStartDate = value; } }
            public string JobName { get { return m_sJobName; } set { m_sJobName = value; } }
            public string JobDesc { get { return m_sJobDesc; } set { m_sJobDesc = value; } }
            //public string DSN { get { return m_sDSN; } set { m_sDSN = value; } }
            public string OutputType { get { return m_sOutputType; } set { m_sOutputType = value; } }
            public string OutputPath { get { return m_sOutputPath; } set { m_sOutputPath = value; } }
            public string OutputPathURL { get { return m_sOutputPathURL; } set { m_sOutputPathURL = value; } }
            public string OutputOptions { get { return m_sOutputOptions; } set { m_sOutputOptions = value; } }
            public string ReportXML { get { return m_sReportXML; } set { m_sReportXML = value; } }
            public string NotificationType { get { return m_sNotificationType; } set { m_sNotificationType = value; } }
            public string NotifyEmail { get { return m_sNotifyEmail; } set { m_sNotifyEmail = value; } }
            public string NotifyMsg { get { return m_sNotifyMsg; } set { m_sNotifyMsg = value; } }
            public string UserId { get { return m_sUserId; } set { m_sUserId = value; } }
            public int ScheduleTypeIdSM { get { return m_iScheduleTypeIdSM; } set { m_iScheduleTypeIdSM = value; } }
        }

        #endregion

        #region JobDetails Class
        /// <summary>
        /// JobDetails Class
        /// </summary>
        public struct JobDetails
        {
            private int m_iJobId;
            private int m_iJobPriority;
            private string m_sJobName;
            private string m_sJobDesc;
            //private string m_sDSN;
            private string m_sOutputType;
            private string m_sOutputPath;
            private string m_sOutputPathURL;
            private string m_sOutputOptions;
            private string m_sReportXML;
            private string m_sStartDateTime;
            private string m_sNotificationType;
            private string m_sNotifyEmail;
            private string m_sNotifyMsg;
            private int m_sAssigned;
            private string m_sAssignedTo;
            private string m_sAssignedDTTM;
            private int m_sComplete;
            private string m_sCompleteDTTM;
            private string m_sUserId;
            private int m_sArchive;
            private int m_sErrorFlag;

            public int JobId { get { return m_iJobId; } set { m_iJobId = value; } }
            public int JobPriority { get { return m_iJobPriority; } set { m_iJobPriority = value; } }
            public string JobName { get { return m_sJobName; } set { m_sJobName = value; } }
            public string JobDesc { get { return m_sJobDesc; } set { m_sJobDesc = value; } }
            //public string DSN { get { return m_sDSN; } set { m_sDSN = value; } }
            public string OutputType { get { return m_sOutputType; } set { m_sOutputType = value; } }
            public string OutputPath { get { return m_sOutputPath; } set { m_sOutputPath = value; } }
            public string OutputPathURL { get { return m_sOutputPathURL; } set { m_sOutputPathURL = value; } }
            public string OutputOptions { get { return m_sOutputOptions; } set { m_sOutputOptions = value; } }
            public string ReportXML { get { return m_sReportXML; } set { m_sReportXML = value; } }
            public string StartDateTime { get { return m_sStartDateTime; } set { m_sStartDateTime = value; } }
            public string NotificationType { get { return m_sNotificationType; } set { m_sNotificationType = value; } }
            public string NotifyEmail { get { return m_sNotifyEmail; } set { m_sNotifyEmail = value; } }
            public string NotifyMsg { get { return m_sNotifyMsg; } set { m_sNotifyMsg = value; } }
            public int Assigned { get { return m_sAssigned; } set { m_sAssigned = value; } }
            public string AssignedTo { get { return m_sAssignedTo; } set { m_sAssignedTo = value; } }
            public string AssignedDTTM { get { return m_sAssignedDTTM; } set { m_sAssignedDTTM = value; } }
            public int Complete { get { return m_sComplete; } set { m_sComplete = value; } }
            public string CompleteDTTM { get { return m_sCompleteDTTM; } set { m_sCompleteDTTM = value; } }
            public string UserId { get { return m_sUserId; } set { m_sUserId = value; } }
            public int Archive { get { return m_sArchive; } set { m_sArchive = value; } }
            public int ErrorFlag { get { return m_sErrorFlag; } set { m_sErrorFlag = value; } }
        }

        #endregion
    }
}
