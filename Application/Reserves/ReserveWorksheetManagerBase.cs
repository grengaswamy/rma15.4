using System;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.Reserves
{
    
    interface IReserveWorksheetManager
    {
        bool RSWSecurityIsAllowed(int p_iParentSID);
    }

    public class ReserveWorksheetManagerBase : IReserveWorksheetManager
    {
        protected UserLogin m_userLogin = null;
        private int m_iClientId = 0;

        public ReserveWorksheetManagerBase(UserLogin objUserLogin, int p_iClientId) { m_userLogin = objUserLogin; m_iClientId = p_iClientId; }

        #region Security Codes for RSW
        // Security Codes for Reserve Worksheet
        public const int GC_RES_WORKSHEET = 2700;
        public const int GC_CLMNT_RSW = 0000; // TODO		
        public const int WC_RES_WORKSHEET = 23700;
        public const int VA_RES_WORKSHEET = 8900;
        public const int VA_CLMNT_RSW = 0000; // TODO
        public const int VA_UNIT_RSW = 0000; // TODO
        public const int DI_RES_WORKSHEET = 62600;
        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        public const int PC_RES_WORKSHEET = 43450;
        //End:Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314

        public bool RSWSecurityIsAllowed(int p_iSecId)
        {
            bool bReturnValue = true;

            try
            {
                switch (p_iSecId)
                {
                    case ReserveFunds.GC_SID:
                        if (!m_userLogin.IsAllowedEx(GC_RES_WORKSHEET))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                     //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
                    case ReserveFunds.PC_SID:
                        if (!m_userLogin.IsAllowedEx(PC_RES_WORKSHEET))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                      //End:Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
                    case ReserveFunds.GC_CLMNTS_SID:
                        if (!m_userLogin.IsAllowedEx(GC_CLMNT_RSW))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case ReserveFunds.WC_SID:
                        if (!m_userLogin.IsAllowedEx(WC_RES_WORKSHEET))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case ReserveFunds.VA_SID:
                        if (!m_userLogin.IsAllowedEx(VA_RES_WORKSHEET))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case ReserveFunds.VA_CLMNTS_SID:
                        if (!m_userLogin.IsAllowedEx(VA_CLMNT_RSW))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case ReserveFunds.VA_UNIT_SID:
                        if (!m_userLogin.IsAllowedEx(VA_UNIT_RSW))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case ReserveFunds.DI_SID:
                        if (!m_userLogin.IsAllowedEx(DI_RES_WORKSHEET))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    default:
                        bReturnValue = true;
                        break;
                }
                return bReturnValue;
            }
            catch
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheetManagerBase.NoSecurityContext", m_iClientId));
            }
        }

        #endregion

    }
}
