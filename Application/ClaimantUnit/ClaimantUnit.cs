﻿
using System;
using System.Xml; 
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel; 
using Riskmaster.ExceptionTypes; 
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Riskmaster.Security;



namespace Riskmaster.Application.ClaimantUnit
{
    public class ClaimantUnit
    {
        string m_sConnString = string.Empty;
        private int m_iClientId = 0;
       
        public ClaimantUnit(string p_sConnectString,int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_sConnString = p_sConnectString;
        }


        public XmlDocument GetUnitInfo(XmlDocument p_objXmlIn)
        {
            string sClaimID = string.Empty;
            //string sSQL = string.Empty;
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objRoot = null;
            XmlElement objElemTemp = null;
            XmlElement objElemTemp1 = null;
            LocalCache objCache =null;
            StringBuilder sChildSQL = null;
            int iPolicySysId = 0;
            int iInsured = 0;
            string sInsured = string.Empty;
            bool bExternal = false;
            string sExternal = string.Empty;
            StringBuilder sSQL = null;
            try
            {
                
                if(p_objXmlIn.SelectSingleNode("//ClaimId")!=null)
                {
                    sClaimID=p_objXmlIn.SelectSingleNode("//ClaimId").InnerText;
                }
                objDOM = new XmlDocument();
                objRoot = objDOM.CreateElement("Units");


                 objCache = new LocalCache(m_sConnString,m_iClientId);
                 sChildSQL = new StringBuilder();
                 sSQL = new StringBuilder();
                
                    sSQL.Append("SELECT UXC.UNIT_ROW_ID AS ROW_ID, UXC.UNIT_ID, V.VIN,V.VEHICLE_MAKE,V.VEHICLE_YEAR, V.VEHICLE_MODEL, UXC.ISINSURED,");
                    sSQL.Append(" P.POLICY_SYSTEM_ID FROM UNIT_X_CLAIM UXC INNER JOIN VEHICLE V ON UXC.UNIT_ID = V.UNIT_ID");
                    sSQL.Append(" LEFT OUTER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID = V.UNIT_ID AND PUD.UNIT_TYPE = 'V'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY_X_UNIT PXU ON PXU.UNIT_ID = V.UNIT_ID AND PXU.UNIT_TYPE = 'V'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID");
                    sSQL.Append(" WHERE CLAIM_ID = " + sClaimID + " ORDER BY V.UNIT_ID");
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnString, sSQL.ToString()))
                    {
                        objElemParent = objDOM.CreateElement("VehicleDetails");

                        while (objReader.Read())
                        {
                            if (objReader.GetValue("ISINSURED") != null && objReader.GetValue("ISINSURED") != "")
                            {
                                iInsured = Convert.ToInt32(objReader.GetValue("ISINSURED"));
                                if (iInsured == -1)
                                {
                                    sInsured = "Yes";
                                }
                                else
                                {
                                    sInsured = "No";
                                }
                            }

                            if (objReader.GetValue("POLICY_SYSTEM_ID") != null && !string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader.GetValue("POLICY_SYSTEM_ID"))))
                            {
                                iPolicySysId = Convert.ToInt32(objReader.GetValue("POLICY_SYSTEM_ID"));
                                if (iPolicySysId > 0)
                                {
                                    sExternal = "Yes";
                                }
                                else
                                {
                                    sExternal = "No";
                                }
                            }
                            else
                            {
                                sExternal = "No";
                            }


                            objElemTemp1 = objDOM.CreateElement("VehicleDetail");
                               
                             

                            objElemTemp = objDOM.CreateElement("ROW_ID");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ROW_ID")) + '|' + Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"))+ "|"+"V";
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("VIN");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VIN"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("VEHICLE_MAKE");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_MAKE"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("VEHICLE_YEAR");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_YEAR"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("VEHICLE_MODEL");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_MODEL"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("ISINSURED");
                            objElemTemp.InnerText = sInsured;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            
                            objElemTemp = objDOM.CreateElement("EXTERNAL");
                            objElemTemp.InnerText = sExternal;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;


                            objElemParent.AppendChild(objElemTemp1);

                            objElemTemp1 = null;
                               
                        }
                        objRoot.AppendChild(objElemParent);
                         
                    }

                        
                    sSQL = new StringBuilder();

                    sSQL.Append("SELECT CXP.ROW_ID, PU.PROPERTY_ID, PU.PIN, PU.DESCRIPTION, PU.ADDR1, PU.ADDR2, PU.ADDR3, PU.ADDR4, PU.CITY, PU.STATE_ID, CXP.INSURED, P.POLICY_SYSTEM_ID");
                    sSQL.Append(" FROM CLAIM_X_PROPERTYLOSS CXP INNER JOIN PROPERTY_UNIT PU ON CXP.PROPERTY_ID = PU.PROPERTY_ID");
                    sSQL.Append(" LEFT OUTER JOIN POINT_UNIT_DATA PUD ON  PUD.UNIT_ID=PU.PROPERTY_ID AND PUD.UNIT_TYPE = 'P'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY_X_UNIT PXU ON PXU.UNIT_ID = PU.PROPERTY_ID AND PXU.UNIT_TYPE = 'P'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID");
                    sSQL.Append(" WHERE CLAIM_ID = " + sClaimID + " ORDER BY PU.PROPERTY_ID");

                    using (DbReader objReader1 = DbFactory.GetDbReader(m_sConnString, sSQL.ToString()))
                    {
                          
                        objElemParent = objDOM.CreateElement("PropertyDetails");
                        while (objReader1.Read())
                        {
                            if (objReader1.GetValue("INSURED") != null && objReader1.GetValue("INSURED") != "")
                            {
                                iInsured = Convert.ToInt32(objReader1.GetValue("INSURED"));
                                if (iInsured == -1)
                                {
                                    sInsured = "Yes";
                                }
                                else
                                {
                                    sInsured = "No";
                                }
                            }

                            if (objReader1.GetValue("POLICY_SYSTEM_ID") != null && !string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader1.GetValue("POLICY_SYSTEM_ID"))))
                            {
                                iPolicySysId = Convert.ToInt32(objReader1.GetValue("POLICY_SYSTEM_ID"));
                                if (iPolicySysId > 0)
                                {
                                    sExternal = "Yes";
                                }
                                else
                                {
                                    sExternal = "No";
                                }
                            }
                            else
                            {
                                sExternal = "No";
                            }

                            objElemTemp1 = objDOM.CreateElement("PropertyDetail");                           

                            objElemTemp = objDOM.CreateElement("ROW_ID");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader1.GetValue("ROW_ID")) + '|' + Conversion.ConvertObjToStr(objReader1.GetValue("PROPERTY_ID")) + "|" + "P";
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("PIN");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader1.GetValue("PIN"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("ADDR1");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader1.GetValue("ADDR1")) + " " + Conversion.ConvertObjToStr(objReader1.GetValue("ADDR2")) + " " + Conversion.ConvertObjToStr(objReader1.GetValue("ADDR3")) + " " + Conversion.ConvertObjToStr(objReader1.GetValue("ADDR4"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("CITY");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader1.GetValue("CITY"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("STATE_ID");
                            objElemTemp.InnerText = objCache.GetStateCode(Conversion.ConvertObjToInt(objReader1.GetValue("STATE_ID"), m_iClientId));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("INSURED");                        
                            objElemTemp.InnerText = sInsured;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("EXTERNAL");
                            objElemTemp.InnerText = sExternal;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemParent.AppendChild(objElemTemp1);

                            objElemTemp1 = null;
                               
                        }
                        objRoot.AppendChild(objElemParent);
                    }
                 
                    sSQL = new StringBuilder();
                    sSQL.Append("SELECT CXS.ROW_ID,CXS.SITE_ID, SI.SITE_NUMBER,SI.ADDR1, SI.ADDR2, SI.ADDR3, SI.ADDR4,SI.CITY,SI.STATE_ID, CXS.ISINSURED, P.POLICY_SYSTEM_ID");
                    sSQL.Append(" FROM CLAIM_X_SITELOSS CXS INNER JOIN SITE_UNIT SI ON CXS.SITE_ID = SI.SITE_ID");
                    sSQL.Append(" LEFT OUTER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID = SI.SITE_ID AND PUD.UNIT_TYPE = 'S'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY_X_UNIT PXU ON PXU.UNIT_ID = SI.SITE_ID AND PXU.UNIT_TYPE = 'S'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID");
                    sSQL.Append(" WHERE CLAIM_ID =" + sClaimID + "  ORDER BY CXS.SITE_ID");
                    using (DbReader objReader2 = DbFactory.GetDbReader(m_sConnString, sSQL.ToString()))
                    {
                        objElemParent = objDOM.CreateElement("SiteUnitDetails");
                        while (objReader2.Read())
                        {

                            if (objReader2.GetValue("ISINSURED") != null && objReader2.GetValue("ISINSURED") != "")
                            {
                                iInsured = Convert.ToInt32(objReader2.GetValue("ISINSURED"));
                                if (iInsured == -1)
                                {
                                    sInsured = "Yes";
                                }
                                else
                                {
                                    sInsured = "No";
                                }
                            }

                            if (objReader2.GetValue("POLICY_SYSTEM_ID") != null && !string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader2.GetValue("POLICY_SYSTEM_ID"))))
                            {
                                iPolicySysId = Convert.ToInt32(objReader2.GetValue("POLICY_SYSTEM_ID"));
                                if (iPolicySysId > 0)
                                {
                                    sExternal = "Yes";
                                }
                                else
                                {
                                    sExternal = "No";
                                }
                            }
                            else
                            {
                                sExternal = "No";
                            }

                            objElemTemp1 = objDOM.CreateElement("SiteUnitDetail");

                            objElemTemp = objDOM.CreateElement("ROW_ID");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("ROW_ID")) + '|' + Conversion.ConvertObjToStr(objReader2.GetValue("SITE_ID")) + "|" + "S";
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("SITE_NUMBER");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("SITE_NUMBER"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("ADDR1");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("ADDR1")) + " " + Conversion.ConvertObjToStr(objReader2.GetValue("ADDR2")) + " " + Conversion.ConvertObjToStr(objReader2.GetValue("ADDR3")) + " " + Conversion.ConvertObjToStr(objReader2.GetValue("ADDR4"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("CITY");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("CITY"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("STATE_ID");
                            objElemTemp.InnerText = objCache.GetStateCode(Conversion.ConvertObjToInt(objReader2.GetValue("STATE_ID"), m_iClientId));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;


                            objElemTemp = objDOM.CreateElement("ISINSURED");                             
                            objElemTemp.InnerText = sInsured;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("EXTERNAL");
                            objElemTemp.InnerText = sExternal;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;


                            objElemParent.AppendChild(objElemTemp1);

                            objElemTemp1 = null;
                        }
                        objRoot.AppendChild(objElemParent);
                    }

                    sSQL = new StringBuilder();

                    sSQL.Append("SELECT CO.ROW_ID, CO.OTHER_UNIT_ID, E.LAST_NAME, E.FIRST_NAME,E.ADDR1, E.ADDR2, E.ADDR3, E.ADDR4,E.CITY, E.STATE_ID, CO.ISINSURED, P.POLICY_SYSTEM_ID");
                    sSQL.Append(" FROM ENTITY E INNER JOIN OTHER_UNIT OU ON OU.ENTITY_ID = E.ENTITY_ID");
                    sSQL.Append(" INNER JOIN CLAIM_X_OTHERUNIT CO ON CO.OTHER_UNIT_ID = OU.OTHER_UNIT_ID");
                    sSQL.Append(" LEFT OUTER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID = OU.OTHER_UNIT_ID AND PUD.UNIT_TYPE = 'SU'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY_X_UNIT PXU ON PXU.UNIT_ID = OU.OTHER_UNIT_ID AND PXU.UNIT_TYPE = 'SU'");
                    sSQL.Append(" LEFT OUTER JOIN POLICY P ON P.POLICY_ID = PXU.POLICY_ID");
                    sSQL.Append(" WHERE CLAIM_ID =" + sClaimID + " ORDER BY OU.OTHER_UNIT_ID");
                    using (DbReader objReader3 = DbFactory.GetDbReader(m_sConnString, sSQL.ToString()))
                    {
                        objElemParent = objDOM.CreateElement("StatUnitDetails");
                        while (objReader3.Read())
                        {

                            if (objReader3.GetValue("ISINSURED") != null && objReader3.GetValue("ISINSURED") != "")
                            {
                                iInsured = Convert.ToInt32(objReader3.GetValue("ISINSURED"));
                                if (iInsured == -1)
                                {
                                    sInsured = "Yes";
                                }
                                else
                                {
                                    sInsured = "No";
                                }
                            }

                            if (objReader3.GetValue("POLICY_SYSTEM_ID") != null && !string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader3.GetValue("POLICY_SYSTEM_ID"))))
                            {
                                iPolicySysId = Convert.ToInt32(objReader3.GetValue("POLICY_SYSTEM_ID"));
                                if (iPolicySysId > 0)
                                {
                                    sExternal = "Yes";
                                }
                                else
                                {
                                    sExternal = "No";
                                }
                            }
                            else
                            {
                                sExternal = "No";
                            }

                            objElemTemp1 = objDOM.CreateElement("StatUnitDetail");

                            objElemTemp = objDOM.CreateElement("ROW_ID");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader3.GetValue("OTHER_UNIT_ID")) + '|' + Conversion.ConvertObjToStr(objReader3.GetValue("OTHER_UNIT_ID")) + "|" + "SU";
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("LAST_NAME");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader3.GetValue("LAST_NAME"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("FIRST_NAME");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader3.GetValue("FIRST_NAME"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("ADDR1");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader3.GetValue("ADDR1")) + " " + Conversion.ConvertObjToStr(objReader3.GetValue("ADDR2")) + " " + Conversion.ConvertObjToStr(objReader3.GetValue("ADDR3")) + " " + Conversion.ConvertObjToStr(objReader3.GetValue("ADDR4"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("CITY");
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader3.GetValue("CITY"));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("STATE_ID");
                            objElemTemp.InnerText = objCache.GetStateCode(Conversion.ConvertObjToInt(objReader3.GetValue("STATE_ID"), m_iClientId));
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("ISINSURED");
                            objElemTemp.InnerText = sInsured;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;

                            objElemTemp = objDOM.CreateElement("EXTERNAL");
                            objElemTemp.InnerText = sExternal;
                            objElemTemp1.AppendChild(objElemTemp);
                            objElemTemp = null;


                            objElemParent.AppendChild(objElemTemp1);

                            objElemTemp1 = null;
                       
                        }
                        objRoot.AppendChild(objElemParent);
                    }

                 
            objDOM.AppendChild(objRoot);
            objRoot = null;
            return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimantUnit.GetUnitInfo.Error",m_iClientId), p_objException);
            }
            finally
            {
                objDOM = null;
                sSQL = null;
            }
        }        
     }
}
