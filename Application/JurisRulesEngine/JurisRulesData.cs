﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;


namespace Riskmaster.Application.JurisRulesEngine
{
    /// <summary>
    /// Builds the appropriate Jurisdictional Rules
    /// </summary>
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("E51C920E-88CC-4046-88F1-6B335C99CA64")]
    [ComVisible(true)]
    public class JurisRulesData: IJurisRulesData
    {
        private const string INCOME_LIMIT_COLUMN = "INCOME_LIMIT";

        /// <summary>
        /// Provides the ability to look up individual Jurisdictional rules
        /// using a special DataTable query
        /// </summary>
        /// <param name="strStateName">string containing the state name</param>
        /// <param name="strStateYear">string containing the state year</param>
        /// <returns>boolean indicating whether or not the Jurisdictional Rules pass
        /// the specified criteria</returns>
        internal bool LookupJurisRulesQuery(string strStateName, string strStateYear)
        {
            BuildJurisRules objBldJurisRules = new BuildJurisRules();
            StringBuilder strSelectStmtBuilder = new StringBuilder();

            DataTable dtJurisRules = objBldJurisRules.BuildJurisRulesData();

            strSelectStmtBuilder.Append("YEAR=" + strStateYear + " ");
            strSelectStmtBuilder.Append("AND STATE='" + strStateName + "' ");
            strSelectStmtBuilder.Append("AND INCOME_LIMIT IS NOT NULL");

            DataRow[] arrRows = dtJurisRules.Select(strSelectStmtBuilder.ToString());

            //If there are rows then find the specific column value
            if (arrRows.Length > 0)
            {
                return true;
            }//if
            else
            {
                return false;
            }//else
        }//method: LookupJurisRulesQuery()

        /// <summary>
        /// Provides the ability to look up individual Jurisdictional rules
        /// using a Rows Find method by specifying the Primary Key
        /// </summary>
        /// <param name="strStateName">string containing the state name</param>
        /// <param name="strStateYear">string containing the state year</param>
        /// <param name="intIncomeLimit">out parameter containing the actual income limit value</param>
        /// <returns>boolean indicating whether or not the Jurisdictional Rules pass
        /// the specified criteria</returns>
        internal bool LookupJurisRules(string strStateName, string strStateYear, out int intIncomeLimit)
        {
            List<object> arrKeys = new List<object>();
            BuildJurisRules objBldJurisRules = new BuildJurisRules();

            DataTable dtJurisRules = objBldJurisRules.BuildJurisRulesData();

            arrKeys.Add(strStateName);
            arrKeys.Add(strStateYear);

            //Retrieve a DataRow if it exists
            DataRow dtRow = dtJurisRules.Rows.Find(arrKeys.ToArray());

            //Check if the row exists
            if (dtRow != null)
            {
                return Int32.TryParse(dtRow[INCOME_LIMIT_COLUMN].ToString(), out intIncomeLimit);
            }//if
            else
            {
                intIncomeLimit = 0;
                return false;
            }//else
        }//method: LookupJurisRules()


        /// <summary>
        /// Finds the specific Jurisdictional Rules record specified by
        /// the state name and year as the filter criteria
        /// </summary>
        /// <param name="strStateName">string containing the state name</param>
        /// <param name="strStateYear">string containing the state year</param>
        /// <returns>integer containing the record value from the DataTable</returns>
        public int FindJurisRulesRecord(string strStateName, string strStateYear)
        {
            bool blnRecordExists = false;
            int intRecordValue = 0;

            //Determine if the record exists
            blnRecordExists = LookupJurisRules(strStateName, strStateYear, out intRecordValue);

            //returns the actual record value from the DataTable
            return intRecordValue;
        }//method: FindJurisRulesRecord()

       
    }
}
