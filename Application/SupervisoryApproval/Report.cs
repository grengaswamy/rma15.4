using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;

using C1.C1PrintDocument;

namespace Riskmaster.Application.SupervisoryApproval
{
	/**************************************************************
	 * $File		: Report.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 02/02/2005
	 * $Author		: Parag Sarin
	 * $Comment		: Report class has functions to create Supervisory Approval Report.
	 * $Source		:  	
	**************************************************************/
	/// <summary>
	/// Report class.
	/// </summary>
	internal class Report
	{
		#region private variables Declaration
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";

		/// <summary>
		/// C1PrintDocumnet object instance.
		/// </summary>
		private C1PrintDocument m_objPrintDoc = null ;

		/// <summary>
		/// CurrentX
		/// </summary>
		private double m_dblCurrentX = 0 ;

		/// <summary>
		/// CurrentY
		/// </summary>
		private double m_dblCurrentY = 0 ;

		/// <summary>
		/// Font object.
		/// </summary>
		private Font m_objFont = null ;

		/// <summary>
		/// Text object.
		/// </summary>
		private RenderText m_objText = null ;

		/// <summary>
		/// PageWidth
		/// </summary>
		private double m_dblPageWidth = 0.0 ;

		/// <summary>
		/// PageHeight
		/// </summary>
		private double m_dblPageHeight = 0.0 ;
	
		/// <summary>
		/// ClientWidth
		/// </summary>
		private double m_dblClientWidth = 0.0 ;

		private string m_SupervisoryApprovalPath = string.Empty;
                
        /// <summary>
        /// Client Id for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;

        //Ash - cloud, config settings moved to DB
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

		/// //rsushilaggar 06/04/2010 MITS 20938
		/// </summary>
		private bool m_bIsLSSEnabled = false;	
		/// <summary>
		/// //rsushilaggar 06/04/2010 MITS 20938
		/// </summary>
		private bool m_bIsEntityApproval = false;	
		//rsushilaggar 06/04/2010 MITS 20938
		public bool IsPayment { get; set; }
		#endregion

		#region Constants Declaration
		/// <summary>
		/// Left alignment
		/// </summary>
		private const string LEFT_ALIGN = "1" ;
		/// <summary>
		/// Right alignment
		/// </summary>
		private const string RIGHT_ALIGN = "2" ;
		/// <summary>
		/// Center alignment
		/// </summary>
		private const string CENTER_ALIGN = "3" ;
		/// <summary>
		/// Number of rows in a report page
		/// </summary>
		private const int NO_OF_ROWS=54;
		#endregion

		#region Report class Constructor 
		/// <summary>
		/// Report class constructor 
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		public Report(string p_sDSN, int p_iClientId)
		{
			m_sDSN=p_sDSN;
            m_iClientId = p_iClientId;
			m_objPrintDoc = new C1PrintDocument();
			m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;			
			m_objPrintDoc.PageSettings.Margins.Top = 0 ;
			m_objPrintDoc.PageSettings.Margins.Left = 0 ;
			m_objPrintDoc.PageSettings.Margins.Bottom = 0 ;
			m_objPrintDoc.PageSettings.Margins.Right = 0 ;
			m_SupervisoryApprovalPath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "SupervisorApproval");
			//rsushilaggar : Vendor's Supervisor Approval MITS 20606 05/05/2010
			SysSettings objSettings = new SysSettings(p_sDSN,m_iClientId);//Ash - cloud
			if (objSettings != null)
			{
				m_bIsLSSEnabled = objSettings.ShowLSSInvoice;
				m_bIsEntityApproval = objSettings.UseEntityApproval;
			}
			//end: rsushilaggar

            // Ash - cloud
            m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sDSN, m_iClientId);
		}
		#endregion

		#region Property Declarations
		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		private double PageHeight 
		{
			get
			{
				return( m_dblPageHeight ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double PageWidth 
		{
			get
			{
				return( m_dblPageWidth ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double ClientWidth 
		{
			get
			{
				return( m_dblClientWidth ) ;
			}	
			set
			{
				m_dblClientWidth=value;
			}
		}
		
		/// <summary>
		/// Read/Write property for CurrentX.
		/// </summary>
		private double CurrentX 
		{
			get
			{
				return( m_dblCurrentX ) ;
			}
			set
			{
				m_dblCurrentX = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CurrentY.
		/// </summary>
		private double CurrentY 
		{
			get
			{
				return( m_dblCurrentY ) ;
			}
			set
			{
				m_dblCurrentY = value ;
			}
		}		
		/// <summary>
		/// Gets unique filename
		/// Changes made by Ratheen Chaturvedi on 8/6/2005 to create folder “SupervisorApprovalTempFiles” in “C” drive
		/// </summary>
		private string TempFile
		{
			get
			{
				string sFileName = string.Empty;
				
				DirectoryInfo objReqFolder = new DirectoryInfo(m_SupervisoryApprovalPath);
				
				if(!objReqFolder.Exists)
					objReqFolder.Create();

				sFileName = String.Format("{0}\\{1}", m_SupervisoryApprovalPath, Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
				
				return sFileName;
			}
		}
		/// <summary>
		/// Gets LSS Invoice ID. Check the settings
		/// Created by Debabrata Biswas
		/// </summary>
		public bool IsLSSEnabled
		{
			get
			{
				return m_bIsLSSEnabled;
			}
		}

		public bool IsEntityApproval
		{
			get 
			{
				return m_bIsEntityApproval;
			}
		}
		#endregion

		#region Public Functions
		/// Name		: CreateReport
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Creates report based on passed xml string
		/// </summary>
		/// <param name="p_sXmlReportData">Xml string containing report data</param>
		/// <param name="p_objMemory">Memory object containing report</param>
		/// <returns>Success -1 or Failure -0</returns>
		public int CreateReport(string p_sXmlReportData,out MemoryStream p_objMemory)
		{
			string sReportData=null;
			DataSet objDS=null;
			double dblCharWidth=0.0;
			double dblLineHeight = 0.0 ;
			double dblLeftMargin = 10.0 ;
			double dblRightMargin = 10.0 ;
			double dblTopMargin = 10.0 ;
			double dblPageWidth = 0.0 ;
			double dblPageHeight = 0.0 ;
			double dblBottomHeader = 0.0 ;
			double dblCurrentY = 0.0 ;
			double dblCurrentX = 0.0 ;	
			string sHeader="";
			string sFile="";
			double iCounter=0;
			StringReader objReader=null;
			string[] arrColAlign = null;
			double[] arrColWidth = null;	
			p_objMemory=null;
			int iReturnValue=0;
			try
			{
				objReader=new StringReader(p_sXmlReportData);


				arrColAlign = new string[13];
				arrColWidth = new double[13];	


				sReportData=p_sXmlReportData;

			
				objDS=new DataSet();

			

				objDS.ReadXml(objReader);
				
			
				StartDoc( true,10,10,10,10 );				
				SetFont( "Arial" , 7 );
				SetFontBold(true);
				ClientWidth=15400;

				dblCharWidth = GetTextWidth( "0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ")/40 ;
				dblLineHeight = GetTextHeight( "W" );
				dblLeftMargin = dblCharWidth;
				//Debabrata Biswas MITS# :13912
				//dblRightMargin = dblCharWidth * 2 ;   
			
				dblTopMargin = dblLineHeight * 5 ;
				dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin ;
				dblPageHeight = PageHeight ;
				dblBottomHeader = dblPageHeight - ( 5 * dblLineHeight ) ;
			
//				Changed by Gagan for MITS - 9191 on 4/6/07 : Start

//				arrColWidth[0] = dblCharWidth * 14  ;// Control #
//				arrColWidth[1] = dblCharWidth * 10  ;// trans date
//				arrColWidth[2] = dblCharWidth * 30  ;// Payee Name
//				arrColWidth[3] = dblCharWidth * 15  ;// Claim Number
//				arrColWidth[4] = dblCharWidth * 19  ;// Payment Amount	
//				arrColWidth[5] = dblCharWidth * 10  ;// User	
//				arrColWidth[6] = dblCharWidth * 19  ;// Transaction Type	
//				arrColWidth[7] = dblCharWidth * 18  ;// Split Amount	
//				arrColWidth[8] = dblCharWidth * 24  ;// From/To Dates	
                //Start rsushilaggar MITS 21394/21395 Date 09/29/2010
				arrColWidth[0] = dblCharWidth * 2.5  ;// Control #
				arrColWidth[1] = dblCharWidth * 2.5  ;// trans date
				arrColWidth[2] = dblCharWidth * 4.5  ;
				// Claim Number
				arrColWidth[3] = dblCharWidth * 2.25  ;
				// Pri Claimant Last Name : Debabrata Biswas MITS# :13912
				arrColWidth[4] = dblCharWidth * 2.25;
				// Pri Claimant First Name : Debabrata Biswas     MITS# :13912  
				arrColWidth[5] = dblCharWidth * 2.25;
				// Payment Amount	
				arrColWidth[6] = dblCharWidth * 2.75  ;
				// User	Changed to 2 from 3
				arrColWidth[7] = dblCharWidth * 2  ;
				// Transaction Type	changed to 3 from 4
				arrColWidth[8] = dblCharWidth * 3.5  ;
				// Split Amount CHANGED TO 2.5 FROM 3	
				arrColWidth[9] = dblCharWidth * 2.5  ;
				// From/To Dates
				arrColWidth[10] = dblCharWidth * 4.5  ;
				// LSS Invoice ID : Debabrata Biswas MITS# :13912
                if (IsLSSEnabled)
                {
                    arrColWidth[11] = dblCharWidth * 2;
                }
                else
                {
                    arrColWidth[11] = dblCharWidth * 0.2;
                }
				// Payee Status : Debabrata Biswas MITS# :13912
				arrColWidth[12] = dblCharWidth * 3;
                //End rsushilaggar
//				Changed by Gagan for MITS - 9191 on 4/6/07 : End
			
				dblCurrentX = dblLeftMargin ;
				dblCurrentY = 900 ;
			
			

				CreateNewPage(m_sDSN,true);

				sHeader= Globalization.GetString("Report.CreateReport.Heading", m_iClientId);//rkaur27
				if (!IsLSSEnabled )
					sHeader = sHeader.Replace("Lss Invoice", "");
				//rsushilaggar 06/04/2010 MITS 20938
				if( !IsPayment || (IsPayment && !IsEntityApproval ))
					sHeader = sHeader.Replace("Payee Status", "");
				//end
		
				CurrentX=dblLeftMargin;
				CurrentY=dblCurrentY;
				//Print header
				PrintTextNoCr(sHeader);
			
			
				arrColAlign[0] = LEFT_ALIGN ;
				
				//Changed by Gagan for MITS - 9191 on 4/6/07 : Start
				//arrColAlign[1] = LEFT_ALIGN ;
				arrColAlign[1] = RIGHT_ALIGN ;
				//Changed by Gagan for MITS - 9191 on 4/6/07 : End
			
				arrColAlign[2] = LEFT_ALIGN;
				// Claim Number 
				arrColAlign[3] = LEFT_ALIGN;
				// Pri Claimant Last Name : Debabrata Biswas MITS# :13912
				arrColAlign[4] = LEFT_ALIGN;
				// Pri Claimant First Name : Debabrata Biswas MITS# :13912
				arrColAlign[5] = LEFT_ALIGN;
				// Payment Amount	
				arrColAlign[6] = RIGHT_ALIGN;
				// User
				arrColAlign[7] = LEFT_ALIGN;
				// Transaction Type
				arrColAlign[8] = LEFT_ALIGN;
				// Split Amount	
				arrColAlign[9] = RIGHT_ALIGN;
				// From/To Dates	
				arrColAlign[10] = LEFT_ALIGN;
				// LSS Invoice ID : Debabrata Biswas MITS# :13912
				arrColAlign[11] = LEFT_ALIGN;
				//Payee Status : rsushilaggar
				arrColAlign[12] = LEFT_ALIGN;

				SetFont( "Arial" , 8 );
				SetFontBold(false);
					
				dblCurrentY=1200;
				iCounter=0;
			
				for (int i=0;i<objDS.Tables[0].Rows.Count ;	i++)
				{
					CreateRow(objDS.Tables[0].Rows[i],dblLeftMargin,ref dblCurrentY,arrColAlign,dblLeftMargin,dblLineHeight,arrColWidth,dblCharWidth);
					iCounter++;
					if (iCounter==NO_OF_ROWS)
					{
						CreateNewPage(m_sDSN,false);
						iCounter=0;
						dblCurrentY=900;
					}
				}
				if ((iCounter+7) > NO_OF_ROWS)
				{
					CreateNewPage(m_sDSN,false);
					dblCurrentY=900;
				}
				else
				{
					dblCurrentY=dblCurrentY+dblLineHeight*2;
				}
				CreateFooter(dblLeftMargin,dblCurrentY,dblLineHeight,250);
				EndDoc();
				sFile=TempFile;
				Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);

				File.Delete(sFile);
				iReturnValue=1;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Report.CreateReport.Error", m_iClientId), p_objException);
			}
			finally
			{
				objDS=null;
				if (objReader!=null)
				{
					objReader.Close();
					objReader=null;
				}
				arrColAlign = null;
				arrColWidth = null;	
				m_objPrintDoc=null;
				m_objFont=null;
				m_objText=null;
			}
			return iReturnValue;

		}
		#endregion

		#region private Report Functions
		/// Name		: PrintHeader
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Prints report header.
		/// </summary>
		/// <param name="p_dblCurrentY">Current Y</param>
		/// <param name="p_iColumnCount">Columns Count</param>
		/// <param name="p_dblPageWidth">Page Width</param>
		/// <param name="p_dblLeftMargin">Left Margin</param>
		/// <param name="p_dblLineHeight">Line Height</param>
		/// <param name="p_arrCol">Columns Text</param>
		/// <param name="p_arrColWidth">Columns Width</param>
		/// <param name="p_arrColAlign">Columns Alignment.</param>
		private void PrintHeader( ref double p_dblCurrentY , int p_iColumnCount , double p_dblPageWidth , 
			double p_dblLeftMargin , double p_dblLineHeight , string[] p_arrCol , 
			double[] p_arrColWidth , string[] p_arrColAlign )
		{
			double dblCurrentX = 0.0 ;
			int iIndex = 0 ;
				
			try
			{				
				dblCurrentX = p_dblLeftMargin ;
				for( iIndex = 0 ; iIndex < p_iColumnCount ; iIndex++ )
				{
					CurrentY = p_dblCurrentY + ( p_dblLineHeight ) ;
					if( p_arrColAlign[iIndex] == LEFT_ALIGN )
					{
						CurrentX = dblCurrentX ;
						PrintText( p_arrCol[iIndex] );
					}
					else
					{
						CurrentX = dblCurrentX + p_arrColWidth[iIndex]
							- GetTextWidth( p_arrCol[iIndex] );
						PrintText( p_arrCol[iIndex] );
					}
					
					dblCurrentX += p_arrColWidth[iIndex] ;
				}
				p_dblCurrentY += p_dblLineHeight * 2 ;

			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Report.PrintHeader.Error", m_iClientId),p_objException);//rkaur27
			}						
		}

		/// Name		: CreateRow
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a record in a report
		/// </summary>
		/// <param name="p_objData">DataRow object containing a row data</param>
		/// <param name="p_dblCurrentX">Current X position</param>
		/// <param name="p_dblCurrentY">Current Y position</param>
		/// <param name="p_arrColAlign">Columns alignment</param>
		/// <param name="p_dblLeftMargin">Left margin</param>
		/// <param name="p_dblLineHeight">Line height</param>
		/// <param name="p_arrColWidth">Column width</param>
		/// <param name="p_dblCharWidth">Character width</param>
		private void CreateRow(DataRow p_objData,double p_dblCurrentX,
			ref double p_dblCurrentY,string[] p_arrColAlign,double p_dblLeftMargin,double p_dblLineHeight,
			double [] p_arrColWidth,double p_dblCharWidth)
		{
			string sData="";
			try
			{
				CurrentX = p_dblCurrentX ;
				CurrentY = p_dblCurrentY ;
				for (int iIndexJ = 0; iIndexJ < 13; iIndexJ++)
				{
					switch( iIndexJ )
					{
						case 0 :
							sData = Conversion.ConvertObjToStr(p_objData["CtlNumber"]) ;
							break;
						case 1 :
							sData = Conversion.ConvertObjToStr(p_objData["TransDate"]) ;
							break ;
						case 2 :
							sData = Conversion.ConvertObjToStr(p_objData["PayeeName"]) ;
							break ;
						case 3 :
							sData = Conversion.ConvertObjToStr(p_objData["ClaimNumber"]);
							break ;
						case 4:
							//Debabrata Biswas MITS# :13912
							sData = Conversion.ConvertObjToStr(p_objData["PrimaryClaimantName"]);
							break;
						case 5:
							sData = "";
							break;
						case 6 :
							sData = Conversion.ConvertObjToStr(p_objData["PaymentAmount"]) ;
							break ;
						case 7 :
							sData = Conversion.ConvertObjToStr(p_objData["User"]) ;
							break ;
						case 8 :
							sData = Conversion.ConvertObjToStr(p_objData["TransactionType"]) ;
							break ;
						case 9 :
							sData = Conversion.ConvertObjToStr(p_objData["SplitAmount"]) ;
							break ;
						case 10 :
							sData = Conversion.ConvertObjToStr(p_objData["FromToDate"]) ;
							break ;
						case 11:
							//rsushilaggar 06/04/2010 MITS 20938
							if (IsLSSEnabled )
							{
								//Debabrata Biswas MITS# :13912
								sData = Conversion.ConvertObjToStr(p_objData["LSSID"]);
							}
							else
							{
								sData = " ";
							}
							break;
						case 12:
							//rsushilaggar 06/04/2010 MITS 20938
							if (IsPayment && IsEntityApproval )
							{
								//Debabrata Biswas MITS# :13912
								sData = Conversion.ConvertObjToStr(p_objData["PayeeStatus"]);
							}
							else
							{
								sData = " ";
							}
							break;
					}

					//Debabrata Biswas Check for valid String MITS# :13912
					if (sData.Length > 0)
					{
						while (GetTextWidth(sData) > p_arrColWidth[iIndexJ])
							sData = sData.Substring(0, sData.Length - 1);
					}
					// Print using alignment
					if( p_arrColAlign[iIndexJ] == LEFT_ALIGN )
					{
						CurrentX = p_dblCurrentX ;
						PrintTextNoCr( " " + sData );
					}
					else
					{
						CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ] 
							- GetTextWidth( sData) - p_dblCharWidth ;
						PrintTextNoCr( sData );
					}
					p_dblCurrentX += p_arrColWidth[iIndexJ] ;
				}
				p_dblCurrentX = p_dblLeftMargin ;
				p_dblCurrentY += p_dblLineHeight;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Report.CreateRow.Error", m_iClientId),p_objException);//rkaur27
			}
		}

		/// Name		: StartDoc
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_bIsLandscape">Landscape or Portrait</param>
		/// <param name="p_Left">Left</param>
		/// <param name="p_Right">Right</param>
		/// <param name="p_Top">Top</param>
		/// <param name="p_Bottom">Bottom</param>
		private void StartDoc( bool p_bIsLandscape ,int p_Left,int p_Right
			,int p_Top,int p_Bottom)
		{		
			try
			{
				m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape ;
				m_objPrintDoc.PageSettings.Margins.Left=p_Left;
				m_objPrintDoc.PageSettings.Margins.Right=p_Right;
				m_objPrintDoc.PageSettings.Margins.Top=p_Top;
				m_objPrintDoc.PageSettings.Margins.Bottom=p_Bottom;
			
				//objRect = m_objPrintDoc.PageSettings.Bounds ;			
				//m_dblPageHeight = ( objRect.Height / 100 ) * 1440 ;
				//m_dblPageWidth = ( objRect.Width / 100 ) * 1440  ;
				if( p_bIsLandscape )
				{
					m_dblPageHeight = 8.5 * 1440 ;
					m_dblPageWidth = 11 * 1440  ;
					// JP ??? Is this correct?     m_dblPageHeight += 700 ;
				}
				else
				{
					m_dblPageHeight = 11 * 1440 ;
					m_dblPageWidth = 8.5 * 1440  ;
					// JP ??? Is this correct?     m_dblPageWidth += 700 ;
				}
				m_objPrintDoc.StartDoc();
			
				m_objFont = new Font( "Arial" , 8  );		
				m_objText = new RenderText( m_objPrintDoc );			
				m_objText.Style.Font = m_objFont ;
				m_objText.Style.WordWrap = false ;
				m_objText.Width = m_dblPageWidth ;	
				
			}
			catch( RMAppException p_objEx )
			{								
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.StartDoc.Error", m_iClientId) , p_objEx );//rkaur27				
			}				
		}

		/// Name		: GetTextHeight
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Text Height
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Height</returns>
		private double GetTextHeight( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundHeight );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.GetTextHeight.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: SetFont
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_sFontName">Font Name</param>
		/// <param name="p_dblFontSize">Font Size</param>
		private void SetFont( string p_sFontName , double p_dblFontSize )
		{
			try
			{
				m_objFont = new Font( p_sFontName , (float)p_dblFontSize );
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.SetFont.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: SetFontBold
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// Set font bold
		/// </summary>
		/// <param name="p_bBold">Bool flag</param>
		private void SetFontBold( bool p_bBold )
		{		
			try
			{
				if( p_bBold )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold );
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font = m_objFont ;

			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.SetFontBold.Error", m_iClientId) , p_objEx );//rkaur27				
			}
 
		}

		/// Name		: PrintText
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintText( string p_sPrintText )
		{
			try
			{
				m_objText.Text = p_sPrintText + "\r" ;	
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect( m_dblCurrentX , m_dblCurrentY , m_objText );			
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.PrintText.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: PrintTextNoCr
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintTextNoCr( string p_sPrintText )
		{
			try
			{
				m_objText.Text = p_sPrintText + "" ;			
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect( m_dblCurrentX , m_dblCurrentY , m_objText );						
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.PrintTextNoCr.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		private void Save( string p_sPath )
		{
			try
			{
				m_objPrintDoc.ExportToPDF( p_sPath , false );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.Save.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: EndDoc
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Call the end event.
		/// </summary>
		private void EndDoc()
		{
			try
			{
				m_objPrintDoc.EndDoc();
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.EndDoc.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: CreateNewPage
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a new page in a report
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		/// <param name="p_bFirstPage">First page or not</param>
		public void CreateNewPage(string p_sDSN,bool p_bFirstPage)
		{
			SysParms objSysSetting = null;
			RenderText m_objText = null;

			try
			{
				if (!p_bFirstPage)
				{
					m_objPrintDoc.NewPage();
				}
				m_objPrintDoc.RenderDirectRectangle(0,0,15600,11700);
				
				m_objPrintDoc.RenderDirectRectangle(0,0,15600,500,Color.Black,1,Color.LightGray);
				m_objPrintDoc.RenderDirectRectangle(0,11300,15600,11900,Color.Black,1,Color.LightGray);
				m_objText = new RenderText( m_objPrintDoc );	
				m_objText.Text=" ";
				CurrentX=m_objText.BoundWidth;
			
				m_objText.Text=Globalization.GetString("Report.CreateReport.TransactionsForSupervisoryApproval", m_iClientId);//rkaur27
				m_objPrintDoc.RenderDirectText(CurrentX,100,m_objText.Text,m_objText.Width,new Font("Arial",11,FontStyle.Bold),Color.Black,AlignHorzEnum.Left);
				m_objText = new RenderText( m_objPrintDoc );

                objSysSetting = new SysParms(p_sDSN, m_iClientId);
				m_objText.Text=objSysSetting.SysSettings.ClientName+"  "+Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now),"d")+"  ";
				objSysSetting=null;
				CurrentX=(ClientWidth-m_objText.BoundWidth);
		
				m_objPrintDoc.RenderDirectText(CurrentX,100,m_objText.Text,m_objText.BoundWidth,new Font("Arial",11,FontStyle.Bold),Color.Black,AlignHorzEnum.Right);
				m_objText = new RenderText( m_objPrintDoc );
			
				m_objText.Text=Globalization.GetString("Report.CreateReport.Page", m_iClientId)+" [@@PageNo@@]";//rkaur27
				CurrentX=(PageWidth-m_objText.BoundWidth)/2;
				m_objPrintDoc.RenderDirectText(CurrentX,11600,m_objText.Text,m_objText.BoundWidth,new Font("Arial",11,FontStyle.Bold),Color.Black,AlignHorzEnum.Center);
			
				m_objText.Text = Globalization.GetString("Report.CreateReport.ConfidentialData", m_iClientId) + " - ";//rkaur27
				CurrentX=(ClientWidth-m_objText.BoundWidth);
				m_objPrintDoc.RenderDirectText(CurrentX,11600,m_objText.Text,m_objText.BoundWidth,new Font("Arial",11,FontStyle.Bold|FontStyle.Italic),Color.Black,AlignHorzEnum.Right);
			
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.CreateNewPage.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: CreateFooter
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates Report Footer
		/// </summary>
		/// <param name="p_dCurrentX">X Position</param>
		/// <param name="p_dCurrentY">Y Position</param>
		/// <param name="p_dHeight">Height</param>
		/// <param name="p_dblSpace">Space between 2 words</param>
		private void CreateFooter(double p_dCurrentX,double p_dCurrentY,double p_dHeight,double p_dblSpace)
		{
			try
			{
				m_objText.Style.Font=new Font("Arial",7.5f,FontStyle.Underline|FontStyle.Bold);
				m_objText.Text = m_MessageSettings["DEF_NOTE"].ToString();
				p_dCurrentY=p_dCurrentY+p_dHeight;
			
				m_objPrintDoc.RenderDirect( p_dCurrentX , p_dCurrentY , m_objText );
				m_objText.Style.Font=new Font("Arial",7.5f,FontStyle.Bold);
				p_dCurrentY=p_dCurrentY+p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_RMPROPRIETARY"].ToString();
	
				m_objPrintDoc.RenderDirect( p_dCurrentX +p_dblSpace, p_dCurrentY , m_objText );
				p_dCurrentY=p_dCurrentY+p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_NAME"].ToString();
			
				m_objPrintDoc.RenderDirect( p_dCurrentX+p_dblSpace , p_dCurrentY , m_objText );
				p_dCurrentY=p_dCurrentY+p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_RMCOPYRIGHT"].ToString();
			
				m_objPrintDoc.RenderDirect( p_dCurrentX+p_dblSpace, p_dCurrentY , m_objText );
				p_dCurrentY=p_dCurrentY+p_dHeight;
				m_objText.Text = m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString();
			
				m_objPrintDoc.RenderDirect( p_dCurrentX+p_dblSpace , p_dCurrentY , m_objText );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.CreateFooter.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		/// Name		: GetTextWidth
		/// Author		: Parag Sarin
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get text width.
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Width</returns>
		private double GetTextWidth( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundWidth );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Report.GetTextWidth.Error", m_iClientId) , p_objEx );//rkaur27				
			}
		}

		#endregion
	}
}
