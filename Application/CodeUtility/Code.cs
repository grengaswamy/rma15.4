using System;
using System.Data; 
using Riskmaster.Db; 
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes; 


namespace Riskmaster.Application.CodeUtility
{
	
	/// <summary>	
	///Author  :   Navneet Sota & Pankaj Chowdhury
	///Dated   :   26th,Nov 2004
	///Purpose :   A class representing the Codes/Codes_Text table. This is a 
	///			wrapper around the Codes, Codes_Text table with the columns of 
	///			the table being exposed as properties. This class is used as 
	///			a container/holder of data while doing the processing in 
	///			UtilityDriver class.
	/// </summary>
	

	internal class Code
	{

		#region Variable Declarations	

		/// <summary>
		/// Private variable for Codes IND_STANDARD_CODE
		/// </summary>
		private int m_iIndStandardCode = 0;

		/// <summary>
		/// Private variable to store Codes LINE_OF_BUS_CODE
		/// </summary>
		private int m_iLineOfBusCode = 0;
	
		/// <summary>
		/// Private variable for Codes CODE_ID
		/// </summary>
		private int m_iCodeId = 0;

		/// <summary>
		/// Private variable for Codes TABLE_ID
		/// </summary>
		private int m_iTableId = 0;
		
		/// <summary>
		/// Private variable for Codes RELATED_CODE_ID
		/// </summary>
		private int m_iRelatedCodeId = 0;

		/// <summary>
		/// Private variable for Codes DELETED_FLAG
		/// </summary>
		private int m_iDeletedFlag = 0;
		
		/// <summary>
		/// Private variable for Codes SHORT_CODE
		/// </summary>
		private string m_sShortCode = "";
			
		/// <summary>
		/// Private integer variable for Codes ORG_GROUP_EID
		/// </summary>
		private int m_iOrgGroupEid =0;
		
		/// <summary>
		/// Private variable for Codes TRIGGER_DATE_FIELD
		/// </summary>
		private string m_sTriggerDateField = ""; 
		
		/// <summary>
		/// Private variable for Codes EFF_START_DATE
		/// </summary>
		private string m_sStartDate = ""; 

		/// <summary>
		/// Private variable for Codes EFF_END_DATE
		/// </summary>
		private string m_sEndDate = ""; 

		/// <summary>
		/// Private variable for Glossary TABLE_NAME
		/// </summary>
		private string m_sUserTableName="";
	
		/// <summary>
		/// Private integer variable for CodeText's LANGUAGE_CODE
		/// </summary>
		private int m_iLangCode = 0;

		/// <summary>
		/// Private string variable for CodeText's CODE_DESC
		/// </summary>
		private string m_sCodeDesc = "";
		
		/// <summary>
		/// Private string variable for ABBREVIATION of Entity table
		/// </summary>
		private string m_sOrgAbbreviation = "";

		/// <summary>
		/// Private variable for Glossary's IND_STAND_TABLE_ID
		/// </summary>
		private int m_iIndStandTableId = 0;

		/// <summary>
		/// Private variable for Glossary's RELATED_TABLE_ID
		/// </summary>
		private int m_iRelatedTableId = 0;

		/// <summary>
		/// Private variable for Glossary's LINE_OF_BUS_FLAG
		/// </summary>
		private int m_iLineOfBusFlag = 0;

		/// <summary>
		/// Private variable for Org Name
		/// </summary>
		private string m_sOrganisation = "";

		/// <summary>
		/// Private variable for Ind Standard Short Code
		/// </summary>
		private string m_sIndShortCode="";

		/// <summary>
		/// Private variable for Ind Standard Code Description
		/// </summary>
		private string m_sIndCodeDesc="";

		/// <summary>
		/// Private variable for Line of Business Short Code
		/// </summary>
		private string m_sLobShortCode="";

		/// <summary>
		/// Private variable for Line of Business Code Description
		/// </summary>
		private string m_sLobCodeDesc="";

		/// <summary>
		/// Private variable for Relative Short Code
		/// </summary>
		private string m_sRelShortCode="";

		/// <summary>
		/// Private variable for Relative Code Description
		/// </summary>
		private string m_sRelCodeDesc="";

        private int m_iClientId = 0;

		#endregion

		#region Constructor

		/// <summary>
		/// Constructor populates the instances from the datarow
		/// </summary>
		/// <remarks></remarks> 
		/// <param name="p_objRow">Datarow for each row</param>
        internal Code(DataRow p_objRow, string p_sConnectionString, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			//Variables for Codes table
			m_iIndStandardCode = DbRow.GetIntField( p_objRow , "IND_STANDARD_CODE");
			m_iLineOfBusCode = DbRow.GetIntField( p_objRow ,"LINE_OF_BUS_CODE");
			m_iCodeId = DbRow.GetIntField( p_objRow, "CODE_ID");
			m_iTableId = DbRow.GetIntField( p_objRow, "TABLE_ID");
			m_sShortCode = DbRow.GetStringField( p_objRow ,"SHORT_CODE");
			m_iRelatedCodeId = DbRow.GetIntField( p_objRow ,"RELATED_CODE_ID");
			m_iDeletedFlag = DbRow.GetIntField( p_objRow, "DELETED_FLAG");
			m_sTriggerDateField = DbRow.GetStringField( p_objRow ,"TRIGGER_DATE_FIELD");
			m_sStartDate = DbRow.GetStringField( p_objRow,"EFF_START_DATE");
			m_sEndDate = DbRow.GetStringField( p_objRow ,"EFF_END_DATE");
			m_iOrgGroupEid = DbRow.GetIntField( p_objRow,"ORG_GROUP_EID");
 
			//Variables for Codes_Text table
			m_sCodeDesc = DbRow.GetStringField( p_objRow ,"CODE_DESC");
			m_iLangCode = DbRow.GetIntField( p_objRow,"LANGUAGE_CODE");
	
			//Variables for Glossary table
			m_iRelatedTableId = DbRow.GetIntField( p_objRow ,"RELATED_TABLE_ID");
			m_iLineOfBusFlag =  DbRow.GetIntField( p_objRow ,"LINE_OF_BUS_FLAG");
			m_iIndStandTableId = DbRow.GetIntField( p_objRow ,"IND_STAND_TABLE_ID");
            m_sUserTableName  = DbRow.GetStringField(p_objRow,"TABLE_NAME");

			//For ind statndard code
			m_sIndShortCode = DbRow.GetStringField(p_objRow,"ICODE");
			m_sIndCodeDesc = DbRow.GetStringField(p_objRow,"IDESC"); 

			//For Line Of Business Codes
			m_sLobShortCode = DbRow.GetStringField(p_objRow,"LCODE");
			m_sLobCodeDesc = DbRow.GetStringField(p_objRow,"LDESC"); 

			//For ind Relative Code			 
			m_sRelShortCode = DbRow.GetStringField(p_objRow,"RCODE");
			m_sRelCodeDesc = DbRow.GetStringField(p_objRow,"RDESC");

			//Populate the Org Details
			LoadEntity( m_iCodeId, p_sConnectionString);

		}  
		#endregion

		#region Properties

		/// <summary>
		/// Internal property IND_STANDARD_CODE
		/// </summary>
		internal int IndStandardCode
		{
			get
			{
				return m_iIndStandardCode;
			}	
		}

		/// <summary>
		/// Internal property for LINE_OF_BUS_CODE
		/// </summary>
		internal int LineOfBusCode
		{
			get
			{
				return m_iLineOfBusCode;
			}	
		}

		/// <summary>
		/// Internal property for CODE_ID
		/// </summary>
		internal int CodeId
		{
			get
			{
				return m_iCodeId;
			}	
		}

		/// <summary>
		/// Internal property for TABLE_ID
		/// </summary>
		internal int TableId
		{
			get
			{
				return m_iTableId;
			}	
		}

		/// <summary>
		/// Internal property for SHORT_CODE
		/// </summary>
		internal string ShortCode
		{
			get
			{
				return m_sShortCode;
			}	
		}

		/// <summary>
		/// Internal property for RELATED_CODE_ID  
		/// </summary>
		internal int RelatedCodeId
		{
			get
			{
				return m_iRelatedCodeId;
			}	
		}

		/// <summary>
		/// Internal property for DELETED_FLAG  
		/// </summary>
		internal int DeletedFlag
		{
			get
			{
				return m_iDeletedFlag;
			}	
		}

		/// <summary>
		/// Internal property for TRIGGER_DATE_FIELD
		/// </summary>
		internal string TriggerDateField
		{
			get
			{
				return m_sTriggerDateField;
			}	
		}

		/// <summary>
		/// Internal property for EFF_START_DATE
		/// </summary>
		internal string StartDate
		{
			get
			{
				return m_sStartDate;
			}	
		}

		/// <summary>
		/// Internal property for EFF_END_DATE
		/// </summary>
		internal string EndDate
		{
			get
			{
				return m_sEndDate;
			}	
		}

		/// <summary>
		/// Internal Property for ORG_GROUP_EID
		/// </summary>
		internal int OrgGroupEid
		{
			get
			{
				return m_iOrgGroupEid;
			}	
		}	

		/// <summary>
		/// Internal property for Code_Text's CODE_DESC
		/// </summary>
		internal string CodeDesc
		{
			get
			{
				return m_sCodeDesc;
			}	
		}	

		/// <summary>
		/// Internal Property for Glossary's RELATED_TABLE_ID
		/// </summary>
		internal int ParentTable
		{
			get
			{
				return m_iRelatedTableId;
			}	
		}

		/// <summary>
		/// Internal property for Glossary's LINE_OF_BUS_FLAG
		/// </summary>
		internal int LineBusFlag
		{
			get
			{
				return m_iLineOfBusFlag;
			}	
		}

		/// <summary>
		/// Internal property IND_STAND_TABLE_ID
		/// </summary>
		internal int IndStandTableId
		{
			get
			{
				return m_iIndStandTableId;
			}	
		}

		/// <summary>
		/// Internal property for Ind Standard Short Code
		/// </summary>
		internal string IndShortCode
		{
			get
			{
				return m_sIndShortCode;
			}	
		}

		/// <summary>
		/// Internal property for Ind Standard Code Description
		/// </summary>
		internal string IndCodeDesc
		{
			get
			{
				return m_sIndCodeDesc;
			}	
		}

		/// <summary>
		/// Internal property for Codes_Text's Short Code
		/// </summary>
		internal string RelShortCode
		{
			get
			{
				return m_sRelShortCode;
			}	
		}

		/// <summary>
		/// Internal property for Codes_Text's CODE_DESC
		/// </summary>
		internal string RelCodeDesc
		{
			get
			{
				return m_sRelCodeDesc  ;
			}	
		}

		/// <summary>
		/// Internal property for Codes_Text's LANGUAGE_CODE
		/// </summary>
		internal int LangCode
		{
			get
			{
				return m_iLangCode;
			}	
		}

		/// <summary>
		/// Internal property for Organisation
		/// </summary>
		internal string Organisation
		{
			get
			{
				return m_sOrganisation;
			}	
		}

		/// <summary>
		/// Internal property for Entity's ABBREVIATION
		/// </summary>
		internal string OrgAbbreviation
		{
			get
			{
				return m_sOrgAbbreviation;
			}
		}

		/// <summary>
		/// Internal property for TABLE_NAME
		/// </summary>
		internal string UserTableName
		{
			get
			{
				return m_sUserTableName ;
			}
		}

		/// <summary>
		/// Internal property for Line of Business Code
		/// </summary>
		internal string LobShortCode
		{
			get
			{
				return m_sLobShortCode;
			}
		}

		/// <summary>
		/// Internal property for Line of Business Description
		/// </summary>
		internal string LobCodeDesc
		{
			get
			{
				return m_sLobCodeDesc; 
			}
		}

		#endregion

		#region Private Methods

		/// Name		: LoadEntity
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Populates the organisation details to an instance
		/// </summary>
		/// <param name="p_iCodeId">Code Id</param>
		private void LoadEntity(int p_iCodeId, string p_sConnectionString)
		{
			DbReader objRdr = null;
			try
			{
				objRdr = DbFactory.GetDbReader(p_sConnectionString ,
					"SELECT B.LAST_NAME ,B.FIRST_NAME, B.ABBREVIATION FROM CODES C,ENTITY B " + 
					"WHERE B.ENTITY_ID=C.ORG_GROUP_EID AND C.CODE_ID=" + m_iCodeId);

				if (objRdr.Read())
				{
					// Mihika 3-Mar-2006 Defect# 2388
					// Removed the stray comma when either of last name or first name is not present
					if (objRdr.GetString("LAST_NAME") != string.Empty && objRdr.GetString("FIRST_NAME") != string.Empty)
						m_sOrganisation = objRdr.GetString("LAST_NAME") + " " + "," + 
							objRdr.GetString("FIRST_NAME");
					else
						m_sOrganisation = objRdr.GetString("LAST_NAME") + objRdr.GetString("FIRST_NAME");

					m_sOrgAbbreviation = objRdr.GetString("ABBREVIATION");
				}
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Code.LoadEntity.DataErr",m_iClientId),
					p_objEx);   
			}
			finally
			{
				if (objRdr!=null)objRdr.Dispose(); 
			}
		}

		#endregion

	}// End class

    /// <summary>	
    ///Author  :   Amandeep Kaur
    ///Dated   :   11/30/2012
    ///Purpose :   A class representing the Codes_Text table. This is a 
    ///			wrapper around the Codes_Text table with the columns of 
    ///			the table being exposed as properties. This class is used as 
    ///			a container/holder of data while doing the processing in 
    ///			UtilityDriver class.
    /// </summary>
    internal class CodeDesc
    {
        #region Variable Declarations

        /// <summary>
        /// Private variable for Codes IND_STANDARD_CODE
        /// </summary>
        private int m_iCodeId = 0;
              
        /// <summary>
        /// Private variable for Codes SHORT_CODE
        /// </summary>
        private string m_sShortCode = "";

        /// <summary>
        /// Private integer variable for CodeText's LANGUAGE_CODE
        /// </summary>
        private int m_iLangCode = 0;

        /// <summary>
        /// Private string variable for CodeText's CODE_DESC
        /// </summary>
        private string m_sCodeDesc = "";		
       
        #endregion
        #region Constructor

		/// <summary>
		/// Constructor populates the instances from the datarow
		/// </summary>
		/// <remarks></remarks> 
		/// <param name="p_objRow">Datarow for each row</param>
		internal CodeDesc(DataRow p_objRow, string p_sConnectionString)
		{
			//Variables for Codes Text table	
            m_iCodeId = DbRow.GetIntField(p_objRow, "CODE_ID");
            m_sShortCode = DbRow.GetStringField(p_objRow, "SHORT_CODE");
			m_sCodeDesc = DbRow.GetStringField( p_objRow ,"CODE_DESC");
			m_iLangCode = DbRow.GetIntField( p_objRow,"LANGUAGE_CODE");	
		}  
		#endregion
        #region Properties

        /// <summary>
        /// Internal property for CODE_ID
        /// </summary>        
        internal int CodeId
        {
            get
            {
                return m_iCodeId;
            }           
        }

        /// <summary>
        /// Internal property for Codes_Text's LANGUAGE_CODE
        /// </summary>
        internal int LangCode
        {
            get
            {
                return m_iLangCode;
            }
        }

        /// <summary>
        /// Internal property for SHORT_CODE
        /// </summary>
        internal string ShortCode
        {
            get
            {
                return m_sShortCode;
            }
        }

        /// <summary>
        /// Internal property for Code_Text's CODE_DESC
        /// </summary>
        internal string CodesDesc
        {
            get
            {
                return m_sCodeDesc;
            }
        }	

        #endregion
    }
} // End namespace
