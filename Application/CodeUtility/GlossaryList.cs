using System;
using System.Collections; 

namespace Riskmaster.Application.CodeUtility
{
	/// <summary>
	///Author  :   Navneet Sota & Pankaj Chowdhury
	///Dated   :   26th,Nov 2004
	///Purpose :   Glossary List contains a collection of Glossary objects [Glossary Table rows]
	/// </summary>
	internal class GlossaryList:CollectionBase
	{

		#region Constructor

		/// <summary>
		/// Default Constructor
		/// </summary>
		internal GlossaryList(){}

		#endregion

		#region Methods

		/// Name		: Add
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds Glossary Object to the collection
		/// </summary>
		/// <param name="p_objGloss">Glossary</param>
		internal void Add(Glossary p_objGloss)
		{
			this.List.Add(p_objGloss);        
		}

		/// Name		: this
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the Glossary object for that index
		/// </summary>
		internal Glossary this[int p_iIndex]
		{        
			get
			{
				return (Glossary)this.List[p_iIndex];            
			}        
		}

		#endregion

	}//End Class
}//End Namespace
