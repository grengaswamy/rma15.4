﻿
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;

using C1.C1PrintDocument;
using System.Collections.Generic;

using System.Text.RegularExpressions; //Bharani - MITS : 36473 - Change 1/3
namespace Riskmaster.Application.CodeUtility
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   26th,Nov 2004
	///Purpose :   Implements all the functionality pertaining to the Table 
	///			Maintenance Module. Returns data to be shown on the Table 
	///			Maintenance Module in the form of XML string. Also 
	///			implements the functionality to save/edit new codes and tables.
	/// </summary>
	public class UtilityDriver
	{
		#region Variable Declaration

		/// <summary>
		/// Internal static variable for Connection String
		/// </summary>
		internal string m_sConnectString="";

		/// <summary>
		/// Private flag to check file status
		/// </summary>
		private bool m_bFileOk=false;

		/// <summary>
		/// Private variable for XML output message
		/// </summary>
		private string m_sMessage="";

		/// <summary>
		/// Private variable to store file name
		/// </summary>
		private string m_sFileName="";

        /// <summary>
        /// Private variable for edit mode
        /// </summary>
		private bool m_bEdit=false;

		/// <summary>
		/// Private variable for SYSTEM_TABLE_NAME
		/// </summary>
		private string m_sSysTableName="";
        
        /// <summary>
		/// Private variable for SYSTEM_TABLE_NAME 's Parent
		/// </summary>
        private string m_sSysTableParentName = "";
        // rahul mits 10194 - 24 sept 2007

		/// <summary>
		/// Private variable for Code Type
		/// </summary>
		private string m_sCodeType ="";

		/// <summary>
		/// Private variable for Table Name
		/// </summary>
		private string m_sTableName="";

		/// <summary>
		/// Private variable for Line count of file
		/// </summary>
		private int m_iCount=0;

		/// <summary>
		/// Private variable for Code Description
		/// </summary>
		private string m_sCodeDesc="";

		/// <summary>
		/// Private variable for Table Id
		/// </summary>
		private int m_iTableId=0;

		/// <summary>
		/// Private variable for Language Code
		/// </summary>
		private string m_sLangCode="";

		/// <summary>
		/// Private variable for DB ID need for generating lang code
		/// </summary>
		private string m_DBId="";

        /// <summary>
        /// Private variable for User ID need for generating lang code
        /// abisht MITS 10932
        /// </summary>
        private int m_UserId = 0;
        /// <summary>
        /// Private variable for lang code        
        /// </summary>
        private int m_LangCode = 0;  //Aman ML Change
        /// <summary>
        /// Private variable for lang code        
        /// </summary>
        private int m_iCodeID = 0;  //Aman ML Change

        /// <summary>
        /// ClientId for multi-tenant environment, cloud
        /// </summary>
        private int m_iClientId = 0;

		/// <summary>
		/// Enumerated type for Glossary Type
		/// </summary>
		private enum GLOSSARYTYPE:int
		{
			GLOSSARY_TYPE_ALLENTITIES = -2,
			GLOSSARY_TYPE_ALLCODES = -1,
			GLOSSARY_TYPE_TABLE = 1,
			GLOSSARY_TYPE_SYSCODE = 2,
			GLOSSARY_TYPE_USERCODE = 3,
			GLOSSARY_TYPE_ENTITY = 4,
			GLOSSARY_TYPE_ORGHIER = 5,
			GLOSSARY_TYPE_SYSSUPP = 6,
			GLOSSARY_TYPE_PEOPLE = 7,
			GLOSSARY_TYPE_ADMINTRACK = 8,
			GLOSSARY_TYPE_JURISDICTIONAL = 9,
			GLOSSARY_TYPE_IND_STAND = 10
		}

		//Changed by Gagan for MITS 8599 : Start

		/// <summary>
		/// C1PrintDocumnet object instance.
		/// </summary>
		private C1PrintDocument m_objPrintDoc = null ;

		/// <summary>
		/// CurrentX
		/// </summary>
		private double m_dblCurrentX = 0 ;
		/// <summary>
		/// CurrentY
		/// </summary>
		private double m_dblCurrentY = 0 ;
		/// <summary>
		/// Font object.
		/// </summary>
		private Font m_objFont = null ;
		/// <summary>
		/// Text object.
		/// </summary>
		private RenderText m_objText = null ;
		/// <summary>
		/// PageWidth
		/// </summary>
		private double m_dblPageWidth = 0.0 ;
		/// <summary>
		/// PageHeight
		/// </summary>
		private double m_dblPageHeight = 0.0 ;		
		/// <summary>
		/// ClientWidth
		/// </summary>
		private double m_dblClientWidth = 0.0 ;
        private string m_sTypeCode = string.Empty;

		//Changed by Gagan for MITS 8599 : End	

        //pmittal5 MITS 12685 08/05/08
        /// <summary>
        /// ArrayList for Org Hierarahcy Names
        /// </summary>
        System.Collections.ArrayList arrListName = null;

        /// <summary>
        /// ArrayList for Org Hierarahcy Levels
        /// </summary>
        System.Collections.ArrayList arrListLevel = null;
        //End - pmittal5

        // Ash - cloud, config settings moved to Db
        //private Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

		#endregion

		#region Properties
		
		//Changed by Gagan for MITS 8599 : Start

		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		private double PageHeight 
		{
			get
			{
				return( m_dblPageHeight ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double PageWidth 
		{
			get
			{
				return( m_dblPageWidth ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double ClientWidth 
		{
			get
			{
				return( m_dblClientWidth ) ;
			}	
			set
			{
				m_dblClientWidth=value;
			}
		}
		
		/// <summary>
		/// Read/Write property for CurrentX.
		/// </summary>
		private double CurrentX 
		{
			get
			{
				return( m_dblCurrentX ) ;
			}
			set
			{
				m_dblCurrentX = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CurrentY.
		/// </summary>
		private double CurrentY 
		{
			get
			{
				return( m_dblCurrentY ) ;
			}
			set
			{
				m_dblCurrentY = value ;
			}
		}		
		

		/// <summary>
		/// Gets unique filename
		/// </summary>
		private string TempFile
		{
			get
			{
				string sFileName = string.Empty;
                string strTempDirPath = string.Empty;

                strTempDirPath = RMConfigurator.TempPath;

                sFileName = String.Format("{0}\\{1}", strTempDirPath, Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                
                return sFileName;
			}
		}

		/// <summary>
		/// instead of passsing the whole login
		/// we can do with setting of few properties
		/// </summary>
		public string NLSCode
		{
			get
			{
				return m_sLangCode;
			}
			//			set 
			//			{
			//				 m_sLangCode = value;
			//			}
		}

		/// <summary>
		/// Passed DBID can be used to set up NLSCode
		/// </summary>
		public string DBID
		{
//			get
//			{
//				return m_DBId;
//			}
			set 
			{
					m_DBId = value;
			}
		}
        /// <summary>
        /// Aman ML Changes
        /// Passed the language code      
        /// </summary>
        public int LanguageCode
        {
            get
            {
                return m_LangCode;
            }
            set
            {
                m_LangCode = value;
            }
        }
        /// <summary>
        /// Aman ML Changes
        /// Passed the Code ID  
        /// </summary>
        public int CodeID
        {
            get
            {
                return m_iCodeID;
            }
            set
            {
                m_iCodeID = value;
            }
        }		
        /// <summary>
        /// Passed User ID that can be used to set up NLSCode
        /// abisht MITS 10932
        /// </summary>
        public int USERID
        {
            //			get
            //			{
            //				return m_UserId;
            //			}
            set
            {
                m_UserId = value;
            }
        }		
		/// <summary>
		/// To identify various join types for query examination and formation
		/// </summary>
		public enum JointType
		{
			INNER,
			LEFT_OUTER,
			RIGHT_OUTER
		}

		//Will hold database type
		internal Db.eDatabaseType m_sDBType;
		/// <summary>
		/// Since there is no default db type as per enum. To handle null comparison
		/// another varaible is required.
		/// </summary>
		internal bool bDBTypeSet = false;// false mean dbtype is null
		
		#endregion

		# region "Constants"

		//Changed by Gagan for MITS 8599 : Start

		private const string LEFT_ALIGN = "1" ;
		/// <summary>
		/// Right alignment
		/// </summary>
		private const string RIGHT_ALIGN = "2" ;
		/// <summary>
		/// Center alignment
		/// </summary>
		private const string CENTER_ALIGN = "3" ;
		/// <summary>
		/// Number of rows in a report page
		/// </summary>
		private const int NO_OF_ROWS=40;

		//Changed by Gagan for MITS 8599 : End

		#endregion

		#region Constructor
        /// <summary>
		/// Default Constructor. Accepts only the Connection String
		/// </summary>
		/// <param name="p_sConnectString"></param>
		public UtilityDriver(string p_sConnectString, int p_iClientId)
		{
			m_sConnectString=p_sConnectString;
			//We need to set DBType to examine query fromation
			SetDBType(p_sConnectString);
            // Ash - cloud, config settings moved to DB
            m_iClientId = p_iClientId;
            m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectString, m_iClientId);
		}

		internal void SetDBType(string p_sConnectString)
		{
			if (!bDBTypeSet)
			{
				DbConnection dbConn = DbFactory.GetDbConnection(m_sConnectString);
				try
				{
					dbConn.Open();
					m_sDBType = dbConn.DatabaseType;
					bDBTypeSet = true;
				}
				finally
				{
					if (dbConn != null)
					{
						if (dbConn.State.Equals(ConnectionState.Open))
						{
							dbConn.Close();
						}
						dbConn.Dispose();
					}				
				}
			}
		}
		#endregion

		#region Methods
		
		/// Name		: GetCode
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets data from the CODES table and returns Code object
		/// </summary>
		/// <param name="p_iCodID">Code Id</param>
		/// <returns>Code Object</returns>
		private Code GetCode(int p_iCodID)
		{
			if (m_sLangCode.Equals(string.Empty))
			{
				if (!m_DBId.Equals(string.Empty))
					m_LangCode = GetNLSCode(int.Parse(m_DBId) );

			}
            m_sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]; //Aman ML Change
			StringBuilder sbSQL = null;
			DataSet objDs =null;
			Code objCod = null;			
			try
			{
				sbSQL = new StringBuilder();
				sbSQL.Append("SELECT C.CODE_ID, C.SHORT_CODE, ");
				sbSQL.Append(" F.IND_STAND_TABLE_ID, F.RELATED_TABLE_ID, F.LINE_OF_BUS_FLAG, G.TABLE_NAME, ");
				//sbSQL.Append(" A.CODE_DESC, A.LANGUAGE_CODE, C.IND_STANDARD_CODE, IND_STD.CODE_DESC IDESC, ");
                sbSQL.Append(" A.CODE_DESC, A.LANGUAGE_CODE, C.IND_STANDARD_CODE, IND_STD_DESC.CODE_DESC IDESC, ");  //pmittal5 Mits 13600
				sbSQL.Append(" IND_STD.SHORT_CODE ICODE, C.LINE_OF_BUS_CODE, LOB.CODE_DESC LDESC, ");
				sbSQL.Append(" LOB.SHORT_CODE LCODE, C.TRIGGER_DATE_FIELD, C.EFF_START_DATE, C.EFF_END_DATE, ");
				//sbSQL.Append(" F.DELETED_FLAG,C.RELATED_CODE_ID, REL_COD.CODE_DESC RDESC, REL_COD.SHORT_CODE RCODE, ");
                sbSQL.Append(" F.DELETED_FLAG,C.RELATED_CODE_ID, REL_CODE_DESC.CODE_DESC RDESC, REL_COD.SHORT_CODE RCODE, ");  //pmittal5 Mits 13600
				sbSQL.Append(" C.TABLE_ID, C.ORG_GROUP_EID ");
												
				StringBuilder sbWhere = new StringBuilder(" WHERE ");
				StringBuilder sbFrom = new StringBuilder(" FROM ");
				string sFrom  = " [CODES][C]";
				string firstJoinAlias ="[C] ";
				string secJoinAlias = " [A] ";
				string secJoin = "[CODES_TEXT] [A]";
				string firstJoinField =" [CODE_ID] ";
				string secJoinField =" [CODE_ID] ";
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);				
				sbFrom.Append(sFrom); 
                if ((m_sDBType == Db.eDatabaseType.DBMS_IS_SQLSRVR))
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                    //sbWhere.Append(" A.CODE_ID =").Append(p_iCodID);
                    sbWhere.Append(string.Format(" A.CODE_ID = {0} ", "~CODE_ID~"));
                    
                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    //sbWhere.Append(" AND A.CODE_ID =").Append(p_iCodID);
                    sbWhere.Append(string.Format(" AND A.CODE_ID = {0} ", "~CODE_ID~"));

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                }
				sFrom="";
				firstJoinAlias =" [C] ";
				secJoin = "[GLOSSARY][F]";
				secJoinAlias ="[F]";
				firstJoinField =" [TABLE_ID] ";
				secJoinField =" [TABLE_ID] ";
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom);
				
				sFrom="";				
				firstJoinAlias =" [C] ";
				secJoin = " [GLOSSARY_TEXT] [G] ";
				secJoinAlias = " [G] ";
				firstJoinField =" [TABLE_ID] ";
				secJoinField =" [TABLE_ID] ";
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                if (m_sLangCode.Equals(string.Empty))
                {
                    sbWhere.Append(string.Empty);
                }
                else
                {
                    sbWhere.Append(string.Format(" AND G.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                    sbWhere.Append(string.Format(" AND A.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~")); //Aman ML Change
                }
                //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                //    " AND G.LANGUAGE_CODE =" + m_sLangCode);
                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom);
				
				sFrom="";
				firstJoinAlias =" [C] ";
				//secJoin = " [CODES_TEXT][IND_STD] ";
                secJoin = " [CODES][IND_STD] ";    //pmittal5 Mits 13600
				secJoinAlias =" [IND_STD] ";
				firstJoinField =" [IND_STANDARD_CODE] ";
				secJoinField =" [CODE_ID] ";

                //pmittal5 Mits 13600 06/15/09 - Modifications done to take short_code of Parent(Related) Code from CODES table
                JoinIt(ref sFrom, sbWhere, firstJoinAlias, secJoin, secJoinAlias, firstJoinField, secJoinField, JointType.LEFT_OUTER);
                sbFrom.Append(sFrom);

                sFrom = "";
                firstJoinAlias = " [IND_STD] ";
                secJoin = " [CODES_TEXT][IND_STD_DESC] ";
                secJoinAlias = " [IND_STD_DESC] ";
                firstJoinField = " [CODE_ID] ";
                secJoinField = " [CODE_ID] ";
                //End - pmittal5

				//-- ABhateja 06.23.2006
				//-- MITS 7132, IS NULL check added for LANGUAGE_CODE.This was a problem with Oracle
				//-- due to which some of the codes were not appearing.
//				sbWhere.Append( m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND IND_STD.LANGUAGE_CODE =" + m_sLangCode);
				
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                if (m_sLangCode.Equals(string.Empty))
                {
                    sbWhere.Append(string.Empty);
                }
                else
                {
                    sbWhere.Append(string.Format(" AND (IND_STD_DESC.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                    sbWhere.Append(" OR IND_STD_DESC.LANGUAGE_CODE IS NULL) ");
                }
                //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                //    //" AND (IND_STD.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD.LANGUAGE_CODE IS NULL)");
                //    " AND (IND_STD_DESC.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD_DESC.LANGUAGE_CODE IS NULL)");  //pmittal5 Mits 13600

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);

				sFrom="";
				firstJoinAlias =" [C] ";
				secJoin = " [CODES_TEXT][LOB] ";
				secJoinAlias = " [LOB] ";
				firstJoinField =" [LINE_OF_BUS_CODE] ";
				secJoinField =" [CODE_ID] ";
//				sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND LOB.LANGUAGE_CODE =" + m_sLangCode);
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                if (m_sLangCode.Equals(string.Empty))
                {
                    sbWhere.Append(string.Empty);
                }
                else
                {
                    sbWhere.Append(string.Format(" AND (LOB.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                    sbWhere.Append(" OR LOB.LANGUAGE_CODE IS NULL) ");
                }

                //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                //    " AND (LOB.LANGUAGE_CODE =" + m_sLangCode + " OR LOB.LANGUAGE_CODE IS NULL)");

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);
				
				sFrom="";
				firstJoinAlias =" [C] ";
				//secJoin = " [CODES_TEXT][REL_COD] ";
                secJoin = " [CODES][REL_COD] ";    //pmittal5 Mits 13600
				secJoinAlias = " [REL_COD] ";
				firstJoinField =" [RELATED_CODE_ID] ";
				secJoinField =" [CODE_ID] ";

                //pmittal5 Mits 13600 06/15/09 - Modifications done to take short_code of Parent(Related) Code from CODES table
                JoinIt(ref sFrom, sbWhere, firstJoinAlias, secJoin, secJoinAlias, firstJoinField, secJoinField, JointType.LEFT_OUTER);
                sbFrom.Append(sFrom);

                sFrom = "";
                firstJoinAlias = " [REL_COD] ";
                secJoin = " [CODES_TEXT][REL_CODE_DESC] ";
                secJoinAlias = " [REL_CODE_DESC] ";
                firstJoinField = " [CODE_ID] ";
                secJoinField = " [CODE_ID] ";
                //End - pmittal5

//				sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND [REL_COD].[LANGUAGE_CODE] =" + m_sLangCode);
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                if (m_sLangCode.Equals(string.Empty))
                {
                    sbWhere.Append(string.Empty);
                }
                else
                {
                    sbWhere.Append(string.Format(" AND ([REL_CODE_DESC].[LANGUAGE_CODE] = {0} ", "~LANGUAGE_CODE~"));
                    sbWhere.Append(" OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL) ");
                }
                //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                //    //" AND ([REL_COD].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_COD].[LANGUAGE_CODE] IS NULL)");
                //    " AND ([REL_CODE_DESC].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL)");  //pmittal5 Mits 13600

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);

				sbSQL.Append(sbFrom.ToString());

				// Fetch non-deleted records...
				sbWhere.Append(" AND (C.DELETED_FLAG <> -1").Append(" OR C.DELETED_FLAG <> NULL)");
				sbSQL.Append(sbWhere.ToString());
								
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                dictParams.Add("CODE_ID", p_iCodID);
                if(!string.IsNullOrEmpty(m_sLangCode))
                dictParams.Add("LANGUAGE_CODE", m_sLangCode);

                objDs = DbFactory.ExecuteDataSet(m_sConnectString, FormatSQLString(sbSQL.ToString()), dictParams, m_iClientId);
                //objDs = DbFactory.GetDataSet(m_sConnectString, FormatSQLString(sbSQL.ToString()));
                
                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				if (objDs.Tables[0].Rows.Count >0)
					objCod = new Code(objDs.Tables[0].Rows[0],m_sConnectString,m_iClientId);
				else
					throw new RMAppException(Globalization.GetString("UtilityDriver.GetCode.NoCode",m_iClientId));				
				return objCod;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetCodeErr", m_iClientId), p_objEx);
			}
			finally
			{
				sbSQL = null;
				if (objDs!=null)objDs.Dispose();
				objCod=null;
			}			
		}

		//TODO: sumit need to test other dbtypes
		/// This function forms the FROM & WHERE part of the SQL query.
		/// </summary>
		/// <param name="p_sFrom">FROM part of the query</param>
		/// <param name="p_sWhere">WHERE part of the query</param>
		/// <param name="p_sTempJoin">Outer table of join</param>		
		/// <param name="p_sTempJoinAnother">Inner Table of join</param>
		/// <param name="p_sTempJoinAnotherAlias"> Alias of Inner Table of join</param>
		/// <param name="p_sField">Join field of the outer table</param>
		/// <param name="p_sField">Join field of the inner table</param>
		/// <param name="jType>Type of Join</param>
		public void JoinIt(ref string p_sFrom, StringBuilder p_sWhere,string p_sTempJoin,string p_sTempJoinAnother,string p_sTempJoinAnotherAlias,string p_sField1,string p_sField2, JointType jType)
		{				
			try
			{
				if (m_sDBType == Db.eDatabaseType.DBMS_IS_ACCESS)
				{
					p_sFrom = "(" + p_sFrom +  InterpretJoin(jType,m_sDBType) + p_sTempJoinAnother;
					p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnotherAlias + "." + p_sField2 + ")";
				}
				else if ((m_sDBType == Db.eDatabaseType.DBMS_IS_SQLSRVR) )
				{
                    //p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    //if (p_sWhere.ToString() != " WHERE ") 
                    //    p_sWhere.Append (" AND ");
                    //psharma compatibility
                    //p_sWhere.Append( p_sTempJoin + "." + p_sField1 + InterpretJoin(jType,m_sDBType) + p_sTempJoinAnotherAlias + "." + p_sField2);
                    p_sFrom = p_sFrom + InterpretJoin(jType, m_sDBType) + p_sTempJoinAnother;
                    p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnotherAlias + "." + p_sField2 ;
				}
                else if ((m_sDBType == Db.eDatabaseType.DBMS_IS_SYBASE))
				{
					p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
					if (p_sWhere.ToString() != " WHERE ") 
						p_sWhere.Append (" AND ");
					p_sWhere.Append( p_sTempJoin + "." + p_sField1 + InterpretJoin(jType,m_sDBType) + p_sTempJoinAnotherAlias + "." + p_sField2);
				}
				else if (m_sDBType == Db.eDatabaseType.DBMS_IS_INFORMIX)
				{
					p_sFrom = p_sFrom + InterpretJoin(jType,m_sDBType) + p_sTempJoinAnother;
					if (p_sWhere.ToString() != " WHERE ") 
						p_sWhere.Append(" AND ");
					p_sWhere.Append( p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnotherAlias + "." + p_sField2);
				}
				else if (m_sDBType == Db.eDatabaseType.DBMS_IS_ORACLE)
				{
					p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
					if (p_sWhere.ToString() != " WHERE ") 
						p_sWhere.Append( " AND ");
					p_sWhere.Append( p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnotherAlias + "." + p_sField2 + InterpretJoin(jType,m_sDBType));
				}
				else if (m_sDBType == Db.eDatabaseType.DBMS_IS_DB2) 
				{
					p_sFrom = "(" + p_sFrom + InterpretJoin(jType, m_sDBType) + p_sTempJoinAnother;
					p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnotherAlias + "." + p_sField2 + ")";
				}
			}
			catch (Exception p_objException)
			{
				throw new StringException (Globalization.GetString ("SearchResults.JoinIt.JoinQueries",m_iClientId),p_objException);//sonali
			}
		}

		private string InterpretJoin(JointType jType, Db.eDatabaseType dbType)
		{
			switch(dbType)
			{
				case Db.eDatabaseType.DBMS_IS_ACCESS :
					if (jType.Equals(JointType.INNER))
						return " INNER JOIN ";
					if (jType.Equals(JointType.LEFT_OUTER))
						return " LEFT JOIN ";
					else
						return " LEFT JOIN ";
				case Db.eDatabaseType.DBMS_IS_DB2 :
					if (jType.Equals(JointType.INNER))
						return " INNER JOIN ";
					if (jType.Equals(JointType.LEFT_OUTER))
						return " LEFT OUTER JOIN ";
					else
						return " LEFT OUTER JOIN ";
				case Db.eDatabaseType.DBMS_IS_ORACLE :
					if (jType.Equals(JointType.INNER))
						return string.Empty;
					if (jType.Equals(JointType.LEFT_OUTER))
						return "(+)";
					else
						return "(+)";
				case Db.eDatabaseType.DBMS_IS_INFORMIX :
					if (jType.Equals(JointType.INNER))
						return " ,OUTER ";
					if (jType.Equals(JointType.LEFT_OUTER))
						return " ,OUTER ";
					else
						return " ,OUTER ";
				case Db.eDatabaseType.DBMS_IS_SQLSRVR :
                    //if (jType.Equals(JointType.INNER))
                    //    return " = ";
                    //if (jType.Equals(JointType.LEFT_OUTER))
                    //    return " *= ";
                    //else
                    //    return " ,OUTER ";
                    if (jType.Equals(JointType.INNER))
                        return " INNER JOIN ";
                    if (jType.Equals(JointType.LEFT_OUTER))
                        return " LEFT OUTER JOIN ";
                    else
                        return " LEFT OUTER JOIN ";
				case Db.eDatabaseType.DBMS_IS_SYBASE :
					if (jType.Equals(JointType.INNER))
						return " INNER JOIN ";
					if (jType.Equals(JointType.LEFT_OUTER))
						return " LEFT JOIN ";
					else
						return " ,OUTER ";
				default :
					return string.Empty;
			}

		
		}

		/// Name		: GetCodes
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get records from CODES table as CODELIST
		/// </summary>
		/// <param name="p_iTableId">Table Id</param>
		/// <returns>List of Codes</returns>
		private CodeList GetCodes(int p_iTableId)
		{
			if (m_sLangCode.Equals(string.Empty))
			{
				if (!m_DBId.Equals(string.Empty))
					m_sLangCode = GetNLSCode(int.Parse(m_DBId) ).ToString();

			}
            m_sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]; //Aman ML Change 
			DataSet objDs =null;
			CodeList objCodLst = null;			
			StringBuilder sbSQL = null;
			try
			{
				sbSQL = new StringBuilder();
				sbSQL.Append("SELECT [C].[CODE_ID], [C].[SHORT_CODE], ");
				sbSQL.Append(" [F].[IND_STAND_TABLE_ID], [F].[RELATED_TABLE_ID], [F].[LINE_OF_BUS_FLAG], [G].[TABLE_NAME], ");
                
                //pmittal5 Mits 13600 06/15/09 - When a code is saved in RMWorld, its SHORT_CODE doesnt get updated in CODES_TEXT.
                //So SHORT_CODE is taken from CODES table instead of CODES_TEXT so that both RMWorld and RMX are in synch
				//sbSQL.Append(" [A].[CODE_DESC], [A].[LANGUAGE_CODE], [C].[IND_STANDARD_CODE], [IND_STD].[CODE_DESC] [IDESC], ");
                sbSQL.Append(" [A].[CODE_DESC], [A].[LANGUAGE_CODE], [C].[IND_STANDARD_CODE], [IND_STD_DESC].[CODE_DESC] [IDESC], ");
				sbSQL.Append(" [IND_STD].[SHORT_CODE] [ICODE], [C].[LINE_OF_BUS_CODE], [LOB].[CODE_DESC] [LDESC], ");
				sbSQL.Append(" [LOB].[SHORT_CODE] [LCODE], [C].[TRIGGER_DATE_FIELD], [C].[EFF_START_DATE], [C].[EFF_END_DATE], ");
                //sbSQL.Append(" [F].[DELETED_FLAG],[C].[RELATED_CODE_ID], [REL_COD].[CODE_DESC] [RDESC], ");  //Mits 13600
                sbSQL.Append(" [F].[DELETED_FLAG],[C].[RELATED_CODE_ID], [REL_CODE_DESC].[CODE_DESC] [RDESC], ");
				sbSQL.Append(" [REL_COD].[SHORT_CODE] [RCODE], [C].[TABLE_ID], [C].[ORG_GROUP_EID] ");
				//sbSQL.Append(" FROM CODES C ");

				StringBuilder sbWhere = new StringBuilder(" WHERE ");
				StringBuilder sbFrom = new StringBuilder(" FROM ");
				string sFrom  = " [CODES][C]";
				string firstJoinAlias ="[C] ";
				string secJoinAlias = " [A] ";
				string secJoin = "[CODES_TEXT] [A]";
				string firstJoinField =" [CODE_ID] ";
				string secJoinField =" [CODE_ID] ";
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom); 
				
				sFrom="";
				firstJoinAlias =" [C] ";
				secJoin = "[GLOSSARY][F]";
				secJoinAlias ="[F]";
				firstJoinField =" [TABLE_ID] ";
				secJoinField =" [TABLE_ID] ";
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom);
				
				sFrom="";				
				firstJoinAlias =" [C] ";
				secJoin = " [GLOSSARY_TEXT] [G] ";
				secJoinAlias = " [G] ";
				firstJoinField =" [TABLE_ID] ";
				secJoinField =" [TABLE_ID] ";
                if ((m_sDBType == Db.eDatabaseType.DBMS_IS_SQLSRVR))
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" G.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(string.Format(" AND A.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~")); //Aman ML Change
                    }

                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    " G.LANGUAGE_CODE =" + m_sLangCode);

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND G.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(string.Format(" AND A.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~")); //Aman ML Change
                    }
                    
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    " AND G.LANGUAGE_CODE =" + m_sLangCode);

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom);
                //Shruti for 11706
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                if (sbWhere.ToString().Length > 7)
                {
                    sbWhere.Append(string.Format(" AND C.TABLE_ID = {0} ", "~TABLE_ID~"));
                }
                else
                {
                    sbWhere.Append(string.Format(" C.TABLE_ID = {0} ", "~TABLE_ID~"));
                }

                //if (sbWhere.ToString().Length > 7)
                //    sbWhere.Append(" AND C.TABLE_ID =").Append(p_iTableId);
                //else
                //{
                //    sbWhere.Append(" C.TABLE_ID =").Append(p_iTableId);
                //}

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				sFrom="";
				firstJoinAlias =" [C] ";
                //secJoin = " [CODES_TEXT][IND_STD] ";  
                secJoin = " [CODES][IND_STD] ";    //Mits 13600
				secJoinAlias =" [IND_STD] ";
				firstJoinField =" [IND_STANDARD_CODE] ";
				secJoinField =" [CODE_ID] ";

                //pmittal5 Mits 13600 06/15/09 - Modifications done to take short_code of Parent(Related) Code from CODES table
                JoinIt(ref sFrom, sbWhere, firstJoinAlias, secJoin, secJoinAlias, firstJoinField, secJoinField, JointType.LEFT_OUTER);
                sbFrom.Append(sFrom);

                sFrom = "";
                firstJoinAlias = " [IND_STD] ";
                secJoin = " [CODES_TEXT][IND_STD_DESC] ";
                secJoinAlias = " [IND_STD_DESC] ";
                firstJoinField = " [CODE_ID] ";
                secJoinField = " [CODE_ID] ";
                //End - pmittal5

				//-- ABhateja 06.23.2006
				//-- MITS 7132, IS NULL check added for LANGUAGE_CODE.This was a problem with Oracle
				//-- due to which some of the codes were not appearing.
				//				sbWhere.Append( m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND IND_STD.LANGUAGE_CODE =" + m_sLangCode);
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND (IND_STD_DESC.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR IND_STD_DESC.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" AND (IND_STD.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD.LANGUAGE_CODE IS NULL)");
                    //" AND (IND_STD_DESC.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD_DESC.LANGUAGE_CODE IS NULL)");  //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" IND_STD_DESC.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR IND_STD_DESC.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" (IND_STD.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD.LANGUAGE_CODE IS NULL)");
                    //" (IND_STD_DESC.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD_DESC.LANGUAGE_CODE IS NULL)");     //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
				
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);

				sFrom="";
				firstJoinAlias =" [C] ";
				secJoin = " [CODES_TEXT][LOB] ";
				secJoinAlias = " [LOB] ";
				firstJoinField =" [LINE_OF_BUS_CODE] ";
				secJoinField =" [CODE_ID] ";
//				sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND LOB.LANGUAGE_CODE =" + m_sLangCode);
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND (LOB.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR LOB.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //" AND (LOB.LANGUAGE_CODE =" + m_sLangCode + " OR LOB.LANGUAGE_CODE IS NULL)");

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" (LOB.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR LOB.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //" (LOB.LANGUAGE_CODE =" + m_sLangCode + " OR LOB.LANGUAGE_CODE IS NULL)");

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);
				
				sFrom="";
				firstJoinAlias =" [C] ";
                //secJoin = " [CODES_TEXT][REL_COD] ";
                secJoin = " [CODES][REL_COD] ";  //Mits 13600
				secJoinAlias = " [REL_COD] ";
				firstJoinField =" [RELATED_CODE_ID] ";
				secJoinField =" [CODE_ID] ";

                //pmittal5 Mits 13600 06/15/09 - Modifications done to take short_code of Parent(Related) Code from CODES table
                JoinIt(ref sFrom, sbWhere, firstJoinAlias, secJoin, secJoinAlias, firstJoinField, secJoinField, JointType.LEFT_OUTER);
                sbFrom.Append(sFrom);

                sFrom = "";
                firstJoinAlias = " [REL_COD] ";
                secJoin = " [CODES_TEXT][REL_CODE_DESC] ";
                secJoinAlias = " [REL_CODE_DESC] ";
                firstJoinField = " [CODE_ID] ";
                secJoinField = " [CODE_ID] ";
                //End - pmittal5

//				sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND [REL_COD].[LANGUAGE_CODE] =" + m_sLangCode);
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND ([REL_CODE_DESC].[LANGUAGE_CODE] = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" AND ([REL_COD].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_COD].[LANGUAGE_CODE] IS NULL)");
                    //" AND ([REL_CODE_DESC].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL)");  //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" ([REL_CODE_DESC].[LANGUAGE_CODE] = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" ([REL_COD].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_COD].[LANGUAGE_CODE] IS NULL)");
                    //" ([REL_CODE_DESC].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL)");      //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    
                }
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);

				sbSQL.Append(sbFrom.ToString());
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    sbWhere.Append(" AND (C.DELETED_FLAG <> -1").Append(" OR C.DELETED_FLAG <> NULL)");
                }
                else
                {
                    sbWhere.Append(" (C.DELETED_FLAG <> -1").Append(" OR C.DELETED_FLAG <> NULL)");
                }

				sbSQL.Append(sbWhere.ToString());

				//Mjain8 Added 9/18/06 MITS 7729
				sbSQL.Append(" ORDER BY [C].[SHORT_CODE]");
				//Mjain8 MITS 7729 ends

				//sbSQL.Append("		LEFT OUTER JOIN CODES_TEXT IND_STD ON C.IND_STANDARD_CODE = IND_STD.CODE_ID");
				//sbSQL.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
				//	" AND IND_STD.LANGUAGE_CODE =" + m_sLangCode);
				
				//sbSQL.Append("		LEFT OUTER JOIN CODES_TEXT LOB ON C.LINE_OF_BUS_CODE = LOB.CODE_ID");
				//sbSQL.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
				//	" AND LOB.LANGUAGE_CODE =" + m_sLangCode);
				
				//sbSQL.Append("		LEFT OUTER JOIN CODES_TEXT REL_COD ON C.Related_Code_Id = REL_COD.CODE_ID");
				//sbSQL.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
				//	" AND REL_COD.LANGUAGE_CODE =" + m_sLangCode);

				//sbSQL.Append(" ,CODES_TEXT A,GLOSSARY F, GLOSSARY_TEXT G");
				//sbSQL.Append(" WHERE C.CODE_ID=A.CODE_ID ");
				//sbSQL.Append(" AND C.TABLE_ID=F.TABLE_ID ");
				//sbSQL.Append(" AND C.TABLE_ID=G.TABLE_ID ");				
				//sbSQL.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
				//	" AND G.LANGUAGE_CODE =" + m_sLangCode);
				//sbSQL.Append(" AND C.TABLE_ID =").Append(p_iTableId);

                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                if (!string.IsNullOrEmpty(m_sLangCode))
                {
                    dictParams.Add("LANGUAGE_CODE", m_sLangCode);
                }

                dictParams.Add("TABLE_ID", p_iTableId);

                objDs = DbFactory.ExecuteDataSet(m_sConnectString, FormatSQLString(sbSQL.ToString()), dictParams, m_iClientId);
                //objDs = DbFactory.GetDataSet(m_sConnectString, FormatSQLString(sbSQL.ToString()));
                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

				objCodLst = new CodeList();
				//Make a list of Code - List
				foreach(DataRow objRow in objDs.Tables[0].Rows)
				{
					objCodLst.Add(new Code(objRow,m_sConnectString,m_iClientId));
				}				
				return objCodLst;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetCodeErr", m_iClientId), p_objEx);
			}
			finally
			{
				if(objDs!=null)objDs.Dispose();
				objCodLst=null;
				sbSQL = null;
			}			
		}
        /// Name		: GetCodes overload
        /// Author		: Amandeep Kaur
        /// Date Created: 11/30/2012
        /// Info        : Overloaded Function includes tweaks to the existing getcodes function.
        /// <summary>
        /// Fetch codes on the basis of supplied arguments
        /// </summary>
        /// <param name="p_iTableId"></param>
        /// <param name="bCodes"></param>
        /// <param name="p_CurrPageIndex"></param>
        /// <param name="totalCount"></param>
        /// <param name="totalPages"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        private CodeList GetCodes(int p_iCodeID, bool bCodes, int p_CurrPageIndex, ref int totalCount, ref int totalPages, ref int pageSize)
        {
            if (m_sLangCode.Equals(string.Empty))
            {
                if (!m_DBId.Equals(string.Empty))
                    m_sLangCode = GetNLSCode(int.Parse(m_DBId)).ToString();

            }
            DataSet objDs = null;
            CodeList objCodLst = null;
            StringBuilder sbSQL = null;
            try
            {
                sbSQL = new StringBuilder();
                
                sbSQL.Append("SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,LANGUAGE_CODE,");
                sbSQL.Append(" ROW_NUMBER() over(order by CODES.SHORT_CODE) AS rno ");              
                sbSQL.Append(" FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");               
                sbSQL.Append(" AND (CODES.DELETED_FLAG <> -1").Append(" OR CODES.DELETED_FLAG <> NULL)");
                sbSQL.Append(" AND CODES.CODE_ID = ~CODE_ID~");
                sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE <> ~LANGUAGE_CODE~");               

                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                dictParams.Add("CODE_ID", p_iCodeID);
                dictParams.Add("LANGUAGE_CODE",Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0])); //Aman ML Change               

                string sQuery;
                string p_sSQL = sbSQL.ToString();
                StringBuilder sbTemp = new StringBuilder();
                int iStartIndex;
                int iEndIndex;
                bool bInSuccess = false;
                sQuery = "SELECT COUNT(*) ";
                int iTotalNumberOfPages = 0;

                int iIndex = p_sSQL.IndexOf("ORDER BY");
                if (iIndex == -1)
                {
                    sQuery = sQuery + p_sSQL.Substring(p_sSQL.IndexOf(" FROM "));
                }
                else
                {
                    sQuery = sQuery + p_sSQL.Substring(p_sSQL.IndexOf(" FROM "), (iIndex - p_sSQL.IndexOf(" FROM ")));
                }

                int iRecordCount = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectString, FormatSQLString(sQuery.ToString()), dictParams).ToString(), out bInSuccess);
                totalCount = iRecordCount;

                int iRecordsPerPage = GetRecordsPerPage();
                pageSize = iRecordsPerPage;
                if (p_CurrPageIndex > 0)
                {
                    if (iRecordCount > iRecordsPerPage)
                        iTotalNumberOfPages = (int)Math.Ceiling((double)iRecordCount / iRecordsPerPage);
                    else
                        iTotalNumberOfPages = 1;

                    iStartIndex = ((p_CurrPageIndex - 1) * iRecordsPerPage) + 1;
                    iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                    if (iEndIndex > (iRecordCount - 1))
                    {
                        if (p_CurrPageIndex != 1 && (iRecordCount == iStartIndex))
                        {
                            p_CurrPageIndex -= 1;
                            iStartIndex = (p_CurrPageIndex * iRecordsPerPage) + 1;
                            iEndIndex = iStartIndex;
                        }
                        else
                            iEndIndex = iRecordCount;
                    }
                }
                else
                {
                    iStartIndex = 1;
                    iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                }

                totalPages = iTotalNumberOfPages;
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT * FROM (");
                sb.Append(sbSQL);
                sb.Append(") ");                
                Db.DbConnection dbConn = DbFactory.GetDbConnection(m_sConnectString);              
                if (dbConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                    sb.Append(string.Format(" AS mytable WHERE rno BETWEEN {0} AND {1}", "~START_INDEX~", "~END_INDEX~"));
                else
                    sb.Append(string.Format(" WHERE rno BETWEEN {0} AND {1}", "~START_INDEX~", "~END_INDEX~"));

                Dictionary<string, object> dictParameters = new Dictionary<string, object>();
                dictParameters.Add("CODE_ID", p_iCodeID);
                dictParameters.Add("LANGUAGE_CODE", Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0])); //Aman ML Change
                dictParameters.Add("START_INDEX", iStartIndex);
                dictParameters.Add("END_INDEX", iEndIndex);
                objDs = DbFactory.ExecuteDataSet(m_sConnectString, FormatSQLString(sb.ToString()), dictParameters, m_iClientId);
                objCodLst = new CodeList();
                //Make a list of Code - List
                foreach (DataRow objRow in objDs.Tables[0].Rows)
                {
                    objCodLst.Add(new CodeDesc(objRow, m_sConnectString));
                }
                return objCodLst;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.GetCodeErr", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDs != null) objDs.Dispose();
                objCodLst = null;
                sbSQL = null;                
            }
        }
        /// Name		: GetCodes overload
        /// Author		: 
        /// Date Created: 
        /// Info        : Overloaded Function includes tweaks to the existing getcodes function.
        /// <summary>
        /// Fetch codes on the basis of supplied arguments
        /// </summary>
        /// <param name="p_iTableId"></param>
        /// <param name="p_CurrPageIndex"></param>
        /// <param name="totalCount"></param>
        /// <param name="totalPages"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        
		private CodeList GetCodes(int p_iTableId, int p_CurrPageIndex, ref int totalCount, ref int totalPages, ref int pageSize)
		{
			if (m_sLangCode.Equals(string.Empty))
			{
				if (!m_DBId.Equals(string.Empty))
					m_sLangCode = GetNLSCode(int.Parse(m_DBId) ).ToString();

			}
            m_sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]; //Aman ML Change
			DataSet objDs =null;
			CodeList objCodLst = null;			
			StringBuilder sbSQL = null;
			try
			{
				sbSQL = new StringBuilder();

                //aanandpraka2:Start changes for implementing table maintenance custom paging
                sbSQL.Append("SELECT CODE_ID,SHORT_CODE,IND_STAND_TABLE_ID,RELATED_TABLE_ID,LINE_OF_BUS_FLAG,TABLE_NAME,");
                sbSQL.Append("CODE_DESC,LANGUAGE_CODE,IND_STANDARD_CODE,IDESC,ICODE,LINE_OF_BUS_CODE,LDESC,LCODE,");
                sbSQL.Append("TRIGGER_DATE_FIELD,EFF_START_DATE,EFF_END_DATE,DELETED_FLAG,RELATED_CODE_ID,RDESC,RCODE,TABLE_ID,ORG_GROUP_EID,rno ");
                sbSQL.Append(" FROM (");
                
                sbSQL.Append("SELECT [C].[CODE_ID], [C].[SHORT_CODE], ");
				sbSQL.Append(" [F].[IND_STAND_TABLE_ID], [F].[RELATED_TABLE_ID], [F].[LINE_OF_BUS_FLAG], [G].[TABLE_NAME], ");
                
                sbSQL.Append(" [A].[CODE_DESC], [A].[LANGUAGE_CODE], [C].[IND_STANDARD_CODE], [IND_STD_DESC].[CODE_DESC] [IDESC], ");
				sbSQL.Append(" [IND_STD].[SHORT_CODE] [ICODE], [C].[LINE_OF_BUS_CODE], [LOB].[CODE_DESC] [LDESC], ");
				sbSQL.Append(" [LOB].[SHORT_CODE] [LCODE], [C].[TRIGGER_DATE_FIELD], [C].[EFF_START_DATE], [C].[EFF_END_DATE], ");
                sbSQL.Append(" [F].[DELETED_FLAG],[C].[RELATED_CODE_ID], [REL_CODE_DESC].[CODE_DESC] [RDESC], ");
				sbSQL.Append(" [REL_COD].[SHORT_CODE] [RCODE], [C].[TABLE_ID], [C].[ORG_GROUP_EID], ");
				sbSQL.Append(" row_number() over(order by [C].[SHORT_CODE]) AS rno ");
                //aanandpraka2:End changes for table maintenance custom paging

				StringBuilder sbWhere = new StringBuilder(" WHERE ");
				StringBuilder sbFrom = new StringBuilder(" FROM ");
				string sFrom  = " [CODES][C]";
				string firstJoinAlias ="[C] ";
				string secJoinAlias = " [A] ";
				string secJoin = "[CODES_TEXT] [A]";
				string firstJoinField =" [CODE_ID] ";
				string secJoinField =" [CODE_ID] ";
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom); 
				
				sFrom="";
				firstJoinAlias =" [C] ";
				secJoin = "[GLOSSARY][F]";
				secJoinAlias ="[F]";
				firstJoinField =" [TABLE_ID] ";
				secJoinField =" [TABLE_ID] ";
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom);
				
				sFrom="";				
				firstJoinAlias =" [C] ";
				secJoin = " [GLOSSARY_TEXT] [G] ";
				secJoinAlias = " [G] ";
				firstJoinField =" [TABLE_ID] ";
				secJoinField =" [TABLE_ID] ";
                if ((m_sDBType == Db.eDatabaseType.DBMS_IS_SQLSRVR))
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" G.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(string.Format(" AND A.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~")); //Aman ML Change
                    }

                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    " G.LANGUAGE_CODE =" + m_sLangCode);

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND G.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                       // sbWhere.Append(string.Format(" AND A.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));  //Aman ML Change
                    }

                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    " AND G.LANGUAGE_CODE =" + m_sLangCode);
                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                }

				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.INNER);
				sbFrom.Append(sFrom);
                //Shruti for 11706
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                if (sbWhere.ToString().Length > 7)
                {
                    sbWhere.Append(string.Format(" AND C.TABLE_ID = {0} ", "~TABLE_ID~"));
                }
                else
                {
                    sbWhere.Append(string.Format(" C.TABLE_ID = {0} ", "~TABLE_ID~"));
                }

                //if (sbWhere.ToString().Length > 7)
                //    sbWhere.Append(" AND C.TABLE_ID =").Append(p_iTableId);
                //else
                //{
                //    sbWhere.Append(" C.TABLE_ID =").Append(p_iTableId);
                //}

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

				sFrom="";
				firstJoinAlias =" [C] ";
                //secJoin = " [CODES_TEXT][IND_STD] ";  
                secJoin = " [CODES][IND_STD] ";    //Mits 13600
				secJoinAlias =" [IND_STD] ";
				firstJoinField =" [IND_STANDARD_CODE] ";
				secJoinField =" [CODE_ID] ";

                //pmittal5 Mits 13600 06/15/09 - Modifications done to take short_code of Parent(Related) Code from CODES table
                JoinIt(ref sFrom, sbWhere, firstJoinAlias, secJoin, secJoinAlias, firstJoinField, secJoinField, JointType.LEFT_OUTER);
                sbFrom.Append(sFrom);

                sFrom = "";
                firstJoinAlias = " [IND_STD] ";
                secJoin = " [CODES_TEXT][IND_STD_DESC] ";
                secJoinAlias = " [IND_STD_DESC] ";
                firstJoinField = " [CODE_ID] ";
                secJoinField = " [CODE_ID] ";
                //End - pmittal5

				//-- ABhateja 06.23.2006
				//-- MITS 7132, IS NULL check added for LANGUAGE_CODE.This was a problem with Oracle
				//-- due to which some of the codes were not appearing.
				//				sbWhere.Append( m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND IND_STD.LANGUAGE_CODE =" + m_sLangCode);
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND (IND_STD_DESC.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR IND_STD_DESC.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" AND (IND_STD.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD.LANGUAGE_CODE IS NULL)");
                    //" AND (IND_STD_DESC.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD_DESC.LANGUAGE_CODE IS NULL)");  //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" IND_STD_DESC.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR IND_STD_DESC.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" (IND_STD.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD.LANGUAGE_CODE IS NULL)");
                    //" (IND_STD_DESC.LANGUAGE_CODE =" + m_sLangCode + " OR IND_STD_DESC.LANGUAGE_CODE IS NULL)");     //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                }

				
				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);

				sFrom="";
				firstJoinAlias =" [C] ";
				secJoin = " [CODES_TEXT][LOB] ";
				secJoinAlias = " [LOB] ";
				firstJoinField =" [LINE_OF_BUS_CODE] ";
				secJoinField =" [CODE_ID] ";
//				sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND LOB.LANGUAGE_CODE =" + m_sLangCode);
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND (LOB.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR LOB.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //" AND (LOB.LANGUAGE_CODE =" + m_sLangCode + " OR LOB.LANGUAGE_CODE IS NULL)");

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" (LOB.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR LOB.LANGUAGE_CODE IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //" (LOB.LANGUAGE_CODE =" + m_sLangCode + " OR LOB.LANGUAGE_CODE IS NULL)");

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                }

				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);
				
				sFrom="";
				firstJoinAlias =" [C] ";
                //secJoin = " [CODES_TEXT][REL_COD] ";
                secJoin = " [CODES][REL_COD] ";  //Mits 13600
				secJoinAlias = " [REL_COD] ";
				firstJoinField =" [RELATED_CODE_ID] ";
				secJoinField =" [CODE_ID] ";

                //pmittal5 Mits 13600 06/15/09 - Modifications done to take short_code of Parent(Related) Code from CODES table
                JoinIt(ref sFrom, sbWhere, firstJoinAlias, secJoin, secJoinAlias, firstJoinField, secJoinField, JointType.LEFT_OUTER);
                sbFrom.Append(sFrom);

                sFrom = "";
                firstJoinAlias = " [REL_COD] ";
                secJoin = " [CODES_TEXT][REL_CODE_DESC] ";
                secJoinAlias = " [REL_CODE_DESC] ";
                firstJoinField = " [CODE_ID] ";
                secJoinField = " [CODE_ID] ";
                //End - pmittal5

//				sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty : 
//					" AND [REL_COD].[LANGUAGE_CODE] =" + m_sLangCode);
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" AND ([REL_CODE_DESC].[LANGUAGE_CODE] = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" AND ([REL_COD].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_COD].[LANGUAGE_CODE] IS NULL)");
                    //" AND ([REL_CODE_DESC].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL)");  //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                }
                else
                {
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        sbWhere.Append(string.Format(" ([REL_CODE_DESC].[LANGUAGE_CODE] = {0} ", "~LANGUAGE_CODE~"));
                        sbWhere.Append(" OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL) ");
                    }
                    //sbWhere.Append(m_sLangCode.Equals(string.Empty) ? string.Empty :
                    //    //" ([REL_COD].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_COD].[LANGUAGE_CODE] IS NULL)");
                    //" ([REL_CODE_DESC].[LANGUAGE_CODE] =" + m_sLangCode + " OR [REL_CODE_DESC].[LANGUAGE_CODE] IS NULL)");      //Mits 13600

                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                }

				JoinIt(ref sFrom,sbWhere,firstJoinAlias,secJoin,secJoinAlias, firstJoinField,secJoinField, JointType.LEFT_OUTER);
				sbFrom.Append(sFrom);

				sbSQL.Append(sbFrom.ToString());
                //Shruti for 11706
                if (sbWhere.ToString().Length > 7)
                {
                    sbWhere.Append(" AND (C.DELETED_FLAG <> -1").Append(" OR C.DELETED_FLAG <> NULL)");
                }
                else
                {
                    sbWhere.Append(" (C.DELETED_FLAG <> -1").Append(" OR C.DELETED_FLAG <> NULL)");
                }
                //Aman MITS 30835
                if ((m_sDBType == Db.eDatabaseType.DBMS_IS_ORACLE))
                {                    
                    if (m_sLangCode.Equals(string.Empty))
                    {
                        sbWhere.Append(string.Empty);
                    }
                    else
                    {
                        if (sbWhere.ToString().Length > 7)
                        {
                            sbWhere.Append(string.Format(" AND A.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        }
                        else
                        {
                            sbWhere.Append(string.Format(" A.LANGUAGE_CODE = {0} ", "~LANGUAGE_CODE~"));
                        }
                    }                   
                }
                //Aman MITS 30835
				sbSQL.Append(sbWhere.ToString());

                //Mjain8 Added 9/18/06 MITS 7729
				//sbSQL.Append("ORDER BY [C].[SHORT_CODE]");
				//Mjain8 MITS 7729 ends

                string sQuery;
                string p_sSQL = sbSQL.ToString();
                StringBuilder sbTemp = new StringBuilder();
                int iStartIndex;
                int iEndIndex;
                bool bInSuccess = false;
                sQuery = "SELECT COUNT(*) ";
                int iTotalNumberOfPages = 0;

                int iIndex = p_sSQL.IndexOf("ORDER BY");
                if (iIndex == -1)
                {
                    sQuery = sQuery + p_sSQL.Substring(p_sSQL.IndexOf("  FROM  ["));                    
                }
                else
                {
                    sQuery = sQuery + p_sSQL.Substring(p_sSQL.IndexOf(" FROM "), (iIndex - p_sSQL.IndexOf(" FROM ")));                    
                }

                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                dictParams.Add("LANGUAGE_CODE", m_sLangCode);
                dictParams.Add("TABLE_ID", p_iTableId);
                
                int iRecordCount = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectString, FormatSQLString(sQuery.ToString()),dictParams).ToString(), out bInSuccess);
                totalCount = iRecordCount;

                int iRecordsPerPage = GetRecordsPerPage();
                pageSize = iRecordsPerPage;
                if (p_CurrPageIndex > 0)
                {
                    if (iRecordCount > iRecordsPerPage)
                        iTotalNumberOfPages = (int)Math.Ceiling((double)iRecordCount / iRecordsPerPage);
                    else
                        iTotalNumberOfPages = 1;

                    iStartIndex = ((p_CurrPageIndex - 1) * iRecordsPerPage) + 1;
                    iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                    if (iEndIndex > (iRecordCount - 1))
                    {
                        if (p_CurrPageIndex != 1 && (iRecordCount == iStartIndex))
                        {
                            p_CurrPageIndex -= 1;
                            iStartIndex = (p_CurrPageIndex * iRecordsPerPage) + 1;
                            iEndIndex = iStartIndex;
                        }
                        else
                            iEndIndex = iRecordCount;
                    }
                }
                else
                {
                    iStartIndex = 1;
                    iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                }

                totalPages = iTotalNumberOfPages;

                sbSQL.Append(") ");
                Db.DbConnection dbConn = DbFactory.GetDbConnection(m_sConnectString);
                if (dbConn.DatabaseType == Db.eDatabaseType.DBMS_IS_SQLSRVR)
                    sbSQL.Append(string.Format(" as mytable WHERE rno BETWEEN {0} AND {1}", "~START_INDEX~", "~END_INDEX~"));
                else
                    sbSQL.Append(string.Format(" WHERE rno BETWEEN {0} AND {1}","~START_INDEX~","~END_INDEX~"));
                //sbSQL.Append(iStartIndex);
                //sbSQL.Append(" AND ");
                //sbSQL.Append(iEndIndex);

                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                Dictionary<string, object> dictParameters = new Dictionary<string, object>();
                dictParameters.Add("LANGUAGE_CODE", m_sLangCode);
                dictParameters.Add("TABLE_ID", p_iTableId);
                dictParameters.Add("START_INDEX", iStartIndex);
                dictParameters.Add("END_INDEX", iEndIndex);
                objDs = DbFactory.ExecuteDataSet(m_sConnectString, FormatSQLString(sbSQL.ToString()), dictParameters, m_iClientId);
                //objDs = DbFactory.GetDataSet(m_sConnectString, FormatSQLString(sbSQL.ToString()));
                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                objCodLst = new CodeList();
				//Make a list of Code - List
				foreach(DataRow objRow in objDs.Tables[0].Rows)
				{
					objCodLst.Add(new Code(objRow,m_sConnectString,m_iClientId));
				}				
				return objCodLst;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetCodeErr", m_iClientId), p_objEx);
			}
			finally
			{
				if(objDs!=null)objDs.Dispose();
				objCodLst=null;
				sbSQL = null;
			}			
		}
		/// Name		: GetTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets data from the GLOSSARY table and returns GLOSSARY object
		/// </summary>
		/// <param name="p_iTableID">Table Id</param>
		/// <returns>Glossary Object</returns>
		private Glossary GetTable(int p_iTableID)
		{
			DataSet objDs =null;
			Glossary  objGloss = null;			
			try
			{
				//Get Glossary Table Details
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                StringBuilder strSQL = new StringBuilder();
                strSQL.Append("SELECT A.*, B.* FROM GLOSSARY A, ");
                strSQL.Append("GLOSSARY_TEXT B WHERE A.TABLE_ID=B.TABLE_ID ");
                strSQL.Append(string.Format(" AND A.TABLE_ID = {0} ", "~TABLE_ID~"));

                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                dictParams.Add("TABLE_ID", p_iTableID);

                objDs = DbFactory.ExecuteDataSet(m_sConnectString, FormatSQLString(strSQL.ToString()), dictParams, m_iClientId);

                //objDs = DbFactory.GetDataSet(m_sConnectString, "SELECT A.*, B.* FROM GLOSSARY A, " +
                //        " GLOSSARY_TEXT B WHERE A.TABLE_ID=B.TABLE_ID AND A.TABLE_ID=" + p_iTableID);

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				if (objDs.Tables[0].Rows.Count >0)
					objGloss = new Glossary(objDs.Tables[0].Rows[0]);
				else
					throw new RMAppException(Globalization.GetString("UtilityDriver.GetTable.NoTable", m_iClientId));				
				
				return objGloss;
			}
			catch(RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}						
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetTable.DataErr", m_iClientId), p_objEx);
			}
			finally
			{
				if (objDs !=null)objDs.Dispose();
				objGloss=null;
			}	
		}

		/// Name		: GetTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for Getting Table list
		/// </summary>
		/// <returns>Glossary List</returns>
		private GlossaryList GetTables()
		{
			return GetTables(new char() ,"",0); 
		}

		/// Name		: GetTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for getting tables
		/// </summary>
		/// <param name="p_iTableId">Table Id</param>
		/// <returns>Glossary List</returns>
		private GlossaryList GetTables(int p_iTableId)
		{
			return GetTables(new char() ,"",p_iTableId);
		}

		/// Name		: GetTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for Table List
		/// </summary>
		/// <param name="p_cAlpha">Alphabet to search for</param>
		/// <returns>Glossary List</returns>
		private GlossaryList GetTables(char p_cAlpha)
		{
			return GetTables(p_cAlpha,"",0);
		}

		/// Name		: GetTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for Glossary List
		/// </summary>
		/// <param name="p_sTypeCode">Code Type</param>
		/// <returns>Glossary List</returns>
		private GlossaryList GetTables(string p_sTypeCode)
		{
			return GetTables(new char() ,p_sTypeCode,0);
		}

		/// Name		: GetTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for Glossary List
		/// </summary>
		/// <param name="p_cAlpha">Alphabet</param>
		/// <param name="p_sTypeCode">Code Type</param>
		/// <returns></returns>
		private GlossaryList GetTables( char p_cAlpha, string p_sTypeCode)
		{
			return GetTables(p_cAlpha,p_sTypeCode,0);
		}
		
		/// Name		: GetTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets Table list as Glossary object list
		/// </summary>
		/// <param name="p_cAlpha">Alphabet</param>
		/// <param name="p_sTypeCode">Code Type</param>
		/// <param name="p_iTableId">Table Id</param>
		/// <returns></returns>
		private GlossaryList GetTables( char p_cAlpha, string p_sTypeCode, int p_iTableId)
		{
			GlossaryList objGlossLst = null;
			int iTypeCode=0;
			LocalCache objCache=null;
			StringBuilder sSQL=null;
            SysSettings sSetting = new SysSettings(m_sConnectString, m_iClientId); //Ash - cloud
			DataSet objDs=null;
            DbConnection objConn = null;//abansal23 : MITS 12286 on 01/07/2009
            string m_sDBType = "";//abansal23 : MITS 12286
            int bCaseInSens=0;//abansal23 : MITS 12286
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
			try
			{
                //abansal23 : MITS 12286 on 01/07/2009 Start
                objConn = DbFactory.GetDbConnection(m_sConnectString);
                objConn.Open();
                m_sDBType = objConn.DatabaseType.ToString();
                objConn.Close();
                //check for Oracle case sensitiveness
                if (m_sDBType == Constants.DB_ORACLE)
                {
                    bCaseInSens = sSetting.OracleCaseIns;
                }
                //abansal23 : MITS 12286 on 01/07/2009 ends
				objCache = new LocalCache(m_sConnectString,m_iClientId); 
				iTypeCode =  objCache.GetCodeId(p_sTypeCode, "GLOSSARY_TYPES");
				objCache= null;
 
				sSQL = new StringBuilder();

                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

               
				
				//Get multiple Table(Glossary) rows
                // start vsharm65 MITS 22728 add top 500 in query to increase performance
                sSQL.Append("SELECT A.*,B.* FROM GLOSSARY A,GLOSSARY_TEXT B WHERE A.TABLE_ID=B.TABLE_ID");
                // End vsharm65 MITS 22728

                if (p_iTableId != 0)
                {
                    sSQL.Append(string.Format(" AND A.TABLE_ID = {0} ", "~TABLE_ID~"));
                    dictParams.Add("TABLE_ID", p_iTableId);
                }
                //Deb Changes
                if ((int)p_cAlpha != 0)//Parijat: 12244--Now selecting an alphabet for listing of the tables will even return the ones which are in lower case.
                {
                    if (objConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                    {
                        sSQL.Append(string.Format(" AND UPPER(B.TABLE_NAME) LIKE '' + {0} + '%'", "~TABLE_NAME~"));
                    }
                    else
                    {
                        sSQL.Append(string.Format(" AND UPPER(B.TABLE_NAME) LIKE {0} || '%'", "~TABLE_NAME~"));
                    }
                    dictParams.Add("TABLE_NAME", p_cAlpha.ToString().ToUpper());
                }
                //Deb Changes
                if (iTypeCode != 0)
                {
                    sSQL.Append(string.Format(" AND A.GLOSSARY_TYPE_CODE= {0} ", "~GLOSSARY_TYPE_CODE~"));
                    dictParams.Add("GLOSSARY_TYPE_CODE", iTypeCode);
                }
                sSQL.Append(" AND (A.DELETED_FLAG<>1 OR A.DELETED_FLAG IS NULL) ");

                //abansal23 : MITS 12286 on 01/07/2009 Starts
                if (bCaseInSens == -1)
                {
                    //Parijat: 11486,12244-- the table list comes in order now in oracle for both for upper case as well as lower case entries.
                    sSQL.Append(" ORDER BY UPPER(B.TABLE_NAME) ASC");
                }
                else
                {
                    sSQL.Append(" ORDER BY B.TABLE_NAME ASC");
                }

                
                
                

                objDs = DbFactory.ExecuteDataSet(m_sConnectString, sSQL.ToString(), dictParams, m_iClientId);

                ////Get multiple Table(Glossary) rows
                //// start vsharm65 MITS 22728 add top 500 in query to increase performance
                ////Deb 
                ////sSQL.Append("SELECT top 500 A.*,B.* FROM GLOSSARY A,GLOSSARY_TEXT B WHERE A.TABLE_ID=B.TABLE_ID");
                //sSQL.Append("SELECT A.*,B.* FROM GLOSSARY A,GLOSSARY_TEXT B WHERE A.TABLE_ID=B.TABLE_ID");
                ////Deb
                //// End vsharm65 MITS 22728
				
                //if (p_iTableId != 0)
                //    sSQL.Append(" AND A.TABLE_ID = " + p_iTableId);

                //if ((int)p_cAlpha != 0)//Parijat: 12244--Now selecting an alphabet for listing of the tables will even return the ones which are in lower case.
                //    sSQL.Append(" AND UPPER(B.TABLE_NAME) LIKE '" + p_cAlpha.ToString().ToUpper() + "%'");

                //if (iTypeCode != 0)
                //    sSQL.Append(" AND A.GLOSSARY_TYPE_CODE=" + iTypeCode);

                //sSQL.Append(" AND (A.DELETED_FLAG<>1 OR A.DELETED_FLAG IS NULL) ");

                ////abansal23 : MITS 12286 on 01/07/2009 Starts
                //if (bCaseInSens == -1)
                //{
                //    //Parijat: 11486,12244-- the table list comes in order now in oracle for both for upper case as well as lower case entries.
                //    sSQL.Append(" ORDER BY UPPER(B.TABLE_NAME) ASC");
                //}
                //else
                //{
                //    sSQL.Append(" ORDER BY B.TABLE_NAME ASC");
                //}

                ////abansal23 : MITS 12286 on 01/07/2009 Ends
                //objDs = DbFactory.GetDataSet(m_sConnectString, sSQL.ToString());
                //sSQL = null;

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				objGlossLst = new GlossaryList();
				foreach(DataRow objRow in objDs.Tables[0].Rows)
				{
					//Add lossary items to the Glossary List
					objGlossLst.Add(new Glossary(objRow));
				}							
				
				return objGlossLst; 
			}
			catch(DataModelException p_objRmAEx)
			{
				throw p_objRmAEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetTables.DataErr", m_iClientId), p_objEx);  
			}
			finally
			{
                if (objCache != null)
                    objCache.Dispose();
				sSQL=null;
				objGlossLst=null;
				if (objDs!=null)objDs.Dispose();
			}
		}


		/// Name		: GetXMLData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// OverLoaded method for GetXMLData
		/// </summary>
		/// <param name="p_sFormName">Form Name</param>
		/// <param name="p_sXML">Input XML</param>
		/// <param name="p_sTypeCode">Code Type</param>
		/// <returns>String Xml Output</returns>
		public XmlDocument GetXMLData( string p_sFormName, string p_sXML, string p_sTypeCode)
		{
			return GetXMLData(p_sFormName ,0 ,p_sXML,p_sTypeCode ,new char());
		}

		/// Name		: GetXMLData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for GetXMLData
		/// </summary>
		/// <param name="p_sFormName">Form Name</param>
		/// <param name="p_iID">Table Id / Code Id</param>
		/// <param name="p_sXML">Input xml string</param>
		/// <returns>String xml output</returns>
		public XmlDocument GetXMLData( string p_sFormName, int p_iID, string p_sXML)
		{
			return GetXMLData(p_sFormName ,p_iID,p_sXML,"",new char());
		}

		/// Name		: GetXMLData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for GetXMLData
		/// </summary>
		/// <param name="p_sFormName">Form Name</param>
		/// <param name="p_iID">Table Id / Code Id</param>
		/// <param name="p_sXML">Input Xml String</param>
		/// <param name="p_cAlpha">Alphabet Character</param>
		/// <returns>String xml output</returns>
		public XmlDocument GetXMLData( string p_sFormName, int p_iID, string p_sXML,char p_cAlpha)
		{
			return GetXMLData(p_sFormName ,p_iID,p_sXML,"",p_cAlpha);
		}

		/// Name		: GetXMLData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for GetXmlData
		/// </summary>
		/// <param name="p_sFormName">Form Name</param>
		/// <param name="p_iID">Table Id / Code Id</param>
		/// <param name="p_sXML">Input Xml data</param>
		/// <param name="p_sTypeCode">Code Type</param>
		/// <returns>String Xml output</returns>
		public XmlDocument GetXMLData( string p_sFormName, int p_iID, string p_sXML, string p_sTypeCode)
		{
			return GetXMLData(p_sFormName ,p_iID,p_sXML,p_sTypeCode,new char());
		}

		/// Name		: GetXMLData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns output data in the form of xml string
		/// </summary>
		/// <param name="p_sFormName">Form Name</param>
		/// <param name="p_iID">Table id/ code id used either way</param>
		/// <param name="p_sXML">Input xml</param>
		/// <param name="p_sTypeCode">Code Type</param>
		/// <param name="p_cAlpha">Alphabet</param>
		/// <returns>XML String</returns>
		public XmlDocument GetXMLData( string p_sFormName, int p_iID, string p_sXML, string p_sTypeCode, char p_cAlpha)
		{
			XmlDocument objXmlDoc=null;
			int iTypeCode=0;			
			try
			{
				objXmlDoc = new XmlDocument();
				objXmlDoc.LoadXml(p_sXML);
			}
			catch(XmlException p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XmlErr", m_iClientId), p_objEx);
			}

			try
			{
				iTypeCode =  Conversion.ConvertStrToInteger(p_sTypeCode);   

				//Raman - added patch to correctly set TableId in case TransTypeCode =  Line_of_bus..bug fix..no:148

				if((p_sFormName.ToUpper().Trim() == "CODELIST") && (p_sTypeCode=="Line_of_Bus") && p_iID == 0)
				{
					LocalCache objLocalCache = new LocalCache(m_sConnectString,m_iClientId);
					p_iID =  objLocalCache.GetTableId("LINE_OF_BUSINESS");
				}
                m_sTypeCode = p_sTypeCode;
			
				switch (p_sFormName.ToUpper().Trim())
				{
					case "CTNAMES":					
						//return AppendXMLTables(objXmlDoc, GetTables(p_cAlpha, p_sTypeCode),p_sTypeCode).OuterXml;
						return AppendXMLTables(objXmlDoc, GetTables(p_cAlpha, p_sTypeCode),p_sTypeCode);
					case "TOPMENU":
						LocalCache objCach = new LocalCache(m_sConnectString,m_iClientId);
						int iTableId = objCach.GetTableId("GLOSSARY_TYPES");
						objCach = null;
						//return AppendXMLTypeCodes(objXmlDoc,GetCodes(iTableId)).OuterXml;
						return AppendXMLTypeCodes(objXmlDoc,GetCodes(iTableId));
					case "CODELIST":
						//return AppendXMLCodes(objXmlDoc,GetCodes(p_iID),  p_sTypeCode).OuterXml;
						return AppendXMLCodes(objXmlDoc,GetCodes(p_iID),  p_sTypeCode);
					case "FRMADDTABLE":
						//return AppendXMLAddTable(objXmlDoc,GetTables(),iTypeCode).OuterXml; 
						return AppendXMLAddTable(objXmlDoc,GetTables(),iTypeCode); 
					case "FRMEDITTABLE":
						//return AppendXMLEditTable(objXmlDoc,GetTable(p_iID), iTypeCode).OuterXml;
						return AppendXMLEditTable(objXmlDoc,GetTable(p_iID), iTypeCode);
					case "FRMEDITCODE":
						//return AppendXMLEditCode(objXmlDoc,GetCode(p_iID)).OuterXml;  					
						return AppendXMLEditCode(objXmlDoc,GetCode(p_iID));  					
					case "FRMADDCODE":
						//return AppendXMLAddCode(objXmlDoc,GetTable(p_iID)).OuterXml;  
						return AppendXMLAddCode(objXmlDoc,GetTable(p_iID));
						//Aman ML Change
                    case "FRMADDNONBASECODEDESC":
                        return AppendXMLAddNonBaseCode(objXmlDoc, GetCode(this.CodeID));
                    case "FRMEDITNONBASECODEDESC":
                        return AppendXMLEditNonBaseCode(objXmlDoc, GetCode(this.CodeID));
												//Aman ML Change
				}
			}
			catch(Exception p_objEx)
			{
				throw p_objEx;
			}
			finally
			{				
				objXmlDoc=null;
			}
			//return "";
			return null;
		}
        /// Name		: AppendXMLEditNonBaseCode
        /// Author		: Amandeep Kaur
        /// Date Created: 11/27/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Appends XML message for codes
        /// </summary>
        /// <param name="p_objXmlDoc">Input Xml Doc</param>
        /// <param name="p_objCod">Code object</param>
        /// <returns>Code Xml</returns>
        private XmlDocument AppendXMLEditNonBaseCode(XmlDocument p_objXmlDoc, Code p_objCod)
        {
            XmlNodeList objNodLst = null;
            LocalCache objCache = null;
            ArrayList objList = null;
            bool bLanguages = false; //Aman ML Changes
            ArrayList arrlstLanguages = null; //Aman ML Changes
            XmlElement objXmlElem = null;
            string[] arrTemp = new string[2];
            try
            {
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                objNodLst = p_objXmlDoc.GetElementsByTagName("control");
                foreach (XmlElement objElm in objNodLst)
                {
                    switch (objElm.GetAttribute("name").ToUpper().Trim())
                    {
                        //following two have same functionality
                        case "CODE":
                            objElm.SetAttribute("value", p_objCod.ShortCode);
                            objElm.SetAttribute("lock", "true");
                            break;
                        case "CODE_DESC":
                            objElm.SetAttribute("value", objCache.GetCodeDesc((p_objCod.CodeId), this.LanguageCode));
                            objElm.SetAttribute("lock", "false");
                            break;
                        case "LANGUAGE":
                            arrlstLanguages = new ArrayList();
                            bLanguages = this.LoadLanguages(ref arrlstLanguages);
                            if (bLanguages)
                            {
                                for (int iIndex = 0; iIndex < (arrlstLanguages.Count + 1); iIndex++)
                                {
                                    if (iIndex == 0)
                                    {
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.InnerText = "";
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }
                                    else
                                    {
                                        arrTemp = ((string)arrlstLanguages[iIndex - 1]).Split('|');
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.SetAttribute("value", arrTemp[1]);
                                        objXmlElem.InnerText = arrTemp[0];

                                        if (arrTemp[1].Equals(this.LanguageCode))
                                        {
                                            objXmlElem.SetAttribute("selected", "");
                                            objXmlElem.SetAttribute("value", arrTemp[1]);
                                        }
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }

                                }
                            }
                            break;
                        case "LANGUAGECODE":
                            objElm.SetAttribute("value", (this.LanguageCode).ToString());
                            break;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
            }
            finally
            {
                objNodLst = null;
                if (objCache != null)
                    objCache.Dispose();
            }

            return p_objXmlDoc;

        }
        /// Name		: AppendXMLAddNonBaseCode
        /// Author		: Amandeep Kaur
        /// Date Created: 11/27/2012	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Gives Table List
        /// </summary>
        /// <param name="p_objXmlDoc">Xml Input document</param>
        /// <param name="p_objGlossLst">Table List</param>
        /// <param name="p_sTypeCode">Type Code</param>
        /// <returns>Xml representing the Table List</returns>
        private XmlDocument AppendXMLAddNonBaseCode(XmlDocument p_objXmlDoc, Code p_objCod)
        {
            XmlNodeList objNodLst = null;
            LocalCache objCache = null;
            ArrayList objList = null;
            bool bLanguages = false; //Aman ML Changes
            ArrayList arrlstLanguages = null; //Aman ML Changes
            XmlElement objXmlElem = null;
            string[] arrTemp = new string[2];
            try
            {
                objCache = new LocalCache(m_sConnectString,m_iClientId);                              
                objNodLst = p_objXmlDoc.GetElementsByTagName("control");              
                foreach (XmlElement objElm in objNodLst)
                {
                    switch (objElm.GetAttribute("name").ToUpper().Trim())
                    {
                        //following two have same functionality
                        case "CODE":
                            objElm.SetAttribute("value", p_objCod.ShortCode);
                            objElm.SetAttribute("lock", "true");
                            break;
                        case "CODE_DESC":
                            objElm.SetAttribute("value", objCache.GetCodeDesc((p_objCod.CodeId), Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0])));
                            objElm.SetAttribute("lock", "false");
                            break;                       
                        case "LANGUAGE":
                            arrlstLanguages = new ArrayList();
                            bLanguages = this.LoadLanguages(ref arrlstLanguages);
                            if (bLanguages)
                            {
                                for (int iIndex = 0; iIndex < (arrlstLanguages.Count + 1); iIndex++)
                                {
                                    if (iIndex == 0)
                                    {
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.InnerText = "";
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }
                                    else
                                    {
                                        arrTemp = ((string)arrlstLanguages[iIndex - 1]).Split('|');
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.SetAttribute("value", arrTemp[1]);
                                        objXmlElem.InnerText = arrTemp[0];
                                        if (arrTemp[1] == RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0])
                                        {
                                            objXmlElem.SetAttribute("selected", "selected");
                                            objXmlElem.SetAttribute("value", arrTemp[1]);
                                        }
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }

                                }
                            }
                            break;                                              
                    }
                }               
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
            }
            finally
            {
                objNodLst = null;
                if (objCache != null)
                    objCache.Dispose();
            }

            return p_objXmlDoc;
        
        }
        
		/// Name		: AppendXMLTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gives Table List
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input document</param>
		/// <param name="p_objGlossLst">Table List</param>
		/// <param name="p_sTypeCode">Type Code</param>
		/// <returns>Xml representing the Table List</returns>
		private XmlDocument AppendXMLTables( XmlDocument p_objXmlDoc, GlossaryList p_objGlossLst, string p_sTypeCode)
		{
			XmlNodeList objXmlLst = null;
			string sFieldName ="";
			try
			{
				objXmlLst = p_objXmlDoc.GetElementsByTagName("control");
				foreach(XmlElement objXmlElem in objXmlLst)
				{
					sFieldName = objXmlElem.GetAttribute("name");
					if (sFieldName.ToUpper().Trim()=="CTNLIST")
					{					
						SetValue(objXmlElem,p_objGlossLst,""); 
						if (p_sTypeCode=="3" || p_sTypeCode=="10")
						{
							objXmlElem.SetAttribute("isdetail", "yes");  
						}					
					}
					else if(sFieldName.ToUpper().Trim()=="TYPECODE")
					{
						objXmlElem.SetAttribute("value", p_sTypeCode);  
					}			
				}
				return p_objXmlDoc;
			}
			catch(RMAppException p_objRmAEx)
			{
				throw p_objRmAEx;
			}						
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId) , 
					p_objEx);
			}
			finally
			{
				objXmlLst =null;
			}			
		}

		/// Name		: AppendXMLCodes
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Appends a list of codes</summary>
		/// <param name="p_objXmlDoc">Input xml document element</param>
		/// <param name="p_objGlossLst">List of Glossary</param>
		/// <param name="p_sName">Name</param>
		/// <returns>Code list xml</returns>
		private XmlDocument AppendXMLCodes( XmlDocument p_objXmlDoc, CodeList p_objCodLst, string p_sName)
		{
			XmlNodeList objXmlNodLst =null;
			string sFldName="";
			try
			{
				objXmlNodLst =  p_objXmlDoc.GetElementsByTagName("button");
				foreach(XmlElement objXmlElm in objXmlNodLst)
				{
					sFldName = objXmlElm.GetAttribute("name");
					if (sFldName.ToUpper().Trim()=="BTNADD")
					{
						if (Conversion.IsNumeric(p_sName))
							objXmlElm.SetAttribute("param", p_sName);  
					}
				}

				objXmlNodLst =  p_objXmlDoc.GetElementsByTagName("control");
				foreach(XmlElement objXmlElm in objXmlNodLst)
				{
					sFldName = objXmlElm.GetAttribute("name");
					if (sFldName.ToUpper().Trim()=="CODELIST" && p_sName != "" && !Conversion.IsNumeric(p_sName))
							objXmlElm.SetAttribute("ctname", p_sName);

					SetValue (objXmlElm ,p_objCodLst,"");
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch ( Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			finally
			{
				objXmlNodLst=null;				
			}
			return p_objXmlDoc;
		}

		/// Name		: AppendXMLAddTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the xml for table list
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document</param>
		/// <param name="p_objGlosLst">Glossary list</param>
		/// <param name="p_iTypeCode">Type Code</param>
		/// <returns>Xmldocument with Table</returns>
		private XmlDocument AppendXMLAddTable( XmlDocument p_objXmlDoc, GlossaryList p_objGlosLst, int p_iTypeCode)
		{
			XmlNodeList objXmlNodLst =null;
			string sFldName="";		
			
			try
			{
				objXmlNodLst =  p_objXmlDoc.GetElementsByTagName("button");
				foreach(XmlElement objXmlElm in objXmlNodLst)
				{
					sFldName = objXmlElm.GetAttribute("name");
					if (sFldName.ToUpper().Trim()=="BTNADD")
					{
						if (p_iTypeCode ==1)
						{
							objXmlElm.SetAttribute("disable", "1");  
						}
					}
				}

				objXmlNodLst =  p_objXmlDoc.GetElementsByTagName("control");
				switch (p_iTypeCode)
				{
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_USERCODE: //For User Code
						foreach(XmlElement objXmlElm in  objXmlNodLst)
						{						
							switch(objXmlElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
								case "IND_STAND_TABLE":
									SetValue(objXmlElm,p_objGlosLst,"");									
									break;
								case "TREE_DISP":
									objXmlElm.SetAttribute("lock", "true");
									break;
								case "ALLOW_ATTACH":
									objXmlElm.SetAttribute("lock", "true");
									break;
								case "TYPECODE":
									objXmlElm.SetAttribute("value", p_iTypeCode.ToString());
									break;
								case "PTABLE":
									SetValue(objXmlElm ,p_objGlosLst,"");
									break;
							}
						}
						break;
				
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_TABLE:
						foreach(XmlElement objXmlElm in  objXmlNodLst)
						{
							switch(objXmlElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
									//following all cases have the same functionality.
									//Thats why they have been put together
								case"IND_STAND_TABLE":
								case"PTABLE":
								case"PTABLE_REQUIRED":
								case"TREE_DISP":
								case"LINE_OF_BUS":
								case"ACTION":
								case"TABLE":
									objXmlElm.SetAttribute("lock", "true");
									break;
								case"TYPECODE":
									objXmlElm.SetAttribute("value", p_iTypeCode.ToString());
									break;
								case"STABLE":
									break;
								case"UTABLE":
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_ENTITY:
						foreach(XmlElement objXmlElm in  objXmlNodLst)
						{
							switch(objXmlElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
								case "PTABLE":
									SetValue (objXmlElm,p_objGlosLst,""); 
									break;
								case "STABLE":
									break;
								case "UTABLE":
									break;
									//following all cases have the same functionality.
									//Thats why they have been put together
								case "IND_STAND_TABLE":								
								case "PTABLE_REQUIRED":								
								case "TREE_DISP":								
								case "LINE_OF_BUS":								
								case "ACTION":								
								case "TABLE":								
								case "ALLOW_ATTACH":
									objXmlElm.SetAttribute("lock", "true");
									break;
								case "TYPECODE":
									objXmlElm.SetAttribute("value", p_iTypeCode.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_PEOPLE:
						foreach(XmlElement objXmlElm in  objXmlNodLst)
						{
							switch(objXmlElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
								case "PTABLE":
									SetValue (objXmlElm,p_objGlosLst,""); 
									break;
								case "STABLE":
									break;
								case "UTABLE":
									break;
									//following all cases have the same functionality.
									//Thats why they have been put together
								case "IND_STAND_TABLE":
								case "PTABLE_REQUIRED":
								case "TREE_DISP":
								case "LINE_OF_BUS":
								case "ACTION":
								case "TABLE":
								case "ALLOW_ATTACH":
									objXmlElm.SetAttribute("lock", "true");
									break;
								case "TYPECODE":
									objXmlElm.SetAttribute("value", p_iTypeCode.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_ADMINTRACK:
						foreach(XmlElement objXmlElm in  objXmlNodLst)
						{
							switch(objXmlElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
								case "PTABLE":
									SetValue (objXmlElm,p_objGlosLst,""); 
									objXmlElm.SetAttribute("lock", "true");
									break;
								case "STABLE":
									break;
								case "UTABLE":
									break;
									//following all cases do the same thing
								case "IND_STAND_TABLE":
								case "PTABLE_REQUIRED":
								case "TREE_DISP":
								case "LINE_OF_BUS":
								case "ACTION":
								case "TABLE":
								case "ALLOW_ATTACH":
									objXmlElm.SetAttribute("lock", "true");
									break;
								case "TYPECODE":
									objXmlElm.SetAttribute("value", p_iTypeCode.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_IND_STAND:
						foreach(XmlElement objXmlElm in  objXmlNodLst)
						{
							switch(objXmlElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
								case "STABLE":
									break;
								case "UTABLE":
									break;
									//following all cases do the same thing
								case "IND_STAND_TABLE":
								case "PTABLE_REQUIRED":
								case "TREE_DISP":
								case "LINE_OF_BUS":
								case "ACTION":
								case "TABLE":
								case "ALLOW_ATTACH":
								case "PTABLE":
									objXmlElm.SetAttribute("lock", "true");
									break;
								case "TYPECODE":
									objXmlElm.SetAttribute("value", p_iTypeCode.ToString());
									break;
							}
						}
						break;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch ( Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			finally
			{
				objXmlNodLst=null;				
			}
			return p_objXmlDoc;
		}

		/// Name		: AppendXMLTypeCodes
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Type Code Xml
		/// </summary>
		/// <param name="p_objXmlDoc">XML Input Document</param>
		/// <param name="p_objGlossLst">Glossary List</param>
		/// <returns>Xml Document for Type Codes</returns>
		private XmlDocument AppendXMLTypeCodes(XmlDocument p_objXmlDoc, CodeList p_objCodLst)
		{
			XmlNodeList objNodLst=null;

			try
			{
				objNodLst = p_objXmlDoc.GetElementsByTagName("control");
				foreach( XmlElement objElm in objNodLst)
				{
					if ( objElm.GetAttribute("name").ToUpper().Trim() == "GLOSSARY_TYPES")
						SetValue (objElm,p_objCodLst,"");
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			finally
			{
				objNodLst=null;
			}
			return p_objXmlDoc;
		}

		/// Name		: AppendXMLAddCode
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	    
		/// <summary>
		/// Adds Xml Codes
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input Doc</param>
		/// <param name="p_objGlossLst">Glossary List</param>
		/// <returns>Xml Document with codes</returns>
		private XmlDocument AppendXMLAddCode( XmlDocument p_objXmlDoc, Glossary p_objGloss)
		{
			XmlNodeList objNodLst = null;
			LocalCache objCache = null;
            ArrayList objList = null;
            bool bLanguages = false; //Aman ML Changes
            ArrayList arrlstLanguages = null; //Aman ML Changes
            XmlElement objXmlElem = null;
            string[] arrTemp = new string[2];
			try
			{
				objCache= new LocalCache(m_sConnectString,m_iClientId); 
				objNodLst = p_objXmlDoc.GetElementsByTagName("button");
				foreach (XmlElement objElm in objNodLst)
				{
					if (objElm.GetAttribute("name").ToUpper().Trim()=="BTNCANCEL")
						objElm.SetAttribute("param", p_objGloss.TableId.ToString()) ;
				}

				objNodLst = p_objXmlDoc.GetElementsByTagName("control");
                //Added by Amitosh for Payee Phrase List
                objList = new ArrayList();
                objList.Add("PAYEE_PHRASE_LIST");
                objList.Add("HOLD_REASON_CODE");//7810
                //End Amitosh
				foreach(XmlElement objElm in objNodLst)
				{
					switch (objElm.GetAttribute("name").ToUpper().Trim())
					{
							//following two have same functionality
						case "CODE":
						case "CODEDESC":
							objElm.SetAttribute("value", "");
                           	break;
						case "PCODE":
                            //Parijat: MITS 20198
                            if (p_objGloss.ReqRelTable == 1)
                            {
                                objElm.SetAttribute("isrequ", "yes");
                            }//End//Parijat: MITS 20198
							objElm.SetAttribute("value", "");
							objElm.SetAttribute("tableid", p_objGloss.RelatedTableId.ToString());
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", objCache.GetTableName(p_objGloss.RelatedTableId));
							objElm.SetAttribute("id", "");
							if((p_objGloss.RelatedTableId ==0) || objList.Contains(p_objGloss.TableName))//Added by amitosh 
								objElm.SetAttribute("lock", "true");
                            //JIRA 7810 start snehal
                            if (objList.Contains(p_objGloss.TableName) && p_objGloss.TableId == objCache.GetTableId("HOLD_REASON_CODE"))
                            {
                                objElm.SetAttribute("lock", "false");

                            }
                            //jira 7810 ends
							break;
						case "IND_STANDARD":
                            //Changed by Gagan for MITS 20198 : start
                            if (p_objGloss.ReqIndTable == 1)
                            {
                                objElm.SetAttribute("isrequ", "yes");
                            }
                            //Changed by Gagan for MITS 20198 : end
							objElm.SetAttribute("value", "");
							objElm.SetAttribute("tableid", p_objGloss.IndTableId.ToString());
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", objCache.GetTableName(p_objGloss.IndTableId));
							objElm.SetAttribute("id", "");
                            if ((p_objGloss.IndTableId == 0) || objList.Contains(p_objGloss.TableName))//Added by amitosh 
								objElm.SetAttribute("lock", "true");
							break;
						case "LINE_OF_BUS":
                            //Changed by Gagan for MITS 20198 : start
                            if (p_objGloss.LineOfBus == 1)
                            {
                                objElm.SetAttribute("isrequ", "yes");
                            }
                            //Changed by Gagan for MITS 20198 : 
							objElm.SetAttribute("value", "");
							objElm.SetAttribute("id", "");
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", "LINE_OF_BUSINESS");
                            if ((p_objGloss.LineOfBus == 0) || objList.Contains(p_objGloss.TableName))//Added by amitosh 
								objElm.SetAttribute("lock", "true");
							break;
						case "TRIGGER_DATE":
							SetValue(objElm);
                            //Added by Amitosh for Payee Phrase List
                            if (objList.Contains(p_objGloss.TableName))
                                objElm.SetAttribute("lock", "true");
							break;
							//following case have same functionality
						case "E_STARTDATE":
						case "E_ENDDATE":						
						case "ACTION":
							objElm.SetAttribute( "value", "");
                            //Added by Amitosh for Payee Phrase List
                            if (objList.Contains(p_objGloss.TableName))
                                objElm.SetAttribute("lock", "true");
							break;
						case "E_ORG":
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", "ORGH");
							objElm.SetAttribute( "value", "");
                            //Added by Amitosh for Payee Phrase List
                            if (objList.Contains(p_objGloss.TableName))
                                objElm.SetAttribute("lock", "true");
							break;
						case "ID":
						case "TABLEID":
							objElm.SetAttribute( "value", p_objGloss.TableId.ToString());
                            break;
                        //JIRA 7810 start snehal
                        case "TABLENAME":
                            objElm.SetAttribute("value", objCache.GetTableName(p_objGloss.TableId));//7810
                            break;
                        //JIRA 7810 end
                            //Aman ML Changes
                        case "LANGUAGE":
                            arrlstLanguages = new ArrayList();
                            bLanguages = this.LoadLanguages(ref arrlstLanguages);
                            if (bLanguages)
                            {                                
                                for (int iIndex = 0; iIndex < (arrlstLanguages.Count + 1); iIndex++)
                                {
                                    if (iIndex == 0)
                                    {                                        
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.InnerText = "";
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }
                                    else
                                    {
                                        arrTemp = ((string)arrlstLanguages[iIndex - 1]).Split('|');
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.SetAttribute("value", arrTemp[1]);
                                        objXmlElem.InnerText = arrTemp[0];                                       
                                        if (arrTemp[1] == RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0])
                                        {
                                            objXmlElem.SetAttribute("selected", "selected");
                                            objXmlElem.SetAttribute("value", arrTemp[1]);
                                        }
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }

                                }
                            }
                            break;
                        //Aman ML Changes
                        case "REQ_FIELDS": //Parijat: MITS 20198
                            if (p_objGloss.ReqRelTable == 1)
                            {
                                string requiredFields = objElm.GetAttribute("value");
                                if (requiredFields != "")
                                {
                                    objElm.SetAttribute("value", requiredFields + "|" + "pCode");
                                }
                            }
                            //Changed by Gagan for MITS 20198 : start

                            if(p_objGloss.LineOfBus == 1)
                            {
                                string requiredFields = objElm.GetAttribute("value");
                                if (requiredFields != "")
                                {
                                    objElm.SetAttribute("value", requiredFields + "|" + "Line_of_Bus");
                                }
                            }
                            if(p_objGloss.ReqIndTable ==1)
                            {
                                string requiredFields = objElm.GetAttribute("value");
                                if (requiredFields != "")
                                {
                                    objElm.SetAttribute("value", requiredFields + "|" + "Ind_Standard");
                                }
                            }
                            //Changed by Gagan for MITS 20198 : end
                            break; //Parijat: MITS 20198
					}
				}
                //Deb Multi Currency
                if (p_objGloss.TableName == "CURRENCY_TYPE")
                {
                    XmlElement xmlElement = p_objXmlDoc.CreateElement("VisibleCurrencyLink");
                    xmlElement.InnerText = Boolean.TrueString;
                    p_objXmlDoc.DocumentElement.AppendChild(xmlElement);
                }
                //Deb Multi Currency
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			finally
			{
				objNodLst=null;
                if (objCache != null)
                    objCache.Dispose();
			}

			return p_objXmlDoc;
		}
               

        /// Name		: LoadLanguages
        /// Author		: Amandeep Kaur
        /// Date Created: 10/23/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************	    
        /// <summary>
        /// Adds Xml Codes
        /// </summary>        
        public bool LoadLanguages(ref ArrayList p_arrlstLanguages)
        {
            bool bReturn = false;
            string sSQL = "";
            DbConnection objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            DbReader objReader = null;

            try
            {
                objConn.Open();              
                sSQL = "SELECT * FROM SYS_LANGUAGES WHERE SUPPORTED_FLAG = -1 ORDER BY LANGUAGE_NAME";                
                objReader = objConn.ExecuteReader(sSQL);

                while (objReader.Read())
                {
                    string sLanguageName = objReader.GetString("LANGUAGE_NAME");
                    int iLanguageID = objReader.GetInt32("LANG_ID");
                    p_arrlstLanguages.Add(sLanguageName + "|" + iLanguageID);
                }
                bReturn = true;
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadLanguages.LoadLanguagesError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {                   
                    objConn.Dispose();                 
                }
            }
            return bReturn;
        }
      
		/// Name		: AppendXMLEditCode
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Appends XML message for codes
		/// </summary>
		/// <param name="p_objXmlDoc">Input Xml Doc</param>
		/// <param name="p_objCod">Code object</param>
		/// <returns>Code Xml</returns>
		private XmlDocument AppendXMLEditCode(XmlDocument p_objXmlDoc, Code p_objCod)
		{
			XmlNodeList objNodLst = null;
			LocalCache objCache = null;
            Glossary p_objGloss = null;
            ArrayList objList = null; //This arraylist will contain all the code tables for whom code detail aspx need to be readonly
            bool bLanguages = false; //Aman ML Changes
            ArrayList arrlstLanguages = null; //Aman ML Changes
            XmlElement objXmlElem = null;
            string[] arrTemp = new string[2];
			try
			{
                //Added by Amitosh for Payee Phrase List
                objList = new ArrayList();
                objList.Add("PAYEE_PHRASE_LIST");
                objList.Add("HOLD_REASON_CODE");//7810
                objNodLst = p_objXmlDoc.GetElementsByTagName("RemoveDelButton");
                foreach (XmlElement objElm in objNodLst)
                {
                    p_objGloss = GetTable(p_objCod.TableId);

                    if (objList.Contains(p_objGloss.TableName) && (string.Equals(p_objCod.ShortCode, "and") || !string.Equals(p_objCod.RelShortCode.ToLower(), "others")))
                        objElm.InnerText = "true";   
                }

                //end amitosh
				objCache= new LocalCache(m_sConnectString,m_iClientId); 

				objNodLst = p_objXmlDoc.GetElementsByTagName("button");
				foreach(XmlElement objElm in objNodLst)
				{
					if (objElm.GetAttribute("name").ToUpper().Trim()=="BTNCANCEL")
						objElm.SetAttribute("param",p_objCod.TableId.ToString());   
				}

				objNodLst = p_objXmlDoc.GetElementsByTagName("control");
				foreach(XmlElement objElm in objNodLst)
				{
					switch (objElm.GetAttribute("name").ToUpper().Trim())
					{
						case "CODE":
							objElm.SetAttribute("value",p_objCod.ShortCode);
                            p_objGloss = GetTable(p_objCod.TableId);
                          //Added by Amitosh for Payee Phrase List
                            if(objList.Contains(p_objGloss.TableName) && string.Equals(p_objCod.ShortCode,"and"))
                                objElm.SetAttribute("lock","true");
							break;
						case "CODE_DESC":
							objElm.SetAttribute("value",p_objCod.CodeDesc);
							break;
						case "PCODE":
                            //Changed by Gagan for MITS 20198 : start
                            p_objGloss = GetTable(p_objCod.TableId);
                            if (p_objGloss.ReqRelTable == 1)
                            {
                                objElm.SetAttribute("isrequ", "yes");
                            }
                            //Changed by Gagan for MITS 20198 : end
							objElm.SetAttribute("value",p_objCod.RelShortCode + " " + p_objCod.RelCodeDesc);
							objElm.SetAttribute("tableid",p_objCod.ParentTable.ToString());
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", objCache.GetTableName(p_objCod.ParentTable));
							objElm.SetAttribute("id",p_objCod.RelatedCodeId.ToString());
							if ((p_objCod.ParentTable == 0) ||objList.Contains(p_objGloss.TableName))  //Added by Amitosh for Payee Phrase List
								objElm.SetAttribute("lock","true");
                            //JIRA 7810 start snehal
                            if (objList.Contains(p_objGloss.TableName) && string.Equals(p_objCod.ShortCode.ToLower(), "others"))
                            {
                                objElm.SetAttribute("lock", "false");

                            }
                            //jira 7810 ends
                      
							break;
						case "IND_STANDARD":
                            //Changed by Gagan for MITS 20198 : start
                            p_objGloss = GetTable(p_objCod.TableId);
                            if (p_objGloss.ReqIndTable == 1)
                            {
                                objElm.SetAttribute("isrequ", "yes");
                            }
                            //Changed by Gagan for MITS 20198 : end
							objElm.SetAttribute("value",p_objCod.IndShortCode + " " + p_objCod.IndCodeDesc);
							objElm.SetAttribute("tableid",p_objCod.IndStandTableId.ToString());
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", objCache.GetTableName(p_objCod.IndStandTableId));
							objElm.SetAttribute("id",p_objCod.IndStandardCode.ToString());
							if((p_objCod.IndStandTableId ==0) ||objList.Contains(p_objGloss.TableName))  //Added by Amitosh for Payee Phrase List
								objElm.SetAttribute("lock","true");
                            
                            
							break;
						case "LINE_OF_BUS":
                            //Changed by Gagan for MITS 20198 : start
                            p_objGloss = GetTable(p_objCod.TableId);
                            if (p_objGloss.LineOfBus == 1)
                            {
                                objElm.SetAttribute("isrequ", "yes");
                            }
                            //Changed by Gagan for MITS 20198 : end
							objElm.SetAttribute("value",p_objCod.LobShortCode  + " " + p_objCod.LobCodeDesc);
							objElm.SetAttribute("id",p_objCod.LineOfBusCode.ToString());
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", "LINE_OF_BUSINESS");
							if ((p_objCod.LineBusFlag==0) || objList.Contains(p_objGloss.TableName))  //Added by Amitosh for Payee Phrase List
								objElm.SetAttribute("lock","true");  
                           
                           
							break;
						case "E_STARTDATE":
							if(p_objCod.StartDate!="")
								objElm.SetAttribute("value",Conversion.GetUIDate(p_objCod.StartDate,m_LangCode.ToString(),m_iClientId) );
                             //Added by Amitosh for Payee Phrase List
                            if(objList.Contains(p_objGloss.TableName))
                                objElm.SetAttribute("lock","true"); 
							break;
						case "E_ENDDATE":
							if(p_objCod.EndDate!="")
                                objElm.SetAttribute("value", Conversion.GetUIDate(p_objCod.EndDate, m_LangCode.ToString(), m_iClientId));
                             //Added by Amitosh for Payee Phrase List
                            if(objList.Contains(p_objGloss.TableName))
                                objElm.SetAttribute("lock","true"); 
							break;
						case "E_ORG":
							if (p_objCod.OrgAbbreviation =="")
								objElm.SetAttribute("value",p_objCod.Organisation);
							else
								objElm.SetAttribute("value",p_objCod.OrgAbbreviation  + "-" + p_objCod.Organisation);
							objElm.SetAttribute("id",p_objCod.OrgGroupEid.ToString());
							// Mihika 24-Feb-2006: Added 'codetable' attribute to implement quick lookup
							objElm.SetAttribute("codetable", "ORGH");
                             //Added by Amitosh for Payee Phrase List
                            if(objList.Contains(p_objGloss.TableName))
                                objElm.SetAttribute("lock","true"); 
							break;
						case "ACTION":
							objElm.SetAttribute("value","edit");  
							break;
						case "ID":
							objElm.SetAttribute("value",p_objCod.CodeId.ToString());    
							break;
						case "TABLEID":
							objElm.SetAttribute("value",p_objCod.TableId.ToString());    
							break;
                        //JIRA 7810 start snehal
                        case "TABLENAME":
                            objElm.SetAttribute("value",objCache.GetTableName( p_objCod.TableId));//7810
                            break;
                        //JIRA 7810 end
						case "TRIGGER_DATE":
							SetValue(objElm,p_objCod.TriggerDateField);
                             //Added by Amitosh for Payee Phrase List
                            if(objList.Contains(p_objGloss.TableName))
                                objElm.SetAttribute("lock", "true"); 
							break;
                        case "REQ_FIELDS": //Parijat: MITS 20198
                            p_objGloss = GetTable(p_objCod.TableId);
                            if (p_objGloss.ReqRelTable == 1)
                            {
                                string requiredFields = objElm.GetAttribute("value");
                                if (requiredFields != "")
                                {
                                    objElm.SetAttribute("value", requiredFields + "|" + "pCode_cid");//ijha:changed pCode to pCode_cid for MITS 24382
                                }
                            }
                            //Changed by Gagan for MITS 20198 : start
                            if(p_objGloss.LineOfBus == 1)
                            {
                                string requiredFields = objElm.GetAttribute("value");
                                if (requiredFields != "")
                                {
                                    objElm.SetAttribute("value", requiredFields + "|" + "Line_of_Bus_cid");//ijha:changed Line_of_Bus to Line_of_Bus_cid for MITS 24382");
                                }
                            }
                            if(p_objGloss.ReqIndTable ==1)
                            {
                                string requiredFields = objElm.GetAttribute("value");
                                if (requiredFields != "")
                                {
                                    objElm.SetAttribute("value", requiredFields + "|" + "Ind_Standard_cid");//ijha:changed Ind_Standard to Ind_Standard_cid for MITS 24382
                                }
                            }
                            //Changed by Gagan for MITS 20198 : end
                            break; //Parijat: MITS 20198 
                        //Aman ML Changes
                        case "LANGUAGE":
                            arrlstLanguages = new ArrayList();
                            bLanguages = this.LoadLanguages(ref arrlstLanguages);
                            if (bLanguages)
                            {
                                for (int iIndex = 0; iIndex < (arrlstLanguages.Count + 1); iIndex++)
                                {
                                    if (iIndex == 0)
                                    {
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.InnerText = "";
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }
                                    else
                                    {
                                        arrTemp = ((string)arrlstLanguages[iIndex - 1]).Split('|');
                                        objXmlElem = p_objXmlDoc.CreateElement("option");
                                        objXmlElem.SetAttribute("value", arrTemp[1]);
                                        objXmlElem.InnerText = arrTemp[0];

                                        if (arrTemp[1].Equals(p_objCod.LangCode))
                                        {
                                            objXmlElem.SetAttribute("selected", "");
                                            objXmlElem.SetAttribute("value", arrTemp[1]);
                                        }
                                        objElm.AppendChild(objXmlElem);
                                        objXmlElem = null;
                                    }

                                }
                            }
                            break;
                        case "LANGUAGECODE":
                            objElm.SetAttribute("value", p_objCod.LangCode.ToString());
                            break;
                        //Aman ML Changes
					}
				}
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			finally
			{
				objNodLst=null;
                if(objCache != null)
                    objCache.Dispose();
			}
			return p_objXmlDoc;
		}

		/// Name		: AppendXMLEditTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets Xml element for Table
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Element Input doc</param>
		/// <param name="p_objGlos">Glossary List</param>
		/// <param name="p_iTypCod">Code Type</param>
		/// <returns>Returns Appended table XML</returns>
		private XmlDocument AppendXMLEditTable(XmlDocument p_objXmlDoc, Glossary p_objGlos , int p_iTypCod)
		{
			XmlNodeList objNodLst = null;
			GlossaryList objGlossLst = null;
            ArrayList objList = null;
			try
			{
                //Added by Amitosh for Payee Phrase List
                objList = new ArrayList();
                objList.Add("PAYEE_PHRASE_LIST");
                objList.Add("POLICY_CLAIM_LOB");

				objGlossLst = GetTables();
				objNodLst =  p_objXmlDoc.GetElementsByTagName("control");

				switch (p_iTypCod)
				{
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_USERCODE:
						foreach(XmlElement objElm in objNodLst)
						{
							switch(objElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
								case "IND_STAND_TABLE":
									SetValue(objElm , objGlossLst ,p_objGlos.IndTableId.ToString());
									break;
								case "PTABLE":
									SetValue(objElm ,objGlossLst, p_objGlos.RelatedTableId.ToString());
                                    if (objList.Contains(p_objGlos.TableName))
                                        objElm.SetAttribute("lock", "true");
                                    //nsachdeva2 MITS:25683 09/22/2011
                                    //if (p_objGlos.RelatedTableId!=0)
                                    //    objElm.SetAttribute ("lock","true"); 
                                    //End MITS:25683
									break;
								case "STABLE":
									objElm.SetAttribute ("value",p_objGlos.TableName);
									objElm.SetAttribute ("lock","true");
									break;
								case "UTABLE":
									objElm.SetAttribute ("value",p_objGlos.UserTableName);
									break;
								case "PTABLE_REQUIRED":
									objElm.SetAttribute ("value",p_objGlos.ReqRelTable.ToString());
                                    if (p_objGlos.RelatedTableId != 0)
                                        objElm.SetAttribute("lock", "true");
                                    //nsachdeva2 MITS:25683 09/22/2011
									//objElm.SetAttribute ("lock","true");
                                    //End MITS:25683
									break;
								case "TREE_DISP":
									objElm.SetAttribute ("value",p_objGlos.IsTree.ToString());
                                    //pmittal5  MITS:12685  07/24/08
                                    if(p_objGlos.RelatedTableId == 0)
                                        objElm.SetAttribute ("lock","true");
									break;
								case "ALLOW_ATTACH":
									objElm.SetAttribute ("value",p_objGlos.IsAttachment.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "LINE_OF_BUS":
									objElm.SetAttribute ("value",p_objGlos.LineOfBus.ToString());
                                    //Added by amitosh for payee phrase
                                    if(objList.Contains(p_objGlos.TableName))
                                    objElm.SetAttribute("lock", "true");
									break;
								case "IND_TABLES_REQUIRED":
									objElm.SetAttribute ("value",p_objGlos.ReqIndTable.ToString());
                                    //Added by amitosh for payee phrase
                                    if (objList.Contains(p_objGlos.TableName))
                                        objElm.SetAttribute("lock", "true");
									break;
								case "ACTION":
									objElm.SetAttribute ("value","edit");
									break;
								case "TABLE":
									objElm.SetAttribute ("value",p_objGlos.TableId.ToString());
									break;
								case "TYPECODE":
									objElm.SetAttribute ("value",p_iTypCod.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_TABLE:
						foreach(XmlElement objElm in objNodLst)
						{
							switch(objElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
									//following cases have same functionality
								case "IND_STAND_TABLE":
								case "PTABLE":
								case "PTABLE_REQUIRED":
								case "TREE_DISP":
								case "LINE_OF_BUS":
									objElm.SetAttribute ("lock","true");
									break;
								case "STABLE":
									objElm.SetAttribute ("value",p_objGlos.TableName);
									objElm.SetAttribute ("lock","true");
									break;
								case "UTABLE":
									objElm.SetAttribute ("value",p_objGlos.UserTableName);
									break;
								case "ALLOW_ATTACH":
									objElm.SetAttribute ("value",p_objGlos.IsAttachment.ToString());
									break;
								case "ACTION":
									objElm.SetAttribute ("value","edit");
									break;
								case "TABLE":
									objElm.SetAttribute ("value",p_objGlos.TableId.ToString());
									break;
								case "TYPECODE":
									objElm.SetAttribute ("value",p_iTypCod.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_ENTITY:
						foreach(XmlElement objElm in objNodLst)
						{
							switch(objElm.GetAttribute("name").ToString().ToUpper().Trim())
							{					
								case "IND_STAND_TABLE":
									objElm.SetAttribute ("lock","true");
									break;
								case "PTABLE":
									SetValue(objElm,objGlossLst, p_objGlos.RelatedTableId.ToString()); 
									objElm.SetAttribute ("lock","true");
									break;
								case "PTABLE_REQUIRED":
									objElm.SetAttribute ("value",p_objGlos.ReqRelTable.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "TREE_DISP":
									objElm.SetAttribute ("value",p_objGlos.IsTree.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "LINE_OF_BUS":
									objElm.SetAttribute ("lock","true");
									break;
								case "STABLE":
									objElm.SetAttribute ("value",p_objGlos.TableName);
									objElm.SetAttribute ("lock","true");
									break;
								case "UTABLE":
									objElm.SetAttribute ("value",p_objGlos.UserTableName);
									break;
								case "ALLOW_ATTACH":
									objElm.SetAttribute ("value",p_objGlos.IsAttachment.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "ACTION":
									objElm.SetAttribute ("value","edit");
									break;
								case "TABLE":
									objElm.SetAttribute ("value",p_objGlos.TableId.ToString());
									break;
								case "TYPECODE":
									objElm.SetAttribute ("value",p_iTypCod.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_PEOPLE:
						foreach(XmlElement objElm in objNodLst)
						{
							switch(objElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
									//following cases have same functionality
								case "IND_STAND_TABLE":
									objElm.SetAttribute ("lock","true");
									break;
								case "PTABLE":
									SetValue(objElm,objGlossLst , p_objGlos.RelatedTableId.ToString()); 
									objElm.SetAttribute ("lock","true");
									break;
								case "PTABLE_REQUIRED":
									objElm.SetAttribute ("value",p_objGlos.ReqRelTable.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "TREE_DISP":
									objElm.SetAttribute ("value",p_objGlos.IsTree.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "LINE_OF_BUS":
									objElm.SetAttribute ("lock","true");
									break;
								case "STABLE":
									objElm.SetAttribute ("value",p_objGlos.TableName);
									objElm.SetAttribute ("lock","true");
									break;
								case "UTABLE":
									objElm.SetAttribute ("value",p_objGlos.UserTableName);
									break;
								case "ALLOW_ATTACH":
									objElm.SetAttribute ("lock","true");
									break;
								case "ACTION":
									objElm.SetAttribute ("value","edit");
									break;
								case "TABLE":
									objElm.SetAttribute ("value",p_objGlos.TableId.ToString());
									break;
								case "TYPECODE":
									objElm.SetAttribute ("value",p_iTypCod.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_ADMINTRACK:
						foreach(XmlElement objElm in objNodLst)
						{
							switch(objElm.GetAttribute("name").ToString().ToUpper().Trim())
							{
									//following cases have same functionality
								case "IND_STAND_TABLE":
								case "PTABLE":
								case "TREE_DISP":
								case "LINE_OF_BUS":
								case "ACTION":
									objElm.SetAttribute ("value","edit");
									break;
								case "PTABLE_REQUIRED":
									objElm.SetAttribute ("value",p_objGlos.ReqRelTable.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "STABLE":
									objElm.SetAttribute ("value",p_objGlos.TableName);
									objElm.SetAttribute ("lock","true");
									break;
								case "UTABLE":
									objElm.SetAttribute ("value",p_objGlos.UserTableName);
									break;
								case "ALLOW_ATTACH":
									objElm.SetAttribute ("value",p_objGlos.IsAttachment.ToString());
									objElm.SetAttribute ("lock","true");
									break;
								case "TABLE":
									objElm.SetAttribute ("value",p_objGlos.TableId.ToString());
									break;
								case "TYPECODE":
									objElm.SetAttribute ("value",p_iTypCod.ToString());
									break;
							}
						}
						break;
					case (int)GLOSSARYTYPE.GLOSSARY_TYPE_IND_STAND:
						foreach(XmlElement objElm in objNodLst)
						{
							switch(objElm.GetAttribute("name").ToString().ToUpper().Trim())
							{					
								case "IND_STAND_TABLE":
								case "PTABLE":
								case "PTABLE_REQUIRED":
								case "TREE_DISP":
								case "LINE_OF_BUS":
								case "ALLOW_ATTACH":
									objElm.SetAttribute ("lock","true");
									break;
								case "STABLE":
									objElm.SetAttribute ("value",p_objGlos.TableName);
									objElm.SetAttribute ("lock","true");
									break;
								case "UTABLE":
									objElm.SetAttribute ("value",p_objGlos.UserTableName);
									break;
								case "ACTION":
									objElm.SetAttribute ("value","edit");
									break;
								case "TABLE":
									objElm.SetAttribute ("value",p_objGlos.TableId.ToString());
									break;
								case "TYPECODE":
									objElm.SetAttribute ("value",p_iTypCod.ToString());
									break;
							}
						}
						break; 
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);  
			}
			finally
			{
				objNodLst = null;
				objGlossLst = null;
			}
			return p_objXmlDoc;
		}

		/// Name		: AppendXMLSaveTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves Table
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document</param>
		/// <returns>Xml Document with Saved Table</returns>
		private XmlDocument AppendXMLSaveTable(XmlDocument p_objXmlDoc)
		{
			XmlNodeList objNodLst = null;

			objNodLst = p_objXmlDoc.GetElementsByTagName("body1");
			foreach(XmlElement objElm in objNodLst)
			{
				//Set attribute to saved
				if(objElm.GetAttribute("req_func")=="yes")
					objElm.SetAttribute("func","refreshme()");  
			}
			objNodLst=null;
			return p_objXmlDoc;
		}

		/// Name		: SetValue
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for SetValue
		/// </summary>
		/// <param name="p_objXmlElem">XmlElement Input</param>
		private void SetValue(XmlElement p_objXmlElem)
		{
			SetValue(p_objXmlElem,new CodeList(),""); 
		}

		/// Name		: SetValue
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for SetValue
		/// </summary>
		/// <param name="p_objXmlElem">XmlElement Input</param>
		/// <param name="p_sxValue">Value to be set</param>
		private void SetValue(XmlElement p_objXmlElem, string p_sxValue)
		{
			SetValue(p_objXmlElem,new CodeList(),p_sxValue); 
		}

		/// Name		: SetValue
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Does the XML manipulation and sets various values
		/// </summary>
		/// <param name="p_objXmlElem">XmlElement to be manipulated</param>
		/// <param name="p_objGlossLst">Code List Object</param>
		/// <param name="p_sxValue">Value to be set</param>
		private void SetValue(XmlElement p_objXmlElem, GlossaryList p_objGlossLst, string p_sxValue)
		{
			int iCodeId=0;
			string sType = "";
			LocalCache objCache = null;
			XmlElement objElm = null;
			try
			{
				objCache= new LocalCache(m_sConnectString,m_iClientId); 
				sType = p_objXmlElem.GetAttribute("type").ToUpper().Trim() ;
				if (sType=="TREEVIEW")
				{
					if(p_objXmlElem.GetAttribute("name").ToUpper().Trim()=="CTNLIST")
					{ 
						foreach(Glossary objGlos in p_objGlossLst)
						{
							objElm = p_objXmlElem.OwnerDocument.CreateElement("option");
							objElm.SetAttribute("tableid",objGlos.TableId.ToString());
							objElm.SetAttribute("value",objGlos.UserTableName );
							p_objXmlElem.AppendChild(objElm);
							objElm = null;
						}
					}
				}
				else if (sType=="COMBOBOX")
				{
					if(p_objXmlElem.GetAttribute("name").ToUpper().Trim()=="IND_STAND_TABLE")
					{
						iCodeId =  objCache.GetCodeId("10","GLOSSARY_TYPES");
						foreach(Glossary objGloss in p_objGlossLst)
						{
							if (objGloss.TypeCode == iCodeId)
							{
								objElm = p_objXmlElem.OwnerDocument.CreateElement(
									p_objXmlElem.GetAttribute("optionname"));
								if(p_sxValue!="")
								{
									if ( p_sxValue == objGloss.TableId.ToString() )
										objElm.SetAttribute("selected", "1");
									else
										objElm.SetAttribute("selected", "0");
								}
								objElm.SetAttribute("tableid", objGloss.TableId.ToString());
								objElm.SetAttribute("name", objGloss.TableName); 
								p_objXmlElem.AppendChild(objElm);
								objElm = null;
							}
						} 
					}
					else if (p_objXmlElem.GetAttribute("name").ToUpper().Trim()=="TRIGGER_DATE")
					{
						//Current System Date Details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Current System Date");
						objElm.SetAttribute("tvalue", "SYSTEM_DATE");
						if(p_sxValue.ToUpper().Trim()=="SYSTEM_DATE")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null; 

						//Event Date Details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Event Date");
						objElm.SetAttribute("tvalue", "EVENT.DATE_OF_EVENT");
						if(p_sxValue.ToUpper().Trim()=="EVENT.DATE_OF_EVENT")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;

						//Claim Date Details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Claim Date");
						objElm.SetAttribute("tvalue", "CLAIM.DATE_OF_CLAIM");
						if(p_sxValue.ToUpper().Trim()=="CLAIM.DATE_OF_CLAIM")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;
  
						//Payment transaction Date Details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Payment Transaction Date");
						objElm.SetAttribute("tvalue", "FUNDS.TRANS_DATE");
						if(p_sxValue.ToUpper().Trim()=="FUNDS.TRANS_DATE")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;

						//Policy Effective Date Details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Policy Effective Date");
						objElm.SetAttribute("tvalue", "POLICY.EFFECTIVE_DATE");
						if(p_sxValue.ToUpper().Trim()=="POLICY.EFFECTIVE_DATE")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;
					}
					else if (p_objXmlElem.GetAttribute("name").ToUpper().Trim()=="PTABLE")
					{
						foreach(Glossary objGloss in p_objGlossLst)
						{
							if (objGloss.TypeCode == objCache.GetCodeId("2","GLOSSARY_TYPES") || 
								objGloss.TypeCode == objCache.GetCodeId("3","GLOSSARY_TYPES") || 
								objGloss.TypeCode == objCache.GetCodeId("10","GLOSSARY_TYPES") )
							{
								//For glossary type codes
								objElm = p_objXmlElem.OwnerDocument.CreateElement(
									p_objXmlElem.GetAttribute("optionname"));
								if (p_sxValue!="")
								{
									if (p_sxValue==objGloss.TableId.ToString())
										objElm.SetAttribute ("selected", "1");
									else
										objElm.SetAttribute("selected", "0");
								}

								objElm.SetAttribute("tableid",objGloss.TableId.ToString());
								objElm.SetAttribute("name",objGloss.TableName);
								p_objXmlElem.AppendChild(objElm);
								objElm = null;
							}
						}
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			finally
			{
                if (objCache != null)
                    objCache.Dispose();
				objElm = null;
			}
		}	

		/// Name		: SetValue
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method for SetValue for Code List
		/// </summary>
		/// <param name="p_objXmlElem">Input Xml Element</param>
		/// <param name="p_objGlossLst">Glossary List</param>
		/// <param name="p_sxValue">Value to set</param>
		private void SetValue(XmlElement p_objXmlElem, CodeList p_objCodLst, string p_sxValue)
		{
			string sType = "";
			string sTemp = "";
			LocalCache objCache = null;
			XmlElement objElm = null;
            bool bAddLOB = false;
            SysSettings objSysSettings = null;
			try
			{
				objCache= new LocalCache(m_sConnectString,m_iClientId); 
				sType = p_objXmlElem.GetAttribute("type").ToUpper().Trim();
				if (sType=="LISTVIEW")
				{
					if(p_objXmlElem.GetAttribute("name").ToUpper().Trim()=="CODELIST")
					{
						foreach(Code objCod in p_objCodLst)
						{
                            if (m_sTypeCode == "Line_of_Bus")
                            {
                                bAddLOB = false;
                                objSysSettings = new SysSettings(m_sConnectString, m_iClientId); //Ash - cloud
                                switch (objCod.ShortCode)
                                {
                                    case "GC":
                                        if (objSysSettings.UseGCLOB)
                                            bAddLOB = true;
                                        break;
                                    case "DI":
                                        if (objSysSettings.UseDILOB)
                                            bAddLOB = true;
                                        break;
                                    case "PC":
                                        if (objSysSettings.UsePCLOB)
                                            bAddLOB = true;
                                        break;
                                    case "VA":
                                        if (objSysSettings.UseVALOB)
                                            bAddLOB = true;
                                        break;
                                    case "WC":
                                        if (objSysSettings.UseWCLOB)
                                            bAddLOB = true;
                                        break;
                                }

                                if (bAddLOB)
                                {
                                    objElm = p_objXmlElem.OwnerDocument.CreateElement("option");
                                    objElm.SetAttribute("id", objCod.CodeId.ToString());
                                    objElm.SetAttribute("code", objCod.ShortCode);
                                    objElm.SetAttribute("Description", objCod.CodeDesc);
                                    if (objCod.RelCodeDesc != "")
                                        objElm.SetAttribute("Parent", objCod.RelShortCode + "-" +
                                            objCod.RelCodeDesc);
                                    else
                                        objElm.SetAttribute("Parent", objCod.RelShortCode);

                                    sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.StartDate), "MM/dd/yyyy");
                                    objElm.SetAttribute("Start-Date", sTemp);
                                    sTemp = "";

                                    sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.EndDate), "MM/dd/yyyy");
                                    objElm.SetAttribute("End-Date", sTemp);
                                    sTemp = "";

                                    objElm.SetAttribute("Organization", objCod.Organisation);
                                    p_objXmlElem.AppendChild(objElm);
                                    objElm = null;
                                }
                            }
                            else
                            {
                              
                                    objElm = p_objXmlElem.OwnerDocument.CreateElement("option");
                                    objElm.SetAttribute("id", objCod.CodeId.ToString());
                                    objElm.SetAttribute("code", objCod.ShortCode);
                                    objElm.SetAttribute("Description", objCod.CodeDesc);
                                    if (objCod.RelCodeDesc != "")
                                        objElm.SetAttribute("Parent", objCod.RelShortCode + "-" +
                                            objCod.RelCodeDesc);
                                    else
                                        objElm.SetAttribute("Parent", objCod.RelShortCode);

                                    sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.StartDate), "MM/dd/yyyy");
                                    objElm.SetAttribute("Start-Date", sTemp);
                                    sTemp = "";

                                    sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.EndDate), "MM/dd/yyyy");
                                    objElm.SetAttribute("End-Date", sTemp);
                                    sTemp = "";

                                    objElm.SetAttribute("Organization", objCod.Organisation);
                                    p_objXmlElem.AppendChild(objElm);
                                    objElm = null;
                            }
						}
					}
				} 			
				else if (sType=="COMBOBOX")
				{
					if (p_objXmlElem.GetAttribute("name").ToUpper().Trim()=="TRIGGER_DATE")
					{
						//Current System Date details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Current System Date");
						objElm.SetAttribute("tvalue", "SYSTEM_DATE");
						if(p_sxValue.ToUpper().Trim()=="SYSTEM_DATE")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;

						//Event Date Details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Event Date");
						objElm.SetAttribute("tvalue", "EVENT.DATE_OF_EVENT");
						if(p_sxValue.ToUpper().Trim()=="EVENT.DATE_OF_EVENT")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;

						//Claim Date Details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Claim Date");
						objElm.SetAttribute("tvalue", "CLAIM.DATE_OF_CLAIM");
						if(p_sxValue.ToUpper().Trim()=="CLAIM.DATE_OF_CLAIM")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;
  
						//Payment transaction Date details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Payment transaction Date");
						objElm.SetAttribute("tvalue", "FUNDS.TRANS_DATE");
						if(p_sxValue.ToUpper().Trim()=="FUNDS.TRANS_DATE")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;

						//Policy Effective Date details
						objElm = p_objXmlElem.OwnerDocument.CreateElement(
							p_objXmlElem.GetAttribute("optionname"));
						objElm.SetAttribute("tdate", "Policy Effective Date");
						objElm.SetAttribute("tvalue", "POLICY.EFFECTIVE_DATE");
						if(p_sxValue.ToUpper().Trim()=="POLICY.EFFECTIVE_DATE")
							objElm.SetAttribute("selected", "1");
						p_objXmlElem.AppendChild(objElm);
						objElm = null;
					}
					else if (p_objXmlElem.GetAttribute("name").ToUpper().Trim()=="GLOSSARY_TYPES")
					{
						foreach (Code  objCod in p_objCodLst)
						{
							if (objCod.CodeId  ==  objCache.GetCodeId("1", "GLOSSARY_TYPES") ||
								objCod.CodeId ==  objCache.GetCodeId("3", "GLOSSARY_TYPES") ||
								objCod.CodeId ==  objCache.GetCodeId("4", "GLOSSARY_TYPES") ||
								objCod.CodeId ==  objCache.GetCodeId("7", "GLOSSARY_TYPES") ||
								objCod.CodeId ==  objCache.GetCodeId("8", "GLOSSARY_TYPES") ||
								objCod.CodeId ==  objCache.GetCodeId("10", "GLOSSARY_TYPES") )
							{
								objElm = p_objXmlElem.OwnerDocument.CreateElement("TypeCode");
								if (objCod.ShortCode=="3")
									objElm.SetAttribute("selected", "1");
								objElm.SetAttribute("id",objCod.CodeId.ToString());
								objElm.SetAttribute("code",objCod.ShortCode);
								objElm.SetAttribute("Description",objCod.CodeDesc);
								p_objXmlElem.AppendChild(objElm);
								objElm = null;
							}
						}
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			finally
			{
                if (objCache != null)
                    objCache.Dispose();
                objSysSettings =null;
				objElm = null;
			}
		}	

		/// Name		: SaveTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 02/10/2005		   Changes related to LocalCache and Utilities -Pankaj
		///************************************************************
		/// <summary>
		/// Saves Glossary Table to the database
		/// </summary>
		/// <param name="p_sXmlDoc">Xml Input String</param>
		/// <param name="p_sMode">Edit / Add</param>
		/// <param name="p_sInputData">Input Xml String. For structure pls refer design doc</param>
		/// <returns>Xml Document string</returns>
		public XmlDocument SaveTable(string p_sXmlDoc, string p_sMode,string p_sInputData, int p_iDatabaseId)
		{
			XmlDocument objXmlDoc=null;
			XmlNodeList objTblNodLst = null;
			DbConnection objCon=null;
			DbTransaction objTrans=null;
			DbCommand objCmd = null;
			LocalCache objCache = null;
			XmlDocument objXmlDom=null;
			int iTypeCode = 0;
			string sSql="";
            string sLangCode = string.Empty;  //Aman ML Change
			try
			{			
				//Load the Xml Documents
				objXmlDoc =  new XmlDocument();
                objXmlDoc.LoadXml(p_sInputData);
				objXmlDom = new XmlDocument(); 
				objXmlDom.LoadXml(p_sXmlDoc);
            }catch(XmlException p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XmlErr", m_iClientId), p_objEx);
			}
			try
			{            
                objCache = new LocalCache(m_sConnectString,m_iClientId);
				objTblNodLst =  objXmlDoc.GetElementsByTagName("Table");

				if (objTblNodLst == null)
					throw new XmlOperationException(
						Globalization.GetString("UtilityDriver.SaveTable.InvalidXML", m_iClientId));

				objCon = DbFactory.GetDbConnection(m_sConnectString);
				objCon.Open(); 			
				objTrans = objCon.BeginTransaction(); 
				objCmd = objCon.CreateCommand();  
				objCmd.Transaction = objTrans; 

				if (p_sMode.ToUpper().Trim() == "NEW")
				{
					foreach(XmlElement objElem in objTblNodLst)
					{
						string sTypeCod = objElem.GetElementsByTagName("TypeCode")[0].InnerXml;

						if (sTypeCod!="" && Conversion.IsNumeric(sTypeCod) )
							iTypeCode = objCache.GetCodeId(sTypeCod,"GLOSSARY_TYPES");

                        //vsharma65 start MITS 19999
                        //Check if the record already exists
                        if (ChechDuplicateExist("GLOSSARY", "SYSTEM_TABLE_NAME", objElem.GetElementsByTagName("SysTableName")[0].InnerXml, objElem.GetElementsByTagName("TableName")[0].InnerXml, "0", "", "0")) // Mihika: DuplicateExists signature changed
                            throw new RMAppException(Globalization.GetString("UtilityDriver.SaveTable.DuplicateTable", m_iClientId));
                        //vsharma65 End MITS 19999


						//Changed By: Nikhil Garg.     Date: 28-Feb-2005
						//Reason: It was giving an error on passing the connection as there is a transaction attached to 
						//		  this connection and in Utilities it was not assigning this transaction to the command object 
						//		  Hence I passed connectin string and create a new connection	
						//int iTableId = Utilities.GetNextUID(objCon,"GLOSSARY");
                        int iTableId = Utilities.GetNextUID(m_sConnectString, "GLOSSARY", m_iClientId);

                        //Insert GLOSSARY details
                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                        DbWriter writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("GLOSSARY");

                        //if (GetStringValue(objElem.GetElementsByTagName("IndTableId")[0]).Equals("NULL"))
                        //    writer.Fields.Add("IND_STAND_TABLE_ID", DBNull.Value);
                        //else
                        //    writer.Fields.Add("IND_STAND_TABLE_ID", Conversion.ConvertStrToInteger(GetStringValue(objElem.GetElementsByTagName("IndTableId")[0])));

                        writer.Fields.Add("IND_STAND_TABLE_ID", GetStringValue(objElem.GetElementsByTagName("IndTableId")[0]));

                        writer.Fields.Add("LINE_OF_BUS_FLAG", GetValue(objElem.GetElementsByTagName("LineOfBusFlag")[0]));
                        writer.Fields.Add("REQD_IND_TABL_FLAG", GetValue(objElem.GetElementsByTagName("ReqIndTable")[0]));
                        writer.Fields.Add("TABLE_ID", iTableId);

                        if (objElem.GetElementsByTagName("SysTableName")[0] != null && objElem.GetElementsByTagName("SysTableName")[0].InnerXml != "")
                            writer.Fields.Add("SYSTEM_TABLE_NAME", Utilities.StripHTMLTags(objElem.GetElementsByTagName("SysTableName")[0].InnerXml));
                        
                        writer.Fields.Add("GLOSSARY_TYPE_CODE", iTypeCode);
                        writer.Fields.Add("ATTACHMENTS_FLAG", GetValue(objElem.GetElementsByTagName("AttchFlag")[0]));

                        //if (GetStringValue(objElem.GetElementsByTagName("RelatedId")[0]).Equals("NULL"))
                        //    writer.Fields.Add("RELATED_TABLE_ID", DBNull.Value);
                        //else
                        //    writer.Fields.Add("RELATED_TABLE_ID", Conversion.ConvertStrToInteger(GetStringValue(objElem.GetElementsByTagName("RelatedId")[0])));

                        writer.Fields.Add("RELATED_TABLE_ID", GetStringValue(objElem.GetElementsByTagName("RelatedId")[0]));

                        writer.Fields.Add("REQD_REL_TABL_FLAG", GetValue(objElem.GetElementsByTagName("RelTableFlag")[0]));
                        writer.Fields.Add("RM_USER_ID", Utilities.StripHTMLTags(objElem.GetElementsByTagName("UserId")[0].InnerXml));
                        writer.Fields.Add("DTTM_LAST_UPDATE", Conversion.ToDbDateTime(DateTime.Now).ToString());
                        writer.Fields.Add("TREE_DISPLAY_FLAG", GetValue(objElem.GetElementsByTagName("TreeFlag")[0]));

                        writer.Execute(objTrans);
                        writer.Reset(true);

                        //sSql = String.Format("INSERT INTO GLOSSARY(IND_STAND_TABLE_ID,LINE_OF_BUS_FLAG," +
                        //    "REQD_IND_TABL_FLAG,TABLE_ID,SYSTEM_TABLE_NAME,GLOSSARY_TYPE_CODE,ATTACHMENTS_FLAG," +
                        //    "RELATED_TABLE_ID,REQD_REL_TABL_FLAG,RM_USER_ID,DTTM_LAST_UPDATE,TREE_DISPLAY_FLAG) " +
                        //    "VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},'{9}','{10}',{11})",
                        //    GetStringValue(objElem.GetElementsByTagName("IndTableId")[0]),
                        //    GetValue(objElem.GetElementsByTagName("LineOfBusFlag")[0]),
                        //    GetValue(objElem.GetElementsByTagName("ReqIndTable")[0]),
                        //    iTableId,
                        //    GetStringValue(objElem.GetElementsByTagName("SysTableName")[0]),
                        //    iTypeCode,
                        //    GetValue(objElem.GetElementsByTagName("AttchFlag")[0]),
                        //    GetStringValue(objElem.GetElementsByTagName("RelatedId")[0]),
                        //    GetValue(objElem.GetElementsByTagName("RelTableFlag")[0]),
                        //    objElem.GetElementsByTagName("UserId")[0].InnerXml,
                        //    Conversion.ToDbDateTime(DateTime.Now).ToString(),
                        //    GetValue(objElem.GetElementsByTagName("TreeFlag")[0]));

                        //objCmd.CommandText = sSql;
                        //objCmd.ExecuteNonQuery();

                        //Insert GLOSSARY_TEXT details
                        //DbWriter writerGlossaryText = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("GLOSSARY_TEXT");

                        writer.Fields.Add("TABLE_ID", iTableId);

                        if (objElem.GetElementsByTagName("TableName")[0] != null && objElem.GetElementsByTagName("TableName")[0].InnerXml != "")
                            writer.Fields.Add("TABLE_NAME", Utilities.StripHTMLTags(objElem.GetElementsByTagName("TableName")[0].InnerXml));
                        
                        //Aman ML Change for saving the table in Base Language in CODES_TEXT table.
                        //writer.Fields.Add("LANGUAGE_CODE", GetNLSCode(p_iDatabaseId));
                        if (RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture") != null) //Aman ML Change
                        {
                            sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
                        }
                        writer.Fields.Add("LANGUAGE_CODE", sLangCode);
                        //Aman ML Change
                        writer.Execute(objTrans);
                        writer.Reset(true);

                        //sSql = String.Format("INSERT INTO GLOSSARY_TEXT(TABLE_ID,TABLE_NAME," +
                        //    "LANGUAGE_CODE) VALUES ({0},{1},{2})", iTableId,
                        //    GetStringValue(objElem.GetElementsByTagName("TableName")[0]),
                        //    GetNLSCode(p_iDatabaseId));
                        //objCmd.CommandText = sSql;
                        //objCmd.ExecuteNonQuery();

                        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    }
                }
                else if (p_sMode.ToUpper().Trim() == "EDIT")
                {
                    foreach (XmlElement objElem in objTblNodLst)
                    {
                        //Update GLOSSARY details
                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                        DbWriter writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("GLOSSARY");
                        writer.Where.Add(String.Format("TABLE_ID={0}", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml)));

                        //if (GetStringValue(objElem.GetElementsByTagName("IndTableId")[0]).Equals("NULL"))
                        //    writer.Fields.Add("IND_STAND_TABLE_ID", DBNull.Value);
                        //else
                        //    writer.Fields.Add("IND_STAND_TABLE_ID", Conversion.ConvertStrToInteger(GetStringValue(objElem.GetElementsByTagName("IndTableId")[0])));

                        writer.Fields.Add("IND_STAND_TABLE_ID", GetStringValue(objElem.GetElementsByTagName("IndTableId")[0]));

                        //if (GetStringValue(objElem.GetElementsByTagName("RelatedId")[0]).Equals("NULL"))
                        //    writer.Fields.Add("RELATED_TABLE_ID", DBNull.Value);
                        //else
                        //    writer.Fields.Add("RELATED_TABLE_ID", Conversion.ConvertStrToInteger(GetStringValue(objElem.GetElementsByTagName("RelatedId")[0])));

                        writer.Fields.Add("RELATED_TABLE_ID", GetStringValue(objElem.GetElementsByTagName("RelatedId")[0]));

                        writer.Fields.Add("REQD_IND_TABL_FLAG", GetValue(objElem.GetElementsByTagName("ReqIndTable")[0]));
                        writer.Fields.Add("REQD_REL_TABL_FLAG", GetValue(objElem.GetElementsByTagName("RelTableFlag")[0]));
                        writer.Fields.Add("ATTACHMENTS_FLAG", GetValue(objElem.GetElementsByTagName("AttchFlag")[0]));
                        writer.Fields.Add("TREE_DISPLAY_FLAG", GetValue(objElem.GetElementsByTagName("TreeFlag")[0]));
                        writer.Fields.Add("LINE_OF_BUS_FLAG", GetValue(objElem.GetElementsByTagName("LineOfBusFlag")[0]));

                        writer.Execute(objTrans);
                        writer.Reset(true);

                        //sSql = String.Format("UPDATE GLOSSARY SET IND_STAND_TABLE_ID={0}," +
                        //    "REQD_IND_TABL_FLAG={7}, RELATED_TABLE_ID={1}, REQD_REL_TABL_FLAG={2}, " +
                        //    "ATTACHMENTS_FLAG={3}, TREE_DISPLAY_FLAG={4}, LINE_OF_BUS_FLAG={5} " +
                        //    "WHERE TABLE_ID={6}",
                        //    GetStringValue(objElem.GetElementsByTagName("IndTableId")[0]),
                        //    GetStringValue(objElem.GetElementsByTagName("RelatedId")[0]),
                        //    GetValue(objElem.GetElementsByTagName("RelTableFlag")[0]),
                        //    GetValue(objElem.GetElementsByTagName("AttchFlag")[0]),
                        //    GetValue(objElem.GetElementsByTagName("TreeFlag")[0]),
                        //    GetValue(objElem.GetElementsByTagName("LineOfBusFlag")[0]),
                        //    objElem.GetElementsByTagName("TableId")[0].InnerXml,
                        //    GetValue(objElem.GetElementsByTagName("ReqIndTable")[0]));
                        //objCmd.CommandText = sSql;
                        //objCmd.ExecuteNonQuery();

                        //Update GLOSSARY_TEXT
                        //DbWriter writerGlossaryText = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("GLOSSARY_TEXT");
                        writer.Where.Add(String.Format("TABLE_ID={0}", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("TableId")[0].InnerXml))));

                        if (objElem.GetElementsByTagName("TableName")[0] != null && objElem.GetElementsByTagName("TableName")[0].InnerXml != "")
                            writer.Fields.Add("TABLE_NAME", Utilities.StripHTMLTags(objElem.GetElementsByTagName("TableName")[0].InnerXml));

                        writer.Execute(objTrans);
                        writer.Reset(true);

                        //sSql = String.Format("UPDATE GLOSSARY_TEXT SET TABLE_NAME={0} WHERE TABLE_ID={1}",
                        //    GetStringValue(objElem.GetElementsByTagName("TableName")[0]),
                        //    objElem.GetElementsByTagName("TableId")[0].InnerXml);
                        //objCmd.CommandText = sSql;
                        //objCmd.ExecuteNonQuery();

                        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
					}
				}
				objTrans.Commit();  
				objCon.Close();
				//return AppendXMLSaveTable(objXmlDom).OuterXml;
				return AppendXMLSaveTable(objXmlDom);
			}				 
			catch(RMAppException p_objEx)
			{
				if(objTrans!=null)objTrans.Rollback();
				throw p_objEx;
			}
			catch(XmlException p_objEx)
			{
				if(objTrans!=null)objTrans.Rollback();  
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			catch(Exception p_objEx)
			{
				if(objTrans!=null)objTrans.Rollback();  
				throw new RMAppException(Globalization.GetString("UtilityDriver.SaveTable.DataErr", m_iClientId), p_objEx);
			}
			finally
			{
				objCmd = null;
				objXmlDoc=null;
				objXmlDom = null;
				objTblNodLst = null;
                if (objCache != null)
                    objCache.Dispose();
				if (objTrans != null)objTrans.Dispose();
				if (objCon != null)objCon.Dispose();
			}
		}

		/// Name		: SaveCode
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 02/10/2005		   Changes related to LocalCache and Utilities -Pankaj
		///************************************************************
		/// <summary>
		/// Saves the code
		/// </summary>
		/// <param name="p_sXmlDoc">Xml Dom Input String</param>
		/// <param name="p_sMode">Edit / Add mode</param>
		/// <param name="p_sInputData">Input xml string</param>
        /// <param name="p_sLoginName">Login user name</param>
		/// <returns>Saved Code Xml String</returns> 
		public void SaveCode(string p_sMode,string p_sInputData, string p_sLoginName)
		{
			XmlDocument objXmlDoc=null;
			XmlNodeList objTblNodLst = null;
			DbConnection objCon=null;
			DbTransaction objTrans =null;
			DbCommand objCmd =null;
			LocalCache objCache = null;
			try
			{
				//Load the Xml Documents
				objXmlDoc =  new XmlDocument();
				objXmlDoc.LoadXml(p_sInputData);
			}catch(XmlException p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XmlErr", m_iClientId), p_objEx);
			}

			try
			{
				objCache = new LocalCache(m_sConnectString,m_iClientId);
				objTblNodLst =  objXmlDoc.GetElementsByTagName("Code");

				if (objTblNodLst == null)
					throw new XmlOperationException(
						Globalization.GetString("UtilityDriver.SaveCode.InvalidXML", m_iClientId));

				objCon = DbFactory.GetDbConnection(m_sConnectString);
				objCon.Open(); 			
				objTrans = objCon.BeginTransaction(); 
				objCmd = objCon.CreateCommand();  
				objCmd.Transaction = objTrans; 

				if (p_sMode == "new")
				{
					foreach(XmlElement objElem in objTblNodLst)
					{
						//Check if the record already exists
						if (DuplicateExists("CODES","SHORT_CODE",
							objElem.GetElementsByTagName("ShortCode")[0].InnerXml,
							objElem.GetElementsByTagName("TableId")[0].InnerXml,"new",
							objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml))// Mihika Defect#2356 Check should include Line of Business
							throw new RMAppException(
								Globalization.GetString("UtilityDriver.SaveCode.DuplicateCode", m_iClientId));
                        //JIRA 7810 snehal start 

                        if (ChildCodeExists("CODES", "RELATED_CODE_ID",
                         objElem.GetElementsByTagName("RelatedCode")[0].InnerXml,
                          objElem.GetElementsByTagName("TableId")[0].InnerXml, "new", objElem.GetElementsByTagName("RelatedCode")[0].InnerXml, "HOLD_REASON_PARENT", "HOLD_REASON_CODE", "OTHERS" ))
                            throw new RMAppException(Globalization.GetString("UtilityDriver.SaveCode.ChildCodeExists", m_iClientId));
                        //JIRA 7810 end
						//Changed By: Nikhil Garg.     Date: 28-Feb-2005
						//Reason: It was giving an error on passing the connection as there is a transaction attached to 
						//		  this connection and in Utilities it was not assigning this transaction to the command object 
						//		  Hence I passed connectin string and create a new connection	
						//int iCodeId = Utilities.GetNextUID(objCon,"CODES");
                        int iCodeId = Utilities.GetNextUID(m_sConnectString, "CODES", m_iClientId);

                        
						//Insert New Codes
                        //rsushilaggar - MITS 18674
                        //MITS 35329  Starts asharma326 
                        var TableID=Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml);
                        var TableName = string.Empty;
                        object objTable = DbFactory.ExecuteScalar(objCon.ConnectionString,
                                string.Format(@"SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID={0}", TableID));
                        {
                            TableName =Convert.ToString(objTable);
                        }
                        //MITS 35329  Ends asharma326 
                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                        DbWriter writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("CODES");

                        if (!string.IsNullOrEmpty((objElem.GetElementsByTagName("StartDt")[0]).InnerXml))
                        {
                            if (objElem.GetElementsByTagName("IndStdCode")[0] != null && objElem.GetElementsByTagName("IndStdCode")[0].InnerXml != "")
                                writer.Fields.Add("IND_STANDARD_CODE", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("IndStdCode")[0].InnerXml));

                            if (objElem.GetElementsByTagName("LineOfBusCode")[0] != null && objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml != "")
                                writer.Fields.Add("LINE_OF_BUS_CODE", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml));
                            
                            writer.Fields.Add("CODE_ID", iCodeId);

                            if (objElem.GetElementsByTagName("TableId")[0] != null && objElem.GetElementsByTagName("TableId")[0].InnerXml != "")
                                writer.Fields.Add("TABLE_ID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml));

                            if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != ""){
                                if(TableName=="ENTITY_PREFIX"||TableName=="ENTITY_SUFFIX")
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                                else
                                    writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                            }

                            if (objElem.GetElementsByTagName("RelatedCode")[0] != null && objElem.GetElementsByTagName("RelatedCode")[0].InnerXml != "")
                                writer.Fields.Add("RELATED_CODE_ID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("RelatedCode")[0].InnerXml));
                            
                            writer.Fields.Add("DELETED_FLAG", 0);
                            writer.Fields.Add("TRIGGER_DATE_FIELD", GetStringValue(objElem.GetElementsByTagName("TriggerDt")[0]));

                            if (objElem.GetElementsByTagName("OrgId")[0] != null && objElem.GetElementsByTagName("OrgId")[0].InnerXml != "")
                                writer.Fields.Add("ORG_GROUP_EID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("OrgId")[0].InnerXml));
                            
                            writer.Fields.Add("DTTM_RCD_ADDED", Conversion.ToDbDateTime(DateTime.Now));
                            writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                            writer.Fields.Add("ADDED_BY_USER", p_sLoginName);
                            writer.Fields.Add("UPDATED_BY_USER", p_sLoginName);
                            writer.Fields.Add("EFF_START_DATE", Conversion.GetDate(GetDateValue(objElem.GetElementsByTagName("StartDt")[0])));
                            writer.Fields.Add("EFF_END_DATE", Conversion.GetDate(GetDateValue(objElem.GetElementsByTagName("EndDt")[0])));
                        }
                        else
                        {
                            if (objElem.GetElementsByTagName("IndStdCode")[0] != null && objElem.GetElementsByTagName("IndStdCode")[0].InnerXml != "")
                                writer.Fields.Add("IND_STANDARD_CODE", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("IndStdCode")[0].InnerXml));

                            if (objElem.GetElementsByTagName("LineOfBusCode")[0] != null && objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml != "")
                                writer.Fields.Add("LINE_OF_BUS_CODE", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml));
                            
                            writer.Fields.Add("CODE_ID", iCodeId);

                            if (objElem.GetElementsByTagName("TableId")[0] != null && objElem.GetElementsByTagName("TableId")[0].InnerXml != "")
                                writer.Fields.Add("TABLE_ID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml));

                            if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != "")
                            {
                                if(TableName=="ENTITY_PREFIX"||TableName=="ENTITY_SUFFIX")
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                                else
                                    writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                                }

                            if (objElem.GetElementsByTagName("RelatedCode")[0] != null && objElem.GetElementsByTagName("RelatedCode")[0].InnerXml != "")
                                writer.Fields.Add("RELATED_CODE_ID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("RelatedCode")[0].InnerXml));
                            
                            writer.Fields.Add("DELETED_FLAG", 0);
                            writer.Fields.Add("TRIGGER_DATE_FIELD", GetStringValue(objElem.GetElementsByTagName("TriggerDt")[0]));

                            if (objElem.GetElementsByTagName("OrgId")[0] != null && objElem.GetElementsByTagName("OrgId")[0].InnerXml != "")
                                writer.Fields.Add("ORG_GROUP_EID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("OrgId")[0].InnerXml));
                            
                            writer.Fields.Add("DTTM_RCD_ADDED", Conversion.ToDbDateTime(DateTime.Now));
                            writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                            writer.Fields.Add("ADDED_BY_USER", p_sLoginName);
                            writer.Fields.Add("UPDATED_BY_USER", p_sLoginName);
                        }


                        writer.Execute(objTrans);
                        writer.Reset(true);

                        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
						// npadhy 10/10/2006 MITS 7774 The Code description was not saved Correctly
						XmlElement objElm=null;
						string CodeDesc = "";	
						objElm = (XmlElement)objXmlDoc.SelectSingleNode("//CodeDesc");
                        //Parijat :Mits 11759 --so that even the single codes are accepted
						if(objElm!=null)
                            CodeDesc = objElm.InnerText.Replace("'", "''");

                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                        writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("CODES_TEXT");

                        writer.Fields.Add("CODE_ID", iCodeId);

                        if (objElem.GetElementsByTagName("LanguageCode")[0] != null && objElem.GetElementsByTagName("LanguageCode")[0].InnerXml != "")
                            writer.Fields.Add("LANGUAGE_CODE", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("LanguageCode")[0].InnerXml)));

                        if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != "")
                        {
                            if (TableName == "ENTITY_PREFIX" || TableName == "ENTITY_SUFFIX")
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                            else
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                        }

                        writer.Fields.Add("CODE_DESC", CodeDesc);

                        writer.Execute(objTrans);
                        writer.Reset(true);

                        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
					}
				}			
				else if (p_sMode == "Edit")
				{
					foreach(XmlElement objElem in objTblNodLst)
					{
						// Mihika: Check for Duplicate codes added in case of editing a record.
						if (DuplicateExists("CODES","SHORT_CODE",
							objElem.GetElementsByTagName("ShortCode")[0].InnerXml,
							objElem.GetElementsByTagName("TableId")[0].InnerXml,"edit",
							objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml))
							throw new RMAppException(
								Globalization.GetString("UtilityDriver.SaveCode.DuplicateCode", m_iClientId));
                        //JIRA 7810 snehal start 
                        if (ChildCodeExists("CODES", "RELATED_CODE_ID",
                          objElem.GetElementsByTagName("RelatedCode")[0].InnerXml,
                           objElem.GetElementsByTagName("TableId")[0].InnerXml, "edit", objElem.GetElementsByTagName("RelatedCode")[0].InnerXml, "HOLD_REASON_PARENT", "HOLD_REASON_CODE", "OTHERS"))
                           throw new RMAppException(Globalization.GetString("UtilityDriver.SaveCode.ChildCodeExists", m_iClientId));
                       //JIRA 7810 end

                        //MITS 35329  Starts asharma326 
                        var TableID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml);
                        var TableName = string.Empty;
                        object objTable = DbFactory.ExecuteScalar(objCon.ConnectionString,
                                string.Format(@"SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID={0}", TableID));
                        {
                            TableName = Convert.ToString(objTable);
                        }
                        //MITS 35329  Ends asharma326 
                        //rsushilaggar - MITS 18674
                        string startDate = !string.IsNullOrEmpty((objElem.GetElementsByTagName("StartDt")[0]).InnerXml) ? Conversion.GetDate(GetDateValue(objElem.GetElementsByTagName("StartDt")[0])) : "NULL";
                        string endDate = !string.IsNullOrEmpty((objElem.GetElementsByTagName("EndDt")[0]).InnerXml) ? Conversion.GetDate(GetDateValue(objElem.GetElementsByTagName("EndDt")[0])) : "NULL";

                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                        DbWriter writer = DbFactory.GetDbWriter(objCon);
                        
                        writer.Tables.Add("CODES");
                        writer.Where.Add(String.Format("CODE_ID={0}", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml)));

                        if (objElem.GetElementsByTagName("IndStdCode")[0] != null && objElem.GetElementsByTagName("IndStdCode")[0].InnerXml != "")
                            writer.Fields.Add("IND_STANDARD_CODE", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("IndStdCode")[0].InnerXml));

                        if (objElem.GetElementsByTagName("LineOfBusCode")[0] != null && objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml != "")
                            writer.Fields.Add("LINE_OF_BUS_CODE", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("LineOfBusCode")[0].InnerXml));

                        if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != "")
                        {
                            if (TableName == "ENTITY_PREFIX" || TableName == "ENTITY_SUFFIX")
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                            else
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                        }

                        if (objElem.GetElementsByTagName("RelatedCode")[0] != null && objElem.GetElementsByTagName("RelatedCode")[0].InnerXml != "")
                            writer.Fields.Add("RELATED_CODE_ID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("RelatedCode")[0].InnerXml));

                        writer.Fields.Add("TRIGGER_DATE_FIELD", GetStringValue(objElem.GetElementsByTagName("TriggerDt")[0]));

                        if (objElem.GetElementsByTagName("OrgId")[0] != null && objElem.GetElementsByTagName("OrgId")[0].InnerXml != "")
                            writer.Fields.Add("ORG_GROUP_EID", Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("OrgId")[0].InnerXml));

                        //Added for the issue 34316 nnithiyanand - Started
                        if (startDate.Equals("NULL"))
                        {
                            writer.Fields.Add("EFF_START_DATE", DBNull.Value);
                        }
                        else
                        {
                            writer.Fields.Add("EFF_START_DATE", startDate);
                        }

                        if (endDate.Equals("NULL"))
                        {
                            writer.Fields.Add("EFF_END_DATE", DBNull.Value);
                        }
                        else
                        {
                            writer.Fields.Add("EFF_END_DATE", endDate);
                        }
                        //Added for the issue 34316 nnithiyanand - Ended

                        //writer.Fields.Add("EFF_START_DATE", startDate); //Commented for issue 34316
                        //writer.Fields.Add("EFF_END_DATE", endDate); //Commented for issue 34316
						
                        writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                        writer.Fields.Add("UPDATED_BY_USER", p_sLoginName);
                        writer.Execute(objTrans);
                        writer.Reset(true);

                        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                        // Rahul (17th sept 2007) - mits 10300 - the cache doesnt gets updated when 
                        // the related code id is chanegd int hr frontend by the user
                        objCache.RemoveRelatedCodeId(Convert.ToInt32((objElem.GetElementsByTagName("TableId")[0]).InnerText));

						// npadhy 10/10/2006 MITS 7774 The Code description was not saved Correctly
						XmlElement objElm=null;
						string CodeDesc = "";	
						objElm = (XmlElement)objXmlDoc.SelectSingleNode("//CodeDesc");
						//Parijat: Mits 11759
                        if(objElm!=null)
                            CodeDesc = objElm.InnerText.Replace("'", "''");

                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                        writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("CODES_TEXT");
                        //Aman ML Change
                        writer.Where.Add(String.Format("CODE_ID={0} AND LANGUAGE_CODE={1}", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("TableId")[0].InnerXml)), Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("LanguageCode")[0].InnerXml))));
                        if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != "")
                        {
                            if (TableName == "ENTITY_PREFIX" || TableName == "ENTITY_SUFFIX")
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                            else
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                        }

                        writer.Fields.Add("CODE_DESC", CodeDesc);

                        writer.Execute(objTrans);
                        writer.Reset(true);
                        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
					}
				}
				else if (p_sMode == "delete")
				{
                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                    DbWriter writer = DbFactory.GetDbWriter(objCon);
                    writer.Tables.Add("CODES");
                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
					foreach(XmlElement objElem in objTblNodLst)
					{
						//Set flag
                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                        writer.Where.Add(String.Format("CODE_ID={0}", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("TableId")[0].InnerXml))));
                        writer.Fields.Add("DELETED_FLAG", -1);
                        writer.Execute(objTrans);
                        writer.Reset(true);                                                            
					}                   
				}
                    //Aman ML Change
                else if (p_sMode == "newNonBase")
                {
                    foreach (XmlElement objElem in objTblNodLst)
                    {   
                        XmlElement objElm = null;
                        string CodeDesc = "";
                        objElm = (XmlElement)objXmlDoc.SelectSingleNode("//CodeDesc");
                        if (objElm != null)
                            CodeDesc = objElm.InnerText.Replace("'", "''");
                        //MITS 35329  Starts asharma326 
                        var TableID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml);
                        var TableName = string.Empty;
                        object objTable = DbFactory.ExecuteScalar(objCon.ConnectionString,
                                string.Format(@"SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID={0}", TableID));
                        {
                            TableName = Convert.ToString(objTable);
                        }
                        //MITS 35329  Ends asharma326 
                        DbWriter writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("CODES_TEXT");
                        if (!DuplicateExists("CODES_TEXT", "LANGUAGE_CODE",
                            objElem.GetElementsByTagName("LanguageCode")[0].InnerXml,
                            objElem.GetElementsByTagName("ID")[0].InnerXml, "newNonBase", "0"))
                        {
                            writer.Fields.Add("CODE_ID", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("ID")[0].InnerXml)));

                            if (objElem.GetElementsByTagName("LanguageCode")[0] != null && objElem.GetElementsByTagName("LanguageCode")[0].InnerXml != "")
                                writer.Fields.Add("LANGUAGE_CODE", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("LanguageCode")[0].InnerXml)));

                            if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != "")
                            {
                                if (TableName == "ENTITY_PREFIX" || TableName == "ENTITY_SUFFIX")
                                    writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                                else
                                    writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                            }

                            writer.Fields.Add("CODE_DESC", CodeDesc);
                        }
                        else
                        {
                            writer.Where.Add(String.Format("CODE_ID={0} AND LANGUAGE_CODE={1}", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("ID")[0].InnerXml)), Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("LanguageCode")[0].InnerXml))));
                            if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != "")
                            {
                                if (TableName == "ENTITY_PREFIX" || TableName == "ENTITY_SUFFIX")
                                    writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                                else
                                    writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                            }

                            writer.Fields.Add("CODE_DESC", CodeDesc); 
                        }                       

                        writer.Execute(objTrans);
                        writer.Reset(true);
                       
                    }
                }
                else if (p_sMode == "EditNonBase")
                {
                    foreach (XmlElement objElem in objTblNodLst)
                    {
                        //MITS 35329  Starts asharma326 
                        var TableID = Conversion.ConvertStrToInteger(objElem.GetElementsByTagName("TableId")[0].InnerXml);
                        var TableName = string.Empty;
                        object objTable = DbFactory.ExecuteScalar(objCon.ConnectionString,
                                string.Format(@"SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID={0}", TableID));
                        {
                            TableName = Convert.ToString(objTable);
                        }
                        //MITS 35329  Ends asharma326
                        XmlElement objElm = null;
                        string CodeDesc = "";
                        objElm = (XmlElement)objXmlDoc.SelectSingleNode("//CodeDesc");
                        if (objElm != null)
                            CodeDesc = objElm.InnerText.Replace("'", "''");

                        DbWriter writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("CODES_TEXT");                       
                        writer.Where.Add(String.Format("CODE_ID={0} AND LANGUAGE_CODE={1}", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("ID")[0].InnerXml)), Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("LanguageCode")[0].InnerXml))));
                        if (objElem.GetElementsByTagName("ShortCode")[0] != null && objElem.GetElementsByTagName("ShortCode")[0].InnerXml != "")
                        {
                            if (TableName == "ENTITY_PREFIX" || TableName == "ENTITY_SUFFIX")
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml.Replace(" ", "-")));  //Ankit Start : MITS 31080 - ShortCode can not contain Spaces in it.
                            else
                                writer.Fields.Add("SHORT_CODE", Utilities.StripHTMLTags(objElem.GetElementsByTagName("ShortCode")[0].InnerXml));
                        }


                        writer.Fields.Add("CODE_DESC", CodeDesc);                       
                        writer.Execute(objTrans);
                        writer.Reset(true);
                    }
                }
                else if (p_sMode == "deleteNonBase")
                {                                                                                        
                    foreach (XmlElement objElem in objTblNodLst)
                    {
                        StringBuilder strSQLQuery = new StringBuilder();
                        strSQLQuery.Append(string.Format("DELETE CODES_TEXT WHERE CODE_ID = {0} AND LANGUAGE_CODE = {1}", "~CODE_ID~", "~CODE_LANG_CODE~"));
                        objCmd.CommandText = FormatSQLString(strSQLQuery.ToString());

                        Dictionary<string, object> dictQParams = new Dictionary<string, object>();
                        dictQParams.Add("CODE_ID", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("ID")[0].InnerXml)));
                        dictQParams.Add("CODE_LANG_CODE", this.LanguageCode);
                        DbFactory.ExecuteNonQuery(objCmd, dictQParams);
                        objCache.RemoveAnyKeyFromCache(string.Format("COD_{0}_{1}", Conversion.ConvertStrToInteger(Utilities.StripHTMLTags(objElem.GetElementsByTagName("ID")[0].InnerXml)), this.LanguageCode));
                    }
                    
                }
                //Aman ML Change
				objTrans.Commit();  
				objCon.Close();				
			}			
			catch(RMAppException p_objEx)
			{
				if(objTrans!=null)objTrans.Rollback();  
				throw p_objEx;
			}
			catch(XmlException p_objEx)
			{
				if(objTrans!=null)objTrans.Rollback();  
				throw new RMAppException(Globalization.GetString("UtilityDriver.XMLProcessErr", m_iClientId), p_objEx);
			}
			catch(Exception p_objEx)
			{
				if(objTrans!=null)objTrans.Rollback();  
				throw new RMAppException(Globalization.GetString("UtilityDriver.SaveCode.DataErr", m_iClientId), p_objEx);
			}
			finally
			{
				objCmd = null;
				objXmlDoc=null;
				objTblNodLst = null;
                if (objCache != null)
                    objCache.Dispose();
				if (objTrans != null)objTrans.Dispose();   
				if (objCon != null)objCon.Dispose();  				
			}
		}

		/// Name		: ImportDataFromTextFile
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Check for the file format and upload if OK.
		/// </summary>
		/// <param name="p_sXml">String xml input</param>
		/// <param name="p_sFileName">File name to be imported</param>
		/// <param name="p_iTypeCode">Code Type</param>
		/// <param name="p_iAction">Action to be taken</param>
		/// <param name="p_bEdit">Add / Edit mode</param>
		/// <returns></returns>
		public XmlDocument ImportDataFromTextFile(string p_sXml, string p_sFileName,
			int p_iTypeCode,int p_iAction, bool p_bEdit,int p_iTableId)
		{
			XmlDocument objXmlDoc=null;
			XmlDocument objRetDoc=null;
			try
			{
				//Load the Xml
				objXmlDoc = new XmlDocument(); 
				objXmlDoc.LoadXml(p_sXml); 
			}
			catch(XmlException p_objEx)
			{
				throw new RMAppException(
					Globalization.GetString("UtilityDriver.ImportDataFromTextFile.XmlErr", m_iClientId), p_objEx);
			}
			try
			{
				switch (p_iAction)
				{
					case 1://Check the File format
						//return CheckFile(objXmlDoc,p_sFileName,p_iTypeCode).OuterXml;
						return CheckFile(objXmlDoc,p_sFileName,p_iTypeCode);
					case 2://Upload the file
						//return ImportFile(objXmlDoc,p_sFileName,p_iTypeCode, p_bEdit).OuterXml;
						return ImportFile(objXmlDoc,p_sFileName,p_iTypeCode, p_bEdit,p_iTableId);
					case 3://Do a check for the file and then upload automatically
						objRetDoc = CheckFile(objXmlDoc,p_sFileName,p_iTypeCode);
						if (m_bFileOk)
							//return ImportFile(objRetDoc, p_sFileName,p_iTypeCode, p_bEdit).OuterXml;
							return ImportFile(objRetDoc, p_sFileName,p_iTypeCode, p_bEdit,p_iTableId);
						else
							//return objRetDoc.OuterXml;
							return objRetDoc;
				}
			
				throw new RMAppException( 
					Globalization.GetString("UtilityDriver.ImportDataFromTextFile.InvalidActionType", m_iClientId));
			}
			catch(Exception p_objEx)
			{
				throw p_objEx;
			}
			finally
			{
				objXmlDoc=null;
				objRetDoc=null;
			}
		}		

		#region GetValue(XmlNode p_objXMLNode)
		/// Name		: GetValue
		/// Author		: Navneet Sota
		/// Date Created: 06/14/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the vlaue 0 or 1 based on the input XML node for checkboxes
		/// </summary>
		/// <param name="p_objXMLNode">Xml ode</param>
		/// <returns>integer</returns>
		private int GetValue(XmlNode p_objXMLNode)
		{
			int iReturnValue = 0;

			if((p_objXMLNode !=null && p_objXMLNode.InnerXml != "") 
				&& (p_objXMLNode.InnerXml == "0" || p_objXMLNode.InnerXml == "1"))
				iReturnValue = 1;

			return iReturnValue;
		}
		#endregion

		#region GetStringValue(XmlNode p_objXMLNode)
		/// Name		: GetStringValue
		/// Author		: Navneet Sota
		/// Date Created: 06/14/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the vlaue 0 or 1 based on the input XML node for checkboxes
		/// </summary>
		/// <param name="p_objXMLNode">Xml ode</param>
		/// <returns>integer</returns>
		private string GetStringValue(XmlNode p_objXMLNode)
		{
			//string sReturnValue = "NULL";
            string sReturnValue =null;

            if (p_objXMLNode != null && p_objXMLNode.InnerXml != "")
            {
                //Deb Changes
                //sReturnValue = "'" + p_objXMLNode.InnerXml + "'";
                sReturnValue = p_objXMLNode.InnerText;
                sReturnValue = Utilities.StripHTMLTags(sReturnValue); // Added for MITS 22248: Yatharth
            }
			return sReturnValue;
		}
        /// Name		: GetStringValueWithQuots
        /// Author		: Parijat Sharma
        /// Date Created: 03/11/2008
        /// For MITS    : 11759
        ///************************************************************
        /// <summary>
        /// Returns string which can also take single quot as input 
        /// </summary>
        /// <param name="p_objXMLNode"></param>
        /// <returns></returns>

        private string GetStringValueWithQuot(XmlNode p_objXMLNode)
        {
            string sReturnValue = "NULL";
            //changed Rakhi for MITS-11493:START
            //if (p_objXMLNode != null && p_objXMLNode.InnerXml != "") //InnerXML changed to InnerText to stop parsing of special chars like & and <>
            //    sReturnValue = "'" + p_objXMLNode.InnerXml .Replace("'", "''") + "'"; 
            if (p_objXMLNode != null && p_objXMLNode.InnerText != "")
                sReturnValue = "'" + p_objXMLNode.InnerText.Replace("'", "''") + "'";
            //changed Rakhi for MITS-11493:END
            return sReturnValue;
        }

		private string GetDateValue(XmlNode p_objXMLNode)
		{
			string sReturnValue = "' '";

			if(p_objXMLNode !=null && p_objXMLNode.InnerXml != "") 
				sReturnValue = p_objXMLNode.InnerXml;

			return sReturnValue;
		}

		#endregion

		#region GetNLSCode(p_iDatabaseId)
		/// Name		: GetNLSCode
		/// Author		: Navneet Sota
		/// Date Created: 06/14/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the NLS code from security database
		/// </summary>
		/// <returns>integer</returns>
	//	private int GetNLSCode(int p_iDatabaseId)
       public int GetNLSCode(int p_iDatabaseId)
		{
			int iReturnValue = 1033;
			StringBuilder sbSQL = null;
			DbReader objReader = null;
			try
			{
				sbSQL = new StringBuilder();
				sbSQL.Append("SELECT USER_TABLE.NLS_CODE ");
				sbSQL.Append("FROM USER_TABLE,USER_DETAILS_TABLE  ");
				sbSQL.Append("WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID= ");
                sbSQL.Append(p_iDatabaseId);
                //abisht MITS 10932
                if (m_UserId != 0)
                {
                    sbSQL.Append(" AND USER_TABLE.USER_ID= ");
                    sbSQL.Append(m_UserId);
                }
				

				objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId),sbSQL.ToString());
				if (objReader.Read())
				{
					iReturnValue = Conversion.ConvertStrToInteger(objReader.GetValue("NLS_CODE").ToString());
					objReader.Close();
				}
			}
			catch(Exception objException)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetNLSCode.Err", m_iClientId) ,objException);
			}
			finally
			{
				sbSQL = null;
				if (objReader != null)
					objReader.Dispose();
			}
			return iReturnValue;
		}
		#endregion

		/// Name		: ImportFile
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 02/10/2005		   Changes related to LocalCache and Utilities -Pankaj
		///************************************************************
		/// <summary>
		/// Import the Flat text file
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input Document</param>
		/// <param name="p_sFileName">File name full path</param>
		/// <param name="p_iCodeType">Type Code</param>
		/// <param name="p_bEdit">Add / Edit mode</param>
		/// <returns>string</returns>
		private XmlDocument ImportFile(XmlDocument p_objXmlDoc, string p_sFileName, int p_iCodeType, bool p_bEdit, int p_iTableId)
		{
			StreamReader objStreamReader = null;
			String[] sColumn=null; 
			char[] delimit = ",".ToCharArray();
			//int iTableId = 0;
			LocalCache objCach = null;
			DbConnection objConn = null;
			DbTransaction objTrans = null;
			DbCommand objCmd = null;
			
            //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
            DbWriter writer = null;
            //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
            Dictionary<string, object> dictParams = null;
			try
			{
				objCach = new LocalCache(m_sConnectString,m_iClientId);  
				
				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				objTrans= objConn.BeginTransaction();
				
				objCmd = objConn.CreateCommand();				
				objCmd.Transaction = objTrans;
                dictParams = new Dictionary<string, object>();
				if (File.Exists(p_sFileName))
				{
					m_sFileName = p_sFileName;
					// Create an instance of StreamReader to read from a file.
					// The using statement also closes the StreamReader.
					using (objStreamReader = new StreamReader(m_sFileName)) 
					{
						string sLine = "";
						if ((sLine = objStreamReader.ReadLine())!= null)
							if ( sLine.ToUpper().Substring(0,7) == "[CODES]")
							{
								m_bFileOk =true;
								m_sCodeDesc =  objStreamReader.ReadLine().Trim();
								sLine =  objStreamReader.ReadLine().Trim();
								sColumn = sLine.Split(delimit);
								if(sColumn.Length > 0)
								{
									m_sTableName = sColumn[0].Trim();
									if(sColumn.Length > 1)
									{
                                        // MITS 16219 MAC
                                        // -- START --
                                        SysSettings objSysSettings = new SysSettings(m_sConnectString, m_iClientId); //Ash - cloud
                                        if (objSysSettings.OracleCaseIns == null || objSysSettings.OracleCaseIns == 0)
                                        {
                                            m_sSysTableName = sColumn[1].Trim().ToUpper().Replace(" ", "_");
                                        }
                                        else
                                        {
                                            m_sSysTableName = sColumn[1].Trim().Replace(" ", "_");
                                        } // else
                                        // -- END --

					
                                        
                                        //Rahul - 24 sept 2007 mits 10194 
                                        if (sColumn.Length > 2)
                                        {
                                            m_sSysTableParentName = sColumn[2].Trim();
                                            if (sColumn.Length > 3)
                                            {
                                                m_sCodeType = sColumn[3].Trim();
                                                if (sColumn.Length > 4)
                                                {
                                                    m_sLangCode = sColumn[4].Trim();
                                                }
                                            }
                                        }
									}
								}
                                //Aman ML Change 11/05/2012
                                //if (m_sLangCode == "0")m_sLangCode = "1033";                                                                                                  
                                m_sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]; //Aman ML Change
                                //Aman ML Change 11/05/2012
								//If Code Nitesh:02Jan2006 Starts
								if (m_sSysTableName!="")
								{
                                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                                    StringBuilder strSQL = new StringBuilder();
                                    strSQL.Append(string.Format(" SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME= {0} ", "~SYSTEM_TABLE_NAME~"));
                                    objCmd.CommandText = FormatSQLString(strSQL.ToString());

                                    //Dictionary<string, object> dictParams = new Dictionary<string, object>();
                                    dictParams.Add("SYSTEM_TABLE_NAME", m_sSysTableName);

                                    object objTableId = DbFactory.ExecuteScalar(objCmd, dictParams);
                                    dictParams.Clear();
                                    objCmd.Parameters.Clear();
                                    //objCmd.CommandText = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME= '" + m_sSysTableName + "'";
                                    //object objTableId = objCmd.ExecuteScalar();

                                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
									if(objTableId!=null && objTableId != System.DBNull.Value)
                                        p_iTableId = Conversion.ConvertObjToInt(objTableId, m_iClientId); 
									else 
										p_iTableId = 0;
								}
								
								if (p_iTableId == 0)
								{
                                    p_iTableId = Utilities.GetNextUID(objConn, "GLOSSARY", objTrans, m_iClientId); 

                                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                                    writer = DbFactory.GetDbWriter(objConn);
                                    writer.Tables.Add("GLOSSARY");

                                    writer.Fields.Add("TABLE_ID", p_iTableId);
                                    writer.Fields.Add("SYSTEM_TABLE_NAME", m_sSysTableName);
                                    writer.Fields.Add("GLOSSARY_TYPE_CODE", m_sCodeType);

                                    writer.Execute(objTrans);
                                    writer.Reset(true);

                                    //Insert into GLOSSARY_TEXT
                                    //writer = DbFactory.GetDbWriter(objConn);
                                    writer.Tables.Add("GLOSSARY_TEXT");

                                    writer.Fields.Add("TABLE_ID", p_iTableId);
                                    writer.Fields.Add("TABLE_NAME", m_sTableName);
                                    writer.Fields.Add("LANGUAGE_CODE", m_sLangCode);

                                    writer.Execute(objTrans);
                                    writer.Reset(true);

                                    //objCmd.CommandText = "INSERT INTO GLOSSARY (TABLE_ID,SYSTEM_TABLE_NAME," +
                                    //    "GLOSSARY_TYPE_CODE) VALUES (" + p_iTableId + ",'" + m_sSysTableName +
                                    //    "', " + m_sCodeType + ")";
                                    //objCmd.ExecuteNonQuery();

                                    ////Insert into GLOSSARY_TEXT
                                    //objCmd.CommandText = "INSERT INTO GLOSSARY_TEXT (TABLE_ID,TABLE_NAME," +
                                    //    "LANGUAGE_CODE) VALUES (" + p_iTableId + ",'" + m_sTableName + "'," +
                                    //    m_sLangCode + ")";
                                    //objCmd.ExecuteNonQuery();

                                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                                }
                                
								while((sLine = objStreamReader.ReadLine())!= null)
								{
									sColumn = sLine.Split(delimit);
									string sShortCode = sColumn[0];
									string sCodeDesc = sColumn[1];
									string sLangCode = sColumn[2];
                                    // rahul - 24 sept 2007 mits 10194 
                                    // codes LOB and Parent info not getting imported while 
                                    // importing codes thru in table maintanance
                                    string sCodeParentName = string.Empty;
                                    int iCodeParentName = 0;
                                    if (sColumn.Length > 2)
                                    {
                                        sCodeParentName = sColumn[2];
                                        sCodeParentName = sCodeParentName.Trim().ToUpper();
                                        objCmd.CommandText = @"SELECT CODE_ID FROM CODES WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + m_sSysTableParentName + "') AND SHORT_CODE='" + sCodeParentName + "'";
                                        object objCodeParentCode = objCmd.ExecuteScalar();
                                        if (objCodeParentCode != null && objCodeParentCode != System.DBNull.Value)
                                            iCodeParentName = Conversion.ConvertObjToInt(objCodeParentCode, m_iClientId); 
                                    }
                                    string sCodeLOB = string.Empty;
                                    int iCodeLOB = 0;
                                    if (sColumn.Length > 3)
                                    {
                                        sCodeLOB = sColumn[3];
                                        sCodeLOB = sCodeLOB.Trim().ToUpper();
                                        objCmd.CommandText = @"SELECT CODE_ID FROM CODES WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'LINE_OF_BUSINESS') AND SHORT_CODE='" + sCodeLOB + "'";
                                        object objLOB = objCmd.ExecuteScalar();
                                        if (objLOB != null && objLOB != System.DBNull.Value)
                                            iCodeLOB = Conversion.ConvertObjToInt(objLOB, m_iClientId);
                                    }  
									// end updates mits 10194
                                    //Aman ML Change
                                    //if (sColumn.Length > 4)
                                    //{
                                    //    sLangCode = sColumn[4];
                                    //}
                                    //if (string.IsNullOrEmpty(sLangCode) || (sLangCode == "0"))
                                    //{
                                    //    sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]; //Aman ML Change
                                    //}
                                    //Aman ML Change
                                    //Nitin Sachdeva; mits: 21014; 09/07/2011; start
                                    //objCmd.CommandText = "SELECT CODE_ID FROM CODES" +
                                    //    " WHERE SHORT_CODE = '" + sShortCode + "' AND TABLE_ID = " + p_iTableId +
                                    //    " AND (LINE_OF_BUS_CODE IS NULL OR LINE_OF_BUS_CODE = 0)";

                                    //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                                    StringBuilder strSQL = new StringBuilder();
                                    

                                    if (iCodeLOB == 0)
                                    {
                                        strSQL.Append("SELECT CODE_ID FROM CODES ");
                                        strSQL.Append(string.Format(" WHERE SHORT_CODE = {0} ", "~SHORT_CODE~"));
                                        strSQL.Append(string.Format(" AND TABLE_ID = {0} ", "~TABLE_ID~"));
                                        strSQL.Append(" AND (LINE_OF_BUS_CODE IS NULL OR LINE_OF_BUS_CODE = 0)");

                                        
                                        dictParams.Add("SHORT_CODE", sShortCode);
                                        dictParams.Add("TABLE_ID", p_iTableId);
                                    }
                                    else
                                    {
                                        strSQL.Append("SELECT CODE_ID FROM CODES ");
                                        strSQL.Append(string.Format(" WHERE SHORT_CODE = {0} ", "~SHORT_CODE~"));
                                        strSQL.Append(string.Format(" AND TABLE_ID = {0} ", "~TABLE_ID~"));
                                        strSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}", "~LINE_OF_BUS_CODE~"));


                                        dictParams.Add("SHORT_CODE", sShortCode);
                                        dictParams.Add("TABLE_ID", p_iTableId);
                                        dictParams.Add("LINE_OF_BUS_CODE", iCodeLOB);
                                    }
                                    
                                    objCmd.CommandText = FormatSQLString(strSQL.ToString());
                                    object objCodeId = DbFactory.ExecuteScalar(objCmd, dictParams);
                                    dictParams.Clear();
                                    objCmd.Parameters.Clear();
                                    //if (iCodeLOB == 0)
                                    //{
                                    //    objCmd.CommandText = "SELECT CODE_ID FROM CODES" +
                                    //    " WHERE SHORT_CODE = '" + sShortCode + "' AND TABLE_ID = " + p_iTableId +
                                    //    " AND (LINE_OF_BUS_CODE IS NULL OR LINE_OF_BUS_CODE = 0)";
                                    //}
                                    //else
                                    //{
                                    //    objCmd.CommandText = "SELECT CODE_ID FROM CODES" +
                                    //    " WHERE SHORT_CODE = '" + sShortCode + "' AND TABLE_ID = " + p_iTableId +
                                    //    " AND LINE_OF_BUS_CODE = " + iCodeLOB;
                                    //}

                                    //object objCodeId = objCmd.ExecuteScalar();

                                    //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                                    //mits: 21014; End
                                    
                                    int iCodeId = -1;

									if(objCodeId!=null && objCodeId != System.DBNull.Value)
                                        iCodeId = Conversion.ConvertObjToInt(objCodeId, m_iClientId); 
									
									if(iCodeId==-1)
									{
                                        iCodeId = Utilities.GetNextUID(objConn, "CODES", objTrans, m_iClientId);

                                        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                                        writer = DbFactory.GetDbWriter(objConn);
                                        writer.Tables.Add("CODES");

                                        writer.Fields.Add("CODE_ID", iCodeId);
                                        writer.Fields.Add("TABLE_ID", p_iTableId);
                                        writer.Fields.Add("SHORT_CODE", sShortCode);
                                        writer.Fields.Add("LINE_OF_BUS_CODE", iCodeLOB);
                                        writer.Fields.Add("IND_STANDARD_CODE", 0);
                                        writer.Fields.Add("DELETED_FLAG", 0);
                                        writer.Fields.Add("RELATED_CODE_ID", iCodeParentName);

                                        writer.Execute(objTrans);
                                        writer.Reset(true);

                                        //writer = DbFactory.GetDbWriter(objConn);
                                        writer.Tables.Add("CODES_TEXT");

                                        writer.Fields.Add("CODE_ID", iCodeId);
                                        //writer.Fields.Add("CODE_DESC", sCodeDesc.Replace("'", "''"));
                                        writer.Fields.Add("CODE_DESC", sCodeDesc);
                                        writer.Fields.Add("SHORT_CODE", sShortCode);
                                        writer.Fields.Add("LANGUAGE_CODE", m_sLangCode);
                                       // writer.Fields.Add("LANGUAGE_CODE", sLangCode);  //Aman ML Change

                                        writer.Execute(objTrans);
                                        writer.Reset(true);

                                        //objCmd.CommandText = "INSERT INTO CODES(CODE_ID,TABLE_ID,SHORT_CODE," +
                                        //    " LINE_OF_BUS_CODE,IND_STANDARD_CODE,DELETED_FLAG,RELATED_CODE_ID)" +
                                        //    " VALUES (" + iCodeId + "," + p_iTableId + ",'" +
                                        //    sShortCode + "'," + iCodeLOB.ToString() + ",0,0," + iCodeParentName.ToString() + ")";
                                        //objCmd.ExecuteNonQuery();

                                        //objCmd.CommandText = "INSERT INTO CODES_TEXT(CODE_ID,CODE_DESC," +
                                        //    "SHORT_CODE,LANGUAGE_CODE) VALUES (" + iCodeId + ",'" +
                                        //    sCodeDesc.Replace("'", "''") + "','" + sShortCode + "'," + m_sLangCode + ")";
                                        //objCmd.ExecuteNonQuery();

                                        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
									}
									else
									{
										if (p_bEdit)
										{
											//Update Codes
                                            //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                                            writer = DbFactory.GetDbWriter(objConn);
                                            writer.Tables.Add("CODES_TEXT");
                                            writer.Where.Add(String.Format("CODE_ID={0} AND LANGUAGE_CODE={1}", iCodeId, m_sLangCode));
                                            //writer.Where.Add(String.Format("CODE_ID={0} AND LANGUAGE_CODE={1}", iCodeId, sLangCode)); //Aman ML Change

                                            writer.Fields.Add("CODE_DESC", sCodeDesc.Replace("'", "''"));

                                            writer.Execute(objTrans);
                                            writer.Reset(true);

                                            //objCmd.CommandText = "UPDATE CODES_TEXT SET CODE_DESC ='" +
                                            //    sCodeDesc.Replace("'", "''") + "' WHERE CODE_ID = " + iCodeId +
                                            //    " AND LANGUAGE_CODE=" + m_sLangCode;
                                            //objCmd.ExecuteNonQuery();

                                            //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
										}
									}
								}
								m_sMessage = Globalization.GetString("UtilityDriver.ImportFile.successful", m_iClientId);
							}
							else
							{
								m_bFileOk = false;
								m_sMessage  =  Globalization.GetString("UtilityDriver.FileNotOk", m_iClientId);
							}
					}
				}
				else
				{
					m_bFileOk = false;
					m_sMessage  =  Globalization.GetString("UtilityDriver.FileNotFound", m_iClientId);
				}
				FileInfo sFile = new FileInfo(p_sFileName);
				sFile.Delete();

                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                StringBuilder strSQLQuery = new StringBuilder();
                strSQLQuery.Append(string.Format("UPDATE GLOSSARY SET DTTM_LAST_UPDATE = {0} ","~DTTM_LAST_UPDATE~"));
                strSQLQuery.Append(" WHERE SYSTEM_TABLE_NAME = 'CODES'");
                objCmd.CommandText = FormatSQLString(strSQLQuery.ToString());

                Dictionary<string, object> dictQParams = new Dictionary<string, object>();
                dictQParams.Add("DTTM_LAST_UPDATE", Conversion.ToDbDateTime(DateTime.Now).ToString());

                DbFactory.ExecuteNonQuery(objCmd, dictQParams);

                //objCmd.CommandText = String.Format("UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '{0}' " +
                //    "WHERE SYSTEM_TABLE_NAME = 'CODES'", Conversion.ToDbDateTime(DateTime.Now));
                //objCmd.ExecuteNonQuery();

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				objTrans.Commit() ;
				// Glossary Update moved inside transaction context above
				//UpdateGlossaryDTTM("CODES");
				AppendXMLMessage(p_objXmlDoc,1);
			}
			catch(RMAppException p_objRmX)
			{
				if(objTrans!=null)
					objTrans.Rollback(); 
				AppendXMLMessage(p_objXmlDoc,2);
				throw p_objRmX;
			}
			catch(Exception p_objEx)
			{
				if(objTrans!=null)
					objTrans.Rollback(); 
				AppendXMLMessage(p_objXmlDoc,2);
				throw new RMAppException(Globalization.GetString("UtilityDriver.ImportFile.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objTrans != null)
				{
					objTrans.Dispose();					
					objTrans = null;
				}
				if(objConn != null)	
				{
					objConn.Close(); 
					objConn.Dispose();
				}
				if(objCmd!=null)
				{
					objCmd = null; 
				}
				if(objStreamReader!=null)
				{
					objStreamReader.Close();					
					objStreamReader.Dispose();
				}
                if (objCach != null)			
				    objCach.Dispose();
                if (dictParams != null)
                {
                    dictParams = null;
                }
			}
			return p_objXmlDoc;
		}

		/// Name		: CheckFile
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Check for the file format
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input Doc</param>
		/// <param name="p_sFileName">File Name</param>
		/// <param name="p_iCodeType">Type Code</param>
		/// <returns>Xml Document</returns>
		private XmlDocument CheckFile(XmlDocument p_objXmlDoc, string p_sFileName, int p_iCodeType)
		{
			StreamReader objRdr = null;
			String[] sColumn = null; 
			char[] delimit = ",".ToCharArray();
			try
			{
				if (File.Exists(p_sFileName))
				{
					m_sFileName = p_sFileName;
					// Create an instance of StreamReader to read from a file.
					// The using statement also closes the StreamReader.
					using (objRdr = new StreamReader(m_sFileName)) 
					{
						string sLine = "";
						sLine = objRdr.ReadLine(); 
						if (sLine != "") 
							if ((sLine.Length >= 7) && (sLine.ToUpper().Substring(0,7) == "[CODES]"))
							{
								m_bFileOk =true;
								//Get code description
								m_sCodeDesc =  objRdr.ReadLine().Trim();
								sLine =  objRdr.ReadLine().Trim();
								sColumn = sLine.Split(delimit);
								//Get Table details
								if(sColumn.Length > 0)
								{
									m_sTableName = sColumn[0].Trim();
									if(sColumn.Length > 1)
									{
										m_sSysTableName = sColumn[1].Trim().ToUpper().Replace(" ","_");
									
										if(sColumn.Length > 3)
										{
											m_sCodeType = sColumn[3].Trim();
											if(sColumn.Length > 4)
											{
												m_sLangCode = sColumn[4].Trim();
											}
										}
									}
								}
								while((sLine = objRdr.ReadLine())!= null)
								{
									m_iCount ++;
								}
								m_sMessage = Globalization.GetString("UtilityDriver.CheckFile.Ask", m_iClientId);
							}
							else
							{
								m_bFileOk = false;
								m_sMessage =  Globalization.GetString("UtilityDriver.FileNotOk", m_iClientId);
							}
					}
				}
				else
				{
					m_bFileOk = false;
					m_sMessage  =  Globalization.GetString("UtilityDriver.FileNotFound", m_iClientId);
				}
				AppendXMLMessage(p_objXmlDoc,0); 
			}
			catch(RMAppException p_objRmX)
			{
				throw p_objRmX;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.CheckFile.Err", m_iClientId), p_objEx);
			}
			finally
			{
                if(objRdr != null)
				    objRdr.Dispose();
			}
			return p_objXmlDoc;
		}

		/// Name		: AppendXMLMessage
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Appends Xml Message to be returned
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input Document element</param>
		/// <param name="p_iMode">Mode</param>
		private void AppendXMLMessage(XmlDocument p_objXmlDoc, int p_iMode)
		{
			XmlElement objXmlElm = null;
			XmlNodeList objNodLst= null;

			try
			{
				objXmlElm = (XmlElement) p_objXmlDoc.SelectSingleNode("//msg");

				switch(p_iMode)
				{
					case 0:
						if (!m_bFileOk )
						{
							objXmlElm.SetAttribute("level", "0");
							objXmlElm.SetAttribute("desc", m_sMessage);
						}
						else
						{
							//Populate Details
							objXmlElm.SetAttribute("level", "1");
							objXmlElm.SetAttribute("desc", m_sMessage);
							objXmlElm.SetAttribute("filename", m_sFileName);
							objXmlElm.SetAttribute("isedit", m_bEdit.ToString());
							objNodLst = p_objXmlDoc.SelectNodes("import/msg/info");
							foreach(XmlElement objElm in objNodLst)
							{
								switch(objElm.GetAttribute("id").ToString())
								{
									case "1":
										objElm.SetAttribute("value",m_sSysTableName);  
										break;
									case "2":
										objElm.SetAttribute("value",m_sTableName);
										break;
									case "3":
										objElm.SetAttribute("value",m_iCount.ToString());
										break;
									case "4":
										objElm.SetAttribute("value",m_sCodeDesc);
										break;
									case "6":
										objElm.SetAttribute("value",m_sCodeType);
										break;
								}
							}
						} 
						break;
					case 1:
						objXmlElm.SetAttribute("level", "2");
						objXmlElm.SetAttribute("desc", m_sMessage);
						objNodLst= p_objXmlDoc.SelectNodes("import/msg/info");
						foreach(XmlElement objElm in objNodLst )
						{
							if (objElm.GetAttribute("id")== "5")
								objElm.SetAttribute("value",m_iTableId.ToString()); 
						}
						break;
					case 2:
						objXmlElm.SetAttribute("level", "3");
						objXmlElm.SetAttribute("desc",
							Globalization.GetString("UtilityDriver.AppendXMLMessage.ImportErr", m_iClientId));
						break;
				}
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(
					Globalization.GetString("UtilityDriver.AppendXMLMessage.XmlErr", m_iClientId), p_objEx);
			}
			finally
			{
				objXmlElm = null;
				objNodLst= null;
			}
		}

		/// Name		: AppendXmlError
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Appends the Errors to the Xml
		/// </summary>
		/// <param name="p_XmlDoc">XmlDocument</param>
		/// <param name="p_sErrMsg">Error Message</param>
		/// <returns>Xml Document containing error details</returns>
		private XmlDocument AppendXmlError(XmlDocument p_objXmlDoc, string p_sErrMsg)
		{
			XmlNodeList objNodLst= p_objXmlDoc.GetElementsByTagName("error");
			foreach(XmlElement objElm in objNodLst)
			{
				//put error message
				objElm.SetAttribute("desc", p_sErrMsg);
				objElm.SetAttribute("value", "1");
			}

			objNodLst= p_objXmlDoc.GetElementsByTagName("button");
			foreach(XmlElement objElm in objNodLst)
			{
				objElm.SetAttribute("disable", "1");
			}
            objNodLst = null;
			return p_objXmlDoc;
		}

		/// Name		: UpdateGlossaryDTTM
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
        /// <summary>
        /// Updates Date and Time for Glossary
        /// </summary>
        /// <param name="p_sTableName">Table Name</param>
		private void UpdateGlossaryDTTM(string p_sTableName)
		{
			DbConnection objCon = null;
			try
			{ 
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                DbWriter writer = DbFactory.GetDbWriter(this.m_sConnectString);
                writer.Tables.Add("GLOSSARY");
                writer.Where.Add(String.Format("SYSTEM_TABLE_NAME={0}", p_sTableName.ToUpper()));

                writer.Fields.Add("DTTM_LAST_UPDATE", Conversion.ToDbDateTime(DateTime.Now).ToString());

                writer.Execute();

                //objCon = DbFactory.GetDbConnection(m_sConnectString);
                //objCon.Open();
                ////Update Glossary for Last Date & Time Modified
                //objCon.ExecuteNonQuery(String.Format("UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '{0}' " +
                //    "WHERE SYSTEM_TABLE_NAME = '{1}'", Conversion.ToDbDateTime(DateTime.Now),
                //    p_sTableName.ToUpper()));

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.UpdateGlossaryDTTM.DataErr", m_iClientId),
					p_objEx);
			}
			finally
			{
				if(objCon!=null)objCon.Dispose(); 
			}
		}

		//Changed by Gagan for MITS 8599 : Start

		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_bIsLandscape">Landscape or Portrait</param>
		/// <param name="p_Left">Left</param>
		/// <param name="p_Right">Right</param>
		/// <param name="p_Top">Top</param>
		/// <param name="p_Bottom">Bottom</param>
		private void StartDoc(bool p_bIsLandscape, int p_Left, int p_Right, int p_Top, int p_Bottom)
		{		
			try
			{
				m_objPrintDoc = new C1PrintDocument();
				m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
				// m_objPrintDoc.PageSettings.PrinterSettings.PrinterName = RMConfigurator.Value("DefaultPrinterName").ToString();

				m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape ;
				m_objPrintDoc.PageSettings.Margins.Left = p_Left;
				m_objPrintDoc.PageSettings.Margins.Right = p_Right;
				m_objPrintDoc.PageSettings.Margins.Top = p_Top;
				m_objPrintDoc.PageSettings.Margins.Bottom = p_Bottom;

				m_objPrintDoc.StartDoc();

				// Causes access to default printer     objRect = m_objPrintDoc.PageSettings.Bounds;
				m_dblPageHeight = 11 * 1440;
				m_dblPageWidth = 8.5 * 1440;
				if(p_bIsLandscape)
					m_dblPageHeight += 700 ;
				else
					m_dblPageWidth += 700 ;
			
				m_objFont = new Font("Arial", 10);		
				m_objText = new RenderText(m_objPrintDoc);			
				m_objText.Style.Font = m_objFont ;
				m_objText.Style.WordWrap = false ;
				m_objText.Width = m_dblPageWidth ;				
			}
			catch( RMAppException p_objEx )
			{								
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.StartDoc.Error", m_iClientId) , p_objEx );				
			}				
		}

		/// <summary>
		/// Set font bold
		/// </summary>
		/// <param name="p_bBold">Bool flag</param>
		private void SetFontBold( bool p_bBold )
		{		
			try
			{
				if( p_bBold )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold );
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font = m_objFont ;

			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.SetFontBold.Error", m_iClientId) , p_objEx );				
			}
 
		}

		/// <summary>
		/// Get text width.
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Width</returns>
		private double GetTextWidth( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundWidth );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetTextWidth.Error", m_iClientId) , p_objEx );				
			}
		}

		/// <summary>
		/// Get Text Height
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Height</returns>
		private double GetTextHeight( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundHeight );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetTextHeight.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Creates a new page in a report
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>        
		/// <param name="p_bFirstPage">First page or not</param>		
        /// <param name="p_lTableID">TableID</param>		
        private void CreateNewPage(string p_sDSN, bool p_bFirstPage, long p_lTableID)
		{
			SysParms objSysSetting = null;
            //Changed by Gagan for MITS 9820 : Start
            string sTableName = string.Empty;
            DbReader objReader = null;
            string sSql = string.Empty;
            //Changed by Gagan for MITS 9820 : End		
			RenderText m_objText = null;
			try
			{
				if (!p_bFirstPage)
				{
					m_objPrintDoc.NewPage();
				}
				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 11700);

				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 500, Color.Black, 1, Color.LightGray);
				m_objPrintDoc.RenderDirectRectangle(0, 11300, 15600, 11900, Color.Black, 1, Color.LightGray);
				m_objText = new RenderText(m_objPrintDoc);	
				m_objText.Text = " ";
				CurrentX = m_objText.BoundWidth;


                objSysSetting = new SysParms(p_sDSN, m_iClientId);
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                StringBuilder strSQL = new StringBuilder();
                strSQL.Append("SELECT TABLE_NAME FROM GLOSSARY_TEXT ");
                strSQL.Append(string.Format(" WHERE TABLE_ID = {0} ", "~TABLE_ID~"));

                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                dictParams.Add("TABLE_ID", p_lTableID);

                objReader = DbFactory.ExecuteReader(m_sConnectString, FormatSQLString(strSQL.ToString()), dictParams);

                ////Changed by Gagan for MITS 9820 : Start
                //sSql = "SELECT TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID = " + p_lTableID.ToString();
                //objReader = DbFactory.GetDbReader(m_sConnectString, sSql);

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                if (objReader.Read())
                {
                    sTableName = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME").ToString());
                    objReader.Close();
                }

				//m_objText.Text = " Organizational Hierarchy"  + "                                                    " + objSysSetting.SysSettings.ClientName +
				//	"                                                    " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "      ";
                m_objText.Text = "    " + sTableName  + "                                                            " + objSysSetting.SysSettings.ClientName +
                	"                                                    " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "      ";
                //Changed by Gagan for MITS 9820 : End
				objSysSetting = null;
				CurrentX = 0; //(ClientWidth - m_objText.BoundWidth);

				m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left);
				m_objText = new RenderText(m_objPrintDoc);
                //Bharani - MITS : 36473 - Change 2/3 - Start
				//m_objText.Text = Globalization.GetString("UtilityDriver.CreateReport.Page") + " [@@PageNo@@]";
                m_objText.Text = Regex.Replace(Globalization.GetString("UtilityDriver.CreateReport.Page", m_iClientId), @"[^a-zA-Z]+", String.Empty) + " [@@PageNo@@]";
                //Bharani - MITS : 36473 - Change 2/3 - End
				CurrentX = (PageWidth - m_objText.BoundWidth)/2;
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center);

                //pmittal5 MITS 12685  08/05/08
				//m_objText.Text = Globalization.GetString("OrgHierarchy.CreateOrgHierarchyReport.ConfidentialData") + " - " + RMConfigurator.Text("message", "id", "DEF_RMCAPTION") + "  ";

                //m_objText.Text = Globalization.GetString("UtilityDriver.CreateReport.ConfidentialData") + " - " + RMConfigurator.Text("message", "id", "DEF_RMCAPTION") + "  ";
                //Bharani - MITS : 36473 - Change 3/3 - Start
                //m_objText.Text = String.Format("{0} - {1}  ", Globalization.GetString("UtilityDriver.CreateReport.ConfidentialData"), m_MessageSettings["DEF_RMCAPTION"]);
                m_objText.Text = Regex.Replace(String.Format("{0} - {1}  ", Globalization.GetString("UtilityDriver.CreateReport.ConfidentialData", m_iClientId), m_MessageSettings["DEF_RMCAPTION"]), @"[^a-zA-Z\s\-]+", String.Empty);
                //Bharani - MITS : 36473 - Change 3/3 - End

				CurrentX = (ClientWidth - m_objText.BoundWidth);
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold|FontStyle.Italic), Color.Black, AlignHorzEnum.Right);
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("OrgHierarchy.CreateReport.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				objSysSetting = null;
                if(m_objText != null)
				    m_objText.Dispose();
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
			}
		}

		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintText( string p_sPrintText )
		{
			try
			{
				m_objText.Text = p_sPrintText + "\r" ;	
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect( m_dblCurrentX , m_dblCurrentY , m_objText );			
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.PrintText.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintTextNoCr(string p_sPrintText)
		{
			try
			{
				m_objText.Text = p_sPrintText + "";			
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);						
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.PrintTextNoCr.Error", m_iClientId), p_objEx);				
			}
		}

        //MGaba2-MITS 11952-04/03/2008

        /// Name		: PrintMultiLineText
        /// Author		: Mona Gaba
        /// Date Created: 04/03/2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Earlier when length of data in a one column> column width,it used to overwrite the data in next column
        ///now,in such scenario,it will be printed in multiple lines. We are keeping record of no of lines in iCount 
        ///which is returned by this function, in order to get the idea at which row,next record should start.In case 
        ///a word is continued in the next line,a hyphen is appended to it.This function is printing character by 
        ///character and once the length of already printed text in that cell exceeds the column width,it is jumped 
        ///to next line
        /// </summary>
        /// <param name="p_sPrintText">Data to be printed in the cell</param>
        /// <param name="p_colLength">Width of Column</param>
        /// <param name="p_colLength">Height of character in points</param>
        /// <returns>Number of rows covered by data in that cell</returns>
        private int PrintMultiLineText(string p_sPrintText, double p_colLength, double p_charHeight)
        {
            try
            {
                int iCount = 0;
                double dWidthAlreadPrinted = 0;

                for (int i = 0; i < p_sPrintText.Length; i++)
                {
                    if ((dWidthAlreadPrinted + GetTextWidth(p_sPrintText.Substring(i, 1))) > (p_colLength - GetTextWidth("M")))
                    {
                        iCount += 1;
                        dWidthAlreadPrinted = 0;        
                        if( (p_sPrintText.Substring(i, 1) != " ") && (p_sPrintText.Substring(i-1, 1) != " ")) 
                            {

                                m_objText.Text = "-";
                                m_objText.AutoWidth = true;
                                m_objPrintDoc.RenderDirect(m_dblCurrentX + dWidthAlreadPrinted, m_dblCurrentY + (iCount * p_charHeight), m_objText);
                                dWidthAlreadPrinted += GetTextWidth("-");
                            }
                    }
                    m_objText.Text = p_sPrintText.Substring(i, 1) + "";
                    m_objText.AutoWidth = true;
                    m_objPrintDoc.RenderDirect(m_dblCurrentX + dWidthAlreadPrinted, m_dblCurrentY + (iCount * p_charHeight), m_objText);
                    dWidthAlreadPrinted += GetTextWidth(p_sPrintText.Substring(i, 1));
                }

                return iCount + 1;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.PrintMultiLineText.Error", m_iClientId), p_objEx);
            }
        }

		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		private void Save(string p_sPath)
		{
			try
			{
				m_objPrintDoc.ExportToPDF(p_sPath ,false );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.Save.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Creates a record in a report
		/// </summary>
		/// <param name="objCod">Code object containing a code data</param>
		/// <param name="p_dblCurrentX">Current X position</param>
		/// <param name="p_dblCurrentY">Current Y position</param>
		/// <param name="p_arrColAlign">Columns alignment</param>
		/// <param name="p_dblLeftMargin">Left margin</param>
		/// <param name="p_dblLineHeight">Line height</param>
		/// <param name="p_arrColWidth">Column width</param>
		/// <param name="p_dblCharWidth">Character width</param>
		/// <param name="p_bLeadingLine">Leading Line Flag</param>
		/// <param name="p_bCalTotal">Calculate Total Flag</param>
		/// <param name="p_dblTotalAll">Over all Total</param>
		/// <param name="p_dblTotalPay">Total Payments</param>
		/// <param name="p_dblTotalCollect">Total Collections</param>
		/// <param name="p_dblTotalVoid">Total Voids</param>
		private void CreateRow(Code objCod, double p_dblCurrentX,
			ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
			double[] p_arrColWidth, double p_dblCharWidth)
		{
			string sData = "";
			string sTemp = "";

			try
			{
				CurrentX = p_dblCurrentX;
				CurrentY = p_dblCurrentY;

                //MGaba2-MITS 11952-04/03/2008
                int iNoOfRowsCovByRecord = 1; //keeping record of no of lines, covered by that record
				for( int iIndexJ =0 ; iIndexJ < 6 ; iIndexJ++ )
				{
					sData = "";
					switch( iIndexJ )
					{						
						case 0 :
							if (objCod.ShortCode!="")
							{
								if(objCod.LineOfBusCode != 0) 
									sData = objCod.ShortCode + "-" + objCod.LobShortCode;
								else
									sData = objCod.ShortCode;
							}														
							break;
						case 1:
							if(objCod.CodeDesc!="")
								sData = objCod.CodeDesc;
							break;
						case 2:
							if (objCod.RelCodeDesc!="" && objCod.RelShortCode!="")
								sData = objCod.RelShortCode + " " + objCod.RelCodeDesc;
							else if(objCod.RelShortCode!="" )
								sData = objCod.RelShortCode;
							break;
						case 3:
							if (objCod.StartDate!="")
							{
								sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.StartDate),"MM/dd/yyyy");
								sData = sTemp;								
							}
							break;
						case 4:
							if (objCod.EndDate!="")
							{
								sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.EndDate),"MM/dd/yyyy");
								sData = sTemp;					
							}
							break;
						case 5:
							if (objCod.OrgAbbreviation=="")
								sData = objCod.Organisation;
							else						
								sData = objCod.OrgAbbreviation + "-" + objCod.Organisation;
							break;
					}					

					//while( GetTextWidth( sData ) > p_arrColWidth[iIndexJ] )
					//	sData = sData.Substring( 0 , sData.Length - 1 );

					// Print using alignment
					if( p_arrColAlign[iIndexJ] == LEFT_ALIGN )
					{
						CurrentX = p_dblCurrentX ;
                        /* MGaba2-MITS 11952-04/03/2008-Start
                         * In case length of data in a column is greater than column's width,new function is called
                         * to print the data on next line
                           If no of lines covered by a cell is greater than no of lines covered by that record till 
                           yet,then no of lines covered by that record gets increased */


                        int iNoOfRowsCovByCell = 1; //keeping record of no of lines, covered by that cell
                        if ((GetTextWidth(sData) > (p_arrColWidth[iIndexJ]-GetTextWidth("M"))) && sData != "")
						{
                            iNoOfRowsCovByCell = PrintMultiLineText(sData, p_arrColWidth[iIndexJ], p_dblLineHeight);
                             if (iNoOfRowsCovByRecord < iNoOfRowsCovByCell)
                             {
                                 iNoOfRowsCovByRecord = iNoOfRowsCovByCell;
                             }
                        }
                        else
                        {
                            PrintTextNoCr( " " + sData );
                        }
                        //MGaba2-MITS 11952-04/03/2008-End
					}
					else
					{
						CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ] 
							- GetTextWidth( sData) - p_dblCharWidth ;
						PrintTextNoCr( sData );
					}
					p_dblCurrentX += p_arrColWidth[iIndexJ] ;
				}
				p_dblCurrentX = p_dblLeftMargin ;
                //MGaba2-MITS 11952-04/03/2008-in case data in a cell in a record is covered on multiple lines
				//p_dblCurrentY += p_dblLineHeight;						
                p_dblCurrentY += iNoOfRowsCovByRecord * p_dblLineHeight;						
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.CreateRow.Error", m_iClientId),p_objException);
			}
		}

		/// <summary>
		/// Call the end event.
		/// </summary>
		private void EndDoc()
		{
			try
			{
				m_objPrintDoc.EndDoc();
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.EndDoc.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Creates Report Footer
		/// </summary>
		/// <param name="p_dCurrentX">X Position</param>
		/// <param name="p_dCurrentY">Y Position</param>
		/// <param name="p_dHeight">Height</param>
		/// <param name="p_dblSpace">Space between 2 words</param>
		private void CreateFooter(double p_dCurrentX, double p_dCurrentY, double p_dHeight, double p_dblSpace)
		{
			try
			{
				m_objText.Style.Font=new Font("Arial", 7f, FontStyle.Underline|FontStyle.Bold);
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_NOTE");
                m_objText.Text = m_MessageSettings["DEF_NOTE"].ToString();
				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objPrintDoc.RenderDirect(p_dCurrentX ,p_dCurrentY ,m_objText);

				m_objText.Style.Font = new Font("Arial", 7f, FontStyle.Bold);
				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMPROPRIETRY");
                m_objText.Text = m_MessageSettings["DEF_RMPROPRIETARY"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_NAME");
                m_objText.Text = m_MessageSettings["DEF_NAME"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMCOPYRIGHT");
                m_objText.Text = m_MessageSettings["DEF_RMCOPYRIGHT"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMRIGHTSRESERVED");
                m_objText.Text = m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.CreateFooter.Error", m_iClientId), p_objEx);				
			}
		}

		//Changed by Gagan for MITS 8599 : End

		/// Name		: GetCodesXML
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded Method
		/// </summary>
		/// <param name="p_sXML">Input Xml String</param>
		/// <param name="p_sTypeCode">Type Code</param>
		/// <returns>String Xml for Codes</returns>
		public XmlDocument GetCodesXML(string p_sXML, string p_sTypeCode)
		{
			return GetCodesXML(0,p_sXML,p_sTypeCode,new char() ); 
		}

		/// Name		: GetCodesXML
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded Method
		/// </summary>
		/// <param name="p_iTable">Table Id</param>
		/// <param name="p_sXML">Input Xml String</param>
		/// <returns>String Xml for Codes</returns>
		public XmlDocument GetCodesXML( int p_iTable, string p_sXML)
		{
			return GetCodesXML(p_iTable,p_sXML,"",new char()); 
		}

		/// Name		: GetCodesXML
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded method
		/// </summary>
		/// <param name="p_sXML">Input Xml string</param>
		/// <param name="p_sTypeCode">Type Code</param>
		/// <param name="p_cAlpha">Alphabet</param>
		/// <returns>String xml for codes</returns>
		public XmlDocument GetCodesXML(string p_sXML,string p_sTypeCode, char p_cAlpha)
		{
			return GetCodesXML(0 ,p_sXML,p_sTypeCode,p_cAlpha); 
		}

		/// Name		: GetCodesXML
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the xml representation of Codes data
		/// </summary>
		/// <param name="p_iTable">Table Id</param>
		/// <param name="p_sXML">Input Xml String</param>
		/// <param name="p_sTypeCode">Code Type</param>
		/// <param name="p_cAlpha">Alphabet</param>
		/// <returns>String representation of Codes</returns>
		public XmlDocument GetCodesXML( int p_iTable, string p_sXML, string p_sTypeCode, char p_cAlpha)
		{
			long lTable = 0;
			string sField ="";
			string sTemp = "";
			XmlDocument objXmlDoc=null;
			CodeList objCodLst = null;
			XmlNodeList objNodLst = null;			
			LocalCache objCache = null;
            //aanandpraka2:Start changes
            int totalCount = 0;
            int totalPages = 0;
            int p_CurrPageIndex = 0;
            string pageNum = string.Empty; 
            int pageSize = 0;
            string sTableName = "";//7810
            //aanandpraka2:End changes
            bool bCodeListView = true;   //pmittal5 MITS:12685 07/28/08
			try
			{
				//Load the Xml document
				objXmlDoc = new XmlDocument(); 
				objXmlDoc.LoadXml(p_sXML); 
			}
			catch(XmlException p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.XmlErr", m_iClientId), p_objEx);
			}

			try
			{
				if (p_iTable ==0 && p_sTypeCode!="")
				{
					objCache = new LocalCache(m_sConnectString,m_iClientId);
					//Get the First table of Types
					lTable = GetFirstTableOfTypes(objCache.GetCodeId(p_sTypeCode,"GLOSSARY_TYPES"),p_cAlpha);
				}
				else
					lTable = p_iTable;				

				//No Table found error
				if (lTable==0)
					return AppendXmlError(objXmlDoc,Globalization.GetString("UtilityDriver.GetCodesXML.NoTable",m_iClientId));

                //pmittal5 MITS:12685 07/28/08
                bCodeListView = IsCodeListView((int)lTable);

				objNodLst= objXmlDoc.GetElementsByTagName("button");
				foreach(XmlElement objElm in objNodLst)
				{
					sField= objElm.GetAttribute("name");
					if (sField.ToUpper().Trim()=="BTNADD")
						if(lTable>0)
							objElm.SetAttribute ("param", lTable.ToString());
				}
                
                //pmittal5 MITS:12685 07/28/08
                if (bCodeListView == false)
                {
                    GetCodesTreeXML(ref objXmlDoc, (int)lTable);
                }
                else if (bCodeListView == true)
                {//End - pmittal5
                    //Get code list
                    //aanandpraka2:Start changes for MITS 22728
                    XmlElement xCurrPageIndex = (XmlElement)objXmlDoc.SelectSingleNode("//group/control");
                    if (xCurrPageIndex != null)
                    {
                        pageNum = xCurrPageIndex.GetAttribute("pagenumber").ToString();
                        objCodLst = GetCodes((int)lTable, Convert.ToInt32(pageNum), ref totalCount, ref totalPages, ref pageSize);
                    }
                    else
                    {
                        objCodLst = GetCodes((int)lTable);
                    }
                    //aanandpraka2:End changes for MITS 22728
                    objNodLst = objXmlDoc.GetElementsByTagName("control");

                    foreach (XmlElement objElm in objNodLst)
                    {
                        sField = objElm.GetAttribute("name");
                        if (sField.ToUpper().Trim() == "CODELIST" && p_sTypeCode != "" && lTable == 0)
                            objElm.SetAttribute("ctname", p_sTypeCode);

                        foreach (Code objCod in objCodLst)
                        {
                            XmlElement objNewElm = objXmlDoc.CreateElement("option");
                            objNewElm.SetAttribute("id", objCod.CodeId.ToString());
                            if (objCod.ShortCode != "")
                            {
                                if (objCod.LineOfBusCode != 0)
                                    objNewElm.SetAttribute("code", objCod.ShortCode + "-" + objCod.LobShortCode);
                                else
                                    objNewElm.SetAttribute("code", objCod.ShortCode);
                            }
                            if (objCod.CodeDesc != "")
                                objNewElm.SetAttribute("Description", objCod.CodeDesc);

                            if (objCod.RelCodeDesc != "" && objCod.RelShortCode != "")
                                objNewElm.SetAttribute("Parent", objCod.RelShortCode + " " + objCod.RelCodeDesc);
                            else if (objCod.RelShortCode != "")
                                objNewElm.SetAttribute("Parent", objCod.RelShortCode);

                            if (objCod.StartDate != "")
                            {
                                sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.StartDate), "MM/dd/yyyy");
                                objNewElm.SetAttribute("Start-Date", sTemp);
                                sTemp = "";
                            }
                            if (objCod.EndDate != "")
                            {
                                sTemp = Conversion.GetDBDateFormat(Conversion.GetDate(objCod.EndDate), "MM/dd/yyyy");
                                objNewElm.SetAttribute("End-Date", sTemp);
                                sTemp = "";
                            }
                            if (objCod.OrgAbbreviation == "")
                                objNewElm.SetAttribute("Organization", objCod.Organisation);
                            else
                                objNewElm.SetAttribute("Organization", objCod.OrgAbbreviation + "-" +
                                    objCod.Organisation);
                            objElm.AppendChild(objNewElm);
                            objNewElm = null;
                        }
                    }
                }
				//return objXmlDoc.OuterXml;
                //pmittal5 MITS 12685 08/07/08
                //aanandpraka2:Start changes for MITS 22728
                XmlElement objGroupElem = (XmlElement)objXmlDoc.SelectSingleNode("//group");
                if (objGroupElem != null)
                {
                    XmlElement objTableElem = objXmlDoc.CreateElement("Table");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(lTable);
                    objGroupElem.AppendChild(objTableElem);
                    //JIRA 7810 start snehal
                    objCache = new LocalCache(m_sConnectString, m_iClientId);
                    sTableName = objCache.GetTableName(Convert.ToInt32(lTable));
                    objTableElem = objXmlDoc.CreateElement("TableName");
                    objTableElem.InnerText = sTableName;
                    objGroupElem.AppendChild(objTableElem);
                    //JIRA 7810 end

                    objTableElem = objXmlDoc.CreateElement("TotalRecordCount");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(totalCount);
                    objGroupElem.AppendChild(objTableElem);

                    objTableElem = objXmlDoc.CreateElement("TotalPages");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(totalPages);
                    objGroupElem.AppendChild(objTableElem);

                    objTableElem = objXmlDoc.CreateElement("CurrentPageNumber");
                    objTableElem.InnerText = pageNum;
                    objGroupElem.AppendChild(objTableElem);

                    objTableElem = objXmlDoc.CreateElement("PageSize");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(pageSize);
                    objGroupElem.AppendChild(objTableElem);
                }

                //End - pmittal5
				return objXmlDoc;
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetCodesXML.DataErr", m_iClientId), p_objEx);
			}
			finally
			{
				objCodLst = null;
				objNodLst = null;
                if (objCache!= null)
			    	objCache.Dispose();
				objXmlDoc = null;
			}			
		}
        /// Name		: GetNonBaseCodesXML
        /// Author		: Amandeep Kaur
        /// Date Created: 11/30/2012	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Overloaded Method
        /// </summary>
        /// <param name="p_iCodeID">Code Id</param>
        /// <param name="p_sXML">Input Xml String</param>
        /// <returns>String Xml for Codes</returns>
        public XmlDocument GetNonBaseCodesXML(int p_iCodeID,string p_sXML)
        {
            long lTable = 0;
            string sField = "";
            string sTemp = "";
            XmlDocument objXmlDoc = null;
            CodeList objCodLst = null;
            XmlNodeList objNodLst = null;
            LocalCache objCache = null;
            //aanandpraka2:Start changes
            int totalCount = 0;
            int totalPages = 0;
            int p_CurrPageIndex = 0;
            string pageNum = string.Empty;
            int pageSize = 0;            
            bool bCodeListView = true;   
            try
            {
                //Load the Xml document
                objXmlDoc = new XmlDocument();
                objXmlDoc.LoadXml(p_sXML);
            }
            catch (XmlException p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.XmlErr", m_iClientId), p_objEx);
            }

            try
            {
                if (bCodeListView == true)
                {
                    XmlElement xCurrPageIndex = (XmlElement)objXmlDoc.SelectSingleNode("//group/control");
                    if (xCurrPageIndex != null)
                    {
                        pageNum = xCurrPageIndex.GetAttribute("pagenumber").ToString();
                        objCodLst = GetCodes(this.CodeID, true, Convert.ToInt32(pageNum), ref totalCount, ref totalPages, ref pageSize);
                    }                   
                    objNodLst = objXmlDoc.GetElementsByTagName("control");

                    foreach (XmlElement objElm in objNodLst)
                    {                        
                        foreach (CodeDesc objCod in objCodLst)
                        {
                            if (objCod.LangCode != Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]))
                            {
                                XmlElement objNewElm = objXmlDoc.CreateElement("option");
                                objNewElm.SetAttribute("id", objCod.CodeId.ToString());
                                if (objCod.ShortCode != "")
                                    objNewElm.SetAttribute("code", objCod.ShortCode);

                                if (objCod.CodesDesc != "")
                                    objNewElm.SetAttribute("Description", objCod.CodesDesc);

                                if (objCod.LangCode != 0)
                                {
                                    objNewElm.SetAttribute("LanguageCode", (objCod.LangCode).ToString());
                                    objNewElm.SetAttribute("Language", GetLanguageDescription(objCod.LangCode));
                                }
                                objElm.AppendChild(objNewElm);
                                objNewElm = null;
                            }                                                       
                        }
                    }
                }              
                XmlElement objGroupElem = (XmlElement)objXmlDoc.SelectSingleNode("//group");
                if (objGroupElem != null)
                {
                    XmlElement objTableElem = objXmlDoc.CreateElement("Table");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(lTable);
                    objGroupElem.AppendChild(objTableElem);


                    objTableElem = objXmlDoc.CreateElement("TotalRecordCount");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(totalCount);
                    objGroupElem.AppendChild(objTableElem);

                    objTableElem = objXmlDoc.CreateElement("TotalPages");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(totalPages);
                    objGroupElem.AppendChild(objTableElem);

                    objTableElem = objXmlDoc.CreateElement("CurrentPageNumber");
                    objTableElem.InnerText = pageNum;
                    objGroupElem.AppendChild(objTableElem);

                    objTableElem = objXmlDoc.CreateElement("PageSize");
                    objTableElem.InnerText = Common.Conversion.ConvertObjToStr(pageSize);
                    objGroupElem.AppendChild(objTableElem);
                }
              
                return objXmlDoc;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.GetCodesXML.DataErr", m_iClientId), p_objEx);
            }
            finally
            {
                objCodLst = null;
                objNodLst = null;
                if (objCache != null)
                    objCache.Dispose();
                objXmlDoc = null;
            }
        }
		//Changed by Gagan for MITS 8599 : Start

        /// Name		: LoadLanguages
        /// Author		: Amandeep Kaur
        /// Date Created: 11/26/2012	
        /// <summary>
        public string GetLanguageDescription(int p_iLangCode)
        {
            string sLangName = string.Empty;
            string sSQL = "";
            DbConnection objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            DbReader objReader = null;

            try
            {
                objConn.Open();
                sSQL = "SELECT * FROM SYS_LANGUAGES WHERE LANG_ID = " + p_iLangCode;
                objReader = objConn.ExecuteReader(sSQL);

                if (objReader.Read())
                {
                    sLangName = objReader.GetString("LANGUAGE_NAME");
                }
                return sLangName;
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("RMWinSecurity.LoadLanguages.LoadLanguagesError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
            return sLangName;
        }

		/// <summary>
		/// Creates Code List Report
		/// </summary>
        ///// <param name="p_iTable">Table ID</param>
        ///// <param name="p_sTypeCode">Code Type</param>
        ///// <param name="p_cAlpha">Alphabet</param>

        /// <param name="objXmlDoc">Input XML</param> 
		/// <param name="p_objMemory">Memory Object</param>
		/// <returns></returns>

        //pmittal5 MITS 12685 08/08/08 
        //public int CreateCodeListReport(int p_iTable, string p_sTypeCode, 
        //    char p_cAlpha, out MemoryStream p_objMemory)
        public int CreateCodeListReport(XmlDocument objXmlDoc, out MemoryStream p_objMemory)
		{
			double dblCharWidth = 0.0;
			double dblLineHeight = 0.0;
			double dblLeftMargin = 10.0;
			double dblRightMargin = 10.0;
			double dblTopMargin = 10.0;
			double dblPageWidth = 0.0;
			double dblPageHeight = 0.0;
			double dblBottomHeader = 0.0;
			double dblCurrentY = 0.0;
			double dblCurrentX = 0.0;	
			double iCounter = 0.0;
			double[] arrColWidth = null;
			int iReturnValue = 0;

			string sHeader = string.Empty;
			string sFile = string.Empty;
			string sSQL = string.Empty;
			string sNumChecks = string.Empty;
			string[] arrColAlign = null;
            bool bCodelistView = false;  //MITS 15646 : Umesh
			
			LocalCache objCache = null;
			long lTable = 0;
			CodeList objCodLst = null;

			p_objMemory = null;
            XmlNode objNode = null;        //pmittal5 MITS:12685 08/08/08
            int iLevels = 0;               //pmittal5 MITS:12685 08/08/08
            XmlElement objElem = null;     //pmittal5 MITS:12685 08/08/08
            XmlElement objXmlElem = null;  //pmittal5 MITS:12685 08/08/08
            try
			{
                //pmittal5 MITS:12685 08/08/08  - Table Id obtained from input XML
                //if (p_iTable == 0 && p_sTypeCode != "")
                //{
                //    objCache = new LocalCache(m_sConnectString);
                //    //Get the First table of Types
                //    lTable = GetFirstTableOfTypes(objCache.GetCodeId(p_sTypeCode, "GLOSSARY_TYPES"), p_cAlpha);
                //}
                //else
                //    lTable = p_iTable;
                lTable = Common.Conversion.ConvertObjToInt64(objXmlDoc.SelectSingleNode("//TableId").InnerText, m_iClientId);
                //End - pmittal5

                arrColAlign = new string[6];
                arrColWidth = new double[6];
                
                StartDoc(true, 10, 10, 10, 10);
                SetFontBold(true);
                ClientWidth = 15400;

                dblCharWidth = GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ") / 40;
                dblLineHeight = GetTextHeight("W");
                dblLeftMargin = dblCharWidth * 2;
                dblRightMargin = dblCharWidth * 2;

                dblTopMargin = dblLineHeight * 5;
                dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin;
                dblPageHeight = PageHeight;
                dblBottomHeader = dblPageHeight - (5 * dblLineHeight);

                //MGaba2:-MITS 11952-Start				
                //arrColWidth[0] = dblCharWidth * 4  ;// Code
                arrColWidth[0] = dblCharWidth * 6;// Code
                //arrColWidth[1] = dblCharWidth * 9  ;// Description
                arrColWidth[1] = dblCharWidth * 11;// Description
                //arrColWidth[2] = dblCharWidth * 9  ;// Parent
                arrColWidth[2] = dblCharWidth * 11;// Parent
                arrColWidth[3] = dblCharWidth * 4;// Start Date
                arrColWidth[4] = dblCharWidth * 4;// End Date	
                //arrColWidth[5] = dblCharWidth * 9  ;// Organiztaion	
                arrColWidth[5] = dblCharWidth * 12;// Organiztaion	
                //MGaba2:-MITS 11952-End

                dblCurrentX = dblLeftMargin;
                dblCurrentY = 900;

                //Changed by Gagan for MITS 9820 : Start
                CreateNewPage(m_sConnectString, true, lTable);
                //Changed by Gagan for MITS 9820 : End

                //sHeader = Globalization.GetString("UtilityDriver.CreateReport.Heading");
                sHeader = "Code                           Description                                            Parent                                                     Start Date      End Date       Organization"; //Aman ML Change
                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;

                //Print header
                //pmittal5 MITS:12685 08/08/08
                GetCodesTreeXML(ref objXmlDoc, (int)lTable);
                objXmlElem = (XmlElement)objXmlDoc.SelectSingleNode("//control[@name='codelist']");
                //if (objXmlElem.GetAttribute("type") == "listview")
                //MITS 15646 : Umesh
                bCodelistView = IsCodeListView((int)lTable);
                if (bCodelistView)
                    PrintTextNoCr(sHeader);

                arrColAlign[0] = LEFT_ALIGN;
                arrColAlign[1] = LEFT_ALIGN;
                arrColAlign[2] = LEFT_ALIGN;
                arrColAlign[3] = LEFT_ALIGN;
                arrColAlign[4] = LEFT_ALIGN;
                arrColAlign[5] = LEFT_ALIGN;

                CurrentX = dblLeftMargin;
                CurrentY = dblCurrentY;

                SetFontBold(false);
                dblCurrentY = 1200;
                iCounter = 0;

                //pmittal5 MITS:12685 08/08/08
                //MITS 15646 : Umesh
                //if (objXmlElem.GetAttribute("type") == "CodetreeView")
                if (!bCodelistView)
                {
                    arrListName = new System.Collections.ArrayList();
                    arrListLevel = new System.Collections.ArrayList();

                    objNode =(XmlElement)objXmlDoc.SelectSingleNode("//control/ul");
                    if (objNode.FirstChild.NodeType == XmlNodeType.Element)
                        objElem = (XmlElement)objNode.FirstChild;
                    else
                        objElem = (XmlElement)objNode.ChildNodes[1];
                    iLevels =Common.Conversion.ConvertObjToInt(objElem.GetAttribute("level"), m_iClientId);

                    RecursiveRead(objNode);

                    for (int i = 0; i < arrListName.Count; i++)
                    {
                        CreateTreeRow(i, iLevels, dblLeftMargin, ref dblCurrentY, arrColAlign,
                                dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth);
                        iCounter++;
                        if (dblCurrentY >= dblPageHeight - (25 * dblLineHeight))
                        {
                            CreateNewPage(m_sConnectString, false, lTable);
                            iCounter = 0;
                            dblCurrentY = 900;
                        }
                    }
                }
                //MITS 15646 : Umesh
                //else if(objXmlElem.GetAttribute("type") == "listview") 
                else if (bCodelistView) 
                { //End - pmittal5
                    //Get code list
                    objCodLst = GetCodes((int)lTable);

                    foreach (Code objCod in objCodLst)
                    {
                        CreateRow(objCod, dblLeftMargin, ref dblCurrentY, arrColAlign,
                            dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth);
                        iCounter++;
                        //MGaba2-MITS 11952-04/03/2008
                        //if data in a record is covered in multiple lines,then we cant use this
                        //if (iCounter == NO_OF_ROWS)
                        if (dblCurrentY >= dblPageHeight - (25 * dblLineHeight))
                        {
                            //Changed by Gagan for MITS 9820 : Start
                            CreateNewPage(m_sConnectString, false, lTable);
                            //Changed by Gagan for MITS 9820 : End
                            iCounter = 0;
                            dblCurrentY = 900;
                        }
                    }
                }

				// Printing the Summary
                //MGaba2-MITS 11952-04/03/2008
                //if ((iCounter + 3) > NO_OF_ROWS)
                if (dblCurrentY + 2 * dblLineHeight >= dblPageHeight - (25 * dblLineHeight))
				{
                    //Changed by Gagan for MITS 9820 : Start
					CreateNewPage(m_sConnectString, false,lTable);
                    //Changed by Gagan for MITS 9820 : End
					dblCurrentY = 900;
					iCounter = 0;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight;
				}

				SetFontBold(true);
				CurrentX = dblLeftMargin;
				CurrentY = dblCurrentY;

				// Printing the Footer
                //MGaba2-MITS 11952-04/03/2008
				//if ((iCounter + 7) > NO_OF_ROWS)
                if (dblCurrentY + 4 * dblLineHeight >= dblPageHeight - (25 * dblLineHeight))
				{
                    //Changed by Gagan for MITS 9820 : Start
					CreateNewPage(m_sConnectString, false, lTable);
                    //Changed by Gagan for MITS 9820 : Start
					dblCurrentY = 900;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight * 2;
				}

				CreateFooter(dblLeftMargin, dblCurrentY, dblLineHeight, 250);
				EndDoc();
				sFile = TempFile;
				Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);

				File.Delete(sFile);			
				iReturnValue=1;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.CreateReport.Error", m_iClientId),p_objException);
			}
			finally
			{
				arrColAlign = null;
				arrColWidth = null;	
                if(m_objPrintDoc != null)
				    m_objPrintDoc.Dispose();
                if(m_objFont != null)
				    m_objFont.Dispose();
                if(m_objText!= null)
				    m_objText.Dispose();
                objCodLst = null;
                //pmittal5 MITS 12685 08/08/08
                objXmlElem = null;
                objElem = null;
			}
			return iReturnValue;
			
		}

		//Changed by Gagan for MITS 8599 : End

		/// Name		: GetFirstTableOfTypes
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the first table in the list
		/// </summary>
		/// <param name="p_iTypeCode">Code Type</param>
		/// <param name="p_cAlpha">Alphabet</param>
		/// <returns>Integer first table Id</returns>
		private long GetFirstTableOfTypes( int p_iTypeCode, char p_cAlpha)
		{
			StringBuilder sSQL = null;
			DataSet objDs=null;
			long lTableId = 0;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();//Deb Changes
			try
			{
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
                //Deb Changes
                sSQL = new StringBuilder();
                sSQL.Append("SELECT A.TABLE_ID FROM GLOSSARY A,GLOSSARY_TEXT B WHERE A.TABLE_ID=B.TABLE_ID ");
                sSQL.Append(" AND (A.DELETED_FLAG<>1 OR A.DELETED_FLAG IS NULL) ");
                if (p_iTypeCode != 0)
                {
                    sSQL.Append(string.Format(" AND A.GLOSSARY_TYPE_CODE= {0} ", "~GLOSSARY_TYPE_CODE~"));
                    dictParams.Add("GLOSSARY_TYPE_CODE", p_iTypeCode);
                }
                if ((int)p_cAlpha != 0)
                {
                    if (DbFactory.GetDatabaseType(m_sConnectString).ToString() == Constants.DB_SQLSRVR)
                    {
                        sSQL.Append(string.Format(" AND UPPER(B.TABLE_NAME) LIKE '' + {0} + '%'", "~TABLE_NAME~"));
                    }
                    else
                    {
                        sSQL.Append(string.Format(" AND UPPER(B.TABLE_NAME) LIKE {0} || '%'", "~TABLE_NAME~"));
                    }
                    dictParams.Add("TABLE_NAME", p_cAlpha.ToString().ToUpper());
                }
                sSQL.Append(" ORDER BY UPPER(B.TABLE_NAME) ASC");
                //Deb Changes
                objDs = DbFactory.ExecuteDataSet(m_sConnectString, FormatSQLString(sSQL.ToString()), dictParams, m_iClientId);

                //sSQL = new StringBuilder();
                ////Query gets the first table in the list
                //sSQL.Append("SELECT A.TABLE_ID FROM GLOSSARY A,GLOSSARY_TEXT B WHERE A.TABLE_ID=B.TABLE_ID");
                //sSQL.Append(" AND (A.DELETED_FLAG<>1 OR A.DELETED_FLAG IS NULL) ");
                //if (p_iTypeCode != 0)
                //{
                //    sSQL.Append(" AND A.GLOSSARY_TYPE_CODE=");
                //    sSQL.Append(p_iTypeCode);
                //}
                //if ((int)p_cAlpha != 0)
                //{//Parijat: 12244--Now selecting an alphabet for listing of the tables will even return the ones which are in lower case.
                //    //hence retrieving the codes for the one at the top
                //    sSQL.Append(" AND UPPER(B.TABLE_NAME) LIKE '");
                //    sSQL.Append(p_cAlpha.ToString().ToUpper());
                //    sSQL.Append("%'");
                //}	//Parijat: 11486,12244-- the table list comes in order now in oracle for both for upper case as well as lower case entries.
                ////hence retrieving the codes forthe one at the top
                //sSQL.Append(" ORDER BY UPPER(B.TABLE_NAME) ASC");

                //objDs = DbFactory.GetDataSet(m_sConnectString, sSQL.ToString());

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
				if (objDs.Tables[0].Rows.Count >0)
					lTableId= DbRow.GetLngField(objDs.Tables[0].Rows[0],"TABLE_ID");    
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.GetFirstTableOfTypes.DataErr", m_iClientId),
					p_objEx);  
			}
			finally
			{
				sSQL = null;
				if(objDs!=null)objDs.Dispose();
			}
			return lTableId; 
		}

		/// Name		: DuplicateExists
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for Duplicate rows in the Table
		/// </summary>
		/// <param name="p_sTableName">Table Name to be Checked</param>
		/// <param name="p_sColName">Column against to be Checked</param>
		/// <param name="p_sColValue">Column value to be compared</param>
		/// <param name="p_sTableId">Table Id</param>
		/// <param name="p_sMode">Add/Edit Mode</param>
		/// <returns>Boolean True/False</returns>
        private bool DuplicateExists(string p_sTableName, string p_sColName, string p_sColValue, string p_sTableId, string p_sMode, string p_sLOB)
		{
			DbReader objRdr = null;
			int iTableId = 0;
			string sSQL="";
            bool bSuccess = false;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();//Deb Changes
			try
			{
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                StringBuilder strSQL = new StringBuilder();
                strSQL.Append("SELECT * FROM " + p_sTableName);
                strSQL.Append(string.Format(" WHERE " + p_sColName + " = {0}", "~COL_VALUE~"));
                dictParams.Add("COL_VALUE", p_sColValue);
                //Deb Changes
                iTableId = Conversion.ConvertStrToInteger(p_sTableId);

                 //Aman ML Changes
                if(iTableId > 0 && p_sMode == "newNonBase" && p_sTableName.Equals("CODES_TEXT"))
                {
                    strSQL.Append(string.Format(" AND CODE_ID = {0}","~TABLE_ID~"));
                    dictParams.Add("TABLE_ID", iTableId);
                }
                //Aman ML Changes
                else if (iTableId > 0 && p_sMode == "edit")//Changed by Amitosh.                
                {
                    strSQL.Append(string.Format(" AND TABLE_ID IN (SELECT TABLE_ID FROM CODES WHERE CODE_ID= {0}", "~TABLE_ID~"));
                    strSQL.Append(string.Format(" ) AND CODE_ID <> {0}", "~TABLE_ID1~"));
                    dictParams.Add("TABLE_ID", iTableId);
                    dictParams.Add("TABLE_ID1", iTableId);
                }
                else if (iTableId > 0)//Changed by Amitosh.
                {
                    strSQL.Append(string.Format(" AND TABLE_ID = {0}", "~TABLE_ID~"));
                    dictParams.Add("TABLE_ID", iTableId);
                }
                if (Conversion.ConvertStrToInteger(p_sLOB) > 0)
                {
                    strSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}", "~LINE_OF_BUS_CODE~"));
                    dictParams.Add("LINE_OF_BUS_CODE", p_sLOB);
                }
                //Deb Changes
                objRdr = DbFactory.ExecuteReader(m_sConnectString, FormatSQLString(strSQL.ToString()), dictParams);

                ////Parijat: MITS 11759 -- to enable single codes
                //p_sColValue = p_sColValue.Replace("'", "''");
                //iTableId = Conversion.ConvertStrToInteger(p_sTableId);
                //sSQL = "SELECT * FROM " + p_sTableName + " WHERE " + p_sColName + "='" + p_sColValue + "'";

                //// Mihika: Corrected the query in case of editing a code.
                //if (iTableId > 0 && p_sMode == "edit")
                //    sSQL = sSQL + " AND TABLE_ID IN (SELECT TABLE_ID FROM CODES WHERE CODE_ID="
                //        + iTableId + ") AND CODE_ID <>" + iTableId;
                //else if (iTableId > 0)
                //    sSQL = sSQL + " AND TABLE_ID=" + iTableId;

                //// Mihika Defect# 2356 The same code can be created for the LOBs. Adding a check for that
                //if (Conversion.ConvertStrToInteger(p_sLOB) > 0)
                //    sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + p_sLOB;
                //objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL);

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                
                if (objRdr.Read())
                    return true;
                else
                    return false;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("UtilityDriver.DuplicateExists.DataErr", m_iClientId),
					p_objEx);  
			}
			finally
			{
				if (objRdr!=null)objRdr.Dispose(); 
			}
		}
        //vsharma65 start MITS 19999 New function to check duplicacy data in both table.
        //JIRA 7810 snehal start 

        private bool ChildCodeExists(string p_sTableName, string p_sColName, string p_sColValue, string p_sTableId, string p_sMode,string p_sRelCode,
            string p_sParentCodeTable, string p_sChildTable, string p_sParentException)
		{
			DbReader objRdr = null;
			int iTableId = 0;
            bool bSuccess = false;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            try
            {

                StringBuilder strSQL = new StringBuilder();
                strSQL.Append("SELECT RELATED_CODE_ID , TABLE_ID FROM " + p_sTableName);
                strSQL.Append(string.Format(" WHERE " + p_sColName + " = {0}", "~COL_VALUE~"));
                dictParams.Add("COL_VALUE", p_sColValue);
                iTableId = Conversion.ConvertStrToInteger(p_sTableId);
                if (p_sMode == "edit")
                {
                    strSQL.Append(string.Format(" AND TABLE_ID IN (SELECT TABLE_ID FROM CODES WHERE CODE_ID= {0}", "~TABLE_ID~)"));
                    strSQL.Append(string.Format(" AND CODE_ID <> {0}", "~TABLE_ID~"));
                    dictParams.Add("TABLE_ID", iTableId);
                }
                else if (p_sMode == "new")
                {

                    strSQL.Append(string.Format(" AND TABLE_ID = {0}", "~TABLE_ID~"));
                    dictParams.Add("TABLE_ID", iTableId);
                }

                LocalCache objLocalCache = new LocalCache(m_sConnectString,m_iClientId);
					

                objRdr = DbFactory.ExecuteReader(m_sConnectString, FormatSQLString(strSQL.ToString()), dictParams);
                if (objRdr.Read())
                {
                    if (objRdr.GetInt32("TABLE_ID") == objLocalCache.GetTableId(p_sChildTable))
                    {
                        if (Conversion.ConvertStrToInteger(p_sRelCode) != objLocalCache.GetCodeId(p_sParentException, p_sParentCodeTable))
                        {
                            bSuccess= true;
                        }
                        else
                        {
                            bSuccess= false;
                        }
                    }

                    else
                        bSuccess= false;
                }
                return bSuccess;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.ChildCodeExists.DataErr", m_iClientId),
                    p_objEx);
            }
			finally
			{
				if (objRdr!=null)objRdr.Dispose(); 
			}
		}
        //JIRA 7810 end
        private bool ChechDuplicateExist(string p_sTableName, string p_sColName, string p_sColValue, string p_tColValue, string p_sTableId, string p_sMode, string p_sLOB)
        {
            DbReader objRdr = null, objRdrgt = null;
            int iTableId = 0;
            string sSQL = "", sSQLGT = "";
            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            StringBuilder strSQLGT = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012

            try
            {
                //PenTesting - srajindersin - 9th Jan 2012
                p_sColValue = p_sColValue.Replace("'", "");
                iTableId = Conversion.ConvertStrToInteger(p_sTableId);
                strSQL.Append(string.Format("SELECT * FROM " + p_sTableName + " WHERE " + p_sColName + "= {0}", "~COL_VALUE~"));
                dictParams.Add("COL_VALUE", p_sColValue);

                if (iTableId > 0 && p_sMode == "edit")
                {
                    strSQL.Append(string.Format(" AND TABLE_ID IN (SELECT TABLE_ID FROM CODES WHERE CODE_ID = {0})","~CODE_ID~"));
                    strSQL.Append(string.Format(" AND CODE_ID <> {0}","~CODE_ID1~"));
                    dictParams.Add("CODE_ID", iTableId);
                    dictParams.Add("CODE_ID1", iTableId);
                }
                else if (iTableId > 0)
                {
                    strSQL.Append(string.Format(" AND TABLE_ID = {0}", "~TABLE_ID~"));
                    dictParams.Add("TABLE_ID", iTableId);
                }

                if (Conversion.ConvertStrToInteger(p_sLOB) > 0)
                {
                    strSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}", "~LINE_OF_BUS_CODE~"));
                    dictParams.Add("LINE_OF_BUS_CODE", p_sLOB);
                }
                objRdr = DbFactory.ExecuteReader(m_sConnectString, strSQL.ToString(), dictParams);

                //Deb Changes
                strSQLGT = strSQLGT.Append(string.Format("SELECT * FROM GLOSSARY_TEXT WHERE TABLE_NAME = {0}", "~TABLE_NAME~"));
                dictParams.Clear();
                dictParams.Add("TABLE_NAME", p_tColValue);
                objRdrgt = DbFactory.ExecuteReader(m_sConnectString, strSQLGT.ToString(), dictParams);
                //p_sColValue = p_sColValue.Replace("'", "''");
                //iTableId = Conversion.ConvertStrToInteger(p_sTableId);
                //sSQL = "SELECT * FROM " + p_sTableName + " WHERE " + p_sColName + "='" + p_sColValue + "'";


                //if (iTableId > 0 && p_sMode == "edit")
                //    sSQL = sSQL + " AND TABLE_ID IN (SELECT TABLE_ID FROM CODES WHERE CODE_ID="
                //        + iTableId + ") AND CODE_ID <>" + iTableId;
                //else if (iTableId > 0)
                //    sSQL = sSQL + " AND TABLE_ID=" + iTableId;


                //if (Conversion.ConvertStrToInteger(p_sLOB) > 0)
                //    sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + p_sLOB;

                //sSQLGT = sSQLGT + "SELECT * FROM GLOSSARY_TEXT WHERE TABLE_NAME= '" + p_tColValue + "'";

                //objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL);
                //objRdrgt = DbFactory.GetDbReader(m_sConnectString, sSQLGT);
                //END PenTesting - srajindersin - 9th Jan 2012

                if (objRdr.Read() || objRdrgt.Read())
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.DuplicateExists.DataErr", m_iClientId),
                    p_objEx);
            }
            finally
            {
                if (objRdr != null) objRdr.Dispose();
            }
        }
        //vsharma65 End MITS 19999

		/// Name		: FormatSQLString
		/// Author		: Nikhil Kumar Garg
		/// Date Created: 11/06/2006		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Removes "[","]" from the SQL Query
		/// </summary>
		/// <param name="p_sSQLString">SQL String containing square brackets</param>
		/// <returns>Formatted SQL string</returns>
		//private string FormatSQLString(string p_sSQLString)
        public string FormatSQLString(string p_sSQLString)
		{
			p_sSQLString=p_sSQLString.Replace("][", " ");
			p_sSQLString=p_sSQLString.Replace("]", "");
			p_sSQLString=p_sSQLString.Replace("[", "");            
			return p_sSQLString;
		}

       	/// Name		 : IsCodeListView
		/// Author		 : pmittal5
		/// Date Created : 07/28/08	
        /// MITS         : 12685
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks Tree Display Flag of table.
		/// </summary>
		/// <param name="iTableId">Table ID</param>
		/// <returns>Boolean value for TREE_DISPLAY_FLAG</returns>
        private bool IsCodeListView(int iTableId)
        {
            bool bCodeListView = true;
            
            //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
            //string sSql = "";
            //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011
            
            DbReader objReader = null;
            try
            {
                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                StringBuilder strSQL = new StringBuilder();
                strSQL.Append("SELECT TREE_DISPLAY_FLAG FROM GLOSSARY ");
                strSQL.Append(string.Format(" WHERE TABLE_ID = {0} ", "~TABLE_ID~"));

                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                dictParams.Add("TABLE_ID", iTableId);

                objReader = DbFactory.ExecuteReader(m_sConnectString, FormatSQLString(strSQL.ToString()), dictParams);

                //sSql = "SELECT TREE_DISPLAY_FLAG FROM GLOSSARY WHERE TABLE_ID = " + iTableId;
                //objReader = DbFactory.GetDbReader(m_sConnectString, sSql);

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                if (objReader.Read())
                {
                    if (Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId) != 0)
                        bCodeListView = false;
                    else
                        bCodeListView = true;
                }
                return bCodeListView;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.IsCodeListView.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

       	/// Name		 : GetCodesTreeXML
		/// Author		 : pmittal5
		/// Date Created : 07/28/08	
        /// MITS         : 12685
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Display the codes in Tree View form.
		/// </summary>
        /// <param name="p_objXmlDoc">Input XML</param>
        /// <param name="iTableId">Table Id</param>
        private void GetCodesTreeXML(ref XmlDocument p_objXmlDoc, int iTableId)
        {
            int iCurrentLevel = 0;
            int iNoParentLevel = 0;
            int iLevels = 0;
            CodeList objCodeList = null;
            XmlElement objParentElem = null;
            XmlElement objChildElem = null;
            XmlElement objTempElem = null;
            XmlElement objTempNoParent = null;
            System.Collections.ArrayList tableId = new System.Collections.ArrayList();
            try
            {
                objParentElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='codelist']");
                if (objParentElem.GetAttribute("type") == "listview")
                {
                    objParentElem.SetAttribute("type", "CodetreeView");
                }
                objParentElem.InnerText = "Code Tree";
                iLevels = ParentTable(ref tableId, 0, iTableId);

                for (iCurrentLevel = iLevels; iCurrentLevel >= 0; iCurrentLevel--)
                {
                    objCodeList = GetCodes((int)tableId[iCurrentLevel]);
                    foreach (Code objCode in objCodeList)
                    {
                        if (iCurrentLevel != iLevels)                   //For levels which are not topmost, Parent-Child relationship has to be checked.
                        {
                            objParentElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//li[@id='" + objCode.RelatedCodeId + "' and @level='" + Conversion.ConvertObjToStr(iCurrentLevel + 1) + "']");
                        }

                        objChildElem = p_objXmlDoc.CreateElement("li");                      //List-item is getting added to the Parent element.
                        objChildElem.SetAttribute("id", Conversion.ConvertObjToStr(objCode.CodeId));
                        objChildElem.SetAttribute("code", objCode.ShortCode);
                        if (iCurrentLevel != 0)
                            objChildElem.SetAttribute("isChild", "0");
                        else
                            objChildElem.SetAttribute("isChild", "1");
                        objChildElem.SetAttribute("level", Conversion.ConvertObjToStr(iCurrentLevel));
                        if (objCode.LineOfBusCode != 0)
                            objChildElem.InnerText = objCode.ShortCode + "-" + GetCode(objCode.LineOfBusCode).ShortCode + "  " + objCode.CodeDesc;
                        else
                            objChildElem.InnerText = objCode.ShortCode + "  " + objCode.CodeDesc;

                        if (objParentElem == null)                     //Adding "no parent" element if current code has no Related code.
                        {
                            iNoParentLevel = iLevels;
                            while (iNoParentLevel > iCurrentLevel)
                            {
                                objParentElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//li[@id='0' and @level='" + Conversion.ConvertObjToStr(iNoParentLevel) + "']");
                                if (objParentElem == null)
                                {
                                    if (iNoParentLevel == iLevels)
                                    {
                                        objParentElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='codelist']");
                                    }
                                    else
                                    {
                                        objParentElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//li[@id='0' and @level='" + Conversion.ConvertObjToStr(iNoParentLevel + 1) + "']");
                                    }
                                    objTempElem = p_objXmlDoc.CreateElement("li");
                                    objTempElem.SetAttribute("id", "0");
                                    objTempElem.SetAttribute("code", "");
                                    objTempElem.SetAttribute("isChild", "0");
                                    objTempElem.SetAttribute("level", Conversion.ConvertObjToStr(iNoParentLevel));
                                    objTempElem.InnerText = "{no parent}";
                                    if (objParentElem.ChildNodes.Count >= 2)
                                    {
                                        if (objParentElem.FirstChild.NodeType == XmlNodeType.Element)
                                            objParentElem.FirstChild.AppendChild(objTempElem);
                                        else
                                            objParentElem.ChildNodes[1].AppendChild(objTempElem);
                                    }
                                    else
                                    {
                                        objTempNoParent = p_objXmlDoc.CreateElement("ul");
                                        objTempNoParent.AppendChild(objTempElem);
                                        objParentElem.AppendChild(objTempNoParent);
                                    }
                                }
                                iNoParentLevel = iNoParentLevel - 1;
                            }
                            objParentElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//li[@id='0' and @level='" + Conversion.ConvertObjToStr(iCurrentLevel + 1) + "']");
                        }

                        if (objParentElem != null)
                        {
                            if (objParentElem.ChildNodes.Count >= 2)
                            {
                                if (objParentElem.FirstChild.NodeType == XmlNodeType.Element)  //Checks if the first childnode is an XML node or a text.
                                    objParentElem.FirstChild.AppendChild(objChildElem);        //Appends successive list-items to the already present "UL" tag
                                else
                                    objParentElem.ChildNodes[1].AppendChild(objChildElem);
                            }
                            else                                                               //For topmost level, first child node has to be created.
                            {
                                objTempElem = p_objXmlDoc.CreateElement("ul");
                                objTempElem.AppendChild(objChildElem);
                                objParentElem.AppendChild(objTempElem);
                            }
                        }
                    }
                }

                //Removing elements which dont have any child element.
                for (int i = 1; i <= iLevels; i++)
                {
                    XmlNodeList objNodeList = p_objXmlDoc.SelectNodes("//li[@level='" + Conversion.ConvertObjToStr(i) + "']");
                    foreach (XmlNode objNode in objNodeList)
                    {
                        if ((objNode.ChildNodes.Count == 0) || (objNode.ChildNodes.Count == 1 && objNode.FirstChild.NodeType != XmlNodeType.Element))
                        {
                            XmlNode objParent = objNode.ParentNode;
                            objParent.RemoveChild(objNode);
                            if (objParent.ChildNodes.Count == 0 || (objParent.ChildNodes.Count == 1 && objParent.FirstChild.NodeType != XmlNodeType.Element))
                                objParent.ParentNode.RemoveChild(objParent);
                        }
                    }
                    objNodeList = null;
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.GetCodesXML.DataErr", m_iClientId), p_objException);
            }
            finally
            {
                objCodeList = null;
                objChildElem = null;
                objParentElem = null;
                objTempElem = null;
                objTempNoParent = null;
            }
        }

        /// Name		 : ParentTable
        /// Author		 : pmittal5
        /// Date Created : 07/29/08	
        /// MITS         : 12685
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Returns the Parent Ids of current table.
        /// </summary>
        /// <param name="tableId">Array containing table ids of parent tables</param>
        /// <param name="iNext">No. of levels</param>
        /// <param name="iNewTableId">table id whose parent is to be found</param>
        private int ParentTable(ref System.Collections.ArrayList tableId, int iNext, int iNewTableId)
        {
            int iParentId = 0;

            //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
            //string sSql = "";
            //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

            DbReader objReader = null;

            try
            {
                tableId.Add(iNewTableId);

                //PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                StringBuilder strSQL = new StringBuilder();
                strSQL.Append("SELECT RELATED_TABLE_ID FROM GLOSSARY ");
                strSQL.Append(string.Format(" WHERE TABLE_ID = {0} ", "~TABLE_ID~"));

                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                dictParams.Add("TABLE_ID", iNewTableId);

                objReader = DbFactory.ExecuteReader(m_sConnectString, FormatSQLString(strSQL.ToString()), dictParams);

                //sSql = "SELECT RELATED_TABLE_ID FROM GLOSSARY WHERE TABLE_ID = " + iNewTableId;
                //objReader = DbFactory.GetDbReader(m_sConnectString, sSql);

                //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

                
                if (objReader.Read())
                    iParentId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                // bpaskova Jira : 3869, added condition to avoid circle in the list, to brake the circle in the recursion
                if (iParentId != 0 && iParentId != null && !isExistInList(tableId, iParentId))
                    iNext = ParentTable(ref tableId, iNext + 1, iParentId);
                return iNext;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.ParentTable.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            } 
        }

        /// Name		 : isExistInList
        /// Author		 : bpaskova
        /// Date Created : 11/12/14	
        /// Jira         : 3869
        /// <summary>
        /// Check for circle in the ID list.
        /// </summary>
        /// <param name="tableIds">List of table IDs.</param>
        /// <param name="iParentId">New table ID.</param>
        /// <returns>true - if the list contains provided new table id, otherwise - false.</returns>
        private bool isExistInList(ArrayList tableIds, int iParentId)
        {
            bool bRetVal = false;
            foreach (int tableId in tableIds)
            {
                if (tableId == iParentId) return true;
            }
            return bRetVal;
        }

        /// Name		 : RecursiveRead
        /// Author		 : pmittal5
        /// Date Created : 08/05/08	
        /// MITS         : 12685
        /// <summary>
        /// Recursively Read xml nodes
        /// </summary>
        /// <param name="objNode"></param>
        private void RecursiveRead(XmlNode objNode)
        {
            XmlNodeList objNodeList;
            try
            {
                objNodeList = objNode.ChildNodes;
                if ((objNodeList.Count == 1) && (objNodeList[0].Name == "ul"))
                {
                    objNodeList = objNodeList[0].ChildNodes;
                }
                if ((objNodeList.Count == 2) && (objNodeList[1].Name == "ul"))
                {
                    objNodeList = objNodeList[1].ChildNodes;
                }
                foreach (XmlElement objTmpNode in objNodeList)
                {
                    arrListName.Add(objTmpNode.FirstChild.InnerText);
                    arrListLevel.Add(objTmpNode.GetAttribute("level").ToString());

                    if (objTmpNode.HasChildNodes && objTmpNode.ChildNodes.Count != 1)
                    {
                        if ((objTmpNode.ChildNodes.Count == 2) && (objTmpNode.ChildNodes[1].Name == "ul") && (objTmpNode.ChildNodes[1].InnerText == "") == false)
                        {
                            RecursiveRead((XmlNode)objTmpNode);
                        }
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.CreateReport.Error", m_iClientId), p_objException);
            }
            finally
            {
                objNodeList = null;
            }
        }
        //aanandpraka2:Start changes for custom paging
        /// <summary>
        /// Function added to compute 
        /// </summary>
        /// <returns></returns>
        public int GetRecordsPerPage()
        {
            StringBuilder sbSQL = null;
            int iReturn = 0;
            DbReader objReader = null;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                sbSQL.Append(" WHERE PARM_NAME = 'CODES_USER_LIMIT' ");
                objReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString());
                if (objReader.Read())
                {
                    iReturn = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
                    if (iReturn < 10)
                        iReturn = 10;//Record per Page should greater than equal to 10
                }
                else
                {
                    //iReturn=0;
                    iReturn = 10;//Record per Page should greater than equal to 10
                }
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                sbSQL = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    //Shivendu to dispose the object
                    objReader.Dispose();
                }
                objReader = null;
            }
            return iReturn;
        }
        //aanandpraka2:End changes for custom paging

        /// Name		 : CreateTreeRow
        /// Author		 : pmittal5
        /// Date Created : 08/05/08	   
        /// MITS         : 12685
        /// <summary>
        /// Creates a record in a report
        /// </summary>
        /// <param name="objNode"></param>
        private void CreateTreeRow(int iCount, int iLevels, double p_dblCurrentX,
            ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
            double[] p_arrColWidth, double p_dblCharWidth)
        {
            int level = 0;
            string sData = "";
            try
            {
                CurrentX = p_dblCurrentX;
                CurrentY = p_dblCurrentY;
 				level = int.Parse(arrListLevel[iCount].ToString());
				
				while(level != iLevels)
				{
					sData = sData  + "    ";
					level ++;
				}			 
				
				sData += arrListName[iCount];	
		
				PrintText(sData);			
								
				p_dblCurrentX = p_dblLeftMargin;
				p_dblCurrentY += p_dblLineHeight;
			}
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UtilityDriver.CreateRow.Error", m_iClientId), p_objException);
            }
        }

        #endregion
	}// End Class

	/// <summary>
	/// Class is used to work with DataRows
	/// </summary>
	internal class DbRow
    {
		/// <summary>
		/// Default constructor
		/// </summary>
		internal DbRow(){}

		/// Name		: GetStringField
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the String value of the column in the Data Row
		/// </summary>
		/// <param name="p_objRow">Data Row</param>
		/// <param name="p_sColumnName">Column Name to be extracted</param>
		/// <returns>String representation of the Column</returns>
		internal static string GetStringField(DataRow p_objRow, string p_sColumnName)
		{
			if (p_objRow[p_sColumnName].ToString()=="")
				return "";
			else
				return Convert.ToString(p_objRow[p_sColumnName]) ; 
		}

		/// Name		: GetIntField
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the Integer value of the column in the Data Row
		/// </summary>
		/// <param name="p_objRow">Data Row</param>
		/// <param name="p_sColumnName">Column Name to be extracted</param>
		/// <returns>Numeric representation of the Column</returns>
		internal static int GetIntField(DataRow p_objRow, string p_sColumnName)
		{
			if (p_objRow[p_sColumnName].ToString()=="" )
				return 0;
			else
				return Convert.ToInt32(p_objRow[p_sColumnName]); 
		}

		/// Name		: GetLngField
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the Long value of the column in the Data Row
		/// </summary>
		/// <param name="p_objRow">Data Row</param>
		/// <param name="p_sColumnName">Column Name to be extracted</param>
		/// <returns>Numeric representation of the Column</returns>
		internal static long GetLngField(DataRow p_objRow, string p_sColumnName)
		{
			if (p_objRow[p_sColumnName].ToString()=="" )
				return 0;
			else
				return Convert.ToInt64(p_objRow[p_sColumnName]); 
		}
	}//End internal class
}// End Namespace
