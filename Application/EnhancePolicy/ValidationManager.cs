﻿
using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;


namespace Riskmaster.Application.EnhancePolicy
{
    /**************************************************************
	 * $File		: ValidationManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Pankaj K Mahli
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
    // Naresh Connection Leak
    public class ValidationManager : Common,IDisposable
    {        
		#region Constructors

        public ValidationManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
            : base(p_sDsnName, p_sUserName, p_sPassword, p_iClientId)
		{						
		}

        public ValidationManager(UserLogin p_oUserLogin, string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
            : base(p_oUserLogin, p_sDsnName, p_sUserName, p_sPassword, p_iClientId)
        {
            m_UserLogin = p_oUserLogin;

        }
        UserLogin m_UserLogin = null;
        public int m_iClientId = 0;
        #endregion
        // Naresh Connection Leak
        public void Dispose()
        {
            if (base.LocalCache != null)
            {
                base.LocalCache.Dispose();
            }
            if (base.DataModelFactory != null)
            {
                base.DataModelFactory.Dispose();
            }
        }
        // pmahli Validate Transaction date for Amend Policy 
        #region Validate Transaction Date for Amend Policy
        public bool ValidateAmendTransactionDate(int p_iPolicyId, string p_sTransactionDate)
        {
            Boolean bRetVal;
            DateTime objEffDate;
            DateTime objExpDate;
            DateTime objTransDate;
            DateTime objTxnDate;
            bRetVal = false;



            PolicyEnh objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);
            objPolicyEnh.MoveTo(p_iPolicyId);

            try
            {

                string sEffDate = Conversion.ConvertObjToStr(base.GetLatestTerm(objPolicyEnh).EffectiveDate);
                objEffDate = Conversion.ToDate(sEffDate);
                string sExpDate = Conversion.ConvertObjToStr(base.GetLatestTerm(objPolicyEnh).ExpirationDate);
                objExpDate = Conversion.ToDate(sExpDate);
                objTransDate = Convert.ToDateTime(p_sTransactionDate);

                // to check that the transaction date is within term dates
                if (!base.IsWithinTermDates(objEffDate, 3, objTransDate))
                {
                    throw new RMAppException(Globalization.GetString("ValidationManager.ValidateAmendTransactionDate.NotWithinTermDate", base.ClientId));
                }
                else if (!base.IsWithinTermDates(objTransDate, 3, objExpDate))
                {
                    throw new RMAppException(Globalization.GetString("ValidationManager.ValidateAmendTransactionDate.NotWithinTermDate", base.ClientId));
                }

                // comparision with previous transaction date
                foreach (PolicyXTransEnh objPolicyTrans in objPolicyEnh.PolicyXTransEnhList)
                {
                    string sTxnDate = Conversion.ConvertObjToStr(objPolicyTrans.EffectiveDate);
                    objTxnDate = Conversion.ToDate(sTxnDate);

                    if (!base.IsWithinTermDates(objTxnDate, 3, objTransDate))
                    {
                        throw new RMAppException(Globalization.GetString("ValidationManager.ValidateAmendTransactionDate.TransExists", base.ClientId));
                    }

                }

                bRetVal = true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {

            }


            return bRetVal;

        }
        #endregion

        //Validate Cancel Policy function : added by Divya 01/07
        public bool ValidateCancelPolicy(string p_sCancelType, string p_sCancelDate, string p_sCancelReason, int p_iPolicyId)
        {

            DateTime objCancelDate;
            DateTime objEffDate;
            DateTime objExpDate;
            TimeSpan objDiff;
            PolicyXTermEnh objTerm;
            PolicyEnh objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);
            objPolicyEnh.MoveTo(p_iPolicyId);
            try
            {


                objTerm = base.GetLatestTerm(objPolicyEnh);

                string sEffDate = Conversion.ConvertObjToStr(objTerm.EffectiveDate);
                objEffDate = Conversion.ToDate(sEffDate);
                string sExpDate = objTerm.ExpirationDate;
                objExpDate = Conversion.ToDate(sExpDate);
                objCancelDate = Convert.ToDateTime(p_sCancelDate);
                if (base.LocalCache.GetShortCode(Convert.ToInt32(p_sCancelType)) == "PR")
                { //if cancel type is pro rata, the cancel date cannot be equal to the term effective date


                    objDiff = objCancelDate.Subtract(objEffDate);
                    if (objDiff.Days <= 0)
                    {
                        string strmsg = Globalization.GetString("EnhancePolicy.ValidationManager.CancelProRataError", base.ClientId);
                        throw new RMAppException(strmsg);

                    }
                    objDiff = objCancelDate.Subtract(objExpDate);
                    if (objDiff.Days > 0)
                    {

                        throw new RMAppException(Globalization.GetString("EnhancePolicy.ValidationManager.CancelProRataError2",base.ClientId));


                    }


                }


                // now make sure there isn't another transaction with a trans date greater
                foreach (PolicyXTransEnh objTxn in objPolicyEnh.PolicyXTransEnhList)
                {
                    DateTime objTxnDate = Conversion.ToDate(objTxn.EffectiveDate);
                    objDiff = objCancelDate.Subtract(objTxnDate);
                    if (objDiff.Days < 0)
                    {

                        throw new RMAppException(Globalization.GetString("EnhancePolicy.ValidationManager.CancelTransDateError", base.ClientId));

                    }
                }
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }

            return true;

        }

        //pmahli 5/31/2007 Validate Reinstate Date
        # region Validate Reinstate Date
        public bool ValidateReinstateDate(int p_iPolicyId, string p_sReinstateDate, Boolean p_bWithLapse)
        {
            Boolean bRetVal;
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iTermNum = 0;
            int iSeqNum = 0;
            string sEffDate = string.Empty;
            string sExpDate = string.Empty;
            string sCanceldate = string.Empty;
            DateTime objEffDate;
            DateTime objExpDate;
            DateTime objCancelDate;
            DateTime objReinstateDate;
            int iReinstateType;
            string sReinstateType = string.Empty;



            bRetVal = false;
            try
            {
                //Get Reinstatement Type
                if (p_bWithLapse == true)
                {
                    iReinstateType = base.CCacheFunctions.GetCodeIDWithShort("RL", "REINSTATEMENT_TYPE");
                    sReinstateType = base.LocalCache.GetShortCode(iReinstateType);
                }
                else
                {
                    iReinstateType = base.CCacheFunctions.GetCodeIDWithShort("RNL", "REINSTATEMENT_TYPE");
                    sReinstateType = base.LocalCache.GetShortCode(iReinstateType);
                }
                sSQL = "SELECT MAX(TERM_NUMBER) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_iPolicyId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    iTermNum = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                }
                objReader.Close();

                sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_iPolicyId + " AND TERM_NUMBER = " + iTermNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    iSeqNum = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                }
                objReader.Close();
                sSQL = "SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_iPolicyId + " AND TERM_NUMBER = " + iTermNum + " AND SEQUENCE_ALPHA = " + iSeqNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    sEffDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                    sExpDate = Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"));
                    sCanceldate = Conversion.ConvertObjToStr(objReader.GetValue("CANCEL_DATE"));
                    objEffDate =Convert.ToDateTime(Conversion.GetUIDate(sEffDate,m_UserLogin.objUser.NlsCode.ToString(),m_iClientId));//vkumar258 ML Changes
                    objExpDate =Convert.ToDateTime( Conversion.GetUIDate(sExpDate,m_UserLogin.objUser.NlsCode.ToString(),m_iClientId));
                    objCancelDate = Convert.ToDateTime(Conversion.GetUIDate(sCanceldate, m_UserLogin.objUser.NlsCode.ToString(), m_iClientId));
                    //Check date entered isn't before the cancelled term's effective date
                    if (sReinstateType == "RL")
                    {
                        objReinstateDate = Convert.ToDateTime(p_sReinstateDate);

                        if (!(objReinstateDate >= objEffDate))
                            throw new RMAppException(Globalization.GetString("ValidationManager.ValidateReinstateDate.EffectiveDate", base.ClientId));
                        else if (!(objExpDate >= objReinstateDate))
                            throw new RMAppException(Globalization.GetString("ValidationManager.ValidateReinstateDate.ExpirationDate", base.ClientId));
                        else if (objReinstateDate == objCancelDate)
                            throw new RMAppException(Globalization.GetString("ValidationManager.ValidateReinstateDate.CancelDate", base.ClientId));
                        //pmahli MITS 9753 
                        //Added Validation for reinstate date greater than cancel date.
                        else if (objReinstateDate < objCancelDate)
                            throw new RMAppException(Globalization.GetString("ValidationManager.ValidateReinstateDate.CancelPriorDate", base.ClientId));
                    }

                }
                objReader.Close();
                bRetVal = true;
                return bRetVal;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                    if (!objReader.IsClosed) objReader.Close();
            }


        }

        # endregion

        public XmlDocument GetCancelPolicyCodeData(XmlDocument p_objXMLIn)
        {
            XmlDocument m_objXMLDoc = null;
            XmlElement objrootnode = null;
            XmlElement objNew = null;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            XmlCDataSection objCData = null;

            base.StartDocument(ref m_objXMLDoc, ref objrootnode, "codes");
            string sCodevalues = base.GetCodes("CANCEL_TYPE");
            string[] sCodeArr = sCodevalues.Split('^');
            objNew = m_objXMLDoc.CreateElement("CancelTypeCodeList");

            xmlOption = m_objXMLDoc.CreateElement("option");
            xmlOptionAttrib = m_objXMLDoc.CreateAttribute("value");
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOptionAttrib = null;
            xmlOption = null;

            for (int i = 0; i < sCodeArr.Length; i++)
            {
                string[] sCodes = sCodeArr[i].Split('~');
                xmlOption = m_objXMLDoc.CreateElement("option");
                objCData = m_objXMLDoc.CreateCDataSection(sCodes[1]);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = m_objXMLDoc.CreateAttribute("value");

                xmlOptionAttrib.Value = sCodes[0];

                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }

            m_objXMLDoc.DocumentElement.AppendChild(objNew);
            sCodevalues = base.GetCodes("CANCEL_REASON");
            sCodeArr = sCodevalues.Split('^');
            objNew = m_objXMLDoc.CreateElement("CancelReasonCodeList");

            xmlOption = m_objXMLDoc.CreateElement("option");
            xmlOptionAttrib = m_objXMLDoc.CreateAttribute("value");
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOptionAttrib = null;
            xmlOption = null;

            for (int i = 0; i < sCodeArr.Length; i++)
            {
                string[] sCodes = sCodeArr[i].Split('~');
                xmlOption = m_objXMLDoc.CreateElement("option");
                objCData = m_objXMLDoc.CreateCDataSection(sCodes[1]);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = m_objXMLDoc.CreateAttribute("value");

                xmlOptionAttrib.Value = sCodes[0];

                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }

            m_objXMLDoc.DocumentElement.AppendChild(objNew);
            return m_objXMLDoc;

        }



        //pmahli Render Audit TermList
        #region Render Audit Termlist
        public XmlDocument RenderAuditTermList(int p_PolicyId)
        {
            //TODO
            XmlDocument objAuditPolicy = null;
            XmlElement objRootElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            string sSQL = string.Empty;
            DbReader objReader = null;
            DbReader objReader1 = null;
            DbReader objReader2 = null;
            DbReader objDbReader = null;
            int iIndex = 0;
            int iSeqAlpha = 0;
            int iTermNum = 0;
            DateTime objExpDate;
            DateTime objCancDate;
            DateTime objDate;
            objDate = System.DateTime.Today;
            bool IsAuditPolicy = false;

            try
            {
                base.StartDocument(ref objAuditPolicy, ref objRootElement, "TermList");
                //Create Header node for Grid
                base.CreateElement(objRootElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "TermNumber", "Term Number");
                base.CreateAndSetElement(objListHeadXmlElement, "EffectiveDate", "Effective Date");
                base.CreateAndSetElement(objListHeadXmlElement, "ExpirationDate", "Expiration Date");
                base.CreateAndSetElement(objListHeadXmlElement, "AuditIndicator", "Audit Indicator");

                //rsushilaggar MITS 21495
                sSQL = "SELECT AUDIT_CANCELLED_POLICY FROM SYS_POL_OPTIONS";
                objDbReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        IsAuditPolicy = Conversion.ConvertObjToBool(objDbReader["AUDIT_CANCELLED_POLICY"], base.ClientId);
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }

                sSQL = "SELECT DISTINCT TERM_NUMBER FROM  POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_PolicyId + "ORDER BY TERM_NUMBER";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                while (objReader.Read())
                {
                    iTermNum = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                    sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM  POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_PolicyId + " AND TERM_NUMBER = " + iTermNum;
                    objReader1 = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader1.Read())
                    {
                        iSeqAlpha = Conversion.ConvertObjToInt(objReader1.GetValue(0), base.ClientId);
                        sSQL = "SELECT * FROM  POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_PolicyId + " AND TERM_NUMBER = " + iTermNum + " AND SEQUENCE_ALPHA = " + iSeqAlpha;
                        objReader2 = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReader2.Read())
                        {
                            objExpDate = Convert.ToDateTime(Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader2.GetValue("EXPIRATION_DATE")), "d"));
                            string cancelDate = objReader2.GetValue("CANCEL_DATE") != null ? Conversion.ConvertObjToStr(objReader2.GetValue("CANCEL_DATE")) : "";
                            if (!string.IsNullOrEmpty(cancelDate) && IsAuditPolicy)
                            {
                                objCancDate = Convert.ToDateTime(Conversion.GetDBDateFormat(cancelDate, "d"));
                            }
                            else
                            {
                                objCancDate = objExpDate;
                            }

                            //rsushilaggar MITS 21495
                            if (objCancDate < objDate)
                            {
                                base.CreateElement(objRootElement, "option", ref objOptionXmlElement);
                                objOptionXmlElement.SetAttribute("ref", "/Instance/Document/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex++ + "]");
                                base.CreateAndSetElement(objOptionXmlElement, "TermNumber", Conversion.ConvertObjToStr(objReader2.GetValue("TERM_NUMBER")));
                                base.CreateAndSetElement(objOptionXmlElement, "EffectiveDate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader2.GetValue("EFFECTIVE_DATE")), "d"));
                                base.CreateAndSetElement(objOptionXmlElement, "ExpirationDate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader2.GetValue("EXPIRATION_DATE")), "d"));
                                base.CreateAndSetElement(objOptionXmlElement, "AuditIndicator", base.CCacheFunctions.GetShortCode(Conversion.ConvertObjToInt(objReader2.GetValue("AUDITED_FLAG"), base.ClientId)));
                                base.CreateAndSetElement(objOptionXmlElement, "UniqueTermNumber", Conversion.ConvertObjToStr(objReader2.GetValue("TERM_NUMBER")));
                            }


                        }
                        objReader2.Close();
                    }
                    objReader1.Close();
                }
                objReader.Close();
                
            }
            catch (Exception ex)
            {
            
            }
            finally
            {
                objRootElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                if (objReader != null)
                    objReader.Close();
                if (objReader1 != null)
                    objReader1.Close();
                if (objReader2 != null)
                    objReader2.Close();

            }
            return (objAuditPolicy);
        }
        #endregion
        //pmahli 02/08/2007 EPA Earned Premium Calulation pop up 
        #region Launch Earned Premium

        public XmlDocument LaunchEarnedPremium(int p_iPolicyID)
        {
            XmlDocument objEffectiveDate = null;
            XmlElement objRootNode = null;

            PolicyEnh objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);
            objPolicyEnh.MoveTo(p_iPolicyID);

            //Manish Multicurrency
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
            LocalCache objCache = null;
            objCache = new LocalCache(base.ConnectionString,ClientId);

            try
            {
                objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), ref sShortCode, ref sDesc);
                sCurrency = sDesc.Split('|')[1];

                base.StartDocument(ref objEffectiveDate, ref objRootNode, "EarnedPremium");
                base.CreateAndSetElement(objRootNode, "EffectiveDate", Conversion.GetUIDate(base.GetLatestTerm(objPolicyEnh).EffectiveDate,m_UserLogin.objUser.NlsCode.ToString(),base.ClientId));
                base.CreateAndSetElement(objRootNode, "BaseCurrencyType", sCurrency);
                return (objEffectiveDate);
            }
            finally
            {
                objPolicyEnh = null;
            }
        }

        #endregion

        // Start Naresh Changes for Policy Billing Tab in Enhanced Policy
        #region Fetch Billing Rules
        /// <summary>
        /// Fetches the Billing Rules still in use from the Database
        /// </summary>
        /// <returns> The Xml Document containing the Billing Rules which are in Use</returns>
        public XmlDocument FetchBillingRules()
        {
            XmlDocument objXmlDocument = null;
            XmlElement objElement = null;
            XmlElement objRootElement = null;
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iBillingRuleCode;
            LocalCache objCache = null;
            string sShortCode = "";
            string sCodeDesc = "";
            try
            {
                objCache = new LocalCache(base.ConnectionString,ClientId);
                sSQL = "SELECT BILLING_RULE_ROWID,BILLING_RULE_CODE FROM SYS_BILL_BLNG_RULE WHERE IN_USE_FLAG <>0";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objXmlDocument = new XmlDocument();
                base.StartDocument(ref objXmlDocument, ref objRootElement, "BillingRuleCodes");
                while (objReader.Read())
                {
                    base.CreateElement(objRootElement, "BillingRuleCode", ref objElement);
                    iBillingRuleCode = Conversion.ConvertObjToInt(objReader.GetValue("BILLING_RULE_CODE"), base.ClientId);
                    objElement.SetAttribute("Billing_Rule_RowId", Conversion.ConvertObjToStr(objReader.GetValue("BILLING_RULE_ROWID")));
                    objCache.GetCodeInfo(iBillingRuleCode, ref sShortCode, ref sCodeDesc);
                    objElement.SetAttribute("Billing_Rule_ShortCode", sShortCode);
                    objElement.SetAttribute("Billing_Rule_CodeDesc", sCodeDesc);
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.FetchBillingRules.Error", base.ClientId), p_objEx);
            }
            finally
            {
                    objReader.Close();
            }
            return (objXmlDocument);
        }
        #endregion Fetch Billing Rules

        #region Fetch Pay Plans
        /// <summary>
        /// Fetches the Pay Plans still in use from Database corresponding to a given state and lob
        /// </summary>
        /// <param name="p_sLob"> The Lob corresponding to which the Pay Plan has to be Fetched</param>
        /// <param name="p_sState"> The State corresponding to which the Pay Plan has to be Fetched</param>
        /// <returns> The Xml Document containing the Pay Plans which are in Useand as per the State and Lob given</returns>
        public XmlDocument FetchPayPlans(string p_sLob, string p_sState)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objElement = null;
            XmlElement objRootElement = null;
            string sSQL = string.Empty;
            DbReader objReader = null;
            LocalCache objCache = null;
            int iLob = 0;
            int iState = 0;
            string sLobShortCode = "";
            string sStateShortCode = "";
            int iPayPlanCode = 0;
            int iBillingRuleCode = 0;
            string sPayPlanShortCode = "";
            string sPayPlanDesc = "";
            string sBillingRuleShortCode = "";
            string sBillingRuleDesc = "";

            try
            {
                objCache = new LocalCache(base.ConnectionString,ClientId);
                if (p_sLob != "")
                {
                    sLobShortCode = p_sLob.Substring(0, p_sLob.IndexOf(" ", 0));
                    iLob = objCache.GetCodeId(sLobShortCode, "POLICY_LOB");
                }
                if (p_sState != "")
                {
                    sStateShortCode = p_sState.Substring(0, p_sState.IndexOf(" ", 0));

                    sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = '" + sStateShortCode + "'";
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                        iState = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ROW_ID"), base.ClientId);
                    objReader.Close();
                }
                sSQL = "SELECT S.PAY_PLAN_CODE,S.PAY_PLAN_ROWID,S.BILLING_RULE_ROWID,SB.BILLING_RULE_CODE ";
                sSQL += "FROM SYS_BILL_PAY_PLAN S LEFT OUTER JOIN SYS_BILL_BLNG_RULE SB ON S.BILLING_RULE_ROWID = SB.BILLING_RULE_ROWID ";
                sSQL += "WHERE STATE = " + iState + " AND LINE_OF_BUSINESS = " + iLob + " AND S.IN_USE_FLAG <>0 ";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objXmlDocument = new XmlDocument();
                base.StartDocument(ref objXmlDocument, ref objRootElement, "PayPlans");
                while (objReader.Read())
                {
                    base.CreateElement(objRootElement, "PayPlan", ref objElement);
                    iPayPlanCode = Conversion.ConvertObjToInt(objReader.GetValue("PAY_PLAN_CODE"), base.ClientId);
                    objElement.SetAttribute("Pay_Plan_RowId", Conversion.ConvertObjToStr(objReader.GetValue("PAY_PLAN_ROWID")));
                    objCache.GetCodeInfo(iPayPlanCode, ref sPayPlanShortCode, ref sPayPlanDesc);
                    objElement.SetAttribute("Pay_Plan_ShortCode", sPayPlanShortCode);
                    objElement.SetAttribute("Pay_Plan_CodeDesc", sPayPlanDesc);
                    objElement.SetAttribute("Billing_Rule_RowId", Conversion.ConvertObjToStr(objReader.GetValue("BILLING_RULE_ROWID")));
                    iBillingRuleCode = Conversion.ConvertObjToInt(objReader.GetValue("BILLING_RULE_CODE"), base.ClientId);
                    objCache.GetCodeInfo(iBillingRuleCode, ref sBillingRuleShortCode, ref sBillingRuleDesc);
                    objElement.SetAttribute("Billing_Rule_ShortCode", sBillingRuleShortCode);
                    objElement.SetAttribute("Billing_Rule_CodeDesc", sBillingRuleDesc);
                    objElement.SetAttribute("LOB", sLobShortCode);
                    objElement.SetAttribute("State", sStateShortCode);
                }
                objReader.Close();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.FetchPayPlans.Error", base.ClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                    if (!objReader.IsClosed) objReader.Close();
            }
            return (objXmlDocument);
        }
        #endregion Fetch Pay Plans
        // End Naresh Changes for Policy Billing Tab in Enhanced Policy
    }
}
