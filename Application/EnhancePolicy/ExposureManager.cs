﻿
using System;
using System.Collections;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
//Anu Tennyson for MITS 18229 - STARTS
using System.Collections.Generic;
//Anu Tennyson for MITS 18229 - ENDS
using System.Globalization;
using System.Threading;

namespace Riskmaster.Application.EnhancePolicy
{
    public struct PolicyExposureUITemp
    {
        private bool bExceptionsEnabled;
        private bool bRemarksEnabled;
        private bool bCovTypeCodeEnabled;
        private bool bEffDateEnabled;
        private bool bExpAmountEnabled;
        private bool bExpDateEnabled;
        private bool bExpRateEnabled;
        private bool bBaseRateEnabled;
        private bool bExpTypeEnabled;
        private bool bHierarchyLevelEnabled;
        private bool bPremAmtEnabled;
        private bool bStatusEnabled;
        private bool bOkEnabled;
        private bool bCancelEnabled;
        //Anu Tennyson for MITS 18229 STRATS  
        private bool bALUARDetailsAddButtonEnabled;
        private bool bALUARDetailsDeleteButtonEnabled;
        private bool bPCUARDetailsAddButtonEnabled;
        private bool bPCUARDetailsDeleteButtonEnabled;
        private bool bScheduleAddButtonEnabled;
        private bool bScheduleDeleteButtonEnabled;
        private bool bScheduleEditButtonEnabled;
        //Anu Tennyson for MITS 18229 ENDS

        public bool ExceptionsEnabled { get { return bExceptionsEnabled; } set { bExceptionsEnabled = value; } }
        public bool RemarksEnabled { get { return bRemarksEnabled; } set { bRemarksEnabled = value; } }
        public bool CovTypeCodeEnabled { get { return bCovTypeCodeEnabled; } set { bCovTypeCodeEnabled = value; } }
        public bool EffDateEnabled { get { return bEffDateEnabled; } set { bEffDateEnabled = value; } }
        public bool ExpAmountEnabled { get { return bExpAmountEnabled; } set { bExpAmountEnabled = value; } }
        public bool ExpDateEnabled { get { return bExpDateEnabled; } set { bExpDateEnabled = value; } }
        public bool ExpRateEnabled { get { return bExpRateEnabled; } set { bExpRateEnabled = value; } }
        public bool BaseRateEnabled { get { return bBaseRateEnabled; } set { bBaseRateEnabled = value; } }
        public bool ExpTypeEnabled { get { return bExpTypeEnabled; } set { bExpTypeEnabled = value; } }
        public bool HierarchyLevelEnabled { get { return bHierarchyLevelEnabled; } set { bHierarchyLevelEnabled = value; } }
        public bool PremAmtEnabled { get { return bPremAmtEnabled; } set { bPremAmtEnabled = value; } }
        public bool StatusEnabled { get { return bStatusEnabled; } set { bStatusEnabled = value; } }
        public bool OkEnabled { get { return bOkEnabled; } set { bOkEnabled = value; } }
        public bool CancelEnabled { get { return bCancelEnabled; } set { bCancelEnabled = value; } }
        //Anu Tennyson for MITS 18229 STARTS
        public bool ALUARDetailsAddButtonEnabled { get { return bALUARDetailsAddButtonEnabled; } set { bALUARDetailsAddButtonEnabled = value; } }
        public bool ALUARDetailsDeleteButtonEnabled { get { return bALUARDetailsDeleteButtonEnabled; } set { bALUARDetailsDeleteButtonEnabled = value; } }
        public bool PCUARDetailsAddButtonEnabled { get { return bPCUARDetailsAddButtonEnabled; } set { bPCUARDetailsAddButtonEnabled = value; } }
        public bool PCUARDetailsDeleteButtonEnabled { get { return bPCUARDetailsDeleteButtonEnabled; } set { bPCUARDetailsDeleteButtonEnabled = value; } }
        public bool ScheduleAddButtonEnabled { get { return bScheduleAddButtonEnabled; } set { bScheduleAddButtonEnabled = value; } }
        public bool ScheduleEditButtonEnabled { get { return bScheduleEditButtonEnabled; } set { bScheduleEditButtonEnabled = value; } }
        public bool ScheduleDeleteButtonEnabled { get { return bScheduleDeleteButtonEnabled; } set { bScheduleDeleteButtonEnabled = value; } }  
        //Anu Tennyson for MITS 18229 ENDS
    }

    // Naresh Connection Leak
    public class ExposureManager :Common,IDisposable
    {
        private const string SC_PROVISIONAL = "PR";
        private const string SC_DELETED = "DE";
        private const string TBL_COVEXP = "COVEXP_STATUS";
        
        #region Constructor

        internal ExposureManager(PolicyEnh p_objPolicyEnh):base( p_objPolicyEnh.Context )
        {
            m_objPolicyEnh = p_objPolicyEnh;
        }

        public ExposureManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
            : base(p_sDsnName, p_sUserName, p_sPassword, p_iClientId)
		{					
		}

        #endregion 

        #region Global Variables
                
        private PolicyEnh m_objPolicyEnh = null;        

        private PolicyEnh PolicyEnh
        {
            get
            {
                return (m_objPolicyEnh);
            }
        }

        #endregion 

        // Naresh Connection Leak
        public void Dispose()
        {
            if (base.LocalCache != null)
            {
                base.LocalCache.Dispose();
            }
            if (base.DataModelFactory != null)
            {
                base.DataModelFactory.Dispose();
            }
        }
        //Sumit - 03/16/2010- MITS# 18229 -Added one more parameter for UAR
        //public void RenderExposureList(XmlDocument objSysEx, int p_iTransNumber, Hashtable p_objUniqueIds)
        //Commneted by Anu Tennyson
        //public void RenderExposureList(XmlDocument objSysEx, int p_iTransNumber, Hashtable p_objUniqueIds, Hashtable p_objUarIds)
        public void RenderExposureList(XmlDocument objSysEx, int p_iTransNumber, Hashtable p_objUniqueIds, Hashtable p_objUarIds, SessionManager objSessionManager, Hashtable p_objScheduleSessionIds)
        {
            XmlElement objExpListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;

            // Create two Sorted List for Sorting the Xml, one will contain the positive Entries, and second will Contain Negative entries
            SortedList objPositiveEntries = null;
            SortedList objNegativeEntries = null;
            bool bSortingNecessary = false;

            int iIndex = 0;
            double dblExposureSum = 0;
            double dblManualPremium = 0;
            string sFlat = "";
            string sSQL = string.Empty;
            DbReader objReader = null;
            XmlAttribute objXmlAttribute = null;

            //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen STARTS
            StringBuilder sbExposureAdded = null;
            StringBuilder sbExpAndUARSessionID = null;
            string sExposureAddedSessionID = string.Empty;
            string sExposureAdded = string.Empty;
            string sLob = string.Empty;
            
            XmlDocument xmlExposureAdded = null;
            Vehicle objVehicle = null;
            PropertyUnit objPropertyUnit = null;
            List<int> iExposureAlreadyPresent = new List<int>();
            sLob = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
            if (sLob == "AL")
            {
                objVehicle = (Vehicle)base.DataModelFactory.GetDataModelObject("Vehicle", false);
            }
            else if (sLob == "PC")
            {
                objPropertyUnit = (PropertyUnit)base.DataModelFactory.GetDataModelObject("PropertyUnit", false);
            }
            //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen ENDS
            try
            {
                objExpListElement = (XmlElement)objSysEx.SelectSingleNode("/SysExData/ExposureList");
                if (objExpListElement != null)
                    objExpListElement.ParentNode.RemoveChild(objExpListElement);

                base.CreateElement(objSysEx.DocumentElement, "ExposureList", ref objExpListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objExpListElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "Type", "Type");
                base.CreateAndSetElement(objListHeadXmlElement, "Description", "Description");
                base.CreateAndSetElement(objListHeadXmlElement, "Status", "Status");
                base.CreateAndSetElement(objListHeadXmlElement, "Amount", "Number of Units");
                base.CreateAndSetElement(objListHeadXmlElement, "Rate", "Rate");
                base.CreateAndSetElement(objListHeadXmlElement, "ProRateAnnualPremium", "Pro-Rata Annual Premium");
                base.CreateAndSetElement(objListHeadXmlElement, "AmendedDate", "Amended Date");
                base.CreateAndSetElement(objListHeadXmlElement, "PremiumAdjustment", "Premium Adjustment");
                base.CreateAndSetElement(objListHeadXmlElement, "EffectiveDate", "Effective Date");
                base.CreateAndSetElement(objListHeadXmlElement, "ExpirationDate", "Expiration Date");
                base.CreateAndSetElement(objListHeadXmlElement, "OrgLevel", "Org Level");

                // Initialize the Sorted Lists
                objPositiveEntries = new SortedList();
                objNegativeEntries = new SortedList();
                //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen STARTS
                sbExposureAdded = new StringBuilder();
                //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen ENDS
                foreach (PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList)
                {
                    //Parijat::MITS 9887 .
                    //Since the newly added Exposures have TransactionId as 0 in case of quote.
                    //but have fixed parent TransactionId in case of Policy (p_iTransNumber).
                    if (objPolicyXExpEnh.TransactionId == p_iTransNumber || objPolicyXExpEnh.TransactionId == 0)
                    //// pmahli- 5/30/2007 Commented or Condition || objPolicyXExpEnh.ExposureId <= 0
                    //if (objPolicyXExpEnh.TransactionId == p_iTransNumber || objPolicyXExpEnh.ExposureId <= 0)
                    {
                        base.CreateElement(objExpListElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SYSEXDATA_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                        string sOrgAbbr = string.Empty;
                        string sOrgName = string.Empty;
                        string sExpCode = string.Empty;
                        string sExpDesc = string.Empty;
                        base.LocalCache.GetCodeInfo(objPolicyXExpEnh.ExposureCode, ref sExpCode, ref sExpDesc);
                        base.LocalCache.GetOrgInfo(objPolicyXExpEnh.HeirarchyLevel, ref sOrgAbbr, ref sOrgName);

                        dblExposureSum += objPolicyXExpEnh.ExposureAmount;
                        dblManualPremium += objPolicyXExpEnh.PrAnnPremAmt;

                        //base.CreateAndSetElement(objOptionXmlElement, "Type", objPolicyXExpEnh.ExposureCode.ToString());
                        //base.CreateAndSetElement(objOptionXmlElement, "Description", base.LocalCache.GetCodeDesc(objPolicyXExpEnh.ExposureCode));
                        base.CreateAndSetElement(objOptionXmlElement, "Type", sExpCode);
                        base.CreateAndSetElement(objOptionXmlElement, "Description", sExpDesc);
                        base.CreateAndSetElement(objOptionXmlElement, "Status", base.LocalCache.GetCodeDesc(objPolicyXExpEnh.Status));
                        base.CreateAndSetElement(objOptionXmlElement, "Amount", objPolicyXExpEnh.ExposureAmount.ToString());
                        //Mukul(6/18/07) Added Percentage sign MITS 9792
                        sSQL = "SELECT FLAT_OR_PERCENT FROM SYS_POL_EXP_RATES WHERE EXPOSURE_ID = " + objPolicyXExpEnh.ExposureCode + " AND STATE = " + this.PolicyEnh.State + " AND EFFECTIVE_DATE <= '" + objPolicyXExpEnh.EffectiveDate + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '" + objPolicyXExpEnh.EffectiveDate + "'))";
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReader.Read())
                        {
                            sFlat = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));
                        }
                        objReader.Close();
                        if (sFlat == "P")
                            base.CreateAndSetElement(objOptionXmlElement, "Rate", objPolicyXExpEnh.ExposureRate.ToString() + "%");
                        else
                            base.CreateAndSetElement(objOptionXmlElement, "Rate", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXExpEnh.ExposureRate, base.ConnectionString, ClientId));
                        base.CreateAndSetElement(objOptionXmlElement, "ProRateAnnualPremium", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXExpEnh.PrAnnPremAmt, base.ConnectionString, ClientId));// string.Format("{0:C}", objPolicyXExpEnh.PrAnnPremAmt));
                        base.CreateAndSetElement(objOptionXmlElement, "AmendedDate", Conversion.GetDBDateFormat(objPolicyXExpEnh.AmendedDate, "MM/dd/yyyy"));
                        base.CreateAndSetElement(objOptionXmlElement, "PremiumAdjustment", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXExpEnh.PremAdjAmt, base.ConnectionString, ClientId));
                        base.CreateAndSetElement(objOptionXmlElement, "EffectiveDate", Conversion.GetDBDateFormat(objPolicyXExpEnh.EffectiveDate, "MM/dd/yyyy"));
                        base.CreateAndSetElement(objOptionXmlElement, "ExpirationDate", Conversion.GetDBDateFormat(objPolicyXExpEnh.ExpirationDate, "MM/dd/yyyy"));
                        base.CreateAndSetElement(objOptionXmlElement, "OrgLevel", sOrgName);
                        string sSessionId = string.Empty ;
                        if( p_objUniqueIds.ContainsKey( objPolicyXExpEnh.ExposureId ) )
                            sSessionId = (string)p_objUniqueIds[objPolicyXExpEnh.ExposureId];
                        //Commented by Anu Tennyson
                        //base.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                        //Commented by Anu Tennyson
                        base.CreateAndSetElement(objOptionXmlElement, "ExposureId", objPolicyXExpEnh.ExposureId.ToString());
                        if (objPolicyXExpEnh.ExposureId > 0)
                        {
                            objPositiveEntries.Add(objPolicyXExpEnh.ExposureId, objOptionXmlElement);
                            bSortingNecessary = true;
                        }
                        else if (objPolicyXExpEnh.ExposureId < 0)
                        {
                            objNegativeEntries.Add(objPolicyXExpEnh.ExposureId, objOptionXmlElement);
                            bSortingNecessary = true;
                        }

                        //Sumit - Start(03/16/2010) - MITS# 18229 -Append Exposure's UAR Session IDs.
                        string sUarSessionIds = string.Empty;
                        if (p_objUarIds.ContainsKey(objPolicyXExpEnh.ExposureId))
                            sUarSessionIds = (string)p_objUarIds[objPolicyXExpEnh.ExposureId];
                        //Sumit - Start(05/05/2010) - MITS# 20483 - Append Uar Schedule Session Ids
                        string sScheduleSessionIds = string.Empty;
                        if (p_objScheduleSessionIds.ContainsKey(objPolicyXExpEnh.ExposureId))
                        {
                            sScheduleSessionIds = (string)p_objScheduleSessionIds[objPolicyXExpEnh.ExposureId];
                        }

                            string[] arrSessionID = sSessionId.Split(new char[] { '|' });
                            sbExpAndUARSessionID = new StringBuilder();
                            string[] arrExpSessionID = null;
                            if (!string.IsNullOrEmpty(arrSessionID[0]))
                            {
                                arrExpSessionID = arrSessionID[0].Split(new char[] { '~' });
                            }
                            if (arrExpSessionID != null && arrExpSessionID.Length > 0 )
                            {
                                sbExpAndUARSessionID.Append(arrExpSessionID[0]);
                            }
                            sbExpAndUARSessionID.Append("~");
                            sbExpAndUARSessionID.Append(sUarSessionIds);
                            if (!string.IsNullOrEmpty(sScheduleSessionIds))
                            {
                                sbExpAndUARSessionID.Append("^");
                                sbExpAndUARSessionID.Append(sScheduleSessionIds);
                            }
                        base.CreateAndSetElement(objOptionXmlElement, "SessionId", sbExpAndUARSessionID.ToString());
                        base.CreateAndSetElement(objOptionXmlElement, "UarSessionIds", sUarSessionIds);
                        //Sumit-End

                        base.CreateAndSetElement(objOptionXmlElement, "ScheduleSessionIds", sScheduleSessionIds);
                        //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen STARTS
                        foreach (PolicyXUar objPolicyXUAR in objPolicyXExpEnh.PolicyXUarList)
                        {
                            if (sLob == "AL")
                            {
                                objVehicle.MoveTo(objPolicyXUAR.UarId);
                                if (!(iExposureAlreadyPresent.Contains(objVehicle.UnitId)))
                                {
                                    sbExposureAdded.Append(objVehicle.Vin);
                                    sbExposureAdded.Append("|");
                                    sbExposureAdded.Append(objVehicle.UnitId.ToString());
                                    sbExposureAdded.Append("~");
                                    iExposureAlreadyPresent.Add(objVehicle.UnitId);
                                }
                            }
                            else if (sLob == "PC")
                            {
                                objPropertyUnit.MoveTo(objPolicyXUAR.UarId);
                                if (!(iExposureAlreadyPresent.Contains(objPropertyUnit.PropertyId)))
                                {
                                    sbExposureAdded.Append(objPropertyUnit.Pin);
                                    sbExposureAdded.Append("|");
                                    sbExposureAdded.Append(objPropertyUnit.PropertyId.ToString());
                                    sbExposureAdded.Append("~");
                                    iExposureAlreadyPresent.Add(objPropertyUnit.PropertyId);
                                }
                            }
                        }
                        //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen ENDS

                    }
                }

                // Sort the XML based on the Exposure Id. The Exposure Id which are Positive are taken to be saved earlier,
                // The Negative ones are getting Inserted now, soe we need to have Exposure Id in the Order 1,2,3 -1,-2,-3
                if (bSortingNecessary)
                    base.SortXml(ref objExpListElement, objPositiveEntries, objNegativeEntries);

                base.ResetSysExData( objSysEx, "SelectedExposureId", "");
                base.ResetSysExData( objSysEx, "NewAddedExposureSessionId", "");
                base.ResetSysExData( objSysEx, "DeletedRowExposureId", "");

                base.ResetSysExData( objSysEx , "ExposureSum", dblExposureSum.ToString());
                base.ResetSysExData( objSysEx , "ExposureManualPremium", dblManualPremium.ToString());
                base.ResetSysExData(objSysEx, "ExposureCount", iIndex.ToString());
                //Sumit - Start(03/16/2010) - MITS# 18229
                base.ResetSysExData(objSysEx, "ExposureListGrid_UarSessionIds", "");
                //Sumit - End
                //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen STARTS
                if (!string.IsNullOrEmpty(sbExposureAdded.ToString()))
                {
                    sExposureAddedSessionID = Utilities.GenerateGuid();
                    xmlExposureAdded = new XmlDocument();
                    sExposureAdded = "<UARAdded>" + sbExposureAdded.ToString() + "</UARAdded>";
                    xmlExposureAdded.LoadXml(sExposureAdded);
                    objSessionManager.SetBinaryItem(sExposureAddedSessionID, Utilities.BinarySerialize(xmlExposureAdded.OuterXml), base.ClientId);
                    base.ResetSysExData(objSysEx, "UARDetailsAddedSessionID", sExposureAddedSessionID);
                }
                //Anu Tennysonn for MITS 18229 - UAR List Field on Coverages Screen ENDS
                //mona-start
                // npadhy Changing the Location of this Code as this is increasing the index. And this will Change the Exposure Count
                base.CreateElement(objExpListElement, "option", ref objOptionXmlElement);
                objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SYSEXDATA_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                objXmlAttribute = objSysEx.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                //objXmlAttribute = null;

                ////Manish Jain for multicurrency
                //string culture = CommonFunctions.GetCulture(0, CommonFunctions.NavFormType.None, base.ConnectionString);
                //Thread.CurrentThread.CurrentCulture = new CultureInfo(culture, false);


                base.CreateAndSetElement(objOptionXmlElement, "Type", "");
                base.CreateAndSetElement(objOptionXmlElement, "Description", "");
                base.CreateAndSetElement(objOptionXmlElement, "Status", "");
                base.CreateAndSetElement(objOptionXmlElement, "Amount", "0");
                base.CreateAndSetElement(objOptionXmlElement, "Rate", "");
                base.CreateAndSetElement(objOptionXmlElement, "ProRateAnnualPremium", string.Format("{0:C}", 0));
                base.CreateAndSetElement(objOptionXmlElement, "AmendedDate", "");
                base.CreateAndSetElement(objOptionXmlElement, "PremiumAdjustment", string.Format("{0:C}", 0));
                base.CreateAndSetElement(objOptionXmlElement, "EffectiveDate", "");
                base.CreateAndSetElement(objOptionXmlElement, "ExpirationDate", "");
                base.CreateAndSetElement(objOptionXmlElement, "OrgLevel", "");
                base.CreateAndSetElement(objOptionXmlElement, "SessionId", "");
                base.CreateAndSetElement(objOptionXmlElement, "ExposureId", "0");


                //mona-end
            }
            finally
            {
                objExpListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                objXmlAttribute = null;
                //Anu Tennyson for MITS 18229 STARTS
                objVehicle = null;
                objPropertyUnit = null;
                //Anu Tennyson for MITS 18229 Ends

            }
        }

        //Anu Tennyson --- Exposure
        //public XmlDocument GetExposure(SessionManager objSessionManager, int p_iExpsoureId, string p_sUniqueId, int p_iPolicyId, int p_iTransNumber , string p_sEffectiveDate , string p_sExpirationDate , int p_iState )
        //Sumit - 03/16/2010 - MITS# 18229 - Added DeletedUarRowIds,EditRowUarSessionIds
        //public XmlDocument GetExposure(SessionManager objSessionManager, int p_iExpsoureId, string p_sUniqueId, int p_iPolicyId, int p_iTransNumber, string p_sEffectiveDate, string p_sExpirationDate, int p_iState, string sFormName)
        public XmlDocument GetExposure(SessionManager objSessionManager, int p_iExpsoureId, string p_sUniqueId, int p_iPolicyId, int p_iTransNumber, string p_sEffectiveDate, string p_sExpirationDate, int p_iState, string sFormName, string sDeletedUarRowIds, string sEditRowUarSessionIds)
        //Anu Tennyson
        {
            PolicyXExpEnh objPolicyXExpEnh = null;
            PolicyEnh objPolicyEnh = null;
            //Sumit - Start(03/16/2010) - MITS# 18229
            Hashtable objExposuresUniqueIds = new Hashtable();
            string sUarID = string.Empty;
            string sUARRowID = string.Empty;
            string sState = string.Empty;
            string sStateCode = string.Empty;
            bool bIsSuccess = false;
            string sStateDesc = string.Empty;
            string sUARAndSchdSession = string.Empty;
            String[] arrSchd = null;
            String[] arrUARSchd = null;
            String[] arrUAR = null;
            //Sumit - End
            DbReader objReader = null;
            DbReader objReader2 = null;
            int iIndex = 0;
            PolicyExposureUITemp structPolicyExposure = new PolicyExposureUITemp();

            string sSQL = string.Empty;
            XmlElement objXMLElement = null;

            //Manish multicurrency
            LocalCache objCache = new LocalCache(base.ConnectionString,ClientId);
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
            

            objPolicyXExpEnh = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);

          
            
            if (! string.IsNullOrEmpty(p_sUniqueId))
            {
                // Load from the session.
                string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(p_sUniqueId)).ToString();

                XmlDocument objPropertyStore = new XmlDocument();
                objPropertyStore.LoadXml(sSessionRawData);

                objPolicyXExpEnh.PopulateObject( objPropertyStore );                 
                //Sumit - Start(03/16/2010) - MITS# 18229 -Update Exposure Children
                if (sFormName == "policyenhal" || sFormName == "policyenhpc")
                {
                    XmlDocument objReturnUarDoc = null;
                    XmlElement objUarRoot = null;
                    objExposuresUniqueIds.Clear();
                    arrUARSchd = sEditRowUarSessionIds.Split('$');
                    for (int iUARDtl = 0; iUARDtl < arrUARSchd.Length; iUARDtl++)
                    {
                        if (!String.IsNullOrEmpty(arrUARSchd[iUARDtl]))
                        {
                            if (arrUARSchd[iUARDtl].Contains("^"))
                            {
                                arrUAR = arrUARSchd[iUARDtl].Split('^');
                            }
                            String[] arrUar = null;
                            if (arrUAR != null)
                            {
                                arrUar = arrUAR[0].Split('|');
                            }
                            else
                            {
                                arrUar = arrUARSchd[0].Split('|');
                            }
                            for (int iUar = 0; iUar < arrUar.Length; iUar++)
                            {
                                if (!String.IsNullOrEmpty(arrUar[iUar]))
                                {
                                    sUarID = string.Empty;
                                    string sSessionRawUarData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrUar[iUar])).ToString();
                                    XmlDocument objPropStore = new XmlDocument();
                                    objPropStore.LoadXml(sSessionRawUarData);
                                    PolicyXUar objUpdateUAR = (PolicyXUar)base.DataModelFactory.GetDataModelObject("PolicyXUar", false);

                                    if (objPropStore.SelectSingleNode("/PolicyXUar/UarId") != null)
                                    {
                                        sUarID = objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString();
                                    }
                                    if (objPropStore.SelectSingleNode("/PolicyXUar/UarRowID") == null || Conversion.CastToType<int>(objPropStore.SelectSingleNode("/PolicyXUar/UarRowID").InnerText, out bIsSuccess) <= 0)
                                    {
                                        StartDocument(ref objReturnUarDoc, ref objUarRoot, "PolicyXUar");
                                        CreateAndSetElement(objUarRoot, "UarRowId", objUpdateUAR.UarRowId.ToString());
                                        CreateAndSetElement(objUarRoot, "PolicyId", objPolicyXExpEnh.PolicyId.ToString());
                                        CreateAndSetElement(objUarRoot, "ExposureId", objPolicyXExpEnh.ExposureId.ToString());
                                        CreateAndSetElement(objUarRoot, "UarId", sUarID);
                                        CreateAndSetElement(objUarRoot, "PolicyStatusCode", base.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());
                                        objUpdateUAR.PopulateObject(objReturnUarDoc);
                                        objPolicyXExpEnh.PolicyXUarList.Add(objUpdateUAR);

                                        int iUarRowID = objUpdateUAR.UarRowId;
                                        if (arrUAR != null)
                                        {
                                            if (arrUAR.Length > 1)
                                            {
                                                sUARAndSchdSession = arrUARSchd[iUARDtl];
                                            }
                                            else
                                            {
                                                sUARAndSchdSession = arrUar[iUar];
                                            }
                                        }
                                        else
                                        {
                                            sUARAndSchdSession = arrUar[iUar];
                                        }

                                        objExposuresUniqueIds.Add(iUarRowID, sUARAndSchdSession);
                                    }
                                    else
                                    {
                                        if (objPropStore.SelectSingleNode("/PolicyXUar/UarRowID") != null)
                                        {
                                            sUARRowID = objPropStore.SelectSingleNode("/PolicyXUar/UarRowID").InnerText.ToString();
                                        }
                                        int iUARRowID = Conversion.CastToType<int>(sUARRowID, out bIsSuccess);
                                        if (arrUAR != null)
                                        {
                                            if (arrUAR.Length > 1)
                                            {
                                                sUARAndSchdSession = arrUARSchd[iUARDtl];
                                            }
                                            else
                                            {
                                                sUARAndSchdSession = arrUar[iUar];
                                            }
                                        }
                                        else
                                        {
                                            sUARAndSchdSession = arrUar[iUar];
                                        }
                                        objExposuresUniqueIds.Add(iUARRowID, sUARAndSchdSession);
                                    }
                                }
                            }

                        }
                    }
                }
                //Sumit - End
            }
            else
            {
                // Load from the database.
                if (p_iExpsoureId > 0)
                    objPolicyXExpEnh.MoveTo(p_iExpsoureId);
            }
            //pmahli 3/22/2007 as the id with be over written by data model class
            //objPolicyXExpEnh.TransactionId = p_iTransNumber;
            objPolicyXExpEnh.PolicyId = p_iPolicyId ;

            if (p_iPolicyId > 0)
            {
                objPolicyEnh.MoveTo(p_iPolicyId);
            }
            else
            {
                objPolicyEnh.State = p_iState;
            }

            string sPolicyStatus = "";
            string sTermEffectiveDate = "";
            string sTermExpirationDate = "";
            string sTransactionType = "";
            string sTransactionStatus = "";
            string sTransactionDate = "";
            // Start Naresh MITS 9770 Amend when Rate was configured to be Fixed
            // This also fixed 9700
            string sAmendDate = "";
            // End Naresh MITS 9770 Amend when Rate was configured to be Fixed
            int iTransactionType = 0;
            int iTermNumber = 0;
            int iTermSeq = 0;
            string sCancelDate = "";

            if (objPolicyEnh.PolicyXTransEnhList.Count == 0)
            {
                if (objPolicyEnh.PolicyId == 0)
                    sPolicyStatus = "Q";
                else
                {
                    sPolicyStatus = base.CCacheFunctions.GetShortCode(objPolicyEnh.PolicyStatusCode);
                }
                sTermEffectiveDate = Conversion.GetDate(p_sEffectiveDate);
                sTermExpirationDate = Conversion.GetDate(p_sExpirationDate);
            }
            else
            {
                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                    {
                        iTransactionType = objPolicyXTransEnh.TransactionType;
                        sPolicyStatus = base.CCacheFunctions.GetShortCode(objPolicyXTransEnh.PolicyStatus);
                        sTransactionType = base.CCacheFunctions.GetShortCode(iTransactionType);
                        sTransactionStatus = base.CCacheFunctions.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                        //pmahli MITS 9817 6/19/2007    
                        sTransactionDate = objPolicyXTransEnh.EffectiveDate;
                        // Start Naresh MITS 9770 Amend when Rate was configured to be Fixed
                        // This also fixed 9700
                        sAmendDate = objPolicyXTransEnh.EffectiveDate;
                        // End Naresh MITS 9770 Amend when Rate was configured to be Fixed
                        iTermNumber = objPolicyXTransEnh.TermNumber;
                        iTermSeq = objPolicyXTransEnh.TermSeqAlpha;
                        break;
                    }
                }

                sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + objPolicyEnh.PolicyId
                    + " AND TERM_NUMBER = " + iTermNumber + " AND SEQUENCE_ALPHA = " + iTermSeq;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    sTermEffectiveDate = objReader.GetString("EFFECTIVE_DATE");
                    sTermExpirationDate = objReader.GetString("EXPIRATION_DATE");
                    sCancelDate = objReader.GetString("CANCEL_DATE");
                }
                objReader.Close();
            }

            int iSequenceAlphaMax = 0 ;
            double dblPrevFullPrem = 0;
            double dblPrevPrPrem = 0;
            string sOtherType = string.Empty;
            bool bSkip = false;

            if (p_iExpsoureId > 0)
            {
                if (sTransactionType == "AU")
                {
                    sSQL = " SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objPolicyEnh.PolicyId 
                        +   " AND EXPOSURE_COUNT=" + objPolicyXExpEnh.ExposureCount 
                        +   " AND SEQUENCE_ALPHA < " + objPolicyXExpEnh.SequenceAlpha 
                        +   " AND TERM_NUMBER=" + objPolicyXExpEnh.TermNumber 
                        +   " AND TRANSACTION_TYPE <> " + base.CCacheFunctions.GetCodeIDWithShort( "AU" , "POLICY_TXN_TYPE") ;
                    objReader = DbFactory.GetDbReader( base.ConnectionString , sSQL );
                    if( objReader.Read() )
                        iSequenceAlphaMax = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                    objReader.Close();

                    sSQL = " SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objPolicyEnh.PolicyId
                        + " AND EXPOSURE_COUNT=" + objPolicyXExpEnh.ExposureCount
                        + " AND TERM_NUMBER=" + objPolicyXExpEnh.TermNumber
                        + " AND SEQUENCE_ALPHA=" + iSequenceAlphaMax;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        dblPrevFullPrem = Conversion.ConvertObjToDouble(objReader.GetValue("FULL_ANN_PREM_AMT"), base.ClientId);
                        dblPrevPrPrem = Conversion.ConvertObjToDouble(objReader.GetValue("PR_ANN_PREM_AMT"), base.ClientId);
                    }
                    objReader.Close();                
                }
                else
                {
                    sSQL = " SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_EXP_ENH WHERE POLICY_ID="
                        + objPolicyEnh.PolicyId + " AND EXPOSURE_COUNT=" + objPolicyXExpEnh.ExposureCount
                        + " AND SEQUENCE_ALPHA < " + objPolicyXExpEnh.SequenceAlpha + " AND TERM_NUMBER="
                        + objPolicyXExpEnh.TermNumber;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                        iSequenceAlphaMax = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                    objReader.Close();

                    sSQL = " SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objPolicyEnh.PolicyId
                        + " AND EXPOSURE_COUNT=" + objPolicyXExpEnh.ExposureCount
                        + " AND TERM_NUMBER=" + objPolicyXExpEnh.TermNumber
                        + " AND SEQUENCE_ALPHA=" + iSequenceAlphaMax;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sSQL = " SELECT * FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), base.ClientId);
                        objReader2 = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        // Commented by Naresh
                        //if (objReader.Read())
                        //{
                        sOtherType = base.CCacheFunctions.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), base.ClientId));
                            if (sOtherType == "AU")
                            {
                                dblPrevFullPrem = 0;
                                dblPrevPrPrem = 0;
                                bSkip = true;
                            }
                        //}
                        objReader2.Close();
                        if (!bSkip)
                        {
                            dblPrevFullPrem = Conversion.ConvertObjToDouble(objReader.GetValue("FULL_ANN_PREM_AMT"), base.ClientId);
                            dblPrevPrPrem = Conversion.ConvertObjToDouble(objReader.GetValue("PR_ANN_PREM_AMT"), base.ClientId);
                        }                    
                    }
                    objReader.Close();
                }
            }
            EnableControls(ref structPolicyExposure);
            if(sTransactionType == "AU")
            {
                if (sTransactionStatus == "PR")
                {
                    EnableControls(ref structPolicyExposure);
                    if (objPolicyXExpEnh.ExposureCode != 0)
                    {
                        structPolicyExposure.ExpTypeEnabled = false;
                    }
                    objPolicyXExpEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                    if (objPolicyXExpEnh.ExposureCode == 0)
                    {
                        objPolicyXExpEnh.TermNumber = iTermNumber;
                        objPolicyXExpEnh.TransactionId = p_iTransNumber;
                        objPolicyXExpEnh.PolicyId = objPolicyEnh.PolicyId;
                        objPolicyXExpEnh.RenewableFlag = true;
                        objPolicyXExpEnh.TransactionType = iTransactionType;
                        objPolicyXExpEnh.SequenceAlpha = 1;
                        if (sCancelDate != "")
                        {
                            objPolicyXExpEnh.ExpirationDate = sCancelDate;
                        }
                        else
                        {
                            objPolicyXExpEnh.ExpirationDate = sTermExpirationDate;
                        }
                        // Start Naresh Add the Exposure Count 
                        // Putting the Exposure Count from Datamodel was incrementing the 
                        // Exposure Count at all times, during Convert to Policy etc.
                        objPolicyXExpEnh.ExposureCount = GetExpCount(objPolicyEnh);
                        // End Naresh Exposure Count
                    }
                    if (objPolicyXExpEnh.SequenceAlpha == 1)
                    {
                        structPolicyExposure.EffDateEnabled = true;
                        structPolicyExposure.ExpDateEnabled = true;
                    }
                    else
                    {
                        structPolicyExposure.EffDateEnabled = false;
                        structPolicyExposure.ExpDateEnabled = false;
                    }
                }
                else if (sTransactionStatus == "OK")
                {
                    DisableControls(ref structPolicyExposure);
                    structPolicyExposure.EffDateEnabled = false;
                    structPolicyExposure.ExpDateEnabled = false;
                }
            }
            else
            {
                switch (sPolicyStatus)
                {
                    // Converted
                    case "CV" :
                        DisableControls(ref structPolicyExposure);
                        structPolicyExposure.EffDateEnabled = false;
                        structPolicyExposure.ExpDateEnabled = false;
                        break;
                    // New or Quote
                    case "Q":
                        objPolicyXExpEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                        objPolicyXExpEnh.EffectiveDate = sTermEffectiveDate;
                        objPolicyXExpEnh.ExpirationDate = sTermExpirationDate;
                        EnableControls(ref structPolicyExposure);
                        structPolicyExposure.EffDateEnabled = false;
                        structPolicyExposure.ExpDateEnabled = false;
                        break;
                    // In Effect
                    case "I":
                        if ((sTransactionType == "EN") &&
                           (sTransactionStatus == "PR") &&
                           (objPolicyXExpEnh.Status != base.CCacheFunctions.GetCodeIDWithShort(SC_DELETED, TBL_COVEXP)))
                        {
                            EnableControls(ref structPolicyExposure);
                            if ((sTransactionType == "EN") &&
                               (objPolicyXExpEnh.ExposureCode != 0))
                            {
                                structPolicyExposure.ExpTypeEnabled = false;
                            }
                            objPolicyXExpEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                            if (objPolicyXExpEnh.ExposureCode == 0)
                            {
                                // MITS 9770 Reopen Fixed
                                sAmendDate = "";
                                objPolicyXExpEnh.EffectiveDate = sTransactionDate;
                                objPolicyXExpEnh.ExpirationDate = sTermExpirationDate;
                                objPolicyXExpEnh.TermNumber = iTermNumber;
                                objPolicyXExpEnh.RenewableFlag = true;
                                objPolicyXExpEnh.TransactionType = iTransactionType;
                                objPolicyXExpEnh.SequenceAlpha = 1;
                                objPolicyXExpEnh.TransactionId = p_iTransNumber;
                                objPolicyXExpEnh.PolicyId = objPolicyEnh.PolicyId;
                                // Start Naresh Add the Exposure Count 
                                // Putting the Exposure Count from Datamodel was incrementing the 
                                // Exposure Count at all times, during Convert to Policy etc.
                                objPolicyXExpEnh.ExposureCount = GetExpCount(objPolicyEnh);
                                // End Naresh Exposure Count
                            }
                            // MITS 9770 Reopen Fixed
                            else
                            {
                                if (LocalCache.GetShortCode(objPolicyXExpEnh.TransactionType) == "EN" && objPolicyXExpEnh.SequenceAlpha == 1)
                                    sAmendDate = "";
                            }
                        }
                        else
                        {
                            DisableControls(ref structPolicyExposure);
                        }
                        structPolicyExposure.EffDateEnabled = false;
                        structPolicyExposure.ExpDateEnabled = false;
                        break;
                    // Provisionally Renewed
                    case "PR":
                        EnableControls(ref structPolicyExposure);
                        if (objPolicyXExpEnh.Status == 0)
                        {
                            objPolicyXExpEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                        }
                        if (objPolicyXExpEnh.ExposureCode == 0)
                        {
                            objPolicyXExpEnh.EffectiveDate = sTermEffectiveDate;
                            objPolicyXExpEnh.ExpirationDate = sTermExpirationDate;
                            objPolicyXExpEnh.TermNumber = iTermNumber;
                            objPolicyXExpEnh.RenewableFlag = true;
                            objPolicyXExpEnh.TransactionType = iTransactionType;
                            objPolicyXExpEnh.SequenceAlpha = 1;
                            objPolicyXExpEnh.TransactionId = p_iTransNumber;
                            objPolicyXExpEnh.PolicyId = objPolicyEnh.PolicyId;
                            // Start Naresh Add the Exposure Count 
                            // Putting the Exposure Count from Datamodel was incrementing the 
                            // Exposure Count at all times, during Convert to Policy etc.
                            objPolicyXExpEnh.ExposureCount = GetExpCount(objPolicyEnh);
                            // End Naresh Exposure Count
                        }
                        structPolicyExposure.EffDateEnabled = false;
                        structPolicyExposure.ExpDateEnabled = false;
                        break;
                    case "C":
                    case "R":
                    case "E":
                    default:
                        //pmahli MITS 9824 6/19/2007 Controls disabled for default case
                        DisableControls(ref structPolicyExposure);
                        structPolicyExposure.EffDateEnabled = false;
                        structPolicyExposure.ExpDateEnabled = false;
                        break;
                }                
            }

            XmlDocument objReturnDoc = null ;
            XmlElement objRoot = null ;
            XmlElement objSuppNode = null ;
            XmlElement objExposureNode = null;
            XmlElement objExposureUI = null;
            XmlElement objExpListElement = null;
            XmlElement objOptionXmlElement = null;

            base.StartDocument(ref objReturnDoc, ref objRoot, "Exposure");
            base.CreateElement( objRoot , "SuppView" , ref objSuppNode );
            base.CreateElement(objRoot, "ExposureData", ref objExposureNode);
            base.CreateElement(objRoot, "ExposureUIStatus", ref objExposureUI);
            base.CreateAndSetElement(objExposureUI, "ExceptionEnable", structPolicyExposure.ExceptionsEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "RemarksEnable", structPolicyExposure.RemarksEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "CovTypeCodeEnable", structPolicyExposure.CovTypeCodeEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "EffDateEnable", structPolicyExposure.EffDateEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ExpAmountEnable", structPolicyExposure.ExpAmountEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ExpDateEnable", structPolicyExposure.ExpDateEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ExpRateEnable", structPolicyExposure.ExpRateEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "BaseRateEnable", structPolicyExposure.BaseRateEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ExpTypeEnable", structPolicyExposure.ExpTypeEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "HierachyLevelEnable", structPolicyExposure.HierarchyLevelEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "PremAmtEnable", structPolicyExposure.PremAmtEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "StatusEnable", structPolicyExposure.StatusEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "OkEnable", structPolicyExposure.OkEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "CancelEnable", structPolicyExposure.CancelEnabled.ToString());
            //Anu Tennyson for MITS 18229 STARTS
            base.CreateAndSetElement(objExposureUI, "ALUARDetailsAddButtonEnabled", structPolicyExposure.ALUARDetailsAddButtonEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ALUARDetailsDeleteButtonEnabled", structPolicyExposure.ALUARDetailsDeleteButtonEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "PCUARDetailsAddButtonEnabled", structPolicyExposure.PCUARDetailsAddButtonEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "PCUARDetailsDeleteButtonEnabled", structPolicyExposure.PCUARDetailsDeleteButtonEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ScheduleAddButtonEnabled", structPolicyExposure.ScheduleAddButtonEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ScheduleEditButtonEnabled", structPolicyExposure.ScheduleEditButtonEnabled.ToString());
            base.CreateAndSetElement(objExposureUI, "ScheduleDeleteButtonEnabled", structPolicyExposure.ScheduleDeleteButtonEnabled.ToString());
            //Anu Tennyson for MITS 18229 ENDS
            // Start Naresh MITS 9770 Amend when Rate was configured to be Fixed
            // This also fixed 9700
            // MITS 9770 Reopen Fixed
            if(sAmendDate != "")
                base.CreateAndSetElement(objExposureUI, "AmendDate", Conversion.ToDate(sAmendDate).ToString("MM/dd/yyyy"));
            else
                base.CreateAndSetElement(objExposureUI, "AmendDate", "");
            // End Naresh MITS 9770 Amend when Rate was configured to be Fixed
            base.CreateAndSetElement(objExposureUI, "PrevProRataPrem", dblPrevPrPrem.ToString());
            base.CreateAndSetElement(objExposureUI, "FullAnnualPrem", dblPrevFullPrem.ToString());
            base.CreateAndSetElement(objExposureUI, "TransactionType", sTransactionType);
            base.CreateAndSetElement(objExposureUI, "PolicyStatus", sPolicyStatus);
            //Anu Tennyson for MITS 18229 1/20/2010 STARTS--exposure - For Vehicle UAR 

            //Manish multicurrency
            objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), ref sShortCode, ref sDesc);
            sCurrency = sDesc.Split('|')[1];
            base.CreateAndSetElement(objRoot, "BaseCurrencyType", sCurrency);

            XmlElement objExpdetailListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;

            //Sumit - Start(04/14/2010) - MITS# 18229 - Update Exposure data for UAR
            if (sFormName == "policyenhpc")
            {
                PropertyUnit objProperty = null;
                objProperty = (PropertyUnit)base.DataModelFactory.GetDataModelObject("PropertyUnit", false);

                string sPropertyIDs = string.Empty;
                if (p_iExpsoureId > 0)
                {
                    ArrayList arrDeletedUarRowIds = new ArrayList();

                    string[] arrDeletedUarRowIDs = sDeletedUarRowIds.Split('|');

                    for (int i = 0; i < arrDeletedUarRowIDs.Length; i++)
                    {
                        arrDeletedUarRowIds.Add(arrDeletedUarRowIDs[i]);
                    }

                    foreach (PolicyXUar itemUar in objPolicyXExpEnh.PolicyXUarList)
                    {
                        if (arrDeletedUarRowIds.Contains(itemUar.UarRowId.ToString()))
                        {
                            objPolicyXExpEnh.PolicyXUarList.Remove(itemUar.UarRowId);
                        }
                    }
                }
                objExpdetailListElement = (XmlElement)objReturnDoc.SelectSingleNode("/Instance/Document/Exposure/PCUARDetailsList");
                if (objExpdetailListElement != null)
                    objExpdetailListElement.ParentNode.RemoveChild(objExpdetailListElement);

                base.CreateElement(objReturnDoc.DocumentElement, "PCUARDetailsList", ref objExpdetailListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objExpdetailListElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "PropertyID", "Property ID");
                base.CreateAndSetElement(objListHeadXmlElement, "Address1", "Address 1");
                base.CreateAndSetElement(objListHeadXmlElement, "Address2", "Address 2");
                base.CreateAndSetElement(objListHeadXmlElement, "Address3", "Address 3");
                base.CreateAndSetElement(objListHeadXmlElement, "Address4", "Address 4");
                base.CreateAndSetElement(objListHeadXmlElement, "City", "City");
                base.CreateAndSetElement(objListHeadXmlElement, "State", "State");

                foreach (PolicyXUar objPolicyXUarDtl in objPolicyXExpEnh.PolicyXUarList)
                {
                    base.CreateElement(objExpdetailListElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_PCDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");
                    objProperty.MoveTo(objPolicyXUarDtl.UarId);

                    int iState = objProperty.StateId;
                    base.LocalCache.GetStateInfo(iState, ref sStateCode, ref sStateDesc);
                    sState = sStateCode + " " + sStateDesc;

                    base.CreateAndSetElement(objOptionXmlElement, "PropertyID", objProperty.Pin);
                    base.CreateAndSetElement(objOptionXmlElement, "Address1", objProperty.Addr1);
                    base.CreateAndSetElement(objOptionXmlElement, "Address2", objProperty.Addr2);
                    base.CreateAndSetElement(objOptionXmlElement, "Address3", objProperty.Addr3);
                    base.CreateAndSetElement(objOptionXmlElement, "Address4", objProperty.Addr4);
                    base.CreateAndSetElement(objOptionXmlElement, "City", objProperty.City);
                    base.CreateAndSetElement(objOptionXmlElement, "State", sState);
                    string sSessionId = string.Empty;
                    if (objExposuresUniqueIds.ContainsKey(objPolicyXUarDtl.UarRowId))
                    {
                        sSessionId = (string)objExposuresUniqueIds[objPolicyXUarDtl.UarRowId];
                    }
                    base.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                    base.CreateAndSetElement(objOptionXmlElement, "UarRowId", objPolicyXUarDtl.UarRowId.ToString());
                    base.CreateAndSetElement(objOptionXmlElement, "ScheduleSessionIds", "");

                    //Adding Property IDs in hidden node.
                    if (sPropertyIDs == "")
                        sPropertyIDs = objProperty.PropertyId.ToString();
                    else
                        sPropertyIDs = sPropertyIDs + "|" + objProperty.PropertyId.ToString();
                }

                base.CreateElement(objExpdetailListElement, "option", ref objExposureUI);
                objExposureUI.SetAttribute("ref", Constants.INSTANCE_PCDOCUMENT_PATH + objExposureUI.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                base.CreateAndSetElement(objExposureUI, "PropertyID", "");
                base.CreateAndSetElement(objExposureUI, "Address1", "");
                base.CreateAndSetElement(objExposureUI, "Address2", "");
                base.CreateAndSetElement(objExposureUI, "Address3", "");
                base.CreateAndSetElement(objExposureUI, "Address4", "");
                base.CreateAndSetElement(objExposureUI, "City", "");
                base.CreateAndSetElement(objExposureUI, "State", "");
                base.CreateAndSetElement(objExposureUI, "SessionId", "");
                base.CreateAndSetElement(objExposureUI, "UarRowId", "");

                objXmlAttribute = objReturnDoc.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objExposureUI.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                //Hidden node for Storing Property Ids
                base.CreateAndSetElement(objReturnDoc.DocumentElement, "PCUARDetailsListGridUarIds", sPropertyIDs);

                //Start:Sumit (10/19/2010) -MITS# 21996 Hidden node for Storing Category code field.
                int iFieldID = GetFieldID("PROPERTY_UNIT", "CATEGORY_CODE", 12);
                base.CreateAndSetElement(objReturnDoc.DocumentElement, "CategoryFieldID", iFieldID.ToString());
                //End:Sumit
            }
            //Sumit - End
            if (sFormName == "policyenhal")
            {
                //Sumit - Start(03/16/2010) - MITS# 18229 - Udpate Exposure data for UAR
                Vehicle objVehicle = null;
                objVehicle = (Vehicle)base.DataModelFactory.GetDataModelObject("Vehicle", false);

                string sVehicleIDs = string.Empty;
                if (p_iExpsoureId > 0)
                {
                    ArrayList arrDeletedUarRowIds = new ArrayList();

                    string[] arrDeletedUarRowIDs = sDeletedUarRowIds.Split('|');

                    for (int i = 0; i < arrDeletedUarRowIDs.Length; i++)
                    {
                        arrDeletedUarRowIds.Add(arrDeletedUarRowIDs[i]);
                    }

                    foreach (PolicyXUar itemUar in objPolicyXExpEnh.PolicyXUarList)
                    {
                        if (arrDeletedUarRowIds.Contains(itemUar.UarRowId.ToString()))
                        {
                            objPolicyXExpEnh.PolicyXUarList.Remove(itemUar.UarRowId);
                        }
                    }
                }
                objExpdetailListElement = (XmlElement)objReturnDoc.SelectSingleNode("/Instance/Document/Exposure/ALUARDetailsList");
                if (objExpdetailListElement != null)
                    objExpdetailListElement.ParentNode.RemoveChild(objExpdetailListElement);

                base.CreateElement(objReturnDoc.DocumentElement, "ALUARDetailsList", ref objExpdetailListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objExpdetailListElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleID", "Vehicle ID");
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleMake", "Vehicle Make");
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleModel", "Vehicle Model");
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleYear", "Vehicle Year");
                base.CreateAndSetElement(objListHeadXmlElement, "State", "State");
                base.CreateAndSetElement(objListHeadXmlElement, "LicenseNumber", "License Number");
                //base.CreateAndSetElement(objListHeadXmlElement, "UnitType", "Unit Type");


                foreach (PolicyXUar objPolicyXUarDtl in objPolicyXExpEnh.PolicyXUarList)
                {
                    base.CreateElement(objExpdetailListElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_ALDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");
                    objVehicle.MoveTo(objPolicyXUarDtl.UarId);

                    int iState = objVehicle.StateRowId;
                    base.LocalCache.GetStateInfo(iState, ref sStateCode, ref sStateDesc);
                    sState = sStateCode + " " + sStateDesc;

                    base.CreateAndSetElement(objOptionXmlElement, "VehicleID", objVehicle.Vin);
                    base.CreateAndSetElement(objOptionXmlElement, "VehicleMake", objVehicle.VehicleMake);
                    base.CreateAndSetElement(objOptionXmlElement, "VehicleModel", objVehicle.VehicleModel);
                    base.CreateAndSetElement(objOptionXmlElement, "VehicleYear", objVehicle.VehicleYear.ToString());
                    base.CreateAndSetElement(objOptionXmlElement, "State", sState);
                    base.CreateAndSetElement(objOptionXmlElement, "LicenseNumber", objVehicle.LicenseNumber);
                    //base.CreateAndSetElement(objOptionXmlElement, "UnitType", base.LocalCache.GetShortCode(objVehicle.UnitTypeCode));
                    string sSessionId = string.Empty;
                    if (objExposuresUniqueIds.ContainsKey(objPolicyXUarDtl.UarRowId))
                        sSessionId = (string)objExposuresUniqueIds[objPolicyXUarDtl.UarRowId];
                    base.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                    base.CreateAndSetElement(objOptionXmlElement, "UarRowId", objPolicyXUarDtl.UarRowId.ToString());

                    //Adding Vehicle IDs in hidden node.
                    if (sVehicleIDs == "")
                        sVehicleIDs = objVehicle.UnitId.ToString();
                    else
                        sVehicleIDs = sVehicleIDs + "|" + objVehicle.UnitId.ToString();
                }

                base.CreateElement(objExpdetailListElement, "option", ref objExposureUI);
                objExposureUI.SetAttribute("ref", Constants.INSTANCE_ALDOCUMENT_PATH + objExposureUI.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                base.CreateAndSetElement(objExposureUI, "VehicleID", "");
                base.CreateAndSetElement(objExposureUI, "VehicleMake", "");
                base.CreateAndSetElement(objExposureUI, "VehicleModel", "");
                base.CreateAndSetElement(objExposureUI, "VehicleYear", "");
                base.CreateAndSetElement(objExposureUI, "State", "");
                base.CreateAndSetElement(objExposureUI, "LicenseNumber", "");
                //base.CreateAndSetElement(objExposureUI, "UnitType", "");
                base.CreateAndSetElement(objExposureUI, "SessionId", "");
                base.CreateAndSetElement(objExposureUI, "UarRowId", "");

                objXmlAttribute = objReturnDoc.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objExposureUI.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                //Hidden node for Storing Vehicle Ids
                base.CreateAndSetElement(objReturnDoc.DocumentElement, "ALUARDetailsListGridUarIds", sVehicleIDs);
                //Sumit - End

                //Start:Sumit (10/19/2010) -MITS# 21996 Hidden node for Storing Vehicle Type field.
                int iFieldID = GetFieldID("VEHICLE", "UNIT_TYPE_CODE", 5);
                base.CreateAndSetElement(objReturnDoc.DocumentElement, "UnitFieldID", iFieldID.ToString());
                //End:Sumit
            }

            XmlDocument objSupp = objPolicyXExpEnh.Supplementals.ViewXml ;

            foreach (XmlNode objNode in objSupp.SelectNodes("//control"))
            {
                XmlAttribute objRefAttribute = objNode.Attributes["ref"];
                if (objRefAttribute != null)
                {
                    objRefAttribute.InnerText = objRefAttribute.InnerText.Replace("/Instance/*/Supplementals", "/Instance/Document/Exposure/ExposureData/PolicyXExpEnh/Supplementals");
                }
                //Raman : Adding an onblur event to enable UI Ok button on all supplemental controls
                objXMLElement = (XmlElement)objNode;
                if (sPolicyStatus == "Q")
                {
                    objXMLElement.SetAttribute("readonly", "false");
                }
                else
                {
                    if ((sTransactionType == "AU" || sTransactionType == "EN" || sTransactionType == "RN") && sTransactionStatus == "PR")
                    {
                        objXMLElement.SetAttribute("readonly", "false");
                    }
                    else
                    {
                        objXMLElement.SetAttribute("readonly", "true");
                    }
                }
                objXMLElement.SetAttribute("onchange", "document.getElementById('btnExpsoureOk').disabled=false;setDataChanged(true);");
            }

            XmlDocument objExposure = new XmlDocument();
            XmlDocument objSerilzationXMl = new XmlDocument();
            objSerilzationXMl.LoadXml("<PolicyXExpEnh><Supplementals/></PolicyXExpEnh>");
            objExposure.LoadXml(objPolicyXExpEnh.SerializeObject(objSerilzationXMl));

            objSuppNode.AppendChild(objReturnDoc.ImportNode((XmlNode)objSupp.DocumentElement, true));
            objExposureNode.AppendChild(objReturnDoc.ImportNode((XmlNode)objExposure.DocumentElement, true));

          

            return objReturnDoc;

        }
        //Anu Tennyson for MITS 18229 1/20/2010 ENDS--exposure - For Vehicle UAR

        // Call Rate
        public XmlDocument CalculateRate(XmlDocument p_objXmlIn)
        {
            PolicyXExpEnh objPolicyXExpEnh;
            PolicyEnh objPolicyEnh = null;
            //Sumit - Start(03/16/2010) - MITS# 18229
            bool m_bIsSuccess = false;
            //XmlDocument p_objXmlOut = new XmlDocument();
            //Sumit - End
            //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            string sPolicyType = string.Empty;
            int iPolicyType = 0;
            //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            //Anu Tennyson for MITS 18229 : Prem Calc STARTS
            string sOrgH = string.Empty;
            //Anu Tennyson for MITS 18229 : Prem Calc ENDS
            objPolicyXExpEnh = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);

            string sXmlLoad = p_objXmlIn.SelectSingleNode("/Exposure/ExposureData").FirstChild.OuterXml;
            XmlDocument objXmlLoad = new XmlDocument();
            objXmlLoad.LoadXml(sXmlLoad);
            objPolicyXExpEnh.PopulateObject(objXmlLoad);
            if( objPolicyXExpEnh.PolicyId > 0 )
                objPolicyEnh.MoveTo(objPolicyXExpEnh.PolicyId);
            else
                //Sumit - Start(03/16/2010) - MITS# 18229 -Updated Xpath
                //objPolicyEnh.State = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//State").InnerText);
                if (p_objXmlIn.SelectSingleNode("/Exposure/State") != null)
                {
                    objPolicyEnh.State = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("/Exposure/State").InnerText, out m_bIsSuccess);
                }
                else
                {
                    if (p_objXmlIn.SelectSingleNode("//State") != null)
                    {
                        objPolicyEnh.State = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//State").InnerText, out m_bIsSuccess);
                    }
                }
            //Sumit - End
            //Anu Tennyson for MITS 18229 : Prem Calc STARTS
            sOrgH = p_objXmlIn.SelectSingleNode("/Exposure/OrgHierarchy").InnerText;
            //Anu Tennyson for MITS 18229 : Prem Calc Ends
            //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            sPolicyType = p_objXmlIn.SelectSingleNode("/Exposure/PolicyType").InnerText;
            iPolicyType = base.LocalCache.GetCodeId(sPolicyType, "POLICY_LOB");
            //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            DateTime reinstDate = DateTime.MinValue;
            string sTransactionType = base.CCacheFunctions.GetShortCode(objPolicyXExpEnh.TransactionType);
            //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            //base.Rate(ref objPolicyXExpEnh, objPolicyEnh.State, base.RoundFlag, objPolicyXExpEnh.PrAnnPremAmt, objPolicyXExpEnh.FullAnnPremAmt, sTransactionType, reinstDate);
            //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
            //base.Rate(ref objPolicyXExpEnh, objPolicyEnh.State, base.RoundFlag, objPolicyXExpEnh.PrAnnPremAmt, objPolicyXExpEnh.FullAnnPremAmt, sTransactionType, reinstDate, iPolicyType);
            //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
            //Anu Tennyson for MITS 18229 : STARTS Prem Calc STARTS
            base.Rate(ref objPolicyXExpEnh, objPolicyEnh.State, base.RoundFlag, objPolicyXExpEnh.PrAnnPremAmt, objPolicyXExpEnh.FullAnnPremAmt, sTransactionType, reinstDate, iPolicyType, sOrgH);
            //Anu Tennyson for MITS 18229 : STARTS Prem Calc ENDS
            //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            //Sumit - Start(03/16/2010) - MITS# 18229 - Commented as Binding data is set to true now.
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;

            base.StartDocument(ref objReturnDoc, ref objRoot, "RateDetails");
            base.CreateAndSetElement(objRoot, "Rate", objPolicyXExpEnh.ExposureRate.ToString());
            base.CreateAndSetElement(objRoot, "BaseRate", objPolicyXExpEnh.ExposureBaseRate.ToString());
            base.CreateAndSetElement(objRoot, "PremAdj", objPolicyXExpEnh.PremAdjAmt.ToString());
            base.CreateAndSetElement(objRoot, "ProRataAnnualPrem", objPolicyXExpEnh.PrAnnPremAmt.ToString());
            base.CreateAndSetElement(objRoot, "FullAnnualPrem", objPolicyXExpEnh.FullAnnPremAmt.ToString());

            return objReturnDoc;
            //Sumit - End


        }
        public XmlDocument SetExposure(SessionManager objSessionManager, XmlDocument p_objPropertyStore )
        {
            try
            {

                //string sEffDateUI = p_objPropertyStore.SelectSingleNode("//Exposure/EffDate").InnerText;  Commented by csingh7 for MITS 20721
                //string sExpDateUI = p_objPropertyStore.SelectSingleNode("//Exposure/ExpDate").InnerText;  Commented by csingh7 for MITS 20721
                string sEffDateUI = p_objPropertyStore.SelectSingleNode("//EffectiveDate").InnerText;  //Added by csingh7 for MITS 20721
                string sExpDateUI = p_objPropertyStore.SelectSingleNode("//ExpirationDate").InnerText; //Added by csingh7 for MITS 20721
                int iTransId = Convert.ToInt32(p_objPropertyStore.SelectSingleNode("//Exposure/TransId").InnerText);
                int iPolId = Convert.ToInt32(p_objPropertyStore.SelectSingleNode("//Exposure/PolId").InnerText);
                XmlDocument p_objPropertyStoreNew = new XmlDocument();

                p_objPropertyStoreNew.LoadXml(p_objPropertyStore.SelectSingleNode("/Exposure/ExposureData/PolicyXExpEnh").OuterXml);

                XmlDocument objReturnDoc = null;
                XmlElement objRoot = null;
                base.StartDocument(ref objReturnDoc, ref objRoot, "SetExposure");

                this.ValidateFields(p_objPropertyStoreNew, sEffDateUI, sExpDateUI, iTransId, iPolId);

                string sUniqueId = Utilities.GenerateGuid();
                objSessionManager.SetBinaryItem(sUniqueId, Utilities.BinarySerialize(p_objPropertyStoreNew.OuterXml), base.ClientId);


                base.CreateAndSetElement(objRoot, "SessionId", sUniqueId);
                return objReturnDoc;
            }
            // Added catch block csingh7 for MITS 20721 , 20722 : Start
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Exposure.SetExposure.Error", base.ClientId), p_objEx);
            }
            // Added catch block csingh7 for MITS 20721 , 20722 : End
            finally
            {
            }
        }

        /// <summary>
        /// Author:Sumit Kumar
        /// Date: 03/16/2010
        /// MITS# : 18229
        /// Description: This method populate the Exposure Child list and display the UAR data on popup screen.
        /// </summary>
        /// <param name="objSessionManager">Session Manager to handle the session.</param>
        /// <param name="p_objPropertyStore">XML Document containing the infrmation from screen.</param>
        /// <returns>XML document which is used to render the property UAR Details Grid.</returns>

        public XmlDocument RenderVehicleUARDetail(SessionManager objSessionManager, XmlDocument p_objPropertyStore)
        {
            Hashtable objExposuresUniqueIds = new Hashtable();
            XmlElement objUARListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;

            string sState = string.Empty;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;

            try
            {
                bool m_bIsSuccess = false;
                int iDeletedUarRowId = Conversion.CastToType<int>(p_objPropertyStore.SelectSingleNode("/Exposure/DeletedUarRowId").InnerText, out m_bIsSuccess);
                int iExposureId = Conversion.CastToType<int>(p_objPropertyStore.SelectSingleNode("/Exposure/ExposureData/PolicyXExpEnh/ExposureId").InnerText, out m_bIsSuccess);
                XmlNode objUarCollectionNode = p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsList");
                PolicyXExpEnh objUpdateUAR = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                Vehicle objVehicle = (Vehicle)base.DataModelFactory.GetDataModelObject("Vehicle", false);
                PolicyXUar objUARDetail = (PolicyXUar)base.DataModelFactory.GetDataModelObject("PolicyXUar", false);
                objExposuresUniqueIds.Clear();
                ArrayList arrUar = new ArrayList();
                p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsListGridUarRowIds").InnerText = "";
                p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsListGridUarSessionIdsPostDelete").InnerText = "";

                //Hidden node for Storing Vehicle IDs
                XmlNode objVehicleIDs = p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsListGridUarIds");

                if (objVehicleIDs != null)
                {
                    objVehicleIDs.InnerText = "";
                }


                if (iExposureId > 0)
                    objUpdateUAR.MoveTo(iExposureId);


                // For Edit Rows.
                if (objUarCollectionNode != null)
                {
                    foreach (XmlNode objOptionNode in objUarCollectionNode.SelectNodes("option"))
                    {
                        string sSessionId = objOptionNode.SelectSingleNode("SessionId").InnerText;
                        int iUarRowId = Conversion.CastToType<int>(objOptionNode.SelectSingleNode("UarRowId").InnerText, out m_bIsSuccess);

                        if (iDeletedUarRowId == iUarRowId)
                        {
                            objUpdateUAR.PolicyXUarList.Remove(iUarRowId);
                            continue;
                        }

                        arrUar.Add(iUarRowId.ToString());

                        if (sSessionId != "")
                        {
                            // Replace the object from the session object.  
                            string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sSessionId)).ToString();
                            XmlDocument objPropStore = new XmlDocument();
                            objPropStore.LoadXml(sSessionRawData);


                            if (iUarRowId > 0)
                            {
                                base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                                base.CreateAndSetElement(objRoot, "UarRowId", objUARDetail.UarRowId.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyId", objUpdateUAR.PolicyId.ToString());
                                base.CreateAndSetElement(objRoot, "ExposureId", objUpdateUAR.ExposureId.ToString());
                                base.CreateAndSetElement(objRoot, "UarId", objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyStatusCode", base.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());
                                objUpdateUAR.PolicyXUarList[iUarRowId].PopulateObject(objReturnDoc);
                            }
                            else
                            {
                                PolicyXUar objUpdateUAROth = (PolicyXUar)base.DataModelFactory.GetDataModelObject("PolicyXUar", false);
                                base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                                base.CreateAndSetElement(objRoot, "UarRowId", iUarRowId.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyId", objUpdateUAR.PolicyId.ToString());
                                base.CreateAndSetElement(objRoot, "ExposureId", objUpdateUAR.ExposureId.ToString());
                                base.CreateAndSetElement(objRoot, "UarId", objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyStatusCode", base.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());

                                objUpdateUAROth.PopulateObject(objReturnDoc);
                                objUpdateUAR.PolicyXUarList.Add(objUpdateUAROth, true);
                            }
                            // Add new session key.
                            objExposuresUniqueIds.Add(iUarRowId, sSessionId);
                            p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsListGridUarRowIds").InnerText += "|" + iUarRowId.ToString();
                            p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsListGridUarSessionIdsPostDelete").InnerText += "|" + sSessionId;
                        }
                    }
                }

                foreach (PolicyXUar item in objUpdateUAR.PolicyXUarList)
                {
                    if (!arrUar.Contains(item.UarRowId.ToString()))
                    {
                        objUpdateUAR.PolicyXUarList.Remove(item.UarRowId);
                    }
                }


                int iIndex = 0;

                string sNewAddedExposureSessionId = p_objPropertyStore.SelectSingleNode("//NewAddedExposureSessionId").InnerText;
                objUARListElement = (XmlElement)p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsList");
                if (objUARListElement != null)
                    objUARListElement.ParentNode.RemoveChild(objUARListElement);
                base.CreateElement(p_objPropertyStore.DocumentElement, "ALUARDetailsList", ref objUARListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objUARListElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleID", "Vehicle ID");
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleMake", "Vehicle Make");
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleModel", "Vehicle Model");
                base.CreateAndSetElement(objListHeadXmlElement, "VehicleYear", "Vehicle Year");
                base.CreateAndSetElement(objListHeadXmlElement, "State", "State");
                base.CreateAndSetElement(objListHeadXmlElement, "LicenseNumber", "License Number");
                //base.CreateAndSetElement(objListHeadXmlElement, "UnitType", "Unit Type");

                //For New Rows
                if (!string.IsNullOrEmpty(sNewAddedExposureSessionId))
                {
                    string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sNewAddedExposureSessionId)).ToString();
                    XmlDocument objPropertyStore = new XmlDocument();
                    objPropertyStore.LoadXml(sSessionRawData);

                    base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                    base.CreateAndSetElement(objRoot, "UarRowId", objUARDetail.UarRowId.ToString());
                    base.CreateAndSetElement(objRoot, "PolicyId", objUpdateUAR.PolicyId.ToString());
                    base.CreateAndSetElement(objRoot, "ExposureId", objUpdateUAR.ExposureId.ToString());
                    base.CreateAndSetElement(objRoot, "UarId", objPropertyStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                    base.CreateAndSetElement(objRoot, "PolicyStatusCode", base.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());

                    objUARDetail.PopulateObject(objReturnDoc);
                    objUpdateUAR.PolicyXUarList.Add(objUARDetail);
                    int iUarRowID = objUARDetail.UarRowId;

                    // Add new session key.
                    objExposuresUniqueIds.Add(iUarRowID, sNewAddedExposureSessionId);
                    p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsListGridUarRowIds").InnerText += "|" + iUarRowID.ToString();
                    p_objPropertyStore.SelectSingleNode("/Exposure/ALUARDetailsListGridUarSessionIdsPostDelete").InnerText += "|" + sNewAddedExposureSessionId;
                }

                foreach (PolicyXUar objPolicyXUarDtl in objUpdateUAR.PolicyXUarList)
                {
                    base.CreateElement(objUARListElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_ALDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");
                    objVehicle.MoveTo(objPolicyXUarDtl.UarId);

                    int iState = objVehicle.StateRowId;
                    base.LocalCache.GetStateInfo(iState, ref sStateCode, ref sStateDesc);
                    sState = sStateCode + " " + sStateDesc;

                    base.CreateAndSetElement(objOptionXmlElement, "VehicleID", objVehicle.Vin);
                    base.CreateAndSetElement(objOptionXmlElement, "VehicleMake", objVehicle.VehicleMake);
                    base.CreateAndSetElement(objOptionXmlElement, "VehicleModel", objVehicle.VehicleModel);
                    base.CreateAndSetElement(objOptionXmlElement, "VehicleYear", objVehicle.VehicleYear.ToString());
                    base.CreateAndSetElement(objOptionXmlElement, "State", sState);
                    base.CreateAndSetElement(objOptionXmlElement, "LicenseNumber", objVehicle.LicenseNumber);
                    //base.CreateAndSetElement(objOptionXmlElement, "UnitType", base.LocalCache.GetShortCode(objVehicle.UnitTypeCode));
                    string sSessionId = string.Empty;
                    if (objExposuresUniqueIds.ContainsKey(objPolicyXUarDtl.UarRowId))
                        sSessionId = (string)objExposuresUniqueIds[objPolicyXUarDtl.UarRowId];
                    base.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                    base.CreateAndSetElement(objOptionXmlElement, "UarRowId", objPolicyXUarDtl.UarRowId.ToString());

                    //Adding Vehicle IDs in hidden node.
                    if (objVehicleIDs != null)
                    {
                        if (objVehicleIDs.InnerText == "")
                        {
                            objVehicleIDs.InnerText = objVehicle.UnitId.ToString();
                        }
                        else
                        {
                            objVehicleIDs.InnerText = objVehicleIDs.InnerText + "|" + objVehicle.UnitId.ToString();
                        }
                    }
                }

                base.CreateElement(objUARListElement, "option", ref objOptionXmlElement);
                objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_ALDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                base.CreateAndSetElement(objOptionXmlElement, "VehicleID", "");
                base.CreateAndSetElement(objOptionXmlElement, "VehicleMake", "");
                base.CreateAndSetElement(objOptionXmlElement, "VehicleModel", "");
                base.CreateAndSetElement(objOptionXmlElement, "VehicleYear", "");
                base.CreateAndSetElement(objOptionXmlElement, "State", "");
                base.CreateAndSetElement(objOptionXmlElement, "LicenseNumber", "");
                //base.CreateAndSetElement(objOptionXmlElement, "UnitType", "");
                base.CreateAndSetElement(objOptionXmlElement, "SessionId", "");
                base.CreateAndSetElement(objOptionXmlElement, "UarRowId", "");

                p_objPropertyStore.SelectSingleNode("//NewAddedExposureSessionId").InnerText = "";
                p_objPropertyStore.SelectSingleNode("/Exposure/DeletedUarRowId").InnerText = "";
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.RenderUarDetail.Error", base.ClientId), objEx);
            }
            finally
            {
                objExposuresUniqueIds = null;
                objUARListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                objReturnDoc = null;
                objRoot = null;
            }
            return p_objPropertyStore;
        }

        /// <summary>
        /// Author:Sumit Kumar
        /// Date: 04/14/2010
        /// MITS# : 18229
        /// Description: This method populate the Exposure Child list and display the Property UAR data on popup screen.
        /// </summary>
        /// <param name="objSessionManager">Session Manager to handle the session.</param>
        /// <param name="p_objPropertyStore">XML Document containing the infrmation from screen.</param>
        /// <returns>XML document which is used to render the property UAR Details Grid.</returns>

        public XmlDocument RenderPropertyUARDetail(SessionManager objSessionManager, XmlDocument p_objPropertyStore)
        {
            Hashtable objExposuresUniqueIds = new Hashtable();
            XmlElement objUARListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;
            //Sumit - Start(05/04/2010) - MITS# 20483
            Hashtable objUarScheduleSessionIds = new Hashtable();
            int iScheduleListGridScheduleRowIds = 0;
            bool bIsSuccess = false;
            //Sumit - End
            StringBuilder sbUARAndScheduleSessionId = null;
            string sState = string.Empty;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;

            try
            {
                bool m_bIsSuccess = false;
                string sNewAddedExposureSessionId = p_objPropertyStore.SelectSingleNode("//NewAddedExposureSessionId").InnerText;
                int iDeletedUarRowId = Conversion.CastToType<int>(p_objPropertyStore.SelectSingleNode("/Exposure/DeletedUarRowId").InnerText, out m_bIsSuccess);
                int iExposureId = Conversion.CastToType<int>(p_objPropertyStore.SelectSingleNode("/Exposure/ExposureData/PolicyXExpEnh/ExposureId").InnerText, out m_bIsSuccess);
                XmlNode objUarCollectionNode = p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsList");
                PolicyXExpEnh objUpdateUAR = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                PropertyUnit objProperty = (PropertyUnit)base.DataModelFactory.GetDataModelObject("PropertyUnit", false);
                PolicyXUar objUARDetail = (PolicyXUar)base.DataModelFactory.GetDataModelObject("PolicyXUar", false);
                //Sumit - Start(05/04/2010) - MITS# 20483
                objUarScheduleSessionIds.Clear();
                XmlNode objExposureUarSchedule = p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGrid_ExposureUarSchedule");
                if (objExposureUarSchedule != null)
                {
                    objExposureUarSchedule.InnerText = string.Empty;
                }
                //Sumit - End
                objExposuresUniqueIds.Clear();
                ArrayList arrUar = new ArrayList();

                if (p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarRowIds") != null)
                {
                    p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarRowIds").InnerText = "";
                }

                if (p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarSessionIdsPostDelete") != null)
                {
                    p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarSessionIdsPostDelete").InnerText = "";
                }
                //Sumit - Start(05/04/2010) - MITS# 20483 - Get Schedule Session IDs
                string sScheduleSessionIds = p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridScheduleSessionIds").InnerText;
                
                string[] arrScheduleListGridScheduleRowIds = null;
                //Sumit - End

                //Hidden node for Storing Property IDs
                XmlNode objPropertyIDs = null;
                if (p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarIds") != null)
                {
                    objPropertyIDs = p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarIds");
                }

                if (objPropertyIDs != null)
                {
                    objPropertyIDs.InnerText = "";
                }


                if (iExposureId > 0)
                    objUpdateUAR.MoveTo(iExposureId);


                // For Edit Rows.
                if (objUarCollectionNode != null)
                {
                    foreach (XmlNode objOptionNode in objUarCollectionNode.SelectNodes("option"))
                    {
                        string sSessionId = objOptionNode.SelectSingleNode("SessionId").InnerText;
                        string[] arrSessions = null;
                        int iUarRowId = 0;
                        if (objOptionNode.SelectSingleNode("UarRowId") != null)
                        {
                            iUarRowId = Conversion.CastToType<int>(objOptionNode.SelectSingleNode("UarRowId").InnerText, out m_bIsSuccess);
                        }
                        string sUarScheduleSessionIDs = string.Empty;
                        if (objOptionNode.SelectSingleNode("ScheduleSessionIds") != null)
                        {
                            sUarScheduleSessionIDs = objOptionNode.SelectSingleNode("ScheduleSessionIds").InnerText;
                        }
                        String[] arrSchedule = null;
                        if (sSessionId.Contains("^"))
                        {
                            arrSessions = sSessionId.Split('^');
                            sSessionId = arrSessions[0];
                            arrSchedule = arrSessions[1].Split('|');
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(sUarScheduleSessionIDs))
                                arrSchedule = sUarScheduleSessionIDs.Split('|');
                            else if (!String.IsNullOrEmpty(sScheduleSessionIds) && String.IsNullOrEmpty(sNewAddedExposureSessionId))
                                arrSchedule = sScheduleSessionIds.Split('|');
                        }
                        //Sumit-End

                        if (iDeletedUarRowId == iUarRowId)
                        {
                            objUpdateUAR.PolicyXUarList.Remove(iUarRowId);
                            continue;
                        }

                        arrUar.Add(iUarRowId.ToString());

                        if (sSessionId != "")
                        {
                            // Replace the object from the session object.  
                            string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sSessionId)).ToString();
                            XmlDocument objPropStore = new XmlDocument();
                            objPropStore.LoadXml(sSessionRawData);


                            if (iUarRowId > 0)
                            {
                                base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                                base.CreateAndSetElement(objRoot, "UarRowId", iUarRowId.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyId", objUpdateUAR.PolicyId.ToString());
                                base.CreateAndSetElement(objRoot, "ExposureId", objUpdateUAR.ExposureId.ToString());
                                base.CreateAndSetElement(objRoot, "UarId", objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyStatusCode", base.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());
                                objUpdateUAR.PolicyXUarList[iUarRowId].UarId = iUarRowId;
                                objUpdateUAR.PolicyXUarList[iUarRowId].PopulateObject(objReturnDoc);

                                //Sumit - Start(05/04/2010) - MITS# 20483 - Update object for UAR Ids as well.
                                if (arrSchedule != null)
                                {
                                    if (objPropStore.SelectSingleNode("/PolicyXUar/ScheduleListGridScheduleRowIds") != null)
                                    {
                                        string sScheduleListGridScheduleRowIds = objPropStore.SelectSingleNode("/PolicyXUar/ScheduleListGridScheduleRowIds").InnerText.ToString();
                                        arrScheduleListGridScheduleRowIds = sScheduleListGridScheduleRowIds.Split('|');
                                    }
                                    for (int iSchedule = 0; iSchedule < arrSchedule.Length; iSchedule++)
                                    {
                                        if (!String.IsNullOrEmpty(arrSchedule[iSchedule]))
                                        {
                                            string sSessionRawScheduleData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrSchedule[iSchedule])).ToString();
                                            XmlDocument objShdStore = new XmlDocument();
                                            objShdStore.LoadXml(sSessionRawScheduleData);
                                            PolicyXPschedEnh objUpdateSchdOth = (PolicyXPschedEnh)base.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                                            base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                                            base.CreateAndSetElement(objRoot, "PschedRowID", objUpdateSchdOth.PschedRowID.ToString());
                                            base.CreateAndSetElement(objRoot, "PolicyId", objUARDetail.PolicyId.ToString());
                                            base.CreateAndSetElement(objRoot, "UarId", iUarRowId.ToString());
                                            base.CreateAndSetElement(objRoot, "Name", objShdStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                            base.CreateAndSetElement(objRoot, "Amount", objShdStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());
                                            if (arrScheduleListGridScheduleRowIds != null)
                                           {
                                              iScheduleListGridScheduleRowIds = Conversion.CastToType<int>(arrScheduleListGridScheduleRowIds[iSchedule], out bIsSuccess);
                                           }
                                            if (iScheduleListGridScheduleRowIds > 0)
                                            {
                                                objUpdateUAR.PolicyXUarList[iUarRowId].PolicyXPschedEnhList[iScheduleListGridScheduleRowIds].PschedRowID = iScheduleListGridScheduleRowIds;
                                                objUpdateUAR.PolicyXUarList[iUarRowId].PolicyXPschedEnhList[iScheduleListGridScheduleRowIds].PopulateObject(objReturnDoc);
                                            }
                                            else
                                            {
                                                objUpdateSchdOth.PopulateObject(objReturnDoc);
                                                objUpdateUAR.PolicyXUarList[iUarRowId].PolicyXPschedEnhList.Add(objUpdateSchdOth);
                                            }
                                        }
                                    }
                                }
                                //Sumit-End
                            }
                            else
                            {
                                PolicyXUar objUpdateUAROth = (PolicyXUar)base.DataModelFactory.GetDataModelObject("PolicyXUar", false);
                                base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                                base.CreateAndSetElement(objRoot, "UarRowId", iUarRowId.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyId", objUpdateUAR.PolicyId.ToString());
                                base.CreateAndSetElement(objRoot, "ExposureId", objUpdateUAR.ExposureId.ToString());
                                base.CreateAndSetElement(objRoot, "UarId", objPropStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyStatusCode", base.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());

                                objUpdateUAROth.PopulateObject(objReturnDoc);
                                objUpdateUAR.PolicyXUarList.Add(objUpdateUAROth, true);
                                //Sumit - Start(05/04/2010) - MITS# 20483 - Update object for UAR Ids as well.
                                if (arrSchedule != null)
                                {
                                    for (int iSchedule = 0; iSchedule < arrSchedule.Length; iSchedule++)
                                    {
                                        if (!String.IsNullOrEmpty(arrSchedule[iSchedule]))
                                        {
                                            string sSessionRawScheduleData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrSchedule[iSchedule])).ToString();
                                            XmlDocument objShdStore = new XmlDocument();
                                            objShdStore.LoadXml(sSessionRawScheduleData);
                                            PolicyXPschedEnh objUpdateSchdOth = (PolicyXPschedEnh)base.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                                            base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                                            base.CreateAndSetElement(objRoot, "PschedRowID", objUpdateSchdOth.PschedRowID.ToString());
                                            base.CreateAndSetElement(objRoot, "PolicyId", objUARDetail.PolicyId.ToString());
                                            base.CreateAndSetElement(objRoot, "UarId", iUarRowId.ToString());
                                            base.CreateAndSetElement(objRoot, "Name", objShdStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                            base.CreateAndSetElement(objRoot, "Amount", objShdStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());

                                            objUpdateSchdOth.PopulateObject(objReturnDoc);
                                            objUpdateUAR.PolicyXUarList[objUpdateUAROth.UarRowId].PolicyXPschedEnhList.Add(objUpdateSchdOth);
                                        }
                                    }
                                }
                                //Sumit-End
                            }
                            // Add new session key.
                            objExposuresUniqueIds.Add(iUarRowId, sSessionId);
                            //Sumit - Start(05/04/2010) - MITS# 20483
                            if (arrSessions != null)
                            {
                                objUarScheduleSessionIds.Add(iUarRowId, arrSessions[1]);
                            }
                            else
                            {
                                if (sScheduleSessionIds != "" && sNewAddedExposureSessionId == "")
                                    objUarScheduleSessionIds.Add(iUarRowId, sScheduleSessionIds);
                                else
                                    objUarScheduleSessionIds.Add(iUarRowId, sUarScheduleSessionIDs);
                            }
                            //Sumit-End

                            if (p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarRowIds") != null)
                            {
                                p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarRowIds").InnerText += "|" + iUarRowId.ToString();
                            }

                            if (p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarSessionIdsPostDelete") != null)
                            {
                                p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarSessionIdsPostDelete").InnerText += "|" + sSessionId;
                            }
                        }
                    }
                }

                foreach (PolicyXUar item in objUpdateUAR.PolicyXUarList)
                {
                    if (!arrUar.Contains(item.UarRowId.ToString()))
                    {
                        objUpdateUAR.PolicyXUarList.Remove(item.UarRowId);
                    }
                }


                int iIndex = 0;


                objUARListElement = (XmlElement)p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsList");
                if (objUARListElement != null)
                    objUARListElement.ParentNode.RemoveChild(objUARListElement);
                base.CreateElement(p_objPropertyStore.DocumentElement, "PCUARDetailsList", ref objUARListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objUARListElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "PropertyID", "Property ID");
                base.CreateAndSetElement(objListHeadXmlElement, "Address1", "Address 1");
                base.CreateAndSetElement(objListHeadXmlElement, "Address2", "Address 2");
                base.CreateAndSetElement(objListHeadXmlElement, "Address3", "Address 3");
                base.CreateAndSetElement(objListHeadXmlElement, "Address4", "Address 4");
                base.CreateAndSetElement(objListHeadXmlElement, "City", "City");
                base.CreateAndSetElement(objListHeadXmlElement, "State", "State");

                //For New Rows
                if (!string.IsNullOrEmpty(sNewAddedExposureSessionId))
                {
                    string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sNewAddedExposureSessionId)).ToString();
                    XmlDocument objPropertyStore = new XmlDocument();
                    objPropertyStore.LoadXml(sSessionRawData);

                    base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                    base.CreateAndSetElement(objRoot, "UarRowId", objUARDetail.UarRowId.ToString());
                    base.CreateAndSetElement(objRoot, "PolicyId", objUpdateUAR.PolicyId.ToString());
                    base.CreateAndSetElement(objRoot, "ExposureId", objUpdateUAR.ExposureId.ToString());
                    base.CreateAndSetElement(objRoot, "UarId", objPropertyStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                    base.CreateAndSetElement(objRoot, "PolicyStatusCode", base.CCacheFunctions.GetCodeIDWithShort("Q", "POLICY_STATUS").ToString());

                    objUARDetail.PopulateObject(objReturnDoc);
                    objUpdateUAR.PolicyXUarList.Add(objUARDetail);
                    int iUarRowID = objUARDetail.UarRowId;

                    // Add new session key.
                    objExposuresUniqueIds.Add(iUarRowID, sNewAddedExposureSessionId);

                    //Sumit - Start(05/04/2010) - MITS# 20483 - Update Uar Children.
                    objUarScheduleSessionIds.Add(iUarRowID, sScheduleSessionIds);
                    String[] arrSchedule = null;
                    arrSchedule = sScheduleSessionIds.Split('|');
                    for (int iSchedule = 0; iSchedule < arrSchedule.Length; iSchedule++)
                    {
                        if (!String.IsNullOrEmpty(arrSchedule[iSchedule]))
                        {
                            string sSessionRawScheduleData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrSchedule[iSchedule])).ToString();
                            XmlDocument objPropStore = new XmlDocument();
                            objPropStore.LoadXml(sSessionRawScheduleData);
                            PolicyXPschedEnh objUpdateSchdOth = (PolicyXPschedEnh)base.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                            base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                            base.CreateAndSetElement(objRoot, "PschedRowID", objUpdateSchdOth.PschedRowID.ToString());
                            base.CreateAndSetElement(objRoot, "PolicyId", objUARDetail.PolicyId.ToString());
                            base.CreateAndSetElement(objRoot, "UarId", objUARDetail.UarRowId.ToString());
                            base.CreateAndSetElement(objRoot, "Name", objPropStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                            base.CreateAndSetElement(objRoot, "Amount", objPropStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());

                            objUpdateSchdOth.PopulateObject(objReturnDoc);
                            objUpdateUAR.PolicyXUarList[iUarRowID].PolicyXPschedEnhList.Add(objUpdateSchdOth);
                        }
                    }
                    //Sumit - End

                    p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarRowIds").InnerText += "|" + iUarRowID.ToString();
                    p_objPropertyStore.SelectSingleNode("/Exposure/PCUARDetailsListGridUarSessionIdsPostDelete").InnerText += "|" + sNewAddedExposureSessionId;
                }

                foreach (PolicyXUar objPolicyXUarDtl in objUpdateUAR.PolicyXUarList)
                {
                    base.CreateElement(objUARListElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_PCDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");
                    objProperty.MoveTo(objPolicyXUarDtl.UarId);

                    int iState = objProperty.StateId;
                    base.LocalCache.GetStateInfo(iState, ref sStateCode, ref sStateDesc);
                    sState = sStateCode + " " + sStateDesc;

                    base.CreateAndSetElement(objOptionXmlElement, "PropertyID", objProperty.Pin);
                    base.CreateAndSetElement(objOptionXmlElement, "Address1", objProperty.Addr1);
                    base.CreateAndSetElement(objOptionXmlElement, "Address2", objProperty.Addr2);
                    base.CreateAndSetElement(objOptionXmlElement, "Address3", objProperty.Addr3);
                    base.CreateAndSetElement(objOptionXmlElement, "Address4", objProperty.Addr4);
                    base.CreateAndSetElement(objOptionXmlElement, "City", objProperty.City);
                    base.CreateAndSetElement(objOptionXmlElement, "State", sState);
                    string sSessionId = string.Empty;
                    string sScheduleSessionID = string.Empty;
                    if (objExposuresUniqueIds.ContainsKey(objPolicyXUarDtl.UarRowId))
                        sSessionId = (string)objExposuresUniqueIds[objPolicyXUarDtl.UarRowId];
                    string sSchdSessionIds = string.Empty;
                    if (objUarScheduleSessionIds.ContainsKey(objPolicyXUarDtl.UarRowId))
                        sSchdSessionIds = (string)objUarScheduleSessionIds[objPolicyXUarDtl.UarRowId];
                    sbUARAndScheduleSessionId = new StringBuilder();
                    sbUARAndScheduleSessionId.Append(sSessionId);
                    sbUARAndScheduleSessionId.Append("^");
                    sbUARAndScheduleSessionId.Append(sSchdSessionIds);
                    base.CreateAndSetElement(objOptionXmlElement, "SessionId", sbUARAndScheduleSessionId.ToString());
                    base.CreateAndSetElement(objOptionXmlElement, "UarRowId", objPolicyXUarDtl.UarRowId.ToString());
                    base.CreateAndSetElement(objOptionXmlElement, "ScheduleSessionIds", sSchdSessionIds);

                    if (objExposureUarSchedule != null)
                    {
                        if (String.IsNullOrEmpty(objExposureUarSchedule.InnerText))
                        {
                            objExposureUarSchedule.InnerText = objPolicyXUarDtl.UarRowId + "^" + sSchdSessionIds;
                        }
                        else
                        {
                            objExposureUarSchedule.InnerText += "%" + objPolicyXUarDtl.UarRowId + "^" + sSchdSessionIds;
                        }
                    }

                    //Sumit-End

                    //Adding Property IDs in hidden node.
                    if (objPropertyIDs != null)
                    {
                        if (objPropertyIDs.InnerText == "")
                        {
                            objPropertyIDs.InnerText = objProperty.PropertyId.ToString();
                        }
                        else
                        {
                            objPropertyIDs.InnerText = objPropertyIDs.InnerText + "|" + objProperty.PropertyId.ToString();
                        }
                    }
                }

                base.CreateElement(objUARListElement, "option", ref objOptionXmlElement);
                objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_PCDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                base.CreateAndSetElement(objOptionXmlElement, "PropertyID", "");
                base.CreateAndSetElement(objOptionXmlElement, "Address1", "");
                base.CreateAndSetElement(objOptionXmlElement, "Address2", "");
                base.CreateAndSetElement(objOptionXmlElement, "Address3", "");
                base.CreateAndSetElement(objOptionXmlElement, "Address4", "");
                base.CreateAndSetElement(objOptionXmlElement, "City", "");
                base.CreateAndSetElement(objOptionXmlElement, "State", "");
                base.CreateAndSetElement(objOptionXmlElement, "SessionId", "");
                base.CreateAndSetElement(objOptionXmlElement, "UarRowId", "");
                base.CreateAndSetElement(objOptionXmlElement, "ScheduleSessionIds", "");

                if (p_objPropertyStore.SelectSingleNode("//NewAddedExposureSessionId") != null)
                {
                    p_objPropertyStore.SelectSingleNode("//NewAddedExposureSessionId").InnerText = "";
                }

                if (p_objPropertyStore.SelectSingleNode("/Exposure/DeletedUarRowId") != null)
                {
                    p_objPropertyStore.SelectSingleNode("/Exposure/DeletedUarRowId").InnerText = "";
                }
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.RenderUarDetail.Error", base.ClientId), objEx);
            }
            finally
            {
                objExposuresUniqueIds = null;
                objUARListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                objReturnDoc = null;
                objRoot = null;
            }
            return p_objPropertyStore;
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/28/2010
        /// MITS#: 20483
        /// Description: This method populate the Property Child list and display the Property Schedule data on popup screen.
        /// </summary>
        /// <param name="objSessionManager">Session Manager for handling session.</param>
        /// <param name="p_objPropertyStore">Input XML from the Schedule screen.</param>
        /// <returns>XML Document which will render the Schedule grid.</returns>

        public XmlDocument RenderScheduleList(SessionManager objSessionManager, XmlDocument p_objPropertyStore)
        {
            Hashtable objScheduleUniqueIds = new Hashtable();
            XmlElement objScheduleListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;

            try
            {
                bool m_bIsSuccess = false;
                int iDeletedScheduleRowId = Conversion.CastToType<int>(p_objPropertyStore.SelectSingleNode("/PolicyXUar/DeleteScheduleRowId").InnerText, out m_bIsSuccess);
                int iUarRowId = Conversion.CastToType<int>(p_objPropertyStore.SelectSingleNode("/PolicyXUar/UarRowID").InnerText, out m_bIsSuccess);
                XmlNode objScheduleCollectionNode = p_objPropertyStore.SelectSingleNode("/PolicyXUar/ScheduleList");
                XmlNode objScheduleSessionIds = p_objPropertyStore.SelectSingleNode("/PolicyXUar/ScheduleListGridSessionIds");
                PolicyXUar objUARDetail = (PolicyXUar)base.DataModelFactory.GetDataModelObject("PolicyXUar", false);
                PolicyXPschedEnh objSchedule = (PolicyXPschedEnh)base.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                objScheduleUniqueIds.Clear();
                ArrayList arrSchedule = new ArrayList();

                XmlNode objScheduleRowIds = p_objPropertyStore.SelectSingleNode("/PolicyXUar/ScheduleListGridScheduleRowIds");
                if (objScheduleRowIds != null)
                {
                    objScheduleRowIds.InnerText = "";
                }

                if (objScheduleSessionIds != null)
                {
                    objScheduleSessionIds.InnerText = "";
                }

                if (iUarRowId > 0)
                    objUARDetail.MoveTo(iUarRowId);


                // For Edit Rows.
                if (objScheduleCollectionNode != null)
                {
                    foreach (XmlNode objOptionNode in objScheduleCollectionNode.SelectNodes("option"))
                    {
                        string sSessionId = objOptionNode.SelectSingleNode("SessionId").InnerText;
                        int iScheduleRowId = Conversion.CastToType<int>(objOptionNode.SelectSingleNode("ScheduleRowId").InnerText, out m_bIsSuccess);

                        if (iDeletedScheduleRowId == iScheduleRowId)
                        {
                            objUARDetail.PolicyXPschedEnhList.Remove(iScheduleRowId);
                            continue;
                        }

                        arrSchedule.Add(iScheduleRowId.ToString());

                        if (sSessionId != "")
                        {
                            // Replace the object from the session object.  
                            string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sSessionId)).ToString();
                            XmlDocument objPropStore = new XmlDocument();
                            objPropStore.LoadXml(sSessionRawData);


                            if (iScheduleRowId > 0)
                            {
                                base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                                base.CreateAndSetElement(objRoot, "PschedRowID", iScheduleRowId.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyId", objUARDetail.PolicyId.ToString());
                                base.CreateAndSetElement(objRoot, "UarId", p_objPropertyStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "Name", objPropStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "Amount", objPropStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());
                                objUARDetail.PolicyXPschedEnhList[iScheduleRowId].PschedRowID = iScheduleRowId;
                                objUARDetail.PolicyXPschedEnhList[iScheduleRowId].PopulateObject(objReturnDoc);
                            }
                            else
                            {
                                PolicyXPschedEnh objScheduleOth = (PolicyXPschedEnh)base.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);
                                base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                                base.CreateAndSetElement(objRoot, "PschedRowID", iScheduleRowId.ToString());
                                base.CreateAndSetElement(objRoot, "PolicyId", objUARDetail.PolicyId.ToString());
                                base.CreateAndSetElement(objRoot, "UarId", p_objPropertyStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "Name", objPropStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                base.CreateAndSetElement(objRoot, "Amount", objPropStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());

                                objScheduleOth.PopulateObject(objReturnDoc);
                                objUARDetail.PolicyXPschedEnhList.Add(objScheduleOth, true);
                            }
                            // Add new session key.
                            objScheduleUniqueIds.Add(iScheduleRowId, sSessionId);

                            if (objScheduleRowIds != null)
                            {
                                if (objScheduleRowIds.InnerText == "")
                                    objScheduleRowIds.InnerText = iScheduleRowId.ToString();
                                else
                                    objScheduleRowIds.InnerText += "|" + iScheduleRowId.ToString();
                            }

                            if (objScheduleSessionIds != null)
                            {
                                if (objScheduleSessionIds.InnerText == "")
                                    objScheduleSessionIds.InnerText = sSessionId;
                                else
                                    objScheduleSessionIds.InnerText += "|" + sSessionId;
                            }
                        }
                    }
                }

                foreach (PolicyXPschedEnh item in objUARDetail.PolicyXPschedEnhList)
                {
                    if (!arrSchedule.Contains(item.PschedRowID.ToString()))
                    {
                        objUARDetail.PolicyXPschedEnhList.Remove(item.PschedRowID);
                    }
                }


                int iIndex = 0;

                string sNewAddedScheduleSessionId = p_objPropertyStore.SelectSingleNode("//NewAddedScheduleSessionId").InnerText;
                objScheduleListElement = (XmlElement)p_objPropertyStore.SelectSingleNode("/PolicyXUar/ScheduleList");
                if (objScheduleListElement != null)
                    objScheduleListElement.ParentNode.RemoveChild(objScheduleListElement);
                base.CreateElement(p_objPropertyStore.DocumentElement, "ScheduleList", ref objScheduleListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objScheduleListElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "Name", "Name");
                base.CreateAndSetElement(objListHeadXmlElement, "Amount", "Amount");

                //For New Rows
                if (!string.IsNullOrEmpty(sNewAddedScheduleSessionId))
                {
                    string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sNewAddedScheduleSessionId)).ToString();
                    XmlDocument objPropertyStore = new XmlDocument();
                    objPropertyStore.LoadXml(sSessionRawData);

                    base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXPschedEnh");
                    base.CreateAndSetElement(objRoot, "PschedRowID", objSchedule.PschedRowID.ToString());
                    base.CreateAndSetElement(objRoot, "PolicyId", objUARDetail.PolicyId.ToString());
                    base.CreateAndSetElement(objRoot, "UarId", p_objPropertyStore.SelectSingleNode("/PolicyXUar/UarId").InnerText.ToString());
                    base.CreateAndSetElement(objRoot, "Name", objPropertyStore.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                    base.CreateAndSetElement(objRoot, "Amount", objPropertyStore.SelectSingleNode("/Schedule/Amount").InnerText.ToString());

                    objSchedule.PopulateObject(objReturnDoc);
                    objUARDetail.PolicyXPschedEnhList.Add(objSchedule);
                    int iScheduleRowID = objSchedule.PschedRowID;

                    // Add new session key.
                    objScheduleUniqueIds.Add(iScheduleRowID, sNewAddedScheduleSessionId);

                    if (objScheduleRowIds != null)
                    {
                        if (objScheduleRowIds.InnerText == "")
                            objScheduleRowIds.InnerText = iScheduleRowID.ToString();
                        else
                            objScheduleRowIds.InnerText += "|" + iScheduleRowID.ToString();
                    }

                    if (objScheduleSessionIds != null)
                    {
                        if (objScheduleSessionIds.InnerText == "")
                            objScheduleSessionIds.InnerText = sNewAddedScheduleSessionId;
                        else
                            objScheduleSessionIds.InnerText += "|" + sNewAddedScheduleSessionId;
                    }
                }

                foreach (PolicyXPschedEnh objPolicyXSchdDtl in objUARDetail.PolicyXPschedEnhList)
                {
                    base.CreateElement(objScheduleListElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SCHDDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                    base.CreateAndSetElement(objOptionXmlElement, "Name", objPolicyXSchdDtl.Name);
                    base.CreateAndSetElement(objOptionXmlElement, "Amount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXSchdDtl.Amount, base.ConnectionString, ClientId));
                    string sSessionId = string.Empty;
                    if (objScheduleUniqueIds.ContainsKey(objPolicyXSchdDtl.PschedRowID))
                        sSessionId = (string)objScheduleUniqueIds[objPolicyXSchdDtl.PschedRowID];
                    base.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                    base.CreateAndSetElement(objOptionXmlElement, "ScheduleRowId", objPolicyXSchdDtl.PschedRowID.ToString());
                }

                base.CreateElement(objScheduleListElement, "option", ref objOptionXmlElement);
                objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SCHDDOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                base.CreateAndSetElement(objOptionXmlElement, "Name", "");
                base.CreateAndSetElement(objOptionXmlElement, "Amount", "");
                base.CreateAndSetElement(objOptionXmlElement, "SessionId", "");
                base.CreateAndSetElement(objOptionXmlElement, "ScheduleRowId", "");

                if (p_objPropertyStore.SelectSingleNode("//NewAddedScheduleSessionId") != null)
                {
                    p_objPropertyStore.SelectSingleNode("//NewAddedScheduleSessionId").InnerText = "";
                }

                if (p_objPropertyStore.SelectSingleNode("/PolicyXUar/DeleteScheduleRowId") != null)
                {
                    p_objPropertyStore.SelectSingleNode("/PolicyXUar/DeleteScheduleRowId").InnerText = "";
                }
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.RenderScheduleDetail.Error", base.ClientId), objEx);
            }
            finally
            {
                objScheduleUniqueIds = null;
                objScheduleListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                objReturnDoc = null;
                objRoot = null;
            }
            return p_objPropertyStore;
        }



        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 03/16/2010
        /// MITS#: 18229
        /// Description: Method to  store Vehicle Data in session.
        /// </summary>
        /// <param name="objSessionManager">Session Manager for handling the Sessions.</param>
        /// <param name="p_objPropertyStore">The input XML document.</param>
        /// <returns>The output XML document containing sessionid.</returns>
        public XmlDocument SetUAR(SessionManager objSessionManager, XmlDocument p_objPropertyStore)
        {
            try
            {
                XmlDocument objReturnDoc = null;
                XmlElement objRoot = null;
                base.StartDocument(ref objReturnDoc, ref objRoot, "SetUAR");
                string sUniqueId = Utilities.GenerateGuid();
                objSessionManager.SetBinaryItem(sUniqueId, Utilities.BinarySerialize(p_objPropertyStore.OuterXml), base.ClientId);
                base.CreateAndSetElement(objRoot, "SessionId", sUniqueId);
                return objReturnDoc;
            }

            finally
            {
            }
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/28/2010
        /// MITS#: 20483
        /// Description: Method to  store Property Schedule Data in session.
        /// </summary>
        /// <param name="objSessionManager">Session Manager to handle the session.</param>
        /// <param name="p_objPropertyStore">Input XML Document.</param>
        /// <returns>The XML Document containing the session id.</returns>
        public XmlDocument SetSchedule(SessionManager objSessionManager, XmlDocument p_objPropertyStore)
        {
            try
            {
                XmlDocument objReturnDoc = null;
                XmlElement objRoot = null;
                base.StartDocument(ref objReturnDoc, ref objRoot, "SetSchedule");
                string sUniqueId = Utilities.GenerateGuid();
                objSessionManager.SetBinaryItem(sUniqueId, Utilities.BinarySerialize(p_objPropertyStore.OuterXml), base.ClientId);
                base.CreateAndSetElement(objRoot, "SessionId", sUniqueId);
                return objReturnDoc;
            }

            finally
            {
            }
        }


        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 04/14/2010
        /// MITS#: 18229
        /// Description: Method to  populate Property Data.
        /// </summary>
        /// <param name="objSessionManager">Session Manager to handle the session.</param>
        /// <param name="objXmlIn">Input XML Document with information from the screen.</param>
        /// <returns>XML Document containing data to populate the screen</returns>
        public XmlDocument PopulateSearchPropUar(XmlDocument objXmlIn, SessionManager objSessionManager)
        {
            XmlDocument objReturnDoc = null;
            XmlDocument objSchedReturnDoc = null;
            int iPropertyID = 0;
            int iScheduleListGridScheduleRowIds = 0;
            string sPIN = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sAddr3 = string.Empty;
            string sAddr4 = string.Empty;
            string sCity = string.Empty;
            string sStateShortCode = string.Empty;
            string sStateDesc = string.Empty;
            string sState = string.Empty;
            int iState = 0;
            bool m_bIsSuccess = false;
            XmlElement objRoot = null;
            XmlElement objSchedRoot = null;
            XmlElement objOptionXmlElement = null;
            LocalCache objLocalCache = null;
            //Sumit - Start(04/27/2010) - MITS# 20483 - Variables for Schedule Grid.
            string p_sSessionId = string.Empty;
            Hashtable objSchdUniqueIds = new Hashtable();
            int iIndex = 0;
            string sDelRowID = string.Empty;
            string[] arrDeletedRowID = null;
            string[] arrSchdDeletedRowID = null;
            string[] arrSchdExpDeletedRowID = null;
            ArrayList arrDeletedUarRowIds = new ArrayList();
            XmlElement objSchdListElement = null;
            XmlElement objSchdListHeadElement = null;
            string[] arrSessionID = null;
            string[] arrScheduleListGridScheduleRowIds = null;
            bool bIsSuccess = false;
            //Sumit - End
            XmlAttribute objXmlAttribute = null;

            try
            {
                PropertyUnit objProperty = (PropertyUnit)base.DataModelFactory.GetDataModelObject("PropertyUnit", false);
                PolicyXUar objPolicyXUar = (PolicyXUar)base.DataModelFactory.GetDataModelObject("PolicyXUar", false);

                objLocalCache = new LocalCache(base.ConnectionString,ClientId);

                //This will work in case of new Property.Otherwise this will be 0.
                int iUarRowID = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("//PropertyID").InnerText, out m_bIsSuccess);
                if (objXmlIn.SelectSingleNode("//DelRowID") != null)
                {
                    sDelRowID = objXmlIn.SelectSingleNode("//DelRowID").InnerText;
                    
                    if (!string.IsNullOrEmpty(sDelRowID))
                    {
                        arrDeletedRowID = sDelRowID.Split('~');
                        for (int iCount = 0; iCount < arrDeletedRowID.Length; iCount++)
                        {
                            if (arrDeletedRowID[iCount].Contains(iUarRowID + "^"))
                            {
                                arrSchdExpDeletedRowID = arrDeletedRowID[iCount].Split('^');
                                arrSchdDeletedRowID = arrSchdExpDeletedRowID[1].Split('|');
                                for (int i = 0; i < arrSchdDeletedRowID.Length; i++)
                                    arrDeletedUarRowIds.Add(arrSchdDeletedRowID[i]);
                            }
                        }
                    }
                }
                if (iUarRowID > 0 && objXmlIn.SelectSingleNode("//SessionId") != null)
                {
                    objPolicyXUar.MoveTo(iUarRowID);
                    iPropertyID = objPolicyXUar.UarId;
                }
                else
                {
                    iPropertyID = iUarRowID;
                }


                //In case of Edit - Property Id is fetched from session.
                if (objXmlIn.SelectSingleNode("//SessionId") != null)
                {
                    p_sSessionId = objXmlIn.SelectSingleNode("//SessionId").InnerText;
                    arrSessionID = p_sSessionId.Split('^');

                    if (!string.IsNullOrEmpty(arrSessionID[0]))
                    {
                        // Load from the session.
                        string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrSessionID[0])).ToString();
                        XmlDocument objPropertyStore = new XmlDocument();
                        objPropertyStore.LoadXml(sSessionRawData);
                        iPropertyID = Conversion.CastToType<int>(objPropertyStore.SelectSingleNode("//UarId").InnerText, out m_bIsSuccess);
                        if (objPropertyStore.SelectSingleNode("//ScheduleListGridScheduleRowIds") != null)
                        {
                            string sScheduleListGridScheduleRowIds = objPropertyStore.SelectSingleNode("/PolicyXUar/ScheduleListGridScheduleRowIds").InnerText.ToString();
                            arrScheduleListGridScheduleRowIds = sScheduleListGridScheduleRowIds.Split('|');
                        }
                    }
                }

                if (iPropertyID > 0)
                {
                    objProperty.MoveTo(iPropertyID);

                    sPIN = objProperty.Pin;
                    sAddr1 = objProperty.Addr1;
                    sAddr2 = objProperty.Addr2;
                    sAddr3 = objProperty.Addr3;
                    sAddr4 = objProperty.Addr4;
                    sCity = objProperty.City;
                    iState = objProperty.StateId;
                    objLocalCache.GetStateInfo(iState, ref sStateShortCode, ref sStateDesc);
                    sState = sStateShortCode + " " + sStateDesc;
                }

                base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                base.CreateAndSetElement(objRoot, "PIN", sPIN);
                base.CreateAndSetElement(objRoot, "Address1", sAddr1);
                base.CreateAndSetElement(objRoot, "Address2", sAddr2);
                base.CreateAndSetElement(objRoot, "Address3", sAddr3);
                base.CreateAndSetElement(objRoot, "Address4", sAddr4);
                base.CreateAndSetElement(objRoot, "City", sCity);
                base.CreateAndSetElement(objRoot, "State", sState);
                base.CreateAndSetElement(objRoot, "UarId", iPropertyID.ToString());
                if (arrSessionID != null)
                {
                    if (arrSessionID.Length > 1)
                    {
                        if (!string.IsNullOrEmpty(arrSessionID[1]))
                        {
                            string[] arrScheduleSessionID = arrSessionID[1].Split('|');
                            if (arrScheduleSessionID != null)
                            {

                                for (int iCount = 0; iCount < arrScheduleSessionID.Length; iCount++)
                                {
                                    if (!String.IsNullOrEmpty(arrScheduleSessionID[iCount]))
                                    {
                                        string sScheduleRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrScheduleSessionID[iCount])).ToString();
                                        XmlDocument objSchedule = new XmlDocument();
                                        objSchedule.LoadXml(sScheduleRawData);

                                        PolicyXPschedEnh objScheduleData = (PolicyXPschedEnh)base.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);
                                        base.StartDocument(ref objSchedReturnDoc, ref objSchedRoot, "PolicyXPschedEnh");
                                        base.CreateAndSetElement(objSchedRoot, "PolicyId", "");
                                        base.CreateAndSetElement(objSchedRoot, "UarId", iPropertyID.ToString());
                                        base.CreateAndSetElement(objSchedRoot, "Name", objSchedule.SelectSingleNode("/Schedule/Name").InnerText.ToString());
                                        base.CreateAndSetElement(objSchedRoot, "Amount", objSchedule.SelectSingleNode("/Schedule/Amount").InnerText.ToString());
                                        iScheduleListGridScheduleRowIds = Conversion.CastToType<int>(arrScheduleListGridScheduleRowIds[iCount], out bIsSuccess);
                                        if (iScheduleListGridScheduleRowIds > 0)
                                        {
                                            base.CreateAndSetElement(objSchedRoot, "PschedRowID", iScheduleListGridScheduleRowIds.ToString());
                                            objPolicyXUar.PolicyXPschedEnhList[iScheduleListGridScheduleRowIds].PschedRowID = iScheduleListGridScheduleRowIds;
                                            objPolicyXUar.PolicyXPschedEnhList[iScheduleListGridScheduleRowIds].PopulateObject(objSchedReturnDoc);
                                            objSchdUniqueIds.Add(iScheduleListGridScheduleRowIds, arrScheduleSessionID[iCount]);
                                        }
                                        else
                                        {
                                            base.CreateAndSetElement(objSchedRoot, "PschedRowID", objScheduleData.PschedRowID.ToString());
                                            objScheduleData.PopulateObject(objSchedReturnDoc);
                                            objPolicyXUar.PolicyXPschedEnhList.Add(objScheduleData);
                                            objSchdUniqueIds.Add(objScheduleData.PschedRowID, arrScheduleSessionID[iCount]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //Change
                foreach (PolicyXPschedEnh objPolicyXSchdDelete in objPolicyXUar.PolicyXPschedEnhList)
                {
                    if (arrDeletedUarRowIds.Contains(objPolicyXSchdDelete.PschedRowID.ToString()))
                    {
                        if (objPolicyXSchdDelete.PschedRowID > 0)
                        {
                            objPolicyXUar.PolicyXPschedEnhList.Remove(objPolicyXSchdDelete.PschedRowID);
                        }
                    }
                }
                //Sumit - Start(04/27/2010) - MITS# 20483 - Create/Populate Schedule data.
                objSchdListElement = (XmlElement)objReturnDoc.SelectSingleNode("/PolicyXUar/ScheduleList");
                if (objSchdListElement != null)
                    objSchdListElement.ParentNode.RemoveChild(objSchdListElement);

                base.CreateElement(objReturnDoc.DocumentElement, "ScheduleList", ref objSchdListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objSchdListElement, "listhead", ref objSchdListHeadElement);
                base.CreateAndSetElement(objSchdListHeadElement, "Name", "Name");
                base.CreateAndSetElement(objSchdListHeadElement, "Amount", "Amount");

                foreach (PolicyXPschedEnh objPolicyXSchdDtl in objPolicyXUar.PolicyXPschedEnhList)
                {
                    base.CreateElement(objSchdListElement, "option", ref objOptionXmlElement);
                    objRoot.SetAttribute("ref", Constants.INSTANCE_SCHDDOCUMENT_PATH + objSchdListHeadElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                    base.CreateAndSetElement(objOptionXmlElement, "Name", objPolicyXSchdDtl.Name);
                    base.CreateAndSetElement(objOptionXmlElement, "Amount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXSchdDtl.Amount, base.ConnectionString, ClientId));
                    string sSessionId = string.Empty;
                    if (objSchdUniqueIds.ContainsKey(objPolicyXSchdDtl.PschedRowID))
                        sSessionId = (string)objSchdUniqueIds[objPolicyXSchdDtl.PschedRowID];
                    base.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                    base.CreateAndSetElement(objOptionXmlElement, "ScheduleRowId", objPolicyXSchdDtl.PschedRowID.ToString());
                }


                base.CreateElement(objSchdListElement, "option", ref objOptionXmlElement);
                objRoot.SetAttribute("ref", Constants.INSTANCE_SCHDDOCUMENT_PATH + objSchdListHeadElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                base.CreateAndSetElement(objOptionXmlElement, "Name", "");
                base.CreateAndSetElement(objOptionXmlElement, "Amount", "");
                base.CreateAndSetElement(objOptionXmlElement, "SessionId", "");
                base.CreateAndSetElement(objOptionXmlElement, "ScheduleRowId", "");

                objXmlAttribute = objReturnDoc.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.PopulateSearchPropUAR.Error", base.ClientId), objEx);
            }
            finally
            {
                objLocalCache = null;
                objRoot = null;
            }
            return objReturnDoc;
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 05/03/2010
        /// MITS#: 20483
        /// Description: Method to  populate Schedule Data.
        /// </summary>
        /// <param name="objXmlIn">Input XML for getting the screen data.</param>
        /// <param name="objSessionManager">Session Manager to handle the session.</param>
        /// <returns></returns>
        public XmlDocument GetSchedule(XmlDocument objXmlIn, SessionManager objSessionManager)
        {
            XmlDocument objReturnDoc = null;
            int iScheduleID = 0;
            string sName = string.Empty;
            string sAmount = string.Empty;

            bool m_bIsSuccess = false;
            XmlElement objRoot = null;
            string p_sSessionId = string.Empty;

            try
            {
                PolicyXPschedEnh objSchedule = (PolicyXPschedEnh)base.DataModelFactory.GetDataModelObject("PolicyXPschedEnh", false);

                if (objXmlIn.SelectSingleNode("//ScheduleID") != null)
                {
                   iScheduleID = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("//ScheduleID").InnerText, out m_bIsSuccess);
                }

                if (objXmlIn.SelectSingleNode("//SessionId") != null)
                {
                    p_sSessionId = objXmlIn.SelectSingleNode("//SessionId").InnerText;
                    if (!string.IsNullOrEmpty(p_sSessionId))
                    {
                        string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(p_sSessionId)).ToString();
                        XmlDocument objPropertyStore = new XmlDocument();
                        objPropertyStore.LoadXml(sSessionRawData);
                        sName = objPropertyStore.SelectSingleNode("//Name").InnerText;
                        sAmount = objPropertyStore.SelectSingleNode("//Amount").InnerText;
                    }
                }

                if (iScheduleID > 0 && (string.IsNullOrEmpty(p_sSessionId)))
                {
                    objSchedule.MoveTo(iScheduleID);

                    sName = objSchedule.Name;
                    sAmount = objSchedule.Amount.ToString();
                }

                base.StartDocument(ref objReturnDoc, ref objRoot, "Schedule");
                base.CreateAndSetElement(objRoot, "Name", sName);
                base.CreateAndSetElement(objRoot, "Amount", sAmount);
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.RenderScheduleDetail.Error", base.ClientId), objEx);
            }
            finally
            {
                objRoot = null;
            }
            return objReturnDoc;
        }

        private void DisableControls(ref PolicyExposureUITemp p_structPolicyExposure)
        {
            p_structPolicyExposure.ExceptionsEnabled = false;
            p_structPolicyExposure.RemarksEnabled = false;
            p_structPolicyExposure.CovTypeCodeEnabled = false;
            p_structPolicyExposure.EffDateEnabled = false;
            p_structPolicyExposure.ExpAmountEnabled = false;
            p_structPolicyExposure.ExpDateEnabled = false;
            p_structPolicyExposure.ExpRateEnabled = false;
            p_structPolicyExposure.BaseRateEnabled = false;
            p_structPolicyExposure.ExpTypeEnabled = false;
            p_structPolicyExposure.HierarchyLevelEnabled = false;
            p_structPolicyExposure.PremAmtEnabled = false;
            p_structPolicyExposure.StatusEnabled = false;
            p_structPolicyExposure.OkEnabled = false;
            //Anu Tennyson for MITS 18229 STARTS
            p_structPolicyExposure.ALUARDetailsAddButtonEnabled = false;
            p_structPolicyExposure.ALUARDetailsDeleteButtonEnabled = false;
            p_structPolicyExposure.PCUARDetailsAddButtonEnabled = false;
            p_structPolicyExposure.PCUARDetailsDeleteButtonEnabled = false;
            p_structPolicyExposure.ScheduleAddButtonEnabled = false;
            p_structPolicyExposure.ScheduleEditButtonEnabled = false;
            p_structPolicyExposure.ScheduleDeleteButtonEnabled = false;
            //Anu Tennyson for MITS 18229 ENDs
        }
        private void EnableControls(ref PolicyExposureUITemp p_structPolicyExposure)
        {
            p_structPolicyExposure.CancelEnabled = true;
            p_structPolicyExposure.ExceptionsEnabled = true;
            p_structPolicyExposure.OkEnabled = true;
            p_structPolicyExposure.RemarksEnabled = true;
            p_structPolicyExposure.CovTypeCodeEnabled = true;
            p_structPolicyExposure.ExpAmountEnabled = true;
            p_structPolicyExposure.ExpTypeEnabled = true;
            p_structPolicyExposure.HierarchyLevelEnabled = true;
            //Anu Tennyson for MITS 18229 STARTS
            p_structPolicyExposure.ALUARDetailsAddButtonEnabled = true;
            p_structPolicyExposure.ALUARDetailsDeleteButtonEnabled = true;
            p_structPolicyExposure.PCUARDetailsAddButtonEnabled = true;
            p_structPolicyExposure.PCUARDetailsDeleteButtonEnabled = true;
            p_structPolicyExposure.ScheduleAddButtonEnabled = true;
            p_structPolicyExposure.ScheduleEditButtonEnabled = true;
            p_structPolicyExposure.ScheduleDeleteButtonEnabled = true;
            //Anu Tennyson for MITS 18229 ENDS
        }
        private bool ValidateFields(XmlDocument objPropertyStore, string sEffDate, string sExpDate, int iTransactionId, int iPolicyId)
        {
            bool bValidate = false;
            PolicyEnh objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);
            PolicyXExpEnh objPolicyXExpEnh = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            PolicyXTransEnh objPolicyXTransEnh = (PolicyXTransEnh)base.DataModelFactory.GetDataModelObject("PolicyXTransEnh", false);

            string sSQL = string.Empty;
            DbReader objReader = null;
            int iTransIdMax = 0;
            string sStatus = string.Empty;

            string sEffectiveDate = string.Empty;
            string sExpirationDate = string.Empty;
            string sCancelDate = string.Empty;

            DateTime objEffDateUI = System.DateTime.Today;
            DateTime objExpDateUI = System.DateTime.Today;
            DateTime objEffectiveDate = System.DateTime.Today;
            DateTime objExpirationDate = System.DateTime.Today;
            DateTime objCancelDate = System.DateTime.Today;
            TimeSpan objDays;
            int iDays = 0;
            objPolicyEnh.MoveTo(iPolicyId);

            objPolicyXTransEnh.MoveTo(iTransactionId);
            int iTransactionType = objPolicyXTransEnh.TransactionType;
            string sTransactionType = base.CCacheFunctions.GetShortCode(iTransactionType);
            int iTermNumber = objPolicyXTransEnh.TermNumber;
            int iTermSeq = objPolicyXTransEnh.TermSeqAlpha;

            objPolicyXExpEnh.PopulateObject(objPropertyStore);
            sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + objPolicyEnh.PolicyId
                    + " AND TERM_NUMBER = " + iTermNumber + " AND SEQUENCE_ALPHA = " + iTermSeq;
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                sEffectiveDate = objReader.GetString("EFFECTIVE_DATE");
                sExpirationDate = objReader.GetString("EXPIRATION_DATE");
                sCancelDate = objReader.GetString("CANCEL_DATE");
            }
            objReader.Close();

            if (objPolicyEnh.PolicyXTransEnhList.Count == 0)
            {
                objEffectiveDate = Convert.ToDateTime(sEffDate);
                objExpirationDate = Convert.ToDateTime(sExpDate);
            }
            else
            {
                objEffectiveDate = Conversion.ToDate(sEffectiveDate);
                objExpirationDate = Conversion.ToDate(sExpirationDate);
            }

            if(!String.Equals(sEffDate,"")) // added ny csingh7 MITS 20721 , 20722
                objEffDateUI  = Convert.ToDateTime(sEffDate);
            if (!String.Equals(sExpDate, "")) // added ny csingh7 MITS 20721 , 20722
                objExpDateUI  = Convert.ToDateTime(sExpDate);
            objDays = objExpDateUI.Subtract(objEffDateUI);
            iDays = objDays.Days;
            


            if (sEffDate == "")
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.EffectiveDate", base.ClientId));
            }
            else if (sExpDate == "")
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.ExpirationDate", base.ClientId));
            }
            else if (iDays < 0)
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.DateValidation1", base.ClientId));
            }
            else if ((objEffDateUI < objEffectiveDate) || (objEffDateUI > objExpirationDate))
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.DateValidation2", base.ClientId));
            }
            else if ((objExpDateUI < objEffectiveDate) || (objExpDateUI > objExpirationDate))
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.DateValidation3", base.ClientId));
            }
            else if (sCancelDate != "")
            {
                // npadhy Start MITS 16357 After Cancelling the Policy, When we try to change the Exposure in Audit Policy, It throws error
                // objCancelDate = Convert.ToDateTime(sCancelDate);
                objCancelDate = Conversion.ToDate(sCancelDate);
                // npadhy End MITS 16357 After Cancelling the Policy, When we try to change the Exposure in Audit Policy, It throws error
                if (objEffDateUI >= objCancelDate)
                {
                    throw new RMAppException(Globalization.GetString("ExposureManager.Validate.CancelDateValidation1", base.ClientId));
                }
                // npadhy Start MITS 16357 This was an extra check which was causing all the validations to fail
                //else if (objExpDateUI > objCancelDate)
                //{
                //    throw new RMAppException(Globalization.GetString("ExposureManager.Validate.cancelDateValidation2"));
                //}
                // npadhy End MITS 16357 This was an extra check which was causing all the validations to fail
            }
            else if (sTransactionType == "AU")
            {
                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + objPolicyXExpEnh.PolicyId + " AND TERM_NUMBER = " + objPolicyXExpEnh.TermNumber + "AND EFFECTIVE_DATE <=" + Conversion.ToDbDate(objEffDateUI) + " AND TRANSACTION_STATUS=" + base.CCacheFunctions.GetCodeIDWithShort("OK", "TRANSACTION_STATUS") + " AND TRANSACTION_TYPE = " + base.CCacheFunctions.GetCodeIDWithShort("AU", "POLICY_TXN_TYPE");
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iTransIdMax = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                sSQL = "SELECT POLICY_STATUS FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iTransIdMax;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                {
                    sStatus = base.CCacheFunctions.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    if (sStatus == "CF " || sStatus == "CPR")
                    {
                        throw new RMAppException(Globalization.GetString("CoverageManager.Validate.CoverageEffectiveDate", base.ClientId));
                    }
                }
                objReader.Close();
            }

            else
                bValidate = true;

            return bValidate;
        }
        // Start Naresh Added the Function here to Get the Coverage Number. 
        // In Datamodel this was creating Problem
        private int GetExpCount(PolicyEnh p_objPolicyEnh)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            int Count = 0;
            sSQL = "SELECT MAX(EXPOSURE_COUNT) FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + p_objPolicyEnh.PolicyId;
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
                Count = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
            foreach (PolicyXExpEnh obj in p_objPolicyEnh.PolicyXExposureEnhList)
                Count = Math.Max(Count, obj.ExposureCount);
            // Start Naresh Connection Leak Changes
            objReader.Close();
            return Count + 1;
        }
        // End Naresh Function ends

        //Anu Tennyson for MITS 18229 1/20/2010 STARTS--exposure - For Vehicle UAR - For Vehicle UAR Starts
        public XmlDocument PopulateSearchVehUAR(XmlDocument objXmlIn)
        {
            XmlDocument objReturnDoc = null;
            string sUnitID = string.Empty;
            StringBuilder sbSql = null;
            string sVIN = string.Empty;
            string sVehicleMake = string.Empty;
            string sVehicleModel = string.Empty;
            string sStateShortID = string.Empty;
            string sStateDesc = string.Empty;
            int iVehicleYear = 0;
            int iState = 0;
            int iLicenseNumber = 0;
            //int iUnitTypeCode = 0;
            bool m_bIsSuccess = false;
            XmlElement objRoot = null;
            LocalCache objLocalCache = null;

            sUnitID = objXmlIn.SelectSingleNode("//UnitID").InnerText;
            try
            {
                sbSql = new StringBuilder();

                sbSql.Append("SELECT VIN, VEHICLE_MAKE, VEHICLE_MODEL, VEHICLE_YEAR, STATE_ROW_ID, LICENSE_NUMBER, UNIT_TYPE_CODE ");
                sbSql.Append("FROM VEHICLE WHERE UNIT_ID = ");
                sbSql.Append(sUnitID);

                using (DbReader objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString()))
                {
                    if (objReader.Read())
                    {
                        objLocalCache = new LocalCache(base.ConnectionString,ClientId);

                        sVIN = objReader.GetValue("VIN").ToString();
                        sVehicleMake = objReader.GetValue("VEHICLE_MAKE").ToString();
                        sVehicleModel = objReader.GetValue("VEHICLE_MODEL").ToString();

                        string sVehicleYear = objReader.GetValue("VEHICLE_YEAR").ToString();
                        iVehicleYear = Conversion.CastToType<int>(sVehicleYear, out m_bIsSuccess);

                        string sStateID = objReader.GetValue("STATE_ROW_ID").ToString();
                        iState = Conversion.CastToType<int>(sStateID, out m_bIsSuccess);
                        //string sStateShortID = objLocalCache.GetStateCode(iState);
                        //string sStateDesc = objLocalCache.GetCodeDesc(iState);
                        objLocalCache.GetStateInfo(iState, ref sStateShortID, ref sStateDesc);
                        string sState = sStateShortID + " " + sStateDesc;

                        string sLicenseNumber = objReader.GetValue("LICENSE_NUMBER").ToString();
                        iLicenseNumber = Conversion.CastToType<int>(sLicenseNumber, out m_bIsSuccess);

                        //string sUnitTypeCodeID = objReader.GetValue("UNIT_TYPE_CODE").ToString();
                        //iUnitTypeCode = Conversion.CastToType<int>(sUnitTypeCodeID, out m_bIsSuccess);
                        //string sUnitTypeShortCode = objLocalCache.GetShortCode(iUnitTypeCode);
                        //string sUnitTypeDesc = objLocalCache.GetCodeDesc(iUnitTypeCode);
                        //string sUnitTypeCode = sUnitTypeShortCode + " " + sUnitTypeDesc;

                        //Sumit - Start(03/16/2010) - MITS# 18229 -Update Document Path and append UAR Id.
                        base.StartDocument(ref objReturnDoc, ref objRoot, "PolicyXUar");
                        base.CreateAndSetElement(objRoot, "VIN", sVIN);
                        base.CreateAndSetElement(objRoot, "VehicleMake", sVehicleMake);
                        base.CreateAndSetElement(objRoot, "VehicleModel", sVehicleModel);
                        base.CreateAndSetElement(objRoot, "VehicleYear", iVehicleYear.ToString());
                        base.CreateAndSetElement(objRoot, "State", sState);
                        base.CreateAndSetElement(objRoot, "LicenseNumber", iLicenseNumber.ToString());
                        //base.CreateAndSetElement(objRoot, "UnitTypeCode", sUnitTypeCode);
                        base.CreateAndSetElement(objRoot, "UarId", sUnitID);
                        //Sumit - End
                    }
                }
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.PopulateSearchVehUAR.Error", base.ClientId), objEx);
            }
            finally
            {

            }

            return objReturnDoc;
        }
        //Anu Tennyson for MITS 18229 1/20/2010 -exposure - For Vehicle UAR ENDS - For Vehicle UAR Ends

        public void RenderExposureList(XmlDocument xmlDocument, int iTransNumber, Hashtable objExposuresUniqueIds)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 10/19/2010
        /// MITS# : 21996
        /// Description : Fetch field ID from Search dictionary of required field.
        /// </summary>
        /// <param name="sFieldTable"></param>
        /// <param name="sFieldName"></param>
        /// <param name="iCatID"></param>
        /// <returns></returns>
        private int GetFieldID(String sFieldTable,String sFieldName,int iCatID)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iFieldID = 0;
            try
            {
                sSQL = "SELECT FIELD_ID FROM SEARCH_DICTIONARY WHERE FIELD_TABLE='" + sFieldTable + "'" + " AND FIELD_NAME='" + sFieldName + "'" + " AND CAT_ID=" + iCatID;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    iFieldID = objReader.GetInt32("FIELD_ID");
                }
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.GetFieldID.Error", base.ClientId), objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
            return iFieldID;
        }
    }
}
