﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.EnhancePolicy.Billing ;
using System.Globalization;
using System.Threading;

namespace Riskmaster.Application.EnhancePolicy
{
    /**************************************************************
	 * $File		: TransactionManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
    public class TransactionManager : Common
    {
        #region Constructor

        internal TransactionManager(PolicyEnh PolicyEnh): base( PolicyEnh.Context )
        {
            m_objPolicyEnh = PolicyEnh;
        }
        
        #endregion 

        #region Global Variables
        
        private PolicyEnh m_objPolicyEnh = null;

        private PolicyEnh PolicyEnh
        {
            get
            {
                return (m_objPolicyEnh);
            }
        }

        #endregion 

        public bool TransactionAlreadyAccepted(int p_iTransactionId)
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            int iTransactionStatusCodeId = 0 ;

            try
            {
                sSQL = " SELECT TRANSACTION_STATUS FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + p_iTransactionId ;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                {
                    iTransactionStatusCodeId = Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_STATUS"), base.ClientId);
                    if (base.LocalCache.GetShortCode(iTransactionStatusCodeId) != Constants.TRANSACTION_STATUS_PROVISIONAL)
                        return true;
                    else
                        return false;
                }
                return false;
            }
            finally
            {
                if( objReader != null )
                    objReader.Close();
            }
        }

        public bool TransactionAlreadyDeleted(int p_iTransactionId)
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            
            try
            {
                sSQL = " SELECT TRANSACTION_ID FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + p_iTransactionId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                return (objReader.Read() ? false : true);
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
            }
        }

        public void AcceptTransaction(int p_iTransactionId)
        {
            PolicyXTransEnh objPolicyXTransEnhTemp = null;
            Billing.BillingMaster objBillingMaster = null;
            DbReader objReader = null;
            DbConnection objConn = null;

            string sSQL = string.Empty;
            string sTransactionType = string.Empty;
            int iTxnStatus = 0;
            int iPolicyStatus = 0 ;
            int iTranPolicyStatus = 0 ;
            int iTermStatus = 0 ;
            int iCvgExpStatus = 0 ;
            int iRenewStatus = 0 ;
            int iTermNumber = 0;
            int iTermId = 0;
            int iTermSequenceAlpha = 0;
            // Naresh Changes for Do Not Bill
            bool bDoNotBill = false;
            try
            {
                objPolicyXTransEnhTemp = (PolicyXTransEnh)base.DataModelFactory.GetDataModelObject("PolicyXTransEnh", false);
                objPolicyXTransEnhTemp.MoveTo(p_iTransactionId);
                
                sSQL = " SELECT DONOTBILL FROM POLICY_X_BILL_ENH WHERE POLICY_ID = "
                                + PolicyEnh.PolicyId + " AND TRANSACTION_ID = " + p_iTransactionId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    bDoNotBill = Conversion.ConvertObjToBool(objReader.GetValue(0), base.ClientId);
                objReader.Close();
                
                sTransactionType = base.LocalCache.GetShortCode(objPolicyXTransEnhTemp.TransactionType);
                iTxnStatus = base.CCacheFunctions.GetCodeIDWithShort("OK", "TRANSACTION_STATUS");

                switch (sTransactionType)
                {
                    case "EN" :
                        iPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTranPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTermStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "TERM_STATUS");
                        iCvgExpStatus = base.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");
                        break;
                    case "RN" :
                        iPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTranPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTermStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "TERM_STATUS");
                        iCvgExpStatus = base.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");
                        iRenewStatus = base.CCacheFunctions.GetCodeIDWithShort("RN", "TERM_STATUS");

                        // Find the latest term record and subtract one to find the previous term record
                        sSQL = " SELECT TERM_NUMBER FROM POLICY_X_TERM_ENH WHERE POLICY_ID = "
                                + PolicyEnh.PolicyId + " AND TRANSACTION_ID = " + p_iTransactionId;
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                        if (objReader.Read())
                        {
                            iTermNumber = Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), base.ClientId);
                            iTermNumber--;
                        }
                        objReader.Close();

                        // update the previous term record with the "renewed" term status
                        sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId
                                + " AND SEQUENCE_ALPHA = (SELECT MAX(SEQUENCE_ALPHA)"
                                + " FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId
                                + " AND TERM_NUMBER = " + iTermNumber + ")";
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReader.Read())
                        {
                            iTermId = Conversion.ConvertObjToInt(objReader.GetValue("TERM_ID"), base.ClientId);
                            iTermSequenceAlpha = Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), base.ClientId);

                            sSQL = " UPDATE POLICY_X_TERM_ENH SET TERM_STATUS = " + iRenewStatus
                                    + "  WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_ID = " + iTermId;

                            objConn = DbFactory.GetDbConnection(base.ConnectionString);
                            objConn.Open();
                            objConn.ExecuteNonQuery(sSQL);
                            objConn.Close();                            
                        }
                        objReader.Close();
                        break;
                    case "AU" :
                        iCvgExpStatus = base.CCacheFunctions.GetCodeIDWithShort("AU", "COVEXP_STATUS");
                        break;
                    case "CPR" :
                        iPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("C", "POLICY_STATUS");
                        iTranPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("CPR", "POLICY_STATUS");
                        iTermStatus = base.CCacheFunctions.GetCodeIDWithShort("C", "TERM_STATUS");
                        iCvgExpStatus = base.CCacheFunctions.GetCodeIDWithShort("CN", "COVEXP_STATUS");
                        break;
                    case "CF" :
                        iPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("C", "POLICY_STATUS");
                        iTranPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("CF", "POLICY_STATUS");
                        iTermStatus = base.CCacheFunctions.GetCodeIDWithShort("C", "TERM_STATUS");
                        iCvgExpStatus = base.CCacheFunctions.GetCodeIDWithShort("CN", "COVEXP_STATUS");
                        break;
                    case "RL" :
                        iPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTranPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTermStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "TERM_STATUS");
                        iCvgExpStatus = base.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");
                        break;
                    case "RNL" :
                        iPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTranPolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                        iTermStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "TERM_STATUS");
                        iCvgExpStatus = base.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");
                        break ;
                    default :
                        return ;                                
                }

                // Now loop through and set the statuses                 
                if (iPolicyStatus != 0)
                    PolicyEnh.PolicyStatusCode = iPolicyStatus;

                foreach (PolicyXTermEnh objPolicyXTermEnh in PolicyEnh.PolicyXTermEnhList)
                    if (objPolicyXTermEnh.TransactionId == p_iTransactionId)
                        objPolicyXTermEnh.TermStatus = iTermStatus;

                foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                    if (objPolicyXCvgEnh.TransactionId == p_iTransactionId && objPolicyXCvgEnh.Status != base.CCacheFunctions.GetCodeIDWithShort("DE", "COVEXP_STATUS"))
                        objPolicyXCvgEnh.Status = iCvgExpStatus;

                foreach( PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList )
                    if (objPolicyXExpEnh.TransactionId == p_iTransactionId && objPolicyXExpEnh.Status != base.CCacheFunctions.GetCodeIDWithShort("DE", "COVEXP_STATUS"))
                        objPolicyXExpEnh.Status = iCvgExpStatus;

                foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId == p_iTransactionId)
                    {
                        objPolicyXTransEnh.TransactionStatus = iTxnStatus;
                        if (iTranPolicyStatus != 0)
                            objPolicyXTransEnh.PolicyStatus = iTranPolicyStatus;
                        objPolicyXTransEnh.DateApproved = Conversion.ToDbDate(DateTime.Now);
                    }
                }


                // For Billing System
                // Post the transaction to Billing 
                if (base.UseBillingSystem)
                {
                    foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId == p_iTransactionId)
                        {
                            if ((objPolicyXTransEnh.WaivePremiumInd == false && objPolicyXTransEnh.ChgBilledPremium != 0) || (sTransactionType == "RN"))
                            {
                                objBillingMaster = new Billing.BillingMaster(PolicyEnh.Context.Factory,base.ClientId);
                                objBillingMaster.Policies.Add(PolicyEnh.PolicyId, PolicyEnh);
                                if (!bDoNotBill)
                                    base.PostToBilling(objBillingMaster, p_iTransactionId, sTransactionType, PolicyEnh);
                                
                                objBillingMaster.Save();
                            }
                        }
                    }
                }

            }
            finally
            {
                objPolicyXTransEnhTemp = null;
                if (objReader != null)
                    objReader.Close();
                if (objConn != null)
                    objConn.Close();
            }

        }

        public void DeleteTransaction(int p_iTransactionId)
        {
            try
            {
                // Remove Discount
                foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
                    if (objPolicyXDcntEnh.TransactionId == p_iTransactionId)
                        PolicyEnh.PolicyXDiscountEnhList.Remove(objPolicyXDcntEnh.DiscountId);
                        
                // Remove Discount Tiers
                foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
                    if (objPolicyXDtierEnh.TransactionId == p_iTransactionId)
                        PolicyEnh.PolicyXDiscountTierEnhList.Remove(objPolicyXDtierEnh.DiscountTierId);
                
                // Remove Rating record.
                foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                    if (objPolicyXRtngEnh.TransactionId == p_iTransactionId)
                        PolicyEnh.PolicyXRatingEnhList.Remove(objPolicyXRtngEnh.RatingId);

                // Remove Exposure record.
                foreach (PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList)
                    if (objPolicyXExpEnh.TransactionId == p_iTransactionId)
                        PolicyEnh.PolicyXExposureEnhList.Remove(objPolicyXExpEnh.ExposureId);

                // Remove Coverage record.
                foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                    if (objPolicyXCvgEnh.TransactionId == p_iTransactionId)
                        PolicyEnh.PolicyXCvgEnhList.Remove(objPolicyXCvgEnh.PolcvgRowId);

                // Remove Term record.
                foreach (PolicyXTermEnh objPolicyXTermEnh in PolicyEnh.PolicyXTermEnhList)
                    if (objPolicyXTermEnh.TransactionId == p_iTransactionId)
                        PolicyEnh.PolicyXTermEnhList.Remove(objPolicyXTermEnh.TermId);    
                
                // Remove Transaction.
                PolicyEnh.PolicyXTransEnhList.Remove(p_iTransactionId);
            }
            finally
            {                
            }

        }

        public void RenderTransactionList(XmlDocument objSysEx, int p_iTransNumber )
        {
            XmlElement objTransactionListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;

            try
            {
                //Manish Jain for multicurrency
                //string culture = CommonFunctions.GetCulture(0, CommonFunctions.NavFormType.None, base.ConnectionString);
                //Thread.CurrentThread.CurrentCulture = new CultureInfo(culture, false);

                objTransactionListElement = (XmlElement)objSysEx.SelectSingleNode("/SysExData/TransactionList");
                if (objTransactionListElement != null)
                    objTransactionListElement.ParentNode.RemoveChild(objTransactionListElement);

                base.CreateElement(objSysEx.DocumentElement, "TransactionList", ref objTransactionListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objTransactionListElement, "listhead", ref objListHeadXmlElement);
                //Sumit-MITS#18251 -08/09/2009 -Added ProcessDate as the first column to capture the system date
               
                base.CreateAndSetElement(objListHeadXmlElement, "ProcessDate", "Process Date");
              
                base.CreateAndSetElement(objListHeadXmlElement, "TransactionType", "Transaction Type");
                base.CreateAndSetElement(objListHeadXmlElement, "TransactionStatus", "Transaction Status");
                base.CreateAndSetElement(objListHeadXmlElement, "EffectiveDate", "Effective Date");
                base.CreateAndSetElement(objListHeadXmlElement, "ExpirationDate", "Expiration Date");
                base.CreateAndSetElement(objListHeadXmlElement, "TransactionPremium", "Transaction Premium");
                base.CreateAndSetElement(objListHeadXmlElement, "EstimatedPremium", "Estimated Premium");
                base.CreateAndSetElement(objListHeadXmlElement, "BilledPremium", "Billed Premium");
                base.CreateAndSetElement(objListHeadXmlElement, "ChangeInBilledPremium", "Change In Billed Premium");
                base.CreateAndSetElement(objListHeadXmlElement, "WaivedPremiumInd", "Waived Premium Ind");
                base.CreateAndSetElement(objListHeadXmlElement, "Term", "Term");
                //Sumit-MITS#18251 -08/09/2009 -Added Comments as the last column to capture the user comments
                base.CreateAndSetElement(objListHeadXmlElement, "Comments", "Comments");

                // Start MITS 9871 call to Old SortedList commented as it was creating problem while Rendering 
                // the Transaction List Properly in Case of Audit
                //for (int iindex = 0; iindex < base.policyxtransenhsorted(policyenh, true).count; iindex++)
                //{
                //    policyxtransenh objpolicyxtransenh = (policyxtransenh)base.policyxtransenhsorted(policyenh, false).getbyindex(iindex);

                //    base.createelement(objtransactionlistelement, "option", ref objoptionxmlelement);
                //    objoptionxmlelement.setattribute("ref", constants.instance_sysexdata_path + objoptionxmlelement.parentnode.localname + "/option[" + iindex.tostring() + "]");

                //    base.createandsetelement(objoptionxmlelement, "type", base.localcache.getcodedesc(objpolicyxtransenh.transactiontype));
                //    base.createandsetelement(objoptionxmlelement, "status", base.localcache.getcodedesc(objpolicyxtransenh.transactionstatus));
                //    base.createandsetelement(objoptionxmlelement, "effectivedate", conversion.getdbdateformat(objpolicyxtransenh.effectivedate, "d"));
                //    base.createandsetelement(objoptionxmlelement, "expirationdate", conversion.getdbdateformat(objpolicyxtransenh.expirationdate, "d"));
                //    base.createandsetelement(objoptionxmlelement, "transactionpremium", string.format("{0:c}", objpolicyxtransenh.transactionprem));
                //    base.createandsetelement(objoptionxmlelement, "estimatedpremium", string.format("{0:c}", objpolicyxtransenh.totalestpremium));
                //    base.createandsetelement(objoptionxmlelement, "billedpremium", string.format("{0:c}", objpolicyxtransenh.totalbilledprem));
                //    base.createandsetelement(objoptionxmlelement, "billedpremium", string.format("{0:c}", objpolicyxtransenh.chgbilledpremium));
                //    base.createandsetelement(objoptionxmlelement, "billedpremium", objpolicyxtransenh.waivepremiumind ? "y" : "n");
                //    base.createandsetelement(objoptionxmlelement, "term", objpolicyxtransenh.termnumber.tostring());
                //    base.createandsetelement(objoptionxmlelement, "transactionid", objpolicyxtransenh.transactionid.tostring());
                //    base.createandsetelement(objoptionxmlelement, "statusshortcode", base.localcache.getshortcode(objpolicyxtransenh.transactionstatus));
                //}

                // Start MITS 9871 call to New SortedList added to Render the Transaction List Properly in Case of Audit
                for (int iIndex = 0; iIndex < base.PolicyXTermSorted(PolicyEnh, true).Count; iIndex++)
                {
                    PolicyXTransEnh objPolicyXTransEnh = (PolicyXTransEnh)base.PolicyXTermSorted(PolicyEnh, false).GetByIndex(iIndex);

                    base.CreateElement(objTransactionListElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SYSEXDATA_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex.ToString() + "]");

                    //Sumit-MITS#18251 -08/09/2009 -Added ProcessDate as the first column to capture the system date
                    base.CreateAndSetElement(objOptionXmlElement, "ProcessDate", Conversion.GetDBDateFormat(objPolicyXTransEnh.DttmRcdAdded, "d"));
                    base.CreateAndSetElement(objOptionXmlElement, "TransactionType", base.LocalCache.GetCodeDesc(objPolicyXTransEnh.TransactionType));
                    base.CreateAndSetElement(objOptionXmlElement, "TransactionStatus", base.LocalCache.GetCodeDesc(objPolicyXTransEnh.TransactionStatus));
                    base.CreateAndSetElement(objOptionXmlElement, "EffectiveDate", Conversion.GetDBDateFormat(objPolicyXTransEnh.EffectiveDate, "d"));
                    base.CreateAndSetElement(objOptionXmlElement, "ExpirationDate", Conversion.GetDBDateFormat(objPolicyXTransEnh.ExpirationDate, "d"));
                    base.CreateAndSetElement(objOptionXmlElement, "TransactionPremium", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXTransEnh.TransactionPrem, base.ConnectionString, ClientId));
                    base.CreateAndSetElement(objOptionXmlElement, "EstimatedPremium", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXTransEnh.TotalEstPremium, base.ConnectionString, ClientId));
                    base.CreateAndSetElement(objOptionXmlElement, "BilledPremium", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXTransEnh.TotalBilledPrem, base.ConnectionString, ClientId));
                    base.CreateAndSetElement(objOptionXmlElement, "ChangeInBilledPremium", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXTransEnh.ChgBilledPremium, base.ConnectionString, ClientId));
                    base.CreateAndSetElement(objOptionXmlElement, "WaivedPremiumInd", objPolicyXTransEnh.WaivePremiumInd ? "Y" : "N");
                    base.CreateAndSetElement(objOptionXmlElement, "Term", objPolicyXTransEnh.TermNumber.ToString()); 
                    //Sumit-MITS#18251 -08/09/2009 -Added for Policy Comments
                    base.CreateAndSetElement(objOptionXmlElement, "Comments", Conversion.ConvertObjToStr(objPolicyXTransEnh.Comments)!=""? "View" : "");
                    //Sumit-MITS#18251 -10/14/2009 -Added Short Code of Transaction Type as well
                    base.CreateAndSetElement(objOptionXmlElement, "StatusShortCode", base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus) + "_" + base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType));
                    base.CreateAndSetElement(objOptionXmlElement, "TransactionId", objPolicyXTransEnh.TransactionId.ToString());
                }
                base.ResetSysExData( objSysEx , "SelectedTransactionId", p_iTransNumber.ToString());

                // R5 Implementation
                base.ResetSysExData(objSysEx, "TransactionHistorySelectedId", p_iTransNumber.ToString());
            }
            finally
            {
                objTransactionListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
            }
        }

    }
}
