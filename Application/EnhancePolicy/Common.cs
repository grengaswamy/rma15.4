﻿

using System;
using System.Xml ;
using Riskmaster.Common ;
using Riskmaster.Settings;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using System.Collections ;
using Riskmaster.Db;
//Anu Tennyson for MITS 18229 STARTS
using System.Text;
//Anu Tennyson for MITS 18229 ENDS
using Riskmaster.Security;

namespace Riskmaster.Application.EnhancePolicy
{
    /**************************************************************
     * $File		: Common.cs
     * $Revision	: 1.0.0.0
     * $Date		: 
     * $Author		: Vaibhav Kaushik
     * $Comment		:  
     * $Source		:  	
    **************************************************************/
    public class Common : Constants
    {
        private int m_iClientId = 0;

        public int ClientId
        {
            get { return m_iClientId; }
            set { m_iClientId = value; }
        }


        UserLogin m_oUserLogin = null;

        internal Common(Context p_objContext ):base()
        {
            m_objContext = p_objContext;
            m_bRoundFlag = setRoundFlag();

        }
        internal Common(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
            : base()
        {
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);
            m_objContext = m_objDataModelFactory.Context;
            m_bRoundFlag = setRoundFlag();

        }
        //vkumar258 - ML changes RMA-6037 Starts
        internal Common(UserLogin p_oUserLogin, string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
            : base()
        {
            m_oUserLogin = p_oUserLogin;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);
            m_objContext = m_objDataModelFactory.Context;
            m_bRoundFlag = setRoundFlag();

        }
        //vkumar258 - ML changes RMA-6037 End

        #region Variable Declaration
        private Context m_objContext = null;
        private DataModelFactory m_objDataModelFactory = null;
        #endregion

        #region Variable Properties

        public string ConnectionString
        {
            get
            {
				return( m_objContext.DbConn.ConnectionString ) ;
            }
        }

        public DataModelFactory DataModelFactory
        {
            get
            {
				return( m_objContext.Factory ) ;
            }
        }

        public LocalCache LocalCache
        {
            get
            {
                return( m_objContext.LocalCache );
            }
        }

        public CCacheFunctions CCacheFunctions
        {
            get
            {
                return (m_objContext.InternalSettings.CacheFunctions);
            }
        }

        public bool UseBillingSystem
        {
            get
            {
                int iUseBillFlag;
                bool bRetFlag = false;

                // TODO : For demo thing, just commenting out this. 
                iUseBillFlag = DataModelFactory.Context.InternalSettings.SysSettings.UseBillingFlag;

                if (iUseBillFlag != 0)
                {
                    bRetFlag = true;
                }

                return bRetFlag;
            }
        }

        private bool m_bRoundFlag = false;
        public bool RoundFlag
        {
            get
            {
                //return (DataModelFactory.Context.InternalSettings.SysSettings.UseRoundFlag);         
                 return m_bRoundFlag;
            }
        }
        
        //Divya 03/29/2006 : Round flag settings -start
        private bool setRoundFlag()
        {
            DbReader objreader = null;
            bool bFlag = false;
            objreader = DbFactory.GetDbReader(m_objContext.DbConn.ConnectionString, "SELECT ROUND_AMTS_FLAG FROM SYS_POL_OPTIONS");
            if (objreader.Read())
                bFlag = Conversion.ConvertObjToBool(objreader.GetValue(0), m_iClientId);

            objreader.Close();
            return bFlag;

        }                
        #endregion

        #region Common Functions Used across the Assembly.

        private SortedList objPolicyXTransEnhSorted = null;
        public SortedList PolicyXTransEnhSorted(PolicyEnh p_objPolicyEnh, bool p_bReloadSortedCollection)
        {
            if (objPolicyXTransEnhSorted == null || p_bReloadSortedCollection)
            {
                string sKeyValue = string.Empty;
                objPolicyXTransEnhSorted = new SortedList();
                foreach (PolicyXTransEnh objPolicyXTransEnh in p_objPolicyEnh.PolicyXTransEnhList)
                {
                    sKeyValue = objPolicyXTransEnh.DttmRcdAdded;
                    objPolicyXTransEnhSorted.Add(sKeyValue, objPolicyXTransEnh);
                }
            }
            return (objPolicyXTransEnhSorted);
        }
        // Start MITS 9871 New SortedList Created to Render the Transaction List Properly in Case of Audit
        private SortedList objPolicyXTermSorted = null;
        public SortedList PolicyXTermSorted(PolicyEnh p_objPolicyEnh, bool p_bReloadSortedCollection)
        {
            if (objPolicyXTermSorted == null || p_bReloadSortedCollection)
            {
                string sKeyValue = string.Empty;
                int iKey = 0;
                objPolicyXTermSorted = new SortedList();
                foreach (PolicyXTransEnh objPolicyXTransEnh in p_objPolicyEnh.PolicyXTransEnhList)
                {
                    sKeyValue = objPolicyXTransEnh.TermNumber.ToString() + objPolicyXTransEnh.TransactionId.ToString();
                    iKey = Conversion.ConvertStrToInteger(sKeyValue);
                    objPolicyXTermSorted.Add(iKey, objPolicyXTransEnh);
                }
            }
            return (objPolicyXTermSorted);
        }
        // End MITS 9871 New SortedList Created to Render the Transaction List Properly in Case of Audit

        public PolicyXTransEnh GetLatestTran( PolicyEnh p_objPolicyEnh, bool p_bReloadSortedCollection )
        {
            this.PolicyXTransEnhSorted(p_objPolicyEnh, p_bReloadSortedCollection);
            if (objPolicyXTermEnhSorted.Count > 0)
                return ((PolicyXTransEnh)objPolicyXTermEnhSorted.GetByIndex(objPolicyXTermEnhSorted.Count - 1));
            else
                return null;
        }

        // Get the Latest Term.
        private SortedList objPolicyXTermEnhSorted = null;
        private SortedList PolicyXTermEnhSorted( PolicyEnh p_objPolicyEnh )
        {
            if (objPolicyXTermEnhSorted == null)
            {
                string sKeyValue = string.Empty;
                objPolicyXTermEnhSorted = new SortedList();
                foreach (PolicyXTermEnh objPolicyXTermEnh in p_objPolicyEnh.PolicyXTermEnhList)
                {
                    sKeyValue = objPolicyXTermEnh.DttmRcdAdded;
                    objPolicyXTermEnhSorted.Add(sKeyValue, objPolicyXTermEnh);
                }
            }
            return (objPolicyXTermEnhSorted);
        }
        public PolicyXTermEnh GetLatestTerm( PolicyEnh p_objPolicyEnh )
        {
            this.PolicyXTermEnhSorted(p_objPolicyEnh);
            if (objPolicyXTermEnhSorted.Count > 0)
                return ((PolicyXTermEnh)objPolicyXTermEnhSorted.GetByIndex(objPolicyXTermEnhSorted.Count - 1));
            else
                return null;
        }

        public bool IsWithinTermDates(DateTime sDate, long lType, DateTime sCompDate)
        {
            DateTime objsDate = System.DateTime.Today;
            DateTime objsCompDate = System.DateTime.Today;
            TimeSpan objDays;
            int iDays = 0;
            bool IsWithinTermDates = true;

            objsDate = sDate;
            objsCompDate = sCompDate;

            switch (lType)
            {
                case 1:
                    objDays = objsDate.Subtract(System.DateTime.Today);
                    iDays = objDays.Days;
                    if (iDays > 0)
                        IsWithinTermDates = true;
                    else
                        IsWithinTermDates = false;
                    break;

                case 2:
                    objDays = System.DateTime.Today.Subtract(objsDate);
                    iDays = objDays.Days;
                    if (iDays > 0)
                        IsWithinTermDates = true;
                    else
                        IsWithinTermDates = false;
                    break;
                case 3:
                    objDays = objsCompDate.Subtract(objsDate);
                    iDays = objDays.Days;
                    if (iDays >= 0)
                        IsWithinTermDates = true;
                    else
                        IsWithinTermDates = false;
                    break;
                case 4:
                    objDays = objsDate.Subtract(objsCompDate);
                    iDays = objDays.Days;
                    if (iDays > 0)
                        IsWithinTermDates = true;
                    else
                        IsWithinTermDates = false;
                    break;
            }

            return IsWithinTermDates;
        }

        internal string GetCodes(string sTableName)
        {
            string sSQL = "";
            string sRetVal = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE ,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY " +
                    " WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + sTableName + "'" +
                    " AND GLOSSARY.TABLE_ID=CODES.TABLE_ID" +
                    " AND CODES.CODE_ID=CODES_TEXT.CODE_ID" +
                    " AND CODES_TEXT.LANGUAGE_CODE=1033" +
                    " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)" +
                    " ORDER BY CODES.SHORT_CODE ASC";

                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);

                while (objReader.Read())
                {
                    if (sRetVal.Trim() == string.Empty)
                    {
                        sRetVal = Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")) + "~" + objReader.GetString("SHORT_CODE") + " " + objReader.GetString("CODE_DESC");
                    }
                    else
                    {
                        sRetVal = sRetVal + "^" + Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")) + "~" + objReader.GetString("SHORT_CODE") + " " + objReader.GetString("CODE_DESC");
                    }
                }

                objReader.Close();

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Function.GetEntityName.Error", ClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sRetVal);
        }

        #endregion

        #region Calculate Rate
        //Rate Calculation function. Added by Divya 01/07
        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
        //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        //public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound)
        //public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, int iPolicyType)
        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation ENDS
        //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        //Anu Tennyson for MITS 18229 : Prem Calc STARTS
        public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, int iPolicyType, string sOrgHIerarchy)
        //Anu Tennyson for MITS 18229 : Prem Calc ENDS
        {
            double dPrevPrPrem = 0;
            double dPrevFullPrem = 0;
            string sType = "";
            DateTime reinstDate = DateTime.MinValue;
            //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            //Rate(ref objE, iState, bRound, dPrevPrPrem, dPrevFullPrem, sType, reinstDate);
            Rate(ref objE, iState, bRound, dPrevPrPrem, dPrevFullPrem, sType, reinstDate, iPolicyType, sOrgHIerarchy);
            //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        }

        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
        //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        //public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, double dPrevPrPrem, double dPrevFullPrem, string sType)
        //public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, double dPrevPrPrem, double dPrevFullPrem, string sType, int iPolicyType)
        //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation ENDS
        public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, double dPrevPrPrem, double dPrevFullPrem, string sType, int iPolicyType, string sOrgHIerarchy)
        {
            DateTime reinstDate = DateTime.MinValue;
            //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
            //Rate(ref objE, iState, bRound, dPrevPrPrem, dPrevFullPrem, sType, reinstDate);
            Rate(ref objE, iState, bRound, dPrevPrPrem, dPrevFullPrem, sType, reinstDate, iPolicyType, sOrgHIerarchy);
            //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        }

        //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        //public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, double dPrevPrPrem, double dPrevFullPrem, string sType, DateTime reinsDate)
        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
        //public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, double dPrevPrPrem, double dPrevFullPrem, string sType, DateTime reinsDate, int iPolicyType)
        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation ENDS
        //Anu Tennyson for MITS 18229 : Prem Calc STARTS
        public void Rate(ref PolicyXExpEnh objE, int iState, bool bRound, double dPrevPrPrem, double dPrevFullPrem, string sType, DateTime reinsDate, int iPolicyType, string sOrgHierarchy)
        //Anu Tennyson for MITS 18229 : Prem Calc ENDS 
        //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
        {
            DbReader objReader = null;
            Double dFullYearAmt = 0, dPrem = 0, dAmtPerDay = 0, dAmendPrem = 0, dOrigPrem = 0, dOrigTotal = 0, dLastAmount = 0, dAmtDiff = 0, dFactor = 0;

            int lTermLength = 0, lOtherTran = 0, lSeq = 0, lCvgDays = 0;

            string sFlat = "", sEffDate, sExpDate = "", sFixed = "", sSQL = "", sPriorAmendDate = "", sSavePriorDate = "", sOtherType = "", sCvgStart = "", sCvgEnd = "", sCancelType = "", sOrigEffDate = "", sOrigExpDate = "";

            //pmahli MITS 9329 - Start
            string sTermEffDate = string.Empty;
            string sTermExpDate = string.Empty;
            int iPolicyId = 0;
            int iOrigTermLength = 0;
            iPolicyId = objE.PolicyId;
            //pmahli MITS 9329 - End
            //Anu Tennyson for MITS 18229 : Prem Calc Starts
            string sOrgH = string.Empty;
            //Anu Tennyson for MITS 18229 : Prem Calc Ends
            bool bStop;
            


            //GET THE RATE
            //must have exp type, state (from parent) and dates
            if ((objE.ExposureCode != 0) && (objE.EffectiveDate != "") && (objE.ExpirationDate != "") && (iState != 0))
            {


                //IF WE ARE DOING A RENEWAL, NEED TO GET NEW RATES, OTHERWISE, JUST NEED THE FLAT OR PERCENT
                sEffDate = objE.EffectiveDate;
                sExpDate = objE.ExpirationDate;
                //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
                //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
                //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
               // sSQL = "SELECT * FROM SYS_POL_EXP_RATES WHERE EXPOSURE_ID = " + objE.ExposureCode + " AND STATE = " + iState + " AND EFFECTIVE_DATE <= '" + sEffDate + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '" + sEffDate + "'))";
                //sSQL = "SELECT * FROM SYS_POL_EXP_RATES WHERE EXPOSURE_ID = " + objE.ExposureCode + " AND STATE = " + iState + " AND LINE_OF_BUSINESS = " + iPolicyType + " AND EFFECTIVE_DATE <= '" + sEffDate + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '" + sEffDate + "'))";
                //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
                //Anu Tennyson for MITS 18229 : Prem Calc STARTS
                sOrgH = OrgHierarchy(sOrgHierarchy);
                sSQL = "SELECT * FROM SYS_POL_EXP_RATES WHERE EXPOSURE_ID = " + objE.ExposureCode + " AND STATE = " + iState + " AND LINE_OF_BUSINESS = " + iPolicyType + " AND ORG_HIERARCHY IN (" + sOrgH + " ) AND EFFECTIVE_DATE <= '" + sEffDate + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '" + sEffDate + "'))";
                //Anu Tennyson for MITS 18229 : Prem Calc ENDS
                objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    if ((sType == "CPR") || (sType == "CF") || (sType == "RL") || (sType == "RNL") || (this.LocalCache.GetShortCode(objE.Status) == "DE"))
                    {
                        sFlat = m_objContext.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("FLAT_OR_PERCENT"), m_iClientId));
                        sFixed = m_objContext.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("FIXED_OR_PRORATE"), m_iClientId));
                    }
                    else
                    {
                        objE.ExposureRate = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        objE.ExposureBaseRate = Conversion.ConvertObjToDouble(objReader.GetValue("BASE_RATE"), m_iClientId);
                        sFlat = m_objContext.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("FLAT_OR_PERCENT"), m_iClientId));
                        sFixed = m_objContext.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("FIXED_OR_PRORATE"), m_iClientId));
                    }
                }
                else
                {
                    objE.ExposureBaseRate = 0;
                    objE.ExposureRate = 0;
                }
                objReader.Close();

                //NOW RATE
                if (sFlat == "P") //is percentage
                    dFullYearAmt = objE.ExposureAmount * (objE.ExposureRate / 100);
                else //is flat
                    dFullYearAmt = objE.ExposureAmount * objE.ExposureRate;




                //rates are based on yearly amounts, so calc one year's premium


                //QUOTE, NEW BUSINESS, OR RENEWAL
                if (sType == "" || sType == "NB" || sType == "RN")
                {
                    if (sFixed == "P") //is pro-rated
                    {
                        // Changed by Naresh Term length = Expiry Date - Effective Date + 1
                        //TimeSpan temp = ((Conversion.ToDate(objE.EffectiveDate)).Subtract(Conversion.ToDate(objE.ExpirationDate)));
                        TimeSpan temp = ((Conversion.ToDate(objE.ExpirationDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                        lTermLength = temp.Days + 1;
                        if (lTermLength == 366)
                            lTermLength = 365;
                        //Divya MITS 8699 changes
                        //dFactor = Conversion.Round((lTermLength * 1.0 / 365), 3);
                        dFactor = lTermLength * 1.0 / 365;

                        
                        dPrem = dFactor * dFullYearAmt;


                        if (objE.ExposureBaseRate != 0)
                        {
                            dPrem = dPrem * (objE.ExposureBaseRate / 100);
                            dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);
                        }
                    }

                    else
                    {
                        //is fixed
                        if (objE.ExposureBaseRate != 0)
                        {
                            dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);
                            dPrem = dFullYearAmt * 1;
                        }
                        else
                        {
                            dPrem = dFullYearAmt * 1;
                        }
                    }

                    if (this.RoundFlag == true) //get rid of decimal places
                    {
                        dPrem = Math.Round(dPrem, 0, MidpointRounding.AwayFromZero);
                        dFullYearAmt = Math.Round(dFullYearAmt, 0, MidpointRounding.AwayFromZero);
                    }
                    else
                    {        //keep 2 decimal places
                        dPrem = Math.Round(dPrem, 2, MidpointRounding.AwayFromZero);//TODO
                        dFullYearAmt = Math.Round(dFullYearAmt, 2, MidpointRounding.AwayFromZero);
                    }

                    objE.FullAnnPremAmt = dFullYearAmt;
                    objE.PremAdjAmt = dPrem;
                    objE.PrAnnPremAmt = dPrem;
                }

                if (sType == "RL" || sType == "RNL")
                {
                    if (sFixed == "P") //is pro-rated
                    {
                        //pmahli MITS 9329-Start
                        //We determine what was the term length of the policy. If policy Term length is 366 Days 
                        //then we don't add 1 day to the reinsatement term length to take care of the 365 days instead of 366 days for the policy
                        sSQL = "SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyId;
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        if (objReader.Read())
                        {
                            sTermEffDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                            sTermExpDate = Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"));
                        }
                        objReader.Close();

                        TimeSpan temp = (Conversion.ToDate(sTermExpDate).Subtract(Conversion.ToDate(sTermEffDate)));
                        iOrigTermLength = temp.Days + 1;
                        //pmahli MITS 9329 -End 
                        temp = ((Conversion.ToDate(objE.ExpirationDate)).Subtract(reinsDate));
                        //pmahli MITS 9329 
                        //We determine what was the term length of the policy. If policy Term length is 366 Days 
                        //then we don't add 1 day to the reinsatement term length to take care of the 365 days instead of 366 days for the policy
                        if (iOrigTermLength == 366)
                            lTermLength = temp.Days;
                        else
                            lTermLength = temp.Days + 1;
                        
                        if (lTermLength == 366)
                            lTermLength = 365;

                        //Divya MITS 8699 changes
                        //dFactor = Conversion.Round((lTermLength * 1.0) / 365, 3);
                        dFactor = (lTermLength * 1.0) / 365;


                        dPrem = dFactor * dFullYearAmt;

                        if (objE.ExposureBaseRate != 0)
                        {
                            dPrem = dPrem * (objE.ExposureBaseRate / 100);
                            dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);

                        }

                        if (bRound == true)//get rid of decimal places
                            dPrem = Math.Round(dPrem, 0, MidpointRounding.AwayFromZero);
                        else //keep 2 decimal places
                            dPrem = Math.Round(dPrem, 2, MidpointRounding.AwayFromZero);




                        objE.PremAdjAmt = dPrem;
                        objE.PrAnnPremAmt = dPrem + dPrevPrPrem;
                    }
                    else
                    {
                        //read to find the greatest term sequence alpha
                        sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + objE.PolicyId + " AND TERM_NUMBER = " + objE.TermNumber;
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        if (objReader.Read())
                            lSeq = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        objReader.Close();



                        //read to find the correct term record
                        sSQL = "SELECT CANCEL_TYPE FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + objE.PolicyId + " AND TERM_NUMBER = " + objE.TermNumber + " and SEQUENCE_ALPHA = " + lSeq;
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        if (objReader.Read())
                            sCancelType = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        objReader.Close();


                        sSQL = "SELECT PR_ANN_PREM_AMT FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " AND SEQUENCE_ALPHA =" + (objE.SequenceAlpha - 1);
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        if (objReader.Read())
                            dPrevPrPrem = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        objReader.Close();
                        if ((sCancelType == "FL") || (dPrevPrPrem == 0))
                        {
                            if (objE.ExposureBaseRate != 0)
                            {
                                dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);
                                dPrem = dFullYearAmt * 1;
                            }
                            else
                                dPrem = dFullYearAmt * 1;

                            if (bRound == true)
                            {
                                dPrem = Math.Round(dPrem, 0, MidpointRounding.AwayFromZero);//TODO
                                dFullYearAmt = Math.Round(dFullYearAmt, 0, MidpointRounding.AwayFromZero);
                            }
                            else //todo
                            {
                                dPrem = Math.Round(dPrem, 2, MidpointRounding.AwayFromZero);
                                dFullYearAmt = Math.Round(dFullYearAmt, 2, MidpointRounding.AwayFromZero);
                            }
                            objE.PremAdjAmt = dPrem;
                            objE.PrAnnPremAmt = dPrem;
                            objE.FullAnnPremAmt = dFullYearAmt;
                        }
                    }
                }

                //AUDIT
                if (sType == "AU")
                {
                    if (sFixed == "P")
                    {
                        //need to make sure we only charge them for the number of days there was actually coverage for
                        sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID =" + objE.PolicyId + " AND EXPOSURE_COUNT=" + objE.ExposureCount;
                        sSQL = sSQL + " AND TERM_NUMBER =" + objE.TermNumber + " AND TRANSACTION_ID <>" + objE.TransactionId;
                        sSQL = sSQL + " AND TRANSACTION_TYPE <>" + this.LocalCache.GetCodeId("AU", "POLICY_TXN_TYPE") + " ORDER BY SEQUENCE_ALPHA";
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        bool bExist = false;
                        while (objReader.Read())
                        {
                            bExist = true;
                            #region While
                            if (this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("STATUS"), m_iClientId)) == "OK")
                            {
                                if (Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), m_iClientId) == 1)
                                {
                                    sCvgStart = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                                    sCvgEnd = Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"));
                                    TimeSpan temp = ((Conversion.ToDate(sCvgEnd)).Subtract(Conversion.ToDate(sCvgStart)));
                                    lCvgDays = temp.Days + 1;
                                }
                                else
                                {
                                    //pmahli MITS 9969
                                    //The query was missing so sOtherType was coming wrong
                                    sSQL = "SELECT * FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), m_iClientId);
                                    DbReader objReaderTemp = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                                    if (objReaderTemp.Read())
                                        sOtherType = this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReaderTemp.GetValue("TRANSACTION_TYPE"), m_iClientId));
                                    objReaderTemp.Close();

                                    if (sOtherType == "RL" || sOtherType == "RNL")
                                    {
                                        sSQL = "SELECT * FROM POLICY_X_TERM_ENH WHERE TRANSACTION_ID =" + Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), m_iClientId);
                                        objReaderTemp = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                                        if (objReaderTemp.Read())
                                            //pmahli MITS 9969 
                                            //wrong reader was used
                                            sCvgStart = Conversion.ConvertObjToStr(objReaderTemp.GetValue("REINSTATEMENT_DATE"));

                                        objReaderTemp.Close();
                                        TimeSpan temp = ((Conversion.ToDate(sCvgEnd)).Subtract(Conversion.ToDate(sCvgStart)));
                                        lCvgDays = lCvgDays + temp.Days + 1;
                                    }
                                }
                            }

                            if (this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("STATUS"), m_iClientId)) == "DE")
                            {
                                sCvgStart = Conversion.ConvertObjToStr(objReader.GetValue("AMENDED_DATE"));
                                TimeSpan temp = ((Conversion.ToDate(sCvgEnd)).Subtract(Conversion.ToDate(sCvgStart)));
                                //pmahli MITS 9969
                                //Braces was missing
                                lCvgDays = lCvgDays - (temp.Days + 1);
                                break;
                            }
                            if (this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("STATUS"), m_iClientId)) == "CN")
                            {
                                sCvgStart = Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"));
                                TimeSpan temp = ((Conversion.ToDate(sCvgEnd)).Subtract(Conversion.ToDate(sCvgStart)));
                                //pmahli MITS 9969
                                //Braces was missing
                                lCvgDays = lCvgDays - (temp.Days + 1);
                            }
                            #endregion
                        }

                        if (bExist)
                        {
                            if (lCvgDays == 366)
                                lCvgDays = 365;
                            //Mukul(07/09/2007) MITS 9984
                            //dFactor = Conversion.Round((lCvgDays * 1.0 / 365), 3); //TODO dRound((lCvgDays / 365), 3)
                            dFactor = (lCvgDays * 1.0) / 365;
                            dPrem = dFactor * dFullYearAmt;
                        }
                        else
                        {
                            objReader.Close();
                            #region If EOF then
                            //start with the dates of the exp
                            sCvgStart = objE.EffectiveDate;
                            sCvgEnd = objE.ExpirationDate;
                            TimeSpan temp = ((Conversion.ToDate(sCvgEnd)).Subtract(Conversion.ToDate(sCvgStart)));
                            lCvgDays = temp.Days + 1;
                            //now use all txns w/i those dates
                            sSQL = "SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID =" + objE.PolicyId + " AND TERM_NUMBER =" + objE.TermNumber + " AND TRANSACTION_ID <>" + objE.TransactionId;
                            sSQL = sSQL + " AND TRANSACTION_TYPE <>" + this.LocalCache.GetCodeId("AU", "POLICY_TXN_TYPE") + " AND EFFECTIVE_DATE >='" + objE.EffectiveDate + "' AND EFFECTIVE_DATE <= '" + objE.ExpirationDate;
                            sSQL = sSQL + "' AND TRANSACTION_STATUS=" + this.LocalCache.GetCodeId("OK", "TRANSACTION_STATUS") + " ORDER BY SEQUENCE_ALPHA";
                            objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                            while (objReader.Read())
                            {
                                sOtherType = this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), m_iClientId));
                                if (sOtherType == "RL" || sOtherType == "RNL")
                                {
                                    sSQL = "SELECT * FROM POLICY_X_TERM_ENH WHERE TRANSACTION_ID =" + Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), m_iClientId);
                                    DbReader objReaderTemp = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                                    if (objReaderTemp.Read())
                                        sCvgStart = Conversion.ConvertObjToStr(objReaderTemp.GetValue("REINSTATEMENT_DATE"));
                                    objReaderTemp.Close();
                                    TimeSpan temp2 = ((Conversion.ToDate(sCvgEnd)).Subtract(Conversion.ToDate(sCvgStart)));
                                    //pmahli MITS 9969
                                    //Braces was missing
                                    lCvgDays = lCvgDays + (temp2.Days + 1);
                                }
                                if (sOtherType == "CPR" || sOtherType == "CF")
                                {
                                    sCvgStart = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                                    TimeSpan temp2 = ((Conversion.ToDate(sCvgEnd)).Subtract(Conversion.ToDate(sCvgStart)));
                                    //pmahli MITS 9969
                                    //Braces was missing
                                    lCvgDays = lCvgDays - (temp2.Days + 1);
                                }
                            }
                            objReader.Close();
                            if (lCvgDays == 366)
                                lCvgDays = 365;

                            //Mukul(07/09/2007) MITS 9984
                            //dFactor = Conversion.Round((lCvgDays * 1.0 / 365), 3);//TODO dRound((lCvgDays / 365), 3)
                            dFactor = (lCvgDays * 1.0 )/ 365;
                            dPrem = dFactor * dFullYearAmt;
                            #endregion
                        }
                    }
                    else
                        //IS FIXED
                        dPrem = dFullYearAmt * 1;

                    objReader.Close();

                    if (objE.ExposureBaseRate != 0)
                    {
                        dPrem = dPrem * (objE.ExposureBaseRate / 100);
                        dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);
                    }

                    if (bRound == true)  //get rid of decimal places
                    {
                        dPrem = Math.Round(dPrem, 0, MidpointRounding.AwayFromZero);
                        dFullYearAmt = Math.Round(dFullYearAmt, 0, MidpointRounding.AwayFromZero);
                    }
                    else //keep 2 decimal places
                    {
                        dPrem = Math.Round(dPrem, 2, MidpointRounding.AwayFromZero);//TODO
                        dFullYearAmt = Math.Round(dFullYearAmt, 2, MidpointRounding.AwayFromZero);//TODO
                    }
                    objE.FullAnnPremAmt = dFullYearAmt;
                    objE.PrAnnPremAmt = dPrem;

                    //if we are auditing an audit, need to show the diff b/w the two for the exp in the PremAdjAmt
                    sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID =" + objE.PolicyId + " AND TERM_NUMBER = " + objE.TermNumber + " AND TRANSACTION_TYPE = " + this.LocalCache.GetCodeId(sType, "POLICY_TXN_TYPE") + " AND TRANSACTION_ID <> " + objE.TransactionId;
                    objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                    if (objReader.Read())
                        lOtherTran = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    objReader.Close();

                    sSQL = "SELECT * FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID=" + lOtherTran;
                    objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                    if (objReader.Read())
                        sOtherType = this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), m_iClientId));
                    objReader.Close();


                    sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE EXPOSURE_CODE =" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " AND POLICY_ID = " + objE.PolicyId + " AND TERM_NUMBER = " + objE.TermNumber + " AND TRANSACTION_ID = " + lOtherTran;
                    objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                    if (objReader.Read())
                        dOrigPrem = Conversion.ConvertObjToDouble(objReader.GetValue("PR_ANN_PREM_AMT"), m_iClientId);
                    objReader.Close();
                    objE.PremAdjAmt = dPrem - dOrigPrem;

                }


                //ENDORSEMENT/AMENDMENT
                if (sType == "EN")
                {

                    //calc the ann prem from the amendment date to the expiration date

                    if (this.LocalCache.GetShortCode(objE.Status) == "DE") //ARE DELETING THE EXPOSURE SO TREAT AS CANCEL PRORATA
                    {
                        sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID =" + objE.PolicyId + " AND EXPOSURE_CODE=" + objE.ExposureCode + "AND EXPOSURE_COUNT=" + objE.ExposureCount + " AND SEQUENCE_ALPHA = 1";
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        if (objReader.Read())
                            sOrigEffDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                        objReader.Close();
                        if (objE.AmendedDate == sOrigEffDate)
                        {
                            objE.FullAnnPremAmt = 0;
                            objE.PremAdjAmt = 0 - objE.PrAnnPremAmt;
                            objE.PrAnnPremAmt = 0;
                        }
                        else
                            if (sFixed == "F")
                            {
                                objE.PrAnnPremAmt = dPrevPrPrem;
                                objE.FullAnnPremAmt = dFullYearAmt;
                                objE.PremAdjAmt = 0;
                            }
                            else
                            {
                                bStop = false;
                                sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " ORDER BY SEQUENCE_ALPHA DESC";
                                objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                                while ((objReader.Read() == true) && (bStop == false))
                                {
                                    sPriorAmendDate = Conversion.ConvertObjToStr(objReader.GetValue("AMENDED_DATE"));
                                    sOtherType = this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), m_iClientId));
                                    if ((sPriorAmendDate == objE.EffectiveDate) || (Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE")) == objE.EffectiveDate) || (Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), m_iClientId) == 1) || sOtherType == "RL" || sOtherType == "RNL")

                                        bStop = true;

                                    dFullYearAmt = Conversion.ConvertObjToDouble(objReader.GetValue("FULL_ANN_PREM_AMT"), m_iClientId);
                                    if (sPriorAmendDate == "")
                                    {
                                        if (sSavePriorDate == "")
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(objE.AmendedDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                                            lTermLength = temp.Days;
                                        }
                                        else
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(sSavePriorDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                                            lTermLength = temp.Days;
                                        }
                                    }
                                    else
                                    {
                                        if (sSavePriorDate == "")
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(objE.AmendedDate)).Subtract(Conversion.ToDate(sPriorAmendDate)));
                                            lTermLength = temp.Days;
                                        }
                                        else
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(sSavePriorDate)).Subtract(Conversion.ToDate(sPriorAmendDate)));
                                            lTermLength = temp.Days;
                                        }
                                    }
                                    //Divya MITS 8699 changes
                                    //dFactor = Conversion.Round((lTermLength * 1.0) / 365, 3);
                                    dFactor = (lTermLength * 1.0) / 365;

                                    dOrigPrem = dFactor * dFullYearAmt;
                                    if (sOtherType == "RNL" || sOtherType == "RL")
                                        dOrigTotal = dOrigTotal + dOrigPrem + Conversion.ConvertObjToDouble(objReader.GetValue("PR_ANN_PREM_AMT"), m_iClientId) - Conversion.ConvertObjToDouble(objReader.GetValue("PREM_ADJ_AMT"), m_iClientId);
                                    else
                                        dOrigTotal = dOrigTotal + dOrigPrem;

                                    sSavePriorDate = sPriorAmendDate;
                                }
                                objReader.Close();
                                if (bRound == true)

                                    //TODO 
                                    dOrigTotal = Math.Round(dOrigTotal, 0, MidpointRounding.AwayFromZero);
                                else
                                    //TODO
                                    dOrigTotal = Math.Round(dOrigTotal, 2, MidpointRounding.AwayFromZero);

                                objE.PrAnnPremAmt = dOrigTotal;
                                objE.PremAdjAmt = dOrigTotal - dPrevPrPrem;
                            }
                    }
                    else //DOING EDIT/ADD NEW EXP
                    {
                        // npadhy MITS 16504 Commenting this piece of code, It is calculating only the Changed Premium
                        // The Code written below will calculate both the Changed Premium + existing premium
                        
                        //if (sFixed == "P") //is pro-rated
                        //{
                        //    if (objE.AmendedDate != "")
                        //    {
                        //        TimeSpan temp = ((Conversion.ToDate(objE.ExpirationDate)).Subtract(Conversion.ToDate(objE.AmendedDate)));
                        //        lTermLength = temp.Days + 1;
                        //    }
                        //    else
                        //    {
                        //        TimeSpan temp = ((Conversion.ToDate(objE.ExpirationDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                        //        lTermLength = temp.Days + 1;
                        //    }


                        //    if (lTermLength == 366)
                        //        lTermLength = 365;
                        //    //pmahli MITS 9770
                        //    //Previous Adjustment was showing adjustment w.r.t the previous Pro-Rata Annual Premium and not w.r.t. original Pro-Rata Annual Premium
                        //    //this was because dPrevPrPrem was coming changed every time Rate function is called as it was picking from DB
                        //    //sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID =" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode ;
                        //    sSQL = "SELECT PR_ANN_PREM_AMT FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " ORDER BY SEQUENCE_ALPHA DESC";
                        //    objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        //    if (objReader.Read())
                        //    {
                        //        dPrevPrPrem = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        //    }
                        //    objReader.Close();
                        //    //pmahli MITS 9770
                        //    //Divya MITS 8699 changes
                        //    //dFactor = Conversion.Round((lTermLength * 1.0) / 365, 3);
                        //    dFactor = (lTermLength * 1.0) / 365;
                        //    dAmendPrem = dFactor * dFullYearAmt;


                        //    if (objE.ExposureBaseRate != 0)
                        //    {
                        //        dAmendPrem = dAmendPrem * (objE.ExposureBaseRate / 100);
                        //        dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);
                        //    }
                        //// Start Naresh MITS 9770 Amend when Rate was configured to be Fixed
                        //}
                        //// End Naresh MITS 9770 Amend when Rate was configured to be Fixed

                        // npadhy MITS 16504 Calculation as per the correct logic
                        // The Code written below will calculate both the Changed Premium + existing premium
                        if (sFixed == "P") //is pro-rated
                        {
                            if (objE.AmendedDate != "")
                            {
                                TimeSpan temp = ((Conversion.ToDate(objE.ExpirationDate)).Subtract(Conversion.ToDate(objE.AmendedDate)));
                                lTermLength = temp.Days + 1;
                            }
                            else
                            {
                                TimeSpan temp = ((Conversion.ToDate(objE.ExpirationDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                                lTermLength = temp.Days + 1;
                            }

                            dPrevPrPrem = 0;
                            if (lTermLength == 366)
                                lTermLength = 365;
                            dFactor = (lTermLength * 1.0) / 365;
                            if (objE.ExposureBaseRate != 0)
                                dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);

                            if (objE.AmendedDate != objE.EffectiveDate)
                            {
                                //'find the last edition of this exposure...
                                sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " ORDER BY SEQUENCE_ALPHA DESC";
                                objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                                if (objReader.Read())
                                {
                                    dLastAmount = Conversion.ConvertObjToDouble(objReader.GetValue("EXPOSURE_AMOUNT"), m_iClientId);
                                    dPrevPrPrem = Conversion.ConvertObjToDouble(objReader.GetValue("PR_ANN_PREM_AMT"), m_iClientId);
                                }

                                objReader.Close();
                                dAmtDiff = objE.ExposureAmount - dLastAmount;
                                
                                // Using the Factor component to calculate the amend prem
                                dAmendPrem = dAmtDiff * dFactor;
                                if (sFlat == "P")
                                {
                                    dAmendPrem = dAmendPrem * (objE.ExposureRate / 100);
                                }
                                else
                                {
                                    dAmendPrem = dAmendPrem * objE.ExposureRate;
                                }
                                //pmahli MITS 10723 10/31/2007 - End 
                                if (objE.ExposureBaseRate != 0)
                                    dAmendPrem = dAmendPrem * (objE.ExposureBaseRate / 100);

                                dAmendPrem = dAmendPrem + dPrevPrPrem;
                            }
                            else
                            {
                                // Get the Premium for the Term
                                dAmendPrem = objE.ExposureAmount * dFactor;
                                if (sFlat == "P")
                                {
                                    dAmendPrem = dAmendPrem * (objE.ExposureRate / 100);
                                }
                                else
                                {
                                    dAmendPrem = dAmendPrem * objE.ExposureRate;
                                }
                                //Shruti for 10723 ends
                                if (objE.ExposureBaseRate != 0)
                                    dAmendPrem = dAmendPrem * (objE.ExposureBaseRate / 100);

                            }
                        }
                        else
                        {
                            //Previous Adjustment was showing adjustment w.r.t the previous Pro-Rata Annual Premium and not w.r.t. original Pro-Rata Annual Premium
                            //this was because dPrevPrPrem was coming changed every time Rate function is called as it was picking from DB
                            //So we are resetting it to Zero and Fetching the correct one using Query
                            dPrevPrPrem = 0;

                            if (objE.ExposureBaseRate != 0)
                                dFullYearAmt = dFullYearAmt * (objE.ExposureBaseRate / 100);

                            if (objE.AmendedDate != objE.EffectiveDate)
                            {
                                //'find the last edition of this exposure...
                                sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " ORDER BY SEQUENCE_ALPHA DESC";
                                objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                                if (objReader.Read())
                                {
                                    dLastAmount = Conversion.ConvertObjToDouble(objReader.GetValue("EXPOSURE_AMOUNT"), m_iClientId);
                                    dPrevPrPrem = Conversion.ConvertObjToDouble(objReader.GetValue("PR_ANN_PREM_AMT"), m_iClientId);
                                }

                                objReader.Close();
                                dAmtDiff = objE.ExposureAmount - dLastAmount;
                                //pmahli MITS 10723 10/31/2007 - Start 
                                //commented the existing code as premium calculation is wrong as exposure rate 
                                //is not being  divided by 100
                                //dAmendPrem = dAmtDiff * objE.ExposureRate;
                                if (sFlat == "P")
                                {
                                    dAmendPrem = dAmtDiff * (objE.ExposureRate / 100);
                                }
                                else
                                {
                                    dAmendPrem = dAmtDiff * objE.ExposureRate;
                                }
                                //pmahli MITS 10723 10/31/2007 - End 
                                if (objE.ExposureBaseRate != 0)
                                    dAmendPrem = dAmendPrem * (objE.ExposureBaseRate / 100);

                                dAmendPrem = dAmendPrem + dPrevPrPrem;
                            }
                            else
                            {
                                //Shruti for 10723 starts
                                if (sFlat == "P")
                                {
                                    dAmendPrem = objE.ExposureAmount * (objE.ExposureRate / 100);
                                }
                                else
                                {
                                    dAmendPrem = objE.ExposureAmount * objE.ExposureRate;
                                }
                                //Shruti for 10723 ends
                                if (objE.ExposureBaseRate != 0)
                                    dAmendPrem = dAmendPrem * (objE.ExposureBaseRate / 100);

                            }
                        }

                        if (bRound == true)//get rid of decimal places
                        {
                            dAmendPrem = Math.Round(dAmendPrem, 0, MidpointRounding.AwayFromZero);
                            dFullYearAmt = Math.Round(dFullYearAmt, 0, MidpointRounding.AwayFromZero);//todo
                        }
                        else //keep 2 decimal places
                        {
                            dAmendPrem = Math.Round(dAmendPrem, 2, MidpointRounding.AwayFromZero);
                            dFullYearAmt = Math.Round(dFullYearAmt, 2, MidpointRounding.AwayFromZero);
                        }

                        objE.FullAnnPremAmt = dFullYearAmt;
                        //pmahli MITS 9770
                        //Previous Adjustment was showing adjustment w.r.t the previous Pro-Rata Annual Premium and not w.r.t. original Pro-Rata Annual Premium
                        //this was because dPrevPrPrem was coming changed every time Rate function is called as it was picking from DB
                        //sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID =" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode ;
                        sSQL = "SELECT PR_ANN_PREM_AMT FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " ORDER BY SEQUENCE_ALPHA DESC";
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        if (objReader.Read())
                        {
                            dPrevPrPrem = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        }
                        objReader.Close();
                        //pmahli MITS 9770
                        //If we are adding it new or amending effective the term eff date...
                        if ((sFixed == "F") || (objE.AmendedDate == "") || (objE.AmendedDate == objE.EffectiveDate))
                        {
                            objE.PrAnnPremAmt = dAmendPrem;
                            if (objE.AmendedDate == "")
                                objE.PremAdjAmt = dAmendPrem;
                            else
                                objE.PremAdjAmt = dAmendPrem - dPrevPrPrem;
                        }
                        //otherwise means we are amending w/ another date....
                        else
                        {
                            //can assume only those exp that are pro-rated are left...
                            bStop = false;
                            sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " ORDER BY SEQUENCE_ALPHA DESC";
                            objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                            while ((objReader.Read() == true) && (bStop == false))
                            {
                                sPriorAmendDate = Conversion.ConvertObjToStr(objReader.GetValue("AMENDED_DATE"));
                                sOtherType = this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), m_iClientId));
                                if ((sPriorAmendDate == "") || (sPriorAmendDate == objE.EffectiveDate) || (Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), m_iClientId) == 1) || (sOtherType == "RL") || (sOtherType == "RNL"))
                                {
                                    bStop = true;
                                    dFullYearAmt = Conversion.ConvertObjToDouble(objReader.GetValue("FULL_ANN_PREM_AMT"), m_iClientId);
                                    dAmtPerDay = dFullYearAmt / 365;
                                    if (sPriorAmendDate == "")
                                    {
                                        if (sSavePriorDate == "")
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(objE.AmendedDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                                            lTermLength = temp.Days;
                                        }
                                        else
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(sSavePriorDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                                            lTermLength = temp.Days;
                                        }
                                    }
                                    else
                                    {
                                        if (sSavePriorDate == "")
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(objE.AmendedDate)).Subtract(Conversion.ToDate(sPriorAmendDate)));
                                            lTermLength = temp.Days;
                                        }
                                        else
                                        {
                                            TimeSpan temp = ((Conversion.ToDate(sSavePriorDate)).Subtract(Conversion.ToDate(sPriorAmendDate)));
                                            lTermLength = temp.Days;
                                        }
                                    }
                                }
                                //Divya MITS 8699 changes
                                //dFactor = Conversion.Round((lTermLength * 1.0) / 365, 3);
                                dFactor = (lTermLength * 1.0) / 365;
                                dOrigPrem = dFactor * dFullYearAmt;

                                if (objE.ExposureBaseRate != 0)
                                    //pmahli MITS 10723 10/31/2007 - Start
                                    //commented this code because the original premium was once again muliplied 
                                    //by the exposure base rate which is incorrect as the original preium is obtained
                                    //after multiplying it to exposure base rate
                                    //dOrigPrem = dOrigPrem * (objE.ExposureBaseRate / 100);
                                    //pmahli MITS 10723 10/31/2007 - End 

                                dOrigTotal = dOrigTotal + dOrigPrem;
                                sSavePriorDate = sPriorAmendDate;

                            }
                            objReader.Close();
                            if (bRound == true)
                                dOrigTotal = Math.Round(dOrigTotal, 0, MidpointRounding.AwayFromZero); //ToDO

                            else
                                dOrigTotal = Math.Round(dOrigTotal, 2, MidpointRounding.AwayFromZero);
                            //TODO

                            objE.PremAdjAmt = dAmendPrem - dPrevPrPrem;
                            objE.PrAnnPremAmt = dAmendPrem; 
                            //objE.PremAdjAmt = objE.PrAnnPremAmt - dPrevPrPrem;
                        }
                        // Start Naresh MITS 9770 Amend when Rate was configured to be Fixed
                        // A brace is removed which was added above
                    }
                }
                //CANCEL PRO-RATA
                if (sType == "CPR")
                {
                    if (sFixed == "F")
                    {
                        if (objE.ExpirationDate == objE.EffectiveDate)
                        {
                            objE.PrAnnPremAmt = 0;
                            objE.PremAdjAmt = 0 - dPrevPrPrem;
                            objE.FullAnnPremAmt = 0;
                        }
                        else
                        {
                            objE.PrAnnPremAmt = dPrevPrPrem;
                            objE.FullAnnPremAmt = dFullYearAmt;
                            objE.PremAdjAmt = 0;
                        }
                    }
                    else
                    {
                        bStop = false;
                        sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + objE.PolicyId + " AND TERM_NUMBER=" + objE.TermNumber;
                        sSQL = sSQL + " AND EXPOSURE_ID <>" + objE.ExposureId + " AND EXPOSURE_CODE=" + objE.ExposureCode + " AND EXPOSURE_COUNT=" + objE.ExposureCount + " ORDER BY SEQUENCE_ALPHA DESC";
                        objReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                        while ((objReader.Read() == true) && (bStop == false))
                        {
                            sPriorAmendDate = Conversion.ConvertObjToStr(objReader.GetValue("AMENDED_DATE"));
                            sOtherType = this.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), m_iClientId));
                            sOrigExpDate = Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"));
                            //Charanpreet 8/29/2008 :The While Loop is not necessary so setting the Stop variable to True so that it does not go into Loop
                            //if ((sPriorAmendDate == objE.EffectiveDate) || (Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE")) == objE.EffectiveDate) || (Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), m_iClientId) == 1) || sOtherType == "RL" || sOtherType == "RNL") Commenetd by Charanpreet RMSC code Retrofit
                                bStop = true;

                            dFullYearAmt = Conversion.ConvertObjToDouble(objReader.GetValue("FULL_ANN_PREM_AMT"), m_iClientId);
                            //dAmtPerDay = dFullYearAmt / 365
                            if (sPriorAmendDate == "")
                            {
                                if (sSavePriorDate == "")
                                {
                                    //TimeSpan temp = ((Conversion.ToDate(objE.AmendedDate)).Subtract(Conversion.ToDate(objE.EffectiveDate))); //Commented by Charanpreet : MITS 9329
                                    TimeSpan temp = ((Conversion.ToDate(sOrigExpDate)).Subtract(Conversion.ToDate(objE.ExpirationDate)));
                                    lTermLength = temp.Days;
                                    lTermLength = lTermLength + 1; //added by Charanpreet : MITS 9329
                                }

                                else
                                {
                                    TimeSpan temp = ((Conversion.ToDate(sSavePriorDate)).Subtract(Conversion.ToDate(objE.EffectiveDate)));
                                    lTermLength = temp.Days;
                                }
                            }
                            else
                            {
                                if (sSavePriorDate == "")
                                {
                                    //TimeSpan temp = ((Conversion.ToDate(objE.AmendedDate)).Subtract(Conversion.ToDate(objE.EffectiveDate))); Commented by Charanpreet MITS 9329
                                    TimeSpan temp = ((Conversion.ToDate(sOrigExpDate)).Subtract(Conversion.ToDate(objE.ExpirationDate)));
                                    lTermLength = temp.Days;
                                    lTermLength = lTermLength + 1; //added by Charanpreet :MITS 9329
                                }
                                else
                                {
                                    TimeSpan temp = ((Conversion.ToDate(sSavePriorDate)).Subtract(Conversion.ToDate(sPriorAmendDate)));
                                    lTermLength = temp.Days;
                                }
                            }
                            //Divya MITS 8699 changes
                            //dFactor = Conversion.Round((lTermLength * 1.0) / 365, 3);
                            dFactor = (lTermLength * 1.0) / 365;
                            dOrigPrem = -(dFactor * dFullYearAmt);                
                            //dOrigTotal = dOrigTotal + dOrigPrem;                   //  Commented by Charanpreet for MITs 9329 

                            dOrigTotal = objE.PrAnnPremAmt + dOrigPrem;
                            sSavePriorDate = sPriorAmendDate;
                        }
                        objReader.Close();
                        if (bRound == true)
                            dOrigTotal = Math.Round(dOrigTotal, 0, MidpointRounding.AwayFromZero);
                        else
                            dOrigTotal = Math.Round(dOrigTotal, 2, MidpointRounding.AwayFromZero);

                        objE.PrAnnPremAmt = dOrigTotal;
                        objE.PremAdjAmt = dOrigTotal - dPrevPrPrem;
                    }
                }
                //CANCEL FLAT
                if (sType == "CF")
                {
                    dPrem = 0;
                    objE.PrAnnPremAmt = dPrem;
                    objE.PremAdjAmt = dPrem - dPrevPrPrem;
                }

            }

        }
        #endregion        

        #region PostToBilling
        internal void PostToBilling(Billing.BillingMaster p_objBillingMaster, int p_iTransNum, string p_sType, PolicyEnh p_objPolicyEnh)
        {
            BillXAccount objBillingXAccount = null;
            Billing.PayPlanManger objPayPlanManger = null;
            Billing.DiaryManager objDiaryManger = null;
            Billing.BillingManager objBillingManager = null;

            string sSQL = string.Empty;
            int iTermNumber = 0;
            int iPolicyId = 0;
            Double dAmount = 0;
            bool bFound = false;
            Double dAmountAlreadyCharged = 0;
            DbReader objReader = null;
            //npadhy RMSC retrofit Starts
            double dTaxRate=0;
            int iTaxType=0;
            double dTax = 0;
            string sEff=string.Empty;  
            string sExp = string.Empty;
            double dPremTax=0;
            double dAmtDue = 0;
            //npadhy RMSC retrofit Ends

            try
            {

                objPayPlanManger = new Billing.PayPlanManger(DataModelFactory,ClientId);
                objDiaryManger = new Billing.DiaryManager(DataModelFactory,ClientId);
                objBillingManager = new Billing.BillingManager(DataModelFactory,ClientId);

                switch (p_sType)
                {
                    case "NB":
                    case "RN":
                        //Create billing account record
                        objBillingXAccount = p_objPolicyEnh.BillXAccountList.AddNew();

                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXBillEnh objPolicyXBillEnh in objPolicyEnh.PolicyXBillEnhList)
                                {
                                    if (objPolicyXBillEnh.TransactionId == p_iTransNum)
                                    {
                                        objBillingXAccount.BillingRuleRowid = objPolicyXBillEnh.BillingRuleRowId;
                                        objBillingXAccount.PayPlanRowid = objPolicyXBillEnh.PayPlanRowId;
                                        objBillingXAccount.PolicyId = objPolicyEnh.PolicyId;
                                    }
                                }
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        objBillingXAccount.AmountDue = objPolicyXTransEnh.TotalBilledPrem;
                                        //npadhy RMSC retrofit Starts
                                        objBillingXAccount.TaxAmount = objPolicyXTransEnh.TranTax;
                                        objBillingXAccount.TaxRate = objPolicyXTransEnh.TaxRate;
                                        objBillingXAccount.TaxType = objPolicyXTransEnh.TaxType;    
                                        //npadhy RMSC retrofit Ends
                                    }
                                }

                                foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                                {
                                    if (objPolicyXTermEnh.TransactionId == p_iTransNum)
                                    {
                                        objBillingXAccount.TermNumber = objPolicyXTermEnh.TermNumber;
                                        objBillingXAccount.PolicyNumber = objPolicyXTermEnh.PolicyNumber;
                                        //npadhy RMSC retrofit Starts
                                        sEff = objPolicyXTermEnh.EffectiveDate;
                                        sExp = objPolicyXTermEnh.ExpirationDate;   
                                        //npadhy RMSC retrofit Ends
                                    }
                                }
                                //npadhy RMSC retrofit Starts
                                //Determine the amount to be charged/refunded
                                sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")"
                                     + " AND EFFECTIVE_DATE <= '" + sEff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + sEff + "') AND IN_USE_FLAG = 1";
                                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                                if (objReader.Read())
                                {
                                    p_objBillingMaster.Account.UseTax = true;
                                }
                                else
                                {
                                    p_objBillingMaster.Account.UseTax = false;   
                                }
                                objReader.Close();
                                //npadhy RMSC retrofit Ends
                            }
                        }

                        p_objBillingMaster.Account = objBillingXAccount;

                        //If a pay plan is selected, create pay plan and installments                   
                        if (p_objBillingMaster.Account.PayPlanRowid != 0)
                        {
                            objPayPlanManger.CreatePayPlanForPolicy(p_objBillingMaster, p_iTransNum, p_sType);
                            bFound = false;
                            foreach (int objKey in p_objBillingMaster.Diaries.Keys)
                            {
                                WpaDiaryEntry objWpaDiaryEntry = (WpaDiaryEntry)p_objBillingMaster.Diaries[objKey];
                                foreach (WpaDiaryAct objWpaDiaryAct in objWpaDiaryEntry.WpaDiaryActList)
                                {
                                    if (LocalCache.GetShortCode(objWpaDiaryAct.ActCode) == "PI")
                                    {
                                        bFound = true;
                                    }
                                }
                            }

                            if (bFound == false)
                                objDiaryManger.ScheduleInvoiceDiaries(p_objBillingMaster);
                        }

                        else
                        {
                            //Create prem item, schedule invoice gen and cancellation diaries
                            objBillingManager.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNum, p_sType);
                            bFound = false;
                            foreach (int objKey in p_objBillingMaster.Diaries.Keys)
                            {
                                WpaDiaryEntry objWpaDiaryEntry = (WpaDiaryEntry)p_objBillingMaster.Diaries[objKey];
                                foreach (WpaDiaryAct objWpaDiaryAct in objWpaDiaryEntry.WpaDiaryActList)
                                {
                                    if (LocalCache.GetShortCode(objWpaDiaryAct.ActCode) == "PI")
                                    {
                                        bFound = true;
                                    }
                                }
                            }
                            if (bFound == false)
                                objDiaryManger.ScheduleInvoiceDiaries(p_objBillingMaster);

                        }
                        objBillingXAccount = null;
                        break;

                    case "EN":
                        objBillingXAccount = p_objPolicyEnh.BillXAccountList.AddNew();
                         //npadhy RMSC retrofit Starts
                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        iTermNumber = objPolicyXTransEnh.TermNumber;
                                        break;
                                    }
                                }
                            }
                        }
                        //npadhy RMSC retrofit Ends
                        sSQL = "SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID =" + p_objPolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber;
                        objBillingManager.FillAccount(p_objBillingMaster.Account, sSQL);
                        //p_objBillingMaster.Account = objBillingXAccount;

                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        p_objBillingMaster.Account.AmountDue = objPolicyXTransEnh.TotalBilledPrem;
                                        //npadhy RMSC retrofit Starts
                                        p_objBillingMaster.Account.TaxAmount = objPolicyXTransEnh.TaxAmount;
                                        p_objBillingMaster.Account.TaxRate = objPolicyXTransEnh.TaxRate;
                                        p_objBillingMaster.Account.TaxType = objPolicyXTransEnh.TaxType;
                                        //npadhy RMSC retrofit Ends

                                    }
                                }
                                //npadhy RMSC retrofit Starts
                                foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                                {
                                    if (objPolicyXTermEnh.TermNumber  == p_objBillingMaster.Account.TermNumber)
                                    {
                                        sEff = objPolicyXTermEnh .EffectiveDate;
                                        sExp = objPolicyXTermEnh.ExpirationDate; 
                                    }
                                }
                                
                                sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")"
                                     + " AND EFFECTIVE_DATE <= '" + sEff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + sEff + "') AND IN_USE_FLAG = 1";
                                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                                if (objReader.Read())
                                {
                                    p_objBillingMaster.Account.UseTax = true;
                                }
                                else
                                {
                                    p_objBillingMaster.Account.UseTax = false;
                                }
                                objReader.Close();
                                //npadhy RMSC retrofit Ends
                            }
                        }

                        if (p_objBillingMaster.Account.PayPlanRowid != 0)
                        {
                            //Adjust payplan amounts
                            objBillingManager.AdjustInstallments(p_objBillingMaster, p_iTransNum, p_sType);
                        }
                        else
                        {
                            //Bill according to bill rule
                            objBillingManager.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNum, p_sType);
                            bFound = false;

                            foreach (int objKey in p_objBillingMaster.Diaries.Keys)
                            {
                                WpaDiaryEntry objWpaDiaryEntry = (WpaDiaryEntry)p_objBillingMaster.Diaries[objKey];
                                foreach (WpaDiaryAct objWpaDiaryAct in objWpaDiaryEntry.WpaDiaryActList)
                                {
                                    if (LocalCache.GetShortCode(objWpaDiaryAct.ActCode) == "PI")
                                    {
                                        bFound = true;
                                    }
                                }
                            }

                            if (bFound == false)
                                objDiaryManger.ScheduleInvoiceDiaries(p_objBillingMaster);
                        }

                        objBillingXAccount = null;
                        break;

                    case "AU":
                        objBillingXAccount = p_objPolicyEnh.BillXAccountList.AddNew();
                        //npadhy RMSC retrofit Starts
                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        iTermNumber = objPolicyXTransEnh.TermNumber;
                                        break;
                                    }
                                }
                            }
                        }
                        //npadhy RMSC retrofit Ends
                        sSQL = "SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID =" + p_objPolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber;
                        objBillingManager.FillAccount(p_objBillingMaster.Account, sSQL);
                        //p_objBillingMaster.Account = objBillingXAccount;

                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        p_objBillingMaster.Account.AmountDue = objPolicyXTransEnh.TotalBilledPrem;
                                         //npadhy RMSC retrofit Starts
                                        p_objBillingMaster.Account.TaxAmount = objPolicyXTransEnh.TranTax;
                                        p_objBillingMaster.Account.TaxType = objPolicyXTransEnh.TaxType;
                                        p_objBillingMaster.Account.TaxRate = objPolicyXTransEnh.TaxRate;
                                        break; 
                                        //npadhy RMSC retrofit Ends
                                    }
                                }

                                //npadhy RMSC retrofit Starts
                                foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                                {
                                    if (objPolicyXTermEnh.TermNumber == p_objBillingMaster.Account.TermNumber)
                                    {
                                        sEff = objPolicyXTermEnh.EffectiveDate;
                                        sExp = objPolicyXTermEnh.ExpirationDate;
                                    }
                                }

                                sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")"
                                     + " AND EFFECTIVE_DATE <= '" + sEff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + sEff + "') AND IN_USE_FLAG = 1";
                                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                                if (objReader.Read())
                                {
                                    p_objBillingMaster.Account.UseTax = true;
                                }
                                else
                                {
                                    p_objBillingMaster.Account.UseTax = false;
                                }
                                objReader.Close();
                                //npadhy RMSC retrofit Ends
                                
                            }
                        }

                        //Bill according to bill rule
                        objBillingManager.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNum, p_sType);
                        bFound = false;

                        foreach (int objKey in p_objBillingMaster.Diaries.Keys)
                        {
                            WpaDiaryEntry objWpaDiaryEntry = (WpaDiaryEntry)p_objBillingMaster.Diaries[objKey];
                            foreach (WpaDiaryAct objWpaDiaryAct in objWpaDiaryEntry.WpaDiaryActList)
                            {
                                if (LocalCache.GetShortCode(objWpaDiaryAct.ActCode) == "PI")
                                {
                                    bFound = true;
                                }
                            }
                        }

                        if (bFound == false)
                            objDiaryManger.ScheduleInvoiceDiaries(p_objBillingMaster);

                        objBillingXAccount = null;
                        break;

                    case "CF":
                    case "CPR":
                        objBillingXAccount = p_objPolicyEnh.BillXAccountList.AddNew();
                        //npadhy RMSC retrofit Starts
                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        iTermNumber = objPolicyXTransEnh.TermNumber;
                                        break;
                                    }
                                }
                            }
                        }
                         //npadhy RMSC retrofit Ends

                        sSQL = "SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID =" + p_objPolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber; //Animesh Modfied RMSC 
                        objBillingManager.FillAccount(p_objBillingMaster.Account, sSQL);
                        //p_objBillingMaster.Account = objBillingXAccount;

                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        p_objBillingMaster.Account.AmountDue = objPolicyXTransEnh.TotalBilledPrem;
                                        iTermNumber = objPolicyXTransEnh.TermNumber;
                                        iPolicyId = objPolicyXTransEnh.PolicyId;
                                        //npadhy RMSC retrofit Starts
                                        dPremTax = objPolicyXTransEnh.TaxAmount;   
                                        //npadhy RMSC retrofit Ends
                                    }
                                }
                                //npadhy RMSC retrofit Starts
                                foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                                {
                                    if (objPolicyXTermEnh.TermNumber == p_objBillingMaster.Account.TermNumber)
                                    {
                                        sEff = objPolicyXTermEnh.EffectiveDate;
                                        sExp = objPolicyXTermEnh.ExpirationDate;
                                    }
                                }

                                sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")"
                                     + " AND EFFECTIVE_DATE <= '" + sEff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + sEff + "') AND IN_USE_FLAG = 1";
                                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                                if (objReader.Read())
                                {
                                    p_objBillingMaster.Account.UseTax = true;
                                }
                                else
                                {
                                    p_objBillingMaster.Account.UseTax = false;
                                }
                                objReader.Close();
                                //npadhy RMSC retrofit Ends
                            }
                        }

                        //Delete any installment diaries
                        if (p_objBillingMaster.Account.PayPlanRowid != 0)
                        {
                            objDiaryManger.DeleteAllInstallmentDiaries(p_objPolicyEnh.PolicyId, iTermNumber, p_objBillingMaster);
                        }

                        //Delete any cancellation diaries
                        objDiaryManger.DeleteAllCancelDiaries(p_objPolicyEnh.PolicyId, p_objBillingMaster);
                        objDiaryManger.DeleteAllCancelPendingDiaries(p_objPolicyEnh.PolicyId, p_objBillingMaster);
                        objDiaryManger.DeleteNoticeDiaries(p_objPolicyEnh.PolicyId, 1, p_objBillingMaster);

                        //Determine the amount to be charged/refunded
                        sSQL = "SELECT SUM(AMOUNT) FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + p_objPolicyEnh.PolicyId + " AND TERM_NUMBER=" + iTermNumber + " AND BILLING_ITEM_TYPE =" + CCacheFunctions.GetCodeIDWithShort("P", "BILLING_ITEM_TYPES");
                        objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                        if (objReader.Read())
                        {
                            dAmountAlreadyCharged = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        }
                        objReader.Close();

                        //npadhy RMSC retrofit Starts
                        sSQL = "SELECT SUM(AMOUNT) FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + p_objPolicyEnh.PolicyId + " AND TERM_NUMBER=" + iTermNumber + " AND BILLING_ITEM_TYPE =" + CCacheFunctions.GetCodeIDWithShort("TAX", "BILLING_ITEM_TYPES");
                        objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                        if (objReader.Read())
                        {
                            dTax = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        }
                        objReader.Close();

                        //changed by Gagan for MITS 19160 : start
                        //Commenting as this code is redundant
                        //if (p_objBillingMaster.Account.UseTax)
                        //{
                        //    if (LocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                        //    {
                        //        dAmtDue = p_objBillingMaster.Account.AmountDue - dPremTax;
                        //    }
                        //    else
                        //    {
                        //        dAmtDue = p_objBillingMaster.Account.AmountDue;    
                        //    }
                        //}
                        //npadhy RMSC retrofit Ends
                        //changed by Gagan for MITS 19160 : end

                        //objBillingManager.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNum, p_sType, p_objBillingMaster.Account.AmountDue - dAmountAlreadyCharged); //Animesh Modified RMSC
                        //objBillingManager.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNum, p_sType, dAmtDue - dAmountAlreadyCharged);
                        double dPremiumAmount = 0.0;

                        //changed by Gagan for MITS 19160 : start
                        //to find just premium amount and send it to CreateChargeItemPolicy
                        if (p_objBillingMaster.Account.UseTax)
                        {
                            if (LocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                            {
                                //Changed by Gagan for MITS 22384 : start
                                dPremiumAmount = p_objBillingMaster.Account.AmountDue - (dAmountAlreadyCharged + dTax); //becomes premium amount + tax on premium
                                dPremiumAmount = (dPremiumAmount)  / (1 + (0.01 * p_objBillingMaster.Account.TaxRate));
                                //Changed by Gagan for MITS 22384 : end
                            }
                            else
                            {
                                //Changed by Gagan for MITS 22384 : start
                                dPremiumAmount = p_objBillingMaster.Account.AmountDue - (dAmountAlreadyCharged + dTax); //becomes premium amount + tax on premium
                                //Changed by Gagan for MITS 22384 : end
                            }
                        }
                        else
                        {
                            //changed by Gagan for MITS 19160 : start
                            dPremiumAmount = p_objBillingMaster.Account.AmountDue - dAmountAlreadyCharged;
                            //changed by Gagan for MITS 19160 : end                        
                        }
              
                        //Send only premium amount to CreateChargeItemPolicy
                        objBillingManager.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNum, p_sType, dPremiumAmount);
                        //changed by Gagan for MITS 19160 : end

                        //Check for existing invoice diary.If not found, create one
                        bFound = false;

                        foreach (int objKey in p_objBillingMaster.Diaries.Keys)
                        {
                            WpaDiaryEntry objWpaDiaryEntry = (WpaDiaryEntry)p_objBillingMaster.Diaries[objKey];
                            foreach (WpaDiaryAct objWpaDiaryAct in objWpaDiaryEntry.WpaDiaryActList)
                            {
                                if (LocalCache.GetShortCode(objWpaDiaryAct.ActCode) == "PI")
                                {
                                    bFound = true;
                                }
                            }
                        }

                        if (bFound == false)
                            objDiaryManger.ScheduleInvoiceDiaries(p_objBillingMaster);

                        objBillingXAccount = null;
                        break;

                    case "RL":
                    case "RNL":
                        objBillingXAccount = p_objPolicyEnh.BillXAccountList.AddNew();
                        //npadhy RMSC retrofit Starts
                        //sSQL = "SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID =" + p_objPolicyEnh.PolicyId;
                        sSQL = "SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID =" + p_objPolicyEnh.PolicyId + " AND TERM_NUMBER = (SELECT MAX(TERM_NUMBER) FROM BILL_X_ACCOUNT WHERE POLICY_ID = " + p_objPolicyEnh.PolicyId + ")";
						//npadhy RMSC retrofit Ends
                        objBillingManager.FillAccount(p_objBillingMaster.Account, sSQL);
                        //p_objBillingMaster.Account = objBillingXAccount;

                        foreach (int objKey in p_objBillingMaster.Policies.Keys)
                        {
                            PolicyEnh objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objKey];
                            if (LocalCache.GetShortCode(objPolicyEnh.PolicyIndicator) == "P")
                            {
                                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                                {
                                    if (objPolicyXTransEnh.TransactionId == p_iTransNum)
                                    {
                                        p_objBillingMaster.Account.AmountDue = objPolicyXTransEnh.TotalBilledPrem;
                                        iTermNumber = objPolicyXTransEnh.TermNumber;
                                        iPolicyId = objPolicyXTransEnh.PolicyId;
                                        dAmount = objPolicyXTransEnh.ChgBilledPremium;
                                        //npadhy RMSC retrofit Starts
                                        p_objBillingMaster.Account.TaxAmount = objPolicyXTransEnh.TranTax;
                                        break; 
                                        //npadhy RMSC retrofit Ends
                                    }
                                }
                                //npadhy RMSC retrofit Starts
                                foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                                {
                                    if (objPolicyXTermEnh.TermNumber == p_objBillingMaster.Account.TermNumber)
                                    {
                                        sEff = objPolicyXTermEnh.EffectiveDate;
                                        sExp = objPolicyXTermEnh.ExpirationDate;
                                    }
                                }

                                sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")"
                                     + " AND EFFECTIVE_DATE <= '" + sEff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + sEff + "') AND IN_USE_FLAG = 1";
                                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                                if (objReader.Read())
                                {
                                    p_objBillingMaster.Account.UseTax = true;
                                }
                                else
                                {
                                    p_objBillingMaster.Account.UseTax = false;
                                }
                                objReader.Close();
                                //npadhy RMSC retrofit Ends
                            }
                        }

                        //Delete any installment diaries
                        if (p_objBillingMaster.Account.PayPlanRowid == 0)
                        {
                            objBillingManager.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNum, p_sType, dAmount);
                            foreach (int objKey in p_objBillingMaster.Diaries.Keys)
                            {
                                WpaDiaryEntry objWpaDiaryEntry = (WpaDiaryEntry)p_objBillingMaster.Diaries[objKey];
                                foreach (WpaDiaryAct objWpaDiaryAct in objWpaDiaryEntry.WpaDiaryActList)
                                {
                                    if (LocalCache.GetShortCode(objWpaDiaryAct.ActCode) == "PI")
                                    {
                                        bFound = true;
                                    }
                                }
                            }

                            if (bFound == false)
                                objDiaryManger.ScheduleInvoiceDiaries(p_objBillingMaster);
                        }
                        else
                        {
                            objPayPlanManger.ReactivatePayPlan(p_objBillingMaster, p_iTransNum, p_sType, dAmount);
                            foreach (int objKey in p_objBillingMaster.Diaries.Keys)
                            {
                                WpaDiaryEntry objWpaDiaryEntry = (WpaDiaryEntry)p_objBillingMaster.Diaries[objKey];
                                foreach (WpaDiaryAct objWpaDiaryAct in objWpaDiaryEntry.WpaDiaryActList)
                                {
                                    if (LocalCache.GetShortCode(objWpaDiaryAct.ActCode) == "PI")
                                    {
                                        bFound = true;
                                    }
                                }
                            }

                            if (bFound == false)
                                objDiaryManger.ScheduleInvoiceDiaries(p_objBillingMaster);
                        }

                        objBillingXAccount = null;
                        break;
                }

            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", ClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();

                objBillingXAccount = null;
                objPayPlanManger = null;
                objDiaryManger = null;
                objBillingManager = null;
            }
        }
        #endregion        

        #region Common XML functions
        /// <summary>
        /// Initialize the XML document
        /// </summary>
        /// <param name="objXmlDocument">XML document</param>
        /// <param name="p_objRootNode">Root Node</param>
        /// <param name="p_objMessagesNode">Messages Node</param>
        /// <param name="p_sRootNodeName">Root Node Name</param>
		internal void StartDocument( ref XmlDocument objXmlDocument , ref XmlElement p_objRootNode, string p_sRootNodeName )
        {
            try
            {
                objXmlDocument = new XmlDocument();
				p_objRootNode = objXmlDocument.CreateElement( p_sRootNodeName );
				objXmlDocument.AppendChild( p_objRootNode );				
            }
			catch( RMAppException p_objEx )
            {
				throw p_objEx ;
            }
			catch( Exception p_objEx )
            {
                throw new RMAppException(Globalization.GetString("TODO", ClientId), p_objEx);				
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Node Name</param>		
		internal void CreateElement( XmlElement p_objParentNode , string p_sNodeName )
        {
			XmlElement objChildNode = null ;
            try
            {
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
            }
			catch( Exception p_objEx )
            {
                throw new RMAppException(Globalization.GetString("TODO", ClientId), p_objEx);
            }
            finally
            {
				objChildNode = null ;
            }

        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
		internal void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode )
        {
            try
            {
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
            }
			catch( Exception p_objEx )
            {
                throw new RMAppException(Globalization.GetString("TODO", ClientId), p_objEx);
            }

        }

        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>		
		internal void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText )
        {
            try
            {
				XmlElement objChildNode = null ;
				this.CreateAndSetElement( p_objParentNode , p_sNodeName , p_sText , ref objChildNode );
            }
			catch( RMAppException p_objEx )
            {
				throw p_objEx ;
            }
			catch( Exception p_objEx )
            {
                throw new RMAppException(Globalization.GetString("TODO", ClientId), p_objEx);
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
		internal void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode )
        {
            try
            {
				CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode );
				p_objChildNode.InnerText = p_sText ;			
            }
			catch( RMAppException p_objEx )
            {
				throw p_objEx ;
            }
			catch( Exception p_objEx )
            {
                throw new RMAppException(Globalization.GetString("TODO", ClientId), p_objEx);
            }
        }

        public void ResetSysExData(XmlDocument SysEx, string sNode, string sValue)
        {
            try { SysEx.DocumentElement.RemoveChild(SysEx.SelectSingleNode("/SysExData/" + sNode)); }
            catch { };
            CreateSysExData(SysEx, sNode, sValue);
        }

        public void CreateSysExData(XmlDocument SysEx, string sNode, string sValue)
        {
            XmlNode objNew;
            //Determine if the specific Node already exists in the SysExData of the Instance Document
            if (SysEx.SelectSingleNode("/SysExData/" + sNode) == null)
            {
                //Create a new XML Element for the specified Xml Node
                objNew = SysEx.CreateElement(sNode);

                //Set the element inner text to an empty string
                objNew.AppendChild(SysEx.CreateCDataSection(sValue));
                //objNew.InnerText = FormDataAdaptor.CData();

                //Add the new XML Element to the existing Instance Data
                SysEx.DocumentElement.AppendChild(objNew);
            }
        }

        public void SortXml(ref XmlElement p_SortXml, SortedList p_objPositiveEntries, SortedList p_objNegativeEntries)
        {


            for (int i = 0; i < p_objPositiveEntries.Count; i++)
            {
                p_SortXml.AppendChild((XmlNode)p_objPositiveEntries.GetByIndex(i));
            }

            for (int i = p_objNegativeEntries.Count - 1; i >= 0; i--)
            {
                p_SortXml.AppendChild((XmlNode)p_objNegativeEntries.GetByIndex(i));
            }

        }

        
        #endregion

        //Anu Tennyson for MITS 18229 STARTS 12/23/2009
        #region to find the sublevel of the selected Org level.
        /// <summary>
        /// Function for finding all the childs for a selected org hierarchy.
        /// </summary>
        /// <param name="sOrgHierarchy">String containing all the selected entity.</param>
        /// <returns>The XML containing the childs of the selected entity.</returns>
        public string OrgHierarchy(string sOrgHierarchy)
        {
            int iDeptTableId = 1012;
            bool bIsSuccess = false;
            string sParentId = string.Empty;
            int iTableId = 0;
            int iEntityid = 0;
            string m_sSystemDate = string.Empty;
            StringBuilder sOrgFilter = new StringBuilder();
            StringBuilder sSql = new StringBuilder();
            string sChildOrg = string.Empty;
            StringBuilder ssSql = new StringBuilder();
            try
            {

                string[] sOrg = sOrgHierarchy.Split(new char[] { ',' });

                m_sSystemDate = Conversion.GetDate(DateTime.Now.ToShortDateString());

                for (int sOrgCount = 0; sOrgCount < sOrg.Length; sOrgCount++)
                {
                    if (sOrg[sOrgCount] != "" && sOrg[sOrgCount] != "0")
                    {
                        iEntityid = Conversion.CastToType<int>(sOrg[sOrgCount], out bIsSuccess);
                        iTableId = this.LocalCache.GetOrgTableId(iEntityid);
                        sOrgFilter.Append(iEntityid);
                        sOrgFilter.Append(",");
                        sParentId = Convert.ToString(sOrgFilter).TrimEnd(',');
                        iTableId = iTableId + 1;

                        while (iTableId <= iDeptTableId)
                        {
                            if (!(string.IsNullOrEmpty(sParentId)))
                            {
                                sSql = new StringBuilder();
                                sSql.Append("SELECT ENTITY_ID FROM ENTITY WHERE ");
                                sSql.Append("PARENT_EID IN (");
                                sSql.Append(sParentId);
                                sSql.Append(")");
                                sSql.Append(" AND ENTITY_TABLE_ID = ");
                                sSql.Append(iTableId);
                                sSql.Append(" AND ENTITY.DELETED_FLAG<>-1 ");
                                sSql.Append("AND ((TRIGGER_DATE_FIELD IS NULL OR TRIGGER_DATE_FIELD = 'NULL')  ");
                                sSql.Append("OR (TRIGGER_DATE_FIELD='SYSTEM_DATE' ");
                                sSql.Append("AND EFF_START_DATE IS NULL AND EFF_END_DATE IS NULL)  ");
                                sSql.Append("OR (TRIGGER_DATE_FIELD='SYSTEM_DATE' AND EFF_START_DATE<= ");
                                sSql.Append(m_sSystemDate);
                                sSql.Append(" AND EFF_END_DATE IS NULL)  OR (TRIGGER_DATE_FIELD='SYSTEM_DATE' ");
                                sSql.Append("AND EFF_START_DATE IS NULL AND EFF_END_DATE>= ");
                                sSql.Append(m_sSystemDate);
                                sSql.Append(")");
                                sSql.Append("OR (TRIGGER_DATE_FIELD='SYSTEM_DATE' AND EFF_START_DATE<= ");
                                sSql.Append(m_sSystemDate);
                                sSql.Append(" AND EFF_END_DATE>= ");
                                sSql.Append(m_sSystemDate);
                                sSql.Append(")");
                                sSql.Append(" OR TRIGGER_DATE_FIELD NOT IN ('SYSTEM_DATE'))");

                                using (DbReader objDBReader = DbFactory.GetDbReader(this.ConnectionString, Convert.ToString(sSql)))
                                {
                                    while (objDBReader.Read())
                                    {
                                        iEntityid = Conversion.CastToType<int>(Convert.ToString(objDBReader["ENTITY_ID"]), out bIsSuccess);
                                        ssSql.Append(iEntityid);
                                        ssSql.Append(",");
                                        sOrgFilter.Append(iEntityid);
                                        sOrgFilter.Append(",");
                                    }
                                }
                            }
                            sParentId = Convert.ToString(ssSql).TrimEnd(',');
                            iTableId = iTableId + 1;
                        }

                    }
                }

                if (String.IsNullOrEmpty(Convert.ToString(sOrgFilter)))
                {
                    sChildOrg = "0";
                }
                else
                {
                    sChildOrg = "0," + Convert.ToString(sOrgFilter).TrimEnd(',');
                }

            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.Org Hierarchy.Error", ClientId), objEx);
            }
            finally
            {
                sSql = null;
                sOrgFilter = null;
                sSql = null;
                ssSql = null;
            }
            return sChildOrg;
        }

        #endregion
        //Anu Tennyson for MITS 18229 ENDS

    }
}
