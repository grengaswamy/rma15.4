﻿
using System;
using System.Xml;
using System.Collections ;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.EnhancePolicy
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
    // Naresh Connection Leak
    public class PrintManager : Common,IDisposable
	{  	
		
		#region Constructors
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public PrintManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId) : base(p_sDsnName, p_sUserName, p_sPassword, p_iClientId)
		{						
		}

        // Naresh Connection Leak
        public void Dispose()
        {
            if (base.LocalCache != null)
            {
                base.LocalCache.Dispose();
            }
            if (base.DataModelFactory != null)
            {
                base.DataModelFactory.Dispose();
            }
        }
		public XmlDocument GetPolicies(int lPolicyID)
		{
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			XmlElement objPolicyNode = null ;
			DbReader objReader = null ;
			string sSQL = string.Empty ; 
			
			
			try
			{
				//sSQL =  "SELECT * FROM POLICY_X_FORMS_ENH, AD_FORMS WHERE AD_FORMS.FORM_ID = POLICY_X_FORMS_ENH.FORM_ID AND POLICY_ID = " + lPolicyID;
                //MGaba2:MITS 15380: Alphabetical order of forms
                //sSQL = "SELECT AD.FORM_ID,AD.FORM_TITLE FROM POLICY_X_FORMS_ENH POL, AD_FORMS AD WHERE AD.FORM_ID = POL.FORM_ID AND POL.POLICY_ID = " + lPolicyID;
                sSQL = "SELECT AD.FORM_ID,AD.FORM_TITLE FROM POLICY_X_FORMS_ENH POL, AD_FORMS AD WHERE AD.FORM_ID = POL.FORM_ID AND POL.POLICY_ID = " + lPolicyID + " ORDER BY AD.FORM_TITLE";
				objReader = DbFactory.GetDbReader( base.ConnectionString , sSQL  );

                base.StartDocument(ref objXmlDocument, ref objRootNode, "SelectedFormNames");
				while( objReader.Read() )
				{
					base.CreateElement( objRootNode , "FormName" , ref objPolicyNode );
                    base.CreateAndSetElement(objPolicyNode, "formid", Conversion.ConvertObjToInt(objReader.GetValue("FORM_ID"), base.ClientId).ToString());
                    base.CreateAndSetElement(objPolicyNode, "formtitle", Conversion.ConvertObjToStr(objReader.GetValue("FORM_TITLE")).ToString());
                    base.CreateAndSetElement(objPolicyNode, "polId", lPolicyID.ToString());
				}

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EnhancePolicy.GetPolicies.Error", base.ClientId), p_objEx);				
			}
			finally
			{
                if (objReader != null)
                    if (!objReader.IsClosed) 
                        objReader.Close();
			}
			return(objXmlDocument);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lPolicyID">policy id</param>
        /// <param name="sAddedFormIds">id of forms from add form</param>
        /// <returns>id and names of all forms to be displayed on print policy page</returns>
        public XmlDocument GetPolicies(int lPolicyID, string sAddedFormIds)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objRootNode = null;
            XmlElement objPolicyNode = null;
            DbReader objReader = null;
            string sSQL = string.Empty; 
            try
            {
                sSQL = "(SELECT POL.POLICY_ID,AD.FORM_ID,AD.FORM_TITLE FROM POLICY_X_FORMS_ENH POL,AD_FORMS AD WHERE POL.POLICY_ID = " + lPolicyID + " AND POL.FORM_ID = AD.FORM_ID)";
                sSQL = sSQL + " UNION ";
                sSQL = sSQL + "(SELECT -1,FORM_ID,FORM_TITLE FROM AD_FORMS WHERE FORM_ID IN (" + sAddedFormIds + ") AND FORM_ID NOT IN (SELECT FORM_ID FROM POLICY_X_FORMS_ENH WHERE POLICY_ID = " + lPolicyID + "))";
                sSQL = sSQL + " ORDER BY FORM_TITLE"; //MGaba2:MITS 15380: Alphabetical order of forms
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                base.StartDocument(ref objXmlDocument, ref objRootNode, "SelectedFormNames");
                
                while (objReader.Read())
                {
                    base.CreateElement(objRootNode, "FormName", ref objPolicyNode);
                    base.CreateAndSetElement(objPolicyNode, "formid", Conversion.ConvertObjToInt(objReader.GetValue("FORM_ID"), base.ClientId).ToString());
                    base.CreateAndSetElement(objPolicyNode, "formtitle", Conversion.ConvertObjToStr(objReader.GetValue("FORM_TITLE")).ToString());
                    base.CreateAndSetElement(objPolicyNode, "polId", Conversion.ConvertObjToStr(objReader.GetValue("POLICY_ID")).ToString());
                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.GetPolicies.Error", base.ClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                    if (!objReader.IsClosed)
                        objReader.Close();
            }
            return (objXmlDocument);
        }
		public XmlDocument LoadAddForms()
		{
			 XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			XmlElement objPolicyNode = null ;
			DbReader objReader = null ;
			string sSQL = string.Empty ; 
			
			
			try
			{
                //MGaba2:MITS 15380: Alphabetical order of forms
                //sSQL = "SELECT * FROM AD_FORMS";
                sSQL = "SELECT * FROM AD_FORMS ORDER BY FORM_TITLE";
				objReader = DbFactory.GetDbReader( base.ConnectionString , sSQL  );

                base.StartDocument(ref objXmlDocument, ref objRootNode, "AvailableFormNames");
				while( objReader.Read() )
				{
					base.CreateElement( objRootNode , "FormName" , ref objPolicyNode );
					base.CreateAndSetElement( objPolicyNode , "formid" , Conversion.ConvertObjToInt( objReader.GetValue( "FORM_ID" ), base.ClientId ).ToString() );
                    base.CreateAndSetElement(objPolicyNode, "formtitle", Conversion.ConvertObjToStr(objReader.GetValue("FORM_TITLE")).ToString());
				}

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EnhancePolicy.LoadAddForms.Error", base.ClientId), p_objEx);				
			}
			finally
			{
                if (objReader != null)
                    if (!objReader.IsClosed)
                        objReader.Close();
			}
			return(objXmlDocument);
		}

		public XmlDocument SaveForms ( string p_sSelectedFormIDs ,int p_lPolicyID)
		{
			
			DbReader objReader = null;
			XmlDocument objXmlDocument = null ;
			string sSQL = string.Empty ; 
			int lFormID;
			string [] sFormIdArray =null;
             int iCount;
				try
				{
					sFormIdArray = p_sSelectedFormIDs.Split(new char[] {','});
					iCount =sFormIdArray.GetLength(0);
					for( int i=0 ;i< iCount ;i++)
					{
						lFormID=Convert.ToInt32(sFormIdArray[i]);
						sSQL="SELECT POLICY_ID FROM POLICY_X_FORMS_ENH WHERE POLICY_ID=" + p_lPolicyID + "AND FORM_ID =" + lFormID;
						objReader = DbFactory.GetDbReader( base.ConnectionString , sSQL  );
						if ( !objReader.Read())
						{
							sSQL =  "INSERT INTO POLICY_X_FORMS_ENH(POLICY_ID,FORM_ID) VALUES (" + p_lPolicyID + "," + lFormID +")" ;
							base.DataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL,base.DataModelFactory.Context.DbTrans);
						}
					}
					}
			
				catch( RMAppException p_objEx )
				{
					throw p_objEx ;
				}
				catch( Exception p_objEx )
				{
                    throw new RMAppException(Globalization.GetString("EnhancePolicy.SaveForms.Error", base.ClientId), p_objEx);				
				}
				finally
				{
                    if (objReader != null)
                        if (!objReader.IsClosed)
                            objReader.Close();
                }			
                //Mona
				//objXmlDocument=LoadAddForms();     
                objXmlDocument = GetPolicies(p_lPolicyID);
			  return(objXmlDocument);
			}

        // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
        public XmlDocument SaveForms(string p_sSelectedFormIDs, int p_lPolicyID, string p_sAddedForms)
        {

            XmlDocument objXmlDocument = null;
            SaveForms(p_sSelectedFormIDs, p_lPolicyID);
            objXmlDocument = GetPolicies(p_lPolicyID, p_sAddedForms);
            return (objXmlDocument);
        }
        // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms

		public XmlDocument DeleteForms( string p_sSelectedFormIDs ,int p_lPolicyID)
		{
	
			XmlDocument objXmlDocument = null ;
			string sSQL = string.Empty ; 
			
			try
			{
				        
						sSQL =  "DELETE FROM POLICY_X_FORMS_ENH WHERE POLICY_ID="+ p_lPolicyID + " AND FORM_ID IN(" + p_sSelectedFormIDs +")" ;
						base.DataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL,base.DataModelFactory.Context.DbTrans);
					
				}
			
			
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("EnhancePolicy.DeleteForms.Error", base.ClientId), p_objEx);				
			}
			finally
			{ 	}			
			objXmlDocument = GetPolicies(p_lPolicyID);	
			return(objXmlDocument);
                
		}

        // npadhy Start MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
        public XmlDocument DeleteForms(string p_sSelectedFormIDs, int p_lPolicyID, string p_sAddedForms)
        {

            XmlDocument objXmlDocument = null;
            DeleteForms(p_sSelectedFormIDs, p_lPolicyID);
            objXmlDocument = GetPolicies(p_lPolicyID, p_sAddedForms);
            return (objXmlDocument);

        }
        // npadhy End MITS 18860 If there are form on the Page which are not saved, then after postback we need to retain those Forms
				
	}
#endregion

}
