﻿
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Data;
using System.Collections;
using System.Drawing;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.Reserves;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Models;
using Riskmaster.Application.FileStorage;

namespace Riskmaster.Application.ReserveWorksheet
{
  public class ReserveWorksheetGeneric : ReserveWorksheetBase
    {
        ReserveWorksheetCommon m_CommonInfo = null;
      //Mits 19027
        private bool bPrevModValtToZero = false;
        SysSettings objSysSetting = null;
        UserLogin objUserlogin = null;//Deb ML Changes
        private int m_iClientId =0;

        #region Constructor

        public ReserveWorksheetGeneric(ReserveWorksheetCommon p_CommonInfo, int p_iClientId): base(p_CommonInfo, p_iClientId)
        {
            m_CommonInfo = p_CommonInfo;
            m_iClientId = p_iClientId;
        }

        #endregion

        #region PublicFunctions

        /// <summary>
        /// This function loads the data of the Reserve Worksheet window.
        /// It is called when a New worksheet is being created, as well as when an existing worksheet has to be viewed.
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <returns>The XML for the Reserve Worrksheet</returns>
        public override void GetReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref ReserveWorksheetResponse worksheetResponse, UserLogin p_objLogin)
        {
            XmlDocument objXMLOut = new XmlDocument();
            //XmlDocument objXmlIn 
            int iClaimId = 0;
            //int iClaimantEId = 0;
            //int iUnitId = 0;
            int iClaimantRowId = 0;
            int iUnitRowId = 0;
            string sClaimStatus = string.Empty;
            string sFromApproved = string.Empty;
            string sRSWID = string.Empty;
            string sSQL = string.Empty;
            string sRSWCustomReserveXML = string.Empty;
            string sRswTypeToLaunch = string.Empty;
            int iSubmittedBy = 0;
            DbReader objReader = null;
            LocalCache objCache = null;
            ReserveWorksheetCustomize objRSWCust;
            string sRSWType = string.Empty;
            int iSubmittedTo = 0;           
            

            try
            {
                //Deb ML Changes
                objUserlogin = p_objLogin;
                base.m_baseUserlogin = p_objLogin;
                //Deb ML Changes
                if (worksheetRequest.RSWId != 0)
                {
                    sRSWID = worksheetRequest.RSWId.ToString();
                }
                else
                {
                    sRSWID = "";
                }
               
                
                //If 'RSWId' is empty or null, then a 'New' Reserve Worksheet is being made
                iClaimId = worksheetRequest.ClaimId;
                //MGaba2:R6:ClaimantRowId/UnitRowId will be sent from Ui rather ClaimantEid/UnitId:Start
                //*******TO DO:In Models,worksheetRequest.ClaimantEId now represents ClaimantRowId.So its name should be changed

                //iClaimantEId = worksheetRequest.ClaimantEId;
                //iUnitId = worksheetRequest.UnitId;

                iClaimantRowId = worksheetRequest.ClaimantEId;
                iUnitRowId = worksheetRequest.UnitId;
                //Mits 19027 shobhana
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;
                //Mits 20147 shobhana
                if (iClaimantRowId != 0)
                {
                    GetClaimFromClaimant(iClaimantRowId, ref iClaimId);                    
                }
                else if (iUnitRowId != 0)
                {
                    GetClaimFromUnit(iUnitRowId, ref iClaimId);                    
                }
                //MGaba2:R6:End

                if (sRSWID != "")
                {
                    sSQL = "SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID;
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                    if (objReader.Read())
                    {
                        objXMLOut.LoadXml(objReader.GetString("RSW_XML"));
                        iSubmittedTo = Riskmaster.Common.Conversion.ConvertStrToInteger(objReader.GetString("SUBMITTED_TO"));
                        iSubmittedBy = Riskmaster.Common.Conversion.ConvertStrToInteger(objReader.GetString("SUBMITTED_BY"));
                        //sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                        //tmalhotra2 mits 27272 
                        if (!string.IsNullOrEmpty(objReader.GetString("RSWXMLTYPE")))
                        {
                            sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                        }
                        else
                        {
                            sRswTypeToLaunch = "Generic";
                        }

                        if (!(sRswTypeToLaunch == "Generic" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History")))
                        { 
                            if (!IsRSWAllowed(worksheetRequest.RSWId, sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                             {
                            if (sRswTypeToLaunch == "Customize" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText=="Approved"||objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText=="History"))

                               {
                              objRSWCust = new ReserveWorksheetCustomize(m_CommonInfo,m_iClientId);
                              objRSWCust.GetReserveWorksheet(worksheetRequest, ref worksheetResponse, p_objLogin);
                                return;

                            
                            
                               }
                            else

                            throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                             }
                       }
                    }
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));
                    
                    if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Rejected")
                    {

                        #region Checking Authorized approver
                        if (iSubmittedBy != 0)
                        {
                            if (m_CommonInfo.UserId != iSubmittedBy) //If a user other than the one submitted by tries to edit the sheet, then perform the following check
                            {
                                int iLevel = 1;
                                iLevel = GetSupervisoryLevel(iSubmittedBy, m_CommonInfo.UserId, iClaimId);
                                if (iLevel == 0)
                                {
                                    if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                        objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "NoAuthEdit";

                                    //sachin 6385 starts
                                    if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                                        objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText = "NoAuthEdit";
                                    //sachin 6385 ends

                                    //making Rejected Worksheet Readonly for UnAuthrized User
                                    objXMLOut = MakeRWReadOnly(objXMLOut);
                                    worksheetResponse.OutputXmlString = objXMLOut.OuterXml;
                                    return;
                                }
                            }
                        }
                        #endregion
                    }

                    // If the user is editing a Rejected worksheet, change the Request Status while rendering on screen.
                    //changed by Nitin for R6 in order to open Rejected sheet in editable mode
                    if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Rejected")
                    {
                        objXMLOut = MakeRWEditable(objXMLOut);
                        //objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText = "Rejected (Edit Mode)";
                        objXMLOut.SelectSingleNode("//control[@name='hdnMode']").InnerText = "Edit";
                        //By Vaibhav for Safeway MITS 13937
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                            objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";

                        //sachin 6385
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                            objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText = "";
                        //sachin 6385
                    }

                    //If the worksheet has to be opened in 'ReadOnly' mode, appropriate function is called and 
                    //controls are rendered as read-only.
                    if (objXMLOut.SelectSingleNode("//control[@name='hdnMode']").InnerText == "ReadOnly")
                    {
                        objXMLOut = MakeRWReadOnly(objXMLOut);
                        //By Vaibhav for Safeway MITS 13939
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                            objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";

                        //sachin 6385
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                            objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText = "";
                        //sachin 6385
                    }

                    if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Pending Approval")
                    {
                        if (m_CommonInfo.UserId != iSubmittedBy)
                        {
                            if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";

                            //sachin 6385
                            if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                                objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText = "";
                            //sachin 6385
                        }
                    }
                    else if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Rejected")
                    {
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                            objXMLOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "";

                        //sachin 6385
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                            objXMLOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText = "";
                        //sachin 6385
                    }
                
                 }               
                else
                {
                    // Create a new Worksheet.
                    if (iClaimId == 0)
                    {
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.ClaimNotFound",m_iClientId));
                    }
                    else
                    {
                        //making SMS Security permission check, whether the current user is 
                        // having the permission to update Reserve amount or not
                        GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);

                        ////checking To Create New Worksheet SMS Permission
                        if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_CREATE))
                            throw new RMAppException("You don't have permission to complete requested operation.");

                        //check,wheather the Claim is closed 
                        if (IsClosedClaim(iClaimId))
                            throw new RMAppException("ReserveWorksheet can not be created for Closed Claim");

                        //FillNewWorkSheet(ref objXMLOut, iClaimId,iClaimantEId,iUnitId,worksheetRequest.FromApproved);
                        FillNewWorkSheet(ref objXMLOut, iClaimId, iClaimantRowId, iUnitRowId, worksheetRequest.FromApproved, worksheetRequest.RSWReservesXML);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                //Deb ML Changes
                objUserlogin = null;
                base.m_baseUserlogin = null;
                //Deb ML Changes
            }
            worksheetResponse.OutputXmlString = objXMLOut.OuterXml;
        }

      //sachin 6385 starts
        public int MoveToRcRowId(int iRcRowId, int iHoldReasonCode, int iTableId)
        {
            string sSQLholdreason = string.Empty;
            DbReader dbHoldReason = null;
            int sHoldRowId = 0;
            try
            {
                sSQLholdreason = "SELECT ROW_ID FROM HOLD_REASON WHERE ON_HOLD_ROW_ID = " + iRcRowId + " AND HOLD_REASON_CODE= " + iHoldReasonCode + " AND ON_HOLD_TABLE_ID= " + iTableId;
                dbHoldReason = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQLholdreason.ToString());
                sSQLholdreason.Remove(0, sSQLholdreason.Length);
                if (dbHoldReason.Read())
                    sHoldRowId = dbHoldReason.GetInt32("ROW_ID");
                return sHoldRowId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (dbHoldReason != null)
                {
                    dbHoldReason.Close();
                    dbHoldReason = null;
                }
            }
        } 
      //sachin 6385 ends

        public override void SubmitWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            XmlNode objNode = null;
            XmlDocument objXmlIn = null;
            int iClaimId = 0;
            int iAdjId = 0;
            int iApproverId = 0;
            int iClaimantRowId = 0;
            int iUnitRowId = 0;

            DbReader objReaderLOB = null;
            int iLOBCode = 0;
            int iClmTypeCode = 0;

            bool bUnderLimit = false;
            bool bUnderClaimIncLimit = false;//sachin 6385
            LocalCache objCache = null;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;
            string sRSWId = string.Empty;
            string sCallFor = string.Empty;

            StringBuilder sbSQL;
            DbConnection objCon = null;
            DbReader objReader = null;
            string sRswTypeToLaunch = string.Empty;
            int iNewStatusCode=0;
            int iHoldStatusCode;//JIRA 6385 Snehal
            bool bDaysApprovalLapsed = false;

            ReserveFunds.structReserves[] arrReserves = null;
            int iRSWID = 0;
            int iSubmToID = 0;
            int iSubmByID = 0;
            string sRSWDateTime = "";
            //skhare7 
            ReserveCurrent objReserveCurrent = null;
            DataModelFactory objDataModelFactory = null;
            int iClaimCurrCode = 0;
            DbWriter objWriter = null;//sachin 6385
            int iRowId = 0;//6385
            int iHoldRowId = 0;//sachin 6385
            string sActionType  = string.Empty;//Sachin 6385
            try
            {
                //Get Supervisory Approval Settings
                ReadRSWCheckOptions();
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objWriter = DbFactory.GetDbWriter(m_CommonInfo.Connectionstring);//sachin 6385
                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWid']");
                if (objNode != null)
                    iRSWID = Conversion.ConvertStrToInteger(objNode.InnerText);

                string sReason = "";

                objNode = objXmlIn.SelectSingleNode("//AppRejReqCom");
                if (objNode != null)
                    sReason = objXmlIn.SelectSingleNode("//AppRejReqCom").InnerText;

                //Sachin 6385
                objNode = objXmlIn.SelectSingleNode("//ActionType");
                if (objNode != null)
                    sActionType = objNode.InnerText;

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnActionType']");
                if (objNode != null)
                    sActionType = objNode.InnerText;
                //Sachin 6385               

                if (iRSWID <= 0)
                {
                    objNode = objXmlIn.SelectSingleNode("//RswId");
                    if (objNode != null)
                        iRSWID = Conversion.ConvertStrToInteger(objNode.InnerText.Trim());

                    sbSQL.Append("SELECT RSWXMLTYPE,RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID);
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objReader.Read())
                    {
                        objXmlIn.LoadXml(objReader.GetString("RSW_XML"));
                        sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");

                        //Added by Shobhana
                        if (!IsRSWAllowed(iRSWID, sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                        {
                            throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                        }
                    }
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));

                    objReader.Close();
                }

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                if (objNode != null)
                {
                    sCallFor = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText;                                  
                }

                    //Get Claim ID
                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']");
                    if (objNode != null)
                        iClaimId = Conversion.ConvertStrToInteger(objNode.InnerText);

                    //Get ClaimantRowId
                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnClaimantRowId']");
                    if (objNode != null)
                        iClaimantRowId = Conversion.ConvertStrToInteger(objNode.InnerText);

                    //Get UnitRowId
                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnitRowId']");
                    if (objNode != null)
                        iUnitRowId = Conversion.ConvertStrToInteger(objNode.InnerText);

                    #region Checking Authorized approver
                    objCon.Open();
                if (sCallFor == "Pending Approval" || (sCallFor == "Pending" && p_WorksheetRequest.RequestName == "Approve") )
                    {
                        sbSQL.Append("SELECT SUBMITTED_TO FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID.ToString());
                        sSubmittedTo = Conversion.ConvertObjToStr(objCon.ExecuteScalar(sbSQL.ToString()));
                        sbSQL.Remove(0, sbSQL.Length);
                        iSubmToID = Conversion.ConvertStrToInteger(sSubmittedTo);
                    if (iSubmToID != 0 && m_bUseSupAppReserves)
                        {
                            if (m_CommonInfo.UserId != iSubmToID) //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                            {
                                int iLevel = 1;
                                iLevel = GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId, iClaimId);
                            bool bSameSmsGroup = false;
                            if (iLevel == 0)
                            {
                                if (m_bAllowAccessGrpSup)
                                {
                                    bSameSmsGroup = CheckUserGroup(iSubmToID, m_CommonInfo.UserId, p_iGroupId, iRSWID);
                                }
                                if (!bSameSmsGroup)
                                {
                                    if (objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                        objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText = "NoAuthSubmit";
                                    p_WorksheetResponse.OutputXmlString = objXmlIn.OuterXml;
                                    return;
                                }
                                }
                            }
                        }
                    }
                    #endregion

                    sbSQL.Append("SELECT SUBMITTED_BY FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID.ToString());
                    iSubmByID = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                    sbSQL.Remove(0, sbSQL.Length);

                    #region Get the Line of Business for the claim

                    sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE, CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId);
                    objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    if (objReaderLOB.Read())
                    {
                        iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                        iClmTypeCode = objReaderLOB.GetInt32("CLAIM_TYPE_CODE");
                        iClaimCurrCode = objReaderLOB.GetInt32("CLAIM_CURR_CODE");
                    }
                    objReaderLOB.Close();
                    sbSQL.Remove(0, sbSQL.Length);

                    #endregion

                    //Check Reserve Limits Enabled/Disabled
                    GetLOBReserveOptions(iClaimId);

                    //Get Supervisory Approval Settings
                    ReadRSWCheckOptions();

                //    arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                //changed by shobhana customization of reserves
                    arrReserves = GetReservesTypeCustom(iLOBCode, iClmTypeCode, p_WorksheetRequest.RSWReservesXML, objXmlIn);
                //end
                    string[,] sResFinal = null;
                    //nsachdeva2 - MITS:25502 - 09/26/2011
                    //sResFinal = new string[arrReserves.GetLength(0), 4];
                    sResFinal = new string[arrReserves.GetLength(0), 5];
                    //End MITS:25502
                    sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objXmlIn,iClaimCurrCode);
                    //aaggarwal29 MITS 27763 -- start 
                    // Previously Security check was happening only if m_bREsetLimitFlag is true, which was not correct.
                    //If user does not have "UPDATE" permissions, he should not be allowed to perform any updates under any condition.
                    GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);
                    if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                    {
                        throw new RMAppException("You don't have permission to complete requested operation.");
                    }
                    //aaggarwal29 MITS 27763 --end
                    if (m_bResLimitFlag) // If the Flag is 'true' then check the reserve limits
                    {
                        bUnderLimit = UnderUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves,iClaimCurrCode);
                        //By Vaibhav on 11/14/08
                        if (bUnderLimit && m_iDaysAppReserves > 0)
                        {
                            sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                            int iTopID = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                            sbSQL.Remove(0, sbSQL.Length);

                            if (iTopID != m_CommonInfo.UserId)
                            {
                                //Check if Days for Approval has lapsed and set the bUnderLimit flag to false
                                //so that the reserve worksheet goes to the next level supervisor
                                sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRSWID);
                                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                                sbSQL.Remove(0, sbSQL.Length);
                                if (objReader.Read())
                                {
                                    sRSWDateTime = objReader.GetString("DTTM_RCD_LAST_UPD");
                                    //By Vaibhav on 11/14/08 : modified  CheckDaysForApprovalLapsed function
                                    // By Vaibhav on 12/03/2008 : MITS 13937
                                    bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, m_CommonInfo.UserId, m_CommonInfo.UserId, iClaimId);
                                    // bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, Conversion.ConvertObjToInt(objReader.GetString("SUBMITTED_TO"), base.ClientId), p_iUserId);
                                }
                                objReader.Close();
                                if (bDaysApprovalLapsed)
                                    bUnderLimit = false;
                            }
                        }
                        //vaibhav code end
                        //to make SMS permission check here
                        //aaggarwal29 code commenting start for MITS 27763 
                        //Moved this code out of if clause as security permissions were getting checked only in some scenarios
                        //GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);
                        //if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                        //{
                        //    //No Security permission is there 
                        //    bUnderLimit = false;
                        //    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSMSCheckToUpdate']");
                        //    if (objNode != null)
                        //        objNode.InnerText = "No";
                        //}
                        //else
                        //{    //aaggarwal29 MITS 27763 -- end
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSMSCheckToUpdate']");
                            if (objNode != null)
                                objNode.InnerText = "Yes";
                        //} 
                        //aaggarwal29 code commenting end for MITS 27763 
                    }
                    else
                        bUnderLimit = true;


                    //sachin 6385 starts
                    if (m_bClaimIncLimitFlag)
                    {
                        bUnderClaimIncLimit = UnderClaimIncUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves, iClaimCurrCode);
                    }
                    else
                        bUnderClaimIncLimit = true;

                    if (bUnderLimit && bUnderClaimIncLimit)//sachin 6385
                    {
                        # region Check Reserve Limits Enabled/Disabled: Supervisory Approval not required

                        //Change the worksheet status and save it as Pending Approval. No supervisory approval is required
                        //A Pending worksheet is sent directly for approval as the reserve limit is not set, approve the worksheet
                        if (sCallFor == "Pending" || sCallFor == "Pending Approval")
                        {
                            // Change the 'Last Modified By' and 'Last Modified Time'
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtLastMod']");
                            if (objNode != null)
                            {
                                objNode.InnerText = DateTime.Now.ToString();
                                objNode.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                            }

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']");
                            if (objNode != null)
                                objNode.InnerText = m_CommonInfo.UserName;

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']");
                            if (objNode != null)
                                objNode.InnerText = "Yes";

                            //sachin 6385 starts
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']");
                            if (objNode != null)
                                objNode.InnerText = "Yes";
                            //sachin 6385 ends

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                            if (objNode != null)
                            {
                                objNode.InnerText = m_CommonInfo.UserId.ToString();
                                sSubmittedTo = m_CommonInfo.UserId.ToString();
                            }

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                            if (objNode != null)
                            {
                                //If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted
                                #region If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted

                                // Update record in database table 'RSW_WORKSHEETS'
                                iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                                objNode.InnerText = "Pending Approval";
                                objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                                // Change the date rejected and rejected by fields
                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtAppRej']");
                                objNode.InnerText = "";
                                objNode.Attributes["dbformat"].InnerText = "";

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejBy']");
                                objNode.InnerText = "";

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                                if (objNode != null)
                                    objNode.InnerText = "ReadOnly";

                                // Update record in database table 'RSW_WORKSHEETS'
                                objXmlIn = SaveReserveWorksheet(objXmlIn);
                                SaveReserveWorksheetHistory(objXmlIn);
                                #endregion

                                //sachin 6385 starts
                                if (sCallFor == "Pending Approval")
                                {
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                    if (sActionType == "Approve")
                                    {
                                        iNewStatusCode = objCache.GetCodeId("AP", "RSW_STATUS");
                                        objNode.InnerText = "Approved";
                                    }
                                    if (sActionType == "Reject")
                                    {
                                        iNewStatusCode = objCache.GetCodeId("RJ", "RSW_STATUS");
                                        objNode.InnerText = "Rejected";
                                    }
                                    objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();
                                    objWriter.Reset(true);
                                    objWriter.Tables.Add("HOLD_REASON");
                                    objWriter.Fields.Add("HOLD_STATUS_CODE", iNewStatusCode);
                                    objWriter.Fields.Add("UPDATED_BY_USER", m_CommonInfo.UserName);
                                    objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                    objWriter.Where.Add("ON_HOLD_ROW_ID = " + iRSWID);
                                    objWriter.Execute();
                                }
                                //sachin 6385 ends

                                objXmlIn = MakeRWReadOnly(objXmlIn);
                                p_WorksheetResponse.OutputXmlString = objXmlIn.OuterXml;
                                return;
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        # region Supervisory Approval is required
                        //Supervisory approval is required. Change the worksheet status and save it as Pending Approval.

                        //Check the setting whether to use the Supervisor Approval
                        //If "no" Supervisor Approval then user will be dispalyed a message and the reserve worksheet will not save.
                        //If "yes" Supervisor Approval then check other settings to get the supervisor and update it accordingly

                        if (!m_bUseSupAppReserves)
                        {
                            //User will be shown the message:
                            //You do not have permission to set the reserve of this amount.
                            throw new RMAppException(Globalization.GetString("ReserveWorksheet.Reserves.PermissionErr",m_iClientId));
                        }
                        else
                        {
                            //Supervisory Approval will be followed as per settings.
                            //Use Current Adjusters supervisory chain
                            if (m_bUseCurrAdjReserves)
                            {
                                #region Use Current Adjusters supervisory chain

//MGaba2:R8:SuperVisory Approval
                                iAdjId = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);
                                //We have a current adjuster who has a RM user id assigned.  
                                //We go after this users limits and work our way up the supervisory chain
                                if (iAdjId > 0)
                                {
                                    //To notify immediate adjuster
                                    if (m_bNotifySupReserves)
                                    {
                                        if (iAdjId != m_CommonInfo.UserId)
                                            if (m_CommonInfo.UserId == iSubmByID)
                                                iApproverId = iAdjId;
                                            else
//MGaba2:R8:SuperVisory Approval
                                                iApproverId = CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId);
                                        else
//MGaba2:R8:SuperVisory Approval
                                            iApproverId = CommonFunctions.GetManagerID(iAdjId, m_CommonInfo.DSNID, base.ClientId);//We might end-up having iApproverId as 0
                                    }
                                    else //Use Current Adjusters supervisory chain
                                        iApproverId = GetApprovalID(iAdjId, iClaimId, m_CommonInfo.UserId, p_iGroupId, sResFinal, arrReserves,iClaimCurrCode);
                                }
                                else //We do not have a current adjuster with a RM user Id Assigned.
                                {
                                    //To notify immediate manager when there is no current adjuster
                                    if (m_bNotifySupReserves)
//MGaba2:R8:SuperVisory Approval
                                        iApproverId = CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId);//We might end-up having iApproverId as 0
                                    else //Check the limits for the user who is logged on and work our way up supervisory chain
                                        iApproverId = GetApprovalID(m_CommonInfo.UserId, iClaimId, m_CommonInfo.UserId, p_iGroupId, sResFinal, arrReserves,iClaimCurrCode);
                                }

                                #endregion
                            }
                            else
                            {
                                #region Use Current Users supervisory chain

                                //To notify immediate manager
                                if (m_bNotifySupReserves)
//MGaba2:R8:SuperVisory Approval
                                    iApproverId = CommonFunctions.GetManagerID(m_CommonInfo.UserId, m_CommonInfo.DSNID, base.ClientId);//We might end-up having iApproverId as 0
                                else //Check the limits for the user who is logged on and work our way up there supervisory chain 
                                    iApproverId = GetApprovalID(m_CommonInfo.UserId, iClaimId, m_CommonInfo.UserId, p_iGroupId, sResFinal, arrReserves,iClaimCurrCode);

                                #endregion
                            }
                        }

                        #endregion

                        if (iApproverId == 0)
                        {     if(!bUnderLimit)                   
                            iApproverId = GetLOBManagerID(iLOBCode, sResFinal);   
                            if(!bUnderClaimIncLimit)
                                iApproverId = GetClaimIncLOBManagerID(iLOBCode, sResFinal);   
                        }

                        #region If valid approver then submit the worksheet to that approver

                        if (iApproverId > 0)
                        {
                            if (iApproverId != m_CommonInfo.UserId)
                            {

                                #region Get Approver information

                                sbSQL.Append("SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName");
                                sbSQL.Append(" FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + iApproverId);
                                sbSQL.Append(" AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + m_CommonInfo.DSNID);
                                objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                                sbSQL.Remove(0, sbSQL.Length);

                                if (objReader.Read())
                                {
                                    sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                                    sSubmittedTo = objReader.GetString("LOGIN_NAME");
                                }
                                else
                                {
                                    sSupName = "";
                                    sSubmittedTo = "";
                                }
                                objReader.Close();

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                                if (objNode != null)
                                {
                                    objNode.InnerText = sSubmittedTo;
                                }

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                                if (objNode != null)
                                {
                                    objNode.InnerText = iApproverId.ToString();
                                    sSubmittedTo = iApproverId.ToString();
                                }

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSupName']");
                                if (objNode != null)
                                {
                                    objNode.InnerText = sSupName;
                                }

                                #endregion

                                // Change the 'Last Modified By' and 'Last Modified Time'
                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtLastMod']");
                                if (objNode != null)
                                {
                                    objNode.InnerText = DateTime.Now.ToString();
                                    objNode.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                                }

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']");
                                if (objNode != null)
                                    objNode.InnerText = m_CommonInfo.UserName;

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                if (objNode != null)
                                {
                                    iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                                    objNode.InnerText = "Pending Approval";
                                    objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();
                                }

                                //sachin 6385 starts
                                if (!bUnderLimit)
                                {
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']");

                                    if (objNode != null)
                                    {
                                        objNode.InnerText = "No";
                                        //JIRA 6385 Start Snehal
                                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnHoldStatus']");                                       
                                        if (objNode != null)
                                        {
                                            objNode.InnerText = "";
                                            objNode.Attributes["Holdcodeid"].InnerText = "";
                                        }
                                        iHoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId);
                                        if (objNode != null)
                                        {
                                            iHoldRowId = MoveToRcRowId(iRSWID, iHoldStatusCode, objCache.GetTableId("RSW_WORKSHEETS"));
                                            //JIRA 7810 Start sachin
                                            if (iHoldRowId <= 0)
                                            {
                                                iRowId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "HOLD_REASON", base.ClientId);
                                                objWriter.Reset(true);
                                                objWriter.Tables.Add("HOLD_REASON");
                                                objWriter.Fields.Add("ROW_ID", iRowId);
                                                objWriter.Fields.Add("ON_HOLD_TABLE_ID", objCache.GetTableId("RSW_WORKSHEETS"));
                                                objWriter.Fields.Add("ON_HOLD_ROW_ID", iRSWID);
                                                objWriter.Fields.Add("HOLD_REASON_CODE", iHoldStatusCode);
                                                objWriter.Fields.Add("HOLD_STATUS_CODE", iNewStatusCode);
                                                objWriter.Fields.Add("DTTM_RCD_ADDED", objXmlIn.SelectSingleNode("//control[@name='dtCreated']/@dbformat").InnerText);
                                                objWriter.Fields.Add("UPDATED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("ADDED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));

                                                objWriter.Execute();
                                            }
                                            //JIRA 6385 end

                                            objNode.InnerText = objCache.GetCodeDesc(iHoldStatusCode);
                                            objNode.Attributes["Holdcodeid"].InnerText = iHoldStatusCode.ToString();
                                        }
                                        //JIRA 6385 End snehal
                                    }
                                }//sachin 6385 ends
                                else if (bUnderLimit && sCallFor == "Pending Approval")
                                {
                                    if (sActionType == "Approve")
                                    {
                                        iHoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId);
                                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                        if (objNode != null)
                                        {
                                            iNewStatusCode = objCache.GetCodeId("AP", "RSW_STATUS");
                                            objNode.InnerText = "Approved";
                                            objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();
                                        }
                                        objWriter.Reset(true);
                                        objWriter.Tables.Add("HOLD_REASON");
                                        objWriter.Fields.Add("HOLD_STATUS_CODE", iNewStatusCode);
                                        objWriter.Fields.Add("UPDATED_BY_USER", m_CommonInfo.UserName);
                                        objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                        objWriter.Where.Add("ON_HOLD_ROW_ID = " + iRSWID + " AND HOLD_REASON_CODE = " + iHoldStatusCode);
                                        objWriter.Execute();
                                    }
                                }
 
                                //sachin 6385 starts
                                if (!bUnderClaimIncLimit) //6385 Snehal
                                {
                                    if (bUnderLimit)
                                    {
                                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnHoldStatus']");
                                        if (objNode != null)
                                        {
                                            objNode.InnerText = "";
                                            objNode.Attributes["Holdcodeid"].InnerText = "";
                                        }
                                    }
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                    if (objNode != null)
                                    {
                                        iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                                        objNode.InnerText = "Pending Approval";
                                        objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();
                                    }
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']");

                                    if (objNode != null)
                                    {
                                        objNode.InnerText = "No";
                                        //JIRA 6385 Start Snehal
                                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnHoldStatus']");                                    
                                        iHoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId);                                        
                                     
                                        if (objNode != null)
                                        {
                                            iHoldRowId = MoveToRcRowId(iRSWID, iHoldStatusCode, objCache.GetTableId("RSW_WORKSHEETS"));
                                            //JIRA sachin 6385 Start
                                            if (iHoldRowId <= 0)
                                            {
                                                iRowId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "HOLD_REASON", base.ClientId);
                                                objWriter.Reset(true);
                                                objWriter.Tables.Add("HOLD_REASON");
                                                objWriter.Fields.Add("ROW_ID", iRowId);
                                                objWriter.Fields.Add("ON_HOLD_TABLE_ID", objCache.GetTableId("RSW_WORKSHEETS"));
                                                objWriter.Fields.Add("ON_HOLD_ROW_ID", iRSWID);
                                                objWriter.Fields.Add("HOLD_REASON_CODE", iHoldStatusCode);
                                                objWriter.Fields.Add("HOLD_STATUS_CODE", iNewStatusCode);
                                                objWriter.Fields.Add("DTTM_RCD_ADDED", objXmlIn.SelectSingleNode("//control[@name='dtCreated']/@dbformat").InnerText);
                                                objWriter.Fields.Add("UPDATED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("ADDED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));

                                                objWriter.Execute();
                                            }
                                           
                                            if (objNode.InnerText == "")
                                            {
                                                objNode.InnerText = objCache.GetCodeDesc(iHoldStatusCode);
                                                objNode.Attributes["Holdcodeid"].InnerText = iHoldStatusCode.ToString();
                                            }
                                            else
                                            {
                                                objNode.InnerText = objNode.InnerText + ", " + objCache.GetCodeDesc(iHoldStatusCode);
                                                objNode.Attributes["Holdcodeid"].InnerText = objNode.Attributes["Holdcodeid"].InnerText + ", " + iHoldStatusCode.ToString();
                                            }
                                        }
                                        //JIRA 6385 end
                                        //JIRA 6385 End Snehal

                                    }
                                }
                                if (bUnderClaimIncLimit && sCallFor == "Pending Approval" )
                                {
                                    if (sActionType == "Approve")
                                    {

                                        iHoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId);
                                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                        if (objNode != null)
                                        {
                                            iNewStatusCode = objCache.GetCodeId("AP", "RSW_STATUS");
                                            objNode.InnerText = "Approved";
                                            objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();
                                        }
                                        objWriter.Reset(true);
                                        objWriter.Tables.Add("HOLD_REASON");
                                        objWriter.Fields.Add("HOLD_STATUS_CODE", iNewStatusCode);
                                        objWriter.Fields.Add("UPDATED_BY_USER", m_CommonInfo.UserName);
                                        objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                        objWriter.Where.Add("ON_HOLD_ROW_ID = " + iRSWID + " AND HOLD_REASON_CODE = " + iHoldStatusCode);
                                        objWriter.Execute();
                                    }
                                }
                                //sachin 6385 ends

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                if (objNode != null)
                                {
                                    //If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted
                                    #region If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted

                                    // Update record in database table 'RSW_WORKSHEETS'
                                    iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                                    objNode.InnerText = "Pending Approval";
                                    objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                                    // Change the date rejected and rejected by fields
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtAppRej']");
                                    objNode.InnerText = "";
                                    objNode.Attributes["dbformat"].InnerText = "";

                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejBy']");
                                    objNode.InnerText = "";

                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                                    if (objNode != null)
                                        objNode.InnerText = "ReadOnly";

                                    //rejection comments
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejComm']");
                                    if (objNode != null)
                                        objNode.InnerText = sReason;

                                    // Update record in database table 'RSW_WORKSHEETS'
                                    objXmlIn = SaveReserveWorksheet(objXmlIn);
                                    SaveReserveWorksheetHistory(objXmlIn);
                                    #endregion

                                    objXmlIn = MakeRWReadOnly(objXmlIn);
                                }
                            }
                            else
                            {
                                //Current User has the Authority as LOB Manager or Top Level Manager
                                //Change the 'Last Modified By' and 'Last Modified Time'
                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtLastMod']");
                                if (objNode != null)
                                {
                                    objNode.InnerText = DateTime.Now.ToString();
                                    objNode.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                                }

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']");
                                if (objNode != null)
                                    objNode.InnerText = m_CommonInfo.UserName;

                                //objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']");
                                //if (objNode != null)
                                //    objNode.InnerText = "Yes";

                                //objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']");
                                //if (objNode != null)
                                //    objNode.InnerText = "Yes";

                                //sachin 6385 starts
                                if (!bUnderLimit)
                                {
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']");

                                    if (objNode != null)
                                    {
                                        objNode.InnerText = "No";
                                        //JIRA 6385 Start Snehal
                                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnHoldStatus']");
                                        if (objNode != null)
                                        {
                                            iHoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId);
                                            iHoldRowId = MoveToRcRowId(iRSWID, iHoldStatusCode, objCache.GetTableId("RSW_WORKSHEETS"));
                                            //JIRA 6385 Start sachin
                                            if (iHoldRowId <= 0)
                                            {
                                                iRowId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "HOLD_REASON", base.ClientId);
                                                objWriter.Reset(true);
                                                objWriter.Tables.Add("HOLD_REASON");
                                                objWriter.Fields.Add("ROW_ID", iRowId);
                                                objWriter.Fields.Add("ON_HOLD_TABLE_ID", objCache.GetTableId("RSW_WORKSHEETS"));
                                                objWriter.Fields.Add("ON_HOLD_ROW_ID", iRSWID);
                                                objWriter.Fields.Add("HOLD_REASON_CODE", iHoldStatusCode);
                                                objWriter.Fields.Add("HOLD_STATUS_CODE", iNewStatusCode);
                                                objWriter.Fields.Add("DTTM_RCD_ADDED", objXmlIn.SelectSingleNode("//control[@name='dtCreated']/@dbformat").InnerText);
                                                objWriter.Fields.Add("UPDATED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("ADDED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));

                                                objWriter.Execute();
                                            }
                                            //JIRA 6385 end

                                            objNode.InnerText = objCache.GetCodeDesc(iHoldStatusCode);
                                            objNode.Attributes["Holdcodeid"].InnerText = iHoldStatusCode.ToString();
                                        }
                                        //JIRA 6385 End Snehal
                                    }
                                }//sachin 6385 ends

                                //sachin 6385 starts
                                if (!bUnderClaimIncLimit) //6385 Snehal
                                {
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']");

                                    if (objNode != null)
                                    {
                                        objNode.InnerText = "No";
                                        //JIRA 6385 Start Snehal
                                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnHoldStatus']");
                                        if (objNode != null)
                                        {
                                            iHoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId);
                                            iHoldRowId = MoveToRcRowId(iRSWID, iHoldStatusCode, objCache.GetTableId("RSW_WORKSHEETS"));
                                            //JIRA 7810 Start sachin
                                            if (iHoldRowId <= 0)
                                            {
                                                iRowId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "HOLD_REASON", base.ClientId);
                                                objWriter.Reset(true);
                                                objWriter.Tables.Add("HOLD_REASON");
                                                objWriter.Fields.Add("ROW_ID", iRowId);
                                                objWriter.Fields.Add("ON_HOLD_TABLE_ID", objCache.GetTableId("RSW_WORKSHEETS"));
                                                objWriter.Fields.Add("ON_HOLD_ROW_ID", iRSWID);
                                                objWriter.Fields.Add("HOLD_REASON_CODE", iHoldStatusCode);
                                                objWriter.Fields.Add("HOLD_STATUS_CODE", iNewStatusCode);
                                                objWriter.Fields.Add("DTTM_RCD_ADDED", objXmlIn.SelectSingleNode("//control[@name='dtCreated']/@dbformat").InnerText);
                                                objWriter.Fields.Add("UPDATED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("ADDED_BY_USER", objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                                                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));

                                                objWriter.Execute();
                                            }
                                           
                                            if (objNode.InnerText == "")
                                            {
                                                objNode.InnerText = objCache.GetCodeDesc(iHoldStatusCode);
                                                objNode.Attributes["Holdcodeid"].InnerText = iHoldStatusCode.ToString();
                                            }
                                            else
                                            {
                                                objNode.InnerText = objNode.InnerText + ", " + objCache.GetCodeDesc(iHoldStatusCode);
                                                objNode.Attributes["Holdcodeid"].InnerText = objNode.Attributes["Holdcodeid"].InnerText + ", " + iHoldStatusCode.ToString();
                                            }
                                        }
                                        //JIRA 6385 End sachin

                                    }
                                } 
                                //sachin 6385 ends
                                //sachin 6385 ends

                                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                if (objNode != null)
                                {
                                    //If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted
                                    #region If an existing Non-Rejected or Rejected (Edit Mode) worksheet is being submitted

                                    // Update record in database table 'RSW_WORKSHEETS'
                                    iNewStatusCode = objCache.GetCodeId("PA", "RSW_STATUS");
                                    objNode.InnerText = "Pending Approval";
                                    objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                                    // Change the date rejected and rejected by fields
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtAppRej']");
                                    objNode.InnerText = "";
                                    objNode.Attributes["dbformat"].InnerText = "";

                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejBy']");
                                    objNode.InnerText = "";

                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                                    if (objNode != null)
                                        objNode.InnerText = "ReadOnly";

                                    //rejection comments
                                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejComm']");
                                    if (objNode != null)
                                        objNode.InnerText = sReason;

                                    // Update record in database table 'RSW_WORKSHEETS'
                                    objXmlIn = SaveReserveWorksheet(objXmlIn);
                                    SaveReserveWorksheetHistory(objXmlIn);
                                    #endregion

                                    objXmlIn = MakeRWReadOnly(objXmlIn);
                                    p_WorksheetResponse.OutputXmlString = objXmlIn.OuterXml;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            //Mgaba2:MITS 19141:If there is existing no supervisory chain it should show exception if reserve limit exceeds
                            //Throwing exception and removing the existing code for readability purpose
                            throw new RMAppException(Globalization.GetString("ReserveWorksheet.LimitsExceedsAndNoSupervisor.Error",m_iClientId));

                        }

                        #endregion
                    }

                    #region send email/diary

                if (iApproverId > 0 && (!m_bDisableDiaryNotify || !m_bDisableEmailNotify))//rsushilaggar 22-Mar-2010 Mits : 19624
                    {
                        // Send an email to the person to whom the worksheet is submitted.
                        string sToEmail = string.Empty;
                        string sFromEmail = string.Empty;
                        string sSubjectEmail = string.Empty;
                      //  string sBodyEmail = string.Empty;
                        string sToUser = string.Empty;
                        string sFromUser = string.Empty;

                        string sRSWType = string.Empty;
                        string sClaimNumber = string.Empty;
                        string sPCName = string.Empty;
                        string sSubmittedBy = string.Empty;
                        string strReason = string.Empty;

                        string sCurAdjEmail = string.Empty;
                        string sCurAdjUser = string.Empty;
                        string sCurAdjLogin = string.Empty;
                        int iAdjID = 0;
                        DbConnection objSecDBConn = null;

                     
                        objSecDBConn = DbFactory.GetDbConnection(m_CommonInfo.SecurityConnectionstring);
                        objSecDBConn.Open();

                        sbSQL.Append("SELECT LOGIN_NAME");
                        sbSQL.Append(" FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + Conversion.ConvertStrToInteger(iApproverId.ToString()));
                        sbSQL.Append(" AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + m_CommonInfo.DSNID);
                        sSubmittedTo = Conversion.ConvertObjToStr(objSecDBConn.ExecuteScalar(sbSQL.ToString()));
                        sbSQL.Remove(0, sbSQL.Length);

                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWType']");
                        if (objNode != null)
                            sRSWType = objNode.Attributes["title"].InnerText;

                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnClaimNumber']");
                        sClaimNumber = objNode.InnerText;

                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnPCName']");
                        sPCName = objNode.InnerText;

                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']");
                        sSubmittedBy = objNode.InnerText;

                        objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejComm']");
                        if (objNode != null)
                        {
                            if (objNode.InnerText != "")
                                strReason = objNode.InnerText;
                        }
                        // Send a diary to the person to whom the worksheet is submitted
                        string sRegarding = sClaimNumber + " * " + sPCName;
                        //skhare7 R8 Fn.Transffered to Common fns
                        CommonFunctions.GetUserDetails(sSubmittedTo, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sToEmail, ref sToUser, m_iClientId);
                        CommonFunctions.GetUserDetails(m_CommonInfo.UserName, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sFromEmail, ref sFromUser, m_iClientId);
                        //skhare7 R8 Fn.Transffered to Common fns end
                        //changed by shobhana 16/06/2010
                        string sBodyEmail = string.Empty;
                    //skhare7 Function is tranfrd to Reservecurrent Data Model
                       // EmailFormat objEMail = null;
                       
                      //  objEMail = new EmailFormat(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                        objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);

                        objReserveCurrent = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                   
                        sBodyEmail = objReserveCurrent.GetEmailFormat("RSWSheet_SUB", iClaimId);
                       // sBodyEmail = sBodyEmail + sExtraText;

                        if (sPCName != "" && sPCName != null)
                        {
                        sSubjectEmail = " Approval Request for Claim " + sClaimNumber + " - " + sPCName;
                      
                        }
                        else
                        {

                        sSubjectEmail = " Approval Request for Claim " + sClaimNumber ;
                        }
                //    MITS 22442
                      //  sBodyEmail = sBodyEmail.Replace("this ReserveWorksheet.", sExtraText)
                        sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", "Claim Reserve Worksheet")
                                          .Replace("{REASON}", strReason)
                                          .Replace("{USER_NAME}", sPCName)
                                          .Replace("{SUBMITTED_BY}", sFromUser)
                                          .Replace("{SUBMITTED_TO}", sToUser);

                        //changed by shobhana MITS 22442
                        if (sSubmittedTo != m_CommonInfo.UserName)
                        {
                            //The appropriate user should be getting an email and a diary
                        //Start: rsushilaggar : 22-Mar-2010 Mits : 19624
                        if(!m_bDisableDiaryNotify)
                            GenerateDiary(sSubjectEmail, sBodyEmail, 1, sSubmittedTo, m_CommonInfo.UserName, sRegarding, iClaimId);
                        if (sToEmail != "" && sFromEmail != "" && !m_bDisableEmailNotify)
                            //skhare7 R8 Fn.Transffered to Common fns
                            CommonFunctions.SendEmail(sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                            //end skhare7
                        //End:  rsushilaggar 
                        }
                    
//MGaba2:R8:SuperVisory Approval
                        iAdjID = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);

                        if ((iAdjID > 0) && (iApproverId != iAdjID))
                        {
                            sbSQL.Append("SELECT LOGIN_NAME");
                            sbSQL.Append(" FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + Conversion.ConvertStrToInteger(iAdjID.ToString()));
                            sbSQL.Append(" AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + m_CommonInfo.DSNID);
                            sCurAdjLogin = Conversion.ConvertObjToStr(objSecDBConn.ExecuteScalar(sbSQL.ToString()));
                            //skhare7 R8 Fn.Transffered to Common fns
                            CommonFunctions.GetUserDetails(sCurAdjLogin, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sCurAdjEmail, ref sCurAdjUser,m_iClientId);
                            //End skhare7
                            if (sPCName != "" && sPCName != null)
                            {
                                sSubjectEmail = " Approval Request for Claim " + sClaimNumber + " - " + sPCName;

                            }
                            else
                            {

                                sSubjectEmail = " Approval Request for Claim " + sClaimNumber;
                            }
                         //   MITS 22442
                                sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", "Claim Reserve Worksheet")
                                                       .Replace("{REASON}", strReason)
                                                       .Replace("{USER_NAME}", sPCName)
                                                       .Replace("{SUBMITTED_TO}", sToUser)
                                                       .Replace("{SUBMITTED_BY}", sFromUser);
                                          


                        
                            //End changed by shobhana 16/06/2010
                 
                        //
                        //Start: rsushilaggar : 22-Mar-2010 Mits : 19624
                        if(!m_bDisableDiaryNotify)
                            GenerateDiary(sSubjectEmail, sBodyEmail, 2, sCurAdjLogin, m_CommonInfo.UserName, sRegarding, iClaimId);
                        if (sToEmail != "" && sFromEmail != "" && !m_bDisableEmailNotify)
                            CommonFunctions.SendEmail(sCurAdjEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                            //skhare7 Transferred to Common fns for supervisory approval 
                        //End:  rsushilaggar 
                        }
                    }
                    #endregion
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    (Globalization.GetString("ReserveWorksheet.SubmitWorksheet.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                objNode = null;
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objReaderLOB != null)
                {
                    objReaderLOB.Dispose();
                    objReaderLOB = null;
                }
                if (objReserveCurrent != null)
                {
                    objReserveCurrent.Dispose();

                }
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();

                }
            }
            p_WorksheetResponse.OutputXmlString = objXmlIn.OuterXml;
            return;
        }

        public override void OnLoadInformation(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            DbConnection objCon = null;
            LocalCache objCache = null;
            string strSQL = string.Empty;
            int iClaimId;
            DbReader objReader = null;
            int iClaimantRowId = 0;
            string sRswTypeToLaunch = string.Empty;
            XmlDocument objXMLOut = new XmlDocument();
            int iUnitRowId = 0;
           
            int iClaimCurrCode = 0;    //Aman Multi Currency--Start
            int iBaseCurrCode = 0;
            StringBuilder sbXmlBuilder = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty; //Aman Multi Currency --end
                
            
            try
            {
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                //iClaimId = p_WorksheetRequest.ClaimId;
                //iClaimantRowId = p_WorksheetRequest.ClaimantEId;
                //iUnitRowId = p_WorksheetRequest.UnitId;

                //For assigning worksheet
           //     AssignReserveWorksheet();

                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objCon.Open();

                iClaimId = p_WorksheetRequest.ClaimId;
                iClaimantRowId = p_WorksheetRequest.ClaimantEId;
                iUnitRowId = p_WorksheetRequest.UnitId;

                //MGaba2:From UI,only ClaimantRowId/UnitRowId can come
                if (iClaimId == 0)
                {
                    if (iClaimantRowId > 0)
                    {
                        GetClaimFromClaimant(iClaimantRowId, ref iClaimId);
                    }
                    else if (iUnitRowId > 0)
                    {
                        GetClaimFromUnit(iUnitRowId, ref iClaimId);
                    }
                }

                //Aman Multi Currency --Start
                sbXmlBuilder = new StringBuilder();
                
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                if (objCache == null)
                {
                    objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                }
                iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(iClaimId, m_CommonInfo.Connectionstring);
                iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_CommonInfo.Connectionstring);
                if (iClaimCurrCode > 0)
                {
                    objCache.GetCodeInfo(iClaimCurrCode, ref sShortCode, ref sDesc);
                }
                else
                {
                    objCache.GetCodeInfo(iBaseCurrCode, ref sShortCode, ref sDesc);
                }

                sbXmlBuilder.Append("<CurrencyTypeList>");
                sbXmlBuilder.Append("<ClaimCurrency>");
                sbXmlBuilder.Append(sDesc.ToString());
                sbXmlBuilder.Append("</ClaimCurrency>");
                sbXmlBuilder.Append("</CurrencyTypeList>");
                p_WorksheetResponse.OutputXmlString = sbXmlBuilder.ToString();
                //Aman Multi Currency --end

                //for Pending 
                strSQL = "SELECT RSW_ROW_ID FROM RSW_WORKSHEETS WHERE CLAIM_ID= " + iClaimId + " AND CLAIMANT_ROW_ID= "
                        + iClaimantRowId + " AND UNIT_ROW_ID= " + iUnitRowId + " AND RSW_STATUS_CODE = "
                        + objCache.GetCodeId("PE", "RSW_STATUS");
                
                p_WorksheetResponse.ExistPending  = Conversion.ConvertObjToInt(objCon.ExecuteScalar(strSQL), base.ClientId);
                
                
                //for Pending Approval
                strSQL = "";
                strSQL = "SELECT RSW_ROW_ID FROM RSW_WORKSHEETS WHERE CLAIM_ID= " + iClaimId + " AND CLAIMANT_ROW_ID= "
                        + iClaimantRowId + " AND UNIT_ROW_ID= " + iUnitRowId + " AND RSW_STATUS_CODE = "
                        + objCache.GetCodeId("PA", "RSW_STATUS");   

                p_WorksheetResponse.ExistPendingApproval = Conversion.ConvertObjToInt(objCon.ExecuteScalar(strSQL), base.ClientId);
                
                //for Approved
                strSQL = "SELECT RSW_ROW_ID FROM RSW_WORKSHEETS WHERE CLAIM_ID= " + iClaimId + " AND CLAIMANT_ROW_ID= "
                        + iClaimantRowId + " AND UNIT_ROW_ID= " + iUnitRowId + " AND RSW_STATUS_CODE = "
                        + objCache.GetCodeId("AP", "RSW_STATUS") + " ORDER BY RSW_ROW_ID DESC ";    

                p_WorksheetResponse.ExistApproved = Conversion.ConvertObjToInt(objCon.ExecuteScalar(strSQL), base.ClientId);
              
               

                //for Rejected
                strSQL = "SELECT RSW_ROW_ID,RSWXMLTYPE,RSW_XML FROM RSW_WORKSHEETS WHERE CLAIM_ID= " + iClaimId + "AND CLAIMANT_ROW_ID= "
                        + iClaimantRowId + " AND UNIT_ROW_ID= " + iUnitRowId + " AND RSW_STATUS_CODE = "
                         + objCache.GetCodeId("RJ", "RSW_STATUS");

                p_WorksheetResponse.ExistRejected = Conversion.ConvertObjToInt(objCon.ExecuteScalar(strSQL), base.ClientId);
               
                
                //making SMS Security permission check, whether the current user is 
                // having the permission to update Reserve amount or not
                GetSecurityIDS(iClaimId,iClaimantRowId,iUnitRowId, 0);
                
                //checking View SMS Permission

                if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_VIEW)) 
                {
                   
                    throw new RMAppException("You don't have permission to complete requested operation.");
                }
                 //   throw new RMAppException(Globalization.GetString("ReserveWorksheet.Load.Err"));
             
                ////checking To Create New Worksheet SMS Permission
                //if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_CREATE))
                //    throw new RMAppException("You don't have permission to complete requested operation.");

                //checking To Update/Approve Worksheet SMS Permission
                if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                {
                    p_WorksheetResponse.IsSMSPermissionToUpdate = false; 
                }
                else
                {
                    p_WorksheetResponse.IsSMSPermissionToUpdate = true; 
                }

                //checking wheather current claim type is allowed to access this ReserveWorksheet
                if (IsClaimAllowedForWorksheet(iClaimId))
                {
                    p_WorksheetResponse.IsClaimAllowedToAccess = true;
                }
                else
                {
                    p_WorksheetResponse.IsClaimAllowedToAccess = false;
                }
                //shobhana 
                if (p_WorksheetRequest.RSWId > 0)
                {
                    strSQL = "SELECT RSW_XML,RSWXMLTYPE FROM RSW_WORKSHEETS WHERE CLAIM_ID= " + iClaimId + " AND CLAIMANT_ROW_ID= "
                       + iClaimantRowId + " AND  RSW_ROW_ID = " + p_WorksheetRequest.RSWId + " AND  UNIT_ROW_ID= " + iUnitRowId + "";

                
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSQL.ToString());
                    if (objReader.Read())
                    {
                        //sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                        //tmalhotra2 mits 27272
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWXmlType']") != null)
                        {
                            sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                        }
                        else
                        {
                            sRswTypeToLaunch = "Generic";
                        }

                        p_WorksheetResponse.RSWType = sRswTypeToLaunch;
                        objXMLOut.LoadXml(objReader.GetString("RSW_XML"));
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWXmlType']") != null)
                            sRswTypeToLaunch = objXMLOut.SelectSingleNode("//control[@name='hdnRSWXmlType']").InnerText;
                        else
                            sRswTypeToLaunch = "Generic";
                        if (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History")
                        {
                            if (!(sRswTypeToLaunch == "Generic" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History")))
                            {
                                if (!IsRSWAllowed(p_WorksheetRequest.RSWId, sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                                {
                                    if (sRswTypeToLaunch == "Customize" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History"))
                                    {

                                        p_WorksheetResponse.RSWType = "Customize";


                                    }
                                    //else

                                    //    throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                                }
                                else
                                    p_WorksheetResponse.RSWType = GetRswTypeToLaunch(iClaimId);

                            }
                            if (!(sRswTypeToLaunch == "Customize" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History")))
                            {
                                if (!IsRSWAllowed(p_WorksheetRequest.RSWId, sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXMLOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                                {
                                    if (sRswTypeToLaunch == "Generic" && (objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "Approved" || objXMLOut.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText == "History"))
                                    {

                                        p_WorksheetResponse.RSWType = "Generic";


                                    }
                                    //else

                                    //    throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                                }
                                else
                                    p_WorksheetResponse.RSWType = GetRswTypeToLaunch(iClaimId);
                            }
                        }
                    }
                   
                    else
                        p_WorksheetResponse.RSWType = GetRswTypeToLaunch(iClaimId);
                }
                else
                p_WorksheetResponse.RSWType = GetRswTypeToLaunch(iClaimId);
               // end
                //changed by shobhana
                AssignReserveWorksheet(p_WorksheetRequest.RSWReservesXML, p_WorksheetResponse.RSWType);
            
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            { 
                throw new RMAppException
                    (Globalization.GetString("ReserveWorksheet.OnLoadInfo.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
                objCache = null;
            }

        }

        ///<summary>
        /// This function saves the Reserve Worksheet: a New as well as an existing worksheet
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <returns>XML to be rendered on screen</returns>
        public override void SaveData(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
            int iRSWId = 0;
            int iStatusCode = 0;
            string sSQL = string.Empty;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            DbReader objReader = null;
            DbConnection objCon = null;
            XmlNode objNode = null;
            XmlDocument objXmlIn = null;
      
            long lUserMId = 0;
            long lClaimId = 0;
            long lClaimantRowId = 0;
            long lUnitRowId = 0;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;
            string sEditGrid = string.Empty; // Grid Names which can only be edited
            string sAddGrid = string.Empty; // Grid Names in which new nodes can be added.
            
            try
            {
                //Added by Shobhana
             
                
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
             
            
                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
                if (!IsRSWAllowed(p_WorksheetRequest.RSWId, p_WorksheetRequest.RSWType, Conversion.ConvertStrToInteger(objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                {
                    throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                }
                // Change the 'Last Modified By' and 'Last Modified Time'
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtLastMod']");
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                if (objNode != null)
                {
                    objNode.InnerText = DateTime.Now.ToString();
                    objNode.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
                }

                objNode = (XmlElement)objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']");
                if (objNode != null)
                    objNode.InnerText = m_CommonInfo.UserName;

                objNode = (XmlElement)objXmlIn.SelectSingleNode("//control[@name='hdnRSWXmlType']");
                if (objNode != null)
                    objNode.InnerText = p_WorksheetRequest.RSWType;
                //added by shobhana Mits 19027
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;
                objNode = (XmlElement)objXmlIn.SelectSingleNode("//control[@name='hdnPrevModValtoZero']");
                if (objNode != null)
                    objNode.InnerText = bPrevModValtToZero.ToString();

                
               


                #region To Get The Reserve Worksheet Supervisor

                sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                        + m_CommonInfo.UserId
                        + " AND USER_TABLE.MANAGER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                        + m_CommonInfo.DSNID;
                objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                if (objReader.Read())
                {
                    lUserMId = objReader.GetInt32("MANAGER_ID");
                    sSubmittedTo = objReader.GetString("LOGIN_NAME");
                }
                else
                {
                    lUserMId = m_CommonInfo.UserId;
                    sSubmittedTo = "";
                }
                objReader.Close();

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                if (objNode != null)
                {
                    objNode.InnerText = sSubmittedTo;
                }

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                if (objNode != null)
                {
                    objNode.InnerText = lUserMId.ToString();
                }

                if (lUserMId != 0)
                {
                    sSQL = "SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                            + lUserMId
                            + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                            + m_CommonInfo.DSNID;
                    objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                    if (objReader.Read())
                    {
                        sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                    }
                    else
                    {
                        sSupName = "";
                    }
                    objReader.Close();
                }

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSupName']");
                if (objNode != null)
                {
                    objNode.InnerText = sSupName;
                }

                #endregion

                iRSWId = Conversion.ConvertStrToInteger(objXmlIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                if (iRSWId == -1) // New worksheet
                {
                    #region New Reserve Worksheet

                    iStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                    // Change 'Request status' from New to Pending.
                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        objNode.InnerText = "Pending";
                        objNode.Attributes["codeid"].InnerText = iStatusCode.ToString();
                    }

                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                    if (objNode != null)
                        objNode.InnerText = "Editable";

                    lClaimId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText);
                    lClaimantRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimantRowId']").InnerText);
                    lUnitRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnUnitRowId']").InnerText);


                    //check if there is any existing reserve worksheet with status as Pending for this claim, then don't save
                    if (CheckPendingRSW(lClaimId, lClaimantRowId, lUnitRowId, objCache.GetCodeId("PE", "RSW_STATUS")))
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.PendingRSWFound",m_iClientId));
                    else
                        objXmlIn = SaveReserveWorksheet(objXmlIn);

                    #endregion
                }
                else // old worksheet
                {
                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");

                    if (objNode != null)
                    {
                        if (objNode.InnerText.IndexOf("Rejected") != -1) // If a 'rejected worksheet is being saved.
                        {
                            #region If a rejected worksheet is being saved

                            //int iNewStatusCode = 0;// objCache.GetCodeId("PE", "RSW_STATUS");

                            int iNewStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                            iStatusCode = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                            objNode.InnerText = "Pending";
                            objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                            // Change the date rejected and rejected by fields
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtAppRej']");
                            objNode.InnerText = "";
                            objNode.Attributes["dbformat"].InnerText = "";

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejBy']");
                            objNode.InnerText = "";

                            //added by Nitin for Rejected in R6
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']");
                            if (objNode != null)
                                objNode.InnerText = "";

                            //sachin 6385 starts
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']");
                            if (objNode != null)
                                objNode.InnerText = "";
                            //sachin 6385 ends

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "Editable";

                            // Update record in database table 'RSW_WORKSHEETS'
                            objXmlIn = SaveReserveWorksheet(objXmlIn);
                            SaveReserveWorksheetHistory(objXmlIn);

                            #endregion
                        }
                        else // If an existing (non-rejected) worksheet is being saved
                        {
                            #region If an existing (non-rejected) worksheet is being saved

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "Editable";

                            // Update record in database table 'RSW_WORKSHEETS'
                            objXmlIn = SaveReserveWorksheet(objXmlIn);
                            SaveReserveWorksheetHistory(objXmlIn);

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            p_WorksheetResponse.OutputXmlString = objXmlIn.OuterXml;
            return;
        }

        ///<summary>
        /// This function saves the Reserve Worksheet: a New as well as an existing worksheet
        /// It is similar to SaveData in impelmentation except that it performs additional security check 
        /// for UPDATE permissions.
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <returns>XML to be rendered on screen</returns>
        /// added by aaggarwal29 -- for MITS 27763
        public override void SaveDataWithUserLogin(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, UserLogin pUserLogin)
        {
            int iRSWId = 0;
            int iStatusCode = 0;
            int iClaimId =0;
            int iClaimantRowId=0;
            int iUnitRowId=0;
            string sSQL = string.Empty;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            DbReader objReader = null;
            DbConnection objCon = null;
            XmlNode objNode = null;
            XmlDocument objXmlIn = null;

            long lUserMId = 0;
            long lClaimId = 0;
            long lClaimantRowId = 0;
            long lUnitRowId = 0;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;
            string sEditGrid = string.Empty; // Grid Names which can only be edited
            string sAddGrid = string.Empty; // Grid Names in which new nodes can be added.

            try
            {
                //Added by Shobhana


                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);


                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
                if (!IsRSWAllowed(p_WorksheetRequest.RSWId, p_WorksheetRequest.RSWType, Conversion.ConvertStrToInteger(objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                {
                    throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                }
                // Change the 'Last Modified By' and 'Last Modified Time'
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtLastMod']");
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                if (objNode != null)
                {
                    objNode.InnerText = DateTime.Now.ToString();
                    objNode.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
                }

                objNode = (XmlElement)objXmlIn.SelectSingleNode("//control[@name='hdnLastModBy']");
                if (objNode != null)
                    objNode.InnerText = m_CommonInfo.UserName;

                objNode = (XmlElement)objXmlIn.SelectSingleNode("//control[@name='hdnRSWXmlType']");
                if (objNode != null)
                    objNode.InnerText = p_WorksheetRequest.RSWType;
                //added by shobhana Mits 19027
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;
                objNode = (XmlElement)objXmlIn.SelectSingleNode("//control[@name='hdnPrevModValtoZero']");
                if (objNode != null)
                    objNode.InnerText = bPrevModValtToZero.ToString();





                #region To Get The Reserve Worksheet Supervisor

                sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                        + m_CommonInfo.UserId
                        + " AND USER_TABLE.MANAGER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                        + m_CommonInfo.DSNID;
                objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                if (objReader.Read())
                {
                    lUserMId = objReader.GetInt32("MANAGER_ID");
                    sSubmittedTo = objReader.GetString("LOGIN_NAME");
                }
                else
                {
                    lUserMId = m_CommonInfo.UserId;
                    sSubmittedTo = "";
                }
                objReader.Close();

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                if (objNode != null)
                {
                    objNode.InnerText = sSubmittedTo;
                }

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                if (objNode != null)
                {
                    objNode.InnerText = lUserMId.ToString();
                }

                if (lUserMId != 0)
                {
                    sSQL = "SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                            + lUserMId
                            + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                            + m_CommonInfo.DSNID;
                    objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                    if (objReader.Read())
                    {
                        sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                    }
                    else
                    {
                        sSupName = "";
                    }
                    objReader.Close();
                }

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnSupName']");
                if (objNode != null)
                {
                    objNode.InnerText = sSupName;
                }

                #endregion

                iRSWId = Conversion.ConvertStrToInteger(objXmlIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                //Get Claim ID
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']");
                if (objNode != null)
                    iClaimId = Conversion.ConvertStrToInteger(objNode.InnerText);

                //Get ClaimantRowId
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnClaimantRowId']");
                if (objNode != null)
                    iClaimantRowId = Conversion.ConvertStrToInteger(objNode.InnerText);

                //Get UnitRowId
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnitRowId']");
                if (objNode != null)
                    iUnitRowId = Conversion.ConvertStrToInteger(objNode.InnerText);

                if (iRSWId == -1) // New worksheet
                {
                    #region New Reserve Worksheet

                    iStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                    // Change 'Request status' from New to Pending.
                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        objNode.InnerText = "Pending";
                        objNode.Attributes["codeid"].InnerText = iStatusCode.ToString();
                    }

                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                    if (objNode != null)
                        objNode.InnerText = "Editable";

                    lClaimId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText);
                    lClaimantRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnClaimantRowId']").InnerText);
                    lUnitRowId = Riskmaster.Common.Conversion.ConvertStrToLong(objXmlIn.SelectSingleNode("//control[@name='hdnUnitRowId']").InnerText);


                    //check if there is any existing reserve worksheet with status as Pending for this claim, then don't save
                    if (CheckPendingRSW(lClaimId, lClaimantRowId, lUnitRowId, objCache.GetCodeId("PE", "RSW_STATUS")))
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.PendingRSWFound",m_iClientId));
                    else
                        objXmlIn = SaveReserveWorksheet(objXmlIn);

                    #endregion
                }
                else // old worksheet
                {
                    //aaggarwal29 MITS -- start

                    GetSecurityIDS(iClaimId, iClaimantRowId, iUnitRowId, 0);
                    if (!pUserLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                    {
                        throw new RMAppException("You don't have permission to complete requested operation.");
                    }
                    //aaggarwal29 MITS --end
                    objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");

                    if (objNode != null)
                    {
                        if (objNode.InnerText.IndexOf("Rejected") != -1) // If a 'rejected worksheet is being saved.
                        {
                            #region If a rejected worksheet is being saved

                            //int iNewStatusCode = 0;// objCache.GetCodeId("PE", "RSW_STATUS");

                            int iNewStatusCode = objCache.GetCodeId("PE", "RSW_STATUS");
                            iStatusCode = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                            objNode.InnerText = "Pending";
                            objNode.Attributes["codeid"].InnerText = iNewStatusCode.ToString();

                            // Change the date rejected and rejected by fields
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdndtAppRej']");
                            objNode.InnerText = "";
                            objNode.Attributes["dbformat"].InnerText = "";

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejBy']");
                            objNode.InnerText = "";

                            //added by Nitin for Rejected in R6
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderLimit']");
                            if (objNode != null)
                                objNode.InnerText = "";

                            //sachin 6385
                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']");
                            if (objNode != null)
                                objNode.InnerText = "";
                            //sachin 6385

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "Editable";

                            // Update record in database table 'RSW_WORKSHEETS'
                            objXmlIn = SaveReserveWorksheet(objXmlIn);
                            SaveReserveWorksheetHistory(objXmlIn);

                            #endregion
                        }
                        else // If an existing (non-rejected) worksheet is being saved
                        {
                            #region If an existing (non-rejected) worksheet is being saved

                            objNode = objXmlIn.SelectSingleNode("//control[@name='hdnMode']");
                            if (objNode != null)
                                objNode.InnerText = "Editable";

                            // Update record in database table 'RSW_WORKSHEETS'
                            objXmlIn = SaveReserveWorksheet(objXmlIn);
                            SaveReserveWorksheetHistory(objXmlIn);

                            #endregion
                        }
                    }
                }
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveData.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            p_WorksheetResponse.OutputXmlString = objXmlIn.OuterXml;
            return;
        }
        // methodadded by aaggarwal29 -- end

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <param name="p_objXMLOut"></param>
        public override void DeleteWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
            LocalCache objCache = null;
            StringBuilder sbSQL = null;
            int iStatusCode = 0;
            int iRSWID = 0;
            DbConnection objConn = null;
            string sReason = "";
            int iRSWHistId = 0;
            int iApproverID = 0;
            
            try
            {
                //Added by Nitin for R6
                iRSWID =  p_WorksheetRequest.RSWId;
                if(iRSWID == 0)
                {
                    throw new RMAppException("ReserveWorksheetId to be deleted is not found");
                }
                
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                iStatusCode = objCache.GetCodeId("DE", "RSW_STATUS");
                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objConn.Open();

                // Update the db table RSW_WORKSHEETS to set the status equal to deleted
                sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                sbSQL.Append(", UPDATED_BY_USER = " + Utilities.FormatSqlFieldValue(m_CommonInfo.UserName));
                sbSQL.Append(", DTTM_RCD_LAST_UPD = '" + Conversion.GetDateTime(DateTime.Now.ToString()));
                sbSQL.Append("' WHERE RSW_ROW_ID = " + iRSWID.ToString());
                objConn.ExecuteNonQuery(sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT APPROVER_ID FROM RSV_APPROVAL_HIST WHERE RSW_ROW_ID = " + iRSWID);
                sbSQL.Append(" AND RSW_HIST_ROW_ID IN (SELECT MAX(RSW_HIST_ROW_ID) FROM RSV_APPROVAL_HIST");
                sbSQL.Append(" WHERE RSW_ROW_ID = " + iRSWID + ")");
                iApproverID = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                sbSQL.Remove(0, sbSQL.Length);
    
                #region Reserve Approval History

                iRSWHistId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSV_APPROVAL_HIST", base.ClientId);
                // Insert into database table 'RSV_APPROVAL_HIST' new record
                sbSQL.Append("INSERT INTO RSV_APPROVAL_HIST (RSW_HIST_ROW_ID, RSW_ROW_ID, RSW_STATUS_CODE, REASON, APPROVER_ID, ");
                sbSQL.Append("CHANGED_BY_USER, DTTM_APPROVAL_CHGD) VALUES (");
                sbSQL.Append(iRSWHistId.ToString() + ", ");
                sbSQL.Append(iRSWID.ToString() + ", ");
                sbSQL.Append(iStatusCode.ToString() + ", ");
                sbSQL.Append(Utilities.FormatSqlFieldValue(sReason) + ", ");
                sbSQL.Append(iApproverID.ToString() + ", ");
                sbSQL.Append(Utilities.FormatSqlFieldValue(m_CommonInfo.UserName) + ", '");
                sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "')");

                objConn.ExecuteNonQuery(sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                
                #endregion

                p_WorksheetResponse.Status = true; 
             }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.DeleteWorksheet.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn = null;
                }
            }
        }
      //changed by shobhana for customization 
        private void UpdateRSWTransAmount(XmlDocument p_objXML, ReserveFunds.structReserves[] arrReserves, string p_sReserveCustomXML,int p_iclaimCurrCode)
        {//MGaba2:r6:Added to make an entry in table used for reporting purpose
            StringBuilder sbSql =null;
            XmlDocument XMLReserve = null;
            XmlNode objNode = null;
            bool bFlag = false;
          
            string sRSWID = string.Empty;
            string strCtrlName = string.Empty;
            DbConnection objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
            DbCommand objDBCom = null;
            DbTransaction objDbTrans = null;
            int iReason = 0;
            //skhare7 R8 
            SysParms objSysSetting = null; 

            try
            {
                //Added by Nitin to implement transasction 
                sbSql = new StringBuilder();
                XMLReserve = new XmlDocument();
                //skhare7 r8
                objSysSetting = new SysParms(m_CommonInfo.Connectionstring, m_iClientId);
                double dblClaimToBaseExcRate = CommonFunctions.ExchangeRateSrc2Dest(p_iclaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, m_CommonInfo.Connectionstring, base.ClientId);
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnRSWid']");
                if (objNode != null)
                {
                    sRSWID = objNode.InnerText.Trim();
                }
                
                objConn.Open();
                
                objDbTrans = objConn.BeginTransaction();
                objDBCom = objConn.CreateCommand();
                objDBCom.Transaction = objDbTrans;

                //Before inserting records for this RSWId ,first deleting TRANS AMOUNT ,to avoid multiple entries and primary key voilation
                sbSql.Append("DELETE FROM RSW_TRANS_AMOUNT WHERE RSW_ROW_ID = " + sRSWID);
                objDBCom.CommandText = sbSql.ToString();
                objDBCom.ExecuteNonQuery();
                sbSql.Remove(0, sbSql.Length);

                //Before inserting records for this RSWId ,first deleting COMMENTS ,to avoid multiple entries and primary key voilation
                sbSql.Append("DELETE FROM RSW_COMMENTS WHERE RSW_ROW_ID = " + sRSWID);
                objDBCom.CommandText = sbSql.ToString();
                objDBCom.ExecuteNonQuery();
                sbSql.Remove(0, sbSql.Length);

                //Now inserting data in RSW_TRANS_AMOUNT  and COMMENTS table
                
                for (int iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                {
               

                     XmlNodeList nodes = p_objXML.SelectNodes("//group");
                     if (nodes.Count > 0)
                     {
                     //   // arrReserves = new ReserveFunds.structReserves[nodes.Count - 1];
                       for (int i = 0; i <= nodes.Count - 2; i++)
                      {
                          if (nodes[i].Attributes["name"].Value != "RESERVESUMMARY")
                        {
                                string[] arrReserveXML = null;
                            arrReserveXML = nodes[i].Attributes["name"].Value.ToString().Split('_');
                            if (arrReserveXML[1] == arrReserves[iCnt].iReserveTypeCode.ToString())
                            {
                                foreach (XmlNode objDisplayColumn in nodes[i].SelectNodes(".//displaycolumn"))
                                {
                                    foreach (XmlNode objControl in objDisplayColumn.SelectNodes("./control"))
                                    {

                                        if (objControl.Attributes["name"].Value.StartsWith("lbl_ReserveType_" + arrReserves[iCnt].iReserveTypeCode.ToString() + "_TransType_"))
                                        {
                                            if (!objControl.Attributes["name"].Value.EndsWith("OtherAdjustment"))
                                            // string sReplaceValue = "lbl_ReserveType_" + arrReserves[iCnt].iReserveTypeCode.ToString() + "_TransType";
                                            {
                                                int sTransCode = Conversion.CastToType<int>((objControl.Attributes["name"].Value.ToString().Replace(("lbl_ReserveType_" + arrReserves[iCnt].iReserveTypeCode.ToString() + "_TransType_"), "")), out bFlag);
                                                string sTransDesc = objControl.Attributes["title"].Value.ToString();// transdesc

                                                sbSql.Append("INSERT INTO RSW_TRANS_AMOUNT(RSW_ROW_ID,RESERVE_TYPE_CODE,TRANS_TYPE,TRANS_DESC,RES_BALANCE,AMOUNT,PAID_TOTAL) VALUES(");
                                                sbSql.Append(sRSWID + ",");
                                                sbSql.Append(arrReserves[iCnt].iReserveTypeCode + ",");
                                                sbSql.Append(sTransCode + ",");
                                                sbSql.Append(Utilities.FormatSqlFieldValue(sTransDesc) + ",");

                                                strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_TransTypeNewReserveBal_" + sTransCode.ToString();
                                                objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                                                if (objNode != null)
						                        {//skhare7 r8 Multicurrency
						                            //sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText) + ",");
                                                  sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText)*dblClaimToBaseExcRate) + ",");
                                                    
						                        }
						                        else
						                        {
						                            sbSql.Append("0.0,");
						                        }
                                                strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_TransTypeIncurredAmount_" + sTransCode.ToString();
                                                objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                                                if (objNode != null)
						                        {
                                                    sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText) * dblClaimToBaseExcRate) + ",");
						                           // sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText) + ",");
						                        }
						                        else
						                        {
						                            sbSql.Append("0.0,");
						                        }
                                                strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_Paid_" + sTransCode.ToString();
                                                objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                                                if (objNode != null)
						                        {
                                                    sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText) * dblClaimToBaseExcRate) );
						                           // sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText));
						                        }
						                        else
						                        {
                                                    sbSql.Append("0.0");
						                        }
                                                sbSql.Append(")");

                                                objDBCom.CommandText = sbSql.ToString();
                                                objDBCom.ExecuteNonQuery();
                                                sbSql.Remove(0, sbSql.Length);

                                            }
                                        }

                                    }

                                }
                              
                                }


                            }
                         }

                     
                 
                 }
                

                    //Added by Nitin One more row for Other TransType
                    sbSql.Append("INSERT INTO RSW_TRANS_AMOUNT(RSW_ROW_ID,RESERVE_TYPE_CODE,TRANS_TYPE,TRANS_DESC,RES_BALANCE,AMOUNT,PAID_TOTAL) VALUES(");
                    sbSql.Append(sRSWID + ",");
                    sbSql.Append(arrReserves[iCnt].iReserveTypeCode + ",");
                    sbSql.Append("-1,'Other Adjustments',");
                    strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_TransTypeIncurredAmount_OtherAdjustment";
                    objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                    if (objNode != null)
                    {
                        sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText) * dblClaimToBaseExcRate) );
                      //  sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText));
                        sbSql.Append(",");
                        sbSql.Append((Conversion.ConvertStrToDouble(objNode.InnerText) * dblClaimToBaseExcRate) );
                        //sbSql.Append(Conversion.ConvertStrToDouble(objNode.InnerText));
                    }
                    else
                    {
                        sbSql.Append("0, 0");
                    }

                    sbSql.Append(",0)");
                    objDBCom.CommandText = sbSql.ToString();
                    objDBCom.ExecuteNonQuery();
                    sbSql.Remove(0, sbSql.Length);

                    //now inserting the comments
                    strCtrlName = "txt_ReserveType_" + arrReserves[iCnt].iReserveTypeCode + "_ReserveWorksheetReason";
                    objNode = p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']");
                    if (objNode != null)
                    {
                        if (objNode.Attributes["reasoncode"] != null)
                            if (objNode.Attributes["reasoncode"].Value != "")
                                iReason = Convert.ToInt32(objNode.Attributes["reasoncode"].Value);
                    }
                    sbSql.Append("INSERT INTO RSW_COMMENTS(RSW_ROW_ID,RESERVE_TYPE_CODE,COMMENTS,REASON_CODE) VALUES(");
                    sbSql.Append(sRSWID + ",");
                    sbSql.Append(arrReserves[iCnt].iReserveTypeCode + ",");
                    sbSql.Append("'" + p_objXML.SelectSingleNode("//control[@name='" + strCtrlName + "']").InnerText + "',");
                    sbSql.Append(iReason + ")");

                    objDBCom.CommandText = sbSql.ToString();
                    objDBCom.ExecuteNonQuery();
                    sbSql.Remove(0, sbSql.Length);
                }
                objDbTrans.Commit(); 
            }
            catch (RMAppException p_objExp)
            {
                objDbTrans.Rollback();
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                objDbTrans.Rollback();
                throw new RMAppException
                    (Globalization.GetString("ReserveWorksheet.UpdateRSWTransAmount.Error", m_iClientId), p_objException);
            }
            finally
            {
                objConn.Close();
                objConn = null;
                objDBCom = null;
             
            }
        }

      /// <summary>
      /// MGaba2:Updating Summary comments for reporting purpose in Rsw_worksheets
      /// </summary>
      /// <param name="p_objXML"></param>
        private void UpdateSummaryComments(XmlDocument p_objXML)
        {

            StringBuilder sbSql = new StringBuilder();
            XmlNode objNode = null;
            string sRSWID = string.Empty;
            DbConnection objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
            DbCommand objDBCom = null;
            DbParameter objDBParam = null;

            try
            {
                //MGaba2:Inserting Reserve Summary Comments for Reporting Purposes
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnRSWid']");
                if (objNode != null)
                {
                    sRSWID = objNode.InnerText.Trim();
                }

                objConn.Open();
                objDBCom = objConn.CreateCommand();
                objDBParam = objDBCom.CreateParameter();

                objDBParam.ParameterName = "SummaryComments";
                objDBParam.SourceColumn = "SUMMARY_COMMENTS";
                objDBParam.Direction = ParameterDirection.Input;
                objDBParam.Value = "";
                objNode = p_objXML.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']");
                if (objNode != null)
                {
                    objDBParam.Value = objNode.InnerText;
                }

                objDBCom.Parameters.Add(objDBParam);

                sbSql.Append("UPDATE RSW_WORKSHEETS SET SUMMARY_COMMENTS=~SummaryComments~ WHERE RSW_ROW_ID=");
                sbSql.Append(sRSWID);

                objDBCom.CommandText = sbSql.ToString();
                objDBCom.ExecuteNonQuery();
                sbSql.Remove(0, sbSql.Length);
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                objConn = null;
                objDBCom = null;
                objDBParam = null;            
            }
        }


        /// <summary>
        /// MGaba2:r6:Functioning of Approve Button:Start
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <param name="p_objXMLOut"></param>
        public override void ApproveReserveWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            string sRSWID = string.Empty;
            StringBuilder sbSQL = null;
            DbReader objRdr = null;
            XmlNode objNode = null;
            int iClaimId = 0;
            DataModelFactory objDataModelFactory = null;
            Claim objClaim = null;
            ReserveFunds.structReserves[] arrReserves = null;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            bool bUnderLimit = false;
            bool bUnderClaimIncLimit = false;//sachin 6385
            XmlDocument objXmlOut = null;
             string sSubmittedTo = string.Empty;
             string sRswStatus = string.Empty;
            string sRSWDateTime = string.Empty;
            bool bDaysApprovalLapsed = false;
            int iSubmToID = 0;
            ReserveCurrent objFunds = null; // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes.
            string sClaimNum = "";
            string sDateEntered = "";
            LocalCache objCache = null;
            int iCnt;
            long[,] larrayResCurrent = null;
            long[,] larrayResHistory = null;
            int iReserveTypeCode = 0;
            double dAmount = 0;
             string sReason = "";
            string sAmt = "";
            DbConnection objCon = null;
            int iClaimantEID = 0;
            int iUnitID = 0;            
            int iStatusCodeID = 0;
             bool bResAdjusted = false;
            string sLastModOn = string.Empty;
            string sSubmittedBy = string.Empty;
            string sAddedByUser = string.Empty;
            //Priya Aggarwal: Document Imaging for Reserve Worksheet - start
            DbTransaction objDbTransaction = null;
            DbCommand cmd = null;
            string fileName = string.Empty;
            int documentId = 0;
            //Priya Aggarwal: Document Imaging for Reserve Worksheet - end
            //MGaba2:R6:Start
            XmlDocument objXmlIn = null;
            ReserveWorksheetRequest objRSWReqForPrint = null;
            ReserveWorksheetResponse objRSWResForPrint = null;
            XmlDocument objXmlInForPrint = null;
            XmlDocument objXmlOutForPrint = null;
            XmlElement objTempElement = null;
            string sRSWStatus = string.Empty;
            string sReservesWithinLimit = "No";
            string sClaimIncWithinLimit = "No";//sachin 6385
            int iAttachRSW = 0;
            int iReserveTrackingLevel = 0;
            string sTableNameForAttach = "CLAIM";
            int iRecordIdForAttach = 0;
            int iClaimantRowId = 0;
            int iUnitRowId = 0;
            string sRswTypeToLaunch = string.Empty;
            //MGaba2:R6:End
            //skhare7 R8 
            int iClaimCurrCode = 0;
            //Aman MITS 27915
            double dExhClaimRateToBase = 0.0;
            SysSettings objSysSetting = null;
            string strHoldReason = string.Empty;
            string StrReason = string.Empty;
            try
            {
                sbSQL = new StringBuilder();
                objXmlOut = new XmlDocument();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString); 

                #region Getting ClaimantId
                //Getting ClaimId
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnClaimId']");
                if (objNode != null)
                    iClaimId = Conversion.ConvertStrToInteger(objNode.InnerText);
                #endregion

                #region Getting Claimant and Unit id
                //MGaba2:Changing from hdnClaimantId to hdnClaimantRowId
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnClaimantRowId']");
                if (objNode != null && objNode.InnerText != "0" && objNode.InnerText != "")
                {
                    iClaimantRowId = Convert.ToInt32(objNode.InnerText);
                    GetClaimantId(iClaimantRowId, ref iClaimantEID);
                }

                //Added by Nitin for Mits 18480
                if (iClaimantRowId == 0)  //Fetch ClaimantRowId for WC and NonOcc Claim
                    GetClaimantEIdForWC(iClaimId, ref iClaimantEID);

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnUnitRowId']");
                if (objNode != null && objNode.InnerText != "0" && objNode.InnerText != "")
                {
                    iUnitRowId = Convert.ToInt32(objNode.InnerText);
                    GetUnitId(iUnitRowId, ref iUnitID);
                }
                #endregion

                //sharishkumar Jira 6385
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnActionType']");
                if (objNode != null)
                {
                    objNode.InnerText = "Approve";
                }
                //sharishkumar Jitra 6385

                #region SMS Security permission check
                //making SMS Security permission check, whether the current user is 
                // having the permission to update Reserve amount or not
                GetSecurityIDS(iClaimId,iClaimantEID,iUnitID, 0);
                
                //checking To Update/Approve Worksheet SMS Permission
                if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                    throw new RMAppException("You don't have permission to complete requested operation.");
                #endregion

                //Check if Claim is closed,then Worksheed cann't be Saved
                if (IsClosedClaim(iClaimId))
                    throw new RMAppException("ReserveWorksheet can not be Approved for Closed Claim.");


                #region Checking for Status and then accordingly submitting it
                //MGaba2:Shifetd Logic from business layer.
                //In case status is other than PA,worksheet will be submitted first
                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                if (objNode != null)
                {
                    sRSWStatus = objNode.InnerText.Trim();
                }


                p_WorksheetRequest.InputXmlString = objXmlIn.InnerXml.ToString();
                //It will go for Submit first in case status of reserve worksheet is Pending or Rejected
                //And after submit,if reserves set are in limits then only it will go for approve
                //rsushilaggar MITS 26332 Date 12/27/2011
                if (sRSWStatus != "Pending Approval" || sRSWStatus != "Pending")
                {
                    SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                    objXmlOut.LoadXml(p_WorksheetResponse.OutputXmlString);
                    
                    if (objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                    {
                        sReservesWithinLimit = objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                    }

                    //sachin 6385
                    if (objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                    {
                        sClaimIncWithinLimit = objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText;
                    }
                    //sachin 6385

                    //added by Nitin for Mits 18882
                    objNode = objXmlOut.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        sRSWStatus = objNode.InnerText.Trim();
                    }

                    //added by Nitin for Mits 18882
                    //rsushilaggar MITS 26332 Date 12/27/2011
                    //if ((sRSWStatus == "Pending Approval" || sRSWStatus != "Pending") && sReservesWithinLimit == "No")
                    //{
                    //    p_WorksheetResponse.OutputXmlString = objXmlOut.OuterXml;
                    //    return;
                    //}


                    objXmlOut = null;
                    objXmlOut = new XmlDocument();
                }

                //If reserve worksheet is in pending approval status
                //or if it is in pending/rejected and reserve changes satisfies the limits
                //then it will go for approval

                //sachin 6385 starts
                //if (sRSWStatus != "Pending Approval" && sReservesWithinLimit == "No")
                 if (sRSWStatus != "Pending Approval" && (sReservesWithinLimit == "No" || sClaimIncWithinLimit =="No"))
                //sachin 6385 ends
                {
                    //added by Nitin for Mits 18882
                    p_WorksheetResponse.OutputXmlString = objXmlOut.OuterXml;
                    return;
                }
                #endregion
                #region Fetching Reserve WorkSheet from Database

                objNode = objXmlIn.SelectSingleNode("//control[@name='hdnRSWid']");
                if (objNode != null)
                {
                    sRSWID = objNode.InnerText.Trim();
                }


                if (sRSWID != "")
                {
                    sbSQL.Append("SELECT RSW_XML,SUBMITTED_TO,RSW_STATUS_CODE,DTTM_RCD_LAST_UPD,SUBMITTED_BY,ADDED_BY_USER,RSWXMLTYPE FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID);
                    objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objRdr.Read())
                    {
                        objXmlOut.LoadXml(objRdr.GetString("RSW_XML"));
                        //Following variables are used later
                        sSubmittedTo = objRdr.GetString("SUBMITTED_TO");
                        sRswStatus = objRdr.GetInt32("RSW_STATUS_CODE").ToString();
                        sRSWDateTime = objRdr.GetString("DTTM_RCD_LAST_UPD");
                        sRswTypeToLaunch = objRdr.GetString("RSWXMLTYPE");
                        sSubmittedBy = objRdr.GetString("SUBMITTED_BY");
                        sAddedByUser = objRdr.GetString("ADDED_BY_USER");

                        if (!IsRSWAllowed(Convert.ToInt32(sRSWID), sRswTypeToLaunch, Conversion.ConvertStrToInteger(objXmlOut.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                        {
                            throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                        }
                        objRdr.Close();
                    }
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));

                #endregion
                    #region Submiting for checking limits
                    //rsushilaggar MITS 26332 Date 12/27/2011
                    //sachin 6385 starts
                    if ((sRSWStatus == "Pending Approval" || sRSWStatus != "Pending") && (sReservesWithinLimit == "No" || sClaimIncWithinLimit == "No"))
                    //sachin 6385 ends
                    {
                        //added by Nitin for Mits 18882
                        p_WorksheetResponse.OutputXmlString = objXmlOut.OuterXml;
                        if (sSubmittedBy != m_CommonInfo.UserId.ToString())
                        {
                            throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove", m_iClientId));
                        }
                        else
                        {
                            if (sReservesWithinLimit == "NoAuthSubmit")
                            {
                                if (sReservesWithinLimit == "No")
                                {
                                    StrReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                                }
                                if (sClaimIncWithinLimit == "No")
                                {
                                    if (StrReason == "")
                                        StrReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                                    else
                                        StrReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId)) + " and " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                                }
                                throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove", m_iClientId) + " as " + StrReason);
                            }
                        }
                            //return;
                        
                    }

                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(iClaimId);

                    //iClaimantEID = objClaim.PrimaryClaimant.ClaimantEid;
                    iClmTypeCode = objClaim.ClaimTypeCode;
                    iLOBCode = objClaim.LineOfBusCode;
                    iClaimCurrCode = objClaim.CurrencyType;

                    //Check Reserve Limits Enabled/Disabled
                    GetLOBReserveOptions(iClaimId);

                    //Get Supervisory Approval Settings
                    ReadRSWCheckOptions();

                    string[,] sResFinal = null;
                    //chaged by shobhana for customization of reserves
                    arrReserves = GetReservesTypeCustom(iLOBCode, iClmTypeCode, p_WorksheetRequest.RSWReservesXML, objXmlOut);

                  //  arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                    sResFinal = new string[arrReserves.GetLength(0), 4];

                    sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objXmlOut, iClaimCurrCode);

                    if (m_bResLimitFlag) //If the Flag is 'true' then check the reserve limits
                    {

                        bUnderLimit = UnderUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves, iClaimCurrCode);

                        if (!bUnderLimit && m_iDaysAppReserves > 0)
                        {
                            int iTopID = 0;
                            sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                            objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                            sbSQL.Remove(0, sbSQL.Length);
                            if (objRdr.Read())
                                iTopID = objRdr.GetInt("USER_ID");
                            objRdr.Close();

                            if (iTopID != m_CommonInfo.UserId)
                            {
                                //Check if Days for Approval has lapsed and set the bUnderLimit flag to false
                                //so that the reserve worksheet goes to the next level supervisor
                                if (sRSWDateTime != string.Empty)
                                {
                                    bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, m_CommonInfo.UserId, m_CommonInfo.UserId, iClaimId);
                                }

                                if (bDaysApprovalLapsed)
                                    bUnderLimit = false;
                            }
                        }
                    }
                    else
                        bUnderLimit = true;

                    //sachin 6385 starts
                    if (m_bClaimIncLimitFlag)
                    {
                        bUnderClaimIncLimit = UnderClaimIncUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves, iClaimCurrCode);
                    }
                    else
                        bUnderClaimIncLimit = true;
                    //sachin 6385 ends

                    //He cannot approve worksheet submitted by sub ordinate
                    //sachin 6385 starts
                    // if (!bUnderLimit) // If the limit is above the user's limit
                    if (!bUnderLimit && !bUnderClaimIncLimit)
                    //sachin 6385 ends
                    {
                        strHoldReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId)) + " and " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove", m_iClientId) + " as " + strHoldReason);
                    }
                    #endregion

                    //sachin 6385 starts
                    //if (bUnderLimit)
                    if (bUnderLimit && bUnderClaimIncLimit)
                    //sachin 6385 ends
                    {
                        // When a 'Pending Approval' worksheet is sent for approval.
                        //MGaba2:To do..confirm from mohit,why do we need this?
                        //if (p_sCallOn == "Pending Approval")
                        //{
                        // Check whether the logged-in user is part of the managerial chain.
                        // Check 'submitted to' field for the worksheet and go on ascending till you find the user.
                        // If not found, return that the user can not approve.

                        objNode = objXmlOut.SelectSingleNode("//control[@name='hdnRSWStatus']");
                        if (objNode != null)
                            if (objNode.Attributes["codeid"].InnerText != "")
                                if (sRswStatus != objNode.Attributes["codeid"].Value)
                                {
                                    objNode.InnerText = "";
                                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.DataChanged",m_iClientId));
                                }

                        #region Checking Authorized approver
                        iSubmToID = Riskmaster.Common.Conversion.ConvertStrToInteger(sSubmittedTo);
                    //rsushilaggar MITS 26332 Date 12/27/2011
                    if (iSubmToID != 0 && m_bUseSupAppReserves)
                        {
                            if (m_CommonInfo.UserId != iSubmToID) //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                            {
                                int iLevel = 1;
                                iLevel = GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId, iClaimId);
                            bool bSameSmsGroup = false;
                            if (iLevel == 0)
                            {
                                if (m_bAllowAccessGrpSup)
                                {
                                    bSameSmsGroup = CheckUserGroup(iSubmToID, m_CommonInfo.UserId, p_iGroupId, Conversion.ConvertStrToInteger(sRSWID));
                                }
                                if (!bSameSmsGroup)
                                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove",m_iClientId));
                            }
                        }
                        }

                        #endregion

                        //If reached here, then the user has the authority to approve the reserves entered.
                        //Modify the reserves. Call the ModifyReserves() function of Funds Adaptor.
                        //objNode = objXmlOut.SelectSingleNode("//control[@name='hdnClaimNum']");

                        objNode = objXmlOut.SelectSingleNode("//control[@name='hdnClaimNumber']");
                        if (objNode != null)
                            sClaimNum = objNode.InnerText;

                        objDataModelFactory = new DataModelFactory(p_objLogin, m_iClientId);

                        sDateEntered = DateTime.Now.ToShortDateString();

                        #region Getting New Reserves

                        int iReserveType = 0;
                        string sReserveDesc = "";
                        string sGPName = "";
                        string sLOB = "";
                        //string sCtrlMName = "";

                        sResFinal = new string[arrReserves.GetLength(0), 4];
                        sLOB = objCache.GetShortCode(iLOBCode);

                        for (iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                        {
                            iReserveType = arrReserves[iCnt].iReserveTypeCode;
                            sReserveDesc = arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "");
                            //sGPName = "//group[@name='" + sReserveDesc + sLOB + "']";
                            sGPName = "//group[@name='ReserveType_" + iReserveType + "']";
                            if (objXmlOut.SelectSingleNode(sGPName) != null)
                            {
                                //sCtrlMName = sReserveDesc.Substring(0, 3) + sLOB; //MITS 31576 srajindersin 02/15/2013 this variable is not used

                                sResFinal[iCnt, 0] = iReserveType.ToString();

                                //objNode = objXmlOut.SelectSingleNode("//control[@name='ResReason" + sCtrlMName + "']");
                                objNode = objXmlOut.SelectSingleNode("//control[@name='txt_ReserveType_" + iReserveType + "_ReserveWorksheetReason']");
                                if (objNode != null)
                                    sResFinal[iCnt, 1] = objNode.InnerText.ToString();

                                //objNode = objXmlOut.SelectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']");
                                objNode = objXmlOut.SelectSingleNode("//control[@name='hdn_ReserveType_" + iReserveType + "_NEWTOTALINCURRED']");
                                if (objNode != null)
                                    sResFinal[iCnt, 2] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNode.InnerText).ToString();
                            }
                        }
                        #endregion


                        #region Entering reserves for claims

                        larrayResCurrent = new long[arrReserves.GetLength(0), 2];
                        larrayResHistory = new long[arrReserves.GetLength(0), 2];
                        //Aman MITS 27915 --start
                        
                        //objSysSetting = new SysParms(m_CommonInfo.Connectionstring);
                        objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);
                        dExhClaimRateToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.BaseCurrencyType, m_CommonInfo.Connectionstring, base.ClientId);
                        //Aman MITS 27915 --end
                        for (iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                        {
                            larrayResCurrent[iCnt, 0] = arrReserves[iCnt].iReserveTypeCode;
                            larrayResHistory[iCnt, 0] = arrReserves[iCnt].iReserveTypeCode;

                            for (int iCount = 0; iCount < sResFinal.GetLength(0); iCount++)
                            {
                                # region Updating Reserves
                                iReserveTypeCode = Riskmaster.Common.Conversion.ConvertStrToInteger(sResFinal[iCount, 0]);
                                sReason = sResFinal[iCount, 1];
                                sAmt = sResFinal[iCount, 2];
                                dAmount = Conversion.ConvertStrToDouble(sAmt) * dExhClaimRateToBase;  //Aman MITS 27915

                                if (iReserveTypeCode == arrReserves[iCnt].iReserveTypeCode)
                                {
                                    //this part of code is commented by Nitin for Mits 18873
                                    //if (dAmount > 0.0)
                                    //{
                                    // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - Begin
                                    //objFunds.ModifyReserves(iClaimId, iClaimantEID, iUnitID, iReserveTypeCode, sDateEntered,
                                    //dAmount, iStatusCodeID, m_CommonInfo.UserName , sReason, m_CommonInfo.UserId , p_iGroupId);

                                    // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - Begin
                                    //objFunds = new ReserveFunds(m_CommonInfo.Connectionstring, sClaimNum, 0, 0, p_objLogin);
                                    objFunds = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                                    // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - End

                                    objFunds.ClaimId = iClaimId;
                                    objFunds.ClaimantEid = iClaimantEID;
                                    objFunds.UnitId = iUnitID;
                                    objFunds.ReserveTypeCode = iReserveTypeCode;
                                    objFunds.DateEntered = sDateEntered;
                                    objFunds.ReserveAmount = dAmount;
                                    objFunds.ResStatusCode = iStatusCodeID;
                                    objFunds.EnteredByUser = m_CommonInfo.UserName;
                                    objFunds.Reason = sReason;
                                    objFunds.iUserId = m_CommonInfo.UserId;
                                    objFunds.sUser = m_CommonInfo.UserName;
                                    objFunds.iGroupId = p_iGroupId;
                                    objFunds.sUpdateType = "Reserves";
                                    objFunds.Save();

                                    objFunds = null;
                                    // mcapps2 Changed from ReserveFunds to ReserveCurrent for Reserve Current data model object changes. - End

                                    bResAdjusted = true;
                                    //We have to get the Reserve Current and Reserve History ID here and put it in a array
                                    larrayResCurrent[iCnt, 1] = GetResCurrentID(iClaimId, iClaimantEID, iReserveTypeCode);
                                    larrayResHistory[iCnt, 1] = GetResHistoryID(iClaimId, iClaimantEID, iReserveTypeCode);
                                    //}
                                    //else
                                    //{
                                    //    //We have to get the Reserve Current and Reserve History ID here and put it in a array
                                    //    larrayResCurrent[iCnt, 1] = 0;
                                    //    larrayResHistory[iCnt, 1] = 0;
                                    //}
                                }
                                # endregion
                            }
                        }
                        #endregion

                        if (bResAdjusted)
                        {
                            //Change the worksheet to change the following info:
                            int iAprCodeId = 0;
                            iAprCodeId = objCache.GetCodeId("AP", "RSW_STATUS");

                            //Worksheet approved on and approved by
                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdndtAppRej']");
                            if (objNode != null)
                            {
                                objNode.InnerText = DateTime.Now.ToString();
                                objNode.Attributes["dbformat"].InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
                            }

                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdnAppRejBy']");
                            if (objNode != null)
                                objNode.InnerText = m_CommonInfo.UserName;

                            //Worksheet - status
                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdnRSWStatus']");
                            if (objNode != null)
                            {
                                objNode.InnerText = "Approved";
                                objNode.Attributes["codeid"].InnerText = iAprCodeId.ToString();
                            }

                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdnLastModBy']");
                            if (objNode != null)
                                sSubmittedBy = objNode.InnerText;

                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdndtLastMod']");
                            if (objNode != null)
                                sLastModOn = objNode.InnerText;

                            //Worksheet - status
                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdnReqStatus']");
                            if (objNode != null)
                            {
                                objNode.InnerText = "Approved";
                                objNode.Attributes["codeid"].InnerText = iAprCodeId.ToString();
                            }

                            //Set the hidden node to a value, in order to refresh the Reserve Listing screen.
                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']");
                            if (objNode != null)
                                objNode.InnerText = "Approve";

                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']");
                            if (objNode != null)
                                objNode.InnerText = "Approve";
                            //Worksheet - approval comments if any.

                            #region If any other approved worksheet is present for that claim, move it to History status

                            objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                            objCon.Open();

                            XmlDocument objPrevAppDoc = new XmlDocument();

                            sbSQL.Append("SELECT RSW_ROW_ID, RSW_XML FROM RSW_WORKSHEETS WHERE CLAIM_ID = " + iClaimId.ToString()
                                + " AND RSW_STATUS_CODE = " + iAprCodeId);

                            objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                            //Mgaba2:cleared sbsql for future use
                            sbSQL.Remove(0, sbSQL.Length);
                            if (objRdr.Read())
                            {
                                int iHisCode = objCache.GetCodeId("HI", "RSW_STATUS");
                                int iHisRSWId = objRdr.GetInt32("RSW_ROW_ID");
                                objPrevAppDoc.LoadXml(objRdr.GetString("RSW_XML"));
                                //MGaba2:Temporarily commenting
                                //Change the XML to update Reserve Worksheet status
                                //objNode = objPrevAppDoc.SelectSingleNode("//control[@name='hdnReqStatus']");
                                //if (objNode != null)
                                //{
                                //    objNode.InnerText = "History";
                                //    objNode.Attributes["codeid"].InnerText = iHisCode.ToString();
                                //}

                                objNode = objPrevAppDoc.SelectSingleNode("//control[@name='hdnRSWStatus']");
                                if (objNode != null)
                                {
                                    objNode.InnerText = "History";
                                    objNode.Attributes["codeid"].InnerText = iHisCode.ToString();
                                }

                                objPrevAppDoc = SaveReserveWorksheet(objPrevAppDoc);
                                SaveReserveWorksheetHistory(objPrevAppDoc);

                                objPrevAppDoc = null;

                                //************Mohit Yadav: For Future Use: Can Insert new records into the RSWTables*****

                            }
                            objRdr.Close();

                            #endregion

                            //Update record in database table 'RSW_WORKSHEETS', to change the following:
                            //RSW_STATUS_CODE, UPDATED_BY_USER, DTTM_RCD_LAST_UPD, APPROVED_BY, DTTM_APPROVED

                            //rejection comments
                            objNode = objXmlOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                            if (objNode != null)
                                if (objXmlIn.SelectSingleNode("//AppRejReqCom") != null)
                                    objNode.InnerText = objXmlIn.SelectSingleNode("//AppRejReqCom").InnerText;
                                else if (objXmlIn.SelectSingleNode("//control[@name='hdnAppRejComm']") != null)
                                    objNode.InnerText = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejComm']").InnerText;


                            //Added by Nitin
                        
                            objXmlOut = SaveReserveWorksheet(objXmlOut);
                            SaveReserveWorksheetHistory(objXmlOut);
                            //*****Mohit Yadav: Reserve Worksheet record link to Reserve_Current and Reserve_History*****
                            SaveReserveWorksheetLinks(objXmlOut, larrayResCurrent, larrayResHistory);
                            UpdateRSWTransAmount(objXmlOut, arrReserves, p_WorksheetRequest.RSWReservesXML,iClaimCurrCode);
                            objXmlOut = MakeRWReadOnly(objXmlOut);

                            //Priya Aggarwal: Document Imaging for Reserve Worksheet - start
                            #region Attach document to Claim
                            //MGaba2:Reading the setting from utilities and in case Attach Reserve WorkSheet is checked in 
                            //LOB Parameters,then only it will be attached with the related record

                            sbSQL.Append("SELECT ATTACH_RSV_WK FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOBCode.ToString());
                            objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());

                            sbSQL.Remove(0, sbSQL.Length);

                            if (objRdr.Read())
                            {
                                iAttachRSW = objRdr.GetInt32("ATTACH_RSV_WK");
                            }
                            if (iAttachRSW == -1)
                            {
                                MemoryStream memoryStream = new MemoryStream();
                                //MITS 14622
                                string dateTimeNow = string.Format("{0:yyyyMMddhhmmss}", System.DateTime.Now);
                                fileName = objClaim.ClaimNumber + "_" + dateTimeNow + ".pdf";

                                //MGaba2:R6:Changed the signatures of PrintWorksheet function.So need to make these changes:Start

                                objXmlInForPrint = new XmlDocument();
                                objXmlOutForPrint = new XmlDocument();
                                objTempElement = objXmlInForPrint.CreateElement("form");
                                objXmlInForPrint.AppendChild(objTempElement);
                                objTempElement = objXmlInForPrint.CreateElement("control");
                                objTempElement.SetAttribute("name", "RSWId");
                                objTempElement.InnerText = sRSWID;
                                objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                objTempElement = objXmlInForPrint.CreateElement("control");
                                objTempElement.SetAttribute("name", "DocPath");
                                //objTempElement.InnerText = p_objLogin.objRiskmasterDatabase.GlobalDocPath.ToString();                            
                                if (p_objLogin.DocumentPath.Length > 0)
                                {
                                    // use StoragePath set for the user
                                    objTempElement.InnerText = p_objLogin.DocumentPath;
                                }
                                else
                                {
                                    // use StoragePath set for the DSN
                                    objTempElement.InnerText = p_objLogin.objRiskmasterDatabase.GlobalDocPath;
                                }
                                objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                objTempElement = objXmlInForPrint.CreateElement("control");
                                objTempElement.SetAttribute("name", "DocPathType");
                                objTempElement.InnerText = p_objLogin.objRiskmasterDatabase.DocPathType.ToString();
                                objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                objTempElement = objXmlInForPrint.CreateElement("control");
                                objTempElement.SetAttribute("name", "FileName");
                                objTempElement.InnerText = fileName;
                                objXmlInForPrint.FirstChild.AppendChild(objTempElement);
                                objRSWReqForPrint = new ReserveWorksheetRequest();
                                objRSWResForPrint = new ReserveWorksheetResponse();
                                objRSWReqForPrint.InputXmlString = objXmlInForPrint.InnerXml.ToString();
                                PrintWorksheet(objRSWReqForPrint, ref objRSWResForPrint);
                                //PrintWorksheet(sRSWID, out memoryStream, true, p_objLogin.objRiskmasterDatabase.GlobalDocPath.ToString(), fileName);
                                //MGaba2:R6:End
                                if (objCon == null)
                                {
                                    objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                                    objCon.Open();
                                }
                                cmd = objCon.CreateCommand();
                                try
                                {
                                    //fileName = objClaim.ClaimNumber + "_" + string.Format("{0:yyyyMMddhhmmss}", System.DateTime.Now) + ".pdf";
                                    documentId = Utilities.GetNextUID(objCon, "DOCUMENT", base.ClientId);
                                    objDbTransaction = objCon.BeginTransaction();
                                    cmd.Transaction = objDbTransaction;
                                    sbSQL.Append("INSERT INTO DOCUMENT (DOCUMENT_ID, DOCUMENT_NAME, DOCUMENT_TYPE,"
                                        + " DOCUMENT_FILENAME,DOC_INTERNAL_TYPE, "
                                        + " FOLDER_ID, DOCUMENT_CLASS, "
                                        + " USER_ID, DOCUMENT_CATEGORY, CREATE_DATE) VALUES("
                                        + documentId + " , 'RESERVE_WORKSHEET',0, '" + fileName + "', 2, 0, 0, '"
                                        + p_objLogin.LoginName + "', 0, "
                                        + dateTimeNow + ")");
                                    cmd.CommandText = sbSQL.ToString();
                                    cmd.ExecuteNonQuery();
                                    sbSQL.Remove(0, sbSQL.Length);

                                    iReserveTrackingLevel = GetReserveTracking(iClaimId, ref iLOBCode);
                                    //MGaba2:To Do Left for iReserveTrackingLevel=2
                                    sTableNameForAttach = "CLAIM";
                                    iRecordIdForAttach = iClaimId;

                                    if (iReserveTrackingLevel != 0)
                                    {
                                        if (iClaimantRowId != 0)
                                        {
                                            sTableNameForAttach = "CLAIMANT";
                                            iRecordIdForAttach = iClaimantRowId;
                                        }
                                        else if (iUnitRowId != 0)
                                        {
                                            sTableNameForAttach = "UNIT";
                                            iRecordIdForAttach = iUnitRowId;
                                        }
                                    }


                                    sbSQL.Append("INSERT INTO DOCUMENT_ATTACH VALUES(" + Utilities.FormatSqlFieldValue(sTableNameForAttach) + ", " + iRecordIdForAttach.ToString()
                                        + ", " + documentId.ToString() + ")");
                                    cmd.CommandText = sbSQL.ToString();
                                    cmd.ExecuteNonQuery();
                                    sbSQL.Remove(0, sbSQL.Length);

                                    objDbTransaction.Commit();
                                }
                                catch (Exception ex)
                                {
                                    string test = ex.Message;
                                    objDbTransaction.Rollback();
                                }
                            }
                            #endregion

                            //Priya Aggarwal: Document Imaging for Reserve Worksheet - end

                            #region Send an email to the person who submitted the worksheet and also create the Diary

                            string sToEmail = string.Empty;
                            string sFromEmail = string.Empty;
                            string sSubjectEmail = string.Empty;
                        //    string sBodyEmail = string.Empty;
                            string sToUser = string.Empty;
                            string sFromUser = string.Empty;

                            string sRSWType = string.Empty;
                            string sPCName = string.Empty;
                            string strReason = string.Empty;
                            int iAdjID = 0;

//MGaba2:R8:SuperVisory Approval
                            iAdjID = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);

                        if (iAdjID > 0 && m_bUseCurrAdjReserves && (!m_bDisableEmailNotify || !m_bDisableDiaryNotify))//Start : rsushilaggar - 22-Mar-2010 Mits : 19624
                            {

                                sbSQL.Append("SELECT LOGIN_NAME");
                                sbSQL.Append(" FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID = " + Conversion.ConvertStrToInteger(iAdjID.ToString()));
                                sbSQL.Append(" AND DSNID = " + m_CommonInfo.DSNID);
                        }
                        else
                        {
                            sbSQL.Append("SELECT LOGIN_NAME");
                            sbSQL.Append(" FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID = " + Conversion.ConvertStrToInteger(sAddedByUser));
                            sbSQL.Append(" AND DSNID = " + m_CommonInfo.DSNID);
                        }
                                objRdr = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                                sbSQL.Remove(0, sbSQL.Length);

                                if (objRdr.Read())
                                    sSubmittedTo = objRdr.GetString("LOGIN_NAME");
                                else
                                    sSubmittedTo = "";

                                objRdr.Close();
                                //MGaba2:R6:Change the name of this node
                                //objNode = objXmlOut.SelectSingleNode("//control[@name='WorksheetType']");
                                objNode = objXmlOut.SelectSingleNode("//control[@name='hdnRSWType']");
                                if (objNode != null)
                                    sRSWType = objNode.Attributes["title"].InnerText;

                                //MGaba2:R6:Change the name of this node.Also added null check
                                //objNode = objXmlOut.SelectSingleNode("//control[@name='hdnpcName']");
                                objNode = objXmlOut.SelectSingleNode("//control[@name='hdnPCName']");
                                if (objNode != null)
                                    sPCName = objNode.InnerText;

                                objNode = objXmlOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                                if (objNode != null)
                                {
                                    if (objNode.InnerText != "")
                                        strReason = objNode.InnerText;
                                }
                                //skhare7 R8 Fn.Transffered to Common fns
                                CommonFunctions.GetUserDetails(sSubmittedTo, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sToEmail, ref sToUser,m_iClientId);
                                CommonFunctions.GetUserDetails(m_CommonInfo.UserName, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sFromEmail, ref sFromUser,m_iClientId);

                            //skhare7 R8 Fn.Transffered to Common fns End

                            //changed by shobhana 16/06/2010

                            string sBodyEmail = string.Empty;
                            //skhare7 changed for R8
                            //  EmailFormat objEMail = null;
                            // objEMail = new EmailFormat(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                            objFunds = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                            sBodyEmail = objFunds.GetEmailFormat("RSWSheet_APP", iClaimId);

                            if (sPCName != "" && sPCName != null)

                            //changed by shobhana MITS 22442
                            {
                                sSubjectEmail = "The Reserve Worksheet for " + sClaimNum + " – " + sPCName + " Approved";

                            }
                            else
                            {
                                sSubjectEmail = "The Reserve Worksheet for " + sClaimNum + " Approved";
                            }
                            if (sLastModOn != null && sLastModOn != "")
                            {
                                DateTime dtLastModOn = Convert.ToDateTime(sLastModOn);
                                sLastModOn = dtLastModOn.ToShortDateString();

                            }
                            sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", "Claim Reserve Worksheet")
                                             .Replace("{APPROVED_BY}", sFromUser)
                                             .Replace("{USER_NAME}", sPCName)
                                             .Replace("{LAST_MODIFIED_DATE}", sLastModOn)
                                             .Replace("{APPROVE_REASON}", strReason);

                            //End changed by shobhana MITS 22442
                            //Send a diary to the person who submitted the worksheet
                            string sRegarding = sClaimNum + " * " + sPCName;
                            if (sSubmittedTo != m_CommonInfo.UserName)
                            {
                                //Start - rsushilaggar - 22-Mar-2010 Mits : 19624
                                //The appropriate user should be getting an email and a diary
                                if (!m_bDisableDiaryNotify)
                                    GenerateDiary(sSubjectEmail, sBodyEmail, 1, sSubmittedTo, m_CommonInfo.UserName, sRegarding, iClaimId);
                                if (sToEmail != "" && sFromEmail != "" && !m_bDisableEmailNotify)
                                    //skhare7 R8 Fn.Transffered to Common fns
                                    CommonFunctions.SendEmail(sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                                //skhare7 R8 Fn.Transffered to Common fns End
                                //End - rsushilaggar
                            }
                        }//if (bResAdjusted)  
                        else
                        {
                            throw new RMAppException("Reserve is not adjusted ,please verify the data you have entered.");
                        }
                    }//end of  if (bUnderLimit)
                            #endregion



                    if (!bUnderLimit)
                    {
                        strHoldReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove", m_iClientId) + " as " + strHoldReason);
                    }
                    else if (!bUnderClaimIncLimit)
                    {
                        strHoldReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.ApproveWorksheet.NoAuthApprove", m_iClientId) + " as " + strHoldReason);
                    }
                }
                }
             //End of try
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    (Globalization.GetString("ReserveWorksheet.ApproveWorksheet.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();

                if (objClaim != null)
                    objClaim.Dispose();

                if (objFunds != null)
                    objFunds = null;

                if (objCache != null)
                    objCache.Dispose();

                if (objCon != null)
                    objCon.Dispose();

                if (objRdr != null)
                    objRdr.Dispose();

            }
            p_WorksheetResponse.OutputXmlString = objXmlOut.OuterXml;
        }

        /// <summary>
        /// This function is called when the worksheet is sent for rejection.
        /// </summary>
        /// <param name="p_objXmlDoc">in XML</param>
        /// <param name="p_iDSNId">DSN id</param>
        /// <param name="p_iUserId">Logged in user id</param>
        /// <param name="p_iGroupId">Logged in user's group id</param>
        /// <returns>Returns the XML to be rendered on the screen.</returns>
        public override void RejectWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId)
        {
            int iRswId = 0;
            StringBuilder sbSQL;
            string sRswStatus = "";
            XmlNode objNod = null;
            DbConnection objCon = null;
            LocalCache objCache = null;
            DbReader objRdr = null;
            DbReader objReaderLOB = null;
            XmlDocument objRswXml = null;
            XmlDocument objXmlIn = null;

            string sSubmittedTo = string.Empty;
            int iSubmToID = 0;
            int iSubmbyID = 0;
            int iClaimId = 0;

            string sSubmittedBy = string.Empty;
            string sLastModOn = string.Empty;
            string sRswTypeToLaunch = string.Empty;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            bool bUnderLimit = false;
            bool bUnderClaimIncLimit = false;//sachin 6385
            ReserveFunds.structReserves[] arrReserves = null;
            //skhare7
            ReserveCurrent objReservecurrent=null;
            DataModelFactory objDataModelFactory = null;
            //skhare7 r8
            int iClaimCurrCode = 0;

            bool bRejectRSW = false;            //Added:Yukti,Dt:12/31/2013, MITS 33496

            try
            {
                sbSQL = new StringBuilder();

                /*Check whether the logging user is part of the managerial chain. 
                 *Check 'SUBMITTED_TO' field for the worksheet and go on ascending till you find the user. 
                 *If not found, return that the user can not reject.*/

                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);

                objNod = objXmlIn.SelectSingleNode("//RswId");
                if (objNod != null)
                    iRswId = Conversion.ConvertObjToInt(objNod.InnerText, base.ClientId);

                if (iRswId <= 0)
                {
                    objNod = objXmlIn.SelectSingleNode("//control[@name='hdnRSWid']");
                    if (objNod != null)
                        iRswId = Conversion.ConvertStrToInteger(objNod.InnerText);
                }

                sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId);
                objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                if (objRdr.Read())
                {
                    sSubmittedTo = objRdr.GetString("SUBMITTED_TO");
                    iSubmbyID = Riskmaster.Common.Conversion.ConvertStrToInteger(objRdr.GetString("SUBMITTED_BY"));
                    iSubmToID = Riskmaster.Common.Conversion.ConvertStrToInteger(objRdr.GetString("SUBMITTED_TO"));
                    sRswStatus = objRdr.GetInt32("RSW_STATUS_CODE").ToString();
                    sRswTypeToLaunch = objRdr.GetString("RSWXMLTYPE");

                    //Added by Shobhana
                    if (!IsRSWAllowed(iRswId, sRswTypeToLaunch,p_WorksheetRequest.ClaimId))
                    {
                        throw new RMAppException("Please check the configuration settings for Reserve worksheet Type.");
                    }
                }
                    objRdr.Close();
                //MGaba2:R6:Shifted it above because we need claim id for supervisory approval :start

                    objRswXml = new XmlDocument();
                    objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

                    sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId);
                    sbSQL.Append(" AND RSW_STATUS_CODE = " + objCache.GetCodeId("PA", "RSW_STATUS"));
                    objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objRdr.Read())
                        objRswXml.LoadXml(objRdr.GetString("RSW_XML"));
                    objRdr.Close();


                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimId']");
                    iClaimId = Conversion.ConvertStrToInteger(objNod.InnerText);
                    //MGaba2:R6:End

                    

                    //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                    #region Check to see if a user other than the one submitted to tries to reject the reserve worksheet
                    if (iSubmToID != 0)
                    {
                        if (m_CommonInfo.UserId != iSubmToID)
                        {
                            //Stop looping if the user is found in the managerial chain
                            int iLevel = 1;
                            iLevel = GetSupervisoryLevel(iSubmToID, m_CommonInfo.UserId, iClaimId);
                            if (iLevel == 0)
                                //Added:Yukti,Dt:12/30/2013, MITS 33496
                                bRejectRSW = true;
                                SendReserveWorkSheetDiaryEmail(ref objRswXml, iClaimId,ref sSubmittedTo, sLastModOn, bRejectRSW);
                                //Ended:Yukti
                                throw new RMAppException(Globalization.GetString("ReserveWorksheet.RejectWorksheet.NoAuthRej",m_iClientId));
                        }
                    }

                    #endregion

                    /*Check whether the reserves submitted are under the user's limits.
                 *If not, return that the user cannot reject.*/

                    //MGaba2:R6:Shifting it above because we need claim id for supervisory approval :start
                    //objRswXml = new XmlDocument();
                    //objCache = new LocalCache(m_CommonInfo.Connectionstring);

                    //sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId);
                    //sbSQL.Append(" AND RSW_STATUS_CODE = " + objCache.GetCodeId("PA", "RSW_STATUS"));
                    //objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    //sbSQL.Remove(0, sbSQL.Length);
                    //if (objRdr.Read())
                    //    objRswXml.LoadXml(objRdr.GetString("RSW_XML"));
                    //objRdr.Close();
                    //MGaba2:R6:End

                    //Check If the status has changed
                    //MGaba2:R6:Changed the node from Rswstatus to control[@name='hdnRSWStatus']
                    //objNod = objRswXml.SelectSingleNode("//RswStatus");

                   

                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNod != null)
                    {
                        //if (objNod.InnerText != "")
                        //if (sRswStatus != objNod.InnerText)
                        if (objNod.Attributes["codeid"].InnerText != "")
                        {
                            if (sRswStatus != objNod.Attributes["codeid"].Value)
                            {
                                objNod.InnerText = "";
                                throw new RMAppException(Globalization.GetString("ReserveWorksheet.DataChanged", m_iClientId));
                            }
                        }
                    }
                    //MGaba2:R6:Shifting it above because we need claim id for supervisory approval :start
                    //objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimId']");
                    //iClaimId = Conversion.ConvertStrToInteger(objNod.InnerText);
                    //MGaba2:R6:End

                    #region Get the Line of Business for the claim

                    sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId);
                    objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    if (objReaderLOB.Read())
                    {
                        iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                        iClmTypeCode = objReaderLOB.GetInt32("CLAIM_TYPE_CODE");
                        iClaimCurrCode = objReaderLOB.GetInt32("CLAIM_CURR_CODE");
                    }
                    objReaderLOB.Close();
                    sbSQL.Remove(0, sbSQL.Length);

                    #endregion

                    //Check Reserve Limits Enabled/Disabled
                    GetLOBReserveOptions(iClaimId);

                    //Get Supervisory Approval Settings
                    ReadRSWCheckOptions();

                    string[,] sResFinal = null;
                    arrReserves = GetReservesTypeCustom(iLOBCode, iClmTypeCode, p_WorksheetRequest.RSWReservesXML, objRswXml);

               //     arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                    sResFinal = new string[arrReserves.GetLength(0), 4];

                    sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objRswXml, iClaimCurrCode);


                
                if (m_bResLimitFlag) //If the Flag is 'true' then check the reserve limits
                    {
                        bUnderLimit = UnderUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves, iClaimCurrCode);

                        //By Vaibhav on 11/14/08
                        if (bUnderLimit && m_iDaysAppReserves > 0)
                        {
                            int iTopID = 0;
                            sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                            objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                            sbSQL.Remove(0, sbSQL.Length);
                            if (objRdr.Read())
                                iTopID = objRdr.GetInt("USER_ID");

                            if (iTopID != m_CommonInfo.UserId)
                            {
                                //Check if Days for Approval has lapsed and set the bUnderLimit flag to false
                                //so that the reserve worksheet goes to the next level supervisor
                                bool bDaysApprovalLapsed = false;
                                string sRSWDateTime = "";

                                sbSQL.Append("SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + iRswId);
                                objRdr = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                                sbSQL.Remove(0, sbSQL.Length);
                                if (objRdr.Read())
                                {
                                    sRSWDateTime = objRdr.GetString("DTTM_RCD_LAST_UPD");
                                    //By Vaibhav on 11/14/08 : modified  CheckDaysForApprovalLapsed function
                                    bDaysApprovalLapsed = CheckDaysForApprovalLapsed(sRSWDateTime, Conversion.ConvertObjToInt(objRdr.GetString("SUBMITTED_TO"), base.ClientId), m_CommonInfo.UserId, iClaimId);
                                }
                                objRdr.Close();
                                if (bDaysApprovalLapsed)
                                    bUnderLimit = false;
                            }
                        }
                        //vaibhav code end
                    }
                    else
                        bUnderLimit = true;

                //sachin 6385 starts
                if (m_bClaimIncLimitFlag)
                {
                    bUnderClaimIncLimit = UnderClaimIncUserLimit(m_CommonInfo.UserId, p_iGroupId, iClaimId, sResFinal, arrReserves, iClaimCurrCode);
                }
                else
                    bUnderClaimIncLimit = true;
                //sachin 6385 ends

                    //He can reject worksheet submitted by sub ordinate, even if its not in limit
                //sachin 6385 starts
                    //if (!bUnderLimit) // If the limit is above the user's limit
                        if (!bUnderLimit || !bUnderClaimIncLimit) // If the limit is above the user's limit
                        //sachin 6385 ends
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.RejectWorksheet.NoAuthRej",m_iClientId));

                    //If reached here, then the user has the authority to reject the reserves entered.
                    //Change the worksheet XML to change the following info:
                    //last modified on and last modified by
                        //sachin 6385 starts
                   // if (bUnderLimit)
                        if (bUnderLimit && bUnderClaimIncLimit)
                        {
                        bRejectRSW = false;     //Added:Yukti,Dt:12/30/2013, MITS 33496
                        //MGaba2:R6:Changed the node from dtLastMod to hdndtLastMod
                        //objNod = objRswXml.SelectSingleNode("//control[@name='dtLastMod']");
                        objNod = objRswXml.SelectSingleNode("//control[@name='hdndtLastMod']");
                        if (objNod != null)
                        {
                        sLastModOn = objNod.InnerText;
                            objNod.InnerText = DateTime.Now.ToString();
                            objNod.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());
                        }

                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnLastModBy']");
                        if (objNod != null)
                        {
                            sSubmittedBy = objNod.InnerText;
                            objNod.InnerText = m_CommonInfo.UserName;
                        }

                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnUnderLimit']");
                        if (objNod != null)
                            objNod.InnerText = "Reject";

                        //status
                        int iRejCode = objCache.GetCodeId("RJ", "RSW_STATUS");
                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnRSWStatus']");
                        objNod.InnerText = "Rejected";
                        objNod.Attributes["codeid"].InnerText = iRejCode.ToString();

                        //Change the date rejected and rejected by fields
                        objNod = objRswXml.SelectSingleNode("//control[@name='hdndtAppRej']");
                        objNod.InnerText = DateTime.Now.ToString();
                        objNod.Attributes["dbformat"].InnerText = Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString());

                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnAppRejBy']");
                        objNod.InnerText = m_CommonInfo.UserName;

                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnMode']");
                        if (objNod != null)
                        {    //MGaba2:R6:Changed the value as rejected worksheet will always open in edit mode
                            //    objNod.InnerText = "ReadOnly";
                            objNod.InnerText = "Edit";
                        }

                        //rejection comments
                        objNod = objRswXml.SelectSingleNode("//control[@name='hdnAppRejComm']");
                        if (objNod != null)
                            if (objXmlIn.SelectSingleNode("//AppRejReqCom") != null)
                                objNod.InnerText = objXmlIn.SelectSingleNode("//AppRejReqCom").InnerText;
                            else if (objXmlIn.SelectSingleNode("//control[@name='hdnAppRejComm']") != null)
                                objNod.InnerText = objXmlIn.SelectSingleNode("//control[@name='hdnAppRejComm']").InnerText;

                        // Update record in database table 'RSW_WORKSHEETS'
                        objRswXml = SaveReserveWorksheet(objRswXml);
                        SaveReserveWorksheetHistory(objRswXml);
                        //MGaba2:R6:Changed the function name as rejected worksheet will always open in edit mode
                        //objRswXml = MakeRWReadOnly(objRswXml);
                        objRswXml = MakeRWEditable(objRswXml);

                        //Insert new record into the RSWTables*****                  
                        //For future use: RSWtoTables(objRswXml, objCon, iNewRswId);
                        //Added:Yukti,Dt:12/30/2013, MITS 33496,Encapsulated below code to a single function
                        SendReserveWorkSheetDiaryEmail(ref objRswXml, iClaimId, ref sSubmittedTo, sLastModOn, bRejectRSW);
                        //#region Send Diary & Email to the user
                        ////Send email to the user who submitted the worksheet
                        //string sToEmail = string.Empty;
                        //string sFromEmail = string.Empty;
                        //string sSubjectEmail = string.Empty;
                        //// string sBodyEmail = string.Empty;
                        //string sToUser = string.Empty;
                        //string sFromUser = string.Empty;

                        //string sCurAdjEmail = string.Empty;
                        //string sCurAdjUser = string.Empty;
                        //string sCurAdjLogin = string.Empty;

                        //string sRSWType = string.Empty;
                        //string sClaimNumber = string.Empty;
                        //string sPCName = string.Empty;
                        //string sReason = string.Empty;

                        ////smahajan6 - For MITS 13750 - Start
                        ////MGaba2:R8:SuperVisory Approval
                        //int iAdjID = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring);
                        //sSubmittedTo = "";
                        ////To Get the Login Name of Current Adjuster
                        //if (iAdjID > 0 && (!m_bDisableEmailNotify || !m_bDisableDiaryNotify))//rsushilaggar : 22-Mar-2010 Mits : 19624
                        //{

                        //    sbSQL.Append("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE");
                        //    sbSQL.Append(" WHERE USER_ID = " + iAdjID);
                        //    sbSQL.Append(" AND DSNID = " + m_CommonInfo.DSNID);
                        //    objRdr = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                        //    sbSQL.Remove(0, sbSQL.Length);

                        //    if (objRdr.Read())
                        //        sSubmittedTo = objRdr.GetString("LOGIN_NAME");
                        //    else
                        //        sSubmittedTo = "";

                        //    objRdr.Close();

                        //    objNod = objRswXml.SelectSingleNode("//control[@name='WorksheetType']");
                        //    if (objNod != null)
                        //        sRSWType = objNod.Attributes["title"].InnerText;

                        //    //objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimNum']");
                        //    objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimNumber']");
                        //    if (objNod != null)
                        //        sClaimNumber = objNod.InnerText;

                        //    objNod = objRswXml.SelectSingleNode("//control[@name='hdnpcName']");
                        //    if (objNod != null)
                        //        sPCName = objNod.InnerText;

                        //    objNod = objRswXml.SelectSingleNode("//control[@name='hdnAppRejComm']");
                        //    if (objNod != null)
                        //    {
                        //        if (objNod.InnerText != "")
                        //            sReason = objNod.InnerText;
                        //    }
                        //    //skhare7 R8 Fn.Transffered to Common fns
                        //    CommonFunctions.GetUserDetails(sSubmittedTo, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sToEmail, ref sToUser);
                        //    CommonFunctions.GetUserDetails(m_CommonInfo.UserName, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sFromEmail, ref sFromUser);
                        //    //skhare7 R8 Fn.Transffered to Common fns end 

                        //    string sBodyEmail = string.Empty;
                        //    //skhare7 Changed  R8 transfrd to Data model
                        //    //EmailFormat objEMail = null;
                        //    // objEMail = new EmailFormat(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                        //    objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                        //    objReservecurrent = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                        //    sBodyEmail = objReservecurrent.GetEmailFormat("RSWSheet_REJ", iClaimId);
                        //    //MITS 20181 Defects Removed 
                        //    // MITS 22442
                        //    if (sPCName != "" && sPCName != null)
                        //    {
                        //        sSubjectEmail = "The Reserve Worksheet for Claim " + sClaimNumber + " - " + sPCName + " Rejected";

                        //    }
                        //    else
                        //    {
                        //        sSubjectEmail = "The Reserve Worksheet for Claim " + sClaimNumber + " Rejected";
                        //    }

                        //    if (sLastModOn != null && sLastModOn != "")
                        //    {
                        //        DateTime dtLastModOn = Convert.ToDateTime(sLastModOn);
                        //        sLastModOn = dtLastModOn.ToShortDateString();

                        //    }
                        //    sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", "Claim Reserve Worksheet")
                        //                      .Replace("{REJECTED_BY}", sFromUser)
                        //                     .Replace("{SUBMITTED_DATE}", sLastModOn)
                        //                     .Replace("{USER_NAME}", sPCName)
                        //                     .Replace("{REASON}", sReason);
                        //    //MITS 20181 Defects Removed 
                        //    //    MITS 22442
                        //    //Changed by shobhana 
                        //    //Send a diary to the person who submitted the worksheet
                        //    string sRegarding = sClaimNumber + " * " + sPCName; // TODO: regarding
                        //    if (sSubmittedTo != m_CommonInfo.UserName)//smahajan6 - For MITS 13750 
                        //    {
                        //        //Start: rsushilaggar : 22-Mar-2010 Mits : 19624
                        //        //The appropriate user should be getting an email, not a diary. No diary should exist
                        //        if (!m_bDisableDiaryNotify)
                        //            GenerateDiary(sSubjectEmail, sBodyEmail, 2, sSubmittedTo, m_CommonInfo.UserName, sRegarding, iClaimId);
                        //        if (sToEmail != "" && sFromEmail != "" && !m_bDisableEmailNotify)
                        //            CommonFunctions.SendEmail(sToEmail, sFromEmail, sSubjectEmail, sBodyEmail);
                        //        //skhare7 R8 Fn.Transffered to Common fns 
                        //        //End : rsushilaggar
                        //    }
                        //} 
                        //#endregion
                        //Ended:Yukti,DT:12/30/2013
                    //smahajan6 - For MITS 13750 - End

                }
                //MGaba2:Return type of function has been changed
                p_WorksheetResponse.OutputXmlString = objRswXml.OuterXml;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
                if (objCache != null)
                    objCache.Dispose();
                if (objRdr != null)
                    objRdr.Dispose();
                if (objReaderLOB != null)
                    objReaderLOB.Dispose();
                if (objReservecurrent != null)
                {
                    objReservecurrent.Dispose();

                }
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();

                }
                objRswXml = null;
                objNod = null;
                
            }
        }

     
        
        /// //Added:Yukti, St:12/30/2013, MITS 33496
        /// aaded to send email and Diary to the user in Reject RSW
        public void SendReserveWorkSheetDiaryEmail(ref XmlDocument objRswXml, int iClaimId, ref string sSubmittedTo, string sLastModOn, bool bRejectRSW)
        {
            #region SendEmail
            string sToEmail = string.Empty;
            string sFromEmail = string.Empty;
            string sSubjectEmail = string.Empty;
            // string sBodyEmail = string.Empty;
            string sToUser = string.Empty;
            string sFromUser = string.Empty;

            string sCurAdjEmail = string.Empty;
            string sCurAdjUser = string.Empty;
            string sCurAdjLogin = string.Empty;

            string sRSWType = string.Empty;
            string sClaimNumber = string.Empty;
            string sPCName = string.Empty;
            string sReason = string.Empty;
            StringBuilder sbSQL;
            DbReader objRdr = null;
            XmlNode objNod = null;
            ReserveCurrent objReservecurrent = null;
            DataModelFactory objDataModelFactory = null;

            //smahajan6 - For MITS 13750 - Start
            //MGaba2:R8:SuperVisory Approval
            try
            {
                sbSQL = new StringBuilder();
                int iAdjID = CommonFunctions.GetAdjusterEID(iClaimId, m_CommonInfo.Connectionstring, base.ClientId);
                //sSubmittedTo = "";    //Yukti,MITS 33496
                //To Get the Login Name of Current Adjuster
                //Added:Yukti,Dt:01/02/2014, Sending Diary even in the case when Adjuster is not attached to the claim
                //if (iAdjID > 0 && (!m_bDisableEmailNotify || !m_bDisableDiaryNotify))//rsushilaggar : 22-Mar-2010 Mits : 19624      
                //if (iAdjID > 0 && (!m_bDisableEmailNotify || !m_bDisableDiaryNotify) || (bRejectRSW && sSubmittedTo != "" && (!m_bDisableEmailNotify || !m_bDisableDiaryNotify)))
                // The if condition is corrected to include case of no adjuster atatched to claim -mbahl3 (JIRA RMACLOUD-2383)
                // The if condition is corrected to remove bRejectRSW which is already handled below -mbahl3 (JIRA RMACLOUD-2383)
                if (!m_bDisableEmailNotify || !m_bDisableDiaryNotify)
                {
                    if (iAdjID > 0 || sSubmittedTo != "")
                    {
                         
                    sbSQL.Append("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE");
                    sbSQL.Append(" WHERE USER_ID = " + iAdjID);
                    sbSQL.Append(" AND DSNID = " + m_CommonInfo.DSNID);
                    objRdr = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);

                    //Added:Yukti, Dt:12/30/2013, MITS 33496
                    if (!bRejectRSW)
                    {
                        if (objRdr.Read())
                            sSubmittedTo = objRdr.GetString("LOGIN_NAME");
                        else
                            sSubmittedTo = "";
                    }
                    else
                    {
                        sbSQL.Append("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE");
                        sbSQL.Append(" WHERE USER_ID = " + sSubmittedTo);
                        sbSQL.Append(" AND DSNID = " + m_CommonInfo.DSNID);
                        objRdr = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sbSQL.ToString());
                        sbSQL.Remove(0, sbSQL.Length);
                        if (objRdr.Read())
                            sSubmittedTo = objRdr.GetString("LOGIN_NAME");
                    }
                    //Ended:Yukti,Dt:12/30/2013


                    objRdr.Close();

                    objNod = objRswXml.SelectSingleNode("//control[@name='WorksheetType']");
                    if (objNod != null)
                        sRSWType = objNod.Attributes["title"].InnerText;

                    //objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimNum']");
                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnClaimNumber']");
                    if (objNod != null)
                        sClaimNumber = objNod.InnerText;

                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnpcName']");
                    if (objNod != null)
                        sPCName = objNod.InnerText;

                    objNod = objRswXml.SelectSingleNode("//control[@name='hdnAppRejComm']");
                    if (objNod != null)
                    {
                        if (objNod.InnerText != "")
                            sReason = objNod.InnerText;
                    }
                    //skhare7 R8 Fn.Transffered to Common fns
                    CommonFunctions.GetUserDetails(sSubmittedTo, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sToEmail, ref sToUser, m_iClientId);
                    CommonFunctions.GetUserDetails(m_CommonInfo.UserName, m_CommonInfo.SecurityConnectionstring, m_CommonInfo.DSNID, ref sFromEmail, ref sFromUser, m_iClientId);
                    //skhare7 R8 Fn.Transffered to Common fns end 

                    string sBodyEmail = string.Empty;
                    //skhare7 Changed  R8 transfrd to Data model
                    //EmailFormat objEMail = null;
                    // objEMail = new EmailFormat(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword);
                    objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                    objReservecurrent = (ReserveCurrent)objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                    sBodyEmail = objReservecurrent.GetEmailFormat("RSWSheet_REJ", iClaimId);
                    //MITS 20181 Defects Removed 
                    // MITS 22442
                    if (sPCName != "" && sPCName != null)
                    {
                        sSubjectEmail = "The Reserve Worksheet for Claim " + sClaimNumber + " - " + sPCName + " Rejected";

                    }
                    else
                    {
                        sSubjectEmail = "The Reserve Worksheet for Claim " + sClaimNumber + " Rejected";
                    }

                    if (sLastModOn != null && sLastModOn != "")
                    {
                        DateTime dtLastModOn = Convert.ToDateTime(sLastModOn);
                        sLastModOn = dtLastModOn.ToShortDateString();

                    }
                    sBodyEmail = sBodyEmail.Replace("{RSW_TYPE}", "Claim Reserve Worksheet")
                                      .Replace("{REJECTED_BY}", sFromUser)
                                     .Replace("{SUBMITTED_DATE}", sLastModOn)
                                     .Replace("{USER_NAME}", sPCName)
                                     .Replace("{REASON}", sReason);
                    //MITS 20181 Defects Removed 
                    //    MITS 22442
                    //Changed by shobhana 
                    //Send a diary to the person who submitted the worksheet
                    string sRegarding = sClaimNumber + " * " + sPCName; // TODO: regarding
                    if (sSubmittedTo != m_CommonInfo.UserName)//smahajan6 - For MITS 13750 
                    {
                        //Start: rsushilaggar : 22-Mar-2010 Mits : 19624
                        //The appropriate user should be getting an email, not a diary. No diary should exist
                        if (!m_bDisableDiaryNotify)
                            GenerateDiary(sSubjectEmail, sBodyEmail, 2, sSubmittedTo, m_CommonInfo.UserName, sRegarding, iClaimId);
                        //Added:Yukti,DT:12/30/2013, MITS 33496
                        if (!bRejectRSW)
                        {
                            if (sToEmail != "" && sFromEmail != "" && !m_bDisableEmailNotify)
                                CommonFunctions.SendEmail(sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, base.ClientId);
                        }
                        //skhare7 R8 Fn.Transffered to Common fns 
                        //End : rsushilaggar
                        }
                    }
                }
            #endregion
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
               
                if (objRdr != null)
                    objRdr.Dispose();
                
                if (objReservecurrent != null)
                {
                    objReservecurrent.Dispose();

                }
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();

                }
               // objRswXml = null; mbahl3 JIRA[RMA 403]
                objNod = null;
                bRejectRSW = false;
                
            }
        }
  
      //Ended:Yukti,Dt:12/30/2013


        /// <summary>
        /// MGaba2:R6 :This function Prints a Worksheet.Changing the signature of this function to make it void
        /// </summary>
        /// <param name="p_sRSWID">RSW ID of the worksheet to be printed</param>
        /// <param name="p_objMemory">out parameter containing the PDF in byte format</param>
        /// <param name="p_IsApproval">Denotes whether being called when approving worksheet</param>
        /// <param name="p_spath">Document Path</param>
        /// <param name="p_sfileName">File Name</param>
        //public void PrintWorksheet(string p_sRSWID, out MemoryStream p_objMemory, bool p_IsApproval, string p_spath, string p_sfileName)
      public override void PrintWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)      
        {
            XmlDocument objRSWDoc = null;
            XmlNode objNode = null;

            DbReader objReader = null;
            StringBuilder sbSQL;

            string sFile = string.Empty;
            PrintWrapper objPrintWrapper = null;

            string sTitle = string.Empty;
            double dLineHeight = 0.0;

            string sText = string.Empty;
            string sClaimNumber = string.Empty;

            //Vaibhav for Safeway on 03/30/2009
            DirectoryInfo objDirectory = null;

            //Vaibhav: Safeway : 03/10/2209: Start
            //Populating the claim related information
            long lClaimID = 0;

            int iLOBCode = 0;
            int iReserveType = 0;
            int iClmTypeCode = 0;

            Claim objClaim = null;
            DataModelFactory objDataModelFactory = null;
            LocalCache objCache = null;

            ReserveFunds.structReserves[] arrReserves = null;

            string sLOB = string.Empty;
            string sReserveType = string.Empty;
            string[,] sResFinal = null;
            //Vaibhav: End

            //MGaba2:R6
            //Transferred code from business layer to application layer in case of click of Print button:Start
            string sRSWID = "0"; 
            MemoryStream objMemory = null;
            XmlElement objTempElement = null;
            string sPathForAttach = string.Empty;
            string sFileNameForAttach = string.Empty;
            XmlDocument objXmlIn = null;
            XmlDocument objXmlOut = null;
            bool bDocPathType = false;
            FileStorageManager objFileStorageManager = null;  
            //MGaba2:R6:End
            string sRswTypeToLaunch = string.Empty;  //MGaba2:MITS 22225
            string[] arrReserveIDs = null; //RMA-10213      achouhan3   Added for Mutlple Print Worksheet
            try
            {

                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString); 

                sbSQL = new StringBuilder();
                objRSWDoc = new XmlDocument();

                //MGaba2:R6:Start
                objNode = objXmlIn.SelectSingleNode("//control[@name='RSWId']");
                if (objNode != null)
                {
                    sRSWID = objNode.InnerText.Trim();
                }
                //MGaba2:R6:End

                //RMA-10213      achouhan3   Added for Mutlple Print Worksheet Starts
                arrReserveIDs = sRSWID.Split(',');
                objPrintWrapper = new PrintWrapper(m_iClientId);
                objPrintWrapper.StartDoc(false);
                dLineHeight = objPrintWrapper.GetTextHeight("W");
                //RMA-10213      achouhan3   Added for Mutlple Print Worksheet Ends

                //MGaba2:MITS 22225:Start
                //In case a WC worksheet is created as Generic but now user prints it with Customize settings
                //or worksheet for any lob other than wc is created as customize but now user prints it with Generic settings
                //Error should come
                //sbSQL.Append("SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + sRSWID);
                 //Foreach loop start to fetch Comma Seperated ResrveID
                foreach (string strReserveID in arrReserveIDs)
                {
                    sbSQL.Append("SELECT RSWXMLTYPE,RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + strReserveID);
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objReader.Read())
                    {
                        objRSWDoc.LoadXml(objReader.GetString("RSW_XML"));
                        sRswTypeToLaunch = objReader.GetString("RSWXMLTYPE");
                        if ((objRSWDoc.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText != "Approved" && objRSWDoc.SelectSingleNode("//control[@name='hdnRSWStatus']").InnerText != "History"))
                        {
                            //RMA-10213      achouhan3   Modified for Multiple Print Worksheet 
                            // if (!IsRSWAllowed(Convert.ToInt32(sRSWID), sRswTypeToLaunch, Convert.ToInt32(objRSWDoc.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                            if (!IsRSWAllowed(Convert.ToInt32(strReserveID), sRswTypeToLaunch, Convert.ToInt32(objRSWDoc.SelectSingleNode("//control[@name='hdnClaimId']").InnerText)))
                            {
                                throw new RMAppException(Globalization.GetString("ReserveWorksheet.ConfigurationChanged.Error", m_iClientId));
                            }
                        }
                    }//MGaba2:MITS 22225:End
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.PrintWorksheet.RSWNotFound", m_iClientId));
                    objReader.Close();
                    objReader.Dispose();

                //Title of the worksheet

                //objNode = objRSWDoc.SelectSingleNode("//control[@name='hdnClaimNum']");
                objNode = objRSWDoc.SelectSingleNode("//control[@name='hdnClaimNumber']");
                if (objNode != null)
                    sTitle += "Reserve Worksheet for " + objNode.InnerText;

                //Vaibhav: Safeway: Start
                objNode = objRSWDoc.SelectSingleNode("//control[@name='hdnClaimId']");
                if (objNode != null)
                    lClaimID = Conversion.ConvertStrToLong(objNode.InnerText);

                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(Conversion.ConvertObjToInt(lClaimID, base.ClientId));
                iLOBCode = objClaim.LineOfBusCode;
                iClmTypeCode = objClaim.ClaimTypeCode;
                sLOB = objCache.GetShortCode(iLOBCode);
                //Vaibhav: End

                    // RMA-10213     achouhan3   Added and Commented for Multiple Print Worksheet Starts
                    int iRsvIndex=Array.IndexOf(arrReserveIDs,strReserveID);
                    //objPrintWrapper = new PrintWrapper(m_iClientId);
                    //objPrintWrapper.StartDoc(false);
                    //dLineHeight = objPrintWrapper.GetTextHeight("W");
                    //CreateNewPage(true, sTitle, objPrintWrapper);
                    if (iRsvIndex==0)
                        CreateNewPage(true, sTitle, objPrintWrapper);
                    else
                        CreateNewPage(false, sTitle, objPrintWrapper);
                    // RMA-10213     achouhan3    Added and Commented for Multiple PrintWorksheet Ends
                    objPrintWrapper.PageLimit = 14700;

                objPrintWrapper.CurrentX = 100;
                objPrintWrapper.CurrentY = 600;

                string sGpName = string.Empty;

                //Render each 'group' element.
			//MGaba2: Mits 18534: Claim Info Section was missing
                XmlNode objCommonSection = objRSWDoc.SelectSingleNode("//commonsection");
                WriteText("", false, objPrintWrapper, sTitle, false, 9);
                if (objCommonSection != null)
                {
                    WriteText(objCommonSection.Attributes["title"].InnerText.ToUpper(), false, objPrintWrapper, sTitle, true, 9);
                    WriteText("", false, objPrintWrapper, sTitle, false, 9);
                    PrintAllWorksheets(objCommonSection, objRSWDoc, objPrintWrapper, sLOB, "ClaimInfo");
                }
                WriteText("", false, objPrintWrapper, sTitle, false, 9);
                foreach (XmlNode objGroup in objRSWDoc.SelectNodes("//group"))
                {
                    WriteText("", false, objPrintWrapper, sTitle, false, 9);
                    WriteText(objGroup.Attributes["title"].InnerText.ToUpper(), false, objPrintWrapper, sTitle, true, 9);

                    sGpName = objGroup.Attributes["name"].InnerText;
                    switch (sGpName)
                    {
                        //case "ClaimInfo":
                        //    PrintAllWorksheets(objGroup, objRSWDoc, objPrintWrapper, sLOB, sGpName);
                        //    break;
                        case "RESERVESUMMARY":
                            //***********MGaba2:R6:
                            PrintAllWorksheets(objGroup, objRSWDoc, objPrintWrapper, sLOB, sGpName);
                            
                            break;
                        default:
                            {
                               // arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                                //changed by shobhana customization of reserves
                                arrReserves = GetReservesTypeCustom(iLOBCode, iClmTypeCode, p_WorksheetRequest.RSWReservesXML, objRSWDoc);
                                for (int iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                                {
                                    iReserveType = arrReserves[iCnt].iReserveTypeCode;
                                    sReserveType = objCache.GetCodeDesc(iReserveType).Substring(0, 3);
                                    //MGaba2:R6:Reserve WorkSheet:Changed the comparison and sending reserve code rather than 
                                    //description of reserve code
                                    //if (sGpName == arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "") + sLOB)
                                    //PrintAllWorksheets(objGroup, objRSWDoc, objPrintWrapper, sLOB, sReserveType);
                                    if (sGpName == "ReserveType_" + arrReserves[iCnt].iReserveTypeCode)
                                        PrintAllWorksheets(objGroup, objRSWDoc, objPrintWrapper, sLOB, iReserveType.ToString());
                                }
                            }
                            break;
                        }
                    }
                    //RMA-10213      achouhan3   Added for Mutlple Print Worksheet
                    sTitle = String.Empty;
                }

                objPrintWrapper.EndDoc();

               

               sFile = TempFile;


               
                objMemory  = new MemoryStream();
                objPrintWrapper.GetPDFStream(sFile, ref objMemory);
                File.Delete(sFile);

                //MGaba2:R6:Changed the signatures of this function...
                //So need to fetch path and file name from xml in
                objNode = objXmlIn.SelectSingleNode("//control[@name='DocPath']");
                if (objNode != null)
                {
                    sPathForAttach = objNode.InnerText.Trim();
                }
                objNode = objXmlIn.SelectSingleNode("//control[@name='FileName']");
                if (objNode != null)
                {
                    sFileNameForAttach = objNode.InnerText.Trim();
                }

                //Data Stotage Type: File or database
                objNode = objXmlIn.SelectSingleNode("//control[@name='DocPathType']");
                if (objNode != null)
                {
                    bDocPathType =Convert.ToBoolean(Convert.ToInt32(objNode.InnerText.Trim()));
                }
                //MGaba2:R6:End

                //Priya Aggarwal: Document Imaging for Reserve Worksheet - start  
                //MGaba2:R6:Replaced variable names:p_spath with sPathForAttach and p_sfileName with sFileNameForAttach
                if (sPathForAttach != string.Empty)
                {
                    objFileStorageManager = new FileStorageManager(bDocPathType ? StorageType.DatabaseStorage : StorageType.FileSystemStorage, sPathForAttach, m_iClientId);
                    objFileStorageManager.StoreFile(objMemory, sFileNameForAttach);
                }
                //Priya Aggarwal: Document Imaging for Reserve Worksheet - end
                else
                { //MGaba2:R6:Transferred code from business layer to application layer in case of click of Print button
                    objXmlOut = new XmlDocument();                    
                    objTempElement = objXmlOut.CreateElement("RSW");
                    objXmlOut.AppendChild(objTempElement);
                    objTempElement = objXmlOut.CreateElement("File");
                    objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                    objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                    objXmlOut.FirstChild.AppendChild(objTempElement);
                    p_WorksheetResponse.OutputXmlString = objXmlOut.OuterXml;
                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                objPrintWrapper = null;
                objRSWDoc = null;
                objNode = null;
                objCache = null;
                objClaim = null;
                objDataModelFactory = null;
                //Vaibhav on 03/30/2009
                objDirectory = null;
                objFileStorageManager = null;
                objMemory = null;
              
            }
            //MGaba2:Changed the return type of function from string to void
            //return sFile;
        }

      //gagnihotri Reserve Approval
      public override void ListFundsTransactions(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
      {
          //List of Funds Transactions

          ReserveApproval objResApprove = null;
          XmlDocument objAllTrans = null;
          XmlDocument objXmlIn = null;

          try
          {
              //objResApprove = new ReserveApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, m_securityConnectionString, base.DSNID);
              objResApprove = new ReserveApproval(m_CommonInfo, m_iClientId);
              objResApprove.UserId = m_CommonInfo.UserId;
              objResApprove.UserName = m_CommonInfo.UserName;
              objXmlIn = new XmlDocument();
              objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
              //RMA-344     achouhan3       Enable Sorting for Reserve Worksheet Starts
              if (objXmlIn.SelectSingleNode("//IsSort") != null && objXmlIn.SelectSingleNode("//IsSort").InnerText=="True")
                  objResApprove.ListFundsTransactions(out objAllTrans, objXmlIn);
              else
                objResApprove.ListFundsTransactions(out objAllTrans, objXmlIn.SelectSingleNode("//ShowAllItems").InnerText);
              //RMA-344     achouhan3       Enable Sorting for Reserve Worksheet Starts

              if (objAllTrans != null)
              {
                  if (objAllTrans.SelectSingleNode("//ReserveApproval/Approvals") != null)
                  {
                      if (objAllTrans.SelectSingleNode("//ReserveApproval/Approvals").InnerXml.Length > 0)
                      {
                          objXmlIn.SelectSingleNode("//ReserveApprovals").InnerXml = objAllTrans.SelectSingleNode("//ReserveApproval").InnerXml;
                      }
                  }
              }
              p_WorksheetResponse.OutputXmlString = objXmlIn.SelectSingleNode("//ReserveApprovals").OuterXml.ToString();
          }
          catch (RMAppException p_objException)
          {
              throw p_objException;
              //return false;
          }
          catch (Exception p_objException)
          {
              throw new Exception(Globalization.GetString("ReserveApprovalAdaptor.ListFundsTransactions.Error", m_iClientId), p_objException);
              //p_objErrOut.Add(p_objException, Globalization.GetString("ReserveApprovalAdaptor.ListFundsTransactions.Error"), BusinessAdaptorErrorType.Error);
              //return false;
          }
          finally
          {
              objResApprove.Dispose(); // important to kill DataModelFactory
              objResApprove = null;
              objAllTrans = null;
              objXmlIn = null;
          }
          //return true;
      }


      public override void ApproveReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
      {
          //ReserveWorksheet objResWS = null;
          ReserveApproval objResApprove = null;
         // ReserveWorksheetCustomize objResCustomize = null;
        //  string sRSWType = "";
          string sCallFor = "";
          string sClaimIncCallFor = "";//sachin 6385
          string sHoldReason = string.Empty;//sachin 6385
          StringBuilder sbXmlBuilder = null;
          XmlNode objNod = null;
          string sReason = "";
          XmlDocument p_objXmlIn = null;
          XmlDocument objXmlIn = null;
          XmlDocument p_objXmlOut = null;
          string sShowAllItem = string.Empty;
          //RAM-344 achouhan3   Added to Enable Sorting Starts
          string IsSort = String.Empty;
          XmlDocument p_Temp_objXmlIn = null;
          LocalCache objCache = null;//sachin 6385
          objCache = new LocalCache(m_CommonInfo.Connectionstring, ClientId);//sachin 6385
          //RAM-344 achouhan3   Added to Enable Sorting Starts
          //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Starts
          StringBuilder strException = new StringBuilder();
          string strReservrIDs = String.Empty;
          p_objXmlOut = new XmlDocument();

          //achouhan3   ML Changes
          base.m_baseUserlogin = p_objLogin;
          string IsJson=String.Empty; //RMA-10217 Added to check whether request from new screen
          //StringBuilder strCallFor = new StringBuilder();
          //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Ends
          try
          {
              //objResWS = new ReserveWorksheet(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objUser.UserId, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.securityConnectionString, base.DSNID, base.userLogin.objUser.ManagerId);
              p_objXmlIn = new XmlDocument();
              p_objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
              //rejection comments
              objNod = p_objXmlIn.SelectSingleNode("//AppRejReqCom");
              if (objNod != null)
                  sReason = p_objXmlIn.SelectSingleNode("//AppRejReqCom").InnerText;

              //rsushilaggar MITS 26332 date 11/28/2011
              sShowAllItem = p_objXmlIn.SelectSingleNode("//ShowAllItems").InnerText;
              //RMA=344     achouhan3    Added to Enable Sorting Starts
              if (p_objXmlIn.SelectSingleNode("//IsSort") != null)
              {
                  IsSort = p_objXmlIn.SelectSingleNode("//IsSort").InnerText;
                  p_Temp_objXmlIn = new XmlDocument();
                  p_Temp_objXmlIn.LoadXml(p_objXmlIn.InnerXml);
              }
              //RMA=344     achouhan3    Added to Enable Sorting Starts
              //RMA-10217 added by achouhan3 for showing error message 
              if (p_objXmlIn.SelectSingleNode("//IsJson") != null)
              {
                IsJson=p_objXmlIn.SelectSingleNode("//IsJson").InnerText;
              }

              //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Starts

              //sharishkumar Jira 6385
              objNod = p_objXmlIn.SelectSingleNode("//ActionType");
              if (objNod != null)
              {
                  objNod.InnerText = "Approve";
              }           
              //sharishkumar Jira 6385

              XmlElement oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RswId");
              //XmlElement oClaimElement = (XmlElement)p_objXmlIn.SelectSingleNode("//control[@name='hdnClaimId");
              //if (oClaimElement != null)
              //{
              //    arrClaim = oClaimElement.InnerText.Split(',');
              //}
              if (oElement != null && !String.IsNullOrEmpty(oElement.InnerText))
              {
                  strReservrIDs = oElement.InnerText;
                  foreach (string strResvID in oElement.InnerText.Split(','))
                  {
                      try
                      {   
                          //if (arrClaim.Length > 0)
                          //    oClaimElement.InnerText = arrClaim[iClaimIndex].ToString();
                          oElement.InnerText = strResvID;
                          p_WorksheetRequest.InputXmlString = p_objXmlIn.InnerXml;
                          SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                          p_objXmlOut.LoadXml(p_WorksheetResponse.OutputXmlString);

                          //rejection comments
                          objNod = p_objXmlOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                          if (objNod != null)
                              objNod.InnerText = sReason;

                          p_WorksheetRequest.InputXmlString = p_objXmlOut.InnerXml;
                          //sachin 6385 starts
                          if (((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']") == null) 
                              || (p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"] == null) 
                              || (Conversion.ConvertStrToInteger(p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value) == 0)
                              || (Conversion.ConvertStrToInteger(p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value) == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId)))
                              && (!p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value.Contains(",")))
                          {
                              if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                              {
                                  sCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                                  // strCallFor.Append(sCallFor);
                                  // strCallFor.Append(",");
                                  if (sCallFor == "Yes")
                                      //Changes reqd. PLEASE NOTE
                                      //p_objXmlOut = objResWS.ApproveWorksheet(p_objXmlOut, base.userID, base.groupID, base.userLogin, base.userLogin.objRiskmasterDatabase.DataSourceId, "Pending Approval");
                                      //  if (sRSWType == "Generic")
                                      ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                                  else if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")//RMA-10217 added by achouhan3 for showing error message 
                                  {
                                     // strException.Append(strResvID);
                                      strException.Append(strResvID  + "~" +  objCache.GetCodeDesc(Conversion.ConvertObjToInt((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value),m_iClientId)));
                                      strException.Append(",");
                                  }
                                  //ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                                  //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Ends
                              }
                          }
                          else if ((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"] != null
                              && Conversion.ConvertStrToInteger(p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value) == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId)))
                          {
                              if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                              sClaimIncCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText;
                              // strCallFor.Append(sCallFor);
                              // strCallFor.Append(",");
                              if (sClaimIncCallFor == "Yes")
                                  //Changes reqd. PLEASE NOTE
                                  //p_objXmlOut = objResWS.ApproveWorksheet(p_objXmlOut, base.userID, base.groupID, base.userLogin, base.userLogin.objRiskmasterDatabase.DataSourceId, "Pending Approval");
                                  //  if (sRSWType == "Generic")
                                  ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                              else if (sClaimIncCallFor == "No")
                              {
                                  sCallFor = sClaimIncCallFor;
                                  if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")//RMA-10217 added by achouhan3 for showing error message 
                                  {
                                      strException.Append(strResvID + "~" +  objCache.GetCodeDesc(Conversion.ConvertObjToInt((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value),m_iClientId)));
                                      strException.Append(",");
                                  }
                              }
                          }
                          else if((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"] != null) && (p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value.Contains(",")))
                          {
                              if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)                           
                                  sCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                              if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                                  sClaimIncCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText;

                              sHoldReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId)) + " and " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));

                              if (sCallFor== "Yes" && sClaimIncCallFor== "Yes")
                                  ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                              else if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")//RMA-10217 added by achouhan3 for showing error message 
                              {
                                  strException.Append(strResvID + "~" + sHoldReason);
                                  strException.Append(",");
                              }
                          }
                          //sachin 6385 ends
                          // else
                          //   objResCustomize.ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                      }
                      catch (RMAppException p_objException)
                      {
                          //RMA-10217 added by achouhan3 for showing error message
                          if ((!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE"))
                          {
                              strException.AppendLine(strResvID + "~" + p_objException.Message);
                              strException.Append(",");
                          }
                          else
                              throw new RMAppException(p_objException.Message);
                      }
                    }
                }
              p_objXmlOut = null;

              //objResApprove = new ReserveApproval(base.connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.m_securityConnectionString, base.DSNID);
              objResApprove = new ReserveApproval(m_CommonInfo, m_iClientId);
              objResApprove.UserId = m_CommonInfo.UserId;
              objResApprove.UserName = m_CommonInfo.UserName;
              objXmlIn = new XmlDocument();
              objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
              //RMA-344         achouhan3     function calling to apply sorting
              if (IsSort.Trim() == "True")
                  objResApprove.ListFundsTransactions(out p_objXmlOut, p_Temp_objXmlIn);
              else
                  p_objXmlOut = objResApprove.ListFundsTransactions(out p_objXmlOut, sShowAllItem);
              // p_objXmlOut = objResApprove.ListFundsTransactions(out p_objXmlOut, sShowAllItem);
              //RMA-344         achouhan3     function calling to apply sorting
              //p_objXmlOut = objResApprove.ListFundsTransactions(out ,"");

              sbXmlBuilder = new StringBuilder();
              sbXmlBuilder.Append("<ReserveApproval>");

              sbXmlBuilder.Append("<CallFor>");
              if ((!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")) //RMA-10217 added by achouhan3 for showing error message
                sbXmlBuilder.Append(strException.ToString().TrimEnd(','));
              else
                  sbXmlBuilder.Append(sCallFor);
              sbXmlBuilder.Append("</CallFor>");

              sbXmlBuilder.Append("<ReserveApprovals>");
              if (p_objXmlOut != null)
              {
                  if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals") != null)
                  {
                      if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals").InnerXml.Length > 0)
                      {
                          sbXmlBuilder.Append(p_objXmlOut.SelectSingleNode("//ReserveApproval").InnerXml);
                      }
                  }
              }
              sbXmlBuilder.Append("</ReserveApprovals>");
              sbXmlBuilder.Append("</ReserveApproval>");

              //p_objXmlOut = null;
              //p_objXmlOut = new XmlDocument();
              //p_objXmlOut.LoadXml(sbXmlBuilder.ToString());
              p_WorksheetResponse.OutputXmlString = sbXmlBuilder.ToString();

              //return true;
          }
          catch (RMAppException p_objException)
          {
              //By Vaibhav for Safeway RSW MITS 13865
              if (p_objException.Message == "Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.")
              {
                  //p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Warning);
                  throw new Exception("Reserves exceed your authority limits. The worksheet is being sent to your supervisor for approval.", p_objException);
                  //return true;
              }
              else
                  //p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                  throw p_objException;
              //Vaibhav code end
              //return false;
          }
          catch (Exception p_objException)
          {
              //p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.ApproveReserveWorksheet.Error"), BusinessAdaptorErrorType.Error);
              throw new Exception(Globalization.GetString("ReservesAdaptor.ApproveReserveWorksheet.Error", m_iClientId), p_objException);
              //return false;
          }
          finally
          {
//sachin 6385 starts
              if (objCache != null)
              {
                  objCache.Dispose();
                  objCache = null;
              }
              //sachin 6385 ends
              //RMA-5566    achouhan3   Nullify String Builder
              //strCallFor.Remove(0, strCallFor.Length);
              strException.Remove(0, strException.Length);
          }
      }


      /// <summary>
      ///  This function is a wrapper over Riskmaster.Application.ReservWorksheet.RejectWorksheet() function.
      ///  This function rejects the Reserve Worksheet.
      ///  It can be only called when a worksheet is pending for approval from Supervisor
      ///  SAFEWAY: 11/07/2008: Vaibhav Mathur
      /// </summary>
      public override void RejectReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
      {
          //ReserveWorksheet objResWS = null;
          ReserveApproval objResApprove = null;
          string sHoldReason = string.Empty;//sachin 6385
          string sCallFor = "";
          string sClaimIncCallFor = "";//sachin 6385
          StringBuilder sbXmlBuilder = null;
          XmlNode objNod = null;
          //ReserveWorksheetCustomize objResCustomize = null;
         // string sRSWType = "";
          string sReason = "";
          XmlDocument p_objXmlIn = null;
          XmlDocument p_objXmlOut = null;
          string sShowAllItem = string.Empty;
          //RAM-344 achouhan3   Added to Enable Sorting Starts
          string IsSort = String.Empty;
          string IsJson = String.Empty; //RMA-10217 Added to check whether request from new screen
          XmlDocument p_Temp_objXmlIn = null;
          LocalCache objCache = null;//sachin 6385
          objCache = new LocalCache(m_CommonInfo.Connectionstring, ClientId);//sachin 6385
          //RAM-344 achouhan3   Added to Enable Sorting Starts
		   p_objXmlOut = new XmlDocument();
		    //RAM-344 achouhan3   Added to Enable Sorting Starts
          //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Starts
          string strReservrIDs = String.Empty;
          p_objXmlOut = new XmlDocument();
          StringBuilder strException = new StringBuilder();
          StringBuilder strCallFor = new StringBuilder();
          //achouhan3   ML Changes
          base.m_baseUserlogin = p_objLogin;
          //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Ends
          try
          {
              //objResWS = new ReserveWorksheet(base.userLogin.LoginName, base.userLogin.Password, base.userLogin.objUser.UserId, base.userLogin.objRiskmasterDatabase.DataSourceName, base.connectionString, base.securityConnectionString, base.DSNID, base.userLogin.objUser.ManagerId);
              p_objXmlIn = new XmlDocument();
              p_objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
              //Mohit Yadav
              //rejection comments
            //  sRSWType = GetRswTypeToLaunch(p_WorksheetRequest.ClaimId);
              objNod = p_objXmlIn.SelectSingleNode("//AppRejReqCom");
              if (objNod != null)
                  sReason = p_objXmlIn.SelectSingleNode("//AppRejReqCom").InnerText;
           //   if (sRSWType == "Generic")
              //rsushilaggar MITS 26332 date 11/28/2011
              sShowAllItem = p_objXmlIn.SelectSingleNode("//ShowAllItems").InnerText;
              //RMA=344     achouhan3    Added to Enable Sorting Starts
              if (p_objXmlIn.SelectSingleNode("//IsSort") != null)
              {
                  IsSort = p_objXmlIn.SelectSingleNode("//IsSort").InnerText;
                  p_Temp_objXmlIn = new XmlDocument();
                  p_Temp_objXmlIn.LoadXml(p_objXmlIn.InnerXml);
              }
              //RMA=344     achouhan3    Added to Enable Sorting Starts
              //RMA-10217 added by achouhan3 for showing error message 
              if (p_objXmlIn.SelectSingleNode("//IsJson") != null)
              {
                  IsJson = p_objXmlIn.SelectSingleNode("//IsJson").InnerText;
              }
              //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Starts
              //sharishkumar Jira 6385
              objNod = p_objXmlIn.SelectSingleNode("//ActionType");
              if (objNod != null)
              {
                  objNod.InnerText = "Reject";
              }
              //sharishkumar Jitra 6385
              XmlElement oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RswId");
              if (oElement != null && !String.IsNullOrEmpty(oElement.InnerText))
              {
                  
                      strReservrIDs = oElement.InnerText;
                      foreach (string strResvID in oElement.InnerText.Split(','))
                      {
                          try
                          {   
                              oElement.InnerText = strResvID;
                              p_WorksheetRequest.InputXmlString = p_objXmlIn.InnerXml;
                              SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                              //   else
                              //   objResCustomize.SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                              //p_objXmlOut = objResWS.SubmitWorksheet(p_objXmlIn, base.userID, base.groupID, base.userLogin.objRiskmasterDatabase.DataSourceId, sCallFor, base.userLogin);
                              // SubmitWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                              p_objXmlOut.LoadXml(p_WorksheetResponse.OutputXmlString);

                              //rejection comments
                              objNod = p_objXmlOut.SelectSingleNode("//control[@name='hdnAppRejComm']");
                              if (objNod != null)
                                  objNod.InnerText = sReason;

                              p_WorksheetRequest.InputXmlString = p_objXmlOut.InnerXml;
                              //sachin 6385 starts
                              if (((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']") == null)
                                                         || (p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"] == null)
                                                         || (Conversion.ConvertStrToInteger(p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value) == 0)
                                                         || (Conversion.ConvertStrToInteger(p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value) == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId)))
                                                         && (!p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value.Contains(",")))
                              {
                                  if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                  {
                                      sCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                                      // strCallFor.Append(sCallFor);
                                      // strCallFor.Append(",");
                                      if (sCallFor == "Yes")
                                          //Changes reqd. PLEASE NOTE
                                          //p_objXmlOut = objResWS.ApproveWorksheet(p_objXmlOut, base.userID, base.groupID, base.userLogin, base.userLogin.objRiskmasterDatabase.DataSourceId, "Pending Approval");
                                          //  if (sRSWType == "Generic")
                                          RejectWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId);
                                      else if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")//RMA-10217 added by achouhan3 for showing error message 
                                      {
                                          strException.Append(strResvID + "~" + objCache.GetCodeDesc(Conversion.ConvertObjToInt((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value), m_iClientId)));
                                          strException.Append(",");
                                      }
                                      //ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);
                                      //RMA-5566        achouhan3       Allow Multiple Selection for Reserve Worksheet Approval from Reserve Worksheet Approval Screen Ends
                                  }
                              }
                              else if ((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"] != null
                                  && Conversion.ConvertStrToInteger(p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value) == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId)))
                              {
                                  sClaimIncCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText;
                                  // strCallFor.Append(sCallFor);
                                  // strCallFor.Append(",");
                                  if (sClaimIncCallFor == "Yes")
                                      //Changes reqd. PLEASE NOTE
                                      //p_objXmlOut = objResWS.ApproveWorksheet(p_objXmlOut, base.userID, base.groupID, base.userLogin, base.userLogin.objRiskmasterDatabase.DataSourceId, "Pending Approval");
                                      //  if (sRSWType == "Generic")
                                      RejectWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId);
                                  else if (sClaimIncCallFor == "No")
                                  {
                                      sCallFor = sClaimIncCallFor;
                                      if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")//RMA-10217 added by achouhan3 for showing error message 
                                      {
                                          strException.Append(strResvID + "~" + objCache.GetCodeDesc(Conversion.ConvertObjToInt((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value), m_iClientId)));
                                          strException.Append(",");
                                      }
                                  }
                              }
                              //sachin 6385 ends
                              // else
                              //   objResCustomize.ApproveReserveWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId, p_objLogin);                          
                              else if ((p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"] != null) && (p_objXmlOut.SelectSingleNode("//control[@name='hdnHoldStatus']").Attributes["Holdcodeid"].Value.Contains(",")))
                              {
                                  if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']") != null)
                                      sCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderLimit']").InnerText;
                                  if (p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']") != null)
                                      sClaimIncCallFor = p_objXmlOut.SelectSingleNode("//control[@name='hdnUnderClaimIncLimit']").InnerText;

                                  sHoldReason = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId)) + " and " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));

                                  if (sCallFor == "Yes" && sClaimIncCallFor == "Yes")
                                      RejectWorksheet(p_WorksheetRequest, ref p_WorksheetResponse, p_iGroupId);
                                  else if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")//RMA-10217 added by achouhan3 for showing error message 
                                  {
                                      strException.Append(strResvID + "~" + sHoldReason);
                                      strException.Append(",");
                                  }
                              }
                          }
                          catch (RMAppException p_objException)
                          {
                              //RMA-10217 added by achouhan3 for showing error message 
                              if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")
                              {
                                  strException.Append(strResvID + "~" + p_objException.Message);
                                  strException.Append(",");
                              }
                              else
                                  throw new RMAppException(p_objException.Message);
                             // throw new RMAppException(p_objException.Message);
                          }
                      }
                  
              }
              p_objXmlOut = null;

              //objResApprove = new ReserveApproval(base.connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.m_securityConnectionString, base.DSNID);
              objResApprove = new ReserveApproval(m_CommonInfo, m_iClientId);
              objResApprove.UserId = m_CommonInfo.UserId;
              objResApprove.UserName = m_CommonInfo.UserName;
              p_objXmlIn.LoadXml(p_WorksheetRequest.InputXmlString);
              //RMA-344         achouhan3     function calling to apply sorting
              if (IsSort.Trim() == "True")
                  objResApprove.ListFundsTransactions(out p_objXmlOut, p_Temp_objXmlIn);
              else
                  p_objXmlOut = objResApprove.ListFundsTransactions(out p_objXmlOut, sShowAllItem);
              //p_objXmlOut = objResApprove.ListFundsTransactions(out p_objXmlOut, sShowAllItem);
              //RMA-344         achouhan3     function calling to apply sorting

              sbXmlBuilder = new StringBuilder();
              sbXmlBuilder.Append("<ReserveApproval>");

              sbXmlBuilder.Append("<CallFor>");
              //RMA-10217 added by achouhan3 for showing error message 
              if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")
                  sbXmlBuilder.Append(strException.ToString().TrimEnd(','));
              else
                  sbXmlBuilder.Append(sCallFor);
              sbXmlBuilder.Append("</CallFor>");

              sbXmlBuilder.Append("<ReserveApprovals>");
              if (p_objXmlOut != null)
              {
                  if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals") != null)
                  {
                      if (p_objXmlOut.SelectSingleNode("//ReserveApproval/Approvals").InnerXml.Length > 0)
                      {
                          sbXmlBuilder.Append(p_objXmlOut.SelectSingleNode("//ReserveApproval").InnerXml);
                      }
                  }
              }
              sbXmlBuilder.Append("</ReserveApprovals>");
              sbXmlBuilder.Append("</ReserveApproval>");

              //p_objXmlOut = null;
              //p_objXmlOut = new XmlDocument();
              //p_objXmlOut.LoadXml(sbXmlBuilder.ToString());
              p_WorksheetResponse.OutputXmlString = sbXmlBuilder.ToString();

          }
          catch (RMAppException p_objException)
          {
              //By Vaibhav for Safeway RSW MITS 13865
              if (p_objException.Message == "You do not have the authority to reject this worksheet.")
              {
                  //p_objErrOut.Add(p_objException, "You do not have the authority to reject this worksheet. Please use \"Open Worksheet \" then submit it from the reserve worksheet screen.", BusinessAdaptorErrorType.PopupMessage);
                  throw new Exception("You do not have the authority to reject this worksheet. Please use \"Open Worksheet \" then submit it from the reserve worksheet screen.", p_objException);
              }
              else
              {
                  //p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                  throw p_objException;

              }
              //Vaibhav code end
              //return false;
          }
          catch (Exception p_objException)
          {
              //p_objErrOut.Add(p_objException, Globalization.GetString("ReservesWorksheetAdaptor.RejectReserveWorksheet.Error"), BusinessAdaptorErrorType.Error);
              throw new Exception(Globalization.GetString("ReservesWorksheetAdaptor.RejectReserveWorksheet.Error", m_iClientId), p_objException);
              //return false;
          }
          finally
          {
              //sachin 6385 starts
              if (objCache != null)
              {
                  objCache.Dispose();
                  objCache = null;
              }
              //sachin 6385 ends
			  strCallFor.Remove(0, strCallFor.Length);
              strException.Remove(0, strException.Length);
          }
          //return true;
      }
        #endregion


        #region PrivateFunctions

        #region Functions to Create new dynamic Xml
      //changed parameters for custmization by shobhana

      private XmlDocument CreateNewXml(int p_iClaimdId, int p_iClaimantRowId, int p_iUnitRowId, string p_sRSWCustomReserveXML)
        {
            ReserveFunds.structReserves[] arrReserves = null;
            XmlDocument workSheetDoc = null;
            XmlElement objClaimInfoNode = null;
            XmlElement objFormElement = null;
            XmlElement objTempElement = null;
            XmlElement objReserveSummaryNode = null;
            StringBuilder strControlToEditList = null;
            StringBuilder strControlToClearList = null;
            StringBuilder reserveTypesList = null;
            bool reasonCommentsRequired = true;
            StringBuilder strSql = null;
            DbReader objReader = null;            

            try
            {
                workSheetDoc = new XmlDocument();

                objFormElement = workSheetDoc.CreateElement("form");

                //Adding claim info node to form node
                objClaimInfoNode = GetClaimInfoNode(workSheetDoc, p_iClaimdId);

                XmlElement objClaimInfoElement = workSheetDoc.CreateElement("commonsection");
                objClaimInfoElement.SetAttribute("name", "ClaimInfo");
                objClaimInfoElement.SetAttribute("title", "CLAIM INFORMATION");
                CreatingClaimInfoControls(workSheetDoc, objClaimInfoNode, ref objClaimInfoElement, p_iClaimdId);

                objFormElement.AppendChild(objClaimInfoElement);
                
                //Gettting Reserve Types
                 arrReserves = GetRequiredReserveTypeArray(p_iClaimdId, p_sRSWCustomReserveXML);

                strControlToEditList = new StringBuilder();
                strControlToClearList = new StringBuilder();
                reserveTypesList = new StringBuilder();

                //Reading Utility Settings for LOB Reserve Options
                GetLOBReserveOptions(p_iClaimdId);

                //nnorouzi; MITS 18989; See if Reason and Comments are required
                strSql = new StringBuilder();
                strSql.Append(string.Format("SELECT {0} FROM {1} WHERE {2}", "REASON_COMMENTS_RSV_WK", "SYS_PARMS_LOB, CLAIM", "SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE AND CLAIM.CLAIM_ID = " + p_iClaimdId));
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSql.ToString());
                if (objReader != null && objReader.Read())
                {
                    reasonCommentsRequired = Conversion.ConvertObjToBool(objReader[0], base.ClientId);
                }
                else
                {
                    reasonCommentsRequired = true;
                }
                

                for (int iCnt = 0; iCnt < arrReserves.GetLength(0); iCnt++)
                {
                    //GetReserveType Node and append the same with formNode
                    objTempElement = GetReserveTypeNode(workSheetDoc, objClaimInfoNode, arrReserves[iCnt].iReserveTypeCode, arrReserves[iCnt].sReserveDesc, arrReserves[iCnt].sTableValue, arrReserves[iCnt].iReserveTypeId, p_iClaimdId, p_iClaimantRowId, p_iUnitRowId, ref strControlToEditList, ref strControlToClearList, reasonCommentsRequired, p_sRSWCustomReserveXML);

                    if (objTempElement != null)
                    {
                        if (iCnt == 0)
                        {
                            objTempElement.SetAttribute("selected", "1");
                        }
                        
                        reserveTypesList.Append(objTempElement.GetAttribute("name").ToString() + ",");
                        objFormElement.AppendChild(objTempElement);
                    }

                    objTempElement = null;
                }

                objReserveSummaryNode = GetReserveSummaryNode(workSheetDoc, objClaimInfoNode, p_iClaimdId, p_iClaimantRowId, p_iUnitRowId, ref strControlToClearList, ref strControlToEditList, reasonCommentsRequired);
                
                //By Nitin,if No ReserveType has been created for a Claim then ReserveSummary should be come as selected
                if (reserveTypesList.ToString().Length == 0)
                {
                    objReserveSummaryNode.SetAttribute("selected", "1");
                }
                objFormElement.AppendChild(objReserveSummaryNode);

                //populating the hidden field values
                objFormElement.SelectSingleNode("//control[@name='hdnCtrlToClear']").InnerText = strControlToClearList.ToString();
                objFormElement.SelectSingleNode("//control[@name='hdnCtrlToEdit']").InnerText = strControlToEditList.ToString();
                //Mits 20147
                if (objFormElement.SelectSingleNode("//control[@name='hdnPrevModValtoZero']") != null)
                    objFormElement.SelectSingleNode("//control[@name='hdnPrevModValtoZero']").InnerText = bPrevModValtToZero.ToString();
                //Mits 20147 skhare7
                objFormElement.SelectSingleNode("//control[@name='hdnReserveTypeTabs']").InnerText = reserveTypesList.ToString();
                objFormElement.SelectSingleNode("//control[@name='SupName']").InnerText = objClaimInfoNode.SelectSingleNode("//control[@name='txt_ClaimInfo_SupervisorName']").Attributes["title"].Value;
                objFormElement.SelectSingleNode("//control[@name='hdnPCName']").InnerText = objClaimInfoNode.SelectSingleNode("//control[@name='txt_ClaimInfo_PCName']").Attributes["title"].Value;
                objFormElement.SelectSingleNode("//control[@name='hdnClaimNumber']").InnerText = objClaimInfoNode.SelectSingleNode("//control[@name='txt_ClaimInfo_ClaimNumber']").Attributes["title"].Value;

                workSheetDoc.AppendChild(objFormElement);
                return workSheetDoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private XmlElement GetClaimInfoNode(XmlDocument workSheetDoc, int p_iClaimId)
        {
            Claim objClaim = null;
            Claimant objPrimaryClaimant = null;
            ClaimAdjuster objCurAdj = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            XmlElement objClaimInfoElement = null;
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "half123";
            string txtCssClass = "half";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string sSQL = string.Empty;

            try
            {
                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                objPrimaryClaimant = objClaim.PrimaryClaimant;
                objCurAdj = objClaim.CurrentAdjuster;
                objEvent = (objClaim.Parent as Event);


                objClaimInfoElement = GetNewGroupElementForWorkSheet(workSheetDoc, "ClaimInfo", 0, "CLAIM INFORMATION");

                //claim information data
                //line break
                objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, "ClaimInfo_linebreak1");
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //for claim number
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Claim Number :";
                strName = "lbl_ClaimInfo_ClaimNumber";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                strTitle = objClaim.ClaimNumber;
                strName = "txt_ClaimInfo_ClaimNumber";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Claimant :";
                strName = "lbl_ClaimInfo_ClaimantName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                if (objPrimaryClaimant.ClaimantEntity != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                else
                {
                    strTitle = "";
                }

                strName = "txt_ClaimInfo_ClaimantName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //date of Event
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Date of Event :";
                strName = "lbl_ClaimInfo_DateOfEvent";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                strTitle = Conversion.GetDBDateFormat(objEvent.DateOfEvent, "d").ToString();
                strName = "txt_ClaimInfo_DateOfEvent";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //date of Claim
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Date of Claim :";
                strName = "lbl_ClaimInfo_DateOfClaim";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;


                strTitle = Conversion.GetDBDateFormat(objClaim.DateOfClaim, "d").ToString();
                strName = "txt_ClaimInfo_DateOfClaim";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;


                //new display column for Supervisor Name
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

                strTitle = "Supervisor :";
                strName = "lbl_ClaimInfo_SupervisorName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;

                strTitle = GetSupervisorName();
                strName = "txt_ClaimInfo_SupervisorName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;


                //new display column for Primary Claimant
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

                strTitle = "Primary Claimant :";
                strName = "lbl_ClaimInfo_PCName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;


                //Primary Claimant textbox
                if (objPrimaryClaimant.ClaimantEntity != null)
                {
                    if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim() + ", " + objPrimaryClaimant.ClaimantEntity.FirstName.Trim();
                    else
                        strTitle = objPrimaryClaimant.ClaimantEntity.LastName.Trim();
                }
                else
                {
                    strTitle = "";
                }

                strName = "txt_ClaimInfo_PCName";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                //Adjuster
                objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                strTitle = "Adjuster :";
                strName = "lbl_ClaimInfo_Adjuster";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;


                //Adjuster Name
                if (objCurAdj.AdjusterEntity != null)
                {
                    if (objCurAdj.AdjusterEntity.LastName != "" && objCurAdj.AdjusterEntity.FirstName != "")
                        strTitle = objCurAdj.AdjusterEntity.LastName + ", " + objCurAdj.AdjusterEntity.FirstName;
                    else
                        strTitle = objCurAdj.AdjusterEntity.LastName;
                }
                else
                {
                    strTitle = "";
                }

                strName = "txt_ClaimInfo_Adjuster";
                strRef = "//control[@name='" + strName + "']";
                strType = "labelonly";
                strReadOnly = "true";
                objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
                objDisplayColElement.AppendChild(objControlElement);
                objControlElement = null;
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, "ClaimInfo_linebreak2");
                objClaimInfoElement.AppendChild(objDisplayColElement);
                objDisplayColElement = null;

                return objClaimInfoElement;
            }
            catch (Exception ex)
            {
                throw new RMAppException("Error occured while getting claim information",ex.InnerException);
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
                if (objPrimaryClaimant != null)
                    objPrimaryClaimant.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
            }
        }

        private XmlElement GetReserveSummaryNode(XmlDocument workSheetDoc, XmlElement claimInfoElement, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId, ref StringBuilder strControlToClear, ref StringBuilder strControlToEdit, bool reasonCommentsRequired)
        {
            XmlElement objReserveSummaryNode = null;
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "half123";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;
            DbReader objReader=null;
            StringBuilder objSql = null;

            //int iClaimCurrCode = 0;    //Aman Multi Currency--Start
            //int iBaseCurrCode = 0;            
            //LocalCache objCache = null;
            //string sShortCode = string.Empty;
            //string sDesc = string.Empty;

            //objSysSetting = new SysSettings(m_CommonInfo.Connectionstring);
            //if (objCache == null)
            //{
            //    objCache = new LocalCache(m_CommonInfo.Connectionstring);
            //}
            //iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_CommonInfo.Connectionstring);
            //iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_CommonInfo.Connectionstring);
            //if (iClaimCurrCode > 0)
            //{
            //    objCache.GetCodeInfo(iClaimCurrCode, ref sShortCode, ref sDesc);
            //}
            //else
            //{
            //    objCache.GetCodeInfo(iBaseCurrCode, ref sShortCode, ref sDesc);
            //}

            //Aman Multi Currency --end

            objReserveSummaryNode = GetNewGroupElementForWorkSheet(workSheetDoc, "RESERVESUMMARY", 0, "RESERVE SUMMARY");
            objReserveSummaryNode.SetAttribute("onclick", "CreateReserveSummary()");
            
            //getting all hiddend fileds information and appending the same to Reserve Summary Node
            objDisplayColElement = GetHiddenFieldsNode(workSheetDoc, p_iClaimId,p_iClaimantRowId,p_iUnitRowId);
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Creating a blank line
            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, objReserveSummaryNode.GetAttribute("name").ToString() + "_linebreak1");
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;
            
            //Creating Static Claim Information Fields and appending the same to ReserveSummary node
            //CreatingClaimInfoControls(workSheetDoc, claimInfoElement, ref objReserveSummaryNode,p_iClaimId);

            //Creating a blank line
            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, objReserveSummaryNode.GetAttribute("name").ToString() + "_linebreak2");
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Creating blank div , that will store dynamically created Table for Summary
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
            strTitle = "";
            strName = "divReserveSummary";
            strRef = "//control[@name='" + strName + "']";
            strType = "divcontainer";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //creating Comments and Reason controls

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, objReserveSummaryNode.GetAttribute("name").ToString() + "_linebreak3");
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for Comments
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Comments   ";
            strName = "lbl_" + objReserveSummaryNode.GetAttribute("name").ToString() + "_Comments";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            lblCssClass = "ReserveSummaryComments";
            if (reasonCommentsRequired) //nnorouzi; MITS 18989
            {
                lblCssClass += " required";
            }
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + objReserveSummaryNode.GetAttribute("name").ToString() + "_Comments";
            strControlToClear.Append(strName + ",");
            strControlToEdit.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "memo";
            strReadOnly = "false";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("cols", "84");
            objControlElement.SetAttribute("rows", "6");
            objControlElement.SetAttribute("required", "yes");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "hdn_" + objReserveSummaryNode.GetAttribute("name").ToString() + "_Comments";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            //nnorouzi; MITS 18989 start
            strTitle = "";
            strName = "hdn_ReasonCommentsRequired";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = reasonCommentsRequired.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            //nnorouzi; MITS 18989 end

            //Aman Multi Currency --Start
                       
            //strTitle = "";
            //strName = "hdn_currencytype";
            //strRef = "//control[@name = '" + strName + "']";
            //strReadOnly = "";
            //objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            //objControlElement.InnerText = sDesc;
            //objDisplayColElement.AppendChild(objControlElement);
            //Aman Multi Currency --End
            objReserveSummaryNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;
            
            return objReserveSummaryNode;
        }

        private XmlElement GetHiddenFieldsNode(XmlDocument workSheetDoc, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId)
        {
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "half123";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;


            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "";
            strName = "hdnCtrlToClear";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //control to edit hidden field
            strTitle = "";
            strName = "hdnCtrlToEdit";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control having list of group name
            strTitle = "";
            strName = "hdnReserveTypeTabs";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control RSWid
            strTitle = "";
            strName = "hdnRSWid";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "-1";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnSubmittedToID
            strTitle = "";
            strName = "hdnSubmittedToID";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnSubmittedTo
            strTitle = "";
            strName = "hdnSubmittedTo";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnRSWStatus
            strTitle = "";
            strName = "hdnRSWStatus";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("codeid", "0");
            objControlElement.InnerText = "New";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //JIRA 6385 Start Snehal
            //hidden control hdnHoldStatus
            strTitle = "";
            strName = "hdnHoldStatus";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("Holdcodeid", "");
            //objControlElement.InnerText = "New";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            //JIRA 6385 Ends Snehal

            //hidden control hdnAppRejBy
            strTitle = "";
            strName = "hdnAppRejBy";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdndtAppRej
            strTitle = "";
            strName = "hdndtAppRej";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("dbformat", "");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdncreatedBy
            strTitle = "";
            strName = "hdnCreatedBy";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_CommonInfo.UserId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdndtLastMod
            strTitle = "";
            strName = "hdndtLastMod";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = DateTime.Now.ToString();
            objControlElement.SetAttribute("dbformat", Conversion.GetDateTime(DateTime.Now.ToString()));
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnLastModBy
            strTitle = "";
            strName = "hdnLastModBy";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_CommonInfo.UserName;
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control dtCreated
            strTitle = "";
            strName = "dtCreated";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = DateTime.Now.ToString();
            objControlElement.SetAttribute("dbformat", Conversion.GetDateTime(DateTime.Now.ToString()));
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control CLAIMiD
            strTitle = "";
            strName = "hdnClaimId";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = p_iClaimId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control Claim Number
            strTitle = "";
            strName = "hdnClaimNumber";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control UnitID
            strTitle = "";
            //MGaba2:Changing the names from hdnUnitId/hdnClaimantEId to hdnUnitRowId/hdnClaimantRowId
            strName = "hdnUnitRowId";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = p_iUnitRowId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control ClaimantEId
            strTitle = "";
            strName = "hdnClaimantRowId";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = p_iClaimantRowId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            
            //hidden control PrimaryClaimant
            strTitle = "";
            strName = "hdnPCName";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;


            //hidden control for hdnRSWType
            strTitle = "";
            strName = "hdnRSWType";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "0";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for hdnMode
            strTitle = "";
            strName = "hdnMode";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for hdnEditRejected
            strTitle = "";
            strName = "hdnEditRejected";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for hdnUnderLimit
            strTitle = "";
            strName = "hdnUnderLimit";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //sachin 6385 starts
            //hidden control for hdnUnderClaimIncLimit
            strTitle = "";
            strName = "hdnUnderClaimIncLimit";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            //sachin 6385 ends

            //sachin 6385 starts
            //hidden control for hdnUnderClaimIncLimit
            strTitle = "";
            strName = "hdnActionType";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            //sachin 6385 ends

            //hidden control hdnSMSCheckToUpdate
            strTitle = "";
            strName = "hdnSMSCheckToUpdate";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for SupervisorName
            strTitle = "";
            strName = "hdnSupName";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnAppRejComm for reason to be populated from UI
            strTitle = "";
            strName = "hdnAppRejComm";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnAppRejComm for reason to be populated from UI
            strTitle = "";
            strName = "hdnReqStatus";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("codeid", "0");
            objControlElement.InnerText = "New";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control SupervisorName
            strTitle = "";
            strName = "SupName";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control CurrentUser
            strTitle = "";
            strName = "hdnCurrentUser";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_CommonInfo.UserId.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;


            //hidden control hdnCollInRsvBal
            strTitle = "";
            strName = "hdnCollInRsvBal";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_bCollInResBal.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnCollInIncurredBal
            strTitle = "";
            strName = "hdnCollInIncurredBal";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_bCollInIncBal.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control hdnPrevResBelowPaid
            strTitle = "";
            strName = "hdnPrevResBelowPaid";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = m_bPrevResBelowPaid.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control ReserveWorksheet Xml Type
            strTitle = "";
            strName = "hdnRSWXmlType";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //hidden control for Prevent modifying  value to zero flag

            strTitle = "";
            strName = "hdnPrevModValtoZero";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            return objDisplayColElement;
        }

        private ReserveFunds.structReserves[] GetRequiredReserveTypeArray(int p_iClaimdId, string p_sRSWCustomReserveXML)
        {
            StringBuilder sbSQL = null;
            DbReader objReaderLOB = null;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimdId);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReaderLOB.Read())
                {
                    iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReaderLOB.GetInt32("CLAIM_TYPE_CODE");
                }
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                return GetReservesTypeCustom(iLOBCode, iClmTypeCode, p_sRSWCustomReserveXML,null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreatingClaimInfoControls(XmlDocument workSheetDoc, XmlElement claimInfoElement,ref XmlElement claimInfoParentNode,int p_iClaimId)
        {
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = "oneThirdLabel";
            string txtCssClass = "oneThirdLabel";
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;


            //new display column
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_ClaimNumber']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimNumber";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_ClaimNumber']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimNumber";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            //claimInfoParentNode.AppendChild(objDisplayColElement);
            //objDisplayColElement = null;



            //new display column
            //objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_ClaimantName']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimantName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_ClaimantName']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_ClaimantName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            claimInfoParentNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;


            //new display column for claim date
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_DateOfClaim']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfClaim";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_DateOfClaim']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfClaim";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_DateOfEvent']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfEvent";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_DateOfEvent']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_DateOfEvent";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            claimInfoParentNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;


            //new display column for Adjuster Name
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='lbl_ClaimInfo_Adjuster']").Attributes["title"].Value;
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_Adjuster";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_Adjuster']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_Adjuster";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "Supervisor :";
            strName = "lbl_" + claimInfoParentNode.GetAttribute("name").ToString() + "_SupervisorName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = claimInfoElement.SelectSingleNode("//control[@name='txt_ClaimInfo_SupervisorName']").Attributes["title"].Value;
            strName = "txt_" + claimInfoParentNode.GetAttribute("name").ToString() + "_SupervisorName";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, txtCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            claimInfoParentNode.AppendChild(objDisplayColElement);
            objDisplayColElement = null;
        }
      //changed parameters for customization of reserves by shobhana 
      
        private XmlElement GetReserveTypeNode(XmlDocument workSheetDoc, XmlElement claimInfoElement, int p_iReserveTypeCode, string p_sReserveTypeText, string p_sReserveTableValue, int p_iReserveTypeId, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId, ref StringBuilder strControlToEditList, ref StringBuilder strControlToClear, bool reasonCommentsRequired, string p_sRSWCustomReserveXML)
        {
            DbReader objReaderLOB = null;
            DbReader objReader = null;
            XmlElement reserveTypeElement = null;
            XmlDocument XMLReserve = new XmlDocument();
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;
            string lblCssClass = string.Empty;
            string threeCntrlClass = string.Empty;
            string strTitle = string.Empty;
            string strRef = string.Empty;
            string strName = string.Empty;
            string strType = string.Empty;
            string strReadOnly = string.Empty;
            string strPaidAmnt = string.Empty;
            string strOutStanding = string.Empty;
            string strTotalIncured = string.Empty;
            string strSQL = string.Empty;
            string strEndQuery = string.Empty;
            bool bFlag=false;
            //skhare7:MITS 22119
            StringBuilder sbSQL = null;
            //mcapps2 MITS 22943 Start
            StringBuilder sbSQLFromWhere = null;
            StringBuilder sbAlterTableSQL = null;
            StringBuilder sbUpdateTransTypePaid = null;
            StringBuilder sbGetTransTypeData = null;
            StringBuilder sbUpdateTransTypeCollection = null; //Mits 29710 by sachin
            StringBuilder sbUpdateTransSubtractAmount = null; //Mits 29710 by sachin
            string sCurrenDate = string.Empty;//variable for date
            int iMonthNow = 0;				//current month
            int iDay = 0;						//current day
            int iUnitId = 0;
            int iReserveTrackingLevel = 0;
            int iLOBCode = 0;
            int iClaimantEid = 0;
            double dOtherAdjustment = 0.0;
            //End
            double dRes, dIncurred, dColl, dPaid, dBalAmt, dTransPaidAmt,dTransIncurredAmt,dTransNewReserveBal;
            DbConnection objConn = null;
			//mcapps2 MITS 22943 End
            //Creating a new node as ReserveType
            reserveTypeElement = GetNewGroupElementForWorkSheet(workSheetDoc, "ReserveType_" + p_iReserveTypeCode, 0, p_sReserveTypeText);

            
            //Creating and fetching Claim information for this node
            //CreatingClaimInfoControls(workSheetDoc, claimInfoElement, ref reserveTypeElement, p_iClaimId);

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak1");
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            iMonthNow = DateTime.Now.Month;
            iDay = DateTime.Now.Day;

            //new display column for paid
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            if(m_bCollInResBal)
                strTitle = "Current Paid to Date (-collections)  ";
            else
                strTitle = "Current Paid to Date  ";

            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //calculating paid , incurred and outstanding amount 
            dRes = 0.0;
            dIncurred = 0.0;
            dColl = 0.0;
            dPaid = 0.0;
            dBalAmt = 0.0;
            strSQL = string.Empty;

            strSQL = GetQueryToReserveAmounts(p_iReserveTypeCode, p_iClaimId, p_iClaimantRowId, p_iUnitRowId);    

            objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSQL);
            if (objReader.Read())
            {
                //MITS 36042: bkuzhanthaim : Math.Round() added to Rounding values up (12.4965 as 12.50) or down ( 12.5412 as  12.54)
                if (objSysSetting.UseMultiCurrency != 0)//MITS 30285 Start mcapps2
                {
                    //Aman Multi Currency
                    dRes = Math.Round(objReader.GetDouble("CLAIM_CURRENCY_RESERVE_AMOUNT"), 2);
                    dBalAmt = Math.Round(objReader.GetDouble("CLAIM_CURRENCY_BALANCE_AMOUNT"), 2);
                    dPaid = Math.Round(objReader.GetDouble("CLAIM_CURRENCY_PAID_TOTAL"), 2);
                    dIncurred = Math.Round(objReader.GetDouble("CLAIM_CURRENCY_INCURRED_AMOUNT"), 2);
                    dColl = Math.Round(objReader.GetDouble("CLAIM_CURR_COLLECTION_TOTAL"), 2);
                    //Aman Multi Currency
                }
                else
                {
                    dRes = Math.Round(objReader.GetDouble("RESERVE_AMOUNT"), 2);
                    dBalAmt = Math.Round(objReader.GetDouble("BALANCE_AMOUNT"), 2);
                    dPaid = Math.Round(objReader.GetDouble("PAID_TOTAL"), 2);
                    //dIncurred = (objReader.GetDouble("PAID_BALANCE") > dRes)? objReader.GetDouble("PAID_BALANCE") : dRes;
                    dIncurred = Math.Round(objReader.GetDouble("INCURRED_AMOUNT"), 2);
                    dColl = Math.Round(objReader.GetDouble("COLLECTION_TOTAL"), 2);
                }
                //MITS 30285 End mcapps2

            }
            objReader.Close();

            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            //dBalAmt = CalculateReserveBalance(dRes, dPaid, dColl, m_bCollInResBal);
            dBalAmt = CalculateReserveBalance(dRes, dPaid, dColl, m_bCollInResBal,p_iReserveTypeCode);
            if (!IsRecoveryReserve(p_iReserveTypeCode))
            {
                dIncurred = CalculateIncurred(dBalAmt, dPaid, dColl, m_bCollInResBal, m_bCollInIncBal,p_iReserveTypeCode);
            }
            else
            {
                dIncurred = CalculateIncurred(dBalAmt, dPaid, dColl, false, false,p_iReserveTypeCode);
            }
            if (m_bCollInResBal)
                dOtherAdjustment = dBalAmt - dColl;
            else
                dOtherAdjustment = dBalAmt;

            if (dOtherAdjustment < dPaid)
            {
                dOtherAdjustment = 0;
            }
            
            //getting paid after calculating from collections
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            if(!IsRecoveryReserve(p_iReserveTypeCode))
            {
                dPaid = CalculatePaidAmount(dPaid,dColl,m_bCollInResBal); 
            }

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dPaid); 
            objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dPaid));
            objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dPaid, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for Outstanding
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Current Reserve Balance ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Outstanding";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Outstanding";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);  //string.Format("{0:C}", dBalAmt);
            objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dBalAmt));
            objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dBalAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for TotalIncurred
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Current Total Reserve ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TotalIncurred";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TotalIncurred";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dRes);
            objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dRes));
            objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dRes, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            
            //adding hidden field hdnColl_ReserveType for storing collection for this reservetype ,to be used by javascript at UI level
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_Collection";
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = dColl.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            
            //adding hidden field for ReserveAmt
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveAmt";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            
            //adding hidden field hdnReserveChange_ReserveType for storing Reserve amount change for this reservetype ,to be used by javascript at UI level
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveChange";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, "", "", strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak2");
            reserveTypeElement.AppendChild(objDisplayColElement);

            //adding display column for Transaction Type label

            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Transaction Type";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveCatagory";
            strRef = "//control[@name='" + strName + "']";
            //MGaba2:R6:Changed temporarily
            //strType = "labelonly";
            strType = "message";
            strReadOnly = "true";
            threeCntrlClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            if(m_bCollInResBal)
                strTitle = "Paid to Date(-collections)";
            else
                strTitle = "Paid to Date";

            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypePaid";
            strRef = "//control[@name='" + strName + "']";
            threeCntrlClass = "oneThird";
            //MGaba2:R6:Changed temporarily
            //strType = "labelonly";
            strType = "message";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "New Reserve Balance";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal";
            strRef = "//control[@name='" + strName + "']";
            //MGaba2:R6:Changed temporarily
            //strType = "labelonly";
            threeCntrlClass = "oneThird";
            strType = "message";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "New Reserve Amount";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeTotalIncurrend";
            strRef = "";// "//control[@name='" + strName + "']";
            //MGaba2:R6:Changed temporarily
           // strType = "labelonly";
            strType = "message";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Addding column for Each Transction Type under current Reserve Type
            //now fetching transaction type for each request type code  and creating xml nodes for this

            sCurrenDate= DateTime.Now.Year.ToString();

            //for the date string
            if (iMonthNow < 10)
                sCurrenDate = sCurrenDate + "0" + iMonthNow.ToString();
            else
                sCurrenDate = sCurrenDate + iMonthNow.ToString();

            if (iDay < 10)
                sCurrenDate = sCurrenDate + "0" + iDay.ToString();
            else
                sCurrenDate = sCurrenDate + iDay.ToString();
            //skhare7:shobhana add condition for deleted flag MITS:20142
            if(p_sReserveTableValue=="S")
            {
                int iLangCode = objUserlogin.objUser.NlsCode;//Deb ML changes
				//mcapps2 MITS 22943 Start
                sbSQL = new StringBuilder();
                sbSQLFromWhere = new StringBuilder();
                sbSQL.Append("SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId ");
                sbSQLFromWhere.Append(" FROM CODES,CODES_TEXT");
                sbSQLFromWhere.Append(" WHERE  CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =" + p_iReserveTypeCode + " AND CODES.DELETED_FLAG<>-1");
                sbSQLFromWhere.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");
                try
                {
                    if (objConn == null)
                    {
                        objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                    }
                    objConn.Open();
                    objConn.ExecuteNonQuery("DROP TABLE TEMP_TRANSTYP_SUM");
                    objConn.Close();
                }
                catch (Exception p_objEx)
                {
                    objConn.Close();
                } 
                string sDBType = string.Empty;
                sDBType = objConn.DatabaseType.ToString();
                if (sDBType != Constants.DB_SQLSRVR)
                {
                    //Deb ML changes
                    string sOracleCreate = string.Empty;
                    string sOracleSelect = sbSQL.ToString();
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    sbSQL.Append(" UNION ");
                    sbSQL.Append(sOracleSelect);
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    //rsharma220 MITS 34406
                    sbSQL.Append(")");
                    sOracleCreate = sbSQL.ToString();
                    sOracleCreate = "CREATE TABLE TEMP_TRANSTYP_SUM AS (" + sOracleCreate;
                    sbSQL = null;
                    sbSQL = new StringBuilder();
                    sbSQL.Append(sOracleCreate);
                    sbSQL.Append(")");
                    //Deb ML changes
                }
                else
                {
                    //Deb ML changes
                    string sSqlSelect = sbSQL.ToString();
                    sbSQL.Append(" INTO TEMP_TRANSTYP_SUM");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    sbSQL.Append(" UNION ");
                    sbSQL.Append(sSqlSelect);
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    //rsharma220 MITS 34406
                    sbSQL.Append(")");
                    //Deb ML changes
                }
                //sbSQL.Append(sbSQLFromWhere.ToString());

                sbAlterTableSQL = new StringBuilder();
                sbUpdateTransTypePaid = new StringBuilder();

                sbUpdateTransTypeCollection = new StringBuilder(); // Added By Sachin For MitS 29710
                sbUpdateTransSubtractAmount = new StringBuilder(); // Added By Sachin For MitS 29710

                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbAlterTableSQL.Append("ALTER TABLE TEMP_TRANSTYP_SUM ADD TRANS_TYPE_PAID_AMT VARCHAR(25) NULL,TRANS_TYPE_COLLECTION_AMT VARCHAR(25) NULL  "); //Added By Sachin Gupta for MITS 29710
                    sbUpdateTransTypePaid.Append("UPDATE TEMP_TRANSTYP_SUM SET TRANS_TYPE_PAID_AMT = (LTRIM((STR(( ");  //MITS 36042: bkuzhanthaim 
                    sbUpdateTransTypeCollection.Append("UPDATE TEMP_TRANSTYP_SUM SET TRANS_TYPE_COLLECTION_AMT = (LTRIM((STR((");  //MITS 36042: bkuzhanthaim  
                    strEndQuery = ",25,2))))";
                }
                else
                {
                    sbAlterTableSQL.Append("ALTER TABLE TEMP_TRANSTYP_SUM ADD (TRANS_TYPE_PAID_AMT VARCHAR2(25) NULL,TRANS_TYPE_COLLECTION_AMT VARCHAR2(25) NULL)  "); //Added By Sachin Gupta for MITS 29710
                    sbUpdateTransTypePaid.Append("UPDATE TEMP_TRANSTYP_SUM SET TRANS_TYPE_PAID_AMT = ("); //MITS 36042: bkuzhanthaim 
                    sbUpdateTransTypeCollection.Append("UPDATE TEMP_TRANSTYP_SUM SET TRANS_TYPE_COLLECTION_AMT = ("); //MITS 36042: bkuzhanthaim  
                }

                    
                sbUpdateTransTypePaid.Append("SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) PaidAmount");
                sbUpdateTransTypePaid.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                sbUpdateTransTypePaid.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.PAYMENT_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                sbUpdateTransTypePaid.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                sbUpdateTransTypePaid.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = TEMP_TRANSTYP_SUM.TransId AND FUNDS.CLAIM_ID = " + p_iClaimId);

                // Added By Sachin for mits 29710 subtract collection from paid
                     
                sbUpdateTransTypeCollection.Append(" SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) CollectionAmount");
                sbUpdateTransTypeCollection.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                sbUpdateTransTypeCollection.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.COLLECTION_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                sbUpdateTransTypeCollection.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                sbUpdateTransTypeCollection.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = TEMP_TRANSTYP_SUM.TransId AND FUNDS.CLAIM_ID = " + p_iClaimId);

                iReserveTrackingLevel = GetReserveTracking(p_iClaimId, ref iLOBCode);

                if (p_iClaimantRowId > 0)
                {
                    GetClaimantId(p_iClaimantRowId, ref iClaimantEid);

                    //if (iLOBCode == 241)   //GC
                    if (iLOBCode == 241 || iLOBCode == 242 || iLOBCode == 845) //Add or condition by kuladeep for VA/Propery Claim with mits:34961
                    {
                        if (iReserveTrackingLevel == 1)  //detail level tracking is ON
                        {
                            if (iClaimantEid > 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);
                                sbUpdateTransTypeCollection.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);  // Added By Sachin For Mits 29710 add one collection
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //claim level tracking is ON
                        {
                            if (iClaimantEid != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                        else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                        {
                            if (iClaimantEid != 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                                sbUpdateTransTypeCollection.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            }
                        }
                    }
                }
                else if (p_iUnitRowId > 0)
                {
                    GetUnitId(p_iUnitRowId, ref iUnitId);

                    if (iLOBCode == 242) //VA
                    {
                        if (iReserveTrackingLevel == 1) //detail level tracking is ON
                        {
                            if (iUnitId > 0)
                            {
                                sbUpdateTransTypePaid.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                                sbUpdateTransTypeCollection.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //Claim level tracking is ON
                        {
                            if (iUnitId != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                        else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                        {
                            if (iClaimantEid > 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                                sbUpdateTransTypeCollection.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            }
                            else if (iUnitId > 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND UNIT_ID = " + iUnitId);
                                sbUpdateTransTypeCollection.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            }
                        }
                    }
                }

                // Added By Sachin for mits 29710 subtract collection from paid
                // MITS 36042 : SQL : while internal conversion of float data to varchar will round up and down the Amount when the Amount is in lakhs. Fix : STR (Amount as float ,25(length),2(decimal Points))
                sbUpdateTransTypePaid.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE)" + strEndQuery);
                sbUpdateTransTypeCollection.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE)" + strEndQuery);
                //sbUpdateTransSubtractAmount.Append(sbUpdateTransTypePaid.ToString());
                //sbUpdateTransSubtractAmount.Append("-");
                //sbUpdateTransSubtractAmount.Append(sbUpdateTransTypeCollection.ToString());
                
                if (objConn == null)
                {
                    objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                }
                objConn.Open();
                objConn.ExecuteNonQuery(sbSQL.ToString());
                objConn.ExecuteNonQuery(sbAlterTableSQL.ToString());

                objConn.ExecuteNonQuery(sbUpdateTransTypeCollection.ToString());
                objConn.ExecuteNonQuery(sbUpdateTransTypePaid.ToString());
                objConn.Close();

                sbGetTransTypeData = new StringBuilder();
                sbGetTransTypeData.Append("SELECT * FROM TEMP_TRANSTYP_SUM");
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbGetTransTypeData.ToString());
				//mcapps2 MITS 22943 End
                while (objReaderLOB.Read())
                {
                    objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                    strTitle = objReaderLOB.GetValue("TransDesc").ToString();
                    strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransType_" + objReaderLOB.GetInt32("TransId").ToString();
                    strRef = "//control[@name='" + strName + "']";
                    strType = "labelonly";
                    strReadOnly = "true";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    //objControlElement.SetAttribute("width", "56px");
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //setting paid amount in text box
                    strTitle = "";
                    strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid_" + objReaderLOB.GetInt32("TransId").ToString();
                    strRef = "//control[@name='" + strName + "']";
                    strType = "currency";
                    strReadOnly = "true";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    //objControlElement.SetAttribute("width", "56px");
                    //calculate paid for trans type
                    dTransPaidAmt = 0.0;
					//mcapps2 MITS 22943 Start
                    //MITS 36042: bkuzhanthaim : Math.Round() added to Rounding values up (12.4965 as 12.50) or down ( 12.5412 as  12.54)
                    string sPaidAmt = objReaderLOB.GetString("TRANS_TYPE_PAID_AMT") ;
                    string sCollectAmt =  objReaderLOB.GetString("TRANS_TYPE_COLLECTION_AMT") ;
                    sPaidAmt = !string.IsNullOrEmpty(sPaidAmt) ? sPaidAmt : "0.0";
                    //double dPaidAmt = Convert.ToDouble(sPaidAmt);
                    double dPaidAmt = Math.Round(Convert.ToDouble(sPaidAmt), 2);
                    sCollectAmt = !string.IsNullOrEmpty(sCollectAmt) ? sCollectAmt : "0.0";
                    //double dCollectAmt = Convert.ToDouble(sCollectAmt);
                    double dCollectAmt = Math.Round(Convert.ToDouble(sCollectAmt), 2);
                    //MITS 30191 Raman Bhatia: Implementation of recovery reserve
                    if (m_bCollInResBal && !IsRecoveryReserve(p_iReserveTypeCode))
                    {
                        dTransPaidAmt = dPaidAmt - dCollectAmt;
                        if (dCollectAmt > dPaidAmt)
                        {
                            dCollectAmt = dCollectAmt - dPaidAmt;
                            dPaidAmt = 0.0;
                        }
                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dTransPaidAmt));//MITS 31572 srajindersin 02/15/2013
                    }
                    else
                    {
                        dTransPaidAmt = dPaidAmt;
                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dTransPaidAmt));//MITS 31572 srajindersin 02/15/2013
                    }
                    objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt);  
					//mcapps2 MITS 22943 End
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //New Reserve Balance textbox
                    strTitle = "";
                    strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToClear.Append(strName + ",");
                    strControlToEditList.Append(strName + ",");
                    strRef = "//control[@name='" + strName + "']";
                    strType = "currency";
                    strReadOnly = "false";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    objControlElement.SetAttribute("onchange", "TransTypeNewReserveBalance_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + objReaderLOB.GetInt32("TransId").ToString() + "')");
                    if (m_bCollInResBal)
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(dCollectAmt); // string.Format("{0:C}", 0.0);
                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dCollectAmt));
                    }
                    else
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0);
                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(0.0));
                    }
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //adding hidden field for storing new ReserveBal amount
                    strTitle = "";
                    strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToClear.Append(strName + ",");
                    strRef = "//control[@name='" + strName + "']";
                    strType = "hidden";
                    strReadOnly = "";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                    if (m_bCollInResBal)
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(dCollectAmt); //string.Format("{0:C}", 0.0); 
                    }
                    else
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0);
                    }
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //creating TotalInccured Text
                    strTitle = "";
                    strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToEditList.Append(strName + ",");
                    strControlToClear.Append(strName + ",");

                    strRef = "//control[@name='" + strName + "']";
                    strType = "currency";
                    strReadOnly = "false";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    objControlElement.SetAttribute("onchange", "CalculateReserveAmount_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + objReaderLOB.GetInt32("TransId").ToString() + "')");

                    dTransIncurredAmt = 0.0;
                    dTransIncurredAmt = dPaidAmt;
                    //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt); //string.Format("{0:C}", dTransPaidAmt); 
                    objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dPaidAmt));
                    objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dPaidAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //adding hidden field for storing TotalInccured Text
                    strTitle = "";
                    strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToClear.Append(strName + ",");
                    strRef = "//control[@name='" + strName + "']";
                    strType = "hidden";
                    strReadOnly = "";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                    //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dIncurred); //string.Format("{0:C}", dIncurred);
                    //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dIncurred, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
                    objControlElement.InnerText = dIncurred.ToString();
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    reserveTypeElement.AppendChild(objDisplayColElement);
                    objDisplayColElement = null;
                }
                objReaderLOB.Close();
				//mcapps2 MITS 22943 Start
                try
                {
                    if (objConn == null)
                    {
                        objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                    }
                    objConn.Open();
                    objConn.ExecuteNonQuery("DROP TABLE TEMP_TRANSTYP_SUM");
                    objConn.Close();
                }
                catch (Exception p_objEx)
                {
                    objConn.Close();
                }
				//mcapps2 MITS 22943 End
            }
            else if (p_sReserveTableValue == "X")
            {

                XMLReserve.LoadXml(p_sRSWCustomReserveXML);

                XmlNodeList nodes = XMLReserve.SelectNodes("//ReserveCustomized");

                if (nodes.Count > 0)
                {
                    for (int i = 0; i <= nodes.Count - 1; i++)
                    {
                       
                            XmlNode node = nodes[i];
                        if(p_iReserveTypeCode==Conversion.CastToType<int>((nodes[i].Attributes["value"].Value.ToString()),out bFlag))
                        { 
                            foreach (XmlElement ele in node)
                            {

                                if (ele.LocalName == "TransCustomTypes")
                                {
                                    if (node["TransCustomTypes"] != null)
                                    {

                                        objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                                        strTitle = ele.InnerText; ;
                                        strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransType_" + ele.Attributes["value"].Value.ToString();
                                        strRef = "//control[@name='" + strName + "']";
                                        strType = "labelonly";
                                        strReadOnly = "true";
                                        objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                                        //objControlElement.SetAttribute("width", "56px");
                                        objDisplayColElement.AppendChild(objControlElement);
                                        objControlElement = null;

                                        //setting paid amount in text box
                                        strTitle = "";
                                        strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid_" + ele.Attributes["value"].Value.ToString();
                                        strRef = "//control[@name='" + strName + "']";
                                        strType = "currency";
                                        strReadOnly = "true";
                                        objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                                        //objControlElement.SetAttribute("width", "56px");
                                        //claculate paid for trans type
                                        dTransPaidAmt = 0.0;
                                        dTransPaidAmt = GetPaidAmountForTransType(Conversion.ConvertStrToInteger(ele.Attributes["value"].Value), p_iReserveTypeCode, p_iClaimId, p_iClaimantRowId, p_iUnitRowId, m_bCollInResBal);
                                        //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt);  //string.Format("{0:C}", dTransPaidAmt); 
                                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dTransPaidAmt));
                                        objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dTransPaidAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                                        objDisplayColElement.AppendChild(objControlElement);
                                        objControlElement = null;

                                        //New Reserve Balance textbox
                                        strTitle = "";
                                        strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + ele.Attributes["value"].Value.ToString();
                                        strControlToClear.Append(strName + ",");
                                        strControlToEditList.Append(strName + ",");
                                        strRef = "//control[@name='" + strName + "']";
                                        strType = "currency";
                                        strReadOnly = "false";
                                        objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                                        objControlElement.SetAttribute("onchange", "TransTypeNewReserveBalance_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + ele.Attributes["value"].Value.ToString() + "')");
                                        //objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0); // string.Format("{0:C}", 0.0); 
                                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(0.0));
                                        objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, 0.0, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                                        objDisplayColElement.AppendChild(objControlElement);
                                        objControlElement = null;

                                        //adding hidden field for storing new ReserveBal amount
                                        strTitle = "";
                                        strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + ele.Attributes["value"].Value.ToString();
                                        strControlToClear.Append(strName + ",");
                                        strRef = "//control[@name='" + strName + "']";
                                        strType = "hidden";
                                        strReadOnly = "";
                                        objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                                        //objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0); //string.Format("{0:C}", 0.0); 
                                        //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, 0.0, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
                                        objControlElement.InnerText = "0";
                                        objDisplayColElement.AppendChild(objControlElement);
                                        objControlElement = null;

                                        //creating TotalInccured Text
                                        strTitle = "";
                                        strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + ele.Attributes["value"].Value.ToString();
                                        strControlToEditList.Append(strName + ",");
                                        strControlToClear.Append(strName + ",");

                                        strRef = "//control[@name='" + strName + "']";
                                        strType = "currency";
                                        strReadOnly = "false";
                                        objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                                        objControlElement.SetAttribute("onchange", "CalculateReserveAmount_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + ele.Attributes["value"].Value.ToString() + "')");

                                        dTransIncurredAmt = 0.0;
                                        dTransIncurredAmt = dTransPaidAmt;
                                        //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt); //string.Format("{0:C}", dTransPaidAmt); 
                                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dTransPaidAmt));
                                        objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dTransPaidAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                                        objDisplayColElement.AppendChild(objControlElement);
                                        objControlElement = null;

                                        //adding hidden field for storing TotalInccured Text
                                        strTitle = "";
                                        strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + ele.Attributes["value"].Value.ToString();
                                        strControlToClear.Append(strName + ",");
                                        strRef = "//control[@name='" + strName + "']";
                                        strType = "hidden";
                                        strReadOnly = "";
                                        objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                                        //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dIncurred); //string.Format("{0:C}", dIncurred);
                                        //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dIncurred, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
                                        objControlElement.InnerText = dIncurred.ToString();
                                        objDisplayColElement.AppendChild(objControlElement);
                                        objControlElement = null;

                                        reserveTypeElement.AppendChild(objDisplayColElement);
                                        objDisplayColElement = null;
                                     
                                    }
                                }
                              }
                              


                            }

                        

                    }




                }

            }
            else if (p_sReserveTableValue == "C")
            {
                int iLangCode = objUserlogin.objUser.NlsCode;//Aman MITS 31626
                  //skhare7:MITS 22119
                sbSQL = new StringBuilder();
			//MITS 24206 skhare7
                sbSQLFromWhere = new StringBuilder();
              //  sbSQL.Append("SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId ");
                sbSQL.Append(" select  CODE_DESC TransDesc,RSW_CUSTOM_TRANSTYPE.TRANS_TYPE_CODE TransId "); 
                sbSQLFromWhere.Append(" from RSW_CUSTOM_TRANSTYPE inner join CODES_TEXT on RSW_CUSTOM_TRANSTYPE.TRANS_TYPE_CODE=CODES_TEXT.CODE_ID inner join CODES on CODES.CODE_ID=CODES_TEXT.CODE_ID ");
                sbSQLFromWhere.Append(" where CODES.DELETED_FLAG<>-1 AND RSW_CUSTOM_TRANSTYPE.RSW_CUTOM_ID =" + p_iReserveTypeId + "");
                sbSQLFromWhere.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");
                //MITS 24206 skhare7 End
                //mcapps2 MITS 22943 Start
                //sbSQL.Append("SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId ");
                //sbSQLFromWhere.Append(" FROM CODES,CODES_TEXT");
                //sbSQLFromWhere.Append(" WHERE  CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =" + p_iReserveTypeCode + " AND CODES.DELETED_FLAG<>-1");
                //sbSQLFromWhere.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                //sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                //sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                //sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                //sbSQLFromWhere.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");

                try
                {
                    if (objConn == null)
                    {
                        objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                    }
                    objConn.Open();
                    objConn.ExecuteNonQuery("DROP TABLE TEMP_TRANSTYP_SUM");
                    objConn.Close();
                }
                catch (Exception p_objEx)
                {
                    objConn.Close();
                }
                string sDBType = string.Empty;
                sDBType = objConn.DatabaseType.ToString();
                if (sDBType != Constants.DB_SQLSRVR)
                {
                    string sOracleCreate = string.Empty;
                    //Aman MITS 31626
                    string sOracleSelect = sbSQL.ToString();
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    sbSQL.Append(" UNION ");
                    sbSQL.Append(sOracleSelect);
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(")");
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    //Aman MITS 31626
                    sOracleCreate = sbSQL.ToString();
                    sOracleCreate = "CREATE TABLE TEMP_TRANSTYP_SUM AS " + sOracleCreate;
                    sbSQL = null;
                    sbSQL = new StringBuilder();
                    sbSQL.Append(sOracleCreate);
                }
                else
                {
                    //Aman MITS 31626
                    string sSqlSelect = sbSQL.ToString();
                    sbSQL.Append(" INTO TEMP_TRANSTYP_SUM");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    sbSQL.Append(" UNION ");
                    sbSQL.Append(sSqlSelect);
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(" AND CODES.CODE_ID NOT IN (SELECT CODES.CODE_ID");
                    sbSQL.Append(sbSQLFromWhere.ToString());
                    sbSQL.Append(")");
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);
                    //Aman MITS 31626
                }
                //shifting line outside if else block
                // sbSQL.Append(sbSQLFromWhere.ToString());  //Aman MITS 31626

                sbAlterTableSQL = new StringBuilder();
                sbUpdateTransTypePaid = new StringBuilder();

                sbUpdateTransTypeCollection = new StringBuilder();
                sbUpdateTransSubtractAmount = new StringBuilder();

                
                sDBType = objConn.DatabaseType.ToString();
                if (sDBType != Constants.DB_SQLSRVR)
                {
                    sbAlterTableSQL.Append("ALTER TABLE TEMP_TRANSTYP_SUM ADD (TRANS_TYPE_PAID_AMT VARCHAR(25) NULL,TRANS_TYPE_COLLECTION_AMT VARCHAR(25) NULL )"); //Added By Sachin Gupta for MITS 29710 //Aman MITS 31626 
                }
                else
                {
                    sbAlterTableSQL.Append("ALTER TABLE TEMP_TRANSTYP_SUM ADD TRANS_TYPE_PAID_AMT VARCHAR(25) NULL,TRANS_TYPE_COLLECTION_AMT VARCHAR(25) NULL "); //Aman MITS 31626 
                }
                sbUpdateTransTypePaid.Append("UPDATE TEMP_TRANSTYP_SUM SET TRANS_TYPE_PAID_AMT = (");
                sbUpdateTransTypePaid.Append("SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) PaidAmount");
                sbUpdateTransTypePaid.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                sbUpdateTransTypePaid.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.PAYMENT_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                sbUpdateTransTypePaid.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                sbUpdateTransTypePaid.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = TEMP_TRANSTYP_SUM.TransId AND FUNDS.CLAIM_ID = " + p_iClaimId);

                sbUpdateTransTypeCollection.Append("UPDATE TEMP_TRANSTYP_SUM SET TRANS_TYPE_COLLECTION_AMT = (");
                sbUpdateTransTypeCollection.Append(" SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) CollectionAmount");
                sbUpdateTransTypeCollection.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                sbUpdateTransTypeCollection.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.COLLECTION_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                sbUpdateTransTypeCollection.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                sbUpdateTransTypeCollection.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = TEMP_TRANSTYP_SUM.TransId AND FUNDS.CLAIM_ID = " + p_iClaimId);

                iReserveTrackingLevel = GetReserveTracking(p_iClaimId, ref iLOBCode);

                if (p_iClaimantRowId > 0)
                {
                    GetClaimantId(p_iClaimantRowId, ref iClaimantEid);

                    if (iLOBCode == 241)   //GC
                    {
                        if (iReserveTrackingLevel == 1)  //detail level tracking is ON
                        {
                            if (iClaimantEid > 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);
                                sbUpdateTransTypeCollection.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //claim level tracking is ON
                        {
                            if (iClaimantEid != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                        else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                        {
                            if (iClaimantEid != 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                                sbUpdateTransTypeCollection.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            }
                        }
                    }
                }
                else if (p_iUnitRowId > 0)
                {
                    GetUnitId(p_iUnitRowId, ref iUnitId);

                    if (iLOBCode == 242) //VA
                    {
                        if (iReserveTrackingLevel == 1) //detail level tracking is ON
                        {
                            if (iUnitId > 0)
                            {
                                sbUpdateTransTypePaid.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                                sbUpdateTransTypeCollection.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //Claim level tracking is ON
                        {
                            if (iUnitId != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                        else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                        {
                            if (iClaimantEid > 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                                sbUpdateTransTypeCollection.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            }
                            else if (iUnitId > 0)
                            {
                                sbUpdateTransTypePaid.Append(" AND UNIT_ID = " + iUnitId);
                                sbUpdateTransTypeCollection.Append(" AND UNIT_ID = " + iUnitId);
                            }
                        }
                    }
                }

                // Added By Sachin for mits 29710 subtract collection from paid
                sbUpdateTransTypePaid.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE)");
                sbUpdateTransTypeCollection.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE)");
                //sbUpdateTransSubtractAmount.Append(sbUpdateTransTypePaid.ToString());
                //sbUpdateTransSubtractAmount.Append("-");
                //sbUpdateTransSubtractAmount.Append(sbUpdateTransTypeCollection.ToString());

                if (objConn == null)
                {
                    objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                }
                objConn.Open();
                objConn.ExecuteNonQuery(sbSQL.ToString());
                objConn.ExecuteNonQuery(sbAlterTableSQL.ToString());
                objConn.ExecuteNonQuery(sbUpdateTransTypeCollection.ToString());
                objConn.ExecuteNonQuery(sbUpdateTransTypePaid.ToString());
                objConn.Close();

                sbGetTransTypeData = new StringBuilder();
                sbGetTransTypeData.Append("SELECT * FROM TEMP_TRANSTYP_SUM");
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbGetTransTypeData.ToString());
				//mcapps2 MITS 22943 End
                while (objReaderLOB.Read())
                {
                    objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
                    strTitle = objReaderLOB.GetValue("TransDesc").ToString();
                    strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransType_" + objReaderLOB.GetInt32("TransId").ToString();
                    strRef = "//control[@name='" + strName + "']";
                    strType = "labelonly";
                    strReadOnly = "true";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    //objControlElement.SetAttribute("width", "56px");
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //setting paid amount in text box
                    strTitle = "";
                    strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Paid_" + objReaderLOB.GetInt32("TransId").ToString();
                    strRef = "//control[@name='" + strName + "']";
                    strType = "currency";
                    strReadOnly = "true";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    //objControlElement.SetAttribute("width", "56px");
                    //calculate paid for trans type
                    dTransPaidAmt = 0.0;
					//mcapps2 MITS 22943 Start
                    //Raman : March 28 2011: code was crashing due to wrong cast
                    //dTransPaidAmt = objReaderLOB.GetInt32("TRANS_TYPE_PAID_AMT");
                    string sPaidAmt = objReaderLOB.GetString("TRANS_TYPE_PAID_AMT");
                    string sCollectAmt = objReaderLOB.GetString("TRANS_TYPE_COLLECTION_AMT");
                    sPaidAmt = !string.IsNullOrEmpty(sPaidAmt) ? sPaidAmt : "0.0";
                    double dPaidAmt = Convert.ToDouble(sPaidAmt);
                    sCollectAmt = !string.IsNullOrEmpty(sCollectAmt) ? sCollectAmt : "0.0";
                    double dCollectAmt = Convert.ToDouble(sCollectAmt);
                    if (m_bCollInResBal)
                    {
                        dTransPaidAmt = dPaidAmt - dCollectAmt;
                        if (dCollectAmt > dPaidAmt)
                        {
                            dCollectAmt = dCollectAmt - dPaidAmt;
                            dPaidAmt = 0.0;
                        }
                    }
                    else
                    {
                        dTransPaidAmt = dPaidAmt;
                    }
                    objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt);  
                    //dTransPaidAmt = Conversion.ConvertStrToDouble(objReaderLOB.GetString("TRANS_TYPE_PAID_AMT"));
                    //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt);
                    objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dTransPaidAmt));
                    objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dTransPaidAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
					//mcapps2 MITS 22943 End
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //New Reserve Balance textbox
                    strTitle = "";
                    strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToClear.Append(strName + ",");
                    strControlToEditList.Append(strName + ",");
                    strRef = "//control[@name='" + strName + "']";
                    strType = "currency";
                    strReadOnly = "false";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    objControlElement.SetAttribute("onchange", "TransTypeNewReserveBalance_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + objReaderLOB.GetInt32("TransId").ToString() + "')");
                    //objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0); // string.Format("{0:C}", 0.0); 
                    
                    //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, 0.0, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
                    if (m_bCollInResBal)
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(dCollectAmt);
                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dCollectAmt));
                    }
                    else
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0); //string.Format("{0:C}", 0.0); 
                        objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(0.0));
                    }
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //adding hidden field for storing new ReserveBal amount
                    strTitle = "";
                    strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeNewReserveBal_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToClear.Append(strName + ",");
                    strRef = "//control[@name='" + strName + "']";
                    strType = "hidden";
                    strReadOnly = "";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                    if (m_bCollInResBal)
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(dCollectAmt);
                    }
                    else
                    {
                        objControlElement.InnerText = ConvertDoubleToCurrencyStr(0.0); //string.Format("{0:C}", 0.0); 
                    }
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //creating TotalInccured Text
                    strTitle = "";
                    strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToEditList.Append(strName + ",");
                    strControlToClear.Append(strName + ",");

                    strRef = "//control[@name='" + strName + "']";
                    strType = "currency";
                    strReadOnly = "false";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
                    objControlElement.SetAttribute("onchange", "CalculateReserveAmount_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "','" + objReaderLOB.GetInt32("TransId").ToString() + "')");

                    dTransIncurredAmt = 0.0;
                    dTransIncurredAmt = dTransPaidAmt;
                    //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dTransPaidAmt); //string.Format("{0:C}", dTransPaidAmt); 
                    objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dTransPaidAmt));
                    objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dTransPaidAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    //adding hidden field for storing TotalInccured Text
                    strTitle = "";
                    strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_" + objReaderLOB.GetInt32("TransId").ToString();
                    strControlToClear.Append(strName + ",");
                    strRef = "//control[@name='" + strName + "']";
                    strType = "hidden";
                    strReadOnly = "";
                    objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
                    //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dIncurred); //string.Format("{0:C}", dIncurred);
                    //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dIncurred, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
                    objControlElement.InnerText = dIncurred.ToString();
                    objDisplayColElement.AppendChild(objControlElement);
                    objControlElement = null;

                    reserveTypeElement.AppendChild(objDisplayColElement);
                    objDisplayColElement = null;
                }

                objReaderLOB.Close();
                sbSQL = null;
				//mcapps2 MITS 22943 Start
                 try
                {
                    if (objConn == null)
                    {
                        objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                    }
                    objConn.Open();
                    objConn.ExecuteNonQuery("DROP TABLE TEMP_TRANSTYP_SUM");
                    objConn.Close();
                }
                 catch (Exception p_objEx)
                 {
                     objConn.Close();
                 }
				 //mcapps2 MITS 22943 End
            }
                // MITS:20142 end
               

         

          

            //Adding New Other Trans field in order to round off the data starts
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");
            strTitle = "Other Adjustments";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransType_OtherAdjustment";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            lblCssClass = "twoThird";
            strReadOnly = "true";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //creating TotalInccured for Other trans type
            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_OtherAdjustment";
            strControlToEditList.Append(strName + ",");
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "false";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("onchange", "CalculateReserveAmount_OnBlur('" + reserveTypeElement.GetAttribute("name").ToString() + "')");
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt);  //string.Format("{0:C}", dBalAmt);
            objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dOtherAdjustment));
            objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dOtherAdjustment, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //Added by Nitin for Mits 18989
            //adding hidden field for storing Other Field 
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_TransTypeIncurredAmount_OtherAdjustment";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = ConvertDoubleToCurrencyStr(dOtherAdjustment); //Kiran 27910
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt); 
            //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dBalAmt, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
            objControlElement.InnerText = dOtherAdjustment.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //Adding of new Misc field Ends ...........

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak3");
            reserveTypeElement.AppendChild(objDisplayColElement);

            //new display column for New Total Inccured
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            //adding hidden field for storing new inccured amount
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dRes); //string.Format("{0:C}", dIncurred);
            //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dRes, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
            objControlElement.InnerText = dRes.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            
            
            strTitle = "New Total Reserve  ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            strControlToClear.Append(strName + ",");
            //to populate value in this textbox from respective hidden field
            //strRef = "//control[@name='" + "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED" + "']";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dRes); //string.Format("{0:C}", dIncurred); 
            objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dRes));
            objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dRes, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;            


            //new display column for New Outstanding
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            //adding hidden field for storing new outstanding amount
            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt); //string.Format("{0:C}", dBalAmt);
            //objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dBalAmt, m_eNavType, m_CommonInfo.Connectionstring); //Aman Multi Currency
            objControlElement.InnerText = dBalAmt.ToString();
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;
            
            //adding textbox for new outstanding
            strTitle = "New Reserve Balance   ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding";
            strControlToClear.Append(strName + ",");
            //to populate value in this textbox from respective hidden field
            //strRef = "//control[@name='" + "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWOutstanding" + "']";
            strRef = "//control[@name='" + strName + "']";
            strType = "currency";
            strReadOnly = "true";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly); 
            //objControlElement.InnerText = ConvertDoubleToCurrencyStr(dBalAmt); //string.Format("{0:C}", dBalAmt); 
            objControlElement.SetAttribute("CurrencyValue", ConvertDoubleToCurrencyStr(dBalAmt));
            objControlElement.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dBalAmt, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId); //Aman Multi Currency
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            objDisplayColElement = GetLineBreakDisplayElement(workSheetDoc, reserveTypeElement.GetAttribute("name").ToString() + "_linebreak4");
            reserveTypeElement.AppendChild(objDisplayColElement);

            //new display column for Reason
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Reason   ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Reason";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "ReserveTypeReason";
            if (reasonCommentsRequired) //nnorouzi; MITS 18989
            {
                lblCssClass += " required";
            }
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_ReserveWorksheetReason";
            strControlToEditList.Append(strName + ",");
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "code";  //type="code" codetable="REASON" 
            strReadOnly = "";
            lblCssClass = "oneThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, threeCntrlClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("codetable", "REASON");
            objControlElement.SetAttribute("reasoncode", "");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;

            //new display column for Comments
            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            strTitle = "Comments   ";
            strName = "lbl_" + reserveTypeElement.GetAttribute("name").ToString() + "_Comments";
            strRef = "//control[@name='" + strName + "']";
            strType = "labelonly";
            strReadOnly = "true";
            lblCssClass = "ReserveTypeComments";
            if (reasonCommentsRequired) //nnorouzi; MITS 18989
            {
                lblCssClass += " required";
            }
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            strTitle = "";
            strName = "txt_" + reserveTypeElement.GetAttribute("name").ToString() + "_Comments";
            strControlToEditList.Append(strName + ",");
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "memo";
            strReadOnly = "";
            lblCssClass="twoThird";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.SetAttribute("cols", "82");
            objControlElement.SetAttribute("rows", "4");
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            //strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_NEWTOTALINCURRED";
            //strControlToClear.Append(strName + ",");

            strTitle = "";
            strName = "hdn_" + reserveTypeElement.GetAttribute("name").ToString() + "_Comments";
            strControlToClear.Append(strName + ",");
            strRef = "//control[@name='" + strName + "']";
            strType = "hidden";
            strReadOnly = "";
            objControlElement = GetControlElementForWorkSheet(workSheetDoc, strTitle, lblCssClass, strName, strRef, strType, strReadOnly);
            objControlElement.InnerText = "";
            objDisplayColElement.AppendChild(objControlElement);
            objControlElement = null;

            reserveTypeElement.AppendChild(objDisplayColElement);
            objDisplayColElement = null;
            sbSQL = null;
            return reserveTypeElement;
        }

        private XmlElement GetControlElementForWorkSheet(XmlDocument workSheetDoc, string strTitle, string strClass, string strName, string strRef, string strType, string strReadonly)
        {
            XmlElement objControlElement = null;

            objControlElement = workSheetDoc.CreateElement("control");
            objControlElement.SetAttribute("name", strName);
            objControlElement.SetAttribute("ref", strRef);
            objControlElement.SetAttribute("class", strClass);
            objControlElement.SetAttribute("readonly", strReadonly);
            objControlElement.SetAttribute("title", strTitle);
            objControlElement.SetAttribute("type", strType);

            return objControlElement;
        }

        private XmlElement CreateSectionForWorksheet(XmlDocument workSheetDoc, string strName)
        {
            XmlElement objSectionElement = null;

            objSectionElement = workSheetDoc.CreateElement("control");
            objSectionElement.SetAttribute("name", strName);

            return objSectionElement;
            
        }

        private XmlElement GetNewGroupElementForWorkSheet(XmlDocument workSheetDoc, string strName, int iSelected, string strTitle)
        {
            XmlElement objGrouptElement = null;

            objGrouptElement = workSheetDoc.CreateElement("group");
            objGrouptElement.SetAttribute("name", strName);
            if (iSelected == 1)
            {
                objGrouptElement.SetAttribute("selected", "1");
            }
            objGrouptElement.SetAttribute("title", strTitle.ToUpper());

            return objGrouptElement;
        }

        
        private double GetPaidAmountForTransType(int p_iTransId,int p_iReserveTypeCode,int p_iClaimId,int p_iClaimantRowId,int p_iUnitRowId,bool p_bCollInRsvBal)
        {
            StringBuilder sbSQL = null;
            DbReader objReader;
            double dPaidAmount = 0;
            double dCollectionAmount = 0;
            int iClaimantEid=0;
            int iUnitId=0;
            int iReserveTrackingLevel = 0;
            int iLOBCode = 0;

            try
            {
                sbSQL = new StringBuilder();

                //FUNDS.COLLECTION_FLAG = -1 

                sbSQL.Append("SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) PaidAmount");
                sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.PAYMENT_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                sbSQL.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = " + p_iTransId + " AND FUNDS.CLAIM_ID = " + p_iClaimId);

                iReserveTrackingLevel = GetReserveTracking(p_iClaimId, ref iLOBCode);

                if (p_iClaimantRowId > 0)
                {
                    GetClaimantId(p_iClaimantRowId, ref iClaimantEid);

                    if (iLOBCode == 241)   //GC
                    {
                        if (iReserveTrackingLevel == 1)  //detail level tracking is ON
                        {
                            if (iClaimantEid > 0)
                            {
                                sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //claim level tracking is ON
                        {
                            if (iClaimantEid != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                            //changed by shobhana MITS 20774
                        else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                        {
                            if (iClaimantEid != 0)
                            {
                                sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            }
                        }
                        //END changed by shobhana MITS 20774
                    }
                }
                else if (p_iUnitRowId > 0)
                {
                    GetUnitId(p_iUnitRowId, ref iUnitId);

                    if (iLOBCode == 242) //VA
                    {
                        if (iReserveTrackingLevel == 1) //detail level tracking is ON
                        {
                            if (iUnitId > 0)
                            {
                                sbSQL.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                            }
                        }
                        else if (iReserveTrackingLevel == 0)  //Claim level tracking is ON
                        {
                            if (iUnitId != 0)
                            {
                                throw new Exception("Detail level tracking is OFF in utiltiy settings");
                            }
                        }
                        //changed by shobhana MITS 20774
                        else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                        {
                            if (iClaimantEid > 0)
                            {
                                sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            }
                            else if (iUnitId > 0)
                            {
                                //create desired query
                                sbSQL.Append(" AND UNIT_ID = " + iUnitId);
                            }
                        }
                        //END
                    }
                }

                sbSQL.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");
               
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReader.Read())
                {
                    dPaidAmount = objReader.GetDouble("PaidAmount");
                }
                sbSQL.Remove(0, sbSQL.Length);
                objReader.Close();


                if (p_bCollInRsvBal)
                {
                    sbSQL.Append("SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) CollectionAmount");
                    sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT");
                    sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.COLLECTION_FLAG = -1 AND FUNDS.VOID_FLAG = 0 ");
                    sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                    sbSQL.Append(" AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = " + p_iTransId + " AND FUNDS.CLAIM_ID = " + p_iClaimId);

                    if (p_iClaimantRowId > 0)
                    {
                        GetClaimantId(p_iClaimantRowId, ref iClaimantEid);
                        sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + iClaimantEid);
                    }
                    else if (p_iUnitRowId > 0)
                    {
                        GetUnitId(p_iUnitRowId, ref iUnitId);
                        sbSQL.Append("AND FUNDS.UNIT_ID = " + iUnitId);
                    }

                    sbSQL.Append(" GROUP BY FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");
                    
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    if (objReader.Read())
                    {
                        dCollectionAmount  = objReader.GetDouble("CollectionAmount");
                    }
                    sbSQL.Remove(0, sbSQL.Length);
                    objReader.Close();

                    dPaidAmount = dPaidAmount - dCollectionAmount;

                    if (dPaidAmount < 0)
                    {
                        dPaidAmount = 0;
                    }
                }
                
                return dPaidAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private XmlElement GetLineBreakDisplayElement(XmlDocument workSheetDoc, string cntrlName)
        {
            XmlElement objDisplayColElement = null;
            XmlElement objControlElement = null;

            objDisplayColElement = workSheetDoc.CreateElement("displaycolumn");

            objControlElement = workSheetDoc.CreateElement("control");
            objControlElement.SetAttribute("type", "linebreak");
            objControlElement.SetAttribute("name", cntrlName);

            objDisplayColElement.AppendChild(objControlElement);

            return objDisplayColElement;
        }

        private string GetQueryToReserveAmounts(int p_iReserveTypeCode, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId)
        {
            StringBuilder sbSql = null;
            int iReserveTrackingLevel = 0;
            int iLOBCode = 0;
            int iClaimantEid=0;
            int iUnitId=0;
            try
            {
                iReserveTrackingLevel = GetReserveTracking(p_iClaimId, ref iLOBCode);
                if (p_iClaimantRowId != 0)
                {
                    GetClaimantId(p_iClaimantRowId, ref   iClaimantEid);
                }
                else if (p_iUnitRowId != 0)
                {
                    GetUnitId(p_iUnitRowId, ref iUnitId);  
                }
                sbSql = new StringBuilder();
                //akaur9 Multi Currency --Start
                //sbSql.Append("SELECT RC_ROW_ID, PAID_TOTAL - COLLECTION_TOTAL PAID_BALANCE, PAID_TOTAL, RESERVE_AMOUNT,");
                //sbSql.Append(" BALANCE_AMOUNT, INCURRED_AMOUNT, COLLECTION_TOTAL FROM RESERVE_CURRENT");
                //sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);

                //sbSql.Append("SELECT RC_ROW_ID, PAID_TOTAL - COLLECTION_TOTAL PAID_BALANCE, CLAIM_CURRENCY_PAID_TOTAL - CLAIM_CURR_COLLECTION_TOTAL CLAIM_CURRENCY_PAID_BALANCE, PAID_TOTAL, CLAIM_CURRENCY_PAID_TOTAL, RESERVE_AMOUNT,");
                //sbSql.Append(" CLAIM_CURRENCY_RESERVE_AMOUNT, BALANCE_AMOUNT, CLAIM_CURRENCY_BALANCE_AMOUNT, INCURRED_AMOUNT, CLAIM_CURRENCY_INCURRED_AMOUNT, COLLECTION_TOTAL, CLAIM_CURR_COLLECTION_TOTAL FROM RESERVE_CURRENT");
                //sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);


                if (objSysSetting.UseMultiCurrency != 0) //MITS 30285 Start mcapps2
                {
                    //akaur9 Multi Currency --Start
                    sbSql.Append("SELECT RC_ROW_ID, PAID_TOTAL - COLLECTION_TOTAL PAID_BALANCE, CLAIM_CURRENCY_PAID_TOTAL - CLAIM_CURR_COLLECTION_TOTAL CLAIM_CURRENCY_PAID_BALANCE, PAID_TOTAL, CLAIM_CURRENCY_PAID_TOTAL, RESERVE_AMOUNT,");
                    sbSql.Append(" CLAIM_CURRENCY_RESERVE_AMOUNT, BALANCE_AMOUNT, CLAIM_CURRENCY_BALANCE_AMOUNT, INCURRED_AMOUNT, CLAIM_CURRENCY_INCURRED_AMOUNT, COLLECTION_TOTAL, CLAIM_CURR_COLLECTION_TOTAL FROM RESERVE_CURRENT");
                    sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                }
                else
                {
                    sbSql.Append("SELECT RC_ROW_ID, PAID_TOTAL - COLLECTION_TOTAL PAID_BALANCE, PAID_TOTAL, RESERVE_AMOUNT,");
                    sbSql.Append(" BALANCE_AMOUNT, INCURRED_AMOUNT, COLLECTION_TOTAL FROM RESERVE_CURRENT");
                    sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                }
                //MITS 30285 End mcapps2

                //akaur9 Multi Currency --End
                if (iLOBCode == 241 || iLOBCode == 845)   //GC //Add (iLOBCode == 845) condition by kuladeep for mits:34961 
                {
                    if (iReserveTrackingLevel == 1)  //detail level tracking is ON
                    {
                        if (iClaimantEid > 0)
                        {
                            //create desired query
                            sbSql.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                        }
                    }
                    else if (iReserveTrackingLevel == 0)  //claim level tracking is ON
                    {
                        if (iClaimantEid != 0)
                        {
                            throw new Exception("Detail level tracking is OFF in utiltiy settings");
                        }
                    }
                    //changed by shobhana MITS 20774
                    else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                    {
                        if (iClaimantEid != 0)
                        {
                            sbSql.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                        }
                        else
                        {
                            sbSql = new StringBuilder();
                            //akaur9 Multi Currency --Start
                            //sbSql.Append("SELECT (SUM(PAID_TOTAL) - SUM(COLLECTION_TOTAL)) PAID_BALANCE, SUM(PAID_TOTAL) PAID_TOTAL, SUM(RESERVE_AMOUNT) RESERVE_AMOUNT,");
                            //sbSql.Append(" SUM(BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(INCURRED_AMOUNT) INCURRED_AMOUNT,SUM(COLLECTION_TOTAL) COLLECTION_TOTAL FROM RESERVE_CURRENT");
                            //sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode + " ");
                            //sbSql.Append("GROUP BY RESERVE_TYPE_CODE ");

                            //sbSql.Append("SELECT (SUM(PAID_TOTAL) - SUM(COLLECTION_TOTAL)) PAID_BALANCE, (SUM(CLAIM_CURRENCY_PAID_TOTAL) - SUM(CLAIM_CURR_COLLECTION_TOTAL)) CLAIM_CURRENCY_PAID_BALANCE, SUM(PAID_TOTAL) PAID_TOTAL,");
                            //sbSql.Append(" SUM(CLAIM_CURRENCY_PAID_TOTAL) CLAIM_CURRENCY_PAID_TOTAL, SUM(RESERVE_AMOUNT) RESERVE_AMOUNT, SUM(CLAIM_CURRENCY_RESERVE_AMOUNT) CLAIM_CURRENCY_RESERVE_AMOUNT,");
                            //sbSql.Append(" SUM(BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(CLAIM_CURRENCY_BALANCE_AMOUNT) CLAIM_CURRENCY_BALANCE_AMOUNT, SUM(INCURRED_AMOUNT) INCURRED_AMOUNT,");
                            //sbSql.Append(" SUM(CLAIM_CURRENCY_INCURRED_AMOUNT) CLAIM_CURRENCY_INCURRED_AMOUNT, SUM(COLLECTION_TOTAL) COLLECTION_TOTAL, SUM(CLAIM_CURR_COLLECTION_TOTAL) CLAIM_CURR_COLLECTION_TOTAL FROM RESERVE_CURRENT");
                            //sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode + " ");
                            //sbSql.Append("GROUP BY RESERVE_TYPE_CODE ");
                            //akaur9 Multi Currency --End
                            if (objSysSetting.UseMultiCurrency != 0)  //MITS 30285 Start mcapps2
                            {
                                //akaur9 Multi Currency --Start
                                sbSql.Append("SELECT (SUM(PAID_TOTAL) - SUM(COLLECTION_TOTAL)) PAID_BALANCE, (SUM(CLAIM_CURRENCY_PAID_TOTAL) - SUM(CLAIM_CURR_COLLECTION_TOTAL)) CLAIM_CURRENCY_PAID_BALANCE, SUM(PAID_TOTAL) PAID_TOTAL,");
                                sbSql.Append(" SUM(CLAIM_CURRENCY_PAID_TOTAL) CLAIM_CURRENCY_PAID_TOTAL, SUM(RESERVE_AMOUNT) RESERVE_AMOUNT, SUM(CLAIM_CURRENCY_RESERVE_AMOUNT) CLAIM_CURRENCY_RESERVE_AMOUNT,");
                                sbSql.Append(" SUM(BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(CLAIM_CURRENCY_BALANCE_AMOUNT) CLAIM_CURRENCY_BALANCE_AMOUNT, SUM(INCURRED_AMOUNT) INCURRED_AMOUNT,");
                                sbSql.Append(" SUM(CLAIM_CURRENCY_INCURRED_AMOUNT) CLAIM_CURRENCY_INCURRED_AMOUNT, SUM(COLLECTION_TOTAL) COLLECTION_TOTAL, SUM(CLAIM_CURR_COLLECTION_TOTAL) CLAIM_CURR_COLLECTION_TOTAL FROM RESERVE_CURRENT");
                                sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode + " ");
                                sbSql.Append("GROUP BY RESERVE_TYPE_CODE ");
                            }
                            else
                            {
                                sbSql.Append("SELECT (SUM(PAID_TOTAL) - SUM(COLLECTION_TOTAL)) PAID_BALANCE, SUM(PAID_TOTAL) PAID_TOTAL, SUM(RESERVE_AMOUNT) RESERVE_AMOUNT,");
                                sbSql.Append(" SUM(BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(INCURRED_AMOUNT) INCURRED_AMOUNT,SUM(COLLECTION_TOTAL) COLLECTION_TOTAL FROM RESERVE_CURRENT");
                                sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode + " ");
                                sbSql.Append("GROUP BY RESERVE_TYPE_CODE ");
                            }
                            //MITS 30285 End mcapps2 

                        }
                    }
                    //END changed by shobhana MITS 20774
                }
                else if (iLOBCode == 242) //VA
                {
                    if (iReserveTrackingLevel == 1)
                    {
                        if (iUnitId  > 0)
                        {
                            //create desired query
                            sbSql.Append(" AND UNIT_ID = " + iUnitId);
                        }
                        else if (iClaimantEid > 0)//Add by kuladeep for MITS:34903-Regarding work correctly claimant level
                        {
                            //create desired query
                            sbSql.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                        }
                    }
                    else if(iReserveTrackingLevel == 0)  //Claim level tracking is ON
                    {
                        if (iUnitId  != 0)
                        {
                            throw new Exception("Detail level tracking is OFF in utiltiy settings");
                        }
                    }
                    //changed by shobhana MITS 20774
                    else if (iReserveTrackingLevel == 2)  //claim&detail level tracking is ON
                    {
                        if (iClaimantEid > 0)
                        {
                            sbSql.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                        }
                        else if (iUnitId > 0)
                        {
                            //create desired query
                            sbSql.Append(" AND UNIT_ID = " + iUnitId);
                        }   
                        else
                        {
                            sbSql = new StringBuilder();
                            sbSql.Append("SELECT (SUM(PAID_TOTAL) - SUM(COLLECTION_TOTAL)) PAID_BALANCE, SUM(PAID_TOTAL) PAID_TOTAL, SUM(RESERVE_AMOUNT) RESERVE_AMOUNT,");
                            sbSql.Append(" SUM(BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(INCURRED_AMOUNT) INCURRED_AMOUNT,SUM(COLLECTION_TOTAL) COLLECTION_TOTAL FROM RESERVE_CURRENT");
                            sbSql.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode + " ");
                            sbSql.Append("GROUP BY RESERVE_TYPE_CODE ");


                        }
                    }
                    //changed by shobhana MITS 20774
                }

                return sbSql.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
      //changed parameters by shobhana for customization reserves

        private void FillNewWorkSheet(ref XmlDocument p_objXML, int p_iClaimId, int p_iClaimantRowId, int p_iUnitRowId, string p_sFromApproved, string p_sRSWCustomReserveXML)
        {
            LocalCache objCache = null;
            XmlDocument objApprovedXML = null;
            DbReader objReader = null;
            XmlNode objNode = null;
            string[] arrFields = null;
            XmlNode oCurrentNode = null;
            string sTransType = string.Empty;
            string curReserveBalText = string.Empty;
            double dPaidToDateAmount = 0.0;
            double dReserveAmount = 0.0;
            double dReserveBalance = 0.0;
            XmlNode xmlTempNode = null;
            try
            {
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring, m_iClientId);
            objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);           
            // Get the approved xml in objApprovedXML if p_sFromApproved <> empty
            if (!String.IsNullOrEmpty(p_sFromApproved))
            {
                string sSQL = "SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " +
                    p_sFromApproved + " AND RSW_STATUS_CODE = "
                    +   objCache.GetCodeId("AP", "RSW_STATUS");
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                objApprovedXML = new XmlDocument();

                if (objReader.Read())
                    objApprovedXML.LoadXml(objReader.GetValue("RSW_XML").ToString());
                else // In case no xml found, create from scratch
                    p_sFromApproved = "";

                objReader.Close();
            }

            //Creating new fresh Xml
            p_objXML = CreateNewXml(p_iClaimId, p_iClaimantRowId, p_iUnitRowId, p_sRSWCustomReserveXML);
            

            // Populating values from an approved reserve worksheet in to fresh xml
            if (!String.IsNullOrEmpty(p_sFromApproved)) 
            {
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCtrlToClear']");
                if (objNode != null)
                { arrFields = objNode.InnerText.Split(','); }

                for (int count = 0; count < arrFields.Length; count++)
                {
                    objNode = p_objXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']");
                    if (objNode != null)
                    {
                        //Raman 10/12/2009 Adding a null check
                        oCurrentNode = objApprovedXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']");
                        if (oCurrentNode != null)
                        {
                            //changes for added New ReserveBalance textbox in transtypes
                            curReserveBalText = oCurrentNode.Attributes["name"].Value;

                            //cloning only Incurred amount data
                            if (curReserveBalText.Contains("_TransType"))
                            {
                                if (curReserveBalText.Contains("_TransTypeNewReserveBal_"))
                                {
                                    if (curReserveBalText.Contains("txt_ReserveType_"))
                                    {
                                        string sReserveType = curReserveBalText.Substring("txt_ReservType_".Length); //,curReserveBalText.Substring(curReserveBalText.IndexOf("_TransTypeNewReserveBal_")).Length);

                                        sReserveType = sReserveType.Substring(1, sReserveType.Substring(0, (sReserveType.Length - sReserveType.Substring(sReserveType.IndexOf("_TransTypeNewReserveBal_")).Length - 1)).Length);

                                        sTransType = curReserveBalText.Substring(curReserveBalText.LastIndexOf('_') + 1);

                                        //dPaidToDateAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(p_objXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_Paid_" + sTransType + "']").InnerText);
                                        //nsachdeva2 - MITS: 29264 - 7/8/2012
                                        //xmlTempNode = p_objXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_Paid_" + sTransType + "']");
                                        //dPaidToDateAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(xmlTempNode.Attributes["CurrencyValue"].Value);
                                        if (objSysSetting.UseMultiCurrency != 0)
                                        {
                                            xmlTempNode = p_objXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_Paid_" + sTransType + "']");
                                            dPaidToDateAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(xmlTempNode.Attributes["CurrencyValue"].Value);
                                        }
                                        else
                                        {
                                            dPaidToDateAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(p_objXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_Paid_" + sTransType + "']").InnerText);
                                        }
                                        
                                        //dReserveAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(objApprovedXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_TransTypeIncurredAmount_" + sTransType + "']").InnerText);
                                        //xmlTempNode = objApprovedXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_TransTypeIncurredAmount_" + sTransType + "']");
                                        //dReserveAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(xmlTempNode.Attributes["CurrencyValue"].Value);
                                        if (objSysSetting.UseMultiCurrency != 0)
                                        {
                                            xmlTempNode = objApprovedXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_TransTypeIncurredAmount_" + sTransType + "']");
                                            dReserveAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(xmlTempNode.Attributes["CurrencyValue"].Value);
                                        }
                                        else
                                        {
                                        dReserveAmount = Riskmaster.Common.Conversion.ConvertStrToDouble(objApprovedXML.SelectSingleNode("//control[@name='txt_ReserveType_" + sReserveType + "_TransTypeIncurredAmount_" + sTransType + "']").InnerText);
                                        }
                                        //End MITS:29264

                                        //MITS 30191 Raman Bhatia: Implementation of recovery reserve
                                        if (IsRecoveryReserve(Conversion.ConvertStrToInteger(sReserveType)))
                                        {
                                            double dColl = Riskmaster.Common.Conversion.ConvertStrToDouble(p_objXML.SelectSingleNode("//control[@name='hdn_ReserveType_" + sReserveType + "_Collection']").InnerText);
                                            dReserveBalance = dReserveAmount - dColl;
                                        }
                                        else
                                        {
                                            dReserveBalance = dReserveAmount - dPaidToDateAmount;
                                        }

                                        if (objSysSetting.UseMultiCurrency == 0)
                                        {
                                            objNode.InnerText = ConvertDoubleToCurrencyStr(dReserveBalance);  //string.Format("{0:C}", dReserveBalance);
                                        }
                                        else
                                        {
                                            objNode.InnerText = CommonFunctions.ConvertCurrency(p_iClaimId, dReserveBalance, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                                        }
                                            objNode.Attributes["CurrencyValue"].Value = ConvertDoubleToCurrencyStr(dReserveBalance); 
                                        //also populate hdnField corresponding to NewReserveBal

                                       // p_objXML.SelectSingleNode("//control[@name='hdn_ReserveType_" + sReserveType + "_TransTypeNewReserveBal_" + sTransType + "']").InnerText = oCurrentNode.InnerText;
                                        p_objXML.SelectSingleNode("//control[@name='hdn_ReserveType_" + sReserveType + "_TransTypeNewReserveBal_" + sTransType + "']").InnerText = oCurrentNode.InnerText;
                                       // p_objXML.SelectSingleNode("//control[@name='hdn_ReserveType_" + sReserveType + "_TransTypeNewReserveBal_" + sTransType + "']").Attributes["CurrencyValue"].Value = oCurrentNode.Attributes["CurrencyValue"].Value;
                                    }

                                }
                                // else if  (curReserveBalText.Contains("_TransTypeIncurredAmount_"))
                                //{
                                //    if (curReserveBalText.Contains("txt_ReserveType_"))
                                //    {
                                    
                                    
                                //    }
                                

                                //}
                                else
                                {
                                    //nsachdeva2 - MITS: 29264 - 7/8/2012
                                    //if (objNode.Attributes["type"].Value == "currency")
                                    //{
                                    //    objNode.InnerText = oCurrentNode.InnerText;
                                    //    objNode.Attributes["CurrencyValue"].Value = (oCurrentNode.Attributes["CurrencyValue"].Value);
                                    //}
                                    if (objSysSetting.UseMultiCurrency != 0 && objNode.Attributes["type"].Value == "currency")
                                    {
                                        objNode.InnerText = oCurrentNode.InnerText;
                                        objNode.Attributes["CurrencyValue"].Value = (oCurrentNode.Attributes["CurrencyValue"].Value);

                                    }
                                    else if (objNode.Attributes["type"].Value == "currency")
                                    {
                                        objNode.InnerText = oCurrentNode.InnerText;
                                        objNode.Attributes["CurrencyValue"].Value = oCurrentNode.InnerText;
                                    }
                                    //End MITS: 29264
                                    else

                                    objNode.InnerText = oCurrentNode.InnerText;
                                   // objNode.InnerText = oCurrentNode.InnerText;
                                    //objNode.Attributes["CurrencyValue"].Value = ConvertDoubleToCurrencyStr(dReserveBalance); 
                                }
                            }
                        }
                    }
                }
            }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.FillNewCRW.Err", m_iClientId), ex);
            }
            finally
            {
                objCache = null;
                oCurrentNode = null;
            }

        }

        ///<summary>
        /// This function saves the Reserve Worksheet: a New as well as an existing worksheet
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <returns>XML to be rendered on screen</returns>
        private XmlDocument SaveReserveWorksheet(XmlDocument p_objXMLIn)
        {
            XmlNode objNode = null;
            int iRSWId = 0;
            int iRSWHistId = 0;
            int iRowId = 0;//7810
            int iStatusCode = 0;
            StringBuilder sbSQL;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            DbConnection objCon = null;
            DbCommand objCmd = null;
            DbParameter objParameter = null;

            long lUserMId = 0;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;

            //Add by kuladeep for mits:26904 Start
            DbWriter objWriter = null;
            //Add by kuladeep for mits:26904 End
            int iHoldStatusCode = 0;//JIRA 6385 Snehal
          
            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objCache = new LocalCache(m_CommonInfo.Connectionstring, ClientId);//7810
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                if (objNode != null)
                {
                    sSubmittedTo = objNode.InnerText;
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                if (objNode != null)
                {
                    lUserMId = Riskmaster.Common.Conversion.ConvertStrToLong(objNode.InnerText);
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSupName']");
                if (objNode != null)
                {
                    sSupName = objNode.InnerText;
                }

              
                iRSWId = Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                //Mits 19027
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring, m_iClientId);
                bPrevModValtToZero = objSysSetting.PrevResModifyzero;
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnPrevModValtoZero']");
                if (objNode != null)
                    objNode.InnerText = bPrevModValtToZero.ToString();
                if (iRSWId == -1) // New worksheet
                {
                    #region New Reserve Worksheet

                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);
                    }

                    iRSWId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSW_WORKSHEETS", base.ClientId);

                    // Set the control 'hdnRSWid' with the new RSW id
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']");
                    if (objNode != null)
                        objNode.InnerText = iRSWId.ToString();

                    // Insert into database table 'RSW_WORKSHEETS' new record
                    //Change done by kuladeep for mits:26904 Start

                    //sbSQL.Append("INSERT INTO RSW_WORKSHEETS (RSW_ROW_ID,RSWXMLTYPE,CLAIM_ID,CLAIMANT_ROW_ID,UNIT_ROW_ID,RSW_TYPE_CODE, RSW_STATUS_CODE, SUBMITTED_BY, SUBMITTED_TO, ");
                    //sbSQL.Append("ADDED_BY_USER, UPDATED_BY_USER, DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, APPROVED_BY, DTTM_APPROVED,SUMMARY_COMMENTS,RSW_XML) VALUES (");
                    //sbSQL.Append(iRSWId.ToString() + ", '");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWXmlType']").InnerText + "', ");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText + ", ");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimantRowId']").InnerText + ", ");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnUnitRowId']").InnerText + ", ");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWType']").InnerText + ", ");
                    //sbSQL.Append(iStatusCode.ToString() + ", '");
                    //sbSQL.Append(m_CommonInfo.UserId.ToString() + "', '");
                    //sbSQL.Append(lUserMId.ToString() + "', ");
                    //sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnCreatedBy']").InnerText) + ", ");
                    //sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText) + ", '");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='dtCreated']/@dbformat").InnerText + "', '");
                    //sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "', ");
                    //sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText) + ", '");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText + "', '");
                    //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText + "',~XML~)");

                    //objCon.Open();
                    //objCmd = objCon.CreateCommand();

                    //objParameter = objCmd.CreateParameter();
                    //objParameter.Value = p_objXMLIn.OuterXml;
                    //objParameter.ParameterName = "XML";
                    //objCmd.Parameters.Add(objParameter);

                    //objCmd.CommandText = sbSQL.ToString();
                    //sbSQL.Remove(0, sbSQL.Length);
                    //objCmd.ExecuteNonQuery();

                    objWriter = DbFactory.GetDbWriter(m_CommonInfo.Connectionstring);
                    objWriter.Tables.Add("RSW_WORKSHEETS");
                    objWriter.Fields.Add("RSW_ROW_ID", iRSWId);
                    objWriter.Fields.Add("RSWXMLTYPE", p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWXmlType']").InnerText);
                    objWriter.Fields.Add("CLAIM_ID", p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']").InnerText);
                    objWriter.Fields.Add("CLAIMANT_ROW_ID", p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimantRowId']").InnerText);
                    objWriter.Fields.Add("UNIT_ROW_ID", p_objXMLIn.SelectSingleNode("//control[@name='hdnUnitRowId']").InnerText);
                    objWriter.Fields.Add("RSW_TYPE_CODE", p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWType']").InnerText);
                    objWriter.Fields.Add("RSW_STATUS_CODE", iStatusCode);
                    objWriter.Fields.Add("SUBMITTED_BY", m_CommonInfo.UserId);
                    objWriter.Fields.Add("SUBMITTED_TO", lUserMId);
                    // asingh263 Changed for MITS 35300 Starts
                    // No need to call function Utilities.FormatSqlFieldValue as DbWriter handles parametrised queries.
                    //objWriter.Fields.Add("ADDED_BY_USER", Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnCreatedBy']").InnerText));
                    //objWriter.Fields.Add("UPDATED_BY_USER", Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText));
                    objWriter.Fields.Add("ADDED_BY_USER", p_objXMLIn.SelectSingleNode("//control[@name='hdnCreatedBy']").InnerText);
                    objWriter.Fields.Add("UPDATED_BY_USER", p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText);
                    // asingh263 Changed for MITS 35300 Ends
                    objWriter.Fields.Add("DTTM_RCD_ADDED", p_objXMLIn.SelectSingleNode("//control[@name='dtCreated']/@dbformat").InnerText);
                    objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                    // asingh263 Changed for MITS 35300 Starts
                    //objWriter.Fields.Add("APPROVED_BY", Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText));
                    objWriter.Fields.Add("APPROVED_BY", p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText);
                    // asingh263 Changed for MITS 35300 Ends
                    objWriter.Fields.Add("DTTM_APPROVED", p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText);
                    objWriter.Fields.Add("SUMMARY_COMMENTS", p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText.Replace("'", "").Replace('"', ' '));
                    objWriter.Fields.Add("RSW_XML", p_objXMLIn.OuterXml);
                    objWriter.Execute();

                    //Change done by kuladeep for mits:26904 End

                    //For Future Safeway: Adding data into Tables RSWtoTables(p_objXMLIn, objCon, iHisId);

                    #region Reserve Approval History
                    string sReason = "";
                    iRSWHistId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSV_APPROVAL_HIST", base.ClientId);

                    //Change done by kuladeep for mits:26904 Start
            
                    //// Insert into database table 'RSV_APPROVAL_HIST' new record
                    //sbSQL.Append("INSERT INTO RSV_APPROVAL_HIST (RSW_HIST_ROW_ID, RSW_ROW_ID, RSW_STATUS_CODE, REASON, APPROVER_ID, ");
                    //sbSQL.Append("CHANGED_BY_USER, DTTM_APPROVAL_CHGD) VALUES (");
                    //sbSQL.Append(iRSWHistId.ToString() + ", ");
                    //sbSQL.Append(iRSWId.ToString() + ", ");
                    //sbSQL.Append(iStatusCode.ToString() + ", ");
                    //sbSQL.Append(Utilities.FormatSqlFieldValue(sReason) + ", ");
                    //sbSQL.Append(lUserMId.ToString() + ", ");
                    //sbSQL.Append(Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText) + ", '");
                    //sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "')");

                    //objCmd.CommandText = sbSQL.ToString();
                    //sbSQL.Remove(0, sbSQL.Length);
                    //objCmd.ExecuteNonQuery();

                    objWriter.Reset(true);
                    objWriter.Tables.Add("RSV_APPROVAL_HIST");
                    objWriter.Fields.Add("RSW_HIST_ROW_ID", iRSWHistId);
                    objWriter.Fields.Add("RSW_ROW_ID", iRSWId);
                    objWriter.Fields.Add("RSW_STATUS_CODE", iStatusCode);
                    objWriter.Fields.Add("REASON", Utilities.FormatSqlFieldValue(sReason));
                    objWriter.Fields.Add("APPROVER_ID", lUserMId);
                    objWriter.Fields.Add("CHANGED_BY_USER", Utilities.FormatSqlFieldValue(p_objXMLIn.SelectSingleNode("//control[@name='hdnLastModBy']").InnerText));
                    objWriter.Fields.Add("DTTM_APPROVAL_CHGD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                    objWriter.Execute();

                    //Change done by kuladeep for mits:26904 End
                    #endregion

                    #endregion
                }
                else // existing worksheet
                {
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    if (objNode != null)
                    {
                        if (objNode.InnerText.IndexOf("Rejected") != -1) // If a 'rejected worksheet is being saved.
                        {
                            #region If a rejected worksheet is being saved

                            iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                            // Update record in database table 'RSW_WORKSHEETS'
                            //Change done by kuladeep for mits:26904 Start
                            //sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = ");
                            //sbSQL.Append(iStatusCode.ToString());
                            //sbSQL.Append(", UPDATED_BY_USER = '");
                            //sbSQL.Append(m_CommonInfo.UserName);
                            //sbSQL.Append("', DTTM_RCD_LAST_UPD = '");
                            //sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            //sbSQL.Append("', SUMMARY_COMMENTS = '");
                            //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText);
                            //sbSQL.Append("' WHERE RSW_ROW_ID = ");
                            //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                            //objCon.Open();
                            //objCmd = objCon.CreateCommand();

                            //objCmd.CommandText = sbSQL.ToString();
                            //sbSQL.Remove(0, sbSQL.Length);
                            //objCmd.ExecuteNonQuery();

                            objWriter = DbFactory.GetDbWriter(m_CommonInfo.Connectionstring);
                            objWriter.Tables.Add("RSW_WORKSHEETS");
                            objWriter.Fields.Add("RSW_STATUS_CODE", iStatusCode);
                            objWriter.Fields.Add("UPDATED_BY_USER", m_CommonInfo.UserName);
                            objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            objWriter.Fields.Add("SUMMARY_COMMENTS", p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText.Replace("'", "").Replace('"', ' '));
                            objWriter.Where.Add("RSW_ROW_ID="+p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                            objWriter.Execute();
                         

                            //Change done by kuladeep for mits:26904 End


                            //Change done by kuladeep for mits:26904 Start
                            // Update xml in database table 'RSW_WORKSHEETS'
                            //sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_XML = ~XML~ WHERE RSW_ROW_ID = ");
                            //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                            //objParameter = objCmd.CreateParameter();
                            //objParameter.Value = p_objXMLIn.OuterXml;
                            //objParameter.ParameterName = "XML";
                            //objCmd.Parameters.Add(objParameter);

                            //objCmd.CommandText = sbSQL.ToString();
                            //sbSQL.Remove(0, sbSQL.Length);
                            //objCmd.ExecuteNonQuery();

                            objWriter.Reset(true);
                            objWriter.Tables.Add("RSW_WORKSHEETS");
                            objWriter.Fields.Add("RSW_XML", p_objXMLIn.OuterXml);
                            objWriter.Where.Add("RSW_ROW_ID = " + p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                            objWriter.Execute();
                            //Change done by kuladeep for mits:26904 End

                            //For Future Safeway: Insert a new record in RSW Tables RSWtoTables(p_objXMLIn, objCon, iHisId);

                            #endregion
                        }
                        else // If an existing (non-rejected) worksheet is being saved
                        {
                            #region If an existing (non-rejected) worksheet is being saved
                            iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                            // Update record in database table 'RSW_WORKSHEETS'

                            //Add and Change done by kuladeep for mits:26904 Start
                            objWriter = DbFactory.GetDbWriter(m_CommonInfo.Connectionstring);
                            objWriter.Tables.Add("RSW_WORKSHEETS");

                            if (objNode.InnerText != "History")
                            {
                               
                                //Change done by kuladeep for mits:26904 Start
                                //sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                                //sbSQL.Append(", SUBMITTED_TO = '" + lUserMId.ToString() + "'");
                                //sbSQL.Append(", UPDATED_BY_USER = '" + m_CommonInfo.UserName);
                                //sbSQL.Append("', DTTM_RCD_LAST_UPD = '");
                                //sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                //sbSQL.Append("', APPROVED_BY = '");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText);
                                //sbSQL.Append("', DTTM_APPROVED = '");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText);
                                //sbSQL.Append("', SUMMARY_COMMENTS = '");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText);
                                //sbSQL.Append("' WHERE RSW_ROW_ID = ");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                                objWriter.Fields.Add("RSW_STATUS_CODE", iStatusCode);
                                objWriter.Fields.Add("SUBMITTED_TO", lUserMId);
                                objWriter.Fields.Add("UPDATED_BY_USER", m_CommonInfo.UserName);
                                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                objWriter.Fields.Add("APPROVED_BY", p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText);
                                objWriter.Fields.Add("DTTM_APPROVED", p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText);
                                objWriter.Fields.Add("SUMMARY_COMMENTS", p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText.Replace("'", "").Replace('"', ' '));
                                //Change done by kuladeep for mits:26904 End

                            }
                            else if (objNode.InnerText == "Approved")
                            {
                                //Change done by kuladeep for mits:26904 Start
                                //sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                                //sbSQL.Append(", UPDATED_BY_USER = '" + m_CommonInfo.UserName);
                                //sbSQL.Append("', DTTM_RCD_LAST_UPD = '");
                                //sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                //sbSQL.Append("', APPROVED_BY = '");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText);
                                //sbSQL.Append("', DTTM_APPROVED = '");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText);
                                //sbSQL.Append("', SUMMARY_COMMENTS = '");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText);
                                //sbSQL.Append("' WHERE RSW_ROW_ID = ");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                                objWriter.Fields.Add("RSW_STATUS_CODE", iStatusCode);
                                objWriter.Fields.Add("UPDATED_BY_USER", m_CommonInfo.UserName);
                                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                objWriter.Fields.Add("APPROVED_BY", p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejBy']").InnerText);
                                objWriter.Fields.Add("DTTM_APPROVED", p_objXMLIn.SelectSingleNode("//control[@name='hdndtAppRej']/@dbformat").InnerText);
                                objWriter.Fields.Add("SUMMARY_COMMENTS", p_objXMLIn.SelectSingleNode("//control[@name='txt_RESERVESUMMARY_Comments']").InnerText.Replace("'", "").Replace('"', ' '));
                                //Change done by kuladeep for mits:26904 End

                            }
                            else if (objNode.InnerText == "History")
                            {
                                //Change done by kuladeep for mits:26904 Start
                                //sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_STATUS_CODE = " + iStatusCode.ToString());
                                //sbSQL.Append(", DTTM_RCD_LAST_UPD = '");
                                //sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                //sbSQL.Append("' WHERE RSW_ROW_ID = ");
                                //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                                objWriter.Fields.Add("RSW_STATUS_CODE", iStatusCode);
                                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                                //Change done by kuladeep for mits:26904 End
                            }
							//mcapps2 MITS 22943 End
                            //Change done by kuladeep for mits:26904 Start
                            //objCon.Open();
                            //objCmd = objCon.CreateCommand();
                            //objCmd.CommandText = sbSQL.ToString();
                            //objCmd.ExecuteNonQuery();
                            objWriter.Where.Add("RSW_ROW_ID=" + p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                            objWriter.Execute();
                            //Change done by kuladeep for mits:26904 End

                            sbSQL.Remove(0, sbSQL.Length);
                            // Update xml in database table 'RSW_WORKSHEETS'
                            //Change done by kuladeep for mits:26904 Start
                            //sbSQL.Append("UPDATE RSW_WORKSHEETS SET RSW_XML = ~XML~ WHERE RSW_ROW_ID = ");
                            //sbSQL.Append(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                            //objParameter = objCmd.CreateParameter();
                            //objParameter.Value = p_objXMLIn.OuterXml;
                            //objParameter.ParameterName = "XML";
                            //objCmd.Parameters.Add(objParameter);

                            //objCmd.CommandText = sbSQL.ToString();
                            //objCmd.ExecuteNonQuery();
                            //sbSQL.Remove(0, sbSQL.Length);

                            objWriter.Reset(true);
                            objWriter.Tables.Add("RSW_WORKSHEETS");
                            objWriter.Fields.Add("RSW_XML", p_objXMLIn.OuterXml);
                            objWriter.Where.Add("RSW_ROW_ID = " + p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);
                            objWriter.Execute();
                            //Change done by kuladeep for mits:26904 End
                            //For Future Safeway: Update record in database table RSWtoTables

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveReserveWorksheet.Err", m_iClientId), ex);
            }
            finally
               {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
            }
            return p_objXMLIn;
        }

        ///<summary>
        /// This function saves the Reserve Worksheet History of Approvals: a New as well as an existing worksheet
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        private void SaveReserveWorksheetHistory(XmlDocument p_objXMLIn)
        {
            XmlNode objNode = null;
            int iRSWId = 0;
            int iRSWHistId = 0;
            int iStatusCode = 0;
            int iLastStatusCode = 0;
            StringBuilder sbSQL;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            DbConnection objCon = null;
            DbCommand objCmd = null;

            long lUserMId = 0;
            int iApproverID = 0;
            string sSubmittedTo = string.Empty;
            string sSupName = string.Empty;
            string sReason = "";

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedTo']");
                if (objNode != null)
                {
                    sSubmittedTo = objNode.InnerText;
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSubmittedToID']");
                if (objNode != null)
                {
                    lUserMId = Riskmaster.Common.Conversion.ConvertStrToLong(objNode.InnerText);
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnSupName']");
                if (objNode != null)
                {
                    sSupName = objNode.InnerText;
                }

                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnAppRejComm']");
                if (objNode != null)
                {
                    sReason = objNode.InnerText;
                }

                iRSWId = Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                if (iRSWId > 0)
                {
                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                    objCon.Open();
                    iStatusCode = Riskmaster.Common.Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].InnerText);

                    sbSQL.Append("SELECT APPROVER_ID FROM RSV_APPROVAL_HIST WHERE RSW_ROW_ID = " + iRSWId);
                    sbSQL.Append(" AND RSW_HIST_ROW_ID IN (SELECT MAX(RSW_HIST_ROW_ID) FROM RSV_APPROVAL_HIST");
                    sbSQL.Append(" WHERE RSW_ROW_ID = " + iRSWId + ")");
                    iApproverID = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                    sbSQL.Remove(0, sbSQL.Length);

                    sbSQL.Append("SELECT RSW_STATUS_CODE FROM RSV_APPROVAL_HIST WHERE RSW_ROW_ID = " + iRSWId);
                    sbSQL.Append(" AND RSW_HIST_ROW_ID IN (SELECT MAX(RSW_HIST_ROW_ID) FROM RSV_APPROVAL_HIST");
                    sbSQL.Append(" WHERE RSW_ROW_ID = " + iRSWId + ")");
                    iLastStatusCode = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sbSQL.ToString()), base.ClientId);
                    sbSQL.Remove(0, sbSQL.Length);

                    if ((iApproverID != lUserMId) || (iLastStatusCode != iStatusCode))
                    {
                        objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWStatus']");
                        if (objNode != null)
                        {
                            // Update record in database table 'RSW_WORKSHEETS'
                            if (objNode.InnerText != "Pending")
                            {
                                iRSWHistId = Utilities.GetNextUID(m_CommonInfo.Connectionstring, "RSV_APPROVAL_HIST", base.ClientId);

                                // Insert into database table 'RSV_APPROVAL_HIST' new record
                                sbSQL.Append("INSERT INTO RSV_APPROVAL_HIST (RSW_HIST_ROW_ID, RSW_ROW_ID, RSW_STATUS_CODE, REASON, APPROVER_ID, ");
                                sbSQL.Append("CHANGED_BY_USER, DTTM_APPROVAL_CHGD) VALUES (");
                                sbSQL.Append(iRSWHistId.ToString() + ", ");
                                sbSQL.Append(iRSWId.ToString() + ", ");
                                sbSQL.Append(iStatusCode.ToString() + ", ");
                                sbSQL.Append(Utilities.FormatSqlFieldValue(sReason) + ", ");
                                sbSQL.Append(lUserMId.ToString() + ", ");
                                sbSQL.Append(Utilities.FormatSqlFieldValue(m_CommonInfo.UserName) + ", '");
                                sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "')");

                                objCmd = objCon.CreateCommand();
                                objCmd.CommandText = sbSQL.ToString();
                                sbSQL.Remove(0, sbSQL.Length);
                                objCmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveReserveWorksheetHistory.Err", m_iClientId), ex);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
            }
        }       
       #endregion
    }
}
