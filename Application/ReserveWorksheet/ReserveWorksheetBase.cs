﻿
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.Reserves;
using Riskmaster.Models;


///**************************************************************
///* $File		: ReserveWorksheetBase.cs
///* $Revision	: 1.0.0.0
///* $Author	: Nitin Sharma
///* $Comment	: A class representing the Reserve Work Sheet. It exposes various common methods and variables for retrieving
///*			  and modifying the reserve work sheet , that would be used by Genric and Customize implementation for Reserve Worksheet
///* $Source	: Riskmaster.Application 	
///*************************************************************/

namespace Riskmaster.Application.ReserveWorksheet
{
    public class ReserveWorksheetBase
    {
        ReserveWorksheetCommon m_CommonInfo = null;

        #region Variable Declaration

        protected int m_iDaysAppReserves = 0;

        //Added by Nitin for storing HoursAppReserves from Checkoptions
        protected int m_iHoursAppReserves = 0;

        protected bool m_bZeroEscalationReserves = true; // rrachev JIRA RMA-13311

        /// <summary>
        /// Private variable for UseSupervisoryApproval Flag for Reserves
        /// </summary>
        protected bool m_bUseSupAppReserves = false;
        /// <summary>
        /// Private variable for NotifyImmediateSupervisor Flag
        /// </summary>
        protected bool m_bNotifySupReserves = false;
        /// <summary>
        /// Private variable for UseCurrentAdjusterSupervisoryApproval Flag
        /// </summary>
        protected bool m_bUseCurrAdjReserves = false;
        //rsushilaggar MITS 26332 Date 11/24/2011
        /// <summary>
        /// Private variable for AllowAccessGroupSupervisor Flag
        /// </summary>
        protected bool m_bAllowAccessGrpSup = false;
        //End rsuhsilaggar
        /// <summary>
        /// Private variable for ReserveLimit Flag
        /// </summary>
        protected bool m_bResLimitFlag = false;
        protected bool m_bClaimIncLimitFlag = false;//sachin 6385
        /// <summary>
        /// Private variable for PreventReserveBelowPaid Flag
        /// </summary>
        protected bool m_bPrevResBelowPaid = false;
        /// <summary>
        /// Private variable for CollectionInReserveBalance Flag
        /// </summary>
        protected bool m_bCollInResBal = false;
        /// <summary>
        /// Private variable for CollectionInIncurredBalance Flag
        /// </summary>
        protected bool m_bCollInIncBal = false;
        /// <summary>
        /// Private variable for ManagerID
        /// </summary>
        protected int m_iManagerID = 0;
        protected int m_iParentSecurityId = 0;
        /// <summary>
        /// Private variable for Security Id
        /// </summary>
        protected int m_iSecurityId = 0;

        /// <summary>
        /// Private variable to level of supervisor 
        /// </summary>
        private int m_iLevel = 0;

        /// <summary>
        /// Private variable to get claim id to get LOB 
        /// </summary>
        private int m_iClaim_ID = 0;

        //Deb ML changes
        protected UserLogin m_baseUserlogin = null;

        private int m_iClientId = 0;

        protected int ClientId
        {
            get { return m_iClientId; }
            set { m_iClientId = value; }
        }

        /// <summary>
        /// Protected class to contain ClaimId, TotalPaidAmount
        /// </summary>
        protected Dictionary<int, double> dictClaimPaidToDate = null;

        //rsushilaggar: 22-Mar-2010 Mits : 19624
        protected bool m_bDisableEmailNotify = false;
        protected bool m_bDisableDiaryNotify = false;
        protected struct ReserveLimit
        {
            public int iLOBID;
            public int iResType;
            public double dMaxAmount;
        }
        //End rsushilaggar
        protected CommonFunctions.NavFormType m_eNavType = CommonFunctions.NavFormType.Reserve;//Aman Multi Currency
        #endregion

        #region Property Declaration

        /// <summary>
        /// Gets unique filename
        /// MGaba2:R6: Changed the following code to get the file from temp path rather any customized folder  
        /// </summary>
        protected string TempFile
        {
            get
            {
                string sFileName = "";
                try
                {
                    // MGaba2:R6: Changed the following code:Start
                    //DirectoryInfo objReqFolder = new DirectoryInfo(RMConfigurator.Value("Payment_PDF_Filepath"));

                    //if (!objReqFolder.Exists)
                    //    objReqFolder.Create();

                    //sFileName = RMConfigurator.Value("Payment_PDF_Filepath") + "\\" + Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf");
                    
                    sFileName = RMConfigurator.TempPath  + "\\" + Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf");
                    
                    // MGaba2:R6:End
                }
                catch (RMAppException p_objException)
                {
                    throw p_objException;
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.TempFile.Error",m_iClientId), p_objException);
                }
                return sFileName;
            }
        }

        /// <summary>
        /// Gets check option setting DaysForApproval
        /// </summary>
        protected int ResDaysForApproval
        {
            get
            {
                int ResDaysForApproval = 0;
                try
                {
                    ResDaysForApproval = m_iDaysAppReserves;
                }
                catch (RMAppException p_objException)
                {
                    throw p_objException;
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("ReserveWorksheet.ResDaysForApproval.Error",m_iClientId), p_objException);
                }
                return ResDaysForApproval;
            }
        }

        #endregion

        #region Constant Declaration for Security IDs for Reserve
        // *BEGIN* JP 11.20.2005   Security ids for parents and reserve screen contexts
        protected const int WC_SID = 3000;
        protected const int DI_SID = 60000;
        protected const int GC_SID = 150;
        protected const int VA_SID = 6000;

        protected const int WC_RSRV_SID = 5400;
        protected const int WC_RSRV_PAYHIST_SID = 5550;
        protected const int WC_TANDE_SID = 5700;	//Mohit for WC Time And Expense

        protected const int DI_RSRV_SID = 62400;
        protected const int DI_RSRV_PAYHIST_SID = 62550;
        protected const int DI_TANDE_SID = 62700;	//Mohit for DI Time And Expense

        protected const int VA_RSRV_SID = 8550;
        protected const int VA_RSRV_PAYHIST_SID = 8700;
        protected const int VA_CLMNTS_SID = 7650;
        protected const int VA_CLMNTS_RSRV_SID = 7800;
        protected const int VA_CLMNTS_RSRV_PAYHIST_SID = 7950;
        protected const int VA_UNIT_SID = 8100;
        protected const int VA_UNIT_RSRV_SID = 8250;
        protected const int VA_UNIT_RSRV_PAYHIST_SID = 8400;
        protected const int VA_TANDE_SID = 8850;	//Mohit for VA Time And Expense

        protected const int GC_RSRV_SID = 2250;
        protected const int GC_RSRV_PAYHIST_SID = 2400;
        protected const int GC_CLMNTS_SID = 1800;
        protected const int GC_CLMNTS_RSRV_SID = 1950;
        protected const int GC_CLMNTS_RSRV_PAYHIST_SID = 2100;
        protected const int GC_TANDE_SID = 2550;	//Mohit for GC Time And Expense
        //Start:Add by Kuladeep 04/12/2010
        public const int PC_SID = 40000;
        public const int PC_RSRV_SID = 43000;
        public const int PC_CLMNTS_SID = 41800;
        public const int PC_CLMNTS_RSRV_SID = 41950;
        //End:Add by Kuladeep 04/12/2010
        
        #endregion

        #region Constants Declaration for Print Functionality

        /// <summary>
        /// Text Control Type
        /// </summary>
        private const int TEXT_CTRL = 1;

        /// <summary>
        /// Message Control Type
        /// </summary>
        private const int MESSAGE_CTRL = 2;

        /// <summary>
        /// Text Area Control Type
        /// </summary>
        private const int TEXTAREA_CTRL = 3;

        /// <summary>
        /// Combo/Check Box Control Type
        /// </summary>
        private const int COMBO_CTRL = 4;

        /// <summary>
        /// Single Row Control Type
        /// </summary>
        private const int SINGLE_ROW = 5;

        /// <summary>
        /// Sub table/Grid Control Type
        /// </summary>
        private const int SUBTABLE = 6;

        /// <summary>
        /// Line Break Control Type
        /// </summary>
        private const int LINE_BREAK = 7;
        #endregion

        #region Constructor

        public ReserveWorksheetBase(int p_iClientId)
        {
            m_iClientId = p_iClientId;
        }
        
        public ReserveWorksheetBase(ReserveWorksheetCommon p_CommonInfo, int p_iClientId)
        {
            m_CommonInfo = p_CommonInfo;
            m_iClientId = p_iClientId;
        } 
        #endregion

        public virtual void GetReserveWorksheet(ReserveWorksheetRequest worksheetRequest, ref ReserveWorksheetResponse worksheetResponse, UserLogin p_objLogin)
        {

        }

        public virtual void SubmitWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {

        }

        public virtual void SaveData(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {

        }
        //aaggarwal29 MITS 27763 -- start
        public virtual void SaveDataWithUserLogin(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, UserLogin pUserLogin)
        {

        }
        //aaggarwal29 MITS 27763 -- end

        public virtual void OnLoadInformation(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {

        }

        public virtual void ApproveReserveWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            //MGaba2:r6:Functioning of Approve Button
        }


        public virtual void PrintWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
            //MGaba2:r6:Functioning of Print Button
        }

        public virtual void RejectWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse,int p_iGroupId)
        {
            //MGaba2:r6:Functioning of Reject Button
        }


        /// <summary>
        /// This function deletes a Reserve Worksheet
        /// </summary>
        /// <param name="p_iRSWID">RSW ID of the worksheet which needs to be deleted.</param>
        public virtual void DeleteWorksheet(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {

        }

        //gagnihotri Reserve Approval
        public virtual void ListFundsTransactions(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse)
        {
            //List of Funds Transactions
        }

        //gagnihotri Reserve Approval
        public virtual void ApproveReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            //ApproveReserveWorksheetFromApprovalScreen
        }

        //gagnihotri Reserve Approval
        public virtual void RejectReserveWorksheetFromApprovalScreen(ReserveWorksheetRequest p_WorksheetRequest, ref ReserveWorksheetResponse p_WorksheetResponse, int p_iGroupId, UserLogin p_objLogin)
        {
            //RejectReserveWorksheetFromApprovalScreen
        }

        /// <summary>
        /// This function will get the reserve categories for the LOB and Claim Type (if required).
        /// </summary>
        /// <param name="p_iLOBCode">LOB Code</param>
        /// <param name="p_iClmTypeCode">Claim Type Code</param>
        protected ReserveFunds.structReserves[] GetResCategories(int p_iLOBCode, int p_iClmTypeCode)
        {
            StringBuilder sbSQL;
            LobSettings objLobSetting = null;
            bool bResByClaimType = false;
            DataSet objDataSet = null;
            ReserveFunds.structReserves[] arrReserves = null;
            int iCnt;
            DataRow objDataRow = null;
            DbConnection objCon = null;

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objCon.Open();

                sbSQL.Append("SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + p_iLOBCode);
                bResByClaimType = Conversion.ConvertStrToBool(objCon.ExecuteScalar(sbSQL.ToString()).ToString());
                sbSQL.Remove(0, sbSQL.Length);

                // Get reserve buckets based on claim type
                sbSQL.Append("SELECT RESERVE_TYPE_CODE, CODE_DESC");
                if (bResByClaimType)
                {
                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE =" + m_baseUserlogin.objUser.NlsCode);// pgupta93 :RMA-10597
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }
                else // Get all reserve buckets that are available
                {
                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE =" + m_baseUserlogin.objUser.NlsCode);// pgupta93 :RMA-10597
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }

                objDataSet = DbFactory.GetDataSet(m_CommonInfo.Connectionstring, sbSQL.ToString(), m_iClientId);
                sbSQL.Remove(0, sbSQL.Length);
                if (objDataSet.Tables[0] != null)
                {
                    arrReserves = new ReserveFunds.structReserves[objDataSet.Tables[0].Rows.Count];
                    //Populate the values in Reserves Struct
                    for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                    {
                        objDataRow = objDataSet.Tables[0].Rows[iCnt];
                        arrReserves[iCnt].iReserveTypeCode =
                            Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                        arrReserves[iCnt].sReserveDesc = objDataRow["CODE_DESC"].ToString();
                    }
                    //Destroy the Data Row object.
                    objDataRow = null;
                }

            }
            finally
            {
                if (objLobSetting != null)
                    objLobSetting = null;

                if (objDataSet != null)
                    objDataSet.Dispose();

                if (objCon != null)
                {
                    objCon.Close();
                    objCon.Dispose();
                }

            }
            return arrReserves;
        }
        //shobhana for customization of Reserves
        protected ReserveFunds.structReserves[] GetReservesTypeCustom(int p_iLOBCode, int p_iClmTypeCode, string p_sRSWCustomReserveXML, XmlDocument p_XMLReserve)
        {
            StringBuilder sbSQL;
            LobSettings objLobSetting = null;
            bool bResByClaimType = false;
            bool bResSysSetting = false;
            DbReader objReader = null;
            XmlDocument objXMLReserve = null;
            XmlNode objXMLNode = null;
            bool bReserveXMLSet = false;
            string sTableValue=null;
            DataSet objDataSet = null;
            ReserveFunds.structReserves[] arrReserves = null;
            int iCnt;
            DataRow objDataRow = null;
            DbConnection objCon = null;

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objCon.Open();

                objXMLReserve = new XmlDocument();

                if (p_XMLReserve != null)
                {
                  
                    
                    XmlNodeList nodes = p_XMLReserve.SelectNodes("//group");
                    if (nodes.Count > 0)
                    {
                        arrReserves = new ReserveFunds.structReserves[nodes.Count-1];
                        for (int i = 0; i <= nodes.Count - 2; i++)

                        {
                            if (nodes[i].Attributes["name"].Value != "RESERVESUMMARY")
                            {
                                string[] arrReserveXML = null;
                                arrReserveXML =nodes[i].Attributes["name"].Value.ToString().Split('_');


                                arrReserves[i].iReserveTypeCode = Conversion.ConvertStrToInteger(arrReserveXML[1].ToString());

                                arrReserves[i].sReserveDesc = nodes[i].Attributes["title"].Value;

                                arrReserves[i].sTableValue = "DX";//MainDatabase XML in each case except load

                            }
                        }

                    }
                }
                else
                {
                    sbSQL.Append("SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + p_iLOBCode);
                    bResByClaimType = Conversion.ConvertStrToBool(objCon.ExecuteScalar(sbSQL.ToString()).ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (!bResByClaimType)
                        p_iClmTypeCode = 0;
              //   sbSQL.Append("SELECT TOP 1 *   FROM RSW_RES_CUSTOMIZATION WHERE LOB_CODE = " + p_iLOBCode + " AND CLAIM_TYPE_CODE=" + p_iClmTypeCode);
                 sbSQL.Append(" SELECT  * FROM RSW_RES_CUSTOMIZATION ");
                 sbSQL.Append(" WHERE RSW_CUTOM_ID =(Select MAX(RSW_CUTOM_ID) from RSW_RES_CUSTOMIZATION WHERE LOB_CODE=" + p_iLOBCode + " AND CLAIM_TYPE_CODE=" + p_iClmTypeCode + " )");
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objReader.Read())
                    {

                        bResSysSetting = Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(objReader.GetValue("SYS_CLAIM_SET")));

                    }
                    else
                    {
                        //Deb ML Changes
                        sbSQL.Append("SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                        StringBuilder sbSqlSelect =new StringBuilder("SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                        if (bResByClaimType)
                        {
                            sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                            sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                            sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                            sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                            //sbSQL.Append(" ORDER BY CODE_DESC ");

                            sbSqlSelect.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                            sbSqlSelect.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                            sbSqlSelect.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                            sbSqlSelect.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);

                        }
                        else // Get all reserve buckets that are available
                        {
                            sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                            sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                            sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                            //sbSQL.Append(" ORDER BY CODE_DESC ");
                            sbSqlSelect.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                            sbSqlSelect.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                            sbSqlSelect.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                        }
                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE =" + m_baseUserlogin.objUser.NlsCode);
                        sbSQL.Append(" UNION ");
                        sbSqlSelect.Append(" AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE ");
                        if (bResByClaimType)
                        {
                            sbSqlSelect.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                            sbSqlSelect.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                            sbSqlSelect.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                            sbSqlSelect.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                        }
                        else
                        {
                            sbSqlSelect.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                            sbSqlSelect.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                            sbSqlSelect.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                        }
                        sbSqlSelect.Append(" AND CODES_TEXT.LANGUAGE_CODE =" + m_baseUserlogin.objUser.NlsCode);
                        sbSqlSelect.Append(")");
                        sbSQL.Append(sbSqlSelect.ToString());
                        sbSQL.Append(" ORDER BY CODE_DESC ");
                        //Deb ML Changes
                        objDataSet = DbFactory.GetDataSet(m_CommonInfo.Connectionstring, sbSQL.ToString(), m_iClientId);
                        sbSQL.Remove(0, sbSQL.Length);
                        if (objDataSet.Tables[0] != null)
                        {
                            arrReserves = new ReserveFunds.structReserves[objDataSet.Tables[0].Rows.Count];
                            //Populate the values in Reserves Struct
                            for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                            {
                                objDataRow = objDataSet.Tables[0].Rows[iCnt];
                                arrReserves[iCnt].iReserveTypeCode =
                                    Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                                arrReserves[iCnt].sReserveDesc = objDataRow["CODE_DESC"].ToString();
                                arrReserves[iCnt].sTableValue = "S";
                                arrReserves[iCnt].iReserveTypeId = Conversion.ConvertStrToInteger(objDataRow["RSW_CUTOM_ID"].ToString());
                            }
                            //Destroy the Data Row object.
                            objDataRow = null;
                        }


                    }

                        //  if (bResByClaimType)
                        //    {

                        if (p_sRSWCustomReserveXML != "" && p_sRSWCustomReserveXML != null)
                        {
                            objXMLReserve.LoadXml(p_sRSWCustomReserveXML);
                            objXMLNode = objXMLReserve.SelectSingleNode("//SysSetCustom");
                            XmlNodeList nodes = objXMLReserve.SelectNodes("//ReserveCustomized");
                            if (objXMLNode != null)
                            {

                                bReserveXMLSet = Conversion.ConvertStrToBool(objXMLNode.InnerText);

                            }

                            if (bReserveXMLSet)
                            {

                                if (nodes.Count > 0)
                                {
                                    arrReserves = new ReserveFunds.structReserves[nodes.Count];
                                    for (int i = 0; i <= nodes.Count - 1; i++)
                                    {
                                        arrReserves[i].iReserveTypeCode = Conversion.ConvertStrToInteger(nodes[i].Attributes["value"].Value.ToString());

                                        arrReserves[i].sReserveDesc = nodes[i].Attributes["Name"].Value;

                                        arrReserves[i].sTableValue = "X";

                                    }
                                }
                            }
                            else
                            {

                                if (bResByClaimType)
                                {  // Get reserve buckets based on claim type
                                    if (bResSysSetting)
                                    {
                                        sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                        sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                        sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(" UNION ");
                                        sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                        sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                        sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                        sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE ");
                                        sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                        sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                        sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(")");
                                        sbSQL.Append(" ORDER BY CODE_DESC ");


                                        sTableValue = "C";

                                    }
                                    else
                                    {

                                        sbSQL.Append("SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                        sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(" UNION ");
                                        sbSQL.Append(" SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                        sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                        sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE ");
                                        sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                        sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(")");
                                        sbSQL.Append(" ORDER BY CODE_DESC ");

                                        sTableValue = "S";  //Main Setting Table
                                      
                                    }

                                }
                                else
                                {

                                   if (bResSysSetting)
                                    {

                                        sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                        sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                        sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = 0 ");
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(" UNION ");
                                        sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                        sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                        sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = 0 ");
                                        sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE ");
                                        sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                        sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                        sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                        sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CLAIM_TYPE_CODE = 0 ");
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(")");
                                        sbSQL.Append(" ORDER BY CODE_DESC ");


                                        sTableValue = "C";
                                        //}
                                    }
                                    else
                                    {

                                        sbSQL.Append("SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                        sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(" UNION ");
                                        sbSQL.Append(" SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                        sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                        sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                        sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE ");
                                        sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                                        sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                        sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                        sbSQL.Append(")");
                                        sbSQL.Append(" ORDER BY CODE_DESC ");

                                        sTableValue = "S";
                                        
                                    }


                                }

                                objDataSet = DbFactory.GetDataSet(m_CommonInfo.Connectionstring, sbSQL.ToString(), m_iClientId);
                                sbSQL.Remove(0, sbSQL.Length);
                                if (objDataSet.Tables[0] != null)
                                {
                                    arrReserves = new ReserveFunds.structReserves[objDataSet.Tables[0].Rows.Count];
                                    //Populate the values in Reserves Struct
                                    for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                                    {
                                        objDataRow = objDataSet.Tables[0].Rows[iCnt];
                                        arrReserves[iCnt].iReserveTypeCode =
                                            Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                                        arrReserves[iCnt].sReserveDesc = objDataRow["CODE_DESC"].ToString();
                                        arrReserves[iCnt].sTableValue = sTableValue;
                                        arrReserves[iCnt].iReserveTypeId = Conversion.ConvertStrToInteger(objDataRow["RSW_CUTOM_ID"].ToString());
                                    }
                                    //Destroy the Data Row object.
                                    objDataRow = null;
                                }

                            }

                        }
                        else
                        {

                           
                            if (bResByClaimType)
                            {

                                 // Get reserve buckets based on claim type
                               if (bResSysSetting)
                                {
                                    sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                    sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                    sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + " AND CLAIM_TYPE_CODE = " + p_iClmTypeCode +")");
                                    sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(" UNION ");
                                    sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                    sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                    sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + " AND CLAIM_TYPE_CODE = " + p_iClmTypeCode + ")");
                                    sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                    sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE ");
                                    sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                    sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + " AND CLAIM_TYPE_CODE = " + p_iClmTypeCode + ")");
                                    sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(")");
                                    sbSQL.Append(" ORDER BY CODE_DESC ");

                                    sTableValue = "C";//customized table
                                    
                                }
                                else
                                {

                                    sbSQL.Append("SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                    //if (bResByClaimType)
                                    //{
                                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(" UNION ");
                                    sbSQL.Append(" SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                    sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE ");
                                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClmTypeCode);
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(")");
                                    sbSQL.Append(" ORDER BY CODE_DESC ");

                                    sTableValue = "S";
                                 
                                }

                            }
                            else
                            {



                                // Get reserve buckets based on claim type
                                if (bResSysSetting)
                                {

                                    //else
                                    //{
                                    sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                    sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                    sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode +")");
                                    sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = 0 ");
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(" UNION ");
                                    sbSQL.Append("SELECT RESERVE_TYPE_CODE,RSW_CUTOM_ID, CODE_DESC");
                                    sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                    sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                    sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = 0 ");
                                    sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE  ");
                                    sbSQL.Append(" FROM RSW_RES_CUSTOMIZATION INNER JOIN CODES_TEXT ON  ");
                                    sbSQL.Append(" RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE=CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID  ");
                                    sbSQL.Append("  AND  RESERVE_TYPE_CODE in (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE= " + p_iLOBCode + ")");
                                    sbSQL.Append(" AND LOB_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CLAIM_TYPE_CODE = 0 ");
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(")");
                                    sbSQL.Append(" ORDER BY CODE_DESC ");


                                    sTableValue = "C";
                                    //}
                                }
                                else
                                {
                                    sbSQL.Append("SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(" UNION ");
                                    sbSQL.Append("SELECT RESERVE_TYPE_CODE,0 AS RSW_CUTOM_ID, CODE_DESC");
                                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                    sbSQL.Append("  AND CODES_TEXT.CODE_ID NOT IN (SELECT RESERVE_TYPE_CODE  ");
                                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                                    sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOBCode);
                                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=" + m_baseUserlogin.objUser.NlsCode);
                                    sbSQL.Append(")");
                                    sbSQL.Append(" ORDER BY CODE_DESC ");

                                    sTableValue = "S";
                                 
                                }


                            }

                            objDataSet = DbFactory.GetDataSet(m_CommonInfo.Connectionstring, sbSQL.ToString(),m_iClientId);
                            sbSQL.Remove(0, sbSQL.Length);
                            if (objDataSet.Tables[0] != null)
                            {
                                arrReserves = new ReserveFunds.structReserves[objDataSet.Tables[0].Rows.Count];
                                //Populate the values in Reserves Struct
                                for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                                {
                                    objDataRow = objDataSet.Tables[0].Rows[iCnt];
                                    arrReserves[iCnt].iReserveTypeCode =
                                        Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                                    arrReserves[iCnt].sReserveDesc = objDataRow["CODE_DESC"].ToString();

                                    arrReserves[iCnt].sTableValue = sTableValue;
                                    arrReserves[iCnt].iReserveTypeId = Conversion.ConvertStrToInteger(objDataRow["RSW_CUTOM_ID"].ToString());
                                }
                                //Destroy the Data Row object.
                                objDataRow = null;
                            }

                        }

                   
                }
            }
            finally
            {
                if (objLobSetting != null)
                    objLobSetting = null;

                if (objDataSet != null)
                    objDataSet.Dispose();

                if (objCon != null)
                {
                    objCon.Close();
                    objCon.Dispose();
                }

            }
            return arrReserves;
        }
    
        protected bool CheckPendingRSW(long p_lClaimId,long p_lClaimantRowId,long p_lUnitRowId,int p_iStatusCode)
        {
            StringBuilder sbSQL = new StringBuilder();
            DbReader objReader = null;
            bool bReturnVal = false;

            try
            {
                sbSQL.Append("SELECT RSW_ROW_ID FROM RSW_WORKSHEETS WHERE RSW_STATUS_CODE = " + p_iStatusCode + " AND CLAIM_ID = " + p_lClaimId);
                sbSQL.Append(" AND CLAIMANT_ROW_ID = " + p_lClaimantRowId);
                sbSQL.Append(" AND UNIT_ROW_ID = " + p_lUnitRowId);
                
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReader.Read())
                {
                    bReturnVal = true;
                }
                objReader.Close();
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return bReturnVal;
        }
        //skhare7 commented shifted to Reserve current datamodel for R8 
        ///// <summary>
        ///// get the emails address and user name of a user
        ///// </summary>
        ///// <param name="p_sLogInName">log in name of the user</param>
        ///// <param name="p_sSecConStr">security database connection string</param>
        ///// <param name="p_iDSNId">DSN id</param>
        ///// <param name="p_sEmail">email id to be returned</param>
        ///// <param name="p_sUserName">user name to be returned</param>
        //protected void GetUserDetails(string p_sLogInName, string p_sSecConStr, int p_iDSNId, ref string p_sEmail,
        //    ref string p_sUserName)
        //{
        //    string sSQL = string.Empty;
        //    DataSet objDS = null;

        //    try
        //    {
        //        sSQL = "SELECT LAST_NAME, FIRST_NAME, EMAIL_ADDR FROM USER_TABLE, USER_DETAILS_TABLE WHERE "
        //            + " USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME = '"
        //            + p_sLogInName
        //            + "' AND DSNID = " + p_iDSNId;
        //        objDS = DbFactory.GetDataSet(p_sSecConStr, sSQL);
        //        //Vaibhav for Safeway on 03/30/2009: Added condition for checking the dataset contains any row or not
        //        if (objDS.Tables[0].Rows.Count > 0)
        //        {
        //            p_sEmail = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["EMAIL_ADDR"]);
        //            p_sUserName = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["FIRST_NAME"]) + " " +
        //                Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["LAST_NAME"]);
        //        }
        //    }
        //    finally
        //    {
        //        if (objDS != null)
        //            objDS.Dispose();
        //    }
        //}
        //skhare7 commented shifted to common fns for R8  End

        /// <summary>
        /// Get the Supervisory Level
        /// </summary>
        /// <param name="p_iSubmittedByID">Submitted by User Id</param>
        /// <param name="p_iCurrentUserID">Current User Id</param>
        /// <returns>Supervisory Level</returns>
        protected int GetSupervisoryLevel(int p_iSubmittedByID, int p_iCurrentUserID, int p_iClaimID)
        {
            int iManagerID = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;
            bool bInMangChain = false;
            int iReturnVal = 0;

            try
            {
                iManagerID = p_iSubmittedByID;
                if (p_iSubmittedByID != 0 && p_iCurrentUserID != 0)
                {
                    if (p_iCurrentUserID != p_iSubmittedByID) //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                    {
                        while (!bInMangChain && iManagerID != 0) //stop looping if the user is found in the managerial chain
                        {
                            sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                                    + iManagerID
                                    + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                                    + m_CommonInfo.DSNID;
                            objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                            if (objReader.Read())
                            {
                                iManagerID = objReader.GetInt32("MANAGER_ID");
                                iReturnVal += 1;
                                if (iManagerID == p_iCurrentUserID)
                                {
                                    bInMangChain = true;
                                }
                                if (iManagerID == 0)
                                {
                                    int ilevel = 0;
                                    ilevel = GetSupervisorType(p_iCurrentUserID, p_iClaimID);
                                    if (ilevel > 0)
                                        iReturnVal += ilevel;
                                    else
                                        iReturnVal = ilevel;
                                }
                            }
                            else
                            {
                                iReturnVal = 0;
                                iManagerID = 0;
                            }
                            objReader.Close();
                        }
                    }
                    else
                        iReturnVal = 1;
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return iReturnVal;
        }

        /// <summary>
        /// To Get the type of supervisor
        /// </summary>
        /// <param name="p_iManagerID">Manager ID to check</param>
        /// <returns>Type of supervisor: 2 - Top Level Manager, 1 - LOB Manager else 0</returns>
        protected int GetSupervisorType(int p_iManagerID, int p_iClaimID)
        {
            string sSQL = string.Empty;
            int iLOBManagerID = 0;
            int iTopManagerId = 0;
            int iLOB = 0;
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objConn.Open();

                sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";
                iTopManagerId = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);

                if (p_iManagerID == iTopManagerId)
                    return 2;

                sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID.ToString();
                iLOB = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);


                sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOB.ToString();
                iLOBManagerID = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);

                if (p_iManagerID == iLOBManagerID)
                    return 1;
            }
            finally
            {
                objConn = null;
            }
            return 0;
        }

        protected string GetSupervisorName()
        {
            string sSQL = string.Empty;
            string sSupName = string.Empty;
            DbReader objReader = null;
            long lUserMId;


            //Getting Supervisor Name 
            sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                            + m_CommonInfo.UserId
                            + " AND USER_TABLE.MANAGER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                            + m_CommonInfo.DSNID;

            objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
            if (objReader.Read())
                lUserMId = objReader.GetInt32("MANAGER_ID");
            else
                lUserMId = 0;

            if (lUserMId != 0)
            {
                sSQL = "SELECT MANAGER_ID, LOGIN_NAME, USER_TABLE.First_Name as FName, USER_TABLE.Last_Name as LName FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                        + lUserMId
                        + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                        + m_CommonInfo.DSNID;
                objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                if (objReader.Read())
                {
                    sSupName = objReader.GetString("FName") + ", " + objReader.GetString("LName");
                }
                else
                {
                    sSupName = "";
                }
            }

            return sSupName;
        }


        /// <summary>
        /// This function will get the utility setting related to reserves, for the Line Of Business of claim:
        /// Collection in Incurred Balance, Collection in Reserve Balance, Reserve Limit Flag, Prevent Reserves Below Paid
        /// </summary>
        /// <param name="p_lClaimId">Claim ID</param>
        protected void GetLOBReserveOptions(long p_lClaimId)
        {
            StringBuilder sbSQL = new StringBuilder();
            DbReader objReader = null;
            ColLobSettings objLOBSettings = null;
            SysSettings objSysSetting = null;
            int iLOBCode = 0;

            try
            {
                objLOBSettings = new ColLobSettings(m_CommonInfo.Connectionstring,m_iClientId);
                objSysSetting = new SysSettings(m_CommonInfo.Connectionstring,m_iClientId);

                // Get the Line of Business for the claim
                sbSQL.Append("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_lClaimId);
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReader.Read())
                    iLOBCode = objReader.GetInt32("LINE_OF_BUS_CODE");
                objReader.Close();

                m_bCollInIncBal = objLOBSettings[iLOBCode].CollInIncurred;  //Flag for Collection in Incurred Balance
                m_bCollInResBal = objLOBSettings[iLOBCode].CollInRsvBal;    //Flag for Collection in Reserve Balance
                m_bResLimitFlag = objLOBSettings[iLOBCode].ResLmtFlag;
                m_bClaimIncLimitFlag = objLOBSettings[iLOBCode].ClaimIncLmtFlag; //sachin 6385
              //   objLOBSettings[iLOBCode].re//Flag for Reserve Limit Flag
                //m_bPrevResBelowPaid = objSysSetting.PrevResBelowPaid;       //Flag for Preventing Reserves Less Then Paid

            }
            finally
            {
                if (objLOBSettings != null)
                    objLOBSettings = null;
                if (objSysSetting != null)
                    objSysSetting = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }

        /// <summary>
        /// Read Check Options
        /// </summary>
        /// <summary>
        /// Read Check Options
        /// </summary>
        protected void ReadRSWCheckOptions()
        {
            DbReader objReader = null;
            string sSQL = "";
            bool bSetToDefault = true;

            try
            {
                sSQL = "SELECT * FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        //Getting values from Check Options for USE_SUP_APP_RESERVES, DAYS_APP_RESERVES, NOTIFY_SUP_RESERVES, 
                        //USE_CUR_ADJ_RESERVES
                        m_bUseSupAppReserves = objReader.GetBoolean("USE_SUP_APP_RESERVES");
                        m_iDaysAppReserves = objReader.GetInt("DAYS_APP_RESERVES");
                        m_iHoursAppReserves = objReader.GetInt("HOURS_APP_RESERVES");
                        m_bZeroEscalationReserves = objReader.GetBoolean("ZERO_ESCALATION_RESERVES"); // rrachev JIRA RAM-13311
                        m_bNotifySupReserves = objReader.GetBoolean("NOTIFY_SUP_RESERVES");
                        m_bUseCurrAdjReserves = objReader.GetBoolean("USE_CUR_ADJ_RESERVES");
                        //rsushilaggar: 22-Mar-2010 Mits : 19624
                        m_bDisableDiaryNotify = objReader.GetBoolean("DISABLE_DIARY_NOTIFY_FOR_SUPV");
                        m_bDisableEmailNotify = objReader.GetBoolean("DISABLE_EMAIL_NOTIFY_FOR_SUPV");
                        //End - rsushilaggar
                        //rsushilaggar: 22-Mar-2010 Mits : 19624
                        m_bAllowAccessGrpSup = objReader.GetBoolean("ALLOW_SUP_GRP_APPROVE_RSV");
                        //m_bSupNotifyRes = objReader.GetBoolean("USE_SUP_NOTIFY_RSV"); 
                        //End - rsushilaggar
                        bSetToDefault = false;
                    }
                }
                objReader.Close();
                //Setting default values
                if (bSetToDefault)
                {
                    m_bUseSupAppReserves = false;
                    m_iDaysAppReserves = 0;
                    m_bNotifySupReserves = false;
                    m_bUseCurrAdjReserves = false;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.ReadCheckOptions.ErrorCheckOpt",m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }


        /// <summary>
        /// This function will get the reserve amounts for the given reserve categories.
        /// </summary>
        /// <param name="p_iLOBCode">LOB Code</param>
        /// <param name="p_arrReserves">ReserveFunds structReserves</param>
        /// <param name="p_objXMLIn">XML Document</param>
        protected string[,] GetNewReserveAmts(int p_iLOBCode, ReserveFunds.structReserves[] p_arrReserves, XmlDocument p_objXMLIn,int p_iClaimCurrCode)
        {
            int iCnt;
            int iReserveTypeCode = 0;
            string sReserveDesc = "";
            string sLOB = "";
            string[,] sResFinal = null;
            XmlNode objNod = null;
            XmlNode objNode = null;
            LocalCache objCache = null;
            //shobhana
            string sCtrlMName = "";
            string sGPName = "";
           
         
         
          

            try
            {
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                //nsachdeva2 MITS:25502 - 09/26/2011
                //sResFinal = new string[p_arrReserves.GetLength(0), 4];
                sResFinal = new string[p_arrReserves.GetLength(0), 5];
                //End MITS:25502
                sLOB = objCache.GetShortCode(p_iLOBCode);
                objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnClaimId']");
                //skhare7
               
               
                for (iCnt = 0; iCnt < p_arrReserves.GetLength(0); iCnt++)
                {//shobhana
                   
                    iReserveTypeCode = p_arrReserves[iCnt].iReserveTypeCode;

                    sReserveDesc = p_arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "");
               
                    sGPName = "//group[@name='" + sReserveDesc + sLOB + "']";
                    if (sReserveDesc.Length < 3)  //Aman Multi Currency --Start
                    {
                        continue;
                    }//Aman Multi Currency --End
                    sCtrlMName = sReserveDesc.Substring(0, 3) + sLOB;
                    sResFinal[iCnt, 0] = iReserveTypeCode.ToString();



                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='BalAmt" + sCtrlMName + "']");
                    if (objNode != null)
                        sResFinal[iCnt, 1] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNode.InnerText).ToString();
                    objNod = p_objXMLIn.SelectSingleNode("//control[@name='txt_ReserveType_" + iReserveTypeCode.ToString() + "_Outstanding']");
                    if (objNod != null && objNod.Attributes["CurrencyValue"] != null)
                    {
                     //   sResFinal[iCnt, 1] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();
                        sResFinal[iCnt, 1] = objNod.Attributes["CurrencyValue"].Value;  //Aman Multi Currency 
                    }

                    objNode = p_objXMLIn.SelectSingleNode("//control[@name='NewBalAmt" + sCtrlMName + "']");
                    if (objNode != null)
                        sResFinal[iCnt, 2] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNode.InnerText).ToString();
                    objNod = p_objXMLIn.SelectSingleNode("//control[@name='hdn_ReserveType_" + iReserveTypeCode.ToString() + "_NEWOutstanding']");
                    if (objNod != null)
                        sResFinal[iCnt, 2] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();
                   objNode = p_objXMLIn.SelectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']");
                   if (objNode != null)
                       sResFinal[iCnt, 3] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNode.InnerText).ToString();

                    objNod = p_objXMLIn.SelectSingleNode("//control[@name='hdn_ReserveType_" + iReserveTypeCode.ToString() + "_NEWTOTALINCURRED']");
                    if (objNod != null)
                        sResFinal[iCnt, 3] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();

                    //nsachdeva2 MITS:25502 - 09/26/2011
                    objNod = p_objXMLIn.SelectSingleNode("//control[@name='hdn_ReserveType_" + iReserveTypeCode.ToString() + "_ReserveChange']");
                    if (objNod != null)
                        sResFinal[iCnt, 4] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();
                    //End MITS:25502
                }

            }
            finally
            {
                if (objCache != null)
                    objCache = null;
            }
            return sResFinal;
        }
        ////internal string[,] GetNewReserveAmt(int p_iLOBCode, ReserveFunds.structReserves[] p_arrReserves, XmlDocument p_objXMLIn)
        ////{
        ////    int iCnt;
        ////    int iReserveTypeCode = 0;
        ////    string sReserveDesc = "";
        ////    string sGPName = "";
        ////    string sLOB = "";
        ////    string sCtrlMName = "";
        ////    string[,] sResFinal = null;
        ////    XmlNode objNod = null;
        ////    LocalCache objCache = null;

        ////    try
        ////    {
        ////        objCache = new LocalCache(m_CommonInfo.Connectionstring);
        ////        sResFinal = new string[p_arrReserves.GetLength(0), 4];
        ////        sLOB = objCache.GetShortCode(p_iLOBCode);

        ////        for (iCnt = 0; iCnt < p_arrReserves.GetLength(0); iCnt++)
        ////        {
        ////            iReserveTypeCode = p_arrReserves[iCnt].iReserveTypeCode;
        ////            sReserveDesc = p_arrReserves[iCnt].sReserveDesc.Trim().Replace(" ", "");
        ////            sGPName = "//group[@name='" + sReserveDesc + sLOB + "']";

        ////            if (p_objXMLIn.SelectSingleNode(sGPName) != null)
        ////            {
        ////                sCtrlMName = sReserveDesc.Substring(0, 3) + sLOB;

        ////                sResFinal[iCnt, 0] = iReserveTypeCode.ToString();
        ////                objNod = p_objXMLIn.SelectSingleNode("//control[@name='txt_ReserveType_" + iReserveTypeCode.ToString() + "_Outstanding']");
        ////                objNod = p_objXMLIn.SelectSingleNode("//control[@name='BalAmt" + sCtrlMName + "']");
        ////                if (objNod != null)
        ////                    sResFinal[iCnt, 1] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();

        ////                objNod = p_objXMLIn.SelectSingleNode("//control[@name='NewBalAmt" + sCtrlMName + "']");
        ////                if (objNod != null)
        ////                    sResFinal[iCnt, 2] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();

        ////                objNod = p_objXMLIn.SelectSingleNode("//control[@name='hdnNewRes" + sCtrlMName + "']");
        ////                if (objNod != null)
        ////                    sResFinal[iCnt, 3] = Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText).ToString();
        ////            }
        ////        }

        ////    }
        ////    finally
        ////    {
        ////        if (objCache != null)
        ////            objCache = null;
        ////    }
        ////    return sResFinal;
        ////}


        /// <summary>
        /// Function to check whether the entered reserves are within the authority limits of a user
        /// </summary>
        /// <param name="p_objXmlDoc">xml document having the reserves</param>
        /// <param name="p_iUserId">user id to be checked</param>
        /// <param name="p_iGroupId">group id of the user to be checked</param>
        /// <param name="p_iClaimId">claim id</param>
        /// <param name="p_sResFinal">Final Reserve Amounts</param>
        /// <param name="p_arrReserves">Final Reserve Amounts</param>
        /// <returns>true if the reseves are within limits, false otherwise</returns>
        protected bool UnderUserLimit(int p_iUserId, int p_iGroupId, int p_iClaimId, string[,] p_sResFinal, ReserveFunds.structReserves[] p_arrReserves,int p_iClaimCurrCode)
        {
            StringBuilder sbSQL = new StringBuilder();
            int iCnt = 0;

            bool bUnderUserLimit = false;

            try
            {
                for (iCnt = 0; iCnt < p_arrReserves.GetLength(0); iCnt++)
                {
                    # region Get Reserve Limits and Validate If Reserves fall within that limit

                    for (int iCount = 0; iCount < p_sResFinal.GetLength(0); iCount++)
                    {
                        # region Query to find reserve limit for all Reserves
                        if (Riskmaster.Common.Conversion.ConvertStrToInteger(p_sResFinal[iCount, 0]) == p_arrReserves[iCnt].iReserveTypeCode)
                        {
                            //nsachdeva2 MITS:25502 - 09/26/2011
                            //if (Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 3]) > 0.0)
                            //{
                            //    bUnderUserLimit = CheckReserveLimits(p_iClaimId, p_arrReserves[iCnt].iReserveTypeCode,
                            //        Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 3]), p_iUserId, p_iGroupId);
                            //    if (bUnderUserLimit)
                            //        return false;
                            //}
                            if (Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 4]) != 0.0)
                            {
                                if (Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 3]) > 0.0)
                                {
                                    bUnderUserLimit = CheckReserveLimits(p_iClaimId, p_arrReserves[iCnt].iReserveTypeCode,
                                        Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 3]), p_iUserId, p_iGroupId,p_iClaimCurrCode);
                                    if (bUnderUserLimit)
                                        return false;
                                }
                            }
                            //End MITS:25502
                        }
                        # endregion
                    }

                    # endregion
                }
                return true;
            }
            finally
            {
            }
        }

        //sachin 6385 starts
        protected bool UnderClaimIncUserLimit(int p_iUserId, int p_iGroupId, int p_iClaimId, string[,] p_sResFinal, ReserveFunds.structReserves[] p_arrReserves, int p_iClaimCurrCode)
        {
            StringBuilder sbSQL = new StringBuilder();
            int iCnt = 0;
            double claimIncTotal = 0.0;
            bool bUnderClaimIncUserLimit = false;
           
            try
            {
                for (iCnt = 0; iCnt < p_arrReserves.GetLength(0); iCnt++)
                {
                    # region Get Claim Incurred Limits and Validate If Incurred amount fall within that limit

                    for (int iCount = 0; iCount < p_sResFinal.GetLength(0); iCount++)
                    {
                        # region Query to find reserve limit for all Reserves
                        if (Riskmaster.Common.Conversion.ConvertStrToInteger(p_sResFinal[iCount, 0]) == p_arrReserves[iCnt].iReserveTypeCode)
                        {                        
                            if (Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 4]) != 0.0)
                            {
                                if (Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 3]) > 0.0) 
                                {   
                                   claimIncTotal = claimIncTotal + Riskmaster.Common.Conversion.ConvertStrToDouble(p_sResFinal[iCount, 3]);                             
                                }
                            }                        
                        }
                        # endregion
                    }
                    # endregion
                }
                bUnderClaimIncUserLimit = CheckClaimIncLimits(p_iClaimId, claimIncTotal, p_iUserId, p_iGroupId, p_iClaimCurrCode);
                if (bUnderClaimIncUserLimit)
                    return false;
                else
                    return true;
            }
            finally
            {
            }
        }
        //sachin 6385 ends

        /// Name		: CheckReserveLimits
        /// Author		: Mohit Yadav
        /// Date Created: 07/07/2008
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is used for checking the reserve limits from database against a particular claim
        /// and reserve type.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <param name="p_dAmount">amount</param>
        /// <param name="p_iUserId">User Id</param>
        /// <param name="p_iGroupId">Group Id</param>
        protected bool CheckReserveLimits(int p_iClaimId, int p_iReserveTypeCode, double p_dAmount,
            int p_iUserId, int p_iGroupId,int p_iClaimCurrCode)
        {
            double dMaxAmount = 0.0;
            StringBuilder sbSQL = null;

            //Database objects
            DbReader objReader = null;
            DbConnection objConn = null;

            bool bReturnValue = true;


            //***
            DataModelFactory objDataModelFactory = null;
            Claim objClaim = null;
            int iLOBCode = 0;
            int iLOBMgr = 0;
            //skhare7 R8
            //***
            SysParms objSysSetting = null;
            try
            {
                sbSQL = new StringBuilder();

                objSysSetting = new SysParms(m_CommonInfo.Connectionstring, m_iClientId);
                double dExhClaimRateToBase = CommonFunctions.ExchangeRateSrc2Dest(p_iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, m_CommonInfo.Connectionstring, m_iClientId);
                p_dAmount = dExhClaimRateToBase * p_dAmount;
                sbSQL.Append("SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM RESERVE_LIMITS,CLAIM ");
                sbSQL.Append(" WHERE (USER_ID = " + p_iUserId);
                if (p_iGroupId != 0)
                    sbSQL.Append(" OR GROUP_ID = " + p_iGroupId);

                sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_iClaimId);
                sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = RESERVE_LIMITS.LINE_OF_BUS_CODE");
                //nsachdeva2 - MITS 25502 - 10/19/2011
                //sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode+ " OR RESERVE_TYPE_CODE=0");
                sbSQL.Append(" AND ( RESERVE_TYPE_CODE = " + p_iReserveTypeCode + " OR RESERVE_TYPE_CODE=0)");
                //End MITS 25502
                //skhare7 R8 SuperVisory approval Added Reserve type code 0 condition
                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objConn.Open();

                objReader = objConn.ExecuteReader(sbSQL.ToString());

                if (objReader.Read())
                {
                    if (objReader.GetValue("MAX_AMT") is DBNull)
                        bReturnValue = false;

                    if (bReturnValue)
                    {
                        dMaxAmount = Conversion.ConvertStrToDouble(objReader.GetValue("MAX_AMT").ToString());
                        //skhare7 R8 Multicurrency
                        
                        if (p_dAmount > dMaxAmount)
                            bReturnValue = true;
                        else
                            bReturnValue = false;
                    }
                    objReader.Close();
                }
                else
                    bReturnValue = false;
                

                //***  
                //MGaba2:MITS 19099:In case LOB Manager is the current user,we need to check his limit :Start
                sbSQL.Remove(0, sbSQL.Length);
                if (bReturnValue == false)
                {
                    objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);
                    iLOBCode = objClaim.LineOfBusCode;

                    sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE =" + iLOBCode);                                        

                    objReader = objConn.ExecuteReader(sbSQL.ToString());

                    if (objReader.Read())
                    {
                        iLOBMgr = Conversion.ConvertStrToInteger(objReader.GetValue("USER_ID").ToString());
                        objReader.Close();
                        sbSQL.Remove(0, sbSQL.Length);
                        if (p_iUserId == iLOBMgr)
                        {
                            sbSQL.Append("SELECT RESERVE_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE =" + iLOBCode);
                            objReader = objConn.ExecuteReader(sbSQL.ToString());
                            if (objReader.Read())
                            {
                                dMaxAmount = Conversion.ConvertStrToDouble(objReader.GetValue("RESERVE_MAX").ToString());
                            }
                            objReader.Close();


                            if (p_dAmount > dMaxAmount)
                                bReturnValue = true;
                            else
                                bReturnValue = false;
                            sbSQL.Remove(0, sbSQL.Length);
                        }
                    }
                    
                }
                //MGaba2:MITS 19099:End

                //****

                objConn.Close();

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.CheckReserveLimits.DataErr", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objConn != null)
                    objConn.Dispose();
            }
            return bReturnValue;
        }


        //sachin 6385 starts
        protected bool CheckClaimIncLimits(int p_iClaimId, double p_dAmount,
           int p_iUserId, int p_iGroupId, int p_iClaimCurrCode)
        {
            double dMaxAmount = 0.0;
            StringBuilder sbSQL = null;

            //Database objects
            DbReader objReader = null;
            DbConnection objConn = null;

            bool bReturnValue = true;

            //***
            DataModelFactory objDataModelFactory = null;
            Claim objClaim = null;
            int iLOBCode = 0;
            int iLOBMgr = 0;
            
            //***
            SysParms objSysSetting = null;
            try
            {
                sbSQL = new StringBuilder();

                objSysSetting = new SysParms(m_CommonInfo.Connectionstring, m_iClientId);
                double dExhClaimRateToBase = CommonFunctions.ExchangeRateSrc2Dest(p_iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, m_CommonInfo.Connectionstring, m_iClientId);
                p_dAmount = dExhClaimRateToBase * p_dAmount;
                sbSQL.Append("SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM CLAIM_INCURRED_LIMITS,CLAIM ");
                sbSQL.Append(" WHERE (USER_ID = " + p_iUserId);
                if (p_iGroupId != 0)
                    sbSQL.Append(" OR GROUP_ID = " + p_iGroupId);

                sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_iClaimId);
                sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = CLAIM_INCURRED_LIMITS.LINE_OF_BUS_CODE");               
                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objConn.Open();

                objReader = objConn.ExecuteReader(sbSQL.ToString());

                if (objReader.Read())
                {
                    if (objReader.GetValue("MAX_AMT") is DBNull)
                        bReturnValue = false;

                    if (bReturnValue)
                    {
                        dMaxAmount = Conversion.ConvertStrToDouble(objReader.GetValue("MAX_AMT").ToString());                     

                        if (p_dAmount > dMaxAmount)
                            bReturnValue = true;
                        else
                            bReturnValue = false;
                    }
                    objReader.Close();
                }
                else
                    bReturnValue = false;


                //***       
                sbSQL.Remove(0, sbSQL.Length);
                if (bReturnValue == false)
                {
                    objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword, m_iClientId);
                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);
                    iLOBCode = objClaim.LineOfBusCode;

                    sbSQL.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE =" + iLOBCode);

                    objReader = objConn.ExecuteReader(sbSQL.ToString());

                    if (objReader.Read())
                    {
                        iLOBMgr = Conversion.ConvertStrToInteger(objReader.GetValue("USER_ID").ToString());
                        objReader.Close();
                        sbSQL.Remove(0, sbSQL.Length);
                        if (p_iUserId == iLOBMgr)
                        {
                            sbSQL.Append("SELECT CLAIM_INCURRED_MAX FROM LOB_SUP_APPROVAL WHERE LOB_CODE =" + iLOBCode);
                            objReader = objConn.ExecuteReader(sbSQL.ToString());
                            if (objReader.Read())
                            {
                                dMaxAmount = Conversion.ConvertStrToDouble(objReader.GetValue("CLAIM_INCURRED_MAX").ToString());
                            }
                            objReader.Close();


                            if (p_dAmount > dMaxAmount)
                                bReturnValue = true;
                            else
                                bReturnValue = false;
                            sbSQL.Remove(0, sbSQL.Length);
                        }
                    }

                }         
                //****

                objConn.Close();

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.CheckReserveLimits.DataErr", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objConn != null)
                    objConn.Dispose();
            }
            return bReturnValue;
        }
        //sachin 6385 ends 

        /// <summary>
        /////By Vaibhav on 11/14/08 : modified  CheckDaysForApprovalLapsed function
        /// This function will check Days for approval setting and see if days have lapsed
        /// </summary>
        /// <param name="p_sDateTime">DateTime Reserve Worksheet last updated</param>
        /// <param name="p_SubmittedToID">Worksheet Submitted to user</param>
        /// <param name="p_iCurrentUserID">Current user</param>
        /// <returns></returns>
        protected bool CheckDaysForApprovalLapsed(string p_sDateTime, int p_SubmittedToID, int p_iCurrentUserID, int p_iClaimId)
        {
            DateTime CurDateTime;
            DateTime RSWDateTime;
            bool bReturnVal = false;
            int iLevel = 1;

            try
            {
                CurDateTime = DateTime.Now;

                //Get Supervisory Approval Settings
                ReadRSWCheckOptions();

                //Get the level of supervisor
                iLevel = GetSupervisoryLevel(p_SubmittedToID, p_iCurrentUserID, p_iClaimId);

                if (m_iDaysAppReserves > 0)
                {
                    RSWDateTime = ToDateTime(p_sDateTime);
                    RSWDateTime = RSWDateTime.AddDays(iLevel * m_iDaysAppReserves);

                    //Check if days have lapsed
                    if (CurDateTime > RSWDateTime)
                    {
                        bReturnVal = true;
                    }
                }
            }
            finally
            {
            }
            return bReturnVal;
        }

        /// <summary>
        /// To Convert Database Date Time Format to DateTime Format 
        /// </summary>
        /// <param name="DbDateTime">Database Date Time Format</param>
        /// <returns>DateTime Format </returns>
        protected DateTime ToDateTime(string DbDateTime)
        {
            DateTime dDateTime;
            if (DbDateTime.Length == 14)
            {
                dDateTime = Conversion.ToDate(DbDateTime.Substring(0, 8));
                dDateTime = dDateTime.AddHours(Conversion.ConvertStrToDouble(DbDateTime.Substring(8, 2)));
                dDateTime = dDateTime.AddMinutes(Conversion.ConvertStrToDouble(DbDateTime.Substring(10, 2)));
                dDateTime = dDateTime.AddSeconds(Conversion.ConvertStrToDouble(DbDateTime.Substring(12, 2)));
            }
            else
            {
                dDateTime = Conversion.ToDate(DbDateTime);
            }

            return dDateTime;
        }

        /// <summary>
        /// This function makes the worksheet read-only, by making the individual controls read-only.
        /// </summary>
        /// <param name="p_objXML">in XML which needs to be changed</param>
        /// <returns>Modified XML</returns>
        protected XmlDocument MakeRWReadOnly(XmlDocument p_objXML)
        {
            XmlNode objNode = null;
            string[] arrFields = null;
            XmlElement objElm = null;
            string sGrids = string.Empty;

            try
            {
              objNode = p_objXML.SelectSingleNode("//control[@name='hdnCtrlToEdit']");
              //  objNode = p_objXML.SelectSingleNode("//control[@name='CtrlToEdit']");
                arrFields = objNode.InnerText.Split(',');

                for (int count = 0; count < arrFields.Length; count++)
                {
                    objElm = (XmlElement)p_objXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']");
                    if (objElm != null)
                    {
                        switch (objElm.GetAttribute("type"))
                        {
                            case "code":
                                if (objElm.Attributes["required"] != null)
                                    objElm.Attributes["required"].InnerText = "no";
                                else
                                    objElm.SetAttribute("required", "no");
                                break;
                            case "combobox":
                            case "checkbox":
                                if (objElm.Attributes["disabled"] != null)
                                    objElm.Attributes["disabled"].InnerText = "true";
                                else
                                    objElm.SetAttribute("disabled", "true");
                                break;
                            case "textarea":
                            case "text":
                            case "numeric":
                            case "currency":
                            default:
                                if (objElm.Attributes["readonly"] != null)
                                    objElm.Attributes["readonly"].InnerText = "true";
                                else
                                    objElm.SetAttribute("readonly", "true");
                                break;
                        } // end of switch case
                    }
                } // end of for loop
            }
            finally
            {
                objNode = null;
            }
            return p_objXML;
        }


        /// <summary>
        /// This function makes the worksheet read-write (editable) by making the individual controls editable.
        /// </summary>
        /// <param name="p_objXML">XML which needs to be modified</param>
        /// <returns>Modified XML</returns>
        protected XmlDocument MakeRWEditable(XmlDocument p_objXML)
        {
            XmlNode objNode = null;
            string[] arrFields = null;
            XmlElement objElm = null;
            string sGrids = string.Empty;

            try
            {
                objNode = p_objXML.SelectSingleNode("//control[@name='hdnCtrlToEdit']");
                arrFields = objNode.InnerText.Split(',');

                for (int count = 0; count < arrFields.Length; count++)
                {
                    objElm = (XmlElement)p_objXML.SelectSingleNode("//control[@name='" + arrFields[count] + "']");
                    if (objElm != null)
                    {
                        switch (objElm.Attributes["type"].InnerText)
                        {
                            case "combobox":
                            case "checkbox":
                                if (objElm.Attributes["disabled"] != null)
                                    objElm.RemoveAttribute("disabled");
                                break;
                            case "textarea":
                            case "text":
                            case "currency":
                            case "numeric":
                            default:
                                if (objElm.Attributes["readonly"] != null)
                                    objElm.Attributes["readonly"].InnerText = "false";
                                else
                                    objElm.SetAttribute("readonly", "false");
                                break;
                        } // end of switch case
                    }
                } // end of for loop
            }
            finally
            {
                objNode = null;
            }
            return p_objXML;
        }

        ///MGaba2: R8: SuperVisory Approval:Shifting this code to CommonFunctions.cs
        /// <summary>
        /// Get the Adjuster Id
        /// </summary>
        /// <param name="iClaimId">Claim Id</param>
        /// <returns>Adjuster Id</returns>
        //protected int GetAdjusterEID(int iClaimId)
        //{
        //    string sSQL = string.Empty;
        //    int iCurrentAdjusterID = 0;
        //    int iRMUserID = 0;
        //    DbReader objReader = null;
        //    try
        //    {
        //        sSQL = "SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER WHERE CLAIM_ID = " + iClaimId + " AND CURRENT_ADJ_FLAG <> 0";
        //        objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                iCurrentAdjusterID = Conversion.ConvertObjToInt(objReader.GetValue("ADJUSTER_EID"), m_iClientId);
        //            }
        //        }
        //        if (iCurrentAdjusterID > 0)
        //        {
        //            sSQL = "SELECT RM_USER_ID FROM ENTITY WHERE ENTITY_ID = " + iCurrentAdjusterID;
        //            objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
        //            if (objReader != null)
        //            {
        //                while (objReader.Read())
        //                {
        //                    iRMUserID = Conversion.ConvertObjToInt(objReader.GetValue("RM_USER_ID"), m_iClientId);
        //                }
        //            }
        //        }
        //        iCurrentAdjusterID = iRMUserID;
        //    }
        //    catch (RMAppException p_objException)
        //    {
        //        throw p_objException;
        //    }
        //    catch (Exception p_objException)
        //    {
        //        throw new RMAppException(Globalization.GetString("ReserveWorksheet.GetAdjusterEID.Error"), p_objException);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //        {
        //            objReader.Dispose();
        //            objReader = null;
        //        }
        //    }
        //    return (iCurrentAdjusterID);
        //}

        /// <summary>
        /// MGaba2:Added this function to get Claim Id from Claimant RowId
        /// </summary>
        /// <param name="p_iClaimantRowId"></param>
        /// <param name="p_iClaimId"></param>
        protected void GetClaimFromClaimant(int p_iClaimantRowId, ref int p_iClaimId)
        {
            string sSql = string.Empty;
            DbReader objReader = null;

            try
            {
                sSql = "SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_ROW_ID = " + p_iClaimantRowId;

                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSql);
                if (objReader.Read())
                {
                    p_iClaimId = objReader.GetInt32("CLAIM_ID");
                }
                objReader.Close();
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }

        /// <summary>
        /// MGaba2:Added this function to get Claim Id from Unit RowId
        /// </summary>
        /// <param name="p_iUnitRowId"></param>
        /// <param name="p_iClaimId"></param>
        protected void GetClaimFromUnit(int p_iUnitRowId, ref int p_iClaimId)
        {
            string sSql = string.Empty;
            DbReader objReader = null;

            try
            {
                sSql = "SELECT CLAIM_ID FROM UNIT_X_CLAIM WHERE UNIT_ROW_ID = " + p_iUnitRowId;

                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSql);

                if (objReader.Read())
                {
                    p_iClaimId = objReader.GetInt32("CLAIM_ID");
                }
                objReader.Close();
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }

        /// <summary>
        /// MGaba2:Added this function to get ClaimantEId from Claimant RowId
        /// </summary>
        /// <param name="p_iClaimantRowId"></param>
        /// <param name="p_iClaimantId"></param>
        protected void GetClaimantId(int p_iClaimantRowId, ref int p_iClaimantId)
        {
            string sSql = string.Empty;
            DbReader objReader = null;

            try
            {
                sSql = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIMANT_ROW_ID = " + p_iClaimantRowId;

                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSql);
                if (objReader.Read())
                {
                    p_iClaimantId = objReader.GetInt32("CLAIMANT_EID");
                }
                objReader.Close();
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }

        protected void GetClaimantEIdForWC(int p_iClaimId, ref int p_iClaimantEId)
        {
            string sSql = string.Empty;
            DbReader objReader = null;
            int iLOB = 0;
    
            try
            {
                sSql = "SELECT LINE_OF_BUS_CODE  LOB FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSql);
                if (objReader.Read())
                {
                    iLOB = objReader.GetInt32("LOB");
                }
                objReader.Close();

                //if LOB is WC and NonOcc then to fetch claimant rowid
                if (iLOB == 243 || iLOB == 844)
                {
                    sSql = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + p_iClaimId;

                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSql);
                    if (objReader.Read())
                    {
                        p_iClaimantEId = objReader.GetInt32("CLAIMANT_EID");
                    }
                    objReader.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }


        /// <summary>
        /// MGaba2:Added this function to get UnitId from Unit RowId
        /// </summary>
        /// <param name="p_iUnitRowId"></param>
        /// <param name="p_iUnitId"></param>
        protected void GetUnitId(int p_iUnitRowId,ref int p_iUnitId)
        {
            string sSql = string.Empty;
            DbReader objReader = null;

            try
            {
                sSql = "SELECT UNIT_ID FROM UNIT_X_CLAIM WHERE UNIT_ROW_ID = " + p_iUnitRowId;

                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSql);

                if (objReader.Read())
                {
                    p_iUnitId = objReader.GetInt32("UNIT_ID");
                }
                objReader.Close();
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }

        }

        /// <summary>
        /// Get the Approver Id
        /// </summary>
        /// <param name="p_objXmlDoc">XML Document</param>
        /// <param name="iManagerID">Manager Id</param>
        /// <param name="iClaimId">Claim ID</param>
        /// <param name="p_iUserId">User ID</param>
        /// <param name="p_iGroupId">Group ID</param>
        /// <param name="p_sResFinal">Final Reserve Amounts</param>
        /// <param name="p_arrReserves">LOB Reserve Categories</param>
        /// <returns>Approver Id</returns>
        protected int GetApprovalID(int iManagerID, int iClaimId, int p_iUserId, int p_iGroupId, string[,] p_sResFinal, ReserveFunds.structReserves[] p_arrReserves,int p_iClaimCurrCode)
        {
            string sSQL = string.Empty;
            StringBuilder sbSQL = new StringBuilder();

            DbReader objReaderLOB = null;

            int iLOBCode = 0;
            int iThisManagerID = 0;
            int iApproverID = 0;
            int iThisGroupID = 0;

            bool bUnderLimit = false;

            try
            {
                iApproverID = iManagerID;
                iThisManagerID = iManagerID;
                // 12/17/2010 - mcapps2 MITS 22943 Start
                iThisGroupID = p_iGroupId;
                // 12/17/2010 - mcapps2 MITS 22943 End

                #region Get the Line of Business for the claim

                sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId;
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                if (objReaderLOB.Read())
                    iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                objReaderLOB.Close();

                #endregion

                //If there is no manager assigned   
                if (iThisManagerID == 0)
                {
                    #region No Supervisor Assigned
                    iApproverID = GetLOBManagerID(iLOBCode, p_sResFinal);
                    #endregion
                }
                else
                {
                    bUnderLimit = UnderUserLimit(iThisManagerID, p_iGroupId, iClaimId, p_sResFinal, p_arrReserves,p_iClaimCurrCode);

                    if (bUnderLimit)
                    {
                        iApproverID = iThisManagerID;
                    }
                    else
                    {
                        #region Need to check next manager

                        iApproverID = iThisManagerID;
//MGaba2:R8:SuperVisory Approval
                        iThisManagerID = CommonFunctions.GetManagerID(iThisManagerID, m_CommonInfo.DSNID, m_iClientId);
                        // 12/17/2010 - mcapps2 MITS 22943 Start
//MGaba2:R8:SuperVisory Approval
                        iThisGroupID =CommonFunctions.GetGroupID(iThisManagerID,m_CommonInfo.Connectionstring);  //mcapps2 - The manager may not and most likely will not be in the same group as the last user, we need to get the correct group id
                        // 12/17/2010 - mcapps2 MITS 22943 Start

                        # region No Supervisor Assigned
                        //If there is no manager assigned
                        if (iThisManagerID == 0)
                        {
                            iThisManagerID = GetLOBManagerID(iLOBCode, p_sResFinal);
//MGaba2:R8:SuperVisory Approval
                            iThisGroupID = CommonFunctions.GetGroupID(iThisManagerID,m_CommonInfo.Connectionstring);
                        }
                        else
                        {
                            //Not to notify immediate manager
                            if (!m_bNotifySupReserves)
                            {
                                iThisManagerID = GetApprovalID(iThisManagerID, iClaimId, p_iUserId, iThisGroupID, p_sResFinal, p_arrReserves, p_iClaimCurrCode);
                            }
                        }

                        #endregion

                        #endregion
                        iApproverID = iThisManagerID;
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.GetApprovalID.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReaderLOB != null)
                {
                    objReaderLOB.Dispose();
                    objReaderLOB = null;
                }
            }
            return iApproverID;
        }

        /// <summary>
        /// Get the LOB or Top Manager Id
        /// </summary>
        /// <param name="iLOBCode">Line Of Business code ID</param>
        /// <param name="p_sResFinal">Final Reserve Amounts</param>
        /// <returns>LOB or Top Manager Id</returns>
        protected int GetLOBManagerID(int iLOBCode, string[,] p_sResFinal)
        {
            string sSQL = string.Empty;
            long lMaxAmount = 0;
            int iApproverID = 0;

            try
            {
                sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOBCode;

                using (DbReader objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL))
                {
                    while (objReader.Read())
                    {
                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                        lMaxAmount = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader.GetValue("RESERVE_MAX"), m_iClientId);
                        for (int iCount = 0; iCount < p_sResFinal.GetLength(0); iCount++)
                        {
                            if (lMaxAmount < Riskmaster.Common.Conversion.ConvertStrToLong(p_sResFinal[iCount, 3]))
                            {
                                //MGaba2:MITS 19099:Reserve Worksheet doesnot reach LOB level Supervisor if amount doesnt lies in his limit
                                //if Immediate supervisor setting is checked and LOB manager is not the current user,return LOB manager
                                //otherwise top level manager
                                if (m_bNotifySupReserves && iApproverID != 0 &&  m_CommonInfo.UserId != iApproverID)
                                {
                                    return iApproverID;
                                }
                                else
                                {
                                    iApproverID = 0;
                                }
                            }
                        }

                        if (iApproverID > 0)
                            return iApproverID;
                    }
                }

                //Get Top level user
                using (DbReader objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                {
                    if (objReader.Read())
                    {
                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                    }
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.GetLOBManagerID.Error",m_iClientId), p_objException);
            }
            finally
            {

            }
            return iApproverID;
        }

        //sachin 6385
        protected int GetClaimIncLOBManagerID(int iLOBCode, string[,] p_sResFinal)
        {
            string sSQL = string.Empty;
            long lMaxAmount = 0;
            int iApproverID = 0;

            try
            {
                sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOBCode;

                using (DbReader objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL))
                {
                    while (objReader.Read())
                    {
                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                        lMaxAmount = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader.GetValue("CLAIM_INCURRED_MAX"), m_iClientId);
                        for (int iCount = 0; iCount < p_sResFinal.GetLength(0); iCount++)
                        {
                            if (lMaxAmount < Riskmaster.Common.Conversion.ConvertStrToLong(p_sResFinal[iCount, 3]))
                            {
                                //MGaba2:MITS 19099:Reserve Worksheet doesnot reach LOB level Supervisor if amount doesnt lies in his limit
                                //if Immediate supervisor setting is checked and LOB manager is not the current user,return LOB manager
                                //otherwise top level manager
                                if (m_bNotifySupReserves && iApproverID != 0 && m_CommonInfo.UserId != iApproverID)
                                {
                                    return iApproverID;
                                }
                                else
                                {
                                    iApproverID = 0;
                                }
                            }
                        }

                        if (iApproverID > 0)
                            return iApproverID;
                    }
                }

                //Get Top level user
                using (DbReader objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                {
                    if (objReader.Read())
                    {
                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                    }
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.GetClaimIncLOBManagerID.Error", m_iClientId), p_objException);
            }
            finally
            {

            }
            return iApproverID;
        }
        //sachin  6385

        ///MGaba2: R8: SuperVisory Approval:Shifting this code to CommonFunction.cs
        /// <summary>
        /// Get the Manager Id
        /// </summary>
        /// <param name="iRMId">User Id</param>
        /// <returns>Manager Id</returns>
        //protected int GetManagerID(int iRMId)
        //{
        //    int iManagerID = 0;
        //    string sSQL = string.Empty;
        //    DbReader objReader = null;

        //    try
        //    {
        //        sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.MANAGER_ID ";
        //        sSQL = sSQL + " FROM USER_DETAILS_TABLE,USER_TABLE";
        //        sSQL = sSQL + " WHERE USER_DETAILS_TABLE.DSNID = " + m_CommonInfo.DSNID;
        //        sSQL = sSQL + " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";
        //        sSQL = sSQL + " AND USER_TABLE.USER_ID = " + iRMId;
        //        objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                iManagerID = Conversion.ConvertObjToInt(objReader.GetValue("MANAGER_ID"), m_iClientId);
        //            }
        //        }
        //        objReader.Close();
        //    }
        //    catch (RMAppException p_objException)
        //    {
        //        throw p_objException;
        //    }
        //    catch (Exception p_objException)
        //    {
        //        throw new RMAppException(Globalization.GetString("ReserveWorksheet.GetManagerID.Error"), p_objException);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //        {
        //            objReader.Dispose();
        //            objReader = null;
        //        }
        //    }
        //    return iManagerID;
        //}


        ///MGaba2: R8: SuperVisory Approval:Shifting this code to CommonFunction.cs
        /// <summary>
		//mcapps2 MITS 22943 Start
        /// Get the Group Id
        /// </summary>
        /// <param name="iRMId">User Id</param>
        /// <returns>Manager Id</returns>
        //protected int GetGroupID(int iManagerId)
        //{
        //    // 12/17/2010 - mcapps2 MITS 22943
        //    //mcapps2 - The manager may not and most likely will not be in the same group as the last user, we need to get the correct group id
        //    int iThisGroupID = 0;
        //    string sSQL = string.Empty;
        //    DbReader objReader = null;

        //    try
        //    {
        //        sSQL = "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + iManagerId; 
        //        objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
        //        if (objReader != null)
        //        {
        //            while (objReader.Read())
        //            {
        //                iThisGroupID = Conversion.ConvertObjToInt(objReader.GetValue("GROUP_ID"), m_iClientId);
        //            }
        //        }
        //        objReader.Close();
        //    }
        //    catch (RMAppException p_objException)
        //    {
        //        throw p_objException;
        //    }
        //    catch (Exception p_objException)
        //    {
        //        throw new RMAppException("An Error Occured Getting The Group ID For The Manager", p_objException);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //        {
        //            objReader.Dispose();
        //            objReader = null;
        //        }
        //    }
        //    return iThisGroupID;
        //}
		//mcapps2 MITS 22943 End
        /// <summary>
        /// Generates a diary given the various parameters
        /// </summary>
        /// <param name="p_sEntryName">Entry Name</param>
        /// <param name="p_sEntryNotes">Entry Notes</param>
        /// <param name="p_iPriority">Priority of the diary</param>
        /// <param name="p_sAssignedUser">Assigned user</param>
        /// <param name="p_sAssigningUser">Assigning user</param>
        /// <param name="p_sRegarding">Regarding</param>
        /// <param name="p_iClaimId">claim id</param>
        protected void GenerateDiary(string p_sEntryName, string p_sEntryNotes, int p_iPriority, string p_sAssignedUser,
            string p_sAssigningUser, string p_sRegarding, int p_iClaimId)
        {
            DataModelFactory objDataModelFactory = null;
            WpaDiaryEntry objDiary = null;

            try
            {
                objDataModelFactory = new DataModelFactory(m_CommonInfo.DSN, m_CommonInfo.UserName, m_CommonInfo.UserPassword,m_iClientId);
                objDiary = (WpaDiaryEntry)objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);

                objDiary.EntryName = p_sEntryName;
                objDiary.EntryNotes = p_sEntryNotes;
                objDiary.Priority = p_iPriority;
                objDiary.CreateDate = Conversion.ToDbDateTime(DateTime.Now);
                objDiary.StatusOpen = true; // Status Open
                objDiary.AutoConfirm = false;
                objDiary.AssignedUser = p_sAssignedUser;
                objDiary.AssigningUser = p_sAssigningUser;
                objDiary.Regarding = p_sRegarding;
                objDiary.CompleteDate = Conversion.ToDbDate(DateTime.Now); // Complete Date
                objDiary.CompleteTime = Conversion.ToDbTime(DateTime.Now);

                // Attach the claim with the diary
                objDiary.IsAttached = true;
                objDiary.AttachTable = "CLAIM";
                objDiary.AttachRecordid = p_iClaimId;
                //Indu - Adding att_parent_code for Diary enhancement - Mits 33843
                /*Commenting the below line because the attach record Id is claim id itself.So parent record  will also be the same*/
                //objDiary.AttParentCode = p_iClaimId;

                objDiary.Save();
            }
            finally
            {
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                if (objDiary != null)
                    objDiary = null;
            }
        }

        /// <summary>
        /// This function sends an email given the various parameters
        /// </summary>
        /// <param name="p_sTo">User to whom the email needs to be sent</param>
        /// <param name="p_sFrom">Sending user</param>
        /// <param name="p_sSubject">subject of the mail</param>
        /// <param name="p_sBody">Body of the email</param>
        protected void SendEmail(string p_sTo, string p_sFrom, string p_sSubject, string p_sBody)
        {
            Mailer objMailer = null;
            try
            {
                objMailer = new Mailer(m_iClientId);
                objMailer.To = p_sTo;
                objMailer.From = p_sFrom;
                objMailer.Subject = p_sSubject;
                objMailer.Body = p_sBody;

                objMailer.SendMail();
            }
            finally
            {
                if (objMailer != null)
                    objMailer = null;
            }
        }

        protected bool IsRecoveryReserve(int p_iReserveTypeCode)
        {
            LocalCache objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            if (p_iReserveTypeCode != 0 && String.Compare(objCache.GetRelatedShortCode(p_iReserveTypeCode), "R") == 0)
            {
                //This is a recovery reserve.. Balance would be calculated differently
                //for calculation details please refer to rmA13.1 financial equivalency design document
                objCache.Dispose();
                return true;
            }
            else
            {
                objCache.Dispose();
                return false;
            }
        }
        #region CalculateReserveBalance (p_dReserve, p_dPaid, p_dCollect, p_bCollInRsvBal, p_iCStat)

        /// Name		: CalculateReserveBalance
        /// Author		: Mohit Yadav
        /// Date Created: 18/08/2008
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used calculating the Reserve balance on the basis of 
        /// total amount collected and paid.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_dReserve">The Reserve amount</param>
        /// <param name="p_dPaid">The amount already paid to the Claimant</param>
        /// <param name="p_dCollect">The amount already collected.</param>
        /// <param name="p_bCollInRsvBal"></param>
        /// <param name="p_iCStat">Optional parameter</param>
        /// MITS 30191 Raman Bhatia: Implementation of recovery reserve
        protected double CalculateReserveBalance(double p_dReserve, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, int p_iCStat, int p_iReserveTypeCode)
        {
            double dReturnValue = 0;
            double dTemp = 0;

            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
           // bool bIsRecoveryReserve = false;
            LocalCache objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);

            ////MITS 30191 Raman Bhatia: Implementation of recovery reserve
            //if (p_iReserveTypeCode != 0 && String.Compare(objCache.GetRelatedShortCode(p_iReserveTypeCode), "R") == 0)
            //{
            //    //This is a recovery reserve.. Balance would be calculated differently
            //    //for calculation details please refer to rmA13.1 financial equivalency design document

            //    bIsRecoveryReserve = true;
            //}
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve if function is used then why to use another variable
            if (IsRecoveryReserve(p_iReserveTypeCode))
            {
                dReturnValue = p_dReserve - p_dCollect;  //Paid Total would always be 0 for recovery reserves
            }
            else
            {
                if (p_bCollInRsvBal)
                {
                    dTemp = p_dPaid - p_dCollect;
                    //if (dTemp < 0) //rsushilaggar MITS 29710 Date 12/03/2012
                    //    dTemp = 0;
                    dReturnValue = p_dReserve - dTemp;
                }
                else
                {
                    dReturnValue = p_dReserve - p_dPaid;
                }
            }
            objCache.Dispose();
            return dReturnValue;
        }

        /// <summary>
        /// Added by Nitin for R6
        /// </summary>
        /// <param name="p_dReserve"></param>
        /// <param name="p_dPaid"></param>
        /// <param name="p_dCollect"></param>
        /// <param name="p_bCollInRsvBal"></param>
        /// <param name="p_iCStat"></param>
        /// <returns></returns>
        protected double CalculatePaidAmount(double p_dPaid, double p_dCollect,bool p_bCollInRsvBal)
        {
            double dReturnValue = 0;

            if (p_bCollInRsvBal)
            {
                dReturnValue = p_dPaid - p_dCollect;
                //if (dReturnValue < 0) //rsushilaggar MITS 29710 Date 12/03/2012
                //    dReturnValue = 0;
            }
            else
                dReturnValue = p_dPaid; 

            return dReturnValue;
        }

        
        /// Name		: CalculateReserveBalance
        /// Author		: Mohit Yadav
        /// Date Created: 19/08/2008
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used calculating the Reserve balance on the basis of 
        /// total amount collected and paid.
        /// </summary>
        /// <remarks>		
        /// This is an overloaded function for calling the function without p_iCStat value.
        /// This internally calls the same function with p_iCStat = 0
        /// </remarks>
        /// <param name="p_dReserve">The Reserve amount</param>
        /// <param name="p_dPaid">The amount already paid to the Claimant</param>
        /// <param name="p_dCollect">The amount already collected.</param>
        /// <param name="p_bCollInRsvBal"></param>
        protected double CalculateReserveBalance(double p_dReserve, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal , int p_iReserveTypeCode)
        {
            double dReturnValue = 0;
            dReturnValue = CalculateReserveBalance(p_dReserve, p_dPaid, p_dCollect, p_bCollInRsvBal, 0 , p_iReserveTypeCode);

            return dReturnValue;
        }

    #endregion

        #region CalculateIncurred (p_dBalance, p_dPaid, p_dCollect, p_bCollInRsvBal)

        /// Name		: CalculateIncurred
        /// Author		: Mohit Yadav
        /// Date Created: 18/08/2008
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used calculating the Incurred amount on the basis of 
        /// total amount collected and paid.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_dBalance">The remaining balance</param>
        /// <param name="p_dPaid">The amount already paid to the Claimant</param>
        /// <param name="p_dCollect">The amount already collected.</param>
        /// <param name="p_bCollInRsvBal"></param>
        /// <param name="p_bCollInIncurredBal"></param>
        protected double CalculateIncurred(double p_dBalance, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, bool p_bCollInIncurredBal, int p_iReserveTypeCode)
        {
            double dReturnValue = 0;
            double dTemp = 0;
            bool bIsRecoveryReserve = false;
            if (IsRecoveryReserve(p_iReserveTypeCode))
            {
                bIsRecoveryReserve = true;
            }
            if (bIsRecoveryReserve)
            {
                //dReturnValue = (p_dCollect * -1);  //MITS 35837
                if (p_dBalance < 0)
                {
                    dReturnValue = p_dCollect;  //MITS 35837
                }
                else
                {
                    dReturnValue = p_dBalance + p_dCollect;  //MITS 35837
                }
                //dReturnValue = (p_dBalance + p_dCollect);  //MITS 35837
            }
            else
            {
                if (p_bCollInRsvBal)
                {
                    dTemp = p_dPaid - p_dCollect;
                    //if (dTemp < 0) //rsushilaggar MITS 29710 Date 12/03/2012
                    //    dTemp = 0;
                    if (p_dBalance < 0)
                    {
                        dReturnValue = dTemp;
                    }
                    else
                    {
                        dReturnValue = p_dBalance + dTemp;
                    }
                }
                else
                {
                    if (p_dBalance < 0)
                    {
                        dReturnValue = p_dPaid;
                    }
                    else
                    {
                        dReturnValue = p_dBalance + p_dPaid;
                    }
                }

                if (p_bCollInIncurredBal)
                {
                    dReturnValue = dReturnValue - p_dCollect;
                }

                //if (dReturnValue < 0)  //mcapps2 we are not going to set negative amounts to zero any longer
                //{
                //    dReturnValue = 0;
                //}
            }
            return dReturnValue;
        }

        #endregion

        /// <summary>
        /// MGaba2:R6:Retrieves the last Reserve Current unique id counter for the specified claim
        /// </summary>
        /// <param name="p_iClaimId">Claim ID</param>
        /// <param name="p_iClaimantEID">Claimant EID</param>
        ///	<param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <returns>Integer containing the requested unique id value.</returns>
        protected int GetResCurrentID(int p_iClaimId, int p_iClaimantEID, int p_iReserveTypeCode)
        {
            StringBuilder sbSQL;
            DbConnection objCon = null;
            DataSet objDataSet = null;
            DataRow objDataRow = null;

            int iCnt;
            int iNextUID;
            int iMaxUID = 0;

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objCon.Open();

                sbSQL.Append("SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE CLAIM_ID = " + p_iClaimId + " AND");
                sbSQL.Append(" CLAIMANT_EID = " + p_iClaimantEID + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);

                objDataSet = DbFactory.GetDataSet(m_CommonInfo.Connectionstring, sbSQL.ToString(),m_iClientId);
                sbSQL.Remove(0, sbSQL.Length);

                if (objDataSet.Tables[0] != null)
                {
                    for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                    {
                        objDataRow = objDataSet.Tables[0].Rows[iCnt];
                        iNextUID = Conversion.ConvertStrToInteger(objDataRow["RC_ROW_ID"].ToString());

                        if (iMaxUID == 0)
                            iMaxUID = Conversion.ConvertStrToInteger(objDataRow["RC_ROW_ID"].ToString());

                        if (iNextUID > iMaxUID)
                            iMaxUID = iNextUID;
                    }
                    //Destroy the Data Row object.
                    objDataRow = null;
                }
            }
            finally
            {
                if (objDataSet != null)
                    objDataSet = null;

                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
            }
            return iMaxUID;
        }
        /// <summary>
        /// MGaba2:R6:Retrieves the last Reserve History unique id counter for the specified claim
        /// </summary>
        /// <param name="p_iClaimId">Claim ID</param>
        /// <param name="p_iClaimantEID">Claimant EID</param>
        ///	<param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <returns>Integer containing the requested unique id value.</returns>
        protected int GetResHistoryID(int p_iClaimId, int p_iClaimantEID, int p_iReserveTypeCode)
        {
            StringBuilder sbSQL;
            DbConnection objCon = null;
            DataSet objDataSet = null;
            DataRow objDataRow = null;

            int iCnt;
            int iNextUID;
            int iMaxUID = 0;

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objCon.Open();

                sbSQL.Append("SELECT RSV_ROW_ID FROM RESERVE_HISTORY WHERE CLAIM_ID = " + p_iClaimId + " AND");
                sbSQL.Append(" CLAIMANT_EID = " + p_iClaimantEID + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);

                objDataSet = DbFactory.GetDataSet(m_CommonInfo.Connectionstring, sbSQL.ToString(),m_iClientId);
                sbSQL.Remove(0, sbSQL.Length);

                if (objDataSet.Tables[0] != null)
                {
                    for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                    {
                        objDataRow = objDataSet.Tables[0].Rows[iCnt];
                        iNextUID = Conversion.ConvertStrToInteger(objDataRow["RSV_ROW_ID"].ToString());

                        if (iMaxUID == 0)
                            iMaxUID = Conversion.ConvertStrToInteger(objDataRow["RSV_ROW_ID"].ToString());

                        if (iNextUID > iMaxUID)
                            iMaxUID = iNextUID;
                    }
                    //Destroy the Data Row object.
                    objDataRow = null;
                }
            }
            finally
            {
                if (objDataSet != null)
                    objDataSet = null;

                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
            }
            return iMaxUID;
        }
        ///<summary>
        /// MGaba2:R6:This function saves the Reserve Worksheet Links to Reserve Current and Reserve History, once it is approved
        /// </summary>
        /// <param name="p_objXMLIn"></param>
        /// <param name="p_lArrResCurr"></param>
        /// <param name="p_lArrResHist"></param>
        protected void SaveReserveWorksheetLinks(XmlDocument p_objXMLIn, long[,] p_lArrResCurr, long[,] p_lArrResHist)
        {
            int iRSWId = 0;
            StringBuilder sbSQL;
            DbConnection objCon = null;
            DbCommand objCmd = null;

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                iRSWId = Conversion.ConvertStrToInteger(p_objXMLIn.SelectSingleNode("//control[@name='hdnRSWid']").InnerText);

                if (iRSWId > 0)
                {
                    objCon.Open();

                    for (int iCount = 0; iCount < p_lArrResCurr.GetLength(0); iCount++)
                    {
                        if ((p_lArrResCurr[iCount, 1] > 0) || (p_lArrResHist[iCount, 1] > 0))
                        {
                            // Insert into database table 'RSW_CUR_HIS_LINK' new record
                            sbSQL.Append("INSERT INTO RSW_CUR_HIS_LINK (RSW_ROW_ID, RC_ROW_ID, RSV_ROW_ID) VALUES (");
                            sbSQL.Append(iRSWId.ToString() + ", ");
                            sbSQL.Append(p_lArrResCurr[iCount, 1].ToString() + ", ");
                            sbSQL.Append(p_lArrResHist[iCount, 1].ToString() + ")");

                            objCmd = objCon.CreateCommand();
                            objCmd.CommandText = sbSQL.ToString();
                            sbSQL.Remove(0, sbSQL.Length);
                            objCmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.SaveReserveWorksheetLinks.Err",m_iClientId), ex);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
            }
        }

        protected void GetSecurityIDS(int lClaimID, int lClaimantEID, int lUnitID, int lLOB)
        {
            if (m_iParentSecurityId == 0 || m_iSecurityId == 0)
            {
                // Look up LOB if it is not passed - some methods don't have it
                if (lLOB == 0)
                {
                    DbReader objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, "SELECT CLAIM.LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + lClaimID);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            lLOB = objReader.GetInt32("LINE_OF_BUS_CODE");
                        }
                        objReader.Close();
                        objReader.Dispose();
                    }//if (objReader != null)
                }

                switch (lLOB)
                {
                    case 241:
                        if (lClaimantEID == 0)
                        { // claim level
                            m_iParentSecurityId = GC_SID;
                            m_iSecurityId = GC_RSRV_SID;
                        }
                        else
                        { // detail level - assume coming from claimant screen
                            m_iParentSecurityId = GC_CLMNTS_SID;
                            m_iSecurityId = GC_CLMNTS_RSRV_SID;
                        }
                        break;
                    case 242:
                        if (lClaimantEID != 0)
                        { // detail level - coming from claimant screen
                            m_iParentSecurityId = VA_CLMNTS_SID;
                            m_iSecurityId = VA_CLMNTS_RSRV_SID;
                        }
                        else if (lUnitID != 0)
                        { // detail level - coming from unit screen
                            m_iParentSecurityId = VA_UNIT_SID;
                            m_iSecurityId = VA_UNIT_RSRV_SID;
                        }
                        else
                        { // claim level
                            m_iParentSecurityId = VA_SID;
                            m_iSecurityId = VA_RSRV_SID;
                        }
                        break;
                    case 243:
                        m_iParentSecurityId = WC_SID;
                        m_iSecurityId = WC_RSRV_SID;
                        break;
                    case 844:
                        m_iParentSecurityId = DI_SID;
                        m_iSecurityId = DI_RSRV_SID;
                        break;
                    case 845:
                       if (lClaimantEID != 0)
                        { // detail level - coming from claimant screen
                            m_iParentSecurityId = PC_CLMNTS_SID;
                            m_iSecurityId = PC_CLMNTS_RSRV_SID;
                        }
                        else
                        { // claim level
                            m_iParentSecurityId = PC_SID;
                            m_iSecurityId = PC_RSRV_SID;
                        }
                        break;

                    default:
                        throw new RMAppException("Can't find line of business for security context in ReserveFunds (ClaimID: " + lClaimID + ")");
                };
            }

            // Check 
        }

        /// <summary>
        /// since this function is used from Sevice directly for making it public instead of protected
        /// 
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <returns></returns>
        public string GetRswTypeToLaunch(int p_iClaimId)
        {
            //check if value in web.config 
            DbReader objReaderLOB = null;
            int iLobCode = 0;
            string strSql = string.Empty;
            string rswType = string.Empty;


            rswType = RMConfigurationManager.GetNameValueSectionSettings("ReserveWorksheet", m_CommonInfo.Connectionstring, m_iClientId)["ReserveWorksheetType"];

            if (rswType == "Customize")
            {
                strSql = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSql);
                if (objReaderLOB.Read())
                {
                    iLobCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                }
                objReaderLOB.Close();

                if (iLobCode != 243)
                {
                    rswType = "Generic";
                }
            }
            return rswType;
        }

        protected bool IsRSWAllowed(int p_iRswId,string p_sRswTypeToLaunch,int p_iClaimId)
        {
            //check if value in web.config 
            DbReader objReaderLOB = null;
            int iLobCode = 0;
            string strSql = string.Empty;
            string sRswTypeFromConfig = string.Empty;
            bool isAllow = false;

            try
            {
                sRswTypeFromConfig = RMConfigurationManager.GetNameValueSectionSettings("ReserveWorksheet", m_CommonInfo.Connectionstring, m_iClientId)["ReserveWorksheetType"];
                if (sRswTypeFromConfig == "Customize")
                {
                    strSql = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                    objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSql);
                    if (objReaderLOB.Read())
                    {
                        iLobCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                    }
                    objReaderLOB.Close();

                    if (iLobCode != 243)
                    {
                        sRswTypeFromConfig = "Generic";
                    }
                }

                if (sRswTypeFromConfig == "")
                {
                    return false;
                }
                
                if (p_sRswTypeToLaunch != "")
                {
                    if (sRswTypeFromConfig == p_sRswTypeToLaunch)
                    { 
                        isAllow = true; 
                    }
                }
                else
                {
                    if (p_iRswId > 0)
                    {
                        strSql = "SELECT * FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRswId;
                        objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, strSql);
                        if (objReaderLOB.Read())
                        {
                            p_sRswTypeToLaunch = objReaderLOB.GetString("RSWXMLTYPE");
                            if (p_sRswTypeToLaunch == sRswTypeFromConfig)
                            {
                                isAllow = true;
                            }
                        }
                    }
                }
                return isAllow;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      
        protected bool IsClaimAllowedForWorksheet(int p_iClaimId)
        {
            StringBuilder sbSQL = null;
            DbReader objReaderLOB = null;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            int iClmStatusCode = 0;
            bool bIsClmTypeAllowed = false;
            bool bIsClmStatusAllowed = false;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE,CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReaderLOB.Read())
                {
                    iLOBCode = objReaderLOB.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReaderLOB.GetInt32("CLAIM_TYPE_CODE");
                    iClmStatusCode = objReaderLOB.GetInt32("CLAIM_STATUS_CODE");
                }
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT CLAIM_TYPE_CODE FROM SYS_LOB_RSW_CLAIM_TYPE WHERE  LINE_OF_BUS_CODE = " + iLOBCode + " AND CLAIM_TYPE_CODE = " + iClmTypeCode);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReaderLOB.Read())
                {
                    bIsClmTypeAllowed = false;
                }
                else
                {
                    bIsClmTypeAllowed = true;
                }
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT CLAIM_STATUS_CODE FROM SYS_LOB_RSW_CLAIM_STATUS WHERE  LINE_OF_BUS_CODE = " + iLOBCode + " AND CLAIM_STATUS_CODE = " + iClmStatusCode);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReaderLOB.Read())
                {
                    bIsClmStatusAllowed = false;
                }
                else
                {
                    bIsClmStatusAllowed = true;
                }
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                if (bIsClmStatusAllowed == true && bIsClmTypeAllowed == true)
                {
                    return true;
                }
                else if (bIsClmStatusAllowed == true && bIsClmTypeAllowed == false)
                {
                    return false;
                }
                else if (bIsClmStatusAllowed == false  && bIsClmTypeAllowed == true)
                {
                    return false;
                }
                else if (bIsClmStatusAllowed == false && bIsClmTypeAllowed == false)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected bool IsClosedClaim(int p_iClaimId)
        {
            StringBuilder sbSQL = null;
            Riskmaster.Common.LocalCache objCache = null;
            int iClmStatusCode = 0;
            DbReader objReaderLOB = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId);
                objReaderLOB = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                
                if (objReaderLOB.Read())
                {
                    iClmStatusCode = objReaderLOB.GetInt32("CLAIM_STATUS_CODE");
                }
                
                objReaderLOB.Close();
                sbSQL.Remove(0, sbSQL.Length);

                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                
                if (objCache.GetRelatedShortCode(iClmStatusCode).ToLower() == "c")
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected string ConvertDoubleToCurrencyStr(double p_dValueToConvert)
        {
            string strOutputValue = string.Empty;
            try
            {
                strOutputValue = string.Format("{0:C}", p_dValueToConvert);

                //means a -ve number ,now remvoe parenthesis with -ve sign
                if (p_dValueToConvert < 0)
                {
                    if (strOutputValue.Contains("("))
                    {
                       strOutputValue =  strOutputValue.Replace(")", "");
                       strOutputValue = strOutputValue.Replace("(", "");
                    }

                    strOutputValue = strOutputValue.Insert(strOutputValue.IndexOf('$') + 1, "-");
                }

                return strOutputValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #region Protected GetReserveTracking(p_iClaimId, p_sShortCode)

        /// Name		: GetReserveTracking
        /// Author		: Navneet Sota
        /// Date Created: 11/18/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for Retreiving the level of ReserveTracking
        /// </summary>
        /// <remarks>		
        /// Retreive the level of Reserve-Tracking
        /// </remarks>
        /// <param name="p_iClaimId">The Claim Id</param>
        /// <param name="p_sShortCode">Reference Variable: Short Code</param>
        protected int GetReserveTracking(int p_iClaimId, ref int p_iLOBCode)
        {
            //Constant
            const int LINEOFBUSINESS = 243;
            int iReserveTracking = 0;

            //LOB settings
            Riskmaster.Settings.ColLobSettings objLOB = null;
            Riskmaster.Settings.LobSettings objLOBSettings = null;

            //String Objects
            StringBuilder sbSQL = null;

            //Database objects
            DbReader objReader = null;
            DbConnection objConn = null;

            try
            {
                //Get the Values of LOB and Code Description
                sbSQL = new StringBuilder();

                //Table[0]
                sbSQL.Append("SELECT CLAIM.LINE_OF_BUS_CODE  LOB, CODES_TEXT.Code_Desc  CODEDESC");
                sbSQL.Append(" FROM CLAIM, CODES, CODES_TEXT ");
                sbSQL.Append(" WHERE CLAIM.CLAIM_STATUS_CODE = CODES.CODE_ID ");
                sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = CODES_TEXT.CODE_ID ");
                sbSQL.Append(" AND CLAIM_ID = " + p_iClaimId);

                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objConn.Open();

                objReader = objConn.ExecuteReader(sbSQL.ToString());
                if (objReader.Read())
                {
                    p_iLOBCode = Conversion.ConvertStrToInteger(objReader.GetValue("LOB").ToString());
                    objReader.Close();
                }

                objConn.Close();

                //Retreive the level of ReserveTracking
                if (p_iLOBCode == LINEOFBUSINESS)
                    //changed by shobhana MITS 20774
                    iReserveTracking = 3;
                    // iReserveTracking = 2;
                    //END
                else
                {
                    //Instantiate the LOB Settings Class
                    objLOB = new Riskmaster.Settings.ColLobSettings(m_CommonInfo.Connectionstring,m_iClientId);
                    objLOBSettings = objLOB[p_iLOBCode];
                    if (objLOBSettings != null)
                        iReserveTracking = objLOBSettings.ReserveTracking;
                    else
                        //changed by shobhana MITS 20774
                        iReserveTracking = 3;
                    //END
                }
            }//End Main Try
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesTracking.DataErr",m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();

                if (objConn != null)
                    objConn.Dispose();

                sbSQL = null;
                objLOB = null;
                objLOBSettings = null;
            }
            //Return the Result to calling function
            return iReserveTracking;
        }
        #endregion

        #region Assign Reserve worksheets to supervisor

        /// <summary>
        /// Assign Resreve worsksheet to proper user
        /// </summary>
    //changed paramter shobhana for customization
        protected void AssignReserveWorksheet(string p_ReserveXMLString, string p_sRSWTypeLaunch)
        {
            XmlDocument objXMLOut = new XmlDocument();
            XmlNode objNod = null;
            string sReserveTypeTabs = string.Empty;
            double dPaid = 0.0;
            string sSQL = string.Empty;
            DbReader objReader = null;
            LocalCache objCache = null;
            int iPendingApprovalCode = 6024;
            DateTime dtLastUpdatedRecord = DateTime.Now;

            try
            {
                dictClaimPaidToDate = new Dictionary<int, double>();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                iPendingApprovalCode = objCache.GetCodeId("PA", "RSW_STATUS");
                //Get Supervisory Approval Settings
                ReadRSWCheckOptions();

                if (m_iDaysAppReserves > 0 || m_iHoursAppReserves > 0)
                {
                    sSQL = @"SELECT * FROM RSW_WORKSHEETS WHERE RSW_STATUS_CODE = " + iPendingApprovalCode.ToString();
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                    //Iterate over all pending worksheets 
                    while (objReader.Read())
                    {
                        //Assign each reserve worksheets to proper user
                        m_iLevel = 1;
                        m_iClaim_ID = objReader.GetInt32("CLAIM_ID");
                        objXMLOut.LoadXml(objReader.GetString("RSW_XML"));
                        //gagnihotri MITS 19097 Changes start
                        objNod = objXMLOut.SelectSingleNode("//control[@name='hdnReserveTypeTabs']");
                        if (objNod != null)
                        {
                            sReserveTypeTabs = objNod.InnerText;
                            foreach (string sReserveType in sReserveTypeTabs.Split(','))
                            {
                                objNod = null;
                                objNod = objXMLOut.SelectSingleNode("//control[@name='txt_" + sReserveType + "_Paid']");
                                if (objNod != null)
                                    dPaid += Riskmaster.Common.Conversion.ConvertStrToDouble(objNod.InnerText);
                            }
                        }
                        dictClaimPaidToDate.Add(m_iClaim_ID, dPaid);
                        dPaid = 0.0;
                        //gagnihotri MITS 19097 Changes End
                        dtLastUpdatedRecord = Conversion.ToDate(objReader.GetString("DTTM_RCD_LAST_UPD"));
                        if (m_bNotifySupReserves)
                            StepWiseUpdateRSWForUser(Conversion.ConvertObjToInt(objReader.GetValue("RSW_ROW_ID").ToString(), m_iClientId), Conversion.ConvertObjToStr(objReader.GetValue("SUBMITTED_TO").ToString()), dtLastUpdatedRecord, m_iLevel, objXMLOut, p_ReserveXMLString, p_sRSWTypeLaunch);
                        else
                            JumpUpdateRSWForUser(Conversion.ConvertObjToInt(objReader.GetValue("RSW_ROW_ID").ToString(), m_iClientId), Conversion.ConvertObjToStr(objReader.GetValue("SUBMITTED_TO").ToString()), dtLastUpdatedRecord, m_iLevel, objXMLOut, p_ReserveXMLString, p_sRSWTypeLaunch);
                    }
                }

            }
            catch (Exception ex)
            {
                string err = ex.ToString();
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
                objReader = null;
                objCache = null;
            }
        }
        #endregion

        /// <summary>
        /// Submit worksheet to user using jump functionality
        /// </summary>
        /// <param name="p_iRSWID">Worsksheet ID</param>
        /// <param name="p_sSubmittedTo">Submitted to user</param>
        /// <param name="p_dSubmittedOn">Date of submission</param>
        /// <param name="p_iLevel">Level of supervisor</param>
        /// //changed parameters shobhana for customization of reserves
        internal void JumpUpdateRSWForUser(int p_iRSWID, string p_sSubmittedTo, DateTime p_dSubmittedOn, int p_iLevel, XmlDocument p_objXMLDoc, string p_ReserveXMLString, string p_sRSWTypeLaunch)
        {
            int iInterval = 1;
            string sSQL = string.Empty;
            DbConnection objSecureConn = null;
            DbConnection objConn = null;
            string sRswTypeLaunch=null;
            string sSupervisor = "0";
            string sCurrSupervisor = "0";
            int iLOBCode = 0;
            ReserveFunds.structReserves[] arrReserves = null;
            string[,] sResFinal = null;
            int iClmTypeCode = 0;
            int iLOBManagerID = 0;
            int iTopID = 0;
            int iClaimCurrCode=0;

            try
            {
                objSecureConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);
                objSecureConn.Open();
                objConn.Open();
                
                //iDays = p_iLevel * m_iDaysAppReserves;

                if (m_iDaysAppReserves > 0)
                {
                    iInterval = p_iLevel * m_iDaysAppReserves;
                }
                else if (m_iHoursAppReserves > 0)
                {
                    iInterval = p_iLevel * m_iHoursAppReserves;
                }
                else
                    iInterval = 0;

                //Get LOB of current claim
                sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + m_iClaim_ID.ToString();
                iLOBCode = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);

                sSQL = "SELECT CLAIM_TYPE_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + m_iClaim_ID.ToString();
                iClmTypeCode = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);

                sSQL = "SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + m_iClaim_ID.ToString();
                iClaimCurrCode = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);
                if (p_sRSWTypeLaunch == "")
                {

                    p_sRSWTypeLaunch = GetRswTypeToLaunch(m_iClaim_ID);
                }

                        //tmalhotra2 mits 27272
                        if (p_sRSWTypeLaunch == "Generic" || string.IsNullOrEmpty(p_sRSWTypeLaunch))
                        {
                            arrReserves = GetReservesTypeCustom(iLOBCode, iClmTypeCode, p_ReserveXMLString, p_objXMLDoc);
                        
                        }
                        else if (p_sRSWTypeLaunch == "Customize")
                        {
                          arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                        }
                sResFinal = new string[arrReserves.GetLength(0), 4];
                sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, p_objXMLDoc, iClaimCurrCode);

                // if worsksheet has lapsed for the user, find its supervisor 
                // who lies within the reserve limits
                if (AddIntervalToSubmitDate(p_dSubmittedOn,iInterval) < DateTime.Now)
                {
                    iLOBManagerID = GetLOBManagerID(iLOBCode, sResFinal);
                    sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";
                    iTopID = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);

                    //Supervisor within reserve limits
//MGaba2:R8:SuperVisory Approval
                    sSupervisor = CommonFunctions.GetManagerID(Conversion.ConvertStrToInteger(p_sSubmittedTo), m_CommonInfo.DSNID, m_iClientId).ToString();
                    sSupervisor = GetApprovalID(Conversion.ConvertStrToInteger(sSupervisor), m_iClaim_ID, m_CommonInfo.UserId, 0, sResFinal, arrReserves, iClaimCurrCode).ToString();
                    if (sSupervisor == p_sSubmittedTo)
                    {
                        if ((Conversion.ConvertStrToInteger(p_sSubmittedTo) == iLOBManagerID) && (Conversion.ConvertStrToInteger(p_sSubmittedTo) != iTopID))
                        {
                            p_sSubmittedTo = iTopID.ToString();
                            sSQL = "SELECT SUBMITTED_TO FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                            sCurrSupervisor = Conversion.ConvertObjToStr(objConn.ExecuteScalar(sSQL));
                            if (sCurrSupervisor != p_sSubmittedTo)
                            {
                                sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO ='" + p_sSubmittedTo.ToString().Trim() + "' ";
                                sSQL = sSQL + @" ,DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(p_dSubmittedOn).Trim() + "'";
                                sSQL = sSQL + " WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                                objConn.ExecuteNonQuery(sSQL);
                            }
                        }
                    }
                    else
                    {
                        if (sSupervisor != "0")
                        {
                            p_dSubmittedOn = AddIntervalToSubmitDate(p_dSubmittedOn, 0);
                            JumpUpdateRSWForUser(p_iRSWID, sSupervisor, p_dSubmittedOn, m_iLevel, p_objXMLDoc, p_ReserveXMLString, p_sRSWTypeLaunch);
                            //sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO = '" + sSupervisor.ToString() + "' WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                            //objConn.ExecuteNonQuery(sSQL);
                        }
                    }
                }
                else //worksheet has not lapsed for this user: assign it to this user
                {
                    //sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO = '" + p_sSubmittedTo.ToString() + "' WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                    //objConn.ExecuteNonQuery(sSQL);
                    //Check if the currently assigned user is same
                    sSQL = "SELECT SUBMITTED_TO FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                    sCurrSupervisor = Conversion.ConvertObjToStr(objConn.ExecuteScalar(sSQL));
                    if (sCurrSupervisor != p_sSubmittedTo)
                    {
                        sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO ='" + p_sSubmittedTo.ToString().Trim() + "' ";
                        sSQL = sSQL + @" ,DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(p_dSubmittedOn).Trim() + "'";
                        sSQL = sSQL + " WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                        objConn.ExecuteNonQuery(sSQL);
                    }
                }
            }

            finally
            {
                objSecureConn.Close();
                objConn.Close();
                objSecureConn = null;
                objConn = null;
            }
        }
        //shobhana
     
        /// <summary>
        /// Assign worksheet to manager 
        /// </summary>
        /// <param name="p_iRSWID">Worsksheet ID</param>
        /// <param name="p_sSubmittedTo">Submitted to user</param>
        /// <param name="p_dSubmittedOn">Date of submission</param>
        /// <param name="p_iLevel">Level of supervisor</param>
      //changed parameters for customization of reserves by shobhana
        internal void StepWiseUpdateRSWForUser(int p_iRSWID, string p_sSubmittedTo, DateTime p_dSubmittedOn, int p_iLevel, XmlDocument p_objXMLDoc, string p_sReservesXMLstring, string p_sRSWTypeLaunch)
        {
            int iInterval = 1;
            string sSQL = string.Empty;
            string sSupSQL = string.Empty;
            DbConnection objSecureConn = null;
            string sRswTypeLaunch=string.Empty;
            DbConnection objConn = null;
            string sSubmittedTo = "0";
            string sCurrSupervisor = "0";
            int iClaimCurrCode = 0;

            try
            {
                objSecureConn = DbFactory.GetDbConnection(m_CommonInfo.SecurityConnectionstring);
                objConn = DbFactory.GetDbConnection(m_CommonInfo.Connectionstring);

                if (m_iDaysAppReserves > 0)
                {
                    iInterval = p_iLevel * m_iDaysAppReserves;
                }
                else if (m_iHoursAppReserves > 0)
                {
                    iInterval = p_iLevel * m_iHoursAppReserves;
                }
                else
                    iInterval = 0;

                // if worsksheet has lapsed for the user, find its supervisor 
                if (AddIntervalToSubmitDate(p_dSubmittedOn, iInterval) < DateTime.Now)
                {
                    objSecureConn.Open();
                    sSQL = @"SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + p_sSubmittedTo.ToString();
                    sSubmittedTo = Conversion.ConvertObjToStr(objSecureConn.ExecuteScalar(sSQL));
                    objSecureConn.Close();

                    //If supervisor exists for this user, determine if the worksheet has lapsed for it too 
                    //or not otherwise assign worksheet to supervisor
                    if (sSubmittedTo != "0")
                    {
                        //m_iLevel += 1;
                        // By Vaibahv on 11/14/08 : Date submitted updated, so that if worksheet  
                        p_dSubmittedOn = AddIntervalToSubmitDate(p_dSubmittedOn, 0);
                        StepWiseUpdateRSWForUser(p_iRSWID, sSubmittedTo, p_dSubmittedOn, m_iLevel, p_objXMLDoc,p_sReservesXMLstring, p_sRSWTypeLaunch);
                    }
                    else //No supervisor for this user
                    {
                        //check if the worsksheet has lapsed for topmost supervisor too.
                        // if yes, then assign it to LOB manager
                        //iDays = iDays + m_iDaysAppReserves;

                        if (m_iDaysAppReserves > 0)
                        {
                            iInterval = iInterval * m_iDaysAppReserves;
                        }
                        else if (m_iHoursAppReserves > 0)
                        {
                            iInterval = iInterval * m_iHoursAppReserves;
                        }
                        else
                            iInterval = 0;


                        int iLOBCode = 0;
                        int iClmTypeCode = 0;
                        int iLOBManagerID = 0;
                        ReserveFunds.structReserves[] arrReserves = null;
                        string[,] sResFinal = null;

                        objConn.Open();
                        //Get LOB of current claim
                        sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + m_iClaim_ID.ToString();
                        iLOBCode = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);

                        sSQL = "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + m_iClaim_ID.ToString();
                        iClmTypeCode = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);
                        //skhare7 r8
                        sSQL = "SELECT CLAIM_TYPE_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID = " + m_iClaim_ID.ToString();
                            iClaimCurrCode = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);
                        
                        if (p_sRSWTypeLaunch == "")
                        {

                            p_sRSWTypeLaunch = GetRswTypeToLaunch(m_iClaim_ID);
                        }


                        if (p_sRSWTypeLaunch == "Generic")
                        {
                            arrReserves = GetReservesTypeCustom(iLOBCode, iClmTypeCode, p_sReservesXMLstring, p_objXMLDoc);
                        
                        }
                        else if (p_sRSWTypeLaunch == "Customize")
                        {
                          arrReserves = GetResCategories(iLOBCode, iClmTypeCode);
                        }
                        sResFinal = new string[arrReserves.GetLength(0), 4];
                        sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, p_objXMLDoc, iClaimCurrCode);
                        iLOBManagerID = GetLOBManagerID(iLOBCode, sResFinal);

                        sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";
                        int iTopID = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);

                        if (iLOBManagerID == 0)
                        {
                            sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO = '" + p_sSubmittedTo.ToString().Trim() + "', DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(p_dSubmittedOn).Trim() + "'" +
                               " WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                        }
                        else if (iTopID == iLOBManagerID)
                        {
                            sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO = '" + iLOBManagerID.ToString().Trim() + "', DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(AddIntervalToSubmitDate(p_dSubmittedOn, 0)).Trim() + "'" +
                               " WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                            p_sSubmittedTo = iLOBManagerID.ToString();
                        }
                        else
                        {
                            //if lapsed
                            if (AddIntervalToSubmitDate(p_dSubmittedOn, iInterval) < DateTime.Now)
                            {
                                //p_dSubmittedOn = p_dSubmittedOn.AddDays(m_iDaysAppReserves);
                                p_dSubmittedOn = AddIntervalToSubmitDate(p_dSubmittedOn, 0);
                                sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO = '" + iTopID.ToString().Trim() + "', DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(AddIntervalToSubmitDate(p_dSubmittedOn, 0)).Trim() + "'" +
                                       " WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                                p_sSubmittedTo = iTopID.ToString();
                            }
                            else //Worksheet has not lapsed for current supervisor
                            {
                                //Assign worksheet to TOP LEVEL
                                sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO = '" + iLOBManagerID.ToString().Trim() + "', DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(AddIntervalToSubmitDate(p_dSubmittedOn, 0)).Trim() + "'" +
                                       " WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                                p_sSubmittedTo = iLOBManagerID.ToString();
                            }
                        }

                        sSupSQL = "SELECT SUBMITTED_TO FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                        sCurrSupervisor = Conversion.ConvertObjToStr(objConn.ExecuteScalar(sSupSQL));
                        if (sCurrSupervisor != p_sSubmittedTo)
                        {
                            objConn.ExecuteNonQuery(sSQL);
                        }
                        objConn.Close();
                    }
                }
                //worksheet has not lapsed for this user: assign it to this user
                else
                {
                    objConn.Open();
                    sSupSQL = "SELECT SUBMITTED_TO FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                    sCurrSupervisor = Conversion.ConvertObjToStr(objConn.ExecuteScalar(sSupSQL));
                    if (sCurrSupervisor != p_sSubmittedTo)
                    {
                        sSQL = @"UPDATE RSW_WORKSHEETS SET SUBMITTED_TO ='" + p_sSubmittedTo.ToString().Trim() + "' ";
                        sSQL = sSQL + @" ,DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(p_dSubmittedOn).Trim() + "'";
                        sSQL = sSQL + " WHERE RSW_ROW_ID = " + p_iRSWID.ToString();
                        objConn.ExecuteNonQuery(sSQL);
                    }
                    objConn.Close();
                }
            }

            finally
            {
                objSecureConn = null;
                objConn = null;
            }
        }
        /// <summary>
        /// Added by Nitin 
        /// </summary>
        /// <param name="p_dSubmitDate"></param>
        /// <param name="p_interVal"></param>
        /// <returns></returns>
        private DateTime AddIntervalToSubmitDate(DateTime p_dSubmitDate, int p_interVal)
        {
            DateTime dFinalSubmitDate;
            try
            {
                if (p_interVal > 0)
                {
                    if (m_iDaysAppReserves > 0)
                    {
                        dFinalSubmitDate = p_dSubmitDate.AddDays(p_interVal);
                    }
                    else if (m_iHoursAppReserves > 0)
                    {
                        dFinalSubmitDate = p_dSubmitDate.AddHours(p_interVal);
                    }
                    else
                        dFinalSubmitDate = p_dSubmitDate;
                }
                else
                {
                    if (m_iDaysAppReserves > 0)
                    {
                        dFinalSubmitDate = p_dSubmitDate.AddDays(m_iDaysAppReserves);
                    }
                    else if (m_iHoursAppReserves > 0)
                    {
                        dFinalSubmitDate = p_dSubmitDate.AddHours(m_iHoursAppReserves);
                    }
                    else
                        dFinalSubmitDate = p_dSubmitDate;
                }

                return dFinalSubmitDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Functions for Print Functionality

        /// Name		: CreateNewPage
        /// Author		: Mohit Yadav:Retrofit by MGaba2
        /// Date Created: 09/18/2008	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates a new page in a report
        /// </summary>
        /// <param name="p_bFirstPage">First page or not</param>
        /// <param name="p_sSubTitle">SubTitle for the page</param>
        /// <param name="p_objPrintWrapper">print wrapper object</param>
        protected void CreateNewPage(bool p_bFirstPage, string p_sSubTitle, PrintWrapper p_objPrintWrapper)
        {
            SysParms objSysSetting = null;

            try
            {
                if (!p_bFirstPage)
                {
                    p_objPrintWrapper.NewPage();
                }

                p_objPrintWrapper.FillRect(0, 0, 11910, 15600, Color.White);

                // Header contains the title sent as left aligned and client name and date as right aligned

                p_objPrintWrapper.FillRect(0, 0, 11910, 500, Color.LightGray);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 100;
                p_objPrintWrapper.SetFont("Arial", 11);
                p_objPrintWrapper.SetFontBold(true);
                p_objPrintWrapper.PrintTextNoCr(p_sSubTitle);

                objSysSetting = new SysParms(m_CommonInfo.Connectionstring, m_iClientId);

                p_objPrintWrapper.CurrentX = 11600;
                p_objPrintWrapper.PrintTextEndAt(objSysSetting.SysSettings.ClientName + "  " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "  ");
                objSysSetting = null;

                // Footer contains the page number as left aligned and confidential data as right aligned

                p_objPrintWrapper.FillRect(0, 15200, 11910, 15600, Color.LightGray);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 15500 - p_objPrintWrapper.GetTextHeight("V");
                p_objPrintWrapper.PrintTextNoCr("Page" + " [@@PageNo@@]");

                p_objPrintWrapper.CurrentX = 11600;
                p_objPrintWrapper.PrintTextEndAt(Globalization.GetString("ReserveWorksheet.CreateNewPage.ConfidentialData",m_iClientId));

                p_objPrintWrapper.SetFont("Verdana", 9);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.CreateNewPage.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objSysSetting = null;
            }
        }

        /// <summary>
        /// MGaba2:R6:Writes a text onto the PDF, given the various format parameters
        /// </summary>
        /// <param name="p_sText">text to be printed</param>
        /// <param name="p_bContinue">text to be continued after this printing on the same line?</param>
        /// <param name="p_objPrintWrapper">PrintWrapper object</param>
        /// <param name="p_sTitle">Title of the page</param>
        /// <param name="p_bBold">Bold ?</param>
        /// <param name="p_dFontSize">Font Size</param>
        protected void WriteText(string p_sText, bool p_bContinue, PrintWrapper p_objPrintWrapper, string p_sTitle,
            bool p_bBold, double p_dFontSize)
        {
            // If limit has been reached, create a new page in the report
            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }

            p_objPrintWrapper.SetFont(p_dFontSize);
            if (p_bBold)
                p_objPrintWrapper.SetFontBold(true);

            p_objPrintWrapper.PrintTextNoCr(p_sText);

            if (p_bContinue)
                p_objPrintWrapper.CurrentX += p_objPrintWrapper.GetTextWidth(p_sText);
            else
                CreateNewLine(p_objPrintWrapper, p_sTitle);
        }

        /// <summary>
        /// MGaba2:R6:Creates a new line (line break) in the printed document.
        /// </summary>
        /// <param name="p_objPrintWrapper"></param>
        /// <param name="p_sTitle"></param>
        private void CreateNewLine(PrintWrapper p_objPrintWrapper, string p_sTitle)
        {
            try
            {
                p_objPrintWrapper.CurrentY += p_objPrintWrapper.GetTextHeight("W");

                if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
                {
                    CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                    p_objPrintWrapper.CurrentX = 100;
                    p_objPrintWrapper.CurrentY = 600;
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.CreateNewLine.Error",m_iClientId), ex);
            }
        }


        /// <summary>
        /// MGaba2:MITS 18534: If text is coming on multiline like Claimant name
        /// </summary>
        /// <param name="p_objPrintWrapper"></param>
        /// <param name="p_sTitle"></param>
        /// <param name="p_rowCount"></param>
        private void CreateNewLineAfterNRows(PrintWrapper p_objPrintWrapper, string p_sTitle,int p_rowCount)
        {
            try
            {
                p_objPrintWrapper.CurrentY += p_rowCount * p_objPrintWrapper.GetTextHeight("W");

                if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
                {
                    CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                    p_objPrintWrapper.CurrentX = 100;
                    p_objPrintWrapper.CurrentY = 600;
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorkSheet.CreateNewLineAfterNRows.Error",m_iClientId), ex);
            }
        }
        /// <summary>
        /// MGaba2:R6:Prints Reserve Summary
        /// </summary>
        /// <param name="objRSWDoc"></param>
        /// <param name="objPrintWrapper"></param>
        /// 

        private void PrintResSummary(XmlDocument objRSWDoc, PrintWrapper objPrintWrapper)
        {

            XmlNode objXmlCtrl = null;
            string sReserveType = string.Empty;
            string[] sResCats = null;
            string sCtrlName = string.Empty;
            string[,] sArrResSum = null;
            string shdnReserveTypeTabs = string.Empty;
            double dSumPaid = 0;
            double dSumOutStanding = 0;
            double dSumNewOutStanding = 0;
            double dSumNewTotalIncur = 0;
            double dSumReserveChange = 0;
            //Aman Multi Currency--Start
            XmlNode objNode = null;
            string sClaimNumber = string.Empty;
            int iClaimId = 0;
            DbReader objReader = null;
            string sSql = string.Empty;
            try
            {
                objNode = objRSWDoc.SelectSingleNode("//control[@name='hdnClaimNumber']");
                if (objNode != null)
                {
                    sClaimNumber = objNode.InnerText;
                    sSql = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber + "'";
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSql.ToString());
                    if (objReader.Read())
                    {
                        iClaimId = objReader.GetInt(0);
                    }
                }
                objReader.Close();
                //Aman Multi Currency--End

                objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='hdnReserveTypeTabs']");
                if (objXmlCtrl != null)
                {
                    shdnReserveTypeTabs = objXmlCtrl.InnerText;
                    //a hook to remove an extra comma in the end
                    shdnReserveTypeTabs = shdnReserveTypeTabs.Substring(0, shdnReserveTypeTabs.Length - 1);
                    sResCats = shdnReserveTypeTabs.Split(',');

                    sArrResSum = new string[sResCats.Length + 2, 6];

                    sArrResSum[0, 0] = "Reserve Category ";
                    sArrResSum[0, 1] = "Paid ";
                    sArrResSum[0, 2] = "Previous Outstanding ";
                    sArrResSum[0, 3] = "New  Outstanding ";
                    sArrResSum[0, 4] = "New Incurred ";
                    sArrResSum[0, 5] = "Reserve Change ";

                    for (int iCnt = 1; iCnt <= sResCats.GetLength(0); iCnt++)
                    {
                        int j = 0;

                        sReserveType = sResCats[iCnt - 1].Substring(12);
                        sCtrlName = "ReserveType_" + sReserveType;
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//group[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                            sArrResSum[iCnt, j++] = objXmlCtrl.Attributes["title"].Value;

                        sCtrlName = "txt_ReserveType_" + sReserveType + "_Paid";
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                        {
                            sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            dSumPaid += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "txt_ReserveType_" + sReserveType + "_Outstanding";
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                        {
                            sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            dSumOutStanding += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "hdn_ReserveType_" + sReserveType + "_NEWOutstanding";
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                        {
                            //sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            sArrResSum[iCnt, j++] = CommonFunctions.ConvertCurrency(iClaimId, Conversion.ConvertStrToDouble(objXmlCtrl.InnerText), m_eNavType, m_CommonInfo.Connectionstring,m_iClientId);    //Aman Multi Currency                 
                            dSumNewOutStanding += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "hdn_ReserveType_" + sReserveType + "_NEWTOTALINCURRED";
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                        {
                            //sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            sArrResSum[iCnt, j++] = CommonFunctions.ConvertCurrency(iClaimId, Conversion.ConvertStrToDouble(objXmlCtrl.InnerText), m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);    //Aman Multi Currency                 
                            dSumNewTotalIncur += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "hdn_ReserveType_" + sReserveType + "_ReserveChange";
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                        {
                            //sArrResSum[iCnt, j] = objXmlCtrl.InnerText;
                            sArrResSum[iCnt, j] = CommonFunctions.ConvertCurrency(iClaimId, Conversion.ConvertStrToDouble(objXmlCtrl.InnerText), m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);    //Aman Multi Currency                 
                            if (objXmlCtrl.InnerText != "No Change")
                            {
                                dSumReserveChange += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                            }
                        } 
                    } 

                    //sArrResSum[sArrResSum.GetLength(0) - 1, 0] = "SUMMARY TOTALS ";
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 1] = string.Format("{0:C}", dSumPaid);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 2] = string.Format("{0:C}", dSumOutStanding);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 3] = string.Format("{0:C}", dSumNewOutStanding);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 4] = string.Format("{0:C}", dSumNewTotalIncur);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 5] = string.Format("{0:C}", dSumReserveChange);
                    
                    //Aman Multi Currency
                    
                    sArrResSum[sArrResSum.GetLength(0) - 1, 0] = "SUMMARY TOTALS ";
                    sArrResSum[sArrResSum.GetLength(0) - 1, 1] = CommonFunctions.ConvertCurrency(iClaimId, dSumPaid, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 2] = CommonFunctions.ConvertCurrency(iClaimId, dSumOutStanding, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 3] = CommonFunctions.ConvertCurrency(iClaimId, dSumNewOutStanding, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 4] = CommonFunctions.ConvertCurrency(iClaimId, dSumNewTotalIncur, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 5] = CommonFunctions.ConvertCurrency(iClaimId, dSumReserveChange, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);

                    MakeGrid(objPrintWrapper, "", sArrResSum);
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }//Aman Multi Currency
        }



        private void PrintResSummarySafeWay(XmlDocument objRSWDoc, PrintWrapper objPrintWrapper)
        {
            
            XmlNode objXmlCtrl = null;
            string sReserveType = string.Empty;
            string[] sResCats = null;
            string sCtrlName = string.Empty;
            string[,] sArrResSum = null;
            string shdnReserveTypeTabs = string.Empty;
            double dSumPaid = 0;
            double dSumOutStanding = 0;
            double dSumNewOutStanding = 0;
            double dSumNewTotalIncur = 0;
            double dSumReserveChange = 0;
           // double dsumreservechange = 0;
            string hdnreservetypechange = "";
            double dPaid = 0;
            double dOutStanding = 0;
            double dNewTotalIncur = 0;
            XmlNode objXmlCtrl1 = null;
            XmlNode objXmlCtrl2 = null;
            XmlNode objXmlCtrl3 = null;
            string ctrlname1 = "";
            string ctrlname2 = "";
            string ctrlname3 = "";

            //Aman Multi Currency 
            XmlNode objNode = null;
            string sClaimNumber = string.Empty;
            int iClaimId = 0;
            DbReader objReader = null;
            try
            {
                objNode = objRSWDoc.SelectSingleNode("//control[@name='hdnClaimNum']");
                if (objNode != null)
                {
                    sClaimNumber = objNode.InnerText;
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '"+sClaimNumber + "'");
                    if (objReader.Read())
                    {
                        iClaimId = objReader.GetInt(0);
                    }
                }
                objReader.Close();
                objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='hdnReserveTypeTabs']");
                if (objXmlCtrl != null)
                {
                    shdnReserveTypeTabs = objXmlCtrl.InnerText;
                    //a hook to remove an extra comma in the end
                    shdnReserveTypeTabs = shdnReserveTypeTabs.Substring(0, shdnReserveTypeTabs.Length - 1);
                    sResCats = shdnReserveTypeTabs.Split(',');

                    sArrResSum = new string[sResCats.Length + 2, 6];

                    sArrResSum[0, 0] = "Reserve Category ";
                    sArrResSum[0, 1] = "Paid ";
                    sArrResSum[0, 2] = "Previous Outstanding ";
                    sArrResSum[0, 3] = "New  Outstanding ";
                    sArrResSum[0, 4] = "New Incurred ";
                    sArrResSum[0, 5] = "Reserve Change ";

                    for (int iCnt = 1; iCnt <= sResCats.GetLength(0) - 2; iCnt++)
                    {
                        int j = 0;

                        sReserveType = sResCats[iCnt];
                        sCtrlName = sResCats[iCnt];
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//group[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                            sArrResSum[iCnt, j++] = objXmlCtrl.Attributes["title"].Value;

                        sCtrlName = "PaidAmt" + sReserveType.Substring(0, 3) + "WC";

                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");

                        if (objXmlCtrl != null)
                        {
                            sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            dSumPaid += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                            dPaid = Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "BalAmt" + sReserveType.Substring(0, 3) + "WC";

                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");

                        if (objXmlCtrl != null)
                        {
                            sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            dSumOutStanding += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                            dOutStanding = Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "hdn_NewBalAmt" + sReserveType.Substring(0, 3) + "WC";
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                        {
                            //sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            sArrResSum[iCnt, j++] = CommonFunctions.ConvertCurrency(iClaimId, Conversion.ConvertStrToDouble(objXmlCtrl.InnerText), m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);    //Aman Multi Currency                                            
                            dSumNewOutStanding += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "hdn_NewIncAmt" + sReserveType.Substring(0, 3) + "WC";


                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");

                        if (objXmlCtrl != null)
                        {
                            //sArrResSum[iCnt, j++] = objXmlCtrl.InnerText;
                            sArrResSum[iCnt, j++] = CommonFunctions.ConvertCurrency(iClaimId, Conversion.ConvertStrToDouble(objXmlCtrl.InnerText), m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);    //Aman Multi Currency                 
                            dSumNewTotalIncur += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                            dNewTotalIncur = Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                        }

                        sCtrlName = "hdn_REserveChange" + sReserveType.Substring(0, 3) + "WC";
                        objXmlCtrl = objRSWDoc.SelectSingleNode("//control[@name='" + sCtrlName + "']");
                        if (objXmlCtrl != null)
                        {
                            //sArrResSum[iCnt, j] = objXmlCtrl.InnerText;
                            sArrResSum[iCnt, j++] = CommonFunctions.ConvertCurrency(iClaimId, Conversion.ConvertStrToDouble(objXmlCtrl.InnerText), m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);    //Aman Multi Currency                 
                            if (objXmlCtrl.InnerText != "No Change")
                            {
                                dSumReserveChange += Conversion.ConvertStrToDouble(objXmlCtrl.InnerText);
                            }
                        }
                        //             if ((dPaid) + dOutStanding == (dNewTotalIncur))
                        // {
                        //     hdnreservetypechange = "No Change";
                        //     sArrResSum[iCnt, j] = hdnreservetypechange;
                        //    // sArrResSum[iCnt, j] = hdnreservetypechange;
                        //    // m_bReserveAmntChanges = false;
                        // }
                        // else
                        // {
                        //     hdnreservetypechange = ((dNewTotalIncur) - (dPaid) + (dOutStanding)).ToString();
                        //   //  m_bReserveAmntChanges = true;
                        //     sArrResSum[iCnt, j] = (hdnreservetypechange);
                        // }
                        //             if (hdnreservetypechange.ToString() != "No Change") 
                        //{
                        //    dSumReserveChange += Conversion.ConvertStrToDouble(hdnreservetypechange);
                        // }
                    }

                    //sArrResSum[sArrResSum.GetLength(0) - 1, 0] = "SUMMARY TOTALS ";
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 1] = string.Format("{0:C}", dSumPaid);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 2] = string.Format("{0:C}", dSumOutStanding);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 3] = string.Format("{0:C}", dSumNewOutStanding);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 4] = string.Format("{0:C}", dSumNewTotalIncur);
                    //sArrResSum[sArrResSum.GetLength(0) - 1, 5] = string.Format("{0:C}", dSumReserveChange);

                    //Aman Multi Currency --Start
                    sArrResSum[sArrResSum.GetLength(0) - 1, 0] = "SUMMARY TOTALS ";
                    sArrResSum[sArrResSum.GetLength(0) - 1, 1] = CommonFunctions.ConvertCurrency(iClaimId, dSumPaid, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 2] = CommonFunctions.ConvertCurrency(iClaimId, dSumOutStanding, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 3] = CommonFunctions.ConvertCurrency(iClaimId, dSumNewOutStanding, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 4] = CommonFunctions.ConvertCurrency(iClaimId, dSumNewTotalIncur, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    sArrResSum[sArrResSum.GetLength(0) - 1, 5] = CommonFunctions.ConvertCurrency(iClaimId, dSumReserveChange, m_eNavType, m_CommonInfo.Connectionstring, m_iClientId);
                    //Aman Multi Currency --End
                    //MakeGridSummary(objPrintWrapper, "", sArrResSum);
                    MakeGrid(objPrintWrapper, "", sArrResSum);
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }//Aman Multi Currency
        }

        /// <summary>
        /// For All worksheets: Writes a text onto the PDF, given the various format parameters
        /// </summary>
        /// <param name="p_objGroup">XML Group to be printed</param>
        /// <param name="p_objRSWDoc">RSW xml</param>
        /// <param name="p_objPrintWrapper">PrintWrapper object</param>
        /// <param name="p_sLOB">Short Code for LOB</param>
        /// <param name="p_sTabType">Tab Name or Reserve Type</param>
                protected void PrintAllWorksheets(XmlNode p_objGroup, XmlDocument p_objRSWDoc, PrintWrapper p_objPrintWrapper, string p_sLOB, string p_sTabType)
                {
                    //for each displaycolumn in a group
                    string sTitle = string.Empty;
                    string sText = string.Empty;
                    string[] sCols = null;
                    string[,] sGrid = null;
                    int iGridCounter = 0;
                    int iCounter = 0;
                    int iControlType = 0;
                    int iIntControlType = 0;
                    string sGpName = string.Empty;
                    string[] sSRCols = null;
                    string sTextCmb = string.Empty;
                    string sTextCmbLbl = string.Empty;
                    string sLabel = string.Empty;

                    int iGridCol = 0;
                    int iRowCount = 0;
                    int iColumnCount = 0;
                    
                    bool bGridCreated = false;
                    bool bPrintSum = false;//Trigger to print the summary
                    try
                    {
                        sGpName = p_objGroup.Attributes["name"].InnerText;
        //MGaba2:MITS 18534: Reserve Summary section was not coming while Printing PDF
                        if (sGpName == "RESERVESUMMARY")
                        {
                            CreateNewLine(p_objPrintWrapper, sTitle);
                            PrintResSummary(p_objRSWDoc, p_objPrintWrapper);
                        }
                        foreach (XmlNode objDisplayColumn in p_objGroup.SelectNodes(".//displaycolumn"))
                        {
                            //MGaba2:R6:  changing it to 4
                            //sCols = new string[2];
                            sCols = new string[4];
                            sText = "";
                            iCounter = 0;
                            iControlType = LINE_BREAK;

                            //for each 'control' element in a group
                            foreach (XmlNode objControl in objDisplayColumn.SelectNodes("./control"))
                            {
                                switch (objControl.Attributes["type"].Value)
                                {
                                    //Not for linebreak, hidden fields
                                    case "code":
                                    case "currency":
                                    case "labelonly":
                                    case "text":
                                        iGridCol = 0;
                                        if (objControl.Attributes["title"] != null)
                                        {
                                            if (objControl.Attributes["title"].InnerText != "")
                                                sText += objControl.Attributes["title"].InnerText + " ";
                                        }

                                        if (objControl.InnerText != "")
                                        {
                                            if (sText != "")
                                            {
                                                sText += ": ";
                                                sText += objControl.InnerText + " ";
                                            }
                                            else
                                            {
                                                sText += objControl.InnerText + " ";
                                            }
                                        }

                                        if (objControl.Attributes["type"].InnerText == "labelonly")
                                        {
                                            if (objControl.Attributes["name"].InnerText.StartsWith("BlankTitleAmt") == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter += 1;
                                                iControlType = 0;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("BlankTitle") == true)
                                            {
                                                sTextCmbLbl = sText;
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                            }
                                            //MGaba2:R6:Changing the condition for name of particular transaction/row header
                                            //else if (objControl.Attributes["name"].InnerText.StartsWith("BlanklblAmt") == true)
                                            else if (objControl.Attributes["name"].InnerText.Contains("_TransType_") == true)
                                            {
                                                //sText = " ";
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter += 1;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("Blanklbl") == true)
                                            {
                                                //do nothing
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("pcName") == true)
                                            {
                                                if (sTextCmbLbl != "")
                                                    sText = sTextCmbLbl + ": " + sText;

                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("claimnum") == true)
                                            {
                                                if (sTextCmbLbl != "")
                                                    sText = sTextCmbLbl + ": " + sText;
                                               
                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                                // if (objControl.Attributes["name"].InnerText == "claimnum")
                                                //{
                                                //    sTextCmbLbl = objControl.Attributes["name"].InnerText;
                                                //}
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("dtLoss") == true)
                                            {
                                                if (sTextCmbLbl != "")
                                                    sText = sTextCmbLbl + ": " + sText;

                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                            }
                                            //JIRA RMA-9532 nshah28 start
                                            else if (objControl.Attributes["name"].InnerText.Contains("_Paid") == true)
                                            {
                                                //case 1
                                                sText = sText.IndexOf("(-collections)") > 0 ? sText.Substring(0, sText.IndexOf("(-collections)") - 1) + sText.Substring(sText.IndexOf("(-collections)"), sText.Length - sText.IndexOf("(-collections)") - 1) : sText;
                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                                //case 1

                                            }
                                            //RMA-9532 nshah28 ENd
                                            else
                                            {
                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                            }
                                        }
                                        else if (objControl.Attributes["type"].InnerText == "currency")
                                        {
                                            if (objControl.Attributes["name"].InnerText.StartsWith("PaidAmt") == true)
                                            {
                                                if (sTextCmbLbl != "")
                                                    sText = sTextCmbLbl + ": " + sText;

                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("BalAmt") == true)
                                            {
                                                if (sTextCmbLbl != "")
                                                    sText = sTextCmbLbl + ": " + sText;

                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("IncurredAmt") == true)
                                            {
                                                if (sTextCmbLbl != "")
                                                    sText = sTextCmbLbl + ": " + sText;

                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                            }
                                            //MGaba2:R6:Changing the condition for paid and incurred amounts of particular transaction
                                            //else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                                //{
                                                //                iRowCount += 1;
                                                //                sGrid[iGridCounter, iCounter] = sText;
                                                //                iCounter = 0;
                                                //                iControlType = 0;
                                                //                sText = "";
                                                //                if (iGridCounter == sGrid.GetLength(0) - 1)
                                                //                    MakeGrid(p_objPrintWrapper, "", sGrid);
                                                //}
                                            else if (objControl.Attributes["name"].InnerText.Contains("_Paid_") == true) 
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter+= 1;
                                                iControlType = 0;
                                                sText = "";
                                            }
                                                //Added by Nitin for R6 for newly added textbox for NewReserveBalance
                                            else if (objControl.Attributes["name"].InnerText.Contains("_TransTypeNewReserveBal") == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter += 1;
                                                iControlType = 0;
                                                sText = "";
                                            }
                                            else if(objControl.Attributes["name"].InnerText.Contains("_TransTypeIncurredAmount_") == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                if (objControl.Attributes["name"].InnerText.Contains("_TransTypeIncurredAmount_OtherAdjustment") == true)
                                                {
                                                    sGrid[iGridCounter, iCounter+1] = "";
                                                    sGrid[iGridCounter, iCounter + 2] = sText;
                                                    sGrid[iGridCounter, iCounter] = "";
                                                }
                                                iCounter = 0;
                                                iRowCount += 1;
                                                iControlType = 0;
                                                sText = "";
                                                if (iGridCounter == sGrid.GetLength(0) - 1)
                                                    MakeGrid(p_objPrintWrapper, "", sGrid);
                                            }
                                            else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "Rate" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = (sText.IndexOf(":") > 0) ? sText.Replace(":", "") : sText;
                                                iCounter += 1;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.StartsWith("Amt" + iRowCount + "Percent") == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter += 1;
                                                iControlType = 0;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.EndsWith("Rate") == true)
                                            {
                                                iGridCounter += 1;
                                                sGrid[iGridCounter, iGridCol] = sText.IndexOf(':') > 0 ? sText.Substring(0, sText.IndexOf(':') - 1) : sText;
                                                iGridCol += 1;
                                                sGrid[iGridCounter, iGridCol] = sText.IndexOf(':') > 0 ? sText.Substring(sText.IndexOf(':') + 1, sText.Length - sText.IndexOf(':') - 1) : " ";
                                                iControlType = 0;
                                                sText = "";
                                            }
                                            else
                                            {
                                                if (sTextCmbLbl != "")
                                                    sText = sTextCmbLbl + ": " + sText;

                                                sCols[iCounter++] = sText + "\n";
                                                iControlType = TEXT_CTRL;
                                                sText = "";
                                                sTextCmbLbl = "";
                                            }
                                        }
                                        else if (objControl.Attributes["type"].InnerText == "code")
                                        {
                                            if (sTextCmbLbl != "")
                                                sText = sTextCmbLbl + ": " + sText;

                                            sCols[iCounter++] = sText;
                                            iControlType = TEXT_CTRL;
                                            sText = "";
                                        }
                                        else if (objControl.Attributes["type"].InnerText == "text")
                                        {
                                            if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "TxtAmt" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter += 1;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "Weeks" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter += 1;
                                                sText = "";
                                            }
                                            else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "BP" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                            {
                                                sGrid[iGridCounter, iCounter] = sText;
                                                iCounter += 1;
                                                sText = "";
                                            }
                                        }
                                        break;
                                    case "headermessage":
                                    case "headermessage1":
                                    case "headermessage2":
                                        iGridCounter = 0;
                                        iRowCount = GetGridRowCount(p_objGroup, objControl.Attributes["type"].Value);
                                        iColumnCount = GetGridColumnCount(p_objGroup, objControl.Attributes["type"].Value);
                                        sGrid = new string[iRowCount + 1, iColumnCount];

                                        for (int iCnt = 0; iCnt < iColumnCount; iCnt++)
                                        {
                                            switch (iCnt)
                                            {
                                                case 0:
                                                    if (sGpName == "ClaimInfo")
                                                        sGrid[iGridCounter, iCnt] = string.Empty;
                                                    else
                                                        sGrid[iGridCounter, iCnt] = objControl.Attributes["head1"].InnerText;
                                                    break;
                                                case 1:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head3"].InnerText;
                                                    break;
                                                case 2:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head4"].InnerText;
                                                    break;
                                                case 3:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head5"].InnerText;
                                                    break;
                                            }
                                        }
                                        iRowCount = 1;
                                        iColumnCount = 0;
                                        iControlType = 0;
                                        break;
                                    case "headermessage4":
                                        iColumnCount = GetGridColumnCount(p_objGroup, objControl.Attributes["type"].Value);

                                        for (int iCnt = 0; iCnt < iColumnCount; iCnt++)
                                        {
                                            switch (iCnt)
                                            {
                                                case 0:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head1"].InnerText;
                                                    break;
                                                case 1:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head3"].InnerText;
                                                    break;
                                                case 2:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head4"].InnerText;
                                                    break;
                                                case 3:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head5"].InnerText;
                                                    break;
                                                case 4:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head5"].InnerText;
                                                    break;
                                            }
                                        }
                                        iColumnCount = 0;
                                        iControlType = 0;
                                        break;
                                    case "headermessage5":
                                        iGridCounter = 0;
                                        iRowCount = GetGridRowCount(p_objGroup, objControl.Attributes["type"].Value);
                                        iColumnCount = GetGridColumnCount(p_objGroup, objControl.Attributes["type"].Value);
                                        sGrid = new string[iRowCount + 1, iColumnCount];

                                        for (int iCnt = 0; iCnt < iColumnCount; iCnt++)
                                        {
                                            switch (iCnt)
                                            {
                                                case 0:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head1"].InnerText;
                                                    break;
                                                case 1:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head3"].InnerText;
                                                    break;
                                                case 2:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head4"].InnerText;
                                                    break;
                                                case 3:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head6"].InnerText;
                                                    break;
                                                case 4:
                                                    sGrid[iGridCounter, iCnt] = objControl.Attributes["head7"].InnerText;
                                                    break;
                                            }
                                        }
                                        iRowCount = 1;
                                        iColumnCount = 0;
                                        iControlType = 0;
                                        break;
                                    case "message":
                                    //MGaba2:R6:Commented the existing code and added the new code for creating the grid in generic implementation
                                        //To do : Change the type "message in Safeway xml to label only,so that it doesnt come here

                                        //sText = objControl.Attributes["title"].InnerText + " ";
                                        //iControlType = MESSAGE_CTRL;
                                        string[] arrTransaction = null;
                                        if (bGridCreated == false)
                                        {
                                            foreach (XmlNode objMsgNode in objDisplayColumn.SelectNodes("./control[@type='message']"))
                                            {
                                                iColumnCount++;
                                            }
                                            XmlNode objHdnCtrlToEdit=p_objRSWDoc.SelectSingleNode(".//control[@name='hdnCtrlToEdit']");
                                            if (objHdnCtrlToEdit != null)
                                            {
                                                string[] sAllTrans = objHdnCtrlToEdit.InnerText.Split(',');
                                                string sTransOfThisRes = string.Empty;
                                                for (int i = 0; i < sAllTrans.Length; i++)
                                                {
                                                    //if (sAllTrans[i].StartsWith("txt_ReserveType_" + p_sTabType) == true)
                                                    if (sAllTrans[i].StartsWith("txt_ReserveType_" + p_sTabType + "_TransTypeIncurredAmount_") == true)
                                                    {
                                                        if (sTransOfThisRes == string.Empty)
                                                            sTransOfThisRes = sAllTrans[i];
                                                        else
                                                            sTransOfThisRes += "," + sAllTrans[i];
                                                    }
                                                }
                                                arrTransaction = sTransOfThisRes.Split(',');
                                                iRowCount = arrTransaction.Length;
                                            }
                                            if (iRowCount != 0 && iColumnCount != 0)
                                            {
                                                sGrid = new string[iRowCount + 1, iColumnCount];
                                                iGridCounter = 0;
                                                int iCount=0;
                                                foreach (XmlNode objMsgNode in objDisplayColumn.SelectNodes("./control[@type='message']"))
                                                {
                                                    if(objMsgNode.Attributes["title"] !=null)
                                                    {
                                                        sGrid[iGridCounter, iCount] = objMsgNode.Attributes["title"].Value + " " ;
                                                        iCount++;
                                                    }
                                                }
                                                //iGridCounter += 1;
                                                bGridCreated = true;
                                                iRowCount = 1;
                                                iColumnCount = 0;
                                                iControlType = 0;
                                            }
                                        }

                                        break;
                                    case "controlgroup":
                                        //For Control Types: messagewithoutbreak and textarea
                                        if (objControl.SelectSingleNode("./control[@type='messagewithoutbreak']") != null)
                                        {
                                            if (objControl.SelectSingleNode("./control[@type='messagewithoutbreak']").Attributes["title"] != null)
                                            {
                                                sTextCmb = objControl.SelectSingleNode("./control[@type='messagewithoutbreak']").Attributes["title"].InnerText + ": ";
                                                break;
                                            }
                                        }

                                        if (objControl.SelectSingleNode("./control[@type='textarea']") != null)
                                            sText = objControl.SelectSingleNode("./control[@type='textarea']").InnerText;

                                        //Vaibhav for MITS 15418:
                                        if (sTextCmb != "")
                                        {
                                            sText = sTextCmb + "\n" + sText;
                                        }

                                        iIntControlType = TEXTAREA_CTRL;
                                        // Vaibhav code ends

                                        iControlType = TEXT_CTRL;
                                        break;
                                    case "memo": //MGaba2:MITS 18534:Comments were not coming
                                    //case "textarea"://Added by MGaba2:Start
                                        sText = objControl.InnerText;
                                        if (sTextCmb != "")
                                            sText = sTextCmb + " " + sText + "\n";
                                        else
                                        {
                                            sCols[iCounter++] = sText + "\n";
                                          sText ="";
                                        }
                                        iIntControlType = TEXTAREA_CTRL;
                                        sTextCmb = "";
                                        break;//Added by MGaba2:End
                                    case "singlerow":
                                        foreach (XmlNode objCtrlRow in objControl.SelectNodes("./controlrow"))
                                        {
                                            sSRCols = new string[2];
                                            sText = "";
                                            foreach (XmlNode objIntCtrl in objCtrlRow.SelectNodes("./control"))
                                            {
                                                switch (objIntCtrl.Attributes["type"].InnerText)
                                                {
                                                    case "messagewithoutbreak":
                                                        sTextCmb = objIntCtrl.Attributes["title"].Value + ": ";
                                                        break;
                                                    case "textarea":
                                                        sText = objIntCtrl.InnerText;
                                                        if (sTextCmb != "")
                                                            sText = sTextCmb + " " + sText + "\n";
                                                        else
                                                            sText = sText + "\n";
                                                        iIntControlType = TEXTAREA_CTRL;
                                                        sTextCmb = "";
                                                        break;
                                                    case "currency":
                                                    case "labelonly":
                                                    case "code":
                                                    case "text":
                                                        iGridCol = 0;
                                                        if (objIntCtrl.Attributes["title"] != null)
                                                        {
                                                            if (objIntCtrl.Attributes["title"].InnerText != "")
                                                                sText += objIntCtrl.Attributes["title"].InnerText + " ";
                                                        }

                                                        if (objIntCtrl.InnerText != "")
                                                        {
                                                            if (sText != "")
                                                            {
                                                                sText += ": ";
                                                                sText += objIntCtrl.InnerText + " ";
                                                            }
                                                            else
                                                            {
                                                                sText += objIntCtrl.InnerText + " ";
                                                            }
                                                        }

                                                        if (objIntCtrl.Attributes["type"].InnerText == "labelonly")
                                                        {
                                                            if (objIntCtrl.Attributes["name"].InnerText.StartsWith("BlankTitleAmt") == true)
                                                            {
                                                                sGrid[iGridCounter, iCounter] = sText;
                                                                iCounter += 1;
                                                                iControlType = 0;
                                                                sText = "";
                                                            }
                                                            else if (objIntCtrl.Attributes["name"].InnerText.StartsWith("BlankTitle") == true)
                                                            {
                                                                sTextCmbLbl = sText;
                                                                iControlType = TEXT_CTRL;
                                                                sText = "";
                                                            }
                                                            else if (objIntCtrl.Attributes["name"].InnerText.StartsWith("Blanklbl") == true)
                                                            {
                                                                //do nothing
                                                                sText = "";
                                                            }
                                                        }
                                                        else if (objIntCtrl.Attributes["type"].InnerText == "currency")
                                                        {
                                                            if (objIntCtrl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                                            {
                                                                iRowCount += 1;
                                                                sGrid[iGridCounter, iCounter] = sText;
                                                                iCounter = 0;
                                                                iControlType = 0;
                                                                sText = "";
                                                                if (iGridCounter == sGrid.GetLength(0) - 1)
                                                                    MakeGrid(p_objPrintWrapper, "", sGrid);
                                                            }
                                                            else
                                                            {
                                                                if (sTextCmbLbl != "")
                                                                    sText = sTextCmbLbl + ": " + sText;

                                                                sCols[iCounter++] = sText + "\n";
                                                                iControlType = TEXT_CTRL;
                                                                sText = "";
                                                            }
                                                        }
                                                        else if (objIntCtrl.Attributes["type"].InnerText == "code")
                                                        {
                                                            if (sTextCmbLbl != "")
                                                                sText = sTextCmbLbl + ": " + sText;

                                                            sCols[iCounter++] = sText;
                                                            iControlType = TEXT_CTRL;
                                                            sText = "";
                                                        }
                                                        else if (objIntCtrl.Attributes["type"].InnerText == "text")
                                                        {
                                                            if (objIntCtrl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "TxtAmt" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                                            {
                                                                sGrid[iGridCounter, iCounter] = sText;
                                                                iCounter += 1;
                                                                sText = " ";
                                                                sGrid[iGridCounter, iCounter] = sText;
                                                                iCounter += 1;
                                                                sGrid[iGridCounter, iCounter] = sText;
                                                                iCounter += 1;
                                                                sText = "";
                                                            }
                                                        }
                                                        break;
                                                    case "GridAndButtons":
                                                        MakeGrid(objCtrlRow.SelectSingleNode("./" + objIntCtrl.Attributes["target"].InnerText), p_objPrintWrapper, sTitle, objCtrlRow.SelectSingleNode("//control[@name='hdn" + objIntCtrl.Attributes["target"].Value + "']").InnerText);
                                                        iControlType = SINGLE_ROW;
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                            switch (iIntControlType)
                                            {
                                                case TEXT_CTRL:
                                                    MakeTwoCols(sCols, p_objPrintWrapper, sTitle);
                                                    break;
                                                case TEXTAREA_CTRL:
                                                    WriteTextArea(sText, p_objPrintWrapper, sTitle);
                                                    break;
                                                default:
                                                    break;
                                            }
                                            iIntControlType = 0;
                                        }
                                        iIntControlType = SINGLE_ROW;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (sGpName != "ClaimInfo")
                                iGridCounter += 1;

                            // Rendering the various controls.
                            switch (iControlType)
                            {
                                case TEXT_CTRL:
                                    //MakeTwoCols(sCols, p_objPrintWrapper, sTitle);
                                    MakeFourCols(sCols, p_objPrintWrapper, sTitle);
                                    break;
                                case MESSAGE_CTRL:
                                    WriteText(sText, false, p_objPrintWrapper, sTitle, true, 9);
                                    break;
                                case SINGLE_ROW:
                                    break;
                                case LINE_BREAK:
                                    if (sGpName != "ClaimInfo" && sGpName != "RESERVESUMMARY")//MGaba2:MITS 18534:Extra space was coming in Reserve Summary section
                                    {
                                        WriteText("", false, p_objPrintWrapper, sTitle, false, 9);
                                        iGridCounter -= 1;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            iControlType = 0;
                            //Vaibhav for MITS 15418
                            switch (iIntControlType)
                            {
                                case TEXT_CTRL:
                                    MakeTwoCols(sCols, p_objPrintWrapper, sTitle);
                                    break;
                                case TEXTAREA_CTRL:
                                    WriteTextArea(sText, p_objPrintWrapper, sTitle);
                                    break;
                                default:
                                    break;
                            }
                            iIntControlType = 0;
                            //Vaibhav code end

                        }
                        //if (sGpName == "ClaimInfo")
                        //    MakeGrid(p_objPrintWrapper, "", sGrid);
                    }
                    catch (Exception ex)
                    {//MGaba2:R6: Added exception
                        throw ex;
                    }
                    finally
                    {
                        sCols = null;
                        sGrid = null;

                    }

                }
      
        protected void PrintAllWorksheetsSafeWay(XmlNode p_objGroup, XmlDocument p_objRSWDoc, PrintWrapper p_objPrintWrapper, string p_sLOB, string p_sTabType)
        {
            //for each displaycolumn in a group
            string sTitle = string.Empty;
            string sText = string.Empty;
            string[] sCols = null;
            string[,] sGrid = null;
            int iGridCounter = 0;
            int iCounter = 0;
            int iControlType = 0;
            int iIntControlType = 0;
            string sGpName = string.Empty;
            string[] sSRCols = null;
            string sTextCmb = string.Empty;
            string sTextCmbLbl = string.Empty;
            string sLabel = string.Empty;
            bool bGridCreated = false;
            int iGridCol = 0;
            int iRowCount = 0;
            int iColumnCount = 0;

            try
            {
                sGpName = p_objGroup.Attributes["name"].InnerText;
                
                foreach (XmlNode objDisplayColumn in p_objGroup.SelectNodes(".//displaycolumn"))
                {
                    sCols = new string[10];
                    sText = "";
                    iCounter = 0;
                    iControlType = LINE_BREAK;

                    //for each 'control' element in a group
                    foreach (XmlNode objControl in objDisplayColumn.SelectNodes("./control"))
                    {
                        switch (objControl.Attributes["type"].Value)
                        {
                            //Not for linebreak, hidden fields
                            case "code":
                            case "currency":
                            case "labelonly":
                            case "text":
                                iGridCol = 0;
                                if (objControl.Attributes["title"] != null)
                                {
                                    if (objControl.Attributes["title"].InnerText != "")
                                        sText += objControl.Attributes["title"].InnerText + " ";
                                }

                                if (objControl.InnerText != "")
                                {
                                    if (sText != "")
                                    {
                                       // sText += ": ";
                                        sText += objControl.InnerText + " ";
                                    }
                                    else
                                    {
                                        sText += objControl.InnerText + " ";
                                    }
                                }

                                if (objControl.Attributes["type"].InnerText == "labelonly")
                                {

                                    if (objControl.Attributes["name"].InnerText.StartsWith("BlankTitleAmt") && (objControl.Attributes["name"].InnerText.EndsWith("GP")))
                                    {
                                       // if (sGpName != "ClaimInfo")
                                        //{
                                            sGrid[iGridCounter, iCounter] = sText;
                                            iCounter += 1;
                                            sText = "";
                                        
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("Apportion"))
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                    iCounter += 1;
                                      //  iCounter = 0;
                                        sText = "";
                                        
                                    }
                                  
                                    else if (objControl.Attributes["name"].InnerText.EndsWith("RW"))
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                                                             
                                        iCounter = 0;
                                        iRowCount += 1;
                                        iControlType = 0;
                                        sText = "";

                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("BlankTitle") == true)
                                    {
                                        sTextCmbLbl = sText;
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("BlanklblAmt") == true)
                                    {
                                        sText = " ";
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("LblBlank") == true)
                                    {
                                       // sText = " ";
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("lblRes7PerWC") == true)
                                    {
                                        // sText = " ";
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                  
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("Claimlbl") == true)
                                    {
                                        // sText = " ";
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("ClaimLbltxt") == true)
                                    {
                                        
                                            sGrid[iGridCounter, iCounter] = sText;
                                            iCounter = 0;
                                            iRowCount += 1;
                                            iControlType = 0;
                                            sText = "";
                                            if (iGridCounter == sGrid.GetLength(0) - 1)
                                                MakeGrid(p_objPrintWrapper, "", sGrid);
                                        
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("Blanklbl") == true)
                                    {
                                        //do nothing
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("pcName") == true)
                                    {
                                        if (sTextCmbLbl != "")
                                            sText = sTextCmbLbl + ": " + sText;

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }

                                    else if (objControl.Attributes["name"].InnerText.StartsWith("messagewithoutbreak1") == true)//Event Desc
                                    {
                                       
                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("messagewithoutbreak2") == true)//body parts
                                    {
                                        

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("claimnum") == true)
                                    {
                                       
                                        if (sTextCmbLbl != "")
                                            sText = sTextCmbLbl + ": " + sText;

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                        //if (objControl.Attributes["name"].InnerText == "claimnum")
                                        //{
                                        //    sTextCmbLbl = objControl.Attributes["name"].InnerText;
                                        //}
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("dtLoss") == true)
                                    {
                                        if (sTextCmbLbl != "")
                                            sText = sTextCmbLbl + ": " + sText;

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                    else
                                    {
                                        sCols[iCounter++] = sText ;
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                }
                                else if (objControl.Attributes["type"].InnerText == "currency")
                                {
                                    if (objControl.Attributes["name"].InnerText.StartsWith("PaidAmt") == true)
                                    {
                                        if (sTextCmbLbl != "")
                                            sText = sTextCmbLbl + ": " + sText;

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("Amt" + iRowCount + "Percent") && (!objControl.Attributes["name"].InnerText.StartsWith("Amt1PercentPerWC")))
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        iControlType = 0;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("BalAmt") == true)
                                    {
                                        if (sTextCmbLbl != "")
                                            sText = sTextCmbLbl + ": " + sText;

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("Amt1PercentPerWC") == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        iControlType = 0;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("AmtPercent") == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        iControlType = 0;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("IncurredAmt") == true)
                                    {
                                        if (sTextCmbLbl != "")
                                            sText = sTextCmbLbl + ": " + sText;

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("Amt1Apportion") == true)
                                    {
                                        string apptext = sGrid[1, 1];

                                        sGrid[1, 1] = apptext + "  " + " " + "                 " + sText;
                                        //    iCounter += 1;
                                        // iControlType = 0;
                                        sText = "";
                                    }

                                    else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + p_sTabType.Trim() + p_sLOB.Trim()) || objControl.Attributes["name"].InnerText.StartsWith("AmtPerWC") || (objControl.Attributes["name"].InnerText.StartsWith("AmtPerTWC9")||objControl.Attributes["name"].InnerText.StartsWith("txtAmt10PerWC")||objControl.Attributes["name"].InnerText.StartsWith("txtAmt11PerWC")) == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        ////                                        if (objControl.Attributes["name"].InnerText.Contains("_TransTypeIncurredAmount_OtherAdjustment") == true)
                                        ////                                        {
                                        ////                                            sGrid[iGridCounter, iCounter+1] = "";
                                        ////                                            sGrid[iGridCounter, iCounter + 2] = sText;
                                        ////                                            sGrid[iGridCounter, iCounter] = "";
                                        ////                                        }
                                        iCounter = 0;
                                        iRowCount += 1;
                                        iControlType = 0;
                                        sText = "";
                                        if (iGridCounter == sGrid.GetLength(0) - 1)
                                            MakeGrid(p_objPrintWrapper, "", sGrid);
                                       // iRowCount += 1;
                                       //// sGrid[iGridCounter, iCounter] = sText;
                                       // sGrid[iGridCounter, iCounter] = sText;
                                       // iCounter = 0;
                                       // iControlType = 0;
                                       // sText = "";
                                       // if (iGridCounter == sGrid.GetLength(0) - 1)
                                       //     MakeGrid(p_objPrintWrapper, "", sGrid);
                                    }
                                   
                                    else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "Rate" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                    {
                                        if (objControl.Attributes["title"].InnerText.Contains("@") )
                                            objControl.Attributes["title"].InnerText="";
                                        sGrid[iGridCounter, iCounter] = (sText.IndexOf("@") > 0) ? sText.Replace("@", "") : sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("AmtRate") || objControl.Attributes["name"].InnerText.StartsWith("txtAmtRate") == true)
                                    {
                                        
                                        sGrid[iGridCounter, iCounter] =  sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                 

                                    else if (objControl.Attributes["name"].InnerText.StartsWith("txtClaim") == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        //   iControlType = 0;
                                        sText = "";
                                    }
                                 
                                    else if (objControl.Attributes["name"].InnerText.EndsWith("Rate")&& (!objControl.Attributes["name"].InnerText.StartsWith("txtClaim")) )
                                    //{//.IndexOf(':') > 0 ? sText.Substring(sText.IndexOf(':') + 1, sText.Length - sText.IndexOf(':') - 1) : " ";
                                    //   // sText.IndexOf(':') > 0 ? sText.Substring(0, sText.IndexOf(':') - 1) : 
                                    //    //////////iGridCounter += 1;
                                    //    //////////sGrid[iGridCounter, iGridCol] = sText;
                                    //    //////////iGridCol += 1;
                                    //    if (sTextCmbLbl != "")
                                    //        sText = sTextCmbLbl + sText;

                                    //    sCols[iCounter++] = sText;
                                    //    iControlType = TEXT_CTRL;
                                    //    sText = "";
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter = 0;
                                        iRowCount += 1;
                                        iControlType = 0;
                                        sText = "";
                                        if (iGridCounter == sGrid.GetLength(0) - 1)
                                            MakeGrid(p_objPrintWrapper, "", sGrid);
}
                                    else
                                    {
                                        if (sTextCmbLbl != "")
                                            sText = sTextCmbLbl + ": " + sText;

                                        sCols[iCounter++] = sText + "\n";
                                        iControlType = TEXT_CTRL;
                                        sText = "";
                                        sTextCmbLbl = "";
                                    }
                                }
                                else if (objControl.Attributes["type"].InnerText == "code")
                                {
                                    if (sTextCmbLbl != "")
                                        sText = sTextCmbLbl + ": " + sText;

                                    sCols[iCounter++] = sText;
                                    iControlType = TEXT_CTRL;
                                    sText = "";
                                }

                                else if (objControl.Attributes["type"].InnerText == "text")
                                {
                                    if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "TxtAmt" + p_sTabType.Trim() + p_sLOB.Trim()) || objControl.Attributes["name"].InnerText.StartsWith("AmtTxtAmt")|| objControl.Attributes["name"].InnerText.StartsWith("txtClaim")== true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "Weeks" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                                                           iCounter += 1;
                                                                     //   iControlType = 0;
                                                                             sText = "";
                                    }
                               
                                 
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("AmtWeeks") == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        //   iControlType = 0;
                                        sText = "";
                                    }
                                   
                                    else if (objControl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "BP" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("text") == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                    else if (objControl.Attributes["name"].InnerText.StartsWith("AmtBP") == true)
                                    {
                                        sGrid[iGridCounter, iCounter] = sText;
                                        iCounter += 1;
                                        sText = "";
                                    }
                                }
                                break;
                            case "headermessage":
                            case "headermessage1":
                            case "headermessage2":
                                iGridCounter = 0;
                                iRowCount = GetGridRowCount(p_objGroup, objControl.Attributes["type"].Value);
                                iColumnCount = GetGridColumnCount(p_objGroup, objControl.Attributes["type"].Value);
                                sGrid = new string[iRowCount + 1, iColumnCount];

                                for (int iCnt = 0; iCnt < iColumnCount; iCnt++)
                                {
                                    switch (iCnt)
                                    {
                                        case 0:
                                            if (sGpName == "ClaimInfo")
                                                sGrid[iGridCounter, iCnt] = string.Empty;
                                            else
                                                sGrid[iGridCounter, iCnt] = objControl.Attributes["head1"].InnerText;
                                            break;
                                        case 1:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head3"].InnerText;
                                            break;
                                        case 2:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head4"].InnerText;
                                            break;
                                        case 3:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head5"].InnerText;
                                            break;
                                    }
                                }
                                iRowCount = 1;
                                iColumnCount = 0;
                                iControlType = 0;
                                break;
                            case "headermessage4":
                                iColumnCount = GetGridColumnCount(p_objGroup, objControl.Attributes["type"].Value);

                                for (int iCnt = 0; iCnt < iColumnCount; iCnt++)
                                {
                                    switch (iCnt)
                                    {
                                        case 0:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head1"].InnerText;
                                            break;
                                        case 1:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head3"].InnerText;
                                            break;
                                        case 2:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head4"].InnerText;
                                            break;
                                        case 3:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head5"].InnerText;
                                            break;
                                        case 4:
                                            sGrid[iGridCounter, iCnt] = objControl.Attributes["head5"].InnerText;
                                            break;
                                    }
                                }
                                iColumnCount = 0;
                                iControlType = 0;
                                break;
                            case "memo": //MGaba2:MITS 18534:Comments were not coming
                           // case "textarea"://Added by MGaba2:Start
                                //commented shobhana 
                                sText = objControl.InnerText;
                                if (sTextCmbLbl != "")
                                    sText = sTextCmbLbl + ": " + sText;
                                else
                                {
                                    sCols[iCounter++] = sText + "\n";
                                    sText = "";
                                    //sText = "  " + " " + "                                                                                      " + sText + "";
                                }
                                iIntControlType = TEXTAREA_CTRL;
                                sTextCmbLbl = "";
                                break;
                                //sText = objControl.InnerText;
                                //if (sTextCmbLbl != "")
                                //    sText = sTextCmbLbl + ":" + sText + "\n";
                                //else
                                //    sText = sText + "\n";
                                //iIntControlType = TEXTAREA_CTRL;
                                //sTextCmbLbl = "";
                                //break;//Added by MGaba2:End
                            case "message":
                                
                                //To do : Change the type "message in Safeway xml to label only,so that it doesnt come here

                                //sText = objControl.Attributes["title"].InnerText + " ";
                                //iControlType = MESSAGE_CTRL;
                                string[] arrTransaction = null;
                              //  if(p_sTabType!="ClaimInfo")
                              //  {
                               if (bGridCreated == false)
                         {
                            
                                 foreach (XmlNode objMsgNode in objDisplayColumn.SelectNodes("./control[@type='message']"))
                                 {
                                  //   if (objControl.Attributes["name"].InnerText.EndsWith ("MSG")==false)
                                    // {
                                         iColumnCount++;
                                 }
                               //  }
                            // }
                                    XmlNode objHdnCtrlToEdit = p_objRSWDoc.SelectSingleNode(".//control[@name='hdnRowCount']");
                                    if (objHdnCtrlToEdit != null)
                                    {
                                        string[] sAllTrans = objHdnCtrlToEdit.InnerText.Split(',');
                                        string sTransOfThisRes = string.Empty;
                                        for (int i = 0; i < sAllTrans.Length; i++)
                                        {
                                            if (sAllTrans[i].EndsWith(p_sTabType + "GP") == true)
                                        
                                            {
                                                if (sTransOfThisRes == string.Empty)
                                                    sTransOfThisRes = sAllTrans[i];
                                                else
                                                    sTransOfThisRes += "," + sAllTrans[i];
                                            }
                                        }
                                        arrTransaction = sTransOfThisRes.Split(',');
                                        iRowCount = arrTransaction.Length;
                                    }
                                    if (iRowCount != 0 && iColumnCount != 0)
                                    {
                                       
                                          
                                        sGrid = new string[iRowCount + 1, iColumnCount];
                                        iGridCounter = 0;
                                       
                                        int iCount = 0;
                                        foreach (XmlNode objMsgNode in objDisplayColumn.SelectNodes("./control[@type='message']"))
                                        {
                                            if (objMsgNode.Attributes["title"] != null)
                                            {
                                              
                                                    sGrid[iGridCounter, iCount] = objMsgNode.Attributes["title"].Value + " ";

                                                   
                                          
                                                iCount++;
                                            }
                                        }
                                        //iGridCounter += 1;
                                       bGridCreated = true;
                                        iRowCount = 1;
                                        iColumnCount = 0;
                                        iControlType = 0;
                                 }
                              //  }
                        }

                                break;

                       
                            ////case "controlgroup":
                                //For Control Types: messagewithoutbreak and textarea
                                if (objControl.SelectSingleNode("./control[@type='messagewithoutbreak']") != null)
                                {
                                    if (objControl.SelectSingleNode("./control[@type='messagewithoutbreak']").Attributes["title"] != null)
                                    {
                                        sTextCmb = objControl.SelectSingleNode("./control[@type='messagewithoutbreak']").Attributes["title"].InnerText + ": ";
                                        break;
                                    }
                                }

                                if (objControl.SelectSingleNode("./control[@type='textarea']") != null)
                                    sText = objControl.SelectSingleNode("./control[@type='textarea']").InnerText;

                                //Vaibhav for MITS 15418:
                                if (sTextCmb != "")
                                {
                                    sText = sTextCmb + "\n" + sText;
                                }

                                iIntControlType = TEXTAREA_CTRL;
                                //Vaibhav code ends

                                iControlType = TEXT_CTRL;
                                break;
                            case "singlerow":
                                foreach (XmlNode objCtrlRow in objControl.SelectNodes("./controlrow"))
                                {
                                    sSRCols = new string[2];
                                    sText = "";
                                    foreach (XmlNode objIntCtrl in objCtrlRow.SelectNodes("./control"))
                                    {
                                        switch (objIntCtrl.Attributes["type"].InnerText)
                                        {
                                            case "messagewithoutbreak":
                                                sTextCmb = objIntCtrl.Attributes["title"].Value + ": ";
                                                break;
                                            case "textarea":
                                                sText = objIntCtrl.InnerText;
                                                if (sTextCmb != "")
                                                    sText = sTextCmb + " " + sText + "\n";
                                                else
                                                    sText = sText + "\n";
                                                iIntControlType = TEXTAREA_CTRL;
                                                sTextCmb = "";
                                                break;
                                            case "currency":
                                            case "labelonly":
                                            case "code":
                                            case "text":
                                                iGridCol = 0;
                                                if (objIntCtrl.Attributes["title"] != null)
                                                {
                                                    if (objIntCtrl.Attributes["title"].InnerText != "")
                                                        sText += objIntCtrl.Attributes["title"].InnerText + " ";
                                                }

                                                if (objIntCtrl.InnerText != "")
                                                {
                                                    if (sText != "")
                                                    {
                                                        sText += ": ";
                                                        sText += objIntCtrl.InnerText + " ";
                                                    }
                                                    else
                                                    {
                                                        sText += objIntCtrl.InnerText + " ";
                                                    }
                                                }

                                                if (objIntCtrl.Attributes["type"].InnerText == "labelonly")
                                                {
                                                    if (objIntCtrl.Attributes["name"].InnerText.StartsWith("BlankTitleAmt") == true)
                                                    {
                                                        sGrid[iGridCounter, iCounter] = sText;
                                                        iCounter += 1;
                                                      //  iControlType = 0;
                                                        sText = "";
                                                    }
                                                    else if (objIntCtrl.Attributes["name"].InnerText.StartsWith("BlankTitle") == true)
                                                    {
                                                        sTextCmbLbl = sText;
                                                        iControlType = TEXT_CTRL;
                                                        sText = "";
                                                    }
                                                    else if (objIntCtrl.Attributes["name"].InnerText.StartsWith("Blanklbl") == true)
                                                    {
                                                        //do nothing
                                                        sText = "";
                                                    }
                                                }
                                                else if (objIntCtrl.Attributes["type"].InnerText == "currency")
                                                {
                                                    if (objIntCtrl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                                    {
                                                        iRowCount += 1;
                                                        sGrid[iGridCounter, iCounter] = sText;
                                                        iCounter = 0;
                                                        iControlType = 0;
                                                        sText = "";
                                                        if (iGridCounter == sGrid.GetLength(0) - 1)
                                                            MakeGrid(p_objPrintWrapper, "", sGrid);
                                                    }
                                                    else
                                                    {
                                                        if (sTextCmbLbl != "")
                                                            sText = sTextCmbLbl + ": " + sText;

                                                        sCols[iCounter++] = sText + "\n";
                                                        iControlType = TEXT_CTRL;
                                                        sText = "";
                                                    }
                                                }
                                                else if (objIntCtrl.Attributes["type"].InnerText == "code")
                                                {
                                                    if (sTextCmbLbl != "")
                                                        sText = sTextCmbLbl + ": " + sText;

                                                    sCols[iCounter++] = sText;
                                                    iControlType = TEXT_CTRL;
                                                    sText = "";
                                                }
                                                else if (objIntCtrl.Attributes["type"].InnerText == "text")
                                                {
                                                    if (objIntCtrl.Attributes["name"].InnerText.EndsWith("Amt" + iRowCount + "TxtAmt" + p_sTabType.Trim() + p_sLOB.Trim()) == true)
                                                    {
                                                        sGrid[iGridCounter, iCounter] = sText;
                                                        iCounter += 1;
                                                        sText = " ";
                                                        sGrid[iGridCounter, iCounter] = sText;
                                                        iCounter += 1;
                                                        sGrid[iGridCounter, iCounter] = sText;
                                                        iCounter += 1;
                                                        sText = "";
                                                    }
                                                }
                                                break;
                                            case "GridAndButtons":
                                                MakeGrid(objCtrlRow.SelectSingleNode("./" + objIntCtrl.Attributes["target"].InnerText), p_objPrintWrapper, sTitle, objCtrlRow.SelectSingleNode("//control[@name='hdn" + objIntCtrl.Attributes["target"].Value + "']").InnerText);
                                                iControlType = SINGLE_ROW;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    switch (iIntControlType)
                                    {
                                        case TEXT_CTRL:
                                            MakeTwoCols(sCols, p_objPrintWrapper, sTitle);
                                            break;
                                        case TEXTAREA_CTRL:
                                            WriteTextArea(sText, p_objPrintWrapper, sTitle);
                                            break;
                                        default:
                                            break;
                                    }
                                    iIntControlType = 0;
                                }
                                iIntControlType = SINGLE_ROW;
                                break;
                            default:
                                break;
                        }
                    }

                  //  if (sGpName != "ClaimInfo")
                       iGridCounter += 1;

                    // Rendering the various controls.
                    switch (iControlType)
                    {
                        case TEXT_CTRL:
                            if (p_sTabType != "Tem" )
                              //  MakeFiveCols(sCols, p_objPrintWrapper, sTitle);
                            MakeFiveCols(sCols, p_objPrintWrapper, sTitle);
                            else
                                MakeFourCols(sCols, p_objPrintWrapper, sTitle);
                            break;
                        case MESSAGE_CTRL:
                            WriteText(sText, false, p_objPrintWrapper, sTitle, true, 9);
                            break;
                        case SINGLE_ROW:
                            break;
                        case LINE_BREAK:
                          //  sGpName != "ClaimInfo" &&
                            if (sGpName != "RESERVESUMMARYWC")
                            {
                                WriteText("", false, p_objPrintWrapper, sTitle, false, 9);
                                iGridCounter -= 1;
                            }
                            break;
                        default:
                            break;
                    }
                    iControlType = 0;
                    //Vaibhav for MITS 15418
                    switch (iIntControlType)
                    {
                        case TEXT_CTRL:
                            MakeTwoCols(sCols, p_objPrintWrapper, sTitle);
                            break;
                        case TEXTAREA_CTRL:
                            WriteTextArea(sText, p_objPrintWrapper, sTitle);
                            break;
                        default:
                            break;
                    }
                    iIntControlType = 0;
                    //Vaibhav code end

                }
                if (sGpName == "RESERVESUMMARYWC")
                {
                    CreateNewLine(p_objPrintWrapper, sTitle);
                    PrintResSummarySafeWay(p_objRSWDoc, p_objPrintWrapper);
               }
                //if (sGpName == "ClaimInfo")
                //    MakeGrid(p_objPrintWrapper, "", sGrid);
            }
            catch (Exception ex)
            {//MGaba2:R6: Added exception
                throw ex;
            }
            finally
            {
                sCols = null;
                sGrid = null;
            }
            

        }

        /// <summary>
        /// Writes a text area onto the report
        /// </summary>
        /// <param name="p_sCols">p_sCols[1] contains the text o be rendered</param>
        /// <param name="p_objPrintWrapper">PrintWrapper object</param>
        /// <param name="p_sTitle">Title of the page</param>
        private void WriteTextArea(string p_sCols, PrintWrapper p_objPrintWrapper, string p_sTitle)
        {
            int iCutTextPos = 0;
            string sTextToPrint = "";
            string sTextCurLine = "";
            string sText = string.Empty;
            string[] arrWords = null;

            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }
            p_objPrintWrapper.SetFont("Verdana", 9);
            p_objPrintWrapper.SetFontBold(false);

            p_sCols = p_sCols + "\n";
            while (p_sCols.IndexOf("\n") != -1)
            {
                sText = p_sCols.Substring(0, p_sCols.IndexOf("\n"));
                // if a line has more width
                arrWords = sText.Split(new char[] { ' ' });
                for (int i = 0; i < arrWords.Length; i++)
                {
                    sTextToPrint += arrWords[i] + " ";
                    if (p_objPrintWrapper.GetTextWidth(sTextToPrint) > (p_objPrintWrapper.PageWidth - 400))
                    {
                        sTextCurLine = sTextToPrint;
                        iCutTextPos = sTextToPrint.Trim().LastIndexOf(" ");
                        sTextToPrint = sText.Substring(0, iCutTextPos + 1);
                        sText = sText.Substring(iCutTextPos + 1);
                        p_objPrintWrapper.PrintTextNoCr(sTextToPrint);
                        CreateNewLine(p_objPrintWrapper, p_sTitle);
                        p_objPrintWrapper.SetFont("Verdana", 9);
                        sTextToPrint = sTextCurLine.Substring(iCutTextPos + 1, sTextCurLine.Length - iCutTextPos - 1);
                    }
                }
                if (sText != "")
                {
                    p_objPrintWrapper.PrintTextNoCr(sText);
                    CreateNewLine(p_objPrintWrapper, p_sTitle);
                    p_objPrintWrapper.SetFont("Verdana", 9);
                    sTextToPrint = "";
                    sText = "";
                }
                p_sCols = p_sCols.Substring(p_sCols.IndexOf("\n") + 1);
            }
            p_objPrintWrapper.CurrentX = 100;
            CreateNewLine(p_objPrintWrapper, p_sTitle);
            p_objPrintWrapper.SetFont("Verdana", 9);
        }

        private int GetGridRowCount(XmlNode p_objGroup, string sCtrlName)
        {
            int iRowCount = 0;
            Boolean bStart = false;
            string sGpName = string.Empty;
            int iHeaderMsgRowCount = 0;

            try
            {
                sGpName = p_objGroup.Attributes["name"].InnerText;

                foreach (XmlNode objDisplayColumn in p_objGroup.SelectNodes(".//displaycolumn"))
                {
                    foreach (XmlNode objControl in objDisplayColumn.SelectNodes("./control"))
                    {
                        if (objControl.Attributes["type"].Value == "linebreak")
                            if (iRowCount > 0)
                                if (sGpName == "ClaimInfo")
                                    iRowCount -= 1;
                                else
                                    bStart = false;

                        if (objControl.Attributes["type"].Value == "singlerow")
                        {
                            foreach (XmlNode objCtrlRow in objControl.SelectNodes("./controlrow"))
                            {
                                foreach (XmlNode objIntCtrl in objCtrlRow.SelectNodes("./control"))
                                {
                                    if (bStart)
                                        iRowCount += 1;
                                }
                            }
                        }

                        if (bStart)
                            iRowCount += 1;

                        if (objControl.Attributes["type"].Value == sCtrlName)
                            bStart = true;

                        if (objControl.Attributes["type"].Value != sCtrlName && bStart == false)
                            if (objControl.Attributes["type"].InnerText.StartsWith("headermessage") == true)
                            {
                                iHeaderMsgRowCount = 1;
                                bStart = true;
                            }
                    }
                }
                if (sGpName != "ClaimInfo")
                    iRowCount = (iRowCount / GetGridColumnCount(p_objGroup, sCtrlName)) + iHeaderMsgRowCount;
            }
            finally
            {

            }
            return iRowCount;
        }

        private int GetGridColumnCount(XmlNode p_objGroup, string sCtrlName)
        {
            int iColCount = 0;
            Boolean bStart = false;

            try
            {
                foreach (XmlNode objDisplayColumn in p_objGroup.SelectNodes(".//displaycolumn"))
                {
                    foreach (XmlNode objControl in objDisplayColumn.SelectNodes("./control"))
                    {
                        if (bStart)
                            iColCount += 1;

                        if (objControl.Attributes["type"].Value == sCtrlName)
                            bStart = true;
                    }
                    if (iColCount > 0)
                        bStart = false;
                }
            }
            finally
            {

            }
            return iColCount;
        }

        /// <summary>
        /// Renders two columns on the report, depending upon the array passed.
        /// </summary>
        /// <param name="p_sCols">array containing two entries: first and second column</param>
        /// <param name="p_objPrintWrapper">PrintWrapper object</param>
        /// <param name="p_sTitle">Title of the page</param>
        private void MakeTwoCols(string[] p_sCols, PrintWrapper p_objPrintWrapper, string p_sTitle)
        {
            // If page limit has been exceeded, then create a new page.
            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }

            p_objPrintWrapper.SetFont(9);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[0]); // Printing First Column

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 2 - 100);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[1]); // Printing Second Column

            p_objPrintWrapper.CurrentX = 100;
            CreateNewLine(p_objPrintWrapper, p_sTitle);
        }

        /// <summary>
        /// Renders three columns on the report, depending upon the array passed.
        /// </summary>
        /// <param name="p_sCols">array containing two entries: first and second column</param>
        /// <param name="p_objPrintWrapper">PrintWrapper object</param>
        /// <param name="p_sTitle">Title of the page</param>
        private void MakeThreeCols(string[] p_sCols, PrintWrapper p_objPrintWrapper, string p_sTitle)
        {
            // If page limit has been exceeded, then create a new page.
            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }

            p_objPrintWrapper.SetFont(9);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[0]); // Printing First Column

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 3 - 100);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[1]); // Printing Second Column

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 3 - 100);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[2]); // Printing Third Column

            p_objPrintWrapper.CurrentX = 100;
            CreateNewLine(p_objPrintWrapper, p_sTitle);
        }

        /// <summary>
        /// Renders four columns on the report, depending upon the array passed.
        /// </summary>
        /// <param name="p_sCols">array containing two entries: first and second column</param>
        /// <param name="p_objPrintWrapper">PrintWrapper object</param>
        /// <param name="p_sTitle">Title of the page</param>
        private void MakeFourCols(string[] p_sCols, PrintWrapper p_objPrintWrapper, string p_sTitle)
        {
            int iNoOfRows = 1;		//MGaba2:MITS 18534:Represent No of Rows to be skipped in case text is coming on multiline
            int iCount = 0; //MGaba2:MITS 18534:Represent No of Rows to be skipped by that column in case text is coming on multiline
            // If page limit has been exceeded, then create a new page.
            bool bCommentsMultiLine = false;          //added:Yukti,Testing purpose
            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }

            //Added:Yukti,DT:01/08/2012, MITS 30252
            if (string.Compare(p_sCols[0], ("Comments    \n"), true) == 0)
            {
                bCommentsMultiLine = true;
            }
            //Ended:Yukti,DT:01/08/2012, MITS 30252

            p_objPrintWrapper.SetFont(9);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[0]); // Printing First Column

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 4 - 100);
            //p_objPrintWrapper.PrintTextNoCr(p_sCols[1]); // Printing Second Column
            //Yukti, Dt:01/08/2014, MITS 30252
            //iNoOfRows = p_objPrintWrapper.PrintMultiLineText(p_sCols[1], p_objPrintWrapper.PageWidth / 4 - 100);
            iNoOfRows = p_objPrintWrapper.PrintMultiLineText(p_sCols[1], p_objPrintWrapper.PageWidth / 4 - 100, bCommentsMultiLine );
           
            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 4 - 100);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[2]); // Printing Third Column

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 4 - 100);
            //Yukti, Dt:01/08/2014, MITS 30252
            //iCount = p_objPrintWrapper.PrintMultiLineText(p_sCols[3], p_objPrintWrapper.PageWidth / 4 - 100);
            iCount = p_objPrintWrapper.PrintMultiLineText(p_sCols[3], p_objPrintWrapper.PageWidth / 4 - 100, bCommentsMultiLine);
		//MGaba2:MITS 18534: In case text is coming on multiline
            if (iNoOfRows < iCount)
                iNoOfRows = iCount;
            p_objPrintWrapper.CurrentX = 100;
				//MGaba2:MITS 18534: In case text is coming on multiline
            CreateNewLineAfterNRows(p_objPrintWrapper, p_sTitle,iNoOfRows);
        }
        //created by shobhana
        private void MakeFiveCols(string[] p_sCols, PrintWrapper p_objPrintWrapper, string p_sTitle)
        {
            int iNoOfRows = 1;		//MGaba2:MITS 18534:Represent No of Rows to be skipped in case text is coming on multiline
            int iCount = 0; //MGaba2:MITS 18534:Represent No of Rows to be skipped by that column in case text is coming on multiline
            // If page limit has been exceeded, then create a new page.
            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }

            p_objPrintWrapper.SetFont(9);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[0]); // Printing First Column

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 5 - 100);
            //p_objPrintWrapper.PrintTextNoCr(p_sCols[1]); // Printing Second Column
            //Commented :Yukti,DT:01/08/2014, MITS 30252, Send bCommentsMultiLine As False
            //iNoOfRows = p_objPrintWrapper.PrintMultiLineText(p_sCols[1], p_objPrintWrapper.PageWidth / 5 - 100);
            iNoOfRows = p_objPrintWrapper.PrintMultiLineText(p_sCols[1], p_objPrintWrapper.PageWidth / 5 - 100, false);

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 5 - 100);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[2]); // Printing Third Column

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 5 - 100);
            p_objPrintWrapper.PrintTextNoCr(p_sCols[3]);

            p_objPrintWrapper.CurrentX += (p_objPrintWrapper.PageWidth / 5 - 100);
            //iCount = p_objPrintWrapper.PrintMultiLineText(p_sCols[4], p_objPrintWrapper.PageWidth / 5 - 100);
            //Commented :Yukti,DT:01/08/2014, MITS 30252, Send bCommentsMultiLine As False
            //iCount = p_objPrintWrapper.PrintMultiLineText(p_sCols[4], p_objPrintWrapper.PageWidth / 5 - 100);
            iCount = p_objPrintWrapper.PrintMultiLineText(p_sCols[4], p_objPrintWrapper.PageWidth / 5 - 100, false);
            //MGaba2:MITS 18534: In case text is coming on multiline
            if (iNoOfRows < iCount)
                iNoOfRows = iCount;
            p_objPrintWrapper.CurrentX = 100;
            //MGaba2:MITS 18534: In case text is coming on multiline
            CreateNewLineAfterNRows(p_objPrintWrapper, p_sTitle, iNoOfRows);
        }


        /// <summary>
        /// Renders a Grid on the screen
        /// </summary>
        /// <param name="p_objNode">GridAndButtons Node</param>
        /// <param name="p_objPrintWrapper"></param>
        /// <param name="p_sTitle"></param>
        /// <param name="p_sGridList"></param>
        ////protected void MakeGrid(XmlNode p_objNode, PrintWrapper p_objPrintWrapper, string p_sTitle, String p_sGridList)
        ////{
        ////    int iColCount = 0;
        ////    int iRowCount = 0;
        ////    string sText = string.Empty;

        ////    double[] dColSize = null;

        ////    int icols = 0;
        ////    int irows = 0;
        ////    XmlNode objRow = null;

        ////    p_objPrintWrapper.SetFont(8);
        ////    p_objPrintWrapper.SetFontBold(true);

        ////    if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
        ////    {
        ////        CreateNewPage(false, p_sTitle, p_objPrintWrapper);
        ////        p_objPrintWrapper.CurrentX = 100;
        ////        p_objPrintWrapper.CurrentY = 600;
        ////    }

        ////    iColCount = p_objNode.SelectSingleNode("./listhead").ChildNodes.Count;
        ////    iRowCount = p_objNode.SelectNodes("./listrow").Count;

        ////    dColSize = new double[iColCount - 1];

        ////    // Find out each column's size
        ////    for (icols = 1; icols < iColCount; icols++)
        ////    {
        ////        sText = p_objNode.SelectSingleNode("./listhead").ChildNodes[icols].InnerText;
        ////        if (sText.Trim() == "")
        ////            sText = " ";
        ////        dColSize[icols - 1] = p_objPrintWrapper.GetTextWidth(sText);
        ////    }

        ////    for (irows = 1; irows < iRowCount + 1; irows++)
        ////    {
        ////        objRow = p_objNode.SelectNodes("./listrow")[irows - 1];
        ////        if (p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) != -1)
        ////            sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);
        ////        else
        ////            sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.Length - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);

        ////        //sText = sText.Replace(irows + "|", objRow.ChildNodes[1].InnerText + "|"); MITS 16999 : paggarwal2: 06.22.2009
        ////        sText = objRow.ChildNodes[1].InnerText + "|" + sText.Substring(sText.IndexOf(irows + "|") + 2, sText.Length - (sText.IndexOf(irows + "|") + 2));

        ////        var sTestArr = sText.Split('|');
        ////        for (int icnt = 0; icnt < sTestArr.Length; icnt++)
        ////        {
        ////            sText = sTestArr[icnt];
        ////            if (sText.Trim() != "")
        ////                if (dColSize[icnt] < p_objPrintWrapper.GetTextWidth(sText))
        ////                    dColSize[icnt] = p_objPrintWrapper.GetTextWidth(sText);
        ////        }
        ////    }

        ////    // Make the header

        ////    string[] h1 = new string[iColCount];
        ////    string[] h2 = new string[iColCount];
        ////    if (p_objNode.SelectSingleNode("./listhead/RowId") != null)
        ////    {
        ////        for (icols = 1; icols < iColCount; icols++)
        ////        {
        ////            sText = p_objNode.SelectSingleNode("./listhead").ChildNodes[icols].InnerText;
        ////            if (sText.Trim() == "")
        ////            {
        ////                h1[icols] = " ";
        ////                h2[icols] = " ";
        ////            }
        ////            else if (sText.Trim().IndexOf(" ") > 0)
        ////            {
        ////                h1[icols] = sText.Substring(0, sText.IndexOf(" ")).Trim();
        ////                h2[icols] = sText.Substring(sText.IndexOf(" ")).Trim();
        ////            }
        ////            else
        ////            {
        ////                h1[icols] = sText.Trim();
        ////                h2[icols] = " ";
        ////            }
        ////        }
        ////    }
        ////    else
        ////    {
        ////        for (icols = 1; icols < iColCount - 1; icols++)
        ////        {
        ////            sText = p_objNode.SelectSingleNode("./listhead").ChildNodes[icols].InnerText;
        ////            if (sText.Trim() == "")
        ////            {
        ////                h1[icols] = " ";
        ////                h2[icols] = " ";
        ////            }
        ////            else if (sText.Trim().IndexOf(" ") > 0)
        ////            {
        ////                h1[icols] = sText.Substring(0, sText.IndexOf(" ")).Trim();
        ////                h2[icols] = sText.Substring(sText.IndexOf(" ")).Trim();
        ////            }
        ////            else
        ////            {
        ////                h1[icols] = sText.Trim();
        ////                h2[icols] = " ";
        ////            }
        ////        }
        ////    }

        ////    for (icols = 1; icols < iColCount; icols++)
        ////    {
        ////        p_objPrintWrapper.PrintTextNoCr(h1[icols]);
        ////        p_objPrintWrapper.CurrentX += dColSize[icols - 1] + 10;
        ////    }
        ////    p_objPrintWrapper.CurrentX = 100;
        ////    CreateNewLine(p_objPrintWrapper, p_sTitle);

        ////    for (icols = 1; icols < iColCount; icols++)
        ////    {
        ////        p_objPrintWrapper.PrintTextNoCr(h2[icols]);
        ////        p_objPrintWrapper.CurrentX += dColSize[icols - 1] + 10;
        ////    }
        ////    p_objPrintWrapper.CurrentX = 100;
        ////    CreateNewLine(p_objPrintWrapper, p_sTitle);

        ////    // Make the rows
        ////    p_objPrintWrapper.SetFont(8);
        ////    p_objPrintWrapper.SetFontBold(false);

        ////    for (irows = 1; irows < iRowCount + 1; irows++)
        ////    {
        ////        objRow = p_objNode.SelectNodes("./listrow")[irows - 1];

        ////        if (p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) != -1)
        ////            sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);
        ////        else
        ////            sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.Length - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);

        ////        //sText = sText.Replace(irows + "|", objRow.ChildNodes[1].InnerText + "|"); MITS 16999 : paggarwal2: 06.22.2009
        ////        sText = objRow.ChildNodes[1].InnerText + "|" + sText.Substring(sText.IndexOf(irows + "|") + 2, sText.Length - (sText.IndexOf(irows + "|") + 2));

        ////        String[] sTestArr = sText.Split('|');
        ////        for (int icnt = 0; icnt < sTestArr.Length; icnt++)
        ////        {
        ////            sText = sTestArr[icnt];
        ////            if (sText.Trim() != "")
        ////            {
        ////                p_objPrintWrapper.PrintTextNoCr(sText);
        ////                p_objPrintWrapper.CurrentX += dColSize[icnt] + 10;
        ////            }
        ////        }

        ////        p_objPrintWrapper.CurrentX = 100;
        ////        CreateNewLine(p_objPrintWrapper, p_sTitle);
        ////        p_objPrintWrapper.SetFont(8);
        ////    }
        ////    CreateNewLine(p_objPrintWrapper, p_sTitle);
        ////}

        /// <summary>
        /// Renders a Grid on the screen
        /// </summary>
        /// <param name="p_objPrintWrapper"></param>
        /// <param name="p_sTitle"></param>
        /// <param name="p_sGrid"></param>
        protected void MakeGrid(PrintWrapper p_objPrintWrapper, string p_sTitle, String[,] p_sGrid)
        {
            int iColCount = 0;
            int iRowCount = 0;
            string sText = string.Empty;

            double[] dColSize = null;

            int cols = 0;
            int rows = 0;

            p_objPrintWrapper.SetFont(8);
            p_objPrintWrapper.SetFontBold(true);

            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }

            iRowCount = p_sGrid.GetLength(0);
            iColCount = p_sGrid.Length / iRowCount;

            dColSize = new double[iColCount];

            //Find out each column's size
            for (cols = 0; cols < iColCount; cols++)
            {
                sText = p_sGrid[0, cols];
                if (sText == null)
                    sText = "";
                if (sText.Trim() == "")
                    sText = " ";
                dColSize[cols] = p_objPrintWrapper.GetTextWidth(sText);
            }

            for (rows = 0; rows < iRowCount; rows++)
            {
                for (cols = 0; cols < iColCount; cols++)
                {
                    sText = p_sGrid[rows, cols];
                    if (sText == null)
                        sText = "";
                    if (sText.Trim() != "" )
                        if (dColSize[cols] < p_objPrintWrapper.GetTextWidth(sText))
                            dColSize[cols] = p_objPrintWrapper.GetTextWidth(sText);
                }
            }

            //Make the header
            for (cols = 0; cols < iColCount; cols++)
            {
                p_objPrintWrapper.PrintTextNoCr(p_sGrid[0, cols]);
                //p_objPrintWrapper.FillRect(0, 0, dColSize[cols], 300, Color.LightGray);
                p_objPrintWrapper.CurrentX += dColSize[cols] + 10;
            }
            p_objPrintWrapper.CurrentX = 100;
            CreateNewLine(p_objPrintWrapper, p_sTitle);

            //Make the rows
            p_objPrintWrapper.SetFont(8);
            p_objPrintWrapper.SetFontBold(false);
            for (rows = 1; rows < iRowCount; rows++)
            {
                for (cols = 0; cols < iColCount; cols++)
                {
                    sText = p_sGrid[rows, cols];
                    p_objPrintWrapper.PrintTextNoCr(sText);
                    p_objPrintWrapper.CurrentX += dColSize[cols] + 20;
                }
                p_objPrintWrapper.CurrentX = 100;
                CreateNewLine(p_objPrintWrapper, p_sTitle);
                p_objPrintWrapper.SetFont(8);
            }
            CreateNewLine(p_objPrintWrapper, p_sTitle);
        }
        protected void MakeGridSummary(PrintWrapper p_objPrintWrapper, string p_sTitle, String[,] p_sGrid)
        {
            int iColCount = 0;
            int iRowCount = 0;
            string sText = string.Empty;

            double[] dColSize = null;

            int cols = 0;
            int rows = 0;

            p_objPrintWrapper.SetFont(8);
            p_objPrintWrapper.SetFontBold(true);

            //if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            //{
             CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
        //    }

            iRowCount = p_sGrid.GetLength(0);
            iColCount = p_sGrid.Length / iRowCount;

            dColSize = new double[iColCount];

            //Find out each column's size
            for (cols = 0; cols < iColCount; cols++)
            {
                sText = p_sGrid[0, cols];
                if (sText == null)
                    sText = "";
                if (sText.Trim() == "")
                    sText = " ";
                dColSize[cols] = p_objPrintWrapper.GetTextWidth(sText);
            }

            for (rows = 0; rows < iRowCount; rows++)
            {
                for (cols = 0; cols < iColCount; cols++)
                {
                    sText = p_sGrid[rows, cols];
                    if (sText == null)
                        sText = "";
                    if (sText.Trim() != "")
                        if (dColSize[cols] < p_objPrintWrapper.GetTextWidth(sText))
                            dColSize[cols] = p_objPrintWrapper.GetTextWidth(sText);
                }
            }

            //Make the header
            for (cols = 0; cols < iColCount; cols++)
            {
                p_objPrintWrapper.PrintTextNoCr(p_sGrid[0, cols]);
                //p_objPrintWrapper.FillRect(0, 0, dColSize[cols], 300, Color.LightGray);
                p_objPrintWrapper.CurrentX += dColSize[cols] + 10;
            }
            p_objPrintWrapper.CurrentX = 100;
            CreateNewLine(p_objPrintWrapper, p_sTitle);

            //Make the rows
            p_objPrintWrapper.SetFont(8);
            p_objPrintWrapper.SetFontBold(false);
            for (rows = 1; rows < iRowCount; rows++)
            {
                for (cols = 0; cols < iColCount; cols++)
                {
                    sText = p_sGrid[rows, cols];
                    p_objPrintWrapper.PrintTextNoCr(sText);
                    p_objPrintWrapper.CurrentX += dColSize[cols] + 20;
                }
                p_objPrintWrapper.CurrentX = 100;
                CreateNewLine(p_objPrintWrapper, p_sTitle);
                p_objPrintWrapper.SetFont(8);
            }
            CreateNewLine(p_objPrintWrapper, p_sTitle);
        }
        private void MakeGrid(XmlNode p_objNode, PrintWrapper p_objPrintWrapper, string p_sTitle, String p_sGridList)
        {
            int iColCount = 0;
            int iRowCount = 0;
            string sText = string.Empty;

            double[] dColSize = null;

            int icols = 0;
            int irows = 0;
            XmlNode objRow = null;

            p_objPrintWrapper.SetFont(8);
            p_objPrintWrapper.SetFontBold(true);

            if (p_objPrintWrapper.CurrentY >= p_objPrintWrapper.PageLimit && p_objPrintWrapper.PageLimit != 0)
            {
                CreateNewPage(false, p_sTitle, p_objPrintWrapper);
                p_objPrintWrapper.CurrentX = 100;
                p_objPrintWrapper.CurrentY = 600;
            }

            iColCount = p_objNode.SelectSingleNode("./listhead").ChildNodes.Count;
            iRowCount = p_objNode.SelectNodes("./listrow").Count;

            dColSize = new double[iColCount - 1];

            // Find out each column's size
            for (icols = 1; icols < iColCount; icols++)
            {
                sText = p_objNode.SelectSingleNode("./listhead").ChildNodes[icols].InnerText;
                if (sText.Trim() == "")
                    sText = " ";
                dColSize[icols - 1] = p_objPrintWrapper.GetTextWidth(sText);
            }

            for (irows = 1; irows < iRowCount + 1; irows++)
            {
                objRow = p_objNode.SelectNodes("./listrow")[irows - 1];
                if (p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) != -1)
                    sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);
                else
                    sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.Length - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);

                //sText = sText.Replace(irows + "|", objRow.ChildNodes[1].InnerText + "|"); MITS 16999 : paggarwal2: 06.22.2009
                sText = objRow.ChildNodes[1].InnerText + "|" + sText.Substring(sText.IndexOf(irows + "|") + 2, sText.Length - (sText.IndexOf(irows + "|") + 2));

                var sTestArr = sText.Split('|');
                for (int icnt = 0; icnt < sTestArr.Length; icnt++)
                {
                    sText = sTestArr[icnt];
                    if (sText.Trim() != "")
                        if (dColSize[icnt] < p_objPrintWrapper.GetTextWidth(sText))
                            dColSize[icnt] = p_objPrintWrapper.GetTextWidth(sText);
                }
            }

            // Make the header

            string[] h1 = new string[iColCount];
            string[] h2 = new string[iColCount];
            if (p_objNode.SelectSingleNode("./listhead/RowId") != null)
            {
                for (icols = 1; icols < iColCount; icols++)
                {
                    sText = p_objNode.SelectSingleNode("./listhead").ChildNodes[icols].InnerText;
                    if (sText.Trim() == "")
                    {
                        h1[icols] = " ";
                        h2[icols] = " ";
                    }
                    else if (sText.Trim().IndexOf(" ") > 0)
                    {
                        h1[icols] = sText.Substring(0, sText.IndexOf(" ")).Trim();
                        h2[icols] = sText.Substring(sText.IndexOf(" ")).Trim();
                    }
                    else
                    {
                        h1[icols] = sText.Trim();
                        h2[icols] = " ";
                    }
                }
            }
            else
            {
                for (icols = 1; icols < iColCount - 1; icols++)
                {
                    sText = p_objNode.SelectSingleNode("./listhead").ChildNodes[icols].InnerText;
                    if (sText.Trim() == "")
                    {
                        h1[icols] = " ";
                        h2[icols] = " ";
                    }
                    else if (sText.Trim().IndexOf(" ") > 0)
                    {
                        h1[icols] = sText.Substring(0, sText.IndexOf(" ")).Trim();
                        h2[icols] = sText.Substring(sText.IndexOf(" ")).Trim();
                    }
                    else
                    {
                        h1[icols] = sText.Trim();
                        h2[icols] = " ";
                    }
                }
            }

            for (icols = 1; icols < iColCount; icols++)
            {
                p_objPrintWrapper.PrintTextNoCr(h1[icols]);
                p_objPrintWrapper.CurrentX += dColSize[icols - 1] + 10;
            }
            p_objPrintWrapper.CurrentX = 100;
            CreateNewLine(p_objPrintWrapper, p_sTitle);

            for (icols = 1; icols < iColCount; icols++)
            {
                p_objPrintWrapper.PrintTextNoCr(h2[icols]);
                p_objPrintWrapper.CurrentX += dColSize[icols - 1] + 10;
            }
            p_objPrintWrapper.CurrentX = 100;
            CreateNewLine(p_objPrintWrapper, p_sTitle);

            // Make the rows
            p_objPrintWrapper.SetFont(8);
            p_objPrintWrapper.SetFontBold(false);

            for (irows = 1; irows < iRowCount + 1; irows++)
            {
                objRow = p_objNode.SelectNodes("./listrow")[irows - 1];

                if (p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) != -1)
                    sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.IndexOf(';', p_sGridList.IndexOf(";" + irows + "|") + 1) - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);
                else
                    sText = p_sGridList.Substring(p_sGridList.IndexOf(";" + irows + "|", 0) + 1, p_sGridList.Length - p_sGridList.IndexOf(";" + irows + "|", 0) - 1);

                //sText = sText.Replace(irows + "|", objRow.ChildNodes[1].InnerText + "|"); MITS 16999 : paggarwal2: 06.22.2009
                sText = objRow.ChildNodes[1].InnerText + "|" + sText.Substring(sText.IndexOf(irows + "|") + 2, sText.Length - (sText.IndexOf(irows + "|") + 2));

                String[] sTestArr = sText.Split('|');
                for (int icnt = 0; icnt < sTestArr.Length; icnt++)
                {
                    sText = sTestArr[icnt];
                    if (sText.Trim() != "")
                    {
                        p_objPrintWrapper.PrintTextNoCr(sText);
                        p_objPrintWrapper.CurrentX += dColSize[icnt] + 10;
                    }
                }

                p_objPrintWrapper.CurrentX = 100;
                CreateNewLine(p_objPrintWrapper, p_sTitle);
                p_objPrintWrapper.SetFont(8);
            }
            CreateNewLine(p_objPrintWrapper, p_sTitle);
        }
        #endregion

        /// <summary>
        /// Get the Manager Id
        /// </summary>
        /// <param name="iRMId">User Id</param>
        /// <returns>Manager Id</returns>
        protected int GetManagerID(int iRMId)
        {
            int iManagerID = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.MANAGER_ID ";
                sSQL = sSQL + " FROM USER_DETAILS_TABLE,USER_TABLE";
                sSQL = sSQL + " WHERE USER_DETAILS_TABLE.DSNID = " + m_CommonInfo.DSNID;
                sSQL = sSQL + " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";
                sSQL = sSQL + " AND USER_TABLE.USER_ID = " + iRMId;
                objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iManagerID = Conversion.ConvertObjToInt(objReader.GetValue("MANAGER_ID"), m_iClientId);
                    }
                }
                objReader.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveWorksheet.GetManagerID.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return iManagerID;
        }

        /// <summary>
        ///rsushilaggar MITS 26332 Date 12/28/2011 
        /// </summary>
        /// <param name="p_iSubmToID"></param>
        /// <param name="p_iUserId"></param>
        /// <param name="p_iGroupId"></param>
        /// <returns></returns>
        protected bool CheckUserGroup(int p_iSubmToID, int p_iUserId, int p_iGroupId, int p_iRSWRowId)
        {
            bool bIsGroupfound = false;
            int iCurrentUserId = p_iSubmToID;
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iGroupID = 0;
            int iManagerID = 0;
            Dictionary<int, double> dictReserve = null;
            String sSqlReserve = string.Empty;
            List<ReserveLimit> lReserveLimits = null;
            StringBuilder sbSQLTemp = null;
            StringBuilder sbSQL = null;
            LocalCache objCache = null;
            bool blnSuccess = false;
            DbReader objRead = null;
            DbReader objTempRead = null;
            try
            {
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                if (iGroupID != 0)
                {
                    sSqlReserve = "SELECT RL.LINE_OF_BUS_CODE,RELATED_CODE_ID,MAX_AMOUNT FROM RESERVE_LIMITS RL INNER JOIN CODES C ON C.CODE_ID = RL.RESERVE_TYPE_CODE WHERE USER_ID = " + p_iUserId + " OR GROUP_ID =" + iGroupID;
                }
                else
                {
                    sSqlReserve = "SELECT RL.LINE_OF_BUS_CODE,RELATED_CODE_ID,MAX_AMOUNT FROM RESERVE_LIMITS RL INNER JOIN CODES C ON C.CODE_ID = RL.RESERVE_TYPE_CODE WHERE USER_ID = " + p_iUserId;
                }
                objRead = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSqlReserve);
                //dictReserveAmount = new Dictionary<int, double>();
                lReserveLimits = new List<ReserveLimit>();
                while (objRead.Read())
                {
                    ReserveLimit limit;
                    limit.iLOBID = Common.Conversion.ConvertObjToInt(objRead.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                    limit.iResType = Common.Conversion.ConvertObjToInt(objRead.GetValue("RELATED_CODE_ID"), m_iClientId);
                    limit.dMaxAmount = Common.Conversion.ConvertObjToDouble(objRead.GetValue("MAX_AMOUNT"), m_iClientId);
                    lReserveLimits.Add(limit);
                }
                CloseReader(ref objRead);

                while (!bIsGroupfound)
                {
                    if (iCurrentUserId != 0)
                    {
                        sSQL = "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID =" + iCurrentUserId;
                        objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sSQL);
                        if (objReader.Read())
                        {


                            iGroupID = objReader.GetInt32("GROUP_ID");
                            if (iGroupID == p_iGroupId)
                            {
                                sbSQL = new StringBuilder();
                                sbSQL.Append("SELECT RSW_ROW_ID,RSW.CLAIM_ID,C.LINE_OF_BUS_CODE,RSW.SUBMITTED_TO FROM RSW_WORKSHEETS RSW ");
                                sbSQL.Append(" INNER JOIN CLAIM C ON C.CLAIM_ID = RSW.CLAIM_ID ");
                                sbSQL.Append(" WHERE RSW.RSW_ROW_ID = " + p_iRSWRowId);
                                objRead = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());

                                if (objRead.Read())
                                {
                                    sbSQLTemp = new StringBuilder();
                                    dictReserve = new Dictionary<int, double>();
                                    sbSQLTemp.Append("SELECT CODE_ID FROM CODES WHERE TABLE_ID = ");
                                    sbSQLTemp.Append(objCache.GetTableId("MASTER_RESERVE"));
                                    sbSQLTemp.Append(" AND (DELETED_FLAG = 0 OR DELETED_FLAG IS NULL)");
                                    objTempRead = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQLTemp.ToString());
                                    while (objTempRead.Read())
                                    {
                                        dictReserve.Add(objTempRead.GetInt32(0), 0);
                                    }
                                    CloseReader(ref objTempRead);
                                    bool bRecordAdded = false;
                                    bool bTemp = false;
                                    GetRSWReserves(Conversion.CastToType<int>(objRead["CLAIM_ID"].ToString(), out blnSuccess), Conversion.CastToType<int>(objRead["RSW_ROW_ID"].ToString(), out blnSuccess), dictReserve);
                                    foreach (int iCodeId in dictReserve.Keys)
                                    {
                                        ReserveLimit temp = lReserveLimits.Find(c => c.iLOBID == Conversion.CastToType<int>(objRead["LINE_OF_BUS_CODE"].ToString(), out blnSuccess) && c.iResType == iCodeId);
                                        if (temp.dMaxAmount > 0)
                                        {
                                            if (temp.dMaxAmount >= dictReserve[iCodeId])
                                            {
                                                bIsGroupfound = true;
                                            }
                                            else
                                            {
                                                bIsGroupfound = false;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            bIsGroupfound = true;
                                        }
                                    }


                                    break;
                                }
                            }
                        }
                        objReader.Close();
                        sSQL = "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID =" + iCurrentUserId;
                        objReader = DbFactory.GetDbReader(m_CommonInfo.SecurityConnectionstring, sSQL);
                        if (objReader.Read())
                        {
                            iManagerID = objReader.GetInt32("MANAGER_ID");
                            if (iManagerID != 0)
                            {
                                iCurrentUserId = iManagerID;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }

                        objReader.Close();
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {

            }

            return bIsGroupfound;

        }

        /// Name		: CloseReader
        /// Author		: Vaibhav Mathur
        /// Date Created: 09/08/2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Closes a passed DbReader
        /// </summary>
        /// <param name="p_objReader">DbReader</param>
        protected void CloseReader(ref DbReader p_objReader)
        {
            if (p_objReader != null)
            {
                if (!p_objReader.IsClosed)
                {
                    p_objReader.Close();
                }
                p_objReader = null;
            }
        }

        /// <summary>
        /// This function will get the reserve categories for the LOB and Claim Type (if required).
        /// </summary>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iRSWID">Reserve Row ID</param>
        protected void GetRSWReserves(int p_iClaimID, int p_iRSWID, Dictionary<int, double> pdictReserve)
        {
            XmlDocument objXMLOut = new XmlDocument();
            DbReader objReader = null;
            LocalCache objCache = null;
            StringBuilder sbSQL;
            ReserveFunds.structReserves[] arrReserves = null;
            Dictionary<int, double> dictReserveAmount = null;
            int iLOBCode = 0;
            int iClmTypeCode = 0;
            int iRelatedCodeID = 0;
            double dAmount = 0.0;
            int iReserveTypeCode = 0;
            string sAmt = "";
            bool blnSuccess = false;
            try
            {
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_CommonInfo.Connectionstring,ClientId);
                dictReserveAmount = pdictReserve;

                #region Get the Line of Business for the claim
                sbSQL.Append("SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID);
                objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                if (objReader.Read())
                {
                    iLOBCode = objReader.GetInt32("LINE_OF_BUS_CODE");
                    iClmTypeCode = objReader.GetInt32("CLAIM_TYPE_CODE");
                }
                objReader.Dispose();
                sbSQL.Remove(0, sbSQL.Length);

                #endregion

                if (p_iRSWID != 0)
                {
                    sbSQL.Append("SELECT RSW_XML FROM RSW_WORKSHEETS WHERE RSW_ROW_ID = " + p_iRSWID);
                    objReader = DbFactory.GetDbReader(m_CommonInfo.Connectionstring, sbSQL.ToString());
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objReader.Read())
                        objXMLOut.LoadXml(objReader.GetString("RSW_XML"));
                    else
                        throw new RMAppException(Globalization.GetString("ReserveWorksheet.LoadMainData.RSWNotFound",m_iClientId));

                    objReader.Dispose();

                    arrReserves = GetResCategories(iLOBCode, iClmTypeCode);

                    string[,] sResFinal = null;
                    sResFinal = new string[arrReserves.GetLength(0), 4];

                    sResFinal = GetNewReserveAmts(iLOBCode, arrReserves, objXMLOut,0);//Needs to set Currency code here

                    for (int iCount = 0; iCount < sResFinal.GetLength(0); iCount++)
                    {
                        # region Getting Reserves
                        iReserveTypeCode = Conversion.CastToType<int>(sResFinal[iCount, 0], out blnSuccess);
                        sAmt = sResFinal[iCount, 3];
                        dAmount = Conversion.CastToType<double>(sAmt, out blnSuccess);
                        iRelatedCodeID = objCache.GetRelatedCodeId(iReserveTypeCode);
                        if (pdictReserve.ContainsKey(iRelatedCodeID))
                        {
                            if (dAmount > 0.0)
                            {
                                pdictReserve[iRelatedCodeID] += dAmount;
                            }
                        }
                        # endregion
                    }
                }
            }
            finally
            {
                objXMLOut = null;
                sbSQL = null;
                arrReserves = null;
                if (objCache != null)
                    objCache.Dispose();
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }
    }
}
