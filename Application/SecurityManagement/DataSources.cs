using System;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Collections;
using Riskmaster.Security;
using System.Data;
using System.Collections.Generic;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	///Author  :   Tanuj Narula
	///Dated   :   24th,June 2004
	///Purpose :   Riskmaster.Application.SecurityManagement.DataSources class is a Public class and is used for connecting,identifying 
	///            and setting various parameters for riskmaster DataSources.
	/// </summary>
	public class DataSources : RiskmasterDatabase
	{
		/// <summary>
		/// This is code access public key.
		/// </summary>
		private const string CODE_ACCESS_PUBLIC_KEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";

        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;

		/// <summary>
		/// Riskmaster.Application.SecurityManagement.DataSources is default constructor.
		/// </summary>
        public DataSources(int p_iClientId)
            : base(p_iClientId)
		{
			base.EnforceCheckSum = false;
            m_iClientId = p_iClientId;
		}
		/// <summary>
		/// Riskmaster.Application.SecurityManagement.DataSources is the constructor with parameter. Constructor call after the attribute 
		/// [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)].
		/// This constructor calls the function Load() and serves as a wrapper.
		/// First Code Access Security Usage.
		/// This attribute requires that any caller to have a strongname proving that it's CSC Riskmaster Code. 
		/// To ensure that your assembly has an appropriate strongname, place the following line in the 
		/// AssemblyInfo.cs file:
		///[assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")].Here the path of the .snk file has to be specified relative to your project.
		/// </summary>
		/// <param name="p_iDsnId">DsnId,corresponding to which object will be populated.</param>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public DataSources(int p_iDsnId,bool p_bFindDBType, int p_iClientId): base (p_iDsnId, p_bFindDBType, p_iClientId)
		{ 
			try
			{
				base.EnforceCheckSum = false;
				base.FindDBType=p_bFindDBType;
                m_iClientId = p_iClientId;
				this.Load(p_iDsnId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}

		}
        //gagnihotri MITS 11995 Changes made for Audit table Start
        private string m_sUserName = "";

        public DataSources(string p_sUserName, int p_iClientId): base(p_sUserName, p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
            base.EnforceCheckSum = false;
        }

        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLIC_KEY)]
        public DataSources(int p_iDsnId, bool p_bFindDBType, string p_sUserName, int p_iClientId): base(p_iDsnId, p_bFindDBType, p_sUserName, p_iClientId)
        {
            try
            {
                m_sUserName = p_sUserName;
                m_iClientId = p_iClientId;
                base.EnforceCheckSum = false;
                base.FindDBType = p_bFindDBType;
                this.Load(p_iDsnId);
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
            }

        }
        //gagnihotri MITS 11995 Changes made for Audit table End

//		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
//		public DataSources(int p_iDsnId)
//		{ 
//			try
//			{
//				this.Load(p_iDsnId);
//			}
//			catch(Exception p_objException)
//			{
//				throw p_objException;
//			}
//			finally
//			{
//			}
//
//		}
		/// <summary>
		///This function call loads the DataSource collection(m_arrlstDataSourcesCol) defined in PublicFunctions.cs. 
		///First Code Access Security Usage.
		///This attribute requires that any caller to have a strongname proving that it's CSC Riskmaster Code. 
		///To ensure that your assembly has an appropriate strongname, place the following line in the 
		///AssemblyInfo.cs file:
		///[assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")].Here the path of the .snk file has to be specified relative to your project.
		/// </summary>
		/// <returns></returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Load(ref ArrayList p_arrlstDatasourceCol)
		{
			bool bRetVal = false;			 
			
            using(DbReader objReader = DbFactory.ExecuteReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT * FROM DATA_SOURCE_TABLE ORDER BY DSN"))
	        {
        		//This loop will load all the persisted DataSources object to a collection in memory.
				while(objReader.Read())
				{
                    DataSources objDataSource = new DataSources(m_iClientId);
					objDataSource.DataSourceId=objReader.GetInt("DSNID");
					objDataSource.DataSourceName = objReader.GetString("DSN");
					objDataSource.RMUserId = PublicFunctions.Decrypt(objReader.GetString("RM_USERID"));
					objDataSource.RMPassword = PublicFunctions.Decrypt(objReader.GetString("RM_PASSWORD"));
					objDataSource.Status = Convert.ToBoolean(objReader.GetBoolean("STATUS"));
					objDataSource.DbType =(eDatabaseType)objReader.GetInt32("DBTYPE");
					objDataSource.GlobalDocPath = objReader.GetString("GLOBAL_DOC_PATH");
					objDataSource.ConnectionString = objReader.GetString("CONNECTION_STRING");
					objDataSource.NumLicenses =  objReader.GetInt("NUM_LICENSES");
					objDataSource.LicUpdDate = Conversion.ToDate(objReader.GetString("LIC_UPD_DATE"));
					objDataSource.CRC2 = objReader.GetString("CRC2");
					objDataSource.DocPathType = objReader.GetInt("DOC_PATH_TYPE");
					p_arrlstDatasourceCol.Add(objDataSource);
				}//while
				bRetVal=true; 
	        }//using
							
			return bRetVal;
		}

        //srajindersin MITS 30263 dt 11/27/2012
        /// <summary>
        ///This function call returns the Dataset of DataSource
        ///First Code Access Security Usage.
        ///This attribute requires that any caller to have a strongname proving that it's CSC Riskmaster Code. 
        ///To ensure that your assembly has an appropriate strongname, place the following line in the 
        ///AssemblyInfo.cs file:
        ///[assembly: AssemblyKeyFile("..\\..\\..\\Riskmaster.snk")].Here the path of the .snk file has to be specified relative to your project.
        /// </summary>
        /// <returns></returns>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = CODE_ACCESS_PUBLIC_KEY)]
        public DataSet Load()
        {
            DataSet dsDataSources = new DataSet();
            dsDataSources = DbFactory.ExecuteDataSet(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT DSNID,DSN FROM DATA_SOURCE_TABLE ORDER BY DSN", new Dictionary<string, string>(),m_iClientId);

            return dsDataSources;
        }
		/// <summary>
		/// This function call actually removes the DataSource from database.One can directly call this function by passing in the DataSource_Id to be deleted 
		/// or load this class with Datasource_Id and call its Remove() function to do the same job.
		/// </summary>
		/// <param name="p_iDataSourceId"></param>
		/// <returns>True if successful else throws error.</returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool Remove(int p_iDataSourceId)
		{  
			bool bRetVal=false;
			string sSQL="";
			DbConnection objConn = null; 
			DbTransaction objTrans=null;
			int iNoOfRowsAffected=int.Parse("0");
			try
			{
                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
				objConn.Open();
				DbCommand objCommand=objConn.CreateCommand();
				objTrans=objConn.BeginTransaction();
				objCommand.Transaction=objTrans;
				sSQL = "DELETE FROM DATA_SOURCE_TABLE WHERE DSNID=" + p_iDataSourceId;
				objCommand.CommandText=sSQL;
				iNoOfRowsAffected=objCommand.ExecuteNonQuery();
				sSQL = "DELETE FROM USER_DETAILS_TABLE WHERE DSNID=" + p_iDataSourceId;
				objCommand.CommandText=sSQL;
				iNoOfRowsAffected+=objCommand.ExecuteNonQuery();
				objTrans.Commit();
				bRetVal=true;
			}
			catch(Exception p_objException)
			{
				if(objTrans!=null)
					objTrans.Rollback();  
				throw new DataModelException(Globalization.GetString("DataSources.Remove.RemoveError",m_iClientId),p_objException);
			}
			finally
			{
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bRetVal;
		}
	}
}
