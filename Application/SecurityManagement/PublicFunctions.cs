using System;
using System.Xml;
using System.Collections;
using System.Text.RegularExpressions;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security.Encryption;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Text ;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	///Author  :   Tanuj Narula
	///Dated   :   26th,June 2004
	///Purpose :   This class contains the functions which are common across security framework.
	/// </summary>
	public  class PublicFunctions
	{
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public PublicFunctions()
		{
		}
		/// <summary>
		/// Collection for loading the Users.
		/// </summary>
		//public static ArrayList m_arrlstUserCol=null; 
		/// <summary>
		/// Collection for loading the DataSources.
		/// </summary>
		//public static ArrayList m_arrlstDataSourcesCol=null; 
		/// <summary>
		/// Collection for loading the Login information.
		/// </summary>
		//public static ArrayList m_arrlstLoginCol=null;
		/// <summary>
		/// This object will contain the instance of RMWinSecurity class.
		/// </summary>
		public static  RMWinSecurity m_Security=null; 
		/// <summary>
		/// Represents a collection that contains all the modules under a given module group.
		/// </summary>
		//public static ArrayList m_arrlstRMAllModules = null;
		/// <summary>
		/// Represents a collection that contains all the module groups.
		/// </summary>
		//public static ArrayList m_arrlstColGroups = null;
		/// <summary>
		/// This function flags whether the string is empty or not.
		/// </summary>
		/// <param name="p_sVal">Input string</param>
		/// <returns>True if input string is empty,false if it is not.</returns>
		public static bool IsEmptyStr(string p_sVal)
		{
			if(p_sVal=="")
			{
				return true;
			}
			else
			{
				return false;
			}
		}

        //Mridul. 12/02/09. MITS#18227.
        private static int m_iCtrEnhPolEnabled4LOBs;
        /// <summary>
        /// Counter to tell how many lines have enhanced policy enabled.
        /// Based on the value it is decided whether we need to show both Policy Tracking and Policy Management
        /// </summary>
        public static int iCtrEnhPolEnabled4LOBs
        {
            get{return m_iCtrEnhPolEnabled4LOBs;}
        }

        private static bool m_bIsALPolEnhEnabled;
        /// <summary>
        /// Returns true if Enhanced Policy is enabled for AL Policy LOB.
        /// </summary>
        public static bool bIsALPolEnhEnabled
        {
            get{return m_bIsALPolEnhEnabled;}
        }

        private static bool m_bIsGLPolEnhEnabled;
        /// <summary>
        /// Returns true if Enhanced Policy is enabled for GL Policy LOB.
        /// </summary>
        public static bool bIsGLPolEnhEnabled
        {
            get{return m_bIsGLPolEnhEnabled;}
        }

        private static bool m_bIsPCPolEnhEnabled;
        /// <summary>
        /// Returns true if Enhanced Policy is enabled for PC Policy LOB.
        /// </summary>
        public static bool bIsPCPolEnhEnabled
        {
            get { return m_bIsPCPolEnhEnabled; }
        }

        private static bool m_bIsWCPolEnhEnabled;
        /// <summary>
        /// Returns true if Enhanced Policy is enabled for WC Policy LOB.
        /// </summary>
        public static bool bIsWCPolEnhEnabled
        {
            get{return m_bIsWCPolEnhEnabled;}
        }

        public static string GetSessionDSN(int p_iClientId)
		{
            return RMConfigurationSettings.GetSessionDSN(p_iClientId);
		}
		/// <summary>
		/// This function retrieves the inner most exception. 
		/// </summary>
		/// <param name="p_objException">Occured Exception.</param>
		/// <param name="p_objRetException">Inner most exception.</param>
		public static void WorkerForException( Exception p_objException,out Exception p_objRetException)
		{
			try
			{ 
				if(p_objException.InnerException!=null)
				{
					WorkerForException(p_objException.InnerException,out p_objRetException);
				}
				else
				{
					p_objRetException=p_objException;
					return;
				}
				
			}
			catch(Exception p_objExcep)
			{
				throw p_objExcep;
			}
			
		}
		/// <summary>
		/// This function checks the time value and returns the appropriate valu.
		/// </summary>
		/// <param name="p_sTime">Input time.</param>
		/// <returns>Fomatted time</returns>
		public static string CheckAndSetDefaultVal(string p_sTime)
		{
			string sRetVal="";
			if(p_sTime=="")
			{
				sRetVal=FormatTimeToDbTime("12:00 AM");
			}
			else
			{
				sRetVal=FormatTimeToDbTime(p_sTime);
			}
			return sRetVal;
		}
		/// <summary>
		/// This function formats time to DB type time.
		/// </summary>
		/// <param name="p_sTimeString">String to be formatted.</param>
		/// <returns>Formatted time.</returns>
		public static string FormatTimeToDbTime(string p_sTimeString)
		{
			string sRetVal="";
			if(p_sTimeString==null || p_sTimeString=="")
			{
              
				return sRetVal;
			}
			if(p_sTimeString.IndexOf("AM")!=-1 || p_sTimeString.IndexOf("PM")!=-1)
			{
				DateTime dt=DateTime.Parse(p_sTimeString);
				sRetVal=dt.ToString("HHmmss");
				return sRetVal;
				
			}
			else
			{
				sRetVal=p_sTimeString.Substring(0,2)+p_sTimeString.Substring(3,2)+"00";
				return sRetVal;
			}

		}
		
		/// <summary>
		/// This call decrypts the input string.
		/// </summary>
		/// <param name="p_sVal">String to be decrypted.</param>
		/// <returns>Object containing decrypted value.</returns>
		public static string Decrypt(string p_sVal)
		{
			string sRetVal="";
			try
			{
				if(p_sVal!="")
				{
					sRetVal=RMCryptography.DecryptString(p_sVal);
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				
			}
			return sRetVal;
		}

		/// <summary>
		/// This call encrypts the input string.
		/// </summary>
		/// <param name="p_sVal">String to be encrypted.</param>
		/// <returns>Object containing encrypted string.</returns>
		public static string Encrypt(string p_sVal)
		{
            string sRetVal = string.Empty;			
			try
			{
                if (!string.IsNullOrEmpty(p_sVal))
				{
					sRetVal=RMCryptography.EncryptString(p_sVal);
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			return sRetVal;
		}
		/// <summary>
		/// This function call will convert one time format to another.
		/// </summary>
		/// <example>Input string time->2100.Output time string->21:00</example>
		/// <param name="p_sVal">Input time string.</param>
		/// <returns>Formatted time.</returns>
		public static string GetFromDbTime(string p_sVal)
		{
			string sRetVal="";
			try
			{
				if(p_sVal!="")
					sRetVal=p_sVal.Substring(0,2) + ":" +p_sVal.Substring(2,2);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			return sRetVal;
		}
		/// <summary>
		/// This function converts one time format to another format.
		/// </summary>
		/// <example>Input time string->13:00.Output time string->1:00 PM</example>
		/// <param name="p_sVal">String time whose format to be changed.</param>
		/// <returns>Formatted time</returns>

		public static string GetFormattedTime(string p_sVal)
		{

			if(p_sVal=="")
				return "";
			string sHours =""; 
			string sMins ="";
			try
			{
				string[] arrTimeDetails=p_sVal.Split(':');
				sHours=arrTimeDetails[0];
				sMins=arrTimeDetails[1];
				if((Convert.ToInt32(sHours))>12)
				{
					return (((Convert.ToInt32(sHours))-12) + ":" + sMins + " " + "PM");
				}
				else
				{
					if((Convert.ToInt32(sHours))==0)
						return "12:" + sMins + " " + "AM";
					else 
					{
						if((Convert.ToInt32(sHours))==12)
						{
							return p_sVal +" " + "PM";
						}
						else
						{ 
							return p_sVal +" " + "AM";

						}
					}

				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
		}
		/// <summary>
		/// This call will convert time from one format to another format.
		/// </summary>
		/// <param name="p_sVal">Input string time to be converted</param>
		/// <returns>String time in the required format.</returns>
	
		public static string GetTimeFromDbTime(string p_sVal)
		{
			string sRetVal="";
			try
			{
				if(p_sVal!="")
				
				{
					int sHour=Convert.ToInt32(p_sVal.Substring(0,2));
					int sMins=Convert.ToInt32(p_sVal.Substring(2,2));
					string stmpTimeStr=DateTime.Now.AddHours(sHour-DateTime.Now.Hour).AddMinutes(sMins-DateTime.Now.Minute).ToShortTimeString();
					sRetVal=stmpTimeStr.Substring(0,5); 
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			return sRetVal;
		}
		/// <summary>
		/// Determines whether the DSN is valid or not.
		/// </summary>
		/// <param name="p_sConnectStr">The connection string to be validated.</param>
		/// <param name="p_sCallMethod">The calling method for validating the DSN.</param>
		/// <returns>A string representing whether the connection string is valid or not.</returns>
		public static string IsDSNValid(string p_sConnectStr,string p_sCallMethod,ref string p_sMsgErr)
		{
			string sSQL = "", s = "";
			DbConnection objConn = DbFactory.GetDbConnection(p_sConnectStr);
			DbReader objReader = null;
			try
			{
				objConn.Open();
				if(p_sCallMethod != "SET_DOC_PATH")
				{
					sSQL = "SELECT RELEASE_NUMBER FROM SYS_PARMS";
					objReader = objConn.ExecuteReader(sSQL);
					if(!objReader.Read())
						s = "EOF";
					else
						s = objReader.GetString(0);
				}
				else
					s = "OK";  //-- Doc DB is ok if we can connect to it
				return s;
			}
			catch(Exception p_objException)
			{
				p_sMsgErr=p_objException.Message;
				return s;
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
					objConn.Dispose();
					//}
				}
			}
		}
		/// <summary>
		/// This function adds attributes onto buttons.
		/// </summary>
		/// <param name="p_objDoc">Doc containing button tags.</param>
		/// <param name="htAttributes">Hashtable containing attributes to be applied.</param>
		/// <param name="p_bExistingdsn">Flag for existing dsn.</param>
		public static void ProcessButtons(ref XmlDocument p_objDoc, ref Hashtable htAttributes,bool p_bExistingdsn)
		{
			XmlNodeList objButtonList;
            try
            {
                objButtonList = p_objDoc.SelectNodes("//buttonscriptDSNWizard");
                foreach (XmlElement objNode in objButtonList)
                {
                    foreach (DictionaryEntry stKeyVal in htAttributes)
                    {
                        objNode.SetAttribute((string)stKeyVal.Key, (string)stKeyVal.Value);
                    }
                }
                if (p_bExistingdsn)
                {
                    ((XmlElement)objButtonList.Item(1)).SetAttribute("title", "Finish");
                    ((XmlElement)objButtonList.Item(1)).SetAttribute("name", "btnFinish");
                    ((XmlElement)objButtonList.Item(1)).SetAttribute("action", "Finish");


                }

            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objButtonList = null;
            }
		}
		/// <summary>
		/// This function returns the most recently added entity. 
		/// </summary>
		/// <param name="p_sEntityType">Type of the entity to search.</param>
		/// <param name="p_sConnStr">Connection string for DB.</param>
		/// <returns>Id of the entity.</returns>
		public static int  FindRecentlyAddedEntity(string p_sEntityType,string p_sConnStr, int p_iClientId)
		{
			int iReturn = 0;;
			string sSQL = "";
			DbConnection objConn = null;
			DbReader objReader = null;
			try
			{
				switch (p_sEntityType)
				{
					case "User":
					{
						sSQL = "SELECT MAX(user_id) FROM USER_TABLE";
						break;
					}
					case "Datasource":
					{
						sSQL = "SELECT MAX(dsnid) FROM DATA_SOURCE_TABLE";
						break;
					}
					case "Module Security Group":
					{
						sSQL = "SELECT MAX(GROUP_ID) FROM USER_GROUPS";
						break;
					}
						
				}
				if(p_sConnStr=="")
					objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(p_iClientId));
				else
					objConn = DbFactory.GetDbConnection(p_sConnStr);

				objConn.Open();
				objReader = objConn.ExecuteReader(sSQL);
				
				if(objReader.Read())
				{
                    iReturn = Conversion.ConvertObjToInt(objReader.GetValue(0), p_iClientId);
				}
				
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("RMWinSecurity.CheckUsers.CheckUsersError", p_iClientId),p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
					objConn.Dispose();
					//}
				}
			}
			return iReturn;
		}
		/// <summary>
		/// This  function breaks the string into string array. 
		/// </summary>
		/// <param name="p_iIdDB">String containing comma seperated ids.</param>
		/// <returns>String array.</returns>
		public static string[] BreakDbId(string p_iIdDB)
		{
			string[] sIds=null;
			try
			{
				sIds=p_iIdDB.Split(',');
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return sIds;
		}
		/// <summary>
		/// This function gets the conn str for given Dsn id.
		/// </summary>
		/// <param name="p_iDsnId">Dsn for which conn str to be fetched.</param>
		/// <returns>Conn str.</returns>
		public static string GetConnStr(int p_iDsnId, int p_iClientId)
		{
			DataSources objDataSource;
			string sConnStr="";
			try
			{
				objDataSource=new DataSources(p_iDsnId,false, p_iClientId);
				sConnStr = objDataSource.ConnectionString;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objDataSource=null;
			}
			return sConnStr;
		}
		/// <summary>
		/// This function gets the time in AmPm time from db type time.
		/// </summary>
		/// <param name="p_sVal">Db time format string.</param>
		/// <returns>AmPm time. </returns>
		public static string GetPmAmTimeFromDbTimeFormat(string p_sVal)
		{
			string sHours =""; 
			string sMins ="";
			try
			{
				if(p_sVal=="" || p_sVal==null)
					return "";
				if(p_sVal.Length>=4)
				{
					p_sVal=p_sVal.Substring(0,2) + ":" +p_sVal.Substring(2,2);
				
					string[] arrTimeDetails=p_sVal.Split(':');
					sHours=arrTimeDetails[0];
					sMins=arrTimeDetails[1];
					if((Convert.ToInt32(sHours))>12)
					{
						return (((Convert.ToInt32(sHours))-12) + ":" + sMins + " " + "PM");
					}
					else
					{
						if((Convert.ToInt32(sHours))==0)
							return "12:" + sMins + " " + "AM";
						else 
						{
							if((Convert.ToInt32(sHours))==12)
							{
								return p_sVal +" " + "PM";
							}
							else
							{ 
								return p_sVal +" " + "AM";

							}
						}

					}
				}
			}
			catch
			{
				p_sVal=""; //all attempt failed,return blank string..
			}			
			return p_sVal;
		}
		/// <summary>
		/// This function returns the system date time from calendar date. 
		/// </summary>
		/// <param name="p_sCalDate">Calendar date.</param>
		/// <returns>System date time</returns>
		public static DateTime GetDateFrmCalDate(string p_sCalDate)
		{
			
			try
			{
				if(p_sCalDate==null || p_sCalDate=="")
					return DateTime.MinValue;
				return DateTime.Parse(p_sCalDate);
			}
			catch
			{
				return DateTime.MinValue;
			}
			
		}
		/// <summary>
		/// This function gets the conn str for for given dsn id.
		/// </summary>
		/// <param name="p_iDsnId">Dsn id.</param>
		/// <param name="p_objDatasource">outputs datasource object as well.</param>
		/// <returns>Conn str.</returns>
        public static string GetConnStr(int p_iDsnId, out DataSources p_objDatasource, int p_iClientId)
		{
			string sConnStr="";
			try
			{
				p_objDatasource=new DataSources(p_iDsnId,false, p_iClientId);
				sConnStr = p_objDatasource.ConnectionString;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return sConnStr;
		}
		public static void ConstructAllParametersXmlForExistingDsn(ref XmlDocument p_objDoc, int p_iClientId)
		{
			int iDsnId;
			string sServer="";
			string sDB="";
			string sDriver="";;
			DataSources objDs;
			XmlElement objTemp1;
			XmlElement objTemp2;
			string sAddParams="";
			try
			{
                PublicFunctions.m_Security = new RMWinSecurity(p_iClientId);
				iDsnId=Conversion.ConvertObjToInt(p_objDoc.SelectSingleNode("//control[@name='hdEditDsnId']").InnerText.Trim(), p_iClientId);
				PublicFunctions.m_Security.SetupData(iDsnId,ref sServer,ref sDB,ref sDriver,ref sAddParams,out objDs);
				objTemp1 = (XmlElement)p_objDoc.SelectSingleNode("//control[@name='optservers']");
				objTemp2=(XmlElement)p_objDoc.SelectSingleNode("//optionDB[@name='" + sDriver + "']");
                if (objTemp2 != null && objTemp1 != null) // MITS 10603: Adding this if statement to take care of RM world DSN-based data sources (in case of DSN-based records, objTemp is null); so we prevent exceptions to be thrown.
                {
                    objTemp1.Attributes["value"].Value = objTemp2.Attributes["value"].Value;
                }
				p_objDoc.SelectSingleNode("//control[@name='inputServerName']").InnerText=sServer;
				p_objDoc.SelectSingleNode("//control[@name='inputDatabaseName']").InnerText=sDB;
				if(sAddParams.Trim()!="")
				{
					p_objDoc.SelectSingleNode("//control[@name='chkAddParams']").InnerText="True";
					p_objDoc.SelectSingleNode("//control[@name='txtAreaAddParams']").Attributes["value"].Value=sAddParams;
				}
				p_objDoc.SelectSingleNode("//control[@name='inputLogin']").InnerText=objDs.RMUserId;
				p_objDoc.SelectSingleNode("//control[@name='inputPassword']").InnerText=objDs.RMPassword;
				p_objDoc.SelectSingleNode("//control[@name='inputPassword']").InnerText=objDs.RMPassword;
				XmlElement objoptionDb=p_objDoc.CreateElement("optionDB");
				objoptionDb.InnerText=objDs.DataSourceName;
				p_objDoc.SelectSingleNode("//control[@name='inputDatasourcename']").AppendChild(objoptionDb);
              
				
			}
				//catch(Exception p_objException)
				//{
				//	throw p_objException;
				//}
			finally
			{
				objDs=null;
                objTemp1 = null;
                objTemp2 = null;  
			}
			
		}
		/// <summary>
		/// This function logs the exception.
		/// </summary>
		/// <param name="p_objLog">Log item.</param>
        public static void LogError(ref LogItem p_objLog, int p_iClientId)
		{
			try
			{
                Log.Write(p_objLog, p_iClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}
		/// <summary>
		/// This function populates the a xmlelement object with values passed as parameters.
		/// </summary>
		/// <param name="p_objElement">Xmlelement object to populate</param>
		/// <param name="text">Text value</param>
		/// <param name="title">Title of the node</param>
		/// <param name="url">Url of the node</param>
		/// <param name="icon">Icon of the node</param>
		/// <param name="type">Type of the node</param>
		/// <param name="entityType">EntityType of the node</param>
		/// <param name="idDb">Database id of the node</param>
		public static void CreateElement(ref XmlElement p_objElement,string p_sText,
			string p_sTitle,string p_sUrl,string p_sIcon,
			string p_sType,string p_sEntityType,string p_sIdDb)
		{
			p_objElement.SetAttribute("text",p_sText);
			p_objElement.SetAttribute("title",p_sTitle);
			p_objElement.SetAttribute("id","");
			p_objElement.SetAttribute("url",p_sUrl);
			p_objElement.SetAttribute("icon",p_sIcon);
			p_objElement.SetAttribute("type",p_sType);
			p_objElement.SetAttribute("entityType",p_sEntityType);
			p_objElement.SetAttribute("idDb",p_sIdDb);
		}
		/// <summary>
		/// This function populates the a xmlelement object with values passed as parameters.
		/// </summary>
		/// <param name="p_objElement">Xmlelement object to populate</param>
		/// <param name="text">Text value</param>
		/// <param name="title">Title of the node</param>
		/// <param name="url">Url of the node</param>
		/// <param name="icon">Icon of the node</param>
		/// <param name="type">Type of the node</param>
		public static void CreateElement(ref XmlElement p_objElement,string p_sText,
			string p_sTitle,string p_sUrl,string p_sIcon,
			string p_sType)
		{
			p_objElement.SetAttribute("text",p_sText);
			p_objElement.SetAttribute("title",p_sTitle);
			p_objElement.SetAttribute("id","");
			p_objElement.SetAttribute("url",p_sUrl);
			p_objElement.SetAttribute("icon",p_sIcon);
			p_objElement.SetAttribute("type",p_sType);
			p_objElement.SetAttribute("expanded","");
			
		}
        public static void LoggerForWorkerThreads(Exception p_objException, int p_iClientId)
		{
			try
			{
				Exception objInnerMost;
				PublicFunctions.WorkerForException(p_objException,out objInnerMost);
				LogItem objEr=new LogItem();
                objEr.Message = Globalization.GetString("ADController.SaveUserToAD.SaveADErr", p_iClientId);
				objEr.RMExceptSource=p_objException.Source;
				objEr.Category = "CommonWebServiceLog";
				objEr.RMExceptStack=p_objException.StackTrace;
				objEr.RMParamList.Add("Actual Exception",objInnerMost.Message);
                Log.Write(objEr, p_iClientId);
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}

		}
		/// <summary>
		/// This function checks for user existence in USER_MEMBERSHIP table.
		/// </summary>
		/// <param name="p_iGroupID">Grp id.</param>
		/// <param name="p_iUserID">user id.</param>
		/// <param name="p_sConnectionString">Conn str.</param>
		/// <returns>True or false; depending upon existence.</returns>
		public static bool CheckUserMembership(ref int p_iGroupID, int p_iUserID,string p_sConnectionString, int p_iClientId)
		{
			bool bRetVal=false;
			DbConnection objConn = null;
			DbReader objReader = null;
			string sQuery="";
			try
			{
				sQuery="SELECT * FROM USER_MEMBERSHIP Where user_id= "+ p_iUserID;
					    
				objConn=DbFactory.GetDbConnection(p_sConnectionString);
				objConn.Open();
				objReader = objConn.ExecuteReader(sQuery);
				
				if(objReader.Read())
				{
					p_iGroupID=objReader.GetInt32(0);
					bRetVal=true;
				}
				
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("RMWinSecurity.CheckUsers.CheckUsersError", p_iClientId),p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
					objConn.Dispose();
					//}
				}
			}
			return bRetVal;
		}
		//public static int GetNextRecordId(string p_sTableName)
        private static int GetNextRecordId(string p_sTableName, int p_iClientId) //Ash - cloud
		{
			DbReader objReader = null;
			int iNextRecordId = 0 ;
			try
			{
                objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(p_iClientId), "Select Max(Record_Id) RecordId from " + p_sTableName);
				if(objReader.Read())
				{
					iNextRecordId = ( objReader.GetInt("RecordId")+1 ) ;
					objReader.Close() ;
                
				}
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close() ;
					objReader = null;
				}
			}
			return iNextRecordId ;
		}
		public static int GetUserIDForLoginName(string p_sLoginName, int p_iClientId)
		{
			DbReader objReader = null;
			int iRetVal = 0; ;
			try
			{
                objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(p_iClientId), "SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + p_sLoginName + "'");
				if( objReader.Read() )
				{
					iRetVal =  objReader.GetInt("USER_ID") ;
				}
               
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close() ;
					objReader = null;
				}
			}
			return iRetVal ;
		}
				  
		public static void UpdatePasswordHistory(int p_iUserId, int p_iDsnId, string p_sNewPassword, string p_sCreatedBy , bool p_bExistingPwdUpdate, int p_iClientId)
		{
			UserLogins objLogin = null ;
			DbConnection objConn = null;
			StringBuilder sbSql = null ;
			try
			{
				if(p_bExistingPwdUpdate)
				{
					try
					{
						objLogin = new UserLogins( false,p_iClientId );
						objLogin.Load( p_iUserId , p_iDsnId ) ;
						if( objLogin.Password  == p_sNewPassword )
						{
							return ;
						}
					}
					catch{}
				}
					
				sbSql = new StringBuilder() ;
				sbSql.Append("INSERT INTO PASSWORD_HISTORY VALUES (")	;
				sbSql.Append(PublicFunctions.GetNextRecordId("PASSWORD_HISTORY", p_iClientId).ToString()+ "," + p_iUserId.ToString() + ",'" + PublicFunctions.Encrypt( p_sNewPassword )+ "','"+ Conversion.ToDbDateTime( DateTime.Now) + "')")	;
                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(p_iClientId));
				objConn.Open();
				objConn.ExecuteScalar( sbSql.ToString() ) ;
				objConn.Close() ;
					
				
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Close() ;
					objConn = null ;
				}
				objLogin = null;
				sbSql=null;
			}
		}
		/// Name		: IsPwdPolEnabled
		/// Author		: Anurag Agarwal
		/// Date Created	: 11 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will check whether the password policy is enforced or not.
		/// It will read this from Config file.
		/// </summary>
		/// <returns></returns>
        public static bool IsPwdPolEnabled()
        {
            return Conversion.ConvertStrToBool(RMConfigurationManager.GetAppSetting("PasswordPolicy"));
        }

        public static bool IsSMDesignerEnabled(string p_sConn, int p_iClientId)
        {
            return Conversion.ConvertStrToBool(RMConfigurationManager.GetAppSetting("SMDesigner", p_sConn, p_iClientId));
        }

        //zmohammad : JIRA # RMA-2961, RMA-2965 Specific key added for SMreports
        public static bool IsSMReportEnabled()
        {
            return Conversion.ConvertStrToBool(RMConfigurationManager.GetAppSetting("SMReport"));
        }

        /// <summary>
        /// Function will check whether the History Tracking is installed or not
        /// </summary>
        /// <returns></returns>
        public static bool IsHistTrackEnabled(string p_sConn, int p_iClientId)
        {
            return Conversion.ConvertStrToBool(RMConfigurationManager.GetAppSetting("IsHistTrackInstalled",p_sConn,p_iClientId));
        }

        /// Name		: IsScriptEditorEnabled
        /// Author		: Nitesh Deedwania
        /// Date Created	: 06 Oct 2006		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check whether the Script Editor is enabled or not.
        /// </summary>
        /// <returns></returns>
        public static bool IsScriptEditorEnabled(string p_sConnectionString, int p_iClientId)
		{
			bool bRetVal=false;
			DbConnection objConn = null;
			DbReader objReader = null;
			string sQuery=string.Empty ;
			try
			{
				sQuery="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SCRPT_EDI_FLAG'";
					    
				objConn=DbFactory.GetDbConnection(p_sConnectionString);
				objConn.Open();
				objReader = objConn.ExecuteReader(sQuery);
				if(objReader.Read())
				{
					string sValue=objReader.GetString(0);
					bRetVal= Conversion.ConvertStrToBool(sValue);
						
				}
				
			}
			catch(Exception p_objException)
			{
				throw new DataModelException(Globalization.GetString("RMWinSecurity.IsScriptEditorEnabled.IsScriptEditorEnabledError",p_iClientId),p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
					objConn.Dispose();
					//}
				}
			}
			return bRetVal;
		}

        /// Name		: IsBillingSysEnabled
		/// Author		: Shruti Choudhary
		/// Date Created	: 03/20/2007		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will check whether the Billing System is enabled or not.
		/// </summary>
		/// <returns></returns>
		public static bool IsBillingSysEnabled(string p_sConnectionString, int p_iClientId)
		{
			bool bRetVal=false;
			DbConnection objConn = null;
			DbReader objReader = null;
			string sQuery=string.Empty ;
			try
			{
				sQuery="SELECT USE_BILLING_FLAG FROM SYS_PARMS";
					    
				objConn=DbFactory.GetDbConnection(p_sConnectionString);
				objConn.Open();
				objReader = objConn.ExecuteReader(sQuery);
				if(objReader.Read())
				{
                    bRetVal = Conversion.ConvertObjToBool(objReader.GetValue(0), p_iClientId);
				}
				
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("RMWinSecurity.IsBillingSysEnabled.IsBillingSysEnabledError", p_iClientId), p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
					objConn.Dispose();
					//}
				}
			}
			return bRetVal;
		}
        //----------------------MITS 8974-Start-------------------------
        /// Name		: IsEnhPolEnabled
        /// Author		: Divya
        /// Date Created	: 03/21/2007		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 11/23/2009     *   MITS#18227  *    Mridul
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check whether the Billing System is enabled or not.
        /// </summary>
        /// <returns></returns>
        public static bool IsEnhPolEnabled(string p_sConnectionString, int p_iClientId)
        {
            bool bRetVal = false;
            DbConnection objConn = null;
            DbReader objReader = null;
            string sQuery = string.Empty;
            bool blnSuccess = false;
            try
            {
                //Anu Tennyson now USE_ENH_POL_FLAG in SYS_PARMS_LOB for MITS#18227 : 11/23/2009
                //sQuery = "SELECT USE_ENH_POL_FLAG FROM SYS_PARMS"; 
                //objConn = DbFactory.GetDbConnection(p_sConnectionString);
                //objConn.Open();
                //objReader = objConn.ExecuteReader(sQuery);
                // Anu Tennyson For MITS 18227 : 11/23/2009 STARTS
                //if (objReader.Read())
                //{
                //bRetVal = Conversion.ConvertObjToBool(objReader.GetValue(0));
                //}
                sQuery = "SELECT COUNT(USE_ENH_POL_FLAG) FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE <> 844 AND USE_ENH_POL_FLAG = -1";
                using (objConn = DbFactory.GetDbConnection(p_sConnectionString))
                {
                    objConn.Open();

                    //Check if Enhanced Policy is enabled for any LOB
                    m_iCtrEnhPolEnabled4LOBs = Conversion.CastToType<int>(objConn.ExecuteScalar(sQuery).ToString(), out blnSuccess);
                    if (m_iCtrEnhPolEnabled4LOBs > 0)
                        bRetVal = true;
                    
                    //Check for which LOB enhanced policy is enabled.
                    using (objReader = objConn.ExecuteReader("SELECT LINE_OF_BUS_CODE, USE_ENH_POL_FLAG FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE <> 844"))
                    {
                        while (objReader.Read())
                        {
                            switch (objReader[0].ToString())
                            {
                                case "241":
                                    m_bIsGLPolEnhEnabled = (objReader[1].ToString().Equals("-1") ? true : false);
                                    break;
                                case "242":
                                    m_bIsALPolEnhEnabled = (objReader[1].ToString().Equals("-1") ? true : false);
                                    break;
                                case "243":
                                    m_bIsWCPolEnhEnabled = (objReader[1].ToString().Equals("-1") ? true : false);
                                    break;
                                case "845":
                                    m_bIsPCPolEnhEnabled = (objReader[1].ToString().Equals("-1") ? true : false);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }//using objReader
                }//using objConn
            }
            catch (Exception p_objException)
            {
                //Mridul. 12/04/09. MITS 18227. Corrected base issue. The string was copied from other function and was not even defined in Global.resx.
                //throw new DataModelException(Globalization.GetString("RMWinSecurity.IsBillingSysEnabled.IsBillingSysEnabledError"), p_objException);
                throw new DataModelException(Globalization.GetString("RMWinSecurity.IsEnhPolEnabled.IsEnhPolicySysEnabledError",p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                { 
                    objConn.Dispose();
                  
                }
            }
            return bRetVal;
        }

        //----------------------MITS 8974-End------------------------
        /// Name		: IsMCMEnabled
        /// Author		: Raman Bhatia
        /// Date Created	: 11/13/2008		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check whether the MCM System is enabled or not.
        /// </summary>
        /// <returns></returns>
        public static bool IsMCMEnabled(string p_sConnectionString, int p_iClientId)
        {
            bool bRetVal = false;
            DbConnection objConn = null;
            DbReader objReader = null;
            string sQuery = string.Empty;
            try
            {
                sQuery = "SELECT USE_ACROSOFT_INTERFACE FROM SYS_PARMS";

                objConn = DbFactory.GetDbConnection(p_sConnectionString);
                objConn.Open();
                objReader = objConn.ExecuteReader(sQuery);
                if (objReader.Read())
                {
                    bRetVal = Conversion.ConvertObjToBool(objReader.GetValue(0),p_iClientId);
                }

            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("RMWinSecurity.IsMCMEnabled.IsMCMEnabledError", p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    //if(objConn.State==System.Data.ConnectionState.Open)
                    //{
                    //	objConn.Close();
                    objConn.Dispose();
                    //}
                }
            }
            return bRetVal;
        }
		/// Name		: IsPasswordComplex
		/// Author		: Anurag Agarwal
		/// Date Created	: 10 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will check the complexity of Password passed. The rules of complexity checking will be
		/// 1) The Password length must be in between 8 to 20 chars
		/// 2) User can not choose previous passwords, The number is stored in Security database. 
		///		Table PASSWORD_POLICY_PARMS -> Field PREVIOUS_PWDS_TO_KEEP
		/// 3) The password should not contain user full name or account name
		/// 4) Should contain any of the three kind of literals out of four
		///		a) English Uppercase letters (A to Z)
		///		b) English Lowercase letters (a to z)
		///		c) Numerals (0 to 9)
		///		d) Non-alphabetic chars (such as !, $, #, %)
		/// </summary>
		/// <param name="p_iUserId"></param>
		/// <param name="p_sPassword"></param>
		/// <returns></returns>
		public static int IsPasswordComplex(string p_sPassword, int p_iUserId, string p_sLName, string p_sFName,int p_LoggedInUserId, int p_iClientId)
		{
			string sConnStr = "";
			string sLastName = "";
			string sFirstName = "";
			string sLoginName = "";
			string sQuery = "";
			string sPrevPassword = "";
			int iNoOfPreviousPass = 0;
			int iPassMinLength = 0;
			int iPassMaxLength = 0;
			int iRuleSelected = 0;
			int iUseUpperCase = 0;
			int iUseLowerCase = 0;
			int iUseNumerals = 0;
			int iUseSpecial = 0;
			int iUseLoginName = 0;
			DbConnection objConn = null;
			DbReader objReader = null;
            //rsharma220 MITS 33443 Start
            string sLangCode = p_iUserId == 0 ? (new UserLogin(p_LoggedInUserId, p_iClientId)).objUser.NlsCode.ToString() : (new UserLogin(p_iUserId, p_iClientId)).objUser.NlsCode.ToString();
			try
			{
                sConnStr = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(p_iClientId);
				objConn=DbFactory.GetDbConnection(sConnStr);
				objConn.Open();

				//Get Parms data from PARMS table
				sQuery = "SELECT * FROM PASSWORD_POLICY_PARMS";
				objReader = objConn.ExecuteReader(sQuery);
				if(objReader.Read())
				{
					iPassMinLength = Conversion.ConvertStrToInteger(objReader["PWD_LENGTH_MIN"].ToString());
					iPassMaxLength = Conversion.ConvertStrToInteger(objReader["PWD_LENGTH_MAX"].ToString());
					iNoOfPreviousPass = Conversion.ConvertStrToInteger(objReader["PREVIOUS_PWDS_TO_KEEP"].ToString());
					iUseLoginName = Conversion.ConvertStrToInteger(objReader["PWD_CHECK_NAME"].ToString());
					iRuleSelected = Conversion.ConvertStrToInteger(objReader["PWD_VALIDATE_OPTION"].ToString());
					if(iRuleSelected == 4)
					{
						iUseUpperCase = Conversion.ConvertStrToInteger(objReader["PWD_CHECK_UPPERCASE"].ToString());
						iUseLowerCase = Conversion.ConvertStrToInteger(objReader["PWD_CHECK_LOWERCASE"].ToString());
						iUseNumerals = Conversion.ConvertStrToInteger(objReader["PWD_CHECK_NUMERALS"].ToString());
						iUseSpecial = Conversion.ConvertStrToInteger(objReader["PWD_CHECK_SPECIALCHAR"].ToString());
					}
				}
				if(objReader != null)
					objReader.Close();
			
				//Check for Minimum & maximum password length
				if(p_sPassword.Length < iPassMinLength || p_sPassword.Length > iPassMaxLength)
                    throw new RMAppException(string.Format(Globalization.GetString("PublicFunctions.IsPasswordComplex.PasswordLength", p_iClientId), iPassMinLength, iPassMaxLength)); 

				//If option is checked for setting the name as password.
				if(iUseLoginName == 0)
				{
					//If User ID is not passed then program expects that Last Name & First Name would come as input.
					if(p_iUserId == 0)
					{
						sLastName = p_sLName;
						sFirstName = p_sFName;
						sLoginName = sFirstName + sLastName;
						if(sLoginName.Length > 8)
							sLoginName.Substring(0,8);  
					}
					else
					{
						sQuery="SELECT LAST_NAME, FIRST_NAME, LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE " + 
							"USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.USER_ID= "+ p_iUserId;

						objReader = objConn.ExecuteReader(sQuery);
						if(objReader.Read())
						{
							sLastName = objReader["LAST_NAME"].ToString();
							sFirstName = objReader["FIRST_NAME"].ToString();
							sLoginName = objReader["LOGIN_NAME"].ToString();
						}
						else
						{
							if(objReader != null)
								objReader.Close();	
							sQuery="SELECT LAST_NAME, FIRST_NAME FROM USER_TABLE WHERE " + 
								" USER_TABLE.USER_ID= "+ p_iUserId;
							objReader = objConn.ExecuteReader(sQuery);
							if(objReader.Read())
							{
								sLastName = objReader["LAST_NAME"].ToString();
								sFirstName = objReader["FIRST_NAME"].ToString();
								sLoginName = sFirstName + sLastName;
								if(sLoginName.Length > 8)
									sLoginName.Substring(0,8);
							}
						}

						if(objReader != null)
							objReader.Close();
					}
                    //MGaba2:MITS 12572:Error in case last/first name is blank-Added if condition
                    if (sLastName != "")
                    {
						if(p_sPassword.ToLower().IndexOf(sLastName.ToLower()) >= 0)
                            throw new RMAppException(String.Format(Globalization.GetString("PublicFunctions.IsPasswordComplex.CriteriaNotMet",p_iClientId), "name"));
                    }

                    //MGaba2:MITS 12572:Error in case last/first name is blank-Added if condition
                    if (sFirstName != "")
                    {
						if(p_sPassword.ToLower().IndexOf(sFirstName.ToLower()) >= 0)
                            throw new RMAppException(String.Format(Globalization.GetString("PublicFunctions.IsPasswordComplex.CriteriaNotMet", p_iClientId), "name"));
                    }
					//MGaba2:MITS 12572:Error in case last/first name is blank-Added if condition
                    if (sLoginName != "")
                    {
						if(p_sPassword.ToLower().IndexOf(sLoginName.ToLower()) >= 0)
                            throw new RMAppException(String.Format(Globalization.GetString("PublicFunctions.IsPasswordComplex.CriteriaNotMet",p_iClientId), "Login name"));
                    }
				}

				//UserID would be -1 only in the case when new user is created. At the time of new user
				//creation password complexity would be checked prior to inserting the details of user into
				//database. So -1 is passed from the UI.
				if(p_iUserId != -1)
				{
					sQuery = "SELECT PASSWORD FROM PASSWORD_HISTORY WHERE USER_ID = " + p_iUserId + " ORDER BY DTTM_CREATED DESC" ;
					objReader = objConn.ExecuteReader(sQuery);
					int iLoopCount = 1;
					while(objReader.Read() && iNoOfPreviousPass >= iLoopCount)
					{
						sPrevPassword = objReader["PASSWORD"].ToString();
						sPrevPassword = Decrypt(sPrevPassword); 
						if(sPrevPassword.ToLower() == p_sPassword.ToLower())
                            throw new RMAppException(String.Format(Globalization.GetString("PublicFunctions.IsPasswordComplex.CriteriaNotMet",p_iClientId), "last " + iNoOfPreviousPass + " Passwords"));
						iLoopCount++; 
					}
					if(objReader != null)
						objReader.Close();
				}
				
				if(objConn.State == System.Data.ConnectionState.Open)
					objConn.Close();

				//Below complexity is tested on the basis of password rule selected
				//A regular expression is selected based on the passowrd rule selected
				string sPattern = "";
                string sErrorMessage = Globalization.GetString("PublicFunctions.IsPasswordComplex.ErrorMsg", p_iClientId);

				switch (iRuleSelected) 
				{
					case 0:
						sPattern = @"((?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^A-Za-z0-9\s]))";
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdRule0Msg", p_iClientId);
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.CharacterGroups", p_iClientId);
						break;
					case 1:
						sPattern = @"((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9\s])(?=.*[a-z])|(?=.*[^A-Za-z0-9\s])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9\s]))";
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdRule1Msg",p_iClientId);
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.CharacterGroups", p_iClientId);
						break;
					case 2:
						sPattern = @"((?=.*\d)(?=.*[A-Z])|(?=.*\d)(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9\s])|(?=.*[A-Z])(?=.*[a-z])|(?=.*[A-Z])(?=.*[^A-Za-z0-9\s])|(?=.*[a-z])(?=.*[^A-Za-z0-9\s]))";
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdRule2Msg", p_iClientId);
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.CharacterGroups", p_iClientId);
						break;
					case 3:
						sPattern = @"((?=.*\d)|(?=.*[A-Z])|(?=.*[a-z])|(?=.*[^A-Za-z0-9\s]))";
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdRule3Msg", p_iClientId);
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.CharacterGroups", p_iClientId);
						break;
					case 4:
                        sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdRule4Msg", p_iClientId);
						if(iUseUpperCase == 1)
						{
							sPattern = @"(?=.*[A-Z])";
                            sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdLiteralGroup1", p_iClientId);
						}
						if(iUseLowerCase == 1)
						{
							sPattern += @"(?=.*[a-z])";
                            sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdLiteralGroup2", p_iClientId);
						}
						if(iUseNumerals == 1)
						{
							sPattern += @"(?=.*\d)";
                            sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdLiteralGroup3", p_iClientId);
						}
						if(iUseSpecial == 1)
						{
							sPattern += @"(?=.*[^A-Za-z0-9\s])";
                            sErrorMessage += Globalization.GetString("PublicFunctions.IsPasswordComplex.PwdLiteralGroup4", p_iClientId);
						}
						sPattern = "(" + sPattern + ")";
						
						break;
				}
                				
				Regex objRegex = new Regex(sPattern);
				if(!objRegex.IsMatch(p_sPassword))
					throw new RMAppException(sErrorMessage);

			}
			catch(RMAppException p_objRMException)
			{
				throw p_objRMException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PublicFunctions.IsPasswordComplex.Error", p_iClientId) + p_objException.Message);
			}
			finally
			{
				if(objConn != null)
					objConn = null;
				if(objReader != null)
					objReader = null;
			}
			return 1;
		}
        //rsharma220 MITS 33443 End
        ///Created by 
        ///To check what is the printer mode
        ///  //---------------------------------------------
        /// Name		: IsPrintCheckFileOptIsPrinterOnly
        /// Author		:Ashutosh
        /// Date Created	: 11/08/2011		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check whether the check outputr is PrinterOnly System is enabled or not.
        /// </summary>
        /// <returns></returns>
        public static bool IsPrintCheckFileOptIsPrinterOnly(string p_sConnectionString, int p_iClientId)
        {
            bool bRetVal = false;
            DbConnection objConn = null;
            DbReader objReader = null;
            string sQuery = string.Empty;
            try
            {
                sQuery = "sELECT CHECK_TO_FILE  FROM CHECK_OPTIONS ";

                objConn = DbFactory.GetDbConnection(p_sConnectionString);
                objConn.Open();
                objReader = objConn.ExecuteReader(sQuery);
                if (objReader.Read())
                {
                    if(objReader.GetValue(0).ToString() =="0")
                    {
                        bRetVal=true;
                    }
                }

            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("RMWinSecurity.IsPrintCheckFileOptIsPrinterOnly.Error",p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    //if(objConn.State==System.Data.ConnectionState.Open)
                    //{
                    //	objConn.Close();
                    objConn.Dispose();
                    //}
                }
            }
            return bRetVal;
        }
	}
}
