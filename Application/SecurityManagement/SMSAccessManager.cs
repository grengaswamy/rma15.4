using System;
using System.Collections.Generic;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using System.Text;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	/// Summary description for SMSAccessManager. Tanuj
	/// </summary>
	public class SMSAccessManager
	{
        SMSUser m_objSMSUser = null;
        private int m_iClientId = 0;

        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="intUserID">integer containing the user ID</param>
		public SMSAccessManager(int intUserID, int p_iClientId)
		{
            m_iClientId = p_iClientId;
            m_objSMSUser = new SMSUser(intUserID, m_iClientId);
		}//class constructor

		public void ValidateForAdminUser(string p_sLoginName , int p_iUserId)
		{
            string sLoginName = string.Empty;
			
            SMSUser objSMSUser = new SMSUser(p_iUserId,m_iClientId);               

           sLoginName = this.GetLoginNameForUserId ( p_iUserId ) ;

           objSMSUser.IsSMSAdminUser(false, sLoginName);
               	
		}
        
		public string GetLoginNameForUserId(int p_iUserId)
		{
			DbReader objReader = null ;
			string sLoginName = ""  ;
			try
			{
				
					objReader = DbFactory.GetDbReader( SecurityDatabase.GetSecurityDsn(m_iClientId) , String.Format("Select * from user_details_table where USER_ID = {0}", p_iUserId) ) ;

					if( objReader.Read() )
					{
						sLoginName =  objReader.GetString( "LOGIN_NAME" ) ;
						objReader.Close() ;
					}
					objReader.Close() ;
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return sLoginName ;
		}

		public void IsValidSMSUser( ref XmlDocument p_objDoc)
		{
			string sUserLogin = "";
            int iUserID = 0;    //MITS-10662 - Declared new variable
			DbReader objReader = null;            
			try
			{
				/*fetch user from session.*/
				sUserLogin = p_objDoc.SelectSingleNode("//Session/LoginName").InnerText ;
            
				if(sUserLogin.Trim() == "")
				{
					throw new RMAppException( Globalization.GetString( "Users.IsValidSMSUser.error", m_iClientId )) ;
					/*Not a valid user.*/
				}
			
				/*Connect to SMS user list and validate the identity.*/

                //MITS-10662 - Getting the userid from user_details_table
                objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), String.Format("Select * from USER_DETAILS_TABLE where LOGIN_NAME = '{0}'", sUserLogin.Replace("'", "''")));
                if (objReader.Read())
                {
                    iUserID = objReader.GetInt32("USER_ID");
                    objReader.Close();
                    
                    
                    if (!objReader.Read())
                    {
                        throw new PermissionViolationException();
                        //throw new RMAppException( String.Format (Globalization.GetString("Users.IsValidSMSUser.NoAccessError"), sUserLogin )) ;                     
                    }
                    else
                    {
                        p_objDoc.SelectSingleNode("//AuthorizationToken").InnerText = "1";//Authorized Access to SMS.
                    }
                }   //if (objReader.Read()) end
			}
			catch(Exception p_objErr)
			{
				throw p_objErr ;
			}
			finally
			{
				if( objReader!=null )
				{
					objReader.Close() ;
					objReader = null;
				}
			}
		}
	}


	public class SMSUser
	{
		private int m_iRecordId ;
		private int m_iUserId ;
		private string m_sRmLoginName ;
		private string m_sRmPwd ;
        private bool m_bIsAdminUser = false;  //Initialize value to false
        private const int SMS_USER_ENABLED = 1; //1 = Enabled, 0 = Disabled
        private const int SMS_USER_DISABLED = 0; //1 = Enabled, 0 = Disabled
           //skhare7 MITS:22374
        private const int UPS_ACCESS_ENABLED = -1; //-1 = Enabled, 0 = Disabled
        private const int UPS_ACCESS_DISABLED = 0; //-1 = Enabled, 0 = Disabled
      //End   skhare7 MITS:22374

        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;
		public SMSUser(int p_iUserId, int p_iClientId)
		{
            m_iUserId = p_iUserId;
            m_iClientId = p_iClientId;
		}
		public SMSUser(string p_sLoginName, int p_iClientId)
		{
			this.IsSMSAdminUser(true, p_sLoginName) ;
            m_iClientId = p_iClientId;
		}
		public int RecordId
		{
			get
			{
				return m_iRecordId ;
			}
			set
			{
				m_iRecordId = value;
			}
		}
		public int UserId
		{
			get
			{
				return m_iUserId ;
			}
			set
			{
				m_iUserId = value;
			}
		}
		public string RmLoginName
		{
			get
			{
				return m_sRmLoginName ;
			}
			set
			{
				m_sRmLoginName = value;
			}
		}
		public string RmPwd
		{
			get
			{
				return m_sRmPwd ;
			}
			set
			{
				m_sRmPwd = value;
			}
		}
		public bool IsAdminUser
		{
			set
			{
				m_bIsAdminUser = value ;
			}
			get
			{
				return m_bIsAdminUser ;
			}
		}
		public bool ValidAdminCredentials(string p_sPWDFrmUI)
		{

			 return m_sRmPwd==p_sPWDFrmUI ;
			
			
		}
		public void Save()
		{
            Dictionary<string, object> dictParams = new Dictionary<string,object>();
            string strSQL = string.Format("UPDATE USER_TABLE SET IS_SMS_USER = {0} WHERE USER_ID={1}", SMS_USER_ENABLED, "~USER_ID~");

            dictParams.Add("USER_ID", m_iUserId);

            int intRecordsAffected = DbFactory.ExecuteNonQuery(SecurityDatabase.GetSecurityDsn(m_iClientId), strSQL, dictParams);

		}//method: Save()
		public void Remove()
		{
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            string strSQL = string.Format("UPDATE USER_TABLE SET IS_SMS_USER = {0} WHERE USER_ID={1}", SMS_USER_DISABLED, "~USER_ID~");

            dictParams.Add("USER_ID", m_iUserId);

            int intRecordsAffected = DbFactory.ExecuteNonQuery(SecurityDatabase.GetSecurityDsn(m_iClientId), strSQL, dictParams);
		}//method: Remove()
        //skhare7 MITS:22374
        public void UserPrivSave()
        {
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            string strSQL = string.Format("UPDATE USER_TABLE SET IS_USRPVG_ACCESS = {0} WHERE USER_ID={1}", UPS_ACCESS_ENABLED, "~USER_ID~");

            dictParams.Add("USER_ID", m_iUserId);

            int intRecordsAffected = DbFactory.ExecuteNonQuery(SecurityDatabase.GetSecurityDsn(m_iClientId), strSQL, dictParams);

        }//method: UserPrivSave()
     
        public void UserPrivRemove()
        {
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            string strSQL = string.Format("UPDATE USER_TABLE SET IS_USRPVG_ACCESS = {0} WHERE USER_ID={1}", UPS_ACCESS_DISABLED, "~USER_ID~");

            dictParams.Add("USER_ID", m_iUserId);

            int intRecordsAffected = DbFactory.ExecuteNonQuery(SecurityDatabase.GetSecurityDsn(m_iClientId), strSQL, dictParams);
        }//method: UserPrivRemove()

        //End skhare7 MITS:22374
		public void ClearThisInstance()
		{
			this.m_iRecordId = 0;
			this.m_iUserId = 0 ;
			this.m_sRmLoginName = "" ;
		}

        /// <summary>
        /// Removes the specified user from the SMS Security Admins group
        /// </summary>
        /// <param name="p_iUserIdToBeDeleted">integer containing the user ID</param>
		public void DeleteSMSUser( int p_iUserIdToBeDeleted )
		{
            m_iUserId = p_iUserIdToBeDeleted;

            //Call the remove method to remove the user as an SMS Admin
            Remove();
		}//method: DeleteSMSUser()

        
        /// <summary>
        /// Determines whether the specified User ID has been granted access
        /// to Security Management system
        /// </summary>
        /// <param name="p_iUserID">integer containing User ID to verify SMS permissions</param>
        /// <returns>boolean indicating whether or not the user was granted access</returns>
        public bool IsSMSPermittedUser(int p_iUserID)
		{
            bool blnIsPermitted = false;
            
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, string> dictParams = new Dictionary<string, string>();


            strSQL.Append("SELECT ut.USER_ID ");
            strSQL.Append("FROM USER_TABLE ut ");
            strSQL.AppendFormat(string.Format("WHERE ut.USER_ID = {0} ", "~SMS_USERID~"));
            strSQL.AppendFormat(string.Format("AND ut.IS_SMS_USER = {0} ", "~IS_SMS_USER~"));

            dictParams.Add("SMS_USERID", p_iUserID.ToString());
            dictParams.Add("IS_SMS_USER", SMS_USER_ENABLED.ToString());

            object objUserId = DbFactory.ExecuteScalar(SecurityDatabase.GetSecurityDsn(m_iClientId), strSQL.ToString(), dictParams);
                
            if ((objUserId != null))
            {
                blnIsPermitted = true;
            } // if

            return blnIsPermitted;

        }//method: IsSMSPermittedUser()
        public bool IsSMSAndUPSPermittedUser(int p_iUserID, out bool blnIsSMSPermitted)
        {
            bool blnIsUPSPermitted = false;
            blnIsSMSPermitted = false;

            StringBuilder strSQL = new StringBuilder();
            //Dictionary<string, string> dictParams = new Dictionary<string, string>();
            DbReader objReader = null;

            //strSQL.Append("SELECT ut.USER_ID ");
            //strSQL.Append("FROM USER_TABLE ut ");
            //strSQL.AppendFormat(string.Format("WHERE ut.USER_ID = {0} ", "~SMS_USERID~"));
            //strSQL.AppendFormat(string.Format("AND ut.IS_USRPVG_ACCESS = {0} ", "~IS_USRPVG_ACCESS~"));
            using (objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), String.Format("SELECT ut.IS_USRPVG_ACCESS,ut.IS_SMS_USER FROM USER_TABLE ut where ut.USER_ID = {0}", p_iUserID.ToString())))
	            {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                           // if (objReader.GetString(0).Equals(UPS_ACCESS_ENABLED))
                           if (objReader.GetInt16(0).Equals(UPS_ACCESS_ENABLED))
                            {
                                blnIsUPSPermitted = true;
                            }
                           // if (objReader.GetString(1).Equals(SMS_USER_ENABLED))
                           if (objReader.GetInt16(1).Equals(SMS_USER_ENABLED))
                            {
                                blnIsSMSPermitted = true;
                            }
                        }
                    }
                }
            if (objReader != null)
            {
                objReader.Close();
                objReader.Dispose();
            }
            //dictParams.Add("SMS_USERID", p_iUserID.ToString());
            //dictParams.Add("IS_USRPVG_ACCESS", UPS_ACCESS_ENABLED.ToString());

            //object objUserId = DbFactory.ExecuteScalar(SecurityDatabase.GetSecurityDsn(0), strSQL.ToString(), dictParams);

            //if ((objUserId != null))
            //{
            //    blnIsUPSPermitted = true;
            //} // if

            return blnIsUPSPermitted;

        }//method: IsSMSPermittedUser()

        /// <summary>
        /// Determines if an existing user is already a member of the SMS 
        /// Security Administrators
        /// </summary>
        /// <param name="p_bAlreadyKnown"></param>
        /// <param name="p_sLoginName">string containing the user's login name</param>
        /// <returns>boolean indicating whether or not the user is a member of the SMS Administrators group</returns>
		public bool IsSMSAdminUser( bool p_bAlreadyKnown , string p_sLoginName)
		{
            //abansal23: MITS 15117 on 04/18/2009 STARTS
            DbConnection objConn = null;
            string m_sDBType = "";
            objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString();
            objConn.Close();
            //abansal23: MITS 15117 on 04/18/2009 ENDS
        
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            StringBuilder strSQLBldr = new StringBuilder();

            strSQLBldr.Append("SELECT IS_SMS_USER ");
            strSQLBldr.Append("FROM USER_TABLE ut ");
            strSQLBldr.Append("INNER JOIN USER_DETAILS_TABLE udt ");
            strSQLBldr.Append("ON ut.USER_ID = udt.USER_ID ");
            strSQLBldr.AppendFormat(string.Format("WHERE udt.LOGIN_NAME = {0}", "~LOGIN_NAME~"));

            dictParams.Add("LOGIN_NAME", p_sLoginName);

            object objResult = DbFactory.ExecuteScalar(SecurityDatabase.GetSecurityDsn(m_iClientId), strSQLBldr.ToString(), dictParams);

            //If the resultant value is not null
            if (objResult != null)
            {
                bool blnSuccess = false;
                bool IsSMSUser = false;
                int intIsSMSUser = 0;
                //abansal23: MITS 15117 on 04/18/2009 STARTS
                if (m_sDBType == Constants.DB_ORACLE)
                {
                    intIsSMSUser = Conversion.CastToType<int>(objResult.ToString(), out blnSuccess);
                }
                else
                {
                    //Verify that the bit value can be converted to a bool
                    IsSMSUser = Conversion.CastToType<bool>(objResult.ToString(), out blnSuccess);//abansal23: MITS 15117 on 04/16/2009
                }

                //Verify that the conversion succeeded
                if (blnSuccess)
                {
                    //Verify and enable the user as an SMS Security Administrator
                    if ((IsSMSUser) || (intIsSMSUser==1))//abansal23: MITS 15117 on 04/16/2009
                    {
                        m_bIsAdminUser = true;
                    } // if

                } // if
                //abansal23: MITS 15117 on 04/18/2009 ENDS
            } // if

            return m_bIsAdminUser;
		}

	}
}
