/***************************************************************************************************
 *   Date     |  MITS/JIRA  | Programmer | Description                                            
 ***************************************************************************************************
 * 06/04/2014 | 33371       | ajohari2   | Added permission for new Tab "PolicySearchDataMapping" added 
 * 7/24/2014  | RMA-718     | ajohari2   | Changes for TPA Access ON/OFF
 * 07/06/2015 | RMA-8753    | nshah28    | Address Master(Show/Hide Address submenu)
 ***************************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;
using System.Linq;
using System.Xml.Linq;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	/// Summary description for UserPermissions.
	/// </summary>
	public class UserPermissions
	{
		/// <summary>
		/// Constants for security
		/// </summary>

		public const int RMB_RISKMASTER = 1;
		public const int RMB_USERDOC = 50;
		public const int RMB_USERDOC_VIEW = 51;
		public const int RMB_WC = 3000;
		public const int RMB_DI = 60000;
		public const int RMB_VA = 6000;
        //Start:Neha Suresh Jain, 06/01/2010,for claimpc
        public const int RMB_PC = 40000;
        //End:Neha Suresh Jain
		public const int RMB_GC = 150;
		public const int RMB_EV = 11000;
		public const int RMO_MERGE_SETUP = 10;
		//public const int RMO_MERGE_CREATE = 11; //TR 2286 commented By Nitesh 06 April 2006
		public const int RMO_VIEW = 1;
		public const int RMO_UPDATE = 2;
		public const int RMO_CREATE = 3;
		public const int RMO_DELETE = 4;

		public const int RMB_FUNDS = 9500;
		public const int RMB_FUNDS_TRANSACT = 9650;
		public const int RMB_FUNDS_PRINTCHK = 10100;
		public const int RMB_FUNDS_BNKACCT = 9800;
		public const int RMB_FUNDS_VOIDCHK = 10250;
        //Added by Ashutosh 
        public const int RMB_FUNDS_UNCLEARCHECKS = 10251  ;           
        public const int RMB_FUNDS_RECREATECHECKFILE = 10252;
        public const int RMB_FUNDS_RESETPRINTEDCHECK = 10253;
        //
		//added by mcapps2 Moving Combined Payments from utilities to Funds menu
		public const int RMB_FUNDS_COMBINEDPAYMENT = 10350;
		// end mcapps2
		
        // Added By sachin 
        public const int RMB_FUNDS_CustomPaymentNotification = 10700;


		public const int RMB_FUNDS_CLEARCHK = 10200;
		public const int RMB_FUNDS_AUTOCHK = 10400;
		public const int RMB_FUNDS_APPPAYCOV = 10500;
		public const int RMB_FUNDS_DEPOSIT = 10650;
		public const int RMB_FUNDS_SUP_APPROVE_PAYMENTS = 10600;
		
		public const int RMB_DIARY = 19000;
        //Added by Nitin for R6 Diary Config implementation
        public const int RMB_DIARY_HEADER_CONFIG = 19038;
        public const int RMB_DIARY_CALENDAR = 19039;
		public const int RMA_UTILITIES = 100000;
		public const int RMA_REPORTS = 3000000;

		public const int RMB_VEHMAINT = 13500;
		public const int RMB_PEOPMAINT = 14000;
		public const int RMB_EMPMAINT = 14500;
		public const int RMB_PHYMAINT = 21200;
		public const int RMB_STAFFMAINT = 21300;
		public const int RMB_PATIENT_TRACKING = 21000;

        //aaggarwal29 MITS 28152 --Start; Added new entry for Catastrophe Maintenance node
         public const int RMB_CATASTROPHEMAINT = 80000;
        //aaggarwal29 MITS 28152 --End

        //Geeta FMLA Enhancement 
        public const int RMB_LEAVE_ADMINISTRATION = 24000;
        //Shruti Policy Billing Enhancement 
        public const int RMB_POLICY_BILLING = 1200500180;
		
		public const int RMB_CODES = 16000;
		public const int RMB_ENTITIES = 16500;
		
		public const int RMB_ORG = 17000;
		public const int RMB_STATES = 18000;
        //Start:added by nitin goel, MITS 30910, NI coverage group maintenance
        public const int RMB_COVGROUP = 1220400300;
        //end:added by Nitin goel		
		public const int RMB_DIS_PLAN = 22000;
		public const int RMB_POLMGT = 9000;
		public const int RMB_DCC = 359000;
		public const int RMB_SM = 358000;
		public const int RMB_EXECSUMMARYCONF = 350000;
		public const int RMB_EXECSUMMARYREPORTS = 350001;

		public const int RMB_SEARCH = 8000000;
		public const int RMB_CLAIM_SEARCH = 8000001;
		public const int RMB_EVENT_SEARCH = 8000002;
		public const int RMB_EMPLOYEE_SEARCH = 8000003;
		public const int RMB_ENTITY_SEARCH = 8000004;
		public const int RMB_VEHICLE_SEARCH = 8000005;
		public const int RMB_POLICY_SEARCH = 8000006;
		public const int RMB_PAYMENT_SEARCH = 8000007;
		public const int RMB_PATIENT_SEARCH = 8000008;
		public const int RMB_PHYSICIAN_SEARCH = 8000009;
        //Geeta FMLA Enhancement
        public const int RMB_LEAVEPLAN_SEARCH = 8000023;
		//Added by Mohit Yadav for Bug no. 001861
        public const int RMB_DRIVER_SEARCH = 8000025;
		public const int RMB_MEDSTAFF_SEARCH = 8000010;
		public const int RMB_DISABILITYPLAN_SEARCH = 8000011;
		public const int RMB_ADMINTRACKING_SEARCH = 8000020;
		public const int RMB_ENHPOLICY_SEARCH = 8000021;
		public const int RMB_ENHPOLICYILLING_SEARCH = 8000022;
        //Anu Tennyson for MITS 18291-STARTS- For adding Search functionality to Property 10/25/2009
        public const int RMB_PROPERTY_SEARCH = 8000012;
        //Anu Tennyson for MITS 18291-ENDS
        //skhare7 Diary Search
        public const int RMB_DIARY_SEARCH = 8000024;
        //skhare7 End

        //aaggarwal29: MITS 36415 start
        public const int RMB_CATASTROPHE_SEARCH = 8000026;
        //aaggarwal29: MITS 36415 end

//Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
        public const int RMB_POLMGTGL_ENH = 23000;
        public const int RMB_POLMGTAL_ENH = 26000;
        public const int RMB_POLMGTWC_ENH = 25000;
        public const int RMB_POLMGTPC_ENH = 42000;
//End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
		public const int RMB_ADMIN_TRACK = 5000000;

		public const int RMO_JURIS_FORM_OPTIONS=4100000;
		public const int RMO_FROI_OPTIONS=4000000;

        //Mridul 10/26/09 MITS #18230
        public const int RMB_PROPERTYMAINT = 24500;
        //OFAC Check: Payee Check Utility
        public const int RMB_ALLOW_PAYEE_CHECK_REVIEW = 10900;
        //Animesh Inserted Bulk Check Release  //skhare7 RMSC Merge 28397
        private const int RMB_FUNDS_BLKCHKREL = 10810;
        //Animesh Insertion ends//skhare7 RMSC merge

        //Start(09/27/2010):MITS# 22407
        public const int RMB_BATCHES = 1200500197;
        //End:Sumit
        //Manika : MITS 26407
        public const int RMO_SORTMASTER = 50000;
        //public const int RMO_STANDARD_REPORT = 300100; 
		public const int RMO_STANDARD_REPORT = 300000;//atavaragiri :MITS 29341 //
        //End MITS 26407

		//atavaragiri :MITS 29341 //
		public const int RMO_BI = 300100; 
		public const int RMO_OSHA_REPORT = 300200; 
		public const int RMO_FROI_ACORD = 300300; 
		public const int RMO_EXEC_SUMMARY = 300400; 
		public const int RMO_SORTMASTER_REPORT = 300500;
        public const int RMO_DRIVER_MAIN = 25200;  //Aman Driver Changes
		//end : MITS 29341 //
        //spahariya MITS 28867
        public const int RMB_WMEMAIL_DET = 89;
        public const int RMB_WMEMAIL_DET_VIEW = 93;
        //spahariya-End


        /// <summary>
        /// Private pointer to MDIMenu xml
        /// </summary>
        private XElement MDIMenu = null;

        private XmlDocument objAdminConfDoc = null;
		
		/// <summary>
		/// Private Connection String variable
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// Private Security Connection  String variable
		/// </summary>
		private string m_sSecureDSN="";
		/// <summary>
		/// Private string for UserId
		/// </summary>
		private string m_sUserId="";

        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;
			
		public string DSN
		{
			set
			{
				m_sDSN=value;	
			}
		}
		public string UserId
		{
			set
			{
				m_sUserId=value;	
			}
		}
		public string SecurityDSN
		{
			set
			{
				m_sSecureDSN=value;	
			}
		}
		public UserPermissions(string p_sDSN,string p_sSecureDSN,string p_sUserId, int p_iClientId)
		{
			m_sDSN=p_sDSN;
			p_sUserId=m_sUserId;
			m_sSecureDSN=p_sSecureDSN;
            m_iClientId = p_iClientId;
		}
        public UserPermissions(int p_iClientId) { m_iClientId = p_iClientId; }

		#region "RemoveNode(XmlDocument p_objDocument,string p_sNodeName)"
		/// Name			: RemoveNode
		/// Date Created	: 15-Dec-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Removes nodes from the XML.
		/// </summary>
		/// <param name="p_objDocument">
		///		Input Xml from which the node will be removed.
		///	</param>
		/// <param name="p_sNodeName">
		///		Node to be removed.
		///	</param>
		public void RemoveNode(XmlDocument p_objDocument,string p_sNodeName)
		{
			try
			{ 
				XmlNode objNode=p_objDocument.SelectSingleNode("//entry[@id='"+p_sNodeName+"']");
				if (objNode!=null)
					objNode.ParentNode.RemoveChild(objNode);
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(XmlException p_objXmlException)
			{
				throw new RMAppException(Globalization.GetString("UserPermissions.RemoveNode.error", m_iClientId), p_objXmlException);  
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.RemoveNode.error", m_iClientId), p_objException);  
			}
			finally
			{

			}
		}
		/// Name			: SearchRemoveAttributeNode
		/// Date Created	: 02-Mar-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Removes nodes from the XML.
		/// </summary>
		/// <param name="p_objDocument">
		///		Input Xml from which the node will be removed.
		///	</param>
		/// <param name="p_sNodeName">
		///		Node to be removed.
		///	</param>
		private void SearchRemoveAttributeNode(XmlDocument p_objSearchDocument,XmlDocument p_objRemoveDocument,
			string p_sNodeSearchName,string p_sNodeRemoveName)
		{
			try
			{ 
				XmlNode objSearch=p_objSearchDocument.SelectSingleNode(p_sNodeSearchName);
				if (objSearch!=null)
				{
					if (objSearch.InnerText!=null)
					{
						if (objSearch.Attributes["value"]!=null)
						{
							if (objSearch.Attributes["value"].Value!="-1")
							{
								RemoveNode(p_objRemoveDocument,p_sNodeRemoveName);
							}
						}
					}
				}
				
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(XmlException p_objXmlException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveAttributeNode.error", m_iClientId), p_objXmlException);  
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveAttributeNode.error", m_iClientId), p_objException);  
			}
			finally
			{

			}


		}


        private void MDISearchForValueRemoveBySysName(string p_sNodeSearchName, string sysName)
        {
            try
            {
                XmlNode objSearch = this.objAdminConfDoc.SelectSingleNode(p_sNodeSearchName);
                if (objSearch != null)
                {
                    if (objSearch.InnerText != null)
                    {
                        if (objSearch.Attributes["value"] != null)
                        {
                            if (objSearch.Attributes["value"].Value != "-1")
                            {
                                MDIRemoveBySysName(sysName);
                            }
                        }
                    }
                }

            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveAttributeNode.error", m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveAttributeNode.error", m_iClientId), p_objException);
            }
            finally
            {

            }
        }


		/// Name			: SearchRemoveNode
		/// Date Created	: 02-Mar-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Removes nodes from the XML.
		/// </summary>
		/// <param name="p_objDocument">
		///		Input Xml from which the node will be removed.
		///	</param>
		/// <param name="p_sNodeName">
		///		Node to be removed.
		///	</param>
		private void SearchRemoveNode(XmlDocument p_objSearchDocument,XmlDocument p_objRemoveDocument,
			string p_sNodeSearchName,string p_sNodeRemoveName)
		{
			try
			{ 
				XmlNode objSearch=p_objSearchDocument.SelectSingleNode(p_sNodeSearchName);
				if (objSearch!=null)
				{
					if (objSearch.InnerText!=null)
					{
						if (objSearch.InnerText!="-1")
						{
							RemoveNode(p_objRemoveDocument,p_sNodeRemoveName);
						}
					}
				}
				
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(XmlException p_objXmlException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveNode.error", m_iClientId), p_objXmlException);  
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveNode.error", m_iClientId), p_objException);  
			}
			finally
			{

			}
		}


        private void MDISearchRemoveBySysName(string p_sNodeSearchName, string sysName)
        {
            try
            {
                XmlNode objSearch = this.objAdminConfDoc.SelectSingleNode(p_sNodeSearchName);
                if (objSearch != null)
                {
                    if (objSearch.InnerText != null)
                    {
                        if (objSearch.InnerText != "-1")
                        {
                            MDIRemoveBySysName(sysName);
                        }
                    }
                }

            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveNode.error", m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UserPermissions.SearchRemoveNode.error", m_iClientId), p_objException);
            }
            finally
            {

            }
        }

        #endregion

		#region "GetSecuredNavTreeForUser(XmlDocument p_objDocument,Riskmaster.Security.UserLogin p_objUserLogin)"
		/// Name			: GetSecuredNavTreeForUser
		/// Date Created	: 15-Dec-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Get security details in Xml format.
		/// </summary>
		/// <param name="p_sDocument">
		///		Out Parameter. Returns security details in Xml format.
		///	</param>
		/// <returns>0 - Success</returns>		
		public XmlDocument GetSecuredNavTreeForUser(XmlDocument p_objDocument,Riskmaster.Security.UserLogin p_objUserLogin)
		{
			bool blnAllowed=false;
			XmlDocument objTempDoc=null;
			XmlNode objTemlElement=null;
			string sInnerText="";
			//BSB\JP 09.06.2006 This temp login feature kills performance at clients.
			// It was added so that SMS permissions changes take effect w/o the user logging off first.
			// This is not worth the performance penalty we're seeing. Hence I'm removing this.
			// If this is indeed a "can't live without" feature then we need to look at SMS to 
			// update the userlogin object that is in Session at the time the perm changes are being made.
			// Logging in repeatedly with every nav-tree fetch is ludicrous.
			//UserLogin objTempLogin=null;
			int iAll=1;
			//XmlNode objSessionDSN=null;
			//XmlNode objBRSInstalled = null;
			string sView="";
			bool bIsPwdPolicyEnabled = false;
			bool bIsSMSPermittedUser = false; //Flag to keep track of SMS permitted user.--Tanuj
			//bool bIsBRSInstalled = false;// Read BRS Installed setting from Settings.
			//bool bIsBRSEnabled = false;// Read BRS Enabled setting from Riskmaster.Config.
            // skhare7 Used for User Pri.Setup access MITS:22374
            bool bIsUPSPermittedUser = false; 
			try
			{				
				/// TR#1158 Pankaj 01/03/06 Get viewid from xml before its removed
				
				//BSB 01.26.2007 Adding Support for Riskmaster.config Extensibility section
				// and NavTreeMapping elements. 
				// Note: Add is performed first (while we can still add "before" and "after" nodes
				// that may be removed in normal processing.
				// Remove and Replace are completed later since a replaced node may get a 
				// new id value and we want the regular system to be able to control display 
				// based on the original id.
                //TODO: Migrate this security model using a SiteMapDataSource through MDI
				//CustomNavTree.AddNodes(p_objDocument);
				
				//Start-Added the following code for Enhance security--Tanuj
                SMSUser  objSMSUser = null;
				try
				{
                    //MITS-10662 - Changed the query to make it user_id dependent
					//objSMSUser = new SMSUser( p_objUserLogin.LoginName ) ;					
                    //bIsSMSPermittedUser = objSMSUser.IsSMSPermittedUser( p_objUserLogin.LoginName );
                    //objSMSUser = new SMSUser(-p_objUserLogin.UserId);
                    //if (objSMSUser.RecordId != 0 && objSMSUser.RmLoginName != "")
                    //    bIsSMSPermittedUser = true;
                    //else
                    //{
                    //    objSMSUser = new SMSUser(p_objUserLogin.UserId);
                    //    bIsSMSPermittedUser = objSMSUser.IsSMSPermittedUser(p_objUserLogin.UserId);
                    //}
                    //if (objSMSUser.RecordId != 0 && objSMSUser.RmLoginName != "")
                    //    bIsUPSPermittedUser = true;
                    //else
                    //{
                    //    objSMSUser = new SMSUser(p_objUserLogin.UserId);
                    //    bIsUPSPermittedUser = objSMSUser.IsUPSPermittedUser(p_objUserLogin.UserId);
                    //}

                    //smishra25:Commented the code above and setting both the boolean variables in one call
                    objSMSUser = new SMSUser(p_objUserLogin.UserId,m_iClientId);
                    bIsUPSPermittedUser = objSMSUser.IsSMSAndUPSPermittedUser(p_objUserLogin.UserId, out bIsSMSPermittedUser);

				}
				finally
				{
					objSMSUser = null;
				}

				bIsPwdPolicyEnabled = PublicFunctions.IsPwdPolEnabled() ;
				
				p_objDocument.SelectSingleNode("//IsEnhSecurityEnabled").InnerText = bIsPwdPolicyEnabled.ToString() ;
				if(!bIsPwdPolicyEnabled || !bIsSMSPermittedUser)
				{
					this.RemoveNode( p_objDocument , "PwdPolicyParms" ) ;
					this.RemoveNode( p_objDocument , "MaintainUserAccount" );
				}

                // rsolanki2:- (20/11/2007) - R4 optional frameset enhancement
                // removing the Portalsettings node if the logegd in iser is not a 'csc' 
                if (!bIsSMSPermittedUser) 
                {
                    this.RemoveNode(p_objDocument, "PortalSettings");
                }                

                // rsolanki2:- (18/02/2008) - R4 Domain authentication enhancement
                // removing the Portalsettings node if Domain authentication is enabled 
                if (Security.bIsDomainAuthenticationEnabled())
                {
                    this.RemoveNode(p_objDocument, "ChangePassword");
                }
				bool bIsScriptEditorEnabled= PublicFunctions.IsScriptEditorEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId) ;
				if(!bIsScriptEditorEnabled )
				{
					this.RemoveNode( p_objDocument , "ScriptEditor" ) ;
				}
				//End-Added the following code for Enhance security--Tanuj

                // Naresh Code commented as It was not working properly. Code added to make it work
                /*
                //Divya -MITS 8974 Check for Enhanced Policy  - Start
                bool bIsEnhPolEnabled = PublicFunctions.IsEnhPolEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString);
                if (!bIsEnhPolEnabled)
                {
                    this.RemoveNode(p_objDocument, "enhpolicy-tab");
                    this.RemoveNode(p_objDocument, "policybilling-tab");
                    this.RemoveNode(p_objDocument, "EnhancedPolicySetup");
                    this.RemoveNode(p_objDocument, "BillingSystemSetup");

                }
                else
                {
                    this.RemoveNode(p_objDocument, "policy-tab");
                }

                //Divya - MITS 8974 Check for Enhance Policy  - End


                //Shruti - Check for Billing System Flag - Start
                bool bIsBillingSysEnabled = PublicFunctions.IsBillingSysEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString);
                if (!bIsBillingSysEnabled)
                {
                    this.RemoveNode(p_objDocument, "policybilling-tab");
                    this.RemoveNode(p_objDocument, "BillingSystemSetup");
                }
                //Shruti - Check for Billing System Flag - End
                 * */

                // Naresh Code added for Implementing the changes in the Nav Tree when the 
                // Billing and Enhanced Policy is enabled or disabled
                bool bIsEnhPolEnabled = PublicFunctions.IsEnhPolEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
                bool bIsBillingSysEnabled = PublicFunctions.IsBillingSysEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);

                if (!bIsEnhPolEnabled)
                {
                    this.RemoveNode(p_objDocument, "enhpolicy-tab");
                    this.RemoveNode(p_objDocument, "policybilling-tab");
                    this.RemoveNode(p_objDocument, "Batches");
                    this.RemoveNode(p_objDocument, "BillingScheduler");
                    this.RemoveNode(p_objDocument, "enhancedpolicy");
                    this.RemoveNode(p_objDocument, "EnhancedPolicySetup");
                    this.RemoveNode(p_objDocument, "BillingSystemSetup");
                }
                else
                {
                    this.RemoveNode(p_objDocument, "policy-tab");
                    this.RemoveNode(p_objDocument, "policysearch");
                    if (!bIsBillingSysEnabled)
                    {
                        this.RemoveNode(p_objDocument, "policybilling-tab");
                        this.RemoveNode(p_objDocument, "Batches");
                        this.RemoveNode(p_objDocument, "BillingScheduler");
                        this.RemoveNode(p_objDocument, "BillingSystemSetup");
                    }
                }


				// Remove SMS and related items from Navtree if user lacks SMS rights
				if(!bIsSMSPermittedUser)//Flag to keep track of SMS permitted user.If user is permitted to access SMS then let the user have the permissions to access BES and UserPriviligesSetup as well.
				{
					this.RemoveNode( p_objDocument , "SMS" ) ;
					this.RemoveNode( p_objDocument , "BusinessEntitySecurity" );
                        //skhare7 MITS:22374
                    //this.RemoveNode( p_objDocument , "UserPriviligesSetup" );
				}
                //skhare7 MITS:22374 
                if (!bIsUPSPermittedUser)//Flag to keep track to access User Pri.Setup skhare7
                {
                   
                    this.RemoveNode( p_objDocument , "UserPriviligesSetup" );
                }
                //End skhare7 MITS:22374
				objTemlElement = p_objDocument.SelectSingleNode("//parameter/name[.='View']/../value");
				if(objTemlElement!=null)
					sView = objTemlElement.InnerText;

				objTemlElement=p_objDocument.SelectSingleNode("//parameter/name[.='DataSource']/../value");
				//objTempLogin=new UserLogin(p_objUserLogin.LoginName,p_objUserLogin.Password,objTemlElement.InnerText);
				objTemlElement=p_objDocument.SelectSingleNode("//toc");
				sInnerText="<toc>"+objTemlElement.InnerXml+"</toc>";
				objTempDoc=new XmlDocument();
				objTempDoc.InnerXml=sInnerText;
				p_objDocument=new XmlDocument();
				p_objDocument=objTempDoc;
				//p_objUserLogin=objTempLogin;
				if (p_objUserLogin.objRiskmasterDatabase.Status ==true)
				{
					blnAllowed=p_objUserLogin.IsAllowed(RMB_RISKMASTER); 
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"APPLICATION");
						RemoveNode(p_objDocument,"Search");
						RemoveNode(p_objDocument,"RMDiaries");
						RemoveNode(p_objDocument,"maint");
						RemoveNode(p_objDocument,"DocumentManagement");
						RemoveNode(p_objDocument,"RMFunds");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_USERDOC) && p_objUserLogin.IsAllowed(RMB_USERDOC_VIEW); 
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"UserDocuments");
					}
				
					blnAllowed= p_objUserLogin.IsAllowed(RMB_WC);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"wc-tab");
						RemoveNode(p_objDocument,"wc");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_WC,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_WC,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"wc-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"wc","url","&SysCmd=1");
						iAll=0;
					}
					else
					{
						iAll=0;
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_VA);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"va-tab");
						RemoveNode(p_objDocument,"va");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_VA,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_VA,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"va-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"va","url","&SysCmd=1");
						iAll=0;
					}
					else
					{
						iAll=0;
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_DI);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"nonoc-tab");
						RemoveNode(p_objDocument,"nonocc");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_DI,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_DI,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"nonoc-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"nonocc","url","&SysCmd=1");
						iAll=0;
					}
					else
					{
						iAll=0;
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_GC);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"claimgc-tab");
						RemoveNode(p_objDocument,"claimgc");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_GC,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_GC,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"claimgc-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"claimgc","url","&SysCmd=1");
						iAll=0;
					}
					else
					{
						iAll=0;
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_EV);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"event-tab");
						RemoveNode(p_objDocument,"event");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_EV,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_EV,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"event-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"event","url","&SysCmd=1");
						iAll=0;
					}
					else
					{
						iAll=0;
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_ADMIN_TRACK);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"at");
					}
					if (iAll==1)
					{
						RemoveNode(p_objDocument,"Topdown");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMO_MERGE_SETUP);//TR 2286 commented By Nitesh 06 April 2006 && p_objUserLogin.IsAllowed(RMO_MERGE_CREATE);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"WordMergeSetup");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"Search");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_CLAIM_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"claimsearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_EVENT_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"eventssearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_EMPLOYEE_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"employeesearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_ENTITY_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"entitysearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_VEHICLE_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"vehiclesearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_POLICY_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"policysearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_PAYMENT_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"fundsearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_PATIENT_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"patientsearch");
					}
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_DRIVER_SEARCH);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "driversearch");
                    }
					blnAllowed= p_objUserLogin.IsAllowed(RMB_PHYSICIAN_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"physiciansearch");
					}
                    //Geeta FMLA Enhancement
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_LEAVEPLAN_SEARCH);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "leaveplansearch");
                    }
					//Begin: Added by Mohit Yadav for Bug No. 001861
					blnAllowed= p_objUserLogin.IsAllowed(RMB_MEDSTAFF_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"medstaffsearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_DISABILITYPLAN_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"displansearch");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_ADMINTRACKING_SEARCH);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"admintracksearch");
					}
                	//End: Added by Mohit Yadav for Bug No. 001861
                    //Anu Tennyson for MITS 18291: STARTS 10/25/2009
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_PROPERTY_SEARCH);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "propertysearch");
                    }
                    //Anu Tennyson for MITS 18291: ENDS
                    //skhare7 Diary Search
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_DIARY_SEARCH);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "diarysearch");
                    }
					blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS);
					if (!blnAllowed)
					{
                       
						RemoveNode(p_objDocument,"RMFunds");
					}
					else
					{
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_TRANSACT);
                        if (!blnAllowed)
                        {
                            RemoveNode(p_objDocument, "Transactions");
                        }

                       
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_BNKACCT);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"bankaccount-tab");
							RemoveNode(p_objDocument,"bankaccount");
						}
						if (p_objUserLogin.IsAllowedEx(RMB_FUNDS_BNKACCT,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_FUNDS_BNKACCT,RMO_CREATE))
						{
							AppendToNodeAttribute(p_objDocument,"bankaccount-tab","url","&SysCmd=1");
							AppendToNodeAttribute(p_objDocument,"bankaccount","url","&SysCmd=1");
						}
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_VOIDCHK);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"VoidChecks");
						}
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_CLEARCHK);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"MarkChecksCleared");
						}
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_AUTOCHK);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"autochecks-tab");
						}
                        //Added Rakhi for MITS 13084-START:Display Autocheck Records when Automatic Checks have View permission only.
                        else if (p_objUserLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_CREATE))
                        {
                            AppendToNodeAttribute(p_objDocument, "autochecks-tab", "url", "&amp;SysCmd=1");
                        }
                        //Added Rakhi for MITS 13084-START:Display Autocheck Records when Automatic Checks have View permission only.
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_PRINTCHK);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"PrintChecks");
						}
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_APPPAYCOV);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"AddPaymentsToCoverages");
						}
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_DEPOSIT);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"deposit-tab");
						}
						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_DEPOSIT);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"deposit");
						}

                        // Added By Sachin Gupta for MITS 28852
                        blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_CustomPaymentNotification);
                        if (!blnAllowed)
                        {
                            RemoveNode(p_objDocument, "CustomPaymentNotification");
                        }

						blnAllowed= p_objUserLogin.IsAllowed(RMB_FUNDS_SUP_APPROVE_PAYMENTS);
						if (!blnAllowed)
						{
							RemoveNode(p_objDocument,"ApproveFundsTransactions");
						}
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_DIARY);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"RMDiaries");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMA_UTILITIES);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"RMUtilities");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_DCC);
					if (!blnAllowed)
					{
                        RemoveNode(p_objDocument, "reports-listing");//Changed by Shivendu for MITS 17643
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_SM);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"Designer");
						RemoveNode(p_objDocument,"DraftReports");
						RemoveNode(p_objDocument,"PostDraftReports");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_VEHMAINT);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"veh-tab");
						RemoveNode(p_objDocument,"veh");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_VEHMAINT,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_VEHMAINT,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"veh-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"veh","url","&SysCmd=1");
					}
				
                    //Mridul. 10/26/09 MITS#18230
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_PROPERTYMAINT);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "prop-tab");
                        RemoveNode(p_objDocument, "prop");
                    }
                    else if (p_objUserLogin.IsAllowedEx(RMB_PROPERTYMAINT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PROPERTYMAINT, RMO_CREATE))
                    {
                        AppendToNodeAttribute(p_objDocument, "prop-tab", "url", "&SysCmd=1");
                        AppendToNodeAttribute(p_objDocument, "prop", "url", "&SysCmd=1");
                    }

					blnAllowed= p_objUserLogin.IsAllowed(RMB_PEOPMAINT);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"people-tab");
						RemoveNode(p_objDocument,"people");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_PEOPMAINT,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PEOPMAINT,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"people-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"people","url","&SysCmd=1");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_EMPMAINT);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"emp-tab");
						RemoveNode(p_objDocument,"emp");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_EMPMAINT,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_EMPMAINT,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"emp-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"emp","url","&SysCmd=1");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_ENTITIES);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"ent-tab");
						RemoveNode(p_objDocument,"ent");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_ENTITIES,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_ENTITIES,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"ent-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"ent","url","&SysCmd=1");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_DIS_PLAN);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"plan-tab");
						RemoveNode(p_objDocument,"plan");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_DIS_PLAN,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_DIS_PLAN,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"plan-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"plan","url","&SysCmd=1");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_ORG);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"orgMaintenance");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_STATES);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"stateMaintenance");
					}
                    //start: added by nitin goel, MITS 30910, NI changes , 08/29/2013
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_COVGROUP);
                    if (!blnAllowed)                    {
                        RemoveNode(p_objDocument, "CoverageGroupMaintenance");
                    }   
                        //end: added by nitin goel
					blnAllowed= p_objUserLogin.IsAllowed(RMB_POLMGT);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"policy");
						RemoveNode(p_objDocument,"policy-tab");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_POLMGT,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_POLMGT,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"policy-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"policy","url","&SysCmd=1");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_STAFFMAINT);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"staff");
						RemoveNode(p_objDocument,"staff-tab");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_STAFFMAINT,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_STAFFMAINT,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"staff-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"staff","url","&SysCmd=1");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_PHYMAINT);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"phys");
						RemoveNode(p_objDocument,"phys-tab");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_PHYMAINT,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PHYMAINT,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"phys-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"phys","url","&SysCmd=1");
					}

					blnAllowed= p_objUserLogin.IsAllowed(RMB_PATIENT_TRACKING);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"patient");
						RemoveNode(p_objDocument,"patient-tab");
					}
					else if (p_objUserLogin.IsAllowedEx(RMB_PATIENT_TRACKING,RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PATIENT_TRACKING,RMO_CREATE))
					{
						AppendToNodeAttribute(p_objDocument,"patient-tab","url","&SysCmd=1");
						AppendToNodeAttribute(p_objDocument,"patient","url","&SysCmd=1");
					}
                    //Geeta FMLA Enhancement 
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_LEAVE_ADMINISTRATION);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "leaveplan");
                        RemoveNode(p_objDocument, "leaveplan-tab");
                    }
                    else if (p_objUserLogin.IsAllowedEx(RMB_LEAVE_ADMINISTRATION, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_LEAVE_ADMINISTRATION, RMO_CREATE))
                    {
                        AppendToNodeAttribute(p_objDocument, "leaveplan-tab", "url", "&SysCmd=1");
                        AppendToNodeAttribute(p_objDocument, "leaveplan", "url", "&SysCmd=1");
                    }

                    //Shruti Policy Billing 
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_POLICY_BILLING);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "policybilling");
                        RemoveNode(p_objDocument, "policybilling-tab");
                    }
                    else if (p_objUserLogin.IsAllowedEx(RMB_POLICY_BILLING, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_POLICY_BILLING, RMO_CREATE))
                    {
                        AppendToNodeAttribute(p_objDocument, "policybilling-tab", "url", "&SysCmd=1");
                        AppendToNodeAttribute(p_objDocument, "policybilling", "url", "&SysCmd=1");
                    }
					blnAllowed= p_objUserLogin.IsAllowed(RMB_CODES);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"tablemaint");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_EXECSUMMARYREPORTS);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"AdminExecutiveSummaryConfiguration");
						RemoveNode(p_objDocument,"ExecutiveSummaryConfiguration");
					}
					blnAllowed= p_objUserLogin.IsAllowed(RMB_EXECSUMMARYCONF);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"claimexecsearch");
						RemoveNode(p_objDocument,"eventexecsearch");
					}

					//check for First Report of Injury Options security permission
					blnAllowed= p_objUserLogin.IsAllowed(RMO_FROI_OPTIONS);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"FROIOptions");
					}
					//check for Jurisdictional Form Options security permission
					blnAllowed= p_objUserLogin.IsAllowed(RMO_JURIS_FORM_OPTIONS);
					if (!blnAllowed)
					{
						RemoveNode(p_objDocument,"JurisOptions");
					}
                    //Aman Driver Changes
                    blnAllowed = p_objUserLogin.IsAllowed(RMO_DRIVER_MAIN);
                    if (!blnAllowed)
                    {
                        RemoveNode(p_objDocument, "driver");
                        RemoveNode(p_objDocument, "driver-tab");
                    }
                    else if (p_objUserLogin.IsAllowedEx(RMO_DRIVER_MAIN, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMO_DRIVER_MAIN, RMO_CREATE))
                    {
                        AppendToNodeAttribute(p_objDocument, "driver-tab", "url", "&SysCmd=1");
                        AppendToNodeAttribute(p_objDocument, "driver", "url", "&SysCmd=1");
                    }
                    //Aman Driver Changes
				}

				objTempDoc=GetAdminConfig("customize_custom");
				if(objTempDoc.InnerText=="")
				{
					RemoveNode(p_objDocument,"Custom");
				}
                else
                {
                    SearchRemoveNode(objTempDoc, p_objDocument, "//SecuritySetting/ChangeDatabase", "status");
                    SearchRemoveNode(objTempDoc, p_objDocument, "//SecuritySetting/ChangePassword", "ChangePassword");
                    SearchRemoveNode(objTempDoc, p_objDocument, "//SpecialSettings/Show_Maintenance", "maint");

                }
                //if (objTempDoc.InnerText != "")
                //{
                //    RemoveNode(p_objDocument, "Searches");
                //}
				/// TR#1158 Pankaj 01/03/06 Check powerview permissions for the nav-tree
				//if (sView!="")
				//	ApplyPowerViewPermissions(p_objDocument,p_objUserLogin, sView);
				
				objTempDoc=GetAdminConfig("customize_settings");

				SearchRemoveNode(objTempDoc,p_objDocument,"//Buttons/Search[@title='Search']","Search");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Buttons/Document[@title='Documents']","APPLICATION");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Buttons/File[@title='Files']","DocumentManagement");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Buttons/Diary[@title='Diaries']","RMDiaries");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Buttons/Report[@title='Reports']","RMReports");



				
				// 01/17/2007 Rem Umesh
                AppendAttribute(objTempDoc, p_objDocument, "//Other/ShowName", "//toc","currentuser_allowed");
                AppendAttribute(objTempDoc, p_objDocument, "//Other/ShowLogin", "//toc","currentlogin_allowed");
				

				// End Rem Umesh

				objTempDoc=GetAdminConfig("customize_search");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Claim[@title='Claims']","claimsearch");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Event[@title='Events']","eventssearch");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Employee[@title='Employees']","employeesearch");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Entity[@title='Entities']","entitysearch");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Vehicle[@title='Vehicles']","vehiclesearch");

                // Naresh As the Enhanced Policy Serach Option is removed from Utility, It is put here
                if(bIsEnhPolEnabled)
                    SearchRemoveNode(objTempDoc, p_objDocument, "//Search/Policy[@title='Policies']", "enhancedpolicy");
                else
				    SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Policy[@title='Policies']","policysearch");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Fund[@title='Funds']","fundsearch");
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Patient[@title='Patients']","patientsearch");
                SearchRemoveNode(objTempDoc, p_objDocument, "//Search/Driver[@title='Drivers']", "driversearch");//Aman Driver Enh
				SearchRemoveNode(objTempDoc,p_objDocument,"//Search/Physician[@title='Physicians']","physiciansearch");
				// Start Naresh Enhanced Policy Search
                // Naresh This has been commented as there is no option of Checking the Search in Utility
				// SearchRemoveNode(objTempDoc, p_objDocument, "//Search/EnhPolicy[@title='EnhPolicies']", "enhancedpolicy");
				// End Naresh Enhanced Policy Search
				// Start Shruti Leave Plan Search
				SearchRemoveNode(objTempDoc, p_objDocument, "//Search/LeavePlan[@title='LeavePlans']", "leaveplansearch");
				// End Shruti Leave Plan Search

                //Anu Tennyson for MITS 18291:STARTS 10/25/2009
                SearchRemoveNode(objTempDoc, p_objDocument, "//Search/Property[@title='Property']", "Propertysearch");
                //Anu Tennyson for MITS 18291:ENDS
                //skhare7
                SearchRemoveNode(objTempDoc, p_objDocument, "//Search/Diary[@title='Diary']", "diarysearch");
                //skhare7
				objTempDoc=GetAdminConfig("customize_reports");
                UpdateLabel(objTempDoc, p_objDocument, "//ReportMenu/ReportLabel/@title", "//ReportMenu/ReportLabel/@default", "Reports_All");
                UpdateLabel(objTempDoc, p_objDocument, "//ReportMenu/SMLabel/@title", "//ReportMenu/SMLabel/@default", "SMNET");
                UpdateLabel(objTempDoc, p_objDocument, "//ReportMenu/ExecSummLabel/@title","//ReportMenu/ExecSummLabel/@default", "ExecutiveSummaryConfig");
                UpdateLabel(objTempDoc, p_objDocument, "//ReportMenu/OtherLabel/@title", "//ReportMenu/OtherLabel/@default", "Other_Reports");
                UpdateLabel(objTempDoc, p_objDocument, "//ReportMenu/OtherLabel/DCCLabel/@title", "//ReportMenu/OtherLabel/DCCLabel/@default", "DCCReport");
                SearchRemoveAttributeNode(objTempDoc, p_objDocument, "//ReportMenu/ReportLabel", "Reports_All");                           //MITS 9445:Umesh
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ReportLabel/AvlReports[@title='Available Reports:']","AvailableReport");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ReportLabel/JobQueue[@title='Job Queue:']","JobQueue");
				//SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ReportLabel/NewReport[@title='Post New Report:']","PostNewReport");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ReportLabel/DeleteReport[@title='Delete Report:']","DeleteReport");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ReportLabel/ScheduleReport[@title='Schedule Reports:']","ScheduledReports");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ReportLabel/ViewSchedReport[@title='View Scheduled Reports:']","ViewScheduledReports");
				SearchRemoveAttributeNode(objTempDoc,p_objDocument,"//ReportMenu/SMLabel","SMNET");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/SMLabel/Designer","Designer");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/SMLabel/DraftReport","DraftReports");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/SMLabel/PostDraftRpt","PostDraftReports");
                SearchRemoveNode(objTempDoc, p_objDocument, "//ReportMenu/ReportLabel/NewReport[@title='Post New Report:']", "PostNewReport");

				SearchRemoveAttributeNode(objTempDoc,p_objDocument,"//ReportMenu/ExecSummLabel","ExecutiveSummaryConfig");
				//SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ExecSummLabel/Configuration","ESConfig");
				//changed by Shruti on 09 Oct 06 (MITS No. -7891)
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ExecSummLabel/Configuration","ExecutiveSummaryConfiguration");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ExecSummLabel/Claim","claimexecsearch");
				SearchRemoveNode(objTempDoc,p_objDocument,"//ReportMenu/ExecSummLabel/Event","eventexecsearch");
                SearchRemoveNode(objTempDoc, p_objDocument, "//ReportMenu/OtherLabel/DCCLabel", "DCCReport");
                SearchRemoveAttributeNode(objTempDoc, p_objDocument, "//ReportMenu/OtherLabel", "Other_Reports");
                SearchRemoveNode(objTempDoc, p_objDocument, "//ReportMenu/OtherLabel/OSHA300", "PostNewOSHA300Report");
                SearchRemoveNode(objTempDoc, p_objDocument, "//ReportMenu/OtherLabel/OSHA301", "PostNewOSHA301Report");
                SearchRemoveNode(objTempDoc, p_objDocument, "//ReportMenu/OtherLabel/OSHA300A", "PostNewOSHA300AReport");
                //REM customization    Umesh
                if (!PublicFunctions.IsHistTrackEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId))
                {
                    RemoveNode(p_objDocument, "HistoryTracking");
                }
                else
                {
                    //validate db version and edition
                    if (!IsValidDbServer(p_objUserLogin.objRiskmasterDatabase.ConnectionString,p_objUserLogin.objRiskmasterDatabase.DbType))
                    {
                        RemoveNode(p_objDocument, "HistoryTracking");
                    }
                }
                XmlNode objSecurityNode = p_objDocument.SelectSingleNode("//entry[@id='Security']");
                if (objSecurityNode != null)
                {
                    if (objSecurityNode.ChildNodes.Count == 0)
                        RemoveNode(p_objDocument, "Security");
                }
				//BSB 01.26.2007 Adding Support for Riskmaster.config Extensibility section
				// and NavTreeMapping elements. 
				// Note: Add is performed first (while we can still add "before" and "after" nodes
				// that may be removed in normal processing.)
				// Remove and Replace are completed here.
                //TODO: Migrate this security model using a SiteMapDataSource through MDI
                //CustomNavTree.ReplaceNodes(p_objDocument);
                //CustomNavTree.RemoveNodes(p_objDocument);

				return p_objDocument;
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(XmlException p_objXmlException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.GetSecuredNavTreeForUser.error", m_iClientId), p_objXmlException);  
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.GetSecuredNavTreeForUser.error", m_iClientId), p_objException);  
			}
			finally
			{
				//objBRSInstalled = null;
			}
		}

		#endregion
		/// Name		: GetAdminConfig
		/// Author		: Parag Sarin
		/// Date Created: 03/02/2006		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <sumxmary>
		/// Gets the Admin Configuration data.
		/// </summary>
		/// <param name="p_sFileName">File name to be retrieved</param>
		/// <returns>Xml document for Admin Configuration</returns>
		private XmlDocument GetAdminConfig(string p_sFileName)
		{
			XmlDocument objXml=null;
		       
			objXml = new XmlDocument();
            string strContent = RMSessionManager.GetCustomString(p_sFileName, m_iClientId);
			if (! string.IsNullOrEmpty(strContent))
			{
                objXml.LoadXml(strContent);
			}//if

			return objXml;
		}


		#region "private void AppendToNodeAttribute(XmlDocument p_objDocument,string p_sNodeName, string p_sAttributeName, string p_sAppendString)"
		/// Name			: AppendToNodeAttribute
		/// Date Created	: 03-Feb-2005
		/// Created By		: Nikhil Garg
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		Appends a string to a node atribute
		/// </summary>
		/// <param name="p_objDocument">
		///		Input Xml which contains the node.
		///	</param>
		/// <param name="p_sNodeName">
		///		Node whose attribute has to be updated
		///	</param>
		///	<param name="p_sAttributeName">
		///		Attribute whose value has to be updated
		///	</param>
		///	<param name="p_sAppendString">
		///		String to be appended to the attribute value
		///	</param>
		private void AppendToNodeAttribute(XmlDocument p_objDocument,string p_sNodeName, string p_sAttributeName, string p_sAppendString)
		{
			XmlElement objElement=(XmlElement)p_objDocument.SelectSingleNode("//entry[@id='"+p_sNodeName+"']");
			if (objElement!=null)
			{
				string sOldValue=objElement.Attributes[p_sAttributeName].Value;
				if (sOldValue.IndexOf(p_sAppendString)<0)
				{
					objElement.SetAttribute(p_sAttributeName,sOldValue+p_sAppendString);
				}
			}
		}


		/// <summary>
		/// Applies the powerview permissions no the nav-tree
		/// TR#1158 Pankaj 01/03/06 If the system is within a Powerview, the navtree should show only the forms added to the view
		/// </summary>
        /// <param name="p_iViewId">view id</param>
        /// <param name="p_iDsnId">Data Source Id</param>
        private void ApplyPowerViewPermissions(int p_iViewId, int p_iDsnId)
		{
			//Following array maps the id's of the Nav Tree xml along with the actual view form names
			//Anu Tennyson for MITS 18291: STRATS - Added Property in the sMap Array 10/25/2009
            //Start:Neha Suresh Jain, 06/01/2010,added claimpc in sMap Array
			string[] sMap = {"event", "claimgc", "claimwc",	"claimva", "claimdi",
							"employee", "entitymaint", "people", "plan", "policy",
							"physician", "staff", "vehicle", "patient", "bankaccount",
                            "leaveplan", "policybilling", "funds","propertyunit","claimpc","driver"
							 };
            //Anu Tennyson for MITS 18291: ENDS
            //End:Neha Suresh Jain, 06/01/2010
			try
			{
                string sViewDbConnection = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId);
                string sSQL = string.Format("SELECT NET_VIEW_FORMS.FORM_NAME FROM NET_VIEW_FORMS WHERE DATA_SOURCE_ID={0} AND VIEW_ID={1}", p_iDsnId, p_iViewId);
                using (DataSet objDs = DbFactory.GetDataSet(sViewDbConnection, sSQL, m_iClientId))
				{
					for(int i =0; i< sMap.Length; i++)
					{
						objDs.Tables[0].DefaultView.RowFilter = "FORM_NAME='" + sMap[i].ToString() + ".xml'";
						if(objDs.Tables[0].DefaultView.Count==0)
                            MDIRemoveBySysName(sMap[i]);
					}
				}
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.ApplyPowerViewPermissions.Error", m_iClientId), p_objEx);  
			}
		}
		#endregion

		// 01/17/2007 Rem Umesh

		/// Name			: AppendAttribute
		/// Date Created	: 17-jan-2007
		/// Created By		: Umesh Kusvaha
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		Appends  atribute to given node and set its value
		/// </summary>
		///  <param name="p_objSearchDocument">
		///		Input Xml from which the value of added attribute is decided.
		///	</param>
		/// <param name="p_objDocument">
		///		Input Xml in which attirbute has to be added.
		///	</param>
		/// <param name="p_sNodeSearchName">
		///		Node which decide the value of added attribute
		///	</param>
		///	<param name="p_sNodeName">
		///		Node to which the attribute has to be added
		///	</param>
		///	<param name="p_sAttributeName">
		///		name of attribute which is to be added 
		///	</param>
		

		private void AppendAttribute(XmlDocument p_objSearchDocument,XmlDocument p_objDocument,
			string p_sNodeSearchName,string p_sNodeName,string p_sAttributeName)
		{
			XmlNode objDataNode = null;
			XmlNode objSearch=null;
			XmlElement objDataElement=null;
			try
			{
				objDataNode =p_objDocument.SelectSingleNode(p_sNodeName);
				objDataElement=(XmlElement)objDataNode;
				
				objSearch=p_objSearchDocument.SelectSingleNode(p_sNodeSearchName);
				if (objSearch!=null)
				{
						
					if (objSearch.InnerText=="-1")
					{
						objDataElement.SetAttribute(p_sAttributeName,"1");
					}
					else
					{
						objDataElement.SetAttribute(p_sAttributeName,"0");
					}
						
				}

			}

			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.AppendAttribute.Error", m_iClientId), p_objEx);  
			}
			finally
			{
				objDataNode = null;
				objSearch=null;
				objDataElement=null;
			
			}

		}
        private void UpdateLabel(XmlDocument p_objSearchDocument, XmlDocument p_objDocument, string p_sNodeSearchName,string p_sDefaultNode, string p_sNodeName)
        {
            string sLabel = null;
            XmlNode objDataNode = null;
            XmlNode objSearch = null;
            try
            {
                objSearch = p_objSearchDocument.SelectSingleNode(p_sNodeSearchName);
                if(objSearch!=null)
                {
                sLabel = objSearch.InnerText;
                if (sLabel.Trim().Length == 0)
                {
                    objSearch = p_objSearchDocument.SelectSingleNode(p_sDefaultNode);
                    sLabel = objSearch.InnerText;
                }
                objDataNode = p_objDocument.SelectSingleNode("//entry[@id='" + p_sNodeName + "']/@text");
                if (objDataNode != null)
                    objDataNode.InnerText = sLabel;
                }

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPermissions.UpdateLabel.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objDataNode = null;
                sLabel = null;
            }

        }
		// End Rem 


        private void MDIUpdateTitle(string p_sNodeSearchName, string p_sDefaultNode, string sysName)
        {
            string sLabel = null;
            XmlNode objSearch = null;
            try
            {
                objSearch = this.objAdminConfDoc.SelectSingleNode(p_sNodeSearchName);
                if (objSearch != null)
                {
                    sLabel = objSearch.InnerText;
                    if (sLabel.Trim().Length == 0)
                    {
                        objSearch = this.objAdminConfDoc.SelectSingleNode(p_sDefaultNode);
                        if (objSearch != null)
                        {
                            sLabel = objSearch.InnerText;
                        }
                    }
                    if (sLabel.Trim().Length != 0)
                    {
                        IEnumerable <XElement> query = from c in this.MDIMenu.Descendants()
                                                      where c.Attribute("sysName") != null && c.Attribute("sysName").Value.ToLower() == sysName.ToLower()
                                                      select c;
                        XElement el = query.FirstOrDefault();
                        if (el != null)
                        {
                            //correct from "Title" to "title" for Mits 15455
                            el.SetAttributeValue("title", sLabel);
                        }

                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPermissions.UpdateLabel.Error", m_iClientId), p_objEx);
            }
            finally
            {
                sLabel = null;
            }

        }
        // End Rem 


		public  string ApplySecurityOnViewPath(string path ,UserLogin p_objUserLogin, ArrayList listOfPowerViews, int[] corrspondingPermissions)
		{

			if (path.IndexOf("fdm") >-1) 
			{
				if (path.IndexOf("SysCmd")== -1) // and there is no syscmd at present ....
				{


					string formname = path.Split('=')[1].Split('&')[0]; //get the form name					
					int i = listOfPowerViews.IndexOf(formname);
					
					if (i > -1)
					{
						if (!p_objUserLogin.IsAllowedEx(corrspondingPermissions[i],RMO_CREATE))
						{
							return path + "&SysCmd=1";
						}
						else
							return path;
					
					}
					else //cant do anything
						return path;

				}
				else
					return path;
			}
			else 
				return path;
		}
        public int[] GetPermissionsArray()
        {
            //Mridul 10/26/09 MITS#18230
            //int[] corrspondingPermissions = new int[16]; 
            //Start:Neha Suresh Jain, 06/01/2010,for claimpc, Added RMB_PC
            //int[] corrspondingPermissions = new int[17];
            int[] corrspondingPermissions = new int[18];
            corrspondingPermissions[17] = RMB_PC;
            corrspondingPermissions[16] = RMB_PROPERTYMAINT;
            corrspondingPermissions[0] = RMB_EV;
            corrspondingPermissions[1] = RMB_GC;
            corrspondingPermissions[2] = RMB_WC;
            corrspondingPermissions[3] = RMB_VA;
            corrspondingPermissions[4] = RMB_DI;
            corrspondingPermissions[5] = RMB_POLMGT;
            corrspondingPermissions[6] = RMB_PATIENT_TRACKING;
            corrspondingPermissions[7] = RMB_VEHMAINT;
            corrspondingPermissions[8] = RMB_PEOPMAINT;
            corrspondingPermissions[9] = RMB_EMPMAINT;
            corrspondingPermissions[10] = RMB_ENTITIES;
            corrspondingPermissions[11] = RMB_PHYMAINT;
            corrspondingPermissions[12] = RMB_DIS_PLAN;
            corrspondingPermissions[13] = RMB_STAFFMAINT;
            corrspondingPermissions[14] = RMB_FUNDS_BNKACCT;
            corrspondingPermissions[15] = RMB_FUNDS_DEPOSIT; 
            corrspondingPermissions[16] = RMO_DRIVER_MAIN;  //Aman Driver Enh
            return corrspondingPermissions;
        }

	public  ArrayList GetListOfPowerViews()
	{
		ArrayList listOfPowerViews  = new ArrayList();						
		listOfPowerViews.Add("event");
		listOfPowerViews.Add("claimgc");
		listOfPowerViews.Add("claimwc");
		listOfPowerViews.Add("claimva");
		listOfPowerViews.Add("claimdi");
		listOfPowerViews.Add("policy");
		listOfPowerViews.Add("patient");
		listOfPowerViews.Add("vehicle");
		listOfPowerViews.Add("people");
		listOfPowerViews.Add("employee");
		listOfPowerViews.Add("entitymaint");
		listOfPowerViews.Add("physician");
		listOfPowerViews.Add("plan");
		listOfPowerViews.Add("staff");
		listOfPowerViews.Add("bankaccount");
		listOfPowerViews.Add("deposit");
        //Anu Tennyson for MITS 18291 :STARTS  10/25/2009
        listOfPowerViews.Add("propertyunit");
        //Anu Tennyson for MITS 18291 : ENDS
        //Start:Neha Suresh Jain, 06/01/2010,added claimpc
        listOfPowerViews.Add("claimpc");
        listOfPowerViews.Add("driver");  //Aman Driver Enh
        //End:Neha Suresh Jain, 06/01/2010
		return listOfPowerViews;
	}

    /// Name: SecureMDIMenu
    /// Author: Nima Norouzi/FSG/CSC 
    /// Date: March 2009
    /// <sumxmary>
    /// Removes not-permited items from base MDIMenu.
    /// </summary>
    public void MDISecureMenu(ref XElement MDIMenu, UserLogin p_objUserLogin, int p_iViewId, int p_iDsnId)
    {
        bool blnAllowed = false;
        this.MDIMenu = MDIMenu;
        XmlNode objTemlElement = null;
        string sInnerText = "";
        //BSB\JP 09.06.2006 This temp login feature kills performance at clients.
        // It was added so that SMS permissions changes take effect w/o the user logging off first.
        // This is not worth the performance penalty we're seeing. Hence I'm removing this.
        // If this is indeed a "can't live without" feature then we need to look at SMS to 
        // update the userlogin object that is in Session at the time the perm changes are being made.
        // Logging in repeatedly with every nav-tree fetch is ludicrous.
        //UserLogin objTempLogin=null;
        int iAll = 1;
        //XmlNode objSessionDSN=null;
        //XmlNode objBRSInstalled = null;
        string sView = "";
        bool bIsPwdPolicyEnabled = false;
        bool bIsSMDesignerEnabled = false;
        //zmohammad : JIRA # RMA-2961, RMA-2965 Specific key added for SMreports
        bool bIsSMReportEnabled = false;
        //Raman 03/08/2009 : R6 Work Loss / Restriction
        bool bAutoCrtWrkLossRest = false;
        bool bIsSMSPermittedUser = false; //Flag to keep track of SMS permitted user.--Tanuj
        //skhare7 MITS:22374
        bool bIsUPSPermittedUser = false; //User Pri Setup access skhare7
        //skhare7 MITS:22374
        //bool bIsBRSInstalled = false;// Read BRS Installed setting from Settings.
        //bool bIsBRSEnabled = false;// Read BRS Enabled setting from Riskmaster.Config.
        //Anu Tennyson for MITS 18229 STARTS
        int iEnabledCount = 0;
        //Anu Tennyson for MITS 18229 ENDS
        //Start(07/05/2010): Sumit
        SysSettings oSettings=null;
        //End:Sumit
        string sSQL = null; //Amitosh for R8 enhancement
        bool bPolicyInterface = false;
        //Manika : MITS 26407
        //Added By nitika for TPA setting
        bool bTPA = false;
        //Added By nitika for TPA setting
        //added by swati for DCI and CLUE 
        bool bDCI = false;
        bool bCLUE = false;
        //change end here by swati
        bool bIsSortMasterEnabled = p_objUserLogin.IsAllowedEx(RMO_SORTMASTER);
        bool bIsStandardReportEnabled = p_objUserLogin.IsAllowedEx(RMO_STANDARD_REPORT);
		//atavaragiri :MITS 29341 //
		bool isBIEnabled = p_objUserLogin.IsAllowedEx(RMO_BI);
		bool isBatchPrintingEnabled=p_objUserLogin.IsAllowedEx(RMO_FROI_ACORD);
		bool isExecSummaryEnabled = p_objUserLogin.IsAllowedEx(RMO_EXEC_SUMMARY);
		bool isOshaReportEnabled = p_objUserLogin.IsAllowedEx(RMO_OSHA_REPORT);
		bool isSortMasterReportEnabled = p_objUserLogin.IsAllowedEx(RMO_SORTMASTER_REPORT); 
		// end:MITS 29341 //
        bool isCloudDeployed = (Convert.ToBoolean(RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower()))?true:false;//rkaur27 : RMACLOUD -2383
        //bool bIsSortMasterDotNetEnabled = p_objUserLogin.IsAllowedEx(RMO_SORTMASTERDOTNET);
        //MITS 26407 End
        // Payal: RMA-7586-- Starts
        LobSettings oLobSettings = null;
        ColLobSettings oColLobSettings = null;
        int iLob = 0;
        bool bUseSupAppReserves = false;
        DbReader objReader = null;
        bool bUseReserveWorksheet = false;
        DataTable dtLobList = null;
        LocalCache objCache = null;
        int iTableId = 0;
        // Payal: RMA-7586 -- Ends
        try
        {
            /// TR#1158 Pankaj 01/03/06 Get viewid from xml before its removed

            //BSB 01.26.2007 Adding Support for Riskmaster.config Extensibility section
            // and NavTreeMapping elements. 
            // Note: Add is performed first (while we can still add "before" and "after" nodes
            // that may be removed in normal processing.
            // Remove and Replace are completed later since a replaced node may get a 
            // new id value and we want the regular system to be able to control display 
            // based on the original id.

            //CustomNavTree.AddNodes(p_objDocument);

            //Start-Added the following code for Enhance security--Tanuj
            SMSUser objSMSUser = null;
            try
            {
                //MITS-10662 - Changed the query to make it user_id dependent
                //objSMSUser = new SMSUser( p_objUserLogin.LoginName ) ;					
                //bIsSMSPermittedUser = objSMSUser.IsSMSPermittedUser( p_objUserLogin.LoginName );
                //objSMSUser = new SMSUser(-p_objUserLogin.UserId);
                //if (objSMSUser.RecordId != 0 && objSMSUser.RmLoginName != "")
                //    bIsSMSPermittedUser = true;
                //else
                //{
                //    objSMSUser = new SMSUser(p_objUserLogin.UserId);
                   // bIsSMSPermittedUser = objSMSUser.IsSMSPermittedUser(p_objUserLogin.UserId);
                //}
                //if (objSMSUser.RecordId != 0 && objSMSUser.RmLoginName != "")
                //    bIsUPSPermittedUser = true;
                //else
                //{
                    //objSMSUser = new SMSUser(p_objUserLogin.UserId);

                //smishra25:Commented the code above and setting both the boolean variables in one call
                   objSMSUser = new SMSUser(p_objUserLogin.UserId, m_iClientId);
                    bIsUPSPermittedUser = objSMSUser.IsSMSAndUPSPermittedUser(p_objUserLogin.UserId, out bIsSMSPermittedUser);
                //}
            }
            finally
            {
                objSMSUser = null;
            }

            //Start(07/05/2010): Sumit - Object to refer Syssettings.
            oSettings = new SysSettings(p_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
            //End: Sumit
            ////Deb:BOB//skhare7 R8 In case of carrier claim Transaction amd auto check screen should be visible in menu
            //if (oSettings.MultiCovgPerClm == -1)
            //{
            //    this.MDIRemoveBySysName("funds");
            //    this.MDIRemoveBySysName("autoclaimchecks");
            //}
            //rkatyal: 7767 start
            if (oSettings.UseEntityRole == true)
            {
                this.MDIRemoveBySysName("people");
                this.MDIRemoveBySysName("driver");
                this.MDIRemoveBySysName("employee");
                this.MDIRemoveBySysName("patient");
                this.MDIRemoveBySysName("physician");
                this.MDIRemoveBySysName("staff");
            }
            //rkatyal: 7767 end

            if (oSettings.UseMultiCurrency != -1)
            {
                this.MDIRemoveBySysName("MultiCurrencySetup");
            }

            //mbahl3 strataware enhancement
            if (oSettings.StrataWare != -1)
            {
                this.MDIRemoveBySysName("StrataWare");
            }
            //mbahl3 strataware enhancement
            //Start averma62 - Enable VSS setting
            if (oSettings.EnableVSS != -1)
            {
                this.MDIRemoveBySysName("VSSInterfaceLog");
            }
            //End averma62 - Enable VSS setting

            ////Start Pradyumna MITS 36013 6/6/2014 
            //if (oSettings.UseStagingPolicySystem == true)
            //{
            //    this.MDIRemoveBySysName("PolicySystemSetup");
            //}
            //else
            //{
            //    this.MDIRemoveBySysName("PolicySearchConfig");
            //}
            ////End - Pradyumna MITS 36013 6/6/2014 
            bIsPwdPolicyEnabled = PublicFunctions.IsPwdPolEnabled();

            //p_objDocument.SelectSingleNode("//IsEnhSecurityEnabled").InnerText = bIsPwdPolicyEnabled.ToString();
            if (!bIsPwdPolicyEnabled || !bIsSMSPermittedUser)
            {
                this.MDIRemoveBySysName("PasswordPolicyParameters");
                this.MDIRemoveBySysName("PwdPolUnlockUserAccnt");
            }


            bIsSMDesignerEnabled = PublicFunctions.IsSMDesignerEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
            if (!bIsSMDesignerEnabled)
            {
                this.MDIRemoveBySysName("StdReportsDesigners");
            }

            //zmohammad : JIRA # RMA-2961, RMA-2965 Specific key added for SMreports
            bIsSMReportEnabled = PublicFunctions.IsSMReportEnabled();
            if (!bIsSMReportEnabled)
            {
                this.MDIRemoveBySysName("StdReportsQueue");
                this.MDIRemoveBySysName("ReportCustom");
            }

            // rsolanki2:- (20/11/2007) - R4 optional frameset enhancement
            // removing the Portalsettings node if the logegd in iser is not a 'csc' 
            if (!bIsSMSPermittedUser)
            {
                this.MDIRemoveBySysName("PortalSettings");
            }

            // rsolanki2:- (18/02/2008) - R4 Domain authentication enhancement
            // removing the Portalsettings node if Domain authentication is enabled 
            if (Security.bIsDomainAuthenticationEnabled())
            {
                this.MDIRemoveBySysName("ChangePassword");
            }
            bool bIsScriptEditorEnabled = PublicFunctions.IsScriptEditorEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
            if (!bIsScriptEditorEnabled)
            {
                this.MDIRemoveBySysName("ScriptEditor");
            }
            //End-Added the following code for Enhance security--Tanuj
            //Manika : MITS 26407 
            if (!bIsSortMasterEnabled && !bIsStandardReportEnabled)
            {
                this.MDIRemoveBySysName("ReportsRoot");
            }
            //MITS 26407 End

			// atavaragiri :MITS 29341//
			if (!bIsStandardReportEnabled)
			{
				this.MDIRemoveBySysName("BISRedirect");
				this.MDIRemoveBySysName("batchprinting");
				this.MDIRemoveBySysName("ExecSummaries");
				this.MDIRemoveBySysName("postosha300");
				this.MDIRemoveBySysName("postosha301");
				this.MDIRemoveBySysName("postosha300A");
				this.MDIRemoveBySysName("postoshasharps");
				this.MDIRemoveBySysName("OtherReports");
				
			}


			if(!isBIEnabled)
				this.MDIRemoveBySysName("BISRedirect");
			if(!isBatchPrintingEnabled)
				this.MDIRemoveBySysName("batchprinting");
			if(!isExecSummaryEnabled)
				this.MDIRemoveBySysName("ExecSummaries");
			if (!isOshaReportEnabled )
			{
				this.MDIRemoveBySysName("postosha300");
				this.MDIRemoveBySysName("postosha301");
				this.MDIRemoveBySysName("postosha300A");
				this.MDIRemoveBySysName("postoshasharps");
				
			}

            // If OSHA and SORTMASTER are unchecked,Standard Report Queue should not be visible.
            //zmohammad RMA-5691/MITS 37433 : Splitting their control. Start
            if (!isSortMasterReportEnabled)
            {
                this.MDIRemoveBySysName("StdReportsQueue");
                this.MDIRemoveBySysName("StdReportsDesigners");
            }

            if (!isOshaReportEnabled)
            {
                this.MDIRemoveBySysName("OshaReportsQueue");
                this.MDIRemoveBySysName("OshaReports");
            }
            //zmohammad RMA-5691/MITS 37433 : Splitting their control. End
            // If Sortmaster is unchecked,Std Reports Designer should not be visible//
            // if (!isSortMasterReportEnabled)
            //    this.MDIRemoveBySysName("StdReportsDesigners");


			// end:MITS 29341 //
            //rkaur27 : RMACLOUD -2383 : Start - Upload files option should only be visible in multi-tenant environment
            if (!(isCloudDeployed && m_iClientId != 0))
            {
                this.MDIRemoveBySysName("UploadFiles");
            }
            //rkaur27 : RMACLOUD -2383 : End

            //JIRA RMA-7532 pgupta93:Start
            if (oSettings.MultiCovgPerClm == -1)
            {
                this.MDIRemoveBySysName("AddPaymentsToCoverages");
                //abahl3 starts, rma 111468,11121,11120
                this.MDIRemoveBySysName("autoclaimchecks");
                this.MDIRemoveBySysName("funds");
                //abahl3 ends, rma 111468,11121,11120
            }
            //JIRA RMA-7532 pgupta93:End 


            // Naresh Code commented as It was not working properly. Code added to make it work
            /*
            //Divya -MITS 8974 Check for Enhanced Policy  - Start
            bool bIsEnhPolEnabled = PublicFunctions.IsEnhPolEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString);
            if (!bIsEnhPolEnabled)
            {
                this.MDIRemoveBySysName("enhpolicy-tab");
                this.MDIRemoveBySysName("policybilling-tab");
                this.MDIRemoveBySysName("EnhancedPolicySetup");
                this.MDIRemoveBySysName("BillingSystemSetup");

            }
            else
            {
                this.MDIRemoveBySysName("policy-tab");
            }

            //Divya - MITS 8974 Check for Enhance Policy  - End


            //Shruti - Check for Billing System Flag - Start
            bool bIsBillingSysEnabled = PublicFunctions.IsBillingSysEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString);
            if (!bIsBillingSysEnabled)
            {
                this.MDIRemoveBySysName("policybilling-tab");
                this.MDIRemoveBySysName("BillingSystemSetup");
            }
            //Shruti - Check for Billing System Flag - End
             * */

            // Naresh Code added for Implementing the changes in the Nav Tree when the 
            // Billing and Enhanced Policy is enabled or disabled
            bool bIsEnhPolEnabled = PublicFunctions.IsEnhPolEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
            bool bIsBillingSysEnabled = PublicFunctions.IsBillingSysEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
            //Anu Tennyson for MITS 18229-STARTS- 11/23/2009
            int iIsEnhPolOrPolTrack = PublicFunctions.iCtrEnhPolEnabled4LOBs;
            bool bIsPCEnhPolEnabled = PublicFunctions.bIsPCPolEnhEnabled;
            bool bIsWCEnhPolEnabled = PublicFunctions.bIsWCPolEnhEnabled;
            bool bIsGLEnhPolEnabled = PublicFunctions.bIsGLPolEnhEnabled;
            bool bIsALEnhPolEnabled = PublicFunctions.bIsALPolEnhEnabled;
            bool bIsChecked = true;
            bool bIsGLEnhPolAllowed = p_objUserLogin.IsAllowedEx(RMB_POLMGTGL_ENH);
            bool bIsALEnhPolAllowed = p_objUserLogin.IsAllowedEx(RMB_POLMGTAL_ENH);
            bool bIsPCEnhPolAllowed = p_objUserLogin.IsAllowedEx(RMB_POLMGTPC_ENH);
            bool bIsWCEnhPolAllowed = p_objUserLogin.IsAllowedEx(RMB_POLMGTWC_ENH);

            //Start(07/05/2010): Sumit - LOB Flags to check if enabled from Utilities.
            bool bIsClaimGCEnabled = oSettings.UseGCLOB;
            bool bIsClaimDIEnabled = oSettings.UseDILOB;
            bool bIsClaimPCEnabled = oSettings.UsePCLOB;
            bool bIsClaimVAEnabled = oSettings.UseVALOB;
            bool bIsClaimWCEnabled = oSettings.UseWCLOB;
            //End : Sumit
            bPolicyInterface = oSettings.UsePolicyInterface;
            bTPA = oSettings.UseTPA;

            //added by swati for DCI and CLUE
            bDCI = oSettings.UseDCIReportingFields;
            bCLUE = oSettings.UseCLUEReportingFields;
            //change end here
            //Start(09/27/2010):MITS# 22407 Sumit - Billing flag to check if enabled from SMS.
            bool bIsBillingAllowed = p_objUserLogin.IsAllowedEx(RMB_POLICY_BILLING);
            bool bIsBatchesAllowed = p_objUserLogin.IsAllowedEx(RMB_BATCHES);
            //End:Sumit
            this.MDIRemoveBySysName("policyenh"); // Policy Management 
            
            //Added by Nitika for TPA
            if (!bTPA && !bCLUE && !bDCI)
            {
                this.MDIRemoveBySysName("TPASetup");
                this.MDIRemoveBySysName("TPACodeMapping");
            }
            //Added by Nitika for TPA

            //Payal: RMA-7586 --Starts

            oColLobSettings = new ColLobSettings(p_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
            objCache = new LocalCache(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
            iTableId = objCache.GetTableId("LINE_OF_BUSINESS");
            dtLobList = GetLOBList(p_objUserLogin, iTableId);

            for (int i = 0; i < dtLobList.Rows.Count; i++)
            {
                switch (dtLobList.Rows[i]["SHORT_CODE"].ToString())
                {
                    case "GC":
                        if (bIsClaimGCEnabled)
                        {

                            bUseReserveWorksheet = oColLobSettings[objCache.GetCodeId("GC", "LINE_OF_BUSINESS")].UseReserveWorkSheet;
                        }
                        break;
                    case "DI":
                        if (bIsClaimDIEnabled)
                        {

                            bUseReserveWorksheet = oColLobSettings[objCache.GetCodeId("DI", "LINE_OF_BUSINESS")].UseReserveWorkSheet;
                        }
                        break;
                    case "PC":
                        if (bIsClaimPCEnabled)
                        {
                            bUseReserveWorksheet = oColLobSettings[objCache.GetCodeId("PC", "LINE_OF_BUSINESS")].UseReserveWorkSheet;
                        }
                        break;
                    case "VA":
                        if (bIsClaimVAEnabled)
                        {

                            bUseReserveWorksheet = oColLobSettings[objCache.GetCodeId("VA", "LINE_OF_BUSINESS")].UseReserveWorkSheet;
                        }
                        break;
                    case "WC":
                        if (bIsClaimWCEnabled)
                        {

                            bUseReserveWorksheet = oColLobSettings[objCache.GetCodeId("WC", "LINE_OF_BUSINESS")].UseReserveWorkSheet;
                        }
                        break;
                }
                if (bUseReserveWorksheet)
                {
                    break;
                }
            }
            if (!bUseReserveWorksheet)
            {
                //pgupta93: RMA-8253 :RMA-9874 START
                //Payal: RMA:9142 starts
               // this.MDIRemoveBySysName("ReserveWorkSheet");
               // this.MDIRemoveByParams("Title", "ReserveWorkSheet", "trans");
                //Payal: RMA:9142 ends

                this.MDIRemoveBySysName("ReserveWorkSheetApprove");
                this.MDIRemoveBySysName("MyPendingReserveWorkSheet");
                //pgupta93: RMA-8253 :RMA-9874 END
            }

            //RMA-13175: start
            //sSQL = "SELECT USE_SUP_APP_RESERVES FROM CHECK_OPTIONS";
            //objReader = DbFactory.GetDbReader(p_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL.ToString());
            //using (objReader)
            //{
            //    if (objReader.Read())
            //    {
            //        bUseSupAppReserves = objReader.GetBoolean("USE_SUP_APP_RESERVES");
            //    }
            //}
            //if (!bUseSupAppReserves)
            //{
            //    //pgupta93: RMA-8253 :RMA-9874 START
            //    //Payal: RMA:9142 starts
            //    this.MDIRemoveBySysName("Reserves");
            //    //this.MDIRemoveByParams("Title", "Reserves", "trans");
            //    //Payal: RMA:9142 ends  
            //}
            //Payal: RMA-7586 --Ends
            //RMA-13175: end

            //Added by Amitosh for mits 25163 for policy interface
            if (!bPolicyInterface)
            {
                this.MDIRemoveBySysName("PolicySystemSetup");
            //    this.MDIRemoveBySysName("PolicyBatchFile");
                this.MDIRemoveBySysName("policysysdownload");
                this.MDIRemoveBySysName("PolicyCodeMapping");   

            }
            //else if(oSettings.PolicySystemUpdate != 1)  // PolicySystemUpdate == 1 means Batch Processing is selected for Policy Update
                this.MDIRemoveBySysName("PolicyBatchFile");
                //End Amitosh

            //MITS 33371 ajohari2: Start - This tab will be only available when BES - ON and External Policy - ON
            if (p_objUserLogin.objRiskmasterDatabase.OrgSecFlag)
            {
                if (!bPolicyInterface || !oSettings.EnableTPAAccess) //JIRA RMA-718 ajohari2
                    this.MDIRemoveBySysName("PolicySearchDataMapping");
            }
            else
            {
                this.MDIRemoveBySysName("PolicySearchDataMapping");
            }
            //MITS 33371 ajohari2: End

            //JIRA RMA-8753 nshah28 start(Remove "Address" submenu if set false in General Sys para setup)
            if (!oSettings.AddModifyAddress)
            {
                this.MDIRemoveBySysName("Address"); //Remove from maintanance menu
                this.MDIRemoveBySysName("saddress"); //Remove from search menu
            }
            //JIRA RMA-8753 nshah28 end
            if (!bIsALEnhPolAllowed && !bIsGLEnhPolAllowed && !bIsPCEnhPolAllowed && !bIsWCEnhPolAllowed)
            {
                bIsChecked = false;
                this.MDIRemoveBySysName("spolicyenh");
                this.MDIRemoveBySysName("PolicyRoot");

                //Start(08/16/2010): Sumit - Disable Policy Setup when Enhanced policy is disabled for all LOBs. 
                //Changed by Amitosh for Mits 25163
                
                this.MDIRemoveBySysName("PolicyOptions");
                if(!bPolicyInterface)
                this.MDIRemoveBySysName("PolicySetup");
                //this.MDIRemoveBySysName("PolicyOptions");
                this.MDIRemoveBySysName("policybilling");
                this.MDIRemoveBySysName("Batches");
                this.MDIRemoveBySysName("BillingScheduler");
                this.MDIRemoveBySysName("BillingOptions"); // Billing System Setup
                //End : Sumit
            }
            if (bIsChecked)
            {
                if (iIsEnhPolOrPolTrack == 0)
                {
                    this.MDIRemoveBySysName("spolicyenh");
                    this.MDIRemoveBySysName("PolicyRoot");
                    //Start(08/16/2010): Sumit - Disable Policy Setup when Enhanced policy is disabled for all LOBs. 
                    //Changed by Amitosh for Mits 25163
                    this.MDIRemoveBySysName("PolicyOptions");
                    if (!bPolicyInterface)
                    this.MDIRemoveBySysName("PolicySetup");
                    //this.MDIRemoveBySysName("PolicyOptions"); // Policy Management Setup
                    this.MDIRemoveBySysName("policybilling"); // Policy Billing
                    this.MDIRemoveBySysName("BillingOptions"); // Billing System Setup
                    this.MDIRemoveBySysName("BillingScheduler"); // Billing Scheduler
                    this.MDIRemoveBySysName("Batches"); // Batches
                    //End : Sumit
                }
                else if (iIsEnhPolOrPolTrack == 4)
                {
                    this.MDIRemoveBySysName("spolicy");
                    this.MDIRemoveBySysName("policy");
                    //Start(09/27/2010):Sumit - MITS# 22407
                    //if (!bIsBillingSysEnabled)
                    if (!bIsBillingSysEnabled || !bIsBillingAllowed)
                    //End:Sumit
                    {
                        this.MDIRemoveBySysName("policybilling");
                        this.MDIRemoveBySysName("Batches");
                        this.MDIRemoveBySysName("BillingScheduler");
                        this.MDIRemoveBySysName("BillingOptions"); // Billing System Setup
                    }
                }
                //Start(09/27/2010):Sumit - MITS# 22407
                //else if (!bIsBillingSysEnabled)
                else if (!bIsBillingSysEnabled || !bIsBillingAllowed)
                //End:Sumit
                {
                    this.MDIRemoveBySysName("policybilling");
                    this.MDIRemoveBySysName("Batches");
                    this.MDIRemoveBySysName("BillingScheduler");
                    this.MDIRemoveBySysName("BillingOptions"); // Billing System Setup
                }
                //Start(09/27/2010):Sumit - MITS# 22407
                //Sumit (10/20/2010) - Modified the condition
                if (iIsEnhPolOrPolTrack > 0 && bIsBillingSysEnabled && bIsBillingAllowed)
                {
                    if (!bIsBatchesAllowed)
                    {
                        this.MDIRemoveBySysName("Batches");
                    }
                }
                //End:Sumit
            }

            if (!bIsPCEnhPolAllowed || !bIsPCEnhPolEnabled)
            {
                this.MDIRemoveBySysName("policyenhpc");
                iEnabledCount++;
            }
            if (!bIsWCEnhPolAllowed || !bIsWCEnhPolEnabled)
            {
                this.MDIRemoveBySysName("policyenhwc");
                iEnabledCount++;
            }
            if (!bIsALEnhPolAllowed || !bIsALEnhPolEnabled)
            {
                this.MDIRemoveBySysName("policyenhal");
                iEnabledCount++;
            }
            if (!bIsGLEnhPolAllowed || !bIsGLEnhPolEnabled)
            {
                this.MDIRemoveBySysName("policyenhgl");
                iEnabledCount++;
            }
            if (iEnabledCount == 4)
            {
                this.MDIRemoveBySysName("spolicyenh");
                this.MDIRemoveBySysName("PolicyRoot");
                //Start(08/16/2010): Sumit - Disable Policy Setup when Enhanced policy is disabled for all LOBs. 
                //Changed by Amitosh for Mits 25163
                this.MDIRemoveBySysName("PolicyOptions");
                if (!bPolicyInterface)
                this.MDIRemoveBySysName("PolicySetup");
                //this.MDIRemoveBySysName("PolicyOptions");
                this.MDIRemoveBySysName("policybilling");
                this.MDIRemoveBySysName("Batches");
                this.MDIRemoveBySysName("BillingScheduler");
                this.MDIRemoveBySysName("BillingOptions"); // Billing System Setup
                //End : Sumit
            }
            //Anu Tennyson for MITS 18229-ENDS- 11/23/2009

            //Raman 03/08/2009 : R6 Work Loss / Restriction
            //Start(07/05/2010): Sumit - Moving this function on the top so that it can be used throughout this block.
            //SysSettings oSettings = new SysSettings(p_objUserLogin.objRiskmasterDatabase.ConnectionString);
            //End: Sumit
            bAutoCrtWrkLossRest = oSettings.AutoCrtWrkLossRest;

            //Remove Work Loss/Restrictions Mapping Screen if module is turned off
            if (!bAutoCrtWrkLossRest)
            {
                this.MDIRemoveBySysName("WorkLossRestrictions");
            }
            //Start(07/05/2010): Sumit - This object will get disposed in finally block.
            //oSettings = null;
            //End:Sumit

            // Remove SMS and related items from Navtree if user lacks SMS rights
            if (!bIsSMSPermittedUser)//Flag to keep track of SMS permitted user.If user is permitted to access SMS then let the user have the permissions to access BES and UserPriviligesSetup as well.
            {
                this.MDIRemoveBySysName("ReportCustom"); // Take out Reports Administration Section
                this.MDIRemoveBySysName("SecurityMgtSystem"); // SMS
                this.MDIRemoveBySysName("BES"); // BusinessEntitySecurity
               // this.MDIRemoveBySysName("UserPrivileges"); // User Priviliges Setup
            }
            if (!bIsSMSPermittedUser)//Flag to keep track of SMS permitted user.If user is permitted to access SMS then let the user have the permissions to access BES and UserPriviligesSetup as well.
            {
                this.MDIRemoveBySysName("ReportCustom"); // Take out Reports Administration Section
                this.MDIRemoveBySysName("SecurityMgtSystem"); // SMS
                this.MDIRemoveBySysName("BES"); // BusinessEntitySecurity
                // this.MDIRemoveBySysName("UserPrivileges"); // User Priviliges Setup
            }
            //skhare7 MITS:22374
            if (!bIsUPSPermittedUser)//Flag to keep track of SMS permitted user.If user is permitted to access SMS then let the user have the permissions to access BES and UserPriviligesSetup as well.
            {
              
                 this.MDIRemoveBySysName("UserPrivileges"); // User Priviliges Setup
            }
            //skhare7 MITS:22374
            /* not for MDI
            objTemlElement = p_objDocument.SelectSingleNode("//parameter/name[.='View']/../value");
            if (objTemlElement != null)
                sView = objTemlElement.InnerText;

            objTemlElement = p_objDocument.SelectSingleNode("//parameter/name[.='DataSource']/../value");
            //objTempLogin=new UserLogin(p_objUserLogin.LoginName,p_objUserLogin.Password,objTemlElement.InnerText);
            objTemlElement = p_objDocument.SelectSingleNode("//toc");
            sInnerText = "<toc>" + objTemlElement.InnerXml + "</toc>";
            objAdminConfDoc = new XmlDocument();
            objAdminConfDoc.InnerXml = sInnerText;
            p_objDocument = new XmlDocument();
            p_objDocument = objAdminConfDoc;
            //p_objUserLogin=objTempLogin;
            */

            blnAllowed = p_objUserLogin.IsAllowedEx(RMB_WC);
            //Start(07/05/2010): Sumit - Included LOB Flag to see if enabled from Utilities as well.
            if (!blnAllowed || !bIsClaimWCEnabled)
            {
                MDIRemoveBySysName("claimwc");
            }
            //End:Sumit

            blnAllowed = p_objUserLogin.IsAllowedEx(RMB_VA);
            //Start(07/05/2010): Sumit - Included LOB Flag to see if enabled from Utilities as well.
            if (!blnAllowed || !bIsClaimVAEnabled)
            {
                MDIRemoveBySysName("claimva");
            }
            //End: Sumit

            //Start:Neha Suresh Jain, 06/01/2010,for claimpc
            blnAllowed = p_objUserLogin.IsAllowedEx(RMB_PC);
            //Start(07/05/2010): Sumit - Included LOB Flag to see if enabled from Utilities as well.
            if (!blnAllowed || !bIsClaimPCEnabled)
            {
                MDIRemoveBySysName("claimpc");
            }
            //End: Sumit

            //End:Neha Suresh Jain, 06/01/2010
            blnAllowed = p_objUserLogin.IsAllowedEx(RMB_DI);
            //Start(07/05/2010): Sumit - Included LOB Flag to see if enabled from Utilities as well.
            if (!blnAllowed || !bIsClaimDIEnabled)
            {
                MDIRemoveBySysName("claimdi");
            }
            //End: Sumit

            blnAllowed = p_objUserLogin.IsAllowedEx(RMB_GC);
            //Start(07/05/2010): Sumit - Included LOB Flag to see if enabled from Utilities as well.
            if (!blnAllowed || !bIsClaimGCEnabled)
            {
                MDIRemoveBySysName("claimgc");
            }
            //End: Sumit

            if (!bIsClaimDIEnabled && !bIsClaimPCEnabled && !bIsClaimVAEnabled && !bIsClaimWCEnabled && !bIsClaimGCEnabled)
            {
                MDIRemoveBySysName("LOBParameters");
            }
            //End: Sumit

            //OFAC CHECK - Payee Check Review Page  - START
            blnAllowed = p_objUserLogin.IsAllowed(RMB_ALLOW_PAYEE_CHECK_REVIEW);

            if (!blnAllowed)
            {
                MDIRemoveBySysName("PayeeCheckReview");
            }
            //END

            // npadhy RMA-11632 - As we are now moving the Print Options based on Distrbution Type and different distribution type can have different Print Media (Printer or File)
            // So we remove this condition. Rather on Recreate Checks screen we validate if the Distribution Type Correspond to "To Printer Only", then we show error
            //Ashutosh
            //if (PublicFunctions.IsPrintCheckFileOptIsPrinterOnly(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId))
            //{
            //    MDIRemoveBySysName("ReCreateCheck");
            //}

            if (p_objUserLogin.objRiskmasterDatabase.Status == true)
            {
            
                blnAllowed = p_objUserLogin.IsAllowed(RMB_RISKMASTER);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("SearchRoot");
                    MDIRemoveBySysName("DiariesRoot");
                    MDIRemoveBySysName("MaintenanceRoot");
                    MDIRemoveBySysName("UserDocumentsRoot");
                    MDIRemoveBySysName("FundsRoot");
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_USERDOC) && p_objUserLogin.IsAllowed(RMB_USERDOC_VIEW);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("DocumentList");
                }
                //spahariya MITS 28867
                blnAllowed = p_objUserLogin.IsAllowed(RMB_WMEMAIL_DET) && p_objUserLogin.IsAllowed(RMB_WMEMAIL_DET_VIEW);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("WordMergeEmailDetails");
                }
                //spahariya -End

                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_WC, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_WC, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "wc-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "wc", "url", "&SysCmd=1");
                    iAll = 0;
                }
                else
                {
                    iAll = 0;
                }
                 */

                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_VA, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_VA, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "va-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "va", "url", "&SysCmd=1");
                    iAll = 0;
                }
                else
                {
                    iAll = 0;
                }
                 */

                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_PC, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PC, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "pc-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "pc", "url", "&SysCmd=1");
                    iAll = 0;
                }
                else
                {
                    iAll = 0;
                }
                 */

                /*else if (p_objUserLogin.IsAllowedEx(RMB_DI, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_DI, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "nonoc-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "nonocc", "url", "&SysCmd=1");
                    iAll = 0;
                }
                else
                {
                    iAll = 0;
                }*/

                /*else if (p_objUserLogin.IsAllowedEx(RMB_GC, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_GC, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "claimgc-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "claimgc", "url", "&SysCmd=1");
                    iAll = 0;
                }
                else
                {
                    iAll = 0;
                }*/

                //Start(07/08/2010): Sumit - Remove LOB Parameters setup if all LOBs are disabled from Utilities.


                blnAllowed = p_objUserLogin.IsAllowed(RMB_EV);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("event");
                }
                /*else if (p_objUserLogin.IsAllowedEx(RMB_EV, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_EV, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "event-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "event", "url", "&SysCmd=1");
                    iAll = 0;
                }
                else
                {
                    iAll = 0;
                }
                 */
                blnAllowed = p_objUserLogin.IsAllowed(RMB_ADMIN_TRACK);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("admintrackinglist");
                }
                /* Not for MDI?
                if (iAll == 1)
                {
                    MDIRemoveBySysName("Topdown");
                }
                */
                blnAllowed = p_objUserLogin.IsAllowed(RMO_MERGE_SETUP);//TR 2286 commented By Nitesh 06 April 2006 && p_objUserLogin.IsAllowed(RMO_MERGE_CREATE);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("MergeTemplates"); // Word Merge Setup
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("SearchRoot");
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_CLAIM_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sclaim"); // claim search
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_EVENT_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sevent"); // event search
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_EMPLOYEE_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("semployee");
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_ENTITY_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sentity");
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_VEHICLE_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("svehicle");
                }
                //Rakhi Mits 17929:Changed Code for Policy Search as per Policy and EnhPol Checkbox(Can be implemented with a single checkbox but for now keep the existing)
                if (bIsEnhPolEnabled)
                {
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_ENHPOLICY_SEARCH);
                    if (!blnAllowed)
                        MDIRemoveBySysName("spolicyenh");
                }
                else
                {
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_POLICY_SEARCH);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("spolicy");
                        //smishra54: MITS 25163- Policy Interface Implementation
                        MDIRemoveBySysName("policysysdownload");
                        //smishra54: end
                    }
                }
                //Rakhi Mits 17929:Changed Code for Policy Search as per Policy and Enh Pol Checkbox(Can be implemented with a single checkbox but for now keep the existing)

                blnAllowed = p_objUserLogin.IsAllowed(RMB_PAYMENT_SEARCH);
                if (!blnAllowed)
                {
                    //Rakhi Mits 17929:Corrected Node Name for Search Menu
                    //MDIRemoveBySysName("spayment"); // fund search
                     MDIRemoveBySysName("sfunds");
                    //Rakhi Mits 17929:Corrected Node Name for Search Menu
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_PATIENT_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("spatient");
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_DRIVER_SEARCH);  //Aman Driver Changes
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sdriver");
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_PHYSICIAN_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sphysician");
                }
                //Geeta FMLA Enhancement
                blnAllowed = p_objUserLogin.IsAllowed(RMB_LEAVEPLAN_SEARCH);
                if (!blnAllowed)
                {
                    //Rakhi Mits 17929:Corrected Node Name for Search Menu
                    //MDIRemoveBySysName("leaveplansearch");
                    MDIRemoveBySysName("sleaveplan");
                   //Rakhi Mits 17929:Corrected Node Name for Search Menu
                }
                //Begin: Added by Mohit Yadav for Bug No. 001861
                blnAllowed = p_objUserLogin.IsAllowed(RMB_MEDSTAFF_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("smedstaff");
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_DISABILITYPLAN_SEARCH);
                if (!blnAllowed)
                {
                    //Rakhi Mits 17929:Corrected Node Name for Search Menu
                    //MDIRemoveBySysName("sdp"); // Disability Plan Search
                    MDIRemoveBySysName("splan");
                    //Rakhi Mits 17929:Corrected Node Name for Search Menu
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_ADMINTRACKING_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sat"); // admin tracking search
                }
                //End: Added by Mohit Yadav for Bug No. 001861
                //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
                blnAllowed = p_objUserLogin.IsAllowed(RMB_PROPERTY_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sproperty");
                }
                //Anu Tennyson for MITS 18291 : ENDS
                //skhare7
                blnAllowed = p_objUserLogin.IsAllowed(RMB_DIARY_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("sdiary");
                }
                //skhare7

                //aaggarwal29 : MITS 36415 start
                blnAllowed = p_objUserLogin.IsAllowed(RMB_CATASTROPHE_SEARCH);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("scatastrophe");
                }
                //aaggarwal29: MITS 36415 end

                blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("FundsRoot");
                }
                else
                {
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_TRANSACT);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("funds"); // Transaction
                    }
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_BNKACCT);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("bankaccount");
                    }
                    /* not for MDI
                    if (p_objUserLogin.IsAllowedEx(RMB_FUNDS_BNKACCT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_FUNDS_BNKACCT, RMO_CREATE))
                    {
                        AppendToNodeAttribute(p_objDocument, "bankaccount-tab", "url", "&SysCmd=1");
                        AppendToNodeAttribute(p_objDocument, "bankaccount", "url", "&SysCmd=1");
                    }*/
                    //blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_VOIDCHK);
                    //if (!blnAllowed)
                    //{
                    //    MDIRemoveBySysName("voidclearchecks"); // Void Checks
                    //}
                    // MITS 27284 : Ishan
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_VOIDCHK);
                    if (!blnAllowed)
                    {
                        MDIRemoveBytitle("Void Checks"); // Void Checks
                    }
                    //blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_CLEARCHK);
                    //if (!blnAllowed)
                    //{
                    //    MDIRemoveBySysName("voidclearchecks"; // Mark Checks as Cleared
                    //}
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_CLEARCHK);
                    if (!blnAllowed)
                    {
                        MDIRemoveBytitle("Mark Checks as Cleared"); // Mark Checks as Cleared
                    }
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_AUTOCHK);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("autoclaimchecks");
                    }

                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_UNCLEARCHECKS);
                    if (!blnAllowed)
                    {
                        MDIRemoveBytitle("Un-Clear Checks");      // Un-clear checks
                    }
                    // End Ishan
                    //Start Ashutosh 
                    //blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_UNCLEARCHECKS);
                    //if (!blnAllowed)
                    //{
                    //    MDIRemoveBySysName("voidclearchecks");
                    //}
                        blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_RECREATECHECKFILE);
                        if (!blnAllowed)
                        {
                            MDIRemoveBySysName("ReCreateCheck");
                        }
                        blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_RESETPRINTEDCHECK);
                        if (!blnAllowed)
                        {
                            MDIRemoveBySysName("ReSetChecks");
                        }
                    //ENd Ashutosh
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_COMBINEDPAYMENT);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("CombinedPayment");
                    }
                        
                    /* not for MDI
                    //Added Rakhi for MITS 13084-START:Display Autocheck Records when Automatic Checks have View permission only.
                    else if (p_objUserLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_CREATE))
                    {
                        AppendToNodeAttribute(p_objDocument, "autochecks-tab", "url", "&amp;SysCmd=1");
                    }
                     */
                    //Added Rakhi for MITS 13084-START:Display Autocheck Records when Automatic Checks have View permission only.
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_PRINTCHK);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("PrintChecks");
                    }
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_APPPAYCOV);
                    if (!blnAllowed)
                    {
                        //MDIRemoveBySysName("ApplyPaymentsToCoverages");
                        MDIRemoveBySysName("AddPaymentsToCoverages"); //sanoopsharma - Jira 7585
                    }
                    //Animesh Inserted For RMSC Bulk Check Release //skhare7 RMSC Merge 28397
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_BLKCHKREL);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("BulkCheckRelease");
                    }
                    //Animesh Insertion Ends//skhare7 RMSC merge
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_DEPOSIT);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("deposit");
                    }
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_SUP_APPROVE_PAYMENTS);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("trans"); // Approve Funds Transactions
                    }

                    // Added By Sachin Gupta for MITS 28852
                    blnAllowed = p_objUserLogin.IsAllowed(RMB_FUNDS_CustomPaymentNotification);
                    if (!blnAllowed)
                    {
                        MDIRemoveBySysName("CustomPaymentNotification");
                    }
                }

                blnAllowed = p_objUserLogin.IsAllowed(RMB_DIARY);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("DiariesRoot");
                }
                //Added by Nitin for R6 to apply security settings in MDI Menu for Diary Header Config starts--
                blnAllowed = p_objUserLogin.IsAllowed(RMB_DIARY_HEADER_CONFIG);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("DiaryConfig");
                }
                //Added by Nitin for R6 to apply security settings in MDI Menu for Diary Calendar Module ends


                //Added by Nitin for R6 to apply security settings in MDI Menu for Diary Calendar Module starts--
                blnAllowed = p_objUserLogin.IsAllowed(RMB_DIARY_CALENDAR);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("DiaryCalendar");
                }
                //Added by Nitin for R6 to apply security settings in MDI Menu for Diary Calendar Module ends
                blnAllowed = p_objUserLogin.IsAllowed(RMA_UTILITIES);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("UtilitiesRoot");
                }
                
                blnAllowed = p_objUserLogin.IsAllowed(RMB_DCC);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("reports-listing");//Changed by Shivendu for MITS 17643
					
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_SM);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("smpagerptfields"); // SM Designer
                    MDIRemoveBySysName("smpageopenreport"); // SM Draft Report
                    MDIRemoveBySysName("smpagepostreport"); // SM Post Draft Reports
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_VEHMAINT);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("vehicle");
                }
                //Mridul 10/26/09 MITS#18230                
                blnAllowed = p_objUserLogin.IsAllowed(RMB_PROPERTYMAINT);
                if (!blnAllowed)
                {
                    //Start: Neha Suresh Jain,06/04/2010
                   // MDIRemoveBySysName("property");
                    MDIRemoveBySysName("propertyunit");
                    //End: Neha Suresh Jain
                }
                /*  not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_VEHMAINT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_VEHMAINT, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "veh-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "veh", "url", "&SysCmd=1");
                }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_PEOPMAINT);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("people");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_PEOPMAINT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PEOPMAINT, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "people-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "people", "url", "&SysCmd=1");
                }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_EMPMAINT);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("employee");
                }
                /* not for MDI
            else if (p_objUserLogin.IsAllowedEx(RMB_EMPMAINT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_EMPMAINT, RMO_CREATE))
            {
                AppendToNodeAttribute(p_objDocument, "emp-tab", "url", "&SysCmd=1");
                AppendToNodeAttribute(p_objDocument, "emp", "url", "&SysCmd=1");
            }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_ENTITIES);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("entitymaint");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_ENTITIES, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_ENTITIES, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "ent-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "ent", "url", "&SysCmd=1");
                }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_DIS_PLAN);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("plan");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_DIS_PLAN, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_DIS_PLAN, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "plan-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "plan", "url", "&SysCmd=1");
                }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_ORG);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("OrgHierarchyMaintenance");
                }

                blnAllowed = p_objUserLogin.IsAllowed(RMB_STATES);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("StatesMaintenance");
                }

                //Start:added by Nitin goel MITS 30910,09/02/2013
                blnAllowed = p_objUserLogin.IsAllowed(RMB_COVGROUP);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("CoverageGroupsMaintenance");
                }
                //end: added by nitin goel
                blnAllowed = p_objUserLogin.IsAllowed(RMB_POLMGT);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("policy");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_POLMGT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_POLMGT, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "policy-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "policy", "url", "&SysCmd=1");
                }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_STAFFMAINT);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("staff");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_STAFFMAINT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_STAFFMAINT, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "staff-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "staff", "url", "&SysCmd=1");
                }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_PHYMAINT);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("physician");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_PHYMAINT, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PHYMAINT, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "phys-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "phys", "url", "&SysCmd=1");
                }
                 */

                blnAllowed = p_objUserLogin.IsAllowed(RMB_PATIENT_TRACKING);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("patient");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_PATIENT_TRACKING, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_PATIENT_TRACKING, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "patient-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "patient", "url", "&SysCmd=1");
                }
                 */
                blnAllowed = p_objUserLogin.IsAllowed(RMO_DRIVER_MAIN);  //Aman Driver Enh
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("driver");
                }
                //Geeta FMLA Enhancement 
                blnAllowed = p_objUserLogin.IsAllowed(RMB_LEAVE_ADMINISTRATION);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("leaveplan");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_LEAVE_ADMINISTRATION, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_LEAVE_ADMINISTRATION, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "leaveplan-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "leaveplan", "url", "&SysCmd=1");
                }
                 */

                //Shruti Policy Billing 
                blnAllowed = p_objUserLogin.IsAllowed(RMB_POLICY_BILLING);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("policybilling");
                }
                /* not for MDI
                else if (p_objUserLogin.IsAllowedEx(RMB_POLICY_BILLING, RMO_VIEW) && !p_objUserLogin.IsAllowedEx(RMB_POLICY_BILLING, RMO_CREATE))
                {
                    AppendToNodeAttribute(p_objDocument, "policybilling-tab", "url", "&SysCmd=1");
                    AppendToNodeAttribute(p_objDocument, "policybilling", "url", "&SysCmd=1");
                }
                 */


                blnAllowed = p_objUserLogin.IsAllowed(RMB_CODES);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("TableMaint");
                }
                
                //aaggarwal29 MITS 28152 --Start

                // Added check for catastrophe maintenance, unchecking it from SMS was not removing it from maintenance menu
                blnAllowed = p_objUserLogin.IsAllowed(RMB_CATASTROPHEMAINT);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("catastrophe");
                }

                // Added "If" clause to check if Maintenance has any visible child nodes.
                // If no visible child nodes exist then Maintenance is removed from MDIMenu list.
                if (!CheckIfChildNodesExist("maintenanceroot", "false"))
                {
                    MDIRemoveBySysName("MaintenanceRoot");
                }
                //aaggarwal29 MITS 28152 --End

                blnAllowed = p_objUserLogin.IsAllowed(RMB_EXECSUMMARYREPORTS);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("ExecSummConfig"); // Admin Executive Summary Configuration
                    MDIRemoveBySysName("ExecSummaries"); 
                }
                blnAllowed = p_objUserLogin.IsAllowed(RMB_EXECSUMMARYCONF);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("claimexecsearch");
                    MDIRemoveBySysName("eventexecsearch");
                }

                //check for First Report of Injury Options security permission
                blnAllowed = p_objUserLogin.IsAllowed(RMO_FROI_OPTIONS);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("FROIOptions");
                }
                //check for Jurisdictional Form Options security permission
                blnAllowed = p_objUserLogin.IsAllowed(RMO_JURIS_FORM_OPTIONS);
                if (!blnAllowed)
                {
                    MDIRemoveBySysName("JurisOptions");
                }

                if (!PublicFunctions.IsHistTrackEnabled(p_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId))
                {
                    MDIRemoveBySysName("HistoryTracking");
                }
                else
                { //validate db version and edition
                    if (!IsValidDbServer(p_objUserLogin.objRiskmasterDatabase.ConnectionString, p_objUserLogin.objRiskmasterDatabase.DbType))
                    {
                        MDIRemoveBySysName("HistoryTracking");
                    }
                }
            }

            objAdminConfDoc = GetAdminConfig("customize_custom");
            if (objAdminConfDoc.InnerText == "")
            {
                MDIRemoveBySysName("CustomizeCustom");
            }
            else
            {
                MDISearchRemoveBySysName("//SecuritySetting/ChangeDatabase", "status");
                MDISearchRemoveBySysName("//SecuritySetting/ChangePassword", "ChangePassword");
                MDISearchRemoveBySysName("//SpecialSettings/Show_Maintenance", "maint");

            }
            //if (objAdminConfDoc.InnerText != "")
            //{
            //    MDIRemoveBySysName("Searches");
            //}
            /// TR#1158 Pankaj 01/03/06 Check powerview permissions for the nav-tree
            /// 
            if (p_iViewId != 0)
                ApplyPowerViewPermissions(p_iViewId, p_iDsnId);

            objAdminConfDoc = GetAdminConfig("customize_settings");

            //Start: rsushilaggar MITS 22944 Date 11/08/2010
            MDISearchRemoveBySysName("//Buttons/Document", "DocumentRoot");
            MDISearchRemoveBySysName("//Buttons/Search", "SearchRoot");
            MDISearchRemoveBySysName("//Buttons/File", "UserDocumentsRoot");
            MDISearchRemoveBySysName("//Buttons/Diary", "DiariesRoot");
            MDISearchRemoveBySysName("//Buttons/Report", "ReportsRoot");
            //End: rsushilaggar


            /* Not for MDI
            // 01/17/2007 Rem Umesh
            AppendAttribute(objAdminConfDoc, p_objDocument, "//Other/ShowName", "//toc", "currentuser_allowed");
            AppendAttribute(objAdminConfDoc, p_objDocument, "//Other/ShowLogin", "//toc", "currentlogin_allowed");
            */


            // End Rem Umesh

            //Start by Nitin for Mits 15456 on 20-Apr-2009 in order to correct xpaths of search nodes for objAdminConfig XmlDocument
            objAdminConfDoc = GetAdminConfig("customize_search");
            MDISearchRemoveBySysName("//Search/Catastrophe", "scatastrophe"); // added for MITS 36415 by aaggarwal29 to handle catastrophe search through customization Menu
            MDISearchRemoveBySysName("//Search/Claim", "sclaim");
            MDISearchRemoveBySysName("//Search/Event", "sevent");
            MDISearchRemoveBySysName("//Search/Employee", "semployee");
            MDISearchRemoveBySysName("//Search/Entity", "sentity");
            MDISearchRemoveBySysName("//Search/Vehicle", "svehicle");
            // Naresh As the Enhanced Policy Serach Option is removed from Utility, It is put here
            if (bIsEnhPolEnabled)
                MDISearchRemoveBySysName("//Search/Policy", "spolicyenh");
            else
                MDISearchRemoveBySysName("//Search/Policy", "spolicy");
            MDISearchRemoveBySysName("//Search/Fund", "sfunds");
            MDISearchRemoveBySysName("//Search/Patient", "spatient");
            MDISearchRemoveBySysName("//Search/Driver", "sdriver");
            MDISearchRemoveBySysName("//Search/Physician", "sphysician");
            // Start Naresh Enhanced Policy Search
            // Naresh This has been commented as there is no option of Checking the Search in Utility
            // MDIRemoveBySysName(objAdminConfDoc, p_objDocument, "//Search/EnhPolicy[@title='EnhPolicies']", "enhancedpolicy");
            // End Naresh Enhanced Policy Search
            MDISearchRemoveBySysName("//Search/AdminTrack", "sat");
            MDISearchRemoveBySysName("//Search/MedStaff", "smedstaff");
            MDISearchRemoveBySysName("//Search/DisPlan", "splan");
            // Start Shruti Leave Plan SearchZ
            MDISearchRemoveBySysName("//Search/LeavePlan", "sleaveplan");
            // End Shruti Leave Plan Search
            //End by Nitin for Mits 15456 on 20-Apr-2009 in order to correct xpaths of search nodes for objAdminConfig XmlDocument

            //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
            MDISearchRemoveBySysName("//Search/Property", "sproperty");
            //Anu Tennyson for MITS 18291 : ENDS
            //skhare7
            MDISearchRemoveBySysName("//Search/Diary", "sdiary");
            //skhare7
            ////added by Nitin for Mits 15455 on 18/04/2009
            objAdminConfDoc = GetAdminConfig("customize_reports");
            MDIUpdateTitle("//ReportMenu/BILabel/@title", "//ReportMenu/BILabel/@default", "BISRedirect");
            MDIUpdateTitle("//ReportMenu/ReportLabel/@title", "//ReportMenu/ReportLabel/@default", "StdReportsQueue");
            MDIUpdateTitle("//ReportMenu/SMLabel/@title", "//ReportMenu/SMLabel/@default", "StdReportsDesigners");
            MDIUpdateTitle("//ReportMenu/ExecSummLabel/@title", "//ReportMenu/ExecSummLabel/@default", "ExecSummaries");
            MDIUpdateTitle("//ReportMenu/OtherLabel/@title", "//ReportMenu/OtherLabel/@default", "OtherReports");
            MDIUpdateTitle("//ReportMenu/OtherLabel/DCCLabel/@title", "//ReportMenu/OtherLabel/DCCLabel/@default", "reports-listing");

            MDISearchForValueRemoveBySysName("//ReportMenu/BILabel", "BISRedirect");//BI
            MDISearchForValueRemoveBySysName("//ReportMenu/ReportLabel", "StdReportsQueue");                           //MITS 9445:Umesh
            MDISearchRemoveBySysName("//ReportMenu/ReportLabel/AvlReports", "smreportsel"); // Available Reports
            MDISearchRemoveBySysName("//ReportMenu/ReportLabel/JobQueue", "smrepqueue"); // JobQueue
            MDISearchRemoveBySysName("//ReportMenu/ReportLabel/DeleteReport", "smreportdelete"); // DeleteReport
            MDISearchRemoveBySysName("//ReportMenu/ReportLabel/ScheduleReport", "schedulereport"); // ScheduledReports
            MDISearchRemoveBySysName("//ReportMenu/ReportLabel/ViewSchedReport", "schedulelist"); // ViewScheduledReports

            MDISearchForValueRemoveBySysName("//ReportMenu/SMLabel", "StdReportsDesigners");
            MDISearchRemoveBySysName("//ReportMenu/SMLabel/Designer", "rptfields"); // Designer
            MDISearchRemoveBySysName("//ReportMenu/SMLabel/DraftReport", "openreport"); // DraftReports
            MDISearchRemoveBySysName("//ReportMenu/SMLabel/PostDraftRpt", "postreport"); // PostDraftReports
            MDISearchRemoveBySysName("//ReportMenu/ReportLabel/NewReport", "smpostreport");

            MDISearchForValueRemoveBySysName("//ReportMenu/ExecSummLabel", "ExecSummaries");
            //MDIRemoveBySysName(objAdminConfDoc,p_objDocument,"//ReportMenu/ExecSummLabel/Configuration","ESConfig");
            //changed by Shruti on 09 Oct 06 (MITS No. -7891)
            MDISearchRemoveBySysName("//ReportMenu/ExecSummLabel/Configuration", "ExecSummConfig");
            MDISearchRemoveBySysName("//ReportMenu/ExecSummLabel/Claim", "sclaimexesumm");
            MDISearchRemoveBySysName("//ReportMenu/ExecSummLabel/Event", "seventexesumm");
            MDISearchRemoveBySysName("//ReportMenu/OtherLabel/DCCLabel", "reports-listing");

            MDISearchForValueRemoveBySysName("//ReportMenu/OtherLabel", "OtherReports");
            MDISearchRemoveBySysName("//ReportMenu/OtherLabel/OSHA300", "postosha300"); // Post New OSHA 300 Report
            MDISearchRemoveBySysName("//ReportMenu/OtherLabel/OSHA301", "postosha301"); // Post New OSHA 301 Report
            MDISearchRemoveBySysName("//ReportMenu/OtherLabel/OSHA300A", "postosha300A"); // Post New OSHA 300A Report
            MDISearchRemoveBySysName("//ReportMenu/OtherLabel/OSHASharpsLog", "postoshasharps"); // Post OSHA Sharp Log Reports
            MDISearchRemoveBySysName("//ReportMenu/OtherLabel/DCCLabel","reports-listing");
            //REM customization    Umesh
            //ended by Nitin for Mits 15455 on 18/04/2009

            //Added for REM-Nadim(5/4/2009) 
            objAdminConfDoc = GetAdminConfig("customize_custom");
            if (objAdminConfDoc.InnerText != "")
            {
                MDISearchRemoveBySysName("//RMAdminSettings/SecuritySetting/ChangeDatabase", "ChangeDatabase");
                MDISearchRemoveBySysName("//RMAdminSettings/SecuritySetting/ChangePassword", "ChangePassword");
                MDISearchRemoveBySysName("//RMAdminSettings/SpecialSettings/Show_FundsTransactions", "funds");
                MDISearchRemoveBySysName("//RMAdminSettings/SpecialSettings/Show_AutoCheck", "autoclaimchecks");
                MDISearchRemoveBySysName("//RMAdminSettings/SpecialSettings/Show_Maintenance", "MaintenanceRoot");
            }

            //tanwar2 - ImageRight interface - start
            if (!oSettings.UseImgRight)
            {
                this.MDIRemoveBySysName("ImageRight");
            }
            //tanwar2 - ImageRight interface - end

            //Added for REM-Nadim(5/4/2009) 
            IEnumerable<XElement> query = from c in this.MDIMenu.Descendants()
                                          where c.Attribute("sysName") != null && c.Attribute("sysName").Value.ToLower() == "security"
                                          select c;
            XElement el = query.FirstOrDefault();
            if (el != null && !el.HasElements)
            {
                el.Remove();
            }
            //Added by amitosh for r8 enhancement 
            query = from c in this.MDIMenu.Descendants()
                    where c.Attribute("sysName") != null && c.Attribute("sysName").Value.ToLower() == "weblinks"
                                          select c;
             el = query.FirstOrDefault();
            if (el != null)
            {
       sSQL =" SELECT SYS_SETTINGS.ITEM,SYS_SETTINGS.ROW_ID  FROM WEB_LINKS_PERMISSION,SYS_SETTINGS ";
       sSQL = sSQL + " WHERE USER_ID in " + "("+ p_objUserLogin.UserId + ",0)";
      sSQL = sSQL +" AND  SYS_SETTINGS.ROW_ID = WEB_LINKS_PERMISSION.ROW_ID ";
      //sSQL = sSQL + "ORDER BY SYS_SETTINGS.DTTM_RCD_ADDED desc"; //nnithiyanand - Commented for issue 33744
      sSQL = sSQL + " ORDER BY UPPER(SYS_SETTINGS.ITEM)  ASC"; //nnithiyanand - Added for issue 33744

                using (DbReader dbRdr = DbFactory.GetDbReader(p_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {
                    while (dbRdr.Read())
                    {
                        string sValue = dbRdr.GetString("ITEM");
                        int irowid = dbRdr.GetInt32("ROW_ID");
                      
                        AddNodes(el, sValue, "../UI/RecentRecords/Weblinknavigate.aspx?RowId= " + irowid, "True", "True", sValue, "", true, 0);
                    }

                }
            }
            //end Amitosh

            //Neha 10/03/2011 This setting is not required.
            ////Neha 05/30/2011  Code Relationship will be visible only if carrier claim is checked.
            //if (oSettings.MultiCovgPerClm == 0)
            //{
            //    this.MDIRemoveBySysName("CodeRelationships");
            //}
            //Neha end
            //BSB 01.26.2007 Adding Support for Riskmaster.config Extensibility section
            // and NavTreeMapping elements. 
            // Note: Add is performed first (while we can still add "before" and "after" nodes
            // that may be removed in normal processing.)
            // Remove and Replace are completed here.

            /* not for MDI
            CustomNavTree.ReplaceNodes(p_objDocument);
            CustomNavTree.MDIRemoveBySysNames(p_objDocument);
            
            return p_objDocument;
             */
        }
        catch (RMAppException p_objRMAppException)
        {
            Log.Write("Message :" + p_objRMAppException.Message + "Stack :" + p_objRMAppException.StackTrace, m_iClientId);//Deb : Adde extra lgging for better troubleshooting
            throw p_objRMAppException;
        }
        catch (XmlException p_objXmlException)
        {
            Log.Write("Message :" + p_objXmlException.Message + "Stack :" + p_objXmlException.StackTrace, m_iClientId);
            throw new RMAppException(Globalization.GetString("UserPermissions.GetSecuredNavTreeForUser.error", m_iClientId), p_objXmlException);//Deb : Adde extra lgging for better troubleshooting
        }
        catch (Exception p_objException)
        {
            Log.Write("Message :" + p_objException.Message + "Stack :" + p_objException.StackTrace, m_iClientId);
            throw new RMAppException(Globalization.GetString("UserPermissions.GetSecuredNavTreeForUser.error", m_iClientId), p_objException);//Deb : Adde extra lgging for better troubleshooting
        }
        finally
        {
            //objBRSInstalled = null;
            oSettings = null;
            // Payal: RMA-7586 --Starts
            if (oLobSettings != null)
            {
                oLobSettings = null;
            }
            if (oColLobSettings != null)
            {
                oColLobSettings = null;
            }
            if (objCache != null)
            {
                objCache = null;
            }
            if (objReader != null)
            {
                objReader.Close();
                objReader = null;
            }
            if (dtLobList != null)
            {
                dtLobList = null;
            }
            // Payal: RMA-7586 --Ends
        }
    }

    // MITS 28152: aaggarwal29 -- begin 
        /// <summary>
    /// Added new method as part of MITS 28152
    /// This method searches by a particular node name. It further checks by attribute value
    /// and evaluates if any child node exists or not.
        /// </summary>
        /// <param name="p_sNodeSearchName">gives the sysname of the node to be searched</param>
        /// <param name="p_sAttributeValue">gives the attribute value of the child node to be checked</param>
        /// <returns>true if child node exists else false</returns>
    public bool CheckIfChildNodesExist(string p_sNodeSearchName, string p_sAttributeValue)
    {
			try
			{
                XElement XEObjParent, XEObjChild;
                IEnumerable<XElement> query = from c in this.MDIMenu.Descendants()
                                              where c.Attribute("sysName") != null && c.Attribute("sysName").Value.ToLower() == p_sNodeSearchName 
                                              select c;
                XEObjParent = query.FirstOrDefault();
                if (XEObjParent != null)
                {
                    query = from d in XEObjParent.Descendants()
                    where d.Attribute("sysName") != null && d.Attribute("visible").Value.ToLower() != p_sAttributeValue
                    select d;
                    XEObjChild = query.FirstOrDefault();
                    if (XEObjChild != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;   // if node not found , return true , 
                    //so that in turn no action will be performed in MDIMenu method
                }
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(XmlException p_objXmlException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.CheckIfChildNodesExist.error", m_iClientId), p_objXmlException);  
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("UserPermissions.CheckIfChildNodesExist.error", m_iClientId), p_objException);  
			}
			finally
			{
                
			}
    }
    // MITS 28152: aaggarwal29 -- end


    // MITS 27284 : Ishan
    private void MDIRemoveBytitle(string title)
    {
        try
        {
            IEnumerable<XElement> query = from c in this.MDIMenu.Descendants()
                                          where c.Attribute("title") != null && c.Attribute("title").Value.ToLower() == title.ToLower()
                                          select c;

            XElement el = query.FirstOrDefault();
            if (el != null)
            {
                el.Remove();
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
    }

    private void MDIRemoveBySysName(string sysName)
    {
        try
        {
            IEnumerable<XElement> query = from c in this.MDIMenu.Descendants()
                                          where c.Attribute("sysName") != null && c.Attribute("sysName").Value.ToLower() == sysName.ToLower()
                                          select c;
            
            XElement el = query.FirstOrDefault();
            if (el != null)
            {
                el.Remove();
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
    }


    /// <summary>
    /// Determines whether or not the specified SQL Server platform
    /// is  SQL Server 2008 or onwars Enterprise,Devloper or Evaluatio 
    /// </summary>
    /// <param name="strConnString">string containing the database connection string</param>
    /// <returns>boolean indicating whether or not the database is a version
    /// SQL Server 2008 or onwards with Enterprise,Devloper or Evaluation  </returns>
    private static bool IsValidDbServer(string strConnString, eDatabaseType DbType)
    {
        double dblDbVersion = 0.0;
        bool blnIsValidDbServer = false;
        bool blnParsed = false;
        bool bValidEdition = false;
        double SQL_DB_VERSION = 0.0;

        string sSqlEdition = string.Empty;
        string strSQL = string.Empty;
        string sRegEx = @"((^(?<VERSION>\d*\.\d*)\.?\d*\.?\d*)|( +(?<VERSION>\d*\.\d*)\.?\d*\.?\d*))";
        if (DbType == eDatabaseType.DBMS_IS_ORACLE)
        {
            SQL_DB_VERSION = 10.2;    //Oracle 10.2.0
            // For MITS 29393
            strSQL = "SELECT BANNER AS DBVERSION FROM V$VERSION WHERE UPPER(BANNER) LIKE 'ORACLE%'";
        }
        else
        {
            SQL_DB_VERSION = 10.0;   //sql server 2008
            strSQL = "SELECT SERVERPROPERTY('productversion')AS DBVERSION, SERVERPROPERTY ('edition') AS EDITION";
        }
        using (DbReader dbRdr = DbFactory.GetDbReader(strConnString, strSQL))
        {
            if (dbRdr.Read())
            {
                //version validation
                string sVersion = dbRdr.GetValue("DBVERSION").ToString();
                Match match = Regex.Match(sVersion, sRegEx);
                if (match.Success)
                {
                    blnParsed = Double.TryParse(match.Groups["VERSION"].Value, out dblDbVersion);

                }
                //version validation End

                //Edition validation
                if (DbType == eDatabaseType.DBMS_IS_ORACLE)//Deb : Condition was wrong fixed it
                {
                    bValidEdition = true;  //for oracle no check for edition
                    
                }
                else
                {
                    sSqlEdition = dbRdr.GetString("EDITION").ToLower();
                    bValidEdition = sSqlEdition.Contains("enterprise") | sSqlEdition.Contains("developer") | sSqlEdition.Contains("evaluation");
                }
                //Edition validation end
               
                if (blnParsed && (dblDbVersion >= SQL_DB_VERSION) && bValidEdition)
                {
                    blnIsValidDbServer = true;
                }
            }


        }//using

        return blnIsValidDbServer;
    }
        /// <summary>
        /// To add Item at runtime
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_objParentNode"> </param>
        /// <param name="p_sTitle"></param>
        /// <param name="p_sLocalPath"></param>
        /// <param name="p_sVisible"></param>
        /// <param name="p_sActive"></param>
        /// <param name="p_sSysName"></param>
        /// <param name="p_sParam"></param>
        /// <param name="p_bTaregtFrame"></param>
        /// <param name="p_sRecordID"></param>
    private void AddNodes(XElement p_objParentNode,string p_sTitle,string p_sLocalPath,string p_sVisible,string p_sActive,string p_sSysName,string p_sParam,bool p_bTaregtFrame,int p_iRecordID)
    {
        string sElement = null;
        if (p_bTaregtFrame)
        {
            sElement = "<item title=\"" + p_sTitle + "\" visible=\"" + p_sVisible + "\" active=\"" + p_sActive + "\" sysName=\"" + p_sSysName + "\" recordId=\"" + p_iRecordID + "\" localPath=\"" + p_sLocalPath + "\" params=\"" + p_sParam + "\" targetFrame=\"_blank\" />";
       }
        else
        {
            sElement = "<item title=\"" + p_sTitle + "\" visible=\"" + p_sVisible + "\" active=\"" + p_sActive + "\" sysName=\"" + p_sSysName + "\" recordId=\"" + p_iRecordID + "\" localPath=\"" + p_sLocalPath + "\" params=\"" + p_sParam + "/>";
        }
           XElement sElem = XElement.Parse(@sElement);
        p_objParentNode.Add(sElem);
    
    }

    // Payal : RMA-7586 --Starts
    private DataTable GetLOBList(UserLogin p_objUserLogin, int iTableId)
    {

        string sSQL = string.Empty;
        System.Data.DataSet dbDataSet = null;
        System.Data.DataTable dbTable = null;
        int i = 0;
        try
        {
            sSQL = " SELECT SHORT_CODE FROM CODES WHERE TABLE_ID=" + iTableId + " ";

            using (dbDataSet = DbFactory.GetDataSet(p_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL,m_iClientId))
            {
                if (dbDataSet.Tables.Count > 0)
                {
                    dbTable = dbDataSet.Tables[0];
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            if (dbDataSet != null)
            {
                dbDataSet = null;
            }
        }
        return dbTable;
    }

    /// <summary>
    /// Added new method as a part of Jira:8485 (Deprecated)
    /// In order to remove links from menu based on param name.
    /// </summary>
    /// <author>Payal</author>
    /// <param name="paramkey"> </param>
    /// <param name="paramvalue"></param>
    /// <param name="sysName"></param>
    private void MDIRemoveByParams(string paramkey, string paramvalue, string sysName)
    {
        paramvalue = paramvalue.ToLower();
        sysName = sysName.ToLower();
        IEnumerable<XElement> query = from c in this.MDIMenu.Descendants()
                                      where c.Attribute("sysName") != null && c.Attribute("sysName").Value.ToLower() == sysName
                                      && (!string.IsNullOrEmpty(c.Attribute("params").Value)) && (c.Attribute("params").Value.Contains(paramkey + '='))
                                      && (c.Attribute("params").Value.IndexOf('&', c.Attribute("params").Value.IndexOf(paramkey + '=')) > -1 ? (c.Attribute("params").Value.Substring(c.Attribute("params").Value.IndexOf(paramkey + '=') + (paramkey + '=').Length, c.Attribute("params").Value.IndexOf('&', c.Attribute("params").Value.IndexOf(paramkey + '=')) - (c.Attribute("params").Value.IndexOf(paramkey + '=') + (paramkey + '=').Length)) == paramvalue) : ((c.Attribute("params").Value.Substring(c.Attribute("params").Value.IndexOf(paramkey + '=') + (paramkey + '=').Length)).ToLower() == paramvalue))
                                      select c;

        XElement el = query.FirstOrDefault();

        if (el != null)
        {
            el.Remove();
        }

    }

        // Payal : RMA-7586 --Ends 
}

}

