using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using System.Xml;

namespace Riskmaster.Application.SecurityManagement
{
	/// <summary>
	///Author  :   Aashish Bhateja
	///Dated   :   7th,July 2004
	///Purpose :   Contains the methods & properties related to ModuleGroups.
    /// </summary>
	public class ModuleGroups
	{
		/// <summary>
		/// Represents the connection string for the underlying RM DB.
		/// </summary>
		private string m_sConnectionString = "";

		/// <summary>
		/// Represents a string of allowed modules.
		/// </summary>
		private string m_sAllowedModules = "";

		/// <summary>
		/// Represents the module ID.
		/// </summary>
		private int m_iID = 0;
		
		/// <summary>
		/// Represents the group name.
		/// </summary>
		private string m_sGroupName = "";

        /// <summary>
        /// ClientID for multi-tenancy environment, cloud
        /// </summary>
        private int m_iClientId = 0;

        //abansal
        /// <summary>
        /// Input/Output XmlDocument
        /// </summary>
        XmlDocument m_objXmlDoc = null;

		private const string CODE_ACCESS_PUBLIC_KEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";

		/// <summary>
		/// Constructor for ModuleGroups class.
		/// </summary>
		/// <param name="p_sConnectionstring">
		/// Connection string, to be used to connect to the underlying RM Database.
		/// </param>
		public ModuleGroups(string p_sConnectionstring, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        private string m_sUserName = "";
        public ModuleGroups(string p_sConnectionstring, string p_sUserName, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
        }

		/// <summary>
		/// Represents the name group name.
		/// </summary>
		public string GroupName
		{
			get
			{
				return m_sGroupName;
			}
			set
			{
				m_sGroupName = value;
			}
		}

		/// <summary>
		/// This function is used to get the allowed modules from the 
		/// GROUP_PERMISSIONS table.
		/// </summary>
		/// <param name="p_iGroupID">
		/// Allowed modules are retrieved on the basis of group ID.
		/// </param>
		/// <param name="p_sAllowedModules">
		/// Represents the modules that are allowed.
		/// </param>
		/// <returns>
		/// A boolean values that represents the success/failure.
		/// </returns>
		[System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey=CODE_ACCESS_PUBLIC_KEY)]
		public bool AddPermissions(int p_iGroupID,ref string p_sAllowedModules)
		{
			bool bReturn = false;
			string sSQL = "";
			p_sAllowedModules = "";
			DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
			DbReader objReader = null;
			try
			{
				objConn.Open();
				sSQL = "SELECT FUNC_ID FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + p_iGroupID;
				objReader = objConn.ExecuteReader(sSQL);
			
				while(objReader.Read())
				{
					if(p_sAllowedModules == "")
						p_sAllowedModules = Convert.ToString(objReader.GetInt("FUNC_ID"));
					else
						p_sAllowedModules = p_sAllowedModules + "," + Convert.ToString(objReader.GetInt("FUNC_ID"));
				}
			
				bReturn = true;
			}
			catch(Exception p_objException)
			{
				throw new  DataModelException(Globalization.GetString("ModuleGroups.AddPermissions.AddPermissionsError",m_iClientId),p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}
		/// <summary>
		/// This function gets the DSN specific permissions.
		/// </summary>
		/// <param name="p_iGroupID">Group id</param>
		/// <returns>Arraylist containing permissions.</returns>
		public ArrayList GetDsnPermissions(int p_iGroupID)
		{
			bool bReturn = false;
			string sSQL = "";
			ArrayList arrlstPermissonDsn=new ArrayList();
			DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
			DbReader objReader = null;
			try
			{
				objConn.Open();
				sSQL = "SELECT FUNC_ID FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + p_iGroupID;
				objReader = objConn.ExecuteReader(sSQL);
			
				while(objReader.Read())
				{
					arrlstPermissonDsn.Add(objReader.GetInt32(0));
				}
			
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
				
			}
		   return	 arrlstPermissonDsn;
		}
		/// <summary>
		/// Called from Remove(), this function is used to delete the Module Group
		/// from all the related tables.
		/// </summary>
		/// <returns>
		/// A boolean values that represents the success/failure.
		/// </returns>
		private bool DeleteObject()
		{
			bool bReturn = false;
			DbConnection objConn = null;
			DbTransaction objTrans = null;
			string sSQL = "";
			int iNoOfRowsAffected=int.Parse("0");
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				DbCommand objCommand = objConn.CreateCommand();
				objTrans = objConn.BeginTransaction();
				objCommand.Transaction = objTrans;
				sSQL = "DELETE FROM USER_GROUPS WHERE GROUP_ID=" + m_iID;
				objCommand.CommandText = sSQL;
				iNoOfRowsAffected = objCommand.ExecuteNonQuery(); 

				sSQL = "DELETE FROM USER_MEMBERSHIP WHERE GROUP_ID=" + m_iID;
				objCommand.CommandText = sSQL;
				iNoOfRowsAffected += objCommand.ExecuteNonQuery(); 

				sSQL = "DELETE FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + m_iID;
				objCommand.CommandText = sSQL;
				iNoOfRowsAffected += objCommand.ExecuteNonQuery(); 

				objTrans.Commit();

				if(iNoOfRowsAffected > 0)
					bReturn = true;
				else
                    throw new RecordNotFoundException(Globalization.GetString("ModuleGroups.Remove.RecordNotFound", m_iClientId));
			}
			catch(Exception p_objException)
			{
				if(objTrans != null)
					objTrans.Rollback();

                throw new DataModelException(Globalization.GetString("ModuleGroups.DeleteObject.DeleteError", m_iClientId), p_objException);
			}
			finally
			{
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}

		/// <summary>
		/// Used to get the maximum group ID from USER_GROUPS table.
		/// </summary>
		/// <param name="p_iMAXID">
		/// The maximum group ID which is obtained from the function.
		/// </param>
		/// <returns>
		/// A boolean values that represents the success/failure.
		/// </returns>
		public bool GetMaxGroupID(ref int p_iMAXID)
		{
			bool bReturn = false;
			string sSQL = "";
			DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
			DbReader objReader = null;
			try
			{
				objConn.Open();
				sSQL = "SELECT MAX(GROUP_ID) FROM USER_GROUPS";
				objReader = objConn.ExecuteReader(sSQL);
				
				if(objReader.Read())
				{
					if(Convert.ToString(objReader.GetInt(0)) == "")
						p_iMAXID = 1;
					else
						p_iMAXID = objReader.GetInt(0) + 1;
				}
				else
					p_iMAXID = 1;
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.GetMaxGroupID.MaxGroupIDError", m_iClientId), p_objException);
			}
			finally
			{
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objConn != null)
				{
					//if(objConn.State == System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}

        /// <summary>
        /// To get the xml for loading the page
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml</param>
        /// <returns>output xml</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
             m_objXmlDoc = p_objXmlDocument;
                string sSQL = "";
                DbReader objRdr = null;
                DbConnection objRMConn = DbFactory.GetDbConnection(m_sConnectionString);
                //XmlElement objRowTxt = null;
                //XmlElement objElm = null;
                XmlElement objElm = null;
                XmlElement objChld = null;
                try
                {
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='groups']");
                    //if (objElm == null)
                    //    return ;
                    objRMConn.Open();
                    sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS ORDER BY GROUP_NAME";
                    objRdr = objRMConn.ExecuteReader(sSQL);

                    while (objRdr.Read())
                    {
                        objChld = p_objXmlDocument.CreateElement("option");
                        objChld.SetAttribute("value", objRdr.GetInt32(0).ToString());
                        objChld.InnerText = objRdr.GetString(1);
                        objElm.AppendChild(objChld);
                    }
                }
                catch (LoadModuleGrpException p_objException)
                {
                    throw p_objException;
                }
                catch (Exception p_objException)
                {
                    throw p_objException;
                }
                finally
                {
                    if (objRdr != null)
                    {
                        objRdr.Close();
                        objRdr.Dispose();
                    }
                    if (objRMConn != null)
                    {
                        objRMConn.Dispose();
                    }
                    
                }
                return p_objXmlDocument;
           
        }

		/// <summary>
		/// Used to load all the module groups.
		/// </summary>
		/// <param name="p_arrlstModules">
		/// An ArrayList, whose each element represents a string containing groupname &
		/// user ID that is permitted under that module group, separated by a token. 
		/// </param>
		/// <returns>
		/// A boolean values that represents the success/failure.
		/// </returns>
		public bool Load(ref ArrayList p_arrlstModules,ref ArrayList p_arrlstColGroups)
		{
			bool bReturn = false;
			string sSQL = "";
			DbConnection objRMConn = DbFactory.GetDbConnection(m_sConnectionString);
            //Parijat:change for using user_table for first name lastname for users under a Module Group
            DbConnection objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));

			DbReader objReader = null;
			ModuleGroups objCModuleGroup = null;
			try
			{
				objRMConn.Open();
                objConn.Open();
				sSQL = "SELECT * FROM USER_GROUPS ORDER BY GROUP_NAME";
                objReader = objRMConn.ExecuteReader(sSQL);
				
				while(objReader.Read())
				{
					objCModuleGroup = new ModuleGroups(m_sConnectionString,m_iClientId);
					objCModuleGroup.ModuleGroupID = objReader.GetInt("GROUP_ID");
					objCModuleGroup.GroupName = objReader.GetString("GROUP_NAME");
					p_arrlstColGroups.Add(objCModuleGroup);
				}
				objReader.Close();
				foreach(ModuleGroups NewCModuleGroup in p_arrlstColGroups)
				{
					sSQL = "SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID=" + NewCModuleGroup.ModuleGroupID;
					objReader = objRMConn.ExecuteReader(sSQL);
                    //Parijat: changes required for sorting according to firstname,lastname using user_table for users under a Module Group
                    if (objReader.Read())
                    {
                        string sSQL2 = "SELECT USER_ID, FIRST_NAME, LAST_NAME FROM USER_TABLE WHERE USER_ID = " + objReader.GetInt("USER_ID");
                        while (objReader.Read())
                        {
                           sSQL2 = sSQL2 + " OR USER_ID = " + objReader.GetInt("USER_ID");

                        }
                        sSQL2 = sSQL2 + " ORDER BY FIRST_NAME, LAST_NAME";
                        objReader.Close();
                        objReader = objConn.ExecuteReader(sSQL2);
                    }

                    //end of changes for sorting by firstname Last name for users under a Module Group
					while(objReader.Read())
					{
						p_arrlstModules.Add(NewCModuleGroup.GroupName + "^~^~^" + objReader.GetInt("USER_ID"));
					}
					objReader.Close();
				}
				bReturn = true;
			}
			catch(LoadModuleGrpException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objRMConn!=null)
				{
					//if(objRMConn.State==System.Data.ConnectionState.Open)
					//{
					//	objRMConn.Close();
						objRMConn.Dispose();
					//}
				}
                //Parijat: changes for using User_Table for first name last name for users under a Module Group
                if (objConn != null)
                {

                    objConn.Dispose();

                }
                objCModuleGroup = null;
			}
			return bReturn;
		}

		/// <summary>
		/// Used to remove a particular group from all the related tables.
		/// </summary>
		/// <returns>A boolean values that represents the success/failure.
		/// </returns>
		public bool Remove()
		{
			bool bReturn = false;
			try
			{
				if (!DeleteObject())
					bReturn = false;
				else
					bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.Remove.RemoveError", m_iClientId), p_objException);
			}
			finally
			{
			}
			return bReturn;
		}

		/// <summary>
		/// Used to add a module group.
		/// </summary>
		/// <param name="p_sGroupName">
		/// Name of the module group to be added.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.
		/// </returns>
		public bool Add(string p_sGroupName)
		{
			bool bReturn = false;
			int iAddedGrpId=0;
			try
			{
				if(!SaveObject(p_sGroupName,ref iAddedGrpId))
					bReturn = false;
				else
					bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.Add.AddError", m_iClientId), p_objException);
			}
			finally
			{
			}
			return bReturn;
		}

		/// <summary>
		/// Called from Add(), used to add a module group.
		/// </summary>
		/// <param name="p_sMGName">
		/// Represents the name of the module group to be added.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.
		/// </returns>
		public bool SaveObject(string p_sMGName,ref int p_iAddedGrpId)
		{
			bool bReturn = false;
			int iMAXID = 0;
			DbWriter objWriter = null;
			
			try
			{
				objWriter = DbFactory.GetDbWriter(m_sConnectionString);
				objWriter.Tables.Add("USER_GROUPS");
				objWriter.Fields.Add("GROUP_NAME",p_sMGName.Trim());
			
				if(GetMaxGroupID(ref iMAXID))
				{
					objWriter.Fields.Add("GROUP_ID",iMAXID);
					p_iAddedGrpId=iMAXID;
				}
                //gagnihotri MITS 11995 Changes made for Audit table
                objWriter.Fields.Add("UPDATED_BY_USER", m_sUserName);
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
				objWriter.Execute();
				bReturn = true;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objWriter != null)
				{
					objWriter = null;
				}
			}
			return bReturn;
		}

		/// <summary>
		/// Used to save the users, that are permitted under a given module group.
		/// </summary>
		/// <param name="p_iGroupID">Represents the ID of a group.</param>
		/// <param name="p_iUserID">Represents the user permitted under the group.
		/// </param>
		/// <returns></returns>
		public bool SaveUsers(int p_iGroupID, int p_iUserID)
		{
			bool bReturn = false;
			DbWriter objWriter = null;
			try
			{
				objWriter = DbFactory.GetDbWriter(m_sConnectionString);
				 
				
				objWriter.Tables.Add("USER_MEMBERSHIP");
				objWriter.Fields.Add("GROUP_ID",p_iGroupID);
				objWriter.Fields.Add("USER_ID",p_iUserID);
                //gagnihotri MITS 11995 Changes made for Audit table
                objWriter.Fields.Add("UPDATED_BY_USER", m_sUserName);
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
				objWriter.Execute();
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.SaveUsers.SaveUsersError", m_iClientId), p_objException);
			}
			finally
			{
				if(objWriter != null)
				{
					objWriter = null;
				}
			}
			return bReturn;
		}

		/// <summary>
		/// Used to delete the user permitted under a group.
		/// </summary>
		/// <param name="p_iGroupID">Group ID whose user is to be deleted.</param>
		/// <param name="p_iUserID">User ID to be deleted.</param>
		/// <returns>A boolean values that represents the success/failure.
		/// </returns>
		public bool DeleteUsers(int p_iGroupID, int p_iUserID)
		{
			bool bReturn = false;
			string sSQL = "";
			DbConnection objConn = null;
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				sSQL = "DELETE FROM USER_MEMBERSHIP WHERE GROUP_ID=" + p_iGroupID + "AND USER_ID=" + p_iUserID;
				objConn.ExecuteNonQuery(sSQL); 
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.DeleteUsers.DeleteUsersError", m_iClientId), p_objException);
			}
			finally
			{
				if(objConn!=null)
				{
					//if(objConn.State==System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
			}
			return bReturn;
		}
		/// <summary>
		/// This function checks for duplicate Modul grp. name
		/// </summary>
		/// <param name="p_sDesiredMGrpName">Required name</param>
		/// <returns>True or false</returns>
		public bool ModuleGrpAlreadyExists(string p_sDesiredMGrpName)
		{
			bool bReturn = false;
			string sSQL;
			DbConnection objRMConn = DbFactory.GetDbConnection(m_sConnectionString);
			DbReader objReader = null;
			try
			{
				objRMConn.Open();
				sSQL = "SELECT GROUP_NAME FROM USER_GROUPS ORDER BY GROUP_NAME";
				objReader = objRMConn.ExecuteReader(sSQL);
				
				while(objReader.Read())
				{
					if(objReader.GetString(0).Trim().ToLower()==p_sDesiredMGrpName.Trim().ToLower())
					{
                        bReturn=true;
						break;

					}
				}
				objReader.Close();
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if(objRMConn!=null)
				{
					//if(objRMConn.State==System.Data.ConnectionState.Open)
					//{
					//	objRMConn.Close();
						objRMConn.Dispose();
					//}
				}
			}
			return bReturn;
		}

		//-- Not Yet Tested. Called from SetChildNodes.
		/// <summary>
		/// Used to get all the child nodes.
		/// </summary>
		/// <param name="arrID"></param>
		/// <param name="arrAllID"></param>
		/// <param name="arrChildNodes"></param>
		/// <param name="p_iChildNodes"></param>
		/// <returns>A boolean values that represents the success/failure.
		/// </returns>
		private bool GetAllChildNodes(string[] p_arrID,int[] p_arrAllID,ArrayList p_arrChildNodes,ref string p_iChildNodes)
		{
			bool bReturn = false;
			int iCount = 0, jCount = 0;
			p_iChildNodes = "";
			
			try
			{
				for(iCount=0;iCount<p_arrID.Length;iCount++)
				{
					for(jCount=0;jCount<p_arrAllID.Length;jCount++)
					{
						if(p_arrID[iCount] == p_arrAllID[jCount].ToString())
						{
							if(((string)p_arrChildNodes[jCount]) != "")
							{
								if(p_iChildNodes == "")
									p_iChildNodes = ((string)p_arrChildNodes[jCount]);
								else
									p_iChildNodes = p_iChildNodes + "|" + ((string)p_arrChildNodes[jCount]);
								break;
							}
						}
					}
				}
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.GetAllChildNodes.GetAllChildNodesError", m_iClientId), p_objException);
			}
			finally
			{
			}
			return bReturn;
		}
		
		//-- Not Yet Tested. Called from SetChildNodes.
		/// <summary>
		/// Used to get the child nodes for every node.
		/// </summary>
		/// <param name="arrChildNodes">An array passed by ref, represents the
		/// child nodes for every node.
		/// </param>
		/// <param name="arrAllID">An array representing all the IDs.</param>
		/// <param name="arrAllParentID">An array representing all the Parent IDs.</param>
		/// <returns>A boolean values that represents the success/failure.
		/// </returns>
		public bool GetChildNodes(ref ArrayList p_arrChildNodes, int[] p_arrAllID, int[] p_arrAllParentID)
		{
			bool bReturn = false;
			int iCount = 0, jCount = 0;
			string sChildNodes = "";

			try
			{
				for(iCount=0;iCount<p_arrAllID.Length;iCount++)
				{
					for(jCount=0;jCount<p_arrAllParentID.Length;jCount++)
					{
						if(p_arrAllID[iCount] == p_arrAllParentID[jCount])
						{
							if(sChildNodes == "")
								sChildNodes = p_arrAllID[jCount].ToString();
							else
								sChildNodes = sChildNodes + "|" + p_arrAllID[jCount].ToString();
						}
					}
					p_arrChildNodes.Insert(iCount,sChildNodes);
				}
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.GetChildNodes.GetChildNodesError", m_iClientId), p_objException);
			}
			finally
			{
			}
			return bReturn;
		}
		
		//-- Not Yet Tested. Called from UpdateModuleAccessPermissions.
		/// <summary>
		/// Used to determine whether the p_iID passed to this function has any parent.
		/// If yes, p_iParentID which is passed by ref gives that parent ID.
		/// </summary>
		/// <param name="p_iID">An ID passed to determine whether parent ID exists
		/// for this ID or not.
		/// </param>
		/// <param name="p_arrChild">An array representing all the Child IDs.</param>
		/// <param name="p_arrParent">An array representing all the Parent IDs.</param>
		/// <param name="p_iParentID">Represents the Parent ID for the p_iID</param>
		/// <returns>A boolean values that represents the success/failure.
		/// </returns>
		public bool IsParent(int p_iID, string[] p_arrChild, string[] p_arrParent,ref int p_iParentID)
		{
			bool bReturn = false;
			int jCount = 0;
		    
			try
			{
				if(p_iID == 0)
				{
					p_iParentID = 0;
					bReturn = true;
					return bReturn;
				}
		    
				for(jCount=0;jCount<p_arrChild.Length;jCount++)
				{
					if(p_iID.ToString() == p_arrChild[jCount])
					{	
						p_iParentID = Convert.ToInt32(p_arrParent[jCount]);
						bReturn = true;
						return bReturn;
					}
					else
						bReturn = false;
				}
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			return bReturn;
		}

		/// <summary>
		/// Used to rename the module group.
		/// </summary>
		/// <param name="p_iNewMGName">Represents the new name for the group.</param>
		/// <param name="p_iGroupID">Represents the group ID whose name is to be
		/// changed.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		public bool Rename(string p_sNewMGName,int p_iGroupID)
		{
			bool bReturn = false;
			DbWriter objWriter;
			try
			{
				objWriter = DbFactory.GetDbWriter(m_sConnectionString);
				objWriter.Tables.Add("USER_GROUPS");
				objWriter.Fields.Add("GROUP_NAME",p_sNewMGName.Trim());
                //gagnihotri MITS 11995 Changes made for Audit table
                objWriter.Fields.Add("UPDATED_BY_USER", m_sUserName);
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
				objWriter.Where.Add("GROUP_ID="+p_iGroupID);
				objWriter.Execute();
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.Rename.RenameError", m_iClientId), p_objException);
			}
			finally
			{
			  objWriter = null;
			}
			return bReturn;
		}
		
		//-- Not Yet Tested. Called from UpdateModuleAccessPermissions.
		/// <summary>
		/// Saves the module access permissions based on the p_sAllowedModules.
		/// </summary>
		/// <param name="p_iGroupID">Group ID for which allowed modules are to be
		/// added.
		/// </param>
		/// <param name="p_sAllowedModules">A string representing the allowed 
		/// modules.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		private bool SaveModuleAccessPermissions(int p_iGroupID, string p_sAllowedModules)
		{
			bool bReturn = false;
			string sSQL = "";
			int iCount = 0;
			DbConnection objConn = null;
			DbWriter objWriter;
			DbTransaction objTrans = null;
		    
			try
			{
				string[] arrAllowedModules = p_sAllowedModules.Split(',');

				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				sSQL = "DELETE FROM GROUP_PERMISSIONS WHERE GROUP_ID=" + p_iGroupID;
				objConn.ExecuteNonQuery(sSQL); 
				if(arrAllowedModules.Length==1 && arrAllowedModules[0]=="")
				{
					bReturn = true;
					objConn.Close();
					return bReturn;

				}
				objTrans = objConn.BeginTransaction();
				objWriter = DbFactory.GetDbWriter(m_sConnectionString);
				
				for(iCount=0;iCount<arrAllowedModules.Length;iCount++)
				{
					objWriter.Reset(true);
					objWriter.Tables.Add("GROUP_PERMISSIONS");
					objWriter.Fields.Add("GROUP_ID",p_iGroupID);
					objWriter.Fields.Add("FUNC_ID",arrAllowedModules[iCount]);
                    //gagnihotri MITS 11995 Changes made for Audit table
                    objWriter.Fields.Add("UPDATED_BY_USER", m_sUserName);
                    objWriter.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
					objWriter.Execute();				
				}
			
				objTrans.Commit();
				bReturn = true;
			}
			catch(Exception p_objException)
			{
				if(objTrans != null)
					objTrans.Rollback();
                 throw p_objException;
			}
			finally
			{
				if(objConn != null)
				{
					//if(objConn.State == System.Data.ConnectionState.Open)
					//{
					//	objConn.Close();
						objConn.Dispose();
					//}
				}
				objWriter = null;
				if(objTrans != null)
				{
					objTrans.Dispose();
				}

			}
			return bReturn;
		}

		//-- Not Yet Tested. Will be tested along with other related functions.
		/// <summary>
		/// This function is used to set the child nodes for every node in the module
		/// access permissions.
		/// </summary>
		/// <param name="p_iID">Represents the ID of the selected node.</param>
		/// <param name="p_arrAllID">An array representing all the IDs.</param>
		/// <param name="p_arrAllParentID">An array representing all the Parent IDs.</param>
		/// <param name="p_sModules">A comma separated string representing the 
		/// allowed modules with their ID and parent ID.
		/// </param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		public bool SetChildNodes(int p_iID,int[] p_arrAllID,int[] p_arrAllParentID,ref string p_sModules)
		{
			bool bReturn = false;
			int iCount = 0, jCount = 0, iSelIndex = 0; 
			string[] arrTemp;
			string[] arrSubChildren;
			//int[] arrChildNodes = null;
			ArrayList arrChildNodes = new ArrayList();
			string sChildNodes = "", sSelNodeInfo = "";
		    
			try
			{
				//-- Get the index of the selected node
				for(iCount=0;iCount<p_arrAllID.Length;iCount++)
				{	
					if(p_iID == p_arrAllID[iCount])
					{
						iSelIndex = iCount;
						sSelNodeInfo = p_arrAllID[iCount] + "|" + p_arrAllParentID[iCount];
						break;
					}
				}
		    
				GetChildNodes(ref arrChildNodes,p_arrAllID, p_arrAllParentID);
				arrSubChildren = ((string)arrChildNodes[iSelIndex]).Split('|');
		    
				if(((string)arrChildNodes[iSelIndex]) == "")
				{
					p_sModules = sSelNodeInfo;
					bReturn = true;
					return bReturn;
				}
		    
				for(iCount=0;iCount<arrSubChildren.Length;iCount++)
				{
					if(p_sModules == "")
						p_sModules = arrSubChildren[iCount].ToString();
					else
						p_sModules = p_sModules + "|" + arrSubChildren[iCount];
				}
		    
				GetAllChildNodes(arrSubChildren,p_arrAllID,arrChildNodes,ref sChildNodes);
		    
				if(p_sModules == "")
					p_sModules = sChildNodes;
				else
					p_sModules = p_sModules + "|" + sChildNodes;
			
		    
				while(sChildNodes != "")
				{
					arrTemp = sChildNodes.Split('|');
					arrSubChildren = arrTemp;
					GetAllChildNodes(arrSubChildren,p_arrAllID,arrChildNodes,ref sChildNodes);
					if(p_sModules == "")
						p_sModules = sChildNodes;
					else
						p_sModules = p_sModules + "|" + sChildNodes;
				}
		    
				arrTemp = p_sModules.Split('|');
				p_sModules = "";
				for(iCount=0;iCount<arrTemp.Length;iCount++)
				{
					for(jCount=0;jCount<p_arrAllID.Length;jCount++)
					{
						if(arrTemp[iCount] == p_arrAllID[jCount].ToString())
						{
							if(p_sModules == "")
								p_sModules = arrTemp[iCount] + "|" + p_arrAllParentID[jCount];
							else
								p_sModules = p_sModules + "," + arrTemp[iCount] + "|" + p_arrAllParentID[jCount];
						}
					}
				}
		    
				if(p_sModules != "")
					p_sModules = sSelNodeInfo + "," + p_sModules;
				
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("ModuleGroups.SetChildNodes.SetChildNodesError", m_iClientId), p_objException);
			}
			finally
			{
                arrChildNodes = null;
			}
			return bReturn;
		}

		//-- Not Yet Tested.
		/// <summary>
		/// Updates the module access permissions.
		/// </summary>
		/// <param name="p_iGroupID">Represents the group ID for which module access 
		/// permission is to be updated.</param>
		/// <returns>A boolean values that represents the success/failure.</returns>
		public bool UpdateModuleAccessPermissions(int p_iGroupID)
		{
			bool bReturn = false;
			string[] arrAllowedModules,arrTemp,arrParent = null,arrChild = null;
			bool bOK;
			string sAllowed = "";
			int iCount = 0, iParentID = 0, iNewParent = -1;

			try
			{
				
				arrAllowedModules = m_sAllowedModules.Split(',');
				if(arrAllowedModules.Length==1 && arrAllowedModules[0]=="")
				{
					SaveModuleAccessPermissions(p_iGroupID,sAllowed);
					return true;


				}
		        arrParent=new string[arrAllowedModules.Length];
				arrChild=new string[arrAllowedModules.Length];
				for(iCount=0;iCount<arrAllowedModules.Length;iCount++)
				{
					arrTemp = arrAllowedModules[iCount].Split('|');
					arrParent[iCount] = arrTemp[1];
					arrChild[iCount] = arrTemp[0];
				}
		        
				for(iCount=0;iCount<arrAllowedModules.Length;iCount++)
				{
					iParentID = Convert.ToInt32(arrParent[iCount]);
					bOK = false;
					if(iParentID == 0)
					{
						if(sAllowed == "")
							sAllowed = arrChild[iCount];
						else
							sAllowed = sAllowed + "," + arrChild[iCount];
						bOK = false;
					}
					else
					{
						while(IsParent(iParentID, arrChild, arrParent, ref iNewParent))
						{
							iParentID = iNewParent;
							if(iParentID == 0)
							{
								bOK = true;
								break;
							}
							bOK = true;
							if(!IsParent(iParentID, arrChild, arrParent, ref iNewParent))
							{
								bOK = false;
								break;
							}
						}
					}
					if(bOK)
					{
						if(sAllowed == "")
							sAllowed = arrChild[iCount];
						else
							sAllowed = sAllowed + "," + arrChild[iCount];
					}
				}
				
					if(!SaveModuleAccessPermissions(p_iGroupID,sAllowed))
					{
						bReturn = false;
						return bReturn;
					}
				
				bReturn = true;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
			}
			return bReturn;
		}

		/// <summary>
		/// Represents the Module Group ID.
		/// </summary>
		public int ModuleGroupID
		{
			get
			{
				return m_iID;
			}
			set
			{
				m_iID = value;
			}
		}

		/// <summary>
		/// Represents the allowed modules.
		/// </summary>
		public string GetAllowedModules
		{
			get
			{
				return m_sAllowedModules;
			}
			set
			{
				m_sAllowedModules = value;
			}
		}
	}
	/// <summary>
	/// This exception is holds load module group errors.
	/// </summary>
	public class LoadModuleGrpException:Exception
	{

	}
}
