using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using System.Collections;
using System.Threading;

namespace Riskmaster.Application.SecurityManagement
{
	
	/// <summary>
	/// This class handles the security tree postbacks.
	/// </summary>
	public class PostbackHandler
	{
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public PostbackHandler()
		{
			
		}

        /// <summary>
        /// This function loads the module groups.
        /// </summary>
        /// <param name="p_objPostedDoc">The document to which module groups will be appended.</param>
        /// <param name="objLeftTreeTemplate">Tree template</param>
        /// <param name="p_objContextDsn">Dsn for which module groups to be appended</param>
        /// <param name="p_objNodeToBeLoaded">Xmlelement object to which modules will be appended.</param>
        /// <param name="objDataSource">Datasource Xmlelement object.</param>
        public static void LoadModuleGrpsHandlerForLeftTree(ref XmlDocument p_objPostedDoc, ref XmlDocument objLeftTreeTemplate
            , int p_objContextDsn, ref XmlElement p_objNodeToBeLoaded, ref XmlElement objDataSource, int p_iClientId)
        {
            if (p_objPostedDoc.SelectSingleNode("//IsPostBack") != null
                && p_objPostedDoc.SelectSingleNode("//TreePostedBy") != null
                && p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG") != null)
            {
                if (p_objPostedDoc.SelectSingleNode("//IsPostBack").InnerText == "true"
                    && p_objPostedDoc.SelectSingleNode("//TreePostedBy").InnerText == "LoadModuleGroups"
                    && p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText != "")
                {
                    //srajindersin MITS 30263 dt 11/27/2012
                    if (p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText == p_objContextDsn.ToString())
                    {
                        try
                        {
                            AppendModuleGroups(ref p_objNodeToBeLoaded,
                                Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText, p_iClientId),
                                ref objLeftTreeTemplate, true, p_iClientId);
                        }
                        catch (Exception p_objException)
                        {
                            string sErr = "";
                            if (p_objException.Message.IndexOf("\r\n") != -1)
                            {
                                sErr = p_objException.Message.Replace("\r\n", "");
                            }
                            else
                            {
                                sErr = p_objException.Message;
                            }
                            p_objPostedDoc.SelectSingleNode("//Errors/Warning").InnerText = sErr;
                        }
                        objDataSource.SetAttribute("expanded", "true");

                        p_objPostedDoc.SelectSingleNode("//PostBackResults/PageToBeShownInMainFrame").InnerText = "home?pg=riskmaster/SecurityMgtSystem/NoData";

                        ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded", "true");
                        //srajindersin MITS 30263 dt 11/27/2012
                        Helper.SetPostBackValues(ref p_objPostedDoc, SecurityManagement_us_en.NoData, "Root Node Module Security Groups", p_objContextDsn.ToString());
                    }
                }
            }
        }
		/// <summary>
		/// This function handles the postback eventfor adding new user.
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for new user.</param>
		/// <param name="objLeftTreeTemplate">Security left tree structure.</param>
		public static void AddNewUserHandler(ref XmlDocument p_objPostedDoc,ref XmlDocument objLeftTreeTemplate, int p_iClientId)
		{
			int iUserIdMax=0;
			int iDsnIdSelected=0;
			string sTempIdDb="";
			string sTempDsnIdSelected="";
            string sCompanyName = string.Empty;
			Users objUser;
            try
            {
                objUser = new Users(Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//AddNewUser/NewUserId").InnerText, p_iClientId), p_iClientId);
                iUserIdMax = objUser.UserId;
                //iUserIdMax=Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//AddNewUser/NewUserId").InnerText);
                iDsnIdSelected = Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//UpdateUserEntity/UserEntityDsnIdSelected").InnerText, p_iClientId);
                if (iDsnIdSelected == 0)
                {
                    ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='User' and @idDb='" + iUserIdMax + "']")).SetAttribute("highlight", "true");
                    if (objUser.CompanyName.Trim() != "")
                    {
                        //Parijat:Changes required for Mits 8887, incase new user is added with company name consisting a single quote.
                        sCompanyName = objUser.CompanyName.Trim();
                        sCompanyName = sCompanyName.ToUpper();
                        sCompanyName = sCompanyName.Replace("'", "&quot;");
                        ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node for Company' and @idDb='COMPANY:" + sCompanyName + "']")).SetAttribute("expanded", "true");
                        //

                    }
                    ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Security Users']")).SetAttribute("expanded", "true");

                    Helper.SetPostBackValues(ref p_objPostedDoc,
                        "",
                        "User",
                        iUserIdMax.ToString());
                }
                else
                {
                    sTempDsnIdSelected = iDsnIdSelected.ToString();
                    sTempIdDb = iDsnIdSelected.ToString() + "," + iUserIdMax.ToString();
                    ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='PermUser' and @idDb='" + sTempIdDb + "']")).SetAttribute("highlight", "true");
                    ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Datasource' and @idDb='" + sTempDsnIdSelected + "']")).SetAttribute("expanded", "true");
                    ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded", "true");
                    ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node for PermToLogin' and @idDb='" + sTempDsnIdSelected + "']")).SetAttribute("expanded", "true");

                    Helper.SetPostBackValues(ref p_objPostedDoc,
                        "",
                        "PermUser",
                        iDsnIdSelected.ToString() + "," + iUserIdMax.ToString());
                }



            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objUser = null;
            }
		}
		/// <summary>
		/// This function handles the update user functionality.
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for updating the user.</param>
		/// <param name="objLeftTreeTemplate">Security left tree structure.</param>
        public static void UpdateUserHandler(ref XmlDocument p_objPostedDoc, ref XmlDocument objLeftTreeTemplate, int p_iClientId)
		{
			int iUpdatedUserId=0;
			Users objUser;
			int iUpdatedUserDsnId=0;
            string sCompanyName = string.Empty;
			try
			{
                objUser = new Users(Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//UpdateUserEntity/UserEntityUserId").InnerText, p_iClientId), p_iClientId);

                iUpdatedUserId = Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//UpdateUserEntity/UserEntityUserId").InnerText, p_iClientId);
				((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='User' and @idDb='" + iUpdatedUserId + "']")).Attributes["text"].Value=objUser.FirstName + " " + objUser.LastName;

                iUpdatedUserDsnId = Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//UpdateUserEntity/UserEntityDsnIdSelected").InnerText, p_iClientId);
				if(iUpdatedUserDsnId==0)
				{
					p_objPostedDoc.SelectSingleNode("//PostBackResults/PageToBeShownInMainFrame").InnerText="home?pg=riskmaster/SecurityMgtSystem/user?userid="+iUpdatedUserId.ToString();
				
					((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Security Users']")).SetAttribute("expanded","true") ;
					if (objUser.CompanyName.Trim() != "")
					{
                        //Parijat:Changes required for Mits 8887, incase a user is updated with company name consisting a single quote.
                        sCompanyName = objUser.CompanyName.Trim();
                        sCompanyName = sCompanyName.ToUpper();
                        sCompanyName = sCompanyName.Replace("'", "&quot;");
                        ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node for Company' and @idDb='COMPANY:" + sCompanyName + "']")).SetAttribute("expanded", "true");
                        //
						
					}
					((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='User' and @idDb='" + iUpdatedUserId + "']")).SetAttribute("highlight","true") ;
					Helper.SetPostBackValues(ref p_objPostedDoc,
						"",
						"User",
						iUpdatedUserId.ToString());
				}
				else
				{  
					string sTemp=iUpdatedUserDsnId.ToString() + "," + iUpdatedUserId.ToString()  ;
					string sTempDsnId=iUpdatedUserDsnId.ToString();
					//((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='PermUser' and @idDb='" + sTemp + "']")).SetAttribute("highlight","true") ;
					//((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Datasource' and @idDb='" + sTempDsnId + "']")).SetAttribute("expanded","true") ;
					//((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true") ;
					//((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node for PermToLogin' and @idDb='" + sTempDsnId + "']")).SetAttribute("expanded","true") ;

					Helper.SetPostBackValues(ref p_objPostedDoc,
						"",
						"PermUser",
						iUpdatedUserDsnId.ToString()+","+iUpdatedUserId.ToString());
				}
					                     

			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objUser=null;
			}
		}
		/// <summary>
		/// This function checks whether a dsn has module groups or not.
		/// </summary>
		/// <param name="p_iDsnId">Dsn id</param>
		/// <param name="p_objModuleSecGroupsElement">Module groups node.</param>
        public static void CheckForExistingModuleGrps(int p_iDsnId, ref XmlElement p_objModuleSecGroupsElement, int p_iClientId)
		{
			RMWinSecurity objRmWin=null;
			ArrayList arrlstModuleGps=null;
			ArrayList arrlstAllowedUsers=null;
			
			try
			{
				arrlstModuleGps=new ArrayList();
				arrlstAllowedUsers=new ArrayList();
				objRmWin=new RMWinSecurity(p_iClientId);
				objRmWin.LoadModuleGroups(p_iDsnId,ref arrlstModuleGps,ref arrlstAllowedUsers);
				if(arrlstModuleGps.Count!=0)
				{
					p_objModuleSecGroupsElement.SetAttribute("hasModuleGroups","true");
				}
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				objRmWin=null;
				arrlstModuleGps=null;
				arrlstAllowedUsers=null;
			}

		}
		/// <summary>
		/// This functin appends the module groups for a particular Dsn.
		/// </summary>
		/// <param name="p_objModuleSecGroupsElement">Module security groups element</param>
		/// <param name="p_iDsnId">Dsn id; to which module groups will be appended.</param>
		/// <param name="p_objXmlDoc">Contains the parameters for appending the module groups.</param>
		/// <param name="bInvokedByLoadModuleGrpsHandler">Flags whether this function is invoked from LoadModuleGroups function or not.</param>
		public static void AppendModuleGroups(ref XmlElement p_objModuleSecGroupsElement, int p_iDsnId,
			ref XmlDocument p_objXmlDoc,bool bInvokedByLoadModuleGrpsHandler,  int p_iClientId)
		{
			
			RMWinSecurity objRmWin;
			ArrayList arrlstModuleGps;
			ArrayList arrlstAllowedUsers;
			XmlElement objModuleGrpElement;
			ArrayList arrlstObjectModule;
			ArrayList arrlstObjectUserAllowed;
			XmlElement objUserAllowedInMg;
			bool bHasModuleGrps=false;
			try
			{
				arrlstModuleGps=new ArrayList();
				arrlstAllowedUsers=new ArrayList();
				objRmWin=new RMWinSecurity(p_iClientId);
				try
				{
					objRmWin.LoadModuleGroups(p_iDsnId,ref arrlstModuleGps,ref arrlstAllowedUsers);
				}
				catch(Exception p_objException)
				{
					 throw p_objException;
				}
				foreach(string sTempModuleGrp in  arrlstModuleGps)
				{  
					bHasModuleGrps=true;
					arrlstObjectModule=new ArrayList();
					objModuleGrpElement=p_objXmlDoc.CreateElement("entry");
					Helper.CrackModuleGrpInfo(sTempModuleGrp,ref arrlstObjectModule, p_iClientId);
					Helper.CreateElement(ref objModuleGrpElement,arrlstObjectModule[0].ToString(),"Module Security Group",
						SecurityManagement_us_en.GrpModuleAccessPermission+"?dsnid="+arrlstObjectModule[1].ToString()+"&groupid="+arrlstObjectModule[2].ToString(),
						SecurityManagement_us_en.ModuleSecurityGroups,
						"link","Module Security Group",arrlstObjectModule[1].ToString()+ "," + arrlstObjectModule[2].ToString());
					

					foreach(string sTempAllowedUser in arrlstAllowedUsers)
					{
						arrlstObjectUserAllowed=new ArrayList();
                        Helper.CrackModuleGrpInfo(sTempAllowedUser, ref arrlstObjectUserAllowed, p_iClientId);
						if(arrlstObjectModule[0].ToString().Trim().ToLower() == arrlstObjectUserAllowed[0].ToString().Trim().ToLower())
						{
							objUserAllowedInMg=p_objXmlDoc.CreateElement("entry");
							Helper.CreateElement(ref objUserAllowedInMg,arrlstObjectUserAllowed[2]+ " " + arrlstObjectUserAllowed[3] ,"Allowed User",
                                SecurityManagement_us_en.NoData + "?dsnid=" + arrlstObjectUserAllowed[4].ToString() + "&groupid=" + arrlstObjectModule[2].ToString() + "&userid=" + arrlstObjectUserAllowed[1].ToString(),
								SecurityManagement_us_en.SecUser,
								"link","Module Group User",arrlstObjectUserAllowed[4]+ ","+ arrlstObjectModule[2].ToString() + ","+arrlstObjectUserAllowed[1]);

							objModuleGrpElement.AppendChild(objUserAllowedInMg);
						}

					}
					arrlstObjectUserAllowed=null;
					arrlstObjectModule=null;
					p_objModuleSecGroupsElement.AppendChild(objModuleGrpElement);
				}
			
				if(bHasModuleGrps && bInvokedByLoadModuleGrpsHandler)
				{
					p_objModuleSecGroupsElement.SetAttribute("hasModuleGroups","true");
					p_objModuleSecGroupsElement.SetAttribute("expanded","true");
					p_objModuleSecGroupsElement.SetAttribute("highlight","true");

				}
              
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				 objRmWin=null;
				 arrlstModuleGps=null;
				 arrlstAllowedUsers=null;
				 objModuleGrpElement=null;
				 arrlstObjectModule=null;
				 arrlstObjectUserAllowed=null;
				 objUserAllowedInMg=null;
			}
		}
		
		/// <summary>
		/// This function handles the update permitted user postback.
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for updating the permitted user</param>
		/// <param name="objLeftTreeTemplate">Security left tree structure.</param>
		/// <param name="p_objContextDsn">Datasource object.</param>
		/// <param name="p_objNodeToBeLoaded">Node to which information to be appended.</param>
		/// <param name="objDataSource">Datasource element.</param>
		public static void UpdateUserPermHandler(ref XmlDocument p_objPostedDoc,ref XmlDocument objLeftTreeTemplate 
			,int p_objContextDsn,ref XmlElement p_objNodeToBeLoaded,ref XmlElement objDataSource, int p_iClientId)
			
		{
			string sTempMGIdentity="";
			string sTempPermUserIdentity="";
			int iExistingMGrpId=0;
			string sConnStr="";

			try
			{
				if(p_objPostedDoc.SelectSingleNode("//IsPostBack")!=null
					&& p_objPostedDoc.SelectSingleNode("//TreePostedBy")!=null
					&& p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG")!=null)
				{
					if(p_objPostedDoc.SelectSingleNode("//TreePostedBy").InnerText=="UpdateUserPermEntity"
						&& p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText!=""
						&& p_objPostedDoc.SelectSingleNode("//UpdateUserPermEntity/UserPermModuleGrpSelectedId").InnerText!="")
					{
						//Raman for R5
                        //srajindersin MITS 30263 dt 11/27/2012
                        if(p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText==p_objContextDsn.ToString())
						{
							AppendModuleGroups(ref p_objNodeToBeLoaded,
                                Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText, p_iClientId),
								ref objLeftTreeTemplate,false, p_iClientId);
                            if (Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText, p_iClientId) != 0)
							{
                                sConnStr = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText, p_iClientId), p_iClientId);
							}
							
							PublicFunctions.CheckUserMembership(ref iExistingMGrpId,
                                Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//UpdateUserPermEntity/UserPermEntityUserId").InnerText, p_iClientId),
                                sConnStr, p_iClientId);
							if(iExistingMGrpId!=0)
								p_objPostedDoc.SelectSingleNode("//UpdateUserPermEntity/UserPermModuleGrpSelectedId").InnerText = iExistingMGrpId.ToString();
							sTempMGIdentity=p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText+","+
								p_objPostedDoc.SelectSingleNode("//UpdateUserPermEntity/UserPermModuleGrpSelectedId").InnerText;
							
							sTempPermUserIdentity=p_objPostedDoc.SelectSingleNode("//DsnIdForLoadingMG").InnerText+","+
								iExistingMGrpId.ToString()+","+
								p_objPostedDoc.SelectSingleNode("//UpdateUserPermEntity/UserPermEntityUserId").InnerText;
							
							p_objNodeToBeLoaded.SetAttribute("expanded","true");
								
							objDataSource.SetAttribute("expanded","true");
							p_objNodeToBeLoaded.SetAttribute("expanded","true");
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
							((XmlElement)p_objNodeToBeLoaded.SelectSingleNode("//entry[@entityType='Module Security Group' and @idDb='" + sTempMGIdentity + "']")).SetAttribute("expanded","true");

							((XmlElement)p_objNodeToBeLoaded.SelectSingleNode("//entry[@entityType='Module Group User' and @idDb='" + sTempPermUserIdentity + "']")).SetAttribute("highlight","true");
								
							Helper.SetPostBackValues(ref p_objPostedDoc,"","PermUser",sTempPermUserIdentity);
						}
					}
					else
					{
						//simply focus on to Perm user...
						((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
                        //srajindersin MITS 30263 dt 11/27/2012
                        Helper.SetPostBackValues(ref p_objPostedDoc,"","Root Node Module Security Groups",p_objContextDsn.ToString());
					}
				}
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}
	
		/// <summary>
		/// This function handles the add new datasource functionality.
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for adding a new datasource.</param>
		/// <param name="objLeftTreeTemplate">Security left tree structure.</param>
        public static void AddNewDatasourceHandler(ref XmlDocument p_objPostedDoc, ref XmlDocument objLeftTreeTemplate, int p_iClientId)
		{
			int iDBIdMax=0;
			try
			{
                iDBIdMax = Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//AddNewDatasourceEntity/AddedDBId").InnerText, p_iClientId);
				
				((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Datasource' and @idDb='" + iDBIdMax + "']")).SetAttribute("highlight","true") ;
				Helper.SetPostBackValues(ref p_objPostedDoc,
					"",
					"Datasource",
					iDBIdMax.ToString());
				((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true") ;

			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}
		/// <summary>
		/// This function handles the delete functionality.
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for deleting entity.</param>
		public static void DeleteDBEntities(ref XmlDocument p_objPostedDoc, int p_iClientId)
		{
			string sEntityType="";
			string sIdDb="";
			try
			{
				sEntityType=p_objPostedDoc.SelectSingleNode("//SelectedEntityFromLeftTree").InnerText;
				sIdDb=p_objPostedDoc.SelectSingleNode("//Delete/EntityIdForDeletion").InnerText;
				Delete(sEntityType,sIdDb,p_iClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}
		/// <summary>
		/// This function handles the postback done by delete button. 
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for delete functionality.</param>
		/// <param name="objLeftTreeTemplate">Security left tree structure.</param>
		public static void DeleteHandler(ref XmlDocument p_objPostedDoc,ref XmlDocument objLeftTreeTemplate)
		{
			string sEntityType="";
			string sIdDb="";
			try
			{
				sEntityType=p_objPostedDoc.SelectSingleNode("//SelectedEntityFromLeftTree").InnerText;
				sIdDb=p_objPostedDoc.SelectSingleNode("//Delete/EntityIdForDeletion").InnerText;
				switch (sEntityType)
				{
					case "User":
					{
						((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Security Users']")).SetAttribute("expanded","true");
						break;
					}
					case "Datasource":
					{
						((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
						break;
					}
					case "UserPerm":
					{ 
						string[] arrIds=null;
						try 
						{
							arrIds=PublicFunctions.BreakDbId(sIdDb);
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Datasource' and @idDb='" + arrIds[0] + "']")).SetAttribute("expanded","true");
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node for PermToLogin' and @idDb='" + arrIds[0] + "']")).SetAttribute("expanded","true");
						}
						catch(Exception p_objException)
						{
							throw p_objException;
						}
						finally
						{
							arrIds=null;

						}
						break;
					}
					case "Module Security Group":case "GrpModuleAccessPermission":
					{
						string[] arrIds;
						try
						{
							arrIds=PublicFunctions.BreakDbId(sIdDb);
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Datasource' and @idDb='" + arrIds[0] + "']")).SetAttribute("expanded","true");
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Module Security Groups' and @idDb='" + arrIds[0] + "']")).SetAttribute("expanded","true");
						}
						catch(Exception p_objException)
						{
							throw p_objException;
						}
						finally
						{
							arrIds=null;
						}

						break;
					}
					case "Module Group User":
					{
						string[] arrIds;
						string sMGrpIdentity="";
						try
						{
							arrIds =PublicFunctions.BreakDbId(sIdDb);
							sMGrpIdentity= arrIds[0]+","+arrIds[1];
							
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Datasource' and @idDb='" + arrIds[0] + "']")).SetAttribute("expanded","true");

							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Module Security Groups' and @idDb='" + arrIds[0] + "']")).SetAttribute("expanded","true");
							((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Module Security Group' and @idDb='" + sMGrpIdentity + "']")).SetAttribute("expanded","true");
						}
						catch(Exception p_objException)
						{
							throw p_objException;
						}
						finally
						{
							arrIds=null;
							sMGrpIdentity=null;
						}
						 
						break;
					}
					default:
					{
						throw new RMAppException("Unknown entity selected for deletion");
					}
				}
				((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskamster Security Tree']")).SetAttribute("highlight","true");
				Helper.SetPostBackValues(ref p_objPostedDoc,
					"",
					"Root Node Riskamster Security Tree",
					"");

			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			
		}
		/// <summary>
		/// This function actually deletes the entries from the database.
		/// </summary>
		/// <param name="p_sEntityToBeDeleted">Type of the entity to be deleted</param>
		/// <param name="p_sIdDB">Database id.</param>
        private static void Delete(string p_sEntityToBeDeleted, string p_sIdDB, int p_iClientId)
		{
            SMSAccessManager objSMSAccessMgr ;
			switch (p_sEntityToBeDeleted)
			{
				case "User":
				{
					Users objUser;
					try
					{
                        string[] sDbIds = PublicFunctions.BreakDbId(p_sIdDB);
                        //MITS 14868 : abansal23 Starts
                        //UserLogins objPermUser = new UserLogins(Conversion.ConvertObjToInt(sDbIds[1]), Conversion.ConvertObjToInt(sDbIds[0])); 

                        objSMSAccessMgr = new SMSAccessManager(Conversion.ConvertObjToInt(sDbIds[0], p_iClientId), p_iClientId);
                        //MITS 14868 : abansal23 Ends

                        objSMSAccessMgr.ValidateForAdminUser("", Conversion.ConvertObjToInt(p_sIdDB, p_iClientId));

                        objUser = new Users(p_iClientId);

                        objUser.Remove(Conversion.ConvertObjToInt(p_sIdDB, p_iClientId));
					}
					catch(Exception p_objException)
					{
						throw p_objException;
					}
					finally
					{
						objUser=null;
					}
					break;
				}
				case "Datasource":
				{
					DataSources objDatasource;
					try
					{
                        objDatasource = new DataSources(p_iClientId);
                        objDatasource.Remove(Conversion.ConvertObjToInt(p_sIdDB, p_iClientId));
					}
					catch(Exception p_objException)
					{
						throw p_objException;
					}
					finally
					{
						objDatasource=null;
					}
					break;
				}
				case "PermUser":case "UserPerm":
				{
					UserLogins objPermUser;
					Users objUsers;
					string[] sDbIds;
					try
					{
						

						sDbIds=PublicFunctions.BreakDbId(p_sIdDB) ;

                        objPermUser = new UserLogins(Conversion.ConvertObjToInt(sDbIds[1], p_iClientId), Conversion.ConvertObjToInt(sDbIds[0], p_iClientId), p_iClientId);

                        objSMSAccessMgr = new SMSAccessManager(objPermUser.UserId, p_iClientId);
						objSMSAccessMgr.ValidateForAdminUser ( objPermUser.LoginName , objPermUser.UserId ) ;

                        objPermUser.Remove(Conversion.ConvertObjToInt(sDbIds[1], p_iClientId),
                            Conversion.ConvertObjToInt(sDbIds[0], p_iClientId));
                        objUsers = new Users(p_iClientId);
					}
					catch(Exception p_objException)
					{
						throw p_objException;
					}
					finally
					{
						objPermUser=null;
						sDbIds=null;
					}
					break;
				}
				case "Module Security Group":case "GrpModuleAccessPermission":
				{
					ModuleGroups objMGrp;
					string sConnStr;
					string[] sDbIds;
					try
					{
						sDbIds=PublicFunctions.BreakDbId(p_sIdDB);
                        sConnStr = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(sDbIds[0], p_iClientId), p_iClientId);
                        objMGrp = new ModuleGroups(sConnStr, p_iClientId);
                        objMGrp.ModuleGroupID = Conversion.ConvertObjToInt(sDbIds[1], p_iClientId);
						objMGrp.Remove();
					}
					catch(Exception p_objException)
					{
						throw p_objException;
					}
					finally
					{
						objMGrp=null;
						sConnStr=null;
						sDbIds=null;
					}
					break;
				}
				case "Module Group User":
				{
					ModuleGroups objMGrp;
					string sConnStr;
					string[] sDbIds;
					try
					{
						sDbIds=PublicFunctions.BreakDbId(p_sIdDB);
                        sConnStr = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(sDbIds[0], p_iClientId), p_iClientId);
                        objMGrp = new ModuleGroups(sConnStr, p_iClientId);
                        objMGrp.DeleteUsers(Conversion.ConvertObjToInt(sDbIds[1], p_iClientId), Conversion.ConvertObjToInt(sDbIds[2], p_iClientId));
					}
					catch(Exception p_objException)
					{
						throw p_objException;
					}
					finally
					{
						objMGrp=null;
						sConnStr=null;
						sDbIds=null;
					}
					break;
				}
				default :
				{
					break;
				}
			}
		}
		/// <summary>
		/// This function handles add new module group functionality.
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for adding new module group.</param>
		/// <param name="objLeftTreeTemplate">Security left tree structure.</param>
		/// <param name="p_objContextDsn">Datasource object</param>
		/// <param name="p_objNodeToBeLoaded">Node to which data will be appended.</param>
		/// <param name="objDataSource">Datasource element to be highlighted.</param>
		public static void AddNewModuleGroupHandler(ref XmlDocument p_objPostedDoc,ref XmlDocument objLeftTreeTemplate
            , int p_objContextDsn, ref XmlElement p_objNodeToBeLoaded, ref XmlElement objDataSource, int p_iClientId)
		{
			string sEntityType="";
			string sEntityId="";
			int iDsnId;
			int iAddedMGrpDbId;
			try
			{
			 
				sEntityType=((XmlElement)p_objPostedDoc.SelectSingleNode("//SelectedEntityFromLeftTree")).InnerText;
				sEntityId=((XmlElement)p_objPostedDoc.SelectSingleNode("//Delete/EntityIdForDeletion")).InnerText;
				
						switch (sEntityType)
						{
                            case "Datasource":
                            case "NoData":
                                iDsnId = Conversion.ConvertObjToInt(sEntityId, p_iClientId);
								break;
							case "Root Node Module Security Groups":
							case "Module Security Group":
                            case "GrpModuleAccessPermission":
                            
								
								string[] arrIds;
								string sDsnId="";
								try
								{
									arrIds =PublicFunctions.BreakDbId(sEntityId);
									sDsnId= arrIds[0];
                                    iDsnId = Conversion.ConvertObjToInt(sDsnId, p_iClientId);
			
								}
								catch(Exception p_objException)
								{
									throw p_objException;
								}
								finally
								{
									arrIds=null;
									sDsnId=null;
								}
						
								break;
							default:
								throw new RMAppException("Select a valid entity for adding a module group.");
								
							
						}
                  //srajindersin MITS 30263 dt 11/27/2012
				  if(iDsnId==p_objContextDsn)
				    {
                        iAddedMGrpDbId = Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//AddNewDatasourceEntity/AddedDBId").InnerText, p_iClientId);
					    string sMGrpIndentity="";
					   sMGrpIndentity=iDsnId.ToString()+","+iAddedMGrpDbId.ToString();
						AppendModuleGroups(ref p_objNodeToBeLoaded,
							iDsnId,
                            ref objLeftTreeTemplate, false, p_iClientId);
						objDataSource.SetAttribute("expanded","true");
					    p_objNodeToBeLoaded.SetAttribute("expanded","true");
					    ((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
					    ((XmlElement)p_objNodeToBeLoaded.SelectSingleNode("//entry[@entityType='Module Security Group' and @idDb='" + sMGrpIndentity + "']")).SetAttribute("highlight","true");
                        Helper.SetPostBackValues(ref p_objPostedDoc,
						  "",
						  "Module Security Group",
						  iDsnId.ToString()+","+iAddedMGrpDbId.ToString());
						}
				
			    }
			
			catch(Exception p_objException)
			{
                throw p_objException;
			}

		}
		/// <summary>
		/// This function handles the rename functionality for module group.
		/// </summary>
		/// <param name="p_objPostedDoc">Contains the parameters for adding new module group.</param>
		/// <param name="objLeftTreeTemplate">Security left tree structure.</param>
		/// <param name="p_objContextDsn">Datasource object</param>
		/// <param name="p_objNodeToBeLoaded">Node to which data will be appended.</param>
		/// <param name="objDataSource">Datasource element to be highlighted.</param>
		public static void RenameModuleGroupHandler(ref XmlDocument p_objPostedDoc,ref XmlDocument objLeftTreeTemplate 
				 ,int p_objContextDsn,ref XmlElement p_objNodeToBeLoaded,ref XmlElement objDataSource, int p_iClientId)
			 {
				 string sEntityType="";
				 string sEntityId="";
				 string sConnStr="";
				 int iDsnId;
			     string sDsnId;
				 int iMGrpDbId;
			     string[] arrIds;
			try
			{
			 
				sEntityType=((XmlElement)p_objPostedDoc.SelectSingleNode("//SelectedEntityFromLeftTree")).InnerText;
				sEntityId=((XmlElement)p_objPostedDoc.SelectSingleNode("//Delete/EntityIdForDeletion")).InnerText;
				arrIds =PublicFunctions.BreakDbId(sEntityId);
				sDsnId= arrIds[0];
                sConnStr = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(sDsnId, p_iClientId), p_iClientId);
                iDsnId = Conversion.ConvertObjToInt(sDsnId, p_iClientId);
                //srajindersin MITS 30263 dt 11/27/2012
                if(iDsnId==p_objContextDsn)
				{
                    iMGrpDbId = Conversion.ConvertObjToInt(p_objPostedDoc.SelectSingleNode("//AddNewDatasourceEntity/AddedDBId").InnerText, p_iClientId);
					string sMGrpIndentity="";
					sMGrpIndentity=iDsnId.ToString()+","+iMGrpDbId.ToString();
					AppendModuleGroups(ref p_objNodeToBeLoaded,
						iDsnId,
                        ref objLeftTreeTemplate, false, p_iClientId);
					objDataSource.SetAttribute("expanded","true");
					p_objNodeToBeLoaded.SetAttribute("expanded","true");
					((XmlElement)objLeftTreeTemplate.SelectSingleNode("//entry[@entityType='Root Node Riskmaster Datasources']")).SetAttribute("expanded","true");
					((XmlElement)p_objNodeToBeLoaded.SelectSingleNode("//entry[@entityType='Module Security Group' and @idDb='" + sMGrpIndentity + "']")).SetAttribute("highlight","true");
					Helper.SetPostBackValues(ref p_objPostedDoc,
						"",
						"Module Security Group",
						iDsnId.ToString()+","+iMGrpDbId.ToString());
					}
			}
			
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				arrIds=null;
			}

			 }
	}
	/// <summary>
	/// This class contains utility functions.
	/// </summary>
	public class Helper
	{
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public Helper()
		{
		}
		/// <summary>
		/// This function breaks the concatenated string into array.
		/// </summary>
		/// <param name="p_sConcatenatedStr">String with seperator</param>
		/// <param name="p_objObjectModule"></param>
        public static void CrackModuleGrpInfo(string p_sConcatenatedStr, ref ArrayList p_objObjectModule, int p_iClientId)
		{
			try
			{
                RMWinSecurity.SplitForStringPattern(p_sConcatenatedStr, "^~^~^", ref p_objObjectModule, p_iClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}
		/// <summary>
		/// This function populates the a xmlelement object with values passed as parameters.
		/// </summary>
		/// <param name="p_objElement">Xmlelement object to populate</param>
		/// <param name="text">Text value</param>
		/// <param name="title">Title of the node</param>
		/// <param name="url">Url of the node</param>
		/// <param name="icon">Icon of the node</param>
		/// <param name="type">Type of the node</param>
		/// <param name="entityType">EntityType of the node</param>
		/// <param name="idDb">Database id of the node</param>
		public static void CreateElement(ref XmlElement p_objElement,string text,
			string title,string url,string icon,
			string type,string entityType,string idDb)
		{
			p_objElement.SetAttribute("text",text);
			p_objElement.SetAttribute("title",title);
			p_objElement.SetAttribute("id","");
			p_objElement.SetAttribute("url",url);
			p_objElement.SetAttribute("icon",icon);
			p_objElement.SetAttribute("type",type);
			p_objElement.SetAttribute("entityType",entityType);
			p_objElement.SetAttribute("idDb",idDb);
			
		}
		/// <summary>
		/// This function sets the values for postback.
		/// </summary>
		/// <param name="p_objPostedDoc">Postback document</param>
		/// <param name="p_sUrl">Url to be shown in the Main Frame</param>
		/// <param name="p_sEntityType">Entity type on which focus to be set.</param>
		/// <param name="p_sIdDb">Database id of the node.</param>
		public static void SetPostBackValues(ref XmlDocument p_objPostedDoc,string p_sUrl,string p_sEntityType,string p_sIdDb)
		{
			try
			{
				p_objPostedDoc.SelectSingleNode("//PostBackResults/PageToBeShownInMainFrame").InnerText=p_sUrl;
				p_objPostedDoc.SelectSingleNode("//PostBackResults/entityType").InnerText=p_sEntityType;
				p_objPostedDoc.SelectSingleNode("//PostBackResults/idDb").InnerText=p_sIdDb;
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
		}

	}
	
}
