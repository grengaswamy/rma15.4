using System;
using System.Xml;
using System.Globalization;
using System.Resources;
using System.Collections;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.Application.ClaimLookups
{
	///************************************************************** 
	///* $File		: PiDiagHistLookUp.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 06-Oct-2005
	///* $Author	: Rahul Sharma
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to fetch the list of Diagnosis History and their related data for a ClaimId..
	/// </summary>
	public class PiDiagHistLookUp : IDisposable
	{
		#region "Constructor & Destructor"

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_iUserId">User Id</param>
		/// <param name="p_iUserGroupId">User Group Id</param>		
        public PiDiagHistLookUp(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId) //mbahl3 jira[RMACLOUD-159]
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_iGroupId = p_iUserGroupId ;
            m_iClientId = p_iClientId; //mbahl3 jira[RMACLOUD-159]
			this.Initialize();
		}

        public PiDiagHistLookUp(UserLogin p_oUserlogin, string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId) //vkumar258 ML Changes
        {
            m_UserLogin = p_oUserlogin;
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iUserId = p_iUserId;
            m_iGroupId = p_iUserGroupId;
            m_iClientId = p_iClientId;
            this.Initialize();
        }		
		#endregion

		#region Destructor
		/// <summary>
		/// Destructor
		/// </summary>
		~PiDiagHistLookUp()
		{
            Dispose();
		}
		#endregion

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            m_objXmlDocument = null;	
        }
		#endregion

		#region "Member Variables"
        private int m_iClientId = 0; //mbahl3 jira[RMACLOUD-159]

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;

		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;

		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";
		
		/// <summary>
		///Represents the login name of the Logged in User
		/// </summary>
		private string m_sLoginName = "";

		/// <summary>
		/// Represents the User Id.
		/// </summary>
		private int m_iUserId = 0;

		/// <summary>
		/// The Group id 
		/// </summary>
		private int m_iGroupId = 0;

		/// <summary>
		/// XML Document
		/// </summary>
		private XmlDocument m_objXmlDocument = null;
	
		#endregion

		#region Property declaration

		/// <summary>
		/// Gets & sets the user id
		/// </summary>
		public int UserId{get{return m_iUserId ;}set{m_iUserId = value;}} 


		/// <summary>
		/// Set the Login Name
		/// </summary>
		public string LoginName {get{return m_sLoginName;}set{m_sLoginName=value;}}


		/// <summary>
		/// Set the Group Id Property
		/// </summary>
		public int GroupId{get{return m_iGroupId;}set{m_iGroupId=value;}}

        UserLogin m_UserLogin = null;//vkumar258 ML Changes
        
		#endregion

		#region Public Functions
		#region "LoadListDiagHistory(int p_iClaimId,int p_iEmployeeId)"
		/// Name		: LoadListDiagHistory
		/// Author		: Mohit Yadav
		/// Date Created	: 8 September 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// fetches the data in XML format
		/// </summary>
		/// <param name="p_iClaimId">ClaimId</param>
		/// <param name="p_iEmployeeId">ClaimId</param>
		/// <returns>Returns XML containing required data to print.</returns>
		public XmlDocument LoadListDiagHistory(int p_iClaimId)
		{
			
			XmlElement objRootElement = null;
			XmlElement objXmlNode = null;
			XmlDocument objXmlDocument = null;			
			string sReturnXML = string.Empty; 
			string sSQL = string.Empty;
			string sMaxDate = string.Empty;
			Claim objClaim = null;

			try
			{
				objXmlDocument = new XmlDocument();
				if(p_iClaimId == 0)
					return objXmlDocument;
				objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim",false);
				objClaim.MoveTo(p_iClaimId);
				objRootElement = objXmlDocument.CreateElement("PiDiagHist");
				objRootElement.SetAttribute("username",this.m_sUserName);	
				objRootElement.SetAttribute("PiRowId",objClaim.PrimaryPiEmployee.PiRowId.ToString());
				objRootElement.SetAttribute ("claimnumber",objClaim.ClaimNumber);
                // npadhy Start MITS 18544 Pass the today's date from server to UI to default the Date field 
                objRootElement.SetAttribute("currentdate",Conversion.GetUIDate(System.DateTime.Today.ToShortDateString(),m_UserLogin.objUser.NlsCode.ToString(),m_iClientId));//vkumar258 ML Changes
                // npadhy End MITS 18544 Pass the today's date from server to UI to default the Date field
					
				foreach(PiXDiagHist objPixDiagHist in objClaim.PrimaryPiEmployee.PiXDiagHistList)
				{
					if (objPixDiagHist.DiagnosisCode!=0)
					{
						objXmlNode = objXmlDocument.CreateElement("data");
						objXmlNode.SetAttribute("DisMapId",objPixDiagHist.DisMapId.ToString());
						objXmlNode.SetAttribute("Reason",objPixDiagHist.Reason);
						objXmlNode.SetAttribute("ApprovedByUser",objPixDiagHist.ApprovedByUser);
						objXmlNode.SetAttribute("DiagnosisCode_cid",objPixDiagHist.DiagnosisCode.ToString());
						objXmlNode.SetAttribute("DiagnosisCode",GetCodeDescCodeid(Convert.ToInt32(objPixDiagHist.DiagnosisCode.ToString())));
						objXmlNode.SetAttribute("DisFactor",objPixDiagHist.DisFactor);
						objXmlNode.SetAttribute("PiXDiagrowId",objPixDiagHist.PiXDiagrowId.ToString());
						objXmlNode.SetAttribute("DisOptDuration",objPixDiagHist.DisOptDuration.ToString());
						objXmlNode.SetAttribute("DisGuidelineId",objPixDiagHist.DisGuidelineId.ToString());
						objXmlNode.SetAttribute("DateChanged",Conversion.GetUIDate(objPixDiagHist.DateChanged,m_UserLogin.objUser.NlsCode.ToString(),m_iClientId));//vkumar258 ML Changes
						objXmlNode.SetAttribute("DisMdaTopic",objPixDiagHist.DisMdaTopic);
						objXmlNode.SetAttribute("DisMaxDuration",objPixDiagHist.DisMaxDuration.ToString());
						objXmlNode.SetAttribute("DisMdaId",objPixDiagHist.DisMdaId.ToString());
						objXmlNode.SetAttribute("ChgdByUser",objPixDiagHist.ChgdByUser);
						objXmlNode.SetAttribute("DisMinDuration",objPixDiagHist.DisMinDuration.ToString());
                        objXmlNode.SetAttribute("IsICD10", objPixDiagHist.IsICD10.ToString());  //MITS 32423 : praveen ICD10 Changes
						objRootElement.AppendChild(objXmlNode);
					}
				}
				objXmlDocument.AppendChild(objRootElement);

				return objXmlDocument;

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PiDiagHistLookUp.LoadListDiagHistory.ErrorSummary", m_iClientId), p_objException); //mbahl3 jira[RMACLOUD-159]
			}
			finally
			{
				objXmlDocument = null;
				objRootElement = null;
				if (objClaim !=null) objClaim.Dispose();
			} 
		}
       
        /// Neha		: LoadListDiagHistoryfor per involved 
       public XmlDocument LoadListDiagHistoryForPI(int ipirowid)
        {

            XmlElement objRootElement = null;
            XmlElement objXmlNode = null;
            XmlDocument objXmlDocument = null;
            string sReturnXML = string.Empty;
            string sSQL = string.Empty;
            string sMaxDate = string.Empty;
            PiEmployee objEmployee = null;

            try
            {
                objXmlDocument = new XmlDocument();
                if (ipirowid == 0)
                    return objXmlDocument;
                objEmployee = (PiEmployee)m_objDataModelFactory.GetDataModelObject("PiEmployee", false);
                objEmployee.MoveTo(ipirowid);
                objRootElement = objXmlDocument.CreateElement("PiDiagHist");
                objRootElement.SetAttribute("username", this.m_sUserName);
                objRootElement.SetAttribute("PiRowId", ipirowid.ToString());
                //objRootElement.SetAttribute("claimnumber", objEmployee);
                // npadhy Start MITS 18544 Pass the today's date from server to UI to default the Date field 
                objRootElement.SetAttribute("currentdate", System.DateTime.Today.ToShortDateString());
                // npadhy End MITS 18544 Pass the today's date from server to UI to default the Date field

                foreach (PiXDiagHist objPixDiagHist in objEmployee.PiXDiagHistList)
                {
                    if (objPixDiagHist.DiagnosisCode != 0)
                    {
                        objXmlNode = objXmlDocument.CreateElement("data");
                        objXmlNode.SetAttribute("DisMapId", objPixDiagHist.DisMapId.ToString());
                        objXmlNode.SetAttribute("Reason", objPixDiagHist.Reason);
                        objXmlNode.SetAttribute("ApprovedByUser", objPixDiagHist.ApprovedByUser);
                        objXmlNode.SetAttribute("DiagnosisCode_cid", objPixDiagHist.DiagnosisCode.ToString());
                        objXmlNode.SetAttribute("DiagnosisCode", GetCodeDescCodeid(Convert.ToInt32(objPixDiagHist.DiagnosisCode.ToString())));
                        objXmlNode.SetAttribute("DisFactor", objPixDiagHist.DisFactor);
                        objXmlNode.SetAttribute("PiXDiagrowId", objPixDiagHist.PiXDiagrowId.ToString());
                        objXmlNode.SetAttribute("DisOptDuration", objPixDiagHist.DisOptDuration.ToString());
                        objXmlNode.SetAttribute("DisGuidelineId", objPixDiagHist.DisGuidelineId.ToString());
                        objXmlNode.SetAttribute("DateChanged", Conversion.GetUIDate(objPixDiagHist.DateChanged, m_UserLogin.objUser.NlsCode.ToString(), m_iClientId));//vkumar258 ML Changes
                        objXmlNode.SetAttribute("DisMdaTopic", objPixDiagHist.DisMdaTopic);
                        objXmlNode.SetAttribute("DisMaxDuration", objPixDiagHist.DisMaxDuration.ToString());
                        objXmlNode.SetAttribute("DisMdaId", objPixDiagHist.DisMdaId.ToString());
                        objXmlNode.SetAttribute("ChgdByUser", objPixDiagHist.ChgdByUser);
                        objXmlNode.SetAttribute("DisMinDuration", objPixDiagHist.DisMinDuration.ToString());
                        objXmlNode.SetAttribute("IsICD10", objPixDiagHist.IsICD10.ToString());  //MITS 32423 : praveen ICD10 Changes
                        objRootElement.AppendChild(objXmlNode);
                    }
                }
                objXmlDocument.AppendChild(objRootElement);

                return objXmlDocument;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PiDiagHistLookUp.LoadListDiagHistory.ErrorSummary", m_iClientId), p_objException); //mbahl3 jira[RMACLOUD-159]
            }
            finally
            {
                objXmlDocument = null;
                objRootElement = null;
                if (objEmployee != null) objEmployee.Dispose();
            }
        }
		#endregion
	
		#endregion

		#region "Private Functions"

		#region "Initialize"
		/// Name		: Initialize
		/// Author		: Rahul Sharma
		/// Date Created	: 6 October 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string;
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory(m_sDsnName,m_sUserName,m_sPassword,m_iClientId); //mbahl3 jira[RMACLOUD-159]
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_objXmlDocument = new XmlDocument(); 
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("MMIHistorySummary.Initialize.ErrorInit",m_iClientId) , p_objEx );		//mbahl3 jira[RMACLOUD-159]		
			}			
		}	
		#endregion

		#region "GetCodeDescCodeid(int p_iCode)"
		/// Name		: GetEntity
		/// Author		: Rahul SHarma
		/// Date Created	: 6 October 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Fetches the Code Description and Short Code for the CodeId passed and return a string
		/// after concanate the strings.
		/// </summary>
		/// <param name="p_iCode">CodeId</param>
		/// <returns>String containing short Code and CodeDescription.</returns>
		private string GetCodeDescCodeid(int p_iCode)
		{
			string sShortCode="";
			string sCodeDesc="";
			string sName="";
			LocalCache objLocalCache=new LocalCache(m_sConnectionString,m_iClientId);
			try
			{
				sShortCode=objLocalCache.GetShortCode(p_iCode);
				if(sShortCode=="\" \"")
					sShortCode="";
				sCodeDesc=objLocalCache.GetCodeDesc(p_iCode);
				sName=sShortCode+"  "+sCodeDesc;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PiDiagHistLookUp.GetCodeDescCodeid.ErrorGet", m_iClientId), p_objException); //mbahl3 jira[RMACLOUD-159]
			}
			finally
			{
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
			}

			return sName.Trim();
		}
		#endregion

		#endregion

	}
}
