﻿
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.IO;

namespace Riskmaster.Application.ClaimLookups
{
	///************************************************************** 
	///* $File		: MDALookUp.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 04-Oct-2005
	///* $Author	: Rahul Sharma
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to fetch the list MDA Topics and their related data..
	/// </summary>
	public class MDALookUp
	{
		#region "Constructor"
		/// <summary>
		/// Default constructor.
		/// </summary>
        private int m_iClientId = 0;

        public MDALookUp(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
		#endregion

		#region "Public Functions"
		public XmlDocument GetMDATopicDetails(string p_sConnectionString, int p_iJobClassId, string p_sMDAId)
		{
			XmlDocument objOut;
			string sOutputXML="";
			DbReader objReader=null;

			LocalCache objLocalCache=new LocalCache(p_sConnectionString,m_iClientId);
			try
			{
				string sSQL="SELECT B.MedicalCode, D.TopicId, D.Topic, E.Factors, E.GuidelineID, G.JobClass, F.Minimum, F.Optimum, F.Maximum "+
								" FROM tblMDA_MedicalCodeType A, tblMDA_MedicalCode B, tblMDA_Topic_MedicalCode C, "+
								" tblMDA_Topic D, tblMDA_Guideline E, tblMDA_Durations F, tblMDA_JobClass G "+
								" WHERE A.MedicalCodeTypeId=B.MedicalCodeTypeId "+
								" AND B.MedicalCode=C.MedicalCode "+
								" AND C.TopicID=D.TopicID "+ //C.TopicId = E.TopicId
								" AND D.TopicID=E.TopicID "+
								" AND E.GuidelineID=F.GuidelineID "+
								" AND F.JobClassID=G.JobClassID "+
								" AND A.MedicalCodeType='ICD9' "+
								" AND B.MedicalCode='"+p_sMDAId+ "'"+ //C.MedicalCode = '"+p_sMDAId+ "'"+
								" AND F.JobClassID=" + p_iJobClassId.ToString();

				objReader = DbFactory.GetDbReader(GetMDAConnString(p_sConnectionString),sSQL);
				if(objReader != null)
				{
					sOutputXML="<MDADataGrid>";
					while(objReader.Read())
					{
						sOutputXML += "<MDAData>";
						sOutputXML += "<ICD9>"+objReader.GetString("MedicalCode")+"</ICD9>";
						sOutputXML += "<GuidelineID>" + objReader.GetValue("GuidelineID").ToString()+"</GuidelineID>";
						sOutputXML += "<TopicId>" + objReader.GetValue("TopicId").ToString()+"</TopicId>";
						sOutputXML += "<Topic>"+objReader.GetString("Topic")+"</Topic>";
						sOutputXML += "<Factors>"+objReader.GetString("Factors")+"</Factors>";
						sOutputXML += "<OtherCodes>"+ GetOtherCodes(p_sConnectionString,objReader.GetValue("TopicId").ToString(),p_sMDAId) +"</OtherCodes>";
						sOutputXML += "<Minimum>"+Conversion.ConvertObjToInt(objReader.GetValue("Minimum"), m_iClientId)+"</Minimum>";
						sOutputXML += "<Optimum>"+Conversion.ConvertObjToInt(objReader.GetValue("Optimum"), m_iClientId)+"</Optimum>";
						sOutputXML += "<Maximum>"+Conversion.ConvertObjToInt(objReader.GetValue("Maximum"), m_iClientId)+"</Maximum>";
						sOutputXML += "</MDAData>";
					}
					objReader.Close();
					sOutputXML += "</MDADataGrid>";
				}
				else
				{
                    throw new RMAppException(Globalization.GetString("MDALookUp.GetMDATopicDetails.DatabaseError", m_iClientId));
				}
				objOut=new XmlDocument();
				objOut.LoadXml(sOutputXML);
				return objOut;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("MDALookUp.GetMDATopicDetails.DatabaseError", m_iClientId), p_objEx);
			}
			finally
			{
				if(objReader!=null)
				{
                    objReader.Dispose();			
				}
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
				//objReader.Dispose();
			}
		}

		public void GetMedicalDisabilityGuidelineFile(string p_sMDAHelpFilePath, int p_iTopicId, out MemoryStream p_objGuidelineFile)
		{
			string sFileName=p_sMDAHelpFilePath + p_iTopicId.ToString() + ".html";
			p_objGuidelineFile = new MemoryStream(this.FileAsByteArray(sFileName));
		}

		#endregion

		#region Private Functions
		private string GetMDAConnString(string p_sConnectionString)
		{
			string sSQLParamValues="";
			string sMDADSN="";
			string sMDALoginID="";
			string sMDAPwd="";
			string sMDAConnectString="";
			try
			{
				sSQLParamValues="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='WC_MDA_DSN_NAME'";
                using (DbReader objReader = DbFactory.GetDbReader(p_sConnectionString, sSQLParamValues))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sMDADSN = objReader.GetString("STR_PARM_VALUE");
                        }
                    }
                }
				sSQLParamValues="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='WC_MDA_USER_ID'";
                using (DbReader objReader = DbFactory.GetDbReader(p_sConnectionString, sSQLParamValues))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sMDALoginID = objReader.GetString("STR_PARM_VALUE");
                        }
                    }
                }
				sSQLParamValues="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='WC_MDA_PSWD'";
                using (DbReader objReader = DbFactory.GetDbReader(p_sConnectionString, sSQLParamValues))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sMDAPwd = objReader.GetString("STR_PARM_VALUE");
                        }
                    }
                }
                sMDAConnectString = RMConfigurationManager.GetConnectionString("MDADataSource",m_iClientId);//sonali
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("MDALookUp.GetMDATopicDetails.MDAConnection", m_iClientId), p_objEx);
			}
			finally
			{
			}
			return sMDAConnectString;
		}

		private string GetOtherCodes(string p_sConnectionString, string p_sTopicId, string p_sMDAId)
		{
			//DbReader objReader=null;
			string sOtherCodes=string.Empty;
			
			string sSQL="SELECT C.MedicalCode" +
				" FROM tblMDA_MedicalCodeType A, tblMDA_MedicalCode B, tblMDA_Topic_MedicalCode C"+
				" WHERE A.MedicalCodeTypeId=B.MedicalCodeTypeId "+
				" AND B.MedicalCode=C.MedicalCode "+
				" AND A.MedicalCodeType='ICD9' "+
				" AND C.TopicID=" + p_sTopicId +
				" AND C.MedicalCode<>'" + p_sMDAId + "'";

            using (DbReader objReader = DbFactory.GetDbReader(GetMDAConnString(p_sConnectionString), sSQL))
            {

                //todo: need only ICD9 codes. Modify the query
                if (objReader != null)
                {
                    while (objReader.Read())
                        sOtherCodes = sOtherCodes + Conversion.ConvertObjToStr(objReader.GetValue("MedicalCode")) + ",";

                    objReader.Close();
                    sOtherCodes = sOtherCodes.Trim(new char[] { ',' });
                }
            }
			return sOtherCodes;
		}
		
		/// Name			: FileAsByteArray
		/// Author			: Neelima Dabral
		/// Date Created	: 17-Nov-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method would convert the contents of the file to the binary stream. 
		/// </summary>
		/// <param name="p_sFileName">
		///		File Name
		/// </param>
		/// <returns>Binary stream containing file contents</returns>
		private byte[] FileAsByteArray(string p_sFileName)
		{
			byte[]			arrRet  = null;
			FileStream		objFStream=null;
			BinaryReader	objBReader=null;
			try
			{
				objFStream = new FileStream(p_sFileName,FileMode.Open);
				objBReader = new BinaryReader(objFStream);
				arrRet = objBReader.ReadBytes((int)objFStream.Length);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException
                    (Globalization.GetString("MDAGuideline.FileAsByteArray.Error", m_iClientId), p_objException);						
			}
			finally
			{
				if( objFStream != null) 
				{
					objFStream.Close();
					objFStream = null;
				}
				if( objBReader != null) 
				{
					objBReader.Close();
					objBReader = null;					
				}
			}			
			return arrRet;
		}	
		#endregion
	}
}
