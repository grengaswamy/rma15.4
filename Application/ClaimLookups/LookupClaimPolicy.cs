﻿

using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.Settings;
//using Riskmaster.Security;
using System.Text;

namespace Riskmaster.Application.ClaimLookups
{
	///************************************************************** 
	///* $File		: LookupClaimPolicy.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 25-Aug-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to fetch the list of policies for a Claim.
	/// </summary>
	public class LookupClaimPolicy
	{
		#region "Constructor"
		/// <summary>
		/// Default constructor.
		/// </summary>
		public LookupClaimPolicy()
		{
		}
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
		public LookupClaimPolicy(string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)//sonali-cloud
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;

        /// <summary>
        /// Private variable for Client Id
        /// </summary>
        private int m_iClientId = 0;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		#endregion

		#region "Constants"
		private const string  SC_POLICY_REVOKED = "R";
		private const string SC_POLICY_IN_EFFECT = "I";
		private const string SC_POLICY_EXPIRED = "E";
		private const string SC_POLICY_CANCELED = "C";
		#endregion

		#region "Public Functions"

		/// Name		: GetLookupClaimPolicyList
		/// Author		: Nikhil Kumar Garg
		/// Date Created: 25-Aug-2005
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Gets the List of Policies for the Claim.
		/// </summary>
		/// <param name="p_iClaimTypeCode">Claim Type Code</param>
		/// <param name="p_sEventDate">Event Date</param>
		/// <param name="p_sClaimDate">Claim Date</param>
		/// <param name="p_iDeptEID">Dept EId</param>
        /// <param name="p_iLOB">LOB</param>
        /// <param name="p_iJurisdictionId">Jursidiction ID</param>
		/// <returns>XML Document containing the Policies list</returns>      
		public XmlDocument GetLookupClaimPolicyList(int p_iClaimTypeCode, string p_sEventDate, 
			string p_sClaimDate,int p_iDeptEID, int p_iLOB, int p_iJurisdictionId,int p_iPageNumber,string p_sCurrencyType,int p_iEventStateId) //changed by rkaur7- MIS 16668 - 06/03/2009 
		{
			DbReader objReaderPolicy=null;				//reader object for searching
			DbReader objReaderNextPolicy=null;
            DbReader objTransactionID = null;
			XmlDocument objPoliciesXml=null;			//Xml to return
			XmlElement objPolicies=null;
			XmlElement objElem=null;
			string sSQLSelect=string.Empty;
			string sSQLFrom=string.Empty;
			string sSQLWhere=string.Empty;
			string sSQLOrder=string.Empty;
			string sSQL=string.Empty;
            string sSQLTemp = string.Empty;
            string sTranType=string.Empty;
            string sPolicyStatus=string.Empty;
			LocalCache objCache = null;
			bool bNoNonPrimary=false;
            int iTranId = 0;
			int iLastPolicyID=0;
			int iNextPolicyID=0;
			int iPolicyId=0;
			double dPolLimit=0;
			double dTotalPayments=0;
			double dApp=0;

			//int iPOLICY_STATUS=0;
			int iCN_POLICY_REVOKED=0;
			int iCN_POLICY_IN_EFFECT=0;
			int iCN_POLICY_EXPIRED=0;
			int iCN_POLICY_CANCELED=0;
			int iCoverageTypeCode=0;
			string sGetOrgForDept=string.Empty;
            //Start-Mridul Bansal. 01/11/10. MITS#18229.
            int iPolicyLOB = 0;
            ColLobSettings objLobSettings = null;
            bool bUseEPS = false;
            //End-Mridul Bansal. 01/11/10. MITS#18229.
            //Gagan Safeway Retrofit Policy Jursidiction 
            string sLobName = string.Empty;
            string sComments = string.Empty;    //csingh7 for MITS 20092
            //Start (06/29/10)-Sumit 
            bool bUsePolicyLOBFilter = false;
            //End-Sumit
//ABAHl3: MITS 25163- Policy Interface Implementation
            string sSQL1 = string.Empty;
            int iRecordCount = 0;
            const int iPageSize = 10;
            bool bUsemultiCurrency = false;
            int iUseEventState = 0;
			try
			{
                bool blnParsed = false;
                bool blnPabbloConfigured = Conversion.CastToType<bool>(RMConfigurationManager.GetAppSetting("IsPabbloReady"), out blnParsed);
				m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                bool bUseAdvancedClaim = m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1 ? true : false;
                //Gagan Safeway Retrofit Policy Jursidiction : - LOB name captured as will be required to determine whether jurisdiction to used as filter criterion or not
                sLobName = objCache.GetShortCode(p_iLOB).ToString();
                //Start-Mridul Bansal. 01/11/10. MITS#18229. Transform Claim LOB to appropriate Policy LOB.
                //bool bUseEPS = m_objDataModelFactory.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1 ? true : false;
                objLobSettings = new ColLobSettings(m_objDataModelFactory.Context.DbConn.ConnectionString, m_iClientId);
                bUseEPS = objLobSettings[p_iLOB].UseEnhPolFlag == -1 ? true : false;
                //End-Mridul Bansal. 01/11/10. MITS#18229. Transform Claim LOB to appropriate Policy LOB.

                //Start (06/29/10)-Sumit  - Filter Policy based on LOB
                bUsePolicyLOBFilter = m_objDataModelFactory.Context.InternalSettings.SysSettings.FilterPolicyBasedOnLOB;
                //End - Sumit
                //Start:added by nitin goel, to fix the issue with base , auto department should only be used when department is not manually selected.
                if (p_iDeptEID == 0)
                {
                //Added by Amitosh for MITS 27487
                if (m_objDataModelFactory.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
                {
                    p_iDeptEID = m_objDataModelFactory.Context.InternalSettings.SysSettings.AutoFillDpteid;
                }
                //end Amitosh
                }

                bUsemultiCurrency = Conversion.ConvertStrToBool(m_objDataModelFactory.Context.InternalSettings.SysSettings.UseMultiCurrency.ToString());
                
				if (blnParsed && blnPabbloConfigured)
				{
					Pabblo objPabblo=new Pabblo(m_sConnectionString,m_iClientId);//sonali

					objPabblo.DSN=m_sConnectionString;
					objPabblo.Pwd=m_sPassword;
					objPabblo.UserName=m_sUserName;
					objPabblo.Database=m_sDsnName;
				
					string sMemNum = objPabblo.GetPabbloMemNum(p_iDeptEID).ToString();
					if (sMemNum.Trim().Equals("0") || sMemNum.Trim().Equals(""))
					{
						throw new RMAppException(Globalization.GetString("LookupClaimPolicy.GetLookupClaimPolicyList.MemberNotFoundError",m_iClientId));//sonali
					}
					string sCvgCode=objCache.GetRelatedShortCode(p_iClaimTypeCode).ToString();
					if (sCvgCode.Length == 0 ||  p_sEventDate.Length ==0)
					{
                        throw new RMAppException(Globalization.GetString("LookupClaimPolicy.GetLookupClaimPolicyList.RequiredFieldError", m_iClientId));
					}
					objPabblo.GetPolicyData(sMemNum,p_sEventDate,p_iLOB.ToString(),sCvgCode,p_iClaimTypeCode);
					if (objPabblo.curPolicy.Policy_Num.Length <=0)
					{
                        throw new RMAppException(Globalization.GetString("LookupClaimPolicy.GetLookupClaimPolicyList.PolicyNotFoundError", m_iClientId));
					}
					objPabblo.InsertPolicyData(p_iDeptEID);
					//build select for looking up policies for this claim
                    sSQLSelect = "SELECT POLICY.POLICY_ID,POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,POLICY.EFFECTIVE_DATE,POLICY.EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";

                    sSQLFrom = " FROM POLICY INNER JOIN " +
                            "POLICY_X_CVG_TYPE ON POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID " +
                            "INNER JOIN POLICY_X_INSURED ON POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID " +
                            "INNER JOIN ENTITY ON POLICY_X_INSURED.INSURED_EID = ENTITY.ENTITY_ID ";

					sSQLWhere = " WHERE (POLICY.POLICY_ID = "+objPabblo.curPolicy.Policy_ID+")";
					sSQLOrder = " ORDER BY POLICY_NUMBER ";

					sSQL = sSQLSelect + sSQLFrom + sSQLWhere;

					objReaderPolicy = DbFactory.GetDbReader(m_sConnectionString, String.Format("{0} AND PRIMARY_POLICY_FLG <> 0{1}", sSQL, sSQLOrder));
					objPoliciesXml=new XmlDocument();
					objPolicies=objPoliciesXml.CreateElement("policies");
					objPoliciesXml.AppendChild(objPolicies);
					if (objReaderPolicy.Read())
					{
					
						objElem=objPoliciesXml.CreateElement("policy");
						objPolicies.AppendChild(objElem);

						iPolicyId=Conversion.ConvertObjToInt(objReaderPolicy.GetValue("POLICY_ID"), m_iClientId);

						objElem.SetAttribute("policyid",iPolicyId.ToString());
						objElem.SetAttribute("policynumber",Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_NUMBER")));

						iLastPolicyID=iPolicyId;
                        iNextPolicyID = Conversion.ConvertObjToInt(objReaderPolicy.GetValue("NEXT_POLICY_ID"), m_iClientId);

						objElem.SetAttribute("policyname",Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_NAME")));

						//***********************************
						// Insurer Last Name field added to display...
						//***********************************
						objElem.SetAttribute("insurer",Conversion.ConvertObjToStr(objReaderPolicy.GetValue("LAST_NAME")));

						objElem.SetAttribute("effectivedate",Conversion.ToDate(Conversion.ConvertObjToStr(objReaderPolicy.GetValue("EFFECTIVE_DATE"))).ToShortDateString()); 
						objElem.SetAttribute("expirationdate",Conversion.ToDate(Conversion.ConvertObjToStr(objReaderPolicy.GetValue("EXPIRATION_DATE"))).ToShortDateString()); 
						objElem.SetAttribute("occurencelimit",Conversion.ConvertObjToStr(objReaderPolicy.GetValue("OCCURRENCE_LIMIT")));
                        objElem.SetAttribute("comments", "false");

                        dPolLimit = Conversion.ConvertObjToDouble(objReaderPolicy.GetValue("POLICY_LIMIT"), m_iClientId);
                        dTotalPayments = Conversion.ConvertObjToDouble(objReaderPolicy.GetValue("TOTAL_PAYMENTS"), m_iClientId);
                          
						if (dPolLimit != 0)
							dApp = (dTotalPayments / dPolLimit) * 100;
						
						objElem.SetAttribute("app_proc", "%" + dApp.ToString("00"));

						objElem=null;
					
					}
				
					objReaderPolicy.Close();
					objReaderPolicy=null;
				}
				else
				{
					// preload some code/table ids
					//iPOLICY_STATUS = objCache.GetTableId("POLICY_STATUS");
					iCN_POLICY_REVOKED = objCache.GetCodeId(SC_POLICY_REVOKED,"POLICY_STATUS");
					iCN_POLICY_IN_EFFECT = objCache.GetCodeId(SC_POLICY_IN_EFFECT, "POLICY_STATUS");
					iCN_POLICY_EXPIRED = objCache.GetCodeId(SC_POLICY_EXPIRED, "POLICY_STATUS");
					iCN_POLICY_CANCELED = objCache.GetCodeId(SC_POLICY_CANCELED, "POLICY_STATUS");
					iCoverageTypeCode = objCache.GetRelatedCodeId(p_iClaimTypeCode);
                    if (p_iDeptEID != 0)
                        sGetOrgForDept = GetOrgListForDept(p_iDeptEID);
                    else
                        sGetOrgForDept = "0";

                    
                    if (bUseEPS)
                    {
                        //Start-Mridul Bansal. 01/11/10. MITS#18229. Transform Claim LOB to appropriate Policy LOB.
                        //Start (06/29/10)-Sumit  - Filter Policy based on LOB
                        if (bUsePolicyLOBFilter)
                        {
                            switch (objCache.GetShortCode(p_iLOB))
                            {
                                case "GC":
                                    iPolicyLOB = objCache.GetCodeId("GL", "POLICY_LOB");
                                    break;
                                case "VA":
                                    iPolicyLOB = objCache.GetCodeId("AL", "POLICY_LOB");
                                    break;
                                case "PC":
                                    iPolicyLOB = objCache.GetCodeId("PC", "POLICY_LOB");
                                    break;
                                case "WC":
                                    iPolicyLOB = objCache.GetCodeId("WC", "POLICY_LOB");
                                    break;
                                default:
                                    iPolicyLOB = 0;
                                    break;
                            }
                        }
                        //End - Sumit
                        //End-Mridul Bansal. 01/11/10. MITS#18229. Transform Claim LOB to appropriate Policy LOB.
//ABAHl3: MITS 25163- Policy Interface Implementation
                        //sSQLSelect = "SELECT POLICY_ENH.POLICY_ID,POLICY_X_TERM_ENH.POLICY_NUMBER,POLICY_X_CVG_ENH.NEXT_POLICY_ID,POLICY_ENH.POLICY_NAME,POLICY_X_TERM_ENH.EFFECTIVE_DATE,POLICY_X_TERM_ENH.EXPIRATION_DATE,POLICY_X_CVG_ENH.OCCURRENCE_LIMIT"; csingh7 for MITS 20092
                        sSQLSelect = "SELECT * FROM ( SELECT COUNT(POLICY_ENH.POLICY_INDICATOR ) OVER(PARTITION BY POLICY_ENH.POLICY_INDICATOR ) AS COUNT," +
                         "ROW_NUMBER() OVER(ORDER BY POLICY_NUMBER) AS ROW_NUMBER,";
                        sSQLSelect = sSQLSelect+ " POLICY_ENH.POLICY_ID,POLICY_ENH.COMMENTS,POLICY_X_TERM_ENH.POLICY_NUMBER,POLICY_X_CVG_ENH.NEXT_POLICY_ID,POLICY_ENH.POLICY_NAME,POLICY_X_TERM_ENH.EFFECTIVE_DATE,POLICY_X_TERM_ENH.EXPIRATION_DATE,POLICY_X_CVG_ENH.OCCURRENCE_LIMIT";
                        sSQLSelect = sSQLSelect + ",POLICY_X_CVG_ENH.POLICY_LIMIT,POLICY_X_CVG_ENH.TOTAL_PAYMENTS,ENTITY.LAST_NAME";
                        sSQLSelect = sSQLSelect + ",POLICY_X_TERM_ENH.CANCEL_DATE,POLICY_X_TERM_ENH.REINSTATEMENT_DATE,POLICY_X_TERM_ENH.SEQUENCE_ALPHA,POLICY_X_TERM_ENH.TERM_NUMBER,POLICY_ENH.TRIGGER_CLAIM_FLAG";
                        
                        sSQLFrom = " FROM POLICY_ENH, POLICY_X_TERM_ENH, POLICY_X_CVG_ENH, POLICY_X_INSRD_ENH, ENTITY";
                        sSQLWhere = " WHERE (POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID";
                        sSQLWhere = sSQLWhere + " AND POLICY_ENH.POLICY_ID = POLICY_X_CVG_ENH.POLICY_ID AND POLICY_ENH.INSURER_EID = ENTITY.ENTITY_ID";
                        sSQLWhere = sSQLWhere + " AND POLICY_ENH.POLICY_ID = POLICY_X_TERM_ENH.POLICY_ID AND POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID)";

                        sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_ENH.COVERAGE_TYPE_CODE IN (" + iCoverageTypeCode + ", " + p_iClaimTypeCode + ")";
                        sSQLWhere = sSQLWhere + " AND POLICY_X_INSRD_ENH.INSURED_EID IN (" + sGetOrgForDept + "))";
                        sSQLWhere = sSQLWhere + " AND ((POLICY_X_TERM_ENH.EFFECTIVE_DATE <= '" + p_sEventDate + "'";
                        sSQLWhere = sSQLWhere + " AND POLICY_X_TERM_ENH.EXPIRATION_DATE >= '" + p_sEventDate + "'";
                        sSQLWhere = sSQLWhere + " AND POLICY_ENH.TRIGGER_CLAIM_FLAG = 0)";
                        sSQLWhere = sSQLWhere + " OR (POLICY_X_TERM_ENH.EFFECTIVE_DATE <= '" + p_sClaimDate + "'";
                        sSQLWhere = sSQLWhere + " AND POLICY_X_TERM_ENH.EXPIRATION_DATE >= '" + p_sClaimDate + "'";
                        sSQLWhere = sSQLWhere + " AND POLICY_ENH.TRIGGER_CLAIM_FLAG <> 0))";
                        //Start-Mridul Bansal. 01/11/10. MITS#18229. Restrict policy on LOB basis for enhanced policy.
                        //Start (06/29/10)-Sumit  - Filter Policy based on LOB
                        if (bUsePolicyLOBFilter)
                        {
                            sSQLWhere = sSQLWhere + " AND POLICY_ENH.POLICY_TYPE = '" + iPolicyLOB.ToString() + "'";
                        }
                        //End - Sumit
                        //End-Mridul Bansal. 01/11/10. MITS#18229. Restrict policy on LOB basis for enhanced policy.
                        sSQLWhere = sSQLWhere + " AND (POLICY_ENH.POLICY_INDICATOR = " + m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "POLICY_INDICATOR") + ")";

                        if (bUsemultiCurrency)
                        {
                            sSQLWhere = sSQLWhere + " AND  POLICY_ENH.CURRENCY_CODE = " + p_sCurrencyType;

                        }

                    }
                    else
                    {
                        //build select for looking up policies for this claim
                        // npadhy MITS 18606 Commenting the changes for REM Merge and changing the same code to reflect correct conditions
                        bool bJuris = false;
                        
                        //sSQLSelect = "SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME"; //csingh7 for MITS 20092
                        sSQLSelect ="SELECT * FROM ( SELECT COUNT(POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE) OVER(PARTITION BY POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE) AS COUNT,"+
                            "ROW_NUMBER() OVER(ORDER BY POLICY_NUMBER) AS ROW_NUMBER,";
                        sSQLSelect = sSQLSelect + " POLICY.POLICY_ID,POLICY_NUMBER,POLICY.COMMENTS,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,POLICY.EFFECTIVE_DATE,POLICY.EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";

                        //if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                        //{
                        //    sSQLSelect = sSQLSelect + " ,ISNULL('RMX',POLICY_X_WEB.POLICY_SYSTEM_NAME) POLICY_SYSTEM_NAME";
                            
                        //}
                        //else if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                        //{
                        //    sSQLSelect = sSQLSelect + " , NVL(POLICY_X_WEB.POLICY_SYSTEM_NAME,'RMX') POLICY_SYSTEM_NAME";
                        //}
                        //if(p_iJurisdictionId!=0)
                        //sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY ,POLICY_X_STATE"; //Added by rkaur7- MITS 16668  - Added new table POLICY_X_STATE-  06/03/2009

                        //else
                        //sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY "; //Added by rkaur7- MITS 16668  - Added new table POLICY_X_STATE-  06/03/2009

                        // 
                        sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY ";
                        //if (p_iJurisdictionId != 0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 &&
                            //(m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 0 ||
                                //(m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 1 && sLobName == "WC")))
                             // MITS 27322: Ishan : Check Jurisdiction states for all claims. 
                            if ((p_iEventStateId != 0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 &&
                           (m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 0 )) ||  (p_iJurisdictionId!=0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 &&
                               (m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 1)))
                        {
                            bJuris = true;
                            iUseEventState =m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval;
                            sSQLFrom += " , POLICY_X_STATE";
                        }

                        if (!bUseAdvancedClaim)
                        {
                            sSQLWhere = " WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID";
                            sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)";

                            sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
                            sSQLWhere = sSQLWhere + "      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))";

                            sSQLWhere = sSQLWhere + " AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))";
                            sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ";
                            sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + p_sClaimDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)";
                            sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED;
                            sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + p_sEventDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG = 0))";
                            sSQLWhere = sSQLWhere + " AND ((POLICY.EFFECTIVE_DATE <= '" + p_sEventDate + "'";
                            sSQLWhere = sSQLWhere + "       AND POLICY.EXPIRATION_DATE >= '" + p_sEventDate + "'";
                            sSQLWhere = sSQLWhere + "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)";
                            sSQLWhere = sSQLWhere + "      OR (POLICY.EFFECTIVE_DATE <= '" + p_sClaimDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.EXPIRATION_DATE >= '" + p_sClaimDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))";
                            //Added by rkaur7- MITS 16668 - Added jurisdiction as filter criterion - 06/03/2009
                            //rkaur7- It works based on utility setting of Show Jursidiction and Event State radio button
                            //if (p_iJurisdictionId!=0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval==0)
                            //if (p_iJurisdictionId != 0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.ShowJurisdiction != 0)
                            if (bJuris)
                            {
                                sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ";
                                if(iUseEventState ==0)
                                    sSQLWhere = sSQLWhere + " AND POLICY_X_STATE.STATE_ID =" + p_iEventStateId + " ";
                                else
                                sSQLWhere = sSQLWhere + " AND POLICY_X_STATE.STATE_ID =" + p_iJurisdictionId + " ";
                            }

                            if (bUsemultiCurrency)
                            {
                                sSQLWhere = sSQLWhere + " AND  POLICY.CURRENCY_CODE = " + p_sCurrencyType;

                            }
                        }
                        else
                        {
                      sSQLFrom = sSQLFrom + " ,POLICY_X_UNIT";
                            sSQLWhere = " WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID";
                         sSQLWhere = sSQLWhere + " AND POLICY_X_UNIT.POLICY_ID = POLICY.POLICY_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID";
                         sSQLWhere = sSQLWhere + " AND  POLICY.INSURER_EID = ENTITY.ENTITY_ID)";
                         sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
                         sSQLWhere = sSQLWhere + "      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))";
                       //sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)";
                            sSQLWhere = sSQLWhere + " AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))";
                            sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ";
                            sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + p_sClaimDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)";
                            sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED;
                            sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + p_sEventDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG = 0))";
                            sSQLWhere = sSQLWhere + " AND ((POLICY.EFFECTIVE_DATE <= '" + p_sEventDate + "'";
                            sSQLWhere = sSQLWhere + "       AND POLICY.EXPIRATION_DATE >= '" + p_sEventDate + "'";
                            sSQLWhere = sSQLWhere + "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)";
                            sSQLWhere = sSQLWhere + "      OR (POLICY.EFFECTIVE_DATE <= '" + p_sClaimDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.EXPIRATION_DATE >= '" + p_sClaimDate + "'";
                            sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))";
                             if (bJuris)
                            {
                                sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ";
                                sSQLWhere = sSQLWhere + " AND POLICY_X_STATE.STATE_ID =" + p_iJurisdictionId + " ";
                            }

                            

                        }
                        //end rkaur7
                    }
                    sSQLOrder = " ORDER BY POLICY_NUMBER ";

                    sSQL = sSQLSelect + sSQLFrom + sSQLWhere;
                    //rsushilaggar MITS 28648/28647 Date 06/13/2012
                    //sSQL1 = " ) TEMP WHERE TEMP.ROW_NUMBER >=" + ((iPageSize * (p_iPageNumber - 1)) + 1) + " AND TEMP.ROW_NUMBER <=" + (iPageSize * (p_iPageNumber));
                    sSQL1 = " ) TEMP ";
					objReaderPolicy = DbFactory.GetDbReader(m_sConnectionString, sSQL + " AND PRIMARY_POLICY_FLG <> 0" +  sSQL1);
                    //objReaderNextPolicy = DbFactory.GetDbReader(m_sConnectionString, sSQL + " AND PRIMARY_POLICY_FLG = 0" + sSQLOrder);
                    //if (objReaderNextPolicy.Read())
                    //    bNoNonPrimary=true;

                    //objReaderNextPolicy.Close();

					objPoliciesXml=new XmlDocument();
					objPolicies=objPoliciesXml.CreateElement("policies");
                    objPolicies.SetAttribute("AttachMultiplePolicy", bUseAdvancedClaim.ToString());
					objPoliciesXml.AppendChild(objPolicies);
                    //added by Amitosh for mits 25163 Policy interface
                   
                    objPolicies.SetAttribute("UsePolicyInterface", m_objDataModelFactory.Context.InternalSettings.SysSettings.UsePolicyInterface.ToString());
                    objPoliciesXml.AppendChild(objPolicies);
                    //End Amitosh
					while (objReaderPolicy.Read())
					{
                        if (bUseEPS)
                        {
                            // Claim Date
                            if (Conversion.ConvertObjToInt(objReaderPolicy.GetValue("TRIGGER_CLAIM_FLAG"), m_iClientId) != 0)
                            {
                                sSQLTemp = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + Conversion.ConvertObjToInt(objReaderPolicy.GetValue("POLICY_ID"), m_iClientId);
                                sSQLTemp = sSQLTemp + " AND TERM_NUMBER=" + Conversion.ConvertObjToInt(objReaderPolicy.GetValue("TERM_NUMBER"), m_iClientId);
                                sSQLTemp = sSQLTemp + " AND EFFECTIVE_DATE <= '" + p_sClaimDate + "'";
                                sSQLTemp = sSQLTemp + " AND TRANSACTION_TYPE <> " + m_objDataModelFactory.Context.LocalCache.GetCodeId("AU", "POLICY_TXN_TYPE");
                                objTransactionID = DbFactory.GetDbReader(m_sConnectionString, sSQLTemp);
                                if (objTransactionID.Read())
                                    iTranId = Conversion.ConvertObjToInt(objTransactionID.GetValue(0), m_iClientId);
                                objTransactionID.Close();
                            }
                            // Event Date
                            else
                            {
                                sSQLTemp = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + Conversion.ConvertObjToInt(objReaderPolicy.GetValue("POLICY_ID"), m_iClientId);
                                sSQLTemp = sSQLTemp + " AND TERM_NUMBER=" + Conversion.ConvertObjToInt(objReaderPolicy.GetValue("TERM_NUMBER"), m_iClientId);
                                sSQLTemp = sSQLTemp + " AND EFFECTIVE_DATE <= '" + p_sEventDate + "'";
                                sSQLTemp = sSQLTemp + " AND TRANSACTION_TYPE <> " + m_objDataModelFactory.Context.LocalCache.GetCodeId("AU", "POLICY_TXN_TYPE");
                                objTransactionID = DbFactory.GetDbReader(m_sConnectionString, sSQLTemp);
                                if (objTransactionID.Read())
                                    iTranId = Conversion.ConvertObjToInt(objTransactionID.GetValue(0), m_iClientId);
                                objTransactionID.Close();
                            }
                            sSQLTemp = "SELECT * FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iTranId;
                            objTransactionID = DbFactory.GetDbReader(m_sConnectionString, sSQLTemp);
                            if (objTransactionID.Read())
                            {
                                sTranType = m_objDataModelFactory.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objTransactionID.GetValue("TRANSACTION_TYPE"), m_iClientId));
                                sPolicyStatus = m_objDataModelFactory.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objTransactionID.GetValue("POLICY_STATUS"), m_iClientId));
                            }
                            objTransactionID.Close();
                        }
                        if (!bUseEPS ||
                            (bUseEPS && (sPolicyStatus == "I" || sPolicyStatus == "PCF" || sPolicyStatus == "PCP" || sPolicyStatus == "E")))
                        {
                                if (iLastPolicyID != Conversion.ConvertObjToInt(objReaderPolicy.GetValue("POLICY_ID"), m_iClientId))
                                {
                                    objElem = objPoliciesXml.CreateElement("policy");
                                    objPolicies.AppendChild(objElem);

                                    iPolicyId = Conversion.ConvertObjToInt(objReaderPolicy.GetValue("POLICY_ID"), m_iClientId);

                                    objElem.SetAttribute("policyid", iPolicyId.ToString());
                                    objElem.SetAttribute("policynumber", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_NUMBER")));

                                    iLastPolicyID = iPolicyId;
                                    iNextPolicyID = Conversion.ConvertObjToInt(objReaderPolicy.GetValue("NEXT_POLICY_ID"), m_iClientId);

                                    objElem.SetAttribute("policyname", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_NAME")));

                                    //***********************************
                                    // Insurer Last Name field added to display...
                                    //***********************************
                                    objElem.SetAttribute("insurer", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("LAST_NAME")));

                                    objElem.SetAttribute("effectivedate", Conversion.ToDate(Conversion.ConvertObjToStr(objReaderPolicy.GetValue("EFFECTIVE_DATE"))).ToShortDateString());
                                    objElem.SetAttribute("expirationdate", Conversion.ToDate(Conversion.ConvertObjToStr(objReaderPolicy.GetValue("EXPIRATION_DATE"))).ToShortDateString());
                                    objElem.SetAttribute("occurencelimit", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("OCCURRENCE_LIMIT")));
                                    if (Conversion.ConvertObjToStr(objReaderPolicy.GetValue("COMMENTS")) == null || Conversion.ConvertObjToStr(objReaderPolicy.GetValue("COMMENTS")) == "") //csingh7 for Mits 20092
                                    {
                                        sComments = "false";
                                    }
                                    else
                                    {
                                        sComments = "true";
                                    }
                                    objElem.SetAttribute("comments",sComments); //csingh7 for MITS 20092

                                    dPolLimit = Conversion.ConvertObjToDouble(objReaderPolicy.GetValue("POLICY_LIMIT"), m_iClientId);
                                    dTotalPayments = Conversion.ConvertObjToDouble(objReaderPolicy.GetValue("TOTAL_PAYMENTS"), m_iClientId);

                                    if (dPolLimit != 0)
                                        dApp = (dTotalPayments / dPolLimit) * 100;

                                    objElem.SetAttribute("app_proc", "%" + dApp.ToString("00"));

                                  //  objElem.SetAttribute("policysystem", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_SYSTEM_NAME")));
                                    objElem = null;
                                    iRecordCount = Conversion.ConvertObjToInt(objReaderPolicy.GetValue("COUNT"), m_iClientId);

                                    //if (!(bNoNonPrimary))
                                    //{
                                    //    objReaderNextPolicy = DbFactory.GetDbReader(m_sConnectionString, sSQL + " AND PRIMARY_POLICY_FLG = 0" + sSQLOrder);
                                    //    while (objReaderNextPolicy.Read())
                                    //    {
                                    //        if (iNextPolicyID == Conversion.ConvertObjToInt(objReaderNextPolicy.GetValue("POLICY_ID"), m_iClientId))
                                    //        {
                                    //            objElem = objPoliciesXml.CreateElement("policy");
                                    //            objPolicies.AppendChild(objElem);

                                    //            objElem.SetAttribute("policyid", Conversion.ConvertObjToStr(objReaderNextPolicy.GetValue("POLICY_ID")));
                                    //            iNextPolicyID = Conversion.ConvertObjToInt(objReaderNextPolicy.GetValue("NEXT_POLICY_ID"), m_iClientId);
                                    //            objElem.SetAttribute("policynumber", string.Empty);
                                    //            objElem.SetAttribute("policyname", Conversion.ConvertObjToStr(objReaderNextPolicy.GetValue("POLICY_NAME")));
                                    //            objElem.SetAttribute("effectivedate", Conversion.ToDate(Conversion.ConvertObjToStr(objReaderNextPolicy.GetValue("EFFECTIVE_DATE"))).ToShortDateString());
                                    //            objElem.SetAttribute("expirationdate", Conversion.ToDate(Conversion.ConvertObjToStr(objReaderNextPolicy.GetValue("EXPIRATION_DATE"))).ToShortDateString());
                                    //            objElem.SetAttribute("occurencelimit", Conversion.ConvertObjToStr(objReaderNextPolicy.GetValue("OCCURRENCE_LIMIT")));

                                    //            dPolLimit = Conversion.ConvertObjToDouble(objReaderNextPolicy.GetValue("POLICY_LIMIT"), m_iClientId);
                                    //            dTotalPayments = Conversion.ConvertObjToDouble(objReaderNextPolicy.GetValue("TOTAL_PAYMENTS"), m_iClientId);

                                    //            if (dPolLimit != 0)
                                    //                dApp = (dTotalPayments / dPolLimit) * 100;

                                    //            objElem.SetAttribute("app_proc", "%" + dApp.ToString("00"));

                                    //            objElem = null;
                                    //        }
                                    //    }
                                    //    objReaderNextPolicy.Close();
                                    //    objReaderNextPolicy = null;
                                    //}
                                    }
                                }
                            }
                    //rsushilaggar MITS 28647 Date 06/14/2012
                    if (objPolicies.ChildNodes != null && objPolicies.ChildNodes.Count > 0)
                    {
                        iRecordCount = objPolicies.ChildNodes.Count;
                    }
                    objPolicies.SetAttribute("RecordCount", iRecordCount.ToString());
                    objPoliciesXml.AppendChild(objPolicies);
                    objPolicies.SetAttribute("PageNumber", p_iPageNumber.ToString());
                    objPoliciesXml.AppendChild(objPolicies);
//ABAHl3 End: MITS 25163- Policy Interface Implementation                        
					objReaderPolicy.Close();
					objReaderPolicy=null;
				}
				return objPoliciesXml;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LookupClaimPolicy.GetLookupClaimPolicyList.Error", m_iClientId), p_objEx);
			}
			finally
			{
                if (objReaderPolicy != null)
                {
                    objReaderPolicy.Dispose();
                }
                if (objReaderNextPolicy != null)
                {
                    objReaderNextPolicy.Dispose();
                }
				if (m_objDataModelFactory != null)
				{
                    m_objDataModelFactory.Dispose();
				}
                if (objTransactionID != null)
                {
                    objTransactionID.Dispose();
                }
				objPoliciesXml=null;
				objPolicies=null;
				objElem=null;
                //Start-Mridul Bansal. 01/11/10. MITS#18229.
                objLobSettings=null;
                //End-Mridul Bansal. 01/11/10. MITS#18229.
			}
		}


        //public XmlDocument GetLookupTaggedPolicy(int p_iClaimTypeCode, string p_sEventDate,
        //    string p_sClaimDate, int p_iDeptEID, int p_iLOB, int p_iJurisdictionId, int iTaggedPolicyId)
        //public XmlDocument GetLookupTaggedPolicy(int p_iClaimTypeCode, string p_sEventDate, string p_sClaimDate, int p_iDeptEID, int p_iLOB, int p_iJurisdictionId, string iTaggedPolicyId, int iCurrencyType)
        //{
        //    DbReader objReaderPolicy = null;

        //    XmlDocument objPoliciesXml = null;
        //    XmlElement objPolicies = null;
        //    XmlElement objElem = null;
        //    string sSQLSelect = string.Empty;
        //    string sSQLFrom = string.Empty;
        //    string sSQLWhere = string.Empty;
        //    string sSQLOrder = string.Empty;
        //    string sSQL = string.Empty;
        //    string sSQLTemp = string.Empty;
        //    string sTranType = string.Empty;
        //    string sPolicyStatus = string.Empty;
        //    LocalCache objCache = null;
        //    string sLobName = string.Empty;
        //    string sComments = string.Empty;
        //    int iRecordCount = 0;

        //    double dPolLimit = 0;
        //    double dTotalPayments = 0;
        //    double dApp = 0;
        //    //int iCN_POLICY_REVOKED = 0;
        //    //int iCN_POLICY_IN_EFFECT = 0;
        //    //int iCN_POLICY_EXPIRED = 0;
        //    //int iCN_POLICY_CANCELED = 0;
        //    //int iCoverageTypeCode = 0;

            
        //    string sGetOrgForDept = string.Empty;
        //    try
        //    {
        //        m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword);
        //        m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

        //        objCache = new LocalCache(m_sConnectionString);
        //        bool bUseAdvancedClaim = m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1 ? true : false;

        //       //nsachdeva2:  MITS 25163- Policy Interface Implementation - 1/6/2012 - moved query to new function
            
        //        //sLobName = objCache.GetShortCode(p_iLOB).ToString();

        //        //if (p_iDeptEID != 0)
        //        //    sGetOrgForDept = GetOrgListForDept(p_iDeptEID);
        //        //else
        //        //    sGetOrgForDept = "0";


        //        //iCN_POLICY_REVOKED = objCache.GetCodeId(SC_POLICY_REVOKED, "POLICY_STATUS");
        //        //iCN_POLICY_IN_EFFECT = objCache.GetCodeId(SC_POLICY_IN_EFFECT, "POLICY_STATUS");
        //        //iCN_POLICY_EXPIRED = objCache.GetCodeId(SC_POLICY_EXPIRED, "POLICY_STATUS");
        //        //iCN_POLICY_CANCELED = objCache.GetCodeId(SC_POLICY_CANCELED, "POLICY_STATUS");
        //        //iCoverageTypeCode = objCache.GetRelatedCodeId(p_iClaimTypeCode);

        //        //bool bJuris = false;


        //        //sSQLSelect = "SELECT POLICY.POLICY_ID,POLICY_NUMBER,POLICY.COMMENTS,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";

        //        //if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
        //        //{
        //        //    sSQLSelect = sSQLSelect + " ,ISNULL('RMX',POLICY_X_WEB.POLICY_SYSTEM_NAME) POLICY_SYSTEM_NAME";
        //        //}
        //        //else if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
        //        //{
        //        //    sSQLSelect = sSQLSelect + " , NVL(POLICY_X_WEB.POLICY_SYSTEM_NAME,'RMX') POLICY_SYSTEM_NAME";
        //        //}

        //        //sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY,POLICY_X_WEB ";
        //        //if (p_iJurisdictionId != 0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 &&
        //        //    (m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 0 ||
        //        //        (m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 1 && sLobName == "WC")))
        //        //{
        //        //    bJuris = true;
        //        //    sSQLFrom += " , POLICY_X_STATE";
        //        //}

        //        //sSQLFrom = sSQLFrom + " ,POLICY_X_UNIT";
        //        //sSQLWhere = " WHERE (POLICY.POLICY_ID =" + iTaggedPolicyId;

        //        //sSQLWhere = sSQLWhere + " AND POLICY_X_UNIT.POLICY_ID = POLICY.POLICY_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID";
        //        //sSQLWhere = sSQLWhere + " AND  POLICY.INSURER_EID = ENTITY.ENTITY_ID)";
        //        //sSQLWhere = sSQLWhere + " AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
        //        //sSQLWhere = sSQLWhere + "      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))";
        //        ////sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)";
        //        //sSQLWhere = sSQLWhere + " AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))";
        //        //sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ";
        //        //sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + p_sClaimDate + "'";
        //        //sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)";
        //        //sSQLWhere = sSQLWhere + "      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED;
        //        //sSQLWhere = sSQLWhere + "          AND POLICY.CANCEL_DATE > '" + p_sEventDate + "'";
        //        //sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG = 0))";
        //        //sSQLWhere = sSQLWhere + " AND ((POLICY.EFFECTIVE_DATE <= '" + p_sEventDate + "'";
        //        //sSQLWhere = sSQLWhere + "       AND POLICY.EXPIRATION_DATE >= '" + p_sEventDate + "'";
        //        //sSQLWhere = sSQLWhere + "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)";
        //        //sSQLWhere = sSQLWhere + "      OR (POLICY.EFFECTIVE_DATE <= '" + p_sClaimDate + "'";
        //        //sSQLWhere = sSQLWhere + "          AND POLICY.EXPIRATION_DATE >= '" + p_sClaimDate + "'";
        //        //sSQLWhere = sSQLWhere + "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))";
        //        //if (bJuris)
        //        //{
        //        //    sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ";
        //        //    sSQLWhere = sSQLWhere + " AND POLICY_X_STATE.STATE_ID =" + p_iJurisdictionId + " ";
        //        //}
        //        //sSQL = sSQLSelect + sSQLFrom + sSQLWhere;
        //        //objReaderPolicy = DbFactory.GetDbReader(m_sConnectionString, sSQL + " AND PRIMARY_POLICY_FLG <> 0" + sSQLOrder);
        //        sSQL = GetTaggedPolicySql(p_iClaimTypeCode, p_sEventDate, p_sClaimDate, p_iDeptEID, p_iLOB, p_iJurisdictionId, iTaggedPolicyId, iCurrencyType); 
        //        objReaderPolicy = DbFactory.GetDbReader(m_sConnectionString, sSQL );
        //        //end MITS 25163- Policy Interface Implementation 
        //        objPoliciesXml = new XmlDocument();
        //        objPolicies = objPoliciesXml.CreateElement("policies");
        //        objPolicies.SetAttribute("AttachMultiplePolicy", bUseAdvancedClaim.ToString());
        //        objPoliciesXml.AppendChild(objPolicies);

        //        objPolicies.SetAttribute("UsePolicyInterface", m_objDataModelFactory.Context.InternalSettings.SysSettings.UsePolicyInterface.ToString());
        //        objPoliciesXml.AppendChild(objPolicies);

        //        if (objReaderPolicy.Read())
        //        {
        //            iRecordCount = 1;
        //            objElem = objPoliciesXml.CreateElement("policy");
        //            objPolicies.AppendChild(objElem);

        //            objElem.SetAttribute("policyid", iTaggedPolicyId.ToString());
        //            objElem.SetAttribute("policynumber", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_NUMBER")));
        //            objElem.SetAttribute("policyname", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_NAME")));
        //            objElem.SetAttribute("insurer", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("LAST_NAME")));
        //            objElem.SetAttribute("effectivedate", Conversion.ToDate(Conversion.ConvertObjToStr(objReaderPolicy.GetValue("EFFECTIVE_DATE"))).ToShortDateString());
        //            objElem.SetAttribute("expirationdate", Conversion.ToDate(Conversion.ConvertObjToStr(objReaderPolicy.GetValue("EXPIRATION_DATE"))).ToShortDateString());
        //            objElem.SetAttribute("occurencelimit", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("OCCURRENCE_LIMIT")));
        //            if (Conversion.ConvertObjToStr(objReaderPolicy.GetValue("COMMENTS")) == null || Conversion.ConvertObjToStr(objReaderPolicy.GetValue("COMMENTS")) == "")
        //            {
        //                sComments = "false";
        //            }
        //            else
        //            {
        //                sComments = "true";
        //            }
        //            objElem.SetAttribute("comments", sComments);

        //            dPolLimit = Conversion.ConvertObjToDouble(objReaderPolicy.GetValue("POLICY_LIMIT"), m_iClientId);
        //            dTotalPayments = Conversion.ConvertObjToDouble(objReaderPolicy.GetValue("TOTAL_PAYMENTS"), m_iClientId);

        //            if (dPolLimit != 0)
        //                dApp = (dTotalPayments / dPolLimit) * 100;

        //            objElem.SetAttribute("app_proc", "%" + dApp.ToString("00"));



        //            objElem.SetAttribute("policysystem", Conversion.ConvertObjToStr(objReaderPolicy.GetValue("POLICY_SYSTEM_NAME")));

        //            objElem = null;



        //        }

        //        objPolicies.SetAttribute("RecordCount", iRecordCount.ToString());
        //        objPoliciesXml.AppendChild(objPolicies);
        //        objPolicies.SetAttribute("PageNumber", "1");
        //        objPoliciesXml.AppendChild(objPolicies);


        //        objReaderPolicy.Close();
        //        objReaderPolicy = null;

        //        return objPoliciesXml;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("LookupClaimPolicy.GetLookupClaimPolicyList.Error"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objReaderPolicy != null)
        //        {
        //            objReaderPolicy.Dispose();
        //        }

        //        objPoliciesXml = null;
        //        objPolicies = null;
        //        objElem = null;
        //        if (m_objDataModelFactory != null)
        //        {
        //            m_objDataModelFactory.Dispose();
        //        }
        //    }
        //}

		#endregion

		#region "Private Functions"

			private string GetOrgListForDept(int p_iDeptEId)
			{
				//DbReader objReader=null;	
				string sSQL=string.Empty;
				string sGetOrgListForDept=string.Empty;

				sSQL="SELECT ENTITY.ENTITY_ID, E1.ENTITY_ID, E2.ENTITY_ID, E3.ENTITY_ID, E4.ENTITY_ID, E5.ENTITY_ID, E6.ENTITY_ID, E7.ENTITY_ID " +
					" FROM ENTITY, ENTITY E1, ENTITY E2, ENTITY E3, ENTITY E4, ENTITY E5, ENTITY E6, ENTITY E7 " +
					" WHERE ENTITY.ENTITY_ID=" + p_iDeptEId + " AND E1.ENTITY_ID=ENTITY.PARENT_EID AND "+
					" E2.ENTITY_ID=E1.PARENT_EID AND E3.ENTITY_ID=E2.PARENT_EID AND E4.ENTITY_ID=E3.PARENT_EID " +
					" AND E5.ENTITY_ID=E4.PARENT_EID AND E6.ENTITY_ID=E5.PARENT_EID AND E7.ENTITY_ID=E6.PARENT_EID";

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {

                    if (objReader.Read())
                        sGetOrgListForDept = p_iDeptEId.ToString() + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(1)) + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(2)) + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(3)) + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(4)) + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(5)) + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(6)) + "," +
                            Conversion.ConvertObjToStr(objReader.GetValue(7));
                }
				return sGetOrgListForDept;
										
			}

		#endregion

        //nsachdeva2:  MITS 25163- Policy Interface Implementation - 1/6/2012 
        //public string GetTaggedPolicySql(int p_iClaimTypeCode, string p_sEventDate, string p_sClaimDate, int p_iDeptEID, int p_iLOB, int p_iJurisdictionId, string sTaggedPolicyIds, int iCurrencyType)
        //{
        //    string sSQLSelect = string.Empty;
        //    string sSQLFrom = string.Empty;
        //    StringBuilder sbSQLUnionWhere =  null;
        //    StringBuilder sbSQLWhere = null;
        //    string sSQL = string.Empty;
        //    string sTranType = string.Empty;
        //    string sPolicyStatus = string.Empty;
        //    LocalCache objCache = null;
        //    string sLobName = string.Empty;
        //    string sComments = string.Empty;
        //    int iCN_POLICY_REVOKED = 0;
        //    int iCN_POLICY_IN_EFFECT = 0;
        //    int iCN_POLICY_EXPIRED = 0;
        //    int iCN_POLICY_CANCELED = 0;
        //    int iCoverageTypeCode = 0;
                
        //    string sGetOrgForDept = string.Empty;
        //    try
        //    {
        //        m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword);
        //        m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

        //        objCache = new LocalCache(m_sConnectionString);
        //        //bool bUseAdvancedClaim = m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1 ? true : false;

        //        sLobName = objCache.GetShortCode(p_iLOB).ToString();

        //        if (p_iDeptEID != 0)
        //            sGetOrgForDept = GetOrgListForDept(p_iDeptEID);
        //        else
        //            sGetOrgForDept = "0";


        //        iCN_POLICY_REVOKED = objCache.GetCodeId(SC_POLICY_REVOKED, "POLICY_STATUS");
        //        iCN_POLICY_IN_EFFECT = objCache.GetCodeId(SC_POLICY_IN_EFFECT, "POLICY_STATUS");
        //        iCN_POLICY_EXPIRED = objCache.GetCodeId(SC_POLICY_EXPIRED, "POLICY_STATUS");
        //        iCN_POLICY_CANCELED = objCache.GetCodeId(SC_POLICY_CANCELED, "POLICY_STATUS");
        //        iCoverageTypeCode = objCache.GetRelatedCodeId(p_iClaimTypeCode);

        //        bool bJuris = false;
        //        sbSQLWhere = new StringBuilder();
        //        sbSQLUnionWhere = new StringBuilder();

        //        sSQLSelect = "SELECT POLICY.POLICY_ID,POLICY_NUMBER,POLICY.COMMENTS,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";

        //        if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
        //        {
        //            sSQLSelect = sSQLSelect + " ,ISNULL('RMX',POLICY_X_WEB.POLICY_SYSTEM_NAME) POLICY_SYSTEM_NAME";
        //        }
        //        else if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
        //        {
        //            sSQLSelect = sSQLSelect + " , NVL(POLICY_X_WEB.POLICY_SYSTEM_NAME,'RMX') POLICY_SYSTEM_NAME";
        //        }

        //        sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY,POLICY_X_WEB ";
        //        if (p_iJurisdictionId != 0 && m_objDataModelFactory.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 &&
        //            (m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 0 ||
        //                (m_objDataModelFactory.Context.InternalSettings.SysSettings.StateEval == 1 && sLobName == "WC")))
        //        {
        //            bJuris = true;
        //            sSQLFrom += " , POLICY_X_STATE";
        //        }

        //        sSQLFrom = sSQLFrom + " ,POLICY_X_UNIT";
        //        sbSQLWhere.Append(" WHERE (POLICY.POLICY_ID in (" + sTaggedPolicyIds + ")");
        //        sbSQLWhere.Append(" AND POLICY_X_UNIT.POLICY_ID = POLICY.POLICY_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID");
        //        sbSQLWhere.Append(" AND  POLICY.INSURER_EID = ENTITY.ENTITY_ID)");
        //        sbSQLWhere.Append(" AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode);
        //        sbSQLWhere.Append("      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))");
        //        sbSQLWhere.Append(" AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))");
        //        sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ");
        //        sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + p_sClaimDate + "'");
        //        sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)");
        //        sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED);
        //        sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + p_sEventDate + "'");
        //        sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG = 0))");
        //        sbSQLWhere.Append( " AND ((POLICY.EFFECTIVE_DATE <= '" + p_sEventDate + "'");
        //        sbSQLWhere.Append("       AND POLICY.EXPIRATION_DATE >= '" + p_sEventDate + "'");
        //        sbSQLWhere.Append( "       AND POLICY.TRIGGER_CLAIM_FLAG = 0)");
        //        sbSQLWhere.Append( "      OR (POLICY.EFFECTIVE_DATE <= '" + p_sClaimDate + "'");
        //        sbSQLWhere.Append( "          AND POLICY.EXPIRATION_DATE >= '" + p_sClaimDate + "'");
        //        sbSQLWhere.Append( "          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))");
        //        if (bJuris)
        //        {
        //            sbSQLWhere.Append( " AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ");
        //            sbSQLWhere.Append( " AND POLICY_X_STATE.STATE_ID =" + p_iJurisdictionId + " ");
        //        }
        //        sbSQLWhere.Append(" AND PRIMARY_POLICY_FLG <> 0 AND POLICY.POLICY_SYSTEM_ID = 0");
        //        sSQL = sSQLSelect + sSQLFrom + sbSQLWhere.ToString();

        //        //Added Union to handle Policy_system_id values
        //        sSQL = sSQL + " UNION " + sSQLSelect.Replace("ENTITY.LAST_NAME", "'' as LAST_NAME") + sSQLFrom.Replace(", POLICY_X_INSURED, ENTITY", "")  ;
        //        sbSQLUnionWhere.Append(" WHERE POLICY.POLICY_ID in (" + sTaggedPolicyIds + ")");
        //        sbSQLUnionWhere.Append(" AND POLICY_X_UNIT.POLICY_ID = POLICY.POLICY_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID");
        //        sbSQLUnionWhere.Append(" AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode);
        //        sbSQLUnionWhere.Append(" AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))");
        //        sbSQLUnionWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ");
        //        sbSQLUnionWhere.Append("          AND POLICY.CANCEL_DATE > '" + p_sClaimDate + "'");
        //        sbSQLUnionWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)");
        //        sbSQLUnionWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED);
        //        sbSQLUnionWhere.Append("          AND POLICY.CANCEL_DATE > '" + p_sEventDate + "'");
        //        sbSQLUnionWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG = 0))");
        //        sbSQLUnionWhere.Append(" AND ((POLICY.EFFECTIVE_DATE <= '" + p_sEventDate + "'");
        //        sbSQLUnionWhere.Append("       AND POLICY.EXPIRATION_DATE >= '" + p_sEventDate + "'");
        //        sbSQLUnionWhere.Append("       AND POLICY.TRIGGER_CLAIM_FLAG = 0)");
        //        sbSQLUnionWhere.Append("      OR (POLICY.EFFECTIVE_DATE <= '" + p_sClaimDate + "'");
        //        sbSQLUnionWhere.Append("          AND POLICY.EXPIRATION_DATE >= '" + p_sClaimDate + "'");
        //        sbSQLUnionWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))");
        //        if (bJuris)
        //        {
        //            sbSQLUnionWhere.Append(" AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ");
        //            sbSQLUnionWhere.Append(" AND POLICY_X_STATE.STATE_ID =" + p_iJurisdictionId + " ");
        //        }
        //        sbSQLUnionWhere.Append(" AND PRIMARY_POLICY_FLG <> 0 AND POLICY.POLICY_SYSTEM_ID <> 0");
        //        sbSQLUnionWhere.Append(" AND POLICY.CURRENCY_CODE =" + iCurrencyType);
        //        sSQL = sSQL + sbSQLUnionWhere.ToString();
        //        return sSQL;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("LookupClaimPolicy.GetLookupClaimPolicyList.Error"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objCache != null)
        //        {
        //            objCache.Dispose();
        //        }
               
        //        sbSQLUnionWhere = null;
        //        sbSQLWhere = null;
        //    }
        //}
	}
}
