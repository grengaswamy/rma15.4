﻿
using System;
using System.Xml;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Settings;
using Riskmaster.Security;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Xml.XPath;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.Diaries;
using System.Drawing;
using C1.C1PrintDocument ;

namespace Riskmaster.Application.ImportXML
{
    /// <summary>
    ///Author  :   Manika Dhamia
    ///Dated   :   24th July 2011
    ///Purpose :   This class imports and save the imputXML string.
    /// </summary>
    public class ImportXML
    {
        private string m_sConnectionString = "";

        /// <summary>
        /// The connection string used by the database layer for connection with the security database.
        /// </summary>
        private string m_sSecConnectString = "";

        /// <summary>
        /// Represents the User Id.
        /// </summary>
        private long m_lUserId = 0;
        private long m_lDSNId = 0;

        /// <summary>
        /// The Group id 
        /// </summary>
        private int m_iGroupId = 0;

        /// <summary>
        /// The Page Size
        /// </summary>
        private long m_lPageSize = 0;

        /// <summary>
        /// User Login Name
        /// </summary>
        private string m_sLoginName = string.Empty;

        /// <summary>
        /// User Password
        /// </summary>
        private string m_sPassword = string.Empty;

        /// <summary>
        /// User Data Source Name
        /// </summary>
        private string m_sDSNName = string.Empty;
        private Hashtable m_objConfig = null;
        private static string m_sDBType = "";
        /// <summary>
        /// Enum for display order (order by clause)
        /// </summary>
        //SysSettings objSys = new SysSettings(m_sConnectionString);

        //Added by Amitosh for Attachment through XMLWebService
        private UserLogin m_oUserlogin = null;

        /// <summary>
        /// ClientID for multi-tenant environment, cloud
        /// </summary>
        private int m_iClientId = 0;

        public const int RMO_PERSON_INVOLVED = 11150;
        public const int RMO_GC_ADJUSTER = 1050;
        public const int RMO_VA_ADJUSTER = 6900;
        public const int RMO_WC_ADJUSTER = 3900;
        public const int RMO_DI_ADJUSTER = 60900;
        public const int RMO_PC_ADJUSTER = 41050;
        private const int RMO_Intervention = 12750;
        private const int RMO_DATEDTEXT = 12650;
        private const int RMO_GCSecurityId = 150;
        private const int RMO_WCSecurityId = 3000;
        private const int RMO_VASecurityId = 6000;
        private const int RMO_PCSecurityId = 40000;
        //igupta3 MITS:32002 Security id corrected for DI
        private const int RMO_DISecurityId = 600000;
        private const int RMO_PATIENT = 21000;
        private const int RMO_PHYSICIAN = 21200;
        private const int RMO_GC_CLAIMANTS = 1800;
        private const int RMO_VA_CLAIMANTS = 7650;
        private const int RMO_PC_CLAIMANT = 41800;
        private const int RMO_PC_DEFENDANT = 41650;
        private const int RMO_GC_DEFENDANT = 1650;
        private const int RMO_VA_DEFENDANT = 7500;
        private const int RMO_WC_DEFENDANT = 4500;
        private const int RMO_DI_DEFENDANT = 61500;
        private const int RMO_FUNDS_TRANSACT = 9650;
        private const int RMO_ENTITYMAINTENANCE = 16500;
        private const int RMO_EVENT = 11000;
        private const int RMO_DISABILITY_PLAN = 11000;
        private const int RMO_POLICY = 9000;
        private const int RMO_LEAVE_PLAN = 24000;
        private const int RMO_MED_STAFF = 21300;
        private const int RMO_VEHICLE = 13500;
        private const int RMO_PROPERTY_UNIT = 24500;
        private const int RMO_POLICYENH_WC = 25000;
        private const int RMO_POLICYENH_GL = 23000;
        private const int RMO_POLICYENH_PC = 42000;
        private const int RMO_POLICYENH_AL = 26000;
        public const int RMO_GC_LITIGATION = 1350;
        public const int RMO_VA_LITIGATION = 7200;
        public const int RMO_WC_LITIGATION = 4200;
        public const int RMO_DI_LITIGATION = 61200;
        public const int RMO_PC_LITIGATION = 41350;
        //igupta3   MITS : 32002 Changes Starts
        public const int RMO_GC_SUBROGATION = 44000;
        public const int RMO_VA_SUBROGATION = 44100;
        public const int RMO_WC_SUBROGATION = 44200;
        public const int RMO_PC_SUBROGATION = 44400;
        public const int RMO_DI_SUBROGATION = 44300;
        public const int RMO_VA_UNIT = 8100;
        //igupta3   MITS: 32002 Changes Ends
        string sRecordsUploaded = string.Empty;
        string sFolderIds = string.Empty;
        //end Amitosh
        public XDocument XMLDoc;

        //Deb: Made some variables global
        string nodeName = string.Empty;
        private string DateCreated = string.Empty;
        private string TimeCreated = string.Empty;
        private string CreatedBy = string.Empty;
        //Deb: Made some variables global

        public ImportXML(string p_sConnectionstring, int p_iClientId) //rkaur27
        {
            DbConnection objConn;
            m_sConnectionString = p_sConnectionstring;
            //m_objConfig = RMConfigurationSettings.GetRMMessages();  
            m_iClientId = p_iClientId; //rkaur27
            m_objConfig = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId); //rkaur27
            m_sSecConnectString = RMConfigurationSettings.GetSecurityDSN(m_iClientId);
            objConn = DbFactory.GetDbConnection(m_sSecConnectString);
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            objConn.Close();
        }
        public ImportXML(string p_sConnectionstring, string p_sDSNName, string p_sPassword, string p_sLoginName,int p_iClientId) //rkaur27
        {
            DbConnection objConn;
            m_sConnectionString = p_sConnectionstring;
            m_sDSNName = p_sDSNName;
            m_sLoginName = p_sLoginName;
            m_sPassword = p_sPassword;
            m_iClientId = p_iClientId; //rkaur27
            m_objConfig = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId); //rkaur27
            m_sSecConnectString = RMConfigurationSettings.GetSecurityDSN(m_iClientId); //mbhal3
            objConn = DbFactory.GetDbConnection(m_sSecConnectString);
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            objConn.Close();
            UserLogin objUserLogin = new UserLogin(p_sLoginName, p_sPassword, p_sDSNName,m_iClientId); //mbahl3 
            m_oUserlogin = objUserLogin;

        }

        //Check if the xml already exists in the DB.
        private bool ifXMLExists(string DateCreated, string TimeCreated, string CreatedBy, string p_sConnectString)
        {
            bool XMLExists = false;
            string sSql = string.Empty;
            DbReader objReader = null;
            string sTempDateCreated = string.Empty;
            string sTempTimeCreated = string.Empty;
            DbConnection objConn = null;
            DbCommand objCmd = null;
            try
            {
                //bsharma33 Start changes for MITS 32552
                    sTempDateCreated = Conversion.GetDate(DateCreated);
                    
                    if (string.IsNullOrEmpty(sTempDateCreated))
                    {
                        throw new System.ArgumentOutOfRangeException();
                    }
                    sTempTimeCreated = Conversion.GetTime(TimeCreated);
                    if (string.IsNullOrEmpty(sTempTimeCreated))
                    {
                        throw new System.ArgumentOutOfRangeException();
                    }

                //bsharma33 End changes for MITS 32552
                //sSql = "SELECT IMPORT_USER_ID FROM XML_IMPORT_HISTORY" +
                //            " WHERE XML_DOC_CREATE_DTTM='" + sTempDateCreated + sTempTimeCreated + " ' AND XML_DOC_CREATEDBY='" + CreatedBy + "'";
                // Ijha: MITS 26603: Validation for Oracle and SQL
                sSql = "SELECT IMPORT_USER_ID FROM XML_IMPORT_HISTORY" +
                              " WHERE XML_DOC_CREATE_DTTM='" + sTempDateCreated + sTempTimeCreated + "' AND XML_DOC_CREATEDBY='" + CreatedBy + "'";



                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

                if (objReader.Read())
                {
                    XMLExists = true;
                }
                else
                {
                    sTempDateCreated = Conversion.GetDate(DateCreated);
                    sTempTimeCreated = Conversion.GetTime(TimeCreated);
                    //Deb:Modified the insert query
                    sSql = "INSERT INTO XML_IMPORT_HISTORY(IMPORT_ROW_ID,IMPORT_DTTM,IMPORT_USER_ID,XML_DOC_CREATE_DTTM,XML_DOC_CREATEDBY) VALUES(" + Utilities.GetNextUID(m_sConnectionString, "XML_IMPORT_HISTORY", m_iClientId) + ",'" + Conversion.GetDate(System.DateTime.Now.Date.ToString()) + Conversion.GetTime(System.DateTime.Now.TimeOfDay.ToString()) + "'," + m_oUserlogin.UserId + ",'" + sTempDateCreated + sTempTimeCreated + "','" + CreatedBy + "')"; // insert the value in the database


                    objConn = DbFactory.GetDbConnection(p_sConnectString);
                    objConn.Open();
                    objCmd = objConn.CreateCommand();
                    objCmd.CommandText = sSql;
                    objCmd.ExecuteNonQuery();
                    XMLExists = false;
                }
                return (XMLExists);
            }
            //bsharma33 Startchanges for MITS 32552
            catch (System.ArgumentOutOfRangeException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ImportXML.InvalidDateTime", m_iClientId), p_objException); //mbahl3 
            }
            //bsharma33 End changes for MITS 32552
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetSingleString.GeneralError", m_iClientId), p_objException); //mbahl3 
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (objCmd != null)
                {
                    objConn.Dispose();
                }
            }
        }

        public string ReadXML(out bool bIsEventInvolved, bool rval)
        {
            int iRecordId = 0;
            int iSid = 0;
            string iDateLastUpdated = string.Empty;
            string iUpdatedByUser = string.Empty;
            string iAddedByUser = string.Empty;
            string iDateLastAdded = string.Empty;
            string sSQL = string.Empty;
            string sNodeFound = string.Empty;
            XmlDocument tempDoc = null;
            DataObject objDataModel = null;
            DataModelFactory objDmf = null;
            string sSerialize = string.Empty;
            XmlDocument objSerialize = null;
            bool blnSuccess = false;
            ArrayList newEventNumbers = new ArrayList(); // Added for MITS 29711 by bsharma33
            string sFileText = string.Empty;// Added for MITS 29711 by bsharma33
            int iEventId=0;
            
            //prashbiharis
            Dictionary<string, string> savedClaimList = new Dictionary<string, string>();
            ArrayList inEventList = new ArrayList();
            Dictionary<string, string> inClaimList = new Dictionary<string, string>();
            //prashbiharis
            
            string sFileName = string.Empty;
            try
            {
                XElement objInputXML = XMLDoc.Element("ImportDocument");
                bIsEventInvolved = false;
                if (objInputXML !=null)
                {
                    XElement objHeaderNode = objInputXML.Element("Header");
                    if (objHeaderNode != null)
                    {
                        if (ValidateHeader(objHeaderNode))
                        {
                            try
                            {

                                //Added by Amitosh for Attachment through XMLWebService
                                IEnumerable<XElement> ChildNodes = from c in objHeaderNode.ElementsAfterSelf()
                                                                   select c;
                                if (XMLDoc.XPathSelectElement("//Attachment") != null)
                                {
                                    sNodeFound = "ATTACHMENT";
                                }
                                else
                                {
                                    sNodeFound = ChildNodes.First().Name.LocalName.ToUpper();
                                }
                                switch (sNodeFound)
                                {
                                    case "ATTACHMENT":
                                        bIsEventInvolved = false;
                                        IEnumerable<XElement> ChildAttachment = XMLDoc.XPathSelectElements("//Attachment");
                                       
                                        foreach (XElement oChildNode in ChildAttachment)
                                        {
                                            XElement objParent = oChildNode.Parent;
           
                                            switch (objParent.Name.LocalName.ToUpper())
                                            {
                                                case "CLAIM":
                                                    if (objParent.Element("ClaimId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("ClaimId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("ClaimNumber") != null)
                                                    {
                                                        sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + objParent.Element("ClaimNumber").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("CLAIM_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId)); //mbahl3 
                                                    }

                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword,m_iClientId); //mbahl3
                                                    objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                                                    objDataModel.MoveTo(iRecordId);

                                                    switch ((objDataModel as Claim).LineOfBusCode)
                                                    {
                                                        case 243:
                                                            iSid = RMO_WCSecurityId;//Claim is WC
                                                            break;
                                                        case 242:
                                                            iSid = RMO_VASecurityId;//CLaim is VA
                                                            break;
                                                        case 844:
                                                            iSid = RMO_DISecurityId;//Claim is Non-Occ
                                                            break;
                                                        case 241:
                                                            iSid = RMO_GCSecurityId;//Claim is GC
                                                            break;
                                                        case 845:
                                                            iSid = RMO_PCSecurityId;//Claim is PC
                                                            break;
                                                    }

                                                    break;

                                                case "EVENT":
                                                    if (objParent.Element("EventId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("EventId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("EventNumber") != null)
                                                    {
                                                        sSQL = "SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '" + objParent.Element("EventNumber").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("EVENT_ID"), m_iClientId);

                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId)); //mbahl3 
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);//mbahl3
                                                    objDataModel = (Event)objDmf.GetDataModelObject("Event", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_EVENT;

                                                    break;
                                                case "POLICY":
                                                    if (objParent.Element("PolicyId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("PolicyId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("PolicyName") != null)
                                                    {
                                                        sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_NAME = '" + objParent.Element("PolicyName").Attribute("value").Value + "'";

                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("POLICY_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword,m_iClientId);//mbahl3
                                                    objDataModel = (Policy)objDmf.GetDataModelObject("Policy", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_POLICY;

                                                    break;
                                                case "PATIENT":

                                                    if (objParent.Element("PatientId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(oChildNode.Element("PatientId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("SSN") != null)
                                                    {

                                                        sSQL = "SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_EID = (SELECT ENTITY_ID FROM ENTITY WHERE TAX_ID = '" + objParent.Element("SSN").Attribute("value").Value + "')";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PATIENT_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword,m_iClientId); //mbahl3 
                                                    objDataModel = (Patient)objDmf.GetDataModelObject("Patient", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_PATIENT;

                                                    break;
                                                case "CLAIMANT":

                                                    if (objParent.Element("ClaimantId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("ClaimantId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("SSN") != null && objParent.Element("ClaimNumber") != null)
                                                    {
                                                        sSQL = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT,CLAIM,ENTITY WHERE ENTITY.TAX_ID ='" + objParent.Element("SSN").Attribute("value").Value + "' AND CLAIMANT.CLAIMANT_EID = ENTITY_ID AND CLAIM.CLAIM_NUMBER = '" + objParent.Element("ClaimNumber").Attribute("value").Value + "' AND CLAIM.CLAIM_ID =CLAIMANT.CLAIM_ID";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("CLAIMANT_ROW_ID"), m_iClientId);
                                                            }

                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword,m_iClientId);
                                                    objDataModel = (Claimant)objDmf.GetDataModelObject("Claimant", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    int iClaimId = (objDataModel as Claimant).ClaimId;
                                                    objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                                                    objDataModel.MoveTo(iClaimId);
                                                    switch ((objDataModel as Claim).LineOfBusCode)
                                                    {
                                                        case 242:
                                                            iSid = RMO_VA_CLAIMANTS;//CLaim is VA
                                                            break;
                                                        case 241:
                                                            iSid = RMO_GC_CLAIMANTS;//Claim is GC
                                                            break;
                                                        case 845:
                                                            iSid = RMO_PC_CLAIMANT;//Claim is PC
                                                            break;
                                                    }

                                                    break;
                                                case "DEFENDANT":

                                                    if (objParent.Element("DefendantId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(oChildNode.Element("DefendantId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("SSN") != null && objParent.Element("ClaimNumber") != null)
                                                    {
                                                        sSQL = "SELECT DEFENDANT_ROW_ID FROM DEFENDANT,CLAIM,ENTITY WHERE ENTITY.TAX_ID ='" + objParent.Element("SSN").Attribute("value").Value + "' AND CLAIMANT.CLAIMANT_EID = ENTITY_ID AND CLAIM.CLAIM_NUMBER = '" + objParent.Element("ClaimNumber").Attribute("value").Value + "' AND CLAIM.CLAIM_ID =CLAIMANT.CLAIM_ID";

                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("DEFENDANT_ROW_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (Defendant)objDmf.GetDataModelObject("Defendant", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    int claimId = (objDataModel as Defendant).ClaimId;
                                                    objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                                                    objDataModel.MoveTo(claimId);
                                                    switch ((objDataModel as Claim).LineOfBusCode)
                                                    {
                                                        case 243:
                                                            iSid = RMO_WC_DEFENDANT;//Claim is WC
                                                            break;
                                                        case 242:
                                                            iSid = RMO_VA_DEFENDANT;//CLaim is VA
                                                            break;
                                                        case 844:
                                                            iSid = RMO_DI_DEFENDANT;//Claim is Non-Occ
                                                            break;
                                                        case 241:
                                                            iSid = RMO_GC_DEFENDANT;//Claim is GC
                                                            break;
                                                        case 845:
                                                            iSid = RMO_PC_DEFENDANT;//Claim is PC
                                                            break;
                                                    }

                                                    break;
                                                case "PHYSICIAN":

                                                    if (objParent.Element("PhysEid") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("PhysEid").Attribute("value").Value, out blnSuccess);
                                                    }

                                                    else if (objParent.Element("SSN") != null)
                                                    {
                                                        sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE TAX_ID ='" + objParent.Element("SSN").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("ENTITY_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {

                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }

                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (Physician)objDmf.GetDataModelObject("Physician", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_PHYSICIAN;

                                                    break;
                                                case "FUNDS":

                                                    if (objParent.Element("TransId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("TransId").Attribute("value").Value, out blnSuccess);
                                                    }

                                                    else if (objParent.Element("ControlNumber") != null)
                                                    {
                                                        sSQL = "SELECT  TRANS_ID FROM FUNDS WHERE CTL_NUMBER = '" + objParent.Element("ControlNumber").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("TRANS_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (Funds)objDmf.GetDataModelObject("Funds", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_FUNDS_TRANSACT;


                                                    break;
                                                case "ENTITY":

                                                    if (objParent.Element("EntityId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("EntityId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("SSN") != null)
                                                    {
                                                        sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE TAX_ID ='" + objParent.Element("SSN").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("ENTITY_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (Entity)objDmf.GetDataModelObject("Entity", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_ENTITYMAINTENANCE;

                                                    break;
                                                case "DISABILITYPLAN":

                                                    if (objParent.Element("PlanId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("UnitId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("PlanNumber") != null)
                                                    {
                                                        sSQL = "SELECT PLAN_ID FROM DISABILITY_PLAN WHERE PLAN_NUMBER = '" + objParent.Element("PlanNumber").Attribute("value") + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PLAN_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (DisabilityPlan)objDmf.GetDataModelObject("DisabilityPlan", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_DISABILITY_PLAN;

                                                    break;
                                                case "LEAVEPLAN":
                                                    if (objParent.Element("LPRowId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("LPRowId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("LeavePlanCode") != null)
                                                    {
                                                        sSQL = "SELECT LP_ROW_ID FROM LEAVE_PLAN WHERE LEAVE_PLAN_CODE = '" + objParent.Element("LeavePlanCode").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("LP_ROW_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (LeavePlan)objDmf.GetDataModelObject("LeavePlan", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_LEAVE_PLAN;

                                                    break;
                                                case "STAFF":
                                                    if (objParent.Element("StaffId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("StaffId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("SSN") != null)
                                                    {
                                                        sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE TAX_ID ='" + objParent.Element("SSN").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("ENTITY_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (Entity)objDmf.GetDataModelObject("Entity", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_MED_STAFF;

                                                    break;
                                                case "VEHICLE":

                                                    if (objParent.Element("UnitId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("UnitId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("VehicleId") != null)
                                                    {
                                                        sSQL = "SELECT UNIT_ID FROM VEHICLE WHERE VIN ='" + objParent.Element("VehicleId").Attribute("value").Value + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("UNIT_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);

                                                    objDataModel = (Vehicle)objDmf.GetDataModelObject("Vehicle", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_VEHICLE;

                                                    break;
                                                case "PROPERTYUNIT":
                                                    if (objParent.Element("PropertyRowId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("PropertyRowId").Attribute("value").Value, out blnSuccess);
                                                    }

                                                    if (objParent.Element("PropertyID") != null)
                                                    {
                                                        sSQL = "SELECT PROPERTY_ID FROM PROPERTY_UNIT WHERE PIN = '" + objParent.Element("PropertyID").Attribute("value").Value + "'";

                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PROPERTY_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }

                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (PropertyUnit)objDmf.GetDataModelObject("PropertyUnit", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    iSid = RMO_PROPERTY_UNIT;

                                                    break;
                                                case "POLICYENH":
                                                    if (objParent.Element("PolicyId") != null)
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(objParent.Element("PolicyId").Attribute("value").Value, out blnSuccess);
                                                    }
                                                    else if (objParent.Element("PolicyName") != null)
                                                    {
                                                        sSQL = "SELECT POLICY_ID FROM POLICY_ENH WHERE POLICY_NAME = '" + objParent.Element("PolicyName").Attribute("value").Value + "'";

                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("LP_ROW_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, m_iClientId);
                                                    objDataModel = (PolicyEnh)objDmf.GetDataModelObject("PolicyEnh", false);
                                                    objDataModel.MoveTo(iRecordId);
                                                    using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                    {
                                                        string sPolType = objLocalCache.GetCodeDesc((objDataModel as PolicyEnh).PolicyType);
                                                        switch (sPolType)
                                                        {
                                                            case "Property":
                                                                iSid = RMO_POLICYENH_PC;
                                                                break;
                                                            case "Workers' Compensation":
                                                                iSid = RMO_POLICYENH_WC;
                                                                break;
                                                            case "Auto Liability":
                                                                iSid = RMO_POLICYENH_AL;
                                                                break;
                                                            case "General Liability":
                                                                iSid = RMO_POLICYENH_GL;
                                                                break;
                                                        }
                                                    }

                                                    break;
                                                default:
                                                    throw new RMAppException(Globalization.GetString("ImportXml.Attachmentnotsupported", m_iClientId));
                                            }
                                            

                                            if (iRecordId == 0 || iRecordId == -1)
                                            {
                                                throw new RMAppException(Globalization.GetString("ImportXML.NewRecord", m_iClientId));
                                            }
                                            else
                                            {
                                                SaveAttachment(oChildNode, iRecordId, objParent.Name.LocalName.ToUpper(), iSid);
                                            }
                                        }
                                        break;
                                    case "WPADIARYENTRY":
                                        bIsEventInvolved = false;
                                        IEnumerable<XElement> AllDiaries = objInputXML.Elements("WpaDiaryEntry");
                                        foreach (XElement oDiary in AllDiaries)
                                        {
                                            SaveDiary(oDiary);
                                        }
                                        break;

                                    case "EVENT":
                                        //end Amitosh
                                        bIsEventInvolved = true;
                                        IEnumerable<XElement> AllEvents = objInputXML.Elements("Event");
                                        if (AllEvents != null)
                                        {
                                          
                                            foreach (XElement oEvent in AllEvents)
                                            {
                                                XmlDocument EventTemplate = new XmlDocument();
                                                int numberofclaims = oEvent.Descendants("Claim").Count();
                                                XmlDocument objEventDoc = ToXmlDocument(oEvent);
                                                XmlDocument objectTemp = XMLTemplate(oEvent);
                                                XmlNode CloneUpdated = objectTemp.CloneNode(true);
                                                XmlDocument xmlSerialization = new XmlDocument();
                                                EventTemplate.LoadXml(CloneUpdated.OuterXml);

                                                if (oEvent.Element("EventNumber").LastAttribute.Value != "")
                                                {
                                                    sSQL = "SELECT EVENT_ID,DTTM_RCD_LAST_UPD,UPDATED_BY_USER,ADDED_BY_USER,DTTM_RCD_ADDED FROM EVENT WHERE EVENT_NUMBER = '" + oEvent.Element("EventNumber").Attribute("value").Value + "'";

                                                    using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                    {
                                                        if (objreader.Read())
                                                        {
                                                            iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("EVENT_ID"), m_iClientId);
                                                            iUpdatedByUser = Conversion.ConvertObjToStr(objreader.GetValue("UPDATED_BY_USER"));
                                                            iAddedByUser = Conversion.ConvertObjToStr(objreader.GetValue("ADDED_BY_USER"));
                                                            iDateLastUpdated = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_LAST_UPD"));
                                                            iDateLastAdded = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_ADDED"));
                                                        }
                                                    }
                                                }
                                                else if (oEvent.Element("EventId").LastAttribute.Value != "")
                                                {
                                                    iRecordId = Conversion.CastToType<int>(oEvent.Element("EventId").Attribute("value").Value, out blnSuccess);
                                                }
                                                else
                                                {
                                                    throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                }

                                                XmlNodeList EventIds = EventTemplate.SelectNodes("//EventId");
                                                foreach (XmlNode EventId in EventIds)
                                                {
                                                    EventId.InnerXml = iRecordId.ToString();
                                                }

                                                if (EventTemplate.SelectSingleNode("//EventId").InnerXml != "-1")
                                                {
                                                if (EventTemplate.SelectSingleNode("//UpdatedByUser") != null)
                                                    EventTemplate.SelectSingleNode("//UpdatedByUser").InnerText = iUpdatedByUser;

                                                if (EventTemplate.SelectSingleNode("//AddedByUser") != null)
                                                    EventTemplate.SelectSingleNode("//AddedByUser").InnerText = iAddedByUser;

                                                if (EventTemplate.SelectSingleNode("//DttmRcdLastUpd") != null)
                                                    EventTemplate.SelectSingleNode("//DttmRcdLastUpd").InnerText = iDateLastUpdated.ToString();

                                                if (EventTemplate.SelectSingleNode("//DttmRcdAdded") != null)
                                                    EventTemplate.SelectSingleNode("//DttmRcdAdded").InnerText = iDateLastAdded.ToString();
                                                }

                                                XmlNodeList oClaimList = EventTemplate.SelectNodes("//Event/ClaimList/Claim");
                                                foreach (XmlNode claimnode in oClaimList)
                                                {
                                                    EventTemplate.SelectSingleNode("//ClaimList").RemoveChild(claimnode);
                                                }
                                                XmlNodeList oInterventionList = EventTemplate.SelectNodes("//Event/EventXInterventionList/EventXIntervention");
                                                foreach (XmlNode Intervention in oInterventionList)
                                                {
                                                    EventTemplate.SelectSingleNode("//EventXInterventionList").RemoveChild(Intervention);
                                                }
                                                XmlNodeList oDatedTextList = EventTemplate.SelectNodes("//Event/EventXDatedTextList/EventXDatedText");
                                                foreach (XmlNode DatedText in oDatedTextList)
                                                {
                                                    EventTemplate.SelectSingleNode("//EventXDatedTextList").RemoveChild(DatedText);
                                                }
                                                DataModelFactory objDMF = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword,m_iClientId); //mbahl3
                                                Event objEvent = (Event)objDMF.GetDataModelObject("Event", false);

                                                if (iRecordId > 0)
                                                {
                                                    objEvent.MoveTo(iRecordId);
                                                    xmlSerialization.LoadXml(" <Event><ReporterEntity /><Supplementals /></Event>");
                                                    sSerialize = objEvent.SerializeObject(xmlSerialization);
                                                    objSerialize = new XmlDocument();
                                                    objSerialize.LoadXml(sSerialize);
                                                    tempDoc = new XmlDocument();
                                                    tempDoc.LoadXml(EventTemplate.SelectSingleNode("//Instance").InnerXml);
                                                    tempDoc = ToXmlDocument(oEvent);
                                                    tempDoc.SelectSingleNode("//EventId").Attributes["value"].Value = iRecordId.ToString();
                                                  
                                                    foreach (XmlNode EventId in EventIds)
                                                    {
                                                        EventId.InnerXml = iRecordId.ToString();
                                                    }
                                                    XmlNodeList oClaimList1 = tempDoc.SelectNodes("//Event/ClaimList/Claim");
                                                    foreach (XmlNode claimnode in oClaimList1)
                                                    {
                                                        tempDoc.SelectSingleNode("//ClaimList").RemoveChild(claimnode);
                                                    }
                                                    XmlNodeList oInterventionList1 = tempDoc.SelectNodes("//Event/EventXInterventionList/EventXIntervention");
                                                    foreach (XmlNode Intervention in oInterventionList1)
                                                    {
                                                        tempDoc.SelectSingleNode("//EventXInterventionList").RemoveChild(Intervention);
                                                    }
                                                    XmlNodeList oDatedTextList1 = tempDoc.SelectNodes("//Event/EventXDatedTextList/EventXDatedText");
                                                    foreach (XmlNode DatedText in oDatedTextList1)
                                                    {
                                                        tempDoc.SelectSingleNode("//EventXDatedTextList").RemoveChild(DatedText);
                                                    }

                                                    tempDoc = UpdateRecords(tempDoc, objSerialize);

                                                    EventIds = null;

                                                    EventIds = tempDoc.SelectNodes("//EventId");
                                                    foreach (XmlNode EventId in EventIds)
                                                    {
                                                        EventId.InnerXml = iRecordId.ToString();
                                                    }
                                                    try
                                                    {
                                                        objEvent.PopulateObject(tempDoc);
                                                        objEvent.Save();
                                                        //prashbiharis: populate input Event list
                                                        inEventList.Add(objEvent.EventNumber);
                                                        //prashbiharis
                                                        if (newEventNumbers == null)
                                                            newEventNumbers = new ArrayList();

                                                        newEventNumbers.Add(objEvent.EventNumber);
                                                    }
                                                    //catch (RMAppException e)
                                                    //{
                                                    //    //prashbiharis: populate input Event list
                                                    //    inEventList.Add(objEvent.EventNumber);
                                                    //    //prashbiharis
                                                    //    throw e;
                                                    //}
                                                    catch (Exception e)
                                                    {
                                                        //prashbiharis: populate input Event list
                                                        inEventList.Add(objEvent.EventNumber);
                                                        //prashbiharis
                                                        throw new RMAppException(Globalization.GetString("ImportXml.EventSaveDataModel", m_iClientId));
                                                    }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        objEvent.PopulateObject(EventTemplate);
                                                        objEvent.Save();        // Save the Event
                                                        //prashbiharis: populate input Event list
                                                        inEventList.Add(objEvent.EventNumber);
                                                        //prashbiharis
                                                        if (newEventNumbers==null)
                                                           newEventNumbers = new ArrayList();
                                                        
                                                        newEventNumbers.Add(objEvent.EventNumber);
                                                    }
                                                    //catch (RMAppException e)
                                                    //{
                                                    //    //prashbiharis: populate input Event list
                                                    //    inEventList.Add(objEvent.EventNumber);
                                                    //    //prashbiharis
                                                    //    throw e;
                                                    //}
                                                    catch (Exception e)
                                                    {
                                                        //prashbiharis: populate input Event list
                                                        inEventList.Add(objEvent.EventNumber);
                                                        //prashbiharis
                                                        throw new RMAppException(Globalization.GetString("ImportXml.EventSaveDataModel", m_iClientId));
                                                    }
                                                }
                                                iEventId = iRecordId;
                                                iRecordId = 0;
                                                XmlNodeList AllClaims = objectTemp.SelectNodes("//ClaimList/Claim");
                                                for (int i = 0; i < AllClaims.Count; i++)
                                                {
                                                    XmlNode obClaim = AllClaims.Item(i);
                                                    XmlDocument ClaimDocument = new XmlDocument();
                                                    if (obClaim.SelectSingleNode("ClaimNumber").InnerXml != "")
                                                    {
                                                        sSQL = "SELECT CLAIM_ID, DTTM_RCD_LAST_UPD,UPDATED_BY_USER,ADDED_BY_USER,DTTM_RCD_ADDED FROM CLAIM WHERE CLAIM_NUMBER = '" + obClaim.SelectSingleNode("ClaimNumber").InnerXml + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("CLAIM_ID"), m_iClientId);
                                                                iUpdatedByUser = Conversion.ConvertObjToStr(objreader.GetValue("UPDATED_BY_USER"));
                                                                iAddedByUser = Conversion.ConvertObjToStr(objreader.GetValue("ADDED_BY_USER"));
                                                                iDateLastUpdated = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_LAST_UPD"));
                                                                iDateLastAdded = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_ADDED"));
                                                            }
                                                        }
                                                    }
                                                    else if (obClaim.SelectSingleNode("ClaimId").InnerXml != "")
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(obClaim.SelectSingleNode("ClaimId").InnerXml, out blnSuccess);
                                                    }
                                                    else
                                                    {
                                                        throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId));
                                                    }
                                                    ClaimDocument.LoadXml(obClaim.OuterXml);
                                                    XmlNodeList Claims = ClaimDocument.SelectNodes("//ClaimId");
                                                    foreach (XmlNode Claim in Claims)
                                                    {
                                                        Claim.InnerXml = iRecordId.ToString();
                                                    }

                                                    if (ClaimDocument.SelectSingleNode("//ClaimId").InnerXml != "-1")
                                                    {
                                                    if (ClaimDocument.SelectSingleNode("//Claim/UpdatedByUser") != null)
                                                        ClaimDocument.SelectSingleNode("//Claim/UpdatedByUser").InnerText = iUpdatedByUser;

                                                    if (ClaimDocument.SelectSingleNode("//Claim/AddedByUser") != null)
                                                        ClaimDocument.SelectSingleNode("//Claim/AddedByUser").InnerText = iAddedByUser;

                                                    if (ClaimDocument.SelectSingleNode("//Claim/DttmRcdLastUpd") != null)
                                                        ClaimDocument.SelectSingleNode("//Claim/DttmRcdLastUpd").InnerText = iDateLastUpdated.ToString();

                                                    if (ClaimDocument.SelectSingleNode("//Claim/DttmRcdAdded") != null)
                                                        ClaimDocument.SelectSingleNode("//Claim/DttmRcdAdded").InnerText = iDateLastAdded.ToString();

                                                    }

                                                    if ((ClaimDocument.SelectSingleNode("//EventId") != null)) //&& (objEvent.EventId != null))
                                                    {
                                                        ClaimDocument.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    Claim objectClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                                                    XmlDocument oPopulateClaim = new XmlDocument();

                                                    if (iRecordId > 0)
                                                    {
                                                        objectClaim.MoveTo(iRecordId);
                                                        switch (objectClaim.LineOfBusCode)
                                                        {
                                                            case 243:
                                                                xmlSerialization.LoadXml("<Claim><PrimaryPiEmployee><PiEntity> </PiEntity><PiXDiagHistList><PiXDiagHist></PiXDiagHist></PiXDiagHistList><PiXMMIHistList></PiXMMIHistList><PiXRestrictList></PiXRestrictList><PiXWorkLossList></PiXWorkLossList></PrimaryPiEmployee><CaseManagement><CaseMgrHistList></CaseMgrHistList></CaseManagement> <UnitStat/><Supplementals /><Jurisdictionals /></Claim> ");//Claim is WC
                                                                break;
                                                            case 242:
                                                                xmlSerialization.LoadXml("<Claim> <PrimaryClaimant /><Acord /><Supplementals /> <Jurisdictionals /></Claim>");//CLaim is VA
                                                                break;
                                                            case 844:
                                                                xmlSerialization.LoadXml(" <Claim><PrimaryPiEmployee><PiEntity></PiEntity><PiXDiagHistList><PiXDiagHist></PiXDiagHist></PiXDiagHistList><PiXMMIHistList><PiXMMIHist></PiXMMIHist></PiXMMIHistList><PiXRestrictList></PiXRestrictList><PiXWorkLossList></PiXWorkLossList></PrimaryPiEmployee><CaseManagement><CaseMgrHistList></CaseMgrHistList></CaseManagement><Supplementals /></Claim>");//Claim is Non-Occ
                                                                break;
                                                            case 241:
                                                                xmlSerialization.LoadXml("<Claim><CurrentAdjuster /><PrimaryClaimant /><Parent /><Jurisdictionals /><Acord /><Supplementals /></Claim>");//Claim is GC
                                                                break;
                                                            case 845:
                                                                xmlSerialization.LoadXml(" <Claim><CurrentAdjuster /> <PrimaryClaimant /><Parent /><Jurisdictionals /><PropertyClaim /><Acord /><Supplementals /></Claim>");//Claim is PC
                                                                break;
                                                        }

                                                        IEnumerable<XElement> objClaims = oEvent.XPathSelectElements("//ClaimList/Claim");
                                                        sSerialize = objectClaim.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();
                                                        tempDoc = ToXmlDocument(objClaims.ElementAt(i));
                                                        tempDoc.SelectSingleNode("//ClaimId").Attributes["value"].Value = iRecordId.ToString();
                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);
                                                        try
                                                        {
                                                            objectClaim.PopulateObject(tempDoc);
                                                            objectClaim.Save();
                                                            //prashbiharis
                                                            //Populate input claim list
                                                            inClaimList.Add(objectClaim.ClaimNumber, objEvent.EventNumber);
                                                            //populate savedClaimList
                                                            if (!savedClaimList.ContainsKey(objectClaim.ClaimNumber))
                                                                savedClaimList.Add(objectClaim.ClaimNumber, objEvent.EventNumber);
                                                            //prashbiharis
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    //prashbiharis : Populate input claim list
                                                        //    inClaimList.Add(objectClaim.ClaimNumber, objEvent.EventNumber);
                                                        //    //prashbiharis
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            //prashbiharis : Populate input claim list
                                                            inClaimList.Add(objectClaim.ClaimNumber, objEvent.EventNumber);
                                                            //prashbiharis
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SaveClaimDataModel", m_iClientId));
                                                        }
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            objectClaim.PopulateObject(ClaimDocument, false);
                                                            objectClaim.Save();
                                                            //prashbiharis 
                                                            //Populate input claim list
                                                            inClaimList.Add(objectClaim.ClaimNumber,objEvent.EventNumber);
                                                            // populate savedClaimList
                                                            if (!savedClaimList.ContainsKey(objectClaim.ClaimNumber))
                                                                savedClaimList.Add(objectClaim.ClaimNumber, objEvent.EventNumber);
                                                            //prashbiharis
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    //prashbiharis : Populate input claim list
                                                        //    inClaimList.Add(objectClaim.ClaimNumber,objEvent.EventNumber);
                                                        //    //prashbiharis
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            //prashbiharis : Populate input claim list
                                                            inClaimList.Add(objectClaim.ClaimNumber, objEvent.EventNumber);
                                                            //prashbiharis
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SaveClaimDataModel", m_iClientId));
                                                        }
                                                    }
                                                    if ((objectClaim.LineOfBusCode == objectClaim.Context.LocalCache.GetCodeId("WC", "LINE_OF_BUSINESS")) || 
                                                                (objectClaim.LineOfBusCode == objectClaim.Context.LocalCache.GetCodeId("DI", "LINE_OF_BUSINESS")))
                                                    {
                                                        int iClmtRowId = 0;
                                                        //if (ClaimDocument.SelectSingleNode("//PiRowId") != null)
                                                        //{
                                                        iClmtRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(objectClaim.Context.DbConn.ConnectionString, "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIM_ID = " + objectClaim.ClaimId), m_iClientId) ;
                                                        //}
                                                        //objectClaim.HookPrimaryPiEmployeeByPiRowId(iPiRowId);

                                                        if (iClmtRowId == 0)
                                                        {
                                                            Claimant objectClaimant = (Claimant)objDMF.GetDataModelObject("Claimant", false);
                                                            objectClaimant.ClaimantEid = objectClaim.PrimaryPiEmployee.PiEid;
                                                            objectClaimant.ClaimId = objectClaim.ClaimId;
                                                            objectClaimant.PrimaryClmntFlag = true;
                                                            //objectClaimant.IsXmlImport = true;
                                                            (objectClaimant as IDataModel).DataObjectState = DataObjectState.IsSaving;
                                                            objectClaimant.Save();
                                                            objectClaimant.Dispose();
                                                            objectClaimant = null;
                                                        }

                                                    }
                                                    iRecordId = 0;
                                                   //amitosh : 10/09/2013
                                                    obClaim=null;
                                                    ClaimDocument=null;
                                                    Claims=null;
                                                    objectClaim=null;
                                                    oPopulateClaim=null;
                                                }
                                                //Commented by Amitosh because EventXInterventionList has been removed from EventTemplate.Thus EventXIntervention was not saving
                                                //XmlNodeList oEventXInterventionList = EventTemplate.SelectNodes("//Event/EventXInterventionList/EventXIntervention");  
                                                XmlNodeList oEventXInterventionList = objectTemp.SelectNodes("//Event/EventXInterventionList/EventXIntervention");
                                                for (int i = 0; i < oEventXInterventionList.Count; i++)
                                                {
                                                    iRecordId = 0;
                                                    XmlNode oEventXIntervention = oEventXInterventionList.Item(i);


                                                    XmlDocument InterventionDoc = new XmlDocument();
                                                    InterventionDoc.LoadXml(oEventXIntervention.OuterXml);

                                                    if (InterventionDoc.SelectSingleNode("//EvIntRowId").InnerXml != "-1")
                                                    {
                                                        sSQL = "SELECT INTERVENT_ROW_ID,DTTM_RCD_LAST_UPD,UPDATED_BY_USER,ADDED_BY_USER,DTTM_RCD_ADDED FROM EVENT_X_INTERVENT WHERE EVENT_ID = '" + objEvent.EventId.ToString() + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("INTERVENT_ROW_ID"), m_iClientId);
                                                                iUpdatedByUser = Conversion.ConvertObjToStr(objreader.GetValue("UPDATED_BY_USER"));
                                                                iAddedByUser = Conversion.ConvertObjToStr(objreader.GetValue("ADDED_BY_USER"));
                                                                iDateLastUpdated = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_LAST_UPD"));
                                                                iDateLastAdded = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_ADDED"));
                                                            }
                                                        }
                                                        if (InterventionDoc.SelectSingleNode("//EventXIntervention/UpdatedByUser") != null)
                                                            InterventionDoc.SelectSingleNode("//EventXIntervention/UpdatedByUser").InnerXml = iUpdatedByUser;

                                                        if (InterventionDoc.SelectSingleNode("//EventXIntervention/AddedByUser") != null)
                                                            InterventionDoc.SelectSingleNode("//EventXIntervention/AddedByUser").InnerXml = iAddedByUser;

                                                        if (InterventionDoc.SelectSingleNode("//EventXIntervention/DttmRcdLastUpd") != null)
                                                            InterventionDoc.SelectSingleNode("//EventXIntervention/DttmRcdLastUpd").InnerXml = iDateLastUpdated.ToString();

                                                        if (InterventionDoc.SelectSingleNode("//EventXIntervention/DttmRcdAdded") != null)
                                                            InterventionDoc.SelectSingleNode("//EventXIntervention/DttmRcdAdded").InnerXml = iDateLastAdded.ToString();
                                                    }


                                                    if ((InterventionDoc.SelectSingleNode("//EventId") != null)) //&& (objEvent.EventId != null))
                                                    {
                                                        InterventionDoc.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    EventXIntervention objectIntervention = (EventXIntervention)objDMF.GetDataModelObject("EventXIntervention", false);

                                                    if (iRecordId > 0)
                                                    {
                                                        objectIntervention.MoveTo(iRecordId);
                                                        IEnumerable<XElement> objInterventionList = oEvent.XPathSelectElements("//Event/EventXInterventionList/EventXIntervention");
                                                        xmlSerialization.LoadXml("<EventXIntervention><Supplementals/></EventXIntervention>");
                                                        sSerialize = objectIntervention.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();
                                                        tempDoc = ToXmlDocument(objInterventionList.ElementAt(i));

                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);
                                                        try
                                                        {
                                                            objectIntervention.PopulateObject(tempDoc);
                                                            objectIntervention.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SaveEventXInterventionDataModel", m_iClientId));
                                                        }
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            objectIntervention.PopulateObject(InterventionDoc, false);
                                                            objectIntervention.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SaveEventXInterventionDataModel", m_iClientId));
                                                        }
                                                    }
                                                    iRecordId = 0;
                                               //amitosh : 10/09/2013      
                                            oEventXIntervention=null;
                                            InterventionDoc = null;
                                                }

                                                XmlNodeList oEventXDatedTextList = objectTemp.SelectNodes("//Event/EventXDatedTextList/EventXDatedText");
                                                for (int j = 0; j < oEventXDatedTextList.Count; j++)
                                                {
                                                    iRecordId = 0;
                                                    XmlNode oEventXDatedText = oEventXDatedTextList.Item(j);
                                                    XmlDocument DatedTextDoc = new XmlDocument();
                                                    DatedTextDoc.LoadXml(oEventXDatedText.OuterXml);

                                                    if (DatedTextDoc.SelectSingleNode("//EvDtRowId").InnerXml != "-1")
                                                    {
                                                        sSQL = "SELECT EV_DT_ROW_ID,DTTM_RCD_LAST_UPD,UPDATED_BY_USER,ADDED_BY_USER,DTTM_RCD_ADDED FROM EVENT_X_DATED_TEXT WHERE EVENT_ID = '" + objEvent.EventId.ToString() + "'";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("EV_DT_ROW_ID"), m_iClientId);
                                                                iUpdatedByUser = Conversion.ConvertObjToStr(objreader.GetValue("UPDATED_BY_USER"));
                                                                iAddedByUser = Conversion.ConvertObjToStr(objreader.GetValue("ADDED_BY_USER"));
                                                                iDateLastUpdated = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_LAST_UPD"));
                                                                iDateLastAdded = Conversion.ConvertObjToStr(objreader.GetValue("DTTM_RCD_ADDED"));
                                                            }
                                                        }
                                                        if(DatedTextDoc.SelectSingleNode("//EventXDatedText/UpdatedByUser") !=null)
                                                        DatedTextDoc.SelectSingleNode("//EventXDatedText/UpdatedByUser").InnerText = iUpdatedByUser;

                                                        if(DatedTextDoc.SelectSingleNode("//EventXDatedText/AddedByUser") !=null)
                                                        DatedTextDoc.SelectSingleNode("//EventXDatedText/AddedByUser").InnerText = iAddedByUser;

                                                        if(DatedTextDoc.SelectSingleNode("//EventXDatedText/DttmRcdLastUpd") !=null)
                                                        DatedTextDoc.SelectSingleNode("//EventXDatedText/DttmRcdLastUpd").InnerText = iDateLastUpdated.ToString();

                                                        if (DatedTextDoc.SelectSingleNode("//EventXDatedText/DttmRcdAdded") != null)
                                                        DatedTextDoc.SelectSingleNode("//EventXDatedText/DttmRcdAdded").InnerText = iDateLastAdded.ToString();
                                                    }


                                                    if ((DatedTextDoc.SelectSingleNode("//EventId") != null)) //&& (objEvent.EventId != null))
                                                    {
                                                        DatedTextDoc.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    EventXDatedText objectDatedText = (EventXDatedText)objDMF.GetDataModelObject("EventXDatedText", false);

                                                    if (iRecordId > 0)
                                                    {
                                                        objectDatedText.MoveTo(iRecordId);
                                                        IEnumerable<XElement> objDatedTextList = oEvent.XPathSelectElements("//Event/EventXDatedTextList/EventXDatedText");
                                                        xmlSerialization.LoadXml("<EventXDatedText/>");
                                                        sSerialize = objectDatedText.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();
                                                        tempDoc = ToXmlDocument(objDatedTextList.ElementAt(j));

                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);
                                                        try
                                                        {
                                                            objectDatedText.PopulateObject(tempDoc);
                                                            objectDatedText.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SaveEventXDatedTextDataModel", m_iClientId));
                                                        }
                                                        continue;

                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            objectDatedText.PopulateObject(DatedTextDoc, false);
                                                            objectDatedText.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SaveEventXDatedTextDataModel", m_iClientId));
                                                        }
                                                    }
                                                    iRecordId = 0;
													 //amitosh : 10/09/2013
                                                    oEventXDatedText=null;
                                                        DatedTextDoc=null;
                                                        objectDatedText = null;
                                                }
                                                #region piEmployee
                                                XmlNodeList oPiEmployeeList = objectTemp.SelectNodes("//Event/PiList/PiEmployee");
                                                for (int i = 0; i < oPiEmployeeList.Count; i++)
                                                {
                                                    XmlNode oPiEmployee = oPiEmployeeList.Item(i);

                                                    string sEnityId = string.Empty;
                                                    XmlDocument PiEmployeeDoc = new XmlDocument();
                                                    PiEmployeeDoc.LoadXml(oPiEmployee.OuterXml);
                                                    if (PiEmployeeDoc.SelectSingleNode("//PiRowId") == null)
                                                    {
                                                        sSQL = "SELECT ENTITY.ENTITY_ID ,PERSON_INVOLVED.PI_ROW_ID FROM PERSON_INVOLVED,ENTITY WHERE PERSON_INVOLVED.EVENT_ID = '" + objEvent.EventId.ToString() + "' AND PERSON_INVOLVED.PI_TYPE_CODE=237";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PI_ROW_ID"), m_iClientId);

                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(PiEmployeeDoc.SelectSingleNode("//PiRowId").InnerText, out blnSuccess);
                                                    }

                                                    if ((PiEmployeeDoc.SelectSingleNode("//EventId") != null)) //&& (objEvent.EventId != null))
                                                    {
                                                        PiEmployeeDoc.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    PiEmployee objectPiEmployee = (PiEmployee)objDMF.GetDataModelObject("PiEmployee", false);

                                                    if (iRecordId > 0)
                                                    {
                                                        objectPiEmployee.MoveTo(iRecordId);
                                                        IEnumerable<XElement> objpiEmpList = oEvent.XPathSelectElements("//Event/PiList/PiEmployee");

                                                        xmlSerialization.LoadXml(" <PiEmployee><PiEntity /><Supplementals /></PiEmployee>");
                                                        sSerialize = objectPiEmployee.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();
                                                        tempDoc = ToXmlDocument(objpiEmpList.ElementAt(i));
                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);

                                                        try
                                                        {
                                                            objectPiEmployee.PopulateObject(tempDoc);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                        objectPiEmployee.PiEntity.EntityTableId = objLocalCache.GetTableId("EMPLOYEES");
                                                        objectPiEmployee.PiTypeCode = objLocalCache.GetCodeId("E", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiEmployee.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiEmployeeDataModel", m_iClientId));
                                                        }
                                                        continue;

                                                    }

                                                    else
                                                    {
                                                        try
                                                        {
                                                            // objectPiEmployee.PopulateObject(PiEmployeeDoc, false);
                                                            //bsharma33 :  Mits # 32905

                                                            //if (objectPiEmployee.PiEid > 0)
                                                            //{
                                                            //    objectPiEmployee.PiEntity.EntityId = objectPiEmployee.PiEid;
                                                            //}
                                                            //End change

                                                            //mkaran2 : Start  
                                                            int PiEid = 0;
                                                            
                                                            if (PiEmployeeDoc.SelectSingleNode("//PiEid") != null)
                                                            {
                                                                PiEid = Conversion.CastToType<int>(PiEmployeeDoc.SelectSingleNode("//PiEid").Attributes["codeid"].Value, out blnSuccess);
                                                            }

                                                            if (PiEid > 0 )
                                                            {
                                                                
                                                               
                                                                Employee objectEmployee = (Employee)objDMF.GetDataModelObject("Employee", false);
                                                                objectEmployee.MoveTo(PiEid);
                                                                xmlSerialization.LoadXml(" <Employee> <EmployeeEntity> </EmployeeEntity> <Supplementals /> </Employee>");

                                                                sSerialize = objectEmployee.SerializeObject(xmlSerialization);
                                                                objSerialize = new XmlDocument();
                                                                objSerialize.LoadXml(sSerialize);

                                                                foreach (XmlNode element in PiEmployeeDoc.SelectSingleNode("//PiEmployee").ChildNodes)
                                                                {
                                                                     string Nodename = element.Name;

                                                                     if (Nodename.Equals("EmployeeNumber"))
                                                                     {
                                                                         continue;
                                                                     }
                                                                     if ((objSerialize.SelectSingleNode("//" + Nodename) != null)) 
                                                                     {
                                                                         if (element.Attributes["codeid"] != null)
                                                                         {
                                                                             XmlNode oXmlNodeList = objSerialize.SelectSingleNode("//" + Nodename);
                                                                             oXmlNodeList.Attributes["codeid"].Value = element.Attributes["codeid"].Value;
                                                                         }
                                                                         else
                                                                         element.InnerText = objSerialize.SelectSingleNode("//" + Nodename).InnerText;
                                                                     }

                                                                     foreach (XmlNode childEllement in element.ChildNodes)
                                                                        {
                                                                        string ChildNodename = childEllement.Name;

                                                                        if (ChildNodename !="#text" && (objSerialize.SelectSingleNode("//" + ChildNodename) != null)) 
                                                                        {
                                                                            if (childEllement.Attributes["codeid"] != null)
                                                                            {
                                                                                XmlNode oXmlNodeList = objSerialize.SelectSingleNode("//" + ChildNodename);
                                                                                oXmlNodeList.Attributes["codeid"].Value = childEllement.Attributes["codeid"].Value;
                                                                            }
                                                                            else
                                                                                childEllement.InnerText = objSerialize.SelectSingleNode("//" + ChildNodename).InnerText;
                                                                        }                                                                        
                                                                    }
                                                                }

                                                                objectPiEmployee.PiEntity.EntityId = PiEid;
                                                            }                                                           

                                                            objectPiEmployee.PopulateObject(PiEmployeeDoc, false); 
                                                            //mkaran2 : End
                                                            
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiEmployee.PiEntity.EntityTableId = objLocalCache.GetTableId("EMPLOYEES");
                                                                objectPiEmployee.PiTypeCode = objLocalCache.GetCodeId("E", "PERSON_INV_TYPE");
                                                            }

                                                            objectPiEmployee.Save();
                                                        }
                                                        catch (RMAppException e)
                                                        {
                                                            throw e;
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiEmployeeDataModel", m_iClientId));
                                                        }
                                                    }
                                                    iRecordId = 0;
													 //amitosh : 10/09/2013
                                                    oPiEmployee=null;
                                                    PiEmployeeDoc = null;
                                                }

                                                #endregion

                                                #region piMedical
                                                XmlNodeList oPiMedicalList = objectTemp.SelectNodes("//Event/PiList/PiMedicalStaff");
                                                for (int i = 0; i < oPiMedicalList.Count; i++)
                                                {
                                                    XmlNode oPiMedical = oPiMedicalList.Item(i);
                                                    XmlDocument PiMedical = new XmlDocument();
                                                    PiMedical.LoadXml(oPiMedical.OuterXml);
                                                    if (PiMedical.SelectSingleNode("//PiRowId") == null)
                                                    {
                                                        sSQL = "SELECT ENTITY.ENTITY_ID ,PERSON_INVOLVED.PI_ROW_ID FROM PERSON_INVOLVED,ENTITY WHERE PERSON_INVOLVED.EVENT_ID = '" + objEvent.EventId.ToString() + "' AND PERSON_INVOLVED.PI_TYPE_CODE=237";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PI_ROW_ID"), m_iClientId);

                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(PiMedical.SelectSingleNode("//PiRowId").InnerText, out blnSuccess);
                                                    }


                                                    if ((PiMedical.SelectSingleNode("//EventId") != null))
                                                    {
                                                        PiMedical.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    PiMedicalStaff objectPiMedical = (PiMedicalStaff)objDMF.GetDataModelObject("PiMedicalStaff", false);

                                                    if (iRecordId > 0)
                                                    {
                                                        objectPiMedical.MoveTo(iRecordId);
                                                        IEnumerable<XElement> objPiMedList = oEvent.XPathSelectElements("//Event/PiList/PiMedicalStaff");
                                                        xmlSerialization.LoadXml("<PiMedicalStaff><MedicalStaff><StaffEntity /></MedicalStaff><Supplementals /></PiMedicalStaff>");
                                                        sSerialize = objectPiMedical.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();
                                                        tempDoc = ToXmlDocument(objPiMedList.ElementAt(i));
                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);
                                                        try
                                                        {
                                                            objectPiMedical.PopulateObject(tempDoc);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiMedical.PiEntity.EntityTableId = objLocalCache.GetTableId("MEDICAL_STAFF");
                                                                objectPiMedical.PiTypeCode = objLocalCache.GetCodeId("MED", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiMedical.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiMedicalStaffDataModel", m_iClientId));
                                                        }
                                                        continue;


                                                    }

                                                    else
                                                    {
                                                        try
                                                        {
                                                            objectPiMedical.PopulateObject(PiMedical, false);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiMedical.PiEntity.EntityTableId = objLocalCache.GetTableId("MEDICAL_STAFF");
                                                                objectPiMedical.PiTypeCode = objLocalCache.GetCodeId("MED", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiMedical.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiMedicalStaffDataModel", m_iClientId));
                                                        }
                                                    }
                                                    iRecordId = 0;
													 //amitosh : 10/09/2013
                                                    oPiMedical=null;
                                                    PiMedical = null;
                                                }

                                                #endregion

                                                #region piWitness
                                                XmlNodeList oPiWitnessList = objectTemp.SelectNodes("//Event/PiList/PiWitness");
                                                for (int i = 0; i < oPiWitnessList.Count; i++)
                                                {

                                                    XmlNode oPiWitness = oPiWitnessList.Item(i);
                                                    XmlDocument PiWitnessDoc = new XmlDocument();
                                                    PiWitnessDoc.LoadXml(oPiWitness.OuterXml);

                                                    if (PiWitnessDoc.SelectSingleNode("//PiRowId") == null)
                                                    {
                                                        sSQL = "SELECT ENTITY.ENTITY_ID ,PERSON_INVOLVED.PI_ROW_ID FROM PERSON_INVOLVED,ENTITY WHERE PERSON_INVOLVED.EVENT_ID = '" + objEvent.EventId.ToString() + "' AND PERSON_INVOLVED.PI_TYPE_CODE=237";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PI_ROW_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(PiWitnessDoc.SelectSingleNode("//PiRowId").InnerText, out blnSuccess);
                                                    }


                                                    if ((PiWitnessDoc.SelectSingleNode("//EventId") != null)) //&& (objEvent.EventId != null))
                                                    {
                                                        PiWitnessDoc.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    PiWitness objectPiWitness = (PiWitness)objDMF.GetDataModelObject("PiWitness", false);


                                                    if (iRecordId > 0)
                                                    {

                                                        objectPiWitness.MoveTo(iRecordId);
                                                        IEnumerable<XElement> objPiWitnessList = oEvent.XPathSelectElements("//Event/PiList/PiWitness");
                                                        xmlSerialization.LoadXml("<PiWitness><PiEntity /><Supplementals /></PiWitness>");
                                                        sSerialize = objectPiWitness.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();

                                                        tempDoc = ToXmlDocument(objPiWitnessList.ElementAt(i));
                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);
                                                        try
                                                        {
                                                            objectPiWitness.PopulateObject(tempDoc);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiWitness.PiEntity.EntityTableId = objLocalCache.GetTableId("WITNESS");
                                                                objectPiWitness.PiTypeCode = objLocalCache.GetCodeId("W", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiWitness.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiWitnessDataModel", m_iClientId));
                                                        }
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            objectPiWitness.PopulateObject(PiWitnessDoc, false);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiWitness.PiEntity.EntityTableId = objLocalCache.GetTableId("WITNESS");
                                                                objectPiWitness.PiTypeCode = objLocalCache.GetCodeId("W", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiWitness.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiWitnessDataModel", m_iClientId));
                                                        }
                                                    }
                                                    iRecordId = 0;
                                                     //amitosh : 10/09/2013
                                                    oPiWitness=null;
                                                    PiWitnessDoc = null;
                                                }

                                                #endregion
                                                #region piPhysician
                                                XmlNodeList oPiPhysicianList = objectTemp.SelectNodes("//Event/PiList/PiPhysician");
                                                for (int i = 0; i < oPiPhysicianList.Count; i++)
                                                {
                                                    XmlNode oPiPhysician = oPiPhysicianList.Item(i);
                                                    XmlDocument PiPhysicianDoc = new XmlDocument();
                                                    PiPhysicianDoc.LoadXml(oPiPhysician.OuterXml);

                                                    if (PiPhysicianDoc.SelectSingleNode("//PiRowId") == null)
                                                    {
                                                        sSQL = "SELECT ENTITY.ENTITY_ID ,PERSON_INVOLVED.PI_ROW_ID FROM PERSON_INVOLVED,ENTITY WHERE PERSON_INVOLVED.EVENT_ID = '" + objEvent.EventId.ToString() + "' AND PERSON_INVOLVED.PI_TYPE_CODE=237";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PI_ROW_ID"), m_iClientId);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(PiPhysicianDoc.SelectSingleNode("//PiRowId").InnerText, out blnSuccess);
                                                    }

                                                    if ((PiPhysicianDoc.SelectSingleNode("//EventId") != null)) //&& (objEvent.EventId != null))
                                                    {
                                                        PiPhysicianDoc.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    PiPhysician objectPiPhysician = (PiPhysician)objDMF.GetDataModelObject("PiPhysician", false);

                                                    if (iRecordId > 0)
                                                    {

                                                        objectPiPhysician.MoveTo(iRecordId);
                                                        IEnumerable<XElement> objPiWitnessList = oEvent.XPathSelectElements("//Event/PiList/PiPhysician");
                                                        xmlSerialization.LoadXml(" <PiPhysician><Physician><PhysEntity></PhysEntity></Physician><PiEntity></PiEntity><Supplementals /></PiPhysician>");
                                                        sSerialize = objectPiPhysician.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();

                                                        tempDoc = ToXmlDocument(objPiWitnessList.ElementAt(i));
                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);

                                                        try
                                                        {
                                                            objectPiPhysician.PopulateObject(tempDoc);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiPhysician.PiEntity.EntityTableId = objLocalCache.GetTableId("PHYSICIANS");
                                                                objectPiPhysician.PiTypeCode = objLocalCache.GetCodeId("PHYS", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiPhysician.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiPhysicianDataModel", m_iClientId));
                                                        }
                                                        continue;

                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            objectPiPhysician.PopulateObject(PiPhysicianDoc, false);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiPhysician.PiEntity.EntityTableId = objLocalCache.GetTableId("PHYSICIANS");
                                                                objectPiPhysician.PiTypeCode = objLocalCache.GetCodeId("PHYS", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiPhysician.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiPhysicianDataModel", m_iClientId));
                                                        }
                                                    }

                                                    iRecordId = 0;
													 //amitosh : 10/09/2013
                                                    oPiPhysician=null;
                                                    PiPhysicianDoc = null;
                                                }

                                                #endregion

                                                #region piOther
                                                XmlNodeList oPiOtherList = objectTemp.SelectNodes("//Event/PiList/PiOther");
                                                for (int i = 0; i < oPiOtherList.Count; i++)
                                                {

                                                    XmlNode oPiOther = oPiOtherList.Item(i);
                                                    XmlDocument PiOtherDoc = new XmlDocument();
                                                    PiOtherDoc.LoadXml(oPiOther.OuterXml);

                                                    if (PiOtherDoc.SelectSingleNode("//PiRowId") == null)
                                                    {
                                                        sSQL = "SELECT ENTITY.ENTITY_ID ,PERSON_INVOLVED.PI_ROW_ID FROM PERSON_INVOLVED,ENTITY WHERE PERSON_INVOLVED.EVENT_ID = '" + objEvent.EventId.ToString() + "' AND PERSON_INVOLVED.PI_TYPE_CODE=237";
                                                        using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                                        {
                                                            if (objreader.Read())
                                                            {
                                                                iRecordId = Conversion.ConvertObjToInt(objreader.GetValue("PI_ROW_ID"), m_iClientId);
                                                            }
                                                        }

                                                    }
                                                    else
                                                    {
                                                        iRecordId = Conversion.CastToType<int>(PiOtherDoc.SelectSingleNode("//PiRowId").InnerText, out blnSuccess);
                                                    }

                                                    if ((PiOtherDoc.SelectSingleNode("//EventId") != null))
                                                    {
                                                        PiOtherDoc.SelectSingleNode("//EventId").InnerText = objEvent.EventId.ToString();
                                                    }
                                                    PiOther objectPiOther = (PiOther)objDMF.GetDataModelObject("PiOther", false);


                                                    if (iRecordId > 0)
                                                    {

                                                        objectPiOther.MoveTo(iRecordId);
                                                        IEnumerable<XElement> objPiWitnessList = oEvent.XPathSelectElements("//Event/PiList/PiOther");
                                                        xmlSerialization.LoadXml("<PiOther><PiEntity /><Supplementals /></PiOther>");
                                                        sSerialize = objectPiOther.SerializeObject(xmlSerialization);
                                                        objSerialize = new XmlDocument();
                                                        objSerialize.LoadXml(sSerialize);
                                                        tempDoc = new XmlDocument();

                                                        tempDoc = ToXmlDocument(objPiWitnessList.ElementAt(i));
                                                        tempDoc = UpdateRecords(tempDoc, objSerialize);
                                                        try
                                                        {
                                                            objectPiOther.PopulateObject(tempDoc);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiOther.PiEntity.EntityTableId = objLocalCache.GetTableId("OTHER_PEOPLE");
                                                                objectPiOther.PiTypeCode = objLocalCache.GetCodeId("O", "PERSON_INV_TYPE");
                                                            }
                                                            objectPiOther.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiOtherDataModel", m_iClientId));
                                                        }
                                                        continue;

                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            objectPiOther.PopulateObject(PiOtherDoc, false);
                                                            using (LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId))
                                                            {
                                                                objectPiOther.PiEntity.EntityTableId = objLocalCache.GetTableId("OTHER_PEOPLE");
                                                                objectPiOther.PiTypeCode = objLocalCache.GetCodeId("O", "PERSON_INV_TYPE");
                                                            }

                                                            objectPiOther.Save();
                                                        }
                                                        //catch (RMAppException e)
                                                        //{
                                                        //    throw e;
                                                        //}
                                                        catch (Exception e)
                                                        {
                                                            throw new RMAppException(Globalization.GetString("ImportXml.SavePiOtherDataModel", m_iClientId));
                                                        }
                                                    }

                                                    iRecordId = 0;
													 //amitosh : 10/09/2013
                                                    oPiOther=null;
                                                    PiOtherDoc = null;
                                                }
													 //amitosh : 10/09/2013
                                                EventTemplate = null;
                                                        objEventDoc     = null;         
	
                                                        objectTemp = null;
                                                        CloneUpdated = null;
                                                        xmlSerialization = null;
                                                        EventIds = null;
                                                        oClaimList = null;
                                                        oInterventionList = null;
                                                        oDatedTextList = null;
                                                        objDMF = null;
                                                        objEvent = null;
                                                        AllClaims = null;
                                                        oPiEmployeeList = null;
                                                        oPiWitnessList = null;
                                                        oPiMedicalList = null;
                                                        oPiPhysicianList = null;

                                                        oPiOtherList = null;
                                            }
                                        }
										 //amitosh : 10/09/2013
                                        AllEvents = null;
                                        break;
                                                #endregion

                                    default:
                                        throw new RMAppException(Globalization.GetString("ImportXML.XMLNotSupported", m_iClientId));
                                }
                            }
                            //Deb: Added catch block to update failed import xml
                            catch (Exception e)
                            {
                                string sTempDateCreated = Conversion.GetDate(DateCreated);
                                string sTempTimeCreated = Conversion.GetTime(TimeCreated);
                                string sSql = "UPDATE XML_IMPORT_HISTORY SET STATUS_FLAG=-1 WHERE XML_DOC_CREATE_DTTM='" + sTempDateCreated + sTempTimeCreated + " ' AND XML_DOC_CREATEDBY='" + CreatedBy + "'";
                                DbFactory.ExecuteNonQuery(m_sConnectionString, sSql);
                                throw e;
                            }
                            //Deb: Added catch block to update failed import xml
                        }
                        else
                        {
                            throw new RMAppException(Globalization.GetString("ImportXml.XMLNotValid", m_iClientId));
                        }
                    }
                }

                //prashbiharis: return xml if rval is true
                XmlDocument rdoc = new XmlDocument();
                if (rval)
                {
                    rdoc = (XmlDocument)GetOutputXML(savedClaimList, newEventNumbers, inEventList, inClaimList);
                    return rdoc.OuterXml.ToString();
                }
                // Added for MITS 29711 by bsharma33
                else
                {
                    if (bIsEventInvolved)
                    {
                        if (newEventNumbers!= null && newEventNumbers.Count > 0)
                    {
                        sFileText = "Event numbers created \n------------------------\n";
                        foreach (string eventno in newEventNumbers)
                        {
                            sFileText = sFileText + eventno + "\n";
                        }
                    }
                    else
                    {
                        sFileText = "No new events created.";
                    }
                    }
                    return print(sFileText);
                    // End additions by bsharma33
                }
                //prashbiharis                           
            }

            //catch (RMAppException e)
            //{
            //    throw e;
            //}
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

                if (objDmf != null)
                {
                    objDmf.Dispose();

                }

                if (objDataModel != null)
                {
                    objDataModel.Dispose();
                }
                if (newEventNumbers != null)
                    newEventNumbers = null;
            }
        }
        /// <summary>
        /// To return XML for saved Event and Claims
        /// </summary>
        /// <author>Praveen</author>
        private XmlDocument GetOutputXML(Dictionary<string, string> claimSavedList, ArrayList eventNumberList, ArrayList inEventList, Dictionary<string, string> inClaimList)
        {
            XmlDocument rXML = new XmlDocument();
            XmlElement root = rXML.CreateElement("ImportDetail");
            rXML.AppendChild(root);
            for (int i = 0; i < inEventList.Count; i++)
            {
                 XmlElement rEvent = rXML.CreateElement("Event");
                 rEvent.SetAttribute("EventNumber", inEventList[i].ToString());
                 if (eventNumberList.Contains(inEventList[i]))
                     rEvent.SetAttribute("Status", "Saved Suceesfully");
                 else
                 {
                     rEvent.SetAttribute("Status", "Failed");
                 }
                 root.AppendChild(rEvent);
                 foreach (var iClaim in inClaimList)
                     {
                         if (iClaim.Value.ToString().Equals(inEventList[i].ToString()) )
                         {
                             XmlElement rClaim = rXML.CreateElement("Claim");
                             rClaim.SetAttribute("ClaimNumber", iClaim.Key);
                             if (claimSavedList.ContainsKey(iClaim.Key))
                                 rClaim.SetAttribute("Status", "Saved Suceesfully");
                             else
                             {
                                 rClaim.SetAttribute("Status", "Failed");
                             }
                             rEvent.AppendChild(rClaim);
                         }
                     }

            }
            return rXML;        
        }
        /// <summary>
        /// To save Diary
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_oDiaryEntry"></param>
        /// <returns></returns>
        private bool SaveDiary(XElement p_oDiaryEntry)
        {
            bool bresult = false;
            WPA objDiary = null;
            bool bNotify = false;
            XmlDocument objDoc = null;
            bool blnSuccess = false;
            Hashtable objErrors = null; //srajindersin MITS 26319 04/20/2012

            try
            {

                //objDiary = new WPA(m_sConnectionString, m_sDSNName, m_sPassword, m_sLoginName);
                objDiary = new WPA(m_sConnectionString, m_sDSNName, m_sPassword, m_sLoginName, m_iClientId);

                if (p_oDiaryEntry.Element("notify") != null)
                {
                    bNotify = Conversion.CastToType<bool>(p_oDiaryEntry.Element("notify").Attribute("value").Value,out blnSuccess);
                }

                objDoc = CreateXMLForDiary(p_oDiaryEntry.ToString());
                //objDiary.SaveDiary(ref objDoc, m_sLoginName, m_sPassword, m_sDSNName, bNotify, "", "", m_oUserlogin.UserId); //srajindersin MITS 26319 04/20/2012
                objDiary.SaveDiary(ref objDoc, m_sLoginName, m_sPassword, m_sDSNName, bNotify, "", "", m_oUserlogin.UserId, ref objErrors); //srajindersin MITS 26319 04/20/2012
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDiary != null)
                {
                    objDiary = null;
                }
            }
            return bresult;

        }

        /// <summary>
        /// To create XML for Diary entry
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_sXmlIn"></param>
        /// <returns></returns>
        private XmlDocument CreateXMLForDiary(string p_sXmlIn)
        {
            XmlDocument objXmlDocument = null;
            //XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;
            XmlDocument objXmlIn = null;
            bool blnSuccess = false;
            try
            {
                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_sXmlIn);

                objXmlDocument = new XmlDocument();

                objRootElement = objXmlDocument.CreateElement("SaveDiary");
                //  objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                // objRootElement.AppendChild(objXmlElement);

                objXmlNode = objXmlDocument.CreateElement("EntryId");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryId") != null)
                {
                    if (Conversion.CastToType<int>(objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryId").Attributes["value"].Value, out blnSuccess) > 0)
                    {
                        objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryId").Attributes["value"].Value;
                    }
                    else
                    {
                        objXmlNode.InnerText = "0";
                    }
                }
                else
                {
                    objXmlNode.InnerText = "0";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;


                objXmlNode = objXmlDocument.CreateElement("TaskSubject");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryName") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryName").Attributes["value"].Value;

                }
                else
                {
                    throw new XmlException();
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AssigningUser");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//AssigningUser") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//AssigningUser").Attributes["value"].Value;
                }
                else
                {
                    throw new XmlException();
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AssignedUser");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//AssignedUser") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//AssignedUser").Attributes["value"].Value;
                }
                else
                {
                    throw new XmlException();
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("ActivityString");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//ActivityString") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//ActivityString").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("StatusOpen");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//StatusOpen") != null)
                {
                    if (Conversion.ConvertStrToBool(objXmlIn.SelectSingleNode("//WpaDiaryEntry//StatusOpen").Attributes["value"].Value))
                    {
                        objXmlNode.InnerText = "1";
                    }
                    else
                    {
                        objXmlNode.InnerText = "0";
                    }
                }
                else
                {
                    objXmlNode.InnerText = "1";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("TeTracked");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeTracked") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeTracked").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("TeTotalHours");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeTotalHours") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeTotalHours").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("TeExpenses");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeExpenses") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeExpenses").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("TeStartTime");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeStartTime") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeStartTime").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("TeEndTime");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeEndTime") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//TeEndTime").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }
                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("ResponseDate");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//ResponseDate") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//ResponseDate").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Response");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//Response") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//Response").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("TaskNotes");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryNotes") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryNotes").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("DueDate");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//CompleteDate") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//CompleteDate").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = System.DateTime.Now.Date.ToString("MM/dd/yyyy");
                }


                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;



                objXmlNode = objXmlDocument.CreateElement("CompleteTime");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//CompleteTime") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//CompleteTime").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = Conversion.GetTimeAMPM(System.DateTime.Now.TimeOfDay.ToString());

                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                //rsushilaggar MITS 31377 Date 02/16/2013
                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//CreateDate") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//CreateDate").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = System.DateTime.Now.Date.ToString("MM/dd/yyyy");
                }


                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                //End rsushilaggar

                

                objXmlNode = objXmlDocument.CreateElement("EstimateTime");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//EstimateTime") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//EstimateTime").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "0";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Priority");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//Priority") != null)
                {
                    switch (objXmlIn.SelectSingleNode("//WpaDiaryEntry//Priority").Attributes["value"].Value)
                    {
                        case "Optional":
                            objXmlNode.InnerText = "1";
                            break;
                        case "Important":
                            objXmlNode.InnerText = "2";
                            break;
                        case "Required":
                            objXmlNode.InnerText = "3";
                            break;
                        default:
                            objXmlNode.InnerText = "1";
                            break;
                    }
                }
                else
                {
                    objXmlNode.InnerText = "1";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AutoConfirm");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//AutoConfirm") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry/AutoConfirm").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "false";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("NotifyFlag");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//Notify") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry/Notify").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "false";
                }
                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("EmailNotifyFlag");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//EmailNotify") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry/EmailNotify").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "false";
                }

                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Action");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryId") != null)
                {
                    if (Conversion.CastToType<int>(objXmlIn.SelectSingleNode("//WpaDiaryEntry//EntryId").Attributes["value"].Value, out blnSuccess) > 0)
                    {
                        objXmlNode.InnerText = "Edit";
                    }
                    else
                    {
                        objXmlNode.InnerText = "Create";
                    }
                }
                else
                {
                    objXmlNode.InnerText = "Create";
                }
                objRootElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//AttachTable") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//AttachTable").Attributes["value"].Value;
                    objRootElement.AppendChild(objXmlNode);
                }


                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//AttachRecordId") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//AttachRecordId").Attributes["value"].Value;
                    objRootElement.AppendChild(objXmlNode);
                }
                else if (objXmlIn.SelectSingleNode("//WpaDiaryEntry//AttachRecordNumber") != null)
                {
                    string sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + objXmlIn.SelectSingleNode("//WpaDiaryEntry//AttachRecordNumber").Attributes["value"].Value + "'";
                    using (DbReader objreader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (objreader.Read())
                        {
                            objXmlNode.InnerText = Conversion.ConvertObjToStr(objreader.GetValue("CLAIM_ID"));
                            objRootElement.AppendChild(objXmlNode);
                        }
                        if (objreader.Read())
                        {
                            objXmlNode.InnerText = "";
                            objRootElement.AppendChild(objXmlNode);
                        }
                    }
                }
                else
                {
                    objXmlNode.InnerText = "";
                    objRootElement.AppendChild(objXmlNode);
                }

                //objXmlNode.InnerText = objXmlIn.SelectSingleNode("//WpaDiaryEntry//AttachRecordId").Attributes["value"].Value;
                //objRootElement.AppendChild(objXmlNode);


                objXmlNode = null;
            }
            catch (Exception e)
            {
                throw e;
            }
            return objXmlDocument;
        }

        /// <summary>
        /// To save the attachment 
        /// </summary>
        /// <Author>Amitosh</Author>
        /// <param name="p_oAttachment">Contains the Attachment Details</param>
        /// <param name="p_iRecordId">Attach RecordId </param>
        /// <param name="p_sAttachTable">Attach Table Name</param>
        /// <returns>true for success and false for errror</returns>
        private bool SaveAttachment(XElement p_oAttachment, int p_iRecordId, string p_sAttachTable, int iSid)
        {
            Boolean bSuccess = false;
            Stream sourceStream = null;
            DocumentManager objDocManager = null;
            string sNewFileName = string.Empty;
            XmlDocument objXmlIn = null;
            long lFilesize = 0;
            long lDocumentId = 0;
            int iFolderId = 0;
            bool bError = false;

            try
            {
                byte[] buffer = System.Convert.FromBase64String(p_oAttachment.Element("Base64Content").Attribute("value").Value);
                sourceStream = new MemoryStream(buffer);
                objDocManager = new DocumentManager(m_oUserlogin, m_iClientId);

                if (m_oUserlogin.DocumentPath.Length > 0)
                {
                    // use StoragePath set for the user
                    objDocManager.DestinationStoragePath = m_oUserlogin.DocumentPath;
                    //doc path entered at user level has to be file system
                    objDocManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else if (m_oUserlogin.objRiskmasterDatabase.GlobalDocPath.Length > 0)
                {
                    // use StoragePath set for the DSN
                    objDocManager.DestinationStoragePath = m_oUserlogin.objRiskmasterDatabase.GlobalDocPath;
                    //if no doc path entered at user level
                    objDocManager.DocumentStorageType = (StorageType)m_oUserlogin.objRiskmasterDatabase.DocPathType;
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("ImportXml.DocumentPath", m_iClientId));
                }

                string sXmlin = p_oAttachment.ToString();

                objDocManager.ConnectionString = m_sConnectionString;
                objDocManager.SecurityConnectionString = m_sSecConnectString;
                objDocManager.UserLoginName = m_sLoginName;

                objXmlIn = CreateXMLForAttachment(sXmlin, p_iRecordId, 0, p_sAttachTable, ref iFolderId);
                objDocManager.CreateNewDocument(objXmlIn.OuterXml, iSid, ref sNewFileName, ref lDocumentId);

                objXmlIn = null;

                lFilesize = sourceStream.Length;
                objXmlIn = CreateXMLForAttachment(sXmlin, p_iRecordId, lDocumentId, p_sAttachTable, ref iFolderId);
                objDocManager.AddLargeStream(objXmlIn.OuterXml, sourceStream, iSid, lFilesize.ToString());
                if (string.IsNullOrEmpty(sRecordsUploaded))
                {
                    sRecordsUploaded = lDocumentId.ToString();
                }
                else
                {
                    sRecordsUploaded = sRecordsUploaded + "," + lDocumentId.ToString();
                }
                if (string.IsNullOrEmpty(sFolderIds))
                {
                    sFolderIds = iFolderId.ToString();
                }
                else
                {
                    sFolderIds = sFolderIds + "," + iFolderId.ToString();
                }

                bSuccess = true;
            }
            catch (RMAppException p_objRMException)
            {
                bError = true;
                throw p_objRMException;
            }
            catch (XmlException p_objXMLException)
            {
                bError = true;
                throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.XMLException", m_iClientId), p_objXMLException);
            }
            catch (Exception p_objException)
            {
                bError = true;
                throw new RMAppException(Globalization.GetString("ImportXml.AddDocument.Exception", m_iClientId), p_objException);
            }
            finally
            {
                if (bError && !string.IsNullOrEmpty(sRecordsUploaded) && !string.IsNullOrEmpty(sFolderIds)) //akaur9 MITS 26590
                {
                    objDocManager.Delete(sRecordsUploaded, sFolderIds, iSid);
                }
                if (sourceStream != null)
                {
                    sourceStream.Close();
                }
                if (objDocManager != null)
                {
                    objDocManager = null;
                }
                if (objDocManager != null)
                {
                    objDocManager = null;
                }
                if (objXmlIn != null)
                {
                    objXmlIn = null;
                }
            }

            return bSuccess;
        }

        /// <summary>
        /// Function returns the correct XML Format 
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="p_sXmlIn">XML format recieved  from WebService.Contains only the Attachment details</param>
        /// <param name="p_iRecordId">Attach RecordId</param>
        /// <param name="p_lDocumentId">Document_Id.It is 0 for new record</param>
        /// <param name="p_sAttachTable">Attach Table Name</param>
        /// <returns>Required XML Format</returns>
        private XmlDocument CreateXMLForAttachment(string p_sXmlIn, int p_iRecordId, long p_lDocumentId, string p_sAttachTable, ref int iFolderId)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlNode objXmlNode = null;
            XmlDocument objXmlIn = null;
            bool blnSuccess = false;
            try
            {
                objXmlIn = new XmlDocument();
                objXmlIn.LoadXml(p_sXmlIn);

                objXmlDocument = new XmlDocument();

                objRootElement = objXmlDocument.CreateElement("data");
                objXmlElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                objRootElement.AppendChild(objXmlElement);

                objXmlNode = objXmlDocument.CreateElement("DocumentId");
                objXmlNode.InnerText = p_lDocumentId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;


                objXmlNode = objXmlDocument.CreateElement("FolderId");
                if (objXmlIn.SelectSingleNode("//Attachment//FolderId") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/FolderId").Attributes["value"].Value;

                }
                else
                {
                    objXmlNode.InnerText = "0";
                }

                iFolderId = Conversion.CastToType<int>(objXmlNode.InnerText, out blnSuccess);
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("CreateDate");
                objXmlNode.InnerText = Conversion.ToDbDateTime(System.DateTime.Now);
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Category");
                if (objXmlIn.SelectSingleNode("//Attachment//Category") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/Category").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }


                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;


                objXmlNode = objXmlDocument.CreateElement("Name");
                if (objXmlIn.SelectSingleNode("//Attachment//Title") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/Title").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Class");
                if (objXmlIn.SelectSingleNode("//Attachment//Class") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/Class").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Subject");
                if (objXmlIn.SelectSingleNode("//Attachment//Subject") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/Subject").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Type");
                if (objXmlIn.SelectSingleNode("//Attachment//Type") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/Type").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Notes");
                if (objXmlIn.SelectSingleNode("//Attachment//Notes") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/Notes").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("UserLoginName");
                objXmlNode.InnerText = m_sLoginName;
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FileName");
                if (objXmlIn.SelectSingleNode("//Attachment//FileName") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/FileName").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("FilePath");
                if (objXmlIn.SelectSingleNode("//Attachment//FilePath") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/FilePath").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("Keywords");
                if (objXmlIn.SelectSingleNode("//Attachment//Keywords") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/Keywords").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = "";
                }

                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachTable");
                if (objXmlIn.SelectSingleNode("//Attachment//AttachTable") != null)
                {
                    objXmlNode.InnerText = objXmlIn.SelectSingleNode("//Attachment/AttachTable").Attributes["value"].Value;
                }
                else
                {
                    objXmlNode.InnerText = p_sAttachTable.ToUpper();
                }


                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;

                objXmlNode = objXmlDocument.CreateElement("AttachRecordId");
                objXmlNode.InnerText = p_iRecordId.ToString();
                objXmlElement.AppendChild(objXmlNode);
                objXmlNode = null;
            }
            catch (Exception e)
            {
                throw e;
            }
            return objXmlDocument;
        }

        private void DeleteUploadedAttachments(string p_sRecordsUploaded, string p_sFolderIds, int p_iSid)
        {
            DocumentManager objDocManager = null;

            try
            {
                objDocManager = new DocumentManager(m_oUserlogin, m_iClientId);
                objDocManager.Delete(p_sRecordsUploaded, p_sFolderIds, p_iSid);
            }
            finally
            {
                if (objDocManager != null)
                {
                    objDocManager = null;
                }
            }
        }
        //private static XDocument DocumentToXDocument(XmlDocument doc)
        //{
        //    return XDocument.Parse(doc.OuterXml);
        //}
        public bool ValidateHeader(XElement Header)
        {
            //Deb: Assigned the value to gloabl variable
            DateCreated = Header.Element("DateCreated").Attribute("value").Value;
            TimeCreated = Header.Element("TimeCreated").Attribute("value").Value;
            CreatedBy = Header.Element("CreatedBy").Attribute("value").Value;
            //Deb: Assigned the value to gloabl variable
            return !ifXMLExists(DateCreated, TimeCreated, CreatedBy, m_sConnectionString);
        }
        private void AppendToXML(XmlDocument xmlDoc,string sParentPath, string sElement, string sNodeValue)
        {
            XmlElement objSupp = null;
            XmlNode objNode = null;
            objNode = xmlDoc.SelectSingleNode(sParentPath);
            objSupp = xmlDoc.CreateElement(sElement);

            if (string.IsNullOrEmpty(sNodeValue) != true)
                           objSupp.InnerXml = sNodeValue;

            objNode.AppendChild(objSupp);
            
        }

        private void AppendToXML(XmlDocument xmlDoc, string sParentPath, XElement sElement)
        {
            XmlElement objSupp = null;
             XmlNode objNode = null;
            objNode = xmlDoc.SelectSingleNode(sParentPath);
            objSupp = xmlDoc.CreateElement(sElement.Name.LocalName);
            if (sElement.Attribute("value") != null)
            {
                objSupp.SetAttribute(sElement.Attribute("value").Name.LocalName, sElement.Attribute("value").Value);
            }
            else if (sElement.Attribute("codeid") != null)
            {

                objSupp.SetAttribute(sElement.Attribute("codeid").Name.LocalName, sElement.Attribute("codeid").Value);
            
            }

            objNode.AppendChild(objSupp);

        }
        private XmlDocument XMLTemplate(XElement oParent)
        {
            XmlDocument objEventTemplate = new XmlDocument();//Load the Event Template
            string spath = RMConfigurator.BasePath + "/userdata/XMLTemplate/Event.xml";
            objEventTemplate.Load(spath);
            bool blnSuccess = false;
            
            IEnumerable<XNode> oChild = oParent.Nodes();
            string sCurrentNodeName = string.Empty;
            try
            {
                foreach (XElement oChildNode in oChild)
            {
                string Nodename = oChildNode.Name.LocalName;
                    if (int.Equals(Nodename.IndexOf("List"), -1))
                    {
                        try
                {
                    if (oChildNode.Attribute("codeid") != null)
                    {
                        XmlNode oXmlNodeList = objEventTemplate.SelectSingleNode("//" + Nodename);
                        oXmlNodeList.InnerXml = oChildNode.Value;
                        oXmlNodeList.Attributes["codeid"].Value = oChildNode.Attribute("codeid").Value;
                    }
                    else if (oChildNode.LastAttribute != null)
                    {
                        string NodeValue = oChildNode.LastAttribute.Value;
                        XmlNodeList oXmlNodeList = objEventTemplate.SelectNodes("//" + Nodename);
                        if (string.Equals(Nodename, "EVENTID",StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                            {
                                if (!m_oUserlogin.IsAllowedEx(RMO_EVENT, RMPermissions.RMO_UPDATE))
                                {
                                    //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_EVENT);
                                    throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                }
                            }
                                if (!m_oUserlogin.IsAllowedEx(RMO_EVENT, RMPermissions.RMO_CREATE))
                            {
                                //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_EVENT);
                                throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                            }
                        }
                        foreach (XmlNode oXmlNode in oXmlNodeList)
                        {
                            oXmlNode.InnerXml = NodeValue;
                        }
                    }
                    else
                    {
                        if (Nodename.Equals("Supplementals") == true)
                        {
                            AppendToXML(objEventTemplate, "Instance/Event", "Supplementals", string.Empty);
                        }
                        foreach (XElement oChild1 in oChildNode.Nodes())
                        {
                            if (oChild1.Attribute("codeid") != null)
                            {
                                XmlNode oXmlNodeList = objEventTemplate.SelectSingleNode("//" + Nodename +"/"+ oChild1.Name.LocalName);
                                if (oXmlNodeList != null)
                                {
                                    oXmlNodeList.InnerXml = oChild1.Value;
                                    oXmlNodeList.Attributes["codeid"].Value = oChild1.Attribute("codeid").Value;
                                }
                                        if (Nodename.Equals("Supplementals") == true) // mkaran2 : 32905
                                        {
                                            AppendToXML(objEventTemplate, "Instance/Event/Supplementals", oChild1.Name.LocalName, oChild1.Attribute("codeid").Value);
                            }
                                    }
                            else if (oChild1.LastAttribute != null)
                            {
                                string NodeValue = oChild1.LastAttribute.Value;
                                XmlNodeList oXmlNodeList = objEventTemplate.SelectNodes("//" + Nodename +"/"+ oChild1.Name.LocalName);
                                foreach (XmlNode oXmlNode in oXmlNodeList)
                                {
                                    oXmlNode.InnerXml = NodeValue;
                                }
                                if (Nodename.Equals("Supplementals") == true)
                                {
                                    AppendToXML(objEventTemplate, "Instance/Event/Supplementals", oChild1.Name.LocalName, NodeValue);
                                        }
                                    }
                                }

                            }
                        }
                        catch (RMAppException e)
                        {
                            throw e;
                        }
                        catch (Exception e)
                        {

                            throw new RMAppException(String.Format(Globalization.GetString("ImportXML.EventTemplateGeneration", m_iClientId), nodeName));
                            //"EVENT" + Nodename
                        }
                }
                else
                {
                    int CommitedCountValue = Conversion.CastToType<int>(oChildNode.LastAttribute.Value, out blnSuccess);
                    if (CommitedCountValue > 0)
                    {
                        XmlDocument objChildTemplate = new XmlDocument();
                        IEnumerable<XNode> oGenTwoChildren = oChildNode.Nodes();
                        string Childname = "";
                        switch (Nodename)
                        {
                            case "EventXInterventionList":
                                string sInterventionpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/EventXIntervention.xml"; //Load the EventXIntervention Template
                                objChildTemplate.Load(sInterventionpath);
                                XmlNode ChildNode;
                                    try
                                   {
                                        foreach (XElement oIntervention in oGenTwoChildren)
                                {
                                    Childname = oIntervention.Name.LocalName;

                                    //XmlDocument objClaimChild = new XmlDocument();
                                    IEnumerable<XNode> oC = oIntervention.Nodes();
                                    if (int.Equals(oC.Count(), 0))
                                    {
                                        XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                        string NodeValue = oIntervention.LastAttribute.Value;
                                        if (string.Equals(Childname, "EVINTROWID", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                            {
                                                if (!m_oUserlogin.IsAllowedEx(RMO_Intervention, RMPermissions.RMO_UPDATE))
                                                {
                                                    //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_Intervention);
                                                    throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                }
                                            }
                                            if (!m_oUserlogin.IsAllowedEx(RMO_Intervention, RMPermissions.RMO_CREATE))
                                            {
                                                //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_Intervention);
                                                throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                            }
                                        }
                                        foreach (XmlNode oXmlNode in oXmlNodeList)
                                        {
                                            oXmlNode.InnerXml = NodeValue;
                                        }
                                    }
                                    else
                                    {
                                        foreach (XElement xele in oC)
                                        {
                                            Childname = xele.Name.LocalName;
                                            XmlNode oXmlNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                            string NodeValue = string.Empty;
                                            if (xele.LastAttribute != null)
                                                NodeValue = xele.LastAttribute.Value;
                                            if (oXmlNode != null)
                                            {
                                                if (xele.Attribute("codeid") != null)
                                                        {
                                                            XmlNode oNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                            oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                        }
                                                        else
                                                        {
                                                            ////string NodeValue1 = xele.LastAttribute.Value;
                                                            oXmlNode.InnerXml = NodeValue;
                                                        }
                                            }
                                         
                                        }
                                    }
                                    ChildNode = objChildTemplate.SelectSingleNode("//EventXIntervention");
                                    XmlNode InterventionListPath = objEventTemplate.SelectSingleNode("//EventXInterventionList");
                                    InterventionListPath.AppendChild(InterventionListPath.OwnerDocument.ImportNode(ChildNode, true));
                                }
                                    }
                                    catch (RMAppException e)
                                    {
                                        throw e;
                                    }
                                    catch (Exception e)
                                    {
                                        throw new RMAppException(String.Format(Globalization.GetString("ImportXML.EventXInterventionTemplateGeneration", m_iClientId), Childname));
                                        
                                        //"EventXIntervention" + Childname
                                    }
                                    break;
                                
                            case "ClaimList":

                                    int iSecurityId = 0;
                                    int iClaimantSecurityId = 0;
                                    int iDefendantSecurityId = 0;
                                    int iAdjusterSecurityId = 0;
                                    int iLitigationSecurityId = 0;                                   
                                //igupta3   MITS: 32002 Changes Starts
                                int iSubrogationSecurityId = 0;
                                int iUnitSecurityId = 0;
                                //igupta3   MITS: 32002 Changes ends
                                objEventTemplate.SelectSingleNode("//ClaimList").Attributes["committedcount"].Value = oChildNode.Attribute("committedcount").Value;
                                foreach (XElement oGenTwoChild in oGenTwoChildren)
                                {
                                        try
                                        {
                                            XmlDocument objClaimTemplate = new XmlDocument();
                                    XmlNode ClaimNode;
                                    XmlNode ClaimantNode;
                            
                                    // switch (Conversion.CastToType<int>(oGenTwoChild.Element("LineOfBusCode").Attribute("value").Value))
                                    switch (Conversion.CastToType<int>(oGenTwoChild.Element("LineOfBusCode").Attribute("codeid").Value, out blnSuccess))
                                    {
                                        case 241:
                                            string sgcpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/GeneralClaim.xml"; //Load the GeneralClaim Template
                                            objClaimTemplate.Load(sgcpath);
                                            iSecurityId = RMO_GCSecurityId;
                                            iClaimantSecurityId = RMO_GC_CLAIMANTS;
                                            iDefendantSecurityId =RMO_GC_DEFENDANT;
                                            iAdjusterSecurityId =RMO_GC_ADJUSTER;
                                            iLitigationSecurityId = RMO_GC_LITIGATION;
                                            iSubrogationSecurityId = RMO_GC_SUBROGATION;//igupta3 MITS:32002
                                            break;
                                        case 243:
                                            string wcpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/WorkersCompensation.xml"; //Load the WC Template
                                            objClaimTemplate.Load(wcpath);
                                                iDefendantSecurityId =RMO_WC_DEFENDANT;
                                            iAdjusterSecurityId =RMO_WC_ADJUSTER;
                                            iLitigationSecurityId = RMO_WC_LITIGATION;
                                            iSubrogationSecurityId = RMO_WC_SUBROGATION;//igupta3 MITS:32002
                                            break;
                                        case 242:
                                            string vacpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/VehicleClaim.xml"; //Load the VA Template
                                            objClaimTemplate.Load(vacpath);
                                            iSecurityId = RMO_VASecurityId;
                                            iClaimantSecurityId = RMO_VA_CLAIMANTS;
                                              iDefendantSecurityId =RMO_VA_DEFENDANT;
                                            iAdjusterSecurityId =RMO_VA_ADJUSTER;
                                            iLitigationSecurityId = RMO_VA_LITIGATION;
                                            iSubrogationSecurityId = RMO_VA_SUBROGATION;//igupta3 MITS:32002
                                            iUnitSecurityId = RMO_VA_UNIT;//igupta3 MITS:32002
                                            break;
                                        case 844:
                                            string NonOcccpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/NonOcc.xml"; //Load the NonOcc Template
                                            objClaimTemplate.Load(NonOcccpath);
                                            iSecurityId = RMO_DISecurityId;
                                                iDefendantSecurityId =RMO_DI_DEFENDANT;
                                            iAdjusterSecurityId =RMO_DI_ADJUSTER;
                                            iLitigationSecurityId = RMO_DI_LITIGATION;
                                            iSubrogationSecurityId = RMO_DI_SUBROGATION;//igupta3 MITS:32002
                                            break;
                                        case 845:
                                            string PCpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/PropertyClaim.xml"; //Load the PC Template
                                            objClaimTemplate.Load(PCpath);
                                            iSecurityId = RMO_PCSecurityId;
                                            iClaimantSecurityId = RMO_PC_CLAIMANT;
                                            iDefendantSecurityId =RMO_PC_DEFENDANT;
                                            iAdjusterSecurityId =RMO_PC_ADJUSTER;
                                            iLitigationSecurityId = RMO_PC_LITIGATION;
                                            iSubrogationSecurityId = RMO_PC_SUBROGATION;//igupta3 MITS:32002
                                            break;
                                    }
                                    IEnumerable<XNode> oClaims = oGenTwoChild.Nodes();
                                    foreach (XElement ClaimData in oClaims)
                                    {
                                        Childname = ClaimData.Name.LocalName;

                                        XmlDocument objClaimChild = new XmlDocument();
                                        if (int.Equals(Childname.IndexOf("List"), -1))
                                        {

                                            IEnumerable<XNode> oC = ClaimData.Nodes();
                                            if (oC.Count() == 0)
                                            {
                                                XmlNode oXmlNode = objClaimTemplate.SelectSingleNode("//" + Childname);
                                                string NodeValue = ClaimData.LastAttribute.Value;
                                                if (string.Equals(Childname, "CLAIMID",StringComparison.InvariantCultureIgnoreCase))
                                                {

                                                    if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                    {
                                                        if (!m_oUserlogin.IsAllowedEx(iSecurityId, RMPermissions.RMO_UPDATE))
                                                        {
                                                            //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iSecurityId);
                                                            throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                        }
                                                    }
                                                    if (!m_oUserlogin.IsAllowedEx(iSecurityId, RMPermissions.RMO_CREATE))
                                                    {
                                                        //throw new PermissionViolationException(RMPermissions.RMO_CREATE, iSecurityId);
                                                        throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                    }
                                                }
                                              if (oXmlNode != null)
                                                        {
                                                            if (oXmlNode.Attributes["codeid"] != null)
                                                            {
                                                                oXmlNode.Attributes["codeid"].Value = ClaimData.Attribute("codeid").Value;
                                                            }
                                                            else
                                                            {
                                                                ////string NodeValue1 = xele.LastAttribute.Value;
                                                                oXmlNode.InnerXml = NodeValue;

                                                            }
                                                        }
                                            }
                                            else
                                            {
                                                if (ClaimData.Name.LocalName.Equals("Supplementals") == true)
                                                {
                                                    AppendToXML(objClaimTemplate, "Claim", "Supplementals", string.Empty);
                                                }
                                                foreach (XElement xele in oC)
                                                {
                                                    Childname = xele.Name.LocalName;
                                                    XmlNode oXmlNode = objClaimTemplate.SelectSingleNode("//" + Childname);
                                                            if (oXmlNode !=null && oXmlNode.ChildNodes.Count == 0) //mkaran2 : 32905 : oXmlNode Comes null incase of supp
                                                            {
                                                    string NodeValue = string.Empty;
                                                    if (xele.LastAttribute != null)
                                                        NodeValue = xele.LastAttribute.Value;
                                                    if (oXmlNode != null)
                                                    {
                                                        if (xele.Attribute("codeid") != null)
                                                        {
                                                            XmlNode oNode = objClaimTemplate.SelectSingleNode("//" + Childname);
                                                            oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                        }
                                                        else
                                                        {
                                                            ////string NodeValue1 = xele.LastAttribute.Value;
                                                            oXmlNode.InnerXml = NodeValue;
                                                                    }
                                                                }
                                                                if (ClaimData.Name.LocalName.Equals("Supplementals") == true)
                                                                {
                                                                    AppendToXML(objClaimTemplate, "Claim/Supplementals", Childname, NodeValue);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (xele.Parent.Name.LocalName.Equals("Supplementals") == true) //mkaran2 : MITS 32905
                                                                {
                                                                    AppendToXML(objClaimTemplate, "Claim/Supplementals", Childname, xele.LastAttribute.Value);
                                                                }
                                                                foreach (XElement xele1 in xele.Elements())
                                                                {
                                                                    Childname = xele1.Name.LocalName;
                                                                    XmlNode oXmlNode1 = oXmlNode.SelectSingleNode("//" + Childname);
                                                                    string NodeValue = string.Empty;
                                                                    if (xele1.LastAttribute != null)
                                                                        NodeValue = xele1.LastAttribute.Value;
                                                                    if (oXmlNode1 != null)
                                                                    {
                                                                        if (xele1.Attribute("codeid") != null)
                                                                        {
                                                                            XmlNode oNode = oXmlNode.SelectSingleNode("//" + Childname);
                                                                            oNode.Attributes["codeid"].Value = xele1.Attribute("codeid").Value;
                                                                        }
                                                                        else
                                                                        {
                                                                            ////string NodeValue1 = xele.LastAttribute.Value;
                                                                            oXmlNode1.InnerXml = NodeValue;
                                                        }
                                                    }
                                                    if (ClaimData.Name.LocalName.Equals("Supplementals") == true)
                                                    {
                                                        AppendToXML(objClaimTemplate, "Claim/Supplementals", Childname, NodeValue);
                                                    }
                                                }
                                            }
                                        }
                                                    }
                                                }
                                        else
                                        {
                                            switch (Childname)
                                            {
                                                case "ClaimantList":
                                                    //IEnumerable<XNode> oClaimant = ClaimData.Nodes();
                                                    string sClaimantpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/Claimant.xml"; //Load the ClaimantList Template
                                                    objClaimChild.Load(sClaimantpath);
                                           
                                                    break;
                                                case "DefendantList":
                                                    //IEnumerable<XNode> oDefendant = ClaimData.Nodes();
                                                    string sDefendantpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/Defendant.xml"; //Load the DefendantList Template
                                                    objClaimChild.Load(sDefendantpath);
                                                    break;
                                                case "LitigationList":
                                                    //IEnumerable<XNode> oLitigation = ClaimData.Nodes();
                                                    string sLitigationpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/Litigation.xml"; //Load the LitigationList Template
                                                    objClaimChild.Load(sLitigationpath);
                                                    break;
                                                case "SubrogationList":
                                                    //IEnumerable<XNode> oSubrogation = ClaimData.Nodes();
                                                    string sSubrogationpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/Subrogation.xml"; //Load the SubrogationList Template
                                                    objClaimChild.Load(sSubrogationpath);
                                                    break;
                                                case "PropertyLossList":
                                                    //IEnumerable<XNode> oPropertyLoss = ClaimData.Nodes();
                                                    string sPropertyLosspath = RMConfigurator.BasePath + "/userdata/XMLTemplate/PropertyLoss.xml"; //Load the PropertyLossList Template
                                                    objClaimChild.Load(sPropertyLosspath);
                                                    break;
                                                case "UnitList":
                                                    //IEnumerable<XNode> oUnitList = ClaimData.Nodes();
                                                    string sUnitList = RMConfigurator.BasePath + "/userdata/XMLTemplate/UnitList.xml"; //Load the UnitList Template
                                                    objClaimChild.Load(sUnitList);
                                                    break;
                                                case "LiabilityLossList":
                                                    //IEnumerable<XNode> oLiabilityLossList = ClaimData.Nodes();
                                                    string sLiabilityLossList = RMConfigurator.BasePath + "/userdata/XMLTemplate/LiabilityLoss.xml"; //Load the LiabilityLossList Template
                                                    objClaimChild.Load(sLiabilityLossList);
                                                    break;
                                                case "ArbitrationList":
                                                    //IEnumerable<XNode> oArbitrationList = ClaimData.Nodes();
                                                    string sArbitrationList = RMConfigurator.BasePath + "/userdata/XMLTemplate/ArbitrationList.xml"; //Load the ArbitrationList Template
                                                    objClaimChild.Load(sArbitrationList);
                                                    break;
                                                case "AdjusterList":
                                                    //IEnumerable<XNode> oAdjusterList = ClaimData.Nodes();
                                                    string sAdjusterList = RMConfigurator.BasePath + "/userdata/XMLTemplate/AdjusterList.xml"; //Load the AdjusterList Template
                                                    objClaimChild.Load(sAdjusterList);
                                                    break;
                                            }
                                            IEnumerable<XNode> oChildrenofClaim = ClaimData.Nodes();
                                            objClaimTemplate.SelectSingleNode("//" + Childname).Attributes["committedcount"].Value = ClaimData.Attribute("committedcount").Value;
                                            string GrandChildInfo = "";
                                            foreach (XElement ChildData in oChildrenofClaim)
                                            {
                                                IEnumerable<XNode> oGrandChildrenNodes = ChildData.Nodes();
                                                foreach (XElement oGrandChildnode in oGrandChildrenNodes)
                                                {
                                                    GrandChildInfo = oGrandChildnode.Name.LocalName;
                                                    int NoofChildren = oGrandChildnode.Nodes().Count();
                                                    if (NoofChildren == 0)
                                                    {
                                                                try
                                                                {
                                                        //igupta3 Mits: 32258 Changes starts
                                                        if (oGrandChildnode.Attribute("codeid") != null)
                                                        {
                                                            XmlNode oXmlNodeList = objClaimChild.SelectSingleNode("//" + oGrandChildnode.Name.LocalName);
                                                            if(oXmlNodeList!=null)
                                                            oXmlNodeList.Attributes["codeid"].Value = oGrandChildnode.Attribute("codeid").Value;
                                                        }
                                                        //igupta3 Mits: 32258 Changes ends
                                                        else
                                                        {
                                                            XmlNode oXmlNode = objClaimChild.SelectSingleNode("//" + GrandChildInfo);
                                                            string NodeValue = oGrandChildnode.LastAttribute.Value;
                                                            switch (GrandChildInfo.ToUpper())
                                                            {
                                                                case "CLAIMANTROWID":

                                                                    if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                    {
                                                                        if (!m_oUserlogin.IsAllowedEx(iClaimantSecurityId, RMPermissions.RMO_UPDATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iClaimantSecurityId);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                                        }
                                                                    }
                                                                    if (!m_oUserlogin.IsAllowedEx(iClaimantSecurityId, RMPermissions.RMO_CREATE))
                                                                    {
                                                                        //throw new PermissionViolationException(RMPermissions.RMO_CREATE, iClaimantSecurityId);
                                                                        throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                    }
                                                                    break;
                                                                case "DEFENDANTROWID":

                                                                    if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                    {
                                                                        if (!m_oUserlogin.IsAllowedEx(iDefendantSecurityId, RMPermissions.RMO_UPDATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iDefendantSecurityId);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                                        }
                                                                    }
                                                                    if (!m_oUserlogin.IsAllowedEx(iDefendantSecurityId, RMPermissions.RMO_CREATE))
                                                                    {
                                                                        //throw new PermissionViolationException(RMPermissions.RMO_CREATE, iDefendantSecurityId);
                                                                        throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                    }
                                                                    break;
                                                                case "ADJROWID":

                                                                    if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                    {
                                                                        if (!m_oUserlogin.IsAllowedEx(iAdjusterSecurityId, RMPermissions.RMO_UPDATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iAdjusterSecurityId);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                                        }
                                                                    }
                                                                    if (!m_oUserlogin.IsAllowedEx(iAdjusterSecurityId, RMPermissions.RMO_CREATE))
                                                                    {
                                                                        //   throw new PermissionViolationException(RMPermissions.RMO_CREATE, iAdjusterSecurityId);
                                                                        throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                    }
                                                                    break;
                                                                case "LITIGATIONROWID":

                                                                    if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                    {
                                                                        if (!m_oUserlogin.IsAllowedEx(iLitigationSecurityId, RMPermissions.RMO_UPDATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iLitigationSecurityId);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                                        }
                                                                    }
                                                                    if (!m_oUserlogin.IsAllowedEx(iLitigationSecurityId, RMPermissions.RMO_CREATE))
                                                                    {
                                                                        //throw new PermissionViolationException(RMPermissions.RMO_CREATE, iLitigationSecurityId);
                                                                        throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                    }
                                                                    break;
                                                                //igupta3   MITS:32002 Changes Starts
                                                                case "SUBROGATIONROWID":

                                                                    if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                    {
                                                                        if (!m_oUserlogin.IsAllowedEx(iSubrogationSecurityId, RMPermissions.RMO_UPDATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iSubrogationSecurityId);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                                        }
                                                                    }
                                                                    if (!m_oUserlogin.IsAllowedEx(iSubrogationSecurityId, RMPermissions.RMO_CREATE))
                                                                    {
                                                                        //throw new PermissionViolationException(RMPermissions.RMO_CREATE, iSubrogationSecurityId);
                                                                        throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                    }
                                                                    break;

                                                                case "UNITROWID":

                                                                    if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                    {
                                                                        if (!m_oUserlogin.IsAllowedEx(iUnitSecurityId, RMPermissions.RMO_UPDATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, iUnitSecurityId);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                                        }
                                                                    }
                                                                    if (!m_oUserlogin.IsAllowedEx(iUnitSecurityId, RMPermissions.RMO_CREATE))
                                                                    {
                                                                        //throw new PermissionViolationException(RMPermissions.RMO_CREATE, iUnitSecurityId);
                                                                        throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                    }
                                                                    break;
                                                                //igupta3   MITS:32002 Changes Ends
                                                            }
                                                            if (oXmlNode != null)
                                                                oXmlNode.InnerXml = NodeValue;
                                                        }
                                                    }
                                                                catch (RMAppException e)
                                                                {
                                                                    throw e;
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    throw new RMAppException(String.Format(Globalization.GetString("ImportXML.ClaimChildrenTemplateGeneration", m_iClientId), GrandChildInfo));
                                                                    
                                                                    //"CLAIM CHILD" + GrandChildInfo;

                                                                }

                                                            }
                                                            else
                                                            {
                                                                try
                                                    {
                                                        IEnumerable<XNode> oClaimantsChildren = oGrandChildnode.Nodes();
                                                        if (oGrandChildnode.Name.LocalName.Equals("Supplementals") == true && objClaimChild.SelectSingleNode("//Supplementals") == null)
                                                        {
                                                            AppendToXML(objClaimChild, oGrandChildnode.Parent.Name.LocalName, oGrandChildnode.Name.LocalName, string.Empty);
                                                        }
                                                        foreach (XElement oClaimantsChild in oClaimantsChildren)
                                                        {
                                                            //igupta3 Mits: 32258 Changes starts
                                                            if (oClaimantsChild.Attribute("codeid") != null)
                                                            {
                                                                XmlNode oXmlNodeList = objClaimChild.SelectSingleNode("//" + oGrandChildnode.Name.LocalName + "/" + oClaimantsChild.Name.LocalName);
                                                                if(oXmlNodeList!=null)
                                                                oXmlNodeList.Attributes["codeid"].Value = oClaimantsChild.Attribute("codeid").Value;

                                                            }
                                                            //igupta3 Mits: 32258 Changes ends
                                                            else
                                                            {
                                                                XmlNode oXmlNode = objClaimChild.SelectSingleNode("//" + oGrandChildnode.Name.LocalName + "/" + oClaimantsChild.Name.LocalName); //igupta3 Mits: 32258
                                                                string NodeValue = oClaimantsChild.LastAttribute.Value;
                                                                if (oXmlNode != null)
                                                                    oXmlNode.InnerXml = NodeValue;
                                                                if (oGrandChildnode.Name.LocalName.Equals("Supplementals") == true)
                                                                {
                                                                    AppendToXML(objClaimChild, oGrandChildnode.Parent.Name.LocalName + "/Supplementals", oClaimantsChild.Name.LocalName, NodeValue);
                                                                }
                                                            }
                                                                    }
                                                                }
                                                                catch (RMAppException e)
                                                                {
                                                                    throw e;
                                                                }
                                                                catch (Exception e)
                                                                {
                                                                    throw new RMAppException(String.Format(Globalization.GetString("ImportXML.ClaimChildrenTemplateGeneration", m_iClientId), oGrandChildnode.Name.LocalName));
                                                                    
                                                                    
                                                                    //"CLAIM CHILD"
                                                                    //     oGrandChildnode.Name.LocalName
                                                        }
                                                    }
                                                }
                                                ClaimantNode = objClaimChild.SelectSingleNode("//" + ChildData.Name.LocalName);
                                                XmlNode ClaimantListPath = objClaimTemplate.SelectSingleNode("//" + Childname);
                                                ClaimantListPath.AppendChild(ClaimantListPath.OwnerDocument.ImportNode(ClaimantNode, true));
                                            }
                                        }
                                    }
                                    ClaimNode = objClaimTemplate.SelectSingleNode("//Claim");
                                    XmlNode ClaimListPath = objEventTemplate.SelectSingleNode("//ClaimList");
                                    ClaimListPath.AppendChild(ClaimListPath.OwnerDocument.ImportNode(ClaimNode, true));
                                }
                                        catch (RMAppException e)
                                        {
                                            throw e;
                                        }
                                        
                                        catch (Exception e)
                                        {
                                            throw new RMAppException(String.Format(Globalization.GetString("ImportXML.ClaimTemplateGeneration", m_iClientId), Childname));
                                                                    
                                            
                                            // "CLAIM"
                                        }
                                    }
                                break;
                            case "EventXDatedTextList":
                                string sDatedTextpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/EventXDatedText.xml"; //Load the EventXDatedText Template
                                objChildTemplate.Load(sDatedTextpath);
                                    try
                                    {

                                foreach (XElement oDatedText in oGenTwoChildren)
                                {
                                    Childname = oDatedText.Name.LocalName;
                                    IEnumerable<XNode> oC = oDatedText.Nodes();
                                    if (int.Equals(oC.Count(), 0))
                                    {
                                        XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                        string NodeValue = oDatedText.LastAttribute.Value;
                                        if (string.Equals(Childname, "EVDTROWID",StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                            {
                                                if (!m_oUserlogin.IsAllowedEx(RMO_DATEDTEXT, RMPermissions.RMO_UPDATE))
                                                {
                                                    //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_DATEDTEXT);
                                                    throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                }
                                            }
                                            if (!m_oUserlogin.IsAllowedEx(RMO_DATEDTEXT, RMPermissions.RMO_CREATE)) 
                                            {
                                                //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_DATEDTEXT);
                                                throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                            }
                                        }
                                        foreach (XmlNode oXmlNode in oXmlNodeList)
                                        {
                                            oXmlNode.InnerXml = NodeValue;
                                        }
                                    }
                                    else
                                    {
                                        foreach (XElement xele in oC)
                                        {
                                            Childname = xele.Name.LocalName;
                                            XmlNode oXmlNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                            string NodeValue = string.Empty;
                                            if (xele.LastAttribute != null)
                                                NodeValue = xele.LastAttribute.Value;
                                                if (xele.Attribute("codeid") != null)
                                                {
                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                    
                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                }
                                                else
                                                {
                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                    {
                                                        oXmlNode.InnerXml = NodeValue;
                                                }
                                            }
                                        }
                                    }
                                    ChildNode = objChildTemplate.SelectSingleNode("//EventXDatedText");
                                    XmlNode DatedTextListPath = objEventTemplate.SelectSingleNode("//EventXDatedTextList");
                                    DatedTextListPath.AppendChild(DatedTextListPath.OwnerDocument.ImportNode(ChildNode, true));
                                }
                                    }
                                    catch (RMAppException e)
                                    {
                                        throw e;
                                    }
                                    catch (Exception e)
                                    {
                                        throw new RMAppException(String.Format(Globalization.GetString("ImportXML.EventXDatedTextTemplateGeneration", m_iClientId), Childname));
                                        
                                        //"EventXDatedText" + Childname;
                                    }
                                break;
                            case "PiList":
                                IEnumerable<XNode> oPiList = oChildNode.Nodes();
                                foreach (XElement oPi in oPiList)
                                {
                                    string sPiNodeName = oPi.Name.LocalName;
                                    oGenTwoChildren = oPi.Nodes();
                                    switch (sPiNodeName)
                                    {
                                        case "PiOther":
                                                try
                                                {

                                                    string sPiOtherpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/PiOther.xml"; //Load the EventXDatedText Template
                                            objChildTemplate.Load(sPiOtherpath);

                                            foreach (XElement oPiOther in oGenTwoChildren)
                                            {
                                                Childname = oPiOther.Name.LocalName;
                                                IEnumerable<XNode> oC = oPiOther.Nodes();
                                                if (int.Equals(oC.Count(), 0))
                                                {
                                                    XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                                    string NodeValue = oPiOther.LastAttribute.Value;
                                                    foreach (XmlNode oXmlNode in oXmlNodeList)
                                                    {
                                                        oXmlNode.InnerXml = NodeValue;
                                                    }
                                                }
                                                else
                                                {
                                                    if (Childname.Equals("Supplementals") == true)
                                                    {
                                                        AppendToXML(objChildTemplate, oPiOther.Parent.Name.LocalName, oPiOther.Name.LocalName, string.Empty);
                                                    }
                                                    foreach (XElement xele in oC)
                                                    {
                                                        Childname = xele.Name.LocalName;
                                                        XmlNode oXmlNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                        string NodeValue = string.Empty;
                                                        if (xele.LastAttribute != null)
                                                            NodeValue = xele.LastAttribute.Value;
                                                        if (oPiOther.Name.LocalName.Equals("Supplementals") == true)
                                                        {
                                                            AppendToXML(objChildTemplate, oPiOther.Parent.Name.LocalName + "/" + oPiOther.Name.LocalName, Childname, NodeValue);
                                                        }
                                                        if(oXmlNode!=null)
                                                        {
                                                            if (oXmlNode.Name == "LastName" || oXmlNode.Name == "TaxId" || oXmlNode.Name == "FirstName" || oXmlNode.Name == "BirthDate" || oXmlNode.Name == "MiddleName" || oXmlNode.Name == "Phone1" || oXmlNode.Name == "Abbreviation" || oXmlNode.Name == "Phone2" || oXmlNode.Name == "AlsoKnownAs" || oXmlNode.Name == "FaxNumber" || oXmlNode.Name == "Addr1" || oXmlNode.Name == "Title" || oXmlNode.Name == "Addr2" || oXmlNode.Name == "Addr3" || oXmlNode.Name == "Addr4" || oXmlNode.Name == "SexCode" || oXmlNode.Name == "City" || oXmlNode.Name == "StateId" || oXmlNode.Name == "ZipCode" || oXmlNode.Name == "County" || oXmlNode.Name == "CountryCode")
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//PiEntity//" + Childname);
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNodeList oXmlNodeList1 = objChildTemplate.SelectNodes("//PiEntity//" + Childname);
                                                                        oXmlNode.InnerXml = NodeValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNodeList oXmlNodeList1 = objChildTemplate.SelectNodes("//" + Childname);
                                                                     if (Nodename.ToUpper() == "PIROWID")
                                                                    {
                                                                        if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue,out blnSuccess) > 0)
                                                                        {
                                                                            if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED))
                                                                            {
                                                                                //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_PERSON_INVOLVED);                                          
                                                                                throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));
                                                                            }
                                                                                if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED, RMPermissions.RMO_CREATE))
                                                                            {
                                                                                //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_PERSON_INVOLVED);
                                                                                throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                            }
                                                                        }
                                                                    }
                                                                        oXmlNode.InnerXml = NodeValue;
                                                                }
                                                            }
                                                        }
                                                    }
                                                   
                                                }
                                            }
                                            ChildNode = objChildTemplate.SelectSingleNode("//PiOther");
                                            XmlNode PiOtherPath = objEventTemplate.SelectSingleNode("//PiList");
                                            PiOtherPath.AppendChild(PiOtherPath.OwnerDocument.ImportNode(ChildNode, true));
                                                }
                                                catch (RMAppException e)
                                                {
                                                    throw e;
                                                }
                                                catch (Exception e)
                                                {
                                                    throw new RMAppException(String.Format(Globalization.GetString("ImportXML.PiOtherTemplateGeneration", m_iClientId), Childname));
                                                    
                                                    //  "PiOther" + Childname
                                                }
                                            break;
                                        case "PiPhysician":
                                            string sPiPhysicianpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/PiPhysician.xml"; //Load the EventXDatedText Template
                                            objChildTemplate.Load(sPiPhysicianpath);
                                                try
                                                {
                                            foreach (XElement oPiPhysician in oGenTwoChildren)
                                            {
                                                Childname = oPiPhysician.Name.LocalName;
                                                IEnumerable<XNode> oC = oPiPhysician.Nodes();
                                                if (int.Equals(oC.Count(),0))
                                                {
                                                    XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                                    string NodeValue = oPiPhysician.LastAttribute.Value;
                                                    foreach (XmlNode oXmlNode in oXmlNodeList)
                                                    {
                                                        oXmlNode.InnerXml = NodeValue;
                                                    }
                                                }
                                                else
                                                {
                                                    if (Childname.Equals("Supplementals") == true)
                                                    {
                                                        AppendToXML(objChildTemplate, oPiPhysician.Parent.Name.LocalName, oPiPhysician.Name.LocalName, string.Empty);
                                                    }
                                                    foreach (XElement xele in oC)
                                                    {
                                                        Childname = xele.Name.LocalName;
                                                        XmlNode oXmlNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                        string NodeValue = string.Empty;
                                                        if (xele.LastAttribute != null)
                                                            NodeValue = xele.LastAttribute.Value;

                                                        if (oPiPhysician.Name.LocalName.Equals("Supplementals") == true)
                                                        {
                                                            AppendToXML(objChildTemplate, oPiPhysician.Parent.Name.LocalName + "/" + oPiPhysician.Name.LocalName, Childname, NodeValue);
                                                        }
                                                        if(oXmlNode !=null)
                                                        {
                                                            if (oXmlNode.Name == "LastName" || oXmlNode.Name == "TaxId" || oXmlNode.Name == "FirstName" || oXmlNode.Name == "BirthDate" 
                                                                || oXmlNode.Name == "MiddleName" || oXmlNode.Name == "Phone1" || oXmlNode.Name == "Abbreviation" || oXmlNode.Name == "Phone2" 
                                                                || oXmlNode.Name == "AlsoKnownAs" || oXmlNode.Name == "FaxNumber" || oXmlNode.Name == "Addr1" || oXmlNode.Name == "Title"
                                                                || oXmlNode.Name == "Addr2" || oXmlNode.Name == "Addr3" || oXmlNode.Name == "Addr4" || oXmlNode.Name == "SexCode" || oXmlNode.Name == "City" 
                                                                || oXmlNode.Name == "StateId" || oXmlNode.Name == "ZipCode" || oXmlNode.Name == "County" || oXmlNode.Name == "CountryCode")
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//PhysEntity//" + Childname);
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {  
                                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNode oXmlNode1 = objChildTemplate.SelectSingleNode("//PhysEntity//" + Childname);
                                                                    if(oXmlNode1 !=null)
                                                                        oXmlNode1.InnerXml = NodeValue;
                                                                    
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNodeList oXmlNodeList1 = objChildTemplate.SelectNodes("//" + Childname);
                                                                    if (string.Equals(Nodename, "PIROWID",StringComparison.InvariantCultureIgnoreCase))
                                                                    {
                                                                        if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                        {
                                                                            if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED))
                                                                            {
                                                                                //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_PERSON_INVOLVED);   
                                                                                throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId)); 
                                                                            }
                                                                        }

                                                                        if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED, RMPermissions.RMO_CREATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_PERSON_INVOLVED);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                        }
                                                                    }
                                                                        oXmlNode.InnerXml = NodeValue;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            ChildNode = objChildTemplate.SelectSingleNode("//PiPhysician");
                                            XmlNode PiphyPath = objEventTemplate.SelectSingleNode("//PiList");
                                            PiphyPath.AppendChild(PiphyPath.OwnerDocument.ImportNode(ChildNode, true));
                                                }
                                                catch (RMAppException e)
                                                {
                                                    throw e;
                                                }
                                                catch (Exception e)
                                                {
                                                    throw new RMAppException(String.Format(Globalization.GetString("ImportXML.PiPhysicianTemplateGeneration", m_iClientId), Childname));
                                                    
                                                    //                               "PiPhysician" + Childname
                                                }

                                                break;
                                            case "PiWitness":
                                                string sPiWitnesspath = RMConfigurator.BasePath + "/userdata/XMLTemplate/PiWitness.xml"; //Load the EventXDatedText Template
                                                objChildTemplate.Load(sPiWitnesspath);
                                                try
                                                {
                                            foreach (XElement oPiWitness in oGenTwoChildren)
                                            {
                                                Childname = oPiWitness.Name.LocalName;
                                                IEnumerable<XNode> oC = oPiWitness.Nodes();
                                                if (int.Equals(oC.Count() , 0))
                                                {
                                                    XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                                    string NodeValue = oPiWitness.LastAttribute.Value;
                                                    foreach (XmlNode oXmlNode in oXmlNodeList)
                                                    {
                                                        oXmlNode.InnerXml = NodeValue;
                                                    }
                                                }
                                                else
                                                {
                                                    if (Childname.Equals("Supplementals") == true)
                                                    {
                                                        AppendToXML(objChildTemplate, oPiWitness.Parent.Name.LocalName, oPiWitness.Name.LocalName, string.Empty);
                                                    }
                                                    foreach (XElement xele in oC)
                                                    {
                                                        Childname = xele.Name.LocalName;
                                                        XmlNode oXmlNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                        string NodeValue = string.Empty;
                                                        if (xele.LastAttribute != null)
                                                            NodeValue = xele.LastAttribute.Value;
                                                        if (oPiWitness.Name.LocalName.Equals("Supplementals") == true)
                                                        {
                                                            AppendToXML(objChildTemplate, oPiWitness.Parent.Name.LocalName + "/" + oPiWitness.Name.LocalName, Childname, NodeValue);
                                                        }
                                                        if(oXmlNode !=null)
                                                        {
                                                            if (oXmlNode.Name == "LastName" || oXmlNode.Name == "TaxId" || oXmlNode.Name == "FirstName" || oXmlNode.Name == "BirthDate" 
                                                                || oXmlNode.Name == "MiddleName" || oXmlNode.Name == "Phone1" || oXmlNode.Name == "Abbreviation" || oXmlNode.Name == "Phone2" 
                                                                || oXmlNode.Name == "AlsoKnownAs" || oXmlNode.Name == "FaxNumber" || oXmlNode.Name == "Addr1" || oXmlNode.Name == "Title"
                                                                || oXmlNode.Name == "Addr2" || oXmlNode.Name == "Addr3" || oXmlNode.Name == "Addr4" || oXmlNode.Name == "SexCode" || oXmlNode.Name == "City"
                                                                || oXmlNode.Name == "StateId" || oXmlNode.Name == "ZipCode" || oXmlNode.Name == "County" || oXmlNode.Name == "CountryCode")
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//PiEntity//" + Childname);
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNode oXmlNode1 = objChildTemplate.SelectSingleNode("//PiEntity//" + Childname);
                                                                    if(oXmlNode1!=null)
                                                                        oXmlNode1.InnerXml = NodeValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                                    if (Nodename.ToUpper() == "PIROWID")
                                                                    {
                                                                        if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                        {
                                                                            if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED))
                                                                            {
                                                                                //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_PERSON_INVOLVED);      
                                                                                throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));    
                                                                            }
                                                                        }

                                                                        if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED, RMPermissions.RMO_CREATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_PERSON_INVOLVED);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                        }
                                                                    }
                                                                        oXmlNode.InnerXml = NodeValue;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            ChildNode = objChildTemplate.SelectSingleNode("//PiWitness");
                                            XmlNode PiwitnessPath = objEventTemplate.SelectSingleNode("//PiList");
                                            PiwitnessPath.AppendChild(PiwitnessPath.OwnerDocument.ImportNode(ChildNode, true));
                                                }
                                                catch (RMAppException e)
                                                {
                                                    throw e;
                                                }
                                                catch (Exception e)
                                                {
                                                    throw new RMAppException(String.Format(Globalization.GetString("ImportXML.PiWitnessTemplateGeneration", m_iClientId), Childname));
                                                    //"PiWitness" + Childname
                                                }
                                                break;
                                            case "PiMedicalStaff":
                                                string sPiMedicalStaffpath = RMConfigurator.BasePath + "/userdata/XMLTemplate/PiMedicalStaff.xml"; //Load the EventXDatedText Template
                                                objChildTemplate.Load(sPiMedicalStaffpath);
                                                try
                                                {
                                            foreach (XElement oPiMedicalStaff in oGenTwoChildren)
                                            {
                                                Childname = oPiMedicalStaff.Name.LocalName;
                                                IEnumerable<XNode> oC = oPiMedicalStaff.Nodes();
                                                if (int.Equals(oC.Count(), 0))
                                                {
                                                    XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                                    string NodeValue = oPiMedicalStaff.LastAttribute.Value;
                                                    foreach (XmlNode oXmlNode in oXmlNodeList)
                                                    {
                                                        oXmlNode.InnerXml = NodeValue;
                                                    }
                                                }
                                                else
                                                {
                                                    if (Childname.Equals("Supplementals") == true)
                                                    {
                                                        AppendToXML(objChildTemplate, oPiMedicalStaff.Parent.Name.LocalName, oPiMedicalStaff.Name.LocalName, string.Empty);
                                                    }
                                                    foreach (XElement xele in oC)
                                                    {
                                                        Childname = xele.Name.LocalName;
                                                        XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                                        string NodeValue = string.Empty;
                                                        if(xele.LastAttribute!=null)
                                                           NodeValue = xele.LastAttribute.Value;

                                                        if (oPiMedicalStaff.Name.LocalName.Equals("Supplementals") == true)
                                                        {
                                                            AppendToXML(objChildTemplate, oPiMedicalStaff.Parent.Name.LocalName + "/" + oPiMedicalStaff.Name.LocalName,xele);
                                                        }
                                                        foreach (XmlNode oXmlNode in oXmlNodeList)
                                                        {
                                                            if (oXmlNode.Name == "LastName" || oXmlNode.Name == "TaxId" || oXmlNode.Name == "FirstName" || oXmlNode.Name == "BirthDate" 
                                                                || oXmlNode.Name == "MiddleName" || oXmlNode.Name == "Phone1" || oXmlNode.Name == "Abbreviation" || oXmlNode.Name == "Phone2" 
                                                                || oXmlNode.Name == "AlsoKnownAs" || oXmlNode.Name == "FaxNumber" || oXmlNode.Name == "Addr1" || oXmlNode.Name == "Title"
                                                                || oXmlNode.Name == "Addr2" || oXmlNode.Name == "Addr3" || oXmlNode.Name == "Addr4" || oXmlNode.Name == "SexCode" || oXmlNode.Name == "City"
                                                                || oXmlNode.Name == "StateId" || oXmlNode.Name == "ZipCode" || oXmlNode.Name == "County" || oXmlNode.Name == "CountryCode")
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//MedicalStaff//StaffEntity//" + Childname);
                                                                   oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    //string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNode oXmlNode1 = objChildTemplate.SelectSingleNode("//MedicalStaff//StaffEntity//" + Childname);
                                                                    if(oXmlNode1 !=null)
                                                                        oXmlNode1.InnerXml = NodeValue;
                                                                }
                                                            }
                                                            else if (oXmlNode.Name == "StaffEid" || oXmlNode.Name == "MedicalStaffNumber" || oXmlNode.Name == "BeeperNumber" || oXmlNode.Name == "CellularNumber" 
                                                                    || oXmlNode.Name == "EmergencyContact" || oXmlNode.Name == "PrimaryPolicyId" || oXmlNode.Name == "HireDate" 
                                                                    || oXmlNode.Name == "MaritalStatCode" || oXmlNode.Name == "LicNum" || oXmlNode.Name == "HomeAddr1"
                                                                    || oXmlNode.Name == "LicState" || oXmlNode.Name == "HomeAddr2" || oXmlNode.Name == "HomeAddr3" || oXmlNode.Name == "HomeAddr4" 
                                                                    || oXmlNode.Name == "LicIssueDate" || oXmlNode.Name == "HomeCity" || oXmlNode.Name == "LicExpiryDate" || oXmlNode.Name == "HomeStateId" 
                                                                    || oXmlNode.Name == "LicDeaNum" || oXmlNode.Name == "HomeZipCode" || oXmlNode.Name == "LicDeaExpDate" || oXmlNode.Name == "StaffStatusCode" 
                                                                    || oXmlNode.Name == "StaffStatusCode" || oXmlNode.Name == "StaffPosCode" || oXmlNode.Name == "StaffCatCode" 
                                                                    || oXmlNode.Name == "DeptAssignedEid" || oXmlNode.Name == "PrivilegeList" || oXmlNode.Name == "CertificationList")
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//MedicalStaff//" + Childname);
                                                              
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    //string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNode oXmlNode1 = objChildTemplate.SelectSingleNode("//MedicalStaff//" + Childname);

                                                                    if(oXmlNode1 !=null)
                                                                        oXmlNode1.InnerXml = NodeValue;
                                                                  
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                                   
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    //string NodeValue1 = xele.LastAttribute.Value;
                                                                 
                                                                    if (string.Equals(Nodename, "PIROWID",StringComparison.InvariantCultureIgnoreCase))
                                                                    {
                                                                        if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                        {
                                                                            if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED))
                                                                            {
                                                                                //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_PERSON_INVOLVED);   
                                                                                throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId)); 
                                                                            }
                                                                        }

                                                                        if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED, RMPermissions.RMO_CREATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_PERSON_INVOLVED);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                        }
                                                                    }
                                                                    //foreach (XmlNode oXmlNode1 in oXmlNodeList)
                                                                    //{
                                                                        oXmlNode.InnerXml = NodeValue;
                                                                    //}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            ChildNode = objChildTemplate.SelectSingleNode("//PiMedicalStaff");
                                            XmlNode PiMedicalPath = objEventTemplate.SelectSingleNode("//PiList");
                                            PiMedicalPath.AppendChild(PiMedicalPath.OwnerDocument.ImportNode(ChildNode, true));
                                                }
                                                catch (RMAppException e)
                                                {
                                                    throw e;
                                                }
                                                catch (Exception e)
                                                {
                                                    throw new RMAppException(String.Format(Globalization.GetString("ImportXML.PiMedicalStaffTemplateGeneration", m_iClientId), Childname));
                                                    
                                                    //"PiMedicalStaff" + Childname
                                                }


                                                break;
                                            case "PiEmployee":
                                                string sPiEmployeepath = RMConfigurator.BasePath + "/userdata/XMLTemplate/PiEmployee.xml"; //Load the EventXDatedText Template
                                                objChildTemplate.Load(sPiEmployeepath);
                                                try
                                                {
                                            foreach (XElement oPiEmployee in oGenTwoChildren)
                                            {
                                                Childname = oPiEmployee.Name.LocalName;
                                                //XmlDocument objClaimChild = new XmlDocument();
                                                IEnumerable<XNode> oC = oPiEmployee.Nodes();
                                                if (int.Equals(oC.Count(),  0))
                                                {
                                                    XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                                    string NodeValue = oPiEmployee.LastAttribute.Value;
                                                    foreach (XmlNode oXmlNode in oXmlNodeList)
                                                    {
                                                        //bsharma33 :  Mits # 32905 
                                                        if (oXmlNode.Attributes["codeid"] != null)
                                                        {
                                                            oXmlNode.Attributes["codeid"].Value = NodeValue;
                                                        }
                                                        else
                                                        {
                                                            oXmlNode.InnerXml = NodeValue;
                                                        }
                                                        //End change
                                                    }
                                                }
                                                else
                                                {
                                                    if (Childname.Equals("Supplementals") == true)
                                                    {
                                                        AppendToXML(objChildTemplate, oPiEmployee.Parent.Name.LocalName, oPiEmployee.Name.LocalName, string.Empty);
                                                    }

                                                    foreach (XElement xele in oC)
                                                    {
                                                        Childname = xele.Name.LocalName;
                                                        XmlNodeList oXmlNodeList = objChildTemplate.SelectNodes("//" + Childname);
                                                        string NodeValue = string.Empty;
                                                        if (xele.LastAttribute != null)
                                                            NodeValue = xele.LastAttribute.Value;
                                                        if (oPiEmployee.Name.LocalName.Equals("Supplementals") == true)
                                                        {
                                                            AppendToXML(objChildTemplate, oPiEmployee.Parent.Name.LocalName + "/" + oPiEmployee.Name.LocalName, xele);
                                                        }
                                                        foreach (XmlNode oXmlNode in oXmlNodeList)
                                                        {
                                                            if (oXmlNode.Name == "LastName" || oXmlNode.Name == "TaxId" || oXmlNode.Name == "FirstName" || oXmlNode.Name == "BirthDate" 
                                                                || oXmlNode.Name == "MiddleName" || oXmlNode.Name == "Phone1" || oXmlNode.Name == "Abbreviation" || oXmlNode.Name == "Phone2" 
                                                                || oXmlNode.Name == "AlsoKnownAs" || oXmlNode.Name == "FaxNumber" || oXmlNode.Name == "Addr1" || oXmlNode.Name == "Title"
                                                                || oXmlNode.Name == "Addr2" || oXmlNode.Name == "Addr3" || oXmlNode.Name == "Addr4" || oXmlNode.Name == "SexCode" || oXmlNode.Name == "City" 
                                                                || oXmlNode.Name == "StateId" || oXmlNode.Name == "ZipCode" || oXmlNode.Name == "County" || oXmlNode.Name == "CountryCode")
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//PiEntity//" + Childname);
                                                                    
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    //string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNode oXmlNode1 = objChildTemplate.SelectSingleNode("//PiEntity//" + Childname);
                                                                    if(oXmlNode1 !=null)
                                                                        oXmlNode1.InnerXml = NodeValue;
                                                                 
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (xele.Attribute("codeid") != null)
                                                                {
                                                                    XmlNode oNode = objChildTemplate.SelectSingleNode("//" + Childname);
                                                                    
                                                                    oNode.Attributes["codeid"].Value = xele.Attribute("codeid").Value;
                                                                }
                                                                else
                                                                {
                                                                    ////string NodeValue1 = xele.LastAttribute.Value;
                                                                    XmlNodeList oXmlNodeList1 = objChildTemplate.SelectNodes("//" + Childname);
                                                                    if (Nodename.ToUpper() == "PIROWID")
                                                                    {
                                                                        if (string.IsNullOrEmpty(NodeValue) && Conversion.CastToType<int>(NodeValue, out blnSuccess) > 0)
                                                                        {
                                                                            if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED))
                                                                            {
                                                                                //throw new PermissionViolationException(RMPermissions.RMO_UPDATE, RMO_PERSON_INVOLVED);          
                                                                                throw new RMAppException(Globalization.GetString("Permission.NoUpdate", m_iClientId));        
                                                                            }
                                                                        }
                                                                        if (!m_oUserlogin.IsAllowedEx(RMO_PERSON_INVOLVED, RMPermissions.RMO_CREATE))
                                                                        {
                                                                            //throw new PermissionViolationException(RMPermissions.RMO_CREATE, RMO_PERSON_INVOLVED);
                                                                            throw new RMAppException(Globalization.GetString("Permission.NoCreate", m_iClientId));
                                                                        }
                                                                    }
                                                                        oXmlNode.InnerXml = NodeValue;
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            ChildNode = objChildTemplate.SelectSingleNode("//PiEmployee");
                                            XmlNode PiEmpPath = objEventTemplate.SelectSingleNode("//PiList");
                                            PiEmpPath.AppendChild(PiEmpPath.OwnerDocument.ImportNode(ChildNode, true));
                                                }
                                                catch (RMAppException e)
                                                {
                                                    throw e;
                                                }
                                                catch (Exception e)
                                                {
                                                    throw new RMAppException(String.Format(Globalization.GetString("ImportXML.PiEmployeeTemplateGeneration", m_iClientId), Childname));
                                                    
                                                    //"PiEmployee" + Childname
                                                }
                                                break;
                                    }
                                }

                                break;
                        }
                    }
                    else
                    {
                        XmlNode oListMNode = objEventTemplate.SelectSingleNode("//" + Nodename);
                        oListMNode.Attributes["committedcount"].Value = oChildNode.LastAttribute.Value;
                    }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return objEventTemplate;
        }

        public XmlDocument ToXmlDocument(XElement XElm)
        {
            var xmldoc = new XmlDocument();
            xmldoc.Load(XElm.CreateReader());
            return xmldoc;
        }


        private XmlDocument UpdateRecords(XmlNode objnode, XmlDocument objDoc)
        {
            string Nodename = string.Empty;
            XElement objEle = null;

            try
            {
                objEle = XElement.Parse(objnode.InnerXml);
                foreach (XElement oChildNode in objEle.Nodes())
                {
                    Nodename = oChildNode.Name.LocalName;
                    if (Nodename.Equals("UpdatedByUser") || Nodename.Equals("AddedByUser") || Nodename.Equals("DttmRcdLastUpd") || Nodename.Equals("DttmRcdAdded"))
                    {
                        continue;
                    }
                    if (int.Equals(Nodename.IndexOf("List"),  -1))
                    {
                        if (oChildNode.Attribute("codeid") != null)
                        {
                            XmlNode oXmlNodeList = objDoc.SelectSingleNode("//" + Nodename);
                            oXmlNodeList.InnerXml = oChildNode.Value;
                            oXmlNodeList.Attributes["codeid"].Value = oChildNode.Attribute("codeid").Value;
                        }
                        else if (oChildNode.Attribute("value") != null)
                        {
                            string NodeValue = oChildNode.Attribute("value").Value;
                            XmlNodeList oXmlNodeList = objDoc.SelectNodes("//" + Nodename);

                            foreach (XmlNode oXmlNode in oXmlNodeList)
                            {
                                oXmlNode.InnerXml = NodeValue;
                                if (oXmlNode.Attributes["codeid"] != null)
                                {
                                    oXmlNode.Attributes["codeid"].Value = NodeValue;
                                }
                                else
                                    oXmlNode.InnerXml = NodeValue;
                            }
                        }
                        else
                        {
                            foreach (XElement oChild1 in oChildNode.Nodes())
                            {
                                if (oChild1.Name.LocalName.Equals("UpdatedByUser") || oChild1.Name.LocalName.Equals("AddedByUser") || oChild1.Name.LocalName.Equals("DttmRcdLastUpd") || oChild1.Name.LocalName.Equals("DttmRcdAdded"))
                                {
                                    continue;
                                }
                                if (oChild1.Attribute("codeid") != null)
                                {
                                    XmlNode oXmlNodeList = objDoc.SelectSingleNode("//" + Nodename + "/" + oChild1.Name.LocalName);
                                    if (oXmlNodeList != null)
                                    {
                                        oXmlNodeList.InnerXml = oChild1.Value;
                                        oXmlNodeList.Attributes["codeid"].Value = oChild1.Attribute("codeid").Value;
                                    }
                                }
                                else if (oChild1.LastAttribute != null)
                                {
                                    string NodeValue = oChild1.LastAttribute.Value;
                                    XmlNodeList oXmlNodeList = objDoc.SelectNodes("//" + Nodename + "/" + oChild1.Name.LocalName);
                                    foreach (XmlNode oXmlNode in oXmlNodeList)
                                    {
                                        oXmlNode.InnerXml = NodeValue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("ImportXML.UpdateRecords", m_iClientId));
            }
            finally
            {
                objEle = null;
            }

            return objDoc;
        }


        public XElement GetSupplementals(string p_sNodeName)
        {
            XElement objEle = null;
            DataModelFactory objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword,m_iClientId); //mbahl3
            DataObject objDataModel = null;
            
            try
            {
                switch (p_sNodeName.ToUpper())
                {
                    case "EVENT":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (Event)objDmf.GetDataModelObject("Event", false);
                        objEle = XElement.Parse((objDataModel as Event).Supplementals.SerializeObject());
                        break;

                    case "GENERALCLAIM":
                          objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                          (objDataModel as Claim).LineOfBusCode = 241;
                        objEle = XElement.Parse((objDataModel as Claim).Supplementals.SerializeObject());
                        break;
                    case "WORKERSCOMPENSATIONCLAIM":

                          objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                          (objDataModel as Claim).LineOfBusCode = 243;
                        objEle = XElement.Parse((objDataModel as Claim).Supplementals.SerializeObject());
                        break;
                    case "NONOCCUPATIONALCLAIM":
                          objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                          (objDataModel as Claim).LineOfBusCode = 844;
                        objEle = XElement.Parse((objDataModel as Claim).Supplementals.SerializeObject());
                        break;
                    case "VEHICLEACCIDENTCLAIM":
                          objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                          (objDataModel as Claim).LineOfBusCode = 242;
                        objEle = XElement.Parse((objDataModel as Claim).Supplementals.SerializeObject());
                        break;
                    case "PROPERTYCLAIM":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (Claim)objDmf.GetDataModelObject("Claim", false);
                        (objDataModel as Claim).LineOfBusCode =845;
                        objEle = XElement.Parse((objDataModel as Claim).Supplementals.SerializeObject());
                        break;
                    case "ADJUSTER":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (ClaimAdjuster)objDmf.GetDataModelObject("ClaimAdjuster", false);
                        objEle = XElement.Parse((objDataModel as ClaimAdjuster).Supplementals.SerializeObject());
                        break;
                    case "CLAIMANT":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (Claimant)objDmf.GetDataModelObject("Claimant", false);
                        objEle = XElement.Parse((objDataModel as Claimant).Supplementals.SerializeObject());
                        break;
                    case "LITIGATION":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (ClaimXLitigation)objDmf.GetDataModelObject("ClaimXLitigation", false);
                        objEle = XElement.Parse((objDataModel as ClaimXLitigation).Supplementals.SerializeObject());
                        break;
                    case "DEFENDANT":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (Defendant)objDmf.GetDataModelObject("Defendant", false);
                        objEle = XElement.Parse((objDataModel as Defendant).Supplementals.SerializeObject());
                        break;                   
                    //igupta3 MITS:32002 Changes starts
                    case "SUBROGATION":
                        objDataModel = (ClaimXSubrogation)objDmf.GetDataModelObject("ClaimXSubrogation", false);
                        objEle = XElement.Parse((objDataModel as ClaimXSubrogation).Supplementals.SerializeObject());
                        break;

                    case "UNIT":
                        objDataModel = (UnitXClaim)objDmf.GetDataModelObject("UnitXClaim", false);
                        objEle = XElement.Parse((objDataModel as UnitXClaim).Supplementals.SerializeObject());
                        break;
                    //igupta3 MITS:32002 Changes ends

                    case "INTERVENTION":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (EventXIntervention)objDmf.GetDataModelObject("EventXIntervention", false);
                        objEle = XElement.Parse((objDataModel as EventXIntervention).Supplementals.SerializeObject());
                        break;


                    case "WITNESS":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (PiWitness)objDmf.GetDataModelObject("PiWitness", false);
                        objEle = XElement.Parse((objDataModel as PiWitness).Supplementals.SerializeObject());
                        break;

                    case "EMPLOYEE":

                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (PiEmployee)objDmf.GetDataModelObject("PiEmployee", false);
                        objEle = XElement.Parse((objDataModel as PiEmployee).Supplementals.SerializeObject());
                        break;

                    case "MEDICALSTAFF":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (PiMedicalStaff)objDmf.GetDataModelObject("PiMedicalStaff", false);
                        objEle = XElement.Parse((objDataModel as PiMedicalStaff).Supplementals.SerializeObject());
                        break;

                    case "PHYSICIAN":

                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (PiPhysician)objDmf.GetDataModelObject("PiPhysician", false);
                        objEle = XElement.Parse((objDataModel as PiPhysician).Supplementals.SerializeObject());
                        break;
                    case "PATIENT":
                    case "OTHER PEOPLE":
                    case "DRIVER INSURED":
                    case " DRIVER OTHER":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (PiOther)objDmf.GetDataModelObject("PiOther", false);
                        objEle = XElement.Parse((objDataModel as PiOther).Supplementals.SerializeObject());
                        break;
                    //Diary Supplemental - atennyson3/agupta298 JIRA-4691/MITS#36804 - Start
                    case "WPADIARYENTRY":
                        //objDmf = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword);
                        objDataModel = (WpaDiaryEntry)objDmf.GetDataModelObject("WpaDiaryEntry", false);
                        objEle = XElement.Parse((objDataModel as WpaDiaryEntry).Supplementals.SerializeObject());
                        break;
                    //Diary Supplemental - atennyson3/agupta298 JIRA-4691/MITS#36804 - End
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDmf != null)
                {
                    objDmf.Dispose();
                }
                if (objDataModel != null)
                {
                    objDataModel.Dispose();
                }
            }
            return objEle;

        }
        // Added for MITS 29711 by bsharma33 for PDF generation
      	private void NewPage( C1PrintDocument p_objPrintDoc , NewPageStartedEventArgs p_objArgs )
		{			
			RenderTable objHeaderTable = null ;
			string sHeaderText = string.Empty ;

			try
			{
				sHeaderText = "XML Import Log - Page " 
					+ p_objPrintDoc.PageCount + "                             " 
					+ DateTime.Now.ToShortDateString() ;
			
				objHeaderTable = new RenderTable( p_objPrintDoc );	
				objHeaderTable.Style.Borders.AllEmpty = false;
				objHeaderTable.Style.Borders.Left = new LineDef( Color.Black , 0 );
				objHeaderTable.Style.Borders.Right = new LineDef( Color.Black , 0 );
				objHeaderTable.Style.Borders.Bottom = new LineDef( Color.Black , 0 );
				objHeaderTable.Style.Borders.Top = new LineDef( Color.Black , 0 );

				objHeaderTable.StyleTableCell.BorderTableHorz.Empty = true;
				objHeaderTable.StyleTableCell.BorderTableVert.Empty = true;							
				objHeaderTable.StyleTableCell.BorderTableHorz.WidthPt = 0 ;
				objHeaderTable.StyleTableCell.BorderTableVert.WidthPt = 0 ;

				objHeaderTable.Columns.AddSome(1);
				objHeaderTable.Body.Rows.AddSome(2);
				objHeaderTable.Columns[0].StyleTableCell.TextAlignHorz = AlignHorzEnum.Center;
				objHeaderTable.Columns[0].Width = 11000 ;
			
				objHeaderTable.Body.Cell( 0, 0).StyleTableCell.Font = new Font( "Arial" , 10 ,FontStyle.Bold );									
				objHeaderTable.Body.Cell( 0, 0).RenderText.Text = sHeaderText ;
			
				objHeaderTable.Body.Cell( 1, 0).StyleTableCell.Font = new Font( "Arial" , 10 ,FontStyle.Bold );									
				objHeaderTable.Body.Cell( 1, 0).RenderText.Text = "" ;
			
				p_objPrintDoc.PageHeader.RenderObject = objHeaderTable;				
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ImportXML.NewPage.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if(objHeaderTable!=null)
				{
					objHeaderTable.Dispose();
					objHeaderTable = null ;
				}
			}

		}
        // End additions by bsharma33
        //Added for MITS 29711 by bsharma33 for PDF generation
        public string print(string sText)
        {
            C1PrintDocument objPrintDoc = null ;
            RenderRichText objRichText = null ;			
			RenderTable objTable = null ;
			TableColumn objTableColumn = null ;
            int iRowIndex=0;

            	objPrintDoc = new C1PrintDocument();
				objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;
				objPrintDoc.PageSettings.Margins.Top = 45 ;
				objPrintDoc.PageSettings.Margins.Left = 40 ;
				objPrintDoc.PageSettings.Margins.Bottom = 40 ;
				objPrintDoc.PageSettings.Margins.Right = 40 ;	
				objPrintDoc.PageSettings.Landscape = false ;
				objPrintDoc.NewPageStarted += new NewPageStartedEventHandler( this.NewPage );			
				objPrintDoc.StartDoc();

				objTable = new RenderTable( objPrintDoc );
				objPrintDoc.Style.Borders.AllEmpty = true;
				objTable.Body.StyleTableCell.Font = new Font( "Courier New" , 10 );
			
				objTable.Columns.AddSome( 1 );
				objTableColumn = new TableColumn( objPrintDoc );
				objTableColumn.Width = 10000 ;
				objTable.Columns[0] = objTableColumn ;								 					
			
				objTable.Body.AutoHeight = true ;

				objTable.StyleTableCell.Borders.Bottom = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Top = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Left = new LineDef( Color.Black , 0 );
				objTable.StyleTableCell.Borders.Right = new LineDef( Color.Black , 0 );
					
				objTable.Style.Borders.Left = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Right = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Bottom = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Top = new LineDef( Color.Black , 0 );
			
				objTable.StyleTableCell.Padding.Right = 0 ;
				objTable.StyleTableCell.Padding.Top = 0 ;
				objTable.StyleTableCell.Padding.Bottom = 0 ;			

				objTable.StyleTableCell.BorderTableHorz.Empty = false;
				objTable.StyleTableCell.BorderTableVert.Empty = false;	
		
				objTable.StyleTableCell.BorderTableHorz.WidthPt = 0 ;
				objTable.StyleTableCell.BorderTableVert.WidthPt = 0 ;

				objRichText = new RenderRichText( objPrintDoc );
				
					
             objTable.Body.Rows.AddSome( 6 );
					


                objTable.Body.Cell(iRowIndex, 0).RenderText.Text = sText;
				objPrintDoc.RenderBlock( objTable );

				// End doc.
				objPrintDoc.EndDoc();
			
				// Save the PDF.

                string sPDFPath = RMConfigurator.TempPath + "\\XmlImportLog\\XmlImport" + DateTime.Now.ToString("yyyyMMddHHmmss") + AppDomain.GetCurrentThreadId() + ".pdf";
                objPrintDoc.ExportToPDF(sPDFPath, false);
                MemoryStream objMemoryStream = null;
                FileStream objFileStream = null;
                BinaryReader objBReader = null;
                byte[] arrRet = null;
                objFileStream = new FileStream(sPDFPath, FileMode.Open);
                objBReader = new BinaryReader(objFileStream);
                arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                objMemoryStream = new MemoryStream(arrRet);

                objFileStream.Close();

                // Delete the temp file.
               // File.Delete(sPDFPath);

                return Convert.ToBase64String(objMemoryStream.ToArray());
               
        }
        // End additions by bsharma33
        //Added for MITS 29711 by bsharma33 for PDF generation
        private void PrintMemoString(string p_sMemoString, ref RenderTable p_objTable, ref int p_iRowIndex)
        {
            ////const int NOT_EOL = 0;
            ////const int NEXT_EOL = 1;
            ////const int THIS_EOL = 2;
            ////const int BOTH_EOL = 3;
            const int iCharPerLine = 85;

            string sRemainStr = "";
            ////string sNewStr = "";
            ////string sToPrint = "";
            string sConcat = "";
            ////int i = 0;
            ////int iPtr = 0;
            ////int iEOL = 0;

            //MITS 16892
            string[] strArr;
            string[] stringSeparators = new string[] { "\r\n" };

            try
            {
                #region Old Logic
                //sRemainStr = p_sMemoString;
                ////MITS 17095 : Umesh
                ////replacing &nbsp; by single space
                //sRemainStr = sRemainStr.Replace("&nbsp;", " ");
                //do
                //{
                //    iPtr = sRemainStr.IndexOf(" ");
                //    if (iPtr == -1)
                //        iPtr = sRemainStr.Length + 1;

                //    sNewStr = GetLeftString(sRemainStr, iPtr + 1);
                //    i = sNewStr.IndexOf(Microsoft.VisualBasic.Constants.vbCr);
                //    if (i > -1)
                //    {
                //        iPtr = i + 1;
                //        sNewStr = GetLeftString(sNewStr, i);
                //        if (iEOL == NOT_EOL)
                //            iEOL = NEXT_EOL;
                //        else
                //            iEOL = BOTH_EOL;
                //    }
                //    else
                //    {
                //        i = sNewStr.IndexOf("\n");
                //        if (i > -1)
                //        {                       
                //            iPtr = i;
                //            sNewStr = GetLeftString(sNewStr, i);
                //            if (iEOL == NOT_EOL)
                //                iEOL = NEXT_EOL;
                //            else
                //                iEOL = BOTH_EOL;
                //        }
                //    }
                //    sConcat = sToPrint + sNewStr;
                //    if ((sConcat.Length > iCharPerLine) || (iEOL == THIS_EOL) || (iEOL == BOTH_EOL))
                //    {
                //        //GeneratePrintString(GlobalExecSummConstant.CHARSTRING, sToPrint.Trim(), false, false);
                //        //m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                //        p_objTable.Body.Rows.AddSome(1);
                //        p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sToPrint.Trim();
                //        p_iRowIndex++;
                //        //m_arrlstReportData.Clear();
                //        sToPrint = sNewStr;
                //    }
                //    else
                //        sToPrint = sToPrint + sNewStr;
                //    sRemainStr = GetRemainingString(sRemainStr, iPtr + 1);
                //    if ((iEOL == NEXT_EOL) || (iEOL == BOTH_EOL))
                //        iEOL = THIS_EOL;
                //    else
                //        iEOL = NOT_EOL;
                //} while (sRemainStr != "");
                //if (sToPrint.Trim() != "")
                //{
                //    p_objTable.Body.Rows.AddSome(1);
                //    p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sToPrint.Trim();
                //    p_iRowIndex++;                    
                //    //GeneratePrintString(GlobalExecSummConstant.CHARSTRING, sToPrint.Trim(), false, false);
                //    //m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                //   // m_arrlstReportData.Clear();
                //}
                #endregion Old Logic

                //MITS 16892 New Logic
                sRemainStr = p_sMemoString;
                //sRemainStr = StripHTMLTags(sRemainStr);//Parijat Stripping the HTML tags
                //replacing &nbsp; by single space
                sRemainStr = sRemainStr.Replace("&nbsp;", " ");

                strArr = sRemainStr.Split(stringSeparators, StringSplitOptions.None);
                foreach (string sNewStr in strArr)
                {
                    string sLeftOver = string.Empty;

                    sConcat = sNewStr;
                    while (sConcat.Length > iCharPerLine)
                    {
                        if (sConcat.Substring(iCharPerLine, sConcat.Length - iCharPerLine).IndexOf(" ") == 0)
                        {
                            //In this case no word truncates
                            sLeftOver = string.Empty;

                            p_objTable.Body.Rows.AddSome(1);
                            p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Substring(0, iCharPerLine).Trim();
                            p_iRowIndex++;

                            sConcat = sConcat.Substring(iCharPerLine, sConcat.Length - iCharPerLine);
                        }
                        else
                        {
                            //Word truncates. Get the position of last space character in the string
                            int iPos = 0;
                            iPos = sConcat.Substring(0, iCharPerLine).LastIndexOf(" ");

                            if (iPos != -1)
                            {
                                sLeftOver = sConcat.Substring(iPos, sConcat.Length - iPos).Trim();
                                p_objTable.Body.Rows.AddSome(1);
                                p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Substring(0, iPos).Trim();
                                p_iRowIndex++;
                            }
                            else
                            {
                                sLeftOver = sConcat.Substring(iCharPerLine + 1, sConcat.Length - (iCharPerLine + 1)).Trim();
                                p_objTable.Body.Rows.AddSome(1);
                                p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Substring(0, iCharPerLine).Trim();
                                p_iRowIndex++;
                            }
                            sConcat = sLeftOver;
                        }
                    }
                    if (sConcat != "")
                    {
                        p_objTable.Body.Rows.AddSome(1);
                        if (sConcat.Trim() == "")
                            p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = " ";
                        else
                            p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Trim();
                        p_iRowIndex++;
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ImportXML.PrintMemoString.Error", m_iClientId), p_objException);
            }
        }
        // End additions by bsharma33

    }
}
