using System;
using System.Xml;
using System.IO;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;

namespace Riskmaster.Application.UploadFile
{
	/**************************************************************
	 * $File		: UploadFile.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 13/09/2005
	 * $Author		: Ratheen Chaturvedi
	 * $Comment		: UploadFile class has functions Transfer File
	 * $Source		:  	
	**************************************************************/

	/// <summary>
	/// SupervisorApproval Class.
	/// </summary>
	public class UploadFile
	{
		
		#region Constructor
		/// <summary>
		/// UploadFile class constructor
		/// </summary>
		/// <param name="p_sDSN">Connection string to database</param>
		public UploadFile(){}
		#endregion

		#region public functions
		/// Name		: FileTransfer
		/// Author		: Ratheen Chaturvedi
		/// Date Created:13-September-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <param name="p_sValue">Value</param>
		/// <param name="p_sFieldName">Field name</param>
		/// <summary>
		/// writes the file on the hard disk
		/// </summary>
		/// <param name="p_sXml">Xml string containing report data</param>
		/// <param name="p_objMemory">Memory object containing report</param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
		public bool FileTranfer(string p_sFileContent,string p_sFileName)
		{			
			try
			{	
				if((p_sFileContent!="")&&(p_sFileName!=""))
				{
					char[] b64Data=p_sFileContent.ToCharArray();
					byte[] decodeData=Convert.FromBase64CharArray(b64Data,0,b64Data.Length);
					FileStream fs = new FileStream(p_sFileName, FileMode.Create, FileAccess.Write, FileShare.None);
					fs.Write(decodeData, 0, decodeData.Length);
					fs.Close();
					return true;
				}
				return false;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("UploadFile.FileTransfer.Error"),p_objException);
				return false;
			}
		}

		#endregion


	}
}
