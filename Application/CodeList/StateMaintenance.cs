﻿
using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Db;

namespace Riskmaster.Application.StateMaintenance
{
	///************************************************************** 
	///* $File		: StateMaintenance.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 28-Dec-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to fimplements the functionality to save/edit new states.
	/// </summary>
	public class StateMaintenance
	{
		private string m_sConnectString="";
        private int m_iClientId = 0;
        private string m_sUserName = "";
        string m_sDateTimeStamp = Common.Conversion.ToDbDateTime(DateTime.Now);
        public StateMaintenance(string p_sConnString, int p_iClientId, string p_sUserName)
		{
            m_iClientId = p_iClientId;
			m_sConnectString = p_sConnString;
            m_sUserName = p_sUserName;
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 12/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete the specified record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data to save it</param>
		/// <returns>Saved data alogn with its xml structure</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDocument)
		{
			string sRowId="";
			XmlElement objElm=null;
			DbConnection objConn=null;
			string sSQL="";
           
			try
			{
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='StatesGrid']");
				if(objElm!=null)
					sRowId= objElm.InnerText;

				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();

                if(sRowId!="")
				{
                    //Parijat : Mits 11262
                    //sSQL="DELETE FROM STATES WHERE STATE_ROW_ID="+sRowId;
                    //Adding tracking date-time columns

                    //sSQL = "UPDATE STATES SET DELETED_FLAG = -1 WHERE STATE_ROW_ID=" + sRowId;
                    sSQL = "UPDATE STATES SET DELETED_FLAG = -1 , UPDATED_BY_USER = '" + m_sUserName + "' , DTTM_RCD_LAST_UPD = '" + m_sDateTimeStamp + "' WHERE STATE_ROW_ID=" + sRowId;
				}
				objConn.ExecuteNonQuery(sSQL);
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("StateMaintenance.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objElm=null;
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 12/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the current record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data to save it</param>
		/// <returns>Saved data alogn with its xml structure</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			string sRowId="";
			string sAbbr="";
			string sDesc="";
			string sWkLoss="";
			string sSQL="";
			string sFroi="";
            string sCountryId = string.Empty;  //skhare7 For R8 Multilingual
            	string sSQLCountry=string.Empty;
			int iNextUID=0;
            int iNextUIDCountry=0;
			XmlElement objElm=null;
            int iResult = 0;
			DbConnection objConn=null;
			try
			{
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
				if(objElm!=null)
					sRowId= objElm.InnerText;

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='StateAb']");
				if(objElm!=null)
					sAbbr= objElm.InnerText;

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='StateName']");
				if(objElm!=null)
					sDesc=objElm.InnerText;

                //vsharma65 MITS 23657
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CountryId']");
                if (objElm != null)
                    sCountryId = objElm.GetAttribute("codeid");


				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='lstWrkLoss']");
				if(objElm!=null)
					sWkLoss=objElm.GetAttribute("value");

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Froi']");
				if(objElm!=null)
					sFroi=objElm.InnerText;
				//MITS_7125 & & 7124 Umesh
				if (sFroi=="")
					sFroi="0";
				
				//MITS_7125 & & 7124 Umesh
				
				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
                if (sRowId != string.Empty)
                {
                    sSQL = "SELECT COUNT(CSTATE_ROW_ID) FROM COUNTRY_X_STATE WHERE STATE_ROW_ID=" + sRowId;
                     iResult = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);
                }


                iNextUIDCountry = Utilities.GetNextUID(m_sConnectString, "COUNTRY_X_STATE", m_iClientId);

                if (sRowId != "")
                { //vsharma65 MITS 23657 //skhare7 For R8 Multilingual
                    //JIRA rmA-12160 Raman Bhatia : Start
                    //Adding tracking date-time columns
                    //sSQL = "UPDATE STATES SET DUR_CALC_OPTION='" + sWkLoss + "',FROI_COPIES='" + sFroi + "'" +
                    //    ",STATE_ID='" + sAbbr + "',STATE_NAME='" + sDesc + "' WHERE STATE_ROW_ID=" + sRowId;
                    sSQL = "UPDATE STATES SET DUR_CALC_OPTION='" + sWkLoss + "',FROI_COPIES='" + sFroi + "'" +
                        ",STATE_ID='" + sAbbr + "',STATE_NAME='" + sDesc + "' ,UPDATED_BY_USER='" + m_sUserName + "' , DTTM_RCD_LAST_UPD='" + m_sDateTimeStamp + "' WHERE STATE_ROW_ID=" + sRowId;
                    if(iResult<=0)
                        sSQLCountry = "INSERT INTO COUNTRY_X_STATE (CSTATE_ROW_ID, COUNTRY_ID, STATE_ROW_ID)" +
                  " VALUES(" + iNextUIDCountry.ToString() + ",'" + sCountryId + "'," + sRowId + ")";
                    else
                    sSQLCountry = "UPDATE COUNTRY_X_STATE SET COUNTRY_ID=" + sCountryId + " WHERE STATE_ROW_ID=" + sRowId;
                        
                }
                else
                {
                    //JIRA rmA-12160 Raman Bhatia : Start
                    //Adding tracking date-time columns
                    iNextUID = Utilities.GetNextUID(m_sConnectString, "STATES", m_iClientId);
                    //sSQL = "INSERT INTO STATES (STATE_ROW_ID, STATE_ID, STATE_NAME, EMPLOYER_LIABILITY, MAX_CLAIM_AMOUNT, RATING_METHOD_CODE, USL_LIMIT, WEIGHTING_STEP_VAL, DELETED_FLAG, DUR_CALC_OPTION, AWW_FORM_OPTION, FROI_COPIES)" +
                    //    " VALUES(" + iNextUID.ToString() + ",'" + sAbbr + "','" + sDesc + "',0,0,0,0,0,0," + sWkLoss + ",0," + sFroi + ")";//skhare7 R8 Multilingual
                    
                    //Issue fixes RMA-19571 nshah28 starts
                    sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID= '" + sAbbr + "' AND DELETED_FLAG=-1";
                    iResult = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL),m_iClientId);
                    if (iResult > 0)
                    {
                        sSQL = "UPDATE STATES SET DUR_CALC_OPTION='" + sWkLoss + "',FROI_COPIES='" + sFroi + "'" +
                       ",STATE_ID='" + sAbbr + "',STATE_NAME='" + sDesc + "' ,UPDATED_BY_USER='" + m_sUserName + "' , DTTM_RCD_LAST_UPD='" + m_sDateTimeStamp + "', DELETED_FLAG=0 WHERE DELETED_FLAG=-1 AND STATE_ROW_ID=" + iResult;
                    }
                    else //Issue fixes RMA-19571 nshah28 end
                    sSQL = "INSERT INTO STATES (STATE_ROW_ID, STATE_ID, STATE_NAME, EMPLOYER_LIABILITY, MAX_CLAIM_AMOUNT, RATING_METHOD_CODE, USL_LIMIT, WEIGHTING_STEP_VAL, DELETED_FLAG, DUR_CALC_OPTION, AWW_FORM_OPTION, FROI_COPIES, ADDED_BY_USER , DTTM_RCD_ADDED , UPDATED_BY_USER , DTTM_RCD_LAST_UPD)" +
                        " VALUES(" + iNextUID.ToString() + ",'" + sAbbr + "','" + sDesc + "',0,0,0,0,0,0," + sWkLoss + ",0,'" + sFroi + "','" + m_sUserName + "','" + m_sDateTimeStamp + "','" + m_sUserName + "','" + m_sDateTimeStamp + "')";
                 
                    sSQLCountry = "INSERT INTO COUNTRY_X_STATE (CSTATE_ROW_ID, COUNTRY_ID, STATE_ROW_ID)"  +
                    " VALUES(" + iNextUIDCountry.ToString() + ",'" + sCountryId + "'," + iNextUID.ToString() + ")";
                }

                objConn.ExecuteNonQuery(sSQL);
                     objConn.ExecuteNonQuery(sSQLCountry);
                return p_objXmlDocument;

			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("StateMaintenance.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objElm=null;
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}
		/// Name		: StateMaintenance
		/// Author		: Parag Sarin
		/// Date Created: 12/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for the Selected State
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data and structure</param>
		/// <returns>xml structure and data</returns>
		public XmlDocument GetSelectedStateInfo(XmlDocument p_objXmlDocument)
		{
			XmlElement objElement=null;
			XmlElement objOptionsElement=null;
			string sSQL=string.Empty;
			DbReader objRead=null;
			string sStateName="";
            string sCountryName = "";

			try
			{
				
				objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");  
				if (objElement!=null)
				{
					if (objElement.InnerText !=null)
					{
						if (objElement.InnerText.Trim() !="")
						{
						//mbahl3 mits 31382
                            eDatabaseType DbType = DbFactory.GetDatabaseType(m_sConnectString);
                            if (DbType != eDatabaseType.DBMS_IS_ORACLE)
                            {
                                sSQL = "SELECT DUR_CALC_OPTION,FROI_COPIES,STATES.STATE_ROW_ID,STATE_ID,STATE_NAME, COUNTRY_X_STATE.COUNTRY_ID COUNTRYID, (select SHORT_CODE+' '+CODE_DESC  from CODES_TEXT where CODE_ID =COUNTRY_X_STATE.COUNTRY_ID)  COUNTRY_NAME " +
                     " FROM STATES LEFT OUTER JOIN COUNTRY_X_STATE ON STATES.STATE_ROW_ID=COUNTRY_X_STATE.STATE_ROW_ID WHERE STATES.STATE_ROW_ID =" + objElement.InnerText;
                            }
                            else
                            {
                                sSQL = "SELECT DUR_CALC_OPTION,FROI_COPIES,STATES.STATE_ROW_ID,STATE_ID,STATE_NAME, COUNTRY_X_STATE.COUNTRY_ID COUNTRYID, (select SHORT_CODE||' '||CODE_DESC  from CODES_TEXT where CODE_ID =COUNTRY_X_STATE.COUNTRY_ID)  COUNTRY_NAME " +
                " FROM STATES LEFT OUTER JOIN COUNTRY_X_STATE ON STATES.STATE_ROW_ID=COUNTRY_X_STATE.STATE_ROW_ID WHERE STATES.STATE_ROW_ID =" + objElement.InnerText;
                            }



                            //sSQL = "SELECT DUR_CALC_OPTION,FROI_COPIES,STATE_ROW_ID,STATE_ID,STATE_NAME,(select SHORT_CODE+' '+CODE_DESC  from CODES_TEXT where CODE_ID =COUNTRY_ID) as COUNTRY_NAME FROM STATES WHERE STATE_ROW_ID =" + objElement.InnerText;
                //            sSQL = "SELECT DUR_CALC_OPTION,FROI_COPIES,STATES.STATE_ROW_ID,STATE_ID,STATE_NAME, COUNTRY_X_STATE.COUNTRY_ID COUNTRYID, (select SHORT_CODE+' '+CODE_DESC  from CODES_TEXT where CODE_ID =COUNTRY_X_STATE.COUNTRY_ID)  COUNTRY_NAME " +
                //" FROM STATES LEFT OUTER JOIN COUNTRY_X_STATE ON STATES.STATE_ROW_ID=COUNTRY_X_STATE.STATE_ROW_ID WHERE STATES.STATE_ROW_ID =" + objElement.InnerText;
                            objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);
							while (objRead.Read())
							{
								objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='lstWrkLoss']");	
								objElement.Attributes["value"].Value=Conversion.ConvertObjToStr(objRead.GetValue("DUR_CALC_OPTION"));
								objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='StateAb']");  
								objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("STATE_ID"));
								objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='StateName']");  
								sStateName=Conversion.ConvertObjToStr(objRead.GetValue("STATE_NAME"));
								objElement.InnerText=sStateName;
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CountryId']");
                                sCountryName = Conversion.ConvertObjToStr(objRead.GetValue("COUNTRY_NAME"));
                                objElement.InnerText = sCountryName;
                                objElement.SetAttribute("codeid", Conversion.ConvertObjToStr(objRead.GetValue("COUNTRYID")));
                                //mbahl3 mits 31382
								objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Froi']");  
								objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("FROI_COPIES"));
							}
							if (!objRead.IsClosed)
							{
								objRead.Close();
							}
							objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//group");
							objElement.Attributes["title"].Value="States ["+sStateName+"]";
						}
					}
				}

                // atavaragiri :MITS 23657
               // objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CountryId']");
                //objElement.SetAttribute("codeid", "0");
                // MITS 23657
				objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='lstWrkLoss']");  
                objOptionsElement = p_objXmlDocument.CreateElement("option");
                objOptionsElement.InnerText = "0 - " + Globalization.GetString("CodesListManager.WorkDays", m_iClientId);
				objOptionsElement.SetAttribute("value","0");
				objElement.AppendChild(objOptionsElement);
				objOptionsElement = p_objXmlDocument.CreateElement("option");
                objOptionsElement.InnerText = "1 - " + Globalization.GetString("CodesListManager.Use7Workweek", m_iClientId);
				objOptionsElement.SetAttribute("value","1");
				objElement.AppendChild(objOptionsElement);
				objOptionsElement = p_objXmlDocument.CreateElement("option");
                objOptionsElement.InnerText = "2 - " + Globalization.GetString("CodesListManager.Use5Workweek", m_iClientId);
				objOptionsElement.SetAttribute("value","2");
				objElement.AppendChild(objOptionsElement);
                //Mona:Mits: 23626
                objOptionsElement = p_objXmlDocument.CreateElement("option");
                objOptionsElement.InnerText = "3 - " + Globalization.GetString("CodesListManager.Use6Workweek",m_iClientId);
                objOptionsElement.SetAttribute("value", "3");
                objElement.AppendChild(objOptionsElement);
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("StateMaintenance.GetSelectedStateInfo.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objRead!=null)
				{
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
					objRead=null;
				}
			}
			//INSERT INTO [Wizard_Connect_Noida].[dbo].[STATES]([STATE_ROW_ID], [STATE_ID], [STATE_NAME], [EMPLOYER_LIABILITY], [MAX_CLAIM_AMOUNT], [RATING_METHOD_CODE], [USL_LIMIT], [WEIGHTING_STEP_VAL], [DELETED_FLAG], [DUR_CALC_OPTION], [AWW_FORM_OPTION], [FROI_COPIES])
		}
		/// Name		: StateMaintenance
		/// Author		: Parag Sarin
		/// Date Created: 12/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for the States list
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data and structure</param>
		/// <returns>xml structure and data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlElement objElement=null;
			string sSQL=string.Empty;
			DbReader objRead=null;
			XmlElement objLstRow=null;
			XmlElement objRowTxt=null;
			try
			{
                //Deb 
                eDatabaseType DbType = DbFactory.GetDatabaseType(m_sConnectString);
                if (DbType != eDatabaseType.DBMS_IS_ORACLE)
                {
                    sSQL = "SELECT STATES.STATE_ROW_ID,STATE_ID,STATE_NAME, COUNTRY_X_STATE.COUNTRY_ID, (select SHORT_CODE+' '+CODE_DESC  from CODES_TEXT where CODE_ID =COUNTRY_X_STATE.COUNTRY_ID)  COUNTRY_NAME " +
                    " FROM STATES LEFT OUTER JOIN COUNTRY_X_STATE ON STATES.STATE_ROW_ID=COUNTRY_X_STATE.STATE_ROW_ID WHERE DELETED_FLAG !=-1" +
                            " AND STATES.STATE_ROW_ID >0 ORDER BY STATE_ID";
                }
                else
                {
                    sSQL = "SELECT STATES.STATE_ROW_ID,STATE_ID,STATE_NAME, COUNTRY_X_STATE.COUNTRY_ID, (select SHORT_CODE||' '||CODE_DESC  from CODES_TEXT where CODE_ID =COUNTRY_X_STATE.COUNTRY_ID)  COUNTRY_NAME " +
                   " FROM STATES LEFT OUTER JOIN COUNTRY_X_STATE ON STATES.STATE_ROW_ID=COUNTRY_X_STATE.STATE_ROW_ID WHERE DELETED_FLAG !=-1" +
                           " AND STATES.STATE_ROW_ID >0 ORDER BY STATE_ID";
                }
                //Deb 
				objRead=DbFactory.GetDbReader(m_sConnectString,sSQL);
				objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//StatesList");  
				while (objRead.Read())
				{
					objLstRow = p_objXmlDocument.CreateElement("listrow");
					objRowTxt = p_objXmlDocument.CreateElement("Abbreviation");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("STATE_ID"));
					objLstRow.AppendChild(objRowTxt);
                   
                    objRowTxt = p_objXmlDocument.CreateElement("countryid");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("COUNTRY_ID"));
                    objLstRow.AppendChild(objRowTxt);
                    objRowTxt = p_objXmlDocument.CreateElement("CountryName");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("COUNTRY_NAME"));
					objLstRow.AppendChild(objRowTxt);
					objRowTxt = p_objXmlDocument.CreateElement("Desc");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("STATE_NAME"));
					objLstRow.AppendChild(objRowTxt);
                    objRowTxt = p_objXmlDocument.CreateElement("RowId");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("STATE_ROW_ID"));
                    objLstRow.AppendChild(objRowTxt);
					objElement.AppendChild(objLstRow);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("StateMaintenance.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objRead=null;
			}
		}
		
	}
}
