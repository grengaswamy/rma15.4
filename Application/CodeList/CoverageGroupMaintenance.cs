﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.Application.CodeUtility;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using Riskmaster.Application.FundManagement;
namespace Riskmaster.Application.CoverageGroupMaintenance
{
    ///************************************************************** 
    ///* $File		: CoverageGroupMaintenance.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 13-Aug-2013
    ///* $Author	: Nitin Goel
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to fimplements the functionality to save/edit new CoverageGroups.
    /// </summary>
    public class CoverageGroupMaintenance:IDisposable
    {
        private string m_sConnectString = string.Empty;
        private Security.UserLogin m_UserLogin = null;
        //Added by Nikhil.Code review changes
        /// Private variable to store the instance of Datamodel factory object.Added by Shivendu for MITS 11594
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;


        private int m_iClientId = 0;

        public CoverageGroupMaintenance(string p_sConnectionString, Security.UserLogin p_UserLogin, int p_iClientId)
        {
            m_sConnectString = p_sConnectionString;
            m_UserLogin = p_UserLogin;
          
            m_iClientId = p_iClientId;

            //Added by Nikhil.Code review changes
           
            m_objDataModelFactory = new DataModelFactory(p_UserLogin,m_iClientId);
        }

        #region Dispose
        /// <summary>
        /// Un Initialize the data model factory object. 
        /// </summary>
        public void Dispose()
        {
            try
            {
                // m_objDataModelFactory.UnInitialize();
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
            }
            catch
            {
                // Avoid raising any exception here.				
            }
        }

        #endregion
        /// Name		: Save
        /// Author		: Nitin Goel
        /// Date Created: 12/28/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Delete the specified record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            int iRowId = 0; ;
            XmlElement objElm = null;           
            string sSQL = string.Empty;
         
            CoverageGroup objCovGroup = null;
            bool bSuccess = false;
            try
            {
               // m_objDataModelFactory = new DataModelFactory(m_UserLogin);
                objCovGroup = m_objDataModelFactory.GetDataModelObject("CoverageGroup", false) as CoverageGroup;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CoverageGroupGrid']");
                if (objElm != null)                  
                iRowId = Conversion.CastToType<int>(objElm.InnerText, out bSuccess);            
                if (iRowId>0)
                {
                    objCovGroup.MoveTo(iRowId);
                    objCovGroup.DeletedFlag = true;
                    objCovGroup.Save();
                }               
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                
                if (objCovGroup != null)
                {
                    objCovGroup.Dispose();
                }
            }
        }



        /// Name		: Save
        /// Author		: Nitin Goel
        /// Date Created: 08/13/2013		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            int iRowId = 0;
            string sCovGroupCode = string.Empty;
            string sCovGroupDesc = string.Empty;
            string sTriggerDate = string.Empty;
            string sSQL = string.Empty;
            string sEffStartDate = string.Empty;
            string sEffEndDate = string.Empty;            
            int iNextUID = 0;          
            XmlElement objElm = null;           
         
            CoverageGroup objCovGroup = null;
            ClaimXPolDedAggLimit objClaimXPolDedAggLimit = null;
            int iClaimXPolDedAggLimitRowId=0;
            bool bPreviousSharedDeductibleFlag=false;
            bool bSuccess = false;
            int iDuplicate = 0;
            int iAssociatedCoverage = 0;
            int iReserveTypeAdded = 0;
            bool bUseSharedDeductible = false;

            int iStateRowId = 0;
            //Start - Change by Nikhil on 07/09/14
            string sReserveTypes = string.Empty;          
         
            string[] sReserveTypeList = new string[0];
            //End - Change by Nikhil on 07/09/14
            //Added by Nikhil.Code review changes
            System.Text.StringBuilder sbSQL = null;
            ClaimXPolDedAggLimit oClaimXPolDedAggLimit = null;
            int iClmXPolDedAggLmtId = 0;
            XmlNode xAggLimit = null;
            XmlNode xDedPerEvent = null;
            XmlNode xExcludeExpenseFlag = null;
            double dAggLimit = 0d;
            double dDedPerEvent = 0d;
            bool bExpenseFlag = false;
            bool bIsDataChanged = false;
            int iLangcode = 0;

            //MITS - 37595
            List<int> lstClmXPolDedIds = null;
            int iCurrentClaimXPolDedId = 0;
            ClaimXPolDed oClaimXPolDed = null;
            DeductibleManager objDedManager = null;
            //END - MITS - 37595
            bool bCoverageShortCodeChanged = false;
            try
            {
                iLangcode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                //commented by Nikhil.Code review changes
              //  objFacotry = new DataModelFactory(m_UserLogin);
                objCovGroup = m_objDataModelFactory.GetDataModelObject("CoverageGroup", false) as CoverageGroup;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupId']");
                if (objElm != null)
                    iRowId = Conversion.CastToType<int>(objElm.InnerText, out bSuccess);

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupCode']");
                if (objElm != null)
                    sCovGroupCode = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupDesc']");
                if (objElm != null)
                    sCovGroupDesc = objElm.InnerText;


                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='TriggerEffDate']");              
                if (objElm != null && objElm.Attributes["value"]!=null)
                {
                    if(string.Compare(objElm.GetAttribute("value"),"0")!=0)
                    sTriggerDate = objElm.GetAttribute("value");
                }        
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EffStartDate']");
                if (objElm != null)
                    sEffStartDate = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EffEndDate']");
                if (objElm != null)
                    sEffEndDate = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseSharedDeductible']");
                if (objElm != null)
                    bUseSharedDeductible = Conversion.CastToType<bool>(objElm.InnerText, out bSuccess);

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ApplicableState']");
                if (objElm != null)
                    iStateRowId = Conversion.CastToType<int>(objElm.GetAttribute("codeid"),out bSuccess);

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//ApplicableReserveType");
                if (objElm != null)
                    sReserveTypes = Convert.ToString(objElm.GetAttribute("codeid")).Trim();
                if (!string.IsNullOrEmpty(sReserveTypes))
                {
                    sReserveTypeList = sReserveTypes.Split(' ');

                }
                //Start - Added by Nikhil.


                xAggLimit = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AggregateLimit']");
                xExcludeExpenseFlag = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ExcludeExpenseFlag']");
                xDedPerEvent = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DedPerEvent']");


                oClaimXPolDedAggLimit = m_objDataModelFactory.GetDataModelObject("ClaimXPolDedAggLimit", false) as ClaimXPolDedAggLimit;
                if (iRowId > 0)
                {
                    iClmXPolDedAggLmtId = oClaimXPolDedAggLimit.Context.DbConnLookup.ExecuteInt(String.Format("SELECT {1} FROM {0} WHERE COV_GROUP_CODE ={2}", oClaimXPolDedAggLimit.Table, oClaimXPolDedAggLimit.KeyFieldName, iRowId));
                    if (iClmXPolDedAggLmtId > 0)
                    {
                        oClaimXPolDedAggLimit.MoveTo(iClmXPolDedAggLmtId);

                    }
                }
                if (xAggLimit != null && !string.IsNullOrEmpty(xAggLimit.InnerText))
                {
                    dAggLimit = Conversion.CastToType<double>(xAggLimit.InnerText, out bSuccess);
                    if (bSuccess && dAggLimit != oClaimXPolDedAggLimit.AggLimitAmt)
                    {
                        bIsDataChanged = true;

                    }
                }

                if (xDedPerEvent != null && !string.IsNullOrEmpty(xDedPerEvent.InnerText))
                {
                    dDedPerEvent = Conversion.CastToType<double>(xDedPerEvent.InnerText, out bSuccess);
                    if (bSuccess && dDedPerEvent != oClaimXPolDedAggLimit.SirDedPreventAmt)
                    {
                        bIsDataChanged = true;
                        if (!bUseSharedDeductible)
                        {
                            dDedPerEvent = 0.0;
                        }



                     
                    }
                }
                //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
                if (xExcludeExpenseFlag != null && !string.IsNullOrEmpty(xExcludeExpenseFlag.InnerText))
                {
                    bExpenseFlag = Conversion.CastToType<bool>(xExcludeExpenseFlag.InnerText, out bSuccess);
                    //MITS -37595
                    //if (bSuccess && !bool.Equals(bExpenseFlag, oClaimXPolDedAggLimit.ExcludeExpenseFlag))
                    //{
                    //    bIsDataChanged = true;


                    //}
                    //END - MITS -37595
                }
                //End - ExpenseFlagAddition - nbhatia6 - 07/03/14

                if (bIsDataChanged)
                {
                    //start - jira# rma-6292
                    if (dAggLimit < 0 || dDedPerEvent < 0)
                    {

                        throw new RMAppException(Globalization.GetString("DeductibleManager.AmountLessThanZero.Error", m_iClientId));
                    }
                    //End - jira# rma-6292
                    //Changed By Nikhil on 01/08/2014 for validate event shared deductible amount with group aggregate limit  - Start
                    if (dAggLimit > 0 && dDedPerEvent > dAggLimit)
                    {

                        throw new RMAppException(Globalization.GetString("DeductibleManager.EventAmountLimit.Error", m_iClientId));
                    }
                    //Changed By Nikhil on 01/08/2014 for validate event shared deductible amount with group aggregate limit  - Start


                }


                //End  - Added by Nikhil.
               if (iRowId > 0)
                {
                    //Changed by Nikhil.Code review changes
                  
                    sSQL = String.Format("SELECT COUNT(COVERAGE_GROUP_ID) FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID != {0} AND COV_GROUP LIKE '{1}' ", iRowId, sCovGroupCode.Trim());
                    iDuplicate = objCovGroup.Context.DbConnLookup.ExecuteInt(sSQL);
                    //To check if Coverage Group is already accociated with any claim's coverage.
                    //start - Changed by Nikhil.Code review changes

                    sbSQL = new System.Text.StringBuilder();
                   //MITS - 37595
                    //sbSQL.Append(" SELECT COUNT(*) FROM CLAIM_X_POL_DED CXPD INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID=PXIG.POLICY_X_INSLINE_GROUP_ID ");
                    sbSQL.Append(" SELECT CXPD.CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED CXPD INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID=PXIG.POLICY_X_INSLINE_GROUP_ID ");
                     sbSQL.Append(" INNER JOIN COVERAGE_GROUP CG ON CG.COVERAGE_GROUP_ID = PXIG.COV_GROUP_CODE WHERE CG.COVERAGE_GROUP_ID=").Append(iRowId);
                     //MITS - 37595
                   //iAssociatedCoverage = objCovGroup.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                     lstClmXPolDedIds = new List<int>();
                     using (DbReader oDbReader = objCovGroup.Context.DbConnLookup.ExecuteReader(sbSQL.ToString()))
                     {
                         while (oDbReader.Read())
                         {
                             iAssociatedCoverage++;
                             if (bool.Equals(bExpenseFlag, oClaimXPolDedAggLimit.ExcludeExpenseFlag))
                             {
                                 break;
                             }
                             iCurrentClaimXPolDedId = 0;
                             iCurrentClaimXPolDedId = oDbReader.GetInt32("CLM_X_POL_DED_ID");
                             lstClmXPolDedIds.Add(iCurrentClaimXPolDedId);
                         }
                     }



                    //end - Changed by Nikhil.Code review changes
                    if (!string.IsNullOrEmpty(sReserveTypes))
                    {
                        //Changed by Nikhil.Code review changes
                     
                        sSQL = String.Format("SELECT  COUNT (*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID ={0} AND RESERVE_TYPE_CODE IN ({1})", iRowId, sReserveTypes.Replace(' ', ','));
                        iReserveTypeAdded = objCovGroup.Context.DbConnLookup.ExecuteInt(sSQL);
                    }
               }
                else
                {
                    //Changed by Nikhil.Code review changes
                   
                    sSQL = String.Format("SELECT COUNT(COVERAGE_GROUP_ID) FROM COVERAGE_GROUP WHERE COV_GROUP LIKE '{0}' ", sCovGroupCode.Trim());
                    iDuplicate = objCovGroup.Context.DbConnLookup.ExecuteInt(sSQL);
                }
                if (iDuplicate > 0)
                {
                    //throw new RMAppException("Coverage Group already exists.");
                    //Cloud changes by Nikhil for NI
                    throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.Save.DuplicateCheckError",m_iClientId));

                }
                //End;  Check Duplicate Coverage Group code added
                if (iRowId > 0)
                {
                    objCovGroup.MoveTo(iRowId);
                    if (iAssociatedCoverage > 0)
                    {
                       
                        if (objCovGroup.UseSharedDedcutible != bUseSharedDeductible || objCovGroup.StateRowId != iStateRowId || sReserveTypeList.Length != iReserveTypeAdded || objCovGroup.ReserveTypesList.Count != iReserveTypeAdded || bIsDataChanged)
                       
                        {
                            //Cloud changes by Nikhil for NI
                            throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.Save.AccessRestricted",m_iClientId));
                        }
                        bPreviousSharedDeductibleFlag = objCovGroup.UseSharedDedcutible;

                        //Start - MITS - 37595

                        if(!bool.Equals(bExpenseFlag, oClaimXPolDedAggLimit.ExcludeExpenseFlag))
                        {
                            //TODO: Payment made validation will be executed.
                            objDedManager = new DeductibleManager(m_sConnectString,m_UserLogin,m_iClientId);
                            if (objDedManager.IsPaymentMade(iRowId))
                            {
                                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.NonEditable.AccessRestricted", m_iClientId));
                            }
                        
                        
                        }
                        //end - MITS - 37595
                    }
                    //Start: In order to resolve short code issue consistency in Coverage_text table., JIRA 7022
                    if (string.Compare(objCovGroup.CovGroup, sCovGroupCode, true) != 0)
                    {
                        bCoverageShortCodeChanged = true;
                    }
                    //End:JIRA 7022
                    objCovGroup.CovGroup = sCovGroupCode;     
                    objCovGroup.TriggerDateField = sTriggerDate;
                    objCovGroup.EffStartDate = sEffStartDate;
                    objCovGroup.EffEndDate = sEffEndDate;
                    objCovGroup.StateRowId = iStateRowId;
                    objCovGroup.UseSharedDedcutible = bUseSharedDeductible;
                    objCovGroup.ReserveTypesList.ClearAll();
                    if (sReserveTypeList != null)
                    {
                        for (int i = 0; i <= sReserveTypeList.Length - 1; i++)
                        {
                            objCovGroup.ReserveTypesList.Add(sReserveTypeList[i]);
                        }
                    }
                    objCovGroup.Save();
                    InsertCodeDescription(iRowId, sCovGroupCode, sCovGroupDesc, iLangcode);
                    //Start: In order to resolve short code issue consistency in Coverage_text table., JIRA 7022
                    if (bCoverageShortCodeChanged)
                    {
                        m_objDataModelFactory.Context.DbConnLookup.ExecuteScalar(string.Format("UPDATE COVERAGE_GROUP_TEXT SET COV_GROUP= '{0}' WHERE COVERAGE_GROUP_ID = {1}",sCovGroupCode,iRowId));
                    }
                    //End:JIRA 7022
                    //Start - Added by Nikhil.DedAmount added
                    oClaimXPolDedAggLimit.CovGroupId = objCovGroup.CoverageGroupId;
                    oClaimXPolDedAggLimit.UpdatedByUser = oClaimXPolDedAggLimit.Context.RMUser.LoginName;
                    oClaimXPolDedAggLimit.SirDedPreventAmt = dDedPerEvent;
                    oClaimXPolDedAggLimit.ExcludeExpenseFlag = bExpenseFlag;
                    oClaimXPolDedAggLimit.AggLimitAmt = dAggLimit;
                    oClaimXPolDedAggLimit.Save();


                    //Start - MITS - 37595

                    if (lstClmXPolDedIds.Count > 0)
                    {
                        foreach (int iClaimXPolDedId in lstClmXPolDedIds)
                        {
                            oClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
                            oClaimXPolDed.MoveTo(iClaimXPolDedId);
                            oClaimXPolDed.ExcludeExpenseFlag = bExpenseFlag;
                            oClaimXPolDed.Save();
                        }
                    }
                    //end - MITS - 37595
                }
                //end:added by nitin goel
                else
                {
                    iNextUID = Utilities.GetNextUID(m_sConnectString, "COVERAGE_GROUP", m_iClientId);
                    objCovGroup.CoverageGroupId = iNextUID;
                    objCovGroup.CovGroup = sCovGroupCode;
                  //  objCovGroup.CovGroupDesc = sCovGroupDesc;
                    objCovGroup.TriggerDateField = sTriggerDate;
                    objCovGroup.EffStartDate = sEffStartDate;
                    objCovGroup.EffEndDate = sEffEndDate;
                    objCovGroup.StateRowId = iStateRowId;
                    objCovGroup.UseSharedDedcutible = bUseSharedDeductible;
                    if (sReserveTypeList != null)
                    {
                        for (int i = 0; i <= sReserveTypeList.Length - 1; i++)
                        {
                            objCovGroup.ReserveTypesList.Add(sReserveTypeList[i]);
                        }
                    }
                    objCovGroup.Save();
                    InsertCodeDescription(objCovGroup.CoverageGroupId, sCovGroupCode, sCovGroupDesc, iLangcode);
                    //Start - Added by Nikhil.DedAmount added
                    oClaimXPolDedAggLimit.CovGroupId = objCovGroup.CoverageGroupId;
                    oClaimXPolDedAggLimit.AddedByUser = oClaimXPolDedAggLimit.Context.RMUser.LoginName;
                    oClaimXPolDedAggLimit.SirDedPreventAmt = dDedPerEvent;
                    oClaimXPolDedAggLimit.ExcludeExpenseFlag = bExpenseFlag;
                    oClaimXPolDedAggLimit.AggLimitAmt = dAggLimit;
                    oClaimXPolDedAggLimit.Save();


                }
                return p_objXmlDocument;

            }
            catch (RMAppException)
            {
                throw;
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.Save.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                //if (m_objDataModelFactory != null)
                //{
                //    m_objDataModelFactory.Dispose();
                //}
                if (objCovGroup != null)
                {
                    objCovGroup.Dispose();
                }
                if (objClaimXPolDedAggLimit!=null)
                {
                 objClaimXPolDedAggLimit.Dispose();
                } 
            }
        }
        /// Name		: CoverageGroupMaintenance
        /// Author		: Nitin Goel
        /// Date Created: 08/13/2013		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the Selected Coverage Group
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument GetSelectedCoverageGroupInfo(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            XmlElement objElemTemp = null;
            int iCovGroupId = 0;
            string sSQL = string.Empty;                 
            bool bSuccess = false;
            int iStateRowId=0;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sReserveTypeCode = string.Empty;
            string sReserveTypeDesc = string.Empty;
            int iReserveTypeCodeId = 0;
            LocalCache objLocalCache = null;
            string sReserveTypes = string.Empty;
     //       DataModelFactory objFacotry = null;
            CoverageGroup objCovGroup = null;
            string[] sReserveTypeList = null;
            //Start -  Added by Nikhil.Shared ded amount and limit.
            ClaimXPolDedAggLimit oClaimXPolDedAggLimit = null;
            int iClmXPolDedAggLmtId = 0;
            //end -  Added by Nikhil.Shared ded amount and limit.
            int iLangcode = 0;
            string sCodeDesc = string.Empty;
            try
            {
                objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupId']");
                if (objElement != null)
                {
                    iCovGroupId = Conversion.CastToType<int>(objElement.InnerText, out bSuccess);
                    if (iCovGroupId > 0)
                    {                       
                        //Added by nitin goel, for Disability type code and loss type code                        
                      
                        //Added by nikhil.Multilingual changes
                        iLangcode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                        sCodeDesc = m_objDataModelFactory.Context.DbConnLookup.ExecuteString(String.Format("SELECT GROUP_DESCRIPTION FROM COVERAGE_GROUP_TEXT WHERE COVERAGE_GROUP_ID = {0} AND LANGUAGE_CODE = {1}",iCovGroupId,iLangcode));
                        //End
                        objCovGroup = m_objDataModelFactory.GetDataModelObject("CoverageGroup", false) as CoverageGroup;
                        objCovGroup.MoveTo(iCovGroupId);
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupCode']");
                        objElement.InnerText = Convert.ToString(objCovGroup.CovGroup);
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupDesc']");
                        //cHANGED BY nIKHIL.mULTILINGUAL CHANGES
                        
                        objElement.InnerText = sCodeDesc;
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='TriggerEffDate']");
                        objElement.SetAttribute("value", Convert.ToString(objCovGroup.TriggerDateField));
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EffStartDate']");
                        objElement.InnerText = Conversion.GetUIDate(Convert.ToString(objCovGroup.EffStartDate),m_UserLogin.objUser.NlsCode.ToString(),m_iClientId);
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EffEndDate']");
                        objElement.InnerText = Conversion.GetUIDate(Convert.ToString(objCovGroup.EffEndDate), m_UserLogin.objUser.NlsCode.ToString(), m_iClientId);
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseSharedDeductible']");
                        objElement.InnerText = Convert.ToString(objCovGroup.UseSharedDedcutible);
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ApplicableState']");
                        iStateRowId = Conversion.CastToType<int>(Convert.ToString(objCovGroup.StateRowId), out bSuccess);
                        objLocalCache.GetStateInfo(iStateRowId, ref sStateCode, ref sStateDesc);
                        objElement.SetAttribute("codeid", Convert.ToString(iStateRowId));
                        //changed by nikhil.code review changes
                        
                        objElement.InnerText =String.Concat(sStateCode , " " , sStateDesc);
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//ApplicableReserveType");
                        sReserveTypes = Convert.ToString(objCovGroup.ReserveTypesList);
                        objElement.SetAttribute("codeid", sReserveTypes);
                        sReserveTypeList = sReserveTypes.Split(' ');
                        for (int i = 0; i <= sReserveTypeList.Length - 1; i++)
                        {
                            objElemTemp = p_objXmlDocument.CreateElement("Types");
                            objElemTemp.SetAttribute("value", sReserveTypeList[i]);
                            iReserveTypeCodeId = Conversion.CastToType<int>(sReserveTypeList[i], out bSuccess);
                            objLocalCache.GetCodeInfo(iReserveTypeCodeId, ref sReserveTypeCode,ref sReserveTypeDesc);
                            //changed by nikhil.code review changes
                           
                            objElemTemp.InnerText = String.Concat(sReserveTypeCode , " " , sReserveTypeDesc);
                            objElement.AppendChild(objElemTemp);
                        }
                        //end by nitin goel
                        //Start -  Added by Nikhil.Shared ded amount and limit.
                        oClaimXPolDedAggLimit = m_objDataModelFactory.GetDataModelObject("ClaimXPolDedAggLimit", false) as ClaimXPolDedAggLimit;

                        iClmXPolDedAggLmtId = oClaimXPolDedAggLimit.Context.DbConnLookup.ExecuteInt(String.Format("SELECT {1} FROM {0} WHERE COV_GROUP_CODE ={2}", oClaimXPolDedAggLimit.Table, oClaimXPolDedAggLimit.KeyFieldName, iCovGroupId));
                        if (iClmXPolDedAggLmtId > 0)
                        {
                            oClaimXPolDedAggLimit.MoveTo(iClmXPolDedAggLmtId);

                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DedPerEvent']");
                            objElement.InnerText = Convert.ToString(oClaimXPolDedAggLimit.SirDedPreventAmt);
                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AggregateLimit']");
                            objElement.InnerText = Convert.ToString(oClaimXPolDedAggLimit.AggLimitAmt);
                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ExcludeExpenseFlag']");
                            objElement.InnerText = Convert.ToString(oClaimXPolDedAggLimit.ExcludeExpenseFlag);
                        }


                        //End -   Added by Nikhil.Shared ded amount and limit.
                    }
                    p_objXmlDocument = FillComboWithCodes(p_objXmlDocument);
                }

               

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.GetSelectedCoverageGroupInfo.Error",m_iClientId), p_objEx);
            }
            finally
            {
             
                
                if (objCovGroup != null)
                    objCovGroup.Dispose();
            }
        }
        /// Name		: CoverageGroupMaintenance
        /// Author		: Nitin Goel
        /// Date Created: 08/13/2013		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the Coverage Group list
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;
            DbReader objRead = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            string sTriggerValue=string.Empty;
            StringBuilder sbSQL = null;
            int iLangcode = 0;
            try
            {
                //start  - Added by Nikhil.Multilingual changes
                sbSQL = new StringBuilder();
                iLangcode =  Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);

                sbSQL.Append("SELECT CG.COVERAGE_GROUP_ID,CG.COV_GROUP,CGT.GROUP_DESCRIPTION,CG.TRIGGER_DATE_FIELD,CG.EFF_START_DATE,CG.EFF_END_DATE");
                sbSQL.Append(" FROM COVERAGE_GROUP CG INNER JOIN COVERAGE_GROUP_TEXT CGT ON CG.COVERAGE_GROUP_ID = CGT.COVERAGE_GROUP_ID ");
                sbSQL.Append(" WHERE CGT.LANGUAGE_CODE = ").Append(iLangcode);
                sbSQL.Append(" AND CG.DELETED_FLAG != -1");
                //end  - Added by Nikhil.Multilingual changes
               
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//CoverageGroupList");
                // changed by Nikhil.Multilingual changes
               
                using (objRead = DbFactory.ExecuteReader(m_sConnectString,sbSQL.ToString()))
                  {
                            while (objRead.Read())
                            {
                                objLstRow = p_objXmlDocument.CreateElement("listrow");
                                objRowTxt = p_objXmlDocument.CreateElement("CovGroupCode");
                                objRowTxt.InnerText = Convert.ToString(objRead.GetValue("COV_GROUP"));
                                objLstRow.AppendChild(objRowTxt);
                                objRowTxt = p_objXmlDocument.CreateElement("CovGroupDesc");
                        objRowTxt.InnerText = Convert.ToString(objRead.GetValue("GROUP_DESCRIPTION"));
                                objLstRow.AppendChild(objRowTxt);
                                objRowTxt = p_objXmlDocument.CreateElement("TriggerEffDate");
                                sTriggerValue = Convert.ToString(objRead.GetValue("TRIGGER_DATE_FIELD"));
                                switch (sTriggerValue)
                                {
                                    case "SYSTEM_DATE":
                                        objRowTxt.InnerText = "Current System Date";
                                        break;
                                    case "EVENT.DATE_OF_EVENT":
                                        objRowTxt.InnerText = "Event Date";
                                        break;
                                    case "CLAIM.DATE_OF_CLAIM":
                                         objRowTxt.InnerText = "Claim Date";
                                        break;
                                    case "POLICY.EFFECTIVE_DATE":
                                         objRowTxt.InnerText = "Policy Effective Date";
                                        break;
                                    default :
                                         objRowTxt.InnerText = "";
                                        break;
                                }
                                
                                objLstRow.AppendChild(objRowTxt);
                                objRowTxt = p_objXmlDocument.CreateElement("EffStartDate");
                                objRowTxt.InnerText = Conversion.GetUIDate(Convert.ToString(objRead.GetValue("EFF_START_DATE")),m_UserLogin.objUser.NlsCode.ToString(), m_iClientId);
                                objLstRow.AppendChild(objRowTxt);
                                objRowTxt = p_objXmlDocument.CreateElement("EffEndDate");
                                objRowTxt.InnerText = Conversion.GetUIDate(Convert.ToString(objRead.GetValue("EFF_END_DATE")), m_UserLogin.objUser.NlsCode.ToString(), m_iClientId);
                                objLstRow.AppendChild(objRowTxt);
                                objRowTxt = p_objXmlDocument.CreateElement("CovGroupId");
                                objRowTxt.InnerText = Convert.ToString(objRead.GetValue("COVERAGE_GROUP_ID"));
                                objLstRow.AppendChild(objRowTxt);
                           
                                
                                objElement.AppendChild(objLstRow);
                            }
                  }
                               
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.Get.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (!objRead.IsClosed)
                   objRead.Close();   
                if(objRead!=null)
                objRead.Dispose();
            }

        }

        /// <summary>
        /// Author: Neha Suresh Jain
        /// Date: 08/22/2013
        /// Fucntion to fill the trigger date combo 
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument FillComboWithCodes(XmlDocument p_objXmlDoc)
        {
            XmlNode noValueNode = p_objXmlDoc.SelectSingleNode("//control[@name='triggerDate']");
            XmlNode xSelected = p_objXmlDoc.SelectSingleNode("//control[@name='triggerDateSelected']");
            string sXValue = string.Empty;
            try
            {
                if (xSelected != null)
                {
                    sXValue = xSelected.InnerText;
                }
                if (noValueNode != null)
                {
                    if (string.IsNullOrEmpty(noValueNode.InnerXml))
                    {
                        XmlElement noValue = p_objXmlDoc.CreateElement("option");
                        noValue.SetAttribute("value", "0");
                        noValue.InnerText = string.Empty;
                        noValueNode.AppendChild(noValue);
                        noValue = null;

                        //Current System Date details
                        noValue = p_objXmlDoc.CreateElement("option");
                        noValue.SetAttribute("value", "SYSTEM_DATE");
                        noValue.InnerText = "Current System Date";
                        //changed by nikhil.code review changes
                       
                        if (String.Compare(sXValue.ToUpper().Trim(),"SYSTEM_DATE",true) == 0)
                        {
                            noValue.SetAttribute("selected", "1");
                        }
                        noValueNode.AppendChild(noValue);
                        noValue = null;

                        //Event Date Details
                        noValue = p_objXmlDoc.CreateElement("option");
                        noValue.SetAttribute("value", "EVENT.DATE_OF_EVENT");
                        noValue.InnerText = "Event Date";
                        //changed by nikhil.code review changes
                       
                        if (String.Compare(sXValue.ToUpper().Trim(),"EVENT.DATE_OF_EVENT",true) == 0 )
                        {
                            noValue.SetAttribute("selected", "1");
                        }
                        noValueNode.AppendChild(noValue);
                        noValue = null;

                        //Claim Date Details
                        noValue = p_objXmlDoc.CreateElement("option");
                        noValue.SetAttribute("value", "CLAIM.DATE_OF_CLAIM");
                        noValue.InnerText = "Claim Date";
                        //changed by nikhil.code review changes
                        
                        if (String.Compare(sXValue.ToUpper().Trim(),"CLAIM.DATE_OF_CLAIM",true) == 0)
                        {
                            noValue.SetAttribute("selected", "1");
                        }
                        noValueNode.AppendChild(noValue);
                        noValue = null;

                        
                        noValue = p_objXmlDoc.CreateElement("option");
                        noValue.SetAttribute("value", "POLICY.EFFECTIVE_DATE");
                        noValue.InnerText = "Policy Effective Date";
                        //changed by nikhil.code review changes
                       
                        if (String.Compare(sXValue.ToUpper().Trim(),"POLICY.EFFECTIVE_DATE",true) == 0)
                        {
                            noValue.SetAttribute("selected", "1");
                        }
                        noValueNode.AppendChild(noValue);
                        noValue = null;
                    }
                }
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.FillComboWithCodes.Error",m_iClientId), p_objEx);
            }
            finally
            {
            }
        }


        /// Name		: CoverageGroupMaintenance
        /// Author		: Nikhil Bhatia
        /// Date Created: 10/20/2014		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Multilingual changes
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument GetAdditionalLangCode(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;
            DbReader objRead = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            string sTriggerValue = string.Empty;
            StringBuilder sbSQL = null;
            int iLangCode = 0;
            UtilityDriver objUtilityDriver = null;
            bool bSuccess = false;
            int iCovGroupID = 0;
            try
            {
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//CovGroupId");
                if (objElement!= null)
                {
                    if (!string.IsNullOrEmpty(objElement.InnerText))
                    {
                        iCovGroupID = Conversion.ConvertStrToInteger(objElement.InnerText);
                    }
                }

                objUtilityDriver = new UtilityDriver(m_sConnectString, m_iClientId);
                iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                sbSQL = new StringBuilder();

                sbSQL.Append("SELECT CG.COVERAGE_GROUP_ID, CG.COV_GROUP, CGT.GROUP_DESCRIPTION,CGT.LANGUAGE_CODE,");
                sbSQL.Append(" ROW_NUMBER() over(order by CG.COV_GROUP) AS rno ");
                sbSQL.Append(" FROM COVERAGE_GROUP CG, COVERAGE_GROUP_TEXT CGT WHERE CG.COVERAGE_GROUP_ID = CGT.COVERAGE_GROUP_ID");
                sbSQL.Append(" AND (CG.DELETED_FLAG <> -1").Append(" OR CG.DELETED_FLAG <> NULL)");
                sbSQL.Append(" AND CG.COVERAGE_GROUP_ID =").Append(iCovGroupID);
                sbSQL.Append(" AND CGT.LANGUAGE_CODE <>").Append(iLangCode);

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//CoverageGroupList");
                using (objRead = DbFactory.ExecuteReader(m_sConnectString, sbSQL.ToString()))
                {
                    while (objRead.Read())
                    {
                        objLstRow = p_objXmlDocument.CreateElement("listrow");
                        objRowTxt = p_objXmlDocument.CreateElement("CovGroupCode");
                        objRowTxt.InnerText = Convert.ToString(objRead.GetValue("COV_GROUP"));
                        objLstRow.AppendChild(objRowTxt);
                        objRowTxt = p_objXmlDocument.CreateElement("CovGroupDesc");
                        objRowTxt.InnerText = Convert.ToString(objRead.GetValue("GROUP_DESCRIPTION"));
                        objLstRow.AppendChild(objRowTxt);
                                              
                       
                        objRowTxt = p_objXmlDocument.CreateElement("CovGroupId");
                        objRowTxt.InnerText = Convert.ToString(objRead.GetValue("COVERAGE_GROUP_ID"));
                        objLstRow.AppendChild(objRowTxt);


                        objRowTxt = p_objXmlDocument.CreateElement("languageCode");
                        objRowTxt.InnerText = Convert.ToString(objRead.GetValue("LANGUAGE_CODE"));
                        objLstRow.AppendChild(objRowTxt);

                        objRowTxt = p_objXmlDocument.CreateElement("languageDesc");
                        objRowTxt.InnerText = objUtilityDriver.GetLanguageDescription(Conversion.CastToType<int>(objRead.GetValue("LANGUAGE_CODE").ToString(), out bSuccess));
                        objLstRow.AppendChild(objRowTxt);
                     

                        objElement.AppendChild(objLstRow);
                    }
                }

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.GetAdditionalLangCode.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (!objRead.IsClosed)
                    objRead.Close();
                if (objRead != null)
                    objRead.Dispose();
            }

        }


        /// Name		: Delete
        /// Author		: Nikhil Bhatia
        /// Date Created: 10/20/2014
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Delete the specified record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument DeleteAdditionalLangCode(XmlDocument p_objXmlDocument)
        {
            int iRowId = 0; ;
            XmlElement objElm = null;
            string sSQL = string.Empty;
            //   DataModelFactory objFacotry = null;
            CoverageGroup objCovGroup = null;
            bool bSuccess = false;
            int iLangCode = 0;
            try
            {
                // m_objDataModelFactory = new DataModelFactory(m_UserLogin);
                objCovGroup = m_objDataModelFactory.GetDataModelObject("CoverageGroup", false) as CoverageGroup;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CoverageGroupGrid']");
                if (objElm != null)
                    iRowId = Conversion.CastToType<int>(objElm.InnerText, out bSuccess);
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LanguageCode']");
                if (objElm != null)
                    iLangCode = Conversion.CastToType<int>(objElm.InnerText, out bSuccess);
                if (iRowId > 0 && iLangCode > 0)
                {
                    sSQL = string.Format("DELETE FROM COVERAGE_GROUP_TEXT WHERE COVERAGE_GROUP_ID = {0}  AND LANGUAGE_CODE = {1}", iRowId, iLangCode);
                    m_objDataModelFactory.Context.DbConnLookup.ExecuteNonQuery(sSQL);

                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.DeleteAdditionalLangCode.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                
                if (objCovGroup != null)
                {
                    objCovGroup.Dispose();
                }
            }
        }

        /// Name		: SaveAdditionalLangDesc
        /// Author		: Nikhil Bhatia
        /// Date Created: 10/20/2014
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument SaveAdditionalLangDesc(XmlDocument p_objXmlDocument)
        {
            int iRowId = 0;
            string sCovGroupCode = string.Empty;
            string sCovGroupDesc = string.Empty;
            int iLangCode = 0;
            string sSQL = string.Empty;
        
            int iNextUID = 0;
            XmlElement objElm = null;
            //  DataModelFactory objFacotry = null;
            CoverageGroup objCovGroup = null;
            ClaimXPolDedAggLimit objClaimXPolDedAggLimit = null;
          
            bool bSuccess = false;
            int iDuplicate = 0;
          

            System.Text.StringBuilder sbSQL = null;
            bool bIsDataChanged = false;

            try
            {
                //commented by Nikhil.Code review changes
               

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupId']");
                if (objElm != null)
                    iRowId = Conversion.CastToType<int>(objElm.InnerText, out bSuccess);

                   objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupCode']");
                if (objElm != null)
                    sCovGroupCode = objElm.InnerText;
          
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupDesc']");
                if (objElm != null)
                    sCovGroupDesc = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Language']");
                if (objElm != null && objElm.Attributes["value"] != null)
                {
                    if (string.Compare(objElm.GetAttribute("value"), "0") != 0)
                        iLangCode = Conversion.CastToType<int>(objElm.GetAttribute("value"),out bSuccess);
                }
              
                //Start - Added by Nikhil.


               

                //End  - Added by Nikhil.
                if (iRowId > 0)
                {
                    InsertCodeDescription(iRowId, sCovGroupCode, sCovGroupDesc, iLangCode);
                    //Changed by Nikhil.Code review changes
                   
                }
              
            
                

                
                return p_objXmlDocument;

            }
            catch (RMAppException)
            {
                throw;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.SaveAdditionalLangDesc.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                
                if (objCovGroup != null)
                {
                    objCovGroup.Dispose();
                }
                if (objClaimXPolDedAggLimit != null)
                {
                    objClaimXPolDedAggLimit.Dispose();
                }
            }
        }
        /// Name		: GetSelectedAdditionalLangCode
        /// Author		: Nikhil Bhatia
        /// Date Created: 10/20/2014		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the Selected Coverage Group
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument GetSelectedAdditionalLangCode(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            XmlElement objElemTemp = null;
            int iCovGroupId = 0;
            int iLangcode = 0;
            string sSQL = string.Empty;
            bool bSuccess = false;
            int iStateRowId = 0;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sReserveTypeCode = string.Empty;
            string sReserveTypeDesc = string.Empty;
            int iReserveTypeCodeId = 0;
            LocalCache objLocalCache = null;
            string sReserveTypes = string.Empty;
            
            CoverageGroup objCovGroup = null;
            string[] sReserveTypeList = null;
            //Start -  Added by Nikhil.Shared ded amount and limit.
            ClaimXPolDedAggLimit oClaimXPolDedAggLimit = null;
            int iClmXPolDedAggLmtId = 0;
            ArrayList arrlstLanguages = null; //Aman ML Changes
            UtilityDriver objUtilityDriver = null;
            bool bLanguages = false;
            string[] arrTemp = new string[2];
            DbReader objRead = null;
            
            //end -  Added by Nikhil.Shared ded amount and limit.
            try
            {
                objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupId']");
                if (objElement != null)
                {
                    iCovGroupId = Conversion.CastToType<int>(objElement.InnerText, out bSuccess);
                    if (iCovGroupId > 0)
                    {
                        //Added by nitin goel, for Disability type code and loss type code                        
                      
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LangCode']");
                        if (objElement != null && !string.IsNullOrEmpty(objElement.InnerText))
                        {
                            iLangcode = Conversion.CastToType<int>(objElement.InnerText, out bSuccess);
                        }

                        //using (objRead = DbFactory.ExecuteReader(m_sConnectString, string.Format("SELECT * FROM COVERAGE_GROUP_TEXT WHERE COVERAGE_GROUP_ID={0} AND LANGUAGE_CODE ={1}",iCovGroupId,iLangcode)))
                        using (objRead = DbFactory.ExecuteReader(m_sConnectString, string.Format("SELECT CG.COV_GROUP,CGT.GROUP_DESCRIPTION FROM COVERAGE_GROUP CG INNER JOIN COVERAGE_GROUP_TEXT CGT ON CG.COVERAGE_GROUP_ID=CGT.COVERAGE_GROUP_ID WHERE CG.COVERAGE_GROUP_ID={0} AND CGT.LANGUAGE_CODE ={1}", iCovGroupId, iLangcode)))
                        {
                            while (objRead.Read())
                            {
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupCode']");
                                objElement.InnerText = Convert.ToString(objRead.GetValue("COV_GROUP"));
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CovGroupDesc']");
                                objElement.InnerText = Convert.ToString(objRead.GetValue("GROUP_DESCRIPTION"));
                                
                            }
                        }
                        XmlNode noValueNode = p_objXmlDocument.SelectSingleNode("//control[@name='Language']");
                       
                        arrlstLanguages = new ArrayList();
                        objUtilityDriver = new UtilityDriver(m_sConnectString, m_iClientId);
                        bLanguages = objUtilityDriver.LoadLanguages(ref arrlstLanguages);
                        if (bLanguages)
                        {
                            for (int iIndex = 0; iIndex < (arrlstLanguages.Count + 1); iIndex++)
                            {
                                if (iIndex != 0)
                                {
                                    arrTemp = ((string)arrlstLanguages[iIndex - 1]).Split('|');
                                    objElemTemp = p_objXmlDocument.CreateElement("option");
                                    objElemTemp.SetAttribute("value", arrTemp[1]);
                                    objElemTemp.InnerText = arrTemp[0];

                                    if (string.Compare(arrTemp[1], iLangcode.ToString(), true) == 0)
                                    {
                                        objElemTemp.SetAttribute("selected", "1");
                                        objElemTemp.SetAttribute("value", arrTemp[1]);
                                    }
                                    noValueNode.AppendChild(objElemTemp);
                                    objElemTemp = null;
                                }

                            }
                        }
                    }

                }
                



                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CoverageGroupMaintenance.GetSelectedAdditionalLangCode.Error", m_iClientId), p_objEx);
            }
            finally
            {

            }
        }

        private void InsertCodeDescription(int p_iCovGroupID, string p_sCovGroup, string p_sCovGroupDesc, int p_iLangCode)
        {

         

            int iDuplicate = 0;
         
            string sSQL = string.Empty;
            sSQL = String.Format("SELECT COUNT(*) FROM COVERAGE_GROUP_TEXT WHERE COVERAGE_GROUP_ID = {0} AND LANGUAGE_CODE = {1} ", p_iCovGroupID, p_iLangCode);
            iDuplicate = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sSQL);
            
            if (iDuplicate > 0)
            {
                sSQL = string.Format("UPDATE COVERAGE_GROUP_TEXT SET GROUP_DESCRIPTION = '{0}' WHERE COVERAGE_GROUP_ID = {1} AND LANGUAGE_CODE = {2} ", p_sCovGroupDesc, p_iCovGroupID, p_iLangCode);

            }
            else
            {
                sSQL = string.Format("INSERT INTO COVERAGE_GROUP_TEXT VALUES({0},{1},'{2}','{3}')", p_iCovGroupID, p_iLangCode, p_sCovGroup, p_sCovGroupDesc);
            }
            m_objDataModelFactory.Context.DbConnLookup.ExecuteNonQuery(sSQL);
        }
    }
}
