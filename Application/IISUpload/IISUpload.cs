using System;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

/// <summary>
/// Author  :   Atul Patel
/// Dated   :   1st,July 2004
/// Purpose :   Upload the file.
/// </summary>

namespace Riskmaster.Application.IISUpload
{
	/// <summary>
	/// IISUpload.FileUpload is a public class used to upload the file to the server.
	/// </summary>
	public class FileUpload
	{
		/// <summary>
		/// Private string variable containing File name to upload.
		/// </summary>
		private string m_sFileName = "";
		/// <summary>
		/// Private long variable containing File size to be uploaded.
		/// </summary>
		private long m_lFileSize = 0;
		/// <summary>
		/// Private string variable containing Upload folder path where the files get's uploaded.
		/// </summary>
		private string m_sUploadFolder = "";
		/// <summary>
		/// Private long variable containing Maximum file length to limit the file size to be uploaded.
		/// </summary>
		private long m_lMaxFileLength = 0;

		/// <summary>
		/// IISUpload.FileUpload.UploadFile is a public method used to upload the file 
		/// from client end. Before uploading it will check for the file size, 
		/// permission etc. 
		/// </summary>
		/// <param name="p_objUploadFileName">HTMLInputFile name to upload.</param>
		/// <param name="p_sMessage">Message send to the client after the program execution</param>
		public void UploadFile(System.Web.UI.HtmlControls.HtmlInputFile p_objUploadFileName,ref string p_sMessage)
		{
			try
			{	
				//Checking for valid file
				if ( p_objUploadFileName.PostedFile != null) 
				{		
					//Extract the file name that needs to be uploaded.
					m_sFileName = p_objUploadFileName.PostedFile.FileName.Substring(p_objUploadFileName.PostedFile.FileName.LastIndexOf("\\") + 1) ;
					m_lFileSize = p_objUploadFileName.PostedFile.ContentLength;

					//Checking for the file length
					if(m_sFileName.Length == 0)
						p_sMessage = Globalization.GetString("FileUpload.UploadFile.UploadFailed");

					else if ( m_lFileSize <= 0 )
						p_sMessage = Globalization.GetString("FileUpload.UploadFile.FileSize");
		
					// Checking for the file size
					else if ((m_lFileSize > m_lMaxFileLength ) && ( m_lMaxFileLength != 0))
						p_sMessage = Globalization.GetString("FileUpload.UploadFile.FileMaximumSize");
					
					else
					{
						string sFilePath = "";		//File path .
						string sDirectoryPath = "";	//Directory path.
						string sFilePathTemp = "";	//Temporary file path.
						string sExtention = "";		//Extention of the file.
						int iFileNameStartPos = 0;	//Start position of the file name.
						int iFileNameEndPos = 0;	//End position of the file name.
						int iFileNameLength = 0;	//File name length.

						//Gets the server path to upload the file
						if(m_sUploadFolder.Length == 0)
						{
							sFilePath = System.Web.HttpContext.Current.Server.MapPath(m_sUploadFolder + m_sFileName);
						}
						else
						{
							sFilePath = m_sUploadFolder + m_sFileName;
						}

						//Check if the file does not exist
						if( !File.Exists(sFilePath) )
						{
                            p_objUploadFileName.PostedFile.SaveAs(sFilePath);
							p_sMessage = Globalization.GetString("FileUpload.UploadFile.FileUploaded");
						}

						//If file exists
						else
						{	
							
							//Gets the server path to upload the file
							if(m_sUploadFolder.Length == 0)
							{
								sDirectoryPath = System.Web.HttpContext.Current.Server.MapPath(".\\");
							}
							else
							{
								sDirectoryPath = m_sUploadFolder;
							}
							iFileNameStartPos = sFilePath.LastIndexOf(@"\");
							iFileNameEndPos = sFilePath.LastIndexOf(@".");
							iFileNameLength = sFilePath.Length;
							m_sFileName = sFilePath.Substring(iFileNameStartPos+1,iFileNameEndPos-iFileNameStartPos-1);
							sExtention = sFilePath.Substring(iFileNameEndPos+1,iFileNameLength-iFileNameEndPos-1);
							int iCount = 1;  //Integer variable to append number at the end of the file, if file already exist
							do
							{
								sFilePathTemp = sDirectoryPath+m_sFileName + "-" + iCount ;
								iCount = iCount + 1;
							}
							while(File.Exists(sFilePathTemp+"." + sExtention));

							p_objUploadFileName.PostedFile.SaveAs(sFilePathTemp+"." + sExtention);
							p_sMessage = Globalization.GetString("FileUpload.UploadFile.FileUploaded");
					
						}
					}
				}
			}
			catch(Exception p_objException)
			{
				throw new FileInputOutputException("FileUpload.UploadFile.Error",p_objException);
			}
			finally
			{
				//Destroy the objects.
				p_objUploadFileName = null;
			}
		}
		/// <summary>
		/// Riskmaster.IISUpload.UploadFolder property sets the folder name.
		/// </summary>
		/// <returns>It returns the upload folder name.</returns>
		public string UploadFolder
		{
			get
			{
				return m_sUploadFolder;
			}
			set
			{
				m_sUploadFolder = value;
			}
		}
		/// <summary>
		/// Riskmaster.IISUpload.MaxFileLength property sets the maximum file length.
		/// </summary>
		/// <returns>It returns the maximum file length.</returns>
		public long MaxFileLength
		{
			get
			{
				return m_lMaxFileLength;
			}
			set
			{
				m_lMaxFileLength = value;
			}
		}
	}
}

