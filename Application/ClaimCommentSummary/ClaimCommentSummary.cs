using System;
using System.Xml;
using System.Globalization;
using System.Resources;
using System.Collections;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;

namespace Riskmaster.Application.ClaimCommentSummary
{
	/// <summary>
	/// Summary description for ClaimCommentSummary.
	/// </summary>
	public class ClaimCommentSummary :IDisposable 
	{
		#region "Constructor & Destructor"

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_iUserId">User Id</param>
		/// <param name="p_iUserGroupId">User Group Id</param>		
        public ClaimCommentSummary(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId)//sharishkumar Jira 818
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_iGroupId = p_iUserGroupId ;
            m_iClientId = p_iClientId;//sharishkumar Jira 818
			this.Initialize();
		}		
		#endregion

		#region Destructor
		/// <summary>
		/// Destructor
		/// </summary>
		~ClaimCommentSummary()
		{		
            Dispose();
		}

        public void Dispose()
        {
            if( m_objDataModelFactory != null )			
                m_objDataModelFactory.Dispose();
        }
		#endregion

		#endregion

		#region "Member Variables"

		/// <summary>
		/// Private variable to IndentLevel
		/// </summary>
		private int m_iIndentLevel = 0 ;

		/// <summary>
		/// Private variable to GridEffect
		/// </summary>
		private int m_iGridEffect = 1 ;

		/// <summary>
		/// Private variable for Object Type
		/// </summary>
		private string m_sObjectType = string.Empty;

		/// <summary>
		/// Private variable for Object Data
		/// </summary>
		private string m_sObjectData = string.Empty;

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;

		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;

		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";
		
		/// <summary>
		///Represents the login name of the Logged in User
		/// </summary>
		private string m_sLoginName = "";

		/// <summary>
		/// Represents the User Id.
		/// </summary>
		private int m_iUserId = 0;

		/// <summary>
		/// The Group id 
		/// </summary>
		private int m_iGroupId = 0;

        private int m_iClientId = 0; //sharishkumar Jira 818

		/// <summary>
		/// XML Document
		/// </summary>
		private XmlDocument m_objXmlDocument = null;
	
		#endregion

		#region Property declaration

		/// <summary>
		/// Gets & sets the user id
		/// </summary>
		public int UserId{get{return m_iUserId ;}set{m_iUserId = value;}} 


		/// <summary>
		/// Set the Login Name
		/// </summary>
		public string LoginName {get{return m_sLoginName;}set{m_sLoginName=value;}}


		/// <summary>
		/// Set the Group Id Property
		/// </summary>
		public int GroupId{get{return m_iGroupId;}set{m_iGroupId=value;}}


		/// <summary>
		/// Read/Write property for Object Type.
		/// </summary>
		internal string ObjectType  
		{
			get
			{
				return m_sObjectType;
			}
			set
			{
				m_sObjectType = value ;
			}
		}	


		/// <summary>
		/// Read/Write property for Object Data.
		/// </summary>
		internal string ObjectData  
		{
			get
			{
				return m_sObjectData;
			}
			set
			{
				m_sObjectData = value ;
			}
		}	


		#endregion

		#region "Public Function"

		#region "GetXmlInfo(int p_iId, string p_sScreenName)"
		/// Name		: GetXmlInfo
		/// Author		: Mohit Yadav
		/// Date Created	: 12 August 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// fetches the data in XML format
		/// </summary>
		/// <param name="p_iId">Id</param>
		/// <param name="p_sScreenName">Screen Name</param>
		/// <returns>Returns XML containing required data to print or error messages if fails to get the data</returns>
		public XmlDocument GetXmlInfo(int p_iId, string p_sScreenName, string p_sLangCode)
		{
			
			XmlElement objRootElement = null;

			XmlDocument objXmlDocument = null;
			XmlDocument objXmlDocumentOut = null;
			
			string sReturnXML = string.Empty; 
			string sComment = string.Empty;

			try
			{
				objXmlDocument = new XmlDocument();

				if(p_iId == 0)
					return objXmlDocument;
				
				switch (p_sScreenName)
				{
					case "claim":
						this.ObjectType = "Claim";
						break;
					case "claimwc":
						this.ObjectType = "Claim";
						break;
					case "claimva":
						this.ObjectType = "Claim";
						break;
					case "claimgc":
						this.ObjectType = "Claim";
						break;
					case "claimdi":
						this.ObjectType = "Claim";
						break;
                    //smahajan6 01/28/10 MITS:18230 :Start
                    //Adding Claim Comments Functionality for Property Claim
                    case "claimpc":
                        this.ObjectType = "Claim";
                        break;
                    //smahajan6 01/28/10 MITS:18230 :End
					case "event":
						this.ObjectType = "Event";
						break;
				}

				if (this.ObjectType == "Claim")
				{
					//Code to get Claims object and call ForEachComment function
                    objXmlDocumentOut = ForEachXComment("Claim", 1, p_iId, p_sLangCode);
				}
				if (this.ObjectType == "Event")
				{
					//Code to get Event object and call ForEachComment function
					objXmlDocumentOut = ForEachXComment("Event",1,p_iId,string.Empty);
				}

				objRootElement = objXmlDocument.CreateElement("Report");
				objRootElement.InnerXml = objXmlDocumentOut.OuterXml;
				objXmlDocument.AppendChild(objRootElement);

				return objXmlDocument;

			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ClaimCommentSummary.GetXmlInfo.ErrorSummary",m_iClientId),p_objException);//sharishkumar Jira 818
			}
			finally
			{
				objXmlDocument = null;
				objRootElement = null;
			} 
		}
		#endregion

		#endregion

		#region "Private Functions"

        #region "Initialize"
		/// Name		: Initialize
		/// Author		: Mohit Yadav
		/// Date Created	: 12 August 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string;
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory(m_sDsnName,m_sUserName,m_sPassword,m_iClientId);//sharishkumar Jira 818
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_objXmlDocument = new XmlDocument(); 
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("ClaimCommentSummary.Initialize.ErrorInit", m_iClientId), p_objEx);			//sharishkumar Jira 818	
			}			
		}	
		#endregion

		#region "LoadComment(string p_sHeader, string p_sHeaderText, string p_sComment)"
		private XmlDocument LoadComment(string p_sHeader, string p_sHeaderText, string p_sComment)
		{

			string sRetComment = string.Empty;
			string sHeader = string.Empty;
			string sLoadComment = string.Empty;

			XmlElement objRootElement = null;

			XmlDocument objXmlDocument = null;
			
			try
			{

				objXmlDocument = new XmlDocument();

				objRootElement = objXmlDocument.CreateElement("Comment");

				if ((m_iGridEffect % 2) == 0)
				{
					objRootElement.SetAttribute("bgcolor", "White");
				}
				else
				{
					objRootElement.SetAttribute("bgcolor", "WhiteSmoke");
				}

				objRootElement.SetAttribute("Header", p_sHeader);
				objRootElement.SetAttribute("HeaderText", p_sHeaderText);
	
				p_sComment = p_sComment.Replace("\r\n","<br/>");
				objRootElement.InnerText = p_sComment.Replace("\n","<br/>");
				m_iGridEffect = m_iGridEffect + 1;

				objXmlDocument.AppendChild(objRootElement);

				return objXmlDocument;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ClaimCommentSummary.LoadComment.ErrorSummary", m_iClientId),p_objException);//sharishkumar Jira 818
			}
			finally
			{

			} 
		}

		#endregion

		#region "ForEachXComment(string p_sObjType, int p_iRowCount, int p_iId)"
        private XmlDocument ForEachXComment(string p_sObjType, int p_iRowCount, int p_iId, string p_sLangCode)
		{
			string sRetComment = string.Empty;
			string sSQL = string.Empty;
			string sSQL1 = string.Empty;
			string sSQL2 = string.Empty;
			DbReader objReaderEntity = null;	
		
			XmlElement objRootElement = null;
			XmlElement objXmlNode = null;

			XmlDocument objXmlDocument = null;
			XmlDocument objXmlDocumentOut = null;
			
			try
			{
				objXmlDocument = new XmlDocument();				
				if (p_sLangCode == string.Empty)
                    p_sLangCode = RMConfigurationManager.GetAppSetting(("RMABaseLangCodeWithCulture").ToString().Split('|')[0],m_sConnectionString, m_iClientId);//sharishkumar Jira 818
				switch (p_sObjType)
				{
					case "Claim":
						//Get Claim Level comment
						objXmlDocumentOut = new XmlDocument();

						objRootElement = objXmlDocument.CreateElement("Comments");
						objXmlDocument.AppendChild(objRootElement);

						Claim objClaim = null;
						string sClaimComment = string.Empty;

                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(p_iId);

                        //sClaimComment = objClaim.Comments;  csingh7 : R6 Claim Comment Enhancement Start

                        DbReader objReaderCom = null;

                        sSQL = "SELECT HTMLCOMMENTS FROM COMMENTS_TEXT WHERE ATTACH_TABLE LIKE 'CLAIM' AND ATTACH_RECORDID = " + p_iId;
                        objReaderCom = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                        if (objReaderCom.Read())
                        {
                            sClaimComment = Common.Conversion.ConvertObjToStr(objReaderCom.GetValue("HTMLCOMMENTS")).Trim();
                        }
                        objReaderCom.Close(); //csingh7 : R6 Claim Comment Enhancement End
                        
						
						//MITS 7721.. Raman Bhatia..COMMENT SUMMARY should have a print button.. adding "Regarding" text to headertext
						string sHeaderText = FetchFormTitle(objClaim);
						//objXmlDocumentOut = LoadComment("Claim",sHeaderText,sClaimComment);
                        objXmlDocumentOut = LoadComment(CommonFunctions.FilterBusinessMessage(Globalization.GetString("lblClaim",m_iClientId), p_sLangCode),sHeaderText, sClaimComment);//ksahu5 ML change jira 51//sharishkumar Jira 818
                        
						objXmlNode = objXmlDocument.CreateElement("data");
						if (objXmlDocumentOut.InnerText != "")
						{
							objXmlNode.InnerXml = objXmlDocumentOut.OuterXml;
						}
						objRootElement.AppendChild(objXmlNode);
						objXmlDocumentOut = null;

						m_iIndentLevel = m_iIndentLevel + 1;
						
						//Get Claim Primary Policy Comments
                        if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                        {
						objXmlDocumentOut = ForEachXComment("Policy",1,objClaim.PrimaryPolicyId,string.Empty);
						if (objXmlDocumentOut.InnerText != "")
						{
							objRootElement.InnerXml = objRootElement.InnerXml + objXmlDocumentOut.OuterXml;
                            }
						}
						objXmlDocumentOut = null;

						//Get Litigation Comments
                        objXmlDocumentOut = ForEachXComment("ClaimXLitigation", 1, objClaim.ClaimId, string.Empty);
						if (objXmlDocumentOut.InnerText != "")
						{
							objRootElement.InnerXml = objRootElement.InnerXml + objXmlDocumentOut.OuterXml;
						}
						objXmlDocumentOut = null;

						//Get Claimant Level Comments
                        objXmlDocumentOut = ForEachXComment("Claimant", 1, objClaim.ClaimId, string.Empty);
						if (objXmlDocumentOut.InnerText != "")
						{
							objRootElement.InnerXml = objRootElement.InnerXml + objXmlDocumentOut.OuterXml;
						}
						objXmlDocumentOut = null;

						//Get Defendant Level Comments
                        objXmlDocumentOut = ForEachXComment("Defendant", 1, objClaim.ClaimId, string.Empty);
						if (objXmlDocumentOut.InnerText != "")
						{
							objRootElement.InnerXml = objRootElement.InnerXml + objXmlDocumentOut.OuterXml;
						}
						objXmlDocumentOut = null;
				
						//Get Claim Level Transaction Comments
                        objXmlDocumentOut = ForEachXComment("Funds", 1, objClaim.ClaimId, string.Empty);
						if (objXmlDocumentOut.InnerText != "")
						{
							objRootElement.InnerXml = objRootElement.InnerXml + objXmlDocumentOut.OuterXml;
						}
						objXmlDocumentOut = null;
			 			
						m_iIndentLevel = m_iIndentLevel - 1;

						if (objRootElement.InnerText != "")
						{
							objXmlDocument.AppendChild(objRootElement);
						}
						else
						{
							objXmlNode = objXmlDocument.CreateElement("data");
							//objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("No comments found for this " + p_sObjType + ".","","").OuterXml;
                            objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment(CommonFunctions.FilterBusinessMessage(Globalization.GetString("NoCmntsFound" + p_sObjType, m_iClientId), p_sLangCode), "", "").OuterXml;//sharishkumar Jira 818
                            //objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment(CommonFunctions.FilterBusinessMessage(Globalization.GetString("NoCmntsFound"),p_sLangCode) + p_sObjType + ".", "", "").OuterXml;
							objRootElement.AppendChild(objXmlNode);
							objXmlDocument.AppendChild(objRootElement);
						}
						objClaim = null;
						break;
					case "Event":
						//Get Event Level Comments
						objXmlDocumentOut = new XmlDocument();

						objRootElement = objXmlDocument.CreateElement("Comments");
						objXmlDocument.AppendChild(objRootElement);

						Event objEvent = null;
						string sEventComment = string.Empty;
						objXmlDocumentOut = new XmlDocument();

                        objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                        objEvent.MoveTo(p_iId);

                        //sEventComment = objEvent.Comments; csingh7 : R6 Claim Comment Enhancement Start

                        DbReader objReaderEvent = null;

                        sSQL = "SELECT HTMLCOMMENTS FROM COMMENTS_TEXT WHERE ATTACH_TABLE LIKE 'EVENT' AND ATTACH_RECORDID = " + p_iId;
                        objReaderEvent = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                        if (objReaderEvent.Read())
                        {
                            sEventComment = Common.Conversion.ConvertObjToStr(objReaderEvent.GetValue("HTMLCOMMENTS")).Trim();
                        }
                        objReaderEvent.Close(); //csingh7 : R6 Claim Comment Enhancement End

                        //MGaba2:MITS 17069:In case of Event,only Event Number was coming in the title bar:Start
                        string sCaption = FetchFormTitle(objEvent);
						//objXmlDocumentOut = LoadComment("Event",objEvent.EventNumber,sEventComment);
                        objXmlDocumentOut = LoadComment("Event", sCaption, sEventComment);
                        //MGaba2:MITS 17069:End

						objXmlNode = objXmlDocument.CreateElement("data");
						if (objXmlDocumentOut.InnerText != "")
						{
							objXmlNode.InnerXml = objXmlDocumentOut.OuterXml;
						}
						objRootElement.AppendChild(objXmlNode);
						objXmlDocumentOut = null;

						m_iIndentLevel = m_iIndentLevel + 1;

						//Get Event Level PI Comments	
                        objXmlDocumentOut = ForEachXComment("PI", 1, objEvent.EventId, string.Empty);
						if (objXmlDocumentOut.InnerText != "")
						{
							objRootElement.InnerXml = objRootElement.InnerXml + objXmlDocumentOut.OuterXml;
						}
						objXmlDocumentOut = null;

						//Get Each Claim Branch Comments
						DbReader objReaderClaim = null;

						sSQL = "SELECT * FROM CLAIM WHERE CLAIM.EVENT_ID = " + objEvent.EventId;
						objReaderClaim=DbFactory.GetDbReader(m_sConnectionString,sSQL);

						if(objReaderClaim != null)
						{
							while(objReaderClaim.Read())
							{
								//Mukul(2/6/2007)MITS 8779 Converted all int 16 to int 32
                                objXmlDocumentOut = ForEachXComment("Claims", 1, Convert.ToInt32(objReaderClaim["CLAIM_ID"].ToString()), string.Empty);
								if (objXmlDocumentOut.InnerText != "")
									objRootElement.InnerXml = objRootElement.InnerXml + objXmlDocumentOut.OuterXml;
							}
							objReaderClaim.Close();
							objXmlDocumentOut = null;
						}

						if (objRootElement.InnerText != "")
						{
							objXmlDocument.AppendChild(objRootElement);
						}
						else
						{
							objXmlNode = objXmlDocument.CreateElement("data");
							objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("No comments found for this " + p_sObjType + ".","","").OuterXml;
							objRootElement.AppendChild(objXmlNode);
							objXmlDocument.AppendChild(objRootElement);
						}
						m_iIndentLevel = m_iIndentLevel - 1;
						objEvent = null;
						objReaderClaim = null;
						break;
					case "Funds":
						string sFundsComment = string.Empty;
						objXmlDocumentOut = new XmlDocument();

						DbReader objReaderFunds = null;

						sSQL = "SELECT * FROM FUNDS WHERE FUNDS.CLAIM_ID = " + p_iId + 
							"AND FUNDS.COMMENTS is NOT NULL";
						objReaderFunds=DbFactory.GetDbReader(m_sConnectionString,sSQL);

						if(objReaderFunds != null)
						{
							objXmlNode = objXmlDocumentOut.CreateElement("data");
							while(objReaderFunds.Read())
							{
								if (objReaderFunds["COMMENTS"].ToString() != "")
								{
									sFundsComment = objReaderFunds["COMMENTS"].ToString();
									objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Transaction","#" + objReaderFunds["CTL_NUMBER"].ToString(),sFundsComment).OuterXml;
								}
							}
							objReaderFunds.Close();
							objXmlDocumentOut.AppendChild(objXmlNode);
							objReaderFunds = null;
							return objXmlDocumentOut;
						}
						break;
					case "PI":
						string sPIComment = string.Empty;
						int iPIEId = 0;
						objXmlDocumentOut = new XmlDocument();

						DbReader objReaderPI = null;

						sSQL1 = "SELECT * FROM PERSON_INVOLVED WHERE PERSON_INVOLVED.EVENT_ID = " + p_iId + 
							"AND PERSON_INVOLVED.COMMENTS is NOT NULL";
						objReaderPI=DbFactory.GetDbReader(m_sConnectionString,sSQL1);

						if(objReaderPI != null)
						{
							objXmlNode = objXmlDocumentOut.CreateElement("data");
							while(objReaderPI.Read())
							{
								if (objReaderPI["COMMENTS"].ToString() != "")
								{
									sPIComment = objReaderPI["COMMENTS"].ToString();
									//Mukul(2/6/2007)MITS 8779 Converted all int 16 to int 32
									iPIEId = Convert.ToInt32(objReaderPI["PI_EID"]);

									sSQL2 = "SELECT * FROM ENTITY WHERE ENTITY.ENTITY_ID = " + iPIEId;
									objReaderEntity=DbFactory.GetDbReader(m_sConnectionString,sSQL2);
									if(objReaderEntity != null)
									{
										if(objReaderEntity.Read())
										{
											objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Person Involved",objReaderEntity["LAST_NAME"].ToString() + ", " + objReaderEntity["FIRST_NAME"].ToString(),sPIComment).OuterXml;
										}
									}
									objReaderEntity.Close();
									//objReaderEntity = null;
								}
							}
							objReaderPI.Close();
							objReaderPI = null;
							objXmlDocumentOut.AppendChild(objXmlNode);
							return objXmlDocumentOut;
						}
						break;
					case "ClaimXLitigation":
						string sClaimXLitigationComment = string.Empty;
						objXmlDocumentOut = new XmlDocument();

						DbReader objReader = null;
                        
                        //nsachdeva2 - MITS: 27077 - 6/6/2012
                        ////sSQL = "SELECT * FROM CLAIM_X_LITIGATION WHERE " + 
                        ////    "CLAIM_X_LITIGATION.CLAIM_ID = " + p_iId + 
                        ////    "AND CLAIM_X_LITIGATION.COMMENTS is NOT NULL";
                        sSQL = "SELECT COMMENTS FROM COMMENTS_TEXT WHERE ATTACH_TABLE = 'CLAIM_X_LITIGATION' AND ATTACH_RECORDID = " + p_iId + " AND COMMENTS IS NOT NULL";
                        //END MITS: 27077

						objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL);

						if(objReader != null)
						{
							
							objXmlNode = objXmlDocumentOut.CreateElement("data");
							while(objReader.Read())
							{
								if (objReader["Comments"].ToString()!= "")
								{
									sClaimXLitigationComment = objReader["Comments"].ToString();
									objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Litigation","",sClaimXLitigationComment).OuterXml;
								}
							}
							objReader.Close();
							objXmlDocumentOut.AppendChild(objXmlNode);
							objReader = null;
							return objXmlDocumentOut;
						}
						break;
					case "Claimant":
						//Claimant Entity Comment
						string sClaimantComment = string.Empty;
						int iClaimantEId = 0;			
						objXmlDocumentOut = new XmlDocument();

						DbReader objReaderClaimant = null;

						sSQL1 = "SELECT * FROM CLAIMANT WHERE CLAIMANT.CLAIM_ID = " + p_iId + 
							"AND CLAIMANT.COMMENTS is NOT NULL";
						objReaderClaimant=DbFactory.GetDbReader(m_sConnectionString,sSQL1);

						if(objReaderClaimant != null)
						{
							objXmlNode = objXmlDocumentOut.CreateElement("data");
							while(objReaderClaimant.Read())
							{
								if (objReaderClaimant["COMMENTS"].ToString() != "")
								{
									sClaimantComment = objReaderClaimant["COMMENTS"].ToString();
									//Mukul(2/6/2007)MITS 8779 Converted all int 16 to int 32
									iClaimantEId = Convert.ToInt32(objReaderClaimant["CLAIMANT_EID"].ToString());

									sSQL2 = "SELECT * FROM ENTITY WHERE ENTITY.ENTITY_ID = " + iClaimantEId;
									objReaderEntity=DbFactory.GetDbReader(m_sConnectionString,sSQL2);
									if(objReaderEntity != null)
									{
										if(objReaderEntity.Read())
										{
											objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Claimant",objReaderEntity["LAST_NAME"].ToString() + ", " + objReaderEntity["FIRST_NAME"].ToString(),sClaimantComment).OuterXml;
										}
									}
									objReaderEntity.Close();
									//objReaderEntity = null;
								}
							}
							objReaderClaimant.Close();
							objReaderClaimant = null;
							objXmlDocumentOut.AppendChild(objXmlNode);
							return objXmlDocumentOut;
						}
						break;
					case "Defendant":
						string sDefendantComment = string.Empty;
						int iDefendantEId = 0;
						objXmlDocumentOut = new XmlDocument();

						DbReader objReaderDefendant = null;

						sSQL1 = "SELECT * FROM DEFENDANT WHERE DEFENDANT.CLAIM_ID = " + p_iId + 
							"AND DEFENDANT.COMMENTS is NOT NULL";
						objReaderDefendant=DbFactory.GetDbReader(m_sConnectionString,sSQL1);

						if(objReaderDefendant != null)
						{
							objXmlNode = objXmlDocumentOut.CreateElement("data");
							while(objReaderDefendant.Read())
							{
								if (objReaderDefendant["COMMENTS"].ToString() != "")
								{
									sDefendantComment = objReaderDefendant["COMMENTS"].ToString();
									//Mukul(2/6/2007)MITS 8779 Converted all int 16 to int 32
									iDefendantEId = Convert.ToInt32(objReaderDefendant["DEFENDANT_EID"].ToString());

									sSQL2 = "SELECT * FROM ENTITY WHERE ENTITY.ENTITY_ID = " + iDefendantEId;
									objReaderEntity=DbFactory.GetDbReader(m_sConnectionString,sSQL2);
									if(objReaderEntity != null)
									{
										if(objReaderEntity.Read())
										{
											objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Defendant",objReaderEntity["LAST_NAME"].ToString() + ", " + objReaderEntity["FIRST_NAME"].ToString(),sDefendantComment).OuterXml;
										}
									}
									objReaderEntity.Close();
									//objReaderEntity = null;
								}
							}
							objReaderDefendant.Close();
							objReaderDefendant = null;
							objXmlDocumentOut.AppendChild(objXmlNode);
							return objXmlDocumentOut;
						}

						break;
					case "Policy":
						Policy objPolicy = null;
						string sPolicyComment = string.Empty;
						objXmlDocumentOut = new XmlDocument();

						objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy",false);
						objPolicy.MoveTo(p_iId);

						if (objPolicy.Comments != "")
						{
							sPolicyComment = objPolicy.Comments;
							objXmlNode = objXmlDocumentOut.CreateElement("data");
							objXmlNode.InnerXml = LoadComment("Primary Policy",objPolicy.PolicyName,sPolicyComment).OuterXml;
							objXmlDocumentOut.AppendChild(objXmlNode);
							objPolicy = null;
							return objXmlDocumentOut;
						}
						break;
					case "Claims":
						//Get Claim Level comment with event
						objXmlDocumentOut = new XmlDocument();
						Claim objClaims = null;
                        string sClaimComments = "";
                        objClaims = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaims.MoveTo(p_iId);

                        //sClaimComment = objClaims.Comments;  csingh7 : R6 Claim Comment Enhancement Start

                        DbReader objReaderClaims = null;
                        sSQL = "SELECT HTMLCOMMENTS FROM COMMENTS_TEXT WHERE ATTACH_TABLE LIKE 'CLAIM' AND ATTACH_RECORDID = " + p_iId;
                        objReaderClaims = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                        if (objReaderClaims.Read())
                        {
                            sClaimComments = Common.Conversion.ConvertObjToStr(objReaderClaims.GetValue("HTMLCOMMENTS")).Trim();
                        }
                        objReaderClaims.Close(); //csingh7 : R6 Claim Comment Enhancement End

						objXmlNode = objXmlDocumentOut.CreateElement("data");
                        if (LoadComment("Claim", objClaims.ClaimNumber, sClaimComments).InnerText != "")
                            objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Claim", objClaims.ClaimNumber, sClaimComments).OuterXml;
						
						m_iIndentLevel = m_iIndentLevel + 1;
						
						//Get Claim Primary Policy Comments
                        if (objClaims.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                        {
                            objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
						objPolicy.MoveTo(objClaims.PrimaryPolicyId);

						if (objPolicy.Comments != "")
						{
							sPolicyComment = objPolicy.Comments;
							objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Primary Policy",objPolicy.PolicyName,sPolicyComment).OuterXml;
                            }
                        }
						//Get Litigation Comments

						DbReader objReaderClaimXLitigation = null;

                        //nsachdeva2 - MITS: 27077 - 6/5/2012
                        //sSQL = "SELECT * FROM CLAIM_X_LITIGATION WHERE " + 
                        //    "CLAIM_X_LITIGATION.CLAIM_ID = " + objClaims.ClaimId + 
                        //    "AND CLAIM_X_LITIGATION.COMMENTS is NOT NULL";
                        sSQL = "SELECT COMMENTS FROM COMMENTS_TEXT WHERE ATTACH_TABLE = 'CLAIM_X_LITIGATION' AND ATTACH_RECORDID IN ( SELECT LITIGATION_ROW_ID FROM CLAIM_X_LITIGATION WHERE CLAIM_ID = " + p_iId + ")";
                        //End MITS: 27077
						objReaderClaimXLitigation=DbFactory.GetDbReader(m_sConnectionString,sSQL);

						if(objReaderClaimXLitigation != null)
						{
							while(objReaderClaimXLitigation.Read())
							{
								if (objReaderClaimXLitigation["Comments"].ToString()!= "")
								{
									sClaimXLitigationComment = objReaderClaimXLitigation["Comments"].ToString();
									objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Litigation","",sClaimXLitigationComment).OuterXml;
								}
							}
							objReaderClaimXLitigation.Close();
						}

						//Get Claimant Level Comments

						sSQL1 = "SELECT * FROM CLAIMANT WHERE CLAIMANT.CLAIM_ID = " + objClaims.ClaimId + 
							"AND CLAIMANT.COMMENTS is NOT NULL";
						objReaderClaimant=DbFactory.GetDbReader(m_sConnectionString,sSQL1);

						if(objReaderClaimant != null)
						{
							while(objReaderClaimant.Read())
							{
								if (objReaderClaimant["COMMENTS"].ToString() != "")
								{
									sClaimantComment = objReaderClaimant["COMMENTS"].ToString();
									//Mukul(2/6/2007)MITS 8779 Converted all int 16 to int 32
									iClaimantEId = Convert.ToInt32(objReaderClaimant["CLAIMANT_EID"].ToString());

									sSQL2 = "SELECT * FROM ENTITY WHERE ENTITY.ENTITY_ID = " + iClaimantEId;
									objReaderEntity=DbFactory.GetDbReader(m_sConnectionString,sSQL2);
									if(objReaderEntity != null)
									{
										if(objReaderEntity.Read())
										{
											objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Claimant",objReaderEntity["LAST_NAME"].ToString() + ", " + objReaderEntity["FIRST_NAME"].ToString(),sClaimantComment).OuterXml;
										}
									}
									objReaderEntity.Close();
								}
							}
							objReaderClaimant.Close();
						}

						//Get Defendant Level Comments

						sSQL1 = "SELECT * FROM DEFENDANT WHERE DEFENDANT.CLAIM_ID = " + objClaims.ClaimId + 
							"AND DEFENDANT.COMMENTS is NOT NULL";
						objReaderDefendant=DbFactory.GetDbReader(m_sConnectionString,sSQL1);

						if(objReaderDefendant != null)
						{
							while(objReaderDefendant.Read())
							{
								if (objReaderDefendant["COMMENTS"].ToString() != "")
								{
									sDefendantComment = objReaderDefendant["COMMENTS"].ToString();
									//Mukul(2/6/2007)MITS 8779 Converted all int 16 to int 32
									iDefendantEId = Convert.ToInt32(objReaderDefendant["DEFENDANT_EID"].ToString());

									sSQL2 = "SELECT * FROM ENTITY WHERE ENTITY.ENTITY_ID = " + iDefendantEId;
									objReaderEntity=DbFactory.GetDbReader(m_sConnectionString,sSQL2);
									if(objReaderEntity != null)
									{
										if(objReaderEntity.Read())
										{
											objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Defendant",objReaderEntity["LAST_NAME"].ToString() + ", " + objReaderEntity["FIRST_NAME"].ToString(),sDefendantComment).OuterXml;
										}
									}
									objReaderEntity.Close();
								}
							}
							objReaderDefendant.Close();
						}
				
						//Get Claim Level Transaction Comments

						sSQL = "SELECT * FROM FUNDS WHERE FUNDS.CLAIM_ID = " + objClaims.ClaimId + 
							"AND FUNDS.COMMENTS is NOT NULL";
						objReaderFunds=DbFactory.GetDbReader(m_sConnectionString,sSQL);

						if(objReaderFunds != null)
						{
							while(objReaderFunds.Read())
							{
								if (objReaderFunds["COMMENTS"].ToString() != "")
								{
									sFundsComment = objReaderFunds["COMMENTS"].ToString();
									objXmlNode.InnerXml = objXmlNode.InnerXml + LoadComment("Transaction","#" + objReaderFunds["CTL_NUMBER"].ToString(),sFundsComment).OuterXml;
								}
							}
							objReaderFunds.Close();
						}

						m_iIndentLevel = m_iIndentLevel - 1;
						
						objXmlDocumentOut.AppendChild(objXmlNode);
						return objXmlDocumentOut;

						//break;
					case "Entity":
						break;
				}				
				return objXmlDocument;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ClaimCommentSummary.ForEachXComment.ErrorSummary",m_iClientId),p_objException);//sharishkumar Jira 818
			}
			finally
			{
                if (objReaderEntity != null)
                    objReaderEntity.Dispose();
                objRootElement = null;
				objXmlDocument = null;
			} 
		}

        
        /// <summary>
        ///MGaba2:MITS 17069:In case of Event,Event Number,department name and PI name should come in the title bar of Comment Summary
        /// </summary>
        /// <param name="objEvent">current object of Event</param>
        /// <returns></returns>
        private string FetchFormTitle(Event objEvent)
        {
            int captionLevel = 1006;
            string sCaption = "";
            string sTmp = "";

            try
            {
                if (objEvent.EventId != 0)
                {
                    if (objEvent.Context.InternalSettings.SysSettings.EvCaptionLevel != 0)
                        captionLevel = objEvent.Context.InternalSettings.SysSettings.EvCaptionLevel;
                    else
                        captionLevel = 0;
                    

                    if (captionLevel < 1005 || captionLevel > 1012)
                        captionLevel = 0;

                    if (captionLevel == 0)
                    {
                        sCaption = "";
                    }
                    else if (captionLevel != 1012)
                        sCaption = objEvent.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                    else
                    {
                        objEvent.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid, ref sCaption, ref sTmp);
                        sCaption += " " + sTmp;
                    }
                    sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                    if (objEvent.EventId != 0 && objEvent.PiList.GetPrimaryPi() != null)
                        sCaption = " [" + objEvent.EventNumber + " * " + sCaption + " * " +
                            objEvent.PiList.GetPrimaryPi().PiEntity.GetLastFirstName() + "]";
                    else
                        sCaption = " [" + objEvent.EventNumber + " * " + sCaption + " * - ]";
                }
            }
            catch
            {
                sCaption = "";
            } // catch            
            return sCaption;
        }

		private string FetchFormTitle(Claim objClaim)
		{
			string sCaption="";
			int captionLevel=0;
			string sTmp="";
			LobSettings objLobSettings = null;

			try
			{
				objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
				Event objEvent = (objClaim.Parent as Event);
				//Handle User Specific Caption Level
				if(!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId!=0)
				{
				
					if(objLobSettings.CaptionLevel!=0)
						captionLevel = objLobSettings.CaptionLevel;
					else
						captionLevel = 1006;
                    // Changed by Amitosh for Mits 24084 (02/28/2011)
                    //if (captionLevel < 1006 || captionLevel > 1012)
					if(captionLevel <1005 || captionLevel >1012)
						captionLevel = 0;

					if(captionLevel !=1012)
						sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid,captionLevel,0,ref captionLevel);//captionLevel discarded
					else
					{
						objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid,ref sCaption,ref sTmp);
						sCaption += sTmp; 
					}
					sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
			
					if(objClaim.EventId!=0 && objClaim.PrimaryClaimant!=null && objClaim.PrimaryClaimant.ClaimantEntity!=null)
						sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +	
							objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
					else
						sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
				}
				//Pass this subtitle value to view (ref'ed from @valuepath).
				if(objClaim.IsNew)
					sCaption="";
			} // try

			catch
			{
				sCaption = "";
			} // catch
			finally
			{
				objLobSettings = null;
				
			} // finally

			
			return sCaption;
		}
		#endregion

		#endregion
	}
}
