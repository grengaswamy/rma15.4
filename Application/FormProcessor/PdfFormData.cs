using System;
using System.Collections;

namespace Riskmaster.Application.FormProcessor
{
	///************************************************************** 
	///* $File		: PdfFormData.cs
	///* $Revision	: 1.0.0.0 
	///* $Date		: 22-Dec-2004		
	///* $Author	: Tanuj Narula
	///* $Comment	: This DataStructure will be accessible to scripts.
	///* $Source	: 
	///**************************************************************
	public class PdfFormData:IDisposable
	{    
		#region variables
		/// <summary>
		/// This will contain the key and the corresponding data.
		/// </summary>
		private Hashtable m_htKeyValuePair=null;
		#endregion
		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public PdfFormData()
		{
			this.m_htKeyValuePair=new Hashtable();
		}
		#endregion
		#region Properties
		/// <summary>
		/// This is the property exposing Hashtable.
		/// </summary>
		public Hashtable Form
		{
			get
			{
				return m_htKeyValuePair;
			}
			
		}
		#endregion
		#region Functions
		/// Name		: Dispose
		/// Author		: Tanuj Narula
		/// Date Created: 22-Dec-2004			
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// This is a clean up function.
		/// </summary>
		public void Dispose()
		{
			if(m_htKeyValuePair!=null)
			{
                m_htKeyValuePair=null;
			}
		}
		#endregion
	}
}
