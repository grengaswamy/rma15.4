﻿
using System;
using System.Data;
using System.Xml;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Collections.Generic;

namespace Riskmaster.Application.LSSInterface
{
	/**************************************************************
	 * $File		: CommonFunctions.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 22/09/2005
	 * $Author		: Parag Sarin
	 * $Comment		: CommonFunctions class contains common functions
	 * $Source		:  	
	 ************************************************************
	 * Amendment History
	 * ************************************************************
	 * Date Amended		*	Author		*	Amendment   
	 * 07/31/2006			Manoj/Raj		Changed as per new code addition and old code modification
	**************************************************************/

	/// <summary>
	/// Common Functions for LSS Inteface
	/// </summary>
	public class CommonFunctions
	{
		public CommonFunctions()
        {
        }//class constructor
		
		/// <summary>
		/// Writes Claim log entry 
		/// </summary>
		/// <param name="p_sMessage">Message to log</param>
		public static void logExportError(string p_sMessage,int p_iClientId)
		{
			LogItem oLogItem = new LogItem();
			oLogItem.Category = "Default";
			oLogItem.Message=p_sMessage;
            Log.Write(oLogItem, p_iClientId);
		}

		/// <summary>
		/// Gets value for a specified node from specified Xml.
		/// </summary>
		/// <param name="p_objDOC">Xml</param>
		/// <param name="p_sXmlPath">Node to get value from</param>
		/// <returns>value</returns>
		internal static string GetValue(XmlDocument p_objDOC , string p_sXmlPath)
		{
			if(p_objDOC == null)
				return "";

			XmlNode  objNode=p_objDOC.SelectSingleNode(p_sXmlPath);
			if (objNode!=null)
				return AddAmparsand(objNode.InnerText);
			else
			{
				return "";
			}
		}
		
		/// <summary>
		/// Raj Kumar Goel - Gets value for a specified node from specified ACORD Standard Xml.
		/// </summary>
		/// <param name="p_objDOC">Xml</param>
		/// <param name="p_sXmlPath">Node to get value from</param>
		/// <param name="p_objNSMgr">Namespace Manager</param>
		/// <returns>value</returns>
		internal static string GetValue(XmlDocument p_objDOC, string p_sXmlPath, XmlNamespaceManager p_objNSMgr)
		{
			if(p_objDOC == null)
				return "";

			XmlNode  objNode=p_objDOC.SelectSingleNode(p_sXmlPath, p_objNSMgr);
			if (objNode!=null)
				return AddAmparsand(objNode.InnerXml);
			else
			{
				return "";
			}
		}

		/// <summary>
		/// Raj Kumar Goel - Gets value for a specified node from specified ACORD Standard Xml.
		/// </summary>
        /// <param name="p_objNode">Xml</param>
		/// <param name="p_sXmlPath">Node to get value from</param>
		/// <param name="p_objNSMgr">Namespace Manager</param>
		/// <returns>value</returns>
		internal static string GetValue(XmlNode p_objNode, string p_sXmlPath, XmlNamespaceManager p_objNSMgr)
		{
			if(p_objNode == null)
				return "";
			
			XmlNode  objNode=p_objNode.SelectSingleNode(p_sXmlPath, p_objNSMgr);
			if (objNode!=null)
				return AddAmparsand(objNode.InnerXml);
			else
			{
				return "";
			}
		}

		/// <summary>
		/// Raj Kumar Goel - Gets value for a User defined node from specified ACORD Standard Xml.
		/// </summary>
		/// <param name="p_objNode"></param>
		/// <param name="p_sXmlPath">Node to get value from</param>
        /// <param name="p_sParentName"></param>
		/// <param name="p_objNSMgr">Namespace Manager</param>
		/// <returns>value</returns>
		internal static string GetValue(XmlNode p_objNode, string p_sXmlPath, string p_sParentName, XmlNamespaceManager p_objNSMgr)
		{
			if(p_objNode == null)
				return "";
		
			XmlNodeList objNodes=p_objNode.SelectNodes(p_sXmlPath, p_objNSMgr);
			if(objNodes == null) return "";
			foreach(XmlNode objNode in objNodes)
				if(objNode.PreviousSibling.InnerXml.ToString().Trim().ToUpper().Equals(Convert.ToString(p_sParentName).Trim().ToUpper()))
					return AddAmparsand(objNode.InnerXml);
			return "";
		}

		
		/// <summary>
		/// Replaces special characters in strings
		/// </summary>
		/// <param name="p_sValue">string</param>
		/// <param name="p_iLength">length</param>
		/// <returns>string</returns>
		internal static string RemoveAmparsand(string p_sValue)
		{
			p_sValue= p_sValue.Replace("&", "&amp;");
			p_sValue= p_sValue.Replace(">", "&gt;");
			p_sValue= p_sValue.Replace("<", "&lt;");
			//p_sValue= p_sValue.Replace("-", "&quot;");
			p_sValue= p_sValue.Replace("'", "&apos;");
			return p_sValue;
		}

		/// <summary>
		/// Raj Kumar Goel - Replace &amp; , &gt; from string
		/// </summary>
		/// <param name="p_sValue">string</param>
        /// <param name="p_sValue">length</param>
		/// <returns>string</returns>
		internal static string AddAmparsand(string p_sValue)
		{
			p_sValue= p_sValue.Replace("&amp;", "&");
			p_sValue= p_sValue.Replace("&gt;", ">");
			p_sValue= p_sValue.Replace("&lt;", "<");
			//p_sValue= p_sValue.Replace("&quot;", "-");
			p_sValue= p_sValue.Replace("&apos;", "'");
            p_sValue = p_sValue.Trim();//rsushilaggar MITS 23030 Date 11/10/2010
			return p_sValue;
		}

		/// <summary>
		/// Raj Kumar Goel - Returns the ClaimId based on ClaimNumber
		/// </summary>
		/// <param name="p_sClaimNumber">string</param>
		/// <param name="p_sDSN">DSN</param>
		/// <returns>Int32</returns>
        //Nitin on 23-02-2009 for R5-R4 merging of Mits 12930
        //gagnihotri 10/11/2008 Uncommented for MITS 12930
        internal static int GetClaimId(string p_sClaimNumber, string p_sDSN, int p_iClientId)
        {
            int iClaimId = 0;
            StringBuilder sbSQL = null;
            DbReader objReader = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append(String.Format("SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='{0}'", p_sClaimNumber));
                objReader = DbFactory.GetDbReader(p_sDSN, sbSQL.ToString());
                if (objReader.Read())
                {
                    iClaimId = Conversion.ConvertObjToInt(objReader.GetValue(0), p_iClientId);
                }
                return iClaimId;
            }
            catch (Exception p_objException)
            {
                CommonFunctions.logExportError(p_objException.Message,p_iClientId);
                return 0;
            }
            finally
            {
                sbSQL = null;
                objReader.Close();
                objReader = null;

            }
}

        /// <summary>
        /// Gets the Riskmaster Database Connection String from the Riskmaster Security database
        /// </summary>
        /// <param name="strUserName">string containing the user name to log into the Riskmaster security database</param>
        /// <param name="strPassword">string containing the password to log into the Riskmaster security database</param>
        /// <param name="strDSNName">string containing the DSN Name selection</param>
        /// <returns>Riskmaster Database Connection String</returns>
        public static string GetRiskmasterConnectionString(string strUserName, string strPassword, string strDSNName, int p_iClientId)
		{
            UserLogin objUserLogin = new UserLogin(strUserName, strPassword, strDSNName, p_iClientId);
            string strRiskmasterConnectionString = string.Empty;

            //Set the connection string for the Riskmaster Database
            strRiskmasterConnectionString = objUserLogin.objRiskmasterDatabase.ConnectionString;

            //Clean up
            objUserLogin = null;

            return strRiskmasterConnectionString;
		} // method: GetRiskmasterConnectionString

        /// <summary>
        /// Obtains a handle to a UserLogin object
        /// </summary>
        /// <param name="strUserName">string containing the user name to log into the Riskmaster security database</param>
        /// <param name="strPassword">string containing the password to log into the Riskmaster security database</param>
        /// <param name="strDSNName">string containing the DSN Name selection</param>
        /// <returns>a handle to a UserLogin object</returns>
        public static UserLogin GetUserLoginInstance(string strUserName, string strPassword, string strDSNName, int p_iClientId)
        {
            UserLogin objUserLogin = new UserLogin(strUserName, strPassword, strDSNName, p_iClientId);

            return objUserLogin;
        } // method: GetUserLoginInstance




		#region Log Methods
		/// Name		: iLogDXRequest
		/// Author		: Manoj Agrawal
		/// Date Created: 09/19/2006
		/// <summary>		
		/// This method is used to log entry in DX_REQUEST table
		/// </summary>
		public static int iLogDXRequest(int iReqId, string sPackType, string sReqType, string sReqBy, string sReqStatus, string sReqPacket, string sDSN)
		{
			LocalCache objCache = null;			
			int iPackType = 0;
			int iReqType = 0;
			int iReqBy = 0;
			int iReqStatus = 0;
			int iProcType = 0;
			string sDTTM = "";

			int iReturn = 0;
			bool bRecExists = false;
            DbWriter objWriter = null;

			try
			{
				if(iReqId == 0)
					return iReturn;

				objCache = new LocalCache(sDSN, 0);

				iPackType = objCache.GetCodeId(sPackType, "LSS_PACKETTYPE");
				iReqType = objCache.GetCodeId(sReqType, "LSS_REQUESTTYPE");
				iReqBy = objCache.GetCodeId(sReqBy, "LSS_SYSTEM");
				iReqStatus = objCache.GetCodeId(sReqStatus, "LSS_STATUS");
				iProcType = 0;
				sDTTM = Conversion.GetDateTime(DateTime.Now.ToString());

				//rsushilaggar changes done for LSS - RMX Oracle support
                objWriter = DbFactory.GetDbWriter(sDSN);
                objWriter.Tables.Add("DX_REQUEST");

                //nextRowId = Utilities.GetNextUID(Context.DbConn.ConnectionString, "SUPP_GRID_VALUE");
                //for (int i = 0; i < arr.Length; i++)
                //objWriter.Fields.Add((string)arr[i], this.GetField((string)arr[i]));
                objWriter.Fields.Add("REQUEST_ID", iReqId);
                objWriter.Fields.Add("PACKET_TYPE", iPackType);
                objWriter.Fields.Add("REQUEST_TYPE", iReqType);
                objWriter.Fields.Add("REQUESTED_BY", iReqBy);
                objWriter.Fields.Add("DTTM_RCD_ADDED", sDTTM);
                objWriter.Fields.Add("PROCESSING_TYPE", iProcType);
                objWriter.Fields.Add("REQUEST_STATUS", iReqStatus);
                objWriter.Fields.Add("PACKET", sReqPacket);
                //rsushilaggar 
				object objRecordExists = DbFactory.ExecuteScalar(sDSN, "SELECT REQUEST_ID FROM DX_REQUEST WHERE REQUEST_ID = " + iReqId);
				if (objRecordExists != null)
				{
					bRecExists = true;
				}
				

				if(! bRecExists)
				{
					//record does not exist. create new entry.
                    //DbFactory.ExecuteNonQuery(sDSN, String.Format("INSERT INTO DX_REQUEST (REQUEST_ID, PACKET_TYPE, REQUEST_TYPE, REQUESTED_BY, DTTM_RCD_ADDED, PROCESSING_TYPE, REQUEST_STATUS, PACKET) VALUES ({0},{1},{2},{3},'{4}',{5},{6},'{7}')", iReqId, iPackType, iReqType, iReqBy, sDTTM, iProcType, iReqStatus, sReqPacket.Replace("'", "''")), new Dictionary<string, string>());
                    objWriter.Execute();
				}
				else
				{
					//record already exist. update that.
                    objWriter.Where.Add(String.Format("REQUEST_ID = {0} ", iReqId));
                    //DbFactory.ExecuteNonQuery(sDSN, String.Format("UPDATE DX_REQUEST SET REQUEST_STATUS = {0}, PACKET = '{1}' WHERE REQUEST_ID = {2}", iReqStatus, sReqPacket.Replace("'", "''"), iReqId), new Dictionary<string, string>());
                    objWriter.Execute();
				}

				iReturn = iReqId;			//return iReqId on successful else return 0
			}			//try end
			catch(Exception p_objException)
			{
				iReturn = 0;
			}
			finally
			{
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
			}

			return iReturn;
		}

		/// Name		: iLogDXRequestEvent
		/// Author		: Manoj Agrawal
		/// Date Created: 09/20/2006
		/// <summary>		
		/// This method is used to log entry in DX_REQUEST_EVENT table
		/// </summary>
		public static int iLogDXRequestEvent(int iEventId, int iReqId, string sEventStatus, string sResPacket, string sStartDTTM, string sFinishDTTM, string sDSN)
		{
			LocalCache objCache = null;	
			int iEventStatus = 0;
			int iForeignEventId = 0;
			string sDTTM = "";

			int iReturn = 0;
			bool bRecExists = false;

			try
			{
				if(iEventId == 0 || iReqId == 0)
					return iReturn;

				objCache = new LocalCache(sDSN,0);

				iForeignEventId = 0;
				iEventStatus = objCache.GetCodeId(sEventStatus, "LSS_STATUS");
				sDTTM = Conversion.GetDateTime(DateTime.Now.ToString());

				object objRecordExists = DbFactory.ExecuteScalar(sDSN, "SELECT EVENT_ID FROM DX_REQUEST_EVENT WHERE EVENT_ID = " + iEventId + " AND REQUEST_ID = " + iReqId);
				if (objRecordExists != null)
				{
					bRecExists = true;
				}

				if(! bRecExists)
				{
					//record does not exist. create new entry.
					DbFactory.ExecuteNonQuery(sDSN, "INSERT INTO DX_REQUEST_EVENT (EVENT_ID, FOREIGN_EVENT_ID, REQUEST_ID, EVENT_STATUS, DTTM_RCD_ADDED, EVENT_START_DTTM, EVENT_END_DTTM, RESULT_XML) VALUES (" + iEventId + "," + iForeignEventId + "," + iReqId + "," + iEventStatus + ",'" + sDTTM + "','" + sStartDTTM + "','" + sFinishDTTM + "','" + sResPacket.Replace("'","''") + "')", new Dictionary<string, string>());
				}
				else
				{
					//record already exist. update that.
                    DbFactory.ExecuteNonQuery(sDSN, "UPDATE DX_REQUEST_EVENT SET EVENT_STATUS = " + iEventStatus + ", EVENT_END_DTTM = '" + sFinishDTTM + "', RESULT_XML = '" + sResPacket.Replace("'", "''") + "' WHERE EVENT_ID = " + iEventId + " AND REQUEST_ID = " + iReqId, new Dictionary<string, string>());
			
				}

				iReturn = iEventId;			//return iEventId on successful else return 0
			}			//try end
			catch(Exception p_objException)
			{
				iReturn = 0;
			}
			finally
			{
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
			}

			return iReturn;
		}

		/// Name		: iLogDXEventHist
		/// Author		: Manoj Agrawal
		/// Date Created: 09/20/2006
		/// <summary>		
		/// This method is used to log entry in DX_EVENTHISTORY table
		/// </summary>
        public static int iLogDXEventHist(int iEventId, string sEventStatus, string sDSN, int p_iClientId)
		{
			DbConnection objConn = null;
			LocalCache objCache = null;
			int iHistoryId = 0;
			int iEventStatus = 0;
			string sDTTM = "";

			int iReturn = 0;

			try
			{
				if(iEventId == 0)
					return iReturn;

				objCache = new LocalCache(sDSN,0);

                iHistoryId = Utilities.GetNextUID(sDSN, "DX_EVENTHISTORY", p_iClientId);
				iEventStatus = objCache.GetCodeId(sEventStatus, "LSS_STATUS");
				sDTTM = Conversion.GetDateTime(DateTime.Now.ToString());

				objConn = DbFactory.GetDbConnection(sDSN);
				objConn.Open();
				objConn.ExecuteNonQuery("INSERT INTO DX_EVENTHISTORY (HISTORY_ID, EVENT_ID, EVENT_STATUS, DTTM_RCD_ADDED) VALUES (" + iHistoryId + "," + iEventId + "," + iEventStatus + ",'" + sDTTM + "')");
				objConn.Close();
				//objConn = null;

				iReturn = iHistoryId;			//return iHistoryId on successful else return 0
			}			//try end
			catch(Exception p_objException)
			{
				iReturn = 0;
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Dispose();
                }

				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
			}

			return iReturn;
		}

		/// Name		: iLogDXEventException
		/// Author		: Manoj Agrawal
		/// Date Created: 09/20/2006
		/// <summary>		
		/// This method is used to log entry in DX_EVENTEXCEPTIONS table
		/// </summary>
        public static int iLogDXEventException(int iEventId, string sExcpMsg, string sSource, string sStckTrc, string sDSN, int p_iClientId)
		{
			DbConnection objConn = null;
			LocalCache objCache = null;
			int iExceptionId = 0;
			string sDTTM = "";

			int iReturn = 0;

			try
			{
				if(iEventId == 0)
					return iReturn;

				objCache = new LocalCache(sDSN,0);

                iExceptionId = Utilities.GetNextUID(sDSN, "DX_EVENTEXCEPTIONS", p_iClientId);
				sDTTM = Conversion.GetDateTime(DateTime.Now.ToString());

				objConn = DbFactory.GetDbConnection(sDSN);
				objConn.Open();
				objConn.ExecuteNonQuery("INSERT INTO DX_EVENTEXCEPTIONS (EXCEPTION_ID, EVENT_ID, EXCEPTION_MSG, EXCEPTION_SOURCE, EXCEPTION_STACKTR, DTTM_RCD_ADDED) VALUES (" + iExceptionId + "," + iEventId + ",'" + sExcpMsg.Replace("'","''") + "','" + sSource.Replace("'","''") + "','" + sStckTrc.Replace("'","''") + "','" + sDTTM + "')");
				objConn.Close();
				//objConn = null;

				iReturn = iExceptionId;			//return iExceptionId on successful else return 0
			}			//try end
			catch(Exception p_objException)
			{
				iReturn = 0;
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Dispose();
                }

				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
			}

			return iReturn;
		}		
		#endregion
	}
}
