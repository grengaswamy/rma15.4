using System;
using System.Xml;

	/**************************************************************
	 * $File		: ProcessFactory.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 22/09/2005
	 * $Author		: Parag Sarin
	 * $Comment		: ProcessFactory class
	 * $Source		:  	
	 ************************************************************
	 * Amendment History
	 * ************************************************************
	 * Date Amended		*	Author		*	Amendment   
	 * 07/31/2006			Manoj			Changed the names & structure as per latest design and to include other interfaces
	**************************************************************/

namespace Riskmaster.Application.LSSInterface
{

	//Manoj - commented below code. no use here now.
//	public class PaymentProcessFactory:IPaymentProcessFactory
//	{
//		private string m_sDSN="";
//		private IPaymentProcess objPayment=null;
//		public PaymentProcessFactory(string p_sDSN)
//		{
//			m_sDSN=p_sDSN;
//		}
//		public IPaymentProcess CreatePaymentProcess()
//		{
//			objPayment=new LSSPaymentProcess(m_sDSN);
//			return objPayment;
//		}
//	}

	//Manoj - Changed name & structure to include other interfaces also
	//public class ClaimProcessFactory:IClaimProcessFactory
	public class ExportProcessFactory:IExportProcessFactory
	{

        private string m_sUser = string.Empty, m_sPwd = string.Empty, m_sDsnName = string.Empty;
        private int m_iClientId = 0;

        #region IProcess Members

        /// <summary>
        /// Gets and sets the Riskmaster Database Connection String
        /// </summary>
        public string RiskmasterConnectionString
        {
            get;
            set;
        } // property RiskmasterConnectionString

        #endregion


		//private IClaimProcess objClaim=null;
		private IExportProcess objExport=null;

		//public ClaimProcessFactory(string p_sDSN,string p_sUserName,string p_sPassword,string p_sDsnName)
        public ExportProcessFactory(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sUser=p_sUserName;
			m_sPwd=p_sPassword;
			m_sDsnName=p_sDsnName;

            this.RiskmasterConnectionString = CommonFunctions.GetRiskmasterConnectionString(m_sUser, m_sPwd, m_sDsnName,m_iClientId);
		}

		//public IClaimProcess CreateClaimProcess()
        //{
		//	objClaim=new LSSClaimProcess(m_sDSN,m_sUser,m_sPwd,m_sDsnName);
		//	return objClaim;
		//}
		public IExportProcess CreateExportProcess()
		{
            objExport = new LSSExportProcess(m_sUser, m_sPwd, m_sDsnName, m_iClientId);
			return objExport;
		}



    }


	//Manoj - added new class for Import
	public class ImportProcessFactory:IImportProcessFactory
	{
		private string m_sDSN="";
		private string m_sUser="";
		private string m_sPwd="";
		private string m_sDsnName="";

		private IImportProcess objImport = null;
        private int m_iClientId = 0;
        public ImportProcessFactory(string p_sDSN, string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sDSN=p_sDSN;
			m_sUser=p_sUserName;
			m_sPwd=p_sPassword;
			m_sDsnName=p_sDsnName;
		}

		public IImportProcess CreateImportProcess()
		{
            objImport = new LSSImportProcess(m_sUser, m_sPwd, m_sDsnName, m_iClientId);
			return objImport;
		}
	}

}
