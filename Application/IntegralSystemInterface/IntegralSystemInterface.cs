﻿using System;
using System.Xml;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Settings;
using Riskmaster.Security;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Xml.XPath;
using Riskmaster.DataModel;
using Riskmaster.Models;

namespace Riskmaster.Application.IntegralSystemInterface
{
    public class IntegralSystemInterface
    {
        private string m_sConnectString = string.Empty;
        private string m_sUserName = string.Empty;
        private string m_sDSNName = string.Empty;
        private string m_sPassword = string.Empty;
        private int m_iClientId = 0;
        private string m_sLangCode = string.Empty;

        public IntegralSystemInterface(string conn, int p_iClientId)
        {
            m_sConnectString = conn;
            m_iClientId = p_iClientId;
        }

        public IntegralSystemInterface(UserLogin objUserLogin, int p_iClientId)
        {
            m_sConnectString = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sUserName = objUserLogin.LoginName;
            m_sDSNName = objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sPassword = objUserLogin.Password;
            m_iClientId = p_iClientId;
            m_sLangCode = objUserLogin.objUser.NlsCode.ToString();
        }

        public int SavePolicy(int iPolicyId, string sSessionId, string sPolicyXML,string sSoapXML)
        {
            string sFilePath = string.Empty;
            DataObject objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sExternalPolicyId = string.Empty;
            XmlNode objNode = null;
            string sSQL = string.Empty;
            string sPolNumber = string.Empty;
            string sPolModule = string.Empty;
            string sPolSymbol = string.Empty;
            try
            {
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sPolicyXML);
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objData = (Policy)objDMF.GetDataModelObject("Policy", false);
                objNode = null;
                if (iPolicyId > 0)
                {
                    objNode = objXmlDocument.SelectSingleNode("//PolicyId");
                    objNode.InnerText = iPolicyId.ToString();
                    objNode = null;
                    objData.MoveTo(iPolicyId);
                }
                objData.PopulateObject(objXmlDocument);
                if ((objData as Policy).PolicyId <= 0)  // insert record in policy_x_insurer table for new policy 
                {
                    if ((objData as Policy).InsurerEid != 0)
                    {
                        PolicyXInsurer objPolIns = (objData as Policy).PolicyXInsurerList.AddNew(); ;
                        objPolIns.InsurerCode = (objData as Policy).InsurerEid;
                        objPolIns.PolicyId = (objData as Policy).PolicyId;
                        objPolIns.ResPercentage = 100.0;
                        objPolIns.PrimaryInsurer = true;
                    }
                }
                else // for existing policy, 
                {
                    if ((objData as Policy).PolicyXInsurerList.Count == 0 && (objData as Policy).InsurerEid != 0) //Count = 0 => record does not exists in POLICY_X_INSURER; InsurereEid != 0 => there is insurer info to be saved
                    {
                        PolicyXInsurer objPolIns = (objData as Policy).PolicyXInsurerList.AddNew(); ;
                        objPolIns.InsurerCode = (objData as Policy).InsurerEid;
                        objPolIns.PolicyId = (objData as Policy).PolicyId;
                        objPolIns.ResPercentage = 100.0;
                        objPolIns.PrimaryInsurer = true;
                    }
                }
                objData.FiringScriptFlag = 2;
                (objData as Policy).ExternalPolicyKey = (objData as Policy).PolicySymbol + "," + (objData as Policy).PolicyNumber + "," + (objData as Policy).Module + "," + (objData as Policy).MasterCompany + "," + (objData as Policy).LocationCompany;
                SavePolicyDownloadXMLData(iPolicyId, sSoapXML, (objData as Policy).PolicySystemId, objData.Table, objData);
                objData.Save();
                if (File.Exists(sFilePath))
                    File.Delete(sFilePath);
                return ((objData as Policy).PolicyId);
            }
            catch (Exception e)
            {
                if (objData.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objData.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
                objXmlDocument = null;
            }
        }
        public int GetPSMappedCodeIDFromRMXCodeId(int RMXCodeId, string TableName, string ExceptionParameter, string sPolicySystemID) // aaggarwal29: Code mapping change
        {
            string sSQL = string.Empty;
            int PSMappedCodeID = 0;
            DbReader objDbReader = null;
            SysSettings objSettings = null;
            try
            {
                objSettings = new SysSettings(m_sConnectString, m_iClientId);
                if (objSettings.UseCodeMapping)
                {
                    sSQL = "SELECT POLICY_CODE_MAPPING.PS_CODE_ID FROM POLICY_CODE_MAPPING INNER JOIN GLOSSARY ON POLICY_CODE_MAPPING.RMX_TABLE_ID = GLOSSARY.TABLE_ID" +
                            " INNER JOIN CODES ON POLICY_CODE_MAPPING.PS_CODE_ID = CODES.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME ='" + TableName +
                            "' AND POLICY_CODE_MAPPING.RMX_CODE_ID =" + RMXCodeId + " AND CODES.DELETED_FLAG <> -1"
                            + " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID =" + sPolicySystemID; // aaggarwal29: Code mapping change
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    if (objDbReader.Read())
                    {
                        PSMappedCodeID = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                    }
                }
                if (int.Equals(PSMappedCodeID, 0))
                {
                    if (TableName == "STATES")
                        return 0;
                    else
                        PSMappedCodeID = RMXCodeId;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(String.Format(Globalization.GetString("PolicySystemInterface.GetPSMappedCodesFromRMXCode.FetchingMappedCodesError",m_iClientId,m_sLangCode), ExceptionParameter), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objSettings = null;
            }
            return PSMappedCodeID;
        }
        public int GetPSMappedCodeIDFromRMXCodeId(int RMXCodeId, string TableName, string ExceptionParameter, string sPolicySystemID, int ClaimTypeCd)
        {
            string sSQL = string.Empty;
            int PSMappedCodeID = 0;
            DbReader objDbReader = null;
            SysSettings objSettings = null;
            try
            {
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                if (objSettings.UseCodeMapping)
                {
                    sSQL = "SELECT POLICY_CODE_MAPPING.PS_CODE_ID FROM POLICY_CODE_MAPPING INNER JOIN GLOSSARY ON POLICY_CODE_MAPPING.RMX_TABLE_ID = GLOSSARY.TABLE_ID" +
                            " INNER JOIN CODES ON POLICY_CODE_MAPPING.PS_CODE_ID = CODES.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME ='" + TableName +
                            "' AND POLICY_CODE_MAPPING.RMX_CODE_ID =" + RMXCodeId + " AND CODES.DELETED_FLAG <> -1"
                            + " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID =" + sPolicySystemID   // aaggarwal29: Code mapping change
                            + " AND POLICY_CODE_MAPPING.CLAIM_TYPE_CODE =" + ClaimTypeCd;
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    if (objDbReader.Read())
                    {
                        PSMappedCodeID = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                    }
                }
                if (int.Equals(PSMappedCodeID, 0))
                {
                    if (TableName == "STATES")
                        return 0;
                    else
                        PSMappedCodeID = RMXCodeId;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(String.Format(Globalization.GetString("PolicySystemInterface.GetPSMappedCodesFromRMXCode.FetchingMappedCodesError",m_iClientId,m_sLangCode), ExceptionParameter), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objSettings = null;
            }
            return PSMappedCodeID;
        }
        public int SaveInsurerEntity(string sSessionId, string sDataModelData)
        {
            string sFilePath = string.Empty;
            Entity objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            string sSql = string.Empty;
            XmlDocument objXmlDocument = null;
            int iEntityId = 0;
            bool bDataExists = false;
            string sRefNo = string.Empty;
            //EntityXRole objEntityRole = null; //Payal,RMA:7909
            try
            {
                sFileContent = sDataModelData;
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sFileContent);
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objData = (Entity)objDMF.GetDataModelObject("Entity", false);
                sRefNo = objXmlDocument.SelectSingleNode("//ReferenceNumber").InnerText;
                if (!string.IsNullOrEmpty(sRefNo))
                {
                    sSql = "SELECT POLICY_X_INSURER.INSURER_CODE FROM ENTITY, POLICY_X_INSURER, POLICY_X_WEB, POLICY WHERE  ENTITY.ENTITY_ID = POLICY_X_INSURER.INSURER_CODE and POLICY_X_INSURER.POLICY_ID = POLICY.POLICY_ID and POLICY.POLICY_SYSTEM_ID = POLICY_X_WEB.POLICY_SYSTEM_ID and REFERENCE_NUMBER= '" + sRefNo + "'";
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSql))
                    {
                        if (objRdr.Read())
                        {
                            iEntityId = objRdr.GetInt32(0);
                            bDataExists = true;
                        }
                    }
                }
                if (!bDataExists)
                {
                    objData.PopulateObject(objXmlDocument);
                    objData.FiringScriptFlag = 2;
                    objData.Save();
                    iEntityId = objData.EntityId;
                }
                
                if (File.Exists(sFilePath))
                    File.Delete(sFilePath);
                return iEntityId;
            }
            catch (Exception e)
            {
                if (objData.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objData.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }

                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
			
                objXmlDocument = null;
            }
        }
        public int SaveEntity(string sDataModelXML, string sMode, ref int iAddressId, ref string sAddedRole,int iPolicyId = 0)
        {
            int iRecordId = 0;
            Entity objEntity = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            XmlNode objNode = null;
            int iEntityId = 0;
            LocalCache objCache = null;
            string sAgentNumber = string.Empty;
            int iAddressType = 0;
            bool bSuccess = false;
            XmlNode oAddressListNode = null;
            //EntityXRole objEntityRole = null; //Payal,RMA:7909 
            int iEntityTableId = 0; //Payal,RMA:7909 
            try
            {
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                objDoc = new XmlDocument();
                objDoc.LoadXml(sDataModelXML);
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objEntity = (Entity)objDmf.GetDataModelObject("Entity", false);
                objNode = objDoc.SelectSingleNode("//AddressType");
                iAddressType = string.IsNullOrEmpty(objNode.InnerText) ? 0 : Conversion.CastToType<int>(objNode.InnerText, out bSuccess);
                oAddressListNode = objDoc.SelectSingleNode("//Entity/EntityXAddressesList");
                oAddressListNode.ParentNode.RemoveChild(oAddressListNode);
                switch (sMode)
                {
                    case "driver":
                        iEntityId = CheckDriverDuplication(sDataModelXML, objCache.GetTableId("DRIVERS"));
                        sAddedRole = objCache.GetTableId("DRIVER_INSURED").ToString();
                        break;
                    case "agent":
                        sAgentNumber = objDoc.SelectSingleNode("//ReferenceNumber").InnerText;
                        iEntityId = AgentExists(iPolicyId, objCache.GetTableId("AGENTS"), sAgentNumber);
                        sAddedRole = objCache.GetTableId("AGENTS").ToString();
                        break;
                    case "policy owner":
                        iEntityId = CheckEntityDuplication(sDataModelXML, "POLICY_X_INSURED", sMode);
                        sAddedRole = objCache.GetTableId("POLICY_INSURED").ToString();
                        break;
                    default:
                        iEntityId = CheckEntityDuplication(sDataModelXML, "POLICY_X_ENTITY", sMode);
                        if (Constants.listDriver.Contains(sMode))
                            sAddedRole = objCache.GetTableId("DRIVER_INSURED").ToString();
                        break;
                }
                if (iEntityId > 0)
                {
                    objNode = objDoc.SelectSingleNode("//EntityId");
                    objNode.InnerText = iEntityId.ToString();
                    objEntity.MoveTo(iEntityId);
                }
                objEntity.PopulateObject(objDoc);
                //dbisht6 RMA-17946

                if (objDmf.Context.InternalSettings.SysSettings.UseEntityRole && (objEntity.NameType == 0))
                {
                    if (objDmf.Context.LocalCache.GetGlossaryType(iEntityTableId) == 4)
                        objEntity.NameType = objDmf.Context.LocalCache.GetCodeId("BUS", "ENTITY_NAME_TYPE");
                    else
                        objEntity.NameType = objDmf.Context.LocalCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
                }

                //dbisht6 end
                EntityAddressDuplication(ref objEntity, iAddressType, oAddressListNode);
                
				//iEntityTableId = objEntity.EntityTableId;
             //   iEntityTableId = Conversion.ConvertObjToInteger(objDoc.SelectSingleNode("//Entity/EntityTableId"));// JIRA 11974 pgupta215
                iEntityTableId = Conversion.ConvertStrToInteger(objDoc.SelectSingleNode("//Entity/EntityTableId").InnerText); //Payal RMA:19731
                foreach (EntityXAddresses oEntityxaddress in objEntity.EntityXAddressesList)
                {
                    if (oEntityxaddress.PrimaryAddress == -1)
                    {
                        iAddressId = oEntityxaddress.AddressId;
                        break;
                    }
                }
                objEntity.FiringScriptFlag = 2;
                objEntity.Save();
                iRecordId = objEntity.EntityId;
                CreateSubTypeEntity(iRecordId, iEntityTableId);//Added iEntityTableId for RMA:7909 by Payal
            }
            catch (Exception e)
            {
                if (objEntity.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objEntity.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                objDoc = null;
                objNode = null;
                oAddressListNode = null;
            }
            return iRecordId;
        }
        public void SaveDriver(string sXMl, int iEntityId)
        {
            Driver objDriver = null;
            DataModelFactory objDmf = null;
            int iDriverId = 0;
            LocalCache objCache = null;
            XmlDocument objDoc = null;
            string sSQL = "SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID=" + iEntityId;
            try
            {
                objDoc = new XmlDocument();
                objDoc.LoadXml(sXMl);
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                objDriver = (Driver)objDmf.GetDataModelObject("Driver", false);
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objRdr.Read())
                        iDriverId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                if (iDriverId > 0)
                {
                    objDriver.MoveTo(iDriverId);
                }
                objDriver.DriverTypeCode = objCache.GetCodeId("DRIVER_INSURED", "DRIVER_TYPE");
                objDriver.DriverEId = iEntityId;
                objDriver.FiringScriptFlag = 2;
                objDriver.Save();
            }
            catch (Exception e)
            {
                if (objDriver.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objDriver.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }

                throw e;
            }
            finally
            {
                if (objDriver != null)
                    objDriver.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }
       
        public void SaveDriver(string sLicenceDate, string sDriversLicNo, int iEntityId, int iPolicySystemId, int iMaritalStatus)
        {
            Driver objDriver = null;
            DataModelFactory objDmf = null;
            int iDriverId = 0;
            LocalCache objCache = null;
            string sSQL = "SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID=" + iEntityId;
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objRdr.Read())
                        iDriverId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                objDriver = (Driver)objDmf.GetDataModelObject("Driver", false);
                if (iDriverId > 0)
                    objDriver.MoveTo(iDriverId);
                objDriver.DriverTypeCode = objCache.GetCodeId("DRIVER_INSURED", "DRIVER_TYPE");
                objDriver.LicenceDate = sLicenceDate;
                objDriver.DriversLicNo = sDriversLicNo;
                objDriver.DriverEId = iEntityId;
                objDriver.MaritalStatCode = iMaritalStatus;
                objDriver.FiringScriptFlag = 2;
                objDriver.Save();
            }
            catch (Exception e)
            {
                if (objDriver.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objDriver.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objDriver != null)
                    objDriver.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }
        public void CreateSubTypeEntity(int iEntityId, int iEntityTableId) //Added iEntityTableId for RMA:7909 by Payal
        {
            string sSQL = string.Empty;
            string entityTableName = string.Empty;
            Entity objEntity = null;
            Employee objEmployee = null;
            PiEmployee objPiEmployee = null;
            Patient objPatient = null;
            PiPatient objPiPatient = null;
            MedicalStaff objMedicalStaff = null;
            Physician objPhysician = null;
            DataModelFactory objDMF = null;
            try
            {
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
             // Payal,RMA:7909--Starts
			   /*
			   // objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
               // objEntity.MoveTo(iEntityId);
                //entityTableName = objEntity.Context.LocalCache.GetTableName(objEntity.EntityTableId);
                */
                entityTableName = objDMF.Context.LocalCache.GetTableName(iEntityTableId);
               // Payal,RMA:7909--Ends 
				switch (entityTableName.ToUpper())
                {
                    case "EMPLOYEES":
                        string sEmpNumber = string.Empty;
                        sSQL = "SELECT EMPLOYEE_EID, EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objEmployee = (Employee)objDMF.GetDataModelObject("Employee", false);
                                sEmpNumber = "ExtE" + CommonFunctions.GetUniqueRandomNumber();
                                objEmployee.EmployeeNumber = sEmpNumber;
                                objEmployee.EmployeeEid = iEntityId;
                                objEmployee.FiringScriptFlag = 2;
                                objEmployee.Save();
                                objEmployee.Dispose();
                            }
                        }
                        break;
                    case "PATIENTS":
                        sSQL = "SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objPatient = (Patient)objDMF.GetDataModelObject("Patient", false);
                                objPatient.PatientEid = iEntityId;
                                objPatient.PatientAcctNo = "ExtP" + CommonFunctions.GetUniqueRandomNumber();
                                objPatient.FiringScriptFlag = 2;
                                objPatient.Save();
                                objPatient.Dispose();
                            }
                        }
                        break;
                    case "MEDICAL_STAFF":
                        sSQL = "SELECT STAFF_EID FROM MED_STAFF WHERE STAFF_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objMedicalStaff = (MedicalStaff)objDMF.GetDataModelObject("MedicalStaff", false);
                                objMedicalStaff.StaffEid = iEntityId;
                                objMedicalStaff.MedicalStaffNumber = "ExtM" + CommonFunctions.GetUniqueRandomNumber();
                                objMedicalStaff.FiringScriptFlag = 2;
                                objMedicalStaff.Save();
                                objMedicalStaff.Dispose();
                            }
                        }
                        break;
                    case "PHYSICIANS":
                        sSQL = "SELECT PHYS_EID FROM PHYSICIAN WHERE PHYS_EID = " + iEntityId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (!objReader.Read())
                            {
                                objPhysician = (Physician)objDMF.GetDataModelObject("Physician", false);
                                objPhysician.PhysEid = iEntityId;
                                objPhysician.PhysicianNumber = "ExtPH" + CommonFunctions.GetUniqueRandomNumber();
                                objPhysician.FiringScriptFlag = 2;
                                objPhysician.Save();
                                objPhysician.Dispose();
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                if (objEmployee != null)
                {

                    if (objEmployee.Context.ScriptValidationErrors.Count > 0)
                    {
                        foreach (System.Collections.DictionaryEntry objError in objEmployee.Context.ScriptValidationErrors)
                            throw new Exception(objError.Value.ToString());
                    }
                }
                if (objPiEmployee != null)
                {

                    if (objPiEmployee.Context.ScriptValidationErrors.Count > 0)
                    {
                        foreach (System.Collections.DictionaryEntry objError in objPiEmployee.Context.ScriptValidationErrors)
                            throw new Exception(objError.Value.ToString());
                    }
                }
                if (objPatient != null)
                {

                    if (objPatient.Context.ScriptValidationErrors.Count > 0)
                    {
                        foreach (System.Collections.DictionaryEntry objError in objPatient.Context.ScriptValidationErrors)
                            throw new Exception(objError.Value.ToString());
                    }
                }
                if (objPiPatient != null)
                {

                    if (objPiPatient.Context.ScriptValidationErrors.Count > 0)
                    {
                        foreach (System.Collections.DictionaryEntry objError in objPiPatient.Context.ScriptValidationErrors)
                            throw new Exception(objError.Value.ToString());
                    }
                }
                if (objMedicalStaff != null)
                {

                    if (objMedicalStaff.Context.ScriptValidationErrors.Count > 0)
                    {
                        foreach (System.Collections.DictionaryEntry objError in objMedicalStaff.Context.ScriptValidationErrors)
                            throw new Exception(objError.Value.ToString());
                    }
                }
                if (objPhysician != null)
                {

                    if (objPhysician.Context.ScriptValidationErrors.Count > 0)
                    {
                        foreach (System.Collections.DictionaryEntry objError in objPhysician.Context.ScriptValidationErrors)
                            throw new Exception(objError.Value.ToString());
                    }
                }

                

                throw ex;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objEmployee != null)
                    objEmployee.Dispose();
                if (objPiEmployee != null)
                    objPiEmployee.Dispose();
                if (objPatient != null)
                    objPatient.Dispose();
                if (objPiPatient != null)
                    objPiPatient.Dispose();
                if (objMedicalStaff != null)
                    objMedicalStaff.Dispose();
                if (objPhysician != null)
                    objPhysician.Dispose();
                if (objDMF != null)
                    objDMF.Dispose();
            }
        }
        public int SavePolicyXEntity(int iPolicyId, int iEntityId, int iEntityType, int UnitRowId, string sRoleCode,string sXML)
        {
            PolicyXEntity objPolicyXEntity = null;
            DataModelFactory objDmf = null;
            int iRecordId = 0;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            StringBuilder sbSql = new StringBuilder();
            try
            {
                sbSql.Append("SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE ");
                sbSql.Append(" ENTITY_ID=~ENTITYID~");
                strDictParams.Add("ENTITYID", iEntityId.ToString());
                sbSql.Append(" AND POLICY_ID =~POLICYID~");
                strDictParams.Add("POLICYID", iPolicyId.ToString());
                sbSql.Append(" AND TYPE_CODE =~TYPECODE~");
                strDictParams.Add("TYPECODE", iEntityType.ToString());
                sbSql.Append(" AND EXTERNAL_ROLE =~ROLECD~");
                strDictParams.Add("ROLECD", sRoleCode);
                using (DbReader objRdr = DbFactory.ExecuteReader(m_sConnectString, sbSql.ToString(), strDictParams))
                {
                    if (objRdr.Read())
                        iRecordId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objPolicyXEntity = (PolicyXEntity)objDmf.GetDataModelObject("PolicyXEntity", false);
                if (iRecordId > 0)
                    objPolicyXEntity.MoveTo(iRecordId);
                objPolicyXEntity.PolicyId = iPolicyId;
                objPolicyXEntity.EntityId = iEntityId;
                objPolicyXEntity.TypeCode = iEntityType;
                objPolicyXEntity.PolicyUnitRowid = UnitRowId;
                objPolicyXEntity.ExternalRole = sRoleCode;
                objPolicyXEntity.FiringScriptFlag = 2;
                SavePolicyDownloadXMLData(objPolicyXEntity.PolicyEid, sXML, GetPolicySystemId(iPolicyId), objPolicyXEntity.Table, objPolicyXEntity);
                objPolicyXEntity.Save();
                iRecordId = objPolicyXEntity.PolicyEid;
            }
            catch (Exception e)
            {
                if (objPolicyXEntity.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objPolicyXEntity.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objPolicyXEntity != null)
                    objPolicyXEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
            }
            return iRecordId;
        }
        public int SavePolicyXEntity(int iPolicyId, int iEntityId, int iEntityType, int UnitRowId, string sRoleCode, int iAddressId,string sXML)
        {
            PolicyXEntity objPolicyXEntity = null;
            DataModelFactory objDmf = null;
            int iRecordId = 0;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            StringBuilder sbSql = new StringBuilder();
            try
            {
                sbSql.Append("SELECT POLICYENTITY_ROWID FROM POLICY_X_ENTITY WHERE ");
                sbSql.Append(" ENTITY_ID=~ENTITYID~");
                strDictParams.Add("ENTITYID", iEntityId.ToString());
                sbSql.Append(" AND POLICY_ID =~POLICYID~");
                strDictParams.Add("POLICYID", iPolicyId.ToString());
                sbSql.Append(" AND TYPE_CODE =~TYPECODE~");
                strDictParams.Add("TYPECODE", iEntityType.ToString());
                sbSql.Append(" AND EXTERNAL_ROLE =~ROLECD~");
                strDictParams.Add("ROLECD", sRoleCode);
                sbSql.Append(" AND ADDRESS_ID =~ADDRESSID~");
                strDictParams.Add("ADDRESSID", iAddressId.ToString());
                using (DbReader objRdr = DbFactory.ExecuteReader(m_sConnectString, sbSql.ToString(), strDictParams))
                {
                    if (objRdr.Read())
                        iRecordId = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                }
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objPolicyXEntity = (PolicyXEntity)objDmf.GetDataModelObject("PolicyXEntity", false);
                if (iRecordId > 0)
                    objPolicyXEntity.MoveTo(iRecordId);
                objPolicyXEntity.PolicyId = iPolicyId;
                objPolicyXEntity.EntityId = iEntityId;
                objPolicyXEntity.TypeCode = iEntityType;
                objPolicyXEntity.PolicyUnitRowid = UnitRowId;
                objPolicyXEntity.ExternalRole = sRoleCode;
                objPolicyXEntity.AddressId = iAddressId;
                objPolicyXEntity.FiringScriptFlag = 2;
                SavePolicyDownloadXMLData(objPolicyXEntity.PolicyEid, sXML, GetPolicySystemId(iPolicyId), objPolicyXEntity.Table, objPolicyXEntity);
                objPolicyXEntity.Save();
                iRecordId = objPolicyXEntity.PolicyEid;
            }
            catch (Exception e)
            {
                if (objPolicyXEntity.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objPolicyXEntity.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objPolicyXEntity != null)
                    objPolicyXEntity.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
            }
            return iRecordId;
        }
        public int SaveCoverage(string sXML, int iPolicyXCvgRowId,string sSoapXML, int iPolicySystemId)
        {
            PolicyXCvgType objPolicyXCvgType = null;
            DataModelFactory objDMF = null;
            XmlDocument resultDoc = null;
            int iRecordId = 0;
            XmlNode objNode = null;
            try
            {
                resultDoc = new XmlDocument();
                objDMF = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                resultDoc.LoadXml(sXML);
                objPolicyXCvgType = (PolicyXCvgType)objDMF.GetDataModelObject("PolicyXCvgType", false);
                if (iPolicyXCvgRowId > 0)
                {
                    objNode = resultDoc.SelectSingleNode("//PolcvgRowId");
                    objNode.InnerText = iPolicyXCvgRowId.ToString();
                    objNode = null;
                    objPolicyXCvgType.MoveTo(iPolicyXCvgRowId);
                }
                objPolicyXCvgType.PopulateObject(resultDoc);
                objPolicyXCvgType.FiringScriptFlag = 2;
                SavePolicyDownloadXMLData(objPolicyXCvgType.PolcvgRowId, sSoapXML, iPolicySystemId, objPolicyXCvgType.Table, objPolicyXCvgType);
                objPolicyXCvgType.Save();
                iRecordId = objPolicyXCvgType.PolcvgRowId;
                objPolicyXCvgType.Dispose();
            }
            catch (Exception e)
            {
                if (objPolicyXCvgType.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objPolicyXCvgType.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objPolicyXCvgType != null)
                    objPolicyXCvgType.Dispose();
            }
            return iRecordId;
        }
        public int SaveVehicle(string sXMl, int iPolicyId, UnitData oUnitData)
        {
            int iRecordId = 0;
            Vehicle objVehicle = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            PolicyXUnit objPolicyXUnit = null;
            int iPolicyUnitId = 0;
            try
            {
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                iPolicyUnitId = CheckUnitDuplication(iPolicyId, Constants.UNIT_TYPE_INDICATOR.VEHICLE, 0, GetPolicySystemId(iPolicyId), oUnitData);
                if (iPolicyUnitId > 0)
                {
                    objPolicyXUnit.MoveTo(iPolicyUnitId);
                    iRecordId = objPolicyXUnit.UnitId;
                }
                else
                {
                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXMl);
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                    objVehicle = (Vehicle)objDmf.GetDataModelObject("Vehicle", false);
                    objVehicle.PopulateObject(objDoc);
                    objVehicle.FiringScriptFlag = 2;
                    objVehicle.Save();
                    iRecordId = objVehicle.UnitId;
                }
            }
            catch (Exception e)
            {
                if (objVehicle.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objVehicle.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objVehicle != null)
                    objVehicle.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                objDoc = null;
            }
            return iRecordId;
        }
        public int SaveProperty(string sXMl, int iPolicyId, UnitData oUnitData)
        {
            int iRecordId = 0;
            PropertyUnit objProperty = null;
            DataModelFactory objDmf = null;
            XmlDocument objDoc = null;
            PolicyXUnit objPolicyXUnit = null;
            int iPolicyUnitId = 0;
            try
            {
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                iPolicyUnitId = CheckUnitDuplication(iPolicyId, Constants.UNIT_TYPE_INDICATOR.PROPERTY, 0, GetPolicySystemId(iPolicyId), oUnitData);
                if (iPolicyUnitId > 0)
                {
                    objPolicyXUnit.MoveTo(iPolicyUnitId);
                    iRecordId = objPolicyXUnit.UnitId;
                }
                else
                {
                    objDoc = new XmlDocument();
                    objDoc.LoadXml(sXMl);
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                    objProperty = (PropertyUnit)objDmf.GetDataModelObject("PropertyUnit", false);
                    objProperty.PopulateObject(objDoc);
                    objProperty.FiringScriptFlag = 2;
                    objProperty.Save();
                    iRecordId = objProperty.PropertyId;
                }
            }
            catch (Exception e)
            {
                if (objProperty.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objProperty.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objProperty != null)
                    objProperty.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                objDoc = null;
            }
            return iRecordId;
        }
        public int SavePolicyXUnit(int iPolicyId, int iUnitId, Constants.UNIT_TYPE_INDICATOR sUnitType, int iPolicySystemId, UnitData oUnitData,string sXMl)
        {
            int iRecordId = 0;
            PolicyXUnit objPolicyXUnit = null;
            DataModelFactory objDmf = null;
            int iPolicyUnitId = 0;
            try
            {
                objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
                objPolicyXUnit = (PolicyXUnit)objDmf.GetDataModelObject("PolicyXUnit", false);
                iPolicyUnitId = CheckUnitDuplication(iPolicyId, sUnitType, iUnitId, iPolicySystemId, oUnitData);
                if (iPolicyUnitId > 0)
                    objPolicyXUnit.MoveTo(iPolicyUnitId);
                objPolicyXUnit.PolicyId = iPolicyId;
                objPolicyXUnit.UnitId = iUnitId;
                objPolicyXUnit.UnitType = CommonFunctions.GetUnitTypeIndicatorText(sUnitType);
                objPolicyXUnit.FiringScriptFlag = 2;
                SavePolicyDownloadXMLData(iPolicyUnitId, sXMl, iPolicySystemId, objPolicyXUnit.Table, objPolicyXUnit);
                objPolicyXUnit.Save();
                iRecordId = objPolicyXUnit.PolicyUnitRowId;
            }
            catch (Exception e)
            {
                if (objPolicyXUnit.Context.ScriptValidationErrors.Count > 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in objPolicyXUnit.Context.ScriptValidationErrors)
                        throw new Exception(objError.Value.ToString());
                }
                throw e;
            }
            finally
            {
                if (objPolicyXUnit != null)
                    objPolicyXUnit.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
            }
            return iRecordId;
        }
        public void SavePolicyUnitData(int iUnitId, Constants.UNIT_TYPE_INDICATOR sUnitType, string sUnitNumber)
        {
            DbConnection objCon = null;
            bool bDataExists = false;
            StringBuilder sSQLSelect;
            Dictionary<string, string> strDictParamsSelect = new Dictionary<string, string>();
            DbWriter objwriter = null;
            int rowid;
            try
            {
                sSQLSelect = new StringBuilder("SELECT * FROM INTEGRAL_UNIT_DATA WHERE UNIT_ID=");
                sSQLSelect.Append(iUnitId);
                #region  setting up query for select purpose
                sSQLSelect.Append(" AND UNIT_TYPE= ~sUNITTYPE~ AND UNIT_NUMBER= ~sUNITNUMBER~ ");
                strDictParamsSelect.Add("sUNITTYPE", CommonFunctions.GetUnitTypeIndicatorText(sUnitType));
                strDictParamsSelect.Add("sUNITNUMBER", sUnitNumber);
                #endregion
                #region new region for dbwriter insert operation
                objCon = DbFactory.GetDbConnection(this.m_sConnectString);
                objCon.Open();
                objwriter = DbFactory.GetDbWriter(objCon);
                objwriter.Tables.Add("INTEGRAL_UNIT_DATA");
                rowid = Utilities.GetNextUID(m_sConnectString, "INTEGRAL_UNIT_DATA",m_iClientId);
                objwriter.Fields.Add("ROW_ID", rowid);
                objwriter.Fields.Add("UNIT_ID", iUnitId);
                objwriter.Fields.Add("UNIT_TYPE", CommonFunctions.GetUnitTypeIndicatorText(sUnitType));
                objwriter.Fields.Add("UNIT_NUMBER", sUnitNumber);
                #endregion
                object objRdr = (DbFactory.ExecuteScalar(m_sConnectString, sSQLSelect.ToString(), strDictParamsSelect));
                {
                    if (objRdr != null)
                    {
                        bDataExists = true;
                    }
                }
                if (!bDataExists)
                {
                    objwriter.Execute();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCon != null)
                {
                    objwriter.Dispose();
                    sSQLSelect = null;
                    strDictParamsSelect = null;
                }
            }
        }
        //public bool SavePolicyDownloadXMLData(int iTableRowId, string sXMLData, int iPolicySystemId, string sTableName)
        //{
        //    LocalCache objCache = null;
        //    StringBuilder sbSQL;
        //    DbConnection objCon = null;
        //    DbCommand objCmd = null;
        //    DbParameter objParameter = null;
        //    int iTableId = 0;
        //    bool bReturn = false;
        //    int iPsRowId = 0;
        //    try
        //    {
        //        objCache = new LocalCache(m_sConnectString,m_iClientId);
        //        objCon = DbFactory.GetDbConnection(m_sConnectString);
        //        sbSQL = new StringBuilder();
        //        iTableId = objCache.GetTableId(sTableName);
        //        sbSQL.Append("SELECT PS_ROW_ID FROM PS_DOWNLOAD_DATA_XML WHERE TABLE_ID = " + iTableId);
        //        sbSQL.Append(" AND TABLE_ROW_ID = " + iTableRowId);
        //        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString()))
        //        {
        //            if (objReader.Read())
        //            {
        //                iPsRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
        //            }
        //        }
        //        sbSQL.Remove(0, sbSQL.Length);
        //        if (iPsRowId == 0)
        //        {
        //            if (DbFactory.GetDatabaseType(m_sConnectString) == eDatabaseType.DBMS_IS_ORACLE)
        //            {
        //                sbSQL.Append("INSERT INTO PS_DOWNLOAD_DATA_XML (PS_ROW_ID, TABLE_ID, TABLE_ROW_ID,POLICY_SYSTEM_ID,DTTM_RCD_LAST_UPD, DTTM_RCD_ADDED,UPDATED_BY_USER,ADDED_BY_USER, ACORD_XML) VALUES (");
        //                sbSQL.Append("SEQ_PS_ROW_ID.NEXTVAL, ");
        //            }
        //            else
        //                sbSQL.Append("INSERT INTO PS_DOWNLOAD_DATA_XML (TABLE_ID, TABLE_ROW_ID,POLICY_SYSTEM_ID,DTTM_RCD_LAST_UPD, DTTM_RCD_ADDED,UPDATED_BY_USER,ADDED_BY_USER, ACORD_XML) VALUES (");
        //            sbSQL.Append(iTableId.ToString() + ", ");
        //            sbSQL.Append(iTableRowId.ToString() + ", ");
        //            sbSQL.Append(iPolicySystemId.ToString() + ", '");
        //            sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "', '");
        //            sbSQL.Append(Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "', '");
        //            sbSQL.Append(m_sUserName.ToString() + "', '");
        //            sbSQL.Append("',~XML~ )");
        //            objCon.Open();
        //            objCmd = objCon.CreateCommand();
        //            objParameter = objCmd.CreateParameter();
        //            objParameter.Value = sXMLData;
        //            objParameter.ParameterName = "XML";
        //            objCmd.Parameters.Add(objParameter);
        //            objCmd.CommandText = sbSQL.ToString();
        //            sbSQL.Remove(0, sbSQL.Length);
        //            objCmd.ExecuteNonQuery();
        //            bReturn = true;
        //        }
        //        else
        //        {
        //            sbSQL.Append("UPDATE PS_DOWNLOAD_DATA_XML SET DTTM_RCD_LAST_UPD='" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "'");
        //            sbSQL.Append(" , UPDATED_BY_USER='" + m_sUserName.ToString() + "', ACORD_XML= ~XML~ ");
        //            sbSQL.Append(" WHERE  TABLE_ID = " + iTableId);
        //            sbSQL.Append(" AND TABLE_ROW_ID = " + iTableRowId);
        //            objCon.Open();
        //            objCmd = objCon.CreateCommand();
        //            objParameter = objCmd.CreateParameter();
        //            objParameter.Value = sXMLData;
        //            objParameter.ParameterName = "XML";
        //            objCmd.Parameters.Add(objParameter);
        //            objCmd.CommandText = sbSQL.ToString();
        //            sbSQL.Remove(0, sbSQL.Length);
        //            objCmd.ExecuteNonQuery();
        //            bReturn = true;
        //        }
        //        return bReturn;
        //    }
        //    catch (Exception p_objExp)
        //    {
        //        throw new RMAppException(Globalization.GetString("PolicySystemInterface.SavePolicyDownloadXMLData.ExceptioninSaveXMLData", m_iClientId, m_sLangCode), p_objExp);
        //    }
        //    finally
        //    {
        //        if (objCache != null)
        //        {
        //            objCache.Dispose();
        //            objCache = null;
        //        }
        //        if (objCon != null)
        //        {
        //            objCon.Close();
        //            objCon = null;
        //        }
        //    }
        //}

        public void SavePolicyDownloadXMLData(int iTableRowId, string sXMLData, int iPolicySystemId, string sTableName, DataObject objDataobject)
        {

            PsDownloadXMLData objPsDownloadXMLData = null;
            // DataModelFactory objDmf = null;


            // LocalCache objCache = null;
            StringBuilder sbSQL;
            //DbConnection objCon = null;
            //DbCommand objCmd = null;
            //DbParameter objParameter = null;
            int iTableId = 0;
            bool bReturn = false;
            int iPsRowId = 0;
            try
            {
                sbSQL = new StringBuilder();
                //   objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);

                iTableId = objDataobject.Context.LocalCache.GetTableId(sTableName);

                if (iTableRowId > 0)
                {

                    sbSQL.Append("SELECT PS_ROW_ID FROM PS_DOWNLOAD_DATA_XML WHERE TABLE_ID = " + iTableId);
                    sbSQL.Append(" AND TABLE_ROW_ID = " + iTableRowId);




                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString()))
                    {
                        if (objReader.Read())
                        {
                            iPsRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        }
                    }
                    sbSQL.Remove(0, sbSQL.Length);


                }

                objPsDownloadXMLData = (PsDownloadXMLData)objDataobject.Context.Factory.GetDataModelObject("PsDownloadXMLData", false);


                if (iPsRowId > 0)
                {
                    objPsDownloadXMLData.MoveTo(iPsRowId);
                }

                //objPsDownloadXMLData.TableID = iTableId;
                //objPsDownloadXMLData.TableRowID = iTableRowId;
                objPsDownloadXMLData.AcordXML = sXMLData;
                objPsDownloadXMLData.PolicySystemID = iPolicySystemId;

                switch (sTableName)
                {
                    case "POLICY":
                        (objDataobject as Policy).PsDownloadXMLDataList.Add(objPsDownloadXMLData);
                        break;
                    case "POLICY_X_UNIT":
                        (objDataobject as PolicyXUnit).PsDownloadXMLDataList.Add(objPsDownloadXMLData);
                        break;
                    case "POLICY_X_ENTITY":
                        (objDataobject as PolicyXEntity).PsDownloadXMLDataList.Add(objPsDownloadXMLData);
                        break;
                    case "POLICY_X_CVG_TYPE":
                        (objDataobject as PolicyXCvgType).PsDownloadXMLDataList.Add(objPsDownloadXMLData);
                        break;

                }




                // objPsDownloadXMLData.Save();

                // return bReturn;
            }

            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.SavePolicyDownloadXMLData.ExceptioninSaveXMLData", m_iClientId), p_objExp);
            }
            finally
            {
                if (objPsDownloadXMLData != null)
                {
                    objPsDownloadXMLData.Dispose();
                    //  objPsDownloadXMLData = null;
                }
                //if (objDmf != null)
                //{
                //    objDmf.Dispose();
                //    //objDmf = null;
                //}
                //if (objCache != null)
                //{
                //    objCache.Dispose();
                //    objCache = null;
                //}
                //if (objCon != null)
                //{
                //    objCon.Close();
                //    objCon = null;
                //}

            }

        }
        public string GetPolicySystemParamteres(string sParamter, int iPolicySystemId)
        {
            string sReturnValue = string.Empty;
            string sSQL = "SELECT " + sParamter + " FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + iPolicySystemId;
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (objRdr.Read())
                        sReturnValue = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                }


            }
            catch (Exception e)
            {
                throw e;
            }

            return sReturnValue;
        }

        public DataTable GetPolicySystemInfoById(int iPolicySystemId)
        {
            string sSQL = string.Empty;
            string sPolicySystemTypeCode = string.Empty;
            DataSet objDataset = null;
            DataTable objResult = null;
            LocalCache objCache = null;
            try
            {
                if (iPolicySystemId != 0)
                {
                    sSQL = "SELECT * FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID = " + iPolicySystemId;
                    objDataset = DbFactory.GetDataSet(m_sConnectString, sSQL, m_iClientId);

                    if (objDataset != null && objDataset.Tables[0] != null && objDataset.Tables[0].Rows.Count > 0)
                    {
                        objResult = objDataset.Tables[0];
                        if (Conversion.ConvertObjToInt(objResult.Rows[0]["POLICY_SYSTEM_CODE"], m_iClientId) > 0)
                        {
                            objCache = new LocalCache(m_sConnectString,m_iClientId);
                            sPolicySystemTypeCode = objCache.GetShortCode(Conversion.ConvertObjToInt(objResult.Rows[0]["POLICY_SYSTEM_CODE"], m_iClientId));
                            objResult.Columns.Add("POLICY_SYSTEM_TYPE", Type.GetType("System.String"));
                            objResult.Rows[0]["POLICY_SYSTEM_TYPE"] = sPolicySystemTypeCode;
                        }
                    }
                    else
                        throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemInfoById.NoRecordsFound", m_iClientId, m_sLangCode));
                }
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemInfoById.NoRecordsFound", m_iClientId, m_sLangCode), p_objExp);
            }
            finally
            {
                if (objDataset != null)
                    objDataset.Dispose();
                if (objResult != null)
                    objResult.Dispose();
            }
            return objResult;
        }

        public XmlDocument GetInterestListRoles()
        {
            XmlDocument objXmlDocument = null;
            XmlElement objOptionXmlElement = null;
            XmlElement objRootElement = null;
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            LocalCache objCache = null;
            try
            {
                objXmlDocument = new XmlDocument();
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                sSQL = @"SELECT GT.TABLE_NAME, G.TABLE_ID 
							FROM GLOSSARY_TEXT GT, GLOSSARY G 
							WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE=7  
                            AND GT.TABLE_ID <> " + objCache.GetTableId("DRIVERS")
                                + " ORDER BY GT.TABLE_NAME";

                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                objRootElement = objXmlDocument.CreateElement("EntityTypeList");
                objXmlDocument.AppendChild(objRootElement);

                while (objDbReader.Read())
                {

                    objOptionXmlElement = objXmlDocument.CreateElement("option");
                    objOptionXmlElement.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue(0)));
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue(1));
                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader = null;
                }
                if (objCache != null)
                {

                    objCache.Dispose();
                    objCache = null;

                }
            }
            return objXmlDocument;
        }
        private int AgentExists(int iPolicyId, int iEntityType, string sAgentNumber)
        {
            string sSQL = string.Empty;
            StringBuilder strSQL = new StringBuilder();
            int iReturn = 0;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            SysSettings objSettings = null; //Payal; RMA:7909
            try
            {
                objSettings = new SysSettings(m_sConnectString, m_iClientId); //Payal; RMA:7909
                sSQL = "SELECT ENTITY_ID FROM POLICY_X_ENTITY WHERE POLICY_ID =" + iPolicyId + " AND TYPE_CODE=" + iEntityType;
                iReturn = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sSQL.ToString()),m_iClientId);
                if (iReturn == 0)
                {
                    strSQL.Append("SELECT ENTITY_ID FROM ENTITY WHERE ");
                    strSQL.Append("REFERENCE_NUMBER = ~REFERENCENUMBER~ AND ENTITY.DELETED_FLAG=0 "); //Payal Added(ENTITY.DELETED_FLAG=0) : Worked for JIRA - 10471
                    strDictParams.Add("REFERENCENUMBER", sAgentNumber);
                    //Payal Start : Worked for JIRA - 10062
                    if (!objSettings.UseEntityRole)
                        strSQL.Append(" AND ENTITY_TABLE_ID = " + iEntityType + " ORDER BY ENTITY_ID DESC");
                    //Payal End : Worked for JIRA - 10062
                    iReturn = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, strSQL.ToString(), strDictParams), m_iClientId);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                strSQL = null;
                strDictParams = null;
                objSettings = null; //Payal; RMA:7909
            }
            return iReturn;
        }
        public int GetRMXCodeIdFromPSMappedCode(string PSMappedCode, string TableName, string ExceptionParameter, string sPolicySystemID)
        {
            string sSQL = string.Empty;
            int MappedRMXCodeId = 0;
            DbReader objDbReader = null;
            SysSettings objSettings = null;
            LocalCache objCache = null;
            try
            {
                objSettings = new SysSettings(m_sConnectString,m_iClientId);
                if (objSettings.UseCodeMapping)
                {
                    sSQL = "SELECT POLICY_CODE_MAPPING.RMX_CODE_ID FROM POLICY_CODE_MAPPING INNER JOIN GLOSSARY ON POLICY_CODE_MAPPING.RMX_TABLE_ID = GLOSSARY.TABLE_ID" +
                            " INNER JOIN CODES ON POLICY_CODE_MAPPING.PS_CODE_ID = CODES.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME ='" + TableName +
                            "' AND CODES.SHORT_CODE ='" + PSMappedCode + "'" + " AND CODES.DELETED_FLAG <> -1" +
                             " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID = " + sPolicySystemID;
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                    if (objDbReader.Read())
                    {
                        MappedRMXCodeId = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                    }
                }
                if (int.Equals(MappedRMXCodeId, 0))
                {
                    objCache = new LocalCache(m_sConnectString,m_iClientId);
                    if (TableName == "STATES")
                        MappedRMXCodeId = objCache.GetStateRowID(PSMappedCode);
                    else
                        MappedRMXCodeId = objCache.GetCodeId(PSMappedCode, TableName);
                    if (int.Equals(MappedRMXCodeId, 0))
                    {
                        throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterface.GetRMXCodeIdFromPSMappedCode.FetchingMappedCodesError", m_iClientId, m_sLangCode), PSMappedCode, ExceptionParameter));
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterface.GetRMXCodeIdFromPSMappedCode.FetchingMappedCodesError", m_iClientId, m_sLangCode), PSMappedCode, ExceptionParameter), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objSettings = null;
                if (objCache != null)
                    objCache.Dispose();
            }
            return MappedRMXCodeId;
        }
        public int GetPolicySystemId(int p_iPolicyId)
        {
            string sSQL = string.Empty;
            int iRecordId = 0;
            try
            {
                sSQL = "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE POLICY_ID= " + p_iPolicyId;
                using (DbReader oReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    if (oReader.Read())
                    {
                        iRecordId = Conversion.ConvertObjToInt(oReader.GetValue("POLICY_SYSTEM_ID"), m_iClientId);
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }

            return iRecordId;
        }

        public void AddAddressIDToPolicyInsured(int iAddedPolicyId, int iInsuredEid, int iAddressId)
        {
            StringBuilder ssql = new StringBuilder();
            Dictionary<string, dynamic> dictobj = new Dictionary<string, dynamic>();
            ssql.Append(" UPDATE POLICY_X_INSURED SET ADDRESS_ID = ~ADDRESSID~ ");
            ssql.Append(" WHERE POLICY_ID = ~POLICYID~ AND INSURED_EID = ~INSUREDID~ ");
            dictobj.Add("POLICYID", iAddedPolicyId);
            dictobj.Add("INSUREDID", iInsuredEid);
            dictobj.Add("ADDRESSID", iAddressId);
            try
            {
                DbFactory.ExecuteNonQuery(m_sConnectString, ssql.ToString(), dictobj);
            }
            catch (Exception e) { throw e; }
            finally
            {
                ssql = null;
                dictobj = null;
            }
        }
        #region Duplication and Clean Up functions
        public void OrphanPolicyCleanUp(int iPolicyId)
        {
            string sSQL = string.Empty;
            try
            {
                DbWriter oWriter = DbFactory.GetDbWriter(m_sConnectString);
                sSQL = "DELETE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID IN ( SELECT  POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT WHERE POLICY_ID = " + iPolicyId + ") )";
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);
                sSQL = "DELETE FROM POLICY_X_UNIT WHERE POLICY_ID = " + iPolicyId;
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);
                sSQL = "DELETE FROM POLICY_X_ENTITY WHERE POLICY_ID = " + iPolicyId;
                DbFactory.ExecuteNonQueryTransaction(m_sConnectString, sSQL);
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("IntegralSystemInterface.OrphanPolicyCleanUp.Error", m_iClientId, m_sLangCode), p_objExp);
            }
        }
        public XmlDocument GetDuplicateOptions(string p_sMode, string p_sSelectedValues)
        {
            string sFileContent = string.Empty;
            XmlDocument objXmlDocument = null;
            string sSQL = string.Empty;
            string sFilePath = string.Empty;
            string sOuterXml = string.Empty;
            DataObject objData = null;
            DataModelFactory objDmf = null;
            string sTableName = string.Empty;
            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            XmlElement objChildElement = null;
            try
            {
                switch (p_sMode.ToLower())
                {
                    case "vehicle":
                        sSQL = "SELECT UNIT_ID,VIN,VEHICLE_MAKE,VEHICLE_MODEL,VEHICLE_YEAR FROM VEHICLE WHERE VIN IN (" + p_sSelectedValues + ")";
                        break;
                    case "property":
                        sSQL = "SELECT PROPERTY_ID,PIN,ADDR1,CITY,ZIP_CODE FROM PROPERTY_UNIT WHERE PIN IN (" + p_sSelectedValues + ")";
                        break;
                    case "entity":
                    case "driver":
                        sSQL = "SELECT ENTITY_ID,FIRST_NAME,LAST_NAME,TAX_ID FROM ENTITY WHERE TAX_ID IN (" + p_sSelectedValues + ")";
                        break;
                }
                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement("Document");
                objXmlDocument.AppendChild(objRootElement);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objReader.Read())
                    {
                        switch (p_sMode.ToLower())
                        {
                            case "vehicle":
                                objXmlElement = objXmlDocument.CreateElement(p_sMode.ToLower());
                                objRootElement.AppendChild(objXmlElement);
                                objChildElement = objXmlDocument.CreateElement("Vin");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VIN"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("VehicleMake");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_MAKE"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("VehicleModel");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_MODEL"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("VehicleYear");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("VEHICLE_YEAR"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("RowId");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                break;
                            case "property":
                                objXmlElement = objXmlDocument.CreateElement(p_sMode.ToLower());
                                objRootElement.AppendChild(objXmlElement);
                                objChildElement = objXmlDocument.CreateElement("Pin");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("PIN"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("Addr1");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ADDR1"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("City");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("CITY"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("ZipCode");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("RowId");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("PROPERTY_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                break;
                            case "entity":
                            case "driver":
                                objXmlElement = objXmlDocument.CreateElement(p_sMode.ToLower());
                                objRootElement.AppendChild(objXmlElement);
                                objChildElement = objXmlDocument.CreateElement("FirstName");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("LastName");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("TaxID");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TAX_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                objChildElement = objXmlDocument.CreateElement("RowId");
                                objChildElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ENTITY_ID"));
                                objXmlElement.AppendChild(objChildElement);
                                objChildElement = null;
                                break;
                        }
                    }
                }
                return objXmlDocument;
            }
            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.PolicySysFetchingError", m_iClientId, m_sLangCode), p_objException);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDmf != null)
                    objDmf.Dispose();
                if (objData != null)
                    objData.Dispose();
            }
        }
        public int CheckPolicyDuplication(int iClaimId, string sPolNumber, string sPolSymbol, string sPolModule, string sMasterCompany, int iPolicySystemId, ref bool bIsOrphan)
        {
            string sFilePath = string.Empty;
            DataObject objData = null;
            DataModelFactory objDMF = null;
            string sFileContent = string.Empty;
            string sExternalPolicyId = string.Empty;
            int iPolicyId = 0;
            string sSQL = string.Empty;
            XElement objAccordResponse = null;
            string sLocCompany = string.Empty;
            XElement oElementNode = null;
            bIsOrphan = false;
            int iCount = 0;
            try
            {
                if (!string.IsNullOrEmpty(sPolModule) && !string.IsNullOrEmpty(sPolSymbol) && !string.IsNullOrEmpty(sPolNumber))
                {
                    if (iClaimId > 0)
                    {
                        sSQL = "SELECT COUNT(*) from CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iCount = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                            }
                        }
                        if (iCount == 0)
                        {
                            sSQL = "SELECT MAX(POLICY_ID) POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN (SELECT POLICY_ID FROM CLAIM_X_POLICY) AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "'";
                            bIsOrphan = true;
                        }
                        else if (iCount > 0)
                        {
                            sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + iClaimId + ") AND POLICY_SYSTEM_ID= " + iPolicySystemId + " AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "' ";
                        }
                    }
                    else
                    {
                        sSQL = "SELECT POLICY_ID FROM POLICY WHERE POLICY_ID NOT IN ( SELECT POLICY_ID FROM CLAIM_X_POLICY ) AND POLICY_SYSTEM_ID =" + iPolicySystemId + " AND POLICY_SYMBOL ='" + sPolSymbol + "' AND POLICY_NUMBER ='" + sPolNumber + "' AND MODULE = " + sPolModule + " AND MASTER_COMPANY ='" + sMasterCompany + "' ";
                        bIsOrphan = true; // aaggarwal29: MITS 34580
                    }
                }
                if (!string.IsNullOrEmpty(sSQL))
                {
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            iPolicyId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_ID"), m_iClientId);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDMF != null)
                    objDMF.Dispose();
                if (objData != null)
                    objData.Dispose();
                oElementNode = null;
                objAccordResponse = null;
            }
            return iPolicyId;
        }
        public int CheckEntityDuplication(string p_sXML, string p_sTableName, string sMode)
        {
            XElement oElement = null;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            string sAdd = string.Empty;
            string sTaxId = string.Empty;
            string sEmail = string.Empty;
            string sContact = string.Empty;
            int iZip = 0;
            bool bSuccess = false;
            int iTableId = 0;
            XElement xEntityElement = null;
            StringBuilder sbSql = new StringBuilder();
            LocalCache objCache = null;
            int iEntityID = 0;
            string sPolicyFields = string.Empty;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            SysSettings objSettings = null; //Payal; RMA:7909
            try
            {
                objSettings = new SysSettings(m_sConnectString, m_iClientId); //Payal; RMA:7909
                XElement xEntityTemplate = XElement.Parse(p_sXML);
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                xEntityElement = xEntityTemplate.XPathSelectElement("//Entity/ClientSequenceNumber");
                sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY WHERE ENTITY.CLIENT_SEQ_NUM = '" + xEntityElement.Value + "' AND ENTITY.DELETED_FLAG=0 "); //Payal Added (ENTITY.DELETED_FLAG=0) : Worked for JIRA - 10471
                xEntityElement = xEntityTemplate.XPathSelectElement("//Entity/EntityTableId");
                iTableId = Conversion.CastToType<int>(xEntityElement.Value, out bSuccess);

                //Payal Start : Worked for JIRA - 10062
                if (!objSettings.UseEntityRole) //Payal; RMA:7909
                    sbSql.Append(" AND ENTITY.ENTITY_TABLE_ID =" + iTableId + " ORDER BY ENTITY_ID DESC"); //Added for RMA:9851 by Payal
                //Payal; RMA:7909 Ends
                //Payal End : Worked for JIRA - 10062

               // sbSql.Append(" ORDER BY ENTITY.ENTITY_ID DESC"); //Commented for RMA:9851 by Payal
                if (sbSql != null)
                {
                    iEntityID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams), m_iClientId);
                }
                return iEntityID;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objSettings = null; //Payal; RMA:7909
                if (xEntityElement != null)
                    xEntityElement = null;
                if (oElement != null)
                    oElement = null;
            }
        }
        public int CheckUnitDuplication(int p_iPolicyId, Constants.UNIT_TYPE_INDICATOR p_sUnitType, int iUnitId, int p_iPolicySystemId, UnitData oUnitData)
        {
            string sUnitNumber = string.Empty;
            string sUnitRiskLoc = string.Empty;
            string sUnitRiskSubLoc = string.Empty;
            string sUiteSeqNum = string.Empty;
            string insLine = string.Empty;
            string product = string.Empty;
            int iPolicyUnitRowId = 0;
            StringBuilder sbSql = new StringBuilder();
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectString);
            switch (p_sUnitType)
            {
                case Constants.UNIT_TYPE_INDICATOR.VEHICLE:
                    if (oUnitData.UnitNumber != null)
                        sUnitNumber = oUnitData.UnitNumber.Trim();
                    sbSql.AppendFormat("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT INNER JOIN VEHICLE ON POLICY_X_UNIT.UNIT_ID = VEHICLE.UNIT_ID INNER JOIN INTEGRAL_UNIT_DATA on INTEGRAL_UNIT_DATA.UNIT_ID= POLICY_X_UNIT.UNIT_ID  WHERE POLICY_X_UNIT.POLICY_ID = {0} AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND INTEGRAL_UNIT_DATA.UNIT_NUMBER = '{1}' ", p_iPolicyId, sUnitNumber);
                    break;
                case Constants.UNIT_TYPE_INDICATOR.PROPERTY:
                    if (oUnitData.UnitNumber != null)
                        sUnitNumber = oUnitData.UnitNumber.Trim();
                    sbSql.AppendFormat("SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT INNER JOIN PROPERTY_UNIT ON POLICY_X_UNIT.UNIT_ID = PROPERTY_UNIT.PROPERTY_ID INNER JOIN INTEGRAL_UNIT_DATA ON INTEGRAL_UNIT_DATA.UNIT_ID= POLICY_X_UNIT.UNIT_ID WHERE POLICY_X_UNIT.POLICY_ID = {0} AND POLICY_X_UNIT.UNIT_TYPE = 'P' AND INTEGRAL_UNIT_DATA.UNIT_NUMBER = '{1}' ", p_iPolicyId, sUnitNumber);
                    break;
            }
            if (sbSql != null)
            {
                objConn.Open();
                iPolicyUnitRowId = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sbSql.ToString()), m_iClientId);
            }
            return iPolicyUnitRowId;
        }
        public int CheckDriverDuplication(string p_sXML, int iTableId)
        {
            XElement oElement = null;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            string sAdd = string.Empty;
            string sTaxId = string.Empty;
            string sEmail = string.Empty;
            string sContact = string.Empty;
            int iZip = 0;
            bool bSuccess = false;
            XElement oElements = null;
            StringBuilder sbSql = new StringBuilder();
            LocalCache objCache = null;
            int iEntityID = 0;
            string sPolicyFields = string.Empty;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            SysSettings objSettings = null; //Payal Start : Worked for JIRA - 10062
            try
            {
                objSettings = new SysSettings(m_sConnectString, m_iClientId);   //Payal Start : Worked for JIRA - 10062
                XElement oTemplate = XElement.Parse(p_sXML);
                oElements = oTemplate.XPathSelectElement("//Entity");
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("DriverDupCheckFields", string.Empty);
                string[] arrValues = sPolicyFields.Split(',');
                sbSql.Append("SELECT ENTITY.ENTITY_ID FROM ENTITY WHERE 1=1");
                for (int i = 0; i <= arrValues.Length - 1; i++)
                {
                    if (oElements != null)
                    {
                        oElement = oElements.XPathSelectElement("//" + arrValues[i]);
                        if (oElement != null)
                        {
                            if (string.Compare(arrValues[i], "LastName", false) == 0)
                            {
                                sLastName = oElement.Value;
                                if (sLastName != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.LAST_NAME= ~LASTNAME~");
                                    strDictParams.Add("LASTNAME", sLastName);
                                }
                            }
                            else if (string.Compare(arrValues[i], "FirstName", false) == 0)
                            {
                                sFirstName = oElement.Value;
                                if (sFirstName != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.FIRST_NAME= ~FIRSTNAME~");
                                    strDictParams.Add("FIRSTNAME", sFirstName);
                                }
                            }
                            else if (string.Compare(arrValues[i], "TaxId", false) == 0)
                            {
                                sTaxId = oElement.Value;
                                if (sTaxId != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.TAX_ID= ~TAXID~");
                                    strDictParams.Add("TAXID", sTaxId);
                                }
                            }
                            else if (string.Compare(arrValues[i], "Addr", false) == 0)
                            {
                                oElement = oElements.XPathSelectElement("//Addr1");
                                if (oElement != null)
                                {
                                    sAdd = oElement.Value;
                                    oElement = null;
                                }
                                oElement = oElements.XPathSelectElement("//Addr2");
                                if (oElement != null)
                                {
                                    sAdd = sAdd + oElement.Value;
                                    oElement = null;
                                }
                                if (sAdd != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.ADDR1 + ENTITY.ADDR2= ~ADDR~");
                                    strDictParams.Add("ADDR", sAdd);
                                }
                            }
                            else if (string.Compare(arrValues[i], "EmailAddress", false) == 0)
                            {
                                sEmail = oElement.Value;
                                if (sEmail != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.EMAIL_ADDRESS= ~EMAIL~");
                                    strDictParams.Add("EMAIL", sEmail);
                                }
                            }
                            else if (string.Compare(arrValues[i], "Contact", false) == 0)
                            {
                                sContact = oElement.Value;
                                if (sContact != string.Empty)
                                {
                                    sbSql.Append(" AND ENTITY.CONTACT= ~CONTACT~");
                                    strDictParams.Add("CONTACT", sContact);
                                }
                            }
                            else if (string.Compare(arrValues[i], "ZipCode", false) == 0)
                            {
                                iZip = Conversion.CastToType<int>(oElement.Value, out bSuccess);
                                if (iZip != 0)
                                {
                                    sbSql.Append(" AND ENTITY.ZIP_CODE= ~ZIPCODE~");
                                    strDictParams.Add("ZIPCODE", iZip.ToString());
                                }
                            }
                        }
                    }
                }
                //Payal Start : Worked for JIRA - 10062
               if (!objSettings.UseEntityRole)
                   sbSql.Append(" AND ENTITY.ENTITY_TABLE_ID =" + iTableId);
                //Payal Start : Worked for JIRA - 10062
                sbSql.Append(" AND ENTITY.DELETED_FLAG=0 ORDER BY ENTITY.ENTITY_ID DESC"); //Payal Added (ENTITY.DELETED_FLAG=0) : Worked for JIRA - 10471
                if (sbSql != null)
                {
                    iEntityID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams), m_iClientId);
                }
                return iEntityID;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (oElements != null)
                    oElements = null;
                if (oElement != null)
                    oElement = null;
            }
        }
        public int CheckCoverageDuplication(int p_iPolicyUnitRowId, string sCvgKey, int iPolicySystemID)
        {
            int iPolicyCovRowId = 0;
            StringBuilder sbSql = new StringBuilder();
            XElement xElement = null;
            DbConnection objConn = DbFactory.GetDbConnection(m_sConnectString);
            string sPolicySystemID = iPolicySystemID.ToString();
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            sbSql.AppendFormat("SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE COVERAGE_KEY= '{0}' AND POLICY_UNIT_ROW_ID = '{1}'", sCvgKey, p_iPolicyUnitRowId); // aaggarwal29: Code mapping change            
            if (sbSql != null)
            {
                objConn.Open();
                iPolicyCovRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, sbSql.ToString(), strDictParams), m_iClientId);
            }
            return iPolicyCovRowId;
        }
      
        
        public void EntityAddressDuplication(ref Entity objEntity, int iAddressType, XmlNode oCurrentAddressListNode)
        {
            DataModelFactory objDmf = null;
            bool bSuccess = false;
            bool bAddressExists = false;
            XElement oAddressListElement = XElement.Parse(oCurrentAddressListNode.InnerXml);
            objDmf = new DataModelFactory(m_sDSNName, m_sUserName, m_sPassword, m_iClientId);
            if (objEntity != null && objEntity.EntityId > 0)
            {
                foreach (EntityXAddresses oEntityXAddresses in objEntity.EntityXAddressesList)
                {
                    oEntityXAddresses.PrimaryAddress = 0;
                    if (oEntityXAddresses.AddressType == iAddressType)
                    {
                        oEntityXAddresses.PrimaryAddress = -1;
						//RMA-8753 nshah28(Added by ashish) START
                        //oEntityXAddresses.Addr1 = oAddressListElement.XPathSelectElement("//Addr1").Value.ToString();
                        //oEntityXAddresses.Addr2 = oAddressListElement.XPathSelectElement("//Addr2").Value;
                        //oEntityXAddresses.City = oAddressListElement.XPathSelectElement("//City").Value;
                        //oEntityXAddresses.Country = Conversion.CastToType<int>(oAddressListElement.XPathSelectElement("//Country").Attribute("codeid").Value, out bSuccess);
                        //oEntityXAddresses.State = Conversion.CastToType<int>(oAddressListElement.XPathSelectElement("//State").Value, out bSuccess);
                        //oEntityXAddresses.County = oAddressListElement.XPathSelectElement("//County").Value;
                        //oEntityXAddresses.ZipCode = oAddressListElement.XPathSelectElement("//ZipCode").Value;
						//RMA-8753 nshah28(Added by ashish) END
                        oEntityXAddresses.AddressType = Conversion.CastToType<int>(oAddressListElement.XPathSelectElement("//AddressType").Value, out bSuccess);
                        bAddressExists = true;
                        break;
                    }
                }
            }
            if (!bAddressExists)
            {
                EntityXAddresses oEntityXAddresses = (EntityXAddresses)objDmf.GetDataModelObject("EntityXAddresses", false);
                oEntityXAddresses.PrimaryAddress = -1;
				//RMA-8753 nshah28(Added by ashish) START
                //oEntityXAddresses.Addr1 = oAddressListElement.XPathSelectElement("//Addr1").Value.ToString();
                //oEntityXAddresses.Addr2 = oAddressListElement.XPathSelectElement("//Addr2").Value;
                //oEntityXAddresses.City = oAddressListElement.XPathSelectElement("//City").Value;
                //oEntityXAddresses.Country = Conversion.CastToType<int>(oAddressListElement.XPathSelectElement("//Country").Attribute("codeid").Value, out bSuccess);
                //oEntityXAddresses.State = Conversion.CastToType<int>(oAddressListElement.XPathSelectElement("//State").Value, out bSuccess);
                //oEntityXAddresses.County = oAddressListElement.XPathSelectElement("//County").Value;
                //oEntityXAddresses.ZipCode = oAddressListElement.XPathSelectElement("//ZipCode").Value;
				//RMA-8753 nshah28(Added by ashish) END
                oEntityXAddresses.AddressType = Conversion.CastToType<int>(oAddressListElement.XPathSelectElement("//AddressType").Value, out bSuccess);
                objEntity.EntityXAddressesList.Add(oEntityXAddresses);
            }
            objDmf = null;
            oAddressListElement = null;
        }
        #endregion
        public string GetPolicySystemName(int iPolicySystemId)
        {
            string sSQL = string.Empty;
            string sPolicySystemName = string.Empty;
            try
            {
                if (iPolicySystemId != 0)
                {
                    sSQL = "select SHORT_CODE from CODES where TABLE_ID  in (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'policy_system_type') and CODE_ID in (select POLICY_SYSTEM_CODE from POLICY_X_WEB where POLICY_SYSTEM_ID = '" + iPolicySystemId + "')";
                    using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        if (objDbReader.Read())
                        {
                            sPolicySystemName = objDbReader["SHORT_CODE"].ToString();
                        }
                    }
                }
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("IntegralSystemInterface.GetPolicySystemName.NoRecordsFound", m_iClientId, m_sLangCode), p_objExp);
            }
            return sPolicySystemName;
        }
        public bool SaveEndorsementData(ExternalPolicyData oExternalPolicyData, int iPolicyId, string sTableName, int iTableRowid)
        {
            LocalCache objCache = null;
            StringBuilder sbSQL;
            DbConnection objCon = null;
            DbCommand objCmd = null;
            int iTableId = 0;
            bool bReturn = false;
            DbWriter writer = null;
            string sInsertedFormNo = string.Empty;
            try
            {
                objCache = new LocalCache(m_sConnectString,m_iClientId);
                objCon = DbFactory.GetDbConnection(m_sConnectString);
                sbSQL = new StringBuilder();
                iTableId = objCache.GetTableId(sTableName);
                if ((oExternalPolicyData.PolicyData.Endorsements != null) && (oExternalPolicyData.PolicyData.Endorsements.Count() > 0))
                {
                    objCon.Open();
                    sbSQL.Append("DELETE FROM  PS_ENDORSEMENT WHERE POLICY_ID=" + iPolicyId + "AND TABLE_ID=" + iTableId.ToString() + "AND ROW_ID=" + iTableRowid.ToString());
                    objCmd = objCon.CreateCommand();
                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();
                    objCmd = null;
                    foreach (EndorsementData oEndorsementData in oExternalPolicyData.PolicyData.Endorsements)
                    {
                        writer = DbFactory.GetDbWriter(objCon);
                        writer.Tables.Add("PS_ENDORSEMENT");
                        if (DbFactory.GetDatabaseType(m_sConnectString) == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            writer.Fields.Add("ENDORSEMENT_ID", DbFactory.ExecuteScalar(objCon.ConnectionString, "SELECT SEQ_ENDORSEMENT_ID.NEXTVAL FROM DUAL"));
                        }
                        writer.Fields.Add("POLICY_ID", iPolicyId);
                        writer.Fields.Add("TABLE_ID", iTableId);
                        writer.Fields.Add("ROW_ID", iTableRowid);
                        if (!string.IsNullOrEmpty(oEndorsementData.EndorsementReasonCode))
                        {
                            writer.Fields.Add("FORM_NUMBER", oEndorsementData.EndorsementReasonCode);
                        }
                        if (!string.IsNullOrEmpty(oEndorsementData.EndorsementReasonDesc))
                        {
                            writer.Fields.Add("FORM_DESCRIPTION", oEndorsementData.EndorsementReasonDesc);
                        }
                        if (!string.IsNullOrEmpty(oEndorsementData.EffectiveDate))
                        {
                            writer.Fields.Add("EDITIONDATE", oEndorsementData.EffectiveDate);
                        }
                        writer.Fields.Add("DTTM_RCD_ADDED", Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                        writer.Fields.Add("ADDED_BY_USER", m_sUserName.ToString());
                        writer.Execute();
                    }
                }
                bReturn = true;
                return bReturn;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("PolicySystemInterface.SaveEndorsementData.ExceptioninSaveXMLData", m_iClientId, m_sLangCode), p_objExp);
            }
            finally
            {
                writer = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon.Dispose();
                }
            }
        }
        // this function returns mapped policy types to policy LOB passed as argument
        //BR - MITS 35932
        public DataSet GetPolicyTypesFromLOB(int iLOBCodeID)
        {
            string sSQL = string.Empty;
            string sPolicySystemName = string.Empty;
            DataSet dsPolicyTypes = null;
            try
            {
                if (iLOBCodeID != 0)
                {
                    sSQL = "select SHORT_CODE from CODES_TEXT where CODE_ID in ( select CODE2 from CODE_X_CODE where CODE1 = '" + iLOBCodeID + "' and DELETED_FLAG=0 and REL_TYPE_CODE = (select CODE_ID from CODES where SHORT_CODE = 'POLTOPT' AND TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='CODE_REL_TYPE')))";
                    dsPolicyTypes = DbFactory.GetDataSet(m_sConnectString, sSQL, m_iClientId);
                }
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("IntegralSystemInterface.GetPolicyTypesFromLOB.NoRecordsFound", m_iClientId, m_sLangCode), p_objExp);
            }
            return dsPolicyTypes;
        }
        //BR - MITS 35932 - end
        
    }

}


