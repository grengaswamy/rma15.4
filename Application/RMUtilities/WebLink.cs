﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Text;
using System.Collections;
using Riskmaster.DataModel;
//Ashish Ahuja - Mits 34006 - start
using System.Collections.Generic;
using System.Linq;
//Ashish Ahuja - Mits 34006 - end
namespace Riskmaster.Application.RMUtilities
{
    public class WebLink
    {
        /// <summary>
        /// Dsn ID
        /// </summary>
        int m_iDSNId = 0;



        /// <summary>
        /// Connection String
        /// </summary>
        string m_sConnStr = "";

        /// <summary>
        /// Security Dsn
        /// </summary>
        string m_sSecDsn = "";

        /// <summary>
        /// User Login Id
        /// </summary>
        int m_iUserId = 0;


        /// <summary>
        /// UserName
        /// </summary>
        string m_sUserName = null;
        /// <summary>
        /// DSN Name
        /// </summary>
        string m_sDsnName = null;
        /// <summary>
        /// User Login Password
        /// </summary>
        string m_sPassword = null;
        /// <summary>
        /// User's First name
        /// </summary>
        string m_sUserFirstName = null;
        /// <summary>
        /// users Last name
        /// </summary>
        string m_sUserLastName = null;
        /// <summary>
        /// Document Storage Path
        /// </summary>
        string m_sDocumentPath = null;
        /// <summary>
        /// Group Id
        /// </summary>
        string m_sGroupId = null;
        string m_sPolicyid = "0";
        int m_iClientId = 0; //rkaur27

        public WebLink(string p_sUserName, string p_sPassword, string p_sDatabaseName, int p_iClientId) //rkaur27
        {
            // UserLogin objUserLogin = new UserLogin(p_sUserName, p_sPassword, p_sDatabaseName);
            UserLogin objUserLogin = new UserLogin(p_sUserName, p_sPassword, p_sDatabaseName, p_iClientId);//rkaur27
            m_iDSNId = objUserLogin.objRiskmasterDatabase.DataSourceId;
            m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sSecDsn = Security.SecurityDatabase.GetSecurityDsn(p_iClientId);//rkaur27
            m_iUserId = objUserLogin.UserId;
            m_sUserName = p_sUserName;
            m_sUserFirstName = objUserLogin.objUser.FirstName;
            m_sUserLastName = objUserLogin.objUser.LastName;
            m_sDsnName = objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sDocumentPath = objUserLogin.DocumentPath;
            m_sPassword = p_sPassword;
            m_sGroupId = objUserLogin.GroupId.ToString();
            m_iClientId = p_iClientId; //rkaur27
            objUserLogin = null;
        }

        public XmlDocument Get(XmlDocument p_objXmlDoc)
        {
            Hashtable objHashUsers = null;
            DbReader objReader = null;
            try
            {

                objHashUsers = new Hashtable();

                p_objXmlDoc = GetUrlList(p_objXmlDoc);

                p_objXmlDoc = GetAvailUsers(p_objXmlDoc);
                return p_objXmlDoc;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WebLink.Get.Error", m_iClientId), p_objEx);//rkaur27
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }

        }
        public XmlDocument DeleteUrl(XmlDocument p_objXmlDoc)
        {
            string sSql = null;
            DbConnection objConn = null;
            int irowId = 0;
            XmlElement objSelectElement = null;
            XmlElement objRowTxt = null;
            try
            {
                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId") != null && p_objXmlDoc.SelectSingleNode("form/group/WebLink/rowId").InnerText != "0")
                {
                    irowId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText);

                }

                sSql = "DELETE FROM SYS_SETTINGS WHERE ROW_ID = " + irowId;

                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();
                objConn.ExecuteNonQuery(sSql);

                sSql = "DELETE FROM WEB_LINKS_PERMISSION WHERE ROW_ID = " + irowId;
                objConn.ExecuteNonQuery(sSql);
                p_objXmlDoc = GetUrlList(p_objXmlDoc);
                p_objXmlDoc = GetAvailUsers(p_objXmlDoc);
                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText = "0";

                }


                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType").InnerText = "";
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsers") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsers").InnerText = "";
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLShortName") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLShortName").InnerText = "";
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLString") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLString").InnerText = "";
                }
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink").InnerText = "False";
                }
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                return p_objXmlDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WebLink.Delete.Error", m_iClientId), p_objEx);//rkaur27
            }
            finally
            {
                if (objConn != null)
                    objConn.Dispose();
            }
        }
        private XmlDocument GetAvailUsers(XmlDocument p_objXmlDoc)
        {
            string sSql = null;
            string sSelectedURL = null;
            XmlElement objelem = null;
            XmlElement objSelectElement = null;
            XmlElement objRowTxt = null;
            DbReader objReader = null;
            Hashtable objHashUsers = null;


            try
            {
                objSelectElement = (XmlElement)p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/AvialUser");
                sSql = "SELECT * FROM USER_GROUPS ORDER BY GROUP_NAME";
                objReader = DbFactory.GetDbReader(m_sConnStr, sSql);

                if (objReader != null)
                {
                    objHashUsers = new Hashtable();
                    while (objReader.Read())
                    {
                        objRowTxt = p_objXmlDoc.CreateElement("AvialUseroption");
                        objRowTxt.SetAttribute("value", "g" + objReader["GROUP_ID"].ToString());
                        objRowTxt.InnerText = "*" + objReader["GROUP_NAME"].ToString();
                        objSelectElement.AppendChild((XmlNode)objRowTxt);
                        objHashUsers.Add("g" + objReader["GROUP_ID"].ToString(), objReader["GROUP_NAME"].ToString());
                    }
                }

                sSql = "SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE  WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID=" + m_iDSNId + " ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";

                objReader = DbFactory.GetDbReader(m_sSecDsn, sSql.ToString());

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objRowTxt = p_objXmlDoc.CreateElement("AvialUseroption");
                        objRowTxt.SetAttribute("value", "u" + objReader["USER_ID"].ToString());
                        objRowTxt.InnerText = ((string)(objReader["FIRST_NAME"].ToString()
                            + " "
                            + objReader["LAST_NAME"].ToString())).Trim()
                            + " ("
                            + objReader["LOGIN_NAME"].ToString()
                            + ")";
                        objSelectElement.AppendChild((XmlNode)objRowTxt);
                        objHashUsers.Add("u" + objReader["USER_ID"].ToString(), objRowTxt.InnerText);
                    }


                }
                return p_objXmlDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }

            }

        }
        private StringBuilder HandlePolicyParams(StringBuilder p_sUrl)
        {
            DataModelFactory objDmf = null;
            try
            {
                if (p_sUrl.ToString().IndexOf("<POL") != -1)
                {
                    if (m_sPolicyid != "0")
                    {
                        objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        Policy objPolicy = (Policy)objDmf.GetDataModelObject("Policy", false);
                        objPolicy.MoveTo(Conversion.ConvertStrToInteger(m_sPolicyid));
                        p_sUrl = p_sUrl.Replace("<POLICY>", objPolicy.PolicyNumber);
                        p_sUrl = p_sUrl.Replace("<POLICYID>", objPolicy.PolicyId.ToString());
                        p_sUrl = p_sUrl.Replace("<POLMODULE>", objPolicy.Module.ToString());
                        p_sUrl = p_sUrl.Replace("<POLSYMBOL>", objPolicy.PolicySymbol.ToString());
                        p_sUrl = p_sUrl.Replace("<POLAGENTNAME>", objPolicy.BrokerEntity.FirstName + objPolicy.BrokerEntity.LastName);
                        objPolicy.Dispose();
                    }
                }
            }
            finally
            {
                if (objDmf != null)
                    objDmf.Dispose();
            }
            return p_sUrl;
        }
        private StringBuilder GetParameters(StringBuilder p_sUrl, DataObject p_objData, string p_sType)
        {

            DataModelFactory objDmf = null;
            string sSql = null;
            Event objEvent = null;
            try
            {

                switch (p_sType)
                {
                    case "claim":

                        p_sUrl = p_sUrl.Replace("<CLAIM>", (p_objData as Claim).ClaimNumber);
                        p_sUrl = p_sUrl.Replace("<CLAIMID>", (p_objData as Claim).ClaimId.ToString());

                        objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objEvent = (Event)objDmf.GetDataModelObject("Event", false);
                        objEvent.MoveTo((p_objData as Claim).EventId);
                        p_sUrl = p_sUrl.Replace("<EVENT>", objEvent.EventNumber);
                        p_sUrl = p_sUrl.Replace("<EVENTID>", (p_objData as Claim).EventId.ToString());
                        p_sUrl = HandlePolicyParams(p_sUrl);
                        break;
                    case "policy":

                        p_sUrl = p_sUrl.Replace("<POLICY>", (p_objData as Policy).PolicyNumber);
                        p_sUrl = p_sUrl.Replace("<POLICYID>", (p_objData as Policy).PolicyId.ToString());
                        p_sUrl = p_sUrl.Replace("<POLMODULE>", (p_objData as Policy).Module.ToString());
                        p_sUrl = p_sUrl.Replace("<POLSYMBOL>", (p_objData as Policy).PolicySymbol.ToString());
                        p_sUrl = p_sUrl.Replace("<POLAGENTNAME>", (p_objData as Policy).BrokerEntity.FirstName + (p_objData as Policy).BrokerEntity.LastName);
                        break;
                    case "event":

                        p_sUrl = p_sUrl.Replace("<EVENT>", (p_objData as Event).EventNumber);
                        p_sUrl = p_sUrl.Replace("<EVENTID>", (p_objData as Event).EventId.ToString());
                        break;
                    case "policyenh":

                        p_sUrl = p_sUrl.Replace("<POLICY>", (p_objData as PolicyEnh).PolicyName);
                        p_sUrl = p_sUrl.Replace("<POLICYID>", (p_objData as PolicyEnh).PolicyId.ToString());
                        //  p_sUrl = p_sUrl.Replace("<POLMCO>", (p_objData as PolicyEnh).PolicyXMcoEnhList.ToString());

                        p_sUrl = p_sUrl.Replace("<POLAGENTNAME>", (p_objData as PolicyEnh).BrokerEid.ToString());
                        p_sUrl = p_sUrl.Replace("<POLAGENTCODE>", (p_objData as PolicyEnh).BrokerEntity.FirstName + (p_objData as PolicyEnh).BrokerEntity.LastName);
                        break;
                    case "claimant":

                        p_sUrl = p_sUrl.Replace("<CLAIMANTID.N>", (p_objData as Claimant).ClaimantRowId.ToString());
                        p_sUrl = p_sUrl.Replace("<CLAIMANT.N>", (p_objData as Claimant).ClaimantEntity.FirstName.Trim() + (p_objData as Claimant).ClaimantEntity.LastName.Trim());

                        break;

                }
                return p_sUrl;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objDmf != null)
                    objDmf.Dispose();
            }
        }

        private StringBuilder ReplaceParameter(StringBuilder p_sURL)
        {
            p_sURL = p_sURL.Replace("<DATABASE>", m_sDsnName);
            p_sURL = p_sURL.Replace("<PASSWORD>", m_sPassword);
            p_sURL = p_sURL.Replace("<LOGINNAME>", m_sUserName);
            p_sURL = p_sURL.Replace("<TIME>", System.DateTime.Now.TimeOfDay.ToString());
            p_sURL = p_sURL.Replace("<DATE>", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

            p_sURL = p_sURL.Replace("<DATABASEID>", m_iDSNId.ToString());

            p_sURL = p_sURL.Replace("<USERNAME>", m_sUserFirstName + m_sUserLastName);
            p_sURL = p_sURL.Replace("<UFN>", m_sUserFirstName);
            p_sURL = p_sURL.Replace("<ULN>", m_sUserLastName);
            p_sURL = p_sURL.Replace("<DOCPATH>", m_sDocumentPath);
            p_sURL = p_sURL.Replace("<UID>", m_iUserId.ToString());
            p_sURL = p_sURL.Replace("<GID>", m_sGroupId);


            p_sURL = p_sURL.Replace("<CLAIM>", "");
            p_sURL = p_sURL.Replace("<CLAIMID>", "");

            p_sURL = p_sURL.Replace("<EVENT>", "");
            p_sURL = p_sURL.Replace("<EVENTID>", "");
            p_sURL = p_sURL.Replace("<POLICYID>", "");
            p_sURL = p_sURL.Replace("<POLICY>", "");
            p_sURL = p_sURL.Replace("<POLAGENTNAME>", "");

            p_sURL = p_sURL.Replace("<POLMODULE>", "");
            p_sURL = p_sURL.Replace("<POLSYMBOL>", "");
            p_sURL = p_sURL.Replace("<EVENT>", "");
            p_sURL = p_sURL.Replace("<EVENTID>", "");
            p_sURL = p_sURL.Replace("<POLICY>", "");
            p_sURL = p_sURL.Replace("<POLICYID>", "");
            p_sURL = p_sURL.Replace("<POLMCO>", "");

            p_sURL = p_sURL.Replace("<POLAGENTNAME>", "");
            p_sURL = p_sURL.Replace("<POLAGENTCODE>", "");
            p_sURL = p_sURL.Replace("<CLAIMANTID.N>", "");
            p_sURL = p_sURL.Replace("<CLAIMANT.N>", "");
            return p_sURL;
        }
        public XmlDocument GetUrlList(XmlDocument p_objXmlDoc)
        {

            string sSql = null;
            XmlElement objSelectElement = null;
            XmlElement objRowTxt = null;
            DbReader objReader = null;


            try
            {

                sSql = " SELECT SYS_SETTINGS.ITEM,SYS_SETTINGS.ROW_ID  FROM SYS_SETTINGS ";
                // sSql = sSql + " WHERE ";
                sSql = sSql + " ORDER BY SYS_SETTINGS.DTTM_RCD_ADDED DESC";

                objSelectElement = (XmlElement)p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UrlList");
                objReader = DbFactory.GetDbReader(m_sConnStr, sSql);

                while (objReader.Read())
                {

                    objRowTxt = p_objXmlDoc.CreateElement("AvailURLs");
                    objRowTxt.SetAttribute("value", objReader["ROW_ID"].ToString());
                    objRowTxt.InnerText = objReader["ITEM"].ToString();
                    objSelectElement.AppendChild((XmlNode)objRowTxt);
                }
                return p_objXmlDoc;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }


        }

        public XmlDocument LoadUrl(XmlDocument p_objXmlDoc)
        {
            string sSql = null;
            StringBuilder sUrl = null;
            string sFormName = null;
            Claim objClaim = null;
            Event objEvent = null;
            Policy objPolicy = null;
            PolicyEnh objenhPol = null;
            DataModelFactory objDmf = null;
            Claimant objClaimant = null;

            int iRecordId = 0;
            try
            {
                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId") != null)
                {
                    sSql = "SELECT VALUE FROM SYS_SETTINGS WHERE ROW_ID = " + p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText;

                }
                sUrl = new StringBuilder();
                sUrl.Append("");
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnStr, sSql))
                {
                    if (objReader.Read())
                    {
                        sUrl.Append(objReader.GetString("VALUE"));
                    }
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/FormName") != null)
                {
                    sFormName = p_objXmlDoc.SelectSingleNode("/form/group/WebLink/FormName").InnerText;

                }
                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/Policy") != null)
                {
                    if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/Policy").InnerText != "")
                        m_sPolicyid = p_objXmlDoc.SelectSingleNode("/form/group/WebLink/Policy").InnerText;

                }
                if (sUrl.ToString().Contains("<") && sUrl.ToString().Contains(">"))
                {
                    if (sFormName.Contains("claimant"))
                    {
                        iRecordId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("/form/group/WebLink/RecordId").InnerText);
                        objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objClaimant = (Claimant)objDmf.GetDataModelObject("Claimant", false);
                        objClaimant.MoveTo(iRecordId);
                        sUrl = GetParameters(sUrl, objClaimant, "claimant");

                    }
                    else if (sFormName.Contains("claim"))
                    {
                        iRecordId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("/form/group/WebLink/RecordId").InnerText);
                        objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objClaim = (Claim)objDmf.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iRecordId);
                        sUrl = GetParameters(sUrl, objClaim, "claim");

                    }
                    else if (sFormName.Contains("policyenh"))
                    {
                        objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objenhPol = (PolicyEnh)objDmf.GetDataModelObject("PolicyEnh", false);
                        iRecordId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("/form/group/WebLink/RecordId").InnerText);
                        objenhPol.MoveTo(iRecordId);
                        sUrl = GetParameters(sUrl, objenhPol, "policyenh");
                    }
                    else if (sFormName.Contains("policy"))
                    {
                        objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objPolicy = (Policy)objDmf.GetDataModelObject("Policy", false);
                        iRecordId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("/form/group/WebLink/RecordId").InnerText);
                        objPolicy.MoveTo(iRecordId);
                        sUrl = GetParameters(sUrl, objPolicy, "policy");
                    }

                    else if (sFormName.Contains("event"))
                    {
                        objDmf = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objEvent = (Event)objDmf.GetDataModelObject("Event", false);

                        iRecordId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("/form/group/WebLink/RecordId").InnerText);
                        objEvent.MoveTo(iRecordId);
                        sUrl = GetParameters(sUrl, objEvent, "event");
                    }




                    sUrl = ReplaceParameter(sUrl);

                }

                if (sUrl.ToString().Contains("@"))
                {
                    sUrl = ReplaceFunctions(sUrl);
                }
                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URL") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URL").InnerText = sUrl.ToString();

                }
                return p_objXmlDoc;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WebLink.GetUrl.Error", m_iClientId), p_objEx);//rkaur27
            }
            finally
            {
                if (objClaim != null)
                    objClaim.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
                if (objPolicy != null)
                    objPolicy.Dispose();
                if (objenhPol != null)
                    objenhPol.Dispose();
                if (objDmf != null)
                    objDmf.Dispose();
                if (objClaimant != null)
                    objClaimant.Dispose();

            }


        }
        private StringBuilder ReplaceFunctions(StringBuilder p_sUrl)
        {
            int iStartindex = 0;
            int iEndIndex = 0;
            string sParam = null;
            int iTemp = 0;
            string[] sArg = null;



            p_sUrl = p_sUrl.Replace("@DAY()@", System.DateTime.Now.ToShortDateString());
            p_sUrl = p_sUrl.Replace("@MON()@", System.DateTime.Now.Month.ToString());
            p_sUrl = p_sUrl.Replace("@YEAR()@", System.DateTime.Now.Year.ToString());
            p_sUrl = p_sUrl.Replace("@MIN()@", System.DateTime.Now.Minute.ToString());
            p_sUrl = p_sUrl.Replace("@SEC()@", System.DateTime.Now.Second.ToString());

            p_sUrl = p_sUrl.Replace("@HOUR()@", System.DateTime.Now.Hour.ToString());

            p_sUrl = p_sUrl.Replace("@DSPLYDATE()@", System.DateTime.Now.Date.ToString());
            p_sUrl = p_sUrl.Replace("@DSPLYTIME()@", System.DateTime.Now.TimeOfDay.ToString());


            while (p_sUrl.ToString().Contains("@DOW("))
            {
                int iDay = 0;
                string sTemp = null;
                iStartindex = p_sUrl.ToString().IndexOf("@DOW(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');

                if (string.IsNullOrEmpty(sArg[0]))
                {
                    p_sUrl = p_sUrl.Replace("@DOW()@", System.DateTime.Now.ToString("yyyyMMdd"));
                }
                else
                {
                    sTemp = sArg[1].Substring(0, 2);

                    switch (sTemp.ToLower())
                    {
                        case "tu":
                            iDay = Conversion.ConvertObjToInt(System.DayOfWeek.Tuesday, m_iClientId) - Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);
                            break;
                        case "we":
                            iDay = Conversion.ConvertObjToInt(System.DayOfWeek.Wednesday, m_iClientId) - Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);
                            break;
                        case "th":
                            iDay = Conversion.ConvertObjToInt(System.DayOfWeek.Thursday, m_iClientId) - Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);
                            break;
                        case "fr":
                            iDay = Conversion.ConvertObjToInt(System.DayOfWeek.Friday, m_iClientId) - Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);
                            break;
                        case "sa":
                            iDay = Conversion.ConvertObjToInt(System.DayOfWeek.Saturday, m_iClientId) - Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);
                            break;
                        case "su":
                            iDay = Conversion.ConvertObjToInt(System.DayOfWeek.Sunday, m_iClientId) - Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);
                            break;
                        case "mo":
                            iDay = Conversion.ConvertObjToInt(System.DayOfWeek.Monday, m_iClientId) - Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);
                            break;
                    }


                    p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), (Conversion.ToDate(sArg[0]).AddDays(iDay).ToString("yyyyMMdd")));


                }
            }
            while (p_sUrl.ToString().Contains("@LCASE("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@LCASE(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");


                p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1).ToLower());
            }

            while (p_sUrl.ToString().Contains("@UCASE("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@UCASE(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");


                p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1).ToUpper());
            }

            while (p_sUrl.ToString().Contains("@MID("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@MID(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');
                string sTemp = sArg[0].Substring(Conversion.ConvertStrToInteger(sArg[1]) - 1, Conversion.ConvertStrToInteger(sArg[2]));


                p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sTemp);
            }

            while (p_sUrl.ToString().Contains("@LEFT("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@LEFT(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');
                string sTemp = sArg[0].Substring(0, Conversion.ConvertStrToInteger(sArg[1]));
                p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sTemp);
            }

            while (p_sUrl.ToString().Contains("@RIGHT("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@LEFT(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');
                string sTemp = sArg[0].Substring(sArg[0].Length - Conversion.ConvertStrToInteger(sArg[1]) - 1);
                p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sTemp);
            }

            while (p_sUrl.ToString().Contains("@IIFB("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@IIFB(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                bool bRslt = false;

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');
                if ((!sArg[0].Equals("")) && ((!sArg[1].Equals("") || (!sArg[2].Equals("")))))
                {
                    char[] arr = { '=', '<', '>' };
                    string[] sArgs = sArg[0].Split(arr);
                    iStartindex = sArg[0].IndexOfAny(arr);
                    switch (sArg[0].Substring(iStartindex, 1))
                    {
                        case "=":
                            bRslt = (sArgs[0] == sArgs[1]);
                            break;
                        case "<":
                            bRslt = (Conversion.ConvertStrToInteger(sArgs[0]) < Conversion.ConvertStrToInteger(sArgs[1]));
                            break;
                        case ">":
                            bRslt = (Conversion.ConvertStrToInteger(sArgs[0]) > Conversion.ConvertStrToInteger(sArgs[1]));
                            break;
                        case "":
                            bRslt = false;
                            break;
                    }
                    if (bRslt)
                    {
                        p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sArg[1]);
                    }
                    else
                    {
                        p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sArg[2]);
                    }

                }
            }

            while (p_sUrl.ToString().Contains("@IIFN("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@IIFN(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');
                if ((!sArg[0].Equals("")) && ((!sArg[1].Equals("") || (!sArg[2].Equals("")) || (!sArg[3].Equals("")))))
                {
                    int number = Conversion.ConvertStrToInteger(sArg[0]);
                    if (number > 0)
                    {
                        p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sArg[1]);

                    }
                    if (number < 0)
                    {
                        p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sArg[2]);
                    }
                    if (number == 0)
                    {
                        p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sArg[3]);
                    }

                }
            }

            while (p_sUrl.ToString().Contains("@IIFS("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@IIFS(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');

                if (!string.IsNullOrEmpty(sArg[0]))
                {
                    p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sArg[1]);
                }

                else
                {
                    p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sArg[2]);
                }


            }

            while (p_sUrl.ToString().Contains("@FORMAT("))
            {
                iStartindex = p_sUrl.ToString().IndexOf("@FORMAT(");
                sParam = p_sUrl.ToString().Substring(iStartindex);
                iTemp = sParam.IndexOf("(");
                iEndIndex = sParam.IndexOf(")@");

                sArg = (sParam.Substring(iTemp + 1, iEndIndex - iTemp - 1)).Split(',');
                string sTemp = null;


                if (sArg[1].Contains("hh:mm:ss"))
                {
                    if (!string.IsNullOrEmpty(sArg[0]))
                        sTemp = Conversion.GetDBTimeFormat(sArg[0], "hh:mm:ss");
                    else
                        sTemp = System.DateTime.Now.ToShortTimeString();
                }
                else if (sArg[1].Contains("mm/dd/yyyy"))
                {
                    if (!string.IsNullOrEmpty(sArg[0]))
                        sTemp = Conversion.GetDBDateFormat(sArg[0], "MM/dd/yyyy");
                    else
                        sTemp = System.DateTime.Now.ToShortDateString();
                }
                else if (sArg[1].Contains("####0.00"))
                {

                    sTemp = Conversion.GetCurrencyFormat(Conversion.ConvertStrToDouble(sArg[0])).Substring(1);

                }


                p_sUrl = p_sUrl.Replace(sParam.Substring(0, iEndIndex + 2), sTemp);
            }

            return p_sUrl;
        }

        public XmlDocument SaveUrl(XmlDocument p_objXmlDoc)
        {
            string sSql = null;
            StringBuilder sbSql = null;
            bool bIsNew = false;
            string sURLType = null;
            string sURLValue = null;
            string sURLDescription = null;
            DbConnection objConn = null;
            string[] arrUsers;
            string sSelectedUsers = null;
            DbWriter dbWriter = null;//aanandpraka2:Changes for Pen Testing
            string sUseAsSupplementalLink = null;//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            int iUseAsSupplementalLink = 0;//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            int irowId = 0;
            try
            {



                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId") != null && p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText != "0")
                {
                    irowId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText);

                }
                if (irowId == 0)
                {
                    irowId = Utilities.GetNextUID(m_sConnStr, "SYS_SETTINGS", m_iClientId);
                    bIsNew = true;

                }
                else
                {
                    bIsNew = false;
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType") != null)
                {
                    sURLType = p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType").InnerText;
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsersID") != null)
                {
                    sSelectedUsers = p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsersID").InnerText;
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLShortName") != null)
                {
                    sURLValue = p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLShortName").InnerText;
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLString") != null)
                {
                    sURLDescription = p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLString").InnerText;

                    sURLDescription = System.Web.HttpUtility.HtmlDecode(sURLDescription);

                }
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink") != null)
                {
                    sUseAsSupplementalLink = p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink").InnerText;
                    if (sUseAsSupplementalLink == "True")
                        iUseAsSupplementalLink = -1;
                    else
                        iUseAsSupplementalLink = 0;
                }
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                if (bIsNew)
                {
                    //aanandpraka2:Start changes for Pen Testing
                    dbWriter = DbFactory.GetDbWriter(m_sConnStr);
                    //sSql = "INSERT INTO SYS_SETTINGS VALUES (" + irowId + ",'URL','','" + sURLValue + "','" + sURLDescription + "',null,null,'" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "','" + m_sUserName + "',null,null)";

                    dbWriter.Tables.Add("SYS_SETTINGS");

                    //Enhancement required if table structure changes for SYS_SETTINGS table in future.
                    dbWriter.Fields.Add("ROW_ID", irowId);
                    dbWriter.Fields.Add("TYPE", "URL");
                    dbWriter.Fields.Add("COMPONENT", "");
                    dbWriter.Fields.Add("ITEM", sURLValue);
                    dbWriter.Fields.Add("VALUE", sURLDescription);
                    dbWriter.Fields.Add("DESCRIPTION", null);
                    dbWriter.Fields.Add("CATEGORY", null);
                    dbWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                    dbWriter.Fields.Add("ADDED_BY_USER", m_sUserName);
                    dbWriter.Fields.Add("UPDATED_BY_USER", null);
                    dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", null);
                    dbWriter.Fields.Add("USE_AS_SUPPLEMENTAL_LINK", iUseAsSupplementalLink);//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                }
                else
                {
                    dbWriter = DbFactory.GetDbWriter(m_sConnStr);

                    dbWriter.Tables.Add("SYS_SETTINGS");

                    dbWriter.Fields.Add("TYPE", "URL");
                    dbWriter.Fields.Add("ITEM", sURLValue);
                    dbWriter.Fields.Add("VALUE", sURLDescription);
                    dbWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now.ToString("yyyyMMddHHmmss")); //Changed by Amitosh for MITS 27604  
                    dbWriter.Fields.Add("UPDATED_BY_USER", m_sUserName);
                    dbWriter.Fields.Add("USE_AS_SUPPLEMENTAL_LINK", iUseAsSupplementalLink);//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                    dbWriter.Where.Add("ROW_ID = " + irowId);

                    //sSql = "UPDATE SYS_SETTINGS SET TYPE = 'URL', ITEM = '" + sURLValue + "' ,VALUE = '" + sURLDescription + "',UPDATED_BY_USER = '" + m_sUserName + "',DTTM_RCD_LAST_UPD = '" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "' WHERE ROW_ID = " + irowId;
                }
                dbWriter.Execute();
                //aanandpraka2:End changes

                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();
                //objConn.ExecuteNonQuery(sSql); aanandpraka2
                sSql = "DELETE FROM WEB_LINKS_PERMISSION WHERE ROW_ID = " + irowId;
                objConn.ExecuteNonQuery(sSql);
                arrUsers = sSelectedUsers.Split(' ');
                //Ashish Ahuja - Mits 34006 - start
                HashSet<string> hash = new HashSet<string>(arrUsers);
                arrUsers = hash.ToArray();
                //Ashish Ahuja - Mits 34006 - end
                sbSql = new StringBuilder();
                if (sURLType.ToUpper() == "PUBLIC")
                {
                    sbSql.Append("INSERT INTO WEB_LINKS_PERMISSION VALUES (" + irowId + ",0)");
                    objConn.ExecuteNonQuery(sbSql.ToString());
                }
                else
                {
                    if (arrUsers.Length > 0)
                    {
                        for (int i = 0; i < arrUsers.Length; i++)
                        {
                            if (arrUsers[i].ToString().Trim() != "")
                            {
                                sbSql.Length = 0;
                                sbSql.Append("INSERT INTO WEB_LINKS_PERMISSION VALUES (");

                                sbSql.Append(irowId + "," + arrUsers[i].ToString() + ")");

                                objConn.ExecuteNonQuery(sbSql.ToString());
                            }
                        }
                    }
                }
                p_objXmlDoc = GetUrlList(p_objXmlDoc);
                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText = "0";

                }


                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType").InnerText = "";
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsers") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsers").InnerText = "";
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLShortName") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLShortName").InnerText = "";
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLString") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLString").InnerText = "";
                }
                //WWIG GAP20A - agupta298 - MITS 36804
                if (p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink").InnerText = "False";
                }
                return p_objXmlDoc;

            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WebLink.SaveUrl.Error", m_iClientId), p_objEx);//rkaur27
            }
            finally
            {
                if (objConn != null)
                    objConn.Dispose();
            }


        }
        public XmlDocument GetURLData(XmlDocument p_objXmlDoc)
        {

            string sSql = null;
            XmlElement objSelectElement = null;
            XmlElement objRowTxt = null;
            DbReader objReader = null;
            LocalCache objCache = null;
            string sUserIds = string.Empty;
            string sUserName = string.Empty;
            try
            {

                //sSql = " SELECT ITEM,VALUE FROM SYS_SETTINGS";
                sSql = " SELECT ITEM,VALUE,USE_AS_SUPPLEMENTAL_LINK FROM SYS_SETTINGS";//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691

                if (p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId") != null)
                {
                    sSql = sSql + " WHERE SYS_SETTINGS.ROW_ID = " + p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText;
                }


                objReader = DbFactory.GetDbReader(m_sConnStr, sSql);

                if (objReader.Read())
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLString").InnerText = objReader.GetString("VALUE");

                    p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/URLShortName").InnerText = objReader.GetString("ITEM");

                    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                    if (objReader.GetInt16("USE_AS_SUPPLEMENTAL_LINK") == -1)
                        p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink").InnerText = "True";
                    else
                        p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UseAsSupplementalLink").InnerText = "False";
                    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691

                }


                objCache = new LocalCache(m_sConnStr, m_iClientId);
                objSelectElement = (XmlElement)p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UserList");
                sSql = "SELECT USER_ID FROM WEB_LINKS_PERMISSION WHERE ROW_ID = " + p_objXmlDoc.SelectSingleNode("/form/group/WebLink/rowId").InnerText;
                objReader = DbFactory.GetDbReader(m_sConnStr, sSql);
                while (objReader.Read())
                {
                    if (objReader["USER_ID"].ToString() == "0")
                    {
                        p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType").InnerText = "PUBLIC";
                        objRowTxt = p_objXmlDoc.CreateElement("UserListoption");
                        objRowTxt.SetAttribute("value", "0");

                        objRowTxt.InnerText = "ALL USERS";
                        objSelectElement.AppendChild((XmlNode)objRowTxt);
                    }
                    else
                    {
                        p_objXmlDoc.SelectSingleNode("/form/group/WebLink/URLType").InnerText = "PRIVATE";

                        //pkandhari, this if statement is necessary as it is binding the empty row also.                        
                        if (!string.IsNullOrWhiteSpace(objCache.GetSystemLoginName(objReader.GetInt("USER_ID"), m_iDSNId, m_sSecDsn, m_iClientId)))
                        {
                            objRowTxt = p_objXmlDoc.CreateElement("UserListoption");
                            objRowTxt.SetAttribute("value", objReader["USER_ID"].ToString());
                            objRowTxt.InnerText = objCache.GetSystemLoginName(objReader.GetInt("USER_ID"), m_iDSNId, m_sSecDsn, m_iClientId);

                            if (string.IsNullOrEmpty(sUserIds))
                                sUserIds = objReader["USER_ID"].ToString();
                            else
                                sUserIds = sUserIds + " " + objReader["USER_ID"].ToString();

                            if (string.IsNullOrEmpty(sUserName))
                                sUserName = objCache.GetSystemLoginName(objReader.GetInt("USER_ID"), m_iDSNId, m_sSecDsn, m_iClientId);
                            else
                                sUserName = sUserName + " " + objCache.GetSystemLoginName(objReader.GetInt("USER_ID"), m_iDSNId, m_sSecDsn, m_iClientId);

                            objSelectElement.AppendChild((XmlNode)objRowTxt);
                        }
                    }
                    //objSelectElement.AppendChild((XmlNode)objRowTxt);
                }


                p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsersID").InnerText = sUserIds;

                p_objXmlDoc.SelectSingleNode("/form/group/WebLink/SelectedUsers").InnerText = sUserName;
                // objSelectElement.AppendChild((XmlNode)objRowTxt);

                return p_objXmlDoc;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WebLink.GetData.Error", m_iClientId), p_objEx);//rkaur27
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }

        }
    }
}
