using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches diary information.
	/// </summary>
	public class WPAUtil
	{
		#region Xml Format
		/*<WPAUtil>
			<Owners>
			<User userid="2">parag sarin</User>
			<User userid="3">test test</User>
			</Owners>
			<Transfer>
			<FromUser />
			<ToUser />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Transfer>
			<Purge>
			<User />
			<DiaryStatus>
			<Open />
			<Close />
			</DiaryStatus>
			<Dates>
			<From />
			<To />
			</Dates>
			</Purge>
		</WPAUtil>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// User id
		/// </summary>
		private string m_sUserId="";
		/// <summary>
		/// DSN id
		/// </summary>
		private string m_sDsnId="";
		/// <summary>
		/// Security DB connection string
		/// </summary>
		private string m_sSecureDSN="";
        private int m_iClientId = 0; //psharma206	jira 101

		#endregion

		#region Properties
		/// <summary>
		/// DSN id
		/// </summary>
		public string DSNID
		{
			set
			{
				m_sDsnId=value;
			}	

		}
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		/// <summary>
		/// Security DSN
		/// </summary>
		public string SecureDSN
		{
			set
			{
				m_sSecureDSN=value;
			}	

		}
		/// <summary>
		/// User id
		/// </summary>
		public string UserId
		{
			set
			{
				m_sUserId=value;
			}	

		}
		#endregion
		
		#region Constructor
		/// <summary>
		/// Class Constructor
		/// </summary>
		/// <param name="p_sDSNID">DSN id</param>
		/// <param name="p_sUserId">User id</param>
		/// <param name="p_sSecureDSN">Security DSN</param>
		/// <param name="p_sDSN">Connection string</param>
        public WPAUtil(string p_sDSNID, string p_sUserId, string p_sSecureDSN, string p_sDSN, int p_iClientId)//psharma206	jira 101
		{
			m_sSecureDSN=p_sSecureDSN;
			m_sUserId=p_sUserId;
			m_sDsnId=p_sDSNID;
			m_sDSN=p_sDSN;
            m_iClientId = p_iClientId;//psharma206	jira 101
		}
		/// <summary>
		/// Default Constructor
		/// </summary>
        public WPAUtil(int p_iClientId)
        {
            m_iClientId = p_iClientId;
        }
		#endregion

		#region Private Functions
		private string GetLogingName(string p_sUserId)
		{
			StringBuilder sbSQL=null;
			string sReturn="";
			DbReader objReader=null;
			try
			{
				sbSQL=new StringBuilder();
				sbSQL.Append("SELECT LOGIN_NAME");
				sbSQL.Append(" FROM USER_DETAILS_TABLE");
				sbSQL.Append(" WHERE USER_DETAILS_TABLE.DSNID ="+m_sDsnId);
				sbSQL.Append(" AND USER_DETAILS_TABLE.USER_ID = "+p_sUserId);
				objReader = DbFactory.GetDbReader(m_sSecureDSN,sbSQL.ToString());
				if  (objReader.Read())
				{
					sReturn= Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
				}
				else
				{
					sReturn= "";
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAUtil.GetLogingName.Error", m_iClientId), p_objEx);//psharma206	jira 101
			}
			finally
			{
				sbSQL=null;
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				objReader=null;
			}
			return sReturn;
		}
		private string CreateTransferDiarySQL(XmlDocument p_objDOC)
		{
			string sSQL="";
			string sWhere="";
			string sFrom="";
			string sTo="";
			StringBuilder sbTemp=null;
			XmlDocument objDOM=null;
			XmlNode objNode=null;
			string sStatusOpen="";
			string sStatusClose="";
			try
			{
				objDOM=p_objDOC;
				objNode=objDOM.SelectSingleNode("//WPAUtil/Transfer/ToUser");
				sSQL = " UPDATE WPA_DIARY_ENTRY SET ASSIGNED_USER = '" + GetLogingName(objNode.InnerText) + "' ";
				objNode=objDOM.SelectSingleNode("//WPAUtil/Transfer/FromUser");
				sWhere = " WHERE ASSIGNED_USER= '" + GetLogingName(objNode.InnerText) + "' ";
				sbTemp=new StringBuilder();
				objNode = objDOM.SelectSingleNode("//WPAUtil/Transfer/Dates/From");
				sFrom=objNode.InnerText;
				objNode = objDOM.SelectSingleNode("//WPAUtil/Transfer/Dates/To");
				sTo=objNode.InnerText;
				sFrom=Conversion.GetDate(sFrom);
				sTo=Conversion.GetDate(sTo);
				if ((!sFrom.Equals("")) && (!sTo.Equals("")))
				{
					sbTemp.Append(" COMPLETE_DATE BETWEEN '" +sFrom +"' AND '"+sTo+"'");
				}
				if (!sbTemp.ToString().Equals(""))
				{
					sWhere = sWhere + " AND " + sbTemp.ToString();
				}
				sbTemp=new StringBuilder();
				objNode=objDOM.SelectSingleNode("//WPAUtil/Transfer/DiaryStatus/Open");
				sStatusOpen=objNode.InnerText;
				objNode=objDOM.SelectSingleNode("//WPAUtil/Transfer/DiaryStatus/Close");
				sStatusClose=objNode.InnerText;
				if (sStatusOpen.Trim().ToLower().Equals("true") &&(!sStatusClose.Trim().ToLower().Equals("true")) )
				{
                    //Changed by Gagan for MITS 10642: Start
					sbTemp.Append(" (STATUS_OPEN = -1 OR STATUS_OPEN = 1) ");
                    //sbTemp.Append(" STATUS_OPEN = 1 ");
                    //Changed by Gagan for MITS 10642: End
				}
				else if ((!sStatusOpen.Trim().ToLower().Equals("true")) &&(sStatusClose.Trim().ToLower().Equals("true")) )
				{
					sbTemp.Append(" STATUS_OPEN = 0 ");
				}
				if (!sbTemp.ToString().Equals(""))
				{
					sWhere = sWhere + " AND " + sbTemp.ToString();
				}
				sSQL=sSQL+sWhere;
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAUtil.CreateTransferDiarySQL.Error", m_iClientId), p_objEx);//psharma206	jira 101
			}
			finally
			{
				sbTemp=null;
				objDOM=null;
				objNode=null;
			}
			return sSQL;

		}
		private string CreatePurgeDiarySQL(int p_iDiaryStatus,XmlDocument p_objDOC)
		{
			string sSQL="";
			string sWhere="";
			string sFrom="";
			string sTo="";
			StringBuilder sbTemp=null;
			XmlDocument objDOM=null;
			XmlNode objNode=null;
			string sStatus="";
			string[] arrSelectedUsers=null;
			try
			{
				objDOM=p_objDOC;
				switch(p_iDiaryStatus)
				{
					case 1://open
						objNode=objDOM.SelectSingleNode("//WPAUtil/Purge/DiaryStatus/Open");
						if (objNode.InnerText!=null)
						sStatus=objNode.InnerText;
						if (!sStatus.Trim().ToLower().Equals("true")) return "";
						sSQL = " UPDATE WPA_DIARY_ENTRY SET DIARY_VOID = -1";
						break;
					case 2://close
						objNode=objDOM.SelectSingleNode("//WPAUtil/Purge/DiaryStatus/Close");
						if (objNode.InnerText!=null)
						sStatus=objNode.InnerText;
						if (!sStatus.Trim().ToLower().Equals("true")) return "";
						sSQL = " UPDATE WPA_DIARY_ENTRY SET DIARY_DELETED = -1";
						break;
				}
				switch(p_iDiaryStatus)
				{
					case 1://open
						sWhere = " WHERE STATUS_OPEN <> 0";
						break;
					case 2://close
						sWhere = " WHERE STATUS_OPEN = 0";
						break;
				}
				sbTemp=new StringBuilder();
				objNode = objDOM.SelectSingleNode("//WPAUtil/Purge/User");
				if (objNode.InnerText!=null)
				arrSelectedUsers=objNode.InnerText.Split(' ');
				for(int i=0;i<arrSelectedUsers.Length;i++)
				{
					if (!arrSelectedUsers[i].Trim().Equals(""))
					{
						if (!sbTemp.ToString().Trim().Equals(""))
						{
							sbTemp.Append(" OR ");
						}
						sbTemp.Append(" ASSIGNED_USER='"+GetLogingName(arrSelectedUsers[i])+"'");
					}
				}
				if (!sbTemp.ToString().Equals(""))
				{
					sWhere = sWhere + " AND " + "(" + sbTemp.ToString() + ")";
				}
				sbTemp=new StringBuilder();
				objNode = objDOM.SelectSingleNode("//WPAUtil/Purge/Dates/From");
				if (objNode.InnerText!=null)
				sFrom=objNode.InnerText;
				objNode = objDOM.SelectSingleNode("//WPAUtil/Purge/Dates/To");
				if (objNode.InnerText!=null)
				sTo=objNode.InnerText;
				sFrom=Conversion.GetDate(sFrom);
				sTo=Conversion.GetDate(sTo);
				if ((!sFrom.Equals("")) && (!sTo.Equals("")))
				{
					sbTemp.Append(" COMPLETE_DATE BETWEEN '" +sFrom +"' AND '"+sTo+"'");
				}
				if (!sbTemp.ToString().Equals(""))
				{
					sWhere = sWhere + " AND " + sbTemp.ToString();
				}
				sSQL=sSQL+sWhere;
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAUtil.CreatePurgeDiarySQL.Error", m_iClientId), p_objEx);//psharma206	jira 101
			}
			finally
			{
				sbTemp=null;
				objDOM=null;
				objNode=null;
			}
			return sSQL;

		}
		private XmlDocument GetOwners()
		{
			XmlDocument objDOM=null;
			XmlElement objElemChild=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			DbReader objReader=null;
			string sName="";
			string sFName="";
			string sLName="";
			StringBuilder sbSQL=null;
			try
			{
				sbSQL=new StringBuilder();
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("WPAUtil");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Owners");
				objDOM.FirstChild.AppendChild(objElemChild);
				sbSQL.Append("SELECT USER_DETAILS_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
				sbSQL.Append(" FROM USER_DETAILS_TABLE,USER_TABLE");
				sbSQL.Append(" WHERE USER_DETAILS_TABLE.DSNID ="+m_sDsnId);
				sbSQL.Append(" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
				objReader = DbFactory.GetDbReader(m_sSecureDSN,sbSQL.ToString());
				while  (objReader.Read())
				{
					sName="";
					sFName=Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Trim(); 
					sLName=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")).Trim();
					if (sFName.Equals(""))
					{
						sName=sLName;
					}
					else
					{
						sName=sFName+" "+sLName;
					}
					objElemTemp=objDOM.CreateElement("User");
					objElemTemp.SetAttribute("userid",Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
					objElemTemp.InnerText=sName;
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAUtil.GetOwners.Error", m_iClientId), p_objEx);//psharma206	jira 101
			}
			finally
			{
				objDOM=null;
				objElemChild=null;
				objElemParent=null;
				objElemTemp=null;
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				objReader=null;
				sbSQL=null;
			}
			
		}
		#endregion

		#region Public Functions
		/// Name		: SaveDiaryTransfer
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves Diary Transfer information
		/// </summary>
		/// <param name="p_objDocument">XmlDocument containing data to save</param>
		/// <returns>bool</returns>
		public bool SaveDiaryTransfer(XmlDocument p_objDocument,ref int p_iRowsAffected)
		{
			string sSQL="";
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			int iRowsAffected=0;
			ArrayList arrlstSql=null;
			try
			{
				arrlstSql=new ArrayList();
				sSQL=CreateTransferDiarySQL(p_objDocument);
				if (!sSQL.Trim().Equals(""))
				{
					arrlstSql.Add(sSQL);
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						iRowsAffected=objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
                    if (objTran != null)
                        objTran.Dispose();
					//Raman: We should not populate error collection here
                    /*
                    System.Exception exc=new Exception(iRowsAffected.ToString()+" Rows affected.");
					p_objErrOut.Add(exc,iRowsAffected.ToString()+" Rows affected.",BusinessAdaptorErrorType.PopupMessage);
                    */
                    p_iRowsAffected = iRowsAffected;
					return false;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAUtil.SaveDiaryTransfer.Error", m_iClientId), p_objEx);//psharma206	jira 101
			}
			finally
			{
				objConn = null;
				objTran=null;
				objCommand=null;
				arrlstSql=null;
			}
			return true;
		}
		/// Name		: SaveDiaryPurge
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves Diary Purge information
		/// </summary>
		/// <param name="p_objDocument">XmlDocument containing data to save</param>
		/// <returns>bool</returns>
		public bool SaveDiaryPurge(XmlDocument p_objDocument)
		{
			string sSQL="";
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			ArrayList arrlstSql=null;
			try
			{
				arrlstSql=new ArrayList();
				sSQL=CreatePurgeDiarySQL(1,p_objDocument);
				if (!sSQL.Trim().Equals(""))
				{
					arrlstSql.Add(sSQL);
				}
				sSQL=CreatePurgeDiarySQL(2,p_objDocument);
				if (!sSQL.Trim().Equals(""))
				{
					arrlstSql.Add(sSQL);
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
                    if (objTran != null)
                        objTran.Dispose();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAUtil.SaveDiaryPurge.Error", m_iClientId), p_objEx);//psharma206	jira 101
			}
			finally
			{
				objConn = null;
				objTran=null;
				objCommand=null;
				arrlstSql=null;
			}
			return true;
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get() 
		{
			XmlDocument objDOM=null;
			XmlDocument objTempDOM=null;
			XmlNode objNode=null;
			string sTemp="";
			try
			{
				objDOM=new XmlDocument();
				objDOM=GetOwners();
				sTemp="<Transfer><FromUser/><ToUser/><DiaryStatus><Open/><Close/></DiaryStatus><Dates><From/><To/></Dates></Transfer>";
				objTempDOM=new XmlDocument();
				objTempDOM.LoadXml(sTemp);
				objNode=objDOM.ImportNode(objTempDOM.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNode);
				sTemp="<Purge><User/><DiaryStatus><Open/><Close/></DiaryStatus><Dates><From/><To/></Dates></Purge>";
				objTempDOM.LoadXml(sTemp);
				objNode=objDOM.ImportNode(objTempDOM.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNode);
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAUtil.Get.Error", m_iClientId), p_objEx);//psharma206	jira 101
			}
			finally
			{
				objDOM=null;
				objTempDOM=null;
				objNode=null;
			}
			
		}	
		
		#endregion
	}
}
