using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common; 
using Riskmaster.Security;
 
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   04th,May 2005
	///Purpose :   BRS CPT Basic Code Details Form 
	/// </summary>
	public class BRSCPTBasicCodeDetails:UtilitiesUIBase 
	{
		/// <summary>
		/// String array for database fields mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "CPT"},
			{"CPTCode", "CPT"},
			{"CodeDesc", "CPT_DESC"},
			{"Amount", "AMOUNT"},
			{"TableId", "TABLE_ID"},
			{"CPTCodeComp","CPT"}
									  };
        private int m_iClientId = 0;//Add  by kuladeep for review defect.
        UserLogin m_oUserlogin = null;//nshah28 RMA-9168

		/// Name		: BRSCPTBasicCodeDetails
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string parameter</param>
        public BRSCPTBasicCodeDetails(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			ConnectString = p_sConnString;
			this.Initialize(); 
		}

        //JIRA RMA-9168 nshah28 start 
        public BRSCPTBasicCodeDetails(UserLogin p_oUserLogin, string p_sConnString, int p_iClientId)
            : base(p_oUserLogin, p_sConnString, p_iClientId)
        {
            m_oUserlogin = p_oUserLogin;
            m_iClientId = p_iClientId;
            ConnectString = p_sConnString;
            this.Initialize();
        }
        //JIRA RMA-9168 End 

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Initialize variables and properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "WRCMP_CPT";
			KeyField = "CPT";
			base.InitFields(arrFields);
			base.Initialize();
		}		

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Gets the CPT Codes details
		/// </summary>
		/// <param name="p_objXmlDocument">Input Xml document for structure</param>
		/// <returns>Xml document with data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			string sCPTCode="";
			XmlNode objNode=null;
			LocalCache objCache=null;
			string sShortCode = "";
			string sCodeDesc = ""; 
			int iSpecCode=0;
			XMLDoc = p_objXmlDocument;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
                if (objNode != null)
                    base.WhereClause = "TABLE_ID=" + objNode.InnerText;
                base.Get( true );
                sCPTCode = p_objXmlDocument.SelectSingleNode("//control[@name='CPTCode']").InnerText;
                if (sCPTCode.Length > 5)
                {
                    p_objXmlDocument.SelectSingleNode("//control[@name='CPTCode']").InnerText = sCPTCode.Substring(0, 5);
                    p_objXmlDocument.SelectSingleNode("//control[@name='CPTCodeSupp']").InnerText = sCPTCode.Substring(0, 5) + " - " + sCPTCode.Substring(5, sCPTCode.Length - 5);
                    //Mukul Added 01/25/2007 MITS 8667 Handling specialiy code
                    objCache = new LocalCache(ConnectString, m_iClientId);
                    iSpecCode = objCache.GetCodeId(sCPTCode.Substring(5, sCPTCode.Length - 5), "SPECIALTY");
                    if (iSpecCode > 0)
                    {
                        objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SpecialityCode']");
                        if (objNode != null)
                        {
                            ((XmlElement)objNode).SetAttribute("codeid", iSpecCode.ToString());
                            objCache.GetCodeInfo(iSpecCode, ref sShortCode, ref sCodeDesc);
                            objNode.InnerText = sShortCode + " " + sCodeDesc;
                        }
                    }
                }
                return XMLDoc;
            }
            catch(Exception ex)
            {
                throw new RMAppException(Globalization.GetString("BRSCPTBasicCodeDetails.Get.Error", m_iClientId), ex);
            }
            finally
            {
                objNode = null;
                if (objCache != null)
                    objCache.Dispose();
            }
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Saves the data for the form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data and structure</param>
		/// <returns>xml document with saved data</returns>
		//public XmlDocument Save(XmlDocument p_objXmlDocument)
        public XmlDocument Save(XmlDocument p_objXmlDocument, UserLogin p_objUserLogin)
		{
			int iTableId=0;
			string sCPT="";
			string sSQL="";
			XmlNode objNode=null;
			DbReader objRdr=null;
			string sCodeDesc="";
			double dAmount=0;
			string sRowId="";
			string sCPTCodeComp="";
			DbConnection objCn=null;
			LocalCache objCache=null;
            //Added:yukti, MITS 34530
            string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
            int iLangCode = p_objUserLogin.objUser.NlsCode;
            //Ended:Yukti
			try
			{
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
				if(objNode!=null)
					sRowId= objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
				if(objNode!=null)
					iTableId = Conversion.ConvertStrToInteger(objNode.InnerText);   
				
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CPTCode']");
				if(objNode!=null)
					sCPT = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CodeDesc']");
				if(objNode!=null)
					sCodeDesc = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Amount']");
				if(objNode!=null)
					dAmount = Conversion.ConvertStrToDouble(objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CPTCodeComp']");
				if(objNode!=null)
					sCPTCodeComp = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SpecialityCode']");
				if(objNode!=null)
				{
                    string sCodeId = ((XmlElement)objNode).GetAttribute("codeid");
					if(!string.IsNullOrEmpty(sCodeId) && sCodeId != "0")
					{
                        objCache = new LocalCache(ConnectString, m_iClientId);  
						sCPT = sCPT + objCache.GetShortCode(Conversion.ConvertStrToInteger(sCodeId));  
						objCache.Dispose(); 
					}
				}

				if(string.IsNullOrEmpty(sRowId))
				{
					objRdr=DbFactory.GetDbReader(ConnectString,"SELECT * FROM WRCMP_CPT WHERE TABLE_ID=" +
						iTableId + " AND CPT='" + sCPT + "'");
					if(objRdr.Read())
					{
						throw new RMAppException(Globalization.GetString("BRSCPTBasicCodeDetails.Save.DupErr",m_iClientId));  
					}
                    //Added:Yukti, MITS 34530
                    if (sBaseLangCode.Equals(iLangCode))
                    {
                        sSQL = "INSERT INTO WRCMP_CPT(TABLE_ID,CPT,CPT_DESC,AMOUNT) " +
                            "VALUES( " + iTableId + ",'" + sCPT + "','" + sCodeDesc + "'," + dAmount + ")";
                    }
                    else
                    {
                        sSQL = "INSERT INTO WRCMP_CPT(TABLE_ID,CPT,CPT_DESC,AMOUNT) " +
                            "VALUES( " + iTableId + "," + " N'" + sCPT + "'," + " N'" + sCodeDesc + "'," + dAmount + ")";
                    }
                    //Ended:Yukti, MITS 34530
				}
				else
					sSQL = "UPDATE WRCMP_CPT SET CPT_DESC='" + sCodeDesc + "', AMOUNT=" + dAmount + " WHERE" +
						" TABLE_ID=" + iTableId + " AND CPT='" + sCPTCodeComp + "'" ;

				
				objCn= DbFactory.GetDbConnection(ConnectString);
				objCn.Open();
				objCn.ExecuteNonQuery(sSQL); 
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSCPTBasicCodeDetails.Save.Err", m_iClientId), p_objEx);
			}
			finally
			{
                if (objRdr != null)
                {
                    objRdr.Close();
                    objRdr.Dispose();
                }
				objNode=null;
				if(objCn!=null)
				{
					objCn.Dispose();
					
				}
				if(objCache!=null)
				{
					objCache.Dispose();
					
				}
               
			}
		}
	}
}