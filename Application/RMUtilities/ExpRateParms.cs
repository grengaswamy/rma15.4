using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author	:   Anurag Agarwal
	///Class	:	ExpRateParms
	///Dated	:   06 April 2005
	///Purpose	:   This class is use Add / Modify the Exposure Rates data
	/// </summary>
	public class ExpRateParms
	{
		/// <summary>
		/// Private variable to store Connection String
		/// </summary>
        private string m_sConnectString = "";

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "";
		
		/// <summary>
		/// Private constant variable to store TableName
		/// </summary>
		private const string TABLENAME = "SYS_POL_EXP_RATES";
        //Anu Tennyson for MITS 18231 STARTS 2/2/2010
        /// <summary>
        /// The Code Table for the finding the code id.
        /// </summary>
        private const string RATE_TYPE_CODE_TABLENAME = "RATE_MAINTENANCE_TYPE";
        private const string RANGE_CODE_TABLENAME = "RANGE_TYPE";
        //Anu Tennyson for MITS 18231 ENDS
        /// <summary>
        /// Private constant variable to store Rate Range TableName
        /// MITS 18231
        /// </summary>
        private const string RANGE_TABLENAME = "SYS_POL_RANGE";
        private int m_iClientId = 0;
        private UserLogin m_UserLogin = null; //vkumar258 - RMA-6037
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public ExpRateParms(string p_sLoginName, string p_sConnectionString, int p_iClientId)
		{
			m_sUserName = p_sLoginName;
			m_sConnectString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}

        public ExpRateParms(UserLogin oUserLogin ,string p_sLoginName, string p_sConnectionString, int p_iClientId = 0)//vkumar258 ML Changes
        {
            m_UserLogin= oUserLogin;
            m_sUserName = oUserLogin.LoginName;
            m_sConnectString = p_sConnectionString;
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// Addd by Tushar MITS 18231
        /// </summary>

        private Boolean bIsSucess = false;
		/// Name		: LoadData
		/// Author		: Anurag Agarwal
		/// Date Created	: 5 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Loads Exposure Rate record on the basis of Expraterowid passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">The Expraterowid is passed in the XML
		///		The sample input XML will be
		///				<Document>
		///					<ExpRates>
		///						<amount></amount>
		///						<exposureid></exposureid>
		///						<effectivedate></effectivedate>
		///						<expirationdate></expirationdate>
		///						<expratetype></expratetype>
		///						<state></state>
		///						<fixedorprorate></fixedorprorate>
		///						<baserate></baserate>
		///						<expraterowid>4</expraterowid>
		///					</ExpRates>
		///				</Document>
		/// </param>
		/// <returns>The XML with data is returned 
		///		The sample output XML will be 
		///				<Document>
		///					<ExpRates>
		///						<amount>23</amount>
		///						<exposureid>5266</exposureid>
		///						<effectivedate>20050428</effectivedate>
		///						<expirationdate>20050429</expirationdate>
		///						<expratetype>5238</expratetype>
		///						<state>5</state>
		///						<fixedorprorate>5236</fixedorprorate>
		///						<baserate>12</baserate>
		///						<expraterowid>4</expraterowid>
		///					</ExpRates>
		///				</Document>
		///</returns>
		public XmlDocument LoadData(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbReader objDbReader = null;
			string sExpRateRowId = "";
			XmlElement objExpRatesXMLEle = null;

			string sValue=string.Empty;
			string sShortCode=string.Empty;
			string sDesc=string.Empty;
			XmlElement objElement=null;
			LocalCache objLocalCache=null;
            //Added by Tushar  MITS 18231 2/2/2010
            //string sRateMaintenanceType=string.Empty ;//Deleted by Anu Tennyson 
            int iRateMaintenanceType = 0;
            string sRateMaintenanceType = string.Empty;
            //Anu Tennyson deleted for MITS 18231
            ExposureRate objRates = null;
            XmlElement objExpRatesRangeXMLEle = null;
            XmlElement objExpRangeListXMLEle = null;
            string sExpRateType = string.Empty;
            //End

            try
            {
                objExpRatesXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/ExpRates");
                objExpRangeListXMLEle = (XmlElement)objExpRatesXMLEle.SelectSingleNode("//Document/ExpRates/raterangelist");
                sExpRateRowId = objExpRatesXMLEle.GetElementsByTagName("expraterowid").Item(0).InnerText;

                //In case of Edit
                if (!string.IsNullOrEmpty(sExpRateRowId))
                {
                    sSQL = "SELECT * FROM " + TABLENAME + " WHERE EXP_RATE_ROWID= " + sExpRateRowId;
                    objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);

                    if (objDbReader != null)
                        if (objDbReader.Read())
                        {
                            objExpRatesXMLEle.GetElementsByTagName("amount").Item(0).InnerText = objDbReader["AMOUNT"].ToString();
                            objExpRatesXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText = Common.Conversion.GetUIDate(objDbReader["EFFECTIVE_DATE"].ToString(),m_UserLogin.objUser.NlsCode.ToString(), m_iClientId);//vkumar258 ML Changes
                            objExpRatesXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText = Common.Conversion.GetUIDate(objDbReader["EXPIRATION_DATE"].ToString(), m_UserLogin.objUser.NlsCode.ToString(), m_iClientId);//vkumar258 ML Changes

                            objLocalCache = new LocalCache(m_sConnectString,m_iClientId);

                            //Added by Tushar for VACo
                            //sRateMaintenanceType = objDbReader["TYPE"].ToString();//Deleted by Anu Tennyson
                            //Added By Anu Tennyson STARTS for MITS 18231 2/2/2010
                            iRateMaintenanceType = Conversion.CastToType<int>(Convert.ToString(objDbReader["RATE_MAINTENANCE_TYPE"]), out bIsSucess);
                            sRateMaintenanceType = objLocalCache.GetShortCode(iRateMaintenanceType);
                            //Added By Anu Tennyson ENDS for MITS 18231
                            if (string.Compare(sRateMaintenanceType, "C") != 0)
                                objExpRatesXMLEle.GetElementsByTagName("baserate").Item(0).InnerText = objDbReader["BASE_RATE"].ToString();

                            objElement = (XmlElement)objExpRatesXMLEle.GetElementsByTagName("LINEOFBUSINESS").Item(0);
                            sValue = objDbReader["LINE_OF_BUSINESS"].ToString();
                            objLocalCache.GetCodeInfo(Conversion.CastToType<int>(sValue, out bIsSucess), ref sShortCode, ref sDesc);
                            objElement.InnerText = sShortCode + " " + sDesc;
                            objElement.SetAttribute("codeid", sValue);



                            objElement = (XmlElement)objExpRatesXMLEle.GetElementsByTagName("OrgHierarchyCode").Item(0);
                            sValue = objDbReader["ORG_HIERARCHY"].ToString();
                            objLocalCache.GetOrgInfo(Conversion.CastToType<int>(sValue, out bIsSucess), ref sShortCode, ref sDesc);
                            objElement.InnerText = sShortCode + " " + sDesc;
                            objElement.SetAttribute("codeid", sValue);

                            //End
                            objElement = (XmlElement)objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0);
                            sValue = objDbReader["FLAT_OR_PERCENT"].ToString();
                            objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue), ref sShortCode, ref sDesc);
                            objElement.InnerText = sShortCode + " " + sDesc;
                            objElement.SetAttribute("codeid", sValue);

                            objElement = (XmlElement)objExpRatesXMLEle.GetElementsByTagName("fixedorprorate").Item(0);
                            sValue = objDbReader["FIXED_OR_PRORATE"].ToString();
                            objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue), ref sShortCode, ref sDesc);
                            objElement.InnerText = sShortCode + " " + sDesc;
                            objElement.SetAttribute("codeid", sValue);

                            objElement = (XmlElement)objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0);
                            sValue = objDbReader["EXPOSURE_ID"].ToString();
                            objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue), ref sShortCode, ref sDesc);
                            objElement.InnerText = sShortCode + " " + sDesc;
                            objElement.SetAttribute("codeid", sValue);

                            objElement = (XmlElement)objExpRatesXMLEle.GetElementsByTagName("state").Item(0);
                            sValue = objDbReader["STATE"].ToString();
                            objLocalCache.GetStateInfo(Conversion.ConvertStrToInteger(sValue), ref sShortCode, ref sDesc);
                            objElement.InnerText = sShortCode + " " + sDesc;
                            objElement.SetAttribute("codeid", sValue);
                        }
                    if (objDbReader != null)
                        objDbReader.Close();
                    //Added bY Tushar for MITS 18231

                    objRates = new ExposureRate(m_iClientId);
                    objRates.ConnectionString = m_sConnectString;
                    objRates.RateRangeList.RateRowid = Conversion.CastToType<int>(sExpRateRowId, out bIsSucess);
                    objRates.RateRangeList.LoadData();

                    foreach (RateRange objRateRange in objRates.RateRangeList)
                    {
                        objExpRatesRangeXMLEle = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("raterange"), false);

                        objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("addedbyuser", objRateRange.AddedByUser), true));
                        objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdadded", objRateRange.DttmRcdAdded), true));
                        objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("updatedbyuser", objRateRange.UpdatedByUser), true));
                        objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdlastupd", objRateRange.DttmRcdLastUpd), true));
                        objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("rangetype", string.Format("{0:C}", objRateRange.RangeType)), true));

                        sExpRateType = objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0).InnerText;
                        sExpRateType = sExpRateType.Substring(0, 1);

                        if (string.Compare(sExpRateType, "F") == 0)
                        {
                            objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("modifier", objRateRange.Modifier.ToString()), true));
                            if (string.IsNullOrEmpty(objRateRange.BeginningRange))
                                objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("Deductible", objRateRange.Deductible.ToString()), true));
                            else
                                objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("Deductible", ""), true));
                        }
                        else
                        {
                            objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("modifier", objRateRange.Modifier.ToString() + "%"), true));
                            if (string.IsNullOrEmpty(objRateRange.BeginningRange))
                                objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("Deductible", objRateRange.Deductible.ToString() + "%"), true));
                            else
                                objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("Deductible", string.Format("{0:C}", "")), true));
                        }



                        if (string.IsNullOrEmpty(objRateRange.BeginningRange))
                        {
                            objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("beginningrange", string.Format("{0:C}", "")), true));
                        }
                        else
                        {
                            objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("beginningrange", string.Format("{0:C}", objRateRange.BeginningRange)), true));
                        }
                        objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("raterangerowid", objRateRange.RangeRowId.ToString()), true));
                        if (string.IsNullOrEmpty(objRateRange.EndRange))
                            objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange", string.Format("{0:C}", "")), true));
                        else
                            objExpRatesRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange", string.Format("{0:C}", objRateRange.EndRange)), true));

                        objExpRangeListXMLEle.AppendChild(objExpRatesRangeXMLEle);
                    }

                }

                return p_objInputXMLDoc;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                switch (sRateMaintenanceType)
                {
                    case "E":
                        throw new RMAppException(Globalization.GetString("ExpRateParms.LoadData.Exposure.Error", m_iClientId), p_objExp);
                    case "C":
                        throw new RMAppException(Globalization.GetString("ExpRateParms.LoadData.Coverage.Error", m_iClientId), p_objExp);
                    case "U":
                        throw new RMAppException(Globalization.GetString("ExpRateParms.LoadData.UAR.Error", m_iClientId), p_objExp);
                    default:
                        throw new RMAppException(Globalization.GetString("ExpRateParms.LoadData.Error", m_iClientId), p_objExp);
                }
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (objRates != null)
                    objRates = null;
            }

		}

		/// Name		: Save
		/// Author		: Anurag Agarwal
		/// Date Created	: 6 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves Exposure Rate record on the basis of Expraterowid passed
		/// If new record then Expraterowid will be empty string
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<ExpRates>
		///						<amount>23</amount>
		///						<exposureid>5266</exposureid>
		///						<effectivedate>20050428</effectivedate>
		///						<expirationdate>20050429</expirationdate>
		///						<expratetype>5238</expratetype>
		///						<state>5</state>
		///						<fixedorprorate>5236</fixedorprorate>
		///						<baserate>12</baserate>
		///						<expraterowid>4</expraterowid> OR <expraterowid></expraterowid>(for new record)
		///					</ExpRates>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		public bool Save(XmlDocument p_objInputXMLDoc)
		{
            //string sSQL = string.Empty;
            StringBuilder sSQL = new StringBuilder();
			DbConnection objCon = null; 
            string sExpRateRowId = string.Empty;
			ExposureRate objExposureRate = null;
			bool bIsNew = false;
			XmlElement objExpRatesXMLEle = null;
            //Added By tushar for MITS 18231
            string sRateMaintenanceType = string.Empty; 
            LocalCache objLocalCache = null; 
            XmlNodeList objRangeNodeList = null;
            RateRange objRateRange = null; 
            string sRateRangeRowId = string.Empty; 
            string sTempModifier = string.Empty; 
            string sTempDeductible = string.Empty;
            //Anu Tennyson for MITS 18231 STARTS 2/2/2010
            int iRangeType = 0;

			try
			{
                //Tushar:MITS#18231
                objExpRatesXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/ExpRates");
                sRateMaintenanceType = objExpRatesXMLEle.GetElementsByTagName("RateMaintenanceType").Item(0).InnerText;
                
                //End: MITS#18231
				if(Validate(p_objInputXMLDoc)) 
				{
					objExpRatesXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/ExpRates"); 	

					sExpRateRowId = objExpRatesXMLEle.GetElementsByTagName("expraterowid").Item(0).InnerText;

                    objExposureRate = new ExposureRate(m_iClientId);

					if( string.IsNullOrEmpty (  sExpRateRowId))
					{
						bIsNew = true;
                        objExposureRate.ExpRateRowId = Utilities.GetNextUID(m_sConnectString, TABLENAME, m_iClientId);
						objExposureRate.AddedByUser = m_sUserName;
                        objExposureRate.DttmRcdAdded = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
					}
					else
						objExposureRate.ExpRateRowId = Conversion.ConvertStrToInteger(sExpRateRowId);

					objExposureRate.UpdatedByUser = m_sUserName;
                    objExposureRate.DttmRcdLastUpd = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
					//objExposureRate.ExposureId = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).InnerText);
					objExposureRate.ExposureId = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).Attributes["codeid"].Value);
					//objExposureRate.State = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("state").Item(0).InnerText);
					objExposureRate.State = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value);
					objExposureRate.Amount = Conversion.ConvertStrToDouble(objExpRatesXMLEle.GetElementsByTagName("amount").Item(0).InnerText);
					//objExposureRate.ExpRateType = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0).InnerText);
					objExposureRate.ExpRateType = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("expratetype").Item(0).Attributes["codeid"].Value);
					objExposureRate.EffectiveDate = objExpRatesXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
					objExposureRate.ExpirationDate = objExpRatesXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
					//objExposureRate.FixedOrProRate = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("fixedorprorate").Item(0).InnerText);
					objExposureRate.FixedOrProRate = Conversion.ConvertStrToInteger(objExpRatesXMLEle.GetElementsByTagName("fixedorprorate").Item(0).Attributes["codeid"].Value);
                    //Mukul(6/18/07) Added to save blank expiration date MITS 9789
                    //Added By Tushar for Added LOB and Rate MAintenance MITS 18231
                    //objExposureRate.RateMaintenanceType = sRateMaintenancetypeShortCode.Trim();//Deleted by Anu Tennyson
                    objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
                    
                    //Anu Tennyson for MITS 18231 STARTS 2/2/2010
                    objExposureRate.RateMaintenanceType = objLocalCache.GetCodeId(sRateMaintenanceType.Trim(), RATE_TYPE_CODE_TABLENAME);
                    //Anu Tennyson for MITS 18231 ENDS

                    if (sRateMaintenanceType.ToUpper() != "C")
                    {
                        objExposureRate.BaseRate = Conversion.CastToType<double>(objExpRatesXMLEle.GetElementsByTagName("baserate").Item(0).InnerText,out bIsSucess );
                    }

                    objExposureRate.LineOFBusiness = Conversion.CastToType<int>(objExpRatesXMLEle.GetElementsByTagName("LINEOFBUSINESS").Item(0).Attributes["codeid"].Value,out bIsSucess );
                    objExposureRate.OrgHierarchy = Conversion.CastToType<int>(objExpRatesXMLEle.GetElementsByTagName("OrgHierarchyCode").Item(0).Attributes["codeid"].Value,out bIsSucess );

                     //End
					if(bIsNew)
					{
                        //Parijat : Mits 11446
                        if (objExposureRate.ExpirationDate.Trim() == "" || objExposureRate.ExpirationDate == "0")
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("INSERT INTO {0} (EXP_RATE_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,",TABLENAME);
                            sSQL.Append("UPDATED_BY_USER,EXPOSURE_ID,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,FIXED_OR_PRORATE," );
                            sSQL.Append("BASE_RATE,RATE_MAINTENANCE_TYPE,LINE_OF_BUSINESS,ORG_HIERARCHY) VALUES(");
                            sSQL.AppendFormat("{0},'{1}','{2}','{3}','{4}',{5}, {6},{7},{8},{9},{10},{11},{12},{13},{14})",  
                                objExposureRate.ExpRateRowId,objExposureRate.AddedByUser,objExposureRate.DttmRcdAdded,
                                objExposureRate.DttmRcdLastUpd,objExposureRate.UpdatedByUser,objExposureRate.ExposureId,
                                objExposureRate.State,objExposureRate.Amount,objExposureRate.ExpRateType,
                                UTILITY.FormatDate(objExposureRate.EffectiveDate, true), objExposureRate.FixedOrProRate, objExposureRate.BaseRate,
                                objExposureRate.RateMaintenanceType, objExposureRate.LineOFBusiness, objExposureRate.OrgHierarchy);
                        }
                        else
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("INSERT INTO {0} (EXP_RATE_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,", TABLENAME);
                            sSQL.Append("UPDATED_BY_USER,EXPOSURE_ID,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,EXPIRATION_DATE,FIXED_OR_PRORATE,");
                            sSQL.Append("BASE_RATE,RATE_MAINTENANCE_TYPE,LINE_OF_BUSINESS,ORG_HIERARCHY) VALUES(");
                            sSQL.AppendFormat("{0},'{1}','{2}','{3}','{4}',{5}, {6},{7},{8},{9},{10},{11},{12},{13},{14},{15})",
                                objExposureRate.ExpRateRowId, objExposureRate.AddedByUser, objExposureRate.DttmRcdAdded,
                                objExposureRate.DttmRcdLastUpd, objExposureRate.UpdatedByUser, objExposureRate.ExposureId,
                                objExposureRate.State, objExposureRate.Amount, objExposureRate.ExpRateType,
                                UTILITY.FormatDate(objExposureRate.EffectiveDate, true), UTILITY.FormatDate(objExposureRate.ExpirationDate, true),
                                objExposureRate.FixedOrProRate, objExposureRate.BaseRate,
                                objExposureRate.RateMaintenanceType, objExposureRate.LineOFBusiness, objExposureRate.OrgHierarchy);

                        }
                    }
                    else
                    {
                        //Parijat : Mits 11446
                        if (objExposureRate.ExpirationDate.Trim() == "" || objExposureRate.ExpirationDate == "0")
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("UPDATE {0} SET DTTM_RCD_LAST_UPD = '{1}',", TABLENAME, objExposureRate.DttmRcdLastUpd);
                            sSQL.AppendFormat("UPDATED_BY_USER = '{0}',EXPOSURE_ID = {1}, STATE = {2}, AMOUNT = {3},",
                                objExposureRate.UpdatedByUser, objExposureRate.ExposureId, objExposureRate.State, objExposureRate.Amount);
                            sSQL.AppendFormat("FLAT_OR_PERCENT = {0},EFFECTIVE_DATE = {1}, EXPIRATION_DATE = NULL, FIXED_OR_PRORATE = {2}, BASE_RATE = {3},",
                               objExposureRate.ExpRateType, UTILITY.FormatDate(objExposureRate.EffectiveDate, true), objExposureRate.FixedOrProRate, objExposureRate.BaseRate);
                            sSQL.AppendFormat("LINE_OF_BUSINESS = {0},RATE_MAINTENANCE_TYPE = {1}, ORG_HIERARCHY = {2} WHERE EXP_RATE_ROWID = {3}",
                               objExposureRate.LineOFBusiness, objExposureRate.RateMaintenanceType, objExposureRate.OrgHierarchy, objExposureRate.ExpRateRowId);
                        }
                        else
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("UPDATE {0} SET DTTM_RCD_LAST_UPD = '{1}',", TABLENAME, objExposureRate.DttmRcdLastUpd);
                            sSQL.AppendFormat("UPDATED_BY_USER = '{0}',EXPOSURE_ID = {1}, STATE = {2}, AMOUNT = {3},",
                                objExposureRate.UpdatedByUser, objExposureRate.ExposureId, objExposureRate.State, objExposureRate.Amount);
                            sSQL.AppendFormat("FLAT_OR_PERCENT = {0},EFFECTIVE_DATE = {1}, EXPIRATION_DATE = {2}, FIXED_OR_PRORATE = {3}, BASE_RATE = {4},",
                               objExposureRate.ExpRateType, UTILITY.FormatDate(objExposureRate.EffectiveDate, true), UTILITY.FormatDate(objExposureRate.ExpirationDate, true), objExposureRate.FixedOrProRate, objExposureRate.BaseRate);
                            sSQL.AppendFormat("LINE_OF_BUSINESS = {0},RATE_MAINTENANCE_TYPE = {1}, ORG_HIERARCHY = {2} WHERE EXP_RATE_ROWID = {3}",
                               objExposureRate.LineOFBusiness, objExposureRate.RateMaintenanceType, objExposureRate.OrgHierarchy, objExposureRate.ExpRateRowId);
                        }
                    }

					objCon = DbFactory.GetDbConnection(m_sConnectString);
					objCon.Open();
					objCon.ExecuteNonQuery(sSQL.ToString());
                    //Added bY Tushar for MITS 18231

                    //code for saving the Rate Range data
                    objRangeNodeList = p_objInputXMLDoc.GetElementsByTagName("raterange");
                    objRateRange = new RateRange();
                    foreach (XmlElement objRateRangeXMLEle in objRangeNodeList)
                    {
                        

                        sRateRangeRowId = objRateRangeXMLEle.GetElementsByTagName("raterangerowid").Item(0).InnerText;
                        bIsNew = false;
                        if (string.IsNullOrEmpty(    sRateRangeRowId ))
                        {
                            bIsNew = true;
                            objRateRange.RangeRowId = Utilities.GetNextUID(m_sConnectString, RANGE_TABLENAME, m_iClientId);
                            objRateRange.AddedByUser = m_sUserName;
                            objRateRange.DttmRcdAdded = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
                        }
                        else
                            objRateRange.RangeRowId = Conversion.CastToType<int>  (sRateRangeRowId, out bIsSucess  );

                        objRateRange.UpdatedByUser = m_sUserName;
                        objRateRange.DttmRcdLastUpd = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
                        objRateRange.RateRowid = objExposureRate.ExpRateRowId;
                        
                        sTempModifier = objRateRangeXMLEle.GetElementsByTagName("modifier").Item(0).InnerText;
                        sTempModifier = Conversion.ConvertStrToDouble(sTempModifier).ToString();
                        //if (sTempModifier.IndexOf('%') >= 0)
                        //{
                        //    objRateRangeXMLEle.GetElementsByTagName("modifier").Item(0).InnerText = sTempModifier.Remove(sTempModifier.IndexOf('%'));
                        //}
                        ////Start:Neha suresh Jain, 05/19/2010, MITS 20659
                        //if (sTempModifier.IndexOf('$') >= 0)
                        //{
                        //    objRateRangeXMLEle.GetElementsByTagName("modifier").Item(0).InnerText = sTempModifier.Replace("$", "");
                        //}
                        //End:Neha suresh Jain 
                        sTempDeductible = objRateRangeXMLEle.GetElementsByTagName("Deductible").Item(0).InnerText;
                        sTempDeductible = Conversion.ConvertStrToDouble(sTempDeductible).ToString();
                        //if (sTempDeductible.IndexOf('%') >= 0)
                        //{
                        //    objRateRangeXMLEle.GetElementsByTagName("Deductible").Item(0).InnerText = sTempDeductible.Remove(sTempDeductible.IndexOf('%'));
                        //}
                        ////Start:Neha suresh Jain, 05/19/2010, MITS 20659
                        //if (sTempDeductible.IndexOf('$') >= 0)
                        //{
                        //    objRateRangeXMLEle.GetElementsByTagName("Deductible").Item(0).InnerText = sTempDeductible.Replace("$", "");
                        //}
                        ////End:Neha suresh Jain 
                        objRateRange.RangeType = objRateRangeXMLEle.GetElementsByTagName("rangetype").Item(0).InnerText.Substring(0, 1); 
                        //Anu Tennyson for MITS 18229 STARTS 2/2/2010
                        iRangeType = objLocalCache.GetCodeId(objRateRange.RangeType, RANGE_CODE_TABLENAME);
                        //Anu Tennyson for MITS 18229 ENDS
                        objRateRange.Modifier = Conversion.CastToType<double>(sTempModifier, out bIsSucess);
                        objRateRange.Deductible = Conversion.CastToType<double>(sTempDeductible,out bIsSucess );
                        objRateRange.BeginningRange = objRateRangeXMLEle.GetElementsByTagName("beginningrange").Item(0).InnerText;
                        objRateRange.EndRange = objRateRangeXMLEle.GetElementsByTagName("endrange").Item(0).InnerText;

                        if (bIsNew)
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("INSERT INTO {0} (ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,", RANGE_TABLENAME);
                            sSQL.Append("UPDATED_BY_USER,RATE_ROWID,RANGE_ROWID,RANGE_TYPE,MODIFIER,DEDUCTIBLE,BEGINNING_RANGE,END_RANGE) VALUES(");
                            sSQL.AppendFormat("'{0}','{1}','{2}','{3}',{4},{5}, {6},{7},{8},'{9}','{10}')",
                                objRateRange.AddedByUser, objRateRange.DttmRcdAdded, objRateRange.DttmRcdLastUpd,
                                objRateRange.UpdatedByUser, objRateRange.RateRowid,
                                objRateRange.RangeRowId, iRangeType, objRateRange.Modifier, objRateRange.Deductible,
                                objRateRange.BeginningRange, objRateRange.EndRange);
    
                        }
                        else
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("UPDATE {0} SET DTTM_RCD_LAST_UPD = '{1}', ", RANGE_TABLENAME, objRateRange.DttmRcdLastUpd);
                            sSQL.AppendFormat("UPDATED_BY_USER = '{0}',RATE_ROWID = {1}, RANGE_TYPE = {2}, MODIFIER = {3}, ",
                                objRateRange.UpdatedByUser, objRateRange.RateRowid, iRangeType, objRateRange.Modifier);
                            sSQL.AppendFormat("DEDUCTIBLE = {0},BEGINNING_RANGE = '{1}', END_RANGE = '{2}' WHERE RANGE_ROWID = {3}",
                               objRateRange.Deductible, objRateRange.BeginningRange, objRateRange.EndRange, objRateRange.RangeRowId);

                        }
                        objCon.ExecuteNonQuery(sSQL.ToString());

                        
                    }
                    objRateRange = null;
					objCon.Dispose(); 

					return true;
				}
				else
					return false;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                switch (sRateMaintenanceType)
                {
                    case "E":
                        throw new RMAppException(Globalization.GetString("ExpRateParms.Save.Exposure.Error", m_iClientId), p_objExp);
                    case "C":
                        throw new RMAppException(Globalization.GetString("ExpRateParms.Save.Coverage.Error", m_iClientId), p_objExp);
                    case "U":
                        throw new RMAppException(Globalization.GetString("ExpRateParms.Save.UAR.Error", m_iClientId), p_objExp);
                    default:
                        throw new RMAppException(Globalization.GetString("ExpRateParms.Save.Error", m_iClientId), p_objExp);
                }
			}
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
                //Added By Tushar MITS 18231
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (objRateRange != null)
                    objRateRange = null; 
                //End

            }

		}

		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes Expense Rate record
		/// </summary>
		/// <param name="p_iExpRateRowId">Expense Rate ID</param>
		/// <returns>True for sucess and false for failure</returns>
        public bool Delete(int p_iRateRowId)
		{
            StringBuilder sSQL = new StringBuilder()  ;
			DbConnection objCon = null; 

			try
			{
                if (p_iRateRowId != 0)
                {
                    sSQL.AppendFormat("DELETE FROM {0} WHERE EXP_RATE_ROWID = {1}", TABLENAME, p_iRateRowId);
                    objCon = DbFactory.GetDbConnection(m_sConnectString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL.ToString());

                    sSQL.Length = 0;
                    sSQL.AppendFormat("DELETE FROM {0} WHERE RATE_ROWID = {1}",RANGE_TABLENAME  ,p_iRateRowId);
                    objCon.ExecuteNonQuery(sSQL.ToString());

                    objCon.Dispose();
                }

				return true;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ExpRateParms.Delete.Error", m_iClientId), p_objExp);   
			}
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
            }

		}

        /// Name		: Delete
        /// Author		: Tushar Agarwal
        /// Date Created	: 11 Nov 2009
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///				   *               *               *
        /// ************************************************************
        /// <summary>
        /// Deletes Range Rate record
        /// </summary>
        /// <param name="p_iRangeRateRowId">Range Rate ID</param>
        /// <returns>True for sucess and false for failure</returns>
        /// MITS 18231
        /// 
		public bool DeleteRange(int p_iRangeRateRowId)
		{
			StringBuilder sSQL = new StringBuilder();
			DbConnection objCon = null; 

			try
			{
                if (p_iRangeRateRowId > 0)
				{
                    sSQL.AppendFormat("DELETE FROM {0} WHERE RANGE_ROWID = {1}",RANGE_TABLENAME,  p_iRangeRateRowId);
					objCon = DbFactory.GetDbConnection(m_sConnectString);
					objCon.Open();
					objCon.ExecuteNonQuery(sSQL.ToString());
					objCon.Dispose(); 
				}

				return true;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ExpRateParms.Range.Delete.Error", m_iClientId), p_objExp);   
			}
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
            }

		}
	
	
		/// Name		: Validate
		/// Author		: Anurag Agarwal
		/// Date Created	: 6 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Validate Exposure Rate data for the values passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<ExpRates>
		///						<amount>23</amount>
		///						<exposureid>5266</exposureid>
		///						<effectivedate>20050428</effectivedate>
		///						<expirationdate>20050429</expirationdate>
		///						<expratetype>5238</expratetype>
		///						<state>5</state>
		///						<fixedorprorate>5236</fixedorprorate>
		///						<baserate>12</baserate>
		///						<expraterowid>4</expraterowid> OR <expraterowid></expraterowid>(for new record)
		///					</ExpRates>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		private bool Validate(XmlDocument p_objInputXMLDoc)
		{
			string sExpRateRowId = string.Empty ;
            string sExposureId = string.Empty;
            string sState = string.Empty;
            string sEffectiveDate = string.Empty;
            string sExpirationDate = string.Empty;
            StringBuilder sSQL = new StringBuilder();
			DbReader objDBReader = null;
			DbReader objTempDBReader = null;
			bool bIsReaderHasRows = false;
			XmlElement objExpRatesXMLEle = null;
            //Added bY Tushar MITS 18231
            string sRateMaintType = string.Empty;
            int iRateMaintTypeCodeID = 0;
            XmlNodeList objRateRangeXMLNode=null;
            SortedList slRanges=null; 
            int iEndRange=0 ;
            int  iBegRange=0;
            string sType=string.Empty ;
            int iEndRangePrev = 0;
            int iBegRangeNext = 0;
            string sTmp = string.Empty;
            string sLOB = string.Empty;
            LocalCache objLocalCache = null;
            //End
            try
            {
                objExpRatesXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/ExpRates");

                sExpRateRowId = objExpRatesXMLEle.GetElementsByTagName("expraterowid").Item(0).InnerText;
                //sExposureId = objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).InnerText;
                sExposureId = objExpRatesXMLEle.GetElementsByTagName("exposureid").Item(0).Attributes[0].Value;
                //sState = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).InnerText;
                sState = objExpRatesXMLEle.GetElementsByTagName("state").Item(0).Attributes[0].Value;
                sEffectiveDate = objExpRatesXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
                sExpirationDate = objExpRatesXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
                //Added By Tushar MITS 18231
                sRateMaintType = objExpRatesXMLEle.GetElementsByTagName("RateMaintenanceType").Item(0).InnerText;
                objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
                iRateMaintTypeCodeID = objLocalCache.GetCodeId(sRateMaintType.Trim(), RATE_TYPE_CODE_TABLENAME);
                sLOB = objExpRatesXMLEle.GetElementsByTagName("LINEOFBUSINESS").Item(0).Attributes[0].Value;
                //End


                if (string.IsNullOrEmpty(sExpirationDate))
                {
                    sSQL.Length = 0;
                    // Line of Business condition added By Tushar for MITS 18231
                    sSQL.AppendFormat("SELECT EXP_RATE_ROWID FROM {0} WHERE EXPOSURE_ID = {1} AND STATE = {2} AND",TABLENAME,sExposureId,sState); 
                    sSQL.AppendFormat(" RATE_MAINTENANCE_TYPE = {0} AND LINE_OF_BUSINESS = {1} AND EXPIRATION_DATE IS NULL ",iRateMaintTypeCodeID,sLOB);


                    objDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString());

                    while (objDBReader.Read())
                    {
                        bIsReaderHasRows = true;

                        if (objDBReader["EXP_RATE_ROWID"].ToString() != sExpRateRowId)
                        {
                            switch (sRateMaintType)
                            {
                                case "E":
                                    throw new RMAppException(Globalization.GetString("ExpRateParms.Exposure.Validate.AlreadyExistsWithNoExpDate", m_iClientId));
                                case "C":
                                    throw new RMAppException(Globalization.GetString("ExpRateParms.Coverage.Validate.AlreadyExistsWithNoExpDate", m_iClientId));
                                case "U":
                                    throw new RMAppException(Globalization.GetString("ExpRateParms.UAR.Validate.AlreadyExistsWithNoExpDate", m_iClientId));
                            }

                        }
                        else
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("SELECT COUNT(EXP_RATE_ROWID) FROM {0} WHERE EXPOSURE_ID = {1} AND STATE = {2}", TABLENAME, sExposureId, sState);
                            sSQL.AppendFormat(" AND LINE_OF_BUSINESS = {0} AND EXPIRATION_DATE >= '{1}'",sLOB, Conversion.GetDate(sEffectiveDate));
                            sSQL.AppendFormat(" AND RATE_MAINTENANCE_TYPE = {0}", iRateMaintTypeCodeID);

                            objTempDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString());
                            if (objTempDBReader.Read())
                            {
                                if (Conversion.CastToType<int>(objTempDBReader.GetValue(0).ToString(), out bIsSucess) > 0)
                                {
                                    switch (sRateMaintType)
                                    {
                                        case "E":
                                            throw new RMAppException(Globalization.GetString("ExpRateParms.Exposure.Validate.AlreadyExists", m_iClientId));
                                        case "C":
                                            throw new RMAppException(Globalization.GetString("ExpRateParms.Coverage.Validate.AlreadyExists", m_iClientId));
                                        case "U":
                                            throw new RMAppException(Globalization.GetString("ExpRateParms.UAR.Validate.AlreadyExists", m_iClientId));
                                    }
                                }
                            }
                            objTempDBReader.Close();
                        }
                    }
                    objDBReader.Close();

                    if (bIsReaderHasRows == false)
                    {
                        sSQL.Length = 0;
                        sSQL.AppendFormat("SELECT EXP_RATE_ROWID FROM {0} WHERE EXPOSURE_ID = {1}", TABLENAME, sExposureId);
                        sSQL.AppendFormat(" AND STATE = {0} AND EXPIRATION_DATE >= '{1}'", sState, Conversion.GetDate(sEffectiveDate));

                        objTempDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString());
                        while (objTempDBReader.Read())
                        {
                            if (objTempDBReader["EXP_RATE_ROWID"].ToString() != sExpRateRowId)
                            {
                                switch (sRateMaintType)
                                {
                                    case "E":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.Exposure.Validate.AlreadyExists", m_iClientId));
                                    case "C":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.Coverage.Validate.AlreadyExists", m_iClientId));
                                    case "U":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.UAR.Validate.AlreadyExists", m_iClientId));
                                }
                            }
                        }
                        objTempDBReader.Close();
                    }
                }
                else
                {
                    sSQL.Length = 0;
                    sSQL.AppendFormat("SELECT EXP_RATE_ROWID,EFFECTIVE_DATE FROM {0} WHERE EXPOSURE_ID = {1} AND STATE = {2} AND", TABLENAME, sExposureId, sState);
                    sSQL.AppendFormat(" RATE_MAINTENANCE_TYPE = {0} AND LINE_OF_BUSINESS = {1} AND EXPIRATION_DATE IS NULL ", iRateMaintTypeCodeID, sLOB);


                    objDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString());

                    while (objDBReader.Read())
                    {
                        bIsReaderHasRows = true;

                        if (objDBReader["EXP_RATE_ROWID"].ToString() != sExpRateRowId)
                        {
                            //Start: Neha Suresh Jain,06/08/2010,Added comparision for Expiration Date and Conversion.GetDate function added
                            // if (Conversion.ToDate(sEffectiveDate) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim()))
                            if ((Conversion.ToDate(Conversion.GetDate(sEffectiveDate)) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim())) ||
                                (Conversion.ToDate(Conversion.GetDate(sExpirationDate)) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim())))
                            {
                                switch (sRateMaintType)
                                {
                                    case "E":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.Exposure.Validate.AlreadyExists", m_iClientId));
                                    case "C":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.Coverage.Validate.AlreadyExists", m_iClientId));
                                    case "U":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.UAR.Validate.AlreadyExists", m_iClientId));
                                }
                            }
                            else
                            {
                                sSQL.Length = 0;
                                sSQL.AppendFormat("SELECT EXP_RATE_ROWID FROM {0} WHERE EXPOSURE_ID = {1}",TABLENAME,sExposureId);
                                sSQL.AppendFormat(" AND STATE = {0} AND EXPIRATION_DATE >= '{1}' AND LINE_OF_BUSINESS = {2}",sState,Conversion.GetDate(sEffectiveDate),sLOB);
                                sSQL.AppendFormat(" AND RATE_MAINTENANCE_TYPE = {0} AND EFFECTIVE_DATE <= {1}", iRateMaintTypeCodeID, Conversion.GetDate(sExpirationDate));

                                objTempDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString());
                                while (objTempDBReader.Read())
                                {
                                    if (objTempDBReader["EXP_RATE_ROWID"].ToString() != sExpRateRowId)
                                    {
                                        switch (sRateMaintType)
                                        {
                                            case "E":
                                                throw new RMAppException(Globalization.GetString("ExpRateParms.Exposure.Validate.AlreadyExists", m_iClientId));
                                            case "C":
                                                throw new RMAppException(Globalization.GetString("ExpRateParms.Coverage.Validate.AlreadyExists", m_iClientId));
                                            case "U":
                                                throw new RMAppException(Globalization.GetString("ExpRateParms.UAR.Validate.AlreadyExists", m_iClientId));
                                        }
                                    }
                                }
                                objTempDBReader.Close();
                            }
                        }
                        else
                        {
                            sSQL.Length = 0;
                            sSQL.AppendFormat("SELECT COUNT(EXP_RATE_ROWID) FROM {0} WHERE EXPOSURE_ID = {1} AND STATE = {2}", TABLENAME, sExposureId, sState);
                            sSQL.AppendFormat(" AND LINE_OF_BUSINESS = {0} AND EXPIRATION_DATE >= '{1}'",sLOB, Conversion.GetDate(sEffectiveDate));
                            sSQL.AppendFormat(" AND RATE_MAINTENANCE_TYPE = {0} AND EFFECTIVE_DATE <= {1}", iRateMaintTypeCodeID,Conversion.GetDate(sExpirationDate));

                            objTempDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString());
                            while (objTempDBReader.Read())
                            {
                                if (Conversion.CastToType<int>(objTempDBReader.GetValue(0).ToString(), out bIsSucess) > 0)
                                {
                                    switch (sRateMaintType)
                                    {
                                        case "E":
                                            throw new RMAppException(Globalization.GetString("ExpRateParms.Exposure.Validate.AlreadyExists", m_iClientId));
                                        case "C":
                                            throw new RMAppException(Globalization.GetString("ExpRateParms.Coverage.Validate.AlreadyExists", m_iClientId));
                                        case "U":
                                            throw new RMAppException(Globalization.GetString("ExpRateParms.UAR.Validate.AlreadyExists", m_iClientId));
                                    }
                                }
                            }
                            objTempDBReader.Close();
                        }
                    }
                    objDBReader.Close();

                    if (bIsReaderHasRows == false)
                    {
                        sSQL.Length = 0;
                        sSQL.AppendFormat("SELECT EXP_RATE_ROWID FROM {0} WHERE EXPOSURE_ID = {1} AND STATE = {2}", TABLENAME, sExposureId, sState);
                        //Start:Sumit(09/28/2010)-MITS# 21805
                        sSQL.AppendFormat(" AND EXPIRATION_DATE >= {0} ", Conversion.GetDate(sEffectiveDate));
                        //End:Sumit
                        sSQL.AppendFormat(" AND LINE_OF_BUSINESS = {0} ", sLOB);
                        sSQL.AppendFormat(" AND RATE_MAINTENANCE_TYPE = {0} AND EFFECTIVE_DATE <= {1}", iRateMaintTypeCodeID, Conversion.GetDate(sExpirationDate));

                        objTempDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString());
                        while (objTempDBReader.Read())
                        {
                            if (objTempDBReader["EXP_RATE_ROWID"].ToString() != sExpRateRowId)
                            {
                                switch (sRateMaintType)
                                {
                                    case "E":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.Exposure.Validate.AlreadyExists", m_iClientId));
                                    case "C":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.Coverage.Validate.AlreadyExists", m_iClientId));
                                    case "U":
                                        throw new RMAppException(Globalization.GetString("ExpRateParms.UAR.Validate.AlreadyExists", m_iClientId));
                                }
                            }
                        }
                        objTempDBReader.Close();
                    }
                }
                //Added BY Tushar MITS 18231

                slRanges = new SortedList();
                objRateRangeXMLNode = p_objInputXMLDoc.SelectNodes("//Document/ExpRates/raterangelist/raterange");

                foreach (XmlNode objNode in objRateRangeXMLNode)
                {
                    sType = ((XmlElement)objNode).GetElementsByTagName("rangetype").Item(0).InnerText;
                    sType = sType.Substring(0, 1);
                    if (sType == "R")
                    {

                        sTmp = ((XmlElement)objNode).GetElementsByTagName("beginningrange").Item(0).InnerText;
                        //if (sTmp.IndexOf('%') >= 0)
                        //    sTmp = sTmp.Remove(sTmp.IndexOf('%'));
                        sTmp = Conversion.ConvertStrToDouble(sTmp).ToString();
                        iBegRange = Conversion.CastToType<int>(sTmp, out bIsSucess);


                        sTmp = ((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).InnerText;
                        //if (sTmp.IndexOf('%') >= 0)
                        //    sTmp = sTmp.Remove(sTmp.IndexOf('%'));
                        sTmp = Conversion.ConvertStrToDouble(sTmp).ToString();
                        iEndRange = Conversion.CastToType<int>(sTmp, out bIsSucess);


                        slRanges.Add(iBegRange, iEndRange);
                    }
                }
                for (int iCount = 0; iCount < slRanges.Count - 1; iCount++)
                {
                    iEndRangePrev = Conversion.CastToType<int>(slRanges.GetByIndex(iCount).ToString(), out bIsSucess);
                    iBegRangeNext = Conversion.CastToType<int>(slRanges.GetKey(iCount + 1).ToString(), out bIsSucess);

                    if (iEndRangePrev > iBegRangeNext)
                        throw new RMAppException(Globalization.GetString("ExpRateParms.Validate.RangeExist", m_iClientId));
                }
                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            finally
            {
                if (objDBReader != null)
                    objDBReader.Dispose();
                if (objTempDBReader != null)
                    objTempDBReader.Dispose();
                //Added By Tushar for MITS 18231
                if (slRanges != null)
                    slRanges = null;
                if (objLocalCache != null)
                    objLocalCache = null;
                //End
            }

		}

	}


	/// <summary>
	///Author	:   Anurag Agarwal
	///Class	:	ExposureRate
	///Dated	:   06 April 2005
	///Purpose	:   This class works as a container class for "SYS_POL_EXP_RATES" table
	/// </summary>
	public class ExposureRate
	{
        int m_iClientId = 0;
		public ExposureRate(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Stores the ExpRateRowId value
		/// </summary>
		private int m_iExpRateRowId = 0;

		/// <summary>
		/// Stores the ExposureId value
		/// </summary>
		private int m_iExposureId = 0;

		/// <summary>
		/// Stores the State value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the Amount value
		/// </summary>
		private double m_dAmount = 0.0;

		/// <summary>
		/// Stores the ExpRateType value
		/// </summary>
		private int m_iExpRateType = 0;

		/// <summary>
		/// Stores the BaseRate value
		/// </summary>
		private double m_dBaseRate = 0.0;

		/// <summary>
		/// Stores the EffectiveDate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the ExpirationDate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the FixedOrProRate value
		/// </summary>
		private int m_iFixedOrProRate = 0;

        /// <summary>
        /// Stores the Range value MITS 18231
        /// </summary>
        private double m_dRange = 0.0;

        /// <summary>
        /// Stores the RateRangeList value MITS 18231
        /// </summary>
        private RateRangeList m_objRateRangeList = null;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

        /// <summary>
        /// Stores the RateRowid value
        /// MITS 18231
        /// </summary>
        private int m_iRateRowId = 0;
        /// <summary>
        /// Stores the ConnectionString value
        /// MITS 18231
        /// </summary>
        private string  m_sConnectString = string.Empty ;
		/// <summary>
		/// Read/Write property for ExpRateRowId
		/// </summary>
		internal int ExpRateRowId
		{
			get
			{
				return m_iExpRateRowId;
			}
			set
			{
				m_iExpRateRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExposureId
		/// </summary>
		internal int ExposureId
		{
			get
			{
				return m_iExposureId;
			}
			set
			{
				m_iExposureId = value;
			}
		}

        /// <summary>
        /// Added By Tushar Agarwal for VACo
        /// Read/Write property for Org-Hierarchy
        /// MITS 18231
        /// </summary>
        internal int OrgHierarchy
        {
            get;
            set;
        }
		/// <summary>
		/// Read/Write property for State
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for Amount
		/// </summary>
		internal double Amount
		{
			get
			{
				return m_dAmount;
			}
			set
			{
				m_dAmount = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpRateType
		/// </summary>
		internal int ExpRateType
		{
			get
			{
				return m_iExpRateType;
			}
			set
			{
				m_iExpRateType = value;
			}
		}

		/// <summary>
		/// Read/Write property for BaseRate
		/// </summary>
		internal double BaseRate
		{
			get
			{
				return m_dBaseRate;
			}
			set
			{
				m_dBaseRate = value;
			}
		}

		/// <summary>
		/// Read/Write property for EffectiveDate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpirationDate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for FixedOrProRate
		/// </summary>
		internal int FixedOrProRate
		{
			get
			{
				return m_iFixedOrProRate;
			}
			set
			{
				m_iFixedOrProRate = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
        ///<summary>
        ///Added By Tushar to Read/Write Line of Business
        ///MITS 18231
        ///</Summary>
        internal Int32 LineOFBusiness
        {
            get;

            set;
            
        }

        ///<summary>
        ///Added By Tushar to Read/Write Rate Maintenance Type
        ///MITS 18231
        ///</Summary>
        internal Int32 RateMaintenanceType
        {
            get;

            set;

        }

        /// <summary>
        /// Read/Write property for Range
        /// MITS 18231
        /// </summary>
        internal double Range
        {
            get
            {
                return m_dRange;
            }
            set
            {
                m_dRange = value;
            }
        }
        /// <summary>
        /// Read/Write property for RateRowid
        /// MITS 18231
        /// </summary>
        internal int RateRowId
        {
            get
            {
                return m_iRateRowId ;
            }
            set
            {
                m_iRateRowId = value;
            }
        }
        /// <summary>
        /// Read/Write property for ConnectionString
        /// MITS 18231
        /// </summary>
        internal string ConnectionString
        {
            get
            {
                return m_sConnectString;
            }
            set
            {
                m_sConnectString = value;
            }
        }
        /// <summary>
        /// Read property for RateRangeList
        /// MITS 18231
        /// </summary>
        internal RateRangeList RateRangeList
        {
            get
            {
                if (m_objRateRangeList == null)
                {
                    m_objRateRangeList = new RateRangeList(m_sConnectString, m_iClientId);
                    m_objRateRangeList.Parent = this;
                    m_objRateRangeList.RateRowid = m_iRateRowId;
                    if (m_iRateRowId != 0)
                        m_objRateRangeList.LoadData();
                }
                return m_objRateRangeList;
            }
        }
	}
}
