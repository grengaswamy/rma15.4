using System;
using System.Xml;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 


namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Tushar Agarwal
	///Dated   :   09, Nov 2009
	///Purpose :   Container class for rate range
    ///MITS    :   18231 
	/// </summary>
	public class RateRange
	{
        public RateRange()
		{
		}
        

		/// <summary>
		/// Stores the pParent value
		/// </summary>
		private RateRangeList m_objParent = null;

		/// <summary>
		/// Read/Write property for RateRangeRowId
		/// </summary>
        internal int RangeRowId
        {
            get;
            set;
        }

		/// <summary>
		/// Read/Write property for RateRowid
		/// </summary>
		internal int RateRowid
		{
			get;
			set;
			
		}

		/// <summary>
		/// Read/Write property for Modifier
		/// </summary>
		internal double Modifier
		{
			get;
			set;
		}

        /// <summary>
        /// Read/Write property for Deductible
        /// </summary>
        internal double Deductible
        {
            get;
            set;
            
        }

		/// <summary>
		/// Read/Write property for BeginningRange
		/// </summary>
        internal string BeginningRange
        {
            get;
            set;
            
        }

		/// <summary>
		/// Read/Write property for EndRange
		/// </summary>
        internal string EndRange
        {
            get;
            set;
        }

        /// <summary>
        /// Read/Write property for Type
        /// </summary>
        internal string RangeType
        {
            get;
            set;
        }

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
        internal string AddedByUser
        {
            get;
            set;
            
        }

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
        internal string DttmRcdAdded
        {
            get;
            set;
        }

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
        internal string UpdatedByUser
        {
            get;
            set;
        }

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
        internal string DttmRcdLastUpd
        {
            get;
            set;
            
        }

		/// <summary>
		/// Read/Write property for NewRecord
		/// </summary>
        internal bool NewRecord
        {
            get;
            set;
           
        }

		/// <summary>
		/// Read/Write property for pParent
		/// </summary>
		internal RateRangeList Parent
		{
			//IMPORTANT DO NO SET PARENT PROPERTY FROM PARENT'S OBJECT INITIALIZE EVENT !!!!!!
			get
			{
				return m_objParent ;
			}
			set
			{
				m_objParent = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
        internal bool DataChanged
        {
            get;
            set;
           
        }

		public void Clear()
		{
            


            AddedByUser  = string.Empty;
			DataChanged  = false;
            DttmRcdLastUpd  = string.Empty;
            DttmRcdAdded  = string.Empty;
            AddedByUser  = string.Empty;
			Modifier  = 0;
            Deductible  = 0;
            BeginningRange  = string.Empty;
			RangeRowId  = 0;
			RateRowid  = 0;
            EndRange  = string.Empty;
            RangeType  = string.Empty;
			NewRecord  = false;
		}
	}
}
