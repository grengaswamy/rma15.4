using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.AdminTracking; 

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Shivendu Shekhar
    ///Dated   :   27 Aug 2007
    ///Purpose :   This class is responsible for Getting and Setting the Grid Parameters
    /// </summary>
    public class SupplementalGridParameterSetup
    {
        #region Private variables
        private string m_sConnString = "";
        private string m_sUserId = "";
        private string m_sPassword = "";
        private string m_sDSNName = "";
        private int m_iClientId=0;
        #endregion

        #region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserId">User Id</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDSNName">DSN Name</param>
		/// <param name="p_sConnString">Connection String</param>
        public SupplementalGridParameterSetup(string p_sUserId, string p_sPassword, string p_sDSNName, string p_sConnString, int p_iClientId)
		{
			m_sDSNName = p_sDSNName;
			m_sUserId = p_sUserId;
			m_sPassword = p_sPassword;
			m_sConnString = p_sConnString;
            m_iClientId = p_iClientId;
		}

		#endregion
        #region Public Functions
        /// Name		: Get
        /// Author		: Shivendu Shekhar
        /// Date Created: 07/27/2007		
        /// <summary>
        /// Retrieves data to load on to the screen
        /// </summary>
        /// <param name="p_objXMLDocument">Blank XML structure for the View UI</param>
        /// <returns>XmlDocument containing data to be shown on screen</returns>
        public XmlDocument Get(XmlDocument p_objXMLDocument)
        {
            DbConnection objDbConnection = null;
            StringBuilder sbSql = null;
            DbReader objReader = null;
          
           try
            {
                sbSql = new StringBuilder();

                
                    sbSql.Append("SELECT * FROM GRID_SYS_PARMS");
                    objReader = DbFactory.GetDbReader(m_sConnString, sbSql.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MinRows").InnerText = objReader["MIN_ROWS"].ToString();
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MaxRows").InnerText = objReader["MAX_ROWS"].ToString();
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MinCols").InnerText = objReader["MIN_COLS"].ToString();
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MaxCols").InnerText = objReader["MAX_COLS"].ToString();
                            
                           
                        }
                        else
                        {
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MinRows").InnerText = "1";
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MaxRows").InnerText = "1";
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MinCols").InnerText = "1";
                            p_objXMLDocument.SelectSingleNode("/GridParameters/MaxCols").InnerText = "1";
                            sbSql.Remove(0, sbSql.Length);
                            sbSql.Append("INSERT INTO GRID_SYS_PARMS VALUES ('1','1','1','1')");
                            objDbConnection = DbFactory.GetDbConnection(m_sConnString);
                            if (objDbConnection != null)
                            {
                                objDbConnection.Open();
                                objDbConnection.ExecuteNonQuery(sbSql.ToString());
                                objDbConnection.Close();
                            }
                        }
                        if (objReader != null)
                        {
                            objReader.Dispose();
                        }
                    }
               
                   

                

                return p_objXMLDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SupplementalGridParameterSetup.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                    objReader = null;
                }
                p_objXMLDocument = null;
                if (objDbConnection != null)
                    objDbConnection.Dispose();
                

            }
        }
        /// Name		: Save
        /// Author		: Shivendu Shekhar
        /// Date Created: 05/02/2005		
        /// <summary>
        /// Saves the selected View information into the database
        /// </summary>
        /// <param name="p_objXMLDocument">XmlDocument containing Updated or new view info</param>
        /// <returns>boolean for success/failure</returns>
        public bool Save(XmlDocument p_objXMLDocument)
        {
        
            //DbConnection objDbConnection = null;
            StringBuilder sbSql = null;
            DbReader objReader = null;
            string sRowToUpdate = null;
            //Added by bsharma33 for Regions Pan Testing MITS 26944
            DbWriter writer = null;
            //End additions by bsharma33
            try
            {
                sbSql = new StringBuilder();
                //Added by bsharma33 for Regions Pan Testing MITS 26944
                writer = DbFactory.GetDbWriter(m_sConnString);
                writer.Tables.Add("GRID_SYS_PARMS");
                writer.Fields.Add("MIN_ROWS", p_objXMLDocument.SelectSingleNode("/GridParameters/MinRows").InnerText);
                writer.Fields.Add("MAX_ROWS", p_objXMLDocument.SelectSingleNode("/GridParameters/MaxRows").InnerText);
                writer.Fields.Add("MIN_COLS", p_objXMLDocument.SelectSingleNode("/GridParameters/MinCols").InnerText);
                writer.Fields.Add("MAX_COLS", p_objXMLDocument.SelectSingleNode("/GridParameters/MaxCols").InnerText);
                //End additions by bsharma33
                sbSql.Append("SELECT * FROM GRID_SYS_PARMS");
                objReader = DbFactory.GetDbReader(m_sConnString, sbSql.ToString());
                if (objReader != null)
                {
                   if (objReader.Read())
                    {
                        //Changed by bsharma33 for Regions Pan Testing MITS 26944
                        sRowToUpdate = objReader["MIN_ROWS"].ToString();
                        writer.Where.Add(string.Format(" MIN_ROWS = {0}", sRowToUpdate));
                        //sbSql.Remove(0, sbSql.Length);
                        //sbSql.Append("UPDATE GRID_SYS_PARMS SET MIN_ROWS = " + p_objXMLDocument.SelectSingleNode("/GridParameters/MinRows").InnerText + ",");
                        //sbSql.Append("MAX_ROWS = " + p_objXMLDocument.SelectSingleNode("/GridParameters/MaxRows").InnerText + ",");
                        //sbSql.Append("MIN_COLS = " + p_objXMLDocument.SelectSingleNode("/GridParameters/MinCols").InnerText + ",");
                        //sbSql.Append("MAX_COLS = " + p_objXMLDocument.SelectSingleNode("/GridParameters/MaxCols").InnerText);
                        //sbSql.Append("WHERE MIN_ROWS = " + sRowToUpdate);
                        //objDbConnection = DbFactory.GetDbConnection(m_sConnString);
                        //if (objDbConnection != null)
                          //{
                           
                            //objDbConnection.Open();
                            //objDbConnection.ExecuteNonQuery(sbSql.ToString());
                            //objDbConnection.Close();
                          //}
                    }
                    //if (objReader != null)
                    //{
                    //    objReader.Dispose();
                    //}
                }
                
               // else
                //{
                    //sbSql.Remove(0, sbSql.Length);
                    //sbSql.Append("INSERT INTO GRID_SYS_PARMS(MIN_ROWS,MAX_ROWS,MIN_COLS,MAX_COLS) VALUES (" + p_objXMLDocument.SelectSingleNode("/GridParameters/MinRows").InnerText + ",");
                    //sbSql.Append(p_objXMLDocument.SelectSingleNode("/GridParameters/MaxRows").InnerText + ",");
                    //sbSql.Append(p_objXMLDocument.SelectSingleNode("/GridParameters/MinCols").InnerText + ",");
                    //sbSql.Append(p_objXMLDocument.SelectSingleNode("/GridParameters/MaxCols").InnerText + ")");
                    //objDbConnection = DbFactory.GetDbConnection(m_sConnString);
                    //if (objDbConnection != null)
                    //{
                    //    objDbConnection.Open();
                    //    objDbConnection.ExecuteNonQuery(sbSql.ToString());
                    //    objDbConnection.Close();
                    //}
                   
               // }
                //End changes by bsharma33 for Regions Pan Testing MITS 26944
                //Added by bsharma33 
                if (writer.Fields.Count > 0)
                {
                    writer.Execute();
                }
                //End additions by bsharma33
                return true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SupplementalGridParameterSetup.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                    objReader = null;
                }
                p_objXMLDocument = null;
                //Changed by bsharma33 for MITS 26944, Regions Pan testing --- objDbConnection not beibg used now.
                //if (objDbConnection != null)
                //    objDbConnection.Dispose();
                writer = null;
                //End changes by bsharma33
                

            }
        }
       #endregion
    }
}
