﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Db;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using System.Collections;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
  public   class LimitTrackingSetup
    {
      string m_sConnstr = string.Empty;
      int m_iClientId = 0;
      public LimitTrackingSetup(string p_sConnstr,int p_iClientId)
      {
          m_sConnstr = p_sConnstr;
          m_iClientId = p_iClientId;
      }
      public XmlDocument LoadData(XmlDocument p_inXML)
      {
          string sSQL = string.Empty;
          LocalCache objCache = null;
          XmlElement objElement = null;
          XmlNode objNode = null;
          XmlNode objApproverNode = null;
          XmlAttribute objAttribute = null;

          string sLimitUserIdStr = string.Empty;
          string sLimitUserStr = string.Empty;
          string shdnLimitUserStr = string.Empty;
          string shdnLimitGroupStr = string.Empty;
          

         
           int iUserGroupId=0;
           string sUserGroupName = string.Empty;
          // string iUserId = 0;
           bool bIsGroup = false;
           XmlNode objTempNode = null;
           try
           {



               if (p_inXML.SelectSingleNode("//PolicyLimitExceeded") != null)
               {
                   p_inXML.SelectSingleNode("//PolicyLimitExceeded").InnerText = Conversion.ConvertObjToBool(DbFactory.ExecuteScalar(m_sConnstr, "SELECT STR_PARM_VALUE FROM  PARMS_NAME_VALUE WHERE PARM_NAME='CHECK_POLICY_LIMIT'"), m_iClientId).ToString();

               }



               sSQL = "SELECT * FROM LIMIT_TRACKING_SETUP ";

               objCache = new LocalCache(m_sConnstr, m_iClientId);


               using (DbReader objRdr = DbFactory.GetDbReader(m_sConnstr, sSQL))
               {
                   while (objRdr.Read())
                   {
                       sUserGroupName = string.Empty;
                       iUserGroupId = 0;

                       if (Conversion.ConvertObjToInt(objRdr.GetValue("GROUP_ID"), m_iClientId) > 0)
                       {
                           iUserGroupId = Conversion.ConvertObjToInt(objRdr.GetValue("GROUP_ID"), m_iClientId);
                           sUserGroupName = GetGroupName(iUserGroupId);
                           bIsGroup = true;
                       }
                       else if (Conversion.ConvertObjToInt(objRdr.GetValue("USER_ID"), m_iClientId) > 0)
                       {
                           iUserGroupId = Conversion.ConvertObjToInt(objRdr.GetValue("USER_ID"), m_iClientId);
                           sUserGroupName = GetUserName(iUserGroupId);
                           bIsGroup = false;
                       }


                               objNode = p_inXML.SelectSingleNode("//PolicyLimit");

                               if (string.IsNullOrEmpty(sLimitUserIdStr))
                               {
                                   if (bIsGroup)
                                   {
                                       sLimitUserIdStr = "G" + iUserGroupId.ToString();
                                   }
                                   else
                                   {
                                       sLimitUserIdStr = "U" + iUserGroupId.ToString();
                                   }
                               }
                               else
                               {

                                   if (bIsGroup)
                                   {
                                       sLimitUserIdStr = sLimitUserIdStr + " G" + iUserGroupId.ToString();
                                   }
                                   else
                                   {
                                       sLimitUserIdStr = sLimitUserIdStr + " U" + iUserGroupId.ToString();
                                   }

                               }
                               if (string.IsNullOrEmpty(sLimitUserStr))
                               {
                                   sLimitUserStr = sUserGroupName;
                               }
                               else
                               {
                                   sLimitUserStr = sLimitUserStr + " " + sUserGroupName;
                               }

                               if (!bIsGroup)
                               {
                                   if (string.IsNullOrEmpty(shdnLimitUserStr))
                                   {
                                       shdnLimitUserStr = "U"+iUserGroupId.ToString();
                                   }
                                   else
                                   {
                                       shdnLimitUserStr = shdnLimitUserStr + ",U" + iUserGroupId;
                                   }
                               }
                               else
                               {
                                   if (string.IsNullOrEmpty(shdnLimitGroupStr))
                                   {
                                       shdnLimitGroupStr = "G"+iUserGroupId.ToString();
                                   }
                                   else
                                   {
                                       shdnLimitGroupStr = shdnLimitGroupStr + ",G" + iUserGroupId;
                                   }


                               }

                            


                       objApproverNode = objNode.SelectSingleNode("ApproverList");
                       objElement = p_inXML.CreateElement("Option");
                       objAttribute = p_inXML.CreateAttribute("DisplayName");
                       //objAttribute.Value = "test";

                       objAttribute.Value = sUserGroupName;



                       objElement.Attributes.Append(objAttribute);

                       objAttribute = null;


                       if (bIsGroup)
                       {
                           objAttribute = p_inXML.CreateAttribute("GROUP_ID");
                           objAttribute.Value = "G"+iUserGroupId.ToString();
                           objElement.Attributes.Append(objAttribute);
                           objAttribute = null;

                           objAttribute = p_inXML.CreateAttribute("USER_ID");
                           objAttribute.Value = "0";
                           objElement.Attributes.Append(objAttribute);
                       }
                       else
                       {


                           objAttribute = p_inXML.CreateAttribute("GROUP_ID");
                           objAttribute.Value = "0";
                           objElement.Attributes.Append(objAttribute);
                           objAttribute = null;

                           objAttribute = p_inXML.CreateAttribute("USER_ID");
                           objAttribute.Value ="U"+ iUserGroupId.ToString();

                           objElement.Attributes.Append(objAttribute);
                       }



                       objApproverNode.AppendChild(objElement);
                       objElement = null;

                   }
               }



               objTempNode = p_inXML.SelectSingleNode("//PolicyLimit//UserIdStr");
               if (objTempNode != null)
               {
                   objTempNode.InnerText = sLimitUserIdStr;
               }

               objTempNode = p_inXML.SelectSingleNode("//PolicyLimit//UserStr");
               if (objTempNode != null)
               {
                   objTempNode.InnerText = sLimitUserStr;
               }
               objTempNode = p_inXML.SelectSingleNode("//PolicyLimit//hdnUserStr");
               if (objTempNode != null)
               {
                   objTempNode.InnerText = shdnLimitUserStr;
               }

               objTempNode = p_inXML.SelectSingleNode("//PolicyLimit//hdnGroupStr");
               if (objTempNode != null)
               {
                   objTempNode.InnerText = shdnLimitGroupStr;
               }



           }
           catch (RMAppException rmexp)
           {
               throw rmexp;
           }
           catch (Exception e)
           {
               throw new RMAppException(Globalization.GetString("LimitTrackingSetup.LoadData", m_iClientId));//sonali
           }
           finally
           {
               if (objCache != null)
               {
                   objCache.Dispose();
               }
           }
         return p_inXML;
      }
      private string GetGroupName(int iGroupID)
      {
          string sUserName = string.Empty;

          try
          {
              sUserName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnstr, "SELECT GROUP_NAME FROM  USER_GROUPS WHERE GROUP_ID=" + iGroupID));
          }
          catch (Exception e)
          {
              throw new RMAppException(Globalization.GetString("LimitTrackingSetup.GetGroupName", m_iClientId));//sonali
          }
          return sUserName;
      }

      private string GetUserName(int iUserId)
      {
          string sUserName = string.Empty;

          try
          {
              sUserName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(SecurityDatabase.Dsn, "SELECT DISTINCT  LOGIN_NAME FROM  USER_DETAILS_TABLE WHERE USER_ID=" + iUserId));
          }
          catch (Exception e)
          {
              throw new RMAppException(Globalization.GetString("LimitTrackingSetup.GetUserName", m_iClientId));//sonali
          }
          return sUserName;
      }

      public void SaveData(XmlDocument p_objXMlDoc)
      {
          string sSQL = string.Empty;
          LocalCache objCache = null;
          XmlElement objElement = null;
        //  XmlNode objNode = null;
          //XmlNode objApproverNode = null;
          //XmlAttribute objAttribute = null;
          DbConnection objConn = null;
          DbWriter objWriter = null;
          ArrayList objPolicyLimitIds = new ArrayList();

          
          try
          {


              objWriter = DbFactory.GetDbWriter(m_sConnstr);

              objWriter.Tables.Add("PARMS_NAME_VALUE");

              if ((p_objXMlDoc.SelectSingleNode("//PolicyLimitExceeded") != null) && (string.Equals(p_objXMlDoc.SelectSingleNode("//PolicyLimitExceeded").InnerText, "True", StringComparison.InvariantCultureIgnoreCase)))
              {

                  objWriter.Fields.Add("STR_PARM_VALUE","-1");
              }
              else
              {
                  objWriter.Fields.Add("STR_PARM_VALUE", "0");
              }
              objWriter.Where.Add(" PARM_NAME='CHECK_POLICY_LIMIT'");

              objWriter.Execute();
              objWriter = null;

              sSQL = "DELETE FROM LIMIT_TRACKING_SETUP";


              objConn = DbFactory.GetDbConnection(m_sConnstr);
              objConn.Open();

              objConn.ExecuteNonQuery(sSQL);
              objConn.Close();

              objCache = new LocalCache(m_sConnstr, m_iClientId);

              foreach (XmlNode objNode in p_objXMlDoc.SelectNodes("//GroupIds"))
              {

                  foreach (string sGroupId in objNode.InnerText.Split(','))
                  {
                      if (string.IsNullOrEmpty(sGroupId))
                      {
                          continue;
                      }
                      objWriter = DbFactory.GetDbWriter(m_sConnstr);

                      objWriter.Tables.Add("LIMIT_TRACKING_SETUP");

                     
                              if (objPolicyLimitIds.Contains(sGroupId.Substring(1)))
                              {
                                  continue;
                              }
                              else
                              {
                                  objPolicyLimitIds.Add(sGroupId.Substring(1));
                              }
                              objWriter.Fields.Add("LIMIT_TYPE_CODE", objCache.GetCodeId("COVOL", "LIMIT_TYPE"));
                          

                      //if (Conversion.ConvertStrToInteger(objNode.Attributes["GROUP_ID"].Value) > 0)
                      //{
                          objWriter.Fields.Add("GROUP_ID", sGroupId.Substring(1));
                          objWriter.Fields.Add("USER_ID", "0");
//                      }


                          objWriter.Fields.Add("ROW_ID", Utilities.GetNextUID(m_sConnstr, "LIMIT_TRACKING_SETUP", m_iClientId));

                      objWriter.Execute();
                      objWriter = null;
                  }
              }
               objPolicyLimitIds = new ArrayList();

              

              foreach (XmlNode objNode in p_objXMlDoc.SelectNodes("//UserIds"))
              {
                  foreach (string sUserid in objNode.InnerText.Split(','))
                  {
                      if (string.IsNullOrEmpty(sUserid))
                      {
                          continue;
                      }

                      objWriter = DbFactory.GetDbWriter(m_sConnstr);

                      objWriter.Tables.Add("LIMIT_TRACKING_SETUP");

                   
                              if (objPolicyLimitIds.Contains(sUserid.Substring(1)))
                              {
                                  continue;
                              }
                              else
                              {
                                  objPolicyLimitIds.Add(sUserid.Substring(1));
                              }
                              objWriter.Fields.Add("LIMIT_TYPE_CODE", objCache.GetCodeId("COVOL", "LIMIT_TYPE"));
                          

                      //if (Conversion.ConvertStrToInteger(objNode.Attributes["GROUP_ID"].Value) > 0)
                      //{
                          objWriter.Fields.Add("GROUP_ID", "0");
                          objWriter.Fields.Add("USER_ID", sUserid.Substring(1));
                      //}

                          objWriter.Fields.Add("ROW_ID", Utilities.GetNextUID(m_sConnstr, "LIMIT_TRACKING_SETUP",m_iClientId));
                      objWriter.Execute();
                      objWriter = null;
                  }
              }
          }
          catch (Exception e)
          {
              throw new RMAppException(Globalization.GetString("LimitTrackingSetup.SaveData", m_iClientId));//sonali
          }
          finally
          {
              objWriter = null;
              if (objCache != null)
              {
                  objCache.Dispose();
                  objCache = null;
              }
              if (objConn != null)
              {

                  objConn.Dispose();
                  objConn = null;
              }
          }
      }
    }
}
