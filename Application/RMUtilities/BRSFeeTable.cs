using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Db; 
using System.IO; 


namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   29th,Apr 2005
	///Purpose :   BRS Fee Table List Form 
	/// </summary>
	public class BRSFeeTable:UtilitiesUIListBase  
	{
		/// <summary>
		/// String array for database field mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "TABLE_ID"},
			{"UserTableName","USER_TABLE_NAME"},
			{"CalcType","CALC_TYPE"},
			{"StartDt","START_DATE"},
			{"TableType","TABLE_TYPE"},
			{"CalcTypeCode","CALC_TYPE"},
			{"OdbcName","ODBC_NAME"},
			{"HiddenUserTableName","USER_TABLE_NAME"}
			};

        int m_iClientId = 0;//Add by kuladeep for Cloud.

		/// Name		: BRSFeeTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/29/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public BRSFeeTable(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;//Add by kuladeep for Cloud.
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/29/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize private variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "FEE_TABLES ";
			base.KeyField = "TABLE_ID";
			base.RadioName = "BRSFeeTableList";
			base.WhereClause = "1=1";
			base.OrderByClause = " PRIORITY, START_DATE DESC";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/29/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the data for the form with xml structure
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with structure</param>
		/// <returns>Xml document with data and structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			//XmlElement objNewElm=null;
			DbConnection objConn=null;
			XmlNodeList objNodLst = null;
			try
			{
				XMLDoc = p_objXmlDocument;
                //MITS 11331  : Umesh
                //override the where condition 
                base.WhereClause = "TABLE_ID > 0";
                //End
				base.Get();
				objNodLst = p_objXmlDocument.SelectNodes("//listrow//CalcType");
				foreach(XmlElement objElm in objNodLst)
				{
						objElm.InnerText = UTILITY.BRSDatabaseType[Conversion.ConvertStrToInteger(objElm.InnerText)];

				}

				objConn=DbFactory.GetDbConnection(ConnectString);
				objConn.Open(); 
				objNodLst = p_objXmlDocument.SelectNodes("//listrow//BrsModCount");
				foreach(XmlElement objElm in objNodLst)
				{
					objElm.InnerText = objConn.ExecuteScalar("SELECT COUNT(*) FROM BRS_MOD_VALUES WHERE FEETABLE_ID = " + objElm.ParentNode.FirstChild.InnerText).ToString();
				}

				objNodLst = p_objXmlDocument.SelectNodes("//listrow//OdbcCount");
				foreach(XmlElement objElm in objNodLst)
				{
					objElm.InnerText = objConn.ExecuteInt("SELECT COUNT(*) FROM FEE_TABLES WHERE ODBC_NAME = " + objElm.ParentNode.SelectSingleNode("//OdbcName").ToString()).ToString() ;
				}
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFeeTable.Get.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objNodLst = null;
				if(objConn!=null)
				{
					objConn.Dispose();
				}
			}
		}

		/// Name		: Delete
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/29/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes a record from the fee table list
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document</param>
		/// <returns>Xml document without the deleted record</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDoc)
		{
			string sSQL="";
			string sTableName="";
			string sODBCFeeTable="";
			string sFeeTablePath="";
			XmlElement objNod=null;
			DbReader objRdr=null;
			DbConnection objCon=null;			
			try
			{
				XMLDoc = p_objXmlDoc;
				objNod = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='UserTableName']");
				if(objNod==null)
					throw new RMAppException(Globalization.GetString("BRSFeeTable.Delete.NoDelete", m_iClientId));
				
				sTableName = objNod.InnerText; 

				sSQL = "SELECT ODBC_NAME, DB_PATH FROM BRS_FEESCHD, FEE_TABLES WHERE USER_TABLE_NAME = '" + 
					  sTableName + "' AND ODBC_NAME = FEESCHD_NAME";

				objRdr = DbFactory.GetDbReader( ConnectString,sSQL);
				if(objRdr.Read())
				{
					sODBCFeeTable = objRdr.GetString("ODBC_NAME");
					sFeeTablePath = objRdr.GetString("DB_PATH");
				}
				objRdr.Dispose();
				objNod = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='RowId']");
				base.KeyFieldValue = objNod.InnerText; 
				base.Delete();
				sSQL = "DELETE FROM BRS_FEESCHD WHERE FEESCHD_NAME = '" + sODBCFeeTable + "'";
				objCon = DbFactory.GetDbConnection(ConnectString);
				objCon.Open(); 
				objCon.ExecuteNonQuery(sSQL);
				if (File.Exists(sFeeTablePath))
					File.Delete(sFeeTablePath);
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFeeTable.Delete.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objNod=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
				if(objCon!=null)
				{
					objCon.Dispose();
				}
			}
		}
	}
}