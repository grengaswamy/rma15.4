using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   Implements the Claim Type Change Options form
	/// </summary>
	public class ClaimTypeChangeOptions:UtilitiesUIListBase 
	{

		/// <summary>
		/// Database field array
		/// </summary>
		private string[,] arrFields = {
			{"Lob","LOB"},
			{"CurClaimType","SOURCE_CLAIM_TYPE"},
			{"NewClaimType","DEST_CLAIM_TYPE"},
			{"TrigType","TRIG_TRANS_TYPE"},
            {"IncSchChecks","INC_SCHEDULED_CHECKS"}
									  };

        private int m_iClientId = 0; //rkaur27

		/// Name		: ClaimTypeChangeOptions
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="p_sConnString">Input connection string</param>
		public ClaimTypeChangeOptions(string p_sConnString, int p_iClientId):base(p_sConnString, p_iClientId) //rkaur27
		{
			ConnectString = p_sConnString;
			this.Initialize();
            m_iClientId = p_iClientId;//rkaur27
        }

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the variables
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "CL_CHG_OPTIONS";
			base.KeyField = "CLCHGOPT_ROW_ID";
			base.RadioName = "ClaimList";
			base.WhereClause = "1=1";
			base.OrderByClause = " CLCHGOPT_ROW_ID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default get method.Returns the data for the form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with structure</param>
		/// <returns>Xml structure and output data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNodeList objNodLst = null;
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				objNodLst = p_objXmlDocument.GetElementsByTagName("rowtext");
				foreach(XmlElement objElm in objNodLst)
				{
					objElm.SetAttribute("title", UTILITY.GetCode(objElm.GetAttribute("title"),base.ConnectString,m_iClientId)); 
				}
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimTypeChangeOptions.Get.Err", m_iClientId),p_objEx);
			}
			finally
			{
				objNodLst = null;
			}
		}
	}
}