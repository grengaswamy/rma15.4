using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Security;


namespace Riskmaster.Application.RMUtilities
{
    ///<summary>
    ///Author  :   Shruti
    ///Dated   :   08/22/2007
    ///Purpose :   This class Creates, Edits Max Rate information.
    /// </summary>
    public class FLMaxRate : UtilitiesUIBase
    {

        #region Private Variables
        /// <summary>
        /// User name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        ///  Fields mapping with Database fields
        /// </summary>
        private string[,] arrFields = {
		{"Year", "MAX_YEAR_TEXT"},
		{"MaxRate", "MAX_RATE_AMT"},
        {"MaxRateId","MAX_WC_RATE_ID"}
									  };

        /// <summary>
        /// Connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Xml document object
        /// </summary>
        private XmlDocument m_objXMLDocument = null;

        private int m_iClientId = 0;

        /// <summary>
        /// Key field value
        /// </summary>
        public string KeyFieldValue
        {
            get
            {
                return Conversion.ConvertObjToStr(base.KeyFieldValue);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>
        public FLMaxRate(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            try
            {
                ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
                m_sConnectionString = p_sConnString;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRate.Constructor.Error", m_iClientId), p_objEx);
            }

        }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="p_sLoginName">Login Name</param>
        /// <param name="p_sConnectionString">Connection String</param>
        public FLMaxRate(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId)
        {
            try
            {
                m_sUserName = p_sLoginName;
                ConnectString = p_sConnectionString;
                m_iClientId = p_iClientId;
                m_sConnectionString = p_sConnectionString;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRate.Constructor.Error", m_iClientId), p_objEx);
            }
        }
        #endregion

        #region Public Functions
        /// Name		: Get
        /// Author		: Shruti
        /// Date Created: 08/22/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>Xml containing the data to be populated on screen</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            string sValue = "";
            try
            {
                XMLDoc = p_objXmlDocument;
                base.Get();
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Year']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        sValue = objNode.InnerText;
                        ((XmlElement)objNode).SetAttribute("value", sValue);
                    }
                }
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='MaxRate']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        sValue = objNode.InnerText;
                        ((XmlElement)objNode).SetAttribute("value", sValue);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRate.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
            return XMLDoc;
        }
        /// Name		: Save
        /// Author		: Shruti
        /// Date Created: 08/22/2007	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the data to the database
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public bool Save(XmlDocument p_objXmlDocument)
        {
            bool bReturnValue = false;
            //string sSql = ""; // commentes as not in use
            DbReader objReader = null;
            //Added by bsharma33 for PenTesting MITS 26942 
            System.Collections.Generic.Dictionary<string, string> dictParams = new System.Collections.Generic.Dictionary<string, string>();
            //DbCommand m_objDbCommand = null;
           System.Text.StringBuilder sbSQL = null;
            //DbConnection objDbConnection = null;
            //End Changes by bsharma33 for PenTesting MITS 26942
            try
            {
                sbSQL = new System.Text.StringBuilder();
                XMLDoc = p_objXmlDocument;
                m_objXMLDocument = p_objXmlDocument;
                //MGaba2:MITS 16188:Uncommenting the condition and adding else part
                //Error in case of changing value of rate during edit
                if (m_objXMLDocument.SelectSingleNode("//control[@name='MaxRateId']").InnerText.Trim() == "-1") //Same Year Check should be both for New and Edit.
                {
                    //Changed by bsharma33 for PenTesting MITS 26942
                    //sSql = "SELECT * FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT = " + m_objXMLDocument.SelectSingleNode("//control[@name='Year']").InnerText.Trim();
                    sbSQL.Append(string.Format("SELECT * FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT ={0} ", "~MAX_YEAR_TEXT~"));
                    dictParams.Add("MAX_YEAR_TEXT", m_objXMLDocument.SelectSingleNode("//control[@name='Year']").InnerText.Trim());
                    //objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                   // m_objDbCommand = objDbConnection.CreateCommand();
                   // m_objDbCommand.CommandText = sbSQL.ToString();
                    objReader = DbFactory.ExecuteReader(m_sConnectionString, sbSQL.ToString(), dictParams);
                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    //End changes by bsharma33 for PenTesting MITS 26942
                    if (objReader.Read())
                    {
                        throw new RMAppException(Globalization.GetString("FLMaxRate.Same.Year.Error", m_iClientId));
                    }
                }
                else
                {
                    //Changed by bsharma33 for PenTesting MITS 26942
                    //sSql = "SELECT * FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT = " + m_objXMLDocument.SelectSingleNode("//control[@name='Year']").InnerText.Trim() + " AND MAX_WC_RATE_ID <> " + m_objXMLDocument.SelectSingleNode("//control[@name='MaxRateId']").InnerText.Trim();
                    sbSQL.Append(string.Format("SELECT * FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT ={0}  AND MAX_WC_RATE_ID <> {1}", "~MAX_YEAR_TEXT~", "~MAX_WC_RATE_ID~"));
                    dictParams.Add("MAX_YEAR_TEXT", m_objXMLDocument.SelectSingleNode("//control[@name='Year']").InnerText.Trim());
                    dictParams.Add("MAX_WC_RATE_ID", m_objXMLDocument.SelectSingleNode("//control[@name='MaxRateId']").InnerText.Trim());
                    //objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                    //m_objDbCommand = objDbConnection.CreateCommand();
                   // m_objDbCommand.CommandText = sbSQL.ToString();
                    objReader = DbFactory.ExecuteReader(m_sConnectionString, sbSQL.ToString(), dictParams);
                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    //End changes by bsharma33 for PenTesting MITS 26942
                    if (objReader.Read())
                    {
                        throw new RMAppException(Globalization.GetString("FLMaxRate.Same.Year.Error", m_iClientId));
                    }
                }
                //MGaba2:MITS 16188:End
                base.Save();
                bReturnValue = true;
                return bReturnValue;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRate.Save.Error", m_iClientId), p_objEx);
            }

        }
        #endregion

        #region Private Functions
        new private void Initialize()
        {
            try
            {
                TableName = "WC_MAX_RATE_CALC";
                KeyField = "MAX_WC_RATE_ID";
                base.InitFields(arrFields);
                base.Initialize();
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRate.Initialize.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: Delete
        /// Author		: Shruti
        /// Date Created: 08/22/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes the MaxRate information
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public bool Delete(XmlDocument p_objXmlDocument)
        {
            bool bReturnValue = false;
            string sSQL = "";
            XmlNode objNode = null;
            try
            {
                XMLDoc = p_objXmlDocument;
                base.Delete();
                bReturnValue = true;
                return bReturnValue;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRate.Delete.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
        }
        #endregion
    }
}
