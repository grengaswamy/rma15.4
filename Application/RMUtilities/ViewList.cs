using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	/// Summary description for ViewList.
	/// </summary>
	public class ViewList
	{
		#region Private variables
		private string m_sDSN="";
        private int m_iClientId = 0;
		#endregion

		#region Properties
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		#endregion

		#region Constructor
        public ViewList(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
        public ViewList(string p_sDSN, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sDSN=p_sDSN;
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get()
		{
			DbReader objReader=null;
			XmlDocument objDOM=null;
			StringBuilder sbSql=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			try
			{
				sbSql=new StringBuilder();
				sbSql.Append("SELECT * FROM NET_VIEWS");
				objReader = DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				objDOM=new XmlDocument();

				
				objElemParent = objDOM.CreateElement("Views");
				objDOM.AppendChild(objElemParent);
				while (objReader.Read())
				{	
					objElemTemp=objDOM.CreateElement("option");
					objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetValue("VIEW_ID")));
					objElemTemp.SetAttribute("desc",Conversion.ConvertObjToStr(objReader.GetValue("VIEW_DESC")));
					objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("VIEW_NAME"));
					objElemParent.AppendChild(objElemTemp);
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ViewList.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				objReader=null;
				objDOM=null;
				sbSql=null;
				objElemParent=null;
				objElemTemp=null;
			}
		}
		#endregion
	}
}
