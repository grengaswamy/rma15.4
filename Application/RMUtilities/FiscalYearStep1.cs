﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using System.Collections.Generic;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Fiscal Year information.
	/// </summary>
	public class FiscalYearWizard
	{
		#region Input Xml
		/*
		 <FiscalYearWizard>
			<LineOfBusiness>0</LineOfBusiness>
			<Organization>0</Organization>
			<NoOfPeriods>5</NoOfPeriods>
			<FiscalYear>2006</FiscalYear>
			<FiscalWeekDay>4</FiscalWeekDay>
			<FiscalYearStart>4/22/2006</FiscalYearStart>
			<FiscalYearEnd>6/21/2006</FiscalYearEnd>
			<FiscalQ1Start>4/22/2006</FiscalQ1Start>
			<FiscalQ1End>7/21/2006</FiscalQ1End>
			<FiscalQ2Start>7/22/2006</FiscalQ2Start>
			<FiscalQ2End>10/21/2006</FiscalQ2End>
			<FiscalQ3Start>10/22/2006</FiscalQ3Start>
			<FiscalQ3End>1/21/2007</FiscalQ3End>
			<FiscalQ4Start>1/22/2007</FiscalQ4Start>
			<FiscalQ4End>4/21/2007</FiscalQ4End>
			<FiscalPeriods>
				<Period>
				<StartDate>4/22/2006</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>
				</EndDate>
				</Period>
				<Period>
				<StartDate>
				</StartDate>
				<EndDate>6/21/2006</EndDate>
				</Period>
			</FiscalPeriods>
		</FiscalYearWizard>
		*/
		#endregion

		#region private variables
		private string m_sDSN="";
        private int iLangCode= 1033;
		private const string sTab="                        ";
        private int m_iClientId = 0;//sonali-cloud

		#endregion

		#region Properties
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
        public int LanguageCode
        {
            set
            {
                iLangCode = value;
            }
        }
		#endregion

		#region Constructor
		public FiscalYearWizard(string p_sConnectString, int p_iClientId)//sonali-cloud
		{
			m_sDSN=p_sConnectString;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Private Functions
		private string GetAttributeValue(XmlDocument p_objXmlDocument,string p_sValue,string p_sAttName)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//"+p_sValue);
			if (objNode!=null)
			{
				if (objNode.Attributes[p_sAttName]!=null)
				{
					return objNode.Attributes[p_sAttName].Value;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//"+p_sValue);
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}
		#endregion

		#region Public Functions
		/// Name		: BuildQuaters
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Builds Quaters data
		/// </summary>
		/// <param name="p_objDOC">Xml containg information to build Quaters data</param>
		/// <returns>Xml containg Quaters data</returns>
		public XmlDocument BuildQuaters(XmlDocument p_objDOC)
		{
			XmlNode objNode=null;
            XmlNode objFiscalYear = null;  //Aman MITS 27341
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			string sFYStart="";
			string sFYEnd="";
			string sTempDate="";

            //abisht MITS 10334
            bool bOverlapFlag = false;
            string sSql = string.Empty;
            DbReader objReader = null;
            string sLob = string.Empty;
            string sOrgId = string.Empty;
            string sSqlAnd = string.Empty;
            string sFiscalYear = string.Empty;
            //abisht MITS 10334 - End of changes.

            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012

			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("FiscalYearWizard");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("FiscalYearStart");
				objNode=p_objDOC.SelectSingleNode("//FiscalYearStart");

				if (objNode!=null)
				{
					if (objNode.InnerText !=null)
					{
						sFYStart=objNode.InnerText;
					    
						objElemChild.InnerText=sFYStart;
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalYearEnd");
						sFYEnd=Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).ToString("d");
						objElemChild.InnerText=sFYEnd;
						sTempDate=sFYStart;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ1Start");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ1End");
						sTempDate=Conversion.ToDate(Conversion.GetDate(sTempDate)).AddMonths(3).AddDays(-1).Date.ToString("d");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ2Start");
						sTempDate=Conversion.ToDate(Conversion.GetDate(sTempDate)).AddDays(1).Date.ToString("d");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ2End");
						sTempDate=Conversion.ToDate(Conversion.GetDate(sTempDate)).AddMonths(3).AddDays(-1).Date.ToString("d");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ3Start");
						sTempDate=Conversion.ToDate(Conversion.GetDate(sTempDate)).AddDays(1).Date.ToString("d");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ3End");
						sTempDate=Conversion.ToDate(Conversion.GetDate(sTempDate)).AddMonths(3).AddDays(-1).Date.ToString("d");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ4Start");
						sTempDate=Conversion.ToDate(Conversion.GetDate(sTempDate)).AddDays(1).Date.ToString("d");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);

						objElemChild=objDOM.CreateElement("FiscalQ4End");
						sTempDate=Conversion.ToDate(Conversion.GetDate(sTempDate)).AddMonths(3).AddDays(-1).Date.ToString("d");
						objElemChild.InnerText=sTempDate;
						objDOM.FirstChild.AppendChild(objElemChild);
					}
				}


                //abisht MITS 10334

                //PenTesting - srajindersin - 9th Jan 2012
                strSQL.Append(string.Format(" SELECT * FROM FISCAL_YEAR WHERE (( FY_START_DATE  <= {0} AND {1} <= FY_END_DATE )", "~FY_START_DATE~", "~FY_START_DATE1~"));
                dictParams.Add("FY_START_DATE", Conversion.ConvertStrToInteger(Conversion.GetDate(sFYStart)));
                dictParams.Add("FY_START_DATE1", Conversion.ConvertStrToInteger(Conversion.GetDate(sFYStart)));
                strSQL.Append(string.Format(" OR ( FY_START_DATE  <= {0} AND {1} <= FY_END_DATE ))", "~OR_FY_START_DATE~", "~OR_FY_START_DATE1~"));  //Aman MITS 27341
                dictParams.Add("OR_FY_START_DATE", Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).ToString("d"))));
                dictParams.Add("OR_FY_START_DATE1", Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).ToString("d"))));   //Aman MITS 27341
                //sSql = "SELECT * FROM FISCAL_YEAR WHERE (( FY_START_DATE  <= " + Conversion.ConvertStrToInteger(Conversion.GetDate(sFYStart)) + " AND " + Conversion.ConvertStrToInteger(Conversion.GetDate(sFYStart)) + " <= FY_END_DATE )";
                //sSql = sSql + "OR ( FY_START_DATE  <= " + Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).ToString("d"))) + " AND " + Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).ToString("d"))) + " <= FY_END_DATE ))";
                //END PenTesting - srajindersin - 9th Jan 2012


                objNode = p_objDOC.SelectSingleNode("//LineOfBusinessCode");
                sLob = objNode.InnerText;
                if (!sLob.Equals("0"))
                {
                    //END PenTesting - srajindersin - 9th Jan 2012
                    strSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}", "~LINE_OF_BUS_CODE~"));
                    dictParams.Add("LINE_OF_BUS_CODE", sLob);
                    //sSqlAnd = "  AND LINE_OF_BUS_CODE=" + sLob;
                    //END PenTesting - srajindersin - 9th Jan 2012
                }
                else
                {
                    sSqlAnd = "  AND (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)";
                }
                objNode = p_objDOC.SelectSingleNode("//OrganizationCode");
                sOrgId = objNode.InnerText;
                if (!sOrgId.Equals("0"))
                {
                    //END PenTesting - srajindersin - 9th Jan 2012
                    strSQL.Append(string.Format("  AND ORG_EID = {0}", "~ORG_EID~"));
                    dictParams.Add("ORG_EID", sOrgId);
                    //sSqlAnd = sSqlAnd + "  AND ORG_EID =" + sOrgId;
                    //END PenTesting - srajindersin - 9th Jan 2012
                }
                else
                {
                    sSqlAnd = sSqlAnd + "  AND (ORG_EID = 0 OR ORG_EID IS NULL)";
                }
                //Aman MITS 27341--Start
                objNode = p_objDOC.SelectSingleNode("//Call");               
                if (objNode != null && objNode.InnerText == "0")
                {
                    objFiscalYear = p_objDOC.SelectSingleNode("//FiscalYear");
                    if (objFiscalYear != null)
                    {
                        sFiscalYear = objFiscalYear.InnerText;
                        strSQL.Append(string.Format(" AND FISCAL_YEAR <> {0}", "~FISCAL_YEAR~"));
                        dictParams.Add("FISCAL_YEAR", sFiscalYear);
                    }
                }
                //Aman MITS 27341--End

                //PenTesting - srajindersin - 9th Jan 2012
                //sSql = "SELECT * FROM FISCAL_YEAR WHERE (( FY_START_DATE  <= " + Conversion.ConvertStrToInteger(Conversion.GetDate(sFYStart)) + " AND " + Conversion.ConvertStrToInteger(Conversion.GetDate(sFYStart)) + " <= FY_END_DATE )";
                //sSql = sSql + "OR ( FY_START_DATE  <= " + Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).ToString("d"))) + " AND " + Conversion.ConvertStrToInteger(Conversion.GetDate(Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).ToString("d"))) + " <= FY_END_DATE ))";
                //sSql = sSql + sSqlAnd;
                //objReader = DbFactory.ExecuteReader(m_sDSN, sSql);
                //END PenTesting - srajindersin - 9th Jan 2012
                
                objReader = DbFactory.ExecuteReader(m_sDSN, strSQL.ToString(), dictParams);
                if(objReader.Read())
                {                   
                        bOverlapFlag = true;
                }

                objReader.Dispose();

                //abisht MITS 10334
                objNode = p_objDOC.SelectSingleNode("//OverlapFlag");
                if (objNode != null)
                {
                    objElemChild = objDOM.CreateElement("OverlapFlag");
                    objElemChild.InnerText = bOverlapFlag.ToString();
                    objDOM.FirstChild.AppendChild(objElemChild);
                }

				return objDOM;

			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FiscalYearWizard.BuildQuaters.Error",m_iClientId),p_objEx);//sonali-cloud
			}
			finally
			{
				objNode=null;
                objFiscalYear = null;
				objDOM=null;
				objElemParent=null;
				objElemChild=null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }
			}
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Data to populate screen
		/// </summary>
		/// <param name="p_objDOC"></param>
		/// <returns>Xml Containing data to populate screen</returns>
		public XmlDocument Get(XmlDocument p_objDOC)
		{
			XmlNode objNode=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			LocalCache objCache=null;
			SortedList objList=null;
			StringBuilder sbSQL=null;
			string sSQL="";
			string sFiscalYear=null;
			string sWeekDay="";
			string sOrgId="";
			string sLobId="";
			bool bIsNew=false;
			XmlElement objTemp=null;
			DbReader objReader=null;

            //PenTesting - srajindersin - 9th Jan 2012
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012

			try
			{
				objDOM =new XmlDocument();
				objNode=p_objDOC.SelectSingleNode("//Call");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						if (objNode.InnerText.Equals("1"))
						{
							bIsNew=true;
						}
					}
				}
				objElemParent = objDOM.CreateElement("FiscalYearWizard");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("LineOfBusiness");
				objNode=p_objDOC.SelectSingleNode("//LineOfBusiness");
				if (objNode!=null)
				{
					sLobId=GetValue(p_objDOC,"LineOfBusiness");
					objElemChild.SetAttribute("value",sLobId);	
					if (!sLobId.Trim().Equals("0")&& !sLobId.Equals(""))
					{
                        objCache = new LocalCache(m_sDSN, m_iClientId);
                        objElemChild.InnerText = objCache.GetCodeDesc(Conversion.ConvertObjToInt(sLobId, m_iClientId));
						objCache.Dispose();
					}
					else
					{
						objElemChild.InnerText=CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.Get.AllLOB",m_iClientId),this.iLangCode.ToString());//sonali-cloud
					}
					
				}

				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("Organization");
				objNode=p_objDOC.SelectSingleNode("//Organization");
				if (objNode!=null)
				{
					sOrgId=GetValue(p_objDOC,"Organization");
					objElemChild.SetAttribute("value",sOrgId);	
					if (!sOrgId.Trim().Equals("0")&& !sOrgId.Equals(""))
					{
                        objElemChild.InnerText = UTILITY.GetOrg(sOrgId, m_sDSN, m_iClientId).Split(new char[] { ' ' })[1];
					}
					else
					{
                        objElemChild.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.Get.AllORG", m_iClientId), this.iLangCode.ToString());//sonali-cloud
					}
					
				}
				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("FiscalYear");
				objNode=p_objDOC.SelectSingleNode("//FiscalYear");
				if (objNode!=null)
				{
					sFiscalYear=GetValue(p_objDOC,"FiscalYear");
					objElemChild.InnerText=sFiscalYear;
				}
				objDOM.FirstChild.AppendChild(objElemChild);
				if (!bIsNew)
				{
					sbSQL=new StringBuilder();
				
					if (!sLobId.Equals("0") && !sLobId.Equals(""))
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sbSQL.Append(string.Format("  WHERE LINE_OF_BUS_CODE = {0}", "~LINE_OF_BUS_CODE~"));
                        dictParams.Add("LINE_OF_BUS_CODE", sLobId);
						//sbSQL.Append("  WHERE LINE_OF_BUS_CODE="+sLobId);
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sbSQL.Append("  WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)");
					}
					if (!sOrgId.Equals("0") && !sOrgId.Equals(""))
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sbSQL.Append(string.Format(" AND ORG_EID = {0}", "~ORG_EID~"));
                        dictParams.Add("ORG_EID", sOrgId);
						//sbSQL.Append(" AND ORG_EID ="+sOrgId);
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sbSQL.Append("  AND (ORG_EID = 0 OR ORG_EID IS NULL)");
					}
					if (!sFiscalYear.Equals(""))
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sbSQL.Append(string.Format(" AND FISCAL_YEAR = {0}", "~FISCAL_YEAR~"));
                        dictParams.Add("FISCAL_YEAR", sFiscalYear);
						//sbSQL.Append("   AND FISCAL_YEAR = '"+sFiscalYear+"'");
                        //END PenTesting - srajindersin - 9th Jan 2012
					}

					sSQL=" SELECT * FROM FISCAL_YEAR "+sbSQL.ToString();
                    
                    //PenTesting - srajindersin - 9th Jan 2012
                    objReader = DbFactory.ExecuteReader(m_sDSN, sSQL, dictParams);
					//objReader=DbFactory.GetDbReader(m_sDSN,sSQL);
                    //END PenTesting - srajindersin - 9th Jan 2012
					if (objReader.Read())
					{
						objElemChild=objDOM.CreateElement("FiscalYearStart");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("FY_START_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalYearEnd");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("FY_END_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ1Start");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR1_START_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ1End");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR1_END_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ2Start");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR2_START_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ2End");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR2_END_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ3Start");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR3_START_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ3End");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR3_END_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ4Start");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR4_START_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ4End");
						objElemChild.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("QTR4_END_DATE")),"d");
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("NoOfPeriods");
						objElemChild.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("NUM_PERIODS"));
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalWeekDay");
						objElemChild.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("WEEK_END_DAY"));
						sWeekDay=objElemChild.InnerText;
						objDOM.FirstChild.AppendChild(objElemChild);
					}
					else
					{
						objElemChild=objDOM.CreateElement("FiscalYear");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalYearStart");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalYearEnd");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ1Start");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ1End");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ2Start");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ2End");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ3Start");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ3End");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ4Start");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalQ4End");
						objElemChild.InnerText="";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("NoOfPeriods");
						objElemChild.InnerText="0";
						objDOM.FirstChild.AppendChild(objElemChild);
						objElemChild=objDOM.CreateElement("FiscalWeekDay");
						objElemChild.InnerText="";
						sWeekDay=objElemChild.InnerText;
						objDOM.FirstChild.AppendChild(objElemChild);
					}
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
				}
				else
				{
					objElemChild=objDOM.CreateElement("FiscalYear");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalYearStart");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalYearEnd");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ1Start");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ1End");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ2Start");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ2End");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ3Start");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ3End");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ4Start");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalQ4End");
					objElemChild.InnerText="";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("NoOfPeriods");
					objElemChild.InnerText="0";
					objDOM.FirstChild.AppendChild(objElemChild);
					objElemChild=objDOM.CreateElement("FiscalWeekDay");
					objElemChild.InnerText="";
					sWeekDay=objElemChild.InnerText;
					objDOM.FirstChild.AppendChild(objElemChild);
				}
				objList=new SortedList();
				objList.Add("0","Sunday");
				objList.Add("1","Monday");
				objList.Add("2","Tuesday");
				objList.Add("3","Wednesday");
				objList.Add("4","Thursday");
				objList.Add("5","Friday");
				objList.Add("6","Saturday");
				objElemParent=objDOM.CreateElement("option");
				IDictionaryEnumerator objEnum=objList.GetEnumerator();
				while (objEnum.MoveNext())
				{
					objElemChild=objDOM.CreateElement("option");
					objElemChild.SetAttribute("value",objEnum.Key.ToString());
					objElemChild.InnerText=objEnum.Value.ToString();
					if (sWeekDay.Trim().Equals(objEnum.Key.ToString()))
					{
						objElemChild.SetAttribute("selected","1");
					}
					objElemParent.AppendChild(objElemChild);
				}
				objDOM.FirstChild.AppendChild(objElemParent);
				objElemParent=objDOM.CreateElement("FiscalPeriods");
				if (!bIsNew)
				{
					sSQL="SELECT * FROM FISCAL_YEAR_PERIOD "+sbSQL.ToString();

                    //PenTesting - srajindersin - 9th Jan 2012
                    objReader = DbFactory.ExecuteReader(m_sDSN, sSQL, dictParams);
					//objReader=DbFactory.GetDbReader(m_sDSN,sSQL);
                    //END PenTesting - srajindersin - 9th Jan 2012

					while(objReader.Read())
					{
						string sTempPeriod="";
						string sTempStart="";
						string sTempEnd="";
						objElemChild=objDOM.CreateElement("Period");
						objTemp=objDOM.CreateElement("PeriodNumber");
						sTempPeriod=Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_NUMBER"));
						objTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_NUMBER"));
						objElemChild.AppendChild(objTemp);
						objTemp=objDOM.CreateElement("StartDate");
						sTempStart=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_START_DATE")),"d");
						objTemp.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_START_DATE")),"d");
						objElemChild.AppendChild(objTemp);
						objTemp=objDOM.CreateElement("EndDate");
						sTempEnd=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_END_DATE")),"d");
						objTemp.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_END_DATE")),"d");
						objElemChild.AppendChild(objTemp);
						objTemp=objDOM.CreateElement("Summary");
						objTemp.SetAttribute("value",sTempPeriod+"**"+sTempStart+"**"+sTempEnd);
						objTemp.InnerText="Period #"+sTempPeriod+sTab+sTempStart+sTab+sTempEnd;
						objElemChild.AppendChild(objTemp);
						objElemParent.AppendChild(objElemChild);
					}
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
				}
				objDOM.FirstChild.AppendChild(objElemParent);
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FiscalYearWizard.Get.Error",m_iClientId),p_objEx);//sonali-cloud
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Dispose();
				}
                if (objCache != null)
                {
                    objCache.Dispose();
                }
				sbSQL=null; 
				objReader=null;
				objList=null;
				objNode=null;
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
             }
		}
		/// Name		: BuildPeriods
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Builds FiscalPeriods data
		/// </summary>
		/// <param name="p_objDOC">Xml containg information to build FiscalPeriods data</param>
		/// <returns>Xml containg FiscalPeriods data</returns>
		public XmlDocument BuildPeriods(XmlDocument p_objDOC)
		{
			XmlNode objNode=null;
			XmlDocument objDOM=null;
			string sPeriods="";
			int iPeriods=0;
			string sTmpDate="";
			string sTmpPrev="";
			string sTmpEnd="";
			string sFYStart="";
			string sFYEnd="";
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			try
			{
				objNode=p_objDOC.SelectSingleNode(".//NoOfPeriods");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sPeriods=objNode.InnerText;
					}
					if (sPeriods.Trim()!="")
					{
						iPeriods=Conversion.ConvertStrToInteger(sPeriods);
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalYearStart");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sFYStart=objNode.InnerText;
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalYearEnd");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sFYEnd=objNode.InnerText;
					}
				}
				objDOM=new XmlDocument();
				objElemParent = objDOM.CreateElement("FiscalYearWizard");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("FiscalPeriods");
				objDOM.FirstChild.AppendChild(objElemChild);
				for (int i=1;i<=iPeriods;i++)
				{
					switch(iPeriods)
					{
						case 1:
						case 2:
						case 3:
						case 4:
						case 6:
						case 12:
							if (i==1)
							{
								sTmpDate=sFYStart;
							}
							else
							{
								sTmpDate=Conversion.ToDate(Conversion.GetDate(sTmpPrev)).AddMonths(12/iPeriods).Date.ToString("d");
							}
							sTmpPrev=sTmpDate;
							objElemParent=objDOM.CreateElement("Period");
							objElemTemp=objDOM.CreateElement("PeriodNumber");
							objElemTemp.InnerText=i.ToString();
							objElemParent.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("StartDate");
							objElemTemp.InnerText=sTmpDate;
							objElemParent.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("EndDate");
							objElemTemp.InnerText=Conversion.ToDate(Conversion.GetDate(sTmpPrev)).AddMonths(12/iPeriods).AddDays(-1).Date.ToString("d");
							objElemParent.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("Summary");
							objElemTemp.SetAttribute("value",i.ToString()+"**"+sTmpDate+"**"+Conversion.ToDate(Conversion.GetDate(sTmpPrev)).AddMonths(12/iPeriods).AddDays(-1).Date.ToString("d"));
							objElemTemp.InnerText="Period #"+i.ToString()+sTab+sTmpDate+sTab+Conversion.ToDate(Conversion.GetDate(sTmpPrev)).AddMonths(12/iPeriods).AddDays(-1).Date.ToString("d");
							objElemParent.AppendChild(objElemTemp);	
							objDOM.FirstChild.FirstChild.AppendChild(objElemParent);
							break;
						default:
							if (i==1)
							{
								sTmpDate=sFYStart;
							}
							else
							{
								sTmpDate="";
							}
							if (i==iPeriods)
							{
								sTmpEnd=sFYEnd;
							}
							else
							{
								sTmpEnd="";
							}
							objElemParent=objDOM.CreateElement("Period");
							objElemTemp=objDOM.CreateElement("PeriodNumber");
							objElemTemp.InnerText=i.ToString();
							objElemParent.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("StartDate");
							objElemTemp.InnerText=sTmpDate;
							objElemParent.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("EndDate");
							objElemTemp.InnerText=sTmpEnd;
							objElemParent.AppendChild(objElemTemp);
							objElemTemp=objDOM.CreateElement("Summary");
							objElemTemp.SetAttribute("value",i.ToString()+"**"+sTmpDate+"**"+sTmpEnd);
							objElemTemp.InnerText="Period #"+i.ToString()+sTab+sTmpDate+sTab+sTmpEnd;
							objElemParent.AppendChild(objElemTemp);	
							objDOM.FirstChild.FirstChild.AppendChild(objElemParent);
							break;
					}
				}
				objElemChild=objDOM.CreateElement("FiscalYearEnd");
				objElemChild.InnerText=Conversion.ToDate(Conversion.GetDate(sFYStart)).AddYears(1).AddDays(-1).Date.ToString("d");
				objDOM.FirstChild.AppendChild(objElemChild);
				return objDOM;

			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FiscalYearWizard.BuildPeriods.Error",m_iClientId),p_objEx);//sonali-cloud
			}
			finally
			{
				objNode=null;
				objDOM=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
			}
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the Fiscal data
		/// </summary>
		/// <param name="p_objDOC">Fiscal Data to be Saved</param>
		/// <param name="p_sError">Error while updating data</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objDOC)
		{
			StringBuilder sbSQL=null; 
			int iCnt=0; 
			string sdtFYStart=""; 
			string sdtFYEnd=""; 
			ArrayList alSQL=null;
			string sdtQ1Start=""; 
			string sdtQ1End=""; 
			string sdtQ2Start=""; 
			string sdtQ2End=""; 
			string sdtQ3Start=""; 
			string sdtQ3End=""; 
			string sdtQ4Start=""; 
			string sdtQ4End=""; 
			string sPeriods="";
			string sWeekDay="";
			string sFiscalYear="";
			//FiscalPeriods
			string sdtPerStart="";
			string sdtPerEnd="";
			string sLobCode="";
			string sOrgId="";
			DbReader objReader=null;
			XmlNode objNode=null;
			XmlNodeList objNodeList=null;
			bool bReturnValue=false;

            //PenTesting - srajindersin - 9th Jan 2012
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            DbCommand objCmd = null;
            DbConnection objCon = null;
            DbTransaction objTrans = null;
            //PenTesting - srajindersin - 9th Jan 2012

			try
			{
				sbSQL=new StringBuilder();
				sLobCode=GetAttributeValue(p_objDOC,"LineOfBusiness","value");
				sOrgId=GetAttributeValue(p_objDOC,"Organization","value");
				sFiscalYear=GetValue(p_objDOC,"FiscalYear");
                // akaushik5 Added for MITS 37480 Starts
                string sPreviousFiscalYear = GetValue(p_objDOC, "PreviousFiscalYear");
                // akaushik5 Added for MITS 37480 Ends
				sdtFYStart=Conversion.GetDate(GetValue(p_objDOC,"FiscalYearStart"));
				sdtFYEnd=Conversion.GetDate(GetValue(p_objDOC,"FiscalYearEnd"));
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ1Start");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ1Start = Conversion.GetDate(objNode.InnerText);
						//sdtQ1Start="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ1Start="null";
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ2Start");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ2Start =Conversion.GetDate(objNode.InnerText);
                        //sdtQ2Start="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ2Start="null";
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ3Start");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ3Start = Conversion.GetDate(objNode.InnerText);
                        //sdtQ3Start="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ3Start="null";
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ4Start");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ4Start = Conversion.GetDate(objNode.InnerText);
                        //sdtQ4Start="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ4Start="null";
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ1End");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ1End = Conversion.GetDate(objNode.InnerText);
                        //sdtQ1End="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ1End="null";
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ2End");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ2End = Conversion.GetDate(objNode.InnerText);
                        //sdtQ2End="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ2End="null";
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ3End");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ3End = Conversion.GetDate(objNode.InnerText);
                        //sdtQ3End="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ3End="null";
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FiscalQ4End");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
                        //PenTesting - srajindersin - 9th Jan 2012
                        sdtQ4End = Conversion.GetDate(objNode.InnerText);
                        //sdtQ4End="'"+Conversion.GetDate(objNode.InnerText)+"'";
                        //END PenTesting - srajindersin - 9th Jan 2012
					}
					else
					{
						sdtQ4End="null";
					}
				}
				sPeriods=GetValue(p_objDOC,"NoOfPeriods");
				sWeekDay= GetValue(p_objDOC,"FiscalWeekDay");

                //PenTesting - srajindersin - 9th Jan 2012
                objCon = DbFactory.GetDbConnection(m_sDSN);
                objCon.Open();
                objTrans = objCon.BeginTransaction();
                objCmd = objCon.CreateCommand();
                objCmd.Transaction = objTrans;
                //END PenTesting - srajindersin - 9th Jan 2012

				sbSQL=new StringBuilder();

                //PenTesting - srajindersin - 9th Jan 2012
                sbSQL.Append(string.Format("DELETE FROM FISCAL_YEAR WHERE FISCAL_YEAR = {0}","~FISCAL_YEAR~"));
                sbSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}", "~LINE_OF_BUS_CODE~"));
                sbSQL.Append(string.Format(" AND ORG_EID = {0}","~ORG_EID~"));
                // akaushik5 Changed for MITS 37480 Starts
                //dictParams.Add("FISCAL_YEAR", sFiscalYear);
                dictParams.Add("FISCAL_YEAR", !string.IsNullOrEmpty(sPreviousFiscalYear) ? sPreviousFiscalYear : sFiscalYear);
                // akaushik5 Changed for MITS 37480 Ends
                dictParams.Add("LINE_OF_BUS_CODE", sLobCode);
                dictParams.Add("ORG_EID", sOrgId);

                objCmd.CommandText = sbSQL.ToString();
                DbFactory.ExecuteNonQuery(objCmd, dictParams);
                objCmd.Parameters.Clear();
                sbSQL = new StringBuilder();
                sbSQL.Append(string.Format("DELETE FROM FISCAL_YEAR_PERIOD WHERE FISCAL_YEAR = {0}", "~FISCAL_YEAR_PERIOD~"));
                sbSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}", "~LINE_OF_BUS_CODE_PERIOD~"));
                sbSQL.Append(string.Format(" AND ORG_EID = {0}", "~ORG_EID_PERIOD~"));
                dictParams.Clear();
                // akaushik5 Changed for MITS 37480 Starts
                //dictParams.Add("FISCAL_YEAR_PERIOD", sFiscalYear);
                dictParams.Add("FISCAL_YEAR_PERIOD", !string.IsNullOrEmpty(sPreviousFiscalYear) ? sPreviousFiscalYear : sFiscalYear);
                // akaushik5 Changed for MITS 37480 Ends
                dictParams.Add("LINE_OF_BUS_CODE_PERIOD", sLobCode);
                dictParams.Add("ORG_EID_PERIOD", sOrgId);

                objCmd.CommandText = sbSQL.ToString();
                DbFactory.ExecuteNonQuery(objCmd, dictParams);
                objCmd.Parameters.Clear();
                sbSQL = new StringBuilder();
                sbSQL.Append("INSERT INTO FISCAL_YEAR (FISCAL_YEAR, FY_START_DATE, FY_END_DATE,");
                sbSQL.Append(" QTR1_START_DATE, QTR1_END_DATE, QTR2_START_DATE, QTR2_END_DATE,");
                sbSQL.Append(" QTR3_START_DATE, QTR3_END_DATE, QTR4_START_DATE, QTR4_END_DATE,");
                sbSQL.Append("  NUM_PERIODS, WEEK_END_DAY, LINE_OF_BUS_CODE, ORG_EID)");
                sbSQL.Append(string.Format(" VALUES ( {0}, {1}, {2},", "~FISCAL_YEAR_INSERT~", "~FY_START_DATE~", "~FY_END_DATE~"));
                sbSQL.Append(string.Format(" {0}, {1}, {2}, {3},", "~QTR1_START_DATE~", "~QTR1_END_DATE~", "~QTR2_START_DATE~","~QTR2_END_DATE~"));
                sbSQL.Append(string.Format(" {0}, {1}, {2}, {3},", "~QTR3_START_DATE~", "~QTR3_END_DATE~", "~QTR4_START_DATE~", "~QTR4_END_DATE~"));
                sbSQL.Append(string.Format(" {0}, {1}, {2}, {3} )", "~NUM_PERIODS~", "~WEEK_END_DAY~", "~LINE_OF_BUS_CODE_INSERT~", "~ORG_EID_INSERT~"));
                dictParams.Clear();
                dictParams.Add("FISCAL_YEAR_INSERT", sFiscalYear);
                dictParams.Add("FY_START_DATE", sdtFYStart);
                dictParams.Add("FY_END_DATE", sdtFYEnd);
                dictParams.Add("QTR1_START_DATE", sdtQ1Start);
                dictParams.Add("QTR1_END_DATE", sdtQ1End);
                dictParams.Add("QTR2_START_DATE", sdtQ2Start);
                dictParams.Add("QTR2_END_DATE", sdtQ2End);
                dictParams.Add("QTR3_START_DATE", sdtQ3Start);
                dictParams.Add("QTR3_END_DATE", sdtQ3End);
                dictParams.Add("QTR4_START_DATE", sdtQ4Start);
                dictParams.Add("QTR4_END_DATE", sdtQ4End);
                dictParams.Add("NUM_PERIODS", sPeriods);
                dictParams.Add("WEEK_END_DAY", sWeekDay);
                dictParams.Add("LINE_OF_BUS_CODE_INSERT", sLobCode);
                dictParams.Add("ORG_EID_INSERT", sOrgId);

                objCmd.CommandText = sbSQL.ToString();
                DbFactory.ExecuteNonQuery(objCmd, dictParams);
                objCmd.Parameters.Clear();
                //sbSQL.Append("DELETE FROM FISCAL_YEAR WHERE FISCAL_YEAR ="+sFiscalYear);
                //sbSQL.Append(" AND LINE_OF_BUS_CODE ="+sLobCode);
                //sbSQL.Append(" AND ORG_EID = "+sOrgId);
                //alSQL=new ArrayList();
                //alSQL.Add(sbSQL.ToString());
                //sbSQL=new StringBuilder();
                //sbSQL.Append("DELETE FROM FISCAL_YEAR_PERIOD WHERE FISCAL_YEAR ="+sFiscalYear);
                //sbSQL.Append(" AND LINE_OF_BUS_CODE ="+sLobCode);
                //sbSQL.Append(" AND ORG_EID = "+sOrgId);
                //alSQL.Add(sbSQL.ToString());
                //sbSQL=new StringBuilder();
                //sbSQL.Append("INSERT INTO FISCAL_YEAR (FISCAL_YEAR, FY_START_DATE, FY_END_DATE,");
                //sbSQL.Append(" QTR1_START_DATE, QTR1_END_DATE, QTR2_START_DATE, QTR2_END_DATE,");
                //sbSQL.Append(" QTR3_START_DATE, QTR3_END_DATE, QTR4_START_DATE, QTR4_END_DATE,");
                //sbSQL.Append("  NUM_PERIODS, WEEK_END_DAY, LINE_OF_BUS_CODE, ORG_EID)");
                //sbSQL.Append(" VALUES ('"+ sFiscalYear + "', '" + sdtFYStart + "', '" + sdtFYEnd + "', ");
                //sbSQL.Append( sdtQ1Start + ", " + sdtQ1End + ", " + sdtQ2Start + ", " + sdtQ2End + ", ");
                //sbSQL.Append( sdtQ3Start + ", " + sdtQ3End + ", " + sdtQ4Start + ", " + sdtQ4End + ",");
                //sbSQL.Append( sPeriods + "," + sWeekDay + ", " + sLobCode+ ", " + sOrgId + ")");

                //alSQL.Add(sbSQL.ToString());
                //END PenTesting - srajindersin - 9th Jan 2012

				objNodeList=p_objDOC.SelectNodes("//FiscalPeriods/Period");
				iCnt=1;
				foreach(XmlNode objTempNode in objNodeList)
				{
					sdtPerStart="";
					sdtPerEnd="";
					sbSQL=new StringBuilder();
					objNode=objTempNode.SelectSingleNode(".//StartDate");
					if (objNode!=null)
					{
						if (objNode.InnerText !=null)
						{
							sdtPerStart=objNode.InnerText;
							if (!sdtPerStart.Trim().Equals(""))
							{
                                //END PenTesting - srajindersin - 9th Jan 2012
                                sdtPerStart = Conversion.GetDate(sdtPerStart);
                                //sdtPerStart="'"+Conversion.GetDate(sdtPerStart)+"'";
                                //END PenTesting - srajindersin - 9th Jan 2012
							}
							else
							{
								sdtPerStart="null";
							}
						}
					}
					objNode=objTempNode.SelectSingleNode(".//EndDate");
					if (objNode!=null)
					{
						if (objNode.InnerText !=null)
						{
							sdtPerEnd=objNode.InnerText;
							if (!sdtPerEnd.Trim().Equals(""))
							{
                                //PenTesting - srajindersin - 9th Jan 2012
                                sdtPerEnd = Conversion.GetDate(sdtPerEnd);
                                //sdtPerEnd="'"+Conversion.GetDate(sdtPerEnd)+"'";
                                //END PenTesting - srajindersin - 9th Jan 2012
							}
							else
							{
								sdtPerEnd="null";
							}
						}
					}

                    //PenTesting - srajindersin - 9th Jan 2012
                    sbSQL = new StringBuilder();
                    sbSQL.Append("INSERT INTO FISCAL_YEAR_PERIOD(FISCAL_YEAR, PERIOD_NUMBER, PERIOD_START_DATE,");
                    sbSQL.Append(" PERIOD_END_DATE, LINE_OF_BUS_CODE, ORG_EID)");
                    sbSQL.Append(string.Format(" VALUES ( {0}, {1}, {2}, ", "~FISCAL_YEAR_" + iCnt.ToString() + "~", "~PERIOD_NUMBER_" + iCnt.ToString() + "~", "~PERIOD_START_DATE_" + iCnt.ToString() + "~"));
                    sbSQL.Append(string.Format(" {0}, {1}, {2} ) ", "~PERIOD_END_DATE_" + iCnt.ToString() + "~", "~LINE_OF_BUS_CODE_" + iCnt.ToString() + "~", "~ORG_EID_" + iCnt.ToString() + "~"));
                    dictParams.Clear();
                    dictParams.Add("FISCAL_YEAR_" + iCnt.ToString(), sFiscalYear);
                    dictParams.Add("PERIOD_NUMBER_" + iCnt.ToString(), iCnt.ToString());
                    dictParams.Add("PERIOD_START_DATE_" + iCnt.ToString(), sdtPerStart);
                    dictParams.Add("PERIOD_END_DATE_" + iCnt.ToString(), sdtPerEnd);
                    dictParams.Add("LINE_OF_BUS_CODE_" + iCnt.ToString(), sLobCode);
                    dictParams.Add("ORG_EID_" + iCnt.ToString(), sOrgId);

                    objCmd.CommandText = sbSQL.ToString();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                    objCmd.Parameters.Clear();
                    //sbSQL.Append("INSERT INTO FISCAL_YEAR_PERIOD(FISCAL_YEAR, PERIOD_NUMBER, PERIOD_START_DATE,");
                    //sbSQL.Append(" PERIOD_END_DATE, LINE_OF_BUS_CODE, ORG_EID)");
					
                    //sbSQL.Append(" VALUES ('"+sFiscalYear+ "', " + iCnt.ToString() + "," + sdtPerStart + ", ");
                    //sbSQL.Append(sdtPerEnd +", " +  sLobCode + ", " + sOrgId + ")");

                    //alSQL.Add(sbSQL.ToString());
                    //END PenTesting - srajindersin - 9th Jan 2012

					iCnt++;
				}

                //PenTesting - srajindersin - 9th Jan 2012
                objCmd.Transaction.Commit();
				//UTILITY.Save(alSQL,m_sDSN);
                //END PenTesting - srajindersin - 9th Jan 2012
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
				throw new RMAppException(Globalization.GetString("FiscalYearWizard.Save.Error",m_iClientId),p_objEx);//sonali-cloud
			}
			finally
			{
                objCmd = null;
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objTrans != null)
                {
                    objTrans.Dispose();
                }
				if (objReader!=null)
				{
					objReader.Dispose();
				}
				sbSQL=null; 
				alSQL=null;
				objNode=null;
				objNodeList=null;
			}
		}
		#endregion

	}
}
