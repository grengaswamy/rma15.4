using System;
using System.IO; 
using System.Xml; 
using Riskmaster.Db; 
using Riskmaster.Common; 
using System.Collections;
using Riskmaster.ExceptionTypes; 

namespace Riskmaster.Application.RMUtilities
{

	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   Contains WPA Auto base support classes/structures
	/// </summary>
	internal struct FilterDefinition
	{
		public int TemplateId;
		public string Name;
		public int ID;
		public int FilterType;
		public long FilterMin;
		public long FilterMax;
		public string DefValue;
		public string table;
		public string SQLFill;
		public string SQLFrom;
		public string SQLWhere;
        //rupal:r8 auto diary setup enh
        public string Database;
		public FilterDefinition[] FilterDef; 
	}

	/// <summary>
	/// Struct InfoDefinition
	/// </summary>
	internal struct InfoDefinition
	{
		public string Name;
		public int ID;
		public string AttachTable;
		public string AttachCol;
		public string AttachDesc;
		public string tmpSQL;
		public string SQL;
		public int NumFilters;
		public FilterDefinition[] FilterDef;
	}

	/// <summary>
	/// Struct FilterSetting
	/// </summary>
	internal struct FilterSetting
	{
		public string Name;
		public int Number;
		public object Data;
	}

	/// <summary>
	/// Struct InfoSetting
	/// </summary>
	internal struct InfoSetting
	{
		public string Name;
		public string DefName;
		public int Index;
		public int SendCreate;           //SEND_CREATED
		public int SendUpdate;           //SEND_UPDATED
		public int SendAdj;              //SEND_ADJUSTER
		//Start Naresh MITS 8349 07/11/2006
		public int SendAdjSupervisor;	 //SEND_ADJUSTER_SUPERVISOR
		//End Naresh MITS 8349 07/11/2006
		public int NotifyDays;           //NOTIFICATION_DAYS
		public int RouteDays;            //ROUTE_DAYS
		public int Priority;             //PRIORITY
		public int TaskEst;              //TASK_ESTIMATE
		public int TaskEstType;          //TASK_EST_TYPE
		public int TimeBillable;         //TIME_BILLABLE_FLAG
		public int ExpSchedule;          //EXPORT_SCHEDULE
		public string Instruct;          //INSTRUCTIONS
		public int NumFilters;
		public string Activities;
		public string SendUsers;
		public string NotifyUsers;
		public string RouteUsers;
		public string ProcessDate;
		public FilterSetting[] FilterSet;																															  
		public string SendGroups;
        public string NotifyGroups; //pkandhari Jira 6412
        public string RouteGroups; //pkandhari Jira 6412
		public int iDueDays;
		public int SendSuppField;
		public string SendSuppFieldName;
        public int Escalate_Highest_Level;
        public int Security_Mgmt_Relationship;
        public int ExportEmail;              //EXPORT_EMAIL, RUPAL:R8 AUTO DIARY ENH
	}

	/// <summary>
	/// Struct BestDefinition
	/// </summary>
	struct BestDefinition
	{
		public string Name;
		public int ID;
		public int DEF_ID;
		public int Priority;
		public int TaskEst;
		public int Billable;
		public int Instructions;
		public int NumFilters;
		public int Roles;
		public int LOB;
		public int Topics;
		public FilterSetting[]  FilterSet;
	}

	/// <summary>
	/// Summary description for WPAAUTO.
	/// </summary>
	public class WPAAUTO
	{

		/// <summary>
		/// Connection String
		/// </summary>
		string m_sConnStr = "";      

		/// Name			: WPAAUTO
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 28 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
        public WPAAUTO(string p_sConnStr)
		{
			m_sConnStr = p_sConnStr;
		}

		/// Name			: GetBestDef
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 28 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// gets the Best def file
		/// </summary>
		/// <param name="p_ID">Best Def Id</param>
        internal static BestDefinition GetBestDef(int p_ID, int p_iClientId, string p_sConnectionString)//rkaur27
		{
			int iPtr=0;
			string sKeyword="";
			string sValue="";
			string sFileName="";
			int iFilterNum=0;			
			bool bEnd=false;			
			StreamReader objSRdr = null;
			BestDefinition tmpBest;
			try
			{
                sFileName = String.Format(@"{0}\{1}", UTILITY.GetAppFilesDirectory("WPA"), UTILITY.GetConfigUtilitySettings(p_sConnectionString, p_iClientId)["WPABestDefFile"]);//rkaur27
				tmpBest = new BestDefinition(); 
				tmpBest.Name ="";
				tmpBest.ID =0;
				tmpBest.NumFilters =0;
				tmpBest.FilterSet = new FilterSetting[50]; 

				if (File.Exists(sFileName))
				{
					using (objSRdr = new StreamReader(sFileName)) 
					{
						string sLine = "";
						while ((sLine = objSRdr.ReadLine())!= null)
						{
							if(sLine.Trim()!="")
							{
								if(sLine.Trim().Substring(0,1)=="[")
								{
									sKeyword = sLine.Substring(1,sLine.Length-2);
									if(sLine.Substring(0,1)!="[")
									{
										iFilterNum +=1;
										tmpBest.NumFilters +=1;
										tmpBest.FilterSet[iFilterNum].Name = sKeyword.Substring(1,sKeyword.Length-2);
									}
									else
									{
										if (bEnd)
											return tmpBest;
										iFilterNum = - 1;
										tmpBest.NumFilters =0;
										tmpBest.Name = sKeyword;
									}  
								}
								else
								{
									sLine=sLine.Trim(); 
									iPtr = sLine.IndexOf("=");
									if(sLine.Substring(0,2)=="//" || iPtr<0)
										continue;
									sKeyword= sLine.Trim().ToUpper().Substring(0,iPtr -1).Trim() ;
									sValue=sLine.Substring(iPtr+1,sLine.Length - iPtr-1).Trim() ; 
									switch (sKeyword)
									{
										case "ID":
											if(p_ID.ToString() == sValue )
												bEnd = true;
											tmpBest.ID = Conversion.ConvertStrToInteger(sValue);
											break;
										case "DEF_ID":
											tmpBest.DEF_ID = Conversion.ConvertStrToInteger(sValue);
											break;
										case "PRIORITY":
										switch(sValue.ToUpper())
										{
											case "OPTIONAL":
												tmpBest.Priority =1;
												break;
											case "IMPORTANT":
												tmpBest.Priority =2;
												break;
											case "REQUIRED":
												tmpBest.Priority =3;
												break;
											default:
												tmpBest.Priority =0;
												break;
										}
											break;
										case "TIME_ESTIMATE":
											tmpBest.TaskEst = Conversion.ConvertStrToInteger(sValue)/60;
											break;
										case "BILLABLE":
											if(sValue.ToUpper()=="NO")
												tmpBest.Billable =0;
											else
												tmpBest.Billable =-1;
											break;
										case "INSTRUCTIONS":
											tmpBest.Instructions = Conversion.ConvertStrToInteger(sValue);
											break;
										case "FILTER_ID":
											tmpBest.FilterSet[iFilterNum].Number = Conversion.ConvertStrToInteger(sValue);
											break;
										case "DATA":
											tmpBest.FilterSet[iFilterNum].Data = sValue;
											break;
										case "ROLES":
											tmpBest.Roles = Conversion.ConvertStrToInteger(sValue);
											break;
										case "LOB":
											tmpBest.LOB = Conversion.ConvertStrToInteger(sValue);
											break;
										case "TOPICS":
											tmpBest.Topics = Conversion.ConvertStrToInteger(sValue);
											break;
									}
								}
							}
						}
					}
				}
				if(p_ID>tmpBest.DEF_ID)
					tmpBest.Name ="";			
				return tmpBest;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAAUTO.GetBestDef.Error", p_iClientId), p_objEx);
			}
			finally
			{
				if(objSRdr != null)
				{
					objSRdr.Close();
					objSRdr=null;
				}
			}
		}
	}
}
