﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;
using System.Collections.Generic;
using System.Text;
namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Animesh Sahai 
    ///Dated   :   22/Nov/2007
    ///Purpose :   NonAvailScreen form
    /// </summary>
    public class NonAvailScreen : UtilitiesUIBase
    {
        /// <summary>
        /// Database fields mapping string array
        /// </summary>
        private string[,] arrFields = {
			{"NonAvailGrid", "ROW_ID"},
            {"AdjusterEID","ENTITY_ID"},
			{"StartDateTime","ENTITY_NON_AVL_START"},
			{"EndDateTime","ENTITY_NON_AVL_END"}
          };
        //Rijul start: Worked for 7573 (Issue of 4634 - Epic 340)
        private string DsnName = "";
        private string m_Password = "";
        private string m_UserName = "";
        //Rijul : Worked for 7573 (Issue of 4634 - Epic 340) end
        private int m_iClientId = 0;//Add by kuladeep for Cloud. Jira-58
        /// Name		: NonAvailScreen
        /// Author		: Animesh Sahai 
        /// Date Created: 22/Nov/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default constructor with connection string
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>        
        public NonAvailScreen(string p_UserName, string p_Password, string p_sDatabaseName, string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            //Rijul start: Worked for 7573 (Issue of 4634 - Epic 340)
            m_UserName = p_UserName;
            m_Password = p_Password;
            DsnName = p_sDatabaseName;
            //Rijul : Worked for 7573 (Issue of 4634 - Epic 340) end
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;//Add by kuladeep for Cloud. Jira-58
            this.Initialize();
        }

        /// Name		: Initialize
        /// Author		: Animesh Sahai 
        /// Date Created: 22/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            TableName = "ENTITY_NON_AVL";
            KeyField = "ROW_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        /// Name		: New
        /// Author		: Animesh Sahai 
        /// Date Created: 22/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Ge the structure for a new record
        /// </summary>
        /// <param name="p_objXMLDocument">XML Doc</param>
        /// <returns>Xmldocument with structure</returns>
        public XmlDocument New(XmlDocument p_objXMLDocument)
        {
            try
            {
                return p_objXMLDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NewError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
        }

        /// Name		: Get
        /// Author		: Animesh Sahai 
        /// Date Created: 22/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm;
            string sSQL = "";
            DbConnection objConn = null;
            DbReader objDbReader = null;

            string sValue = "";
            string strRowID = string.Empty;
            try
            {
                XMLDoc = p_objXmlDocument;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowID']");
                if (objElm != null)
                    strRowID = objElm.InnerText;
                if (strRowID != "")
                {
                    //PenTesting - srajindersin - 9th Jan 2012
                    sSQL = String.Format("SELECT * FROM ENTITY_NON_AVL WHERE ROW_ID = {0}","~ROW_ID~");

                    Dictionary<string, object> dictParams = new Dictionary<string, object>();
                    dictParams.Add("ROW_ID", strRowID);

                    objConn = DbFactory.GetDbConnection(ConnectString);
                    objConn.Open();
                    objDbReader = DbFactory.ExecuteReader(ConnectString, sSQL, dictParams);

                    //sSQL = String.Format("SELECT * FROM ENTITY_NON_AVL WHERE ROW_ID = " + strRowID);

                    //objConn = DbFactory.GetDbConnection(ConnectString);
                    //objConn.Open();
                    //objDbReader = objConn.ExecuteReader(sSQL);
                    //END PenTesting - srajindersin - 9th Jan 2012
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            sValue = objDbReader["ENTITY_NON_AVL_START"].ToString();
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='sDate']");
                            if (objElm != null)
                                objElm.InnerText = Conversion.ToDate(sValue.Substring(0, 8).ToString()).ToShortDateString();
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='sTime']");
                            if (objElm != null)
                                objElm.InnerText = Conversion.GetTime(sValue.Substring(8).ToString()).ToString();

                            sValue = objDbReader["ENTITY_NON_AVL_END"].ToString();
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='eDate']");
                            if (objElm != null)
                                objElm.InnerText = Conversion.ToDate(sValue.Substring(0, 8).ToString()).ToShortDateString();
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='eTime']");
                            if (objElm != null)
                                objElm.InnerText = Conversion.GetTime(sValue.Substring(8).ToString()).ToString();
                        }
                        objDbReader.Close();
                    }
                    objConn.Dispose();

                    //*****************************
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NAGetError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
            finally
            {
                objElm = null;
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }

        }

        /// Name		: Delete
        /// Author		: Animesh Sahai 
        /// Date Created: 22/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes a particular record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>next record if any</returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            XMLDoc = p_objXmlDocument;
            //XmlElement objElm = null;
            AdjusterScreen objAdjScreen = new AdjusterScreen(m_UserName, m_Password, DsnName, ConnectString, m_iClientId);//Add by kuladeep for Cloud. Jira-58  //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
            XmlElement objElm = null;
            try
            {
                base.Delete();
                objElm = (XmlElement)XMLDoc.SelectSingleNode("//control[@name='blnPostBack']");
                if (objElm != null)
                    objElm.SetAttribute("value", "No");
                XMLDoc = objAdjScreen.ReloadNonAvailPlan(XMLDoc);
                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NADeleteError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
            finally
            {
                objElm = null;
                objAdjScreen = null;
            }
        }

        /// Name		: Save
        /// Author		: Animesh Sahai 
        /// Date Created: 22/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string sAdjID = "";
            string sRowId = "";
            string sStartDate = "";
            string sStartTime = "";
            string sEndDate = "";
            string sEndTime = "";
            DateTime dtmStart = new DateTime();
            DateTime dtmEnd = new DateTime();
            string sSQL = "";
            XmlElement objElm = null;
            DbConnection objConn = null;

            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            DbCommand objCmd = null;
            //END PenTesting - srajindersin - 9th Jan 2012
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AdjusterID']");
                if (objElm != null)
                    sAdjID = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowID']");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='sDate']");
                if (objElm != null)
                    sStartDate = objElm.InnerText;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='sTime']");
                if (objElm != null)
                    sStartTime = objElm.InnerText;
                if (sStartTime == "")
                {
                    sStartTime = "00:00:00";
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='eDate']");
                if (objElm != null)
                    sEndDate = objElm.InnerText;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='eTime']");
                if (objElm != null)
                    sEndTime = objElm.InnerText;
                if (sEndTime == "")
                {
                    sEndTime = "23:59:00";
                }

                dtmStart = Conversion.ToDate(Conversion.GetDate(sStartDate) + Conversion.GetTime(sStartTime));
                dtmEnd = Conversion.ToDate(Conversion.GetDate(sEndDate) + Conversion.GetTime(sEndTime));
                objConn = DbFactory.GetDbConnection(ConnectString);
                objConn.Open();

                //PenTesting - srajindersin - 9th Jan 2012
                objCmd = objConn.CreateCommand();
                //END PenTesting - srajindersin - 9th Jan 2012

                if (sRowId != "")
                {
                    //PenTesting - srajindersin - 9th Jan 2012
                    sSQL = string.Format("DELETE FROM ENTITY_NON_AVL WHERE ROW_ID = {0}", "~ROW_ID~");
                    objCmd.CommandText = sSQL;

                    dictParams.Add("ROW_ID", sRowId);
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                    objCmd.Parameters.Clear();
                    //sSQL = "DELETE FROM ENTITY_NON_AVL WHERE ROW_ID =" + sRowId;
                    //objConn.ExecuteNonQuery(sSQL);
                    //END PenTesting - srajindersin - 9th Jan 2012
                }

                long lngRowID = Utilities.GetNextUID(ConnectString, "ENTITY_NON_AVL", m_iClientId);

                //PenTesting - srajindersin - 9th Jan 2012
                strSQL = new StringBuilder();
                strSQL.Append("INSERT INTO ENTITY_NON_AVL(ROW_ID,ENTITY_ID,ENTITY_NON_AVL_START,ENTITY_NON_AVL_END)");
                strSQL.Append(string.Format(" VALUES ( {0},{1},{2},{3} )", "~ENTITY_ROW_ID~", "~ENTITY_ID~", "~ENTITY_NON_AVL_START~", "~ENTITY_NON_AVL_END~"));
                objCmd.CommandText = strSQL.ToString();

                dictParams.Clear();
                dictParams.Add("ENTITY_ROW_ID", lngRowID);
                dictParams.Add("ENTITY_ID", sAdjID);
                dictParams.Add("ENTITY_NON_AVL_START", Conversion.ToDbDateTime(dtmStart));
                dictParams.Add("ENTITY_NON_AVL_END", Conversion.ToDbDateTime(dtmEnd));
                DbFactory.ExecuteNonQuery(objCmd, dictParams);
                objCmd.Parameters.Clear();
                //sSQL = "INSERT INTO ENTITY_NON_AVL(ROW_ID,ENTITY_ID,ENTITY_NON_AVL_START,ENTITY_NON_AVL_END) VALUES ( " + lngRowID + "," + sAdjID + ",'" + Conversion.ToDbDateTime(dtmStart) + "','" + Conversion.ToDbDateTime(dtmEnd) + "')";
                //objConn.ExecuteNonQuery(sSQL);
                //END PenTesting - srajindersin - 9th Jan 2012

                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NASaveError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }


    }
}
