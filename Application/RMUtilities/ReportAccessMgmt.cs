using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Report Access Management information.
	/// </summary>
	public class ReportAccessMgmt
	{
		#region Input/Output Xml
		/*
			<ReportsAccess>
				<Reports>
					<option value="2_314" ReportName="Executive Summary Report" ReportDesc="Not Specified" User="p (parag sarin)">Executive Summary Report - Not Specified</option>
				</Reports>
				<Filters>
					<option value="0">All Users</option>
					<option value="2">p (parag sarin)</option>
					<option value="3">t (test test)</option>
					<option value="4">m (manager manager)</option>
				</Filters>
			</ReportsAccess>
		*/
		#endregion


		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// Security DB Connection string
		/// </summary>
		private string m_sSecDSN="";

        private int m_iClientId = 0;

			
		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		/// <summary>
		/// Security DB Connection string
		/// </summary>
		public string SecDSN
		{
			set
			{
				m_sSecDSN=value;
			}	

		}
		#endregion

		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
        public ReportAccessMgmt(int p_iClientId) 
        {
            m_iClientId = p_iClientId;
        }
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		/// <param name="p_sSecureDSN">Security Connection string</param>
        public ReportAccessMgmt(string p_sDSN, string p_sSecureDSN, int p_iClientId)
		{
			m_sDSN=p_sDSN;
			m_sSecDSN=p_sSecureDSN;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Public Functions
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves Report Access Management information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			ArrayList objQuery=null;
			string sQuery="";
			bool bReturnValue=false;
			string sTemp="";
			string sUserId="";
			string sReportId="";
			XmlNode objTempNode=null;
			string[] arrValues=null;
			try
			{
				objQuery=new ArrayList();
				objTempNode=p_objXmlDocument.SelectSingleNode("/ReportsAccess/Reports");
				if (objTempNode!=null)
				{
					if (objTempNode.InnerText!=null)
					{
						arrValues=objTempNode.InnerText.Split(" ".ToCharArray());
					}
					for (int i=0;i<arrValues.Length;i++)
					{
						sTemp=arrValues[i];
						sUserId=arrValues[i].Split("_".ToCharArray())[0];
						sReportId=arrValues[i].Split("_".ToCharArray())[1];
						sQuery="DELETE FROM SM_REPORTS_PERM WHERE USER_ID ="+sUserId+" AND REPORT_ID="+sReportId;
						objQuery.Add(sQuery);
					}
				}
                UTILITY.Save(objQuery, m_sDSN, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ReportAccessMgmt.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objQuery=null;
				objTempNode=null;
				arrValues=null;
			}
			return bReturnValue;
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get(XmlDocument p_objDOM)
		{
			DbReader objReader=null;
			XmlNode objNode=null;
			string sUser="";
			string sReportName="";
			string sReportDesc="";
			XmlDocument objDOM=null;
			StringBuilder sbSql=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sLName="";
			string sName="";
			string sFName="";
			Hashtable htUsers=null;
			IDictionaryEnumerator objEnum=null;
			try
			{
				objNode=p_objDOM.SelectSingleNode("/ReportsAccess/User");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sUser=objNode.InnerText;
					}	
				}
                //start rsushilaggar 16/06/2010 MITS 11941
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("ReportsAccess");
                objDOM.AppendChild(objElemParent);

                objElemChild = objDOM.CreateElement("Filters");
                objDOM.FirstChild.AppendChild(objElemChild);
                objElemTemp = objDOM.CreateElement("option");
                objElemTemp.SetAttribute("value", "0");
                objElemTemp.InnerText = "All Users";
                objElemChild.AppendChild(objElemTemp);

                sbSql = new StringBuilder();
                sbSql.Append("SELECT DISTINCT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME");
                sbSql.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                
                sbSql.Append(" ORDER BY USER_DETAILS_TABLE.LOGIN_NAME");
                //sbSql.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
                objReader = DbFactory.GetDbReader(m_sSecDSN, sbSql.ToString());
                htUsers = new Hashtable();
                while (objReader.Read())
                {
                    sLName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                    sFName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                    sLName = sFName + " " + sLName;
                    sName = Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
                    sName = sName + " (" + sLName + ")";
                    try
                    {
                        htUsers.Add(Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")), sName);
                        objElemTemp = objDOM.CreateElement("option");
                        objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
                        objElemTemp.InnerText = sName;
                        objElemChild.AppendChild(objElemTemp);
                    }
                    catch (Exception p_objException)
                    {
                    }
                }
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                }

				sbSql=new StringBuilder();
				sbSql.Append("SELECT USER_ID, SM_REPORTS.REPORT_ID, REPORT_NAME, REPORT_DESC FROM SM_REPORTS, SM_REPORTS_PERM WHERE SM_REPORTS_PERM.REPORT_ID = SM_REPORTS.REPORT_ID");
				if (!sUser.Trim().Equals("") && (!sUser.Trim().Equals("0")))
				sbSql.Append(" AND USER_ID="+sUser);
				sbSql.Append(" ORDER BY USER_ID,REPORT_NAME");
				
				objElemChild=objDOM.CreateElement("Reports");
				objDOM.FirstChild.AppendChild(objElemChild);
				objReader = DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				while (objReader.Read())
				{	
					objElemTemp=objDOM.CreateElement("option");
					objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"))+"_"+Conversion.ConvertObjToStr(objReader.GetValue("REPORT_ID")));
					sReportName=Conversion.ConvertObjToStr(objReader.GetValue("REPORT_NAME"));
					sReportDesc=Conversion.ConvertObjToStr(objReader.GetValue("REPORT_DESC"));
					objElemTemp.SetAttribute("ReportName",sReportName);
					objElemTemp.SetAttribute("ReportDesc",sReportDesc);
					if (htUsers.ContainsKey(Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"))))
					{
						objElemTemp.SetAttribute("User",htUsers[Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"))].ToString());
					}
					objElemTemp.InnerText=sReportName+" - " +sReportDesc;
					objElemChild.AppendChild(objElemTemp);
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}

                //End rsushilaggar 16/06/2010 MITS 11941
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ReportAccessMgmt.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Dispose();
				}
				objDOM=null;
                objNode = null;
				sbSql=null;
				objElemParent=null;
				objElemChild=null;
				objEnum=null;
				objElemTemp=null;
                htUsers=null;
               
			}
		}
		#endregion
	}
}
