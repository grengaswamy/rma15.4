﻿using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
    public class TPACodeMapping
    {
        string m_sConnString = string.Empty;
        private int m_iClientId = 0;
        public TPACodeMapping(string p_sConnectString, int p_iClientId)
		{
			m_sConnString=p_sConnectString;
            m_iClientId = p_iClientId;            
		}

        public XmlDocument GetTPASystem(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            String sSql = null;
            string sValue = string.Empty;
            try
            {
                //rupal:start                
                sSql = "SELECT TPA_ROW_ID,TPA_SYSTEM FROM TPA_SYSTEM_NAME ORDER BY TPA_SYSTEM";
                //rupal:end
                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("TPACodeMapping");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("SystemName");
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("option");
                    sValue = Conversion.ConvertObjToStr(objReader.GetValue("TPA_ROW_ID"));
                    objElemTemp.SetAttribute("value", sValue);
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TPA_SYSTEM"));
                    objElemChild.AppendChild(objElemTemp);

                }

                objDOM.FirstChild.AppendChild(objElemChild);
                objReader.Close();

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.GetTPASystem.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }

        }

        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlNode objElem = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            String sSql = null;
            string sValue = string.Empty;
            string sTPAName = string.Empty;
            //added by swati
            string sProcessName = string.Empty;
            try
            {
                objElem = p_objXmlDocument.SelectSingleNode("TPATypeCode");

                sTPAName = p_objXmlDocument.SelectSingleNode("//TPATypeCode/SelectedTPAType").InnerText; 

                //added by swati
                if (p_objXmlDocument.SelectSingleNode("//TPATypeCode/SelectedProcessType") != null)
                {
                    sProcessName = p_objXmlDocument.SelectSingleNode("//TPATypeCode/SelectedProcessType").InnerText;
                }
                //if condn added by swati
                if (!string.IsNullOrEmpty(sProcessName))
                {
                    if (!string.IsNullOrEmpty(sTPAName) && string.Compare(sTPAName, "-1") != 0)
                    {
                        sSql = "SELECT RMX_TABLE_ID,GLOSSARY_TEXT.TABLE_NAME FROM TPI_MAP_TABLES,GLOSSARY_TEXT WHERE TPI_MAP_TABLES.RMX_TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND TPI_MAP_TABLES.TPI_SYSTEM_ID = " + sTPAName + " ORDER BY GLOSSARY_TEXT.TABLE_NAME";
                        //rupal:end
                        objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objDOM = new XmlDocument();
                        objElemParent = objDOM.CreateElement("TPACodeMapping");
                        objDOM.AppendChild(objElemParent);
                        objElemChild = objDOM.CreateElement("CodeTypes");
                        while (objReader.Read())
                        {
                            objElemTemp = objDOM.CreateElement("option");
                            //rupal:start
                            //objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID")));
                            sValue = Conversion.ConvertObjToStr(objReader.GetValue("RMX_TABLE_ID"));
                            //objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("RMX_TABLE_ID")));                    
                            objElemTemp.SetAttribute("value", sValue);
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME"));
                            //rupal:end
                            objElemChild.AppendChild(objElemTemp);

                        }

                        objDOM.FirstChild.AppendChild(objElemChild);
                        objReader.Close();
                    }
                    //else condn added by swati
                    else 
                    {
                        sSql = "SELECT GLOSSARY.TABLE_ID, TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND GLOSSARY_TYPE_CODE IN (1,2,3,1064) ORDER BY TABLE_NAME ASC";
                        //rupal:end
                        objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objDOM = new XmlDocument();
                        objElemParent = objDOM.CreateElement("TPACodeMapping");
                        objDOM.AppendChild(objElemParent);
                        objElemChild = objDOM.CreateElement("CodeTypes");
                        while (objReader.Read())
                        {
                            objElemTemp = objDOM.CreateElement("option");
                            //rupal:start
                            //objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID")));
                            sValue = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID"));
                            //objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("RMX_TABLE_ID")));                    
                            objElemTemp.SetAttribute("value", sValue);
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME"));
                            //rupal:end
                            objElemChild.AppendChild(objElemTemp);

                        }

                        objDOM.FirstChild.AppendChild(objElemChild);
                        objReader.Close();
                    }
                }


            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }
            return objDOM;

        }

        public XmlDocument GetCodeDetails(XmlDocument p_objXmlDocument)
        {
            XmlNode objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objDbReader = null;
            String sSql = null;
            string sTableName = string.Empty;
            string sAvailableTPA = string.Empty;
            string sCodeType = string.Empty;
            string sPSTableID = string.Empty;
            string sTableNmsuffix = string.Empty, sTableNm = string.Empty;
            //Anu Tennyson added multiple relationship starts
            int iMultipleMapping = 0;
            //Anu Tennyson Ends
            bool bIsSuccess = false;
            string sMappingTablePrefix = string.Empty; 
            LocalCache objcache = null;
            string sTPAName = string.Empty;
            //added by swati
            string sProcessType = string.Empty;
            int iRMTableId = 0;
            int iTableType = 0;
            string sRMTable = string.Empty;
            //end by swati
            try
            {
                objElemParent = p_objXmlDocument.SelectSingleNode("TPACodeMapping");
                sCodeType = p_objXmlDocument.SelectSingleNode("//TPACodeMapping/SelectedCodeType").InnerText;
                sTPAName = p_objXmlDocument.SelectSingleNode("//TPACodeMapping/SelectedTPAType").InnerText;

                objcache = new LocalCache(m_sConnString, m_iClientId);
                int iStateTableId = objcache.GetTableId("STATES");

                //added by swati
                sProcessType = p_objXmlDocument.SelectSingleNode("//TPACodeMapping/SelectedProcessType").InnerText;
                //change end here by swati

                if (!string.IsNullOrEmpty(sCodeType) && !string.IsNullOrEmpty(sTPAName))
                {
                    sSql = "SELECT SYSTEM_TABLE_NAME, MULTIPLE_MAPPING FROM TPI_MAP_TABLES, GLOSSARY WHERE RMX_TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = " + sCodeType + " AND TPI_MAP_TABLES.TPI_SYSTEM_ID = " + sTPAName;
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                    if (objDbReader.Read())
                    {
                        sTableNmsuffix = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                        //Anu Tennyson added multiple relationship starts
                        iMultipleMapping = Conversion.CastToType<int>(Convert.ToString(objDbReader.GetValue(1)), out bIsSuccess);
                        //Anu Tennyson Ends
                    }
                    objDbReader.Close();

                    sSql = "SELECT TPA_SYSTEM_NAME FROM TPA_SYSTEM_NAME WHERE TPA_ROW_ID = " + sTPAName;
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                    if (objDbReader.Read())
                    {
                        sAvailableTPA = Conversion.ConvertObjToStr(objDbReader.GetValue(0));

                    }
                    objDbReader.Close();

                    sTableNm = sAvailableTPA + "_" + sTableNmsuffix.Trim();

                    sSql = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sTableNm + "'";
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                    if (objDbReader.Read())
                    {
                        sPSTableID = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                    }
                    if (sPSTableID == string.Empty)
                        sPSTableID = "0";

                    //added by swati
                    if (Convert.ToInt32(sCodeType) == iStateTableId)
                    {
                        sSql = string.Empty;
                        sSql = "SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE STATE_ID IS NOT NULL";
                        objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objElemChild = p_objXmlDocument.CreateElement("RMCodes");
                        while (objDbReader.Read())
                        {
                            objElemTemp = p_objXmlDocument.CreateElement("option");
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_ROW_ID")));
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_NAME")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_ID"));
                            objElemChild.AppendChild(objElemTemp);


                        }
                    }
                    //end by swati
                    //else added by swati
                    else
                    {
                        //sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT RMX_CODE_ID FROM TPI_CODE_MAPPING WHERE RMX_TABLE_ID = " + sCodeType + ") AND GLOSSARY.TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                        sSql = string.Empty;
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                        objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objElemChild = p_objXmlDocument.CreateElement("RMCodes");
                        while (objDbReader.Read())
                        {
                            objElemTemp = p_objXmlDocument.CreateElement("option");
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_ID")));
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_DESC")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("SHORT_CODE"));
                            objElemChild.AppendChild(objElemTemp);


                        }
                    }
                    p_objXmlDocument.FirstChild.AppendChild(objElemChild);

                    objElemChild = null;
                    objDbReader.Close();

                    sSql = string.Empty;

                    //Anu Tennyson added multiple relationship starts
                    //sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT TPI_CODE_ID FROM TPI_CODE_MAPPING) AND  GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC";
                    if (iMultipleMapping == 0)
                    {
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT TPI_CODE_ID FROM TPI_CODE_MAPPING WHERE TPI_SYSTEM_ID = " + sTPAName + ") AND  GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC";
                    }
                    else
                    {
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC";
                    }
                    //Anu Tennyson Ends
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                    objElemChild = p_objXmlDocument.CreateElement("TPASysCodes");
                    while (objDbReader.Read())
                    {
                        objElemTemp = p_objXmlDocument.CreateElement("option");
                        objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_ID")));
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_DESC")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("SHORT_CODE"));
                        objElemChild.AppendChild(objElemTemp);
                    }

                    p_objXmlDocument.FirstChild.AppendChild(objElemChild);

                    objDbReader.Close();

                    sSql = string.Empty;
                    sSql = "SELECT TPI_CODE_MAPPING.ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RM.CODE_DESC RM_CODE,TPA.CODE_DESC TPA_SYS FROM TPI_CODE_MAPPING,CODES_TEXT RM,CODES_TEXT TPA WHERE RMX_TABLE_ID = " + sCodeType;
                    sSql = sSql + " AND TPI_SYSTEM_ID = " + sTPAName;
                    sSql = sSql + " AND RM.CODE_ID = RMX_CODE_ID AND TPA.CODE_ID = TPI_CODE_ID";
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                    string sKey = string.Empty;
                    while (objDbReader.Read())
                    {
                        objElemTemp = p_objXmlDocument.CreateElement("MappedCodes");
                        objElemTemp.SetAttribute("RMCode", Conversion.ConvertObjToStr(objDbReader.GetValue("RM_CODE")));
                        objElemTemp.SetAttribute("TPASysCode", Conversion.ConvertObjToStr(objDbReader.GetValue("TPA_SYS")));
                        objElemTemp.SetAttribute("RowId", Conversion.ConvertObjToStr(objDbReader.GetValue("ROW_ID")));
                        sKey = Conversion.ConvertObjToStr(objDbReader.GetValue("RMX_CODE_ID"));
                        objElemTemp.SetAttribute("Key", sKey);
                        p_objXmlDocument.FirstChild.AppendChild(objElemTemp);
                    }


                }
                //added by swati for all other process systems other than TPA
                else if(!string.IsNullOrEmpty(sCodeType) && !string.IsNullOrEmpty(sProcessType))
                {
                    sSql = "SELECT RMX_TABLE_ID, MULTIPLE_MAPPING FROM TPI_MAP_TABLES WHERE TPI_TABLE_ID = " + sCodeType + " AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                    if (objDbReader.Read())
                    {
                        iRMTableId = Conversion.ConvertObjToInt(objDbReader.GetValue(0), m_iClientId);
                        //swati added multiple relationship starts
                        iMultipleMapping = Conversion.CastToType<int>(Convert.ToString(objDbReader.GetValue(1)), out bIsSuccess);
                        //swati Ends

                    }

                    //added by swati
                    if (Convert.ToInt32(iRMTableId) == iStateTableId)
                    {
                        sSql = string.Empty;
                        sSql = "SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE STATE_ID IS NOT NULL";
                        objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objElemChild = p_objXmlDocument.CreateElement("RMCodes");
                        while (objDbReader.Read())
                        {
                            objElemTemp = p_objXmlDocument.CreateElement("option");
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_ROW_ID")));
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_NAME")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_ID"));
                            objElemChild.AppendChild(objElemTemp);


                        }
                    }
                    //end by swati
                    //else added by swati
                    else
                    {
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + iRMTableId + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                        objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objElemChild = p_objXmlDocument.CreateElement("RMCodes");
                        while (objDbReader.Read())
                        {
                            objElemTemp = p_objXmlDocument.CreateElement("option");
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_ID")));
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_DESC")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("SHORT_CODE"));
                            objElemChild.AppendChild(objElemTemp);


                        }
                    }
                    p_objXmlDocument.FirstChild.AppendChild(objElemChild);

                    objElemChild = null;
                    objDbReader.Close();

                    sSql = string.Empty;
                    
                    //added by swati
                    if (Convert.ToInt32(sCodeType) == iStateTableId)
                    {
                        sSql = string.Empty;
                        if (iMultipleMapping == 0)
                        {
                            sSql = "SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE STATE_ID IS NOT NULL AND STATE_ROW_ID NOT IN (SELECT TPI_CODE_ID FROM TPI_CODE_MAPPING WHERE INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType) + ")";
                        }
                        else
                        {
                            sSql = "SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE STATE_ID IS NOT NULL";
                        }
                        objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objElemChild = p_objXmlDocument.CreateElement("TPASysCodes");
                        while (objDbReader.Read())
                        {
                            objElemTemp = p_objXmlDocument.CreateElement("option");
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_ROW_ID")));
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_NAME")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("STATE_ID"));
                            objElemChild.AppendChild(objElemTemp);


                        }
                    }
                    else
                    {
                        sSql = string.Empty;
                        //Anu Tennyson added multiple relationship starts
                        //sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT TPI_CODE_ID FROM TPI_CODE_MAPPING WHERE INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType) + ") AND GLOSSARY.TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                        if (iMultipleMapping == 0)
                        {
                            sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT TPI_CODE_ID FROM TPI_CODE_MAPPING WHERE INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType) + ") AND GLOSSARY.TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                        }
                        else
                        {
                            sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                        }
                        //Anu Tennyson Ends
                        objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        objElemChild = p_objXmlDocument.CreateElement("TPASysCodes");
                        while (objDbReader.Read())
                        {
                            objElemTemp = p_objXmlDocument.CreateElement("option");
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_ID")));
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_DESC")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("SHORT_CODE"));
                            objElemChild.AppendChild(objElemTemp);


                        }
                    }
                    p_objXmlDocument.FirstChild.AppendChild(objElemChild);

                    objElemChild = null;
                    objDbReader.Close();

                    sSql = string.Empty;
                    if (Convert.ToInt32(sCodeType) == iStateTableId)
                    {
                        if (iRMTableId == iStateTableId)
                        {
                            if (DbFactory.GetDatabaseType(m_sConnString) == eDatabaseType.DBMS_IS_ORACLE)
                            {
                                sSql = "SELECT TPI_CODE_MAPPING.ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RM.STATE_ID || ' ' || RM.STATE_NAME RM_CODE,TPA.STATE_ID || ' ' || TPA.STATE_NAME TPA_SYS FROM TPI_CODE_MAPPING,STATES RM,STATES TPA WHERE RMX_TABLE_ID = " + sCodeType;
                                sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                                sSql = sSql + " AND RM.STATE_ROW_ID = RMX_CODE_ID AND TPA.STATE_ROW_ID = TPI_CODE_ID";
                            }
                            else
                            {
                                sSql = "SELECT TPI_CODE_MAPPING.ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RM.STATE_ID + ' ' + RM.STATE_NAME RM_CODE,TPA.STATE_ID + ' ' + TPA.STATE_NAME TPA_SYS FROM TPI_CODE_MAPPING,STATES RM,STATES TPA WHERE RMX_TABLE_ID = " + sCodeType;
                                sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                                sSql = sSql + " AND RM.STATE_ROW_ID = RMX_CODE_ID AND TPA.STATE_ROW_ID = TPI_CODE_ID";
                            }
                        }
                        else
                        {
                            if (DbFactory.GetDatabaseType(m_sConnString) == eDatabaseType.DBMS_IS_ORACLE)
                            {
                                sSql = "SELECT TPI_CODE_MAPPING.ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RM.SHORT_CODE || ' ' || RM.CODE_DESC RM_CODE,TPA.STATE_ID || ' ' || TPA.STATE_NAME TPA_SYS FROM TPI_CODE_MAPPING,CODES_TEXT RM,STATES TPA WHERE RMX_TABLE_ID = " + sCodeType;
                                sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                                sSql = sSql + " AND RM.CODE_ID = RMX_CODE_ID AND TPA.STATE_ROW_ID = TPI_CODE_ID";
                            }
                            else
                            {
                                sSql = "SELECT TPI_CODE_MAPPING.ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RM.SHORT_CODE + ' ' + RM.CODE_DESC RM_CODE,TPA.STATE_ID + ' ' + TPA.STATE_NAME TPA_SYS FROM TPI_CODE_MAPPING,CODES_TEXT RM,STATES TPA WHERE RMX_TABLE_ID = " + sCodeType;
                                sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                                sSql = sSql + " AND RM.CODE_ID = RMX_CODE_ID AND TPA.STATE_ROW_ID = TPI_CODE_ID";
                            }
                        }
                    }
                    else if (iRMTableId == iStateTableId) //neha goel added condition for mapping DCI NCCI Jurisdiction codes table with rmA States table
                    {
                        if (DbFactory.GetDatabaseType(m_sConnString) == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            sSql = "SELECT TC.ROW_ID,TC.RMX_CODE_ID,TC.TPI_CODE_ID,C.SHORT_CODE || ' ' || CT.CODE_DESC RM_CODE, RM.STATE_ID || ' ' || RM.STATE_NAME TPA_SYS FROM TPI_CODE_MAPPING TC, CODES C,CODES_TEXT CT,STATES RM WHERE TC.RMX_TABLE_ID = " + sCodeType;
                            sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                            sSql = sSql + " AND C.CODE_ID = TPI_CODE_ID AND CT.CODE_ID = C.CODE_ID AND RM.STATE_ROW_ID = RMX_CODE_ID";
                        }
                        else
                        {
                            sSql = "SELECT TC.ROW_ID,TC.RMX_CODE_ID,TC.TPI_CODE_ID,C.SHORT_CODE + ' ' + CT.CODE_DESC RM_CODE, RM.STATE_ID + ' '+ RM.STATE_NAME TPA_SYS FROM TPI_CODE_MAPPING TC, CODES C,CODES_TEXT CT,STATES RM WHERE TC.RMX_TABLE_ID = " + sCodeType;
                            sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                            sSql = sSql + " AND C.CODE_ID = TPI_CODE_ID AND CT.CODE_ID = C.CODE_ID AND RM.STATE_ROW_ID = RMX_CODE_ID";
                        }
                    }
                    else
                    {
                        if (DbFactory.GetDatabaseType(m_sConnString) == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            sSql = "SELECT TPI_CODE_MAPPING.ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RM.SHORT_CODE || ' ' || RM.CODE_DESC RM_CODE,TPA.SHORT_CODE || ' ' || TPA.CODE_DESC TPA_SYS FROM TPI_CODE_MAPPING,CODES_TEXT RM,CODES_TEXT TPA WHERE RMX_TABLE_ID = " + sCodeType;
                            sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                            sSql = sSql + " AND RM.CODE_ID = RMX_CODE_ID AND TPA.CODE_ID = TPI_CODE_ID";
                        }
                        else
                        {
                            sSql = "SELECT TPI_CODE_MAPPING.ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RM.SHORT_CODE + ' ' + RM.CODE_DESC RM_CODE,TPA.SHORT_CODE + ' ' + TPA.CODE_DESC TPA_SYS FROM TPI_CODE_MAPPING,CODES_TEXT RM,CODES_TEXT TPA WHERE RMX_TABLE_ID = " + sCodeType;
                            sSql = sSql + " AND TPI_SYSTEM_ID = 0 AND INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessType);
                            sSql = sSql + " AND RM.CODE_ID = RMX_CODE_ID AND TPA.CODE_ID = TPI_CODE_ID";
                        }
                    }
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                    string sKey = string.Empty;
                    while (objDbReader.Read())
                    {
                        objElemTemp = p_objXmlDocument.CreateElement("MappedCodes");
                        objElemTemp.SetAttribute("RMCode", Conversion.ConvertObjToStr(objDbReader.GetValue("RM_CODE")));
                        objElemTemp.SetAttribute("TPASysCode", Conversion.ConvertObjToStr(objDbReader.GetValue("TPA_SYS")));
                        objElemTemp.SetAttribute("RowId", Conversion.ConvertObjToStr(objDbReader.GetValue("ROW_ID")));
                        sKey = Conversion.ConvertObjToStr(objDbReader.GetValue("RMX_CODE_ID"));
                        objElemTemp.SetAttribute("Key", sKey);
                        p_objXmlDocument.FirstChild.AppendChild(objElemTemp);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                if (objcache != null)
                    objcache.Dispose();
            }

            return p_objXmlDocument;

        }

        public bool SaveTPACodeInfo(XmlDocument p_objInputXMLDoc, out string sError)
        {
            string sSQL = "";
            DbConnection objCon = null;
            XmlElement objTPASysXMLEle = null;
            string sTPASystemId = string.Empty;
            string sRMCodeId = string.Empty;
            string sTPASysCodeId = string.Empty;
            string sCodeType = string.Empty;
            //added by swati
            string sProcessId = string.Empty;
            int iMappingCount = 0;
            //change end here by swati
            try
            {
                objTPASysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//TPACodeMapping/AddCode");
                sTPASystemId = objTPASysXMLEle.GetElementsByTagName("TPASystemName").Item(0).InnerText;
                sRMCodeId = objTPASysXMLEle.GetElementsByTagName("RMCode").Item(0).InnerText;
                sTPASysCodeId = objTPASysXMLEle.GetElementsByTagName("TPASysCode").Item(0).InnerText;
                //if condn added by swati
                if (string.IsNullOrEmpty(sTPASystemId))
                {
                    sTPASystemId = "0";
                }
                sCodeType = objTPASysXMLEle.GetElementsByTagName("CodeType").Item(0).InnerText;
                //added by swati
                sProcessId = objTPASysXMLEle.GetElementsByTagName("ProcessType").Item(0).InnerText;
                if (string.IsNullOrEmpty(sProcessId))
                {
                    sProcessId = "0";
                }

                sSQL = "SELECT COUNT(*) FROM TPI_CODE_MAPPING WHERE RMX_CODE_ID = " + sRMCodeId + " AND TPI_CODE_ID = " + sTPASysCodeId + " AND RMX_TABLE_ID = " +
                    sCodeType + " AND TPI_SYSTEM_ID = " + sTPASystemId + " AND INTERFACE_ROW_ID = " + sProcessId;
                objCon = DbFactory.GetDbConnection(m_sConnString);
                objCon.Open();
                iMappingCount = Conversion.ConvertObjToInt(objCon.ExecuteScalar(sSQL), m_iClientId);

                if (iMappingCount > 0)
                {
                    sError = "Code Mapping already exists.";
                }
                else
                {
                    //INTERFACE_ROW_ID added by swati for other process types
                    sSQL = string.Empty;
                    //sSQL = "INSERT INTO TPI_CODE_MAPPING(ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RMX_TABLE_ID,TPI_SYSTEM_ID)" +
                    sSQL = "INSERT INTO TPI_CODE_MAPPING(ROW_ID,RMX_CODE_ID,TPI_CODE_ID,RMX_TABLE_ID,TPI_SYSTEM_ID,INTERFACE_ROW_ID)" +
                                "VALUES(" + Utilities.GetNextUID(m_sConnString, "TPI_CODE_MAPPING", m_iClientId) + "," + sRMCodeId + "," + sTPASysCodeId + "," +
                        //sCodeType + "," + sTPASystemId + ")";
                                sCodeType + "," + sTPASystemId + "," + sProcessId + ")";


                    objCon = DbFactory.GetDbConnection(m_sConnString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);
                    sError = string.Empty;                    
                }
                //change end here by swati
                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.Save.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null)
                    objCon.Close();
                    objCon.Dispose();

                if (objTPASysXMLEle != null)
                    objTPASysXMLEle = null;

            }
        }

        public XmlDocument DeleteTPACodeMapping(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            XmlElement objElm = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            //added by swati
            string sProcessId = string.Empty;
            //change end here by swati
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//DeletedCode");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                //added by swati
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//ProcessType");
                if (objElm != null)
                    sProcessId = objElm.InnerText;
                if (string.IsNullOrEmpty(sProcessId))
                    sProcessId = "0";
                //change end here by swati

                objConn = DbFactory.GetDbConnection(m_sConnString);
                objConn.Open();

                if (sRowId != "")
                {
                    sSQL = "DELETE FROM TPI_CODE_MAPPING  WHERE ROW_ID IN (" + sRowId + ") AND INTERFACE_ROW_ID = " + sProcessId;
                }
                objConn.ExecuteNonQuery(sSQL);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.Delete.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        
        public bool AddNewMapping(XmlDocument p_objInputXMLDoc, out string sError)
        {
            string sSQL = "";
            string sSQLTPA = "";
            DbConnection objCon = null;
            DbConnection objConn = null;
            XmlElement objTPASysXMLEle = null;
            string sTPASystemName = string.Empty;
            string sTPAName = string.Empty;
            string sLossCode = string.Empty;
            string sReserveType = string.Empty;
            string sReserveAmount = string.Empty;
            string sCoverageLossLOBID = string.Empty;
            int iTPACount = 0;
            int iTPAId = 0;

            try
            {
                objTPASysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//TPACodeMappingAdaptor/AddCode");
                //spahariya MITS 30911
                sTPASystemName = objTPASysXMLEle.GetElementsByTagName("TPASystemName").Item(0).InnerText;
                sTPAName = objTPASysXMLEle.GetElementsByTagName("TPAName").Item(0).InnerText;


                sSQLTPA = "SELECT COUNT(*) FROM TPA_SYSTEM_NAME WHERE TPA_SYSTEM_NAME = '" + sTPASystemName + "'";

                objConn = DbFactory.GetDbConnection(m_sConnString);
                objConn.Open();
                iTPACount = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQLTPA), m_iClientId);
                iTPAId = Utilities.GetNextUID(m_sConnString, "TPA_SYSTEM_NAME", m_iClientId);

                //if (iTPACount == 0)
                //{
                //    sSQL = "INSERT INTO TPA_SYSTEM_NAME(TPA_ROW_ID,TPA_SYSTEM_NAME,TPA_SYSTEM)" +
                //            "VALUES(" + iTPAId + ",'" + sTPASystemName + "','" + sTPAName + "')";
                //    objCon = DbFactory.GetDbConnection(m_sConnString);
                //    objCon.Open();
                //    objCon.ExecuteNonQuery(sSQL);
                //    sError = string.Empty;
                //}
                //else
                //{
                //    sError = "TPA Already Exist";
                //}

                //if (sError == "")     
                //{
                //    InsertTPAMapTablesIntoGlossary(sTPASystemName,iTPAId);
                //}

                if (iTPACount != 0)
                {
                    sError = "TPA Already Exist";
                }
                else
                {
                    InsertTPAMapTablesIntoGlossary(sTPASystemName, iTPAId);
                    sSQL = "INSERT INTO TPA_SYSTEM_NAME(TPA_ROW_ID,TPA_SYSTEM_NAME,TPA_SYSTEM)" +
                           "VALUES(" + iTPAId + ",'" + sTPASystemName + "','" + sTPAName + "')";
                    objCon = DbFactory.GetDbConnection(m_sConnString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);
                    sError = string.Empty;
                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.SaveTPA.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objTPASysXMLEle != null)
                {
                    objTPASysXMLEle = null;
                }
            }
        }

        public XmlDocument GetTPAMapping(XmlDocument p_objXmlDocument)
        {
            string sSQL = "";
            XmlElement objElm = null;
            XmlElement objTPANode = null;
            XmlElement objElemTemp = null;
            DbConnection objConn = null;
            DbReader objDbReader = null;
            XmlElement objRootElement = null;
            //LocalCache objCache = null;

            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//TPACodeMappingAdaptor/AddCode");
                objTPANode = (XmlElement)p_objXmlDocument.CreateNode(XmlNodeType.Element, "AvailableTPAList", "");
                sSQL = "SELECT TPA_ROW_ID,TPA_SYSTEM_NAME,TPA_SYSTEM FROM TPA_SYSTEM_NAME"; 
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                while (objDbReader.Read())
                {
                    //objCache = new LocalCache(m_sConnString);
                    objElemTemp = p_objXmlDocument.CreateElement("TPA");
                    objElemTemp.SetAttribute("TPARowID", Conversion.ConvertObjToStr(objDbReader.GetValue("TPA_ROW_ID")));
                    objElemTemp.SetAttribute("TPASystemName", Conversion.ConvertObjToStr(objDbReader.GetValue("TPA_SYSTEM_NAME")));
                    objElemTemp.SetAttribute("TPAName", Conversion.ConvertObjToStr(objDbReader.GetValue("TPA_SYSTEM")));
                    objTPANode.AppendChild(objElemTemp);

                }

                p_objXmlDocument.FirstChild.AppendChild(objTPANode);

                return p_objXmlDocument;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.GetTPAMapping.Error", m_iClientId), p_objExp);
            }
            finally
            {
                //if (objCache != null)
                //    objCache = null;
                if (objElm != null)
                    objElm = null;
            }
        }

        public XmlDocument DeleteTPAMapping(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            XmlElement objElm = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            string sSQLTPA = string.Empty;
            DbReader objReader = null;
            int iTPATableId = 0;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//DeletedCode");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objConn = DbFactory.GetDbConnection(m_sConnString);
                objConn.Open();

                if (sRowId != "")
                {
                    sSQL = "SELECT TPI_TABLE_ID FROM TPI_MAP_TABLES WHERE TPI_SYSTEM_ID IN(" + sRowId + ")";
                }

                objReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                while (objReader.Read())
                {
                    iTPATableId = objReader.GetInt(0);
                    sSQLTPA = "DELETE FROM GLOSSARY WHERE TABLE_ID =" + iTPATableId;
                    DbFactory.ExecuteNonQuery(m_sConnString, sSQLTPA);
                    sSQLTPA = "DELETE FROM GLOSSARY_TEXT WHERE TABLE_ID =" + iTPATableId;
                    DbFactory.ExecuteNonQuery(m_sConnString, sSQLTPA);
                }


                if (sRowId != "")
                {
                    sSQL = "DELETE FROM TPI_MAP_TABLES WHERE TPI_SYSTEM_ID IN(" + sRowId + ")";
                }
                DbFactory.ExecuteNonQuery(m_sConnString, sSQL);

                if (sRowId != "")
                {
                    sSQL = "DELETE FROM TPA_SYSTEM_NAME WHERE TPA_ROW_ID IN(" + sRowId + ")";
                }
                DbFactory.ExecuteNonQuery(m_sConnString, sSQL);
              
                //objConn.ExecuteNonQuery(sSQL);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.DeleteTPA.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        public bool InsertTPAMapTablesIntoGlossary(string p_SystemTableName,int iTPAid) 
        {
            DbReader objReader = null;
            DbReader objReaderGlos = null;
            LocalCache objCache = null;
            bool bSuccess = false;
            int iCount = 0;
            int iPSTableId = 0;
            string sPSTableName = string.Empty;
            string sSQL = string.Empty;
            string sSQLInsert = string.Empty;
            int iRMXTableId = 0;
            int iRowId = 0;
            string sRMXTableName = string.Empty;
            string ssQLTPA = string.Empty;
          
            try
            {

                objCache = new LocalCache(m_sConnString, m_iClientId);

                sSQL = "SELECT TPARISK_TABLE_NAME,TPARISK_ROW_ID FROM TPA_RISMASTER_TABLES";

                objReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                while (objReader.Read())
                {
                    sRMXTableName = Conversion.ConvertObjToStr(objReader.GetValue("TPARISK_TABLE_NAME"));
                    iRowId = Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("TPARISK_ROW_ID")), out bSuccess);

                    sPSTableName = p_SystemTableName + "_" + sRMXTableName;
                    
                    iRMXTableId = objCache.GetTableId(sRMXTableName);
                    
                    //checkif glossary already conrtains a row for the table
                    sSQL = "SELECT COUNT(TABLE_ID) FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + sPSTableName + "'";
                    objReaderGlos = DbFactory.GetDbReader(m_sConnString, sSQL);
                    if (objReaderGlos.Read())
                    {
                        iCount = objReaderGlos.GetInt(0);
                        objReaderGlos.Close();
                        if (iCount == 0) //if table not already exists=> insert
                        {
                            //get next table id
                            sSQL = "SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'GLOSSARY'";
                            objReaderGlos = DbFactory.GetDbReader(m_sConnString, sSQL);
                            if (objReaderGlos.Read())
                            {
                                iPSTableId = objReaderGlos.GetInt(0);
                                objReaderGlos.Close();
                                //update next table id
                                DbFactory.ExecuteNonQuery(m_sConnString, (String.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID={0} WHERE SYSTEM_TABLE_NAME='GLOSSARY'", (iPSTableId + 1))));
                            }
                            //insert into glossary
                            sSQLInsert = "INSERT INTO GLOSSARY (TABLE_ID, SYSTEM_TABLE_NAME, GLOSSARY_TYPE_CODE, NEXT_UNIQUE_ID, " +
                            "DELETED_FLAG, REQD_REL_TABL_FLAG, ATTACHMENTS_FLAG, REQD_IND_TABL_FLAG, LINE_OF_BUS_FLAG) " +
                            "VALUES(" + iPSTableId + ",'" + sPSTableName + "',3,1,0,0,0,0,0)";
                            DbFactory.ExecuteNonQuery(m_sConnString, sSQLInsert);

                            //insert into glossary text
                            //insert the values into the GLOSSARY_TEXT table
                            sSQLInsert = "INSERT INTO GLOSSARY_TEXT (TABLE_ID, TABLE_NAME, LANGUAGE_CODE) VALUES(" + iPSTableId + ",'" + sPSTableName + "',1033)";
                            DbFactory.ExecuteNonQuery(m_sConnString, sSQLInsert);

                            //update glosary timestamp
                            string sTimeStamp = DateTime.Now.ToString("yyyyMMddHHmm00");
                            sSQLInsert = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + sTimeStamp + "'  WHERE SYSTEM_TABLE_NAME = '" + sPSTableName + "'";
                            DbFactory.ExecuteNonQuery(m_sConnString, sSQLInsert);

                            sSQLInsert = "INSERT INTO TPI_MAP_TABLES(RMX_TABLE_ID,TPI_TABLE_ID,TPI_SYSTEM_ID) VALUES (" + iRMXTableId + "," + iPSTableId + "," + iTPAid + ")";
                            DbFactory.ExecuteNonQuery(m_sConnString, sSQLInsert);
                        }
                    }   
                }
                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SetUpTPASystem.InsertTPAMapTablesIntoGlossary.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objReaderGlos != null)
                    objReaderGlos.Dispose();
                if (objCache != null)
                    objCache.Dispose();
            }
        }

        public XmlDocument RefershNewMapping(XmlDocument p_objXmlDocument)
        {
            string sSQLTPA = "";
            DbConnection objCon = null;
            DbConnection objConn = null;
            DbReader objDbReader = null;
            LocalCache objCache = null;

            try
            {
                sSQLTPA = "SELECT TPA_ROW_ID,TPA_SYSTEM_NAME FROM TPA_SYSTEM_NAME" ;
                objConn = DbFactory.GetDbConnection(m_sConnString);
                objConn.Open();
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQLTPA);
                while (objDbReader.Read())
                {
                    string sTPAName = string.Empty;
                    int iTPAId = 0;
                    objCache = new LocalCache(m_sConnString, m_iClientId);
                    sTPAName = Conversion.ConvertObjToStr(objDbReader.GetValue("TPA_SYSTEM_NAME"));
                    iTPAId = Conversion.ConvertObjToInt(objDbReader.GetValue("TPA_ROW_ID"), m_iClientId);
                    InsertTPAMapTablesIntoGlossary(sTPAName, iTPAId);

                }

                return p_objXmlDocument;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.SaveTPA.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objCon = null;
                }
                if (objCache != null)
                    objCache = null;
            }
        }

        //added by swati for DCI and CLUE - other process types
        public XmlDocument GetProcessTypes(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            DbConnection objCon = null;
            String sSql = null;
            string sValue = string.Empty;
            bool bMarkTPA = false;
            bool bAvailProcess = false;
            int iNextUID = 0;
            string  sProcessId = string.Empty;
            string sProcessName = string.Empty;
            try
            {
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("AvailableProcess");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("SystemName");
                //added for DCI Reporting
                sSql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'USE_DCI_FIELDS'";
                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objReader.Read())
                {
                    bAvailProcess = objReader.GetBoolean(0);
                    if (bAvailProcess)
                    {
                        //sSql = "SELECT INTERFACE_ROW_ID,INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME ORDER BY INTERFACE_SYSTEM";
                        sSql = string.Empty;
                        sSql = "SELECT INTERFACE_ROW_ID,INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME WHERE INTERFACE_SYSTEM = 'DCI'";
                        objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        if (objReader.Read())
                        {
                            sProcessId = Conversion.ConvertObjToStr(objReader.GetValue("INTERFACE_ROW_ID"));
                            sProcessName = Conversion.ConvertObjToStr(objReader.GetValue("INTERFACE_SYSTEM"));
                        }
                        else
                        {
                            sSql = string.Empty;
                            iNextUID = Utilities.GetNextUID(m_sConnString, "TPI_INTERFACE_NAME", m_iClientId);
                            sProcessId = Conversion.ConvertObjToStr(iNextUID);
                            sProcessName = "DCI";
                            sSql = "INSERT INTO TPI_INTERFACE_NAME VALUES (" + iNextUID + ", 'DCI')";
                            objCon = DbFactory.GetDbConnection(m_sConnString);
                            objCon.Open();
                            objCon.ExecuteNonQuery(sSql);
                        }
                        objElemTemp = objDOM.CreateElement("option");
                        sValue = sProcessId;
                        objElemTemp.SetAttribute("value", sValue);
                        objElemTemp.InnerText = sProcessName;
                        objElemChild.AppendChild(objElemTemp);
                    }
                }
                objReader.Close();

                //added for CLUE Reporting
                bAvailProcess = false;
                sSql = string.Empty;
                sSql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'USE_CLUE_FIELDS'";
                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objReader.Read())
                {
                    bAvailProcess = objReader.GetBoolean(0);
                    if (bAvailProcess)
                    {                       
                        //sSql = "SELECT INTERFACE_ROW_ID,INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME ORDER BY INTERFACE_SYSTEM";
                        sSql = string.Empty;
                        sSql = "SELECT INTERFACE_ROW_ID,INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME WHERE INTERFACE_SYSTEM = 'CLUE'";
                        objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                        if (objReader.Read())
                        {
                            sProcessId = Conversion.ConvertObjToStr(objReader.GetValue("INTERFACE_ROW_ID"));
                            sProcessName = Conversion.ConvertObjToStr(objReader.GetValue("INTERFACE_SYSTEM"));
                        }
                        else
                        {
                            sSql = string.Empty;
                            iNextUID = Utilities.GetNextUID(m_sConnString, "TPI_INTERFACE_NAME", m_iClientId);
                            sProcessId = Conversion.ConvertObjToStr(iNextUID);
                            sProcessName = "CLUE";
                            sSql = "INSERT INTO TPI_INTERFACE_NAME VALUES (" + iNextUID + ", 'CLUE')";
                            objCon = DbFactory.GetDbConnection(m_sConnString);
                            objCon.Open();
                            objCon.ExecuteNonQuery(sSql);
                        }
                        objElemTemp = objDOM.CreateElement("option");
                        sValue = sProcessId;
                        objElemTemp.SetAttribute("value", sValue);
                        objElemTemp.InnerText = sProcessName;
                        objElemChild.AppendChild(objElemTemp);
                    }
                }
                objReader.Close();

                //added for any other interface
                sSql = string.Empty;
                sSql = "SELECT INTERFACE_ROW_ID,INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME WHERE INTERFACE_SYSTEM NOT IN ('CLUE','DCI','TPA' )";
                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                while (objReader.Read())
                {
                    sProcessId = Conversion.ConvertObjToStr(objReader.GetValue("INTERFACE_ROW_ID"));
                    sProcessName = Conversion.ConvertObjToStr(objReader.GetValue("INTERFACE_SYSTEM"));
                    objElemTemp = objDOM.CreateElement("option");
                    sValue = sProcessId;
                    objElemTemp.SetAttribute("value", sValue);
                    objElemTemp.InnerText = sProcessName;
                    objElemChild.AppendChild(objElemTemp);
                }
                objReader.Close();
                //change end here

                //added for TPA
                sSql = string.Empty;
                sSql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'USE_TPA'";
                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objReader.Read())
                {
                    bMarkTPA = objReader.GetBoolean(0);
                    if (bMarkTPA)
                    {
                        objElemTemp = objDOM.CreateElement("option");
                        sValue = "0";
                        objElemTemp.SetAttribute("value", sValue);
                        objElemTemp.InnerText = "TPA";
                        objElemChild.AppendChild(objElemTemp);
                    }
                }

                objDOM.FirstChild.AppendChild(objElemChild);
                objReader.Close();                

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.GetAvailableProcess.Error", m_iClientId), p_objEx);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;

            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }

        }



        public XmlDocument GetAvailableProcess(XmlDocument p_objXmlDocument)
        {
            string sSQL = "";
            XmlElement objElm = null;
            XmlElement objProcessNode = null;
            XmlElement objElemTemp = null;
            DbConnection objConn = null;
            DbReader objDbReader = null;
            XmlElement objRootElement = null;
            //LocalCache objCache = null;//commented under code review

            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//TPACodeMappingAdaptor/AddCode");
                objProcessNode = (XmlElement)p_objXmlDocument.CreateNode(XmlNodeType.Element, "AvailableProcessList", "");
                //sSQL = "SELECT INTERFACE_ROW_ID,INTERFACE_SYSTEM_NAME,INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME";
                sSQL = "SELECT INTERFACE_ROW_ID,INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME";
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                while (objDbReader.Read())
                {
                    //objCache = new LocalCache(m_sConnString);//commented under code review
                    objElemTemp = p_objXmlDocument.CreateElement("ProcessType");
                    objElemTemp.SetAttribute("ProcessRowID", Conversion.ConvertObjToStr(objDbReader.GetValue("INTERFACE_ROW_ID")));
                    //objElemTemp.SetAttribute("ProcessSystemName", Conversion.ConvertObjToStr(objDbReader.GetValue("INTERFACE_SYSTEM_NAME")));
                    objElemTemp.SetAttribute("ProcessName", Conversion.ConvertObjToStr(objDbReader.GetValue("INTERFACE_SYSTEM")));
                    objProcessNode.AppendChild(objElemTemp);

                }

                p_objXmlDocument.FirstChild.AppendChild(objProcessNode);

                return p_objXmlDocument;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.GetProcessMapping.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

                if (objElm != null)
                    objElm = null;
                //if (objCache != null)
                //    objCache = null;
            }
        }

        public bool AddNewProcess(XmlDocument p_objInputXMLDoc, out string sError)
        {
            string sSQL = "";
            string sSQLProcess = "";            
            //DbConnection objConn = null;
            XmlElement objProcessSysXMLEle = null;
            string sProcessSystemName = string.Empty;
            string sProcessName = string.Empty;
            string sLossCode = string.Empty;
            string sReserveType = string.Empty;
            string sReserveAmount = string.Empty;
            string sCoverageLossLOBID = string.Empty;
            int iProcessCount = 0;
            int iProcessId = 0;

            try
            {
                objProcessSysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//TPACodeMappingAdaptor/AddCode");
                
                //sProcessSystemName = objProcessSysXMLEle.GetElementsByTagName("ProcessSystemName").Item(0).InnerText;
                if (objProcessSysXMLEle.GetElementsByTagName("ProcessName") != null)
                {
                    sProcessName = objProcessSysXMLEle.GetElementsByTagName("ProcessName").Item(0).InnerText;
                }


                sSQLProcess = "SELECT COUNT(*) FROM TPI_INTERFACE_NAME WHERE INTERFACE_SYSTEM = '" + sProcessName + "'";

                //objConn = DbFactory.GetDbConnection(m_sConnString);
                //objConn.Open();
                iProcessCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnString, sSQLProcess), m_iClientId);
                iProcessId = Utilities.GetNextUID(m_sConnString, "TPI_INTERFACE_NAME", m_iClientId);

                if (iProcessCount != 0)
                {
                    sError = Globalization.GetString("TPACodeMapping.SaveProcess.Validation", m_iClientId);// "Process Already Exist";
                }
                else
                {
                    //InsertTPAMapTablesIntoGlossary(sProcessSystemName, iProcessId);
                    sSQL = "INSERT INTO TPI_INTERFACE_NAME(INTERFACE_ROW_ID,INTERFACE_SYSTEM)" +
                           "VALUES(" + iProcessId + ", '" + sProcessName + "')";
                    //objConn = DbFactory.GetDbConnection(m_sConnString);
                    //objConn.Open();
                    DbFactory.ExecuteNonQuery(m_sConnString,sSQL);
                    sError = string.Empty;
                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.SaveProcess.Error", m_iClientId), p_objExp);
            }
            finally
            {                
                //if (objConn != null)
                //{
                //    objConn.Dispose();
                //    objConn = null;
                //}
                if (objProcessSysXMLEle != null)
                {
                    objProcessSysXMLEle = null;
                }
            }
        }        

        public XmlDocument DeleteProcessMapping(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            XmlElement objElm = null;
            //DbConnection objConn = null;
            string sSQL = string.Empty;
            string sSQLProcess = string.Empty;
            DbReader objReader = null;
            int iProcessTableId = 0;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//DeletedCode");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                //objConn = DbFactory.GetDbConnection(m_sConnString);
                //objConn.Open();

                //if (sRowId != "")
                //{
                //    sSQL = "SELECT TPI_TABLE_ID FROM TPI_MAP_TABLES WHERE TPI_SYSTEM_ID IN(" + sRowId + ")";
                //}

                //objReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                //while (objReader.Read())
                //{
                //    iProcessTableId = objReader.GetInt(0);
                //    sSQLProcess = "DELETE FROM GLOSSARY WHERE TABLE_ID =" + iProcessTableId;
                //    DbFactory.ExecuteNonQuery(m_sConnString, sSQLProcess);
                //    sSQLProcess = "DELETE FROM GLOSSARY_TEXT WHERE TABLE_ID =" + iProcessTableId;
                //    DbFactory.ExecuteNonQuery(m_sConnString, sSQLProcess);
                //}


                //if (sRowId != "")
                //{
                //    sSQL = "DELETE FROM TPI_MAP_TABLES WHERE TPI_SYSTEM_ID IN(" + sRowId + ")";
                //}
                //DbFactory.ExecuteNonQuery(m_sConnString, sSQL);

                if (sRowId != "")
                {
                    sSQL = "DELETE FROM TPI_INTERFACE_NAME WHERE INTERFACE_ROW_ID IN(" + sRowId + ")";
                }
                DbFactory.ExecuteNonQuery(m_sConnString, sSQL);

                //objConn.ExecuteNonQuery(sSQL);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.DeleteTPA.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                //if (objConn != null)
                //{
                //    objConn.Dispose();
                //    objConn = null;
                //}
            }
        }

        public XmlDocument GetProcessName(XmlDocument p_objXmlDocument)
        {
            string sProcessRowId = string.Empty;
            XmlElement objXMLElement = null;
            string sProcessName = string.Empty;
            string sSQL = string.Empty;
            XmlElement objProcessNode = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;  

            try
            {
                objXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//ProcessId");
                if (objXMLElement != null)
                    sProcessRowId = objXMLElement.InnerText;

                sSQL = "SELECT INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME WHERE INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessRowId);
                objReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                if (objReader.Read())
                {
                    sProcessName = objReader.GetString(0);
                }

                objElemTemp = p_objXmlDocument.CreateElement("ProcessType");
                objProcessNode = (XmlElement)p_objXmlDocument.CreateNode(XmlNodeType.Element, "AvailableProcessList", "");
                objElemTemp.SetAttribute("ProcessName", sProcessName);
                objProcessNode.AppendChild(objElemTemp);
                p_objXmlDocument.FirstChild.AppendChild(objProcessNode);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.GetProcessName.Error", m_iClientId), p_objEx);
            }
            finally
            {
                
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }           
        }              

        public XmlDocument GetTableDetails(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            String sSql = null;
            string sValue = string.Empty;
            try
            {
                //rupal:start                
                sSql = "SELECT GLOSSARY.TABLE_ID, TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND GLOSSARY_TYPE_CODE IN (1,2,3,1064) ORDER BY TABLE_NAME ASC";
                //rupal:end
                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("TableCodeMapping");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("SystemName");
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("option");
                    sValue = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID"));
                    objElemTemp.SetAttribute("value", sValue);
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME"));
                    objElemChild.AppendChild(objElemTemp);

                }

                objDOM.FirstChild.AppendChild(objElemChild);
                objReader.Close();

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.GetTPASystem.Error", m_iClientId), p_objEx);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;

            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }

        }
               

        public XmlDocument GetTableMapping(XmlDocument p_objXmlDocument)
        {
            string sSQL = string.Empty;            
            string sProcessRowId = string.Empty;
            XmlElement objElm = null;
            XmlElement objTPANode = null;
            XmlElement objElemTemp = null;            
            DbReader objDbReader = null;
            XmlElement objXMLElement = null;
            //LocalCache objCache = null;//commented under code review

            try
            {
                if (p_objXmlDocument != null)
                {
                    objXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//ProcessId");
                    if (objXMLElement != null)
                        sProcessRowId = objXMLElement.InnerText;

                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//TPACodeMappingAdaptor/AddCode");
                    objTPANode = (XmlElement)p_objXmlDocument.CreateNode(XmlNodeType.Element, "AvailableTableList", "");
                    sSQL = "SELECT TPI_MAP_ROW_ID,RMX_TABLE_ID,TPI_TABLE_ID FROM TPI_MAP_TABLES WHERE INTERFACE_ROW_ID = " + Convert.ToInt32(sProcessRowId);
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                    while (objDbReader.Read())
                    {
                        //objCache = new LocalCache(m_sConnString);//commented under code review
                        objElemTemp = p_objXmlDocument.CreateElement("TableMapping");
                        objElemTemp.SetAttribute("MapRowId", Conversion.ConvertObjToStr(objDbReader.GetValue("TPI_MAP_ROW_ID")));
                        objElemTemp.SetAttribute("RMXTable", TableName(Conversion.ConvertObjToInt(objDbReader.GetValue("RMX_TABLE_ID"), m_iClientId)));
                        objElemTemp.SetAttribute("ThirdPartyTable", TableName(Conversion.ConvertObjToInt(objDbReader.GetValue("TPI_TABLE_ID"), m_iClientId)));

                        objTPANode.AppendChild(objElemTemp);

                    }

                    p_objXmlDocument.FirstChild.AppendChild(objTPANode);
                }

                return p_objXmlDocument;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.GetTPAMapping.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                if (objElm != null)
                    objElm = null;
                //if (objCache != null)
                //    objCache = null;
            }
        }

        public string TableName(int iTableId)
        {
            string sSQL = string.Empty;
            string sTableName = string.Empty;
            //DbReader objDbReader = null;
            sSQL = "SELECT TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID = " + iTableId;
            //objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
            sTableName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnString, sSQL));
            //if (objDbReader.Read())
            //{
                //sTableName = objDbReader.GetString(0);
            //}
            return sTableName;
        }

        public bool AddNewTableMapping(XmlDocument p_objInputXMLDoc, out string sError)
        {
            string sSQL = string.Empty;  
            DbConnection objConn = null;
            XmlElement objTPASysXMLEle = null;
            string sProcessTableName = string.Empty;
            string sRMTableName = string.Empty;
            string sProcessId = string.Empty;
            string sLossCode = string.Empty;
            string sReserveType = string.Empty;
            string sReserveAmount = string.Empty;
            string sCoverageLossLOBID = string.Empty;
            int iMapCount = 0;
            int iRowId = 0;
            sError = string.Empty;

            try
            {
                if (p_objInputXMLDoc != null)
                {
                    objTPASysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//TPACodeMappingAdaptor/AddCode");

                    if (objTPASysXMLEle != null)
                    {
                        sProcessTableName = objTPASysXMLEle.GetElementsByTagName("ProcessTable").Item(0).InnerText;
                        sRMTableName = objTPASysXMLEle.GetElementsByTagName("RMTable").Item(0).InnerText;
                        sProcessId = objTPASysXMLEle.GetElementsByTagName("ProcessID").Item(0).InnerText;


                        sSQL = "SELECT COUNT(*) FROM TPI_MAP_TABLES WHERE RMX_TABLE_ID = " + sRMTableName + " AND TPI_TABLE_ID = " + sProcessTableName + " AND INTERFACE_ROW_ID = " + sProcessId;

                        objConn = DbFactory.GetDbConnection(m_sConnString);
                        objConn.Open();
                        iMapCount = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);


                        if (iMapCount != 0)
                        {
                            sError = Globalization.GetString("TPACodeMapping.SaveTPA.Validation", m_iClientId); //"Table Mapping Already Exist";
                        }
                        else
                        {
                            sSQL = string.Empty;
                            if (DbFactory.GetDatabaseType(m_sConnString) == eDatabaseType.DBMS_IS_ORACLE)
                            {
                                sSQL = "SELECT SEQ_PK_TPI_MAP_TABLES.NEXTVAL FROM DUAL";
                                iRowId = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL), m_iClientId);
                                sSQL = string.Empty;
                                sSQL = "INSERT INTO TPI_MAP_TABLES(TPI_MAP_ROW_ID,RMX_TABLE_ID,TPI_TABLE_ID,TPI_SYSTEM_ID,INTERFACE_ROW_ID) VALUES (" + iRowId + "," + sRMTableName + "," + sProcessTableName + "," + 0 + "," + Convert.ToInt32(sProcessId) + ")";
                            }
                            else
                            {
                                sSQL = string.Empty;
                                sSQL = "INSERT INTO TPI_MAP_TABLES(RMX_TABLE_ID,TPI_TABLE_ID,TPI_SYSTEM_ID,INTERFACE_ROW_ID) VALUES (" + sRMTableName + "," + sProcessTableName + "," + 0 + "," + Convert.ToInt32(sProcessId) + ")";
                            }
                            DbFactory.ExecuteNonQuery(m_sConnString, sSQL);
                            sError = string.Empty;
                        }
                    }
                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                Log.Write(p_objRMExp.Message + " Stack Trace: " + p_objRMExp.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.SaveTPA.Error", m_iClientId), p_objExp);
            }
            finally
            {                
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objTPASysXMLEle != null)
                {
                    objTPASysXMLEle = null;
                }
            }
        }

        public XmlDocument DeleteTableMapping(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            string sProcessId = string.Empty;
            XmlElement objElm = null;
            //DbConnection objConn = null;
            string sSQL = string.Empty;
            // sSQLTPA = string.Empty;           
            try
            {
                if (p_objXmlDocument != null)
                {
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//DeletedCode");
                    if (objElm != null)
                        sRowId = objElm.InnerText;

                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//ProcessId");
                    if (objElm != null)
                        sProcessId = objElm.InnerText;

                    //objConn = DbFactory.GetDbConnection(m_sConnString);
                    //objConn.Open();

                    if (!string.IsNullOrEmpty(sRowId) && !string.IsNullOrEmpty(sProcessId))
                    {
                        //sSQL = "DELETE FROM TPI_MAP_TABLES WHERE INTERFACE_ROW_ID =" + sProcessId + " AND TPI_MAP_ROW_ID = " + sRowId;
                        sSQL = "DELETE FROM TPI_MAP_TABLES WHERE INTERFACE_ROW_ID =" + sProcessId + " AND TPI_MAP_ROW_ID in (" + sRowId + ")";
                        DbFactory.ExecuteNonQuery(m_sConnString, sSQL);
                    }
                    
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TPACodeMapping.DeleteTableMapping.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                //if (objConn != null)
                //{
                //    objConn.Dispose();
                //    objConn = null;
                //}
            }
        }
        //change end here by swati

    }
}
