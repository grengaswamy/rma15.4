using System;
using System.IO; 
using System.Data; 
using System.Xml; 
using Riskmaster.Db; 
using Riskmaster.Common;
using System.Collections; 
using Riskmaster.Security; 
using Riskmaster.ExceptionTypes;
using System.Text;
//Start-Mridul Bansal. 01/19/10. MITS318230
using Riskmaster.Settings;
//End-Mridul Bansal. 01/19/10. MITS318230
//amar mits 30818
using System.Collections.Generic;
using Riskmaster.Security.Encryption;
using System.Xml.Linq;
//amar mits 30818


namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   16th,May 2005
	///Purpose :   WPA Auto Diary Setup Wizard Form 
	/// </summary>
	public class WPAAutoDiarySetup
	{
        //asharma326 MITS 35669 
        //public static XElement xElementFilters = null;
        public static Dictionary<string, Dictionary<string, string>> xFilters = null;

        public Dictionary<string, Dictionary<string, string>> Filters
        {
            get
            {
                if (xFilters == null)
                {
                    xFilters = new Dictionary<string, Dictionary<string, string>>();
                }

                return xFilters;
            }
        }
		/// <summary>
		/// Info setting Object
		/// </summary>
		private InfoSetting objInfoSet ;

		/// <summary>
		/// Database type
		/// </summary>
		private int m_iDbType= 0;

		/// <summary>
		/// Info definition Object
		/// </summary>
		private InfoDefinition objInfoDef ;

		/// <summary>
		/// Private variable for connection string
		/// </summary>
		string m_sConnStr="";

		/// <summary>
		/// Dsn Id
		/// </summary>
		int m_iDsnId=0;
        private int m_iClientId = 0;
        UserLogin objUserLogin = null;//vkumar258 ML changes

		/// Name			: WPAAutoDiarySetup
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 16 May 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sUser">User Name</param>
		/// <param name="p_sPwd">Password</param>
		/// <param name="p_sDsn">Dsn Name</param>
		public WPAAutoDiarySetup(string p_sUser, string p_sPwd, string p_sDsn,int p_iClientId)//sonali
		{			
			objInfoSet = new InfoSetting(); 
			objInfoDef = new InfoDefinition();
            m_iClientId = p_iClientId;
			objUserLogin = new UserLogin(p_sUser,p_sPwd,p_sDsn,m_iClientId);//sonali
			m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;
			m_iDbType = (int)objUserLogin.objRiskmasterDatabase.DbType;
			m_iDsnId=objUserLogin.objRiskmasterDatabase.DataSourceId;
			//objUserLogin=null;
		}


		/// Name			: Get
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 16 May 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the xml structure with data for the first step of the wizard
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document structure</param>
		/// <returns>Xml document with data and its structure for UI</returns>
		public XmlDocument Get(XmlDocument p_objXmlDoc) 
		{
			XmlElement objDefElm = null;
			XmlElement objElm = null;
			XmlElement objAvlFltrs = null;
			XmlElement objSelFltrs = null;
			XmlElement objFltrElm = null;
			XmlElement objNewElm = null;
			DataSet objDs = null;
			DataSet objDsGroups=null;
			int iDefId=0;
			int iTempId=0;
			string[] arrStrng=null;
			string sSecDsn="";

			XmlElement objElementTemp=null;
			string sTempId=string.Empty;
			string sTempName=string.Empty;
			
			try
			{
				sSecDsn = SecurityDatabase.GetSecurityDsn(m_iClientId);//sonali
				objDefElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefId");
				if(objDefElm!=null)
					iDefId = Conversion.ConvertStrToInteger(objDefElm.InnerText);

				if(iDefId>0)
					LoadInfoSet(p_objXmlDoc,iDefId); 

				//LoadInfoDef(p_objXmlDoc, -1);

				//Changed by Shruti on 10th oct 06 (MITS 7894) - starts

				if (!(objInfoSet.Index > 0))
					LoadInfoDef(p_objXmlDoc, -1);

				//Changed by Shruti on 10th oct 06 (MITS 7894) - ends

//				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DiaryTemplate']//level[@value='" + objInfoSet.Index +  "']");
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryTemplate//level[@value='" + objInfoSet.Index + "']");
                //tanwar2 - corrected XPath - Jan 4th, 2012
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryTemplate/level[@value='" + objInfoSet.Index + "']");

				if(objElm!=null)
				{
                    objElementTemp = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryTemplate");
					objElementTemp.SetAttribute("selectedid",objElm.GetAttribute("value"));
					iTempId = Conversion.ConvertStrToInteger(objElm.GetAttribute("value"));
					objElm.SetAttribute("selected","1"); 
				}

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryName"); 
				if(objElm!=null)
					objElm.InnerText = objInfoSet.Name;

				//commented by Nitin for R5
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='PriorityLevel']//level[@id='" + objInfoSet.Priority +  "']");
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//PriorityLevel//level[@id='" + objInfoSet.Priority + "']");
                
                //added by Nitin for R5
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//PriorityLevel");
                if(objElm!=null)
				{
                    objElm.SetAttribute("selectedid", objInfoSet.Priority.ToString());
                }

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefIndex"); 
				if(objElm!=null)
					objElm.InnerText = objInfoSet.Index.ToString()  ;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//NoOfDays"); 
				if(objElm!=null)
					objElm.InnerText = objInfoSet.iDueDays.ToString();

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Security_Mgmt_Relationship");
                if (objElm != null)
                {
                    if (objInfoSet.Security_Mgmt_Relationship.ToString() == "1" || objInfoSet.Security_Mgmt_Relationship.ToString() == "-1")
                        objElm.InnerText = "1";
                    else
                        objElm.InnerText = "0";
                }

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Escalate_Highestlevel");
                if (objElm != null)
                {
                    if (objInfoSet.Escalate_Highest_Level.ToString() == "1" || objInfoSet.Escalate_Highest_Level.ToString() == "-1")
                        objElm.InnerText = "1";
                    //Ashish Ahuja : Mits 34043 start
                    else if (objInfoSet.Escalate_Highest_Level.ToString() == "2")
                        objElm.InnerText = "2";
                    else if (objInfoSet.Escalate_Highest_Level.ToString() == "3")
                        objElm.InnerText = "3";
                    else if (objInfoSet.Escalate_Highest_Level.ToString() == "4")
                        objElm.InnerText = "4";
                    //Ashish Ahuja : Mits 34043 end
                    else
                        objElm.InnerText = "0";
                }

                objAvlFltrs = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/AvlFilters"); 

//				if( iTempId ==0) iTempId++;
//				LoadInfoDef(p_objXmlDoc,iTempId); 

				//Populate the available filters
				for(int iCtr = 0; iCtr <= objInfoDef.NumFilters - 1; iCtr++)
				{
					objNewElm = p_objXmlDoc.CreateElement("level");
					objNewElm.SetAttribute("templateid",objInfoDef.FilterDef[iCtr].TemplateId.ToString());
					objNewElm.SetAttribute("id",objInfoDef.FilterDef[iCtr].ID.ToString());
					objNewElm.SetAttribute("name",objInfoDef.FilterDef[iCtr].Name);
					objNewElm.SetAttribute("SQLFill",objInfoDef.FilterDef[iCtr].SQLFill);
					objNewElm.SetAttribute("FilterType",objInfoDef.FilterDef[iCtr].FilterType.ToString());
					if (objInfoDef.FilterDef[iCtr].DefValue!=null)
						objNewElm.SetAttribute("DefValue",objInfoDef.FilterDef[iCtr].DefValue.ToString());
					else
						objNewElm.SetAttribute("DefValue",string.Empty);
                    //rupal:start, R8 Auto Diary Setup Enh
                    if (!string.IsNullOrEmpty(objInfoDef.FilterDef[iCtr].Database))
                        objNewElm.SetAttribute("Database", objInfoDef.FilterDef[iCtr].Database.ToString());
                    else
                        objNewElm.SetAttribute("Database", string.Empty);
                    //rupal:end
					objAvlFltrs.AppendChild(objNewElm);
				}			
				if(iDefId>0)
				{
                    objSelFltrs = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/SelectedFilters"); 
					for(int iCtr = 0; iCtr <= objInfoSet.NumFilters -1 ; iCtr++)
					{
						//Populate the available filters
                        //tanwar2 - corrected XPath - Jan 4th, 2012
                        //objFltrElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/AvlFilters//level[@id='" + objInfoSet.FilterSet[iCtr].Number + "']"); 
                        objFltrElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/AvlFilters/level[@id='" + objInfoSet.FilterSet[iCtr].Number + "']"); 
						if(objFltrElm!=null)
						{
							objNewElm = p_objXmlDoc.CreateElement("level");
							objNewElm.SetAttribute("id",objFltrElm.GetAttribute("id"));
							objNewElm.SetAttribute("templateid",objInfoSet.Index.ToString());
							objNewElm.SetAttribute("name",objFltrElm.GetAttribute("name"));
							objNewElm.SetAttribute("number",objInfoSet.FilterSet[iCtr].Number.ToString());
							objNewElm.SetAttribute("data",objInfoSet.FilterSet[iCtr].Data.ToString());
							objNewElm.SetAttribute("SQLFill",objFltrElm.GetAttribute("SQLFill"));
							objNewElm.SetAttribute("FilterType",objFltrElm.GetAttribute("FilterType"));
							objNewElm.SetAttribute("DefValue",objFltrElm.GetAttribute("DefValue"));
                            //rupal:start, R8 Auto Diary Setup Enh 
                            if (objFltrElm.HasAttribute("Database"))
                                objNewElm.SetAttribute("Database", objFltrElm.GetAttribute("Database"));
                            else
                                objNewElm.SetAttribute("Database", string.Empty);
                            //rupal:end
							objSelFltrs.AppendChild(objNewElm);
						}
					}			

					//Populate work activities
                    //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity//WorkActivityLevelList"); 
                    //tanwar2 - corrected XPath - Jan 4th, 2012
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity/WorkActivityLevelList"); 
					if (objInfoSet.Activities!=null)
					{
						sTempId=string.Empty;
						sTempName=string.Empty;
						arrStrng = objInfoSet.Activities.Split(',');
						for(int iCtr=0;iCtr<=arrStrng.Length-2;iCtr++)
						{						
							objNewElm = p_objXmlDoc.CreateElement("level");
							objNewElm.SetAttribute("id",arrStrng[iCtr].Substring(0,arrStrng[iCtr].IndexOf("|")));
							sTempId=sTempId + arrStrng[iCtr].Substring(0,arrStrng[iCtr].IndexOf("|")) + "|";
							objNewElm.SetAttribute("name",arrStrng[iCtr].Substring(arrStrng[iCtr].IndexOf("|")+1, arrStrng[iCtr].Length - arrStrng[iCtr].IndexOf("|")-1) );
							sTempName=sTempName + arrStrng[iCtr].Substring(arrStrng[iCtr].IndexOf("|")+1, arrStrng[iCtr].Length - arrStrng[iCtr].IndexOf("|")-1) + "|";
							objElm.AppendChild(objNewElm);
						}
						if (sTempId.Length>0) 
							sTempId=sTempId.Substring(0,sTempId.Length-1);
						if (sTempName.Length>0)
							sTempName=sTempName.Substring(0,sTempName.Length-1);
						((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivityIdList")).InnerText=sTempId;
						((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivityNameList")).InnerText=sTempName;
					}

					objDs = DbFactory.GetDataSet(sSecDsn,"SELECT * FROM USER_DETAILS_TABLE", m_iClientId);
					//Users to be sent
                    //tanwar2 - corrected XPath - Jan 4th, 2012
					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendUsers");
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendUsers");
					if (objInfoSet.SendUsers !=null)
					{
						sTempId=string.Empty;
						sTempName=string.Empty;
						arrStrng = objInfoSet.SendUsers.Split(',');
						for(int iCtr=0;iCtr<=arrStrng.Length-2;iCtr++)
						{						
							objDs.Tables[0].DefaultView.RowFilter = "DSNID = " + m_iDsnId + " AND USER_ID=" + arrStrng[iCtr];
							objNewElm = p_objXmlDoc.CreateElement("level");
							objNewElm.SetAttribute("id",arrStrng[iCtr]);
							sTempId=sTempId + arrStrng[iCtr] + "|";
							objNewElm.SetAttribute("type","1");
							if (objDs.Tables[0].DefaultView.Count>0)
							{
								objNewElm.SetAttribute("name",objDs.Tables[0].DefaultView[0]["LOGIN_NAME"].ToString());
								sTempName=sTempName + objDs.Tables[0].DefaultView[0]["LOGIN_NAME"].ToString() + "|";
							}
							objElm.AppendChild(objNewElm);
						}
						if (sTempId.Length>0) 
							sTempId=sTempId.Substring(0,sTempId.Length-1);
						if (sTempName.Length>0)
							sTempName=sTempName.Substring(0,sTempName.Length-1);
                        //tanwar2 - corrected XPath - Jan 4th, 2012
						//((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendUsersIdList")).InnerText=sTempId;
						//((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendUsersNameList")).InnerText=sTempName;
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendUsersIdList")).InnerText = sTempId;
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendUsersNameList")).InnerText = sTempName;
					}

					//Users to be notified
					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified//UsersNotifiedLevelList"); 
                    //tanwar2 - corrected XPath - Jan 4th, 2012
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified/UsersNotifiedLevelList"); 
					if (objInfoSet.NotifyUsers !=null)
					{
						sTempId=string.Empty;
						sTempName=string.Empty;
						arrStrng = objInfoSet.NotifyUsers.Split(',');
						for(int iCtr=0;iCtr<=arrStrng.Length-2;iCtr++)
						{						
							objDs.Tables[0].DefaultView.RowFilter = "DSNID = " + m_iDsnId + " AND  USER_ID=" + arrStrng[iCtr];
							objNewElm = p_objXmlDoc.CreateElement("level");
							objNewElm.SetAttribute("id",arrStrng[iCtr]);
							sTempId=sTempId + arrStrng[iCtr] + "|";
							objNewElm.SetAttribute("type","2");
							if (objDs.Tables[0].DefaultView.Count>0)
							{
								objNewElm.SetAttribute("name",objDs.Tables[0].DefaultView[0]["LOGIN_NAME"].ToString());
								sTempName=sTempName + objDs.Tables[0].DefaultView[0]["LOGIN_NAME"].ToString() + "|";
							}
							objElm.AppendChild(objNewElm);
						}
						if (sTempId.Length>0) 
							sTempId=sTempId.Substring(0,sTempId.Length-1);
						if (sTempName.Length>0)
							sTempName=sTempName.Substring(0,sTempName.Length-1);
						//((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified//UsersNotifiedIdList")).InnerText=sTempId;
						//((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified//UsersNotifiedNameList")).InnerText=sTempName;
                        //tanwar2 - corrected XPath - Jan 4th, 2012
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified/UsersNotifiedIdList")).InnerText = sTempId;
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified/UsersNotifiedNameList")).InnerText = sTempName;
					}

					//Users to be routed
					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted/UsersRoutedLevels"); 
					if (objInfoSet.RouteUsers!=null)
					{
						sTempId=string.Empty;
						sTempName=string.Empty;
						arrStrng = objInfoSet.RouteUsers.Split(',');
						for(int iCtr=0;iCtr<=arrStrng.Length-2;iCtr++)
						{						
							objDs.Tables[0].DefaultView.RowFilter = "DSNID = " + m_iDsnId + " AND  USER_ID=" + arrStrng[iCtr];
							objNewElm = p_objXmlDoc.CreateElement("level");
							objNewElm.SetAttribute("id",arrStrng[iCtr]);
							sTempId=sTempId + arrStrng[iCtr] + "|";
							objNewElm.SetAttribute("type","3");
							if (objDs.Tables[0].DefaultView.Count>0)
							{
								objNewElm.SetAttribute("name",objDs.Tables[0].DefaultView[0]["LOGIN_NAME"].ToString());
								sTempName=sTempName + objDs.Tables[0].DefaultView[0]["LOGIN_NAME"].ToString() + "|";
							}
							objElm.AppendChild(objNewElm);
						}
						if (sTempId.Length>0) 
							sTempId=sTempId.Substring(0,sTempId.Length-1);
						if (sTempName.Length>0)
							sTempName=sTempName.Substring(0,sTempName.Length-1);
						((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted/UsersRoutedIdList")).InnerText=sTempId;
						((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted/UsersRoutedNameList")).InnerText=sTempName;
					}

					objDsGroups=DbFactory.GetDataSet(m_sConnStr,"SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS", m_iClientId);
                    //tanwar2- corrected XPath - Jan 4th 2012
					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendGroups"); 
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendGroups");
					if (objInfoSet.SendGroups!=null)
					{
						sTempId=string.Empty;
						sTempName=string.Empty;
						arrStrng = objInfoSet.SendGroups.Split(',');
						for(int iCtr=0;iCtr<=arrStrng.Length-2;iCtr++)
						{						
							objDsGroups.Tables[0].DefaultView.RowFilter = "GROUP_ID=" + arrStrng[iCtr];
							objNewElm = p_objXmlDoc.CreateElement("level");
							objNewElm.SetAttribute("id",arrStrng[iCtr]);
							sTempId=sTempId + arrStrng[iCtr] + "|";
							objNewElm.SetAttribute("type","4");
							if (objDsGroups.Tables[0].DefaultView.Count>0)	
							{
								objNewElm.SetAttribute("name",objDsGroups.Tables[0].DefaultView[0]["GROUP_NAME"].ToString());
								sTempName=sTempName + objDsGroups.Tables[0].DefaultView[0]["GROUP_NAME"].ToString() + "|";
							}
							objElm.AppendChild(objNewElm);
						}
						if (sTempId.Length>0) 
							sTempId=sTempId.Substring(0,sTempId.Length-1);
						if (sTempName.Length>0)
							sTempName=sTempName.Substring(0,sTempName.Length-1);
                        //tanwar2  - corrected XPath - Jan 4th, 2012
						//((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendGroupsIdList")).InnerText=sTempId;
						//((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendGroupsNameList")).InnerText=sTempName;
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendGroupsIdList")).InnerText = sTempId;
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendGroupsNameList")).InnerText = sTempName;
					}

                    //pkandhari Jira 6412 starts
                    if (objInfoSet.NotifyGroups != null)
                    {
                        sTempId = string.Empty;
                        sTempName = string.Empty;
                        arrStrng = objInfoSet.NotifyGroups.Split(',');
                        for (int iCtr = 0; iCtr <= arrStrng.Length - 2; iCtr++)
                        {
                            objDsGroups.Tables[0].DefaultView.RowFilter = "GROUP_ID=" + arrStrng[iCtr];
                            objNewElm = p_objXmlDoc.CreateElement("level");
                            objNewElm.SetAttribute("id", arrStrng[iCtr]);
                            sTempId = sTempId + arrStrng[iCtr] + "|";
                            objNewElm.SetAttribute("type", "4");
                            if (objDsGroups.Tables[0].DefaultView.Count > 0)
                            {
                                objNewElm.SetAttribute("name", objDsGroups.Tables[0].DefaultView[0]["GROUP_NAME"].ToString());
                                sTempName = sTempName + objDsGroups.Tables[0].DefaultView[0]["GROUP_NAME"].ToString() + "|";
                            }
                            objElm.AppendChild(objNewElm);
                        }
                        if (sTempId.Length > 0)
                            sTempId = sTempId.Substring(0, sTempId.Length - 1);
                        if (sTempName.Length > 0)
                            sTempName = sTempName.Substring(0, sTempName.Length - 1);
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsNotifiedIdList")).InnerText = sTempId;
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsNotifiedNameList")).InnerText = sTempName;
                    }

                    if (objInfoSet.RouteGroups != null)
                    {
                        sTempId = string.Empty;
                        sTempName = string.Empty;
                        arrStrng = objInfoSet.RouteGroups.Split(',');
                        for (int iCtr = 0; iCtr <= arrStrng.Length - 2; iCtr++)
                        {
                            objDsGroups.Tables[0].DefaultView.RowFilter = "GROUP_ID=" + arrStrng[iCtr];
                            objNewElm = p_objXmlDoc.CreateElement("level");
                            objNewElm.SetAttribute("id", arrStrng[iCtr]);
                            sTempId = sTempId + arrStrng[iCtr] + "|";
                            objNewElm.SetAttribute("type", "4");
                            if (objDsGroups.Tables[0].DefaultView.Count > 0)
                            {
                                objNewElm.SetAttribute("name", objDsGroups.Tables[0].DefaultView[0]["GROUP_NAME"].ToString());
                                sTempName = sTempName + objDsGroups.Tables[0].DefaultView[0]["GROUP_NAME"].ToString() + "|";
                            }
                            objElm.AppendChild(objNewElm);
                        }
                        if (sTempId.Length > 0)
                            sTempId = sTempId.Substring(0, sTempId.Length - 1);
                        if (sTempName.Length > 0)
                            sTempName = sTempName.Substring(0, sTempName.Length - 1);
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsRoutedIdList")).InnerText = sTempId;
                        ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsRoutedNameList")).InnerText = sTempName;
                    }
                    //pkandahri Jira 6412 ends

					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//UserCreated");
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/UserCreated");
					if(objElm!=null)
					{
						if (objInfoSet.SendCreate.ToString()=="1" || objInfoSet.SendCreate.ToString()=="-1")
							objElm.InnerText = "1";   
						else
							objElm.InnerText = "0";   
					}

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UserModified");
					if(objElm!=null)
					{
						if (objInfoSet.SendUpdate.ToString()=="1" || objInfoSet.SendUpdate.ToString()=="-1")
							objElm.InnerText = "1";   
						else
							objElm.InnerText = "0";   
					}

					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//AdjAssig");
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/AdjAssig");
					if(objElm!=null)
					{
						if (objInfoSet.SendAdj.ToString()=="1" || objInfoSet.SendAdj.ToString()=="-1")
							objElm.InnerText = "1";   
						else
							objElm.InnerText = "0";   
					}

					//Start Naresh MITS 8349 07/11/2006
					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//AdjAssigSupervisor");
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/AdjAssigSupervisor");
					if(objElm!=null)
					{
						if (objInfoSet.SendAdjSupervisor.ToString()=="1" || objInfoSet.SendAdjSupervisor.ToString()=="-1")
							objElm.InnerText = "1";   
						else
							objElm.InnerText = "0";   
					}
					//End Naresh MITS 8349 07/11/2006

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//NotificationDays");
					if(objElm!=null)
						objElm.InnerText = objInfoSet.NotifyDays.ToString();

					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted//ReRoutedDays");
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted/ReRoutedDays");
					if(objElm!=null)
						objElm.InnerText = objInfoSet.RouteDays.ToString();

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//EstTime");
					if(objElm!=null)
						objElm.InnerText = objInfoSet.TaskEst.ToString();

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//MinsHrs");
					if(objElm!=null)
						objElm.InnerText = objInfoSet.TaskEstType.ToString();
                    //rupal:r8 auto diary enh
					//objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ExportFlag");
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ExportToEmailFlag");
                    if (objElm != null)
                    {
                        //rupal:r8 auto diary enh
                        //objElm.InnerText = objInfoSet.ExpSchedule.ToString();
                        objElm.InnerText = objInfoSet.ExportEmail.ToString();
                    }
					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//TimeAssoFlag");
					if(objElm!=null)
						objElm.InnerText = objInfoSet.TimeBillable.ToString();

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Instruction");
					if(objElm!=null)
						objElm.InnerText = objInfoSet.Instruct;

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryDate");
					if(objElm!=null)
						//objElm.InnerText = UTILITY.FormatDate(objInfoSet.ProcessDate,false);
                    objElm.InnerText = Conversion.GetUIDate(objInfoSet.ProcessDate, objUserLogin.objUser.NlsCode.ToString(), m_iClientId);//vkumar258 ML Changes
                    //mits 25439 - tanwar2 - 4th Jan 2012 - moved out of loop and corrected XPath - start
                    /* *** objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//EventSuppUser");
                    if (objElm != null)
                    {
                        if (objInfoSet.SendSuppField == -1 || objInfoSet.SendSuppField == 1)
                            objElm.InnerText = "1";
                        else
                            objElm.InnerText = "0";
                    }
                    FillSuppCombo(ref p_objXmlDoc); *** */
				}

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/EventSuppUser");
                if (objElm != null)
                {
                    if (objInfoSet.SendSuppField == -1 || objInfoSet.SendSuppField == 1)
                        objElm.InnerText = "1";
                    else
                        objElm.InnerText = "0";
                }
                FillSuppCombo(ref p_objXmlDoc);
                ////mits 25439 - tanwar2 - 4th Jan 2012 - moved out of loop and corrected XPath - end
                //var ExtractFilterXMl = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters");
                //xElementFilters = XElement.Parse(ExtractFilterXMl.OuterXml);
                //asharma326 MITS 35669 
                XmlNodeList xmlNodeList = p_objXmlDoc.SelectNodes("//Filters/*/level");
                Dictionary<string, string> temp = new Dictionary<string, string>();

                //Bharani - MITS : 35878 - Commenting the below Lines and adding code below that 
                //foreach (XmlNode node in xmlNodeList)
                //{
                //    //if (xFilters == null)
                //    //{
                //    //    xFilters = new Dictionary<string, Dictionary<string, string>>();
                //    //}

                //    if (!Filters.ContainsKey(node.Attributes["templateid"].Value))
                //    {
                //        if (!temp.ContainsKey(node.Attributes["id"].Value))
                //        {
                //            temp.Add(node.Attributes["id"].Value, node.Attributes["SQLFill"].Value);
                //        }
                //    }

                //    node.Attributes["SQLFill"].Value = string.Empty;
                //}

                //objElm = p_objXmlDoc.SelectSingleNode("//Filters/*/level") as XmlElement; // ("//Filters/*/level");

                //if (objElm != null && objElm.HasAttribute("templateid") && !Filters.ContainsKey(objElm.Attributes["templateid"].Value))
                //{
                //    Filters.Add(objElm.Attributes["templateid"].Value, temp);
                //}

                //Bharani - MITS : 35878 - New Code Start

                string sPrevious = string.Empty;
                bool bLastItemRequired = false;

                foreach (XmlNode node in xmlNodeList)
                {
                    if (!Filters.ContainsKey(node.Attributes["templateid"].Value))
                    {
                        if (!temp.ContainsKey(node.Attributes["id"].Value))
                        {
                            temp.Add(node.Attributes["id"].Value, node.Attributes["SQLFill"].Value);
                            bLastItemRequired = true;
                        }
                        else 
                        {
                            if (node.NextSibling != null)
                            {
                                bLastItemRequired = false;
                                Dictionary<string, string> temp2 = new Dictionary<string, string>();
                                foreach (KeyValuePair<string, string> kv in temp)
                                 {
                                    temp2.Add(kv.Key, kv.Value);
                                 }
                                Filters.Add(sPrevious, temp2);
                                temp.Clear();
                                temp.Add(node.Attributes["id"].Value, node.Attributes["SQLFill"].Value);
                            }
                            else
                            {
                                bLastItemRequired = false;
                                Dictionary<string, string> temp2 = new Dictionary<string, string>();
                                foreach (KeyValuePair<string, string> kv in temp)
                                {
                                    temp2.Add(kv.Key, kv.Value);
                                }
                                Filters.Add(sPrevious, temp2);
                            }
                        }
                        sPrevious = node.Attributes["templateid"].Value;
                        node.Attributes["SQLFill"].Value = string.Empty;
                    }                    
                }

                objElm = p_objXmlDoc.SelectSingleNode("//Filters/*/level") as XmlElement; // ("//Filters/*/level");
                
                if (objElm != null && objElm.HasAttribute("templateid"))
                {
                    if (bLastItemRequired == true)
                    {
                        Dictionary<string, string> temp4 = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> kv in temp)
                        {
                            temp4.Add(kv.Key, kv.Value);
                        }
                        Filters.Add(sPrevious, temp4);
                    }                    
                }
                
                //Bharani - MITS : 35878 - New Code End

				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("WPAAutoDiarySetup.Get.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				objDefElm = null;
				objElm = null;
				objAvlFltrs = null;
				objSelFltrs = null;
				objFltrElm = null;
				objNewElm = null;
				if(objDs!=null)
				{
					objDs.Dispose();
					objDs = null;
				}
				if(objDsGroups!=null)
				{
					objDsGroups.Dispose();
					objDsGroups=null;
				}
			}
  
		}

		/// Name			: Save
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 16 May 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Saves the data for autodiary wizard
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data to be saved</param>
		/// <returns>Saved xml document</returns>
		public XmlDocument Save(XmlDocument p_objXmlDoc)
		{
			int iRowId=0;
			int iDefIndex=0;
			int iSendCreate=0;
			int iSendUpdate=0;
			int iSendAdj=0;
			//Start Naresh MITS 8349 07/11/2006
			int iSendAdjSupervisor=0;
			//End Naresh MITS 8349 07/11/2006
			int iSendNotifyDys=0;			
			int iRouteDays=0;
			int iPriority=0;
			int iTaskEst=0;
			int iTaskEstTyp=0;
			int iTimBillable=0;
			int iExpSchedule=0;
            //rupal:r8 auto diary enh
            int iExpEmail = 0;
			int iBstPracId=0;
			int iDueDays=0;
			int iSendSupFlds=0;
            int iSecurityMgmtRelationship = 0;
            int iEscalate_Highestlevel = 0;
			string sAutoName="";
			string sDefName="";
			string sInstruct="";
			string sProcDt="";
			string sSendSupName="";
			XmlElement objElm = null;
			DbConnection objConn = null;
			DbCommand objCommand = null;
			DbTransaction objTran = null;
			XmlNodeList objNodLst=null;

			XmlElement objTemp=null;
			string[] arrIds;
			string[] arrNames;

			try
			{				
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefId");
				if(objElm!=null)
					iRowId = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryName");
				if(objElm!=null)
					sAutoName = objElm.InnerText;

                //commented by Nitin for R5 implementation on 23/12/2008
                ////objTemp = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DiaryTemplate']");
                ////objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DiaryTemplate']//level[@value='"+ objTemp.Attributes["selectedid"].Value +"']");

                //added by Nitin in place of above code
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryTemplate");
                if(objElm!=null)
				{
                    sDefName = objElm.GetAttribute("selectedname");
                    iDefIndex = Conversion.ConvertStrToInteger(objElm.Attributes["selectedid"].Value);
				}

//				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DefIndex']");
//				if(objElm!=null)
//					iDefIndex = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Security_Mgmt_Relationship");
                if (objElm != null)
                    iSecurityMgmtRelationship = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Escalate_Highestlevel");
                if (objElm != null)
                    iEscalate_Highestlevel = Conversion.ConvertStrToInteger(objElm.InnerText);


                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//NoOfDays");
				if(objElm!=null)
					iDueDays= Conversion.ConvertStrToInteger(objElm.InnerText);

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//UserCreated");
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/UserCreated");
				if(objElm!=null)
					iSendCreate = Conversion.ConvertStrToInteger(objElm.InnerText);

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//UserModified");
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/UserModified");
				if(objElm!=null)
					iSendUpdate = Conversion.ConvertStrToInteger(objElm.InnerText);

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//AdjAssig");
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/AdjAssig");
				if(objElm!=null)
					iSendAdj = Conversion.ConvertStrToInteger(objElm.InnerText);

				// Start - Naresh MITS 8349 06/11/2006
                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//AdjAssigSupervisor");
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/AdjAssigSupervisor");
				if(objElm!=null)
					iSendAdjSupervisor = Conversion.ConvertStrToInteger(objElm.InnerText);
				// End - Naresh MITS 8349 06/11/2006

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//NotificationDays");
				if(objElm!=null)
					iSendNotifyDys = Conversion.ConvertStrToInteger(objElm.InnerText);

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted//ReRoutedDays");
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted/ReRoutedDays");
				if(objElm!=null)
					iRouteDays = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//PriorityLevel");
				if(objElm!=null)
                    iPriority = Conversion.ConvertStrToInteger(objElm.GetAttribute("selectedid"));

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//EstTime");
				if(objElm!=null)
					iTaskEst = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//MinsHrs");
				if(objElm!=null)
					iTaskEstTyp = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//TimeAssoFlag");
				if(objElm!=null)
					iTimBillable = Conversion.ConvertStrToInteger(objElm.InnerText);

                //RUPAL:START, R8 AUTO DIARY ENH
                /*
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ExportFlag");
				if(objElm!=null)
					iExpSchedule = Conversion.ConvertStrToInteger(objElm.InnerText);
                */
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ExportToEmailFlag");
                if (objElm != null)
                    iExpEmail = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Instruction");
				if(objElm!=null)
					sInstruct = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryDate");
                if (objElm != null)
                    //sProcDt =UTILITY.FormatDate(objElm.InnerText,true);
                    sProcDt = Conversion.GetDate(objElm.InnerText);// objUserLogin.objUser.NlsCode.ToString(), m_iClientId);//vkumar258 ML Changes

				if(iRowId==0)
					iBstPracId =0;

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//EventSuppUser");
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/EventSuppUser");
				if(objElm!=null)
					iSendSupFlds = Conversion.ConvertStrToInteger(objElm.InnerText);

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SuppField");
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SuppField");
                if (objElm != null)
                    sSendSupName = objElm.GetAttribute("selectedname");

				if(iRowId==0)
				{
                    iRowId = Utilities.GetNextUID(m_sConnStr, "WPA_AUTO_SET", m_iClientId);
				}

				objConn = DbFactory.GetDbConnection(m_sConnStr);
				objConn.Open();
				objCommand=objConn.CreateCommand();
                //jramkumar for MITS 34882 
                //objTran=objConn.BeginTransaction();
                //objCommand.Transaction=objTran;

				objCommand.CommandText = "DELETE FROM WPA_AUTO_SET WHERE AUTO_ROW_ID = " + iRowId;
				objCommand.ExecuteNonQuery();

				// Start - Naresh MITS 8349 06/11/2006
                //rupal:r8 auto diary enh, added the feild "EXPORT_EMAIL"
                //objCommand.CommandText = "INSERT INTO WPA_AUTO_SET(AUTO_ROW_ID, AUTO_NAME, DEF_NAME, DEF_INDEX," + 
                //    "SEND_CREATED, SEND_UPDATED, SEND_ADJUSTER,SEND_ADJUSTER_SUPERVISOR, NOTIFICATION_DAYS, ROUTE_DAYS, PRIORITY, TASK_ESTIMATE," + 
                //    "TASK_EST_TYPE, TIME_BILLABLE_FLAG, EXPORT_SCHEDULE, INSTRUCTIONS, PROCESS_DATE, BEST_PRACT_ID, DUE_DAYS, "+
                //    "SEND_SUPPFIELD, SEND_SUPPFIELDNAME,SECURITY_MGMT_RELATIONSHIP,ESCALATE_HIGHEST_LEVEL,EXPORT_EMAIL) VALUES(" +
                //    iRowId + ",'" + sAutoName + "','" + sDefName + "'," + iDefIndex + "," + iSendCreate + "," + iSendUpdate + "," + iSendAdj + "," + iSendAdjSupervisor + "," + 
                //    iSendNotifyDys + "," + iRouteDays + "," + iPriority + "," + iTaskEst + "," + iTaskEstTyp + "," +
                //    iTimBillable + "," + iExpSchedule + ",'" + sInstruct + "','" + sProcDt  + "'," + iBstPracId + "," +iDueDays +  "," +
                //    iSendSupFlds + ",'" + sSendSupName + "'," + iSecurityMgmtRelationship + "," + iEscalate_Highestlevel + "," + iExpEmail + ")";

                //// End - Naresh MITS 8349 06/11/2006
                //amar mits 30818 starts
                string strSQL = string.Empty;
                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                DbConnection objCon = null;
                objCon = DbFactory.GetDbConnection(m_sConnStr);
                objCon.Open();

                strSQL = string.Format("INSERT INTO WPA_AUTO_SET (AUTO_ROW_ID, AUTO_NAME, DEF_NAME, DEF_INDEX," +
                    "SEND_CREATED, SEND_UPDATED, SEND_ADJUSTER,SEND_ADJUSTER_SUPERVISOR, NOTIFICATION_DAYS, ROUTE_DAYS, PRIORITY, TASK_ESTIMATE," +
                    "TASK_EST_TYPE, TIME_BILLABLE_FLAG, EXPORT_SCHEDULE, INSTRUCTIONS, PROCESS_DATE, BEST_PRACT_ID, DUE_DAYS, " +
                    "SEND_SUPPFIELD, SEND_SUPPFIELDNAME,SECURITY_MGMT_RELATIONSHIP,ESCALATE_HIGHEST_LEVEL,EXPORT_EMAIL) VALUES("
                    + "{0},{1},{2},{3},"
                    + "{4},{5},{6},{7},{8},{9},{10},{11},"
                    + "{12},{13},{14},{15},{16},{17},{18},"
                    + "{19},{20},{21},{22},{23})",
                    "~iRowId~", "~sAutoName~", "~sDefName~", "~iDefIndex~", "~iSendCreate~", "~iSendUpdate~", "~iSendAdj~", "~iSendAdjSupervisor~",
                    "~iSendNotifyDys~", "~iRouteDays~", "~iPriority~", "~iTaskEst~", "~iTaskEstTyp~", "~iTimBillable~", "~iExpSchedule~", "~sInstruct~",
                    "~sProcDt~", "~iBstPracId~", "~iDueDays~", "~iSendSupFlds~", "~sSendSupName~", "~iSecurityMgmtRelationship~", "~iEscalate_Highestlevel~", " ~iExpEmail~");

                dictParams.Add("iRowId", iRowId);
                dictParams.Add("sAutoName", sAutoName);
                dictParams.Add("sDefName", sDefName);
                dictParams.Add("iDefIndex", iDefIndex);
                dictParams.Add("iSendCreate", iSendCreate);
                dictParams.Add("iSendUpdate", iSendUpdate);
                dictParams.Add("iSendAdj", iSendAdj);
                dictParams.Add("iSendAdjSupervisor", iSendAdjSupervisor);
                dictParams.Add("iSendNotifyDys", iSendNotifyDys);
                dictParams.Add("iRouteDays", iRouteDays);
                dictParams.Add("iPriority", iPriority);
                dictParams.Add("iTaskEst", iTaskEst);
                dictParams.Add("iTaskEstTyp", iTaskEstTyp);
                dictParams.Add("iTimBillable", iTimBillable);
                dictParams.Add("iExpSchedule", iExpSchedule);
                dictParams.Add("sInstruct", sInstruct);
                dictParams.Add("sProcDt", sProcDt);
                dictParams.Add("iBstPracId", iBstPracId);
                dictParams.Add("iDueDays", iDueDays);
                dictParams.Add("iSendSupFlds", iSendSupFlds);
                dictParams.Add("sSendSupName", sSendSupName);
                dictParams.Add("iSecurityMgmtRelationship", iSecurityMgmtRelationship);
                dictParams.Add("iEscalate_Highestlevel", iEscalate_Highestlevel);
                dictParams.Add("iExpEmail", iExpEmail);

                DbFactory.ExecuteNonQuery(m_sConnStr, strSQL, dictParams);
                //amar mits 30818 ends
                //objCommand.ExecuteNonQuery();

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //objNodLst = p_objXmlDoc.SelectNodes("//Filters//SelectedFilters//level");
                objNodLst = p_objXmlDoc.SelectNodes("//Filters/SelectedFilters/level");
				objCommand.CommandText = "DELETE FROM WPA_AUTO_FILTER WHERE AUTO_ID = " + iRowId;
				objCommand.ExecuteNonQuery();
				foreach(XmlNode objNod in objNodLst)
				{
					objCommand.CommandText = "INSERT INTO WPA_AUTO_FILTER(AUTO_ID,FILTER_INDEX,FILTER_DATA) VALUES(" +
						iRowId  + "," +  ((XmlElement)objNod).GetAttribute("id") + ",'" +   ((XmlElement)objNod).GetAttribute("data") + "')";
					objCommand.ExecuteNonQuery();
				}

				objCommand.CommandText = "DELETE FROM WPA_AUTO_ACT WHERE AUTO_ID = " + iRowId;
				objCommand.ExecuteNonQuery();
                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //if (((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity//WorkActivityIdList")).InnerText.Trim() != string.Empty)
                if (((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity/WorkActivityIdList")).InnerText.Trim() != string.Empty)
				{
                    //tanwar2 - Corrected XPath - Jan 4th, 2012
                    //arrIds =((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity//WorkActivityIdList")).InnerText.Split('|');
                    //arrNames = ((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity//WorkActivityNameList")).InnerText.Split('|');
                    arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity/WorkActivityIdList")).InnerText.Split('|');
                    arrNames = ((XmlElement)p_objXmlDoc.SelectSingleNode("//WorkActivity/WorkActivityNameList")).InnerText.Split('|');
					for(int iCtr = 0; iCtr < arrIds.Length ; iCtr++)
					{
                        //amar mits 30818 starts
                        dictParams = new Dictionary<string, object>();

                        //Array.Clear(arrIds,0,iCtr);
                        //objCommand.CommandText = "INSERT INTO WPA_AUTO_ACT(AUTO_ID,ACTIVITY_CODE,ACTIVITY_TEXT) VALUES(" +
                        //    iRowId  + "," +  arrIds[iCtr] + ",'" +   arrNames[iCtr] + "')";

                        strSQL = string.Format("INSERT INTO WPA_AUTO_ACT(AUTO_ID,ACTIVITY_CODE,ACTIVITY_TEXT) VALUES({0},{1},{2})", "~iRowId~", "~arrIds~", "~arrNames~");
                        dictParams.Add("iRowId", iRowId);
                        dictParams.Add("arrIds", arrIds[iCtr]);
                        dictParams.Add("arrNames", arrNames[iCtr]);
                        //objCmd.CommandText = strSQL.ToString();
                        DbFactory.ExecuteNonQuery(m_sConnStr, strSQL, dictParams);
                        //amar mits 30818 ends

					}
				}
                //jramkumar for MITS 34882 
                objTran = objConn.BeginTransaction();
                objCommand.Transaction = objTran;
				objCommand.CommandText = "DELETE FROM WPA_AUTO_USER WHERE AUTO_ID = " + iRowId;
				objCommand.ExecuteNonQuery();

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //if (((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendUsersIdList")).InnerText.Trim() != string.Empty)
                if (((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendUsersIdList")).InnerText.Trim() != string.Empty)
				{
                    //tanwar2 - Corrected XPath - Jan 4th, 2012
                    //arrIds =((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendUsersIdList")).InnerText.Split('|');
                    arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendUsersIdList")).InnerText.Replace(',', '|').Trim('|').Split('|'); //pkandhari Jira 6412
                    for(int iCtr = 0; iCtr < arrIds.Length ; iCtr++)
                    {
                        objCommand.CommandText = "INSERT INTO WPA_AUTO_USER(AUTO_ID,USER_ID,USER_TYPE) VALUES(" +
                                        iRowId  + "," +  arrIds[iCtr] + ",'1')";
                        objCommand.ExecuteNonQuery();
                    }
                }

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //if (((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified//UsersNotifiedIdList")).InnerText.Trim() != string.Empty)
                if (((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified/UsersNotifiedIdList")).InnerText.Trim() != string.Empty)
				{
                    //tanwar2 - Corrected XPath - Jan 4th, 2012
                    //arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified//UsersNotifiedIdList")).InnerText.Split('|');
                    arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersNotified/UsersNotifiedIdList")).InnerText.Replace(',', '|').Trim('|').Split('|'); //pkandhari Jira 6412
					for(int iCtr = 0; iCtr < arrIds.Length ; iCtr++)
					{
                        //nsachdeva2 - MITS: 27358 - 02/15/2012
                        //objCommand.CommandText = "INSERT INTO WPA_AUTO_USER(AUTO_`ID,USER_ID,USER_TYPE) VALUES(" +
                        objCommand.CommandText = "INSERT INTO WPA_AUTO_USER(AUTO_ID,USER_ID,USER_TYPE) VALUES(" +
							iRowId  + "," +  arrIds[iCtr] + ",'2')";
                        //End MITS: 27358
						objCommand.ExecuteNonQuery();
					}
				}

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //if (((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted//UsersRoutedIdList")).InnerText.Trim() != string.Empty)
                if (((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted/UsersRoutedIdList")).InnerText.Trim() != string.Empty)
				{
                    //tanwar2 - Corrected XPath - Jan 4th, 2012
                    //arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted//UsersRoutedIdList")).InnerText.Split('|');
                    arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//UsersRouted/UsersRoutedIdList")).InnerText.Replace(',', '|').Trim('|').Split('|'); //pkandhari Jira 6412
					for(int iCtr = 0; iCtr < arrIds.Length ; iCtr++)
					{
						objCommand.CommandText = "INSERT INTO WPA_AUTO_USER(AUTO_ID,USER_ID,USER_TYPE) VALUES(" +
							iRowId  + "," +  arrIds[iCtr]  + ",'3')";
						objCommand.ExecuteNonQuery();
					}
				}

                //tanwar2 - Corrected XPath - Jan 4th, 2012
                //if (((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendGroupsIdList")).InnerText.Trim() != string.Empty)
                if (((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendGroupsIdList")).InnerText.Trim() != string.Empty)
				{
                    //tanwar2 - Corrected XPath - Jan 4th, 2012
                    //arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo//SendGroupsIdList")).InnerText.Split('|');
                    arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SendGroupsIdList")).InnerText.Replace(',', '|').Trim('|').Split('|'); //pkandhari Jira 6412
                    for (int iCtr = 0; iCtr < arrIds.Length; iCtr++)
                    {
                        objCommand.CommandText = "INSERT INTO WPA_AUTO_USER(AUTO_ID,USER_ID,USER_TYPE) VALUES(" +
                                iRowId + "," + arrIds[iCtr] + ",'4')";
                        objCommand.ExecuteNonQuery();
                    }
                }

                //pkandhari Jira 6412 starts
                if (((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsNotifiedIdList")).InnerText.Trim() != string.Empty)
                {
                    arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsNotifiedIdList")).InnerText.Replace(',', '|').Trim('|').Split('|');
                    for (int iCtr = 0; iCtr < arrIds.Length; iCtr++)
                    {
                        objCommand.CommandText = "INSERT INTO WPA_AUTO_USER(AUTO_ID,USER_ID,USER_TYPE) VALUES(" +
                            iRowId + "," + arrIds[iCtr] + ",'5')";
                        objCommand.ExecuteNonQuery();
                    }
                }

                if (((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsRoutedIdList")).InnerText.Trim() != string.Empty)
                {
                    arrIds = ((XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/GroupsRoutedIdList")).InnerText.Replace(',', '|').Trim('|').Split('|');
                    for (int iCtr = 0; iCtr < arrIds.Length; iCtr++)
                    {
                        objCommand.CommandText = "INSERT INTO WPA_AUTO_USER(AUTO_ID,USER_ID,USER_TYPE) VALUES(" +
                            iRowId  + "," + arrIds[iCtr] + ",'6')";
                        objCommand.ExecuteNonQuery();
                    }
                }
                //pkandhari Jira 6412 ends

				objTran.Commit();

				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
				objTran.Rollback();
				throw new RMAppException (Globalization.GetString("WPAAutoDiarySetup.Save.Err",m_iClientId),p_objEx);   //sonali
			}
			finally
			{
				if(objTran!=null)
				{
					objTran.Dispose();
					objTran = null;
				}
				if(objConn != null)
				{
					objConn.Dispose();
					objConn = null;
				}
				objNodLst=null;
				objCommand = null;
				objElm = null;
			}
		}


		/// Name			: LoadInfoSet
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 16 May 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the InfoSet data
		/// </summary>
		/// <param name="p_objXml">Input xml document with data</param>
		/// <param name="p_ID">Auto Row Id</param>
		private void LoadInfoSet(XmlDocument p_objXml, int p_ID)
		{
			int iFilter = 0;
			DbReader objRdr = null;
			try
			{
				objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM WPA_AUTO_SET WHERE AUTO_ROW_ID = " + p_ID);
				if(objRdr!=null)
				{
					if(objRdr.Read())
					{
						WPAAUTO objAuto = new WPAAUTO(m_sConnStr); 
						LoadInfoDef(p_objXml,objRdr.GetInt32("DEF_INDEX"));					
						objInfoSet.Name = objRdr.GetString("AUTO_NAME");
                        p_objXml.SelectSingleNode("//DiaryName").InnerText = objRdr.GetString("AUTO_NAME");
						objInfoSet.Index = objRdr.GetInt32("DEF_INDEX");
						objInfoSet.DefName = objInfoSet.Name;
						objInfoSet.SendCreate = objRdr.GetInt16("SEND_CREATED");
						objInfoSet.SendUpdate = objRdr.GetInt16("SEND_UPDATED");
						objInfoSet.SendAdj = objRdr.GetInt16("SEND_ADJUSTER");
						//Start Naresh MITS 8349 07/11/2006
						objInfoSet.SendAdjSupervisor = objRdr.GetInt16("SEND_ADJUSTER_SUPERVISOR");
						//End Naresh MITS 8349 07/11/2006
						objInfoSet.NotifyDays = objRdr.GetInt32("NOTIFICATION_DAYS");
						objInfoSet.RouteDays = objRdr.GetInt32("ROUTE_DAYS");
						objInfoSet.Priority = objRdr.GetInt32("PRIORITY");
						objInfoSet.TaskEst = objRdr.GetInt32("TASK_ESTIMATE");
						objInfoSet.TaskEstType = objRdr.GetInt32("TASK_EST_TYPE");
						objInfoSet.TimeBillable = objRdr.GetInt16("TIME_BILLABLE_FLAG");
						objInfoSet.ExpSchedule = objRdr.GetInt32("EXPORT_SCHEDULE");                       
                        //rupal:start, r8 auto diary enh
                        objInfoSet.ExportEmail = objRdr.GetInt32("EXPORT_EMAIL");                        
                        //rupal:end
						objInfoSet.Instruct = objRdr.GetString("INSTRUCTIONS");
						objInfoSet.ProcessDate = objRdr.GetString("PROCESS_DATE");
						objInfoSet.iDueDays = objRdr.GetInt16("DUE_DAYS");
						objInfoSet.SendSuppField = objRdr.GetInt16("SEND_SUPPFIELD");
						objInfoSet.SendSuppFieldName = objRdr.GetString("SEND_SUPPFIELDNAME");
                        objInfoSet.Escalate_Highest_Level = objRdr.GetInt16("ESCALATE_HIGHEST_LEVEL");
                        objInfoSet.Security_Mgmt_Relationship = objRdr.GetInt16("SECURITY_MGMT_RELATIONSHIP");
					}  
				}

				objRdr.Close();
			
				//Get Filters
				objInfoSet.FilterSet = new  FilterSetting[100];
				objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM WPA_AUTO_FILTER WHERE AUTO_ID = " + p_ID);
				if(objRdr!=null)
				{
					while(objRdr.Read())
					{
						objInfoSet.FilterSet[iFilter].Number = objRdr.GetInt32("FILTER_INDEX");
						objInfoSet.FilterSet[iFilter].Data = objRdr.GetString("FILTER_DATA");
						iFilter +=1;
					}
				}
				objRdr.Dispose();
				objInfoSet.NumFilters = iFilter;

				//Get Activities
				objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM WPA_AUTO_ACT WHERE AUTO_ID = " + p_ID);
				if(objRdr!=null)
				{
					while(objRdr.Read())
					{
						objInfoSet.Activities += objRdr.GetInt32("ACTIVITY_CODE") + "|" + objRdr.GetString("ACTIVITY_TEXT") + ",";
					}
				}
				objRdr.Close();
 
				//Get Users
				objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM WPA_AUTO_USER WHERE AUTO_ID = " + p_ID);
				if(objRdr!=null)
				{
					while(objRdr.Read())
					{
						switch(objRdr.GetInt32("USER_TYPE").ToString())
						{
							case "1":
								objInfoSet.SendUsers += objRdr.GetInt32("USER_ID") + ",";
								break;
							case "2":
								objInfoSet.NotifyUsers += objRdr.GetInt32("USER_ID") + ",";
								break;
							case "3":
								objInfoSet.RouteUsers += objRdr.GetInt32("USER_ID") + ",";
								break;
							case "4":
								objInfoSet.SendGroups += objRdr.GetInt32("USER_ID") + ",";
								break;
                                // pkandhari Jira 6412 starts
                            case "5":
                                objInfoSet.NotifyGroups += objRdr.GetInt32("USER_ID") + ",";
                                break;
                            case "6":
                                objInfoSet.RouteGroups += objRdr.GetInt32("USER_ID") + ",";
                                break;
                                // pkandhari Jira 6412 ends
						}
					}
				}
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("WPAAutoDiarySetup.LoadInfoSet.Err",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr= null;
				}
			}
		}


		/// Name			: LoadInfoDef
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 16 May 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Load the Info Definition data
		/// </summary>
		/// <param name="p_objXml">Input xml document with data</param>
		/// <param name="p_ID">row id</param>
		/// <returns>xml document with data</returns>
		public XmlDocument LoadInfoDef(XmlDocument p_objXml,int p_ID)
		{
			int iPtr = 0;
			int iInfoNum = 0;
			int iFilterNum = -1;
			int iNumFilters = 0;
			int iTmplateNo=0;
			int iDbType =0;
			bool bEnd = false;
            //Start-Mridul Bansal. 01/19/10. MITS#18229.
			//bool bUseEnhPol=false;
            bool bUseEnhPolforGL = false;
            bool bUseEnhPolforAL = false;
            bool bUseEnhPolforPC = false;
            bool bUseEnhPolforWC = false;
            const int LOB_FOR_GC = 241;
            const int LOB_FOR_WC = 243;
            const int LOB_FOR_VA = 242;
            const int LOB_FOR_PC = 845;
            ColLobSettings objLobSettings = null;
            //End-Mridul Bansal. 01/19/10. MITS#18229.
			string sFileName="";
			string sKeyWord="";
			string sValue = "";
			string sTmp ="";
			RMConfigurator objConfig=null;
			StreamReader objSRdr = null;
			XmlElement objTmplate = null;
			XmlElement objNewTmplate= null;
			XmlElement objDiaryTemp = null;
			XmlElement objAvlFltrs = null;
			Riskmaster.Settings.SysSettings objSys = null;

			try
			{
				objInfoDef.NumFilters=0; 
				objSys  = new Riskmaster.Settings.SysSettings(m_sConnStr, m_iClientId); //sonali 
                //Start-Mridul Bansal. 01/19/10. MITS#18229
				//bUseEnhPol = Conversion.ConvertLongToBool(objSys.UseEnhPolFlag);
                objLobSettings = new ColLobSettings(m_sConnStr,m_iClientId);
                bUseEnhPolforAL = objLobSettings[LOB_FOR_VA].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolforGL = objLobSettings[LOB_FOR_GC].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolforPC = objLobSettings[LOB_FOR_PC].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolforWC = objLobSettings[LOB_FOR_WC].UseEnhPolFlag == -1 ? true : false;
                //End-Mridul Bansal. 01/19/10. MITS#18229
				objSys = null;
                sFileName = String.Format(@"{0}\{1}", RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "WPA"), UTILITY.GetConfigUtilitySettings(m_sConnStr, m_iClientId)["WPAInfoDevFile"]);//rkaur27
                //Geeta 03/22/07 : Modified for Mits number 7073
				//objInfoDef.FilterDef = new FilterDefinition[110];
                //rupal:for R8 Auto Diary Enhancement
                objInfoDef.FilterDef = new FilterDefinition[182];
                
                objTmplate = (XmlElement)p_objXml.SelectSingleNode("//DiaryTemplate");
                objAvlFltrs = (XmlElement)p_objXml.SelectSingleNode("//Filters/AvlFilters");

				objDiaryTemp = p_objXml.CreateElement("AutoDiaryTemplate");
				p_objXml.DocumentElement.AppendChild(objDiaryTemp);			

				if (File.Exists(sFileName))
				{
					using (objSRdr = new StreamReader(sFileName)) 
					{
						string sLine = "";
						while ((sLine = objSRdr.ReadLine())!= null)
						{
							sLine = sLine.Trim();
							if(sLine!="")
							{
								if (sLine.Substring(0,1)=="[")
								{
									sKeyWord = sLine.Substring(1,sLine.Length - 2);
									if(sKeyWord.ToUpper()=="FILTER")
									{
										iFilterNum +=1;
										iNumFilters +=1; 
										objInfoDef.NumFilters++; 
									}
									else
									{
										if (bEnd)
											break;
										if (p_ID > 0)
										{
											iFilterNum = -1;
											objInfoDef.NumFilters=0; 
										}
										iNumFilters = -1;
										iInfoNum  += 1;
										objInfoDef.Name = sKeyWord; 
										if(iNumFilters == -1)
										{
											if(sLine.ToUpper()=="[POLICY REVIEW]")
                                            {
                                                
                                                //Start-Mridul Bansal. 01/19/10. MITS#18229
												//if(!bUseEnhPol)
                                                if (!bUseEnhPolforAL || !bUseEnhPolforGL || !bUseEnhPolforPC || !bUseEnhPolforWC)
                                                //End-Mridul Bansal. 01/19/10. MITS#18229
												{
													
													objNewTmplate = p_objXml.CreateElement("level");
													objNewTmplate.SetAttribute("name",sKeyWord);
                                                    //rupal:start, r8 auto diary enh
                                                    //following line assumes that the templates in def file are
                                                    //in proper sequence, i.e if 12 templates are defined, their templated id would be
                                                    //1 to 12 in sequence. Actully this is not an ideal situation, because if we remove any template
                                                    //from def file in future, we will not change the template id of other filters to follow the strict  sequence
                                                    //for e.g if  any template with id 9 has become obsolete and we want to remove it from the def file,
                                                    // we will just remove the template but not change/shift the id of other templates yo follow the sequence
                                                    //therefore following line is incorrect as it assumes that all id in def file are in sequnce with increament value as 1
                                                    //we actually need to put the def_inex of the template here, so we will do it in switch case where we find the def_indx
                                                    
                                                    //iTmplateNo++;
													//objNewTmplate.SetAttribute("value",iTmplateNo.ToString());    
                                                    //objTmplate.AppendChild(objNewTmplate);
                                                    //rupal:end
													
												}
											}
											else if(sLine.ToUpper()=="[POLICY MANAGEMENT REVIEW]")
                                            {
                                                
                                                //Start-Mridul Bansal. 01/19/10. MITS#18229
                                                //if(bUseEnhPol)
                                                if (bUseEnhPolforAL || bUseEnhPolforGL || bUseEnhPolforPC || bUseEnhPolforWC)
                                                //End-Mridul Bansal. 01/19/10. MITS#18229
												{
													
													objNewTmplate = p_objXml.CreateElement("level");
													objNewTmplate.SetAttribute("name",sKeyWord);
                                                    //rupal:start,r8 nauto diary enh
                                                    //iTmplateNo++;
													//objNewTmplate.SetAttribute("value",iTmplateNo.ToString());
													//objTmplate.AppendChild(objNewTmplate);
                                                    //rupal:end, r8 auto diary enh
												}
											}
											else
											{
												
												objNewTmplate = p_objXml.CreateElement("level");
												objNewTmplate.SetAttribute("name",sKeyWord);
                                                //rupal:start,r8 nauto diary enh
                                                //iTmplateNo++;
                                                //objNewTmplate.SetAttribute("value",iTmplateNo.ToString());
                                                //objTmplate.AppendChild(objNewTmplate);
                                                //rupal:end, r8 auto diary enh
											}
										}									
									}
								}
								else
								{
									iPtr =  sLine.IndexOf("=");
									if(sLine.Substring(0,2)=="//" || iPtr == 0 )
										iPtr  = 1;
									sKeyWord = sLine.Substring(0, iPtr) ;
									sValue = sLine.Substring(iPtr+1,sLine.Length-iPtr-1).Trim();
									switch(sKeyWord.ToUpper()) 
									{
                                        //rupal:start,r8 auto diary setup enh
                                        case "DATABASE":
                                            objInfoDef.FilterDef[iFilterNum].Database = sValue;
                                            break;
                                        //rupal:end
										case "NAME":
											objInfoDef.FilterDef[iFilterNum].Name  = sValue;
											break;
										case "ID":
											if (iNumFilters == -1)
											{
                                                objInfoDef.ID = Conversion.ConvertStrToInteger(sValue);
                                                if (p_ID == Conversion.ConvertStrToInteger(sValue))
													bEnd = true;
                                                //rupal:start, r8 auto diary enh                                                
                                                //adding child here solves two problem,
                                                //1. we will add correct def_index of the template
                                                //2.in cae of edit mode in auto diary setup, we will add only the template that was
                                                //selected for edit
                                                if (p_ID > 0)
                                                {
                                                    if (bEnd == true)
                                                    {                                                        
                                                        objNewTmplate.SetAttribute("value", sValue);
                                                        objTmplate.AppendChild(objNewTmplate);                                                     
                                                    }
                                                }
                                                else
                                                {
                                                    
                                                    objNewTmplate.SetAttribute("value", sValue);
                                                    objTmplate.AppendChild(objNewTmplate);                                                    
                                                }
                                                //rupal:end, r8 auto diary enh
											}
											else
											{
												objInfoDef.FilterDef[iFilterNum].ID = Conversion.ConvertStrToInteger(sValue);
												objInfoDef.FilterDef[iFilterNum].TemplateId=objInfoDef.ID;
											}
											break;
										case "ATTACH_TABLE":
											objInfoDef.AttachTable = sKeyWord;
											break;
										case "ATTACH_COLUMN":
											objInfoDef.AttachCol =  sKeyWord;
											break;
										case "ATTACH_DESC":
											objInfoDef.AttachDesc = sKeyWord;
											break;
										case "SQLFILL":	
											objInfoDef.FilterDef[iFilterNum].SQLFill=sValue;
											objInfoDef.tmpSQL = sValue;
											break;
										case "SQLFROM":
											objInfoDef.FilterDef[iFilterNum].SQLFrom=sValue;
											objInfoDef.tmpSQL = sValue;
											break;
										case "SQLWHERE":
										case "TMPSQL":
											//TMPSQL SQLFILL and SQL have the same code
										case "SQL":
											if(sValue.Substring(0,1)=="[")
											{
												iPtr = sValue.IndexOf("]");
												sTmp = sValue.Substring(2,iPtr-2);
												sValue = sValue.Substring(0, iPtr+1);
												switch(sTmp.ToUpper())
												{
													case "SQLSERVER":
														iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
														break;
													case "SYBASE":
														iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
														break;
													case "INFORMIX":
														iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
														break;
													case "ORACLE":
														iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
														break;
													case "DB2":
														iDbType = (int)eDatabaseType.DBMS_IS_DB2;
														break;
													default:
														iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
														break;
												}
												if (iDbType == m_iDbType )
												{
													objInfoDef.tmpSQL = sValue;
													if (iFilterNum >= 0)
														objInfoDef.FilterDef[iFilterNum].SQLWhere=sValue;
												}
											}
											else
											{
												objInfoDef.tmpSQL = sValue;
												if (iFilterNum >= 0)
													objInfoDef.FilterDef[iFilterNum].SQLWhere=sValue;
											}
											break;
										case "TYPE":
										switch(sValue.ToUpper())
										{
											case "CHECKBOX":
												objInfoDef.FilterDef[iFilterNum].FilterType = 1;
												break;
											case "LIST":
												objInfoDef.FilterDef[iFilterNum].FilterType = 2;
												break;
											case "TOPX":
												objInfoDef.FilterDef[iFilterNum].FilterType = 3;
												break;
											case "COMBO":
												objInfoDef.FilterDef[iFilterNum].FilterType = 4;
												break;
											case "DURATION":
												objInfoDef.FilterDef[iFilterNum].FilterType = 5;
												break;
                                            case "NO_OPTION":
                                                objInfoDef.FilterDef[iFilterNum].FilterType = 6;
												break;
											default:
												objInfoDef.FilterDef[iFilterNum].FilterType = 0;
												break;
										}
											break;
										case "MIN":
											// MIN and DEFAULT have the same logic
										switch (sValue)
										{
											case "CURRENT_YEAR":
												objInfoDef.FilterDef[iFilterNum].FilterMin = System.DateTime.Now.Year;
												break;
											default:
												objInfoDef.FilterDef[iFilterNum].FilterMin =Conversion.ConvertStrToLong(sValue);
												break;
										}
											break;
										case "DEFAULT":
											objInfoDef.FilterDef[iFilterNum].DefValue=sValue;
											break;
										case "MAX":
											objInfoDef.FilterDef[iFilterNum].FilterMax = Conversion.ConvertStrToLong(sValue);
											break;
										case "TABLE":
											objInfoDef.FilterDef[iFilterNum].table = sValue;
											break;
                                         
									}
								}
							}						
						}
					}
					if(p_ID>objInfoDef.ID)
						objInfoDef.Name = "";
				}
				return p_objXml;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("WPAAutoDiarySetup.LoadInfoDef.Err",m_iClientId),p_objEx); //sonali 
			}
			finally
			{
                if (objSRdr != null)
                    objSRdr.Dispose();
				objTmplate = null;
				objNewTmplate= null;
				objDiaryTemp = null;
				objAvlFltrs = null;
				objSys = null;
			}
		}


		public XmlDocument GetAutoDiaryWorkActivities(XmlDocument p_objXmlDoc,UserLogin objUser) 
		{
			XmlElement objDiary = null;
			int iDiaryId=0;
			XmlDocument objReturnDoc=null;

			XmlElement objRootElement=null;
			XmlElement objChildElement=null;

			DbReader objRdr = null;
			string sSQL=string.Empty;

			try
			{
				objDiary = (XmlElement)p_objXmlDoc.SelectSingleNode("//DiaryId");
				if(objDiary!=null)
					iDiaryId = Conversion.ConvertStrToInteger(objDiary.InnerText);

				objReturnDoc=new XmlDocument();
				objRootElement=objReturnDoc.CreateElement("WorkActivities");
				objRootElement.SetAttribute("diaryid",iDiaryId.ToString());
				objReturnDoc.AppendChild(objRootElement);

                //Asharma326 MITS 33772 Starts //nnithiyanand 34316

                eDatabaseType edt = DbFactory.GetDatabaseType(m_sConnStr);
                string DateforDB = "";
                if (edt == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    DateforDB = "GETDATE()";
                }
                else { DateforDB = " TO_CHAR(sysdate,'YYYYMMDD')"; }

                //Asharma326 MITS 33772 Ends //nnithiyanand 34316

                //Aman ML Change --start
				sSQL =	"SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT, GLOSSARY " +
					" WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'WPA_ACTIVITIES' " +
					" AND CODES.CODE_ID = CODES_TEXT.CODE_ID " +
					" AND CODES.TABLE_ID = GLOSSARY.TABLE_ID " +
					" AND (CODES.DELETED_FLAG=0 OR CODES.DELETED_FLAG IS NULL)" +
                    " AND (CODES_TEXT.LANGUAGE_CODE = 1033) " +
					" AND (GLOSSARY.DELETED_FLAG=0 OR GLOSSARY.DELETED_FLAG IS NULL) " +
                    " AND (NULLIF(codes.EFF_END_DATE,'NULL') >= " + DateforDB + " or CODES.EFF_END_DATE IS NULL) " +   //nnithiyanand 34316//Added for mits 35036 sharishkumar
                    //Added by Sharishkumar for Mits 35036
                    "AND (NULLIF(CODES.EFF_START_DATE,'NULL') <= " + DateforDB + " or CODES.EFF_START_DATE IS NULL or CODES.EFF_START_DATE = 'NULL') " +
                    ////End Mits 35036
					" ORDER BY CODES_TEXT.CODE_DESC";
                StringBuilder sbSQL = new StringBuilder();
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, "WPA_ACTIVITIES", objUser.objUser.NlsCode); 
				//objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                objRdr = DbFactory.GetDbReader(m_sConnStr, sbSQL.ToString());    //Aman ML Change --end
				if(objRdr!=null)
				{
					while(objRdr.Read())
					{
						objChildElement=objReturnDoc.CreateElement("activity");
						objChildElement.SetAttribute("id",objRdr.GetInt32("CODE_ID").ToString());
						objChildElement.InnerText=objRdr.GetString(1) + " - " + objRdr.GetString(2);//skhare7 MITS 31259
						objRootElement.AppendChild(objChildElement);
					}
					objRdr.Close();
				}
				return objReturnDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("WPAAutoDiarySetup.GetAutoDiaryWorkActivities.Err",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr= null;
				}
				objReturnDoc=null;
				objRootElement=null;
				objChildElement=null;
			}
		}

		private void FillSuppCombo(ref XmlDocument p_objXmlDoc)
		{
			DbReader objRdr = null;
			string sSQL=string.Empty;
			XmlElement objSuppField=null;
			XmlElement objNewElm=null;

			try
			{
				sSQL="SELECT SYS_FIELD_NAME,CODE_FILE_ID FROM SUPP_DICTIONARY WHERE FIELD_TYPE = 8 AND";
                sSQL = sSQL + " SUPP_TABLE_NAME = 'EVENT_SUPP' AND DELETE_FLAG = 0";

                objSuppField = (XmlElement)p_objXmlDoc.SelectSingleNode("//SendDiariesTo/SuppField");

				objNewElm = p_objXmlDoc.CreateElement("level");
				objNewElm.SetAttribute("id","0");
				objNewElm.InnerText=string.Empty;
				objSuppField.AppendChild(objNewElm);

				objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
				if(objRdr!=null)
				{
					while(objRdr.Read())
					{
						objNewElm = p_objXmlDoc.CreateElement("level");
						objNewElm.SetAttribute("id",objRdr.GetInt32("CODE_FILE_ID").ToString());
						objNewElm.InnerText="EVENT_SUPP."+objRdr.GetString("SYS_FIELD_NAME");
						if (objNewElm.InnerText==objInfoSet.SendSuppFieldName)
							objSuppField.SetAttribute("selectedid",objRdr.GetInt32("CODE_FILE_ID").ToString());
						objSuppField.AppendChild(objNewElm);
					}
					objRdr.Close();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("WPAAutoDiarySetup.Get.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr= null;
				}
				objSuppField=null;
				objNewElm=null;
			}
		}
	}
}
