using System;
using C1.C1PrintDocument;
using System.Drawing; 
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;
using System.IO;

namespace Riskmaster.Application.RMUtilities
{
	/**************************************************************
	 * $File		: PrintWrapper.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 04/05/2005
	 * $Author		: Parag Sarin
	 * $Comment		: PrintWrapper class has basic functions to create Print Document.
	 * $Source		:  	
	**************************************************************/
	public class PrintWrapper
	{
		#region Variable Declarations
		/// <summary>
		/// Private variable to store C1PrintDocumnet object instance.
		/// </summary>
		C1PrintDocument m_objPrintDoc = null ;
		/// <summary>
		/// Private variable to store CurrentX
		/// </summary>
		private double m_dblCurrentX = 0 ;
		/// <summary>
		/// Private variable to store CurrentY
		/// </summary>
		private double m_dblCurrentY = 0 ;
		/// <summary>
		/// Private variable to store CurrentX
		/// </summary>
		private double m_dblPageLimit = 0 ;
		/// <summary>
		/// Private variable to store Font object.
		/// </summary>
		private Font m_objFont = null ;
		/// <summary>
		/// Private variable to store Text object.
		/// </summary>
		private RenderText m_objText = null ;
		/// <summary>
		/// Private variable to store PageWidth
		/// </summary>
		private double m_dblPageWidth = 0.0 ;
		/// <summary>
		/// Private variable to store PageHeight
		/// </summary>
		private double m_dblPageHeight = 0.0 ;		

		private int m_iPageNumber = 1;

        private int m_iClientId = 0;

		#endregion 

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>		
        internal PrintWrapper(int p_iClientId)
		{			
			m_objPrintDoc = new C1PrintDocument();
			m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;			
			m_objPrintDoc.PageSettings.Margins.Top = 0 ;
			m_objPrintDoc.PageSettings.Margins.Left = 0 ;
			m_objPrintDoc.PageSettings.Margins.Bottom = 0 ;
			m_objPrintDoc.PageSettings.Margins.Right = 0 ;
            m_iClientId = p_iClientId;	
		}
		#endregion 

		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_bIsLandscape">IsLandscape</param>
		internal void StartDoc( bool p_bIsLandscape )
		{		
            //Rectangle objRect ;

            try
            {
                m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape;
                //RMConfigurator objConfig=null;
                //objConfig=new RMConfigurator();
                //m_objPrintDoc.PageSettings.PrinterSettings.PrinterName=objConfig.GetValue("PrinterName");
                //objConfig=null;
                //TR 2048  Nitesh 01March2006: Starts
                //				m_objPrintDoc.StartDoc();


                //				objRect = m_objPrintDoc.PageSettings.Bounds ;			
                //				m_dblPageHeight = ( objRect.Height / 100 ) * 1440 ;
                //				m_dblPageWidth = ( objRect.Width / 100 ) * 1440  ;
                //				if( p_bIsLandscape )
                //					m_dblPageHeight += 700 ;
                //				else
                //					m_dblPageWidth += 700 ;
                //			
                if (p_bIsLandscape)
                {
                    m_dblPageHeight = 8.6 * 1440;
                    m_dblPageWidth = 11.2 * 1440;

                }
                else
                {
                    m_dblPageHeight = 11 * 1440;
                    m_dblPageWidth = 8.7 * 1440;

                }
                m_objPrintDoc.StartDoc();
                //TR 2048  Nitesh 01March2006: Ends
                m_objFont = new Font("Arial", 6);
                m_objText = new RenderText(m_objPrintDoc);
                m_objText.Style.Font = m_objFont;
                m_objText.Style.WordWrap = false;
                //m_objText.Width = m_dblPageWidth ;		
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.StartDoc.Error", m_iClientId), p_objEx);
            }
            
		}
		
		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		internal double PageHeight 
		{
			get
			{
				return( m_dblPageHeight ) ;
			}			
		}
		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		internal double PageLimit 
		{
			set
			{
				m_dblPageLimit = value;
			}			
		}

		/// <summary>
		/// Read property for PageNumber.
		/// </summary>
		internal int PageNumber
		{
			get
			{
				return(m_iPageNumber);
			}			
		}

		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		internal double PageWidth 
		{
			get
			{
				return( m_dblPageWidth ) ;
			}			
		}
		/// <summary>
		/// Read/Write property for CurrentX.
		/// </summary>
		internal double CurrentX 
		{
			get
			{
				return( m_dblCurrentX ) ;
			}
			set
			{
				m_dblCurrentX = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CurrentY.
		/// </summary>
		internal double CurrentY 
		{
			get
			{
				return( m_dblCurrentY ) ;
			}
			set
			{
				m_dblCurrentY = value ;
			}
		}		

		/// <summary>
		/// Read property for FontSize.
		/// </summary>
		internal double FontSize 
		{
			get
			{
				return( m_objFont.Size) ;
			}
		}

		/// <summary>
		/// Read property for FontSize.
		/// </summary>
		internal string FontName 
		{
			get
			{
				return(m_objFont.Name) ;
			}
		}
		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void PrintTextNew( string p_sPrintText , bool p_bIsContinue)
		{
			try
			{
				if(p_bIsContinue)
				{
					m_objText.Text = p_sPrintText;
				}
				else
				{
					m_objText.Text = p_sPrintText + "\r";
				}
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX , m_dblCurrentY , m_objText);
				if(p_bIsContinue)
					m_dblCurrentX += GetTextWidth(m_objText.Text); 
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextNew.Error", m_iClientId), p_objEx);				
			}
		}
		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void PrintText( string p_sPrintText , bool p_bIsContinue)
		{
			try
			{
				if(m_dblCurrentY >= m_dblPageLimit && m_dblPageLimit != 0) 
				{
					this.NewPage();
					m_dblCurrentX = 75;
					m_dblCurrentY = (500 - this.GetTextWidth(p_sPrintText))/2 + 75; 
					m_iPageNumber++;
				}
 
				if(p_bIsContinue)
				{
					m_objText.Text = p_sPrintText;
				}
				else
				{
					m_objText.Text = p_sPrintText + "\r";
				}
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX , m_dblCurrentY , m_objText);
				if(p_bIsContinue)
					m_dblCurrentX += GetTextWidth(m_objText.Text); 
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintText.Error", m_iClientId), p_objEx);				
			}
		}
		
		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void PrintNextLine()
		{
			try
			{
				m_objText.Text = "\n\r" ;
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX , m_dblCurrentY , m_objText);
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextNoCr.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void FillRect(double p_dX1, double p_dY1, double p_dX2, double p_dY2)
		{
			try
			{
				C1DocStyle objStyle = new C1DocStyle(m_objPrintDoc);
				objStyle.BackColor = System.Drawing.Color.LightGray; 
				m_objPrintDoc.RenderDirectRectangle(p_dX1,p_dY1,p_dX2,p_dY2,objStyle);    
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextNoCr.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Print text such that it ends at given X Co-ordinate.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		internal void PrintTextEndAt( string p_sPrintText )
		{
			try
			{
				this.CurrentX = m_dblCurrentX - this.GetTextWidth( p_sPrintText ) ;
				this.PrintText( p_sPrintText,false);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextEndAt.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_sFontName">Font Name</param>
		/// <param name="p_dblFontSize">Font Size</param>
		internal void SetFont( string p_sFontName , double p_dblFontSize )
		{
			try
			{
				m_objFont = new Font( p_sFontName , (float)p_dblFontSize );
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_dblFontSize">Font Size</param>
		internal void SetFont(double p_dblFontSize)
		{
			try
			{
				m_objFont = new Font( m_objFont.Name , (float)p_dblFontSize );
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_sFontName">Font Name</param>
		internal void SetFont( string p_sFontName)
		{
			try
			{
				m_objFont = new Font( p_sFontName , m_objFont.Size);
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Set Font Bold
		/// </summary>
		/// <param name="p_bBold">Bool flag</param>
		internal void SetFontBold( bool p_bBold )
		{		
			try
			{
				if( p_bBold )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold );
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font = m_objFont ;

			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFontBold.Error", m_iClientId), p_objEx);				
			}
 
		}
		/// <summary>
		/// Set Font Italic
		/// </summary>
		/// <param name="p_bItalic">Bool flag</param>
		internal void SetFontUnderline( bool p_bUnderline )
		{		
			try
			{
				if( p_bUnderline )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold | FontStyle.Underline);
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font  = m_objFont ;
				
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFontItalic.Error", m_iClientId), p_objEx);				
			}
		}
		/// <summary>
		/// Set Font Italic
		/// </summary>
		/// <param name="p_bItalic">Bool flag</param>
		internal void SetFontItalic( bool p_bItalic )
		{		
			try
			{
				if( p_bItalic )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold | FontStyle.Italic);
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font  = m_objFont ;
				
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFontItalic.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Get text width.
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Width</returns>
		internal double GetTextWidth( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundWidth );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextWidth.Error", m_iClientId), p_objEx);				
			}
		}
		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		internal void GetPDFStream( string p_sPath,ref MemoryStream p_objMemoryStream )
		{
			FileStream objFileStream=null;
			BinaryReader objBReader=null;
			Byte[] arrRet=null;
            try
            {
                this.Save(p_sPath);
                objFileStream = new FileStream(p_sPath, FileMode.Open);
                objBReader = new BinaryReader(objFileStream);
                arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                p_objMemoryStream = new MemoryStream(arrRet);
                objFileStream.Close();

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if(objFileStream !=null)
                  objFileStream.Dispose();
              if (objBReader != null)
                  objBReader.Close();
            }
		}
		/// <summary>
		/// Get Text Height
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Height</returns>
		internal double GetTextHeight( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundHeight );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextHeight.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		internal void Save( string p_sPath )
		{
			try
			{
				m_objPrintDoc.ExportToPDF( p_sPath , false );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		internal void GetPDFStream( MemoryStream p_objMemoryStream )
		{
			try
			{
				m_objPrintDoc.Save( p_objMemoryStream);
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Call the end event.
		/// </summary>
		internal void EndDoc()
		{
			try
			{
				m_objPrintDoc.EndDoc();
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.EndDoc.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Print Line
		/// </summary>
		/// <param name="p_dblFromX">From X</param>
		/// <param name="p_dblFromY">From Y</param>
		/// <param name="p_dblToX">To X</param>
		/// <param name="p_dblToY">To Y</param>
		internal void PrintLine( double p_dblFromX , double p_dblFromY , double p_dblToX , double p_dblToY )
		{
			try
			{
				m_objPrintDoc.RenderDirectLine( p_dblFromX , p_dblFromY , p_dblToX , p_dblToY );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintLine.Error", m_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Start a new page.
		/// </summary>
		internal void NewPage()
		{
			try
			{
				m_objPrintDoc.NewPage();
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PrintWrapper.NewPage.Error", m_iClientId), p_objEx);				
			}
		}
	}
}
