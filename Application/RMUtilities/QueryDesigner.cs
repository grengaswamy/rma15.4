using System;
using System.Xml;
using Riskmaster.Db; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 
using System.Data;
using System.Collections.Generic;
using Riskmaster.Cache;
using System.Text.RegularExpressions;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   19th,Apr 2005
	///Purpose :   Implements the Query Designer Form
	/// </summary>
	public class QueryDesigner
	{
		/// <summary>
		/// Private variable for Connection String
		/// </summary>
		string m_sConStr="";

        string m_sViewConnString = string.Empty;

        /// <summary>
        /// Client ID for multi-tenant environment, cloud
        /// </summary>
        private int m_iClientId = 0;

		/// Name		: MedicalInfo
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
        /// //dvatsa
		public QueryDesigner(string p_sConnStr,int p_iClientId)
		{
			m_sConStr=p_sConnStr;
            //dvatsa
            m_iClientId = p_iClientId;
			m_sViewConnString = ConfigurationInfo.GetViewConnectionString(m_iClientId);
		}
        //Mits 15728:Asif start

        /// <summary>
        /// Finds the case sensitiveness for Oracle database
        /// </summary>
        /// <returns>boolean value indicating case sensitiveness</returns>
        private bool IsCaseInSensitveSearch()
        {
            int iResult;
            string sSQL = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT * FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConStr, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iResult = objReader.GetInt("ORACLE_CASE_INS");
                        objReader.Close();
                        if (iResult == 1 || iResult == -1)
                            return true;
                    }
                }
            }
            //Riskmaster.Db is throwing following exception so need to catch the same
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("QueryDesigner.Get.GetErr",m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }
            }
            return false;
        }
        //Mits 15728:Asif End
		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
//		The xml output structure is as follows
//		<SearchCategory CatName="Claim Search" id="1">
//			<Search id= "1">Standard Claim Search</Search>
//			<Search id= "2">New Claim Search</Search>
//		</SearchCategory>
		/// <summary>
		/// Gets the various query views
		/// </summary>
		/// <param name="p_objXmlDoc"></param>
		/// <returns></returns>
		public XmlDocument Get(XmlDocument p_objXmlDoc)
		{
			string sSQL="";
			DbReader objRdr=null;
			XmlElement objNewElm=null;
			XmlElement objCatNod=null;
			XmlElement objRoot=null;
            //Shruti for 8254
            XmlElement objSearchNames = null;
            int iPrevCatId = 0;
            int iCurCatId = 0;
            DbConnection objDbconn = null;//Mits 15728:Asif 
            bool bCaseInSens = false;//Mits 15728:Asif 
            //Added:Yukti, DT:10/01/2014, JIRA 3450
            string sLangCode = string.Empty;
            const string SearchKeyword = "srh";
            string sPageId = string.Empty;
           // Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            Dictionary<string, string> strDictResourceValues = new Dictionary<string, string>();
            string sResourceVal = string.Empty;
            int iLangCode = 0;
            XmlElement objElm = null;
			try
			{
                //Added:Yukti, DT:09/25/2014, JIRA 2170
                sLangCode = ((XmlElement)p_objXmlDoc.SelectSingleNode("//LangId")).InnerText;
                iLangCode = Convert.ToInt32(sLangCode);
                objElm = ((XmlElement)p_objXmlDoc.SelectSingleNode("//PageId"));
                if (objElm != null)
                    sPageId = objElm.InnerText;

               
                //if (!strDictParams.ContainsKey("PageId"))
                //    strDictParams.Add("PageId", sPageId);
                sSQL = string.Format("SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID='{0}' AND LANGUAGE_ID={1}",sPageId, Convert.ToInt32(iLangCode));
                objRdr = DbFactory.ExecuteReader(m_sViewConnString, sSQL.ToString());
                //sSQL = "SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID= '" + sPageId + "' AND LANGUAGE_ID=" + iLangCode;
                //objRdr = DbFactory.ExecuteReader(m_sViewConnString, sSQL.ToString());
                while (objRdr.Read())
                {
                    strDictResourceValues.Add(Conversion.ConvertObjToStr(objRdr.GetValue(0)), Conversion.ConvertObjToStr(objRdr.GetValue(1)));
                }
                objRdr.Close();
                //Ended:Yukti, DT:09/25/2014, JIRA 2170
                objSearchNames = p_objXmlDoc.CreateElement("SearchNames");
				objRoot = (XmlElement)p_objXmlDoc.SelectSingleNode("//SearchView");
				//Get the Search Categories
				//sSQL="SELECT CAT_ID,CAT_NAME FROM SEARCH_CAT ORDER BY CAT_ID";
                //MITS 15720
                sSQL = "SELECT DISTINCT SEARCH_CAT.CAT_ID,SEARCH_CAT.CAT_NAME FROM SEARCH_CAT ,SEARCH_VIEW WHERE SEARCH_CAT.CAT_ID = SEARCH_VIEW.CAT_ID ORDER BY SEARCH_CAT.CAT_ID";
                objRdr=DbFactory.GetDbReader(m_sConStr, sSQL);
				while(objRdr.Read())
				{
					objNewElm=p_objXmlDoc.CreateElement("SearchCategory");
					objNewElm.SetAttribute("CatId",objRdr.GetInt32("CAT_ID").ToString());
                    strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(objRdr.GetString("CAT_NAME").ToString(), "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                    objNewElm.SetAttribute("CatName", sResourceVal != null ? sResourceVal : objRdr.GetString("CAT_NAME").ToString());
					//objNewElm.SetAttribute("CatName",objRdr.GetString("CAT_NAME").ToString());
					objRoot.AppendChild(objNewElm);
				}
				objRdr.Close();
 
				//Get the Search Views
                //Mits 15728:Asif Start
				//sSQL="SELECT CAT_ID,VIEW_ID,VIEW_NAME FROM SEARCH_VIEW ORDER BY CAT_ID";
                objDbconn=DbFactory.GetDbConnection(m_sConStr);
                objDbconn.Open();                                            
                if (objDbconn.DatabaseType.ToString() == Constants.DB_ORACLE)
                {
                    bCaseInSens = IsCaseInSensitveSearch();
                    objDbconn.Close();
                    objDbconn.Dispose();
                    if (!bCaseInSens)
                    {
                        sSQL = "SELECT CAT_ID,VIEW_ID,VIEW_NAME FROM SEARCH_VIEW ORDER BY VIEW_NAME";
                    }
                    else
                    {
                        sSQL = "SELECT CAT_ID,VIEW_ID,VIEW_NAME FROM SEARCH_VIEW ORDER BY UPPER(VIEW_NAME)";
                    }
                }
                else
                {
                    sSQL = "SELECT CAT_ID,VIEW_ID,VIEW_NAME FROM SEARCH_VIEW ORDER BY VIEW_NAME";
                }
                //Mits 15728:Asif End

                //akaur9 R8 Mobile Adjuster used the app setting to decide whether to display the searches made just for mobile on UI or not --Start
                //the View IDs of the searches of mobile are negative and start from -100 and continue to be less for further searches
                bool bMobileSearchOnUI = false;
                bool.TryParse(RMConfigurationManager.GetAppSetting("DisplayMobileSearchOnUI", m_sConStr, m_iClientId).ToString(), out bMobileSearchOnUI);
                //rsharma220 Mobility Windows changes start
                bool bMobilityWindowsSearchOnUI = false;
                bool.TryParse(RMConfigurationManager.GetAppSetting("DisplayMobileWindowsSearchOnUI", m_sConStr, m_iClientId).ToString(), out bMobilityWindowsSearchOnUI);

                if (!bMobileSearchOnUI && !bMobilityWindowsSearchOnUI)
                {
                    int iIndexOfOrder = sSQL.IndexOf("ORDER BY");
                    sSQL = sSQL.Insert(iIndexOfOrder, "WHERE VIEW_ID > -100 ");
                }
                else if ((!bMobilityWindowsSearchOnUI && bMobileSearchOnUI) || (bMobilityWindowsSearchOnUI && bMobileSearchOnUI))
                {
                    int iIndexOfOrder = sSQL.IndexOf("ORDER BY");
                    sSQL = sSQL.Insert(iIndexOfOrder, "WHERE VIEW_ID > -1000 ");
                }
                else if (!bMobileSearchOnUI && bMobilityWindowsSearchOnUI)
                {
                    int iIndexOfOrder = sSQL.IndexOf("ORDER BY");
                    sSQL = sSQL.Insert(iIndexOfOrder, "WHERE (VIEW_ID <= -1000 OR VIEW_ID > -100)  ");
                }
                //rsharma220 Mobility Windows changes End
                //akaur9 R8 Mobile Adjuster used the app setting to decide whether to display the searches made just for mobile on UI or not --End
				
                objRdr=DbFactory.GetDbReader(m_sConStr, sSQL);
              
				while(objRdr.Read()) 
				{
					objCatNod = (XmlElement)p_objXmlDoc.SelectSingleNode("//SearchCategory[@CatId='" + objRdr.GetInt32("CAT_ID") +"']");
					if(objCatNod!=null)
					{
						objNewElm = p_objXmlDoc.CreateElement("Search");
						objNewElm.SetAttribute("SearchId",objRdr.GetInt32("VIEW_ID").ToString());
						objNewElm.SetAttribute("SearchName",objRdr.GetString("VIEW_NAME"));
						objCatNod.AppendChild(objNewElm);
                        //Shruti for 8254
                        iCurCatId = objRdr.GetInt32("CAT_ID");
                        if (iCurCatId != iPrevCatId)
                        {
                            if (objSearchNames.InnerText == string.Empty)
                            {
                                // bpaskova JIRA 4350 changed separator from "^" to "*~~(^)~~*"
                                objSearchNames.InnerText = objRdr.GetInt32("CAT_ID").ToString() + "*~~(^)~~*" +
                                                           objNewElm.Attributes["SearchName"].Value;
                            }
                            else
                            {
                                // bpaskova JIRA 4350 changed separators from "^" to "*~~(^)~~*" and ";" to "*~~(;)~~*"
                                objSearchNames.InnerText = objSearchNames.InnerText + "*~~(;)~~*" + objRdr.GetInt32("CAT_ID").ToString() + "*~~(^)~~*" +
                                                           objNewElm.Attributes["SearchName"].Value;
                            }
                        }
                        else
                        {
                            objSearchNames.InnerText = objSearchNames.InnerText + "*~~(!)~~*" + objNewElm.Attributes["SearchName"].Value;
                        }
                        iPrevCatId = iCurCatId;
					}
				}
                p_objXmlDoc.DocumentElement.AppendChild(objSearchNames);
                //Shruti for 8254 ends
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
                //dvatsa(cloud)
				throw new RMAppException(Globalization.GetString("QueryDesigner.Get.GetErr",m_iClientId),p_objEx);
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}				
				objNewElm=null;
				objCatNod=null;
                objRoot = null;
                if (objElm != null)
                {
                    objElm = null;
                }
                if (strDictResourceValues != null)
                {
                    strDictResourceValues = null;
                }
			}
		}

		/// Name		: Delete
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deleting a search view
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input Structure</param>
		/// <param name="p_iViewId">View Id to be deleted</param>
		/// <returns>Xml Output structure</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDoc)
		{
			DbConnection objCon=null;
			DbCommand objCmd=null;
			DbTransaction objTran=null;

			XmlElement objElm = null;
			string sTemp = "";
			string sCodeIds = "";
			string [] arrCodes = {""};
			try
			{	
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
				sCodeIds = objElm.InnerText;
				if (sCodeIds.Trim() != "")
				{
					objCon=DbFactory.GetDbConnection(m_sConStr);
					objCon.Open(); 
					objCmd=objCon.CreateCommand();
					objTran=objCon.BeginTransaction();
					objCmd.Transaction = objTran;
					objCmd.CommandType= CommandType.Text;  

					arrCodes = new string[1];
					if(sCodeIds.IndexOf(" ") > 0)
						arrCodes = sCodeIds.Split(' ');
					else
						arrCodes[0] = sCodeIds;

					for(int i = 0; i < arrCodes.Length; i++)  
					{
						objCmd.CommandText  = "DELETE FROM SEARCH_VIEW WHERE VIEW_ID = " + Conversion.ConvertStrToInteger(arrCodes[i]);
						objCmd.ExecuteNonQuery();
						objCmd.CommandText  = "DELETE FROM SEARCH_VIEW_DEF WHERE VIEW_ID = " + Conversion.ConvertStrToInteger(arrCodes[i]);
						objCmd.ExecuteNonQuery();
						objCmd.CommandText  = "DELETE FROM SEARCH_VIEW_PERM WHERE VIEW_ID = " + Conversion.ConvertStrToInteger(arrCodes[i]);
						objCmd.ExecuteNonQuery();
					}
					objTran.Commit(); 
				}				
				return this.Get( p_objXmlDoc);
			}
			catch(Exception p_objEx)
			{
				if(objTran!=null) 
					objTran.Rollback();
                //dvatsa
				throw new RMAppException(Globalization.GetString("QueryDesigner.Get.GetErr",m_iClientId),p_objEx);
			}
			finally
			{
				objCmd =null;
                if(objTran != null)
				    objTran.Dispose();
				if(objCon!=null)
					objCon.Dispose();
                objElm = null;
            }
		}
	}
}