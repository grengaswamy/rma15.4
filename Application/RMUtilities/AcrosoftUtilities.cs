using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;

using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Application.FileStorage;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;

using C1.C1PrintDocument;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Raman Bhatia
	///Dated   :   14 Aug 2006
	///Purpose :   Acrosoft Interface Utilities
	/// </summary>
	public class AcrosoftUtil
    {
        #region private class variables
        UserLogin m_userLogin = null;
        string m_sConnectionString = String.Empty;
		DataModelFactory m_objDataModelFactory = null;
		FileStorageManager objFileStorageManager = null;
		StorageType m_enmDocumentStorageType = 0;
		string m_sDestinationStoragePath = String.Empty;

        // Ash - cloud, config settings
        private int m_iClientId = 0;
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

        #endregion

        /// Name		: acrosoftutil
		/// Author		: Raman Bhatia
		/// Date Created: 08/14/2006	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		public AcrosoftUtil(UserLogin p_userLogin, int p_iClientId)
		{
			m_userLogin = p_userLogin;
			m_sConnectionString = p_userLogin.objRiskmasterDatabase.ConnectionString;
			m_enmDocumentStorageType = StorageType.FileSystemStorage;
			m_sDestinationStoragePath = string.Empty;
            m_iClientId = p_iClientId; //Ash - cloud, config settings
            m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId); //Ash - cloud, config settings
            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();
		} // constructor

		/// Name		: PreFillFolders
		/// Author		: Raman Bhatia
		/// Date Created: 08/14/2006		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Moves All the existing document attachments into the AcrosoftFolders
		/// This utility assumes that the folder structure has been generated already
		/// </summary>
		public MemoryStream PreFillFolders()
        {
            #region local variables
            DbReader objDbReader=null;
			Claim objClaim = null;
			Event objEvent = null;
			PiEmployee objPI = null;
			PiPatient objPatient = null;
			PiWitness objWitness = null;
			EventXDatedText objDatedText = null;
			Claimant objClaimant = null;
			Defendant objDefendant = null;
			Policy objPolicy = null;
			MemoryStream objFileContent = null;
			string sSQL = string.Empty; 
			string sTableName = string.Empty;
			string sEventNumber = string.Empty;
			string sClaimNumber = string.Empty;
			string sPolicyName = string.Empty;
			string sEventId = string.Empty;
			string sClaimId = string.Empty;
			string sAcrosoftAttachmentsTypeKey = string.Empty;
			string sAcrosoftGeneralStorageTypeKey = string.Empty;
			string sAcrosoftPolicyTypeKey = string.Empty;
			string sAcrosoftUsersTypeKey = string.Empty;
			string sFileName = string.Empty;
			int iRetCd = 0;
			byte[] bFileContent = null;
			string sDocTitle = string.Empty;
			string sCreateTs = string.Empty;
			string sModifiedCreateTs = string.Empty;
			Acrosoft objAcrosoft = null;
			string sAppExcpXml = string.Empty;
			string sSessionId = string.Empty;
			string sReturnXmlstring = string.Empty;
			XmlDocument objXmlDoc = null;
			XmlElement objTempElement = null;
			XmlElement objParentElement = null;
			string sAttachedRecord = String.Empty;
			string sPIEmployeeId = string.Empty;
			string sPIPatientId = string.Empty;
			string sPIWitnessId = string.Empty;
			string sEventDatedText = string.Empty;
			string sClaimant = string.Empty;
			string sDefendant = string.Empty;
			string sPolicyId = string.Empty;
			string sFormName = string.Empty;
			string sFormFileName = string.Empty;
			string sAuthor = string.Empty;
			MemoryStream objMemoryStream = null;
			string sEventFolderFriendlyName = "";
			string sClaimFolderFriendlyName = "";
			string sPolicyFolderFriendlyName = "";
			string sUsersFolderFriendlyName = "";
			string sGeneralFolderFriendlyName = "";
            #endregion

            try
			{
				sReturnXmlstring = "<Documents><Attachments><event></event><claim></claim><piemployee></piemployee><pipatient></pipatient><piwitness></piwitness>"
					+"<eventdatedtext></eventdatedtext><claimant></claimant><defendant></defendant><policy></policy></Attachments><Templates></Templates><TemplateHeaders></TemplateHeaders><orphandocuments></orphandocuments></Documents>";
				objXmlDoc = new XmlDocument();
				objXmlDoc.LoadXml(sReturnXmlstring);
                sAcrosoftAttachmentsTypeKey = AcrosoftSection.AcrosoftAttachmentsTypeKey;
                sAcrosoftGeneralStorageTypeKey = AcrosoftSection.AcrosoftGeneralStorageTypeKey;
                sAcrosoftPolicyTypeKey = AcrosoftSection.AcrosoftPolicyTypeKey;
                sAcrosoftUsersTypeKey = AcrosoftSection.AcrosoftUsersTypeKey;
                sEventFolderFriendlyName = AcrosoftSection.EventFolderFriendlyName;
                sClaimFolderFriendlyName = AcrosoftSection.ClaimFolderFriendlyName;
                sPolicyFolderFriendlyName = AcrosoftSection.PolicyFolderFriendlyName;
                sUsersFolderFriendlyName = AcrosoftSection.UsersFolderFriendlyName;
                sGeneralFolderFriendlyName = AcrosoftSection.GeneralFolderFriendlyName;
				m_enmDocumentStorageType = (StorageType) m_userLogin.objRiskmasterDatabase.DocPathType;
				m_sDestinationStoragePath = m_userLogin.objRiskmasterDatabase.GlobalDocPath;
                objFileStorageManager = new FileStorageManager(this.m_enmDocumentStorageType, this.m_sDestinationStoragePath, m_iClientId);
				objFileContent = new MemoryStream();
                m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);

				//Loop through all attachments and store them in acrosoft

				sSQL = "SELECT DOCUMENT.DOCUMENT_ID,DOCUMENT.DOCUMENT_NAME," 
					+ "DOCUMENT_ATTACH.TABLE_NAME,DOCUMENT_ATTACH.RECORD_ID,"
					+ "DOCUMENT.DOCUMENT_FILENAME,DOCUMENT.CREATE_DATE, DOCUMENT.FOLDER_ID,"
					+ "DOCUMENT.USER_ID, DOCUMENT.DOCUMENT_TYPE FROM DOCUMENT LEFT OUTER JOIN DOCUMENT_ATTACH" 
					+ " ON DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID"
					+ " ORDER BY TABLE_NAME";

				objDbReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
				
				objAcrosoft = new Acrosoft(m_iClientId);	//dvatsa-cloud
				objAcrosoft.Authenticate(m_userLogin.LoginName , m_userLogin.Password , out sSessionId , out sAppExcpXml);
				
				while(objDbReader.Read())
				{
					//Looping through all attachments
					//In case its unable to find an attachment or crashes somewhere it makes sense to continue
					//Store Details in an xml though
				
					try
					{
						sFileName = objDbReader.GetValue("DOCUMENT_FILENAME").ToString();
						sTableName = objDbReader.GetValue("TABLE_NAME").ToString();
						sDocTitle = objDbReader.GetValue("DOCUMENT_NAME").ToString();
						sCreateTs = objDbReader.GetValue("CREATE_DATE").ToString();
						sAuthor = objDbReader.GetValue("USER_ID").ToString();
						sAttachedRecord = "";
						if(sCreateTs!="")
						{
							sModifiedCreateTs = sCreateTs.Substring(0 , 4) + "/" + sCreateTs.Substring(4 , 2) + "/"
								+ sCreateTs.Substring(6 , 2) + " " + sCreateTs.Substring(8 , 2) + ":"
								+ sCreateTs.Substring(10 , 2);
						} // if

						switch(sTableName.ToLower())
						{
							case "event":    sEventId = objDbReader.GetValue("RECORD_ID").ToString();
								objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event" , false);
								objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));	
								sEventNumber = objEvent.EventNumber;
								sAttachedRecord = sEventNumber;	  
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , "" , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml);
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml); 	 
									} // if
		
								} // if	
									  
								break;
							case "claim":   sClaimId = objDbReader.GetValue("RECORD_ID").ToString();
								objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim" , false);
								objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));	
								sEventNumber = objClaim.EventNumber;
								sClaimNumber = objClaim.ClaimNumber;
								sAttachedRecord = sClaimNumber;	
										  
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , sClaimNumber , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
										
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml); 	
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml); 	
									} // if
	
								} // if
	  
								break;

							case "piemployee":    sPIEmployeeId = objDbReader.GetValue("RECORD_ID").ToString();
								objPI = (PiEmployee)m_objDataModelFactory.GetDataModelObject("PiEmployee" , false);
								objPI.MoveTo(Conversion.ConvertStrToInteger(sPIEmployeeId));	
								sEventId = objPI.EventId.ToString();
								objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event" , false);
								objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));	
								sEventNumber = objEvent.EventNumber; 
								sAttachedRecord = sEventNumber;	
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , "" , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml);
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml);
									} // if
		
								} // if
											
									  
								break;

							case "pipatient":    sPIPatientId = objDbReader.GetValue("RECORD_ID").ToString();
								objPatient = (PiPatient)m_objDataModelFactory.GetDataModelObject("PiPatient" , false);
								objPatient.MoveTo(Conversion.ConvertStrToInteger(sPIPatientId));	
								sEventId = objPatient.EventId.ToString();
								objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event" , false);
								objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));	
								sEventNumber = objEvent.EventNumber; 
								sAttachedRecord = sEventNumber;	
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , "" , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml);
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml);
									} // if
		
								} // if
											
									  
								break;

							case "piwitness":    sPIWitnessId = objDbReader.GetValue("RECORD_ID").ToString();
								objWitness = (PiWitness)m_objDataModelFactory.GetDataModelObject("PiWitness" , false);
								objWitness.MoveTo(Conversion.ConvertStrToInteger(sPIWitnessId));	
								sEventId = objWitness.EventId.ToString();
								objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event" , false);
								objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));	
								sEventNumber = objEvent.EventNumber; 
								sAttachedRecord = sEventNumber;	
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , "" , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml);
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml);
									} // if
		
								} // if
											
									  
								break;

							case "eventdatedtext":    sEventDatedText = objDbReader.GetValue("RECORD_ID").ToString();
								objDatedText = (EventXDatedText)m_objDataModelFactory.GetDataModelObject("EventXDatedText" , false);
								objDatedText.MoveTo(Conversion.ConvertStrToInteger(sEventDatedText));	
								sEventId = objDatedText.EventId.ToString();
								objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event" , false);
								objEvent.MoveTo(Conversion.ConvertStrToInteger(sEventId));	
								sEventNumber = objEvent.EventNumber; 
								sAttachedRecord = sEventNumber;	
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , "" , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml);
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sEventNumber , "" , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml);
									} // if
		
								} // if
											
									  
								break;

							case "claimant":    sClaimant = objDbReader.GetValue("RECORD_ID").ToString();
								objClaimant = (Claimant)m_objDataModelFactory.GetDataModelObject("Claimant" , false);
								objClaimant.MoveTo(Conversion.ConvertStrToInteger(sClaimant));	
								sClaimId = objClaimant.ClaimId.ToString();
								objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim" , false);
								objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));	
								sEventNumber = objClaim.EventNumber; 
								sClaimNumber = objClaim.ClaimNumber;
								sAttachedRecord = sClaimNumber;	
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , sClaimNumber , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml); 	
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml); 	
									} // if
		
								} // if
											
									  
								break;

							case "defendant":    sDefendant = objDbReader.GetValue("RECORD_ID").ToString();
								objDefendant = (Defendant)m_objDataModelFactory.GetDataModelObject("Defendant" , false);
								objDefendant.MoveTo(Conversion.ConvertStrToInteger(sDefendant));	
								sClaimId = objDefendant.ClaimId.ToString();
								objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim" , false);
								objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));	
								sEventNumber = objClaim.EventNumber; 
								sClaimNumber = objClaim.ClaimNumber;
								sAttachedRecord = sClaimNumber;	
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateAttachmentFolder(sSessionId , sAcrosoftAttachmentsTypeKey , sEventNumber , sClaimNumber , sEventFolderFriendlyName , sClaimFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml); 	
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml); 	
									} // if
		
								} // if
											
									  
								break;

							case "policy":    sPolicyId = objDbReader.GetValue("RECORD_ID").ToString();
								objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy" , false);
								objPolicy.MoveTo(Conversion.ConvertStrToInteger(sPolicyId));	
								sPolicyName = objPolicy.PolicyName;
								sAttachedRecord = sPolicyName;
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreatePolicyFolder(sSessionId , sAcrosoftPolicyTypeKey , sPolicyName, sPolicyFolderFriendlyName, out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId, sFileName , bFileContent ,sDocTitle , "" , sPolicyName , sAcrosoftPolicyTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml);
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftAttachmentsTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml); 	 
									} // if
		
								} // if
											
									  
								break;

							default:
								//Raman Bhatia... What to do of remaining documents??
								//if a document is not attached to any of the above then consider
								//moving it to the user documents for that user.. i mean its better than losing it..
								
								sTableName = "OrphanDocuments";
								iRetCd = objFileStorageManager.RetrieveFile(sFileName , out objFileContent);
											   	
								if(iRetCd==1)
								{
									bFileContent = objFileContent.ToArray();
									objAcrosoft.CreateUserFolder(sSessionId , sAcrosoftUsersTypeKey , sAuthor , sUsersFolderFriendlyName , out sAppExcpXml); 
									//Posting document to Acrosoft
									iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent , sDocTitle , "" , sAuthor , sAcrosoftUsersTypeKey , "" , sModifiedCreateTs , sAuthor , out sAppExcpXml);
									if(iRetCd!=0)
									{
										iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFileName , bFileContent ,sDocTitle , "" , sEventNumber , sClaimNumber , sAcrosoftUsersTypeKey , "" , sModifiedCreateTs , "" , out sAppExcpXml); 	 
									} // if
		
								} // if

								break;
						} // switch
						objTempElement = objXmlDoc.CreateElement("DocumentName");
						objTempElement.InnerText = sFileName;
						if(iRetCd==0)
							objTempElement.SetAttribute("Exported" , "True");
						else
							objTempElement.SetAttribute("Exported" , "False");
						
					} // try
					catch
					{
						objTempElement = objXmlDoc.CreateElement("DocumentName");
						objTempElement.InnerText = sFileName;
						objTempElement.SetAttribute("Exported" , "False");
							
					} // catch
					finally
					{
						objTempElement.SetAttribute("AttachedRecord" , sAttachedRecord);
						objParentElement = (XmlElement) objXmlDoc.SelectSingleNode("//" + sTableName.ToLower());
						objParentElement.AppendChild(objTempElement);	
					} // finally


				} // while
					
				objAcrosoft.CreateGeneralFolder(sSessionId , sAcrosoftGeneralStorageTypeKey , "Templates" , sGeneralFolderFriendlyName , out sAppExcpXml);
				
				//Loop through all mail-merge templates and store them in acrosoft
				
				sSQL = "SELECT DISTINCT MERGE_FORM.FORM_ID,FORM_NAME,FORM_FILE_NAME FROM MERGE_FORM";

				objDbReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
				
				//Looping through all templates
				//In case its unable to find a template or crashes somewhere it makes sense to continue
				//Store Details in an xml though
				
				while(objDbReader.Read())
				{
					try
					{
						sFormName = objDbReader.GetValue("FORM_NAME").ToString();
						sFormFileName = objDbReader.GetValue("FORM_FILE_NAME").ToString();

						iRetCd = objFileStorageManager.RetrieveFile(sFormFileName , out objFileContent);
											   	
						if(iRetCd==1)
						{
							bFileContent = objFileContent.ToArray();
							//Posting document to Acrosoft
							iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFormFileName , bFileContent , sFormName , "" , "Templates" , sAcrosoftGeneralStorageTypeKey , "" , "" , sAuthor , out sAppExcpXml);
 							if(iRetCd!=0)
 							{
								iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFormFileName , bFileContent , sFormName , "" , "Templates" , sAcrosoftGeneralStorageTypeKey , "" , "" , "" , out sAppExcpXml); 
 							} // if
			
						} // if
						objTempElement = objXmlDoc.CreateElement("TemplateName");
						objTempElement.InnerText = sFormFileName;
						if(iRetCd==0)
							objTempElement.SetAttribute("Exported" , "True");
						else
							objTempElement.SetAttribute("Exported" , "False");
					}
					catch
					{
						objTempElement = objXmlDoc.CreateElement("TemplateName");
						objTempElement.InnerText = sFormFileName;
						objTempElement.SetAttribute("Exported" , "False");
					} // catch
					finally
					{
						objParentElement = (XmlElement) objXmlDoc.SelectSingleNode("//Templates");
						objParentElement.AppendChild(objTempElement);	
					} // finally

					//Posting hdr files
					try
					{
						sFormName = objDbReader.GetValue("FORM_NAME").ToString();
						sFormFileName = objDbReader.GetValue("FORM_FILE_NAME").ToString();
							
						sFormFileName = sFormFileName.Replace(sFormFileName.Substring(sFormFileName.Length-4,4),".HDR");

						iRetCd = objFileStorageManager.RetrieveFile(sFormFileName , out objFileContent);
											   	
						if(iRetCd==1)
						{
							bFileContent = objFileContent.ToArray();
							//Posting document to Acrosoft
							iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFormFileName , bFileContent , sFormName , "" , "Templates" , sAcrosoftGeneralStorageTypeKey , "" , "" , sAuthor , out sAppExcpXml);
							if(iRetCd!=0)
							{
								iRetCd = objAcrosoft.StoreObjectBuffer(sSessionId , sFormFileName , bFileContent , sFormName , "" , "Templates" , sAcrosoftGeneralStorageTypeKey , "" , "" , "" , out sAppExcpXml);
							} // if
 										
						} // if
						objTempElement = objXmlDoc.CreateElement("HeaderName");
						objTempElement.InnerText = sFormFileName;
						if(iRetCd==0)
							objTempElement.SetAttribute("Exported" , "True");
						else
							objTempElement.SetAttribute("Exported" , "False");
					}
					catch
					{
						objTempElement = objXmlDoc.CreateElement("HeaderName");
						objTempElement.InnerText = sFormFileName;
						objTempElement.SetAttribute("Exported" , "False");
					} // catch
					finally
					{
						objParentElement = (XmlElement) objXmlDoc.SelectSingleNode("//TemplateHeaders");
						objParentElement.AppendChild(objTempElement);	
					} // finally
				} // while
				objMemoryStream = new MemoryStream();
				objMemoryStream = PrintUploadReport(objXmlDoc);
				return objMemoryStream;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Acrosoft.PreFillFolders.Error", m_iClientId), p_objEx); 
			}
			finally
			{
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objEvent != null)
                     objEvent.Dispose();
                if (objClaim != null)
                     objClaim.Dispose();
                if (objPI != null)
                    objPI.Dispose();
                if (objPolicy != null)
                     objPolicy.Dispose();
                if (objClaimant != null)
                    objClaimant.Dispose();
                if (objDefendant != null)
                    objDefendant.Dispose();
                if (objDatedText != null)
                    objDatedText.Dispose();
                if (objWitness != null)
                    objWitness.Dispose();
                if (objPatient != null)
                    objPatient.Dispose();
                if(objFileContent !=null)
                    objFileContent.Dispose();
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
		public MemoryStream PrintUploadReport(XmlDocument p_objXmlDocument)
        {
            #region local variables
            PrintWrapper objPrintWrapper = null;
			MemoryStream objMemoryStream = null;
			DbReader objDbReader = null;
            string sPath = String.Empty;
			LocalCache objCache=null;
			XmlNodeList objNodeList = null;
			XmlNodeList objParentNodeList = null;
			int iCount = 1;
            string sRecordType = String.Empty;
            #endregion

            try
			{
				objPrintWrapper = new PrintWrapper(m_iClientId);
				objPrintWrapper.StartDoc(false);
	
				CreateHeader(ref objPrintWrapper);
				
				objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
				objPrintWrapper.CurrentX = 4000;
				objPrintWrapper.SetFontBold(true); 
				objPrintWrapper.PrintTextNew(Globalization.GetString("AcrosoftUtil.PrintUploadReport.Header",m_iClientId),true);
				
				//Printing Attachements Data
				objParentNodeList = p_objXmlDocument.SelectSingleNode("//Attachments").ChildNodes;
				
				foreach (XmlNode objParentNode in objParentNodeList)
				{
					objNodeList = p_objXmlDocument.SelectNodes("//" + objParentNode.Name + "/DocumentName");
					sRecordType = objParentNode.Name;
					PrintRecordData(ref objPrintWrapper , objNodeList , true , ref iCount , sRecordType);
					
				} // foreach
				
				//Printing Template Data

				sRecordType = "Templates";
				objNodeList = p_objXmlDocument.SelectNodes("//Templates/TemplateName");
				PrintRecordData(ref objPrintWrapper , objNodeList , false , ref iCount , sRecordType);

				//Printing Template Headers Data

				sRecordType = "TemplatesHeaders";
				objNodeList = p_objXmlDocument.SelectNodes("//TemplateHeaders/HeaderName");
				PrintRecordData(ref objPrintWrapper , objNodeList , false , ref iCount , sRecordType);

				//Printing Orphan Documents

				sRecordType = "OrphanDocuments";
				objNodeList = p_objXmlDocument.SelectNodes("//orphandocuments/DocumentName");
				PrintRecordData(ref objPrintWrapper , objNodeList , false , ref iCount , sRecordType);
				
				CreateFooter(objPrintWrapper,150,objPrintWrapper.CurrentY,objPrintWrapper.GetTextHeight("W"),250);
				
				objPrintWrapper.EndDoc();

				objMemoryStream = new MemoryStream();
				
                sPath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "FiscalYearList");
				if (!Directory.Exists(sPath))
				{
					Directory.CreateDirectory(sPath);
				}
                sPath = String.Format(@"{0}\{1}", sPath, Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf")); ;
				objPrintWrapper.GetPDFStream(sPath,ref objMemoryStream); 
				File.Delete(sPath);
				return objMemoryStream;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Error",m_iClientId),p_objExp);   
			}
			finally
			{
				objPrintWrapper = null;
				objCache = null;
				objMemoryStream = null;
				if(objDbReader != null)
					objDbReader.Close();
				objDbReader = null;
				if (objCache!=null)
					objCache.Dispose();
               
                objNodeList = null;
                objParentNodeList = null;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <param name="p_sValue"></param>
        /// <param name="iCount"></param>
        /// <returns></returns>
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue,ref int iCount)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//control[@name='"+p_sValue+"']");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objPrintWrapper"></param>
        /// <param name="objNodeList"></param>
        /// <param name="p_bRecordAttached"></param>
        /// <param name="iCount"></param>
        /// <param name="p_sRecordType"></param>
		private void PrintRecordData(ref PrintWrapper objPrintWrapper , XmlNodeList objNodeList , bool p_bRecordAttached , ref int iCount , string p_sRecordType)
		{
			string sTruncatedFileName = "";
				
			objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
			objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
			objPrintWrapper.CurrentX = 300;
			objPrintWrapper.SetFontBold(true); 
			objPrintWrapper.PrintTextNew(iCount++.ToString() + ") " + Globalization.GetString("AcrosoftUtil.PrintUploadReport." + p_sRecordType,m_iClientId),true);
			
			objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
			objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
			objPrintWrapper.CurrentX = 300;
			objPrintWrapper.SetFontBold(true); 
			objPrintWrapper.PrintTextNew(Globalization.GetString("AcrosoftUtil.PrintUploadReport.FileName",m_iClientId),true);

			objPrintWrapper.CurrentX = 5300;
			objPrintWrapper.SetFontBold(true); 
			objPrintWrapper.PrintTextNew(Globalization.GetString("AcrosoftUtil.PrintUploadReport.Attached Record",m_iClientId),true);

			objPrintWrapper.CurrentX = 9300;
			objPrintWrapper.SetFontBold(true); 
			objPrintWrapper.PrintTextNew(Globalization.GetString("AcrosoftUtil.PrintUploadReport.Status",m_iClientId),true);
			
			objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");

			foreach (XmlNode objXmlNode in objNodeList)
			{
						
				if (objPrintWrapper.CurrentY > 14500)
				{
					objPrintWrapper.NewPage();
					CreateHeader(ref objPrintWrapper);
				}
				objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");

				if(objXmlNode.InnerText.Length > 45)
				{
					sTruncatedFileName = objXmlNode.InnerText.Substring(0,45);
				} // if
				else
				{
					sTruncatedFileName = objXmlNode.InnerText;
				} // else
						
				objPrintWrapper.CurrentX = 300;
				objPrintWrapper.SetFontBold(false); 
				objPrintWrapper.PrintTextNew(sTruncatedFileName,true);
				   
				if(p_bRecordAttached == true)
				{
					objPrintWrapper.CurrentX = 5300;
					objPrintWrapper.SetFontBold(false); 
					objPrintWrapper.PrintTextNew(objXmlNode.Attributes["AttachedRecord"].InnerText ,true);
				} // if

				objPrintWrapper.CurrentX = 9300;
				objPrintWrapper.SetFontBold(false); 
				objPrintWrapper.PrintTextNew(objXmlNode.Attributes["Exported"].InnerText  ,true);
			}
				
			if (((objPrintWrapper.CurrentY)+(objPrintWrapper.GetTextHeight("W")*8)) > 14500)
			{
				objPrintWrapper.NewPage();
				CreateHeader(ref objPrintWrapper);
			}
		} // method: PrintRecordData

		#region Report Private Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objPrintWrapper"></param>
		private void CreateHeader(ref PrintWrapper objPrintWrapper)
        {
            #region local variables
            double dBorderWidth=0;
			double dMarginWidth = 0;
			double dPageWidth = 0;
			double dPageHeight = 0;
			double dClientWidth = 0;
			double dClientHeight = 0;
			double dTotalLines = 0;
			double dTextHeight = 0;
			string sDate = "";
			string sCompanyName="";
			string sFooterText = "";
			double dBodyHeight = 0;
			SysParms objSysSetting=null;
            #endregion

            try
			{
				sDate = DateTime.Now.ToShortDateString();
   
				//PrintBorder
				dBorderWidth = 2;
				dMarginWidth = 75;
				dPageWidth = objPrintWrapper.PageWidth - 2 * (dBorderWidth + dMarginWidth);
				dPageHeight = objPrintWrapper.PageHeight - 2 * (dBorderWidth + dMarginWidth);
				dClientWidth = objPrintWrapper.PageWidth - 2 * dMarginWidth;
				dClientHeight = objPrintWrapper.PageHeight - 2 * (dMarginWidth + 50);
				//Moved from top. Referenced ClientHeight before it was set.
				dTotalLines = Math.Abs(dClientHeight / objPrintWrapper.GetTextHeight("W") - 10);
                objSysSetting = new SysParms(m_userLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
				sCompanyName=objSysSetting.SysSettings.ClientName;
				objSysSetting=null;
				dTextHeight = objPrintWrapper.GetTextHeight(sCompanyName);
  
				objPrintWrapper.SetFont("Arial",10);
				objPrintWrapper.SetFontBold(true);
				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth,dMarginWidth);

				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth,dMarginWidth, dPageHeight - dMarginWidth);
				objPrintWrapper.PrintLine(dPageWidth - dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth, dPageHeight - dMarginWidth);
				objPrintWrapper.PrintLine(dMarginWidth,dPageHeight - dMarginWidth,dPageWidth - dMarginWidth,dPageHeight - dMarginWidth);
				
				//print header
				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth + 500,dPageWidth - dMarginWidth,dMarginWidth + 500);
				objPrintWrapper.FillRect(dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth,dMarginWidth + 500);

				if(sCompanyName.Length < 36)
				{
					objPrintWrapper.CurrentX = (dPageWidth - objPrintWrapper.GetTextWidth(sCompanyName)) / 2;
					objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
					objPrintWrapper.PrintTextNew(sCompanyName,true);

					objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sDate + "W");
					objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
					objPrintWrapper.PrintTextNew(sDate,true);
				}
				else
				{
					objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sCompanyName + " " + sDate + " ");
					objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
                    objPrintWrapper.PrintTextNew(String.Format("{0} {1}", sCompanyName, sDate), true);
				}
				
				//print footer
				objPrintWrapper.PrintLine(dMarginWidth,dPageHeight - dMarginWidth - 500,dPageWidth - dMarginWidth - 500,dPageHeight - dMarginWidth);
				objPrintWrapper.FillRect(dMarginWidth,dPageHeight - dMarginWidth - 500,dPageWidth - dMarginWidth,dPageHeight - dMarginWidth);
                sFooterText = String.Format("{0} - {1}  ", Globalization.GetString("ORGSEC.PrintOrgSec.ConfidentialData",m_iClientId), m_MessageSettings["DEF_RMCAPTION"]);
				objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sFooterText) - 1;
				objPrintWrapper.CurrentY = (dPageHeight - 500) + objPrintWrapper.GetTextWidth("W");
				objPrintWrapper.SetFontItalic(true);  
				objPrintWrapper.PrintTextNew(sFooterText,true);
				objPrintWrapper.SetFontItalic(false);
                sFooterText = String.Format("{0} [@@PageNo@@]", Globalization.GetString("ORGSEC.PrintOrgSec.PageX",m_iClientId));
				objPrintWrapper.CurrentX = (dPageWidth - objPrintWrapper.GetTextWidth(sFooterText)) / 2;
				objPrintWrapper.SetFontBold(true);  
				objPrintWrapper.PrintTextNew(sFooterText,true);
				objPrintWrapper.SetFontBold(false);

				dBodyHeight = dClientHeight - 2 * 500;
				objPrintWrapper.PageLimit = dBodyHeight;
				objPrintWrapper.CurrentY=400;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("FiscalYearsList.CreateHeader.Error",m_iClientId),p_objExp);   
			}
			finally
			{
				objSysSetting=null;
			}

		}
		private void CreateFooter(PrintWrapper p_objPrintWrapper,double p_dCurrentX,double p_dCurrentY,double p_dHeight,double p_dblSpace)
		{
			p_objPrintWrapper.CurrentY+= (p_objPrintWrapper.GetTextHeight("W")*3);
			p_objPrintWrapper.CurrentX=p_dCurrentX;
			p_objPrintWrapper.SetFont("Arial",7.5f);
			p_objPrintWrapper.SetFontBold(true);
			p_objPrintWrapper.SetFontUnderline(true);
            p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_NOTE"].ToString(), true);
			
			p_objPrintWrapper.CurrentX=p_dCurrentX;
			p_objPrintWrapper.CurrentY +=p_dHeight;
			p_objPrintWrapper.SetFont("Arial",7.5f);
			p_objPrintWrapper.SetFontBold(true);
            p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_RMPROPRIETARY"].ToString(), true);
			
			p_objPrintWrapper.CurrentX=p_dCurrentX;
			p_objPrintWrapper.CurrentY +=p_dHeight;
			p_objPrintWrapper.SetFont("Arial",7.5f);
			p_objPrintWrapper.SetFontBold(true);
            p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_NAME"].ToString(), true);

			p_objPrintWrapper.CurrentX=p_dCurrentX;
			p_objPrintWrapper.CurrentY +=p_dHeight;
			p_objPrintWrapper.SetFont("Arial",7.5f);
			p_objPrintWrapper.SetFontBold(true);
            p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_RMCOPYRIGHT"].ToString(), true);

			p_objPrintWrapper.CurrentX=p_dCurrentX;
			p_objPrintWrapper.CurrentY +=p_dHeight;
			p_objPrintWrapper.SetFont("Arial",7.5f);
			p_objPrintWrapper.SetFontBold(true);
            p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString(), true);
		}
		#endregion
	}	
}