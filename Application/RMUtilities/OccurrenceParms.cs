using System;
using System.Xml;
using System.Collections; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21 Mar 2005
	///Purpose :   This class Fetches Occurrence Parms data.
	/// </summary>
	public class OccurrenceParms:UtilityUIFormBase
	{
		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnectionString = "";
        /// <summary>
        /// User Login Name
		///gagnihotri MITS 11995 Changes made for Audit table
        /// </summary>
        private string m_sUserName = "";
		/// <summary>
		/// String array for Occurrence Parms field mapping
		/// </summary>
        /// 
        private int m_iClientId = 0; //rkaur27
		private string[,] arrFields = {
			//First tab - Client Information
			{"FacilityName", "OC_FAC_NAME"},
			{"FacilityAddress", "OC_FAC_ADDR1"},
			{"Address2", "OC_FAC_ADDR2"},
			{"City", "OC_FAC_ADDR3"},
			{"StateZip", "OC_FAC_ADDR4"},
			{"HCFANumber", "OC_FAC_HCFA_NO"},
			{"ContactPerson", "OC_FAC_CONTACT"},
			{"FacilityPhone", "OC_FAC_PHONE"},
			{"EventCateg", "EVENT_CAT_CODE"},
			{"MediCateg", "EVENT_CAT_CODE"},
			{"SentinelCateg", "EVENT_IND_CODE"},
			{"EquipmentCateg", "EVENT_CAT_CODE"},
									  };

        //Aman ML Change
        /// <summary>
        /// user's Language Code
        /// </summary>
        private int m_iLangCode = 0;
		/// <summary>
		/// String array for Occurrence Parms child field mapping
		/// </summary>
		private string[,] arrChilds = {
			//{"XMLTag", "Table Name", "Key Field", "Value Field", "Where Clause"}
			{"FallEventCateg", string.Format("SELECT EVENT_CAT_CODE FROM SYS_EVENT_CAT WHERE DISPLAY_ID = {0}", (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Fall)},
			{"Medication", string.Format("SELECT EVENT_CAT_CODE FROM SYS_EVENT_CAT WHERE DISPLAY_ID = {0}", (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Medication)},
			{"Sentinel", "SELECT EVENT_IND_CODE FROM SYS_SENT_EVENT"},
			{"Equipment", string.Format("SELECT EVENT_CAT_CODE FROM SYS_EVENT_CAT WHERE DISPLAY_ID = {0}", (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment)}
									  };

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public OccurrenceParms(string p_sConnString, int p_iClientId)//Add by kuladeep for review defect.
            : base(p_sConnString, p_iClientId) //rkaur27
		{
			ConnectString = p_sConnString;
			m_sConnectionString = p_sConnString;
            m_iClientId = p_iClientId; //rkaur27
			this.Initialize();
		}
        #region Properties
        //Aman ML Change
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion
        //gagnihotri MITS 11995 Changes made for Audit table
        public OccurrenceParms(string p_sConnString, string p_sUserName, int p_iClientId)//Add by kuladeep for review defect.
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_sConnectionString = p_sConnString;
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId; //rkaur27
            this.Initialize();
        }

		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_PARMS";
			KeyField = "";
			InitFields(arrFields);
			base.ChildArray = arrChilds;			
			base.Initialize();
		}

		/// <summary>
		/// Get the data for the Discount List form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data structure</param>
		/// <returns>Xml document with data and the form structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
                base.LanguageCode = this.LanguageCode;  //Aman ML Change
				base.Get();
				return p_objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("OccurrenceParms.Get.Err", m_iClientId),p_objEx);
			}

		}

		/// <summary>
		/// Saves the from
		/// </summary>
		/// <param name="p_objXmlDocument">XML Doc containing Data</param>
		/// <returns>XML Doc</returns>
        //public bool Save(XmlDocument p_objXmlDocument)
        //{
        //    string sSQL = "";
        //    DbReader objDbReader = null;
        //    DbConnection objConn = null;
        //    XmlElement objGroupXmlElement = null;
        //    XmlElement objControlXMLElement = null;
        //    int iCodeId = 0;
        //    string sCodeIds = "";
        //    string [] arrCodes = {""};

        //    try
        //    {
        //        objGroupXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//group[@name='OccParmsEventConfig']");  
			
        //        //saving enhancement made for sentinel event and fall /medwatch indicators
        //        //first thing is to delete all records
        //        sSQL = "DELETE FROM SYS_EVENT_CAT";
        //        objConn = DbFactory.GetDbConnection(m_sConnectionString);
        //        objConn.Open();
        //        objConn.ExecuteNonQuery(sSQL);
 	 			  
        //        //then save  the edit is an option in case they select the same item for both.  
        //        //If this happens, it will be saved as medwatch, first save fall information
        //        objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='FallEventCategList']");
				
        //        if(objControlXMLElement != null)
        //        {
        //            sCodeIds = objControlXMLElement.InnerText;
        //            if(sCodeIds.Trim() != "")
        //            {
        //                arrCodes = new string[1];
        //                if(sCodeIds.IndexOf(",") > 0)
        //                    arrCodes = sCodeIds.Split(',');
        //                else
        //                    arrCodes[0] = sCodeIds;
  
        //                for(int i = 0; i < arrCodes.Length; i++)  
        //                {
        //                    iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());  
        //                    sSQL = "SELECT * FROM SYS_EVENT_CAT WHERE EVENT_CAT_CODE = " + iCodeId;
        //                    objDbReader = objConn.ExecuteReader(sSQL);
        //                    if(objDbReader != null)
        //                    {
        //                        if(objDbReader.Read())
        //                        {
        //                            objDbReader.Close();
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "UPDATE SYS_EVENT_CAT SET DISPLAY_ID = 1, UPDATED_BY_USER = '" + m_sUserName + "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) + "WHERE EVENT_CAT_CODE = " + iCodeId;
        //                            objConn.ExecuteNonQuery(sSQL);	
        //                        }
        //                        else
        //                        {
        //                            objDbReader.Close();
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(" + iCodeId + ",1,'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
        //                            objConn.ExecuteNonQuery(sSQL);	
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        //now do the medwatch
        //        objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='MedicationList']");	
        //        if(objControlXMLElement != null)
        //        {
        //            sCodeIds = objControlXMLElement.InnerText;
        //            if(sCodeIds.Trim() != "")
        //            {
        //                arrCodes = new string[1]; 
        //                if(sCodeIds.IndexOf(",") > 0)
        //                    arrCodes = sCodeIds.Split(',');
        //                else
        //                    arrCodes[0] = sCodeIds;
  
        //                for(int i = 0; i < arrCodes.Length; i++)  
        //                {
        //                    iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());  
        //                    sSQL = "SELECT * FROM SYS_EVENT_CAT WHERE EVENT_CAT_CODE = " + iCodeId;
        //                    objDbReader = objConn.ExecuteReader(sSQL);;
        //                    if(objDbReader != null)
        //                    {
        //                        if(objDbReader.Read())
        //                        {
        //                            objDbReader.Close();
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "UPDATE SYS_EVENT_CAT SET DISPLAY_ID = 2, UPDATED_BY_USER = '" + m_sUserName + "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) + "WHERE EVENT_CAT_CODE = " + iCodeId;
        //                            objConn.ExecuteNonQuery(sSQL);	
        //                        }
        //                        else
        //                        {
        //                            objDbReader.Close();
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(" + iCodeId + ",2,'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
        //                            objConn.ExecuteNonQuery(sSQL);	
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        //Do Equipment
        //        objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='EquipmentList']");	
        //        if(objControlXMLElement != null)
        //        {
        //            sCodeIds = objControlXMLElement.InnerText;
        //            if(sCodeIds.Trim() != "")
        //            {
        //                arrCodes = new string[1];
        //                if(sCodeIds.IndexOf(",") > 0)
        //                    arrCodes = sCodeIds.Split(',');
        //                else
        //                    arrCodes[0] = sCodeIds;
  
        //                for(int i = 0; i < arrCodes.Length; i++)  
        //                {
        //                    iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());
        //                    sSQL = "SELECT * FROM SYS_EVENT_CAT WHERE EVENT_CAT_CODE = " + iCodeId;
        //                    objDbReader = objConn.ExecuteReader(sSQL);
        //                    if(objDbReader != null)
        //                    {
        //                        if(objDbReader.Read())
        //                        {
        //                            objDbReader.Close();
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "UPDATE SYS_EVENT_CAT SET DISPLAY_ID = 3, UPDATED_BY_USER = '" + m_sUserName + "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) + "WHERE EVENT_CAT_CODE = " + iCodeId;
        //                            objConn.ExecuteNonQuery(sSQL);	
        //                        }
        //                        else
        //                        {
        //                            objDbReader.Close();
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(" + iCodeId + ",3,'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
        //                            objConn.ExecuteNonQuery(sSQL);	
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        //now do the sentinel event table
        //        sSQL = "DELETE FROM SYS_SENT_EVENT";
        //        objConn = DbFactory.GetDbConnection(m_sConnectionString);
        //        objConn.Open();
        //        objConn.ExecuteNonQuery(sSQL);
 	 		
				
        //        objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='SentinelList']");
				
        //        if(objControlXMLElement != null)
        //        {
        //            sCodeIds = objControlXMLElement.InnerText;
        //            if(sCodeIds.Trim() != "")
        //            {
        //                arrCodes = new string[1];
        //                if(sCodeIds.IndexOf(",") > 0)
        //                    arrCodes = sCodeIds.Split(',');
        //                else
        //                    arrCodes[0] = sCodeIds;
  
        //                for(int i = 0; i < arrCodes.Length; i++)  
        //                {
        //                    iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());  
        //                    sSQL = "SELECT * FROM SYS_SENT_EVENT WHERE EVENT_IND_CODE = " + iCodeId;
        //                    objDbReader = objConn.ExecuteReader(sSQL);;
        //                    if(objDbReader != null)
        //                    {
        //                        if(!objDbReader.Read()) 
        //                        {
        //                            objDbReader.Close();
        //                            sSQL = "INSERT INTO SYS_SENT_EVENT (EVENT_IND_CODE) VALUES(" + iCodeId + ")";
        //                            objConn.ExecuteNonQuery(sSQL);	
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        if(objConn != null)
        //            objConn.Close();
 
        //        objGroupXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//group[@name='OccParmsFacInfo']");  

        //        SysSettings objSysSettings = new SysSettings(m_sConnectionString); 
        //        objSysSettings.OCFacName = objGroupXmlElement.SelectSingleNode("//control[@name = 'FacilityName']").InnerText;
        //        objSysSettings.OCFacAddr1 = objGroupXmlElement.SelectSingleNode("//control[@name = 'FacilityAddress']").InnerText;
        //        objSysSettings.OCFacAddr2 = objGroupXmlElement.SelectSingleNode("//control[@name = 'Address2']").InnerText;
        //        objSysSettings.OCFacAddr3 = objGroupXmlElement.SelectSingleNode("//control[@name = 'City']").InnerText;
        //        objSysSettings.OCFacAddr4 = objGroupXmlElement.SelectSingleNode("//control[@name = 'StateZip']").InnerText;
        //        objSysSettings.OCFacHCFANo = objGroupXmlElement.SelectSingleNode("//control[@name = 'HCFANumber']").InnerText;
        //        objSysSettings.OCFacContact = objGroupXmlElement.SelectSingleNode("//control[@name = 'ContactPerson']").InnerText;
        //        objSysSettings.OCFacPhone = objGroupXmlElement.SelectSingleNode("//control[@name = 'FacilityPhone']").InnerText;
        //        objSysSettings.SaveSettings();

        //        return true;
        //    }
        //    catch(RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch(Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("OccurrenceParms.Save.Err"),p_objEx);
        //    }
        //    finally
        //    {
        //        if (objDbReader != null)
        //            objDbReader.Dispose();
        //        if (objConn != null)
        //            objConn.Dispose();
        //        objGroupXmlElement = null;
        //        objControlXMLElement = null;
        //    }
        //}

        //amrit: changes make in save function:searching controls directly without considering group first from p_objXmlDocument

        public bool Save(XmlDocument p_objXmlDocument)
        {
            string sSQL = "";
            DbReader objDbReader = null;
            DbConnection objConn = null;
            XmlElement objControlXMLElement = null;
            int iCodeId = 0;
            string sCodeIds = "";
            string[] arrCodes = { "" };

            try
            {
                //by amrit objGroupXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//group[@name='OccParmsEventConfig']");  

                //saving enhancement made for sentinel event and fall /medwatch indicators
                //first thing is to delete all records



                // by amrit
                sSQL = "DELETE FROM SYS_EVENT_CAT";
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);

                //then save  the edit is an option in case they select the same item for both.  
                //If this happens, it will be saved as medwatch, first save fall information
                //by amrit objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='FallEventCategList']");
                objControlXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FallEventCateg']");
                if (objControlXMLElement != null)
                {
                    //by amrit sCodeIds = objControlXMLElement.InnerText;
                    sCodeIds = objControlXMLElement.Attributes["codeid"].Value;
                    if (sCodeIds.Trim() != "")
                    {
                        arrCodes = new string[1];
                        if (sCodeIds.IndexOf(" ") > 0)
                            arrCodes = sCodeIds.Split(' ');
                        else
                            arrCodes[0] = sCodeIds;

                        for (int i = 0; i < arrCodes.Length; i++)
                        {
                            iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());
                            sSQL = "SELECT * FROM SYS_EVENT_CAT WHERE EVENT_CAT_CODE = " + iCodeId;
                            objDbReader = objConn.ExecuteReader(sSQL);
                            if (objDbReader != null)
                            {
                                if (objDbReader.Read())
                                {
                                    objDbReader.Close();
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    //sSQL = "UPDATE SYS_EVENT_CAT SET DISPLAY_ID = 1, UPDATED_BY_USER = '" + m_sUserName + "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) + "WHERE EVENT_CAT_CODE = " + iCodeId;
                                    sSQL = string.Format("UPDATE SYS_EVENT_CAT SET DISPLAY_ID = {0}, UPDATED_BY_USER = '{1}', DTTM_RCD_LAST_UPD = '{2}' WHERE EVENT_CAT_CODE = {3} ", (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Fall, m_sUserName, Conversion.ToDbDateTime(DateTime.Now), iCodeId);
                                    objConn.ExecuteNonQuery(sSQL);
                                }
                                else
                                {
                                    objDbReader.Close();
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    //sSQL = "INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(" + iCodeId + ",1,'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                                    sSQL = string.Format("INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES({0},{1},'{2}','{3}')", iCodeId, (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Fall, m_sUserName, Conversion.ToDbDateTime(DateTime.Now));
                                    objConn.ExecuteNonQuery(sSQL);
                                }
                            }
                        }
                    }
                }

                //now do the medwatch
                //by amrit objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='MedicationList']");	
                objControlXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Medication']");
                if (objControlXMLElement != null)
                {
                    //by amrit sCodeIds = objControlXMLElement.InnerText;
                    sCodeIds = objControlXMLElement.Attributes["codeid"].Value;
                    if (sCodeIds.Trim() != "")
                    {
                        arrCodes = new string[1];
                        if (sCodeIds.IndexOf(" ") > 0)
                            arrCodes = sCodeIds.Split(' ');
                        else
                            arrCodes[0] = sCodeIds;

                        for (int i = 0; i < arrCodes.Length; i++)
                        {
                            iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());
                            sSQL = "SELECT * FROM SYS_EVENT_CAT WHERE EVENT_CAT_CODE = " + iCodeId;
                            objDbReader = objConn.ExecuteReader(sSQL); ;
                            if (objDbReader != null)
                            {
                                if (objDbReader.Read())
                                {
                                    objDbReader.Close();
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    //sSQL = "UPDATE SYS_EVENT_CAT SET DISPLAY_ID = 1, UPDATED_BY_USER = '" + m_sUserName + "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) + "WHERE EVENT_CAT_CODE = " + iCodeId;
                                    sSQL = string.Format("UPDATE SYS_EVENT_CAT SET DISPLAY_ID = {0}, UPDATED_BY_USER = '{1}', DTTM_RCD_LAST_UPD = '{2}' WHERE EVENT_CAT_CODE = {3} ", (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Medication, m_sUserName, Conversion.ToDbDateTime(DateTime.Now), iCodeId);
                                    objConn.ExecuteNonQuery(sSQL);
                                }
                                else
                                {
                                    objDbReader.Close();
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    //sSQL = "INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(" + iCodeId + ",1,'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                                    sSQL = string.Format("INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES({0},{1},'{2}','{3}')", iCodeId, (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Medication, m_sUserName, Conversion.ToDbDateTime(DateTime.Now));
                                    objConn.ExecuteNonQuery(sSQL);
                                }
                            }
                        }
                    }
                }

                //Do Equipment
                //by amrit objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='EquipmentList']");	
                objControlXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Equipment']");
                if (objControlXMLElement != null)
                {
                    //by amrit sCodeIds = objControlXMLElement.InnerText;
                    sCodeIds = objControlXMLElement.Attributes["codeid"].Value;
                    if (sCodeIds.Trim() != "")
                    {
                        arrCodes = new string[1];
                        if (sCodeIds.IndexOf(" ") > 0)
                            arrCodes = sCodeIds.Split(' ');
                        else
                            arrCodes[0] = sCodeIds;

                        for (int i = 0; i < arrCodes.Length; i++)
                        {
                            iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());
                            sSQL = "SELECT * FROM SYS_EVENT_CAT WHERE EVENT_CAT_CODE = " + iCodeId;
                            objDbReader = objConn.ExecuteReader(sSQL);
                            if (objDbReader != null)
                            {
                                if (objDbReader.Read())
                                {
                                    objDbReader.Close();
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    //sSQL = "UPDATE SYS_EVENT_CAT SET DISPLAY_ID = 1, UPDATED_BY_USER = '" + m_sUserName + "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) + "WHERE EVENT_CAT_CODE = " + iCodeId;
                                    sSQL = string.Format("UPDATE SYS_EVENT_CAT SET DISPLAY_ID = {0}, UPDATED_BY_USER = '{1}', DTTM_RCD_LAST_UPD = '{2}' WHERE EVENT_CAT_CODE = {3} ", (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment, m_sUserName, Conversion.ToDbDateTime(DateTime.Now), iCodeId);
                                    objConn.ExecuteNonQuery(sSQL);
                                }
                                else
                                {
                                    objDbReader.Close();
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    //sSQL = "INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(" + iCodeId + ",1,'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                                    sSQL = string.Format("INSERT INTO SYS_EVENT_CAT (EVENT_CAT_CODE,DISPLAY_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES({0},{1},'{2}','{3}')", iCodeId, (int)Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment, m_sUserName, Conversion.ToDbDateTime(DateTime.Now));
                                    objConn.ExecuteNonQuery(sSQL);
                                }
                            }
                        }
                    }
                }

                //by amrit
                sSQL = "DELETE FROM SYS_SENT_EVENT";
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);


                //by amrit objControlXMLElement = (XmlElement)objGroupXmlElement.SelectSingleNode("//control[@name='SentinelList']");
                objControlXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Sentinel']");
                if (objControlXMLElement != null)
                {
                    //by amrit sCodeIds = objControlXMLElement.InnerText;
                    sCodeIds = objControlXMLElement.Attributes["codeid"].Value;
                    if (sCodeIds.Trim() != "")
                    {
                        arrCodes = new string[1];
                        if (sCodeIds.IndexOf(" ") > 0)
                            arrCodes = sCodeIds.Split(' ');
                        else
                            arrCodes[0] = sCodeIds;

                        for (int i = 0; i < arrCodes.Length; i++)
                        {
                            iCodeId = Conversion.ConvertStrToInteger(arrCodes[i].Trim().ToString());
                            sSQL = "SELECT * FROM SYS_SENT_EVENT WHERE EVENT_IND_CODE = " + iCodeId;
                            objDbReader = objConn.ExecuteReader(sSQL); ;
                            if (objDbReader != null)
                            {
                                if (!objDbReader.Read())
                                {
                                    objDbReader.Close();
                                    sSQL = "INSERT INTO SYS_SENT_EVENT (EVENT_IND_CODE) VALUES(" + iCodeId + ")";
                                    objConn.ExecuteNonQuery(sSQL);
                                }
                            }
                        }
                    }
                }

                if (objConn != null)
                    objConn.Close();

                //by amrit objGroupXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//group[@name='OccParmsFacInfo']");  

                SysSettings objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
          
                objSysSettings.OCFacName = p_objXmlDocument.SelectSingleNode("//control[@name = 'FacilityName']").InnerText;
                objSysSettings.OCFacAddr1 = p_objXmlDocument.SelectSingleNode("//control[@name = 'FacilityAddress']").InnerText;
                objSysSettings.OCFacAddr2 = p_objXmlDocument.SelectSingleNode("//control[@name = 'Address2']").InnerText;
                objSysSettings.OCFacAddr3 = p_objXmlDocument.SelectSingleNode("//control[@name = 'City']").InnerText;
                objSysSettings.OCFacAddr4 = p_objXmlDocument.SelectSingleNode("//control[@name = 'StateZip']").InnerText;
                objSysSettings.OCFacHCFANo = p_objXmlDocument.SelectSingleNode("//control[@name = 'HCFANumber']").InnerText;
                objSysSettings.OCFacContact = p_objXmlDocument.SelectSingleNode("//control[@name = 'ContactPerson']").InnerText;
                objSysSettings.OCFacPhone = p_objXmlDocument.SelectSingleNode("//control[@name = 'FacilityPhone']").InnerText;
                objSysSettings.SaveSettings();

                return true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("OccurrenceParms.Save.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objConn != null)
                    objConn.Dispose();
                //objGroupXmlElement = null;
                objControlXMLElement = null;
            }
        }


	}
}
