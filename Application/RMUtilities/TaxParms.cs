﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
	///Author	:   Animesh Sahai 
	///Class	:	TaxParms
	///Dated	:   30 July 2009
	///Purpose	:   This class is use Add / Modify the Tax data
	/// </summary>
    public class TaxParms
    {
        /// <summary>
		/// Private variable to store Connection String
		/// </summary>
		private string m_sConnectString = "";

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "";
		
		/// <summary>
		/// Private constant variable to store TableName
		/// </summary>
		private const string TABLENAME = "SYS_POL_TAX";
        private int m_iClientId = 0;
        /// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public TaxParms(string p_sLoginName, string p_sConnectionString, int p_iClientId)
		{
			m_sUserName = p_sLoginName;
			m_sConnectString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}

        /// Name		: LoadData
		/// Author		: Animesh Sahai
		/// Date Created	: 30 july 2009
		/// <summary>
		/// Loads Tax record on the basis of PolicyTaxrowid passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">The PolicyTaxrowid is passed in the XML
		///		The sample input XML will be
		///				<Document>
		///					<Tax>
		///						<use></use>
		///						<amount></amount>
		///						<effectivedate></effectivedate>
		///						<expirationdate></expirationdate>
		///						<taxtype></taxtype>
		///						<lob></lob>
		///						<state></state>
		///						<PolicyTaxrowid>4</PolicyTaxrowid>
		///					</Tax>
		///				</Document>
		/// </param>
		/// <returns>The XML with data is returned 
		///		The sample output XML will be 
		///				<Document>
		///					<Tax>
		///						<use></use>
		///						<amount></amount>
		///						<effectivedate></effectivedate>
		///						<expirationdate></expirationdate>
		///						<taxtype></taxtype>
		///						<lob></lob>
		///						<state></state>
		///						<PolicyTaxrowid>4</PolicyTaxrowid>
		///					</Tax>
		///				</Document>
		///</returns>
		public XmlDocument LoadData(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbReader objDbReader = null;
			string sTaxRowId = "";
			XmlElement objTaxXMLEle = null;

			string sValue=string.Empty;
			string sShortCode=string.Empty;
			string sDesc=string.Empty;
			XmlElement objElement=null;
			LocalCache objLocalCache=null;

			try
			{
				objTaxXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/Tax");

				sTaxRowId = objTaxXMLEle.GetElementsByTagName("PolicyTaxrowid").Item(0).InnerText;

				//In case of Edit
				if(sTaxRowId != "")
				{
					sSQL = "SELECT * FROM " + TABLENAME + " WHERE TAX_ROWID= " + sTaxRowId;
					objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
				
					if(objDbReader != null)
						if(objDbReader.Read())
						{
							sValue=objDbReader["IN_USE_FLAG"].ToString();
							if (sValue.Equals("-1") || sValue.Equals("1"))
								objTaxXMLEle.GetElementsByTagName("use").Item(0).InnerText = "True";
							else
								objTaxXMLEle.GetElementsByTagName("use").Item(0).InnerText= "";
							
							objTaxXMLEle.GetElementsByTagName("amount").Item(0).InnerText = objDbReader["AMOUNT"].ToString();
							objTaxXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText =  UTILITY.FormatDate(objDbReader["EFFECTIVE_DATE"].ToString(),false);
							objTaxXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText = UTILITY.FormatDate(objDbReader["EXPIRATION_DATE"].ToString(),false);

                            objLocalCache = new LocalCache(m_sConnectString, m_iClientId);

							objElement=(XmlElement)objTaxXMLEle.GetElementsByTagName("taxtype").Item(0);
							sValue= objDbReader["FLAT_OR_PERCENT"].ToString();
							objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);

							objElement=(XmlElement)objTaxXMLEle.GetElementsByTagName("lob").Item(0);
							sValue= objDbReader["LINE_OF_BUSINESS"].ToString();
							objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);

							objElement=(XmlElement)objTaxXMLEle.GetElementsByTagName("state").Item(0);
							sValue= objDbReader["STATE"].ToString();
							objLocalCache.GetStateInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);
						}
					if(objDbReader != null)
						objDbReader.Close();
				}

				return p_objInputXMLDoc;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("TaxParms.LoadData.Error", m_iClientId), p_objExp);   
			}
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }

		}

        /// Name		: Save
		/// Author		: Animesh Sahai
		/// Date Created	: 30 July 2009
		/// <summary>
		/// Saves Tax record on the basis of PolicyTaxrowid passed
		/// If new record then PolicyTaxrowid will be empty string
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<Tax>
		///						<use>1</use>
		///						<amount>28</amount>
		///						<effectivedate>20050406</effectivedate>
		///						<expirationdate>20050418</expirationdate>
		///						<taxtype>5237</taxtype>
		///						<lob>5188</lob>
		///						<state>73</state>
		///						<PolicyTaxrowid>4</PolicyTaxrowid> OR <PolicyTaxrowid></PolicyTaxrowid>(for new record)
		///					</Tax>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		public bool Save(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbConnection objCon = null; 
			string sTaxRowId = "";
			Taxes objTaxes = null;
			bool bIsNew = false;
			XmlElement objTaxXMLEle = null;

			try
			{
				if(Validate(p_objInputXMLDoc)) 
				{
					objTaxXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/Tax"); 	

					sTaxRowId = objTaxXMLEle.GetElementsByTagName("PolicyTaxrowid").Item(0).InnerText;

					objTaxes = new Taxes();

					if(sTaxRowId == "")
					{
						bIsNew = true;
                        objTaxes.TaxRowId = Utilities.GetNextUID(m_sConnectString, TABLENAME, m_iClientId);
						objTaxes.AddedByUser = m_sUserName;
                        objTaxes.DttmRcdAdded = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));
					}
					else
						objTaxes.TaxRowId = Conversion.ConvertStrToInteger(sTaxRowId); 

					objTaxes.UpdatedByUser = m_sUserName;
                    objTaxes.DttmRcdLastUpd = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));    
					
					if (objTaxXMLEle.GetElementsByTagName("use").Item(0).InnerText.ToLower().Trim().Equals("true"))
						objTaxes.Use=1;
					else
						objTaxes.Use=0;

                    objTaxes.State = Conversion.ConvertStrToInteger(objTaxXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value);
					objTaxes.Amount = Conversion.ConvertStrToDouble(objTaxXMLEle.GetElementsByTagName("amount").Item(0).InnerText);
					objTaxes.TaxType = Conversion.ConvertStrToInteger(objTaxXMLEle.GetElementsByTagName("taxtype").Item(0).Attributes["codeid"].Value);
					objTaxes.EffectiveDate = objTaxXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
					objTaxes.ExpirationDate = objTaxXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
					objTaxes.LOB = Conversion.ConvertStrToInteger(objTaxXMLEle.GetElementsByTagName("lob").Item(0).Attributes["codeid"].Value);

					if(bIsNew)
					{
                        if (objTaxes.ExpirationDate.Trim() == "" || objTaxes.ExpirationDate == "0")
                        {
                            sSQL = "INSERT INTO " + TABLENAME + " (TAX_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," +
                            "UPDATED_BY_USER,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,LINE_OF_BUSINESS," +
                            "IN_USE_FLAG) VALUES(" + objTaxes.TaxRowId + ",'" + objTaxes.AddedByUser + "'," + objTaxes.DttmRcdAdded + "," +
                            objTaxes.DttmRcdLastUpd + ",'" + objTaxes.UpdatedByUser + "'," +
                            objTaxes.State + "," + objTaxes.Amount + "," + objTaxes.TaxType + ",'" + UTILITY.FormatDate(objTaxes.EffectiveDate, true) + "',"  +
                            objTaxes.LOB + "," + objTaxes.Use + ")";
                        }
                        else
                        {
                            sSQL = "INSERT INTO " + TABLENAME + " (TAX_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," +
                            "UPDATED_BY_USER,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,EXPIRATION_DATE,LINE_OF_BUSINESS," +
                            "IN_USE_FLAG) VALUES(" + objTaxes.TaxRowId + ",'" + objTaxes.AddedByUser + "'," + objTaxes.DttmRcdAdded + "," +
                            objTaxes.DttmRcdLastUpd + ",'" + objTaxes.UpdatedByUser + "'," +
                            objTaxes.State + "," + objTaxes.Amount + "," + objTaxes.TaxType + ",'" + UTILITY.FormatDate(objTaxes.EffectiveDate, true) + "','" +
                            UTILITY.FormatDate(objTaxes.ExpirationDate, true) + "'," + objTaxes.LOB + "," + objTaxes.Use + ")";
                        }
					}
					else
					{
                        if (objTaxes.ExpirationDate.Trim() == "" || objTaxes.ExpirationDate == "0")
                        {
                            sSQL = "UPDATE " + TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objTaxes.DttmRcdLastUpd + "," +
                                "UPDATED_BY_USER='" + objTaxes.UpdatedByUser + "'," + "STATE=" + objTaxes.State +
                                "," + "AMOUNT=" + objTaxes.Amount + "," + "FLAT_OR_PERCENT=" + objTaxes.TaxType + "," +
                                "EFFECTIVE_DATE='" + UTILITY.FormatDate(objTaxes.EffectiveDate, true) + "'," + "EXPIRATION_DATE= NULL"  + "," +
                                "LINE_OF_BUSINESS=" + objTaxes.LOB + "," + "IN_USE_FLAG=" + objTaxes.Use +
                                " WHERE TAX_ROWID=" + objTaxes.TaxRowId;
                        }
                        else
                        {
                            sSQL = "UPDATE " + TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objTaxes.DttmRcdLastUpd + "," +
                                "UPDATED_BY_USER='" + objTaxes.UpdatedByUser + "'," + "STATE=" + objTaxes.State +
                                "," + "AMOUNT=" + objTaxes.Amount + "," + "FLAT_OR_PERCENT=" + objTaxes.TaxType + "," +
                                "EFFECTIVE_DATE='" + UTILITY.FormatDate(objTaxes.EffectiveDate, true) + "'," + "EXPIRATION_DATE='" + UTILITY.FormatDate(objTaxes.ExpirationDate, true) + "'," +
                                "LINE_OF_BUSINESS=" + objTaxes.LOB + "," + "IN_USE_FLAG=" + objTaxes.Use +
                                " WHERE TAX_ROWID=" + objTaxes.TaxRowId;
                        }
					}

					objCon = DbFactory.GetDbConnection(m_sConnectString);
					objCon.Open();
					objCon.ExecuteNonQuery(sSQL);
					objCon.Dispose(); 

					return true;
				}
				else
					return false;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("TaxParms.Save.Error", m_iClientId), p_objExp);   
			}
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
            }
        }

        /// Name		: Delete
		/// Author		: Animesh Sahai
		/// Date Created	: 30 July 2009
		/// <summary>
		/// Deletes Tax record
		/// </summary>
		/// <param name="p_iExpConRowId">Tax ID</param>
		/// <returns>True for sucess and false for failure</returns>
		internal bool Delete(int p_iTaxRowId)
		{
			string sSQL = "";
			DbConnection objCon = null;
            try
            {
                if (p_iTaxRowId != 0)
                {
                    sSQL = "DELETE FROM " + TABLENAME + " WHERE TAX_ROWID = " + p_iTaxRowId;
                    objCon = DbFactory.GetDbConnection(m_sConnectString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);
                    objCon.Dispose();
                }
                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TaxParms.Delete.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null) 
                    objCon.Dispose(); 
            }
		}

        /// Name		: Validate
		/// Author		: Animesh Sahai
		/// Date Created	: 30 July 2009
		/// <summary>
		/// Validate Tax data for the values passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<Tax>
		///						<use></use>
		///						<amount></amount>
		///						<effectivedate></effectivedate>
		///						<expirationdate></expirationdate>
		///						<taxtype></taxtype>
		///						<lob></lob>
		///						<state></state>
		///						<PolicyTaxrowid>4</PolicyTaxrowid>
		///					</Tax>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		private bool Validate(XmlDocument p_objInputXMLDoc)
		{
			string sTaxRowId = "";
			string sLOB = "";
			string sState = "";
			string sEffectiveDate = "";
			string sExpirationDate = "";
			string sSQL = "";
			DbReader objDBReader = null;
			DbReader objTempDBReader = null;
			bool bIsReaderHasRows = false;
			XmlElement objTaxXMLEle = null;

			try
			{
				objTaxXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/Tax");

				sTaxRowId = objTaxXMLEle.GetElementsByTagName("PolicyTaxrowid").Item(0).InnerText;
                sLOB = objTaxXMLEle.GetElementsByTagName("lob").Item(0).Attributes["codeid"].Value;
                sState = objTaxXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value;
                sEffectiveDate = objTaxXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
                sExpirationDate = objTaxXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;

				if(sExpirationDate.Trim() == "")
				{
					sSQL = "SELECT TAX_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + 
						sLOB + " AND STATE = " + sState + " AND EXPIRATION_DATE IS NULL ";
				
					objDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

					while(objDBReader.Read())
					{
						bIsReaderHasRows = true;

						if(objDBReader["TAX_ROWID"].ToString() != sTaxRowId)
                            throw new RMAppException(Globalization.GetString("TaxParms.Validate.AlreadyExistsWithNoExpDate", m_iClientId));
						else
						{
							sSQL = "SELECT COUNT(TAX_ROWID) FROM " + TABLENAME + " WHERE" + 
								" LINE_OF_BUSINESS = " + sLOB + " AND STATE = " + sState + 
								" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'";

							objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
							if(objTempDBReader.Read())
							{
								if(Conversion.ConvertObjToInt(objTempDBReader.GetValue(0), m_iClientId) > 0)
                                    throw new RMAppException(Globalization.GetString("TaxParms.Validate.AlreadyExists", m_iClientId));
							}
							objTempDBReader.Close();
						}
					}
					objDBReader.Close();

					if(bIsReaderHasRows == false)
					{
						sSQL = "SELECT TAX_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + sLOB + 
							" AND STATE = " + sState + " AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'";

						objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						while(objTempDBReader.Read())
						{
							if(objTempDBReader["TAX_ROWID"].ToString() != sTaxRowId)
                                throw new RMAppException(Globalization.GetString("TaxParms.Validate.AlreadyExists", m_iClientId));
						}
						objTempDBReader.Close();
					}
				}
				else
				{
					sSQL = "SELECT TAX_ROWID, EFFECTIVE_DATE FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + 
						sLOB + " AND STATE = " + sState + " AND EXPIRATION_DATE IS NULL ";
				
					objDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

					while(objDBReader.Read())
					{
						bIsReaderHasRows = true;

						if(objDBReader["TAX_ROWID"].ToString() != sTaxRowId)
						{
							//if(Conversion.ToDate(sEffectiveDate) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim()))
                            if (DateTime.Parse(sEffectiveDate) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim()))
                                throw new RMAppException(Globalization.GetString("TaxParms.Validate.AlreadyExists", m_iClientId));
							else
							{
								sSQL = "SELECT TAX_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + sLOB + 
									" AND STATE = " + sState + " AND EXPIRATION_DATE >= '" + 
									Conversion.GetDate(sEffectiveDate) + "' AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

								objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
								while(objTempDBReader.Read())
								{
									if(objTempDBReader["TAX_ROWID"].ToString() != sTaxRowId)
                                        throw new RMAppException(Globalization.GetString("TaxParms.Validate.AlreadyExists", m_iClientId));
								}
								objTempDBReader.Close();
							}
						}
						else
						{
							sSQL = "SELECT COUNT(TAX_ROWID) FROM " + TABLENAME + " WHERE" + 
								" LINE_OF_BUSINESS = " + sLOB + " AND STATE = " + sState + 
								" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'" + 
								" AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

							objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
							while(objTempDBReader.Read())
							{
								if(Conversion.ConvertObjToInt(objTempDBReader.GetValue(0), m_iClientId) > 0)
                                    throw new RMAppException(Globalization.GetString("TaxParms.Validate.AlreadyExists", m_iClientId));
							}
							objTempDBReader.Close();
						}
					}
					objDBReader.Close();
 
					if(bIsReaderHasRows == false)
					{
						sSQL = "SELECT TAX_ROWID FROM " + TABLENAME + " WHERE" + 
							" LINE_OF_BUSINESS = " + sLOB + " AND STATE = " + sState + 
							" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'" + 
							" AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

						objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						while(objTempDBReader.Read())
						{
							if(objTempDBReader["TAX_ROWID"].ToString() != sTaxRowId)
                                throw new RMAppException(Globalization.GetString("TaxParms.Validate.AlreadyExists", m_iClientId));
						}
						objTempDBReader.Close();
					}
				}
				return true;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
            finally
            {
                if (objDBReader != null)
                    objDBReader.Dispose();
                if (objTempDBReader != null)
                    objTempDBReader.Dispose();
            }

		}
    }

    /// <summary>
	///Author	:   Animesh Sahai
	///Class	:	Taxes
	///Dated	:   30 July 2009
	///Purpose	:   This class works as a container class for "SYS_POL_TAX" table
	/// </summary>
	public class Taxes
	{
		public Taxes()
		{
		}

		/// <summary>
		/// Stores the TaxRowId value
		/// </summary>
		private int m_iTaxRowId = 0;

		/// <summary>
		/// Stores the Use value
		/// </summary>
		private int m_iUse = 0;

		/// <summary>
		/// Stores the LOB value
		/// </summary>
		private int m_iLOB = 0;

		/// <summary>
		/// Stores the State value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the Amount value
		/// </summary>
		private double m_dAmount = 0.0;

		/// <summary>
		/// Stores the TaxType value
		/// </summary>
		private int m_iTaxType = 0;

		/// <summary>
		/// Stores the EffectiveDate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the ExpirationDate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

		/// <summary>
		/// Read/Write property for TaxRowId
		/// </summary>
		internal int TaxRowId
		{
			get
			{
				return m_iTaxRowId;
			}
			set
			{
				m_iTaxRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for Use
		/// </summary>
		internal int Use
		{
			get
			{
				return m_iUse;
			}
			set
			{
				m_iUse = value;
			}
		}

		/// <summary>
		/// Read/Write property for LOB
		/// </summary>
		internal int LOB
		{
			get
			{
				return m_iLOB;
			}
			set
			{
				m_iLOB = value;
			}
		}

		/// <summary>
		/// Read/Write property for State
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for Amount
		/// </summary>
		internal double Amount
		{
			get
			{
				return m_dAmount;
			}
			set
			{
				m_dAmount = value;
			}
		}

		/// <summary>
		/// Read/Write property for TaxType
		/// </summary>
		internal int TaxType
		{
			get
			{
				return m_iTaxType;
			}
			set
			{
				m_iTaxType = value;
			}
		}

		/// <summary>
		/// Read/Write property for EffectiveDate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpirationDate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
	}

}
