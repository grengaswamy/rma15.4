using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Data;
using System.Text;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.Security;
//Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
using Riskmaster.Settings;
using Riskmaster.Application.SecurityManagement;
//End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Pankaj Chowdhury
    ///Dated   :   09th,Jul 2005
    ///Purpose :   This is the main class that implements the functionality
    ///				of Admin Tracking, Supplemental and Jurisdictional Tables. Returns XML string for each 
    ///				of the public method called.
    /// </summary>
    public class TableManager
    {
        /// <summary>
        /// Private connection string variable
        /// </summary>
        string m_sConnStr = "";
        string m_sUserName = "";    //gagnihotri MITS 11995 Changes made for Audit table
        private int m_iClientId = 0;//psharma206 jira 57

        /// <summary>
        /// Default Connection String
        /// </summary>
        /// <param name="p_sConnStr">Connect</param>
        public TableManager(string p_sConnStr, int p_iClientId)//psharma206 jira 57
        {
            m_sConnStr = p_sConnStr;
            m_iClientId = p_iClientId;//psharma206 jira 57
        }
        //gagnihotri MITS 11995 Changes made for Audit table
        public TableManager(string p_sConnStr, string p_sUserName, int p_iClientId)//psharma206 jira 57
        {
            m_sConnStr = p_sConnStr;
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;//psharma206 jira 57
        }

        /// Name		: CreateTable
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/08/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Overloaded function CreateTable
        /// </summary>
        /// <param name="p_sSysTableName">System Table Name</param>
        /// <param name="p_sUserTableName">User Table Name</param>
        /// <param name="p_sTableType">Table Type. Admin Tracking=8, supplemental= 6, jurisdictional=9</param>
        public void CreateTable(string p_sSysTableName, string p_sUserTableName, string p_sTableType)
        {
            if (p_sSysTableName.Length > 12)
                p_sSysTableName = p_sSysTableName.Substring(0, 12).ToUpper().Trim();
            else
                p_sSysTableName = p_sSysTableName.Substring(0, p_sSysTableName.Length).ToUpper().Trim();
            CreateTable(p_sSysTableName, p_sUserTableName, p_sTableType, p_sSysTableName + "_ID", 7, 0);
        }

        /// Name		: CreateTable
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/08/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function taken from Utilities bAddTable
        /// Creates table in the Database
        /// </summary>
        /// <param name="p_sSysTableName">System Table Name</param>
        /// <param name="p_sUserTableName">User Table Name</param>
        /// <param name="p_sTableType">Table Type. Admin Tracking=8, supplemental= 6, jurisdictional=9</param>
        /// <param name="p_sFieldName">Field Name</param>
        /// <param name="p_iFieldType">Field Type</param>
        /// <param name="p_iFieldSize">Field Size</param>
        public void CreateTable(string p_sSysTableName, string p_sUserTableName, string p_sTableType,
            string p_sFieldName, int p_iFieldType, int p_iFieldSize)
        {
            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012
            
            string sSQL = "";
            int iTableId = 0;
            int iFieldId = 0;
            int iTableCount = 0; // Counter for user tables
            DbCommand objCmd = null;
            LocalCache objCache = null;
            DbConnection objCon = null;
            DbTransaction objTrans = null;
            try
            {
                //If invalid input raise error
                if (p_sUserTableName.Trim() == "" || p_sSysTableName.Trim() == "")
                    throw new RMAppException(Globalization.GetString("TableManager.CreateTable.InvalidInput", m_iClientId));

                objCon = DbFactory.GetDbConnection(m_sConnStr);
                objCon.Open();
                objCmd = objCon.CreateCommand();

                //Geeta 10-Oct-06 : Check for user table name for fixing MITS 8020
                //PenTesting - srajindersin - 9th Jan 2012
                strSQL.Append(string.Format("SELECT COUNT(TABLE_ID) FROM GLOSSARY_TEXT WHERE TABLE_NAME = {0}", "~TABLE_NAME~"));
                dictParams.Add("TABLE_NAME", p_sUserTableName);

                objCmd.CommandText = strSQL.ToString();
                iTableCount = Conversion.ConvertStrToInteger(DbFactory.ExecuteScalar(objCmd,dictParams).ToString());
                //sSQL = "SELECT COUNT(TABLE_ID) FROM GLOSSARY_TEXT WHERE TABLE_NAME = '" +
                //        p_sUserTableName + "'";

                //iTableCount = Conversion.ConvertStrToInteger(objCon.ExecuteScalar(sSQL).ToString());

                //END PenTesting - srajindersin - 9th Jan 2012
                if (iTableCount > 0)
                {
                    throw new RMAppException(Globalization.GetString("ADMForms.CreateTable.DataErr", m_iClientId));
                }
                else
                {
                    //Make the create table query
                    switch (objCon.DatabaseType)
                    {
                        case eDatabaseType.DBMS_IS_ACCESS:
                            sSQL = "CREATE TABLE " + p_sSysTableName + " (" +
                                p_sFieldName + " " + TypeMap(objCon, p_iFieldType, p_iFieldSize) + ")";
                            break;
                        //Following have the same sql
                        case eDatabaseType.DBMS_IS_INFORMIX:
                        case eDatabaseType.DBMS_IS_SQLSRVR:
                        case eDatabaseType.DBMS_IS_SYBASE:
                        case eDatabaseType.DBMS_IS_ORACLE:
                        case eDatabaseType.DBMS_IS_DB2:
                            sSQL = "CREATE TABLE " + p_sSysTableName + " (" +
                                p_sFieldName + " " + TypeMap(objCon, p_iFieldType, p_iFieldSize) +
                                " NOT NULL)";
                            break;
                    }


                    //objCmd=objCon.CreateCommand();
                    objTrans = objCon.BeginTransaction();
                    objCmd.Transaction = objTrans;
                    objCmd.CommandText = sSQL;
                    objCmd.ExecuteNonQuery();

                    CreatePrimaryKey(objCmd, p_sSysTableName, p_sFieldName);

                    //Initialize LocalCache
                    objCache = new LocalCache(m_sConnStr, m_iClientId);

                    iTableId = Utilities.GetNextUID(m_sConnStr, "GLOSSARY", m_iClientId);

                    //Glossary entry
                    //PenTesting - srajindersin - 9th Jan 2012
                    DbWriter writer = DbFactory.GetDbWriter(objCon);
                    writer.Tables.Add("GLOSSARY");

                    writer.Fields.Add("TABLE_ID", iTableId);
                    writer.Fields.Add("SYSTEM_TABLE_NAME",p_sSysTableName);
                    writer.Fields.Add("GLOSSARY_TYPE_CODE", objCache.GetCodeId(p_sTableType, "GLOSSARY_TYPES"));
                    writer.Fields.Add("ATTACHMENTS_FLAG", 0);
                    writer.Fields.Add("RELATED_TABLE_ID", 0);
                    writer.Fields.Add("REQD_REL_TABL_FLAG", 0);
                    writer.Fields.Add("DTTM_LAST_UPDATE",  Conversion.ToDbDateTime(DateTime.Now).ToString());
                    writer.Fields.Add("DELETED_FLAG", 0);

                    writer.Execute(objTrans);
                    writer.Reset(true);
                    //sSQL = "INSERT INTO GLOSSARY(TABLE_ID,SYSTEM_TABLE_NAME,GLOSSARY_TYPE_CODE,ATTACHMENTS_FLAG," +
                    //    "RELATED_TABLE_ID,REQD_REL_TABL_FLAG,DTTM_LAST_UPDATE,DELETED_FLAG) VALUES (" +
                    //    iTableId + ",'" + p_sSysTableName + "'," +
                    //    objCache.GetCodeId(p_sTableType, "GLOSSARY_TYPES") +
                    //    ",0,0,0,'" + Conversion.ToDbDateTime(DateTime.Now) + "',0)";

                    //objCmd.CommandText = sSQL;
                    //objCmd.ExecuteNonQuery();

                    writer.Tables.Add("GLOSSARY_TEXT");

                    writer.Fields.Add("TABLE_ID", iTableId);
                    writer.Fields.Add("TABLE_NAME", p_sUserTableName.Trim());
                    writer.Fields.Add("LANGUAGE_CODE", 1033);

                    writer.Execute(objTrans);
                    writer.Reset(true);
                    //sSQL = "INSERT INTO GLOSSARY_TEXT(TABLE_ID,TABLE_NAME,LANGUAGE_CODE) VALUES (" +
                    //    iTableId + ",'" + p_sUserTableName.Trim() + "',1033)";
                    //objCmd.CommandText = sSQL;
                    //objCmd.ExecuteNonQuery();

                    //sSQL = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + Conversion.ToDbDateTime(DateTime.Now) +
                    //    "' WHERE SYSTEM_TABLE_NAME = '" + p_sSysTableName + "'";
                    //objCmd.CommandText = sSQL;
                    //objCmd.ExecuteNonQuery();

                    //END PenTesting - srajindersin - 9th Jan 2012

                    iFieldId = Utilities.GetNextUID(m_sConnStr, "SUPP_DICTIONARY", m_iClientId);

                    //gagnihotri MITS 11995 Changes made for Audit table
                    //PenTesting - srajindersin - 9th Jan 2012
                    writer.Tables.Add("SUPP_DICTIONARY");

                    writer.Fields.Add("FIELD_ID", iFieldId);
                    writer.Fields.Add("SUPP_TABLE_NAME", p_sSysTableName );
                    writer.Fields.Add("SYS_FIELD_NAME", p_sFieldName );
                    writer.Fields.Add("DELETE_FLAG", 0);
                    writer.Fields.Add("FIELD_SIZE", 0);
                    writer.Fields.Add("REQUIRED_FLAG", 0);
                    writer.Fields.Add("FAS_REPORTABLE", 0);//asharma326 MITS 32386
                    writer.Fields.Add("LOOKUP_FLAG", 0);
                    writer.Fields.Add("IS_PATTERNED", 0);
                    writer.Fields.Add("CODE_FILE_ID", 0);
                    writer.Fields.Add("GRP_ASSOC_FLAG", 0);
                    writer.Fields.Add("NETVISIBLE_FLAG", 0);
                    writer.Fields.Add("FIELD_TYPE", 7);
                    writer.Fields.Add("UPDATED_BY_USER", m_sUserName );
                    writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));

                    writer.Execute(objTrans);
                    writer.Reset(true);
                    //sSQL = "INSERT INTO SUPP_DICTIONARY(FIELD_ID,SUPP_TABLE_NAME,SYS_FIELD_NAME,DELETE_FLAG," +
                    //    "FIELD_SIZE, REQUIRED_FLAG,LOOKUP_FLAG,IS_PATTERNED,CODE_FILE_ID,GRP_ASSOC_FLAG,NETVISIBLE_FLAG," +
                    //    "FIELD_TYPE, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES ( " + iFieldId + ",'" + p_sSysTableName +
                    //    "','" + p_sFieldName + "',0,0,0,0,0,0,0,0,7,'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                    //objCmd.CommandText = sSQL;
                    //objCmd.ExecuteNonQuery();
                    //END PenTesting - srajindersin - 9th Jan 2012

                    //commit
                    objCmd.Transaction.Commit();
                }
            }
            catch (RMAppException p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("ADMForms.CreateTable.DataErr", m_iClientId), p_objEx);
            }
            finally
            {
                objCmd = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objTrans != null)
                {
                    objTrans.Dispose();
                }
            }
        }


        /// Name		: CreatePrimaryKey
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/09/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates Index on Primary Key
        /// </summary>
        /// <param name="p_objCmd">DB Command</param>
        /// <param name="p_sSysTableName">System Table Name</param>
        /// <param name="p_sKeyField">Key Field</param>
        private void CreatePrimaryKey(DbCommand p_objCmd, string p_sSysTableName, string p_sKeyField)
        {
            string sKeyName = "";
            string sSql = "";
            int iLen = 0;
            try
            {
                iLen = p_sSysTableName.Trim().Length;
                if (iLen > 16)
                    iLen = 16;

                //Primary Key Name
                sKeyName = p_sSysTableName.ToUpper().Trim().Substring(0, iLen) + "_PK";
                sSql = "CREATE UNIQUE INDEX " + sKeyName + " ON " + p_sSysTableName.ToUpper().Trim() +
                    " (" + p_sKeyField.ToUpper().Trim() + ")";
                if (p_objCmd.Connection.DatabaseType == eDatabaseType.DBMS_IS_ACCESS)
                    sSql = sSql + " WITH PRIMARY";

                p_objCmd.CommandText = sSql;
                p_objCmd.ExecuteNonQuery();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TableManager.CreatePrimaryKey.DataErr", m_iClientId), p_objEx);
            }
        }

        /// Name		: TypeMap
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/09/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Translates RM supplemental type to DB type string
        /// </summary>
        /// <param name="p_objCon">Connection Object</param>
        /// <param name="p_iSuppFieldType">Field Type</param>
        /// <param name="p_iSize">Field Size</param>
        /// <returns>Concatenated String</returns>
        internal static string TypeMap(DbConnection p_objCon, int p_iSuppFieldType, int p_iSize)
        {
            string sType = "";
            switch (p_iSuppFieldType)
            {
                //string, date, time, lookups
                case 0:
                case 3:
                case 4:
                case 10:
                case 12:
                case 13:
                //Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                case 18:
                case 19:
                //End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                    switch (p_objCon.DatabaseType)
                    {
                        case eDatabaseType.DBMS_IS_ACCESS:
                            sType = "TEXT";
                            break;
                        case eDatabaseType.DBMS_IS_INFORMIX:
                        case eDatabaseType.DBMS_IS_SQLSRVR:
                        case eDatabaseType.DBMS_IS_SYBASE:
                        case eDatabaseType.DBMS_IS_DB2:
                            sType = "VARCHAR";
                            break;
                        case eDatabaseType.DBMS_IS_ORACLE:
                            sType = "VARCHAR2";
                            break;
                    }
                    //Add Size
                    sType = sType + "(" + p_iSize.ToString() + ")";
                    break;
                //number, currency (float)
                case 1:
                case 2:
                    switch (p_objCon.DatabaseType)
                    {
                        case eDatabaseType.DBMS_IS_ACCESS:
                            sType = "DOUBLE";
                            break;
                        case eDatabaseType.DBMS_IS_INFORMIX:
                        case eDatabaseType.DBMS_IS_SQLSRVR:
                        case eDatabaseType.DBMS_IS_SYBASE:
                        case eDatabaseType.DBMS_IS_DB2:
                            sType = "FLOAT";
                            break;
                        case eDatabaseType.DBMS_IS_ORACLE:
                            sType = "NUMBER";
                            break;
                    }
                    break;
                //code, primary key, entity, state, MultiCode, MultiState, MultiEntity
                case 6:
                case 7:
                case 8:
                case 9:
                case 14:
                case 15:
                case 16:
                case 22://sharishkumar Jira 6415
                case 31: // //MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22      
                    switch (p_objCon.DatabaseType)
                    {
                        case eDatabaseType.DBMS_IS_ACCESS:
                            sType = "LONG";
                            break;
                        case eDatabaseType.DBMS_IS_SQLSRVR:
                        case eDatabaseType.DBMS_IS_SYBASE:
                            sType = "INTEGER";
                            break;
                        case eDatabaseType.DBMS_IS_INFORMIX:
                        case eDatabaseType.DBMS_IS_DB2:
                            sType = "INTEGER";
                            break;
                        case eDatabaseType.DBMS_IS_ORACLE:
                            sType = "NUMBER(10,0)";
                            break;
                    }
                    break;
                //multi-valued and free text
                case 5:
                case 25://case for HTML Text asharma326 JIRA 6422
                case 11:
                    switch (p_objCon.DatabaseType)
                    {
                        case eDatabaseType.DBMS_IS_ACCESS:
                            sType = "MEMO";
                            break;
                        case eDatabaseType.DBMS_IS_INFORMIX:
                        case eDatabaseType.DBMS_IS_SQLSRVR:
                        case eDatabaseType.DBMS_IS_SYBASE:
                            sType = "TEXT";
                            break;
                        case eDatabaseType.DBMS_IS_DB2:
                            sType = "CLOB(10M)";
                            break;
                        case eDatabaseType.DBMS_IS_ORACLE:
                           // sType = "VARCHAR2(2000)";
                            sType = "CLOB";//nadim 11699
                            break;
                    }
                    break;
                case 23://WWIG GAP20A - agupta298 - MITS 36804 - Added for Hyperlink - JIRA - 4691
                    switch (p_objCon.DatabaseType)
                    {
                        case eDatabaseType.DBMS_IS_ACCESS:
                            sType = "TEXT";
                            break;
                        case eDatabaseType.DBMS_IS_INFORMIX:
                        case eDatabaseType.DBMS_IS_SQLSRVR:
                        case eDatabaseType.DBMS_IS_SYBASE:
                        case eDatabaseType.DBMS_IS_DB2:
                            sType = "VARCHAR(MAX)";
                            break;
                        case eDatabaseType.DBMS_IS_ORACLE:
                            sType = "CLOB";
                            break;
                    }
                    break;
            }
            return sType;
        }

        /// Name		: DeleteTable
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/09/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Delete the table
        /// </summary>
        /// <param name="p_sSysTableName">System Table Name</param>
        /// <param name="p_sUserTableName">User Table Name</param>
        public void DeleteTable(string p_sSysTableName, string p_sUserTableName)
        {
            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012

            string sSQL = "";
            DbCommand objCmd = null;
            DbConnection objCon = null;
            DbTransaction objTrans = null;
            DbReader objRdr = null;
            string sTableId = "";
           string sSecConnString = string.Empty;//mbahl3
                     
            try
            {            
                sSecConnString = RMConfigurationManager.GetConnectionString("RMXSecurity", m_iClientId); //mbahl3
                //Check for valid Input
                if (p_sSysTableName.Trim() == "" || p_sUserTableName.Trim() == "")
                    throw new RMAppException(Globalization.GetString("TableManager.DeleteTable.InvalidInput", m_iClientId));

                p_sSysTableName = p_sSysTableName.ToUpper().Trim();

                //-- ABhateja, 10.09.2006, MITS 8022 -START-
                //-- Get the table id before deleting the table entry from the glossary_text table.
                //-- This is required in order to uniquely identify the table_name in glossary_text table.

                //PenTesting - srajindersin - 9th Jan 2012
                strSQL.Append(string.Format("SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = {0}", "~SYSTEM_TABLE_NAME~"));
                dictParams.Add("SYSTEM_TABLE_NAME", p_sSysTableName);

                objRdr = DbFactory.ExecuteReader(m_sConnStr, strSQL.ToString(), dictParams);

                //sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = " + Utilities.FormatSqlFieldValue(p_sSysTableName);
                //objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                //END PenTesting - srajindersin - 9th Jan 2012
                
                while (objRdr.Read())
                {
                    sTableId = objRdr.GetInt32(0).ToString();
                }
                objRdr.Dispose();
                //-- ABhateja, 10.09.2006, MITS 8022 -END-

                //Drop table
                //sSQL = "DROP TABLE " + p_sSysTableName;

                objCon = DbFactory.GetDbConnection(m_sConnStr);
                objCon.Open();

                objCmd = objCon.CreateCommand();
                objTrans = objCon.BeginTransaction();
                objCmd.Transaction = objTrans;

               
                //MITS 31300 Raman: Seems like the following statement was left uncommented
                    //objCmd.ExecuteNonQuery();

                //delete glossary entry
                strSQL = new StringBuilder();
                strSQL.Append(string.Format("DELETE FROM GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME = {0}", "~SYSTEM_TABLE_NAME~"));
                dictParams.Clear();
                dictParams.Add("SYSTEM_TABLE_NAME", p_sSysTableName);
                objCmd.CommandText = strSQL.ToString();

                DbFactory.ExecuteNonQuery(objCmd, dictParams); 
                objCmd.Parameters.Clear();

                //sSQL = "DELETE FROM GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" +
                //    p_sSysTableName + "'";
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();
                //END PenTesting - srajindersin - 9th Jan 2012

                //-- ABhateja, 10.09.2006, MITS 8022 -START-
                //-- Changed the query below.Now the table will be deleted from the glossary_text based on the 
                //-- table_id and not table_name.As a result the parameter p_sUserTableName is not being used in the 
                //-- function anymore.
                //				sSQL = "DELETE FROM GLOSSARY_TEXT WHERE GLOSSARY_TEXT.TABLE_NAME = '" + 
                //					p_sUserTableName + "'";
                //PenTesting - srajindersin - 9th Jan 2012
                strSQL = new StringBuilder();
                strSQL.Append(string.Format("DELETE FROM GLOSSARY_TEXT WHERE GLOSSARY_TEXT.TABLE_ID = {0}", "~TABLE_ID~"));
                dictParams.Clear();
                dictParams.Add("TABLE_ID", sTableId);
                objCmd.CommandText = strSQL.ToString(); 

                    DbFactory.ExecuteNonQuery(objCmd, dictParams); objCmd.Parameters.Clear();

                //sSQL = "DELETE FROM GLOSSARY_TEXT WHERE GLOSSARY_TEXT.TABLE_ID = " + sTableId;
                ////-- ABhateja, 10.09.2006, MITS 8022 -END-
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();

                strSQL = new StringBuilder();
                strSQL.Append(string.Format("UPDATE GLOSSARY SET DTTM_LAST_UPDATE =  {0}", "~DTTM_LAST_UPDATE~"));
                strSQL.Append(" WHERE SYSTEM_TABLE_NAME = 'GLOSSARY'");
                dictParams.Clear();
                dictParams.Add("DTTM_LAST_UPDATE", Conversion.ToDbDateTime(DateTime.Now));

                objCmd.CommandText = strSQL.ToString();
                DbFactory.ExecuteNonQuery(objCmd, dictParams); 
                objCmd.Parameters.Clear();

                //sSQL = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + Conversion.ToDbDateTime(DateTime.Now) +
                //    "' WHERE SYSTEM_TABLE_NAME = 'GLOSSARY'";
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();

                //Delete supp_dictionary entry
                strSQL = new StringBuilder();
                strSQL.Append(string.Format("DELETE FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = {0}", "~SUPP_TABLE_NAME~"));
                dictParams.Clear();
                dictParams.Add("SUPP_TABLE_NAME", p_sSysTableName);

                objCmd.CommandText = strSQL.ToString();
                DbFactory.ExecuteNonQuery(objCmd, dictParams); 
                objCmd.Parameters.Clear();

                //PenTesting - srajindersin - 9th Jan 2012
                sSQL = "DROP TABLE " + p_sSysTableName;
                objCmd.CommandText = sSQL;
                objCmd.ExecuteNonQuery();

                objCmd.Transaction.Commit();
                 //mbahl3
                //Delete supp_dictionary entry

                object objExecuteQuery = null;
                strSQL = new StringBuilder();
                strSQL.Append(string.Format("SELECT FUNC_ID FROM FUNCTION_LIST WHERE FUNCTION_NAME= {0}", "~FUNCTION_NAME~"));
                dictParams.Clear();
                dictParams.Add("FUNCTION_NAME", p_sUserTableName);
                objExecuteQuery = DbFactory.ExecuteScalar(sSecConnString,strSQL.ToString(), dictParams);

                strSQL = new StringBuilder();
                strSQL.Append(string.Format("DELETE FROM FUNCTION_LIST WHERE FUNC_ID ={0}", "~FUNC_ID~"));
                dictParams.Clear();
                dictParams.Add("FUNC_ID", Conversion.ConvertObjToStr(objExecuteQuery));
                objCmd.CommandText = strSQL.ToString();
                DbFactory.ExecuteNonQuery(sSecConnString,strSQL.ToString(), dictParams);

                strSQL = new StringBuilder();
                strSQL.Append(string.Format("DELETE FROM GROUP_PERMISSIONS WHERE FUNC_ID ={0}", "~FUNC_ID~"));
                dictParams.Clear();
                dictParams.Add("FUNC_ID", Conversion.ConvertObjToStr(objExecuteQuery));
                objCmd.CommandText = strSQL.ToString();
                DbFactory.ExecuteNonQuery(m_sConnStr, strSQL.ToString(), dictParams);
                //sSQL = "DELETE FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" +
                //    p_sSysTableName + "'";
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();
                //END PenTesting - srajindersin - 9th Jan 2012

                //commit
                
            }
            catch (DataModelException p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TableManager.DeleteTable.DataErr", m_iClientId), p_objEx);
            }
            finally
            {
                objCmd = null;
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }

                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objTrans != null)
                {
                    objTrans.Dispose();
                }
            }
        }
    }


    /// <summary>
    ///Author  :   Pankaj Chowdhury
    ///Dated   :   09,Jul 2005
    ///Purpose :   Field List
    /// </summary>
    public class FieldList : UtilitiesUIListBase
    {
        /// <summary>
        /// Private database field mapping string array
        /// </summary>
        private string[,] arrFields = {
			{"RowId","FIELD_ID"},
			{"SeqNo","SEQ_NUM"},
			{"SysFieldName","SYS_FIELD_NAME"},
			{"UserPrompt","USER_PROMPT"},                                        
			{"FieldType","FIELD_TYPE"},
			{"Size","FIELD_SIZE"},
			{"Required","REQUIRED_FLAG"},
            {"FASReportable","FAS_REPORTABLE"},//asharma326 MITS 32386
			{"LookUp","LOOKUP_FLAG"},
			{"Delete","DELETE_FLAG"},
			{"IsPattern","IS_PATTERNED"},
			{"Pattern","PATTERN"},
			{"CodeFileId","CODE_FILE_ID"},
			//{"NetVisible","NETVISIBLE_FLAG"},
			{"GrpAssoFlag","GRP_ASSOC_FLAG"},
			{"AssoField","ASSOC_FIELD"}
									  };

        private string m_sUserName = "";    //gagnihotri MITS 11995 Changes made for Audit table
        private int m_iClientId = 0;
        /// Name		: Holidays
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/28/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default Constructor with Connection String
        /// </summary>
        /// <param name="p_sConnString">Input Connection String parameter</param>
        public FieldList(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            this.Initialize();
            m_iClientId = p_iClientId;
        }

        //gagnihotri MITS 11995 Changes made for Audit table
        public FieldList(string p_sConnString, string p_sUserName, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_sUserName = p_sUserName;
            this.Initialize();
            m_iClientId = p_iClientId;
        }

        /// Name		: Initialize
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/28/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            base.TableName = "SUPP_DICTIONARY";
            base.KeyField = "FIELD_ID";
            base.RadioName = "FieldList";
            base.WhereClause = " FIELD_TYPE <> 7 AND FIELD_TYPE <> 20 ";
            base.OrderByClause = " SEQ_NUM, FIELD_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        /// Name		: Get
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/12/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the holidays list
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            string sSysTableName = "";
            string sTableType = "";
            string sAssocs = "";
            string sGAField = "";
            string sSQL = "";
            string sTmp = "";
            int iSeq = 0;
            int iCodeId = 0;
            //bool bFirstOne=false;
            LocalCache objCache = null;
            DbReader objRdr = null;
            try
            {
                XMLDoc = p_objXmlDocument;
                objNode = p_objXmlDocument.SelectSingleNode("//TableType");
                if (objNode != null)
                    sTableType = objNode.InnerText;
                objNode = p_objXmlDocument.SelectSingleNode("//SysTableName");
                if (objNode != null)
                    sSysTableName = objNode.InnerText;
                //Changed by bsharma33 for MITS 26917
                //base.WhereClause = base.WhereClause + " AND SUPP_TABLE_NAME = '" + sSysTableName + "'";
                base.WhereClause = base.WhereClause + string.Format(" AND SUPP_TABLE_NAME = {0}","~sSysTableName~");
                base.dictParams.Add("sSysTableName", sSysTableName);
                base.Get();
                base.dictParams.Clear();
                //End Changes by bsharma33 for MITS 26917
                objNodLst = p_objXmlDocument.SelectNodes("//listrow//DisplaySeq");

                foreach (XmlNode objNod in objNodLst)
                {
                    iSeq++;
                    objNod.InnerText = iSeq.ToString();
                }

                objCache = new LocalCache(ConnectString, m_iClientId);
                objNodLst = p_objXmlDocument.SelectNodes("//listrow//FieldType");
                foreach (XmlNode objNod in objNodLst)
                {
                    ((XmlElement)objNod).SetAttribute("value", objNod.InnerText);
                    if (objNod.InnerText == "6" || objNod.InnerText == "8")
                        objNod.InnerText = TypeToName(objNod.InnerText) + " =>> (" + objCache.GetTableName(Conversion.ConvertStrToInteger(objNod.NextSibling.InnerText)) + ")";
                    else
                        objNod.InnerText = TypeToName(objNod.InnerText);
                }

                objNodLst = p_objXmlDocument.SelectNodes("//listrow//Size");
                foreach (XmlNode objNod in objNodLst)
                {
                    if (((XmlElement)objNod.PreviousSibling.PreviousSibling).GetAttribute("value") != "0")
                        objNod.InnerText = "N/A";
                }

                if (sTableType == "6" || sTableType == "9")
                {
                    objNodLst = p_objXmlDocument.SelectNodes("//listrow//GrpAssoFlag");
                    foreach (XmlNode objNod in objNodLst)
                    {
                        if (objNod.InnerText == "-1")
                        {
                            sAssocs = "";
                            sGAField = objNod.NextSibling.InnerText;
                            //MITS 15937 : Umesh
                            //We should use name of element instead of using firstchild, sibling etc as position of element may get changed. 
                            sSQL = "SELECT * FROM SUPP_ASSOC WHERE FIELD_ID = " + objNod.ParentNode.SelectSingleNode("RowId").InnerText;
                            objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                            while (objRdr.Read())
                            {
                                iCodeId = objRdr.GetInt32("ASSOC_CODE_ID");
                                if (sTableType == "9")
                                    sTmp = objCache.GetStateCode(iCodeId);
                                else
                                {
                                    string sFirst = "";
                                    string sLast = "";
                                    int iEid, iState, iStateId, iStateRowId;
                                    iEid = sGAField.Length > 4 ? 4 : sGAField.Length;
                                    iState = sGAField.Length > 6 ? 6 : sGAField.Length;
                                    iStateId = sGAField.Length > 8 ? 8 : sGAField.Length;
                                    iStateRowId = sGAField.Length > 12 ? 12 : sGAField.Length;
                                    //if (sGAField.Substring(sGAField.Length - iEid, iEid) == "_EID")
                                    if (sGAField.IndexOf("_EID") != -1)
                                    {
                                        objCache.GetEntityInfo(iCodeId, ref sFirst, ref sLast);
                                        sTmp = sLast + " " + sFirst;
                                    }
                                    else if ((sGAField.Substring(sGAField.Length - iState, iState) == "_STATE") ||
                                        (sGAField.Substring(sGAField.Length - iStateId, iStateId) == "STATE_ID") ||
                                        (sGAField.Substring(sGAField.Length - iStateRowId, iStateRowId) == "STATE_ROW_ID"))
                                    {
                                        sTmp = UTILITY.GetState(iCodeId.ToString(), ConnectString, m_iClientId);
                                    }
                                    else
                                    {
                                        //25/12/2007 Abhishek, MITS 11097 - START
                                        if (sSysTableName == "ENTITY_SUPP")
                                        {
                                            //JIRA RMA-4690: neha goel:05202014 added if else so that PI_SUPP works both for Entity type table and Code tables MITS # 35595
                                            if (sGAField.Contains("ENTITY.ENTITY_TABLE_ID"))
                                            {
                                                objCache.GetCodeInfoForEntity(iCodeId, ref sFirst, ref sLast);
                                            }
                                            else
                                            {
                                                objCache.GetCodeInfo(iCodeId, ref sFirst, ref sLast);
                                            }
                                        }
                                        //JIRA RMA-4690:change here by swati for WWIG group association issue MITS # 35595 added PI_SUPP condition
                                        else if (string.Compare(sSysTableName, "PI_SUPP", true) == 0)
                                        {
                                            //JIRA RMA-4690:neha goel:05202014 added if else so that PI_SUPP works both for Entity and Code tables MITS # 35595
                                            if (sGAField.Contains("ENTITY.ENTITY_TABLE_ID"))
                                            {
                                                objCache.GetCodeInfoForEntity(iCodeId, ref sFirst, ref sLast);
                                            }
                                            else
                                            {
                                                objCache.GetCodeInfo(iCodeId, ref sFirst, ref sLast);
                                            }
                                        }//JIRA RMA-4690:END
                                        else
                                        {
                                            objCache.GetCodeInfo(iCodeId, ref sFirst, ref sLast);
                                        } //25/12/2007 Abhishek, MITS 11097 - END
                                        sTmp = sFirst + " " + sLast;
                                    }
                                }
                                if (sAssocs == "")
                                    sAssocs = sTmp;
                                else
                                {
                                    if (sTableType == "6")
                                        sAssocs = sAssocs + "," + sTmp;
                                    else
                                        sAssocs = sAssocs + "-" + sTmp;
                                }
                            }
                            objRdr.Close();
                            if (sTableType == "6")
                                sAssocs = sGAField + "=" + sAssocs;
                            objNod.NextSibling.NextSibling.InnerText = sAssocs;
                        }
                    }
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FieldList.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
                objNodLst = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
            }
        }

        public XmlDocument SwapField(XmlDocument p_objXmlDoc)
        {
            XmlNode objNode = null;
            int iFieldId1 = 0;
            int iSeqNo1 = 0;
            int iFieldId2 = 0;
            int iSeqNo2 = 0;
            DbCommand objCmd = null;
            DbConnection objCon = null;
            DbTransaction objTrans = null;
            try
            {
                objCon = DbFactory.GetDbConnection(ConnectString);
                objCon.Open();
                objCmd = objCon.CreateCommand();
                objTrans = objCon.BeginTransaction();
                objCmd.Transaction = objTrans;

                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Field1']");
                if (objNode != null)
                {
                    iFieldId1 = Conversion.ConvertStrToInteger(objNode.InnerText);
                }

                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='SeqNo1']");
                if (objNode != null)
                {
                    iSeqNo1 = Conversion.ConvertStrToInteger(objNode.InnerText);
                }

                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Field2']");
                if (objNode != null)
                {
                    iFieldId2 = Conversion.ConvertStrToInteger(objNode.InnerText);
                }

                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='SeqNo2']");
                if (objNode != null)
                {
                    iSeqNo2 = Conversion.ConvertStrToInteger(objNode.InnerText);
                }
                //gagnihotri MITS 11995 Changes made for Audit table
                objCmd.CommandText = "UPDATE SUPP_DICTIONARY SET SEQ_NUM = " + iSeqNo2 +
                    ", UPDATED_BY_USER = '" + m_sUserName +
                    "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) +
                    " WHERE FIELD_ID = " + iFieldId1;
                objCmd.ExecuteNonQuery();
                //gagnihotri MITS 11995 Changes made for Audit table
                objCmd.CommandText = "UPDATE SUPP_DICTIONARY SET SEQ_NUM = " + iSeqNo1 +
                    ", UPDATED_BY_USER = '" + m_sUserName +
                    "', DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(DateTime.Now) +
                    " WHERE FIELD_ID = " + iFieldId2;
                objCmd.ExecuteNonQuery();

                objCmd.Transaction.Commit();
                //Added by Shivendu for MITS 11484
                Riskmaster.DataModel.SupplementalObject.ClearTableDefinitionCache();
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("FieldList.SwapField.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
                if (objTrans != null)
                {
                    objTrans.Dispose();
                }
                if (objCon != null)
                {
                    objCon.Dispose();
                }
            }
        }

        internal static string TypeToName(string p_sType)
        {
            switch (p_sType)
            {
                case "0":
                    return "String";
                case "1":
                    return "Number";
                case "2":
                    return "Currency";
                case "3":
                    return "Date";
                case "4":
                    return "Time";
                case "5":
                    return "Multi Text/Codes";
                case "6":
                    return "Code";
                case "7":
                    return "Primary Record Index";
                case "8":
                    return "Entity";
                case "9":
                    return "State";
                case "10":
                    return "Claim Number Lookup";
                case "11":
                    return "Free Text";
                case "12":
                    return "Event Number Lookup";
                case "13":
                    return "Vehicle Lookup";
                case "14":
                    return "Multi-Code";
                case "15":
                    return "Multi-State";
                case "16":
                    return "Multi-Entity";
                //Added by Shivendu for Supplemental Grid
                case "17":
                    return "Grid";
                //Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                case "18":
                    //Rename the name of Policy Number Lookup to Policy Tracking Lookup,05/05/2010,MITS 20618
                    //return "Policy Number Lookup";
                    return "Policy Tracking Lookup";
                case "19":
                    //Rename the name of Policy Management Number Lookup to Policy Management Lookup,05/05/2010,MITS 20618
                    //return "Policy Management Number Lookup";
                    return "Policy Management Lookup";
                case "25":
                    return "HTML Text";//asharma326 JIRA 6422
                //End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,,MITS 20373
                case "31"://MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 
                    return "checkbox";
                //sachin sharishkumar Jira 6415
                case "22":
                    return "User Lookup";
                //sharishkumar Jira 6415 ends
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                case "23":
                    return "Hyperlink";
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                default:
                    return "<unknown>";
            }
        }

    }


    /// <summary>
    ///Author  :   Pankaj Chowdhury
    ///Dated   :   09,Jul 2005
    ///Purpose :   Index List
    /// </summary>
    public class IndexList
    {
        /// <summary>
        /// Connection String variable
        /// </summary>
        string m_sConnStr = "";
        private int m_iClientId = 0;//psharma206
        /// Name		: Indexlist
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/12/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default Constructor with Connection String
        /// </summary>
        /// <param name="p_sConnString">Input Connection String parameter</param>
        public IndexList(string p_sConnString, int p_iClientId)//psharma206
        {
            m_sConnStr = p_sConnString;
            m_iClientId = p_iClientId;
        }


        /// Name		: GetIndexList
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/12/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the holidays list
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument GetIndexList(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            string sTableName = "";
            DataSet objDsIndex = null;
            DataSet objDsIndexCol = null;
            XmlElement objNewLstRow = null;
            XmlElement objNewCol = null;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode("//SysTableName");
                sTableName = objNode.InnerText;
                objDsIndex = GetIndexes(sTableName);

                objNode = p_objXmlDocument.SelectSingleNode("//IndexList");
                if (objDsIndex.Tables.Count > 0)
                {
                    foreach (DataRow objRow in objDsIndex.Tables[0].Rows)
                    {
                        //Old code when index detail was found using sql statement and not with stored proc
                        //					sIndexFields="";
                        //                    objNewLstRow = p_objXmlDocument.CreateElement("listrow");
                        //                    objNode.AppendChild(objNewLstRow);  //Listrow created
                        //					
                        //					//Add rowid name
                        //					objNewCol=p_objXmlDocument.CreateElement("RowId");
                        //					//objNewCol.InnerText= objRow["TABLE_ID"].ToString()+ "|" + objRow["INDEX_ID"].ToString();
                        //					objNewLstRow.AppendChild(objNewCol);
                        //
                        //					//Add index name
                        //					objNewCol=p_objXmlDocument.CreateElement("IndexName");
                        //					objNewCol.InnerText= objRow["INDEX_NAME"].ToString();
                        //					objNewLstRow.AppendChild(objNewCol);
                        //
                        //					objDsIndexCol= GetIndexCols(sTableName,objRow["INDEX_NAME"].ToString());
                        //					foreach(DataRow oRow in objDsIndexCol.Tables[0].Rows)
                        //					{
                        //						if(sIndexFields=="")
                        //							sIndexFields= oRow["COLUMN_NAME"].ToString();
                        //						else
                        //							sIndexFields+= "," +  oRow["COLUMN_NAME"].ToString(); 
                        //					}
                        //
                        //					//Add field list
                        //					objNewCol=p_objXmlDocument.CreateElement("IndexField");
                        //					objNewCol.InnerText= sIndexFields;
                        //					objNewLstRow.AppendChild(objNewCol);	
                        //				
                        //					//Add Unique
                        //					objNewCol=p_objXmlDocument.CreateElement("Unique");
                        //					objNewLstRow.AppendChild(objNewCol);
                        //					int iLen = 0;
                        //					if(objNewCol.PreviousSibling.PreviousSibling.InnerText.Length>3)
                        //						iLen=3;
                        //					else
                        //						iLen=objNewCol.PreviousSibling.PreviousSibling.InnerText.Length;
                        //					if(objNewCol.PreviousSibling.PreviousSibling.InnerText.Substring( 
                        //						objNewCol.PreviousSibling.PreviousSibling.InnerText.Length-iLen,iLen)=="_PK")
                        //						objNewCol.InnerText="Yes";
                        //					else
                        //						objNewCol.InnerText="No";


                        //Get index details using stored proc

                        objNewLstRow = p_objXmlDocument.CreateElement("listrow");
                        objNode.AppendChild(objNewLstRow);  //Listrow created

                        //Add rowid name
                        objNewCol = p_objXmlDocument.CreateElement("RowId");
                        objNewCol.InnerText = objRow["INDEX_NAME"].ToString();
                        objNewLstRow.AppendChild(objNewCol);

                        //Add index name
                        objNewCol = p_objXmlDocument.CreateElement("IndexName");
                        objNewCol.InnerText = objRow["INDEX_NAME"].ToString();
                        objNewLstRow.AppendChild(objNewCol);

                        //Add field list
                        objNewCol = p_objXmlDocument.CreateElement("IndexField");
                        objNewCol.InnerText = objRow["INDEX_KEYS"].ToString();
                        objNewLstRow.AppendChild(objNewCol);

                        //Add Unique
                        objNewCol = p_objXmlDocument.CreateElement("Unique");
                        objNewLstRow.AppendChild(objNewCol);
                        if (objRow["INDEX_DESCRIPTION"].ToString().ToUpper().IndexOf("UNIQUE") > 0)
                            objNewCol.InnerText = "Yes";
                        else
                            objNewCol.InnerText = "No";

                    }
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IndexList.GetIndexList.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
                objNewLstRow = null;
                objNewCol = null;
                if (objDsIndex != null)
                {
                    objDsIndex.Dispose();
                    objDsIndex = null;
                }
                if (objDsIndexCol != null)
                {
                    objDsIndexCol.Dispose();
                    objDsIndexCol = null;
                }
            }
        }

        /// Name		: CreateIndex
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/12/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the holidays list
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument CreateIndex(XmlDocument p_objXmlDocument)
        {
            string sTableName = "";
            string sIndexName = "";
            string sIndexFields = "";
            XmlNode objNode = null;
            DbConnection objCon = null;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SysTableName']");
                sTableName = objNode.InnerText;

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='IndexName']");
                sIndexName = objNode.InnerText;
                if (sIndexName.Length > 18)
                    sIndexName = sIndexName.Substring(0, 18);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SelectedFieldList']");
                sIndexFields = objNode.InnerText;

                objCon = DbFactory.GetDbConnection(m_sConnStr);
                objCon.Open();

                objCon.ExecuteNonQuery("CREATE INDEX " + sIndexName + " ON " + sTableName + " (" + sIndexFields + ")");

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IndexList.CreateIndex.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
                if (objCon != null)
                    objCon.Dispose();
            }
        }


        /// Name		: GetColumnList
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/12/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the holidays list
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument GetAddIndex(XmlDocument p_objXmlDocument)
        {
            string sTableName = "";
            XmlNode objNode = null;
            DataSet objDs = null;
            XmlElement objNew = null;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SysTableName']");
                sTableName = objNode.InnerText;

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AvlFieldsList']");

                objDs = DbFactory.GetDataSet(m_sConnStr, "SELECT * FROM " + sTableName + " WHERE 1=2", m_iClientId);
                foreach (DataColumn objCol in objDs.Tables[0].Columns)
                {
                    objNew = p_objXmlDocument.CreateElement("option");
                    objNew.SetAttribute("value", objCol.ColumnName);
                    objNew.InnerText = objCol.ColumnName;
                    objNode.AppendChild(objNew);
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IndexList.CreateIndex.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDs != null)
                {
                    objDs.Dispose();
                    objDs = null;
                }
                objNew = null;
            }
        }

        private DataSet GetIndexes(string p_sTableName)
        {
            DbConnection objConn = null;
            //Added by bsharma33 for Regions Pen Testing MITS 26942.
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            //End changes by bsharma33 for Regions Pen Testing MITS 26942.
            string sSQL = "";
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();
                switch (objConn.DatabaseType)
                {
                    case eDatabaseType.DBMS_IS_ACCESS:
                        break;
                    case eDatabaseType.DBMS_IS_INFORMIX:
                        break;
                    //					case eDatabaseType.DBMS_IS_SQLSRVR:
                    //						sSQL = "SELECT SYSINDEXES.ID TABLE_ID, SYSINDEXES.INDID INDEX_ID, " + 
                    //							"SYSINDEXES.NAME INDEX_NAME FROM SYSINDEXES,SYSOBJECTS WHERE " + 
                    //							"SYSINDEXES.ID=SYSOBJECTS.ID AND SYSOBJECTS.NAME='" + p_sTableName + "' AND " +
                    //							"SYSINDEXES.KEYS IS NOT NULL";
                    //						break;
                    //Index can be got directly from the stored proc
                    case eDatabaseType.DBMS_IS_SQLSRVR:
                        sSQL = "SP_HELPINDEX " + p_sTableName;
                        break;
                    case eDatabaseType.DBMS_IS_SYBASE:
                        break;
                    case eDatabaseType.DBMS_IS_ORACLE:
                        //Changed by bsharma33 for Regions Pen Testing MITS 26942.
                        //sSQL = "SELECT INDEX_NAME,UNIQUENESS FROM USER_INDEXES WHERE TABLE_NAME='" + p_sTableName + "'";
                        //DataSet objDsIndxNam = DbFactory.GetDataSet(m_sConnStr, sSQL);
                        sSQL = string.Format("SELECT INDEX_NAME,UNIQUENESS FROM USER_INDEXES WHERE TABLE_NAME={0}", "~p_sTableName~");
                        dictParams.Add("p_sTableName", p_sTableName);
                        DataSet objDsIndxNam = DbFactory.ExecuteDataSet(m_sConnStr, sSQL, dictParams, m_iClientId);
                        dictParams.Clear();
                        //End changes by bsharma33 for Regions Pen Testing MITS 26942.
                        DataSet objDsIndxCol = null;

                        // Create manual DS to return data in
                        // old - didn't work    DataSet objRetDs= DbFactory.GetDataSet(m_sConnStr,"SELECT INDEX_KEYS INDEX_NAME,INDEX_KEYS INDEX_DESCRIPTION,INDEX_KEYS FROM USER_IND_COLUMNS WHERE 1=2");
                        DataSet objRetDs = new DataSet();
                        DataTable oDT = new DataTable("USER_IND_COLUMNS");
                        DataColumn oDC = new DataColumn("INDEX_NAME", System.Type.GetType("System.String"));
                        oDT.Columns.Add(oDC);
                        oDC = new DataColumn("INDEX_DESCRIPTION", System.Type.GetType("System.String"));
                        oDT.Columns.Add(oDC);
                        oDC = new DataColumn("INDEX_KEYS", System.Type.GetType("System.String"));
                        oDT.Columns.Add(oDC);
                        objRetDs.Tables.Add(oDT);

                        foreach (DataRow objRow in objDsIndxNam.Tables[0].Rows)
                        {
                            DataRow objNewR = objRetDs.Tables[0].NewRow();
                            //Changed by bsharma33 for Regions Pen Testing MITS 26942.
                            //sSQL = "SELECT INDEX_KEYS INDEX_NAME,INDEX_KEYS INDEX_DESCRIPTION,INDEX_KEYS FROM USER_IND_COLUMNS WHERE INDEX_NAME='" + objDsIndxNam.Tables[0].Columns["INDEX_NAME"].ToString() + "'";
                            //sSQL = "SELECT COLUMN_NAME FROM USER_IND_COLUMNS WHERE INDEX_NAME='" + objRow["INDEX_NAME"].ToString() + "'";
                            sSQL = string.Format("SELECT COLUMN_NAME FROM USER_IND_COLUMNS WHERE INDEX_NAME={0}", "~INDEX_NAME~");
                            dictParams.Add("INDEX_NAME", objRow["INDEX_NAME"].ToString());
                            //End changes by bsharma33 for Regions Pen Testing MITS 26942.
                            string sKeys = "";
                            //objDsIndxCol = DbFactory.GetDataSet(m_sConnStr, sSQL);
                            objDsIndxCol = DbFactory.ExecuteDataSet(m_sConnStr, sSQL, dictParams, m_iClientId);
                            dictParams.Clear();
                            foreach (DataRow oR in objDsIndxCol.Tables[0].Rows)
                            {
                                if (sKeys == "")
                                    sKeys = oR["COLUMN_NAME"].ToString();
                                else
                                    sKeys = sKeys + "," + oR["COLUMN_NAME"].ToString();
                            }

                            objNewR["INDEX_NAME"] = objRow["INDEX_NAME"].ToString();
                            if (objRow["UNIQUENESS"].ToString().IndexOf("NONUNIQUE") > 0)
                                objNewR["INDEX_DESCRIPTION"] = "";
                            else
                                objNewR["INDEX_DESCRIPTION"] = objRow["UNIQUENESS"].ToString();
                            objNewR["INDEX_KEYS"] = sKeys;
                            objRetDs.Tables[0].Rows.Add(objNewR);
                        }
                        if (objDsIndxCol != null)
                        {
                            objDsIndxCol.Dispose();
                        }
                        if (objDsIndxNam != null)
                        {
                            objDsIndxNam.Dispose();
                        }
                        return objRetDs;
                    case eDatabaseType.DBMS_IS_DB2:
                        break;
                }
                return DbFactory.GetDataSet(m_sConnStr, sSQL, m_iClientId);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }


        private DataSet GetIndexCols(string p_sTableName, string p_sIndexName)
        {
            DbConnection objConn = null;
            string sSQL = "";
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();
                switch (objConn.DatabaseType)
                {
                    case eDatabaseType.DBMS_IS_ACCESS:
                        break;
                    case eDatabaseType.DBMS_IS_INFORMIX:
                        break;
                    case eDatabaseType.DBMS_IS_SQLSRVR:
                        sSQL = "SELECT SYSINDEXES.ID TABLE_ID, SYSINDEXES.INDID INDEX_ID ," +
                            "SYSINDEXKEYS.COLID COLUMN_ID,SYSCOLUMNS.NAME COLUMN_NAME " +
                            "FROM SYSINDEXKEYS,SYSCOLUMNS,SYSOBJECTS,SYSINDEXES WHERE " +
                            "SYSINDEXKEYS.INDID=SYSINDEXES.INDID AND SYSINDEXES.NAME='" +
                            p_sIndexName + "' AND SYSINDEXKEYS.ID=SYSOBJECTS.ID AND " +
                            "SYSINDEXKEYS.COLID=SYSCOLUMNS.COLID AND SYSCOLUMNS.ID=SYSOBJECTS.ID " +
                            "AND SYSOBJECTS.NAME='" + p_sTableName + "'";
                        break;
                    case eDatabaseType.DBMS_IS_SYBASE:
                        break;
                    case eDatabaseType.DBMS_IS_ORACLE:
                        break;
                    case eDatabaseType.DBMS_IS_DB2:
                        break;
                }
                return DbFactory.GetDataSet(m_sConnStr, sSQL, m_iClientId);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }
    }


    /// <summary>
    ///Author  :   Pankaj Chowdhury
    ///Dated   :   10th ,Jul 2005
    ///Purpose :   Datafield Form
    /// </summary>
    public class TableField : UtilitiesUIBase
    {
        /// <summary>
        /// Database fields mapping string array
        /// </summary>
        private string[,] arrFields = {
			{"RowId", "FIELD_ID"},
			{"SeqNo", "SEQ_NUM"},
			{"SuppTblName", "SUPP_TABLE_NAME"},
			{"SysFieldName", "SYS_FIELD_NAME"},
			{"FieldType", "FIELD_TYPE"},
			{"FieldSize", "FIELD_SIZE"},
			{"RequiredFlag", "REQUIRED_FLAG"},
            {"FASReportableflag","FAS_REPORTABLE"},//asharma326 MITS 32386
			{"DeleteFlag", "DELETE_FLAG"},
			{"LookupFlag", "LOOKUP_FLAG"},
			{"IsPattern", "IS_PATTERNED"},
			{"Pattern", "PATTERN"},
			{"CodeFileId", "CODE_FILE_ID"},
			{"GrpAssoFlag", "GRP_ASSOC_FLAG"},
			{"AssoField", "ASSOC_FIELD"},
			{"HelpContext", "HELP_CONTEXT_ID"},
			{"NetVisibleFlag", "NETVISIBLE_FLAG"},
			{"UserPrompt", "USER_PROMPT"},
            {"MultiSelect", "MULTISELECT"},
            {"chkhtmltextconfig", "HTMLTXTCNFG"}
									  };

        private string m_sUserName = "";    //gagnihotri MITS 11995 Changes made for Audit table

        private int m_iClientId = 0;
        //Added By Abhishek For supplemental upgrade
        #region Private variables
        private string m_netViewFormsConnString = "";
        private string s_DataSourceId = "329";
        private string m_sUserId = "";
        private string m_sPassword = "";
        private string m_sDSNName = "";
        private string m_sDSNId = "";
        private string m_sSecurityDSN = "";
        private string m_sConnStr = "";
        #endregion
        //		/// <summary>
        //		/// String array for combo values
        //		/// </summary>
        //		private string[,] arrChilds = {
        //			{"OrgHighLevel", "TABLE_ID", "SYSTEM_TABLE_NAME", "GLOSSARY" , "TABLE_ID BETWEEN 1005 AND 1012 OR TABLE_ID=0"},
        //			{"OrgLowLevel",  "TABLE_ID", "SYSTEM_TABLE_NAME", "GLOSSARY" , "TABLE_ID BETWEEN 1005 AND 1012 OR TABLE_ID=0"}
        //									  };

        /// Name		: TableField
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/10/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default constructor with connection string
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>
        public TableField(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

        //gagnihotri MITS 11995 Changes made for Audit table
        public TableField(string p_sConnString, string p_sUserName, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

        /// <summary>
        /// Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserId">User ID</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDSNName">DSN Name</param>
        /// <param name="p_sDSNId">DSN Id</param>
        /// <param name="p_sConnString">Connection String</param>
        public TableField(string p_sUserId, string p_sPassword, string p_sDSNName, string p_sDSNId, string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            m_iClientId = p_iClientId;
            ConnectString = p_sConnString;
            m_sConnStr = p_sConnString;
            m_sUserName = p_sUserId;
            m_sDSNName = p_sDSNName;
            m_sUserId = p_sUserId;
            m_sPassword = p_sPassword;
            m_sDSNId = p_sDSNId;
            m_sSecurityDSN = SecurityDatabase.GetSecurityDsn(m_iClientId);
            //Get the connection string from the currently accessible configuration file
            m_netViewFormsConnString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId);
           
            this.Initialize();
        }

        /// Name		: Initialize
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/12/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            TableName = "SUPP_DICTIONARY";
            KeyField = "FIELD_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }


        /// Name		: Get
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/10/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>

//Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
       // public XmlDocument Get(XmlDocument p_objXmlDocument)
        public XmlDocument Get(XmlDocument p_objXmlDocument, UserLogin p_userLogin)
//End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
        {
            XmlNode objNod = null;
            XmlElement objNewElm = null;
            XmlNode objNodCodeLst = null;
            XmlNode objListType = null;
            XmlNode objTemp = null;
            DbReader objRdr = null;
            LocalCache objCache = null;
            string sTableType = "";
            string sSQL = "";
            string sSQL1 = "";
            string sRowId = "";
            //zalam 07/25/2008 Mits:-12959 Start
            string sSourceField = "";
            string sSourceFieldTemp = "";
            XmlNodeList objNodeList = null;
            //zalam 07/25/2008 Mits:-12959 End
            //MGaba2:R7:Prompt on Deletion if History Tracking ON:Start
            string sSysFieldName = string.Empty;
            string sSysTableName = string.Empty;            
            SysSettings objSysSettings = null;
            //MGaba2:R7:Prompt on Deletion if History Tracking ON:End

//Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
            XmlNode objEnhNode = null;
            ColLobSettings oLobSettings = null;
            LocalCache m_objLocalCache = null;

            bool bIsPolEnhALAllowed = false;
            bool bIsPolEnhPCAllowed = false;
            bool bIsPolEnhGLAllowed = false;
            bool bIsPolEnhWCAllowed = false;
            bool bIsPolTrackAllowed = false;
            XmlNode objTrackNode = null;
//End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010

            // npadhy - JIRA 6415 - Check if the entered value correspond to user or group
            int iUserOrGroup = 0;
            try
            {
                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='TableType']");
                if (objNod != null)
                    sTableType = objNod.InnerText;

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objNod != null)
                    sRowId = objNod.InnerText;
				//rkaur27 : RMA-18764 - Start
                if((!string.IsNullOrEmpty(sRowId) && sRowId.Trim()!="0"))
                {
                    sSQL = "SELECT DTTM_RCD_LAST_UPD FROM SUPP_DICTIONARY WHERE FIELD_ID =" + sRowId;
                    objRdr = DbFactory.GetDbReader(ConnectString, sSQL);

                    while (objRdr.Read())
                    {
                        p_objXmlDocument.SelectSingleNode("//internal[@name='DttmLastUpdtd']").InnerText = objRdr.GetValue(0).ToString();
                    }
                }
				//rkaur27 : RMA-18764 - End
                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']");
                if (objNod != null)
                {
                    objListType = p_objXmlDocument.SelectSingleNode("//control[@name='ListType']");
                    if (objListType.InnerText != "")
                    {
                        //zalam 07/25/2008 Mits:-12959 Start
                        sSourceField = p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']/@value").InnerText;
                        objNodeList = p_objXmlDocument.SelectNodes("//control[@name='SourceField']//option");
                        foreach (XmlNode objNode in objNodeList)
                        {
                            sSourceFieldTemp = ((XmlElement)objNode).GetAttribute("value");
                            if (sSourceFieldTemp == sSourceField)
                            {
                                if (sSourceField.IndexOf("'") >= 0)
                                {
                                    objListType.InnerText = objListType.InnerText.Replace("'", "`");
                                    ((XmlElement)objNode).SetAttribute("value", objListType.InnerText);
                                    ((XmlElement)objNod).SetAttribute("value", objListType.InnerText);
                                }
                                break;
                            }
                        }
                        //zalam 07/25/2008 Mits:-12959 End

                        p_objXmlDocument.SelectSingleNode("//control[@name='AssoField']").InnerText = ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']//option[@value='" + objListType.InnerText + "']")).GetAttribute("fieldname");

                        //zalam 07/25/2008 Mits:-12959 Start
                        if (objListType.InnerText.IndexOf("`") >= 0)
                        {
                            XmlNode objNodeOption = p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']//option[@value='" + objListType.InnerText + "']");
                            objListType.InnerText = objListType.InnerText.Replace("`", "'");
                            ((XmlElement)objNodeOption).SetAttribute("value", objListType.InnerText);
                            ((XmlElement)objNod).SetAttribute("value", objListType.InnerText);
                        }
                        //zalam 07/25/2008 Mits:-12959 End

                        objNodCodeLst = p_objXmlDocument.SelectSingleNode("//control[@name='GAList']");
                        SourceFieldChanged(p_objXmlDocument);
                        objNodCodeLst.InnerText = "";
                        //((XmlElement)objNodCodeLst).SetAttribute("codetable",((XmlElement)objNod).GetAttribute("value")); 
                        objListType.InnerText = "";
                        objNod = p_objXmlDocument.SelectSingleNode("//control[@name='FieldType']");
                        if (objNod != null)
                        {
                            if (((XmlElement)objNod).GetAttribute("value") == "")
                            {

                                ((XmlElement)objNod).SetAttribute("value", p_objXmlDocument.SelectSingleNode("//control[@name='hdnFieldType']").InnerText);
                                objTemp = p_objXmlDocument.SelectSingleNode("//form");
                                ((XmlElement)objTemp).Attributes["title"].Value = "Modify Field";
                                objTemp = p_objXmlDocument.SelectSingleNode("//group");
                                ((XmlElement)objTemp).Attributes["title"].Value = "Add/Edit Field";
                            }
                        }
                        return p_objXmlDocument;
                    }
                    //Type must be for supplemental
                    if (sTableType == "6")
                    {
                        if (p_objXmlDocument.SelectSingleNode("//control[@name='ControlChanged']").InnerText != "FIELDTYPE")
                        {
                            FillListWithGAFields(p_objXmlDocument);
                            if (sRowId == "")//Return if this is add mode
                            {
                                p_objXmlDocument.SelectSingleNode("//control[@name='ControlChanged']").InnerText = "";
                                return p_objXmlDocument;
                            }
                        }
                    }
                }

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='FieldType']");
                if (objNod != null)
                {
                    if (((XmlElement)objNod).GetAttribute("value") != "")
                    {
                        if (objNod != null)
                        {
                            if (((XmlElement)objNod).GetAttribute("value") != "")
                            {

                                objTemp = p_objXmlDocument.SelectSingleNode("//control[@name='hdnFieldType']");
                                objTemp.InnerText = ((XmlElement)objNod).GetAttribute("value");
                            }
                        }
                        LoadCodeFile(p_objXmlDocument, ((XmlElement)objNod).GetAttribute("value"));
                        if (p_objXmlDocument.SelectSingleNode("//control[@name='ControlChanged']").InnerText == "FIELDTYPE")
                        {
                            p_objXmlDocument.SelectSingleNode("//control[@name='ControlChanged']").InnerText = "";
                            return p_objXmlDocument;
                        }
                    }
                }
                XMLDoc = p_objXmlDocument;
               
                base.Get();

                //MGaba2:R7 In case hist tracking is ON for that field
                //then an alert needs to be prompted to user in case of deletion                
                objSysSettings = new SysSettings(ConnectString,m_iClientId);            
              
                if (objSysSettings.HistTrackEnable)
                {
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='SysFieldName']");
                    if (objNod != null)
                    {
                        sSysFieldName = objNod.InnerText;
                    }
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='SysTableName']");
                    if (objNod != null)
                    {
                        sSysTableName = objNod.InnerText;
                    }

                    if (sSysFieldName != string.Empty && sSysTableName != string.Empty)
                    {
                        sSQL = string.Format(@"SELECT COUNT(HC.COLUMN_ID) FROM HIST_TRACK_TABLES HT 
                                            INNER JOIN HIST_TRACK_DICTIONARY HD ON HT.TABLE_ID=HD.TABLE_ID 
                                            INNER JOIN HIST_TRACK_COLUMNS HC ON HC.COLUMN_ID=HD.COLUMN_ID 
                                            WHERE HT.TABLE_NAME = '{0}' AND HD.COLUMN_NAME = '{1}'", sSysTableName, sSysFieldName);
                        objNod = p_objXmlDocument.SelectSingleNode("//control[@name='hdnHistoryTracking']");
                        if (objNod != null)
                        {
                            objNod.InnerText = Convert.ToBoolean(DbFactory.ExecuteScalar(ConnectString, sSQL)).ToString();
                        }
                    }
                }
                //MGaba2:R7 End
                
                if (((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FieldType']")).GetAttribute("value") != "")
                {
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='FieldType']");
                    if (objNod != null)
                    {
                        if (((XmlElement)objNod).GetAttribute("value") != "")
                        {
                            if (objNod != null)
                            {
                                if (((XmlElement)objNod).GetAttribute("value") != "")
                                {

                                    objTemp = p_objXmlDocument.SelectSingleNode("//control[@name='hdnFieldType']");
                                    objTemp.InnerText = ((XmlElement)objNod).GetAttribute("value");
									// npadhy - JIRA 6415 Retrieve the selected value of User/Group
                                    // Check if the Field Type is 22 (UserLookup) then retrieve the value of if the user is selected or Group
                                    if (objTemp.InnerText == "22")
                                    {
                                        // Retrieve the value from DB against the field Id
                                       
                                        if (sRowId != string.Empty)
                                        {
                                            sSQL = String.Format("Select USER_GROUP_IND from SUPP_USERGROUP_DICT WHERE FIELD_ID = {0}", sRowId);
                                            object objReturnVal = DbFactory.ExecuteScalar(ConnectString, sSQL);

                                            if (objReturnVal != null)
                                                iUserOrGroup = Conversion.ConvertObjToInt(objReturnVal, m_iClientId);

                                            objTemp = p_objXmlDocument.SelectSingleNode("//control[@name='ddUsersGroups']");
                                            ((XmlElement) objTemp).SetAttribute("value", iUserOrGroup.ToString());
                                        }
                                    }
                                    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                                    if (((XmlElement)objNod).GetAttribute("value") == "23")
                                    {
                                        sSQL = String.Format("SELECT WEB_LINK_ID FROM SUPP_DICTIONARY WHERE FIELD_ID = {0}", sRowId);
                                        objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                                        while (objRdr.Read())
                                        {
                                            ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CodeFileId']")).SetAttribute("value", objRdr.GetInt32(0).ToString());
                                        }
                                    }
                                    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                                }
                            }
                        }
                    }

                    LoadCodeFile(p_objXmlDocument, ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FieldType']")).GetAttribute("value"));
                }

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='FieldType']");
                if (objNod != null)
                {
                    objNod.InnerText = "";
                    //Modified by Shivendu for Supplemental Grid

//Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
                    //for (int iCtr = 0; iCtr <= 17; iCtr++)
                    for (int iCtr = 0; iCtr <= 19; iCtr++)
//End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
                    {
                        objNewElm = p_objXmlDocument.CreateElement("option");
                        objNewElm.SetAttribute("value", iCtr.ToString());
                        objNewElm.InnerText = FieldList.TypeToName(iCtr.ToString());
                        objNod.AppendChild(objNewElm);
                    }
                }

//Start:Added by Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010
                bIsPolTrackAllowed = p_userLogin.IsAllowed(Riskmaster.Application.SecurityManagement.UserPermissions.RMB_POLMGT);
                objTrackNode = p_objXmlDocument.SelectSingleNode("//internal[@name='TrackPolFlag']");
                m_objLocalCache = new LocalCache(base.ConnectString, m_iClientId);
                oLobSettings = new ColLobSettings(base.ConnectString,m_iClientId);
                if (objTrackNode != null)
                {
                    if (bIsPolTrackAllowed == true)
                    {
                        if (!(oLobSettings[m_objLocalCache.GetCodeId("PC", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1 &&
                            oLobSettings[m_objLocalCache.GetCodeId("GC", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1 &&
                            oLobSettings[m_objLocalCache.GetCodeId("WC", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1 &&
                            oLobSettings[m_objLocalCache.GetCodeId("VA", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1))
                        {
                            p_objXmlDocument.SelectSingleNode("//internal[@name='TrackPolFlag']").InnerText = "1";
                        }
                    }
                }
                bIsPolEnhALAllowed = p_userLogin.IsAllowed(Riskmaster.Application.SecurityManagement.UserPermissions.RMB_POLMGTAL_ENH);
                bIsPolEnhPCAllowed = p_userLogin.IsAllowed(Riskmaster.Application.SecurityManagement.UserPermissions.RMB_POLMGTPC_ENH);
                bIsPolEnhGLAllowed = p_userLogin.IsAllowed(Riskmaster.Application.SecurityManagement.UserPermissions.RMB_POLMGTGL_ENH);
                bIsPolEnhWCAllowed = p_userLogin.IsAllowed(Riskmaster.Application.SecurityManagement.UserPermissions.RMB_POLMGTWC_ENH);

                objEnhNode = p_objXmlDocument.SelectSingleNode("//internal[@name='EnhPolFlag']");
                if (objEnhNode !=  null)
                {
                    if (bIsPolEnhALAllowed || bIsPolEnhPCAllowed || bIsPolEnhGLAllowed || bIsPolEnhWCAllowed)
                    {
                        if (oLobSettings[m_objLocalCache.GetCodeId("PC", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1 ||
                            oLobSettings[m_objLocalCache.GetCodeId("GC", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1 ||
                            oLobSettings[m_objLocalCache.GetCodeId("WC", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1 ||
                            oLobSettings[m_objLocalCache.GetCodeId("VA", "LINE_OF_BUSINESS")].UseEnhPolFlag == -1)
                        {
                            p_objXmlDocument.SelectSingleNode("//internal[@name='EnhPolFlag']").InnerText = "1";
                        }
                    }
                }
//End:Nitin Goel:To Enable/Disable policy number lookup on the line of Policy Management/Policy Tracking ,06/10/2010

                if (sTableType == "6" && sRowId != "")
                {
                    //Geeta 03/20/07 : Modified for fixing mits number 8883
                    if (p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']/option") != null)
                    {
                        //zalam 07/25/2008 Mits:-12959 Start
                        //pmittal5 Mits 14458 03/12/09 -Reverting back the change made in 12959
                        //sSourceField = p_objXmlDocument.SelectSingleNode("//control[@name='AssoField']").InnerText;
                        ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']")).SetAttribute("value", ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']/option[@fieldname='" + p_objXmlDocument.SelectSingleNode("//control[@name='AssoField']").InnerText + "']")).GetAttribute("value"));
                        //((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']")).SetAttribute("value", sSourceField);
                        //End - pmittal5
                        //zalam 07/25/2008 Mits:-12959 End
                    }
                    SourceFieldChanged(p_objXmlDocument);
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='GAList']");
                    if (objNod != null)
                    {
                        sSQL = "SELECT ASSOC_CODE_ID FROM SUPP_ASSOC WHERE FIELD_ID = " + sRowId;
                        objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                        while (objRdr.Read())
                        {
                            objNewElm = p_objXmlDocument.CreateElement("Item");
                            objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString());
                            switch (((XmlElement)objNod).GetAttribute("type"))
                            {
                                case "codelist":
                                    objNewElm.InnerText = PopulateValueForCode(
                                        ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value")
                                        , objRdr.GetInt32(0).ToString());
                                    break;
                                case "orglist":
                                    objNewElm.InnerText = UTILITY.GetOrg(objRdr.GetInt32(0).ToString(), ConnectString, m_iClientId);
                                    break;
                                //abisht MITS 10740
                                case "eidlookup":
                                    string sFirstEid = "";
                                    string sLastEid = "";
                                    if (objCache == null)
                                    {
                                        objCache = new LocalCache(ConnectString, m_iClientId);
                                    }
                                    objCache.GetEntityInfo(objRdr.GetInt32(0), ref sFirstEid, ref sLastEid);
                                    objNewElm.InnerText = sLastEid;
                                    //zalam 08/18/2008 Mits:-13015 Start
                                    //if (sFirstEid != "" )
                                    if (sFirstEid != "" && sLastEid != "")
                                        objNewElm.InnerText += ", " + sFirstEid;
                                    else if (sLastEid == "" && sFirstEid != "")
                                        objNewElm.InnerText = sFirstEid;
                                    else
                                        objNewElm.InnerText = sLastEid;
                                    //zalam 08/18/2008 Mits:-13015 End
                                    //((XmlElement)objNewElm).SetAttribute("codeid", Conversion.ConvertObjToStr(objRdr.GetInt32(0)));
                                    ((XmlElement)objNod).SetAttribute("codeid", Conversion.ConvertObjToStr(objRdr.GetInt32(0)));
                                    break;
                                case "entitylookup":
                                    string sFirst = "";
                                    string sLast = "";
                                    objCache = new LocalCache(ConnectString, m_iClientId);
                                    objCache.GetEntityInfo(objRdr.GetInt32(0), ref sFirst, ref sLast);
                                    objNewElm.InnerText = sLast;
                                    if (sFirst != "")
                                        objNewElm.InnerText += ", " + sFirst;
                                    break;

                            }

                            objNod.AppendChild(objNewElm);
                        }
                        objRdr.Dispose();
                        XmlNode objTmp = p_objXmlDocument.SelectSingleNode("//control[@name='AssoField']");
                        string sAssoField = objTmp.InnerText;
                        //zalam 07/25/2008 Mits:-12959 Start
                        if (sAssoField.IndexOf("'") >= 0)
                        {
                            sAssoField = sAssoField.Replace("'", "`");
                        }
                        //zalam 07/25/2008 Mits:-12959 End
						

                        XmlNode objTmp2 = p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']//option[@fieldname='" + sAssoField + "']");
                        if (objTmp2 != null)
                        {
                            sAssoField = ((XmlElement)objTmp2).GetAttribute("value");
                            ((XmlElement)objNod).SetAttribute("codetable", sAssoField);
                            ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']")).SetAttribute("value", sAssoField);
                        }
                        else
                        {
                            ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']")).SetAttribute("value", sAssoField);
                            ((XmlElement)objNod).SetAttribute("codetable", sAssoField);
                        }

                        //zalam 07/25/2008 Mits:-12959 Start
                        if (sAssoField.IndexOf("`") >= 0)
                        {
                            sAssoField = sAssoField.Replace("`", "'");
                            ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceField']")).SetAttribute("value", sAssoField);
                        }
                        //zalam 07/25/2008 Mits:-12959 End

                        objTmp2 = null;
                        SourceFieldChanged(p_objXmlDocument);
                    }
                }
                //************For Bug No. 000196*****************
                if (sTableType == "9")
                {
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='lblGroup']");
                    if (objNod != null)
                    {
                        ((XmlElement)objNod).SetAttribute("sectiontitle", "Jurisdiction Association");
                    }
                }
                //**********************************************

                if (sTableType == "9" && sRowId != "" && p_objXmlDocument.SelectSingleNode("//control[@name='GrpAssoFlag']").InnerText == "-1")
                {
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='Juridiction']");
                    if (objNod != null)
                    {
                        sSQL = "SELECT ASSOC_CODE_ID FROM SUPP_ASSOC WHERE FIELD_ID = " + sRowId;
                        objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                        while (objRdr.Read())
                        {
                            //*************For Bug No. 000196*****************
                            objNewElm = p_objXmlDocument.CreateElement("Item");
                            //************************************************
                            objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString());
                            objNewElm.InnerText = UTILITY.GetState(objRdr.GetInt32(0).ToString(), ConnectString, m_iClientId);
                            objNod.AppendChild(objNewElm);
                        }
                        objRdr.Dispose();
                    }
                }

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TableField.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objNod = null;
                objNewElm = null;
                objNodCodeLst = null;
                objListType = null;
                objEnhNode = null;
                objTrackNode = null;
                if (oLobSettings != null)
                {
                    oLobSettings = null;
                }
                if (m_objLocalCache != null)
                {
                    m_objLocalCache = null;
                }
                objSysSettings = null;
            }
        }

        private string CullTableName(string p_spassed, char p_delim)
        {
            if (p_delim != ' ')
            {
                if (p_spassed.IndexOf(p_delim.ToString()) > -1)
                {
                    return p_spassed.Split(p_delim)[0];
                }
                else
                    return p_spassed;
            }
            else
            {
                if (p_spassed.IndexOf("$") > -1)
                {
                    return p_spassed.Split('$')[0];
                }
                else if (p_spassed.IndexOf('*') > -1)
                {
                    //zalam 08/18/2008 Mits:-13015 Start
                    if (p_spassed.IndexOf("JUDGES") > -1 || p_spassed.IndexOf("ATTORNEYS") > -1 || p_spassed.IndexOf("EMPLOYEES") > -1 || p_spassed.IndexOf("LEASE_COMPANY") > -1 || p_spassed.IndexOf("ATTORNEY_FIRMS") > -1)
                        return "LOOKUP";
                    else
                        return p_spassed.Split('*')[0];
                    //return p_spassed.Split('*')[0];
                    //zalam 08/18/2008 Mits:-13015 End
                }
                //zalam 08/05/2008 Mits:-12959 Start
                //else if (p_spassed == "ATTORNEY.STATE_ID" || p_spassed == "JUDGE.STATE_ID" || p_spassed == "EVENT.STATE_ID" || p_spassed == "ENTITY.STATE_ID")
                else if (p_spassed.IndexOf("STATE_ID") > -1 || p_spassed.IndexOf("STATE_ROW_ID") > -1 || p_spassed.IndexOf("DRIVLIC_STATE") > -1)
                {
                    p_spassed = "STATES";
                    return p_spassed;
                }
                //zalam 08/05/2008 Mits:-12959 End
                //zalam 08/18/2008 Mits:-13015 Start               
                else if (p_spassed.IndexOf("SUPERVISOR_EID") > -1 || p_spassed.IndexOf("CO_ATTORNEY_EID") > -1 || p_spassed.IndexOf("JUDGE_EID") > -1 || p_spassed.IndexOf("LEASING_CO_EID") > -1 || p_spassed.IndexOf("ATTORNEY.PARENT_EID") > -1)
                {
                    return "LOOKUP";
                }
                //zalam 08/18/2008 Mits:-13015 End
                else
                    return p_spassed;
            }
        }
        /// Name		: SourceFieldChanged
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/10/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// SourceFieldChanged
        /// </summary>
        /// <param name="p_objXmlDoc">input xml document</param>
        private void SourceFieldChanged(XmlDocument p_objXmlDoc)
        {
            string sTableName = "";
            XmlNode objNod = null;
            LocalCache objCache = null;
            DbReader objRdr = null;
            int iTypeCode = 0;//zalam 08/18/2008 Mits:-13015
            try
            {
                objNod = p_objXmlDoc.SelectSingleNode("//control[@name='GAList']");
                if (objNod != null)
                {
                    sTableName = ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value");
                    sTableName = CullTableName(sTableName, ' ');
                    objRdr = DbFactory.GetDbReader(ConnectString, "SELECT GLOSSARY_TYPE_CODE FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + sTableName + "'");
                    //zalam 08/18/2008 Mits:-13015 Start
                    //if (objRdr.Read()
                    if (objRdr.Read() || sTableName == "LOOKUP")
                    {
                        if (sTableName == "LOOKUP")
                            iTypeCode = 0;
                        else
                            iTypeCode = objRdr.GetInt32("GLOSSARY_TYPE_CODE");
                        //iTypeCode = objRdr.GetInt32("GLOSSARY_TYPE_CODE");
                        //zalam 08/18/2008 Mits:-13015 End
                        switch (iTypeCode)
                        {
                            case 4:
                            case 7:
                                //person search screen
                                //abisht MITS 10740
                                //zalam 08/18/2008 Mits:-13015 Start
                                //zalam 07/25/2008 Mits:-12959 Start
                                //if (((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value") == "EMPLOYEES*PI/Employee Supervisor" ||
                                //if (((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value") == "EMPLOYEES*PI/Employee Supervisor" ||
                                //       ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value") == "JUDGES*Judge" ||
                                //       ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value") == "ATTORNEYS*Attorney" ||
                                //       ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value") == "ATTORNEY_FIRMS*Attorney's Firm" ||
                                //       ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value") == "EMPLOYEES*Supervisor" ||
                                //       ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']")).GetAttribute("value") == "LEASE_COMPANY*Leasing Company")
                                // //zalam 07/25/2008 Mits:-12959 End
                                //{
                                //    ((XmlElement)objNod).SetAttribute("type", "eidlookup");
                                //}
                                //else
                                //{
                                ((XmlElement)objNod).SetAttribute("type", "entitylookup");
                                //}
                                //zalam 08/18/2008 Mits:-13015 End
                                break;
                            case 5:
                                //org hierarchy
                                ((XmlElement)objNod).SetAttribute("type", "orglist");
                                break;
                            default:
                                if (sTableName == "STATES")
                                {
                                    ((XmlElement)objNod).SetAttribute("codetable", "STATES");
                                    ((XmlElement)objNod).SetAttribute("type", "codelist");
                                    //show state lookup
                                }
                                else if (sTableName == "ENTITY")
                                {
                                    //show entity lookup
                                    ((XmlElement)objNod).SetAttribute("codetable", "ENTITY");
                                    ((XmlElement)objNod).SetAttribute("type", "codelist");
                                }
                                //zalam 08/18/2008 Mits:-13015 Start
                                else if (sTableName == "LOOKUP")
                                {
                                    ((XmlElement)objNod).SetAttribute("codetable", "LOOKUP");
                                    ((XmlElement)objNod).SetAttribute("type", "eidlookup");
                                }
                                //zalam 08/18/2008 Mits:-13015 End
								else
                                {
                                    if (iTypeCode == 2 || iTypeCode == 3)
                                    {
                                        ((XmlElement)objNod).SetAttribute("codetable", sTableName);
                                        ((XmlElement)objNod).SetAttribute("type", "codelist");
                                    }
                                }

                                break;
                        }
                    }
                }

            }
            finally
            {
                objNod = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
            }
        }

        /// Name		: LoadCodeFile
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/10/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// LoadCodeFile
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <param name="p_sFieldType"></param>
        private void LoadCodeFile(XmlDocument p_objXmlDoc, string p_sFieldType)
        {
            string sSQL = "";
            DbReader objRdr = null;
            XmlNode objCodeFileNode = null;
            XmlElement objNewElm = null;
            try
            {
                objCodeFileNode = p_objXmlDoc.SelectSingleNode("//control[@name='CodeFileId']");
                objCodeFileNode.InnerText = "";
                if (p_sFieldType == "6" || p_sFieldType == "5" || p_sFieldType == "14")
                {
                    ((XmlElement)objCodeFileNode).SetAttribute("title", "Code File");
                    //Modified the query for MITS 19126:smishra25
                    sSQL = "SELECT GLOSSARY.TABLE_ID,GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE " +
                        "GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND (GLOSSARY.DELETED_FLAG <> 1 OR GLOSSARY.DELETED_FLAG IS NULL) AND  GLOSSARY.GLOSSARY_TYPE_CODE IN (" +
                        "SELECT CODE_ID FROM CODES,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='GLOSSARY_TYPES' AND " +
                        "GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.SHORT_CODE IN ('2','3','10')) AND GLOSSARY_TEXT.LANGUAGE_CODE = 1033 " + // Changed 1064 to 10: MITS 19126:smishra25
                        "ORDER BY GLOSSARY_TEXT.TABLE_NAME";
                    ((XmlElement)objCodeFileNode).SetAttribute("title", "Code File");
                }
                else if (p_sFieldType == "8" || p_sFieldType == "16")
                {
                    sSQL = "SELECT GLOSSARY.TABLE_ID,GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE " +
                        "GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND  GLOSSARY.GLOSSARY_TYPE_CODE IN (" +
                        "SELECT CODE_ID FROM CODES,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME='GLOSSARY_TYPES' AND " +
                        "GLOSSARY.TABLE_ID=CODES.TABLE_ID AND CODES.SHORT_CODE IN ('4','5','7')) AND GLOSSARY_TEXT.LANGUAGE_CODE = 1033 " +
                        "ORDER BY GLOSSARY_TEXT.TABLE_NAME";
                    ((XmlElement)objCodeFileNode).SetAttribute("title", "Entity File");
                }
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                else if (p_sFieldType == "23")
                {
                    sSQL = "SELECT SYS_SETTINGS.ROW_ID,SYS_SETTINGS.ITEM FROM SYS_SETTINGS WHERE SYS_SETTINGS.USE_AS_SUPPLEMENTAL_LINK = -1";
                    ((XmlElement)objCodeFileNode).SetAttribute("title", "HyperLink");
                }
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                else
                    return;
                objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                while (objRdr.Read())
                {
                    objNewElm = p_objXmlDoc.CreateElement("option");
                    objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString());
                    objNewElm.InnerText = objRdr.GetString(1);
                    objCodeFileNode.AppendChild(objNewElm);
                }
            }
            finally
            {
                objNewElm = null;
                objCodeFileNode = null;
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
            }
        }

        /// Name		: Save
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/10/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string sTableType = ""; //Admintracking=8, supplemental=6,jurisdictional=9
            XmlNode objNod = null;
            int iRowId = 0;
            int iFieldType = 0;
            int iFieldSize = 0;
            int iIsPatterned = 0;
            int iReq = 0;
            int iFASReportable = 0;
            int iMultiSelect = 0;//sharishkumar Jira 6415
            int iUserGroups = 0;//sharishkumar Jira 6415
            int iChkhtmltextConfig = 0;
            int iLookup = 0;
            int iCodeFileId = 0;
            int iGroupAsso = 0;
            int iNetVisible = 0;
            int iDelete = 0;
            int iHelpId = 0;
            bool isAdmin = false; //zmohammad MITs 33885 : Flag for Admin Tracking Tables.
            //Added by Shivendu for Supplemental Grid
            int iColumnNumber = 0;
            string sDttmLastUpd = string.Empty;//rkaur27 : RMA-18764
            string sSysTableName = "";
            string sSysFieldName = "";
            string sUserFieldName = "";
            string sGroupAsso = "";
            string sGroupAssoField = "";
            string sPattern = "";
             //starts by Nitin
            string[] arrGAListHidden = null;
            int varHideLOB_GC = -1;
            int varHideLOB_WC = -1;
            int varHideLOB_VA = -1;
            int varHideLOB_NOC= -1;

            //Start: Neha Suresh Jain, 04/21/2010, for PC LOB
            int varHideLOB_PC = -1;
            //End: Neha Suresh Jain, 04/21/2010
            //ends by Nitin
            //Added by Shivendu for Supplemental Grid
            string sColumns = "";
            LocalCache objCache = null; //Mgaba2:-MITS 8144
            try
            {
                XMLDoc = p_objXmlDocument;
                // Anjaneya : MITS 11781
                p_objXmlDocument.InnerXml = p_objXmlDocument.InnerXml.Replace("'", "''");

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='TableType']");
                if (objNod != null)
                    sTableType = objNod.InnerText;

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objNod != null)
                    iRowId = Conversion.ConvertStrToInteger(objNod.InnerText);

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='FieldType']");
                if (objNod != null)
                    iFieldType = Conversion.ConvertStrToInteger(((XmlElement)objNod).GetAttribute("value"));

                //pmittal5 Mits 14459 02/18/09 
                if (iFieldType == 0)
                {
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='hdnFieldType']");
                    if (objNod != null)
                        iFieldType = Conversion.ConvertStrToInteger(((XmlElement)objNod).InnerText);
                }
                //End - pmittal5
                //Added by Shivendu for Supplemental Grid


                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='ColCount']");
                if (objNod != null)
                    if (objNod.InnerText != null || objNod.InnerText != "")
                        iColumnNumber = Conversion.ConvertStrToInteger(objNod.InnerText);

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='Columns']");
                if (objNod != null)
                    sColumns = objNod.InnerText;



                //Added by Shivendu for Supplemental Grid

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='SysTableName']");
                if (objNod != null)
                    sSysTableName = objNod.InnerText;

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='SysFieldName']");
                if (objNod != null)
                    sSysFieldName = objNod.InnerText;

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='UserPrompt']");
                if (objNod != null)
                    sUserFieldName = objNod.InnerText;

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='FieldSize']");
                if (objNod != null)
                    iFieldSize = Conversion.ConvertStrToInteger(objNod.InnerText);

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='RequiredFlag']");
                if (objNod != null)
                {
                    if (objNod.InnerText.ToLower().Trim().Equals("true"))
                        iReq = -1;
                    else
                        iReq = 0;
                }
                //Asharma326 MITS 32386 Starts
                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='FASReportableflag']");
                if (objNod != null)
                {
                    if (objNod.InnerText.ToLower().Trim().Equals("true"))
                        iFASReportable = -1;
                    else
                        iFASReportable = 0;
                }
                //Asharma326 MITS 32386 Ends

                //sharishkumar Jira 6415 starts
                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='MultiSelect']");
                if (objNod != null)
                {
                    if (objNod.InnerText.ToLower().Trim().Equals("true"))
                        iMultiSelect = -1;
                    else
                        iMultiSelect = 0;
                }
                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='ddUsersGroups']");
                if (objNod != null)
                {
                    iUserGroups = Convert.ToInt32(((XmlElement)objNod).GetAttribute("value"));
                }
                //sharishkumar Jira 6415 ends

                //Asharma326 MITS 32386 Starts
                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='chkhtmltextconfig']");
                if (objNod != null)
                {
                    if (objNod.InnerText.ToLower().Trim().Equals("true"))
                        iChkhtmltextConfig = -1;
                    else
                        iChkhtmltextConfig = 0;
                }
                //Asharma326 MITS 32386 Ends

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='NetVisibleFlag']");
                if (objNod != null)
                {
                    if (objNod.InnerText.ToLower().Trim().Equals("true"))
                        iNetVisible = -1;
                    else
                        iNetVisible = 0;
                }

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='DeleteFlag']");
                if (objNod != null)
                {
                    if (objNod.InnerText.ToLower().Trim().Equals("true"))
                        iDelete = -1;
                    else
                        iDelete = 0;
                }

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='IsPattern']");
                if (objNod != null)
                    iIsPatterned = Conversion.ConvertStrToInteger(objNod.InnerText);

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='Pattern']");
                if (objNod != null)
                    sPattern = ((XmlElement)objNod).GetAttribute("value");

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='LookupFlag']");
                if (objNod != null)
                {
                    if (objNod.InnerText.ToLower().Trim().Equals("true"))
                        iLookup = -1;
                    else
                        iLookup = 0;
                }
                //MGaba2-MITS 8144-Added for state type of supplemental field
                //in case of state and MultiState,CodeFileId doesnt exists,should contain table id of STATES
                if (iFieldType == 9 || iFieldType == 15)
                {
                    objCache = new LocalCache(ConnectString, m_iClientId);
                    iCodeFileId = objCache.GetTableId("STATES");
                }
                else
                {
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='CodeFileId']");
                    if (objNod != null)
                        iCodeFileId = Conversion.ConvertStrToInteger(((XmlElement)objNod).GetAttribute("value"));
                }
                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='AssoField']");
                if (objNod != null)
                    sGroupAssoField = objNod.InnerText;

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='GAListHidden']");
                if (objNod != null)
                    sGroupAsso = objNod.InnerText;

                 //Added by Nitin FOR SAFEWAY PERFORMANCE ISSUE

                if (sGroupAssoField == "CLAIM.LINE_OF_BUS_CODE")
                {
                    arrGAListHidden = sGroupAsso.Split(' ');
                    for (int iCtr = 0; iCtr < arrGAListHidden.Length; iCtr++)
                    {
                        switch (arrGAListHidden[iCtr])
                        {
                            case "241":    //for GC
                                varHideLOB_GC = 0;
                                break;
                            case "243":   //for WC
                                varHideLOB_WC = 0;
                                break;
                            case "242":  //for VA
                                varHideLOB_VA = 0;
                                break;
                            case "844":  //for NOC
                                varHideLOB_NOC = 0;
                                break;
                                //Start: Neha Suresh Jain, 04/21/2010, for PC LOB
                            case "845":  //for PC
                                varHideLOB_PC = 0;
                                break;
                                //End: Neha Suresh Jain, 04/21/2010
                        }
                    }
                }
                else
                {
                    varHideLOB_GC = 0;
                    varHideLOB_WC = 0;
                    varHideLOB_VA = 0;
                    varHideLOB_NOC = 0;
                    //Start: Neha Suresh Jain, 04/21/2010, for PC LOB
                    varHideLOB_PC = 0;
                    //End: Neha Suresh Jain, 04/21/2010
                }
                //Ended by Nitin

				if(sTableType=="9")
                {
                    objNod = p_objXmlDocument.SelectSingleNode("//control[@name='JuridictionHidden']");
                    if (objNod != null)
                        sGroupAsso = objNod.InnerText;
                }

                //zmohammad MITs 33885 Start : Flag for Admin Tracking Tables.
                if (sTableType == "8")
                {
                    isAdmin = true;
                }
                else
                {
                    isAdmin = false;
                }
                //zmohammad MITs 33885 End : Flag for Admin Tracking Tables.
                if (sGroupAsso == "")
                    iGroupAsso = 0;
                else
                    iGroupAsso = -1;

                objNod = p_objXmlDocument.SelectSingleNode("//control[@name='HelpContext']");
                if (objNod != null)
                    iHelpId = Conversion.ConvertStrToInteger(objNod.InnerText);
                //rkaur27 : RMA-18764 - Start
                objNod = p_objXmlDocument.SelectSingleNode("//internal[@name='DttmLastUpdtd']");
                if (objNod != null)
                    sDttmLastUpd = objNod.InnerText;
                //rkaur27 : RMA-18764 - End
                //Start: Neha Suresh Jain, 04/21/2010, for PC LOB
                //Modified by Shivendu for Supplemental Grid. Passing two extra parameters sColumns,iColumnNumber

                //CreateField(iRowId, sSysTableName, sSysFieldName, sUserFieldName, iFieldType, iFieldSize, iReq,
                //   iIsPatterned, iLookup, iCodeFileId, iGroupAsso, sGroupAsso, iNetVisible, iDelete, sGroupAssoField, sPattern, iHelpId, sColumns, iColumnNumber,
                //   varHideLOB_WC, varHideLOB_GC, varHideLOB_VA, varHideLOB_NOC);
                //Ritesh:Start-MITS 26890 
                bool objflag;

                /*CreateField(iRowId, sSysTableName, sSysFieldName, sUserFieldName, iFieldType, iFieldSize, iReq,
                  iIsPatterned, iLookup, iCodeFileId, iGroupAsso, sGroupAsso, iNetVisible, iDelete, sGroupAssoField, sPattern, iHelpId, sColumns, iColumnNumber,
                  varHideLOB_WC, varHideLOB_GC, varHideLOB_VA, varHideLOB_NOC, varHideLOB_PC);*/

                //sharishkumar Jira 6415 starts
                //objflag=CreateField(iRowId, sSysTableName, sSysFieldName, sUserFieldName, iFieldType, iFieldSize, iReq,
                //  iIsPatterned, iLookup, iCodeFileId, iGroupAsso, sGroupAsso, iNetVisible, iDelete, sGroupAssoField, sPattern, iHelpId, sColumns, iColumnNumber,
                //  varHideLOB_WC, varHideLOB_GC, varHideLOB_VA, varHideLOB_NOC, varHideLOB_PC, isAdmin, iFASReportable);

                objflag = CreateField(iRowId, sSysTableName, sSysFieldName, sUserFieldName, iFieldType, iFieldSize, iReq,
                  iIsPatterned, iLookup, iCodeFileId, iGroupAsso, sGroupAsso, iNetVisible, iDelete, sGroupAssoField, sPattern, iHelpId, sColumns, iColumnNumber,
                  varHideLOB_WC, varHideLOB_GC, varHideLOB_VA, varHideLOB_NOC, varHideLOB_PC, isAdmin, iFASReportable, iMultiSelect, iUserGroups, iChkhtmltextConfig, sDttmLastUpd);
                //sharishkumar Jira 6415 ends

                //End: Neha Suresh Jain, 04/21/2010
                objNod = p_objXmlDocument.SelectSingleNode("//internal[@name='Duplicate_Flag']");
                //Changed by bsharma33 check missed objNod=null in the else part.
                if (objNod!=null)
                {
                 if (objflag == false)
                {
                   // if (objNod != null)
                    //{
                        objNod.InnerText = "Yes";
                    //}
                }
                else
                    objNod.InnerText = "No";
                }
                //Ritesh:End-MITS 26890 
                //End changes by bsharma33
                return XMLDoc;
            }
            catch (RMAppException p_objEx)
            {

                throw new RMAppException(p_objEx.Message, p_objEx);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TableField.Save.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNod = null;
            }
        }

        /// <summary>
        /// GetGAForStateTable
        /// </summary>
        /// <param name="p_sStateTableName">tablename</param>
        /// <returns>ga state</returns>
        private string GetGAForStateTable(string p_sStateTableName)
        {
            switch (p_sStateTableName)
            {
                case "WC_":
                case "_SUPP":
                    return "CLAIM.FILING_STATE_ID";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Function upgrades aspx of specified form in NET_VIEW_FORMS table
        /// </summary>
        /// <param name="xmlToUpgrade">Form Name to be upgraded</param>
        /// <returns></returns>
        //public bool UpgradeXmlToAspx(string sXmlName)
        public bool UpgradeXmlToAspx(string sXmlName,bool pIsPowerviewable)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlDocument xdocAdmin = new XmlDocument();
            StringBuilder convertedAspx;
            int iUpdate = 0;
            int iAdminTableRows = 0;
            DbCommand cmd = null;
            DbParameter objParam = null;
            DbParameter objParamXml = null;
            DbParameter objParamCaption = null;
            DbConnection oConn = null;
            StringBuilder sbSql = new StringBuilder("");
            StringBuilder sbSelectSql = new StringBuilder("");
            StringBuilder sbAdminTablesCount = new StringBuilder("");
            string sSQLAdminTablesList = string.Empty;
            DataSet ds = null;
            //DataSet dsAdminTables = null;
            ArrayList retAdminTables = new ArrayList();
            bool bSuccess = false;
            string sPageName = "";
            string sFormName = "";
            PowerViewMode CurrentMode = PowerViewMode.SUPP;
            UpgradeSection CurrentSection = UpgradeSection.ONLYSUPP;
            //Start - Fix for : Supplemental setup override the read-only property tp false always for powerview.
            bool bReadOnly = false;
            XmlElement objElm = null;
            bool bIsSucess = false;
            //End
            try
            {

                sbSelectSql.Append("SELECT FORM_NAME,VIEW_XML,VIEW_ID FROM NET_VIEW_FORMS WHERE FORM_NAME NOT LIKE '%LIST.XML' AND FORM_NAME NOT LIKE '%.XSL' AND FORM_NAME NOT LIKE '%.SCC' AND DATA_SOURCE_ID ='" + m_sDSNId + "'");
                if (sXmlName != "")
                {
                    sbSelectSql.Append("AND FORM_NAME = '");
                    sbSelectSql.Append(sXmlName);
                    sbSelectSql.Append("'");
                }
                ds = DbFactory.GetDataSet(m_netViewFormsConnString, sbSelectSql.ToString(), m_iClientId);
                oConn = DbFactory.GetDbConnection(m_netViewFormsConnString); ;
                PowerViewUpgrade pvUpgrade = new PowerViewUpgrade(m_iClientId);
                XmlDocument objJurisData = new XmlDocument();

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    oConn.Open();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        sFormName = row["FORM_NAME"].ToString();
                        sbSql.Remove(0, sbSql.Length);
                        xdoc.LoadXml(row["VIEW_XML"].ToString());
                        objElm = (XmlElement)xdoc.GetElementsByTagName("form")[0];
                        if (objElm != null && objElm.Attributes["readonly"] != null)
                            bReadOnly = Conversion.CastToType<bool>(objElm.Attributes["readonly"].Value, out bIsSucess);
                        if (row["VIEW_ID"].ToString() == "0" || !pIsPowerviewable) //RMA-4691-Supplementals not getting update in Riskmaster views case of Non-Powerviewable FDM pages
                        {
                            if (row["FORM_NAME"].ToString().IndexOf("admintracking") < 0)
                            {
                                    UTILITY.AddSuppDefinitionPowerViewUpgrade(m_sUserId, m_sPassword, m_netViewFormsConnString, m_sDSNName, xdoc, false, out retAdminTables, null,CurrentMode,"",CurrentSection,m_iClientId);
                            }
                            else if (sFormName.IndexOf("admintracking.xml") > -1)
                            {
                                UTILITY.AddSuppDefinitionPowerViewUpgrade(m_sUserId, m_sPassword, m_netViewFormsConnString, m_sDSNName, xdoc, true, out retAdminTables, null, CurrentMode, "", CurrentSection,m_iClientId);

                                foreach (string[] arrItem in retAdminTables)
                                {
                                    xdocAdmin.LoadXml(xdoc.OuterXml);
                                    sbSql.Remove(0, sbSql.Length);
                                    sbAdminTablesCount.Remove(0, sbAdminTablesCount.Length);
                                    ((XmlElement)xdocAdmin.GetElementsByTagName("form")[0]).SetAttribute("supp", arrItem[0].ToString());
                                    ((XmlNode)xdocAdmin.SelectSingleNode("//internal[@name='SysFormName']")).Attributes["value"].Value = "admintracking|" + arrItem[0].ToString();
                                    UTILITY.AddSuppDefinitionPowerViewUpgrade(m_sUserId, m_sPassword, m_netViewFormsConnString, m_sDSNName, xdocAdmin, false, out retAdminTables, null, CurrentMode, "", CurrentSection,m_iClientId);

                                    pvUpgrade.UpgradeXmlToAspx(bReadOnly, false, xdocAdmin, sFormName.Insert(sFormName.IndexOf('.'), arrItem[0].ToString()), out convertedAspx);
                                    cmd = oConn.CreateCommand();
                                    // akaushk5 Changed for MITS 37700 Starts
                                    //sbAdminTablesCount.Append("SELECT COUNT(*) FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND FORM_NAME LIKE '%" + arrItem[0] + "%' AND DATA_SOURCE_ID = '" + m_sDSNId + "'");
                                    sbAdminTablesCount.AppendFormat("SELECT COUNT(*) FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND FORM_NAME = '{0}' AND DATA_SOURCE_ID = '{1}'", string.Format("admintracking|{0}.xml", arrItem[0]), m_sDSNId);
                                    // akaushk5 Changed for MITS 37700 Ends
                                    cmd.CommandText = sbAdminTablesCount.ToString();
                                    iAdminTableRows = Convert.ToInt32(cmd.ExecuteScalar()); //(int)cmd.ExecuteScalar();
                                    if (iAdminTableRows <= 0)
                                    {
                                        cmd = oConn.CreateCommand();
                                        
                                        objParamXml = cmd.CreateParameter();
                                        objParamXml.Direction = ParameterDirection.Input;
                                        objParamXml.Value = xdocAdmin.OuterXml;
                                        objParamXml.ParameterName = "XML";
                                        objParamXml.SourceColumn = "VIEW_XML";
                                       
                                        objParam = cmd.CreateParameter();
                                        objParam.Direction = ParameterDirection.Input;
                                        objParam.Value = convertedAspx.ToString();
                                        objParam.ParameterName = "ASPX";
                                        objParam.SourceColumn = "PAGE_CONTENT";

                                        //igupta3 MITS:25132 Changes starts
                                        objParamCaption = cmd.CreateParameter();
                                        objParamCaption.Direction = ParameterDirection.Input;
                                        objParamCaption.Value = Convert.ToString(arrItem[1]);
                                        objParamCaption.ParameterName = "PRMCAPTION";
                                        objParamCaption.SourceColumn = "CAPTION";

                                        cmd.Parameters.Add(objParamCaption);
                                        cmd.Parameters.Add(objParamXml);
                                        cmd.Parameters.Add(objParam);
                                        sSQLAdminTablesList = "INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML,PAGE_CONTENT,PAGE_NAME,LAST_UPDATED,DATA_SOURCE_ID) VALUES (" +
                                                              "0" + ",'" + "admintracking|" + arrItem[0] + ".xml" +
                                                              //"'," + "1" + ",'" + arrItem[1] + "'," +
                                                              "'," + "1" + ",~PRMCAPTION~," +
                                                              "~XML~,~ASPX~,'" + "admintracking" + arrItem[0] + ".aspx" + "','" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "','" + m_sDSNId + "')";
                                        //igupta3 MITS:25132 Changes ends
                                        cmd.CommandText = sSQLAdminTablesList;
                                        iUpdate = cmd.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        cmd = oConn.CreateCommand();
                                        objParamXml = cmd.CreateParameter();
                                        objParamXml.Direction = ParameterDirection.Input;
                                        objParamXml.Value = xdocAdmin.OuterXml;
                                        objParamXml.ParameterName = "XML";
                                        objParamXml.SourceColumn = "VIEW_XML";
                                        objParam = cmd.CreateParameter();
                                        objParam.Direction = ParameterDirection.Input;
                                        objParam.Value = convertedAspx.ToString();
                                        objParam.ParameterName = "ASPX";
                                        objParam.SourceColumn = "PAGE_CONTENT";
                                        cmd.Parameters.Add(objParamXml);
                                        cmd.Parameters.Add(objParam);
                                        sPageName = sFormName.Replace("xml", "aspx");
                                        sbSql.Append("UPDATE NET_VIEW_FORMS SET VIEW_XML=~XML~,PAGE_CONTENT =~ASPX~,LAST_UPDATED = '" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "',PAGE_NAME = '" + "admintracking" + arrItem[0] + ".aspx" + "' WHERE FORM_NAME = '" + "admintracking|" + arrItem[0] + ".xml" + "' AND VIEW_ID = " + row["VIEW_ID"].ToString() + " AND DATA_SOURCE_ID ='" + m_sDSNId + "'");
                                        cmd.CommandText = sbSql.ToString();
                                        iUpdate = cmd.ExecuteNonQuery();
                                    }
                                }
                                continue;


                            }
                            else
                            {
                                continue;
                            }
                        }
                        pvUpgrade.UpgradeXmlToAspx(bReadOnly, false, xdoc, row["FORM_NAME"].ToString(), out convertedAspx);
                        cmd = oConn.CreateCommand();
                        objParam = cmd.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = convertedAspx.ToString();
                        objParam.ParameterName = "ASPX";
                        objParam.SourceColumn = "PAGE_CONTENT";
                        cmd.Parameters.Add(objParam);

                        sPageName = row["FORM_NAME"].ToString().Replace("xml", "aspx");
                        sbSql.Append("UPDATE NET_VIEW_FORMS SET PAGE_CONTENT = ~ASPX~,PAGE_NAME = '" + sPageName.Replace("|", "") + "',LAST_UPDATED = '" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "' WHERE FORM_NAME = '" + row["FORM_NAME"].ToString() + "' AND VIEW_ID = " + row["VIEW_ID"].ToString() + " AND DATA_SOURCE_ID ='" + m_sDSNId + "'");
                        cmd.CommandText = sbSql.ToString();
                        iUpdate = cmd.ExecuteNonQuery();

                    }
                }
                else
                {
                    bSuccess = false;
                    return bSuccess;
                }


                bSuccess = true;
                return bSuccess;
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("TableManager.UpgradeXmlToAspx.Error", m_iClientId), ex);
            }
            finally
            {
                oConn.Close();
                cmd = null;
                if (oConn != null)
                    oConn.Dispose();
                if (ds != null)
                    ds.Dispose();

            }
        }

        /// <summary>
        /// Function upgrades/inserts aspx of specified form in JURIS_VIEWS table
        /// </summary>
        /// <param name="sStateSuppName">The state name for which the upgrade needs to be run</param>
        /// <returns>It returns whether the conversion was successful</returns>
        public bool UpgradeXmlToAspxJuris(string sStateSuppName)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlDocument xdocAdmin = new XmlDocument();
            StringBuilder convertedAspx;
            int iUpdate = 0;
            int iAdminTableRows = 0;
            DbCommand cmd = null;
            DbParameter objParam = null;
            DbParameter objParamXml = null;
            DbConnection oConn = null;
            StringBuilder sbSql = new StringBuilder("");
            StringBuilder sbSelectSql = new StringBuilder("");
            StringBuilder sbAdminTablesCount = new StringBuilder("");
            string sSQLAdminTablesList = string.Empty;
            DataSet ds = null;
            //DataSet dsAdminTables = null;
            ArrayList retAdminTables = new ArrayList();
            bool bSuccess = false;
            string sPageName = "";
            string sFormName = "";
            XmlDocument objJuris = null;
            XmlNodeList objJurisData = null;
            XmlDocument objIndividualStateData = null;
            int iStateId = 0;
            object objDataSourceDB = null;
            string sFormattedXml = "";
            StringBuilder sResponse = new StringBuilder();
            PowerViewMode CurrentMode = PowerViewMode.JURIS;
            UpgradeSection CurrentSection = UpgradeSection.ONLYJURIS;

            try
            {
                // Naresh We do not need to do any of the stuff in UpgradeXmlToAspx as we do not want to modify the Claimwc.xml
                oConn = DbFactory.GetDbConnection(m_netViewFormsConnString); ;
                PowerViewUpgrade pvUpgrade = new PowerViewUpgrade(m_iClientId);
                oConn.Open();
                objJuris = new XmlDocument();
                UTILITY.AddSuppDefinitionPowerViewUpgrade(m_sUserId, m_sPassword, m_netViewFormsConnString, m_sDSNName, xdoc, false, out retAdminTables, objJuris, CurrentMode, sStateSuppName,CurrentSection,m_iClientId);
                objJurisData = objJuris.SelectNodes("/JurisData/*");

                foreach (XmlNode objJurisNode in objJurisData)
                {
                    // Get the State
                    sbSql.Remove(0, sbSql.Length);

                    iStateId = Int32.Parse(objJurisNode.Attributes["StateId"].Value);
                    objIndividualStateData = new XmlDocument();
                    objIndividualStateData.LoadXml(objJurisNode.OuterXml);

                    pvUpgrade.UpgradeXmlTagToAspxFormJuris(false, false, objIndividualStateData, sFormName, out convertedAspx);

                    sResponse.Append("<%@ Import Namespace=\"System.Data\" %> <%@ Import Namespace=\"System.Xml\" %> <%@ Register Assembly=\"AjaxControlToolkit\" Namespace=\"AjaxControlToolkit\" TagPrefix=\"cc1\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" TagName=\"CodeLookUp\" TagPrefix=\"uc\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" TagName=\"UserControlDataGrid\" TagPrefix=\"dg\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" TagName=\"MultiCode\" TagPrefix=\"uc\" %>");
                    sResponse.Append("<%@ Register Src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" TagName=\"PleaseWaitDialog\"   TagPrefix=\"uc\" %>");
                    sResponse.Append("<%@ Register Assembly=\"MultiCurrencyCustomControl\" Namespace=\"MultiCurrencyCustomControl\" TagPrefix=\"mc\" %>");//asingh263 MITS 35514
                    sResponse.Append(convertedAspx.ToString());

                    // Check the Existence of Record in DB
                    sbSql.Append("SELECT DATA_SOURCE_ID FROM JURIS_VIEWS WHERE DATA_SOURCE_ID = " + m_sDSNId + " AND STATE_ID = " + iStateId.ToString());
                    cmd = oConn.CreateCommand();
                    cmd.CommandText = sbSql.ToString();
                    objDataSourceDB = cmd.ExecuteScalar();
                    cmd = oConn.CreateCommand();
                    objParam = cmd.CreateParameter();
                    objParam.Direction = ParameterDirection.Input;
                    objParam.Value = sResponse.ToString();
                    objParam.ParameterName = "ASPX";
                    objParam.SourceColumn = "PAGE_CONTENT";
                    cmd.Parameters.Add(objParam);
                    sbSql.Remove(0, sbSql.Length);
                    if (objDataSourceDB == null)
                    {
                        sbSql.Append("INSERT INTO JURIS_VIEWS(DATA_SOURCE_ID, STATE_ID, LAST_UPDATED, PAGE_CONTENT) VALUES(" + m_sDSNId + "," + iStateId.ToString() + ",'" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "', ~ASPX~ )");
                    }
                    else
                    {
                        sbSql.Append("UPDATE JURIS_VIEWS SET LAST_UPDATED = '" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "', PAGE_CONTENT=~ASPX~ WHERE DATA_SOURCE_ID = " + m_sDSNId + "AND STATE_ID = " + iStateId.ToString());
                    }

                    convertedAspx.Remove(0, convertedAspx.Length);
                    sResponse.Remove(0, convertedAspx.Length);
                    cmd.CommandText = sbSql.ToString();
                    iUpdate = cmd.ExecuteNonQuery();
                }
                bSuccess = true;
                return bSuccess;
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("TableManager.UpgradeXmlToAspx.Error", m_iClientId), ex);
            }
            finally
            {
                oConn.Close();
                cmd = null;
                if (oConn != null)
                    oConn.Dispose();
                if (ds != null)
                    ds.Dispose();

            }
        }



        /// Name		: CreateField
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/09/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates a Field in the Database Table
        /// </summary>
        /// <param name="p_iFieldId">Field Id</param>
        /// <param name="p_sSysTableName">System Table Name</param>
        /// <param name="p_sSysFieldName">System Field Name</param>
        /// <param name="p_sUserFieldName">User Field Name</param>
        /// <param name="p_iFieldType">Field Type</param>
        /// <param name="p_iFieldSize">Field Size</param>
        /// <param name="p_iReqd">Required Flag</param>
        /// <param name="p_iIsPatterned">Patterned</param>
        /// <param name="p_iLkp">Lookup Flag</param>
        /// <param name="p_iCodFilId">Code Field Id</param>
        /// <param name="p_iIsReq">Required</param>
        /// <param name="p_iGrpAss">Asso Flag</param>
        /// <param name="p_sGrpAssoArr">Asso Data</param>

        //Neha Suresh Jain, 04/21/2010, for PC LOB, CreateField(PC parameter added)

         //private void CreateField(int p_iFieldId, string p_sSysTableName, string p_sSysFieldName, string p_sUserFieldName,
         //   int p_iFieldType, int p_iFieldSize, int p_iReqd, int p_iIsPatterned, int p_iLkp,
         //   int p_iCodFilId, int p_iGrpAss, string p_sGrpAssoArr, int p_iNetVisible, int p_iDelete,
        //   string p_sGrpAssoField, string p_sPattern, int p_iHelpId, string sColumns, int iColumnNumber, int p_iLob_WC, int p_iLob_GC, int p_iLob_VA, int p_iLob_NOC)   //Ritesh:MITS 26890

        //sharishkumar Jira 6415 starts
       //private bool CreateField(int p_iFieldId, string p_sSysTableName, string p_sSysFieldName, string p_sUserFieldName,
       //     int p_iFieldType, int p_iFieldSize, int p_iReqd, int p_iIsPatterned, int p_iLkp,
       //     int p_iCodFilId, int p_iGrpAss, string p_sGrpAssoArr, int p_iNetVisible, int p_iDelete,
       //     string p_sGrpAssoField, string p_sPattern, int p_iHelpId, string sColumns, int iColumnNumber, int p_iLob_WC, int p_iLob_GC, int p_iLob_VA, int p_iLob_NOC, int p_iLob_PC, bool isAdmin, int p_iFASreportable)  //Ritesh:MITS 26890
        private bool CreateField(int p_iFieldId, string p_sSysTableName, string p_sSysFieldName, string p_sUserFieldName,
           int p_iFieldType, int p_iFieldSize, int p_iReqd, int p_iIsPatterned, int p_iLkp,
           int p_iCodFilId, int p_iGrpAss, string p_sGrpAssoArr, int p_iNetVisible, int p_iDelete,
           string p_sGrpAssoField, string p_sPattern, int p_iHelpId, string sColumns, int iColumnNumber, int p_iLob_WC, int p_iLob_GC, int p_iLob_VA, int p_iLob_NOC, int p_iLob_PC, bool isAdmin, int p_iFASreportable, int p_iMultiSelect, int p_iUserGroups, int p_ihtmltextConfig,string p_sDttmLastUpd)//asharma326 Added p_iChkhtmltextConfig for storing HTML Configurable flag.
        //sharishkumar Jira 6415 ends
        {
            string[] arrId;
            string sFieldName = "";
            string sSQL = "";
            string sSQLSrch = "";   // atavaragiri MITS 26342
            string sSQLDisp = "";   // atavaragiri MITS 26342
            int iFieldSize = 0;
            int iHelpId = 0;
            int iFieldId = 0;
            bool bPrevDeletedStatus = false; //Mgaba2:Whether field was already deleted
            DbCommand objCmd = null;
            DbConnection objCon = null;
            DbTransaction objTrans = null;
            LocalCache objCache = null;
            DbReader objreader = null;   //Ritesh:MITS 26890 
            int iWebLinkRowId = 0; //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
           
            
            //Added by Shivendu for Supplemental Grid
            bool rColumn = true;
            
            try
            {

                objCon = DbFactory.GetDbConnection(ConnectString);
                objCon.Open();
                objCmd = objCon.CreateCommand();
                objTrans = objCon.BeginTransaction();
                objCmd.Transaction = objTrans;
                objCache = new LocalCache(ConnectString, m_iClientId);
                //Ritesh:Start-MITS 26890 
                if (p_iFieldId == 0)
                {
                    sFieldName = ExtendFieldName(p_sSysFieldName, p_iFieldType);
                    string sQuery = "SELECT SYS_FIELD_NAME FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + p_sSysTableName + "'";
                    sQuery = sQuery + " AND SYS_FIELD_NAME = '" + sFieldName + "'";
                    objCmd.CommandText = sQuery;
                    objreader = DbFactory.GetDbReader(ConnectString, sQuery);
                    if (objreader.Read())
                    {
                        return false;
                    }
                }
				//rkaur27 : RMA-18764 - Start
                else 
                {
                    string sQuery = "SELECT DTTM_RCD_LAST_UPD FROM SUPP_DICTIONARY WHERE FIELD_ID = " + p_iFieldId;
                    objreader = DbFactory.GetDbReader(ConnectString, sQuery);
                    while(objreader.Read())
                    {
                        if (string.Compare(p_sDttmLastUpd, objreader.GetValue("DTTM_RCD_LAST_UPD").ToString()) != 0)
                        {
                            throw new RMAppException(String.Format(Globalization.GetString("Supplementals.PopulateObject.Exception.RecordAlreadyChanged", m_iClientId), p_sUserFieldName));
                        }
                    }
                }
				//rkaur27 : RMA-18764 - End
                //Ritesh:End-MITS 26890 
                //Append type to field name
                if (p_iFieldId == 0)//add mode
                {
                    sFieldName = ExtendFieldName(p_sSysFieldName, p_iFieldType);
                    switch (p_iFieldType)
                    {
                        case 0:
                            iFieldSize = p_iFieldSize;
                            break;
                        case 3:
                            iFieldSize = 8;
                            break;
                        case 4:
                            iFieldSize = 6;
                            break;
                        case 10:
                        case 12:
                        //Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                        case 18:
                        case 19:
                        //End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                            iFieldSize = 25;
                            break;
                        case 13:
                            iFieldSize = 20;
                            break;
                        case 15:
                            p_iCodFilId = objCache.GetTableId("STATES");
                            break;
                        //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                        case 23:
                            iWebLinkRowId = p_iCodFilId;
                            p_iCodFilId = 0;
                            break;
                        //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                        default:
                            iFieldSize = 0;
                            break;
                    }

                    //Don't add supp field to DB for true Multi-Type fields:Added by Shivendu for MITS 9331
					// npadhy Jira 6415 We do not want User Lookup to be included here
                    if ((p_iFieldType < 14 || p_iFieldType > 17) && p_iFieldType != 22)
                    {
                        //gagnihotri 06/17/2008 MITS 12660
                        //Adding a default value 0 for the code type fields
                        if (p_iFieldType == 6 || p_iFieldType == 31)//MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 
                        {
                            //Generate the query for Alter table
                            switch (objCon.DatabaseType)
                            {
                                case eDatabaseType.DBMS_IS_ACCESS:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD COLUMN " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " DEFAULT 0";
                                    break;
                                case eDatabaseType.DBMS_IS_INFORMIX:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD ( " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " DEFAULT 0)";
                                    break;
                                case eDatabaseType.DBMS_IS_SQLSRVR:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " DEFAULT 0 WITH VALUES NULL";
                                    break;
                                case eDatabaseType.DBMS_IS_SYBASE:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " DEFAULT 0";
                                    break;
                                case eDatabaseType.DBMS_IS_ORACLE:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD (" +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " DEFAULT 0 NULL)";
                                    break;
                                case eDatabaseType.DBMS_IS_DB2:
                                    sSQL = "CREATE TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " WITH DEFAULT 0";
                                    break;
                            }
                        }
                        else
                        {
                            //Generate the query for Alter table
                            switch (objCon.DatabaseType)
                            {
                                case eDatabaseType.DBMS_IS_ACCESS:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD COLUMN " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize);
                                    break;
                                case eDatabaseType.DBMS_IS_INFORMIX:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD ( " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + ")";
                                    break;
                                case eDatabaseType.DBMS_IS_SQLSRVR:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " NULL";
                                    break;
                                case eDatabaseType.DBMS_IS_SYBASE:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " NULL";
                                    break;
                                case eDatabaseType.DBMS_IS_ORACLE:
                                    sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD (" +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize) + " NULL)";
                                    break;
                                case eDatabaseType.DBMS_IS_DB2:
                                    sSQL = "CREATE TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " +
                                        sFieldName.ToUpper().Trim() + " " + TableManager.TypeMap(objCon, p_iFieldType, iFieldSize);
                                    break;
                            }
                        }
                        objCmd.CommandText = sSQL;
                        objCmd.ExecuteNonQuery();
                    }   // end of the third if block




                    //add primary key entry to SUPP_DICTIONARY table
                    iFieldId = Utilities.GetNextUID(ConnectString, "SUPP_DICTIONARY", m_iClientId);

                    iHelpId = Utilities.GetNextUID(ConnectString, "HELP_DEF", m_iClientId);
                    //Start: Neha Suresh Jain, 04/21/2010, for PC LOB, insert into HIDE_LOB_PC_FLAG

                    //gagnihotri MITS 11995 Changes made for Audit table
                    //sSQL = "INSERT INTO SUPP_DICTIONARY(FIELD_ID,SEQ_NUM,SUPP_TABLE_NAME,USER_PROMPT,SYS_FIELD_NAME," +
                    //"DELETE_FLAG,FIELD_TYPE,FIELD_SIZE,REQUIRED_FLAG,IS_PATTERNED,LOOKUP_FLAG," +
                    //"CODE_FILE_ID,HELP_CONTEXT_ID,GRP_ASSOC_FLAG,NETVISIBLE_FLAG,ASSOC_FIELD,PATTERN,UPDATED_BY_USER,DTTM_RCD_LAST_UPD," +
                    //"HIDE_LOB_GC_FLAG,HIDE_LOB_WC_FLAG,HIDE_LOB_VA_FLAG,HIDE_LOB_NO_FLAG)" +
                    //"VALUES ( " + iFieldId + "," + iFieldId + ",'" + p_sSysTableName.ToUpper().Trim() + "','" +
                    //p_sUserFieldName + "','" + sFieldName + "',0," + p_iFieldType + "," +
                    //iFieldSize + "," + p_iReqd + "," + p_iIsPatterned + "," +
                    //p_iLkp + "," + p_iCodFilId + "," + iHelpId + "," + p_iGrpAss + "," + p_iNetVisible + ",'" +
                    //p_sGrpAssoField + "','" + p_sPattern + "','" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + "," + p_iLob_GC + "," + p_iLob_WC + "," +
                    //p_iLob_VA + "," + p_iLob_NOC + ")";

                    //sharishkumar Jira 6415 starts
                   // sSQL = "INSERT INTO SUPP_DICTIONARY(FIELD_ID,SEQ_NUM,SUPP_TABLE_NAME,USER_PROMPT,SYS_FIELD_NAME," +
                   //"DELETE_FLAG,FIELD_TYPE,FIELD_SIZE,REQUIRED_FLAG,IS_PATTERNED,LOOKUP_FLAG," +
                   //"CODE_FILE_ID,HELP_CONTEXT_ID,GRP_ASSOC_FLAG,NETVISIBLE_FLAG,ASSOC_FIELD,PATTERN,UPDATED_BY_USER,DTTM_RCD_LAST_UPD," +
                   //"HIDE_LOB_GC_FLAG,HIDE_LOB_WC_FLAG,HIDE_LOB_VA_FLAG,HIDE_LOB_NO_FLAG,HIDE_LOB_PC_FLAG,FAS_REPORTABLE)" +
                   //"VALUES ( " + iFieldId + "," + iFieldId + ",'" + p_sSysTableName.ToUpper().Trim() + "','" +
                   //p_sUserFieldName + "','" + sFieldName + "',0," + p_iFieldType + "," +
                   //iFieldSize + "," + p_iReqd + "," + p_iIsPatterned + "," +
                   //p_iLkp + "," + p_iCodFilId + "," + iHelpId + "," + p_iGrpAss + "," + p_iNetVisible + ",'" +
                   //p_sGrpAssoField + "','" + p_sPattern + "','" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + "," + p_iLob_GC + "," + p_iLob_WC + "," +
                   //p_iLob_VA + "," + p_iLob_NOC + "," + p_iLob_PC + "," + p_iFASreportable + ")";//asharma326 MITS 32386 p_iFASreportable

                    sSQL = "INSERT INTO SUPP_DICTIONARY(FIELD_ID,SEQ_NUM,SUPP_TABLE_NAME,USER_PROMPT,SYS_FIELD_NAME," +
                   "DELETE_FLAG,FIELD_TYPE,FIELD_SIZE,REQUIRED_FLAG,IS_PATTERNED,LOOKUP_FLAG," +
                   "CODE_FILE_ID,HELP_CONTEXT_ID,GRP_ASSOC_FLAG,NETVISIBLE_FLAG,ASSOC_FIELD,PATTERN,UPDATED_BY_USER,DTTM_RCD_LAST_UPD," +
                   "HIDE_LOB_GC_FLAG,HIDE_LOB_WC_FLAG,HIDE_LOB_VA_FLAG,HIDE_LOB_NO_FLAG,HIDE_LOB_PC_FLAG,FAS_REPORTABLE,MULTISELECT,HTMLTXTCNFG,WEB_LINK_ID)" + //WWIG GAP20A - agupta298 - MITS 36804 - added WEB_LINK_ID - JIRA - 4691
                   "VALUES ( " + iFieldId + "," + iFieldId + ",'" + p_sSysTableName.ToUpper().Trim() + "','" +
                   p_sUserFieldName + "','" + sFieldName + "',0," + p_iFieldType + "," +
                   iFieldSize + "," + p_iReqd + "," + p_iIsPatterned + "," +
                   p_iLkp + "," + p_iCodFilId + "," + iHelpId + "," + p_iGrpAss + "," + p_iNetVisible + ",'" +
                   p_sGrpAssoField + "','" + p_sPattern + "','" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + "," + p_iLob_GC + "," + p_iLob_WC + "," +
                   p_iLob_VA + "," + p_iLob_NOC + "," + p_iLob_PC + "," + p_iFASreportable + "," + p_iMultiSelect + "," + p_ihtmltextConfig + "," + iWebLinkRowId + ")";//asharma326 JIRA 6422 added p_ihtmltextConfig //WWIG GAP20A - agupta298 - MITS 36804 - added WEB_LINK_ID - JIRA - 4691
                   //sharishkumar Jira 6415 ends

                    objCmd.CommandText = sSQL;
                    objCmd.ExecuteNonQuery();

                    // npadhy - Start - RMA 6415 Need to insert the UserGroupIndicator corresponding to the Field Type
                    if (p_iFieldType == 22)
                    {
                        sSQLSrch = "INSERT INTO SUPP_USERGROUP_DICT(FIELD_ID,USER_GROUP_IND)"
                           + "VALUES ( " + iFieldId + ", " + p_iUserGroups + ")";


                        objCmd.CommandText = sSQLSrch;
                        objCmd.ExecuteNonQuery();
                    }
                    // npadhy - End - RMA 6415 Need to insert the UserGroupIndicator corresponding to the Field Type

                    // start :atavaragiri mits 26342 //
                    sSQLDisp = "SELECT DISPLAY_CAT FROM SEARCH_DICTIONARY WHERE DISPLAY_CAT='OSHA Supplemental' ";

                    objCmd.CommandText = sSQLDisp;
                    objreader = DbFactory.GetDbReader(m_sConnStr, sSQLDisp);
                    
                    
                    
                    if(!objreader.Read())
                    {
                             if (p_iFieldType == 6 && p_sSysTableName == "EVENT_X_OSHA_SUPP" )
                        {

                            sSQLSrch = "INSERT INTO SEARCH_DICTIONARY(FIELD_ID,CAT_ID,FIELD_NAME,FIELD_DESC,FIELD_TABLE,DISPLAY_CAT,CODE_TABLE,FIELD_TYPE)"
                            + "VALUES ( '" + iFieldId + "',1,'" + sFieldName + "','" + p_sUserFieldName + "','"
                            + p_sSysTableName.ToUpper().Trim() + "','OSHA Supplemental', 'YES_NO',3)";


                            objCmd.CommandText = sSQLSrch;
                            objCmd.ExecuteNonQuery();
                        }


                    } // end:atavaragiri mits 26342

                    //End: Neha Suresh Jain, 04/21/2010

                }  // end of the second if inside the try block
                else
                {
                    //MGaba2:Checking whether the field was already deleted in case of Updation
                    
                    if (p_iDelete == 0)
                    {
                        sSQL="SELECT DELETE_FLAG FROM SUPP_DICTIONARY WHERE FIELD_ID=" + p_iFieldId;
                        objCmd.CommandText = sSQL;
                        bPrevDeletedStatus = Convert.ToBoolean(objCmd.ExecuteScalar()); 
                    }

                    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                    if (p_iFieldType == 23)
                    {
                        iWebLinkRowId = p_iCodFilId;
                        p_iCodFilId = 0;
                    }
                    //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                    iFieldId = p_iFieldId;
                    iHelpId = p_iHelpId;
                    //Start: Neha Suresh Jain, 04/21/2010, for PC LOB, update field HIDE_LOB_PC_FLAG

                    //gagnihotri MITS 11995 Changes made for Audit table
                    //sSQL = "UPDATE SUPP_DICTIONARY SET USER_PROMPT='" + p_sUserFieldName + "',DELETE_FLAG=" + p_iDelete +
                    //    ",REQUIRED_FLAG=" + p_iReqd + ",IS_PATTERNED=" + p_iIsPatterned + ", LOOKUP_FLAG=" + p_iLkp +
                    //    ",CODE_FILE_ID=" + p_iCodFilId + ",HELP_CONTEXT_ID=" + iHelpId + ",GRP_ASSOC_FLAG=" + p_iGrpAss +
                    //    ",NETVISIBLE_FLAG=" + p_iNetVisible + ",ASSOC_FIELD='" + p_sGrpAssoField +
                    //    "',PATTERN='" + p_sPattern + "',UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" +
                    //    Conversion.ToDbDateTime(DateTime.Now) + ",HIDE_LOB_GC_FLAG=" + p_iLob_GC + ",HIDE_LOB_WC_FLAG=" + p_iLob_WC + ",HIDE_LOB_VA_FLAG=" + p_iLob_VA + ",HIDE_LOB_NO_FLAG=" + p_iLob_NOC + " WHERE FIELD_ID=" + p_iFieldId;

                    sSQL = "UPDATE SUPP_DICTIONARY SET USER_PROMPT='" + p_sUserFieldName + "',DELETE_FLAG=" + p_iDelete +
                      ",REQUIRED_FLAG=" + p_iReqd +",FAS_REPORTABLE=" + p_iFASreportable + ",IS_PATTERNED=" + p_iIsPatterned + ", LOOKUP_FLAG=" + p_iLkp +
                      ",CODE_FILE_ID=" + p_iCodFilId + ",HELP_CONTEXT_ID=" + iHelpId + ",GRP_ASSOC_FLAG=" + p_iGrpAss + ", HTMLTXTCNFG=" + p_ihtmltextConfig +
                      ",NETVISIBLE_FLAG=" + p_iNetVisible + ",ASSOC_FIELD='" + p_sGrpAssoField +
                      "',PATTERN='" + p_sPattern + "',UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" +
                      Conversion.ToDbDateTime(DateTime.Now) + ",HIDE_LOB_GC_FLAG=" + p_iLob_GC + ",HIDE_LOB_WC_FLAG=" + p_iLob_WC + ",HIDE_LOB_VA_FLAG=" + p_iLob_VA + ",HIDE_LOB_NO_FLAG=" + p_iLob_NOC + ",HIDE_LOB_PC_FLAG=" + p_iLob_PC + " ,WEB_LINK_ID=" + iWebLinkRowId + " WHERE FIELD_ID=" + p_iFieldId; //WWIG GAP20A - agupta298 - MITS 36804 - For WEB_LINK_ID - JIRA - 4691
                    //asharma326 MITS 32386 add p_iFASreportable
                    //asharma326 JIRA 6422 added p_ihtmltextConfig
                    //End: Neha Suresh Jain, 04/21/2010
                    objCmd.CommandText = sSQL;
                    objCmd.ExecuteNonQuery();

                    // atavaragiri mits 26342 //
                    sSQLSrch = "UPDATE SEARCH_DICTIONARY SET FIELD_DESC='" + p_sUserFieldName +"' WHERE FIELD_ID=" + p_iFieldId ;

                    objCmd.CommandText = sSQLSrch;
                    objCmd.ExecuteNonQuery();

                    // end:mits 26342
                 }        
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();


                //Update help details
                //sSQL = "DELETE FROM REQ_DEF WHERE LABEL_ID = " + iHelpId;
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();

                //sSQL = "INSERT INTO REQ_DEF (LABEL_ID,REQUIRED_FLAG) VALUES (" + iHelpId + "," + 
                //    p_iReqd + ")";
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();

                //Update glossary modification history
                //sSQL = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + Conversion.ToDbDateTime(DateTime.Now) + 
                //    "' WHERE SYSTEM_TABLE_NAME = 'REQ_DEF'" ;					
                //objCmd.CommandText = sSQL;
                //objCmd.ExecuteNonQuery();

                sSQL = "DELETE FROM SUPP_ASSOC WHERE FIELD_ID=" + iFieldId;
                objCmd.CommandText = sSQL;
                objCmd.ExecuteNonQuery();

                arrId = p_sGrpAssoArr.Split(' ');
                for (int iCtr = 0; iCtr < arrId.Length; iCtr++)
                {
                    if (arrId[iCtr] != "")
                    {
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSQL = "INSERT INTO SUPP_ASSOC(FIELD_ID,ASSOC_CODE_ID,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES (" + iFieldId + "," + arrId[iCtr] + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                        objCmd.CommandText = sSQL;
                        objCmd.ExecuteNonQuery();
                    }
                }

                //commit
                objCmd.Transaction.Commit();

                //MGaba2:Updating Hist_track_dictionary:Start    
                //zmohammad MITs 33885 Start : Admin Tracking fields not to be entered in History Tracking tables.
                if (!isAdmin)
                {
                    UpdateHistoryTrackingMetaData(p_iFieldId, p_sSysTableName.ToUpper().Trim(), p_sUserFieldName, p_iFieldId == 0 ? sFieldName : p_sSysFieldName, p_iFieldType, p_iFieldId == 0 ? iFieldSize : p_iFieldSize, p_iDelete, bPrevDeletedStatus, p_iCodFilId);
                }
                //zmohammad MITs 33885 End : Admin Tracking fields not to be entered in History Tracking tables.
                //MGaba2:End
                //Added by Shivendu for Supplemental Grid
                //bkumar33 has changed for Mits 17087
               // if (iColumnNumber != 0)
                if (iColumnNumber != 0 && sColumns!="")
                //End : bkumar33 has changed for Mits 17087
                    rColumn = UpdateGrid(iColumnNumber, sColumns, p_sSysTableName, sFieldName, p_iFieldId);

                //Added by Shivendu for Supplemental Grid
                Riskmaster.DataModel.SupplementalObject.ClearTableDefinitionCache();
            }    // end of the try block

            catch (DataException p_objEx)
            {
                //p_objErrOut.Add(Globalization.GetString("TableManager.TableField.CreateField.ExecuteException"), Globalization.GetString("TableManager.TableField.CreateField.ExecuteError"), BusinessAdaptorErrorType.PopupMessage);
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("TableField.CreateField.DataErr", m_iClientId), p_objEx);
            }
            finally
            {
                objCmd = null;
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objCache != null)
                    objCache.Dispose();
                if (objTrans != null)
                    objTrans.Dispose();
                if (objreader != null)        //Ritesh:MITS 26890
                    objreader.Dispose();
                
            }
            return true;
        }

    
        /// <summary>
        /// MGaba2:05/18/2010:Getting Database DataType on basis of RM FieldType
        /// </summary>
        /// <param name="p_iFieldType"></param>
        /// <param name="p_iFieldSize"></param>
        /// <param name="p_sDBType"></param>
        /// <param name="p_iColumnSize"></param>
        /// <param name="p_iNumericPrecision"></param>
        /// <param name="p_iNumericScale"></param>
       private void GetDbDataTypeAndSize(int p_iFieldType,int p_iFieldSize,ref string p_sDBType,ref int p_iColumnSize,ref int p_iNumericPrecision,ref int p_iNumericScale)
        {

            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_sConnStr);

            p_sDBType = string.Empty;
            p_iColumnSize = 0;
            p_iNumericPrecision = 0;
            p_iNumericScale = 0;

            switch(p_iFieldType)
               {
                   case 0:
                   case 3:
                   case 4:
                   case 10:
                   case 12:
                   case 13:
                       if(objConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                       {
                           p_sDBType = "VARCHAR2";
                       }
                       else
                       {
                           p_sDBType = "VARCHAR";
                       }
                       p_iColumnSize = p_iFieldSize;
                       break ;
                   case 1:
                   case 2:
                       if(objConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                       {
                           p_sDBType = "NUMBER";
                           p_iNumericPrecision = 0;  
                       }
                       else
                       {
                           p_sDBType = "FLOAT";
                           p_iNumericPrecision = 53;
                       }
                       p_iNumericScale = 0;
                       break;
                   case 6:
                   case 7:
                   case 8:
                   case 9:
                   case 14:
                   case 15:
                   case 16:
                       if (objConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                       {
                           p_sDBType = "NUMBER";
                       }
                       else
                       {
                           p_sDBType = "INT";
                       }
                       p_iNumericPrecision = 10;
                       p_iNumericScale = 0;
                       break;
               }
            
        }


/// <summary>
/// MGaba2:05/19/2010
/// In case History Tracking is ON,then on creation or updation of Supplemental Field,
/// an entry should be created or updated in HIST_TRACK_DICTIONARY. 
/// </summary>
/// <param name="p_iFieldId"></param>
/// <param name="p_sSysTableName"></param>
/// <param name="p_sUserFieldName"></param>
/// <param name="p_sFieldName"></param>
/// <param name="p_iFieldType"></param>
/// <param name="p_iFieldSize"></param>
/// <param name="p_iDelete"></param>
/// <param name="bPrevDeleted"></param>
/// <param name="p_iRelatedTableId"></param>
       private void UpdateHistoryTrackingMetaData(int p_iFieldId, string p_sSysTableName, string p_sUserFieldName, string p_sFieldName, int p_iFieldType, int p_iFieldSize, int p_iDelete, bool bPrevDeleted,int p_iRelatedTableId)
       {
           DbConnection objConn = null;
           string sSql = string.Empty;
           SysSettings objSysSettings = null;           
           
           int iTableId = 0;
           int iColumnId=0;
           string sDBDataType=string.Empty ;                          
           string sRelatedTableName = "";
           int iColumnSize=0 ;
           int iNumericPrecision = 0;
           int iNumericScale = 0;           
           
           try
           {             
               objSysSettings = new SysSettings(ConnectString,m_iClientId);
               objConn = DbFactory.GetDbConnection(m_sConnStr);

               //Memo and FreeText types will not be supported by History Tracking Feature
               if (p_iFieldType == 5 || p_iFieldType == 11 || p_iFieldType == 17)
                   return;                        

               objConn.Open();

               //Getting Table Id FROM HIST_TRACK_TABLES
               sSql =string.Format("SELECT TABLE_ID FROM HIST_TRACK_TABLES WHERE TABLE_NAME = {0}", Utilities.FormatSqlFieldValue(p_sSysTableName));
               iTableId = Convert.ToInt32(objConn.ExecuteScalar(sSql));              

               //Getting related Table name in case of code/entity/state fields
               switch (p_iFieldType)
               {
                   case 6:
                   case 8:
                   case 9:
                   case 14:
                   case 15:
                   case 16:
                       sSql = string.Format("SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = {0}", p_iRelatedTableId);
                       sRelatedTableName = Convert.ToString(objConn.ExecuteScalar(sSql));                   
                       break;
                   default:                       
                       break;
               }
               //MGaba2:MITS 21863: Issue with supplemental field having name in small case 
               p_sFieldName = p_sFieldName.ToUpper();

               if (p_iFieldId == 0) //New Supplemental Field
               {
                  //Getting Database DataType on basis of RMX Field Type
                   GetDbDataTypeAndSize(p_iFieldType,p_iFieldSize,ref sDBDataType,ref iColumnSize,ref iNumericPrecision,ref iNumericScale) ;     

                   //Inserting the record in HIST_TRACK_DICTIONARY
                   if (objConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                   {
                       sSql = string.Format(@"INSERT INTO HIST_TRACK_DICTIONARY(COLUMN_ID,COLUMN_NAME,USER_PROMPT,TABLE_ID,DATATYPE,
                                              COLUMN_SIZE,NUMERIC_PRECISION,NUMERIC_SCALE,RMDATA_TYPE,SYSTEM_TABLE_NAME) VALUES
                                              (HIST_TRACK_DICTIONARY_SEQ.NEXTVAL,{0},{1},{2},{3},{4},{5},{6},{7},{8})", 
                                              Utilities.FormatSqlFieldValue(p_sFieldName), Utilities.FormatSqlFieldValue(p_sUserFieldName), 
                                              iTableId, Utilities.FormatSqlFieldValue(sDBDataType), iColumnSize, iNumericPrecision,
                                              iNumericScale, p_iFieldType, Utilities.FormatSqlFieldValue(sRelatedTableName));
                   }
                   else
                   {
                       sSql = string.Format(@"INSERT INTO HIST_TRACK_DICTIONARY(COLUMN_NAME,USER_PROMPT,TABLE_ID,DATATYPE,COLUMN_SIZE,
                                               NUMERIC_PRECISION,NUMERIC_SCALE,RMDATA_TYPE,SYSTEM_TABLE_NAME) VALUES
                                               ({0},{1},{2},{3},{4},{5},{6},{7},{8})", Utilities.FormatSqlFieldValue(p_sFieldName),
                                               Utilities.FormatSqlFieldValue(p_sUserFieldName), iTableId, 
                                               Utilities.FormatSqlFieldValue(sDBDataType), iColumnSize, iNumericPrecision, 
                                               iNumericScale, p_iFieldType, Utilities.FormatSqlFieldValue(sRelatedTableName)); 
                   }                 
                  
                   objConn.ExecuteNonQuery(sSql);                  
                   
                   if (objSysSettings.HistTrackEnable)
                   {
                       ModifyAuditTable(iTableId, p_sSysTableName , p_sFieldName,p_iFieldType,p_iFieldSize);
                   }
               }
               else
               {
                   //In case of updating an existing Supplemental Field,there are three cases
                   //1.When User is simply updating the user friendly name/Code Table Name
                   //2.When user id unchecking the deleted flag
                   //3.When user is deleting the field
                                      
                   if (p_iDelete == 0)
                   {//User has only changed the user prompt name
                       if (bPrevDeleted == false)
                       {
                           //In case User is simply updating the user friendly name/Code Table Name
                           sSql = string.Format(@"UPDATE HIST_TRACK_DICTIONARY SET USER_PROMPT = {0},SYSTEM_TABLE_NAME={1}
                                                WHERE COLUMN_NAME ={2} AND TABLE_ID = {3}", Utilities.FormatSqlFieldValue(p_sUserFieldName),
                                                Utilities.FormatSqlFieldValue(sRelatedTableName),
                                                Utilities.FormatSqlFieldValue(p_sFieldName), iTableId);
                           objConn.ExecuteNonQuery(sSql);                          
                       }
                       else
                       {//User has unchecked the Deleted Flag.In this case,we have to insert entry in hist_track_dictionary
                           GetDbDataTypeAndSize(p_iFieldType, p_iFieldSize, ref sDBDataType, ref iColumnSize, ref iNumericPrecision, ref iNumericScale);                           
                           //Inserting the record in HIST_TRACK_DICTIONARY
                           if (objConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                           {
                               sSql = string.Format(@"INSERT INTO HIST_TRACK_DICTIONARY(COLUMN_ID,COLUMN_NAME,USER_PROMPT,TABLE_ID,
                                                    DATATYPE,COLUMN_SIZE,NUMERIC_PRECISION,NUMERIC_SCALE,RMDATA_TYPE,SYSTEM_TABLE_NAME)
                                                    VALUES(HIST_TRACK_DICTIONARY_SEQ.NEXTVAL,{0},{1},{2},{3},{4},{5},{6},{7},{8})",
                                                    Utilities.FormatSqlFieldValue(p_sFieldName), Utilities.FormatSqlFieldValue(p_sUserFieldName),
                                                    iTableId, Utilities.FormatSqlFieldValue(sDBDataType), iColumnSize, iNumericPrecision,
                                                    iNumericScale, p_iFieldType, Utilities.FormatSqlFieldValue(sRelatedTableName));
                           }
                           else
                           {
                               sSql = string.Format(@"INSERT INTO HIST_TRACK_DICTIONARY(COLUMN_NAME,USER_PROMPT,TABLE_ID,DATATYPE,
                                                    COLUMN_SIZE,NUMERIC_PRECISION,NUMERIC_SCALE,RMDATA_TYPE,SYSTEM_TABLE_NAME) VALUES
                                                    ({0},{1},{2},{3},{4},{5},{6},{7},{8})", Utilities.FormatSqlFieldValue(p_sFieldName), 
                                                    Utilities.FormatSqlFieldValue(p_sUserFieldName), iTableId, 
                                                    Utilities.FormatSqlFieldValue(sDBDataType), iColumnSize, iNumericPrecision, 
                                                    iNumericScale, p_iFieldType, Utilities.FormatSqlFieldValue(sRelatedTableName));
                           } 
                           objConn.ExecuteNonQuery(sSql);
                           

                           //Altering Audit table in case this field is not the part of corresponding AUDIT TABLE
                           if (objSysSettings.HistTrackEnable)
                           {
                               ModifyAuditTable(iTableId, p_sSysTableName, p_sFieldName, p_iFieldType, p_iFieldSize);
                           }
                       }                      
                   }
                   else
                   { //User has deleted that field

                       //Getting Column Id from HIST_TRACK_DICTIONARY
                       sSql = string.Format(@"SELECT COLUMN_ID FROM HIST_TRACK_DICTIONARY WHERE COLUMN_NAME ={0} AND TABLE_ID = {1}",
                                              Utilities.FormatSqlFieldValue(p_sFieldName), iTableId);
                       iColumnId = Convert.ToInt32(objConn.ExecuteScalar(sSql));                      

                       //Deleting from HIST_TRACK_DICTIONARY and HIST_TRACK_COLUMNS                      

                       sSql = string.Format("DELETE FROM HIST_TRACK_COLUMNS WHERE COLUMN_ID ={0}", iColumnId);
                       bool bSelected = Convert.ToBoolean(objConn.ExecuteNonQuery(sSql));

                       sSql = string.Format("DELETE FROM HIST_TRACK_DICTIONARY WHERE COLUMN_ID ={0}", iColumnId);
                       objConn.ExecuteNonQuery(sSql);
                       

                       //If Tracking was ON on that column,then UPDATED_FLAG of HIST_TRACK_TABLES needs to be set
                       if (bSelected)
                       {
                           sSql = string.Format(@"UPDATE HIST_TRACK_TABLES SET UPDATED_FLAG=-1,UPDATED_BY_USER='{1}',
                                                DTTM_RCD_LAST_UPD='{2}' WHERE TABLE_ID = {0} AND ENABLED_FLAG = -1", 
                                                iTableId, m_sUserName, System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                           objConn.ExecuteNonQuery(sSql);                           
                       }
                   }
               }
               objConn.Close();
           }
           catch (RMAppException p_objEx)
           {
               throw p_objEx;
           }
           catch (Exception p_objEx)
           {
               throw new RMAppException(Globalization.GetString("TableManager.UpdateHistoryTrackingMetaData.Error", m_iClientId), p_objEx);
           }
           finally
           {
               if (objConn != null)
               {
                   objConn.Dispose();
                   objConn = null;
               }
               objSysSettings = null; 
           }
       }

        /// <summary>
        /// Mgaba2:R7:In case History Tracking is ON and audit table for that table has already been created then
        /// an additional column needs to be added
        /// </summary>
        /// <param name="p_iTableId"></param>
        /// <param name="p_sTableName"></param>
        /// <param name="p_sFieldName"></param>
        /// <param name="p_iFieldType"></param>
        /// <param name="p_iFieldSize"></param>
       private void ModifyAuditTable(int p_iTableId, string p_sTableName, string p_sFieldName, int p_iFieldType, int p_iFieldSize)
       {
           string sSql = string.Empty;      
           bool bCreatedAuditTable = false;           
           string sHistTrackConnStr = string.Empty;
           int iDatabaseId = 0;
           DataSet objDS = null;
           StringBuilder sbSql = null;
           try
           {
               sSql= string.Format("SELECT CREATED_TABLE_FLAG FROM HIST_TRACK_TABLES WHERE TABLE_ID = {0}", p_iTableId);
               using (DbConnection objRMConn = DbFactory.GetDbConnection(ConnectString))
               {
                   objRMConn.Open();
                   bCreatedAuditTable = Convert.ToBoolean(objRMConn.ExecuteScalar(sSql));
                   if (bCreatedAuditTable)
                   {                       
                       if (objRMConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                       {
                           sSql = string.Format("SELECT DATABASE_ID FROM SYS.DATABASES WHERE NAME='{0}'", objRMConn.Database);
                       }
                       else
                       {
                           sSql = string.Format("SELECT USER_ID FROM USER_USERS");
                       }
                       iDatabaseId = Convert.ToInt32(objRMConn.ExecuteScalar(sSql));
                   }
                   objRMConn.Close();
               }               

               if (bCreatedAuditTable)
               {
                   sHistTrackConnStr = RMConfigurationManager.GetConnectionString("HistoryDataSource", m_iClientId);
                   using (DbConnection objAuditConn = DbFactory.GetDbConnection(sHistTrackConnStr))
                   {
                       objAuditConn.Open();
                       sSql= string.Format("SELECT * FROM {0}_HIST_{1} WHERE 0 = 1", p_sTableName, iDatabaseId);
                       objDS = DbFactory.GetDataSet(sHistTrackConnStr, sSql, m_iClientId);                                             

                       if (!objDS.Tables[0].Columns.Contains(p_sFieldName))
                       {
                           sbSql = new StringBuilder();
                           sbSql = sbSql.AppendFormat("ALTER TABLE {0}_HIST_{1} ADD {2} {3}", p_sTableName, iDatabaseId, p_sFieldName, TableManager.TypeMap(objAuditConn, p_iFieldType, p_iFieldSize));
                           if (p_iFieldType == 6)
                           {
                              sbSql = sbSql.Append(" DEFAULT 0");                               
                           }
                           sbSql = sbSql.Append(" NULL");

                           objAuditConn.ExecuteNonQuery(sbSql.ToString());                          
                       }
                       objAuditConn.Close();
                   }                   
               }             
           }
           catch (RMAppException p_objEx)
           {
               throw p_objEx;
           }
           catch (Exception p_objEx)
           {
               throw new RMAppException(Globalization.GetString("TableManager.ModifyAuditTable.Error", m_iClientId), p_objEx);
           }
       }

        /// Name		: ExtendFieldName
        /// Author		: Pankaj Chowdhury
        /// Date Created: 07/09/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Adds Field Type to the Field Name
        /// </summary>
        /// <param name="p_sFieldName">Field Name</param>
        /// <param name="p_iFieldType">Field Type</param>
        /// <returns>String containing concatenated field name</returns>
        private string ExtendFieldName(string p_sFieldName, int p_iFieldType)
        {
            int iLen = p_sFieldName.Length;
            if (iLen > 11)
                iLen = 11;
            switch (p_iFieldType)
            {
                case 0:
                    return p_sFieldName.Substring(0, iLen) + "_TEXT";
                case 1:
                    return p_sFieldName.Substring(0, iLen) + "_NUM";
                case 2:
                    return p_sFieldName.Substring(0, iLen) + "_AMT";
                case 3:
                    return p_sFieldName.Substring(0, iLen) + "_DATE";
                case 4:
                    return p_sFieldName.Substring(0, iLen) + "_TIME";
                case 5:
                    return p_sFieldName.Substring(0, iLen) + "_TXCD";
                case 6:
                    return p_sFieldName.Substring(0, iLen) + "_CODE";
                case 8:
                    return p_sFieldName.Substring(0, iLen) + "_EID";
                case 9:
                    return p_sFieldName.Substring(0, iLen) + "_STATE";
                case 10:
                case 12:
                case 13:
                //Start:Added by Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                case 18:
                case 19:
                //End:Nitin Goel for Policy Lookup for Admin Tracking,03/22/2010,MITS 20373
                    return p_sFieldName.Substring(0, iLen) + "_TEXT";
                case 11:
                    return p_sFieldName.Substring(0, iLen) + "_MEMO";
                //Added by Shivendu for Supplemental Grid
                case 17:
                    return p_sFieldName.Substring(0, iLen) + "_GRID";
                case 31:
                    return p_sFieldName.Substring(0, iLen) + "_FLAG"; // //MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 
                //sharishkumar Jira 6415 starts
                case 22:
                    return p_sFieldName.Substring(0, iLen) + "_USRGRP";
                //sharishkumar Jira 6415 ends
                case 25:
                    return p_sFieldName.Substring(0, iLen) + "_HTML";//case fro HTML text
                default:
                    return p_sFieldName.Substring(0, iLen);
            }
        }

        private void FillListWithGAFields(XmlDocument p_objXmlDoc)
        {
            string sGAFileName = "";
            string sSysTableName = "";
            string sTableName = "";
            string sFieldName = "";
            string sDesc = "";
            XmlNode objNode = null;
            XmlNode objNodGA = null;
            XmlElement objNewElm = null;

            try
            {
                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='SysTableName']");
                sSysTableName = objNode.InnerText;

                objNodGA = p_objXmlDoc.SelectSingleNode("//control[@name='SourceField']");
                objNodGA.InnerText = "";

                sGAFileName = String.Format("{0}\\RMSUPPGA.XML", UTILITY.GetAppFilesDirectory("TableMaintenance"));

                objNewElm = p_objXmlDoc.CreateElement("option");
                objNewElm.SetAttribute("value", "0");
                objNewElm.SetAttribute("fieldname", "");
                objNewElm.InnerText = "None";
                objNodGA.AppendChild(objNewElm);

                if (File.Exists(sGAFileName))
                {
                    XElement oSuppGA = XElement.Load(sGAFileName);
                    XElement oPageGA = oSuppGA.XPathSelectElement(string.Format("//page[@supptablename='{0}']", sSysTableName));
                    if (oPageGA == null)
                        return;

                    IEnumerable<XElement> oFormGAFields = from el in oPageGA.Elements("field")
                                                          orderby (string)el.Attribute("description")
                                                          select el;

                    foreach (XElement oField in oFormGAFields)
                    {
                        sFieldName = oField.Attribute("name").Value;
                        sTableName = oField.Attribute("codetable").Value;
                        sDesc = oField.Attribute("description").Value;
                        objNewElm = p_objXmlDoc.CreateElement("option");
                        objNewElm.SetAttribute("value", sTableName + "*" + sDesc);
                        objNewElm.SetAttribute("fieldname", sFieldName);
                        objNewElm.InnerText = sDesc;
                        objNodGA.AppendChild(objNewElm);
                    }
                    //mudabbir starts : added for displaying supplemental fields of type code and state in Source Field dropdown mits 36625
                    string sSql = string.Empty;
                    DbReader objRdr = null;
                    List<int> tableidlist= new List<int>();
                    List<string> sysFieldName = new List<string>();
                    List<string> sysUserPrompt = new List<string>();
                    sSql = "SELECT CODE_FILE_ID,SYS_FIELD_NAME,USER_PROMPT FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME ='" + sSysTableName + "' and FIELD_TYPE in (6,9) and DELETE_FLAG = 0";
                    objRdr = DbFactory.GetDbReader(ConnectString, sSql);

                    while (objRdr.Read())
                    {
                        tableidlist.Add(objRdr.GetInt32("CODE_FILE_ID"));
                        sysFieldName.Add(objRdr.GetString("SYS_FIELD_NAME"));
                        sysUserPrompt.Add(objRdr.GetString("USER_PROMPT"));
                    }
                    if (objRdr != null)
                        objRdr.Dispose();

                    for (int i = 0; i < tableidlist.Count; i++)
                    {
                        sSql = "SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = '"+ tableidlist[i] + "'";//codetable
                        objRdr = DbFactory.GetDbReader(ConnectString, sSql);
                        if (objRdr.Read())
                        {
                            if (!objRdr.IsDBNull(0))
                            {
                                sTableName = objRdr.GetValue(0).ToString();

                            }
                        }
                        if (objRdr != null)
                            objRdr.Dispose();


                        objNewElm = p_objXmlDoc.CreateElement("option");
                        objNewElm.SetAttribute("value", sTableName + "*" + sysUserPrompt[i]);
                        objNewElm.SetAttribute("fieldname", sSysTableName + "." + sysFieldName[i]);
                        objNewElm.InnerText = sysUserPrompt[i];
                        objNodGA.AppendChild(objNewElm);
                    }

                    //mudabbir end
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("TableManager.FillListWithGAField.Error", m_iClientId), ex);
            }
            finally
            {
            }
        }

        
        //Cache stores STATES differently
        private string PopulateValueForCode(string p_sTableName, string p_id)
        {
            string sTblName = CullTableName(p_sTableName, '.');
            switch (CullTableName(sTblName, ' '))
            {
                case "STATES":
                    return UTILITY.GetState(p_id, ConnectString, m_iClientId);
                case "ENTITY":   //25/12/2007 Abhishek, MITS 11097 - START
                    return UTILITY.GetEntityCode(p_id, ConnectString,m_iClientId);  //25/12/2007 Abhishek, MITS 11097 - END
                case "EVENT": //04/03/2009 Abhishek 
                     LocalCache objCache = new LocalCache(ConnectString,m_iClientId);
                     string sFirst = "";
                     string sLast="";
                     objCache.GetEntityInfo(long.Parse(p_id) , ref sFirst, ref sLast);
                     objCache = null;
                     return sLast + sFirst;
                default:
                     return UTILITY.GetCode(p_id, ConnectString, m_iClientId);
            }
        }
        /// <summary>
        /// Added by Shivendu for Supplemental Grid.
        /// Adds Columns to the Grid
        /// </summary>
        /// <param name="iColumnNumber">Number of Columns</param>
        /// <param name="sColumns">Columns</param>
        /// <param name="p_sSysTableName">System Table Name</param>
        /// <param name="sFieldName">System Field Name</param>
        /// <param name="iFieldId">Field ID</param>
        /// <param name="p_objErrOut">Error object</param>
        /// <returns>true/false for success/failure</returns>
        public bool UpdateGrid(int iColumnNumber, string sColumns, string p_sSysTableName, string sFieldName, int iFieldId)
        {
            string sSQL = "";
            string[] sColCaption = new string[iColumnNumber];
            long lField = 0;
            long lColNum = 0;
            long lColId = 0;
            int iDupColCount = 0;
            DbCommand objCmd = null;
            DbConnection objCon = null;
            DbReader objRdr = null;
            bool bColumnToDelete = false;

            try
            {
                sColCaption = sColumns.Split(',');
                if (iFieldId == 0)
                {
                    sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SYS_FIELD_NAME = '" + sFieldName.Trim() + "' AND SUPP_TABLE_NAME = '" + p_sSysTableName.Trim() + "'";
                    objRdr = DbFactory.GetDbReader(ConnectString, sSQL);

                    while (objRdr.Read())
                    {
                        lField = objRdr.GetInt32("FIELD_ID");
                    }
                    if (objRdr != null)
                        objRdr.Dispose();
                    if (lField == 0)
                        return false;
                }
                objCon = DbFactory.GetDbConnection(ConnectString);
                objCon.Open();
                objCmd = objCon.CreateCommand();

                for (int i = 0; i < iColumnNumber; i++)
                {

                    iDupColCount = 0;
                    if (iFieldId == 0)
                        sSQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" + lField + "' AND COL_HEADER = '" + sColCaption[i] + "' AND DELETED_FLAG <> -1";
                    else
                        sSQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" + iFieldId + "' AND COL_HEADER = '" + sColCaption[i] + "' AND DELETED_FLAG <> -1";
                    objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                    while (objRdr.Read())
                    {
                        iDupColCount++;
                    }
                    if (objRdr != null)
                        objRdr.Dispose();
                    if (iDupColCount != 0)
                        continue;
                    if (iFieldId == 0)
                        lColNum = GetNextColNum(lField);
                    else
                        lColNum = GetNextColNum(iFieldId);
                    lColId = Utilities.GetNextUID(ConnectString, "SUPP_GRID_DICT", m_iClientId);
                    if (iFieldId == 0)
                        sSQL = "INSERT INTO SUPP_GRID_DICT (GRID_DICT_ROW_ID,FIELD_ID,COL_HEADER,COL_NO,DELETED_FLAG) VALUES (" + lColId + "," + lField + ",'" + sColCaption[i] + "'," + lColNum + ",0)";
                    else
                        sSQL = "INSERT INTO SUPP_GRID_DICT (GRID_DICT_ROW_ID,FIELD_ID,COL_HEADER,COL_NO,DELETED_FLAG) VALUES (" + lColId + "," + iFieldId + ",'" + sColCaption[i] + "'," + lColNum + ",0)";
                    objCmd.CommandText = sSQL;
                    objCmd.ExecuteNonQuery();
                }
                if (iFieldId != 0)
                {
                    sSQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" + iFieldId + "' AND DELETED_FLAG <> -1";
                    objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                    while (objRdr.Read())
                    {
                        bColumnToDelete = true;
                        for (int i = 0; i < iColumnNumber; i++)
                        {

                            if (sColCaption[i].Equals(objRdr["COL_HEADER"].ToString()))
                            {
                                bColumnToDelete = false;
                                break;
                            }

                        }
                        if (bColumnToDelete)
                        {
                            sSQL = "UPDATE SUPP_GRID_DICT SET DELETED_FLAG = -1 WHERE FIELD_ID=" + iFieldId + " AND COL_HEADER='" + objRdr["COL_HEADER"].ToString() + "'";
                            objCmd.CommandText = sSQL;
                            objCmd.ExecuteNonQuery();
                            sSQL = "DELETE FROM SUPP_GRID_VALUE WHERE FIELD_ID=" + iFieldId + " AND COL_NUM =" + Conversion.ConvertStrToInteger(objRdr["COL_NO"].ToString());
                            objCmd.CommandText = sSQL;
                            objCmd.ExecuteNonQuery();

                        }
                    }
                    if (objRdr != null)
                        objRdr.Dispose();
                    //mudabbir start :  added to update Column numbers with sequential order after delete - Mits 36626
                    
                    int nColumns = 0;
                    int noOfrows = 0;
                    int j = 0, n = -1; ;
                    string strRows = string.Empty;
                    List<int> lstcols= new List<int>();
                    sSQL = "SELECT COUNT(DISTINCT ROW_NUM) FROM SUPP_GRID_VALUE WHERE FIELD_ID=" + iFieldId + "";
                    objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                    while (objRdr.Read())
                    {
                        noOfrows = objRdr.GetInt32(0);//rkaur27 : RMA-18489
                    }
                    if (objRdr != null)
                        objRdr.Dispose();
                    
                    for (int k = 0; k < noOfrows; k++)
                    {
                        
                        if (k == noOfrows - 1)
                        {
                            strRows = strRows + "" + ++n + "";
                        }
                        else
                        strRows = strRows + "" + ++n + "" + ",";
                    }
                    sSQL = "SELECT DISTINCT COL_NUM FROM SUPP_GRID_VALUE WHERE FIELD_ID= "+ iFieldId+"";
                    objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                    while (objRdr.Read())
                    {
                        lstcols.Add(Conversion.ConvertObjToInt(objRdr["COL_NUM"]));
                    }
                    if (objRdr != null)
                        objRdr.Dispose();

                    sSQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" + iFieldId + "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC";
                    objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                    while (objRdr.Read())
                    {
                        sSQL = "UPDATE SUPP_GRID_DICT SET COL_NO = '" + nColumns + "' WHERE FIELD_ID=" + iFieldId + " AND COL_HEADER='" + objRdr["COL_HEADER"].ToString() + "'";
                        objCmd.CommandText = sSQL;
                        objCmd.ExecuteNonQuery();
                        if (lstcols!= null && lstcols.Count >j)
                        {
                            sSQL = "UPDATE SUPP_GRID_VALUE SET COL_NUM = '" + nColumns + "' WHERE FIELD_ID=" + iFieldId + " AND ROW_NUM IN ( " + strRows + ")" + "AND COL_NUM =" + lstcols[j] + ""; ;
                            objCmd.CommandText = sSQL;
                            objCmd.ExecuteNonQuery();
                        }

                        nColumns++;
                        j++;
                    }
                    if (objRdr != null)
                        objRdr.Dispose();


                    //mudabbir ends
                }

                objCon.Close();
                return true;

            }
            catch (DataException p_objEx)
            {
                //p_objErrOut.Add(Globalization.GetString("TableManager.TableField.CreateField.ExecuteException"), Globalization.GetString("TableManager.TableField.CreateField.ExecuteError"), BusinessAdaptorErrorType.PopupMessage);
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                if (objCmd.Transaction != null)
                {
                    objCmd.Transaction.Rollback();
                }
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TableField.CreateField.DataErr", m_iClientId), p_objEx);
            }
            finally
            {
                objCmd = null;
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objRdr != null)
                    objRdr.Dispose();

            }
        }
        /// <summary>
        /// Added by Shivendu for Supplemental Grid.
        /// To fetch the next column number from SUPP_GRID_DICT
        /// </summary>
        /// <param name="lField">Field ID</param>
        /// <returns>Next Column Number</returns>
        public long GetNextColNum(long lField)
        {
            DbReader objRdr = null;
            string sSQL = "";
            long lColNum = 0;
            long lTemp = 0;
            int count = 0;

            try
            {

                sSQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID='" + lField + "'";
                objRdr = DbFactory.GetDbReader(ConnectString, sSQL);

                while (objRdr.Read())
                {
                    count++;
                    lTemp = objRdr.GetInt32("COL_NO");
                    if (lColNum < lTemp)
                        lColNum = lTemp;
                }

                if (count == 0)
                    return 0;
                return lColNum + 1;

            }
            catch (DataException p_objEx)
            {
                //p_objErrOut.Add(Globalization.GetString("TableManager.TableField.CreateField.ExecuteException"), Globalization.GetString("TableManager.TableField.CreateField.ExecuteError"), BusinessAdaptorErrorType.PopupMessage);
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {

                throw p_objEx;
            }
            catch (Exception p_objEx)
            {

                throw new RMAppException(Globalization.GetString("TableField.CreateField.DataErr", m_iClientId), p_objEx);
            }
            finally
            {

                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }

            }

        }
        /// <summary>
        /// Added by Shivendu for Supplemental Grid.
        /// To fetch grid parameters and column captions
        /// </summary>
        /// <param name="p_objErrOut">Error adaptor object</param>
        /// <param name="p_objXmlDocument">Output XML</param>
        /// <returns>max,min cols and column captions</returns>
        public XmlDocument GetGridInformation(XmlDocument p_objXmlDocument)
        {
            DbConnection objDbConnection = null;
            StringBuilder sbSql = null;
            DbReader objReader = null;
            XmlNode objNod = null;
            string sSQL = null;
            string sFieldName = null;
            string sSysTableName = null;
            string sExistingColumns = "";
            int iRowId = 0;
            int lField = 0;

            try
            {

                objNod = p_objXmlDocument.SelectSingleNode("/XMLCol/RowId");
                if (objNod != null)
                    iRowId = Conversion.ConvertStrToInteger(objNod.InnerText);

                objNod = p_objXmlDocument.SelectSingleNode("/XMLCol/SysFieldName");
                if (objNod != null)
                    sFieldName = objNod.InnerText;
                sFieldName = ExtendFieldName(sFieldName, 17);

                objNod = p_objXmlDocument.SelectSingleNode("/XMLCol/SysTableName");
                if (objNod != null)
                    sSysTableName = objNod.InnerText;

                if (iRowId == 0)
                {
                    sSQL = "SELECT * FROM SUPP_DICTIONARY WHERE SYS_FIELD_NAME = '" + sFieldName.Trim() + "' AND SUPP_TABLE_NAME = '" + sSysTableName.Trim() + "'";
                    objReader = DbFactory.GetDbReader(ConnectString, sSQL);
                    while (objReader.Read())
                    {
                        lField = objReader.GetInt32("FIELD_ID");
                    }
                    if (objReader != null)
                        objReader.Dispose();
                    if (lField != 0)
                    {
                        p_objXmlDocument.SelectSingleNode("/XMLCol/DuplicateField").InnerText = "Duplicate";
                        return p_objXmlDocument;
                    }

                }
                else
                {

                    sSQL = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" + iRowId + "' AND DELETED_FLAG <> -1 ORDER BY COL_NO";
                    objReader = DbFactory.GetDbReader(ConnectString, sSQL);
                    while (objReader.Read())
                    {
                        sExistingColumns = sExistingColumns + objReader["COL_HEADER"].ToString() + ",";
                    }
                    if (objReader != null)
                        objReader.Dispose();
                    if (sExistingColumns != "")
                        sExistingColumns = sExistingColumns.Remove(sExistingColumns.Length - 1);
                    p_objXmlDocument.SelectSingleNode("/XMLCol/ExistingColumns").InnerText = sExistingColumns;


                }

                sbSql = new StringBuilder();
                sbSql.Append("SELECT * FROM GRID_SYS_PARMS");
                objReader = DbFactory.GetDbReader(ConnectString, sbSql.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {

                        p_objXmlDocument.SelectSingleNode("/XMLCol/MinCols").InnerText = objReader["MIN_COLS"].ToString();
                        p_objXmlDocument.SelectSingleNode("/XMLCol/MaxCols").InnerText = objReader["MAX_COLS"].ToString();


                    }
                    if (objReader != null)
                    {
                        objReader.Dispose();
                    }
                }

                else
                {

                    p_objXmlDocument.SelectSingleNode("/XMLCol/MinCols").InnerText = "1";
                    p_objXmlDocument.SelectSingleNode("/XMLCol/MinCols").InnerText = "1";
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("INSERT INTO GRID_SYS_PARMS VALUES ('1','1','1','1')");
                    objDbConnection = DbFactory.GetDbConnection(ConnectString);
                    if (objDbConnection != null)
                    {
                        objDbConnection.Open();
                        objDbConnection.ExecuteNonQuery(sbSql.ToString());
                        objDbConnection.Close();
                    }
                }




                return p_objXmlDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TableManager.GetGridInformation.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                    objReader = null;
                }
                p_objXmlDocument = null;
                if (objDbConnection != null)
                    objDbConnection.Dispose();


            }


        }
        //Asharma326 MITS 32386 Starts
        public void UpdateAllFASFields(string p_suppTableName, string p_CheckUncheck)
        {
            try
            {
                if (p_CheckUncheck == "true")
                {
                    string sql = "UPDATE SUPP_DICTIONARY SET FAS_REPORTABLE= -1 WHERE SUPP_TABLE_NAME='" + p_suppTableName + "'";
                    DbFactory.ExecuteNonQuery(ConnectString, sql);
                }
                else
                {
                    string sql = "UPDATE SUPP_DICTIONARY SET FAS_REPORTABLE= 0 WHERE SUPP_TABLE_NAME='" + p_suppTableName + "'";
                    DbFactory.ExecuteNonQuery(ConnectString, sql);
                }
            }
            catch
            { //no need to handle
            }

        }
        //Asharma326 MITS 32386 Ends
    }
}
