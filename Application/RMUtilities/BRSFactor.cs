using System;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;   
using Riskmaster.Db; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   04th,May 2005
	///Purpose :   Factor Form 
	/// </summary>
	public class BRSFactor : UtilitiesUIBase 
	{
		/// <summary>
		/// String array for database fields mapping
		/// </summary>
		private string[,] arrFields = {
			//{"RowId", "CLINICAL_CAT_CODE"},
			{"ClinicalCat", "CLINICAL_CAT_CODE"},
			{"StartCpt", "BEGPROC"},
			{"EndCpt", "ENDPROC"},
			{"ConvFact", "FACTOR"},
			{"TableId", "TABLE_ID"}
			};

        private int m_iClientId=0;
		/// Name		: BRSFactor
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public BRSFactor(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			ConnectString = p_sConnString;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Initialize the variables and properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "WRCMP_FACTOR";
			KeyField = "CLINICAL_CAT_CODE";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: New
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Ge the structure for a new record
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>Xmldocument with structure</returns>
		public XmlDocument New(XmlDocument p_objXMLDocument)
		{
			try
			{
				return p_objXMLDocument; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFactor.New.Err", m_iClientId), p_objEx);
			}
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Get the data for a particular id
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>xml data and structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode = null;
			try
			{
				XMLDoc = p_objXmlDocument;
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
				if(objNode!=null)
					base.WhereClause = "TABLE_ID = " + objNode.InnerText;
				base.Get();
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFactor.Get.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objNode = null;
			}
		}

		/// Name		: Delete
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Deletes a particular record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Next record if any</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDocument)
		{	
			XmlNode objNode = null;		
			try
			{
				XMLDoc = p_objXmlDocument;
				objNode = p_objXmlDocument.SelectSingleNode("//TableId");
				if(objNode!=null)
					base.WhereClause = "TABLE_ID = " + objNode.InnerText;
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ClinicalCat']");
				if(objNode!=null)
					((XmlElement)objNode).SetAttribute("codeid",objNode.InnerText);  
				base.Delete();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFactor.Delete.Err", m_iClientId), p_objEx);
			}
            finally
            {
                objNode = null;
            }
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the current record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data to save it</param>
		/// <returns>Saved data alogn with its xml structure</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			int iTableId=0;
			XmlNode objNode=null;
			int iClinicalCode=0;
			string sRowId="";
			string sBegProc="";
			string sEndProc="";
			string sSQL="";
			string sShortCode="";
			double dFactor=0;
			DbReader objRdr=null;
			DbConnection objCn=null;
			LocalCache objCache=null;
			try
			{
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
				if(objNode!=null)
					sRowId= objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
				if(objNode!=null)
					iTableId = Conversion.ConvertStrToInteger(objNode.InnerText);   
				
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ClinicalCat']");
				if(objNode!=null)
					iClinicalCode = Conversion.ConvertStrToInteger(((XmlElement)objNode).GetAttribute("codeid"));

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='StartCpt']");
				if(objNode!=null)
					sBegProc = objNode.InnerText; 

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='EndCpt']");
				if(objNode!=null)
					sEndProc = objNode.InnerText; 

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ConvFact']");
				if(objNode!=null)
					dFactor = Conversion.ConvertStrToDouble(objNode.InnerText);

                objCache = new LocalCache(ConnectString, m_iClientId);
                sShortCode= objCache.GetShortCode(iClinicalCode);
				if(sShortCode=="ANES")
				{
					sBegProc="00100";
					sEndProc="01999";
				}
				else if(sShortCode=="MD-ANES")
				{
					sBegProc="10040";
					sEndProc="69990";
				}

				if(sRowId=="")
				{
					objRdr=DbFactory.GetDbReader(ConnectString,"SELECT * FROM WRCMP_FACTOR WHERE TABLE_ID=" +
						iTableId + " AND CLINICAL_CAT_CODE=" + iClinicalCode);
					if(objRdr.Read())
					{
                        throw new RMAppException(Globalization.GetString("BRSFactor.Save.DupErr", m_iClientId));  
					}  
					//Insert Query 
					sSQL="INSERT INTO WRCMP_FACTOR(TABLE_ID,CLINICAL_CAT_CODE,BEGPROC,ENDPROC,FACTOR) " +
						"VALUES( " + iTableId + "," + iClinicalCode + ",'" + sBegProc + "','" + sEndProc + "'," + dFactor + ")";
				}
				else
					//Update query
					sSQL = "UPDATE WRCMP_FACTOR SET BEGPROC='" + sBegProc + "', ENDPROC='" + sEndProc + "', FACTOR=" + dFactor + " WHERE" +
						" TABLE_ID=" + iTableId + " AND CLINICAL_CAT_CODE=" + iClinicalCode ;

				objCn= DbFactory.GetDbConnection(ConnectString);
				objCn.Open();
				objCn.ExecuteNonQuery(sSQL);
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFactor.Save.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
				if(objCache!=null)
				{
					objCache.Dispose();
				}
                if (objCn != null)
                {
                    objCn.Dispose();
                }
                
			}
		}
	}
}