using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Text;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Pankaj Chowdhury
    ///Dated   :   19th,Apr 2005
    ///Purpose :   Implements the User Privileges Setup Form
    /// </summary>
    public class UserPrivileges : IDisposable
    {
        /// <summary>
        /// Dsn ID
        /// </summary>
        int m_iDSNId = 0;

        /// <summary>
        /// Language Code for the user
        /// </summary>
        int m_iLangCode = 0;

        /// <summary>
        /// Line of Business Code
        /// </summary>
        int m_iRecordId = 0;

        /// <summary>
        /// Connection String
        /// </summary>
        string m_sConnStr = "";

        /// <summary>
        /// Security Dsn
        /// </summary>
        string m_sSecDsn = "";

        /// <summary>
        /// gagnihotri MITS 11995 Changes made for Audit table
        /// User Login Name
        /// </summary>
        string m_sUserName = "";

        /// <summary>
        /// Input/Output XmlDocument
        /// </summary>
        XmlDocument m_objXmlDoc = null;

        /// <summary>
        /// Dataset to Cache User Table
        /// </summary>
        DataSet m_objDsUserTbl = null;

        /// <summary>
        /// Dataset to Cache User Groups
        /// </summary>
        DataSet m_objDsUserGrps = null;

        private int m_iClientId = 0;

        #region Properties
        //Aman ML Change
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion

        /// Name		: UserPrivileges
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="p_sUserName">User Name</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDatabaseName">Dsn Name</param>
        public UserPrivileges(string p_sUserName, string p_sPassword, string p_sDatabaseName, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            UserLogin objUserLogin = new UserLogin(p_sUserName, p_sPassword, p_sDatabaseName, m_iClientId);
            m_iDSNId = objUserLogin.objRiskmasterDatabase.DataSourceId;
            m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sSecDsn = Security.SecurityDatabase.GetSecurityDsn(m_iClientId);
            m_sUserName = p_sUserName;      //gagnihotri MITS 11995 Changes made for Audit table
            objUserLogin = null;
        }

        /// Name		: Get
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get method returns the data for User Privileges
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml structure document</param>
        /// <returns>Xml Document with Data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            LocalCache objCache = null;
            XmlElement objLOB = null;
            XmlElement objLOBControl = null;
			//Mgaba2: R8:SuperVisory Approval
            SysSettings objSettings = null;
            XmlElement objElement = null;
            int iSelectedUserId = 0;
            int iSelectedGroupId = 0;
            XmlElement objXEle = null;
			//Mgaba2: R8:SuperVisory Approval
            string sCarrierClaim = string.Empty;
            try
            {
                m_objXmlDoc = p_objXmlDocument;
                //Mgaba2: R8:SuperVisory Approval:Start
				//Need to know whether Carrier Claims is ON or not-To change labels dynamically
                objSettings = new SysSettings(m_sConnStr, m_iClientId);
                objXEle = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='hdnAdvancedClaims']");
                if (objXEle != null)
                {
                    objXEle.InnerText = objSettings.MultiCovgPerClm.ToString();
                    //skhare7 R8 supervisory approval
                    sCarrierClaim = objXEle.InnerText;
                    //skhare7 R8 supervisory approval End
                }
               
                //Mgaba2: R8:SuperVisory Approval:End

                //Get LOB
                objLOB = (XmlElement)p_objXmlDocument.SelectSingleNode("//LineOfBusiness");
                if (objLOB.InnerText != "")
                    m_iRecordId = Conversion.ConvertStrToInteger(objLOB.InnerText);

                //Get selected user/group id
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//selecteduser");
                if (objElement.InnerText != "")
                    iSelectedUserId = Conversion.ConvertStrToInteger(objElement.InnerText);

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//selectedgroup");
                if (objElement.InnerText != "")
                    iSelectedGroupId = Conversion.ConvertStrToInteger(objElement.InnerText);

                //Load LOB Combo
                objCache = new LocalCache(m_sConnStr, m_iClientId);
                UTILITY.LoadListWithCodes(m_sConnStr, p_objXmlDocument, "LOB", objCache.GetTableId("LINE_OF_BUSINESS"), m_iClientId);
                objCache.Dispose();

                //Mgaba2: R8:SuperVisory Approval:Start

                //Considering General System Parameter Setting of selection of LOBs
                //Also in case Carrier Claims is ON: We will have only two LOBs,WC and GC
                objLOBControl = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LOB']");
                if (objLOBControl != null)
                {
                    for (int i = (objLOBControl.ChildNodes.Count - 1); i >= 0; i--)
                    {
                        switch (objLOBControl.ChildNodes[i].Attributes["value"].Value)
                        {
                            case "241":
                                if (!objSettings.UseGCLOB)
                                    objLOBControl.RemoveChild(objLOBControl.ChildNodes[i]);
                                break;
                            case "242":
                                if (!objSettings.UseVALOB || objSettings.MultiCovgPerClm.ToString() == "-1")
                                    objLOBControl.RemoveChild(objLOBControl.ChildNodes[i]);
                                break;
                            case "243":
                                if (!objSettings.UseWCLOB)
                                    objLOBControl.RemoveChild(objLOBControl.ChildNodes[i]);
                                break;
                            case "844":
                                if (!objSettings.UseDILOB || objSettings.MultiCovgPerClm.ToString() == "-1")
                                    objLOBControl.RemoveChild(objLOBControl.ChildNodes[i]);
                                break;
                            case "845":
                                if (!objSettings.UsePCLOB || objSettings.MultiCovgPerClm.ToString() == "-1")
                                    objLOBControl.RemoveChild(objLOBControl.ChildNodes[i]);
                                break;
                        }
                    }

                    //Mgaba2: R8:SuperVisory Approval:End
                    if (m_iRecordId == 0)
                    {
                        //objLOBControl = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LOB']");                   
                        m_iRecordId = Conversion.ConvertStrToInteger(((XmlElement)objLOBControl.ChildNodes[0]).GetAttribute("value"));
                    }
                }
                //Load Users and Groups
                LoadGroupUserList();

                //Load the Limit Flags
                LoadLimitFlags(m_iRecordId);

                LoadReserveCombo();

                //abansal23 MITS 17982 Users authority limits not consecutive 09/21/2009 Starts
                //Load the List Tabs
                if (iSelectedUserId > 0 || iSelectedGroupId > 0)
                {

                    string sUserGroupId = string.Empty;
                    if (iSelectedGroupId > 0)
                        sUserGroupId = string.Format("GROUP_ID ={0}", iSelectedGroupId);

                    if (iSelectedUserId > 0)
                        sUserGroupId = string.Format("USER_ID ={0}", iSelectedUserId);

                    StringBuilder sbSQL = new StringBuilder();
                    sbSQL.Append(string.Format("SELECT * FROM RESERVE_LIMITS WHERE LINE_OF_BUS_CODE = {0} ", m_iRecordId));
                    sbSQL.Append(" AND " + sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("ReserveLimitsList", sbSQL.ToString(), sCarrierClaim);

                    sbSQL.Length = 0;
                    sbSQL.Append(string.Format("SELECT * FROM PRT_CHECK_LIMITS WHERE LINE_OF_BUS_CODE = {0} ", m_iRecordId));
                    sbSQL.Append(" AND " + sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("PrintCheckLimitsList", sbSQL.ToString(), sCarrierClaim);

                    sbSQL.Length = 0;
                    sbSQL.Append(string.Format("SELECT * FROM FUNDS_LIMITS WHERE LINE_OF_BUS_CODE = {0} ", m_iRecordId));
                    sbSQL.Append(" AND " + sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("PaymentLimitsList", sbSQL.ToString(), sCarrierClaim);

                    sbSQL.Length = 0;
                    sbSQL.Append(string.Format("SELECT * FROM FUNDS_DET_LIMITS WHERE LINE_OF_BUS_CODE = {0} ", m_iRecordId));
                    sbSQL.Append(" AND " + sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("PayDetailLimitsList", sbSQL.ToString(), sCarrierClaim);

                    sbSQL.Length = 0;
                    sbSQL.Append(string.Format("SELECT * FROM CLAIM_ACC_LIMITS WHERE LINE_OF_BUS_CODE = {0} ", m_iRecordId));
                    sbSQL.Append(" AND " + sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("ClaimLimitsList", sbSQL.ToString(), sCarrierClaim);

                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT * FROM EVENT_ACC_LIMITS WHERE ");
                    sbSQL.Append(sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("EventTypeList", sbSQL.ToString(), sCarrierClaim);

                    sbSQL.Length = 0;
                    sbSQL.Append(string.Format("SELECT * FROM CL_RES_PAY_LMTS WHERE LINE_OF_BUS_CODE = {0} ", m_iRecordId));
                    sbSQL.Append(" AND " + sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("PerClaimPayLimitsList", sbSQL.ToString(), sCarrierClaim);

                    //Jira 6385- Incurred Limit
                    sbSQL.Length = 0;
                    sbSQL.Append(string.Format("SELECT * FROM CLAIM_INCURRED_LIMITS WHERE LINE_OF_BUS_CODE = {0} ", m_iRecordId));
                    sbSQL.Append(" AND " + sUserGroupId + " ORDER BY USER_ID,GROUP_ID");
                    LoadLists("ClaimsTotalIncurredLimitsList", sbSQL.ToString(), sCarrierClaim);
                    //Jira 6385- Incurred Limit ends
                }
                
                //abansal23 MITS 17982 Users authority limits not consecutive 09/21/2009 Ends

                return p_objXmlDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.Get.GetErr", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objLOB = null;
                objSettings = null;
            }
        }

        /// Name		: Save
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Public method for Saving User Privileges
        /// </summary>
        /// <param name="p_objXmlDoc">Input xml document with data</param>
        /// <returns>Saved xml document with data</returns>
        public XmlDocument Save(XmlDocument p_objXmlDoc)
        {
            XmlElement objAction = null;
            objAction = (XmlElement)p_objXmlDoc.SelectSingleNode("//actionSelectedOnScreen");

            switch (objAction.InnerText)
            {
                case "CheckBoxSelected":
                    EnableLimitsSave(p_objXmlDoc);
                    break;
                case "AddLimit":
                    AddLimit(p_objXmlDoc);
                    break;
                case "DeleteLimit":
                    DeleteLimit(p_objXmlDoc);
                    break;
            }
            return p_objXmlDoc;
        }

        private void EnableLimitsSave(XmlDocument p_objXmlDoc)
        {
            XmlElement objLOB = null;
            XmlElement objSelectedTab = null;
            int iRecordId = 0;
            string sSQL = string.Empty;
            XmlElement objEnableLimits = null;
            string sEnableLimits = string.Empty;
            //asharma326 MITS 30874 Starts
            XmlElement objEnableLimitsUser = null;
            string sEnableLimitsUser = string.Empty;
            //asharma326 MITS 30874 Ends
            DbConnection objCon = null;
            DbCommand objCmd = null;

            try
            {
                //Get LOB
                objLOB = (XmlElement)p_objXmlDoc.SelectSingleNode("//LineOfBusiness");
                if (objLOB.InnerText != "")
                    iRecordId = Conversion.ConvertStrToInteger(objLOB.InnerText);

                //get selected tab
                objSelectedTab = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectedTab");

                objEnableLimits = (XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/EnableLimits");
                if (objEnableLimits.InnerText == "true")
                    sEnableLimits = "1";
                else
                    sEnableLimits = "0";
                //asharma326 MITS 30874
                objEnableLimitsUser = (XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/EnableLimitsUsers");
                if (objEnableLimitsUser.InnerText == "true")
                    sEnableLimitsUser = "1";
                else
                    sEnableLimitsUser = "0";

                if (sEnableLimits.Equals("0") && sEnableLimitsUser.Equals("1"))
                    sEnableLimitsUser = "0";
                //Asharma326 MITS 30874 

                switch (objSelectedTab.InnerText)
                {
                    //Asharma326 Add Restricted User flags MITS 30874 Starts
                    //gagnihotri MITS 11995 Changes made for Audit table Start
                    case "ReserveLimits":
                        sSQL = "UPDATE SYS_PARMS_LOB SET RES_LMT_FLAG=" + sEnableLimits +
                            ",RES_LMT_USR_FLAG=" + sEnableLimitsUser +
                            ",UPDATED_BY_USER='" + m_sUserName +
                            "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                            " WHERE LINE_OF_BUS_CODE=" + iRecordId;
                        break;
                    case "PrintCheckLimits":
                        sSQL = "UPDATE SYS_PARMS_LOB SET PRT_CHK_LMT_FLAG=" + sEnableLimits +
                            ",PRT_CHK_LMT_USR_FLAG=" + sEnableLimitsUser +
                            ",UPDATED_BY_USER='" + m_sUserName +
                            "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                            " WHERE LINE_OF_BUS_CODE=" + iRecordId;
                        break;
                    case "PaymentLimits":
                        sSQL = "UPDATE SYS_PARMS_LOB SET PAY_LMT_FLAG=" + sEnableLimits +
                            ",PAY_LMT_USR_FLAG=" + sEnableLimitsUser +
                            ",UPDATED_BY_USER='" + m_sUserName +
                            "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                            " WHERE LINE_OF_BUS_CODE=" + iRecordId;
                        break;
                    case "PayDetailLimits":
                        sSQL = "UPDATE SYS_PARMS_LOB SET PAY_DET_LMT_FLAG=" + sEnableLimits +
                            ",PAY_DET_LMT_USR_FLAG=" + sEnableLimitsUser +
                            ",UPDATED_BY_USER='" + m_sUserName +
                            "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                            " WHERE LINE_OF_BUS_CODE=" + iRecordId;
                        break;
                    case "ClaimLimits":
                        sSQL = "UPDATE SYS_PARMS_LOB SET CLM_ACC_LMT_FLAG=" + sEnableLimits +
                              ",CLM_ACC_LMT_USR_FLAG=" + sEnableLimitsUser +
                            ",UPDATED_BY_USER='" + m_sUserName +
                            "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                            " WHERE LINE_OF_BUS_CODE=" + iRecordId;
                        break;
                    case "EventTypeLimits":
                        sSQL = "UPDATE SYS_PARMS SET EV_TYPE_ACC_FLAG =" + sEnableLimits +
                            ",EV_TYPE_ACC_USR_FLAG=" + sEnableLimitsUser +
                        ",UPDATED_BY_USER='" + m_sUserName +
                        "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now);
                        break;
                    case "PerClaimPayLimits":
                        sSQL = "UPDATE SYS_PARMS_LOB SET PER_CL_LMT_FLAG=" + sEnableLimits +
                            ",PER_CL_LMT_USR_FLAG=" + sEnableLimitsUser +
                            ",UPDATED_BY_USER='" + m_sUserName +
                            "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                            " WHERE LINE_OF_BUS_CODE=" + iRecordId;
                        break;
                     //sharishkumar Jira 6385 starts
                    case "ClaimsTotalIncurredLimits":
                        sSQL = "UPDATE SYS_PARMS_LOB SET CLM_INC_LMT_FLAG=" + sEnableLimits +
                            ",CLM_INC_LMT_USR_FLAG=" + sEnableLimitsUser +
                            ",UPDATED_BY_USER='" + m_sUserName +
                            "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                            " WHERE LINE_OF_BUS_CODE=" + iRecordId;
                        break;
                    //sharishkumar Jira 6385 ends
                    //gagnihotri MITS 11995 Changes made for Audit table End
                    //Asharma326 Add Restricted User flags MITS 30874 Starts
                }

                objCon = DbFactory.GetDbConnection(m_sConnStr);
                objCon.Open();
                objCmd = objCon.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                objCmd.CommandText = sSQL;
                objCmd.ExecuteNonQuery();
                objCon.Close();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.Save.SaveErr", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                objLOB = null;
                objSelectedTab = null;
                objEnableLimits = null;
                objCmd = null;
            }
        }


        private void AddLimit(XmlDocument p_objXmlDoc)
        {
            XmlElement objSelectedTab = null;
            DbConnection objCon = null;
            DbCommand objCmd = null;
            string sUserGroup = string.Empty;
            string[] sarrUserGroups = null;
            DbReader objRdr = null;
            string sSQL = string.Empty;
            XmlElement objLOB = null;
            XmlElement objElement = null;

            string sLOB = string.Empty;
            string sGroupId = string.Empty;

            string sUserId = string.Empty;
            string sReserveType = string.Empty;
            double dblMaxAmount = 0;

            string sClaimStatus = string.Empty;
            string sClaimType = string.Empty;
            string sAllowCreate = string.Empty;
            string sAllowAccess = string.Empty;
            string sEventType = string.Empty;

            int iSelectedUserId = 0;
            int iSelectedGroupId = 0;
            string sHasOverRideAuthority = string.Empty;    //Changes done by Gaurav for MITS 30226.
            string sPreventPayAboveLmt = string.Empty;      //Changes done by Gaurav for MITS 30226.

            //aanandpraka2:Start changes for MITS 26510
            int iCounter = 0;
            string strCodeID = string.Empty;
            string sResTypeValue = string.Empty;
            LOCParms objLOCparms;
            XmlDocument objXMLDom = null;
            //aanandpraka2:End changes
            try
            {
                //Get LOB
                objLOB = (XmlElement)p_objXmlDoc.SelectSingleNode("//LineOfBusiness");
                if (objLOB.InnerText != "")
                    sLOB = objLOB.InnerText;

                //get selected tab
                objSelectedTab = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectedTab");

                //By Amit
                //sUserGroup = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/UserGroup")).InnerText;
                //Get selected user/group id
                objElement = (XmlElement)p_objXmlDoc.SelectSingleNode("//selecteduser");
                if (objElement.InnerText != "")
                    iSelectedUserId = Conversion.ConvertStrToInteger(objElement.InnerText);

                objElement = (XmlElement)p_objXmlDoc.SelectSingleNode("//selectedgroup");
                if (objElement.InnerText != "")
                    iSelectedGroupId = Conversion.ConvertStrToInteger(objElement.InnerText);
                
                //sUserGroup = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/UserGroup")).Attributes["value"].InnerText;
                sarrUserGroups = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/SelectedUserGroup")).InnerText.Split(' ');
              
                objCon = DbFactory.GetDbConnection(m_sConnStr);
                objCon.Open();
                objCmd = objCon.CreateCommand();
                objCmd.CommandType = CommandType.Text;

                for (int i = 0; i < sarrUserGroups.Length; i++)
                {
                    if (iSelectedUserId > 0)
                    {
                        sGroupId = "0";
                        sUserId = sarrUserGroups[i];
                    }
                    else
                    {
                        sUserId = "0";
                        sGroupId = sarrUserGroups[i];
                    }


                    switch (objSelectedTab.InnerText)
                    {
                        case "ReserveLimits":
                            sReserveType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ReserveType")).InnerText;
                            dblMaxAmount = Conversion.ConvertStrToDouble(((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/MaxAmount")).InnerText);
                        //Added by Amitosh for R8 enhancement Supervisory Approval
                            sClaimType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimType")).Attributes["codeid"].Value;
                            if (sClaimType == string.Empty)
                                sClaimType = "0";

                            sSQL = "SELECT * FROM RESERVE_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                            sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RESERVE_TYPE_CODE = " + sReserveType + " AND CLAIM_TYPE_CODE = " + sClaimType; //Added by Amitosh for R8 enhancement Supervisory Approval

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objRdr.Close();
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "UPDATE RESERVE_LIMITS SET MAX_AMOUNT=" + dblMaxAmount.ToString() + ",UPDATED_BY_USER='" + m_sUserName;
                                sSQL = sSQL + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RESERVE_TYPE_CODE = " + sReserveType + " AND CLAIM_TYPE_CODE = " + sClaimType;//Added by Amitosh for R8 enhancement Supervisory Approval
                            }
                            else
                            {
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "INSERT INTO RESERVE_LIMITS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,RESERVE_TYPE_CODE,MAX_AMOUNT,UPDATED_BY_USER,DTTM_RCD_LAST_UPD,CLAIM_TYPE_CODE) VALUES(" +
                                    sGroupId + "," + sUserId + "," + sLOB + "," + sReserveType + "," + dblMaxAmount.ToString() + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + "," + sClaimType + ")";//Added by Amitosh for R8 enhancement Supervisory Approval
                            }
                            break;
                        case "PrintCheckLimits":
                            dblMaxAmount = Conversion.ConvertStrToDouble(((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/MaxAmount")).InnerText);
                            //Added by Amitosh for R8 enhancement Supervisory Approval
                            sClaimType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimType")).Attributes["codeid"].Value;
                            if (sClaimType == string.Empty)
                                sClaimType = "0";

                            sSQL = "SELECT * FROM PRT_CHECK_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                            sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND CLAIM_TYPE_CODE = " + sClaimType; //Added by Amitosh for R8 enhancement Supervisory Approval

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())       
                            {
                                objRdr.Close();
                                sSQL = "UPDATE PRT_CHECK_LIMITS SET MAX_AMOUNT=" + dblMaxAmount.ToString() + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND CLAIM_TYPE_CODE = " + sClaimType; //Added by Amitosh for R8 enhancement Supervisory Approval
                            }
                            else
                            {
                                sSQL = "INSERT INTO PRT_CHECK_LIMITS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,MAX_AMOUNT,CLAIM_TYPE_CODE) VALUES(" +
                                    sGroupId + "," + sUserId + "," + sLOB + "," + dblMaxAmount.ToString() + "," + sClaimType + ")"; //Added by Amitosh for R8 enhancement Supervisory Approval
                            }
                            break;
                        case "PaymentLimits":
                            dblMaxAmount = Conversion.ConvertStrToDouble(((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/MaxAmount")).InnerText);
                            //Added by Amitosh for R8 enhancement Supervisory Approval
                            sClaimType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimType")).Attributes["codeid"].Value;
                            if (sClaimType == string.Empty)
                                sClaimType = "0";

                            sSQL = "SELECT * FROM FUNDS_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                            sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND CLAIM_TYPE_CODE = " + sClaimType; //Added by Amitosh for R8 enhancement Supervisory Approval

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objRdr.Close();
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "UPDATE FUNDS_LIMITS SET MAX_AMOUNT=" + dblMaxAmount.ToString() + ",UPDATED_BY_USER='" + m_sUserName;
                                sSQL = sSQL + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND CLAIM_TYPE_CODE =" + sClaimType;
                            }
                            else
                            {
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "INSERT INTO FUNDS_LIMITS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,MAX_AMOUNT,UPDATED_BY_USER,DTTM_RCD_LAST_UPD,CLAIM_TYPE_CODE) VALUES(" +
                                    sGroupId + "," + sUserId + "," + sLOB + "," + dblMaxAmount.ToString() + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + "," + sClaimType +")";
                            }
                            break;
                            //sharishkumar Jira 6385 starts
                        case "ClaimsTotalIncurredLimits":
                            dblMaxAmount = Conversion.ConvertStrToDouble(((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/MaxAmount")).InnerText);
                                                       
                            sClaimType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimType")).Attributes["codeid"].Value;
                            if (sClaimType == string.Empty)
                                sClaimType = "0";

                            sSQL = "SELECT * FROM CLAIM_INCURRED_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;                            
                            sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND CLAIM_TYPE_CODE = " + sClaimType;

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objRdr.Close();

                                sSQL = "UPDATE CLAIM_INCURRED_LIMITS SET MAX_AMOUNT=" + dblMaxAmount.ToString() + ",UPDATED_BY_USER='" + m_sUserName;                               
                                sSQL = sSQL + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND CLAIM_TYPE_CODE =" + sClaimType;
                            }
                            else
                            {
                                sSQL = "INSERT INTO CLAIM_INCURRED_LIMITS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,MAX_AMOUNT,UPDATED_BY_USER,DTTM_RCD_LAST_UPD,CLAIM_TYPE_CODE) VALUES(" +
                                sGroupId + "," + sUserId + "," + sLOB + "," + dblMaxAmount.ToString() + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + "," + sClaimType +" )";                                                               
                            }
                            break;
                        //sharishkumar Jira 6385 ends
                        case "PayDetailLimits":
                            sReserveType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ReserveType")).InnerText;
                            dblMaxAmount = Conversion.ConvertStrToDouble(((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/MaxAmount")).InnerText);
                            //Added by Amitosh for R8 enhancement Supervisory Approval
                            sClaimType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimType")).Attributes["codeid"].Value;
                            if (sClaimType == string.Empty)
                                sClaimType = "0";

                            sSQL = "SELECT * FROM FUNDS_DET_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                            sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RESERVE_TYPE_CODE = " + sReserveType + " AND CLAIM_TYPE_CODE = " + sClaimType; //Added by Amitosh for R8 enhancement Supervisory Approval

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objRdr.Close();
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "UPDATE FUNDS_DET_LIMITS SET MAX_AMOUNT=" + dblMaxAmount.ToString() + ",UPDATED_BY_USER='" + m_sUserName;
                                sSQL = sSQL + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RESERVE_TYPE_CODE = " + sReserveType +" AND CLAIM_TYPE_CODE=" + sClaimType;
                            }
                            else
                            {
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "INSERT INTO FUNDS_DET_LIMITS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,RESERVE_TYPE_CODE,MAX_AMOUNT,UPDATED_BY_USER,DTTM_RCD_LAST_UPD,CLAIM_TYPE_CODE) VALUES(" +
                                    sGroupId + "," + sUserId + "," + sLOB + "," + sReserveType + "," + dblMaxAmount.ToString() + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + "," + sClaimType +")";
                            }
                            break;
                        case "ClaimLimits":
                            sClaimType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimType")).Attributes["codeid"].Value;
                            if (sClaimType == string.Empty)
                                sClaimType = "0";

                            sClaimStatus = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimStatus")).Attributes["codeid"].Value;
                            if (sClaimStatus == string.Empty)
                                sClaimStatus = "0";

                            if (((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/AllowCreate")).InnerText == "True")
                                sAllowCreate = "1";
                            else
                                sAllowCreate = "0";

                            if (((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/AllowAccess")).InnerText == "True")
                                sAllowAccess = "1";
                            else
                                sAllowAccess = "0";

                            sSQL = "SELECT * FROM CLAIM_ACC_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                            sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + sClaimType + " AND CLAIM_STATUS_CODE = " + sClaimStatus + " AND LINE_OF_BUS_CODE =" + sLOB; // edit by Rahul - mits 9772

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objRdr.Close();
                                sSQL = "UPDATE CLAIM_ACC_LIMITS SET CREATE_FLAG=" + sAllowCreate + " ,ACCESS_FLAG=" + sAllowAccess + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + sClaimType + " AND CLAIM_STATUS_CODE = " + sClaimStatus + " AND LINE_OF_BUS_CODE =" + sLOB; // edit by Rahul - mits 9772
                            }
                            else
                            {
                                sSQL = "INSERT INTO CLAIM_ACC_LIMITS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,CLAIM_TYPE_CODE,CREATE_FLAG,ACCESS_FLAG,CLAIM_STATUS_CODE) VALUES(" +
                                    sGroupId + "," + sUserId + "," + sLOB + "," + sClaimType + "," + sAllowCreate + "," + sAllowAccess + "," + sClaimStatus + ")";
                            }
                            break;
                        case "EventTypeLimits":
                            sEventType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/EventType")).Attributes["codeid"].Value;
                            if (sEventType == string.Empty)
                                sEventType = "0";

                            if (((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/AllowCreate")).InnerText == "True")
                                sAllowCreate = "1";
                            else
                                sAllowCreate = "0";

                            if (((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/AllowAccess")).InnerText == "True")
                                sAllowAccess = "1";
                            else
                                sAllowAccess = "0";

                            sSQL = "SELECT * FROM EVENT_ACC_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                            sSQL = sSQL + " AND EVENT_TYPE_CODE = " + sEventType;

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objRdr.Close();
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "UPDATE EVENT_ACC_LIMITS SET CREATE_FLAG=" + sAllowCreate + " ,ACCESS_FLAG=" + sAllowAccess + ",UPDATED_BY_USER='" + m_sUserName;
                                sSQL = sSQL + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                sSQL = sSQL + " AND EVENT_TYPE_CODE = " + sEventType;
                            }
                            else
                            {
                                //gagnihotri MITS 11995 Changes made for Audit table
                                sSQL = "INSERT INTO EVENT_ACC_LIMITS(GROUP_ID,USER_ID,EVENT_TYPE_CODE,CREATE_FLAG,ACCESS_FLAG,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES(" +
                                    sGroupId + "," + sUserId + "," + sEventType + "," + sAllowCreate + "," + sAllowAccess + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
                            }
                            break;
                        case "PerClaimPayLimits":
                            sReserveType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ReserveType")).InnerText;
                            dblMaxAmount = Conversion.ConvertStrToDouble(((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/MaxAmount")).InnerText);
                            //Added by Amitosh for R8 enhancement of OverRide Authority
                            if (((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/HasOverRideAuthority")).InnerText == "True")
                                sHasOverRideAuthority = "1";
                            else
                                sHasOverRideAuthority = "0";
                            ////End amitosh
                            //Added by Gaurav for MITS 30226.
                            if (((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/PreventPayAboveLmt")).InnerText == "True")
                                sPreventPayAboveLmt = "1";
                            else
                                sPreventPayAboveLmt = "0";
                            //End Gaurav.

                            sClaimType = ((XmlElement)p_objXmlDoc.SelectSingleNode("//" + objSelectedTab.InnerText + "/ClaimType")).Attributes["codeid"].Value;
                            if (sClaimType == string.Empty)
                                sClaimType = "0";

                            //aanandpraka2:Start changes for MITS 26510
                            ColLobSettings cLobSettings = new ColLobSettings(m_sConnStr,m_iClientId);
                            LobSettings lsettings = cLobSettings[Convert.ToInt32(sLOB)];
                            if (lsettings.ResByClmType != null)
                            {
                                if (Convert.ToString(lsettings.ResByClmType).ToLower() == "true")
                                {
                                    objLOCparms = new LOCParms(m_sConnStr, m_iClientId);
                                    objXMLDom = objLOCparms.GetReserveType(sLOB, sClaimType, true, ref strCodeID);
                                    if (objXMLDom != null)
                                    {
                                        XmlNodeList xmlNodeLst = objXMLDom.GetElementsByTagName("Reserve");
                                        if (xmlNodeLst != null)
                                        {
                                            for (iCounter = 0; iCounter < xmlNodeLst.Count; iCounter++)
                                            {
                                                //sResTypeValue = xmlNodeLst.Item(0).Attributes["value"].Value.ToString();  Ishan: MITS 28898 : resrve type not matched
                                                sResTypeValue = xmlNodeLst.Item(iCounter).Attributes["value"].Value.ToString();
                                                if (string.Compare(sResTypeValue, sReserveType) == 0)
                                                { break; }
                                            }

                                            if (iCounter == xmlNodeLst.Count)
                                            {
                                                throw new RMAppException("This reserve type cannot be linked to the selected claim type.");
                                            }
                                        }
                                    }
                                }
                            }
                            //aanandpraka2:End changes
                            sSQL = "SELECT * FROM CL_RES_PAY_LMTS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                            sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RES_TYPE_CODE = " + sReserveType;
                            sSQL = sSQL + " AND CLAIM_TYPE_CODE=" + sClaimType;

                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objRdr.Close();
                                //Added by Amitosh for R8 enhancement of OverRide Authority
                                //sSQL = "UPDATE CL_RES_PAY_LMTS SET MAX_AMOUNT=" + dblMaxAmount.ToString() + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                                //sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RES_TYPE_CODE = " + sReserveType;
                                //sSQL = sSQL + " AND CLAIM_TYPE_CODE=" + sClaimType;

                                sSQL = "UPDATE CL_RES_PAY_LMTS SET MAX_AMOUNT=" + dblMaxAmount.ToString() + ", HAS_OVERRIDE_AUTHORITY=" + sHasOverRideAuthority + ", PREVENT_PAYMENTS_ABOVE_LIMITS=" + sPreventPayAboveLmt + " WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId; //Changes done by Gaurav for MITS 30226.
                                sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RES_TYPE_CODE = " + sReserveType;
                                sSQL = sSQL + " AND CLAIM_TYPE_CODE=" + sClaimType;

                                //End Amitosh
                            }
                            else
                            {
                                //Added by Amitosh for R8 enhancement of OverRide Authority
                                //sSQL = "INSERT INTO CL_RES_PAY_LMTS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,RES_TYPE_CODE,MAX_AMOUNT,CLAIM_TYPE_CODE) VALUES(" +
                                //   sGroupId + "," + sUserId + "," + sLOB + "," + sReserveType + "," + dblMaxAmount.ToString() + "," + sClaimType + ")";

                                sSQL = "INSERT INTO CL_RES_PAY_LMTS(GROUP_ID,USER_ID,LINE_OF_BUS_CODE,RES_TYPE_CODE,MAX_AMOUNT,CLAIM_TYPE_CODE,HAS_OVERRIDE_AUTHORITY,PREVENT_PAYMENTS_ABOVE_LIMITS) VALUES(" +
                                    sGroupId + "," + sUserId + "," + sLOB + "," + sReserveType + "," + dblMaxAmount.ToString() + "," + sClaimType + "," + sHasOverRideAuthority.ToString() + "," + sPreventPayAboveLmt.ToString() + ")";     //Changes done by Gaurav for MITS 30226.
                                //End Amitosh
                            }
                            break;
                    }
                    objCmd.CommandText = sSQL;
                    objCmd.ExecuteNonQuery();
                }
                objCon.Close();
            }
            //aanandpraka2:Changes for MITS 26510-Catch block added
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            //aanandpraka2:End changes
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.Save.SaveErr", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
                objCmd = null;
            }
        }


        private void DeleteLimit(XmlDocument p_objXmlDoc)
        {
            string sSelectedId = string.Empty;
            XmlElement objSelectedTab = null;
            XmlElement objLOB = null;  // edit by Rahul - Mits 9772
            string sSQL = string.Empty;

            DbConnection objCon = null;
            DbCommand objCmd = null;

            string sUserId = string.Empty;
            string sGroupId = string.Empty;
            string sReserveType = string.Empty;
            string sClaimStatus = string.Empty;
            string sClaimType = string.Empty;
            string sEventType = string.Empty;
            string sLOB = string.Empty;
            string sCoverageType = string.Empty; //nsachdeva2 - MITS 28088 - 04/11/2012
            string[] sarrValue = null;
            string sCarrierClaim = string.Empty; //averma62 - MITS 28929

            try
            {
                //Get selected row id
                sSelectedId = ((XmlElement)p_objXmlDoc.SelectSingleNode("//LimitToDelete")).InnerText;
                sarrValue = sSelectedId.Split('|');

                //get selected tab
                objSelectedTab = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectedTab");

                objCon = DbFactory.GetDbConnection(m_sConnStr);
                objCon.Open();
                objCmd = objCon.CreateCommand();
                objCmd.CommandType = CommandType.Text;
                //Start - averma62 - MITS 28929
                sCarrierClaim = ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='hdnAdvancedClaims']")).InnerText;
                //End - averma62 - MITS 28929
                
                switch (objSelectedTab.InnerText)
                {
                    case "ReserveLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sLOB = sarrValue[2];
                        sReserveType = sarrValue[3];
                        sSQL = "DELETE FROM RESERVE_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                        sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RESERVE_TYPE_CODE = " + sReserveType;
                        //rsharma220 MITS 32711 Start
                        if (sCarrierClaim == "-1")
                        {
                            sClaimType = sarrValue[4];
                            sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + sClaimType;
                        }
                        //rsharma220 MITS 32711 End
                        break;
                    case "PrintCheckLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sLOB = sarrValue[2];
                        //nsachdeva2 - MITS 28088 - 04/11/2012
                        //Start - averma62 - MITS 28929
                        if (sCarrierClaim == "-1")
                        {
                            sCoverageType = sarrValue[3];
                            sSQL = "DELETE FROM PRT_CHECK_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId + " AND CLAIM_TYPE_CODE = " + sCoverageType;
                        }
                        else
                        {
                            sSQL = "DELETE FROM PRT_CHECK_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                        }
                        //End - averma62 - MITS 28929  
                
                        //End MITS 28088
                        sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB;
                        break;
                    case "PaymentLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sLOB = sarrValue[2];
                        sSQL = "DELETE FROM FUNDS_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                        sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB;
                        //Mgaba2:MITS 29073
                        if (sCarrierClaim == "-1")
                        {
                            sClaimType = sarrValue[3];
                            sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + sClaimType;
                        }
                        break;
                    case "PayDetailLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sLOB = sarrValue[2];
                        sReserveType = sarrValue[3];
                        sSQL = "DELETE FROM FUNDS_DET_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId + " AND LINE_OF_BUS_CODE = " + sLOB
                              + " AND RESERVE_TYPE_CODE = " + sReserveType;
                        if (sCarrierClaim == "-1")  //tmalhotra3:MITS 34256
                        {
                            sClaimType = sarrValue[4];
                            sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + sClaimType;
                        }
                        //asharma326 MITS 36838
                        //else
                        //{
                        //sReserveType = sarrValue[3];

                        //    sSQL = sSQL + " AND RESERVE_TYPE_CODE = " + sReserveType;
                        //}
                      
                        break;
                    case "ClaimLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sClaimType = sarrValue[2];
                        sClaimStatus = sarrValue[3];
                        //Get LOB - Edit by Rahul(Mits 9772)
                        objLOB = (XmlElement)p_objXmlDoc.SelectSingleNode("//LineOfBusiness");
                        if (objLOB.InnerText != "")
                            sLOB = objLOB.InnerText;

                        sSQL = "DELETE FROM CLAIM_ACC_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                        sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + sClaimType + " AND CLAIM_STATUS_CODE = " + sClaimStatus + " AND LINE_OF_BUS_CODE = " + sLOB; // edit by rahul - mits 9772
                        break;
                    case "EventTypeLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sEventType = sarrValue[2];
                        sSQL = "DELETE FROM EVENT_ACC_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                        sSQL = sSQL + " AND EVENT_TYPE_CODE = " + sEventType;
                        break;
                    case "PerClaimPayLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sLOB = sarrValue[2];
                        sReserveType = sarrValue[3];
                        sClaimType = sarrValue[4];
                        sSQL = "DELETE FROM CL_RES_PAY_LMTS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                        sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB + " AND RES_TYPE_CODE = " + sReserveType;
                        sSQL = sSQL + " AND CLAIM_TYPE_CODE=" + sClaimType;
                        break;
                    //sharishkumar Jira 6385 starts
                    case "ClaimsTotalIncurredLimits":
                        sGroupId = sarrValue[0];
                        sUserId = sarrValue[1];
                        sLOB = sarrValue[2];
                        sSQL = "DELETE FROM CLAIM_INCURRED_LIMITS WHERE GROUP_ID = " + sGroupId + " AND USER_ID = " + sUserId;
                        sSQL = sSQL + " AND LINE_OF_BUS_CODE = " + sLOB;
                        if (sCarrierClaim == "-1")
                        {
                            sClaimType = sarrValue[3];
                            sSQL = sSQL + " AND CLAIM_TYPE_CODE = " + sClaimType;
                        }
                        break;
                    //sharishkumar Jira 6385 ends
                }
                objCmd.CommandText = sSQL;
                objCmd.ExecuteNonQuery();
                objCon.Close();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.Save.SaveErr", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                objCmd = null;
            }
        }

        /// Name		: LoadGroupUserList
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Populate the Users and Groups List
        /// </summary>
        private void LoadGroupUserList()
        {
            string sSQL = "";
            DbReader objRdr = null;
            XmlElement objRowTxt = null;
            XmlElement objElm = null;
            DbConnection objConn = null;//abansal23 : MITS 14784
            string m_sDBType = "";//abansal23 : MITS 14784
            bool bCaseInSens = false;
            try
            {
                //abansal23 : MITS 14784 Start
                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();
                m_sDBType = objConn.DatabaseType.ToString();
                objConn.Close();
                //check for Oracle case sensitiveness
                if (m_sDBType == Constants.DB_ORACLE)
                {
                    bCaseInSens = IsCaseInSensitveSearch();
                }
                //Load the Group List
                if (bCaseInSens == false)
                    sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS ORDER BY GROUP_NAME";//abansal23 : MITS 14784
                else
                    sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS ORDER BY UPPER(GROUP_NAME)";//abansal23 : MITS 14784
                //abansal23 : MITS 14784 Start

                objElm = (XmlElement)m_objXmlDoc.SelectSingleNode("//Groups");
                objElm.InnerText = string.Empty; //rma 7476
                objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);

                while (objRdr.Read())
                {
                    objRowTxt = m_objXmlDoc.CreateElement("Group");
                    objRowTxt.SetAttribute("value", objRdr.GetInt32("GROUP_ID").ToString());
                    objRowTxt.InnerText = objRdr.GetString("GROUP_NAME");
                    objElm.AppendChild(objRowTxt);
                }
                objRdr.Close();

                //Load the Users List
                //abansal23 : MITS 14784 Start
                if (bCaseInSens == false)
                    sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME " +
                        " FROM USER_DETAILS_TABLE,USER_TABLE" +
                        " WHERE USER_DETAILS_TABLE.DSNID = " + m_iDSNId +
                        " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME";//abansal23 : MITS 14784
                else
                    sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME AS LAST_NAME,USER_TABLE.FIRST_NAME AS FIRST_NAME " +
                    " FROM USER_DETAILS_TABLE,USER_TABLE" +
                    " WHERE USER_DETAILS_TABLE.DSNID = " + m_iDSNId +
                    " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ORDER BY UPPER(LAST_NAME)";//abansal23 : MITS 14784
                objElm = (XmlElement)m_objXmlDoc.SelectSingleNode("//Users");
                objElm.InnerText = string.Empty; //rma 7476
                objRdr = DbFactory.GetDbReader(m_sSecDsn, sSQL);
                while (objRdr.Read())
                {
                    string sLastName = objRdr.GetString("LAST_NAME");
                    string sName = objRdr.GetString("FIRST_NAME");
                    if (sLastName != "")
                        sName = sLastName + ", " + sName;
                    objRowTxt = m_objXmlDoc.CreateElement("User");
                    objRowTxt.SetAttribute("value", objRdr.GetInt32("USER_ID").ToString());
                    objRowTxt.InnerText = sName;
                    objElm.AppendChild(objRowTxt);
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.LoadGroupUserList.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objRdr != null)
                    objRdr.Dispose();
                objRowTxt = null;
                objElm = null;
            }
        }

        /// Name		: LoadLimitFlags
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Load the limit flags list
        /// </summary>
        /// <param name="p_iLOB"></param>
        private void LoadLimitFlags(int p_iLOB)
        {
            string sSQL = "";
            DbReader objRdr = null;
            XmlElement objNode = null;

            try
            {
                //Asharma326 MITS 30874 Add Columns

                //sharishkumar Jira 6385 starts
                //sSQL = "SELECT RES_LMT_USR_FLAG,RES_LMT_FLAG,PAY_LMT_USR_FLAG,PAY_LMT_FLAG,PRT_CHK_LMT_USR_FLAG,PRT_CHK_LMT_FLAG,PAY_DET_LMT_USR_FLAG,PAY_DET_LMT_FLAG,CLM_ACC_LMT_USR_FLAG,CLM_ACC_LMT_FLAG,PER_CL_LMT_USR_FLAG,PER_CL_LMT_FLAG FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE =" + p_iLOB;
                sSQL = "SELECT RES_LMT_USR_FLAG,RES_LMT_FLAG,PAY_LMT_USR_FLAG,PAY_LMT_FLAG,PRT_CHK_LMT_USR_FLAG,PRT_CHK_LMT_FLAG,PAY_DET_LMT_USR_FLAG,PAY_DET_LMT_FLAG,CLM_ACC_LMT_USR_FLAG,CLM_ACC_LMT_FLAG,PER_CL_LMT_USR_FLAG,PER_CL_LMT_FLAG,CLM_INC_LMT_FLAG,CLM_INC_LMT_USR_FLAG FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE =" + p_iLOB;
                //sharishkumar Jira 6385 ends

                objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                if (objRdr.Read())
                {
                    //Asharma326 MITS 30874 Starts
                    //Asharma326 Reserve Limit Flag User
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblResLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("RES_LMT_USR_FLAG").ToString();

                    // Asharma326 Pay Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPaymentLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PAY_LMT_USR_FLAG").ToString();

                    //Asharma326 Print Check Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPaymentDetailLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PAY_DET_LMT_USR_FLAG").ToString();

                    //Asharma326 Claim limits flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblClaimLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("CLM_ACC_LMT_USR_FLAG").ToString();

                    //Asharma326 Per Claim Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPerClaimPaymentLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PER_CL_LMT_USR_FLAG").ToString();

                    //Asharma326 Print Check Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPrntChkLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PRT_CHK_LMT_USR_FLAG").ToString();

                    //Asharma326 MITS 30874 Ends
                    //Reserve Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblResLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("RES_LMT_FLAG").ToString();

                    //Pay Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPaymentLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PAY_LMT_FLAG").ToString();

                    //Print Check Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPaymentDetailLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PAY_DET_LMT_FLAG").ToString();

                    //Claim limits flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblClaimLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("CLM_ACC_LMT_FLAG").ToString();

                    //Per Claim Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPerClaimPaymentLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PER_CL_LMT_FLAG").ToString();

                    //Print Check Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblPrntChkLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("PRT_CHK_LMT_FLAG").ToString();

                    //sharishkumar Jira 6385 starts
                    //Claim Incurred Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblClaimsTotalIncurredLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("CLM_INC_LMT_FLAG").ToString();

                    //Claim Incurred Limit Flag
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblClaimsTotalIncurredLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("CLM_INC_LMT_USR_FLAG").ToString();
                    //sharishkumar Jira 6385 ends
                }
                objRdr.Close();

                sSQL = "SELECT EV_TYPE_ACC_USR_FLAG,EV_TYPE_ACC_FLAG FROM SYS_PARMS";
                objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                if (objRdr.Read())
                {
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblEventTypeLimits']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("EV_TYPE_ACC_FLAG").ToString();
                    //Asharma326 MITS 30874
                    objNode = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='EnblEventTypeLimitsUsers']");
                    if (objNode != null)
                        objNode.InnerText = objRdr.GetInt16("EV_TYPE_ACC_USR_FLAG").ToString();
                    //Asharma326 MITS 30874
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.LoadLimitFlags.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objRdr != null)
                    objRdr.Dispose();
                objNode = null;
            }
        }

        /// Name		: LoadReserveCombo
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Loads the reserve list to the combo
        /// </summary>
        /// <param name="p_sConStr">Connection String</param>
       
        /// <param name="p_iTableId">Table Id</param>
        private void LoadReserveCombo()
        {
            string sSQL = "";
            XmlElement objElm = null;
            XmlElement objChld = null;
            bool bResByClaimType = false;

            try
            {
                objElm = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='ReserveType']");
                if (objElm == null)
                    return;

                //MITS 10306   : Umesh
                sSQL = "SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE=" + m_iRecordId;
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    if (objRdr.Read())
                    {
                        bResByClaimType = Conversion.ConvertObjToBool(objRdr.GetValue("RES_BY_CLM_TYPE"), m_iClientId);
                    }
                }
                //Aman Made the changes in the query
                if (bResByClaimType == true)
                {
                    //Parijat:MITS 10672
                    sSQL = "SELECT DISTINCT SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_CLM_TYPE_RES, CODES_TEXT, CODES " +
                    "WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND " +
                    "CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + m_iRecordId +
                    "ORDER BY CODES_TEXT.CODE_DESC";
                }
                else
                {
                    //Parijat:MITS 10672
                    sSQL = "SELECT DISTINCT SYS_LOB_RESERVES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_LOB_RESERVES, CODES_TEXT, CODES " +
                        "WHERE SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND " +
                        "CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + m_iRecordId +
                        "ORDER BY CODES_TEXT.CODE_DESC";
                }
                StringBuilder sbSQL = new StringBuilder();
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, "RESERVE_TYPE", this.LanguageCode);//Aman ML Change
                //skhare7 R8 Enhancement supervisory approval
              
                    objChld = m_objXmlDoc.CreateElement("option");
                    objChld.SetAttribute("value", "0");
                    objChld.InnerText = "---ALL---";
                    objElm.AppendChild(objChld);
                
                //skhare7 R8 Enhancement supervisory approval
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sbSQL.ToString()))  //Aman ML Change
                {
                    while (objRdr.Read())
                    {
                        objChld = m_objXmlDoc.CreateElement("option");
                        objChld.SetAttribute("value", objRdr.GetInt32("RESERVE_TYPE_CODE").ToString());
                        objChld.InnerText = objRdr.GetString(1) + " " + objRdr.GetString(2);  //Aman ML Change
                        objElm.AppendChild(objChld);
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.LoadReserveCombo.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                objChld = null;
            }
        }

        /// Name		: LoadLists
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Loads the various Tab Lists
        /// </summary>
        /// <param name="p_sList">List</param>
        /// <param name="p_sSQL">SQL</param>
        /// //added by skhare7 p_sCarrierClaim
        private void LoadLists(string p_sList, string p_sSQL,string p_sCarrierClaim)
        {
            int iGroupId = 0;
            int iUserId = 0;
            int iID = 0;
            string sName = "";
            string sFirstName = "";
            string sUserGroupCol = "";
            DbReader objRdr = null;
            XmlNode objNode = null;
            XmlElement objElm = null;
            XmlNodeList objNodeLst = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            string sColName = string.Empty;

            LocalCache objLocalCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            //skhare7 R8 supervisory approval
            string sRowData = string.Empty;
            XmlElement objRowData = null;
            //skhare7 R8 supervisory approval End

            try
            {
                //Cache the User Table and User Groups dataset
                if (m_objDsUserTbl == null)
                    m_objDsUserTbl = DbFactory.GetDataSet(m_sSecDsn, "SELECT USER_ID,LAST_NAME,FIRST_NAME FROM USER_TABLE",m_iClientId);
                if (m_objDsUserGrps == null)
                    m_objDsUserGrps = DbFactory.GetDataSet(m_sConnStr, "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS", m_iClientId);
           
                objRdr = DbFactory.GetDbReader(m_sConnStr, p_sSQL);
                objLocalCache = new LocalCache(m_sConnStr, m_iClientId);

                while (objRdr.Read())
                {
                    objLstRow = m_objXmlDoc.CreateElement("listrow");
                    iGroupId = objRdr.GetInt32("GROUP_ID");
                    iUserId = objRdr.GetInt32("USER_ID");
                    sFirstName = string.Empty;
                    sName = string.Empty;

                    if (iGroupId > 0)
                    {
                        //Groups
                        iID = iGroupId;
                        m_objDsUserGrps.Tables[0].DefaultView.RowFilter = "GROUP_ID = " + iGroupId;
                        if (m_objDsUserGrps.Tables[0].DefaultView.Count > 0)
                            sName = m_objDsUserGrps.Tables[0].DefaultView[0]["GROUP_NAME"].ToString();
                        sUserGroupCol = "Group";
                    }
                    else
                    {
                        //Users
                        m_objDsUserTbl.Tables[0].DefaultView.RowFilter = "USER_ID = " + iUserId;
                        if (m_objDsUserTbl.Tables[0].DefaultView.Count > 0)
                        {
                            sFirstName = m_objDsUserTbl.Tables[0].DefaultView[0]["FIRST_NAME"].ToString();
                            sName = m_objDsUserTbl.Tables[0].DefaultView[0]["LAST_NAME"].ToString();
                        }
                        else
                        {
                            //If the user no longer eixsts, does not show this record
                            continue;
                        }
                        if (sFirstName != "")
                            sName = sName + ", " + sFirstName;
                        iID = iUserId;
                        sUserGroupCol = "User";
                    }

                    objNode = m_objXmlDoc.SelectSingleNode("//" + p_sList);
                    objNodeLst = objNode.FirstChild.ChildNodes;



                    //Populate the list
                    for (int i = 0; i < objNodeLst.Count - 1; i++)
                    {
                        objElm = (XmlElement)objNodeLst[i];
                        objRowTxt = m_objXmlDoc.CreateElement(objNodeLst[i].Name);

                        sColName = objNodeLst[i].Name;
                        switch (sColName)
                        {
                            case "Group":
                                objRowTxt.InnerText = sName;
                                break;
                            case "Max":
                                objRowTxt.InnerText = objRdr.GetDouble("MAX_AMOUNT").ToString();
                                break;
                            case "ResType":
                                //skhare7 R8 Supervisory Approval Enhancement


                               
                                if (p_sList == "PerClaimPayLimitsList")
                                {
                                    if (objRdr.GetInt32("RES_TYPE_CODE").ToString() == "0")
                                    {

                                        objRowTxt.InnerText = "---ALL---";

                                    }
                                    else
                                    {
                                        //rsharma220 MITS 34101 Start
                                        objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr.GetInt32("RES_TYPE_CODE").ToString()), ref sShortCode, ref sDesc, m_iLangCode);
                                        objRowTxt.InnerText = sShortCode + " " + sDesc;
                                    }
                                }
                                else
                                {
                                    if (objRdr.GetInt32("RESERVE_TYPE_CODE").ToString() == "0")
                                    {

                                        objRowTxt.InnerText = "---ALL---";

                                    }
                                    else
                                    {
                                        objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr.GetInt32("RESERVE_TYPE_CODE").ToString()), ref sShortCode, ref sDesc, m_iLangCode);
                                        objRowTxt.InnerText = sShortCode + " " + sDesc;
                                    }
                                }
                                      
                                    
                                
                               
                                //skhare7 R8 Supervisory Approval Enhancement End

                                break;
                            case "ClaimType":
                                //objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr.GetInt32("CLAIM_TYPE_CODE").ToString()), ref sShortCode, ref sDesc);
                                //objRowTxt.InnerText = sShortCode + " " + sDesc;
                               //Added by Amitosh for R8 enhancement of supervisory approval
                                if (objRdr.GetInt32("CLAIM_TYPE_CODE").ToString() == "0")
                                {
                                    objRowTxt.InnerText = "--ALL--";
                                }
                                else
                                {
                                    objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr.GetInt32("CLAIM_TYPE_CODE").ToString()), ref sShortCode, ref sDesc, m_iLangCode);
                                    objRowTxt.InnerText = sShortCode + " " + sDesc;
                                }
                                break;
                            case "ClaimStatus":
                                //vsharma205-Jira-RMA-19478 Starts----When we do not enter claim status in Claim limit, It is not displaying "ALL" in the grid
                                if (objRdr.GetInt32("CLAIM_STATUS_CODE").ToString() == "0")
                                {
                                    objRowTxt.InnerText = "--ALL--";
                                }
                                else
                                {
                                //vsharma205-Jira-RMA-19478 Ends
                                    objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr.GetInt32("CLAIM_STATUS_CODE").ToString()), ref sShortCode, ref sDesc, m_iLangCode);
                                    objRowTxt.InnerText = sShortCode + " " + sDesc;
                                }//vsharma205-Jira-RMA-19478 Starts-----vsharma205-Jira-RMA-19478 Ends//
                                break;
                            case "Access":
                                if (objRdr.GetInt16("ACCESS_FLAG").ToString() == "1" || objRdr.GetInt16("ACCESS_FLAG").ToString() == "-1")
                                    objRowTxt.InnerText = "Yes";
                                else
                                    objRowTxt.InnerText = "No";
                                break;
                            case "Create":
                                if (objRdr.GetInt16("CREATE_FLAG").ToString() == "1" || objRdr.GetInt16("CREATE_FLAG").ToString() == "-1")
                                    objRowTxt.InnerText = "Yes";
                                else
                                    objRowTxt.InnerText = "No";
                                break;
                            case "EventType":
                                objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRdr.GetInt32("EVENT_TYPE_CODE").ToString()), ref sShortCode, ref sDesc, m_iLangCode);
                                //rsharma220 MITS 34101 End
                                objRowTxt.InnerText = sShortCode + " " + sDesc;
                                break;
                                //Added by Amitosh for R8 enhancement of OverRide Authority
                            case "OverRideAuthority":
                                  if (objRdr.GetInt16("HAS_OVERRIDE_AUTHORITY").ToString() == "1" )
                                      objRowTxt.InnerText = "Yes";
                                  else
                                      objRowTxt.InnerText = "No";
                                break;
                            case "PreventPaymentAboveLimit":
                                if (objRdr.GetInt16("PREVENT_PAYMENTS_ABOVE_LIMITS").ToString() == "1")
                                      objRowTxt.InnerText = "Yes";
                                  else
                                      objRowTxt.InnerText = "No";
                                  break;
                        }
                        objLstRow.AppendChild(objRowTxt);
                    }

                    objRowTxt = m_objXmlDoc.CreateElement("RowId");
                    //skhare7 R8 supervisory approval
                    if (string.Compare(p_sCarrierClaim, "0") == 0)
                    {
                        switch (p_sList)
                        {
                            case "ReserveLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("RESERVE_TYPE_CODE").ToString();
                                                   

                                break;
                            case "PrintCheckLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString();
                                                   
                                break;
                            case "PaymentLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString();
                                break;
                            case "PayDetailLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    //nsachdeva2 - MITS:27371 - 02/08/2012 
                                                    objRdr.GetInt32("RESERVE_TYPE_CODE").ToString();
                                break;
                            case "ClaimLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_STATUS_CODE").ToString();
                                break;
                            case "EventTypeList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("EVENT_TYPE_CODE").ToString();
                                break;
                            case "PerClaimPayLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("RES_TYPE_CODE").ToString() +"|" +
                                                   objRdr.GetInt32("CLAIM_TYPE_CODE").ToString() +"|"+
                                                     objRdr.GetInt16("HAS_OVERRIDE_AUTHORITY").ToString() + "|" + //Added by Amitosh for R8 enhancement of OverRide Authority
                                                    objRdr.GetInt16("PREVENT_PAYMENTS_ABOVE_LIMITS").ToString();
                                break;
                                //sharishkumar Jira 6385 starts
                            case "ClaimsTotalIncurredLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString();//syadav55 Jira 6385                                                    
                                break;
                            //sharishkumar Jira 6385 ends
                        }
                    }
                    else

                    { 
                    
                     switch (p_sList)
                        {
                            case "ReserveLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("RESERVE_TYPE_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString();

                                break;
                            case "PrintCheckLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString();
                                break;
                            case "PaymentLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString();
                                break;
                            case "PayDetailLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("RESERVE_TYPE_CODE").ToString() + "|" +//asharma326 MITS Paydetailslist 36838
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString(); 
                                break;
                            case "ClaimLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_STATUS_CODE").ToString();
                                break;
                            case "EventTypeList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("EVENT_TYPE_CODE").ToString();
                                break;
                            case "PerClaimPayLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("RES_TYPE_CODE").ToString() + "|" +
                                                    objRdr.GetInt32("CLAIM_TYPE_CODE").ToString() + "|" +
                                                     objRdr.GetInt16("HAS_OVERRIDE_AUTHORITY").ToString() + "|" + //Added by Amitosh for R8 enhancement of OverRide Authority
                                                     objRdr.GetInt16("PREVENT_PAYMENTS_ABOVE_LIMITS").ToString();
                                break;
                            //sharishkumar Jira 6385 starts
                            case "ClaimsTotalIncurredLimitsList":
                                objRowTxt.InnerText = objRdr.GetInt32("GROUP_ID").ToString() + "|" +
                                                    objRdr.GetInt32("USER_ID").ToString() + "|" +
                                                    objRdr.GetInt32("LINE_OF_BUS_CODE").ToString() + "|" +
                                                     objRdr.GetInt32("CLAIM_TYPE_CODE").ToString(); //syadav55 Jira 6385                                                     
                                break;
                            //sharishkumar Jira 6385 ends
                        }
                    }
                    
                    
                    

                    objLstRow.AppendChild(objRowTxt);


                    objNode.AppendChild((XmlNode)objLstRow);
                    sRowData = sRowData + "," + objRowTxt.InnerText;

                }  //skhare7 R8 supervisory approval end
                if (m_objXmlDoc.SelectSingleNode("//" + p_sList + "RowData") == null)
                {
                    objRowData = m_objXmlDoc.CreateElement(p_sList + "RowData");
                    m_objXmlDoc.SelectSingleNode("//displaycolumn").AppendChild((XmlNode)objRowData);
                    objRowData.InnerText = sRowData;
                }
               


                
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UserPrivileges.LoadLists.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
                objNode = null;
                objElm = null;
                objNodeLst = null;
                objLstRow = null;
                objRowTxt = null;
            }
        }


        #region IDisposable Members

        /// Name		: Dispose
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/19/2005
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Public Dispose
        /// </summary>
        public void Dispose()
        {
            if (m_objDsUserGrps != null)
                m_objDsUserGrps.Dispose();
            if (m_objDsUserTbl != null)
                m_objDsUserTbl.Dispose();
            m_objXmlDoc = null;
        }

        #endregion

        #region Case Sensitiveness
        /// Name		: IsCaseInSensitveSearch
        /// Author		: Sumeet Rathod
        /// Date Created: 11/29/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Finds the case sensitiveness for Oracle database
        /// </summary>
        /// <returns>boolean value indicating case sensitiveness</returns>
        private bool IsCaseInSensitveSearch()
        {
            int iResult;
            string sSQL = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT * FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnStr, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iResult = objReader.GetInt("ORACLE_CASE_INS");
                        objReader.Close();
                        if (iResult == 1 || iResult == -1)
                            return true;
                    }
                }
            }
            //Riskmaster.Db is throwing following exception so need to catch the same
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UserPrilileges.IsCaseInSensitveSearch.DataError", m_iClientId), p_objException);
            }

            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }
            }
            return false;
        }
        #endregion
    }
}
