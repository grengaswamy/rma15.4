﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	/// Summary description for ActivityLogAdaptor.
	/// </summary>
 
	public class LSSInterfaceLog
	{
		private string m_sConnectionString = "";
        private int m_iLangCode=0;
        private int m_iClientId = 0;
		public LSSInterfaceLog(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}
        //tmalhotra3 ML Changes
        public int LanguageCode 
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
		private XmlDocument GetData(string p_sDataType)
		{
			XmlDocument objType = null;
			XmlElement objTmpElement = null;
			DbReader objReader = null;
			LocalCache objCache = null;
			string sCode = "";
			string sDesc = "";
			string sSQL = "";
			objType = new XmlDocument();
            try
            {
                objTmpElement = objType.CreateElement(p_sDataType);
                objType.AppendChild(objTmpElement);

                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                switch (p_sDataType)
                {
                    case "PacketType":
                        sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("LSS_PACKETTYPE");
                        break;
                    case "RequestType":
                        sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("LSS_REQUESTTYPE");
                        break;
                    case "Status":
                        sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("LSS_STATUS");
                        break;
                }
                objCache.Dispose();

                objTmpElement = objType.CreateElement("option");
                objTmpElement.InnerText = "";
                objTmpElement.SetAttribute("value", "");
                objType.FirstChild.AppendChild(objTmpElement);
                if (sSQL != "")
                {
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        objTmpElement = objType.CreateElement("option");

                        objCache.GetCodeInfo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId), ref sCode, ref sDesc, this.LanguageCode);
                        //objTmpElement.InnerText = sCode + " " + sDesc;
                        objTmpElement.InnerText = sDesc;
                        objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                        objType.FirstChild.AppendChild(objTmpElement);
                    }
                   
                }
                return objType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objType = null;
                objTmpElement = null;
                if (objReader!= null)
                    objReader.Dispose();
                if(objCache != null)
                    objCache.Dispose();
            }
		}

		public XmlDocument Search(XmlDocument p_objSearchParam)
		{
			XmlElement objElement = null;
			XmlDocument objXmlDocument = null;
			XmlElement objTmpElement = null;

			DataSet objDS = null;
			DbReader objReader=null;
			string sSQL = "";
			string sTmpParam = "";

			string sCode = "";
			string sCodeDesc = "";
			string sTmp = "";
			int iTmp = 0;
			bool bLSSSupported = true;

			try
			{
				objXmlDocument=new XmlDocument();

				objElement=objXmlDocument.CreateElement("SearchResults");


				//check if LSS Interface DBUpgrade has been run on database or not. if not then it will return blank result.
				try
				{
					objDS = DbFactory.GetDataSet(m_sConnectionString,"SELECT COUNT(*) FROM DX_REQUEST", m_iClientId);
				}
				catch(Exception objExp)
				{
					bLSSSupported = false;
				}
				finally
				{
					if (objDS != null)
						objDS.Dispose();
					
				}

				if(bLSSSupported == true)
				{
					if (p_objSearchParam.SelectSingleNode("//FirstTime").InnerText!="1")
					{
						sSQL = "SELECT REQ.REQUEST_ID, REQ.PACKET_TYPE, REQ.REQUEST_TYPE, REQ.REQUEST_STATUS, REQ.DTTM_RCD_ADDED, REQ.PACKET, EVE.EVENT_ID, EVE.RESULT_XML ";
						sSQL = sSQL + " FROM DX_REQUEST REQ, DX_REQUEST_EVENT EVE WHERE REQ.REQUEST_ID = EVE.REQUEST_ID";

						sTmpParam = p_objSearchParam.SelectSingleNode("//RequestId").InnerText;
						if (sTmpParam != "")
						{
							sSQL = sSQL + " AND REQ.REQUEST_ID like '" + sTmpParam.Replace("'", "''").Replace("*", "%") + "'";
						}

						sTmpParam = p_objSearchParam.SelectSingleNode("//PacketType").InnerText;
						if (sTmpParam != "")
						{
							sSQL = sSQL + " AND REQ.PACKET_TYPE = '" + sTmpParam.Replace("'", "''") + "'";
						}

						sTmpParam = p_objSearchParam.SelectSingleNode("//RequestType").InnerText;
						if (sTmpParam != "")
						{
							sSQL = sSQL + " AND REQ.REQUEST_TYPE = '" + sTmpParam.Replace("'","''") + "'";
						}

						sTmpParam = p_objSearchParam.SelectSingleNode("//Status").InnerText;
						if (sTmpParam != "")
						{
							sSQL = sSQL + " AND REQ.REQUEST_STATUS = '" + sTmpParam.Replace("'","''") + "'";
						}

						sTmpParam = p_objSearchParam.SelectSingleNode("//FromDate").InnerText;
						if (sTmpParam != "")
						{
							sSQL = sSQL + " AND REQ.DTTM_RCD_ADDED >= '" + Conversion.GetDate(sTmpParam.Replace("'","''")) + "000000'";
						}

						sTmpParam = p_objSearchParam.SelectSingleNode("//ToDate").InnerText;
						if (sTmpParam!="")
						{
							sSQL = sSQL + " AND REQ.DTTM_RCD_ADDED <= '" + Conversion.GetDate(sTmpParam.Replace("'","''")) + "999999'";
						}

						sSQL = sSQL + " ORDER BY REQ.REQUEST_ID DESC";

                        LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId);
						objDS = DbFactory.GetDataSet(m_sConnectionString,sSQL, m_iClientId);
						foreach(DataRow dr in objDS.Tables[0].Rows)
						{
							sCode = "";
							sCodeDesc = "";
							sTmp = "";
							iTmp = 0;

							objTmpElement=objXmlDocument.CreateElement("row");
							objTmpElement.SetAttribute("RequestId", Conversion.ConvertObjToStr(dr["REQUEST_ID"]));

							objTmpElement.SetAttribute("EventId", Conversion.ConvertObjToStr(dr["EVENT_ID"]));

							objCache.GetCodeInfo(Conversion.ConvertObjToInt(dr["PACKET_TYPE"], m_iClientId), ref sCode, ref sCodeDesc, this.LanguageCode);
							objTmpElement.SetAttribute("PacketType", sCodeDesc);

							objCache.GetCodeInfo(Conversion.ConvertObjToInt(dr["REQUEST_TYPE"], m_iClientId), ref sCode, ref sCodeDesc,this.LanguageCode);
							objTmpElement.SetAttribute("RequestType", sCodeDesc);

							objCache.GetCodeInfo(Conversion.ConvertObjToInt(dr["REQUEST_STATUS"], m_iClientId), ref sCode, ref sCodeDesc,this.LanguageCode);
							objTmpElement.SetAttribute("Status", sCodeDesc);

							if (Conversion.ConvertObjToStr(dr["DTTM_RCD_ADDED"]).Length == 14)
							{
								objTmpElement.SetAttribute("Date", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(dr["DTTM_RCD_ADDED"]), "d"));
								objTmpElement.SetAttribute("Time", Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr(dr["DTTM_RCD_ADDED"]).Substring(8,6), "t"));
							}
							else
							{
								objTmpElement.SetAttribute("Date", "");
								objTmpElement.SetAttribute("Time", "");
							}

							objTmpElement.SetAttribute("RequestPacket", Conversion.ConvertObjToStr(dr["PACKET"]).Trim().Replace("'", "#?#%?%#?#"));

                            //gagnihotri 05/11/2009 MITS 16393
                            objTmpElement.SetAttribute("ResponsePacket", Conversion.ConvertObjToStr(dr["RESULT_XML"]).Trim().Replace("'", "#?#%?%#?#"));

							sTmp = "";
							objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT EVENT_STATUS FROM DX_EVENTHISTORY WHERE EVENT_ID = " + Conversion.ConvertObjToStr(dr["EVENT_ID"]) + " ORDER BY HISTORY_ID");
							while (objReader.Read())
							{
								objCache.GetCodeInfo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId), ref sCode, ref sCodeDesc,this.LanguageCode);
								if(sTmp.Equals(""))
									sTmp = sCodeDesc;
								else
									sTmp = sTmp + "\\n" + sCodeDesc;
							}
							objReader.Close();
							objReader = null;
							objTmpElement.SetAttribute("Events", sTmp.Trim().Replace("'", "&apos;"));


							sTmp = "";
							objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT EXCEPTION_MSG FROM DX_EVENTEXCEPTIONS WHERE EVENT_ID = " + Conversion.ConvertObjToStr(dr["EVENT_ID"]));
							if (objReader.Read())
							{
								sTmp = Conversion.ConvertObjToStr(objReader.GetValue(0));
							}
							objReader.Close();
							
							objTmpElement.SetAttribute("IsException", sTmp.Trim().Equals("")?"No":"Yes");
							objTmpElement.SetAttribute("ExceptionsMsg", sTmp.Trim().Replace("'", "&apos;"));

							objElement.AppendChild(objTmpElement);
						}
						
					}
				}

				objXmlDocument.AppendChild(objElement);
				XmlNode objNode = objXmlDocument.ImportNode((XmlNode)GetData("PacketType").SelectSingleNode("//PacketType"), true);
				objXmlDocument.FirstChild.AppendChild(objNode);
				objNode = objXmlDocument.ImportNode((XmlNode)GetData("RequestType").SelectSingleNode("//RequestType"), true);
				objXmlDocument.FirstChild.AppendChild(objNode);
				objNode = objXmlDocument.ImportNode((XmlNode)GetData("Status").SelectSingleNode("//Status"), true);
				objXmlDocument.FirstChild.AppendChild(objNode);
				return objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LSSInterfaceLog.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objXmlDocument = null;
				objElement = null;
                objTmpElement = null;
				if (objDS != null)
					objDS.Dispose();
                if (objReader != null)
                    objReader.Dispose();
			}
		}
	}
    
}
