/**********************************************************************************************
 *   Date     |  JIRA/MITS   | Programmer | Description                                        *
 **********************************************************************************************
 * 03/14/2014 | 35039        | pgupta93   | Changes req for search code description on lookup
 * 7/24/2014  | RMA-718      | ajohari2   | Changes for TPA Access ON/OFF
 **********************************************************************************************/
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.IO;
using System.Collections;//Sumit
using Riskmaster.Settings;//Sumit
using Riskmaster.Security.Encryption;//Sumit
using System.Text;//Sumit
using System.Collections.Generic;
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   07th,Apr 2005
	///Purpose :   Implements the System Parameter form
	/// </summary>
	public class SystemParameter: UtilitiesBase 
	{
        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;
		/// <summary>
		/// Two dimensional array for the fields mapping
		/// </summary>
		private string[,] arrFields = {
			//First Tab - System Settings
			//General Parameters Tab
			{"RowId","ROW_ID"},
			{"EvPrefix","EV_PREFIX"},								
			{"EvCaptionLevel","EV_CAPTION_LEVEL"},
			//Added OrgTimeZone Enhancement for R7: Yatharth
			//{"OrgTimeZoneLevel","ORG_TIMEZONE_LEVEL"},
			{"EvIncludeYearFlag","EV_INC_YEAR_FLAG"},
			{"AutoSelectPolicy","AUTO_SELECT_POLICY"},
			{"AutoNumberVeh","AUTO_NUM_VIN"},
			{"ListSmDir","SM_LIMIT_DIRECT"},
			{"DefaultTime","DEFAULT_TIME_FLAG"},
			{"DefaultDept","DEFAULT_DEPT_FLAG"},
			//{"UseEnhPol","USE_ENH_POL_FLAG"},//Sumit - 09/21/2009 MITS#18227 Moved to LOB Parameters Setup screen
			{"SmtpSvr","SMTP_SERVER"},
			{"P2Url","P2_URL"},
			//{"FinHis","FIN_HIST_EVAL"},Asif Change done for r5
			{"P2Login","P2_LOGIN"},
			{"P2Pass","P2_PASS"},
			{"UseTnE","USETANDE"},
			{"RateOrgLvl","RATE_LEVEL"},
			{"BankAccount","TANDEACCT_ID"},
			{"CPUrl","CLAIMANT_PROCESS_URL"},
			{"CPUserID","CLAIMANT_PROCESS_LOGIN"},
			{"CPPass","CLAIMANT_PROCESS_PASS"},
			{"BillingFlag","USE_BILLING_FLAG"},
			{"ClaimantProcFlag","USE_CLAIMANT_PROC_FLAG"},
			{"UseAcrosoftInterface","USE_ACROSOFT_INTERFACE"},
			{"MultipleInsurer","MULTIPLE_INSURER"}, // Mihika MITS 8656 - Added node for 'Multiple Insurers'
			{"PolicyDropDown","POLICY_DROP_DOWN"}, // Mihika MITS 8657 - Added node for 'Policy DropDown"
			{"DeleteAllClaimDiaries","DELETE_ALL_CLAIM_DIARIES"},// Pankaj MITS 8658 - Added node for 'Delete all claim diaries"
			{"OracleCaseIns","ORACLE_CASE_INS"},  //abisht MITS 10526
			//New checkboxes Added in R5 Start Asif
			 {"MultipleReInsurer","MULTIPLE_REINSURER"},
			 {"UsePrgrmType","USE_PRGM_TYPE"},
			 {"UseBrokerBES","USE_BROKER_BES"},
			  {"AutoLaunchDiary","AUTO_LAUNCH_DIARY"},//For Diary
		 //Not needed anymore : Changed by Gagan
			{"DefAssignedTo","DEFAULT_ASSIGNED_TO"},
			//New checkboxes Added in R5 End
            {"DenDiaries","REVIEWED_DIARIES_DEN"},//Add by kuladeep for mits:25083    
			//Funds Settings Tab
			{"AccOwnrFnds","ACC_LINK_FLAG"},
			{"SubBankAcc","USE_SUB_ACCOUNT"},
			{"AssignSubAcc","ASSIGN_SUB_ACCOUNT"},
			{"AccDepositBal","NO_SUB_DEP_CHECK"},
			{"FltrOthrPpl","FILTER_OTH_PEOPLE"},
			{"ShowResOrphns","RES_SHOW_ORPHANS"},
			{"PrvntBckDtng","PREV_RES_BACKDATING"},
			{"PrvntModfyngRes","PREV_RES_MODIFYZERO"},
			{"PrvntBelowPaidRes","PREV_RES_BELOWPAID"},
			{"ReCalcResLoad","RES_DO_NOT_RECALC"},
			{"UseMasterBankAcc","USE_MAST_BANK_ACCT"},
		   //stara Mits 16667 05/14/09
			{"UseResFilter","USE_RES_FILTER"},
			//ends here--stara 05/14/09
			//{"OrderBankAcct","ORDER_BANK_ACC"},Asif Change done for r5
            //Animesh Inserted for RMSC Bill Review Fees //skhare7 RMSC Merge 28397
            {"BillReviewFee","USE_BILL_REV_FEE"},
            {"OrgBillRevFee","ORG_BILL_REV_FEE"},
             {"OrgBillLevelEntityId","ORG_BILL_REV_FEE_EID"},
            //Animesh Insertion Ends //skahre7 RMSC merge
			{"ClaimLetterFlag","CLM_LTR_FLAG"}, //added by Navdeep for Chubb
			{"AutoFROIACORDFlag","AUTO_FROIACORD_FLAG"}, //added by Navdeep for Chubb - Auto FROI ACORD
			{"PhysicianSearchFlag","HOSP_PHYS_ANY_ENT"},
			//Employee Settings Tab
			//Settings Group
			{"MasterBankAcc","WC_EMP_PREFIX"},
			{"AutoNumBankEmp","AUTO_NUM_WC_EMP"},
			{"ExcludeHolidays","EXCL_HOLIDAYS"},
			{"AllowEditWCEmp","EDIT_WC_EMP_NUM"},
			//Work Shifts Group
			{"Shift1Name","SHIFT_1_NAME"},
			{"Shift1Start","SHIFT_1_START"},
			{"Shift1End","SHIFT_1_END"},
			{"Shift2Name","SHIFT_2_NAME"},
			{"Shift2Start","SHIFT_2_START"},
			{"Shift2End","SHIFT_2_END"},
			{"Shift3Name","SHIFT_3_NAME"},
			{"Shift3Start","SHIFT_3_START"},
			{"Shift3End","SHIFT_3_END"},
			//Work Days Group
			{"WorkSun","WORK_SUN"},
			{"WorkMon","WORK_MON"},
			{"WorkTue","WORK_TUE"},
			{"WorkWed","WORK_WED"},
			{"WorkThu","WORK_THU"},
			{"WorkFri","WORK_FRI"},
			{"WorkSat","WORK_SAT"},

			//Diaries / Text Fields Tab
			//Diary Settings Group
			{"AttchDiaryVis","ATTACH_DIARY_VIS"},
			{"AllowDiaryPeek","ALLOW_GLOBAL_PEEK"},
			{"DiaryOrgLvl","DIARY_ORG_LEVEL"},

			//Free Text Settings Group										  
			{"DateStampText","DATE_STAMP_FLAG"},
			{"FreezeFreeText","FREEZE_TEXT_FLAG"},
			{"AllowEditDateStamp","FREEZE_DATE_FLAG"},
			{"FreezEventDesc","FREEZE_EVENT_DESC"},
			{"FreezLocDesc","FREEZE_LOC_DESC"},
			{"DateEventDesc","DATE_EVENT_DESC"},
			//Adjuster Text Settings Group
			{"AllowEditDateAdjuster","DATE_ADJUSTER_FLAG"},
			{"UseFlWCMaxRate","USE_FL_WC_MAX_FLAG"},
			//Nadim Enhance Notes Setting for Enhancement 12820           
			{"EnhanceNotesPrintOrder1","ENH_PRINT_ORDER1"},
			{"EnhanceNotesPrintOrder2","ENH_PRINT_ORDER2"},
			{"EnhanceNotesPrintOrder3","ENH_PRINT_ORDER3"},
			//Nadim Enhance Notes Setting for Enhancement 12820
			//Changed by Gagan for MITS 12334 : Start
			//Freeze Enhanced Notes
			{"FreezeExistingNotes","FRZ_EXISTING_NOTES"},
			//Changed by Gagan for MITS 12334 : End
			 //Gagan Safeway Retrofit Policy Jursidiction : START
			{"ShowJurisdiction","SHOW_JURISDICTION"},
			 //Gagan Safeway Retrofit Policy Jurisdiction : END
			//MITS 14204 : Umesh
			{"AllowTaskDescriptionFilter","ALLOW_TD_FILTER"},
			{"EnhNoteTime","ENH_NOTE_TIME_LIMIT"},//Parijat:Post Editable Enhanced Note
			{"EnhNoteEditRight","ENH_NOTE_EDT_RIGHTS"},//Parijat:Post Editable Enhanced Note
			//Raman 03/08/2009 : R6 Work Loss / Restriction
			{"AutoCrtWrkLossRest","CREATE_WL_RES_RCDS"},
			//{"TaskManagerDiary","TASK_MGR_DIARY"},     
			{"UseLegacyComments","USE_LEGACY_COMMENTS"},     
			//Added Rakhi for R7:Add Emp Data Elements
			//{"UseMultipleAddresses","USE_MUL_ADDRESSES"} ,
			////Added Rakhi for R7:Add Emp Data Elements
			//{"UseOutlook","USE_OUTLOOK"}//Added by Shivendu for Outlook email setting
			{"RMXLSSEnable","RMX_LSS_ENABLE"},
            //nsachdeva2: MITS: 23654: 09/19/2011
            {"FreezeDiaryCompletionDate", "FREEZE_DIARY_COMPLETION_DT"},
            //End MITS: 23654

             {"StratawareConsumerUrl", "CONSUMERURL"}, //mbahl3 strataware enhancement
            {"StratwareServiceUrl", "SERVICEURL"} //mbahl3 strataware enhancement
		};


		private Hashtable arrSystemFields;
        private string strUserName = string.Empty; // PSHEKHAWAT 09/25/2012 Added to track user for PSO excluded Event types entry
        private int intUserId = 0; // Ashish Ahuja Added To Change IgnoreSsn Check value on the basis of userid
		/// Name		: SystemParameter
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/07/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor accepting Connection String
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
		public SystemParameter(string p_sConnString, int p_iClientId):base(p_sConnString,p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

        public SystemParameter(string p_sConnString, string p_sUserName, int p_iClientId)
            : base(p_sConnString, p_sUserName,p_iClientId)
		{
			ConnectString = p_sConnString;
            strUserName = p_sUserName; // PSHEKHAWAT 09/25/2012 Added to track user for PSO excluded Event types entry
            m_iClientId = p_iClientId;
			this.Initialize();
		}
        //Ashish Ahuja
        public SystemParameter(string p_sConnString, int p_sUserId, int p_iClientId)
            : base(p_sConnString, p_sUserId,p_iClientId)
        {
            ConnectString = p_sConnString;
            intUserId = p_sUserId;
            m_iClientId = p_iClientId;
            this.Initialize();
        }
		#region System Parameter Add-On Modules
		//Start(06/24/2010) - Sumit - Commenting as Password will be different for all clients.
		//private const string USE_CASE_MGT_KEY = "CSCRMACTIVATECMGTKEY03";
		//private const string SCRIPT_EDITOR_KEY = "CSCRMSEDITORACTIVATECMGTKEY";
		//Start(06/23/2010) - Sumit -  Following key will be used for GC LOB.
		private const string ENHANCED_POLICY_KEY = "CSCRMENHPOLACTIVATE03";
		//private const string ACROSOFT_INTERFACE_KEY = "CSCRMACTIVATEASKEY";
		//End - Sumit
		#endregion

		//Start(07/01/2010) - Sumit
		#region System Parameter Modules Names
		private const string USE_CASE_MGT_KEY = "USECGMT";
        private const string USE_CARRIER_CLAIMS_KEY = "USECRRCLM";
		private const string USE_SCRIPT_EDITOR_KEY = "USESCTEDITOR";
		private const string USE_ACROSOFT_INTERFACE_KEY = "USEMCM";
		private const string USE_ENHANCED_POLICY_KEY = "USEENHPOL";
		private const string USE_BRS_KEY = "USEBRS";
		private const string USE_MASTER_KEY = "USEMASTER";
        private const string USE_PSO_REPORTING_KEY = "USEPSOREPSYS"; //spahariya MITS 25646 activation code for PSO reporting system
		#endregion
		//End - Sumit
        //Mona:PaperVisionMerge:Animesh Inserted 
        private const string USE_PAPERVISION_KEY = "USEPAPERVISION";
        //Animesh Insertion ends
        //rbhatia4:R8: Use Media View Setting : July 05 2011
        private const string USE_MEDIA_VIEW_INTERFACE_KEY = "USEMEDIAVIEW";
		//Start(07/01/2010) - Sumit
		#region Line of Business variables
		private const string USE_GC_LOB_KEY = "USEGCLOB";
		private const string USE_DI_LOB_KEY = "USEDILOB";
		private const string USE_PC_LOB_KEY = "USEPCLOB";
		private const string USE_VA_LOB_KEY = "USEVALOB";
		private const string USE_WC_LOB_KEY = "USEWCLOB";
		#endregion
		//End - Sumit

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/07/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the variables
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_PARMS";
			KeyField = "ROW_ID";
			base.InitFields(arrFields);
			InitSystemFields();
			base.Initialize();
		}

		private void InitSystemFields()
		{
			arrSystemFields = new Hashtable();
			//Added Rakhi for R7:Add Emp Data Elements
			arrSystemFields.Add("UseMultipleAddresses", "USE_MUL_ADDRESSES");
			//Added by Shivendu for Outlook email setting
			arrSystemFields.Add("UseOutlook", "USE_OUTLOOK");
			arrSystemFields.Add("FileSizeLimitForOutlook", "SIZE_LIMIT_OUTLOOK");
			//Added by Yatharth for Time Zone setting
			arrSystemFields.Add("OrgTimeZoneLevel", "ORG_TIMEZONE_LEVEL");
			// Added by Alok for Task Manager Diary
			arrSystemFields.Add("TaskManagerDiary", "TASK_MGR_DIARY");
            arrSystemFields.Add("TaskManagerEmail", "TASK_MGR_EMAIL");
			//Mridul 10/26/09 MITS#18230
			arrSystemFields.Add("AutoNumPin", "AUTO_NUM_PIN");

			//Start: Debabrata Biswas 06/02/2010 MITS 20606
			arrSystemFields.Add("UseEntityApproval", "USE_ENTITY_APPROVAL");
			arrSystemFields.Add("ShowLSSInvoice", "SHOW_LSS_INVOICE");
			//End: Debabrata Biswas 06/02/2010 MITS 20606

			//START (06/29/2010): Sumit -Filter Policy based on LOB
			arrSystemFields.Add("UsePolicyLOBFilter", "USE_POLICY_LOB");
			//End: Sumit

			//START (07/02/2010): Sumit -Enable BRS 
			arrSystemFields.Add("UseBRS", "USE_BRS");
			//End: Sumit

			//START (07/02/2010): Sumit -Enable LOB 
			arrSystemFields.Add("UseGCLOB", "USE_GC_LOB");
			arrSystemFields.Add("UseDILOB", "USE_DI_LOB");
			arrSystemFields.Add("UsePCLOB", "USE_PC_LOB");
			arrSystemFields.Add("UseVALOB", "USE_VA_LOB");
			arrSystemFields.Add("UseWCLOB", "USE_WC_LOB");
			//End: Sumit

            ////Mona:PaperVisionMerge:Animesh Inserted For PaperVision Interface.
            arrSystemFields.Add("UsePaperVisionInterface", "USE_PAPERVISION");
            //Animesh insertion Ends
			arrSystemFields.Add("ResultQuickLkpUp", "QCKLKPUP_USER_LIMIT");

			//OFAC Check Setting Added
			arrSystemFields.Add("DoOfacCheck", "DO_OFAC_CHECK");

			//Records Per Page for Payee Check Review Screen Setting
			arrSystemFields.Add("ResultPayeeCheckReview", "RECORDS_PAYEE_REVIEW");

			//Added by mcapps2 to allow void of cleared payments
			arrSystemFields.Add("AllowVoidClearedPmts", "ALLOW_VOID_CLRD_PMTS");

            //rsushilaggar MITS 21970 DATE 10/01/2010
            arrSystemFields.Add("LSSChecksOnHold", "LSS_CHECKS_ON_HOLD");
			//rupal:start, r8 enh to add person involved as payee type
            arrSystemFields.Add("AddPersonInvolvedAsPayeeType", "PI_AS_PAYEE_TYPE");                 
			//rupal:end

            //Bkuzhanthaim : MITS-36026/RMA-338 : To add OrgHierarchy as Payee Type in Transaction Screen : Start
            arrSystemFields.Add("AddOrgHierarchyAsPayeeType", "ORG_HIER_PAYEE_TYPE");
            //Bkuzhanthaim : MITS-36026/RMA-338 : End
            arrSystemFields.Add("IsPayeeAddressSelect", "PAYEE_ADDRESS_SELECT");//pkandhari JIRA 6421
           
			arrSystemFields.Add("FreezeSuppFreeText", "FREEZE_SUP_TEXT_FLAG");
            arrSystemFields.Add("FreezePayOrderFreeText", "FREEZE_PAY_TEXT_FLAG");//jramkumar for MITS 32095
            arrSystemFields.Add("DateStampHTMLText", "DATE_STAMP_HTML");//asharma326 JIRA 6422 HTML Field

            // npadhy Start MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
            arrSystemFields.Add("MaxDiscTier", "MAX_DISC_TIER");
            arrSystemFields.Add("MaxDiscounts", "MAX_DISCOUNTS");
            // npadhy End MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
            arrSystemFields.Add("MultiCovgPerClm", "MULTI_COVG_CLM");
            arrSystemFields.Add("UnitStat", "UNIT_STAT");
            arrSystemFields.Add("AutoPopulateDpt", "AUTO_POPU_DPT");
            arrSystemFields.Add("AutoFillDpteid", "AUTO_POPU_DPT_ID");
            arrSystemFields.Add("EnhNotePolicyInClaim", "ENH_NOTE_POL_CLAIM");//williams-neha goel 07/21/2010::MITS 21704
            arrSystemFields.Add("BaseCurrencyType", "BASE_CURRENCY_CODE");//Deb MultiCurr
            arrSystemFields.Add("UseMultiCurrency", "USE_MULTI_CURRENCY");
             //Added by amitosh for mits 23476 (05/11/2011)
            arrSystemFields.Add("UseClaimantName", "USE_CLAIMANT_NAME");
            //Added by averma62 rmA-VSS integration
            arrSystemFields.Add("EnableVSS", "ENABLE_VSS");
            arrSystemFields.Add("AdjAssignmentAutoDiary", "ADJ_ASIGN_AUTO_DIARY");     //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            //Ankit Start : Worked on MITS - 32386 - FAS Integration
            arrSystemFields.Add("EnableFAS", "ENABLE_FAS");
            arrSystemFields.Add("FileLocationSelection", "FILE_LOCATION");
            arrSystemFields.Add("SharedLocation", "SHARED_LOCATION");
            arrSystemFields.Add("FASServer", "FAS_SERVER");
            arrSystemFields.Add("FASUserId", "FAS_USER_ID");
            arrSystemFields.Add("FASPassword", "FAS_PASSWORD");
            arrSystemFields.Add("FASFolder", "FAS_FOLDER");
            //Ankit End
            //rbhatia4:R8: Use Media View Setting : July 05 2011
            arrSystemFields.Add("UseMediaViewInterface", "USE_MEDIA_VIEW");
            arrSystemFields.Add("EnableClaimActivityLog", "CLAIM_ACT_LOG"); //nsachdeva2: mits:26418 - claim activity log
            //Added by Amitosh for R8 enhancement of Policy interface
            arrSystemFields.Add("UsePolicyInterface", "USE_POLICY_INTERFACE");
            //mbahl3 mits 30224
            arrSystemFields.Add("StatusActive", "STATUS_ACTIVE");
            //mbahl3 mits 30224
            //mbahl3 Strataware Enhancement
            arrSystemFields.Add("StrataWare", "USE_STRATAWARE");
            arrSystemFields.Add("EnableSingleuser", "ENABLESINGLEUSER");
			 arrSystemFields.Add("StratawareUserID", "STRATAWARE_USER_ID");
           
            //mbahl3 Strataware Enhancement
            arrSystemFields.Add("AllowPolicySearch", "ALLOW_POLICY_SEARCH");   //averma62 MITS 25163- Policy Interface Implementation
            //arrSystemFields.Add("UploadSuppToPolicy", "UPLOAD_SUPPTO_POLICY");
            arrSystemFields.Add("UseCodeMapping", "USE_CODE_MAPPING");
            // Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
            arrSystemFields.Add("UploadCheckTotal", "UPLOAD_CHECK_TOTAL");
            arrSystemFields.Add("EnableTPAAccess", "Enable_TPA_Access"); //JIRA RMA-718 ajohari2
            //nsachdeva2:  MITS 25163- Policy Interface Implementation - 1/6/2012 - commented not required
            //arrSystemFields.Add("MaxPolUnits", "MAX_POL_UNITS");
            //arrSystemFields.Add("MaxPolEntities", "MAX_POL_ENTITIES");
            //End MITS 25163
            //skhare7 For Multilingual R8 enhancement
            //arrSystemFields.Add("Country", "COUNTRY_ID");
            //arrSystemFields.Add("PhoneFormat", "PHONENUMBER_FORMAT");
            //arrSystemFields.Add("ZipcodeFormat", "ZIPCODE_FORMAT");
            //arrSystemFields.Add("Dateformat", "DATE_FORMAT");
            //arrSystemFields.Add("Timeformat", "TIME_FORMAT");
            //skhare7 For Multilingual R8 enhancement End
            //smishra54: Point Policy Interface
            arrSystemFields.Add("PolicySearchCount", "POLICY_SEARCH_COUNT"); 
 			arrSystemFields.Add("ShowMediaViewButton", "SHOW_MEDIA_VIEW");
            arrSystemFields.Add("OpenPointPolicy", "OPEN_POINT_POLICY");
            arrSystemFields.Add("AddClaimantPI", "ADD_CLAIMANT_PI"); //hlv MITS 29356 11/13/12
            //spahariya MITS 28867 Add adjuster as Person Involved
            arrSystemFields.Add("AddAdjAsPI", "ADD_ADJ_AS_PI"); 
			//igupta3 MITS# 32846 -Multi diary selection
            arrSystemFields.Add("CurrentDateForDiaries", "USE_CUR_DATE_DIARY");
            arrSystemFields.Add("NotifyAssignerOfCompletion", "NOTIFY_ASSIGNER");
			//Added By Nitika for FNOL Reserve
			arrSystemFields.Add("FNOLReserve", "FNOL_RESERVE");
            arrSystemFields.Add("SearchCodeDescription", "SEARCH_CODE_DESC");//MITS:35039
            arrSystemFields.Add("ClaimantLength", "CLAIMANT_LENGTH");//Mits 36708
            arrSystemFields.Add("AllowNotesAtClaimant", "ENH_NOTE_CLMNT");/*gbindra WWIG MITS#34104 01312014*/
            arrSystemFields.Add("NoReqFieldsForPolicySearch", "NO_REQ_FLD_POLSEARCH"); //neha goel MITS# 33414 for WWIG 11212013
            arrSystemFields.Add("UseStagingPolicySystem", "USE_STAGING_POL_SYS"); //neha goel MITS# 33414 for WWIG 11212013
            arrSystemFields.Add("UseEntityRole", "USE_ENTITY_ROLE"); //skhare7 JIRS 340 entity role enh
            //added by nitin goel, MITS 33588,07/29/2014
            //Start: Changed by Sumit Agarwal to use for InsuredClaimDepartment: 09/23/2014: MITS 33588
            //arrSystemFields.Add("UseClaimDept", "CLAIM_DEPT_FLAG");
            arrSystemFields.Add("UseInsuredClaimDept", "INS_CLM_DEPT_FLAG");
            //End: Changed by Sumit Agarwal to use for InsuredClaimDepartment: 09/23/2014: MITS 33588
            //added by swati for TPA
            arrSystemFields.Add("UseTPA", "USE_TPA");
            //added by swati for DCI and CLUE process type
            arrSystemFields.Add("UseDCIReportingFields", "USE_DCI_FIELDS");
            arrSystemFields.Add("UseCLUEReportingFields", "USE_CLUE_FIELDS");
            //change end here by swati
            arrSystemFields.Add("UseNMVTISReqFields", "USE_NMVTIS_FIELDS");//Added By agupta298 - PMC GAP08 - NMVTIS Reporting - RMA-4694
			
		//tanwar2 - ImageRight
            arrSystemFields.Add("UseImgRight", "USE_IMAGE_RIGHT");
            arrSystemFields.Add("UseImgRightWebService", "USE_IMAGE_RIGHT_WS");
            arrSystemFields.Add("AllowAutoLogin", "ALLOW_AUTO_LOGIN");  //Added:Yukti, JIRA 5504
            arrSystemFields.Add("AddModifyAddress", "ADD_ADDRESS_FLAG");  //RMA-8753 nshah28 
            arrSystemFields.Add("AllowSummaryForBookedReserve", "ALW_SMR_BOOKED_RES");  //RMA-16584 nshah28
			arrSystemFields.Add("UseSilverlight", "USE_SILVERLIGHT");//Added by mmudabbir 36022- 06/19/2014 
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/07/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get method returns the data
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlElement objElm = null;
			XmlElement objTempElement = null;//Sumit - 09/21/2009 MITS#18227
			DbConnection objCon = null;
            XmlDocument objDocTemp = null; // PSHEKHAWAT
            XmlNode objNode = null; // PSHEKHAWAT
            string strTemp = string.Empty;
			int iDbData=0;
			int iMin=0;
			int iMax=0;
			int iRecmnd=0;
			int iFinHisEval = 0;//Asif added For RadioButton
			int iStateEval = 0;//added by rkaur7 for polciy changes in Utility
			int iOrderBankAccount = 0;
			int iUseLegacyComments = 0;  
			bool blnIsTaskExist = false;
			bool blnSuccess = false;
            string smtpserver = string.Empty;
			TaskManager objTaskMgr = null;
            //skhare7 R8 MultiLingual
            //int iCountry = 0;
            MultiLanguage objML = null;
            UserLogin objUserlogin = null;
            bool bReturn = false;
			 //mbahl3 Strataware Enhancement
			try
			{
				XMLDoc = p_objXmlDocument;

				if(p_objXmlDocument.SelectSingleNode("//control[@name='hdAction']").InnerText=="SubBankAcct")
					return LoadTandEBankAcc(p_objXmlDocument);

				if(p_objXmlDocument.SelectSingleNode("//control[@name='hdAction']").InnerText=="FinHistChange")
					return FinHistChange(p_objXmlDocument);
                //skhare7 r8 MulitLingual
                //LoadCountry(p_objXmlDocument);
              
                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='Country']");

                //if (objElm != null)
                //{
                //    iCountry = Conversion.CastToType<int>(objElm.GetAttribute("value").ToString(), out blnSuccess);
                //    if (iCountry != 0)
                //    {
                //        if (p_objXmlDocument.SelectSingleNode("//control[@name='hdAction']").InnerText == "CountryChanged")
                //        {

                            //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='ZipcodeFormat']");
                            //if (objElm != null)
                            //{
                            //    objElm.SetAttribute("value", "0");
                            //}
                            //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='PhoneFormat']");
                            //if (objElm != null)
                            //{
                            //    objElm.SetAttribute("value", "0");
                            //}
                            //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='Dateformat']");
                            //if (objElm != null)
                            //{
                            //    objElm.SetAttribute("value", "0");
                            //}
                            //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='Timeformat']");
                            //if (objElm != null)
                            //{
                            //    objElm.SetAttribute("value", "0");
                            //}
                //            return MultiLingualFormats(p_objXmlDocument, iCountry);



                //        }
                   
                           
                //    }
                //}
				base.Get();

                GetSystemSettings(p_objXmlDocument);
                //if (objElm != null)
                //{
                //    iCountry = Conversion.CastToType<int>(objElm.GetAttribute("value").ToString(), out blnSuccess);
                //    if (iCountry != 0)
                //    {
                //        MultiLingualFormats(p_objXmlDocument, iCountry);
                    
                    
                //    }
                //}

                 // psharma206  smtp cloud start
                smtpserver = RMConfigurationManager.GetAppSetting("SMTPServer");
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SmtpSvr']");
                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true")
                {
                    objElm.InnerText = smtpserver;
                }
                // psharma206  smtp cloud end

				objCon =  DbFactory.GetDbConnection(ConnectString);
				objCon.Open();
				//abisht MITS 10526
				if (objCon.DatabaseType.ToString() != Constants.DB_ORACLE)
				{
					objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='OracleCaseIns']");
					if (objElm != null)
					{
						objElm.SetAttribute("hideExceptOracle", "true");
						objElm.RemoveAttribute("title");
					}
				}
				else
				{
					objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='OracleCaseIns']");
					if (objElm != null)
					{
						objElm.SetAttribute("hideExceptOracle", "false");
					}
				}
				//Asif Start For Financial history radiobutton
				objElm = null;
				iFinHisEval = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT FIN_HIST_EVAL FROM SYS_PARMS WHERE ROW_ID=1"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/FinHis");
				if (objElm != null)
				{
						objElm.InnerText =iFinHisEval.ToString();
				}
				//Asif End For financial history Radiobutton

				//rkaur7 Start For State radiobutton
				objElm = null;
				iStateEval = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STATE_EVAL FROM SYS_PARMS WHERE ROW_ID=1"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/State");
				if (objElm != null)
				{
					objElm.InnerText = iStateEval.ToString();
				}
				//rkaur7 End 
				//Asif Start For Order BankAccount radiobutton
				objElm = null;
				iOrderBankAccount = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT ORDER_BANK_ACC FROM SYS_PARMS WHERE ROW_ID=1"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/OrderBankAcct");
				if (objElm != null)
				{
					objElm.InnerText = iOrderBankAccount.ToString();
				}
				//Asif End For For Order BankAccount radiobutton
                //Ashish Ahuja :Start

                objElm = null;
                string data = objCon.ExecuteString("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + intUserId + "");
                if (data != null && data != "")
                {
                    XmlDocument dataXmlDoc = new XmlDocument();
                    dataXmlDoc.LoadXml(data);

                    XmlElement dataElement = (XmlElement)dataXmlDoc.SelectSingleNode("setting");
                    if (dataElement["IgnoreSSNChecking"] != null && dataElement["IgnoreSSNChecking"].InnerText != null)
                    {
                        iDbData = Conversion.ConvertStrToInteger(dataElement["IgnoreSSNChecking"].InnerText);
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='IgnoreSSNChecking']");
                        if (objElm != null)
                        {
                            if (iDbData == 1)
                                objElm.InnerText = "True";
                            else
                                objElm.InnerText = "";
                        }
                    }
                }
                //Ashish Ahuja :End

				objElm = null;
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='CASE_MGT_FLAG'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseCaseMgmt']");
				if(objElm!=null)
				{
					if (iDbData == -1 )
						objElm.InnerText ="True";
					else
						objElm.InnerText ="";
				}

                //added by swati
                objElm = null;
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USE_DCI_FIELDS'"));
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='UseDCIReportingFields']");
                if (objElm != null)
                {
                    if (iDbData == -1)
                        objElm.InnerText = "True";
                    else
                        objElm.InnerText = "";
                }

                objElm = null;
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USE_CLUE_FIELDS'"));
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='UseCLUEReportingFields']");
                if (objElm != null)
                {
                    if (iDbData == -1)
                        objElm.InnerText = "True";
                    else
                        objElm.InnerText = "";
                }
                //change end here 
                //*************************************************************
				//Animesh Inserted 
				objElm = null;
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ADJ_AUTOASSIGN_FLAG'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseAutoAdj']");
				if (objElm != null)
				{
					if (iDbData == 0) //off or null flag
					{
						objElm.InnerText = ""; //set the checkbox unchecked
					}
                    else if (iDbData == 1 || iDbData == 2)
					{
                        objElm.InnerText = "True"; //checkbox is checked
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/RBAutoAssignAdj");
                        if (objElm != null)
                        {
                            objElm.InnerText = iDbData.ToString(); //child radio button is checked
                        }
					}
				}
				//Animesh Insertion ends
				//*************************************************************
                //Ashish Ahuja: Claims Made Jira 1342 Start
                objElm = null;
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='RPT_DATE_AUTO_FLAG'"));
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseAutoClaimReported']");
                if (objElm != null)
                {
                    if (iDbData == 0) //off or null flag
                    {
                        objElm.InnerText = ""; //set the checkbox unchecked
                    }
                    else if (iDbData == 1 || iDbData == 2)
                    {
                        objElm.InnerText = "True"; //checkbox is checked
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/RBAutoAssignClaimReported");
                        if (objElm != null)
                        {
                            objElm.InnerText = iDbData.ToString(); //child radio button is checked
                        }
                    }
                }
                //Ashish Ahuja: Claims Made Jira 1342 End
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SCRPT_EDI_FLAG'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseScriptEditor']");
				if(objElm!=null)
				{
					if (iDbData == -1 )
						objElm.InnerText ="True";
					else
						objElm.InnerText ="";
				}
                //spahariya MITS 25646-start
                iDbData = Conversion.CastToType<int>(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='PSO_REP_FLAG'"), out blnSuccess);
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UsePSOReportingSys']");
                if (objElm != null)
                {
                    if (iDbData == -1)
                        objElm.InnerText = "True";
                    else
                        objElm.InnerText = "";
                }
                //spahariya -end
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='CODES_USER_LIMIT'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CodeList']");
				if(objElm!=null)
				{
					if (iDbData>0)
						objElm.InnerText = iDbData.ToString();
					else
						objElm.InnerText = "500";

					iMin = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='CODES_MIN_LIMIT'"));
					iMax = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='CODES_MAX_LIMIT'"));
					iRecmnd = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='CODES_RM_LIMIT'"));
					objElm.SetAttribute("tooltip","Min : " + iMin + ", Max : " + iMax + ", Recommended : " + iRecmnd );
				}

				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SRCH_USER_LIMIT'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ResultLkpUp']");
				if(objElm!=null)
				{
					if (iDbData>0)
						objElm.InnerText = iDbData.ToString();
					else
						objElm.InnerText = "500";

					iMin = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SRCH_MIN_LIMIT'"));
					iMax = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SRCH_MAX_LIMIT'"));
					iRecmnd = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SRCH_RM_LIMIT'"));
					objElm.SetAttribute("tooltip","Min : " + iMin + ", Max : " + iMax + ", Recommended : " + iRecmnd );
				}
				//Parijat -19713
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENH_USER_LIMIT'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ResultEnhNot']");
				if (objElm != null)
				{
					if (iDbData > 0)
						objElm.InnerText = iDbData.ToString();
					else
						objElm.InnerText = "40";
				}
				//Parijat -end 19713
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='CASE_MGT_FLAG'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseCaseMgmt']");
				if(objElm!=null)
				{
					if (iDbData == -1 )
						objElm.InnerText ="True";
					else
						objElm.InnerText ="";
				}

				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='QUALITY_MGT_FLAG'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseQualityMgt']");
				if(objElm!=null)
				{
					if (iDbData == -1 )
						objElm.InnerText ="True";
					else
						objElm.InnerText ="";
				}
				//Code Added For getting  Show All Transaction Types for Invoice Detail checkbox value from database
				//Geeta 08/06/08  Mits 12319
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USE_ALL_TRANS_TYPES'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseAllTransTypes']");
				if (objElm != null)
				{
					if (iDbData == -1)
						objElm.InnerText = "True";
					else
						objElm.InnerText = "";
				}
				string sData;
				sData= objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SSN_MASK_JURIS'");
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PrivacySettingsText']");
				if(objElm!=null)
				{
					objElm.InnerText = sData;
				}

				//*******************************
				//Mohit Yadav for Bug No. 000182
				//*******************************
				iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='BRS_2ND_TBL'"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BRS2FeeTbl']");
				if(objElm!=null)
				{
					if (iDbData == -1 )
						objElm.InnerText ="True";
					else
						objElm.InnerText ="";
				}
				//MITS 19014  Use Legacy Comments
				objElm = null;
				iUseLegacyComments = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT USE_LEGACY_COMMENTS FROM SYS_PARMS WHERE ROW_ID=1"));
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/hdUseLgcyComments");
				if (objElm != null)
				{
					objElm.InnerText = iUseLegacyComments.ToString();
				}
				//*******************************
				//objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FinHis1']");//Asif
				//objElm.Attributes["value"].Value=p_objXmlDocument.SelectSingleNode("//control[@name='FinHis']").Attributes["value"].Value;//Asif
				//objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='OrderBankAcct1']");//Asif
				//objElm.Attributes["value"].Value=p_objXmlDocument.SelectSingleNode("//control[@name='OrderBankAcct']").Attributes["value"].Value;//Asif
				//Sumit - 09/21/2009 MITS#18227 Pass control to Disable Billing Flag
				//Start:Change by kuladeep 04/21/2010 Retrofit for MITS 18278
				//iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT COUNT(USE_ENH_POL_FLAG) FROM SYS_PARMS_LOB WHERE USE_ENH_POL_FLAG=-1"));
				iDbData = Conversion.CastToType<int>(objCon.ExecuteString("SELECT COUNT(USE_ENH_POL_FLAG) FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE IN('241','242','243','845') AND USE_ENH_POL_FLAG=-1"),out blnSuccess);
				//End:Change by kuladeep 04/21/2010 Retrofit for MITS 18278
				if (iDbData == 0)
				{
					objTempElement = p_objXmlDocument.CreateElement("DisableControl");
					objTempElement.InnerText = "BillingFlag";
					p_objXmlDocument.ChildNodes[0].AppendChild(objTempElement);
					objCon.ExecuteString("UPDATE SYS_PARMS SET USE_BILLING_FLAG=0");
				}
				//Start:Add by kuladeep to check Enhance Policy Active for all LOB Retrofit for MITS 18278
				objTempElement = p_objXmlDocument.CreateElement("EnhPolicyStatus");
				objTempElement.InnerText = Convert.ToString(iDbData);
				p_objXmlDocument.ChildNodes[0].AppendChild(objTempElement);
				//End:Add by kuladeep to check Enhance Policy Active for all LOB Retrofit for MITS 18278
				p_objXmlDocument=LoadTandEBankAcc(p_objXmlDocument);
				
				//Geeta : R7 Task Manager Auto Diary
				objTaskMgr = new TaskManager(m_iClientId);
				objTaskMgr.GetTasksForTaskMgrDiary(null, out blnIsTaskExist);
				p_objXmlDocument.SelectSingleNode("//control[@name='hdUpdateTasks']").InnerText = blnIsTaskExist.ToString();
				//Start: rsushilaggar 06/02/2010 MITS 20606
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RMXLSSEnable']");
				if (objElm != null && objElm.InnerText != "True")
				{
					objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ShowLSSInvoice']");
					if (objElm != null)
					{
						objElm.InnerText = "";
					}
					XmlElement element = p_objXmlDocument.CreateElement("DisableControls");
					XmlElement element1 = p_objXmlDocument.CreateElement("Item");
					element1.InnerText = "ShowLSSInvoice";
					element.AppendChild(element1);
					objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//form");
					if (objElm != null)
						objElm.AppendChild(element);
				   
				}
				//end: rsushilaggar 06/02/2010 MITS 20606
					//PSHEKHAWAT 09/26/2012 - Added to show excluded event types for PSO reporting system
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//SystemSettings/group/displaycolumn/systemcontrol[@name='PSOEventTypes']");
                objDocTemp = GetPSOEventType(ref strTemp);
                objNode = p_objXmlDocument.ImportNode(objDocTemp.DocumentElement, true);

                objElm.SetAttribute("codeid", strTemp);
                objElm.AppendChild(objNode);
                //objElm.AppendChild(objTempElement);
                //p_objXmlDocument.FirstChild.FirstChild.AppendChild(objElm);
                //p_objXmlDocument.ChildNodes[2].AppendChild(objElm);
                // PSHEKHAWAT - Addition Ends
				
				//Deb : MITS 25598
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='DOCLIST_USER_LIMIT'"));
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DocList']");
                if (objElm != null)
                {
                    if (iDbData > 0)
                        objElm.InnerText = iDbData.ToString();
                    else
                        objElm.InnerText = "20";
                }
                //Deb : MITS 25598	
                //Ankit- Start changes for MITS 27074 Custom paging
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='DIARYLIST_USER_LIMIT'"));
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DiaryList']");
                if (objElm != null)
                {
                    if (iDbData > 0)
                        objElm.InnerText = iDbData.ToString();
                    else
                        objElm.InnerText = "10";
                }
                //Ankit- End changes for MITS 27074 Custom paging
                //Deb MITS 25775 
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='MASK_SSN'"));
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='maskSSN']");
                if (objElm != null)
                {
                    if (iDbData == 0)
                        objElm.InnerText = "False";
                    else
                        objElm.InnerText = "True";
                }
                //Deb MITS 25775 
                //Ankit MITS 29834
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='PAY_HISTORY_LIMIT'"));
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PaymentHistory']");
                if (objElm != null)
                {
                    if (iDbData > 0)
                        objElm.InnerText = iDbData.ToString();
                    else
                        objElm.InnerText = "10";
                }
                //Ankit MITS 29834
                //Added  by Amitosh for R8 enhancement of Policy interface
                objElm = null;
                iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='POLICY_SYSTEM_UPDATE'"));
               if (iDbData == 0 || iDbData == 1)
                    {
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/PolicySysUpdate");
                        if (objElm != null)
                        {
                            objElm.InnerText = iDbData.ToString(); //child radio button is checked
                        }
                    }
               objElm = null;
               iDbData = Conversion.ConvertStrToInteger(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='POLICY_CVG_TYPE'"));
               if (iDbData == 0 || iDbData == 1)
               {
                   objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/PolicyCvgType");
                   if (objElm != null)
                   {
                       objElm.InnerText = iDbData.ToString(); //child radio button is checked
                   }
               }
			    //mbahl3 Strataware Enhancement
               objElm = null;
              
               iDbData = Conversion.CastToType<int>(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='STRATAWARE_USER_ID'"), out bReturn);
               if (iDbData >0)
               {
                   objUserlogin = new UserLogin(iDbData, m_iClientId);//Code changes for RMA-6296

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name ='StratawareUserName']");
                   
                   if (objElm != null)
                   {
                       objElm.InnerText = objUserlogin.objUser.FirstName + " " + objUserlogin.objUser.LastName; //child radio button is checked
                   }
    //select * from USER_DETAILS_TABLE
                  // string sUserName = objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='STRATAWARE_USER_ID'")); 
               }
               //skhare7 JIRA 340-start
               iDbData = Conversion.CastToType<int>(objCon.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='USE_ENTITY_ROLE'"), out blnSuccess);
               objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseEntityRole']");
               if (objElm != null)
               {
                   if (iDbData == -1)
                       objElm.InnerText = "True";
                   else
                       objElm.InnerText = "";
               }
               //skhare7 JIRA 340-End 
               objElm = null;
			    //mbahl3 Strataware Enhancement
                //end Amitosh
                //Deb : ML changes
               objML = new MultiLanguage(ConnectString, m_iClientId);
               //Start Add By ttumula2 on 22 Dec 2014 RMA-6033
               // XMLDoc = objML.GetLanguages(p_objXmlDocument);
                //End Add By ttumula2 on 22 Dec 2014 RMA-6033
                XMLDoc = objML.GetLanguageFormats(p_objXmlDocument);
                //Deb : ML changes
				return XMLDoc; 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.Get.GetErr", m_iClientId), p_objEx);
			}
			finally
			{
				objElm=null;
				objTempElement = null;
				if(objCon!=null)
				{
					objCon.Dispose();
					objCon=null;
				}
				objTaskMgr = null;
                objML = null;
                objDocTemp = null; // PSHEKHAWAT
                objNode = null; // PSHEKHAWAT
                objUserlogin = null;
				 //mbahl3 Strataware Enhancement
			}
		}
		  /// <summary>
		/// Gets the PSO excluded Event type
		/// </summary>
        /// <returns>Xml containing the Event types</returns>
		private XmlDocument GetPSOEventType(ref string strCodeId)//asif
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			DbReader objReader=null;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("PSOEventType");
				objDOM.AppendChild(objElemParent);

                sSQL = "Select SYS_EVENT_TYPE_PSO.EVENT_TYPE_CODE, CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_EVENT_TYPE_PSO, CODES_TEXT, CODES Where SYS_EVENT_TYPE_PSO.EVENT_TYPE_CODE = CODES.CODE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODES_TEXT.CODE_DESC";

                objReader = DbFactory.GetDbReader(ConnectString, sSQL);
				while(objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("EventType");
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("EVENT_TYPE_CODE")));
					//objElemTemp.InnerText=UTILITY.GetCode(Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")),m_sDSN);;
                    objElemTemp.InnerText = objReader.GetString("SHORT_CODE") + " " + objReader.GetString("CODE_DESC");
					objDOM.FirstChild.AppendChild(objElemTemp);
                    //Asif Start

                    strCodeId = strCodeId + " " + Conversion.ConvertObjToStr(objReader.GetValue("EVENT_TYPE_CODE"));
				}
				
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LOCParms.GetReserveType.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objDOM=null;
				objElemParent=null;
				objElemTemp=null;
				if (objReader!=null)
				{
					objReader.Dispose();
				}
				
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/07/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Takes the xml with data as input and saves back to Database
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data</param>
		/// <returns>xml document with saved data</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			DbConnection objCon = null;
			XmlElement objElm = null;
			string sSQL = string.Empty;
            string sParamValue = string.Empty;

            //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            DbCommand objCmd = null;
            //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
            SysSettings objSysSetting = null;
			try
			{
				XMLDoc = p_objXmlDocument;
				UseLegacyCommentsTrigger(p_objXmlDocument); // MITS 19014 : Use Legacy Comments
				base.Save();

				// npadhy 06/09/2010 Once the save of SYS_PARMS column is done, we need to save the PARM_NAME_VALUE columns as well
				// Since this is specific to General System Paramter, just adding the function here.
                //Asharma326 MITS 32386 Starts
                if ((XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='FASPassword']") != null)
                {
                    objSysSetting = new SysSettings(ConnectString, m_iClientId);
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='FASPassword']");
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        if ((string.IsNullOrEmpty(objSysSetting.FASPassword))
                            || (RMCryptography.EncryptString(objSysSetting.FASPassword) != objElm.InnerText))
                        {
                            objElm.InnerText = RMCryptography.EncryptString(objElm.InnerText);
                        }
                    }
                }
                //Asharma326 MITS 32386 Ends
				SaveSystemSettings(p_objXmlDocument);
				//Update Use case management flag in params_name_value
				objCon =  DbFactory.GetDbConnection(ConnectString);
				objCon.Open();

                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                objCmd = objCon.CreateCommand();
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012

				sSQL = "UPDATE PARMS_NAME_VALUE  SET STR_PARM_VALUE = '";
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseCaseMgmt']");
				if(objElm!=null)
				{
					if (objElm.InnerText == "True" )
						objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'CASE_MGT_FLAG'");
					else
						objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'CASE_MGT_FLAG'");
				}
				//**********************************************

                //added by swati
                sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '";
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='UseDCIReportingFields']");
                if (objElm != null)
                {
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'USE_DCI_FIELDS'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'USE_DCI_FIELDS'");
                }

                sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '";
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='UseCLUEReportingFields']");
                if (objElm != null)
                {
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'USE_CLUE_FIELDS'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'USE_CLUE_FIELDS'");
                }
                //change end here by swati

				//Animesh inserted CPE MITS 18738
				//Update Use Auto Assign Adjuster flag in params_name_value
                sParamValue = (0).ToString();
                
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                strSQL.Append("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = ");
				//sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '";
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseAutoAdj']");
				if (objElm != null)
				{
                    if (objElm.InnerText == true.ToString())
                    {
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/RBAutoAssignAdj");
                        if ( objElm != null && (objElm.InnerText == (1).ToString() ||objElm.InnerText == (2).ToString()) )
                        {
                            sParamValue = objElm.InnerText;
                        }
                    }
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    strSQL.Append(string.Format(" {0} WHERE PARM_NAME = 'ADJ_AUTOASSIGN_FLAG'","~STR_PARM_VALUE~"));
                    dictParams.Add("STR_PARM_VALUE", sParamValue);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                    //objCon.ExecuteNonQuery(sSQL + sParamValue + "' WHERE PARM_NAME = 'ADJ_AUTOASSIGN_FLAG'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}
				//Animesh Insertion Ends
                //Ashish Ahuja: Claims Made Jira 1342
                sParamValue = (0).ToString();
                dictParams.Clear();
                strSQL.Clear();
                strSQL.Append("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = ");
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseAutoClaimReported']");
                if (objElm != null)
                {
                    if (objElm.InnerText == true.ToString())
                    {
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/RBAutoAssignClaimReported");
                        if (objElm != null && (objElm.InnerText == (1).ToString() || objElm.InnerText == (2).ToString()))
                        {
                            sParamValue = objElm.InnerText;
                        }
                    }
                    strSQL.Append(string.Format(" {0} WHERE PARM_NAME = 'RPT_DATE_AUTO_FLAG'", "~STR_PARM_VALUE~"));
                    dictParams.Add("STR_PARM_VALUE", sParamValue);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //Ashish Ahuja: Claims Made Jira 1342
				//**********************************************
				//Asif for Updating the Financial History RadioButton Start
                
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                strSQL = new StringBuilder();
                strSQL.Append("UPDATE SYS_PARMS SET FIN_HIST_EVAL = ");
				//sSQL = "UPDATE SYS_PARMS SET FIN_HIST_EVAL ='";
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/FinHis");
				if (objElm != null)
				{
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    strSQL.Append(string.Format(" {0} ", "~FIN_HIST_EVAL~"));
                    dictParams.Clear();
                    dictParams.Add("FIN_HIST_EVAL", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                    //objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}
				//Asif for Updating the Financial History RadioButton End

				//rkaur7 start - update state radio button value
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                strSQL = new StringBuilder();
                strSQL.Append("UPDATE SYS_PARMS SET STATE_EVAL = ");
                //sSQL = "UPDATE SYS_PARMS SET STATE_EVAL ='";
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/State");
				if (objElm != null)
				{
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    strSQL.Append(string.Format(" {0} ", "~STATE_EVAL~"));
                    dictParams.Clear();
                    dictParams.Add("STATE_EVAL", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                    //objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}
				//rkaur7 end

				//Asif for Updating the Order Bank Account RadioButton Start
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                strSQL = new StringBuilder();
                strSQL.Append("UPDATE SYS_PARMS  SET ORDER_BANK_ACC = ");
                //sSQL = "UPDATE SYS_PARMS  SET ORDER_BANK_ACC ='";
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/OrderBankAcct");
				if (objElm != null)
				{
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    strSQL.Append(string.Format(" {0} ", "~ORDER_BANK_ACC~"));
                    dictParams.Clear();
                    dictParams.Add("ORDER_BANK_ACC", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                    //objCon.ExecuteNonQuery(sSQL+ objElm.InnerText+ "'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}
				//sSQL = "WHERE ROW_ID=1";
				//Asif for Updating the OrderBank Account RadioButton End

				//Update Use Script Editor flag in params_name_value
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                sSQL = "UPDATE PARMS_NAME_VALUE  SET STR_PARM_VALUE = ";
				//sSQL = "UPDATE PARMS_NAME_VALUE  SET STR_PARM_VALUE = '";
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseScriptEditor']");
				if(objElm!=null)
				{
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "'-1' WHERE PARM_NAME = 'SCRPT_EDI_FLAG'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "'0' WHERE PARM_NAME = 'SCRPT_EDI_FLAG'");
                    //if (objElm.InnerText == "True" )
                    //    objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'SCRPT_EDI_FLAG'");
                    //else
                    //    objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'SCRPT_EDI_FLAG'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}
				
				//spahariya MITS 25646 Update Use PSO Reorting system flag in params_name_value
                //sSQL = "UPDATE PARMS_NAME_VALUE  SET STR_PARM_VALUE = '";
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UsePSOReportingSys']");
                if (objElm != null)
                {
                    if (string.Compare(objElm.InnerText,"True", true) == 0)
                        objCon.ExecuteNonQuery(sSQL + "'-1' WHERE PARM_NAME = 'PSO_REP_FLAG'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "'0' WHERE PARM_NAME = 'PSO_REP_FLAG'");
                }
                //spahariya - end
				//Update No of records in Code List
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CodeList']");
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                if (objElm != null)
                {
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format(sSQL + " {0} WHERE PARM_NAME = 'CODES_USER_LIMIT'", "~STR_PARM_VALUE~"));
                    dictParams.Clear();
                    dictParams.Add("STR_PARM_VALUE", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //if (objElm != null)
                //    objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "' WHERE PARM_NAME = 'CODES_USER_LIMIT'");
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				

				//Nietsh RMX Gaps MITS:6441 starts
				//Update No of records in Privacy Settings Text
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PrivacySettingsText']");
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                if (objElm != null)
                {
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format(sSQL + " {0} WHERE PARM_NAME = 'SSN_MASK_JURIS'", "~STR_PARM_VALUE~"));
                    dictParams.Clear();
                    dictParams.Add("STR_PARM_VALUE", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //if(objElm!=null)
                //    objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "' WHERE PARM_NAME = 'SSN_MASK_JURIS'");
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				//Nietsh RMX Gaps MITS:6441 Ends

				//Update No of records in Search Lookup
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ResultLkpUp']");
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                if (objElm != null)
                {
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format(sSQL + " {0} WHERE PARM_NAME = 'SRCH_USER_LIMIT'", "~STR_PARM_VALUE~"));
                    dictParams.Clear();
                    dictParams.Add("STR_PARM_VALUE", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //if(objElm!=null)
                //    objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "' WHERE PARM_NAME = 'SRCH_USER_LIMIT'");
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012

				//Parijat - 19713 -Update No of records
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ResultEnhNot']");
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                if (objElm != null)
                {
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format(sSQL + " {0} WHERE PARM_NAME = 'ENH_USER_LIMIT'", "~STR_PARM_VALUE~"));
                    dictParams.Clear();
                    dictParams.Add("STR_PARM_VALUE", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //if (objElm != null)
                //    objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "' WHERE PARM_NAME = 'ENH_USER_LIMIT'");
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				//Parijat - end 19713
				//Deb : MITS 25598
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DocList']");
                //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                if (objElm != null)
                {
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format(sSQL + " {0} WHERE PARM_NAME = 'DOCLIST_USER_LIMIT'", "~STR_PARM_VALUE~"));
                    dictParams.Clear();
                    dictParams.Add("STR_PARM_VALUE", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //if (objElm != null)
                //    objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "' WHERE PARM_NAME = 'DOCLIST_USER_LIMIT'");
                //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                //Deb : MITS 25598
                //Ankit-MITS 27074-Start changes                
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DiaryList']");
                if (objElm != null)
                {                    
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format(sSQL + " {0} WHERE PARM_NAME = 'DIARYLIST_USER_LIMIT'", "~STR_PARM_VALUE~"));
                    dictParams.Clear();
                    dictParams.Add("STR_PARM_VALUE", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //Ankit-MITS 27074-End changes
                //Ankit MITS 29834
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PaymentHistory']");
                if (objElm != null)
                {
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format(sSQL + " {0} WHERE PARM_NAME = 'PAY_HISTORY_LIMIT'", "~STR_PARM_VALUE~"));
                    dictParams.Clear();
                    dictParams.Add("STR_PARM_VALUE", objElm.InnerText);

                    objCmd.CommandText = strSQL.ToString();
                    objCmd.Parameters.Clear();
                    DbFactory.ExecuteNonQuery(objCmd, dictParams);
                }
                //Ankit MITS 29834
                //Deb MITS 25775 
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='maskSSN']");
                if (objElm != null)
                {
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "'-1' WHERE PARM_NAME = 'MASK_SSN'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "'0' WHERE PARM_NAME = 'MASK_SSN'");

                    //if (objElm.InnerText == "True")
                    //    objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'MASK_SSN'");
                    //else
                    //    objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'MASK_SSN'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                }
                //Deb MITS 25775 
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseQualityMgt']");
				if(objElm!=null)
				{
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "'-1' WHERE PARM_NAME = 'QUALITY_MGT_FLAG'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "'0' WHERE PARM_NAME = 'QUALITY_MGT_FLAG'");

                    //if (objElm.InnerText == "True" )
                    //    objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'QUALITY_MGT_FLAG'");
                    //else
                    //    objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'QUALITY_MGT_FLAG'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}
				//Code Added For Saving Show All Transaction Types for Invoice Detail checkbox in databse
				//Geeta 08/06/08  Mits 12319
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseAllTransTypes']");
				if (objElm != null)
				{
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "'-1' WHERE PARM_NAME = 'USE_ALL_TRANS_TYPES'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "'0' WHERE PARM_NAME = 'USE_ALL_TRANS_TYPES'");

                    //if (objElm.InnerText == "True")
                    //    objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'USE_ALL_TRANS_TYPES'");
                    //else
                    //    objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'USE_ALL_TRANS_TYPES'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}


				//***********************************
				//Mohit Yadav for Bug No. -- 000182
				//***********************************
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BRS2FeeTbl']");
				if(objElm!=null)
				{
                    //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "'-1' WHERE PARM_NAME = 'BRS_2ND_TBL'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "'0' WHERE PARM_NAME = 'BRS_2ND_TBL'");

                    //if (objElm.InnerText == "True" )
                    //    objCon.ExecuteNonQuery(sSQL + "-1' WHERE PARM_NAME = 'BRS_2ND_TBL'");
                    //else
                    //    objCon.ExecuteNonQuery(sSQL + "0' WHERE PARM_NAME = 'BRS_2ND_TBL'");
                    //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
				}

                //Deb:BOB
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='MultiCovgPerClm']");
                if (objElm != null)
                {
                    if (objElm.InnerText == "True")
                    {
                        objCon.ExecuteNonQuery("UPDATE SYS_PARMS_LOB SET USE_ENH_POL_FLAG = 0 WHERE LINE_OF_BUS_CODE IN (241,242,243,845)");
                        objCon.ExecuteNonQuery("UPDATE SYS_PARMS_LOB SET USE_RSV_WK = 0 WHERE LINE_OF_BUS_CODE IN (241,242,243,845,844)");
                        //Mona: R8.2 : BRS in Carrier claim
                        //objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='USE_BRS'");
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='USE_DI_LOB'");
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='USE_PC_LOB'");
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='USE_VA_LOB'");
                        objCon.ExecuteNonQuery("UPDATE SYS_PARMS SET AUTO_SELECT_POLICY = 0");
                    }

                }

				//***********************************
                //Deb:BOB

 // PSHEKHAWAT 09/25/2012 - Added to save those Events types which are to be excluded from PSO Reporting System
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//SystemSettings/group/displaycolumn/systemcontrol[@name='PSOEventTypes']");
                objCon.ExecuteNonQuery("DELETE from SYS_EVENT_TYPE_PSO");
                if (objElm != null)
                {
                    string strEventTypes = objElm.GetAttribute("codeid");
                    string[] straEventTypes = strEventTypes.Split(' ');
                    foreach (string strEventType in straEventTypes)
                    {
                        if (!string.IsNullOrEmpty(strEventType.Trim()))
                        {
                            objCon.ExecuteNonQuery("INSERT INTO SYS_EVENT_TYPE_PSO(EVENT_TYPE_CODE,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES('" 
                                + strEventType + "','" + strUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")");
                        }
                    }
                }
                XmlDocument objDocTemp = null;
                XmlNode objNode = null;
                string strTemp = string.Empty;
                objDocTemp = GetPSOEventType(ref strTemp);
                objNode = p_objXmlDocument.ImportNode(objDocTemp.DocumentElement, true);

                objElm.SetAttribute("codeid", strTemp);
                objElm.AppendChild(objNode);     
				
            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='MultiCovgPerClm']");
                if (objElm != null)
                {
                    if (objElm.InnerText == "True")
                    {
                        objCon.ExecuteNonQuery("UPDATE SYS_PARMS_LOB SET USE_ENH_POL_FLAG = 0,USE_RSV_WK = 0,RESERVE_TRACKING = 0 WHERE LINE_OF_BUS_CODE IN (241,242,243,845)");
                        //Mona: R8.2 : BRS in Carrier claim
                        //objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME IN ('USE_BRS','USE_DI_LOB','USE_PC_LOB','USE_VA_LOB')");
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME IN ('USE_DI_LOB','USE_PC_LOB','USE_VA_LOB')");
                        objCon.ExecuteNonQuery("UPDATE SYS_PARMS SET AUTO_SELECT_POLICY = 0");
                        objCon.ExecuteNonQuery("UPDATE SYS_PARMS SET ACC_LINK_FLAG = -1");
                        //rupal: commented below line as in R8 this "Use Reserve Filter" will be enabled and depend upon user's chice
                        //objCon.ExecuteNonQuery("UPDATE SYS_PARMS SET USE_RES_FILTER = -1");
                 
                    }
                    else
                    {
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME ='USE_POLICY_INTERFACE'"); //Added by Amitosh for mits 25163
                    }

                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='AutoPopulateDpt']");
                if (objElm != null)
                {
                    if (objElm.InnerText == "False")
                    {
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='AUTO_POPU_DPT_ID'");
                    }
                }//Deb:MITS 25124
                //Added by Amitosh for r8 enhancement of Policy interface
                sParamValue = "0";
                sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '";
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/PolicySysUpdate");
                if (objElm != null && (objElm.InnerText == "1" || objElm.InnerText == "0"))
                {
                    sParamValue = objElm.InnerText;
                }

                objCon.ExecuteNonQuery(sSQL + sParamValue + "' WHERE PARM_NAME = 'POLICY_SYSTEM_UPDATE'");

                sParamValue = "0";
                sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '";
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//group/PolicyCvgType");
                if (objElm != null && (objElm.InnerText == "1" || objElm.InnerText == "0"))
                {
                    sParamValue = objElm.InnerText;
                }

                objCon.ExecuteNonQuery(sSQL + sParamValue + "' WHERE PARM_NAME = 'POLICY_CVG_TYPE'");
			    //mbahl3 Strataware Enhancement objElm = null;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='StrataWare']");
                if (objElm != null)
                {
                    if (!string.Equals(objElm.InnerText,"True",StringComparison.InvariantCultureIgnoreCase))
                    {
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='EnableSingleuser'");
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='STRATAWARE_USER_ID'");
                    }
                   

                }
                objElm = null;
                 objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='EnableSingleuser']");
                if (objElm != null)
                {
                    if (!string.Equals(objElm.InnerText,"True",StringComparison.InvariantCultureIgnoreCase))
                    {
                        objCon.ExecuteNonQuery("UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = '0' WHERE PARM_NAME='STRATAWARE_USER_ID'");
                    }
                   

                }
                //skhare7 Entity Role JIRA 340
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseEntityRole']");
                if (objElm != null)
                {
                    
                    if (objElm.InnerText == "True")
                        objCon.ExecuteNonQuery(sSQL + "'-1' WHERE PARM_NAME = 'USE_ENTITY_ROLE'");
                    else
                        objCon.ExecuteNonQuery(sSQL + "'0' WHERE PARM_NAME = 'USE_ENTITY_ROLE'");

                    
                }
                //skhare7 Entity Role JIRA 340
                objElm = null;
				 //mbahl3 Strataware Enhancement
                //End Amitosh
                //Deb -Multi Curr
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name ='BaseCurrencyType']");
                if (objElm != null)//rkotak - added null check
                    objElm.InnerText = UTILITY.GetCode(objElm.Attributes["codeid"].Value, ConnectString, m_iClientId);
                //if (objElmBase != null)
                //{
                //    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name ='SourceCurrencyType']");
                //    if (objElm != null)
                //    {
                //        int icurrcode = 0;
                //        int iCodeID = Utilities.GetNextUID(objCon.ConnectionString, "CURRENCY_RATE");
                //        int ibasecode=Convert.ToInt32(objElmBase.Attributes["codeid"].Value);
                //        if (!string.IsNullOrEmpty((objElm.Attributes["codeid"].Value)))
                //            icurrcode=Convert.ToInt32(objElm.Attributes["codeid"].Value);
                //        int iCount = objCon.ExecuteInt(string.Format("SELECT COUNT(1) FROM CURRENCY_RATE WHERE DESTINATION_CURRENCY_CODE={0} AND SOURCE_CURRENCY_CODE={1}", ibasecode, icurrcode));
                //        if (iCount > 0 && ibasecode > 0 && icurrcode > 0)
                //        {
                //            //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                //            strSQL = new StringBuilder();
                //            strSQL.Append(string.Format("UPDATE [CURRENCY_RATE] SET [DESTINATION_CURRENCY_CODE]={0},[SOURCE_CURRENCY_CODE]={1},[EXCHANGE_RATE]={2}", "~DESTINATION_CURRENCY_CODE~", "~SOURCE_CURRENCY_CODE~", "~EXCHANGE_RATE~"));
                //            strSQL.Append(string.Format(" WHERE DESTINATION_CURRENCY_CODE={3} AND SOURCE_CURRENCY_CODE={4}", "~DESTINATION_CURRENCY_CODE~", "~SOURCE_CURRENCY_CODE~"));

                //            dictParams.Clear();
                //            dictParams.Add("DESTINATION_CURRENCY_CODE", iCodeID);
                //            dictParams.Add("SOURCE_CURRENCY_CODE", ibasecode);
                //            dictParams.Add("EXCHANGE_RATE", 1.0);
                //            dictParams.Add("DESTINATION_CURRENCY_CODE", iCodeID);
                //            dictParams.Add("SOURCE_CURRENCY_CODE", ibasecode);

                //            objCmd.CommandText = strSQL.ToString();
                //            objCmd.Parameters.Clear();
                //            DbFactory.ExecuteNonQuery(objCmd, dictParams);
                //        //    sSQL = string.Format("UPDATE [CURRENCY_RATE] SET [DESTINATION_CURRENCY_CODE]={0},[SOURCE_CURRENCY_CODE]={1},[EXCHANGE_RATE]={2} " +
                //        //" WHERE DESTINATION_CURRENCY_CODE={3} AND SOURCE_CURRENCY_CODE={4}", iCodeID, ibasecode, 1.0, iCodeID, ibasecode);
                //        //    objCon.ExecuteNonQuery(sSQL);
                //            //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                            
                //        }
                //        else if(ibasecode> 0 && icurrcode > 0)
                //        {
                //            //PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                //            strSQL = new StringBuilder();
                //            strSQL.Append("INSERT INTO [CURRENCY_RATE] ([CURR_ROW_ID],[DESTINATION_CURRENCY_CODE],[SOURCE_CURRENCY_CODE],[EXCHANGE_RATE],[EFFECTIVE_DATE],[DTTM_RCD_ADDED],[ADDED_BY_USER])");
                //            strSQL.Append(string.Format(" VALUES ({0},{1},{2},", "~CURR_ROW_ID~", "~DESTINATION_CURRENCY_CODE~", "~SOURCE_CURRENCY_CODE~"));
                //            strSQL.Append(string.Format("{3},{4},", "~EXCHANGE_RATE~", "~EFFECTIVE_DATE~"));
                //            strSQL.Append(string.Format("{5},{6})", "~DTTM_RCD_ADDED~", "~ADDED_BY_USER~"));

                //            dictParams.Clear();
                //            dictParams.Add("CURR_ROW_ID", iCodeID);
                //            dictParams.Add("DESTINATION_CURRENCY_CODE", ibasecode);
                //            dictParams.Add("SOURCE_CURRENCY_CODE", icurrcode);
                //            dictParams.Add("EXCHANGE_RATE", 1.0);
                //            dictParams.Add("EFFECTIVE_DATE", "");
                //            dictParams.Add("DTTM_RCD_ADDED", Conversion.ToDbDateTime(DateTime.Now).ToString());
                //            dictParams.Add("ADDED_BY_USER", "csc");

                //            objCmd.CommandText = strSQL.ToString();
                //            objCmd.Parameters.Clear();
                //            DbFactory.ExecuteNonQuery(objCmd, dictParams);
                //        //    sSQL = string.Format("INSERT INTO [CURRENCY_RATE] ([CURR_ROW_ID],[DESTINATION_CURRENCY_CODE],[SOURCE_CURRENCY_CODE],[EXCHANGE_RATE],[EFFECTIVE_DATE],[DTTM_RCD_ADDED],[ADDED_BY_USER])" +
                //        //" VALUES ({0},{1},{2},{3},'{4}','{5}','{6}')", iCodeID, ibasecode, icurrcode, 1.0, "", Conversion.ToDbDateTime(DateTime.Now).ToString(), "csc");
                //        //objCon.ExecuteNonQuery(sSQL);
                //            //END PenTesting - MITS 26943 - srajindersin - 11th Jan 2012
                //        }
                //    }
                //}
                //Deb -Multi Curr
				return XMLDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.Save.SaveErr", m_iClientId), p_objEx);
			}
			finally
			{
				if(objCon!=null)
				{
					objCon.Dispose();
					objCon=null;
				}
				objElm = null; 
			}
		}

        private void GetSystemSettings(XmlDocument p_objXmlDocument)
		{
			XmlElement objXmlElement = null;
			string sControlType = string.Empty;
			string sValue = string.Empty;
            Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(ConnectString, m_iClientId);
			foreach (string sPropertyName in arrSystemFields.Keys)
			{

				objXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//SystemSettings/group/displaycolumn/systemcontrol[@name ='" + sPropertyName + "']");
				if (objXmlElement != null)
				{
					sValue = objSettings.RetrieveSystemSettings(arrSystemFields[sPropertyName].ToString());
					sControlType = objXmlElement.GetAttribute("type").ToLower();
					switch (sControlType)
					{
						case "checkbox":
							if (sValue.Equals("-1") || sValue.Equals("1") || sValue.Equals("True"))
								objXmlElement.InnerText = "True";
							else
								objXmlElement.InnerText = "";
							break;
						//Radio button and combo are same except appearance
						case "radio":
						case "combobox":
							objXmlElement.SetAttribute("value", sValue);
							break;
						case "date":
							objXmlElement.InnerText = Conversion.GetDBDateFormat(sValue, "d");
							break;
						case "time":
							objXmlElement.InnerText = Conversion.GetDBTimeFormat(sValue, "t");
							break;
						case "code":
							objXmlElement.SetAttribute("codeid", sValue);
                            objXmlElement.InnerText = UTILITY.GetCode(sValue, ConnectString, m_iClientId);
							break;
						case "orgh":
							objXmlElement.SetAttribute("codeid", sValue);
                            objXmlElement.InnerText = UTILITY.GetOrg(sValue, ConnectString, m_iClientId);
                            //objXmlElement.SetAttribute("tooltip", objXmlElement.InnerText);
							break;
                      
						default:
							objXmlElement.InnerText = sValue;
							break;
					}
				}
			}
		}
		private void SaveSystemSettings(XmlDocument p_objXmlDocument)
		    {
			XmlNode objNode = null;
			string sControlType = string.Empty;
			string sValue = string.Empty;
            Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(ConnectString, m_iClientId);
			try
			{
				foreach (string sPropertyName in arrSystemFields.Keys)
				{
					objNode = p_objXmlDocument.SelectSingleNode("//SystemSettings/group/displaycolumn/systemcontrol[@name ='" + sPropertyName + "']");
					if (objNode != null)
					{
						sControlType = ((XmlElement)objNode).GetAttribute("type").Trim().ToLower();
						switch (sControlType)
						{
							case "radio":
							case "combobox":
								sValue = ((XmlElement)objNode).GetAttribute("value").ToUpper() == "NULL" ? "null" : ((XmlElement)objNode).GetAttribute("value");
								break;
							case "checkbox":
								sValue = objNode.InnerText.ToLower() == "true" ? "-1" : "0";
								break;
							case "date":
								sValue = Conversion.GetDate(objNode.InnerText);
								break;
							case "time":
								sValue = Conversion.GetTime(objNode.InnerText);
								break;
							case "code":
								sValue = ((XmlElement)objNode).GetAttribute("codeid");
								break;
							case "orgh":
								sValue = ((XmlElement)objNode).GetAttribute("codeid");
                              
								break;
                           
							case "numeric":
								sValue = objNode.InnerText == "" ? "0" : objNode.InnerText;
								break;
							default:
								//sValue = objNode.InnerText.ToUpper() == "NULL" ? "null" : "'" + objNode.InnerText + "'";
								sValue = objNode.InnerText.ToUpper() == "NULL" ? "null" : objNode.InnerText;
								break;
						}
						objSettings.AssignSystemSettings(arrSystemFields[sPropertyName].ToString(), sValue);
					}
					else
					{
                        throw new RMAppException(Globalization.GetString("SystemParameter.ValidateXml", m_iClientId));
					}
				}
				objSettings.SaveSystemSettings();
			}
			catch (RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.Save.SaveErr", m_iClientId), p_objEx);
			}
			//return true;
		}


		/// Name		: UseCaseMgt
		/// Author		: Pankaj Chowdhury
		/// Date Created: 07/20/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for the Use Case Management password
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument UseCaseMgt(XmlDocument p_objXmlDoc)
		{
			string sPwd="";
			string sActualPwd="";
			XmlNode objNode=null;
			//Start(06/23/2010) - Sumit
			ArrayList AdditionalLicenseParams = new ArrayList();
			//End - Sumit

			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
				//Start(06/23/2010) - Sumit
				AdditionalLicenseParams.Add(USE_CASE_MGT_KEY);
				sActualPwd = GenerateLicense(AdditionalLicenseParams);
				//sActualPwd = USE_CASE_MGT_KEY;
				//End - Sumit
                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
				{
					//correct pwd
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="-1"; 
					p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText="close"; 
				}
				else
				{
					//wrong password
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="0";
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type","labelonly"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title"); 

				}
				p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText="";
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.UseCaseMgt.Err", m_iClientId), p_objEx);  
			}
			finally
			{
				objNode=null;
			}
		}

		/// Name		: UseScriptEditor
		/// Author		: Nitesh Deedwania
		/// Date Created: 10/06/2006
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for the Use Script Editor password
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument UseScriptEditor(XmlDocument p_objXmlDoc)
		{
			string sPwd="";
			string sActualPwd="";
			XmlNode objNode=null;
			//Start(06/23/2010) - Sumit
			ArrayList AdditionalLicenseParams = new ArrayList();
			//End - Sumit

			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
				//Start(06/23/2010) - Sumit
				AdditionalLicenseParams.Add(USE_SCRIPT_EDITOR_KEY);
				sActualPwd = GenerateLicense(AdditionalLicenseParams);
				//sActualPwd = SCRIPT_EDITOR_KEY;
				//End - Sumit

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
				{
					//correct pwd
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="-1"; 
					p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText="close"; 
				}
				else
				{
					//wrong password
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="0";
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type","labelonly"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title"); 

				}
				p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText="";
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.UseScriptEditor.Err", m_iClientId), p_objEx);  
			}
			finally
			{
				objNode=null;
			}
		}


        /// <summary>
        /// Checks for the Use Carrier Claims password
        /// </summary>
        /// <param name="p_objXmlDoc">Xml Document with data to save</param>
        public XmlDocument UseCarrierClaims(XmlDocument p_objXmlDoc)
        {
            string sPwd = "";
            string sActualPwd = "";
            XmlNode objNode = null;
            ArrayList AdditionalLicenseParams = new ArrayList();

            try
            {
                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
                AdditionalLicenseParams.Add(USE_CARRIER_CLAIMS_KEY);
                sActualPwd = GenerateLicense(AdditionalLicenseParams);

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
                {
                    //correct pwd
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "-1";
                    p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close";
                }
                else
                {
                    //wrong password
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "0";
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type", "labelonly");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title");

                }
                p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText = "";
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SystemParameter.UseCarrierClaims.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
        }
        

		/// Name		: UseBRS
		/// Author		: Sumit Kumar
		/// Date Created: 02/07/2010
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for the Use Bill Review System password
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument UseBRS(XmlDocument p_objXmlDoc)
		{
			string sPwd = "";
			string sActualPwd = "";
			XmlNode objNode = null;
			ArrayList AdditionalLicenseParams = new ArrayList();

			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
				AdditionalLicenseParams.Add(USE_BRS_KEY);
				sActualPwd = GenerateLicense(AdditionalLicenseParams);

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
				{
					//correct pwd
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "-1";
					p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close";
				}
				else
				{
					//wrong password
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "0";
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type", "labelonly");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type", "hidden");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type", "hidden");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title");

				}
				p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText = "";
				return p_objXmlDoc;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.UseBRS.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode = null;
			}
		}

		/// Name		: UseLOB
		/// Author		: Sumit Kumar
		/// Date Created: 02/07/2010
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for Activating the Lines of Business (GC,DI,PC,VA and WC)
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument UseLOB(XmlDocument p_objXmlDoc,string sLOB)
		{
			string sPwd = string.Empty;
			string sActualPwd = string.Empty;
			string sActualKey = string.Empty;
			XmlNode objNode = null;
			ArrayList AdditionalLicenseParams = new ArrayList();

			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
				switch (sLOB)
				{
					case "GC":
						sActualKey = USE_GC_LOB_KEY;
						break;
					case "DI":
						sActualKey = USE_DI_LOB_KEY;
						break;
					case "PC":
						sActualKey = USE_PC_LOB_KEY;
						break;
					case "VA":
						sActualKey = USE_VA_LOB_KEY;
						break;
					case "WC":
						sActualKey = USE_WC_LOB_KEY;
						break;
				}

				if (!String.IsNullOrEmpty(sActualKey))
				{
                    AdditionalLicenseParams.Add(sActualKey);
					sActualPwd = GenerateLicense(AdditionalLicenseParams);
				}

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
				{
					//correct pwd
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "-1";
					p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close";
				}
				else
				{
					//wrong password
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "0";
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type", "labelonly");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type", "hidden");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type", "hidden");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title");

				}
				p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText = "";
				return p_objXmlDoc;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.Use" + sLOB + "LOB.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode = null;
			}
		}

		/// Name		: UseEnhPol
		/// Author		: Pankaj Chowdhury
		/// Date Created: 07/25/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for the Use Enhanced Policy Flag password
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument UseEnhPol(XmlDocument p_objXmlDoc)
		{
			string sPwd="";
			string sActualPwd="";
			XmlNode objNode=null;
			//Start(06/23/2010) - Sumit
			bool m_bSuccess = false;
			string sSelectedLOB = string.Empty;
			string sSelectedLOBShortCode = string.Empty;
			ArrayList AdditionalLicenseParams = new ArrayList();
            LocalCache objCache = new LocalCache(ConnectString, m_iClientId);
			//End - Sumit
			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
				//Start(06/23/2010) - Sumit
				AdditionalLicenseParams.Add(USE_ENHANCED_POLICY_KEY);

				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='SelectedLOB']");
				if (objNode != null)
				{
					sSelectedLOB = objNode.InnerText;
					sSelectedLOBShortCode = objCache.GetShortCode(Conversion.CastToType<int>(sSelectedLOB, out m_bSuccess));
					if (!string.IsNullOrEmpty(sSelectedLOBShortCode))
					{
						AdditionalLicenseParams.Add(sSelectedLOBShortCode);
					}
				}

				sActualPwd = GenerateLicense(AdditionalLicenseParams);

				//End - Sumit

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
				{
					//correct pwd
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="-1"; 
					p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText="close"; 
				}
				else
				{
					//wrong password
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="0"; 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type","labelonly"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title"); 

				}
				p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText="";
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.UseEnhPol.Err", m_iClientId), p_objEx);  
			}
			finally
			{
				objNode=null;

				//Sumit
				if (objCache != null)
				{
					objCache.Dispose();
				}
			}
		}


		/// Name		: GenerateLicense
		/// Author		: Sumit Kumar
		/// Date Created: 06/24/2010
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Generate the License key for Policy,BRS,Case Management and other modules.
		///Additional Parameters required to Generate License.This seggregates as
		/// for which module license is being generated.
		///Parameter 1 : Module Name e.g for Policy "ENHPOL" is used.
		///Parameter 2 : Line of Business . This is required for Policy. 
		///Other parameters may be added as per the requirement.
		/// </summary>
		public string GenerateLicense(ArrayList AdditionalLicenseParams)
		{
			StringBuilder sLicensekey = null;
			string sEncryptedLicensekey = string.Empty;
			
			try
			{
				sLicensekey = new StringBuilder();

				foreach (string sVal in AdditionalLicenseParams)
				{
					if (!string.IsNullOrEmpty(sVal))
					{
						sLicensekey.Append(sVal);
					}
				}

				sEncryptedLicensekey = RMCryptography.EncryptString(sLicensekey.ToString());

				return sEncryptedLicensekey;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.GenLicense.Err", m_iClientId), p_objEx);
			}
			finally
			{
				sLicensekey = null;
			}
		}


		/// Name		: UsePolicyBilling
		/// Author		: Shruti Choudhary
		/// Date Created: 03/19/2007
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for the Use Billing System Flag password
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument UsePolicyBilling(XmlDocument p_objXmlDoc)
		{
			string sPwd = "";
			string sActualPwd = "";
			XmlNode objNode = null;
			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
				if (objNode != null)
					sPwd = objNode.InnerText;

				sActualPwd = ENHANCED_POLICY_KEY;
				if (sPwd == sActualPwd)
				{
					//correct pwd
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "-1";
					p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close";
				}
				else
				{
					//wrong password
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "0";
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type", "labelonly");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type", "hidden");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type", "hidden");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title");

				}
				p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText = "";
				return p_objXmlDoc;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.UsePolicyBilling.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode = null;
			}
		}


		/// Name		: FinHistChange
		/// Author		: Pankaj Chowdhury
		/// Date Created: 07/25/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Financial History Change
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document</param>
		/// <returns>xml input</returns>
		private XmlDocument FinHistChange(XmlDocument p_objXmlDoc)
		{
			DbConnection objCon=null;
			try
			{
				objCon=DbFactory.GetDbConnection(base.ConnectString);
				objCon.Open();
				objCon.ExecuteNonQuery("UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '00000000' WHERE SYSTEM_TABLE_NAME = 'FINANCIAL_HIST'"); 
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.FinHistChange.Err", m_iClientId), p_objEx);  
			}
			finally
			{
				if(objCon!=null)
				{
					objCon.Dispose();
					objCon=null;
				}
			}
		}

		/// Name		: LoadTandEBankAcc
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/07/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the Time and Expense details
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		private XmlDocument LoadTandEBankAcc(XmlDocument p_objXmlDocument)
		{
			int iFlag = 0;
			string sSQL="";
			DbReader objRdr = null;
			XmlElement objBankAccElm = null;
			XmlElement objNewElm = null;
			XmlNode objNod=null;
			try
			{
				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='SubBankAcc']"); 
				if(objNod!=null)
					//if(objNod.InnerText=="")//Asif In r4 incase of checkbox unchecked nothing was coming but in r5 False comes incase if the chekbox in unchecked.
						if (objNod.InnerText == "False")
					{
						objRdr = DbFactory.GetDbReader(ConnectString,"SELECT USE_SUB_ACCOUNT FROM SYS_PARMS");
						if(objRdr.Read())
							iFlag = objRdr.GetInt16(0);
						objRdr.Dispose(); 
					}
					else
					{
						iFlag = Conversion.ConvertStrToInteger(objNod.InnerText);  
					}
				//Start: rsushilaggar MITS-19004
				if(iFlag == 0)
					sSQL = "SELECT ACCOUNT_NAME,ACCOUNT_ID FROM ACCOUNT ORDER BY ACCOUNT_NAME ASC";
				else
					sSQL = "SELECT SUB_ACC_NAME,SUB_ROW_ID FROM BANK_ACC_SUB ORDER BY SUB_ACC_NAME ASC";
				//End- rsushilaggar

				//Populate bank account combo
				objBankAccElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BankAccount']");
				objBankAccElm.InnerText ="";
				if(objBankAccElm!=null)
				{
					objRdr = DbFactory.GetDbReader(ConnectString,sSQL);
					while(objRdr.Read())
					{
						objNewElm = p_objXmlDocument.CreateElement("option");
						objNewElm.SetAttribute("value",objRdr.GetInt32(1).ToString());
						objNewElm.InnerText = objRdr.GetString(0);
						objBankAccElm.AppendChild(objNewElm);
						objNewElm=null;
					} 
				}
				objRdr.Dispose(); 
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemParameter.LoadTandEBankAcc.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objRdr != null)objRdr.Dispose();
				objBankAccElm = null;
				objNewElm = null;
				objNod=null;
			}
		}

		//Raman Bhatia --- ACROSOFT INTEGRATION -- RMX PHASE 2
		/// Name		: UseAcrosoftInterface
		/// Author		: Raman Bhatia
		/// Date Created: 07/12/2006
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for the Use Acrosoft Interface password
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument UseAcrosoftInterface(XmlDocument p_objXmlDoc)
		{
			string sPwd="";
			string sActualPwd="";
			XmlNode objNode=null;
			//Start(06/23/2010) - Sumit
			ArrayList AdditionalLicenseParams = new ArrayList();
			//End - Sumit

			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
				//Start(06/23/2010) - Sumit
				AdditionalLicenseParams.Add(USE_ACROSOFT_INTERFACE_KEY);
				sActualPwd = GenerateLicense(AdditionalLicenseParams);
				//sActualPwd = ACROSOFT_INTERFACE_KEY;
				//End - Sumit

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
				{
					//correct pwd
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="-1"; 
					p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText="close"; 
				}
				else
				{
					//wrong password
					p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText="0";
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type","labelonly"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type","hidden"); 
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
					((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title"); 

				}
				p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText="";
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("SystemParameter.UseAcrosoftInterface.Error",m_iClientId),p_objEx);  
			}
			finally
			{
				objNode=null;
			}
		}
		
		 /// Name		: UsePSOReportingSys
        /// Author		: Sonam Pahariya
        /// Date Created: 08/05/2011
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Checks for the Use PSO Reporting System password
        /// </summary>
        /// <param name="p_objXmlDoc">Xml Document with data to save</param>
        public XmlDocument UsePSOReportingSys(XmlDocument p_objXmlDoc)
        {
            string sPwd = "";
            string sActualPwd = "";
            XmlNode objNode = null;
            //Start(06/23/2010) - Sumit
            ArrayList AdditionalLicenseParams = new ArrayList();
            //End - Sumit

            try
            {
                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                if (objNode != null)
                    sPwd = objNode.InnerText;

                //Start(06/23/2010) - Sumit
                AdditionalLicenseParams.Add(USE_PSO_REPORTING_KEY);
                sActualPwd = GenerateLicense(AdditionalLicenseParams);
                //sActualPwd = SCRIPT_EDITOR_KEY;
                //End - Sumit

                if (sPwd == sActualPwd || sPwd == USE_MASTER_KEY)
                {
                    //correct pwd
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "-1";
                    p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close";
                }
                else
                {
                    //wrong password
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "0";
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type", "labelonly");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title");

                }
                p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText = "";
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SystemParameter.UsePSOReportingSys.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
        }
		

        /// Name		: UseMediaViewInterface
        /// Author		: Raman Bhatia
        /// Date Created: 07/06/2011
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Checks for the Use MediaView Interface password
        /// </summary>
        /// <param name="p_objXmlDoc">Xml Document with data to save</param>
        public XmlDocument UseMediaViewInterface(XmlDocument p_objXmlDoc)
        {
            string sPwd = "";
            string sActualPwd = "";
            XmlNode objNode = null;
            ArrayList AdditionalLicenseParams = new ArrayList();
           

            try
            {
                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
                //Start(06/23/2010) - Sumit
                AdditionalLicenseParams.Add(USE_MEDIA_VIEW_INTERFACE_KEY);
                sActualPwd = GenerateLicense(AdditionalLicenseParams);
                //sActualPwd = MediaView_INTERFACE_KEY;
                //End - Sumit

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
                {
                    //correct pwd
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "-1";
                    p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close";
                }
                else
                {
                    //wrong password
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "0";
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type", "labelonly");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title");

                }
                p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText = "";
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SystemParameter.UseMediaViewInterface.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
        }

		private void UseLegacyCommentsTrigger(XmlDocument p_objXmlDoc)
		{
			int iUseLegacyComments = 0;
			int iUseLgcyComments = 0;
			int iTriggerExist = 0;
			DbReader objRdr = null;
			XmlNode objNode = null;
			DbConnection objDBCon = null;
			string sSQL = "";
			try
			{
				objDBCon = DbFactory.GetDbConnection(ConnectString);
				objDBCon.Open();
				objRdr = objDBCon.ExecuteReader("SELECT USE_LEGACY_COMMENTS FROM SYS_PARMS");
				if (objRdr.Read())
					iUseLegacyComments = objRdr.GetInt16(0);
				objRdr.Dispose();
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='UseLegacyComments']");
				iUseLgcyComments = Conversion.ConvertStrToInteger(objNode.InnerText);
				if (iUseLegacyComments != iUseLgcyComments)
				{
                    if (objDBCon.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                    { 
                        sSQL = "SELECT * FROM SYSOBJECTS WHERE NAME = 'MODIFY_CLAIM_COMMENTS' AND XTYPE = 'TR'"; 
                    }
                    else if (objDBCon.DatabaseType.ToString() == Constants.DB_ORACLE)
                    {
                        //zmohammad MITs : 32035 Fix for Oracle query
                        int iIndexUID = ConnectString.IndexOf("UID=") + 4;
                        string sOwner = ConnectString.Substring(iIndexUID, ConnectString.IndexOf(";PWD") - iIndexUID);
                        sSQL = "SELECT * FROM ALL_OBJECTS WHERE OBJECT_TYPE = 'TRIGGER' AND OBJECT_NAME='MODIFY_CLAIM_COMMENTS' AND OWNER = '" + sOwner.ToUpper() + "'";
                    }
                    
                    objRdr = objDBCon.ExecuteReader(sSQL);
					if (objRdr.Read())
						iTriggerExist = -1;
					else
						iTriggerExist = 0;
					objRdr.Dispose();
					if (iUseLegacyComments == 0 && iUseLgcyComments == -1)
					{
						if (iTriggerExist == 0)
						{
							if (objDBCon.DatabaseType.ToString() == Constants.DB_SQLSRVR)
							{
								string sPath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"Claims\Comment_SQL_Trigger.sql");
								ExcecuteScript(sPath, false, objDBCon);
							}
							else if (objDBCon.DatabaseType.ToString() == Constants.DB_ORACLE)
							{
								string sPath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"Claims\Comment_Oracle_Trigger.sql");
								ExcecuteScript(sPath, false, objDBCon);
							}
						}
					}
					if (iUseLegacyComments == -1 && iUseLgcyComments == 0)
					{

						if (iTriggerExist == -1)
						{
							objDBCon.ExecuteNonQuery("DROP TRIGGER MODIFY_CLAIM_COMMENTS");
							objDBCon.ExecuteNonQuery("DROP TRIGGER MODIFY_EVENT_COMMENTS");
						}
					}
				}
			}
			catch (Exception p_objEx)
			{
				throw p_objEx;  
				
			}
			finally
			{
				objNode=null;
				if (objDBCon != null)
				{
					objDBCon.Dispose();
					objDBCon = null;
				}
			}
		}
	   
        //Mona:PaperVisionMerge:Animesh Sahai --- PaperVision Enhancement
        /// Name		: UsePaperVisionInterface
        /// Author		: Animesh Sahai
        /// Date Created: 05/07/2009
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Checks for the Use PaperVision Interface password
        /// </summary>
        /// <param name="p_objXmlDoc">Xml Document with data to save</param>
        public XmlDocument UsePaperVisionInterface(XmlDocument p_objXmlDoc)
        {
            string sPwd = "";
            string sActualPwd = "";
            XmlNode objNode = null;
            ArrayList AdditionalLicenseParams = new ArrayList();

            try
            {
                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
                //Deb
                if (objNode != null)
                {
                    sPwd = objNode.InnerText;
                    sPwd = RMCryptography.EncryptString(sPwd);
                }
                //Deb
                AdditionalLicenseParams.Add(USE_PAPERVISION_KEY);
                sActualPwd = GenerateLicense(AdditionalLicenseParams);

                if (sPwd == sActualPwd || sPwd == RMCryptography.EncryptString(USE_MASTER_KEY))
                {
                    //correct pwd
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "-1";
                    p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close";
                }
                else
                {
                    //wrong password
                    p_objXmlDoc.SelectSingleNode("//control[@name='Success']").InnerText = "0";
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LabelMsg']")).SetAttribute("type", "labelonly");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).SetAttribute("type", "hidden");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label']")).Attributes.RemoveNamedItem("title");
                    ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Password']")).Attributes.RemoveNamedItem("title");

                }
                p_objXmlDoc.SelectSingleNode("//control[@name='Password']").InnerText = "";
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SystemParameter.UsePaperVisionInterface.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
        }

		private void RunSql(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql);
			}
			catch (Exception p_objExp)
			{
				throw p_objExp;
			}
		}

		private void ExcecuteScript(string p_sScriptFilePath, bool p_bIgnoreErrors, DbConnection p_objConn)
		{
			string sContent;
			bool bIsProceed = false;
			string sSQL = "";
			try
			{
				StreamReader objReader = File.OpenText(p_sScriptFilePath);
				while (true)
				{
					if ((sContent = objReader.ReadLine()) != null)
					{
						if (sContent.Trim() != "")
						{
							bIsProceed = true;

							if (sContent.ToUpper().Trim() != "GO" && sContent.ToUpper().Trim() != "/")
							{
								bIsProceed = false;

								sSQL += " " + sContent;
							}

							if (bIsProceed)
							{
								if (!p_bIgnoreErrors)
									RunSql(sSQL, p_objConn);
								else
								{
									try
									{
										RunSql(sSQL, p_objConn);

									}
									catch (Exception)
									{
									}
								}
								sSQL = "";
							}
						}
					}
					else
						break;
				}
				objReader.Close();
			}
			catch (Exception p_objExp)
			{
				throw p_objExp; 
			}
		}
        /// Name		: LoadCountry
        /// Author		: shobhana
        
        /// <summary>
        /// Get the Country details
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml structure document</param>
        //private XmlDocument LoadCountry(XmlDocument p_objXmlDocument)
        //{
           
        //    string sSQL = "";
        //    DbReader objRdr = null;
        //    XmlElement objCountryElm = null;
        //    XmlElement objNewElm = null;
          
        //    try
        //    {

        //        sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC COUNTRY FROM CODES,CODES_TEXT WHERE TABLE_ID=1024 AND CODES.CODE_ID=CODES_TEXT.CODE_ID ORDER BY CODES.CODE_ID";
              
        //        //Populate country combo
        //        objCountryElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='Country']");
        //        objCountryElm.InnerText = "";
        //        if (objCountryElm != null)
        //        {
        //            objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
        //            while (objRdr.Read())
        //            {
        //                objNewElm = p_objXmlDocument.CreateElement("option");
        //                objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString() );
        //                objNewElm.InnerText = objRdr.GetString(1) + " " + objRdr.GetString(2);
        //                objCountryElm.AppendChild(objNewElm);
        //                objNewElm = null;
        //            }
        //        }
        //        objRdr.Dispose();
        //        return p_objXmlDocument;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("SystemParameter.LoadTandEBankAcc.Err"), p_objEx);
        //    }
        //    finally
        //    {
        //        if (objRdr != null) objRdr.Dispose();
        //        objCountryElm = null;
        //        objNewElm = null;
                
        //    }
        //}
        /// Name		: MultiLingualFormats
        /// Author		: skhare7
        /// Date Created: 06/222/2012
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// ZipCodeFormat
        /// </summary>
        /// <param name="p_objXmlDoc">Input xml document</param>
        /// <returns>xml input</returns>
        //private XmlDocument MultiLingualFormats(XmlDocument p_objXmlDoc,int p_iCountryId)
        //{
        //    string sSQL = "";
        //    DbReader objRdr = null;
        //    XmlElement objElm = null;
        //    XmlElement objNewElm = null;

        //    try
        //    {

        //        sSQL = "SELECT ZIPCODE_FORMAT_ROWID,ZIPCODE_FORMATDESC FROM ZIPCODE_FORMAT WHERE COUNTRY_ID=" + p_iCountryId + "  ORDER BY ZIPCODE_FORMAT_ROWID";

        //        //Populate country combo
        //        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//systemcontrol[@name='ZipcodeFormat']");
        //        objElm.InnerText = "";
        //        if (objElm != null)
        //        {
                   
        //            objNewElm = p_objXmlDoc.CreateElement("option");
        //            objNewElm.SetAttribute("value", "0");
        //            objNewElm.InnerText = "--SELECT--";
        //            objElm.AppendChild(objNewElm);
        //            objNewElm = null;
        //            objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
        //            while (objRdr.Read())
        //            {
        //                objNewElm = p_objXmlDoc.CreateElement("option");
        //                objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString());
        //                objNewElm.InnerText = objRdr.GetString(1);
        //                objElm.AppendChild(objNewElm);
        //                objNewElm = null;
        //            }
        //        }
        //        sSQL = "SELECT PHONE_FORMAT_ROWID,PHONE_FORMATDESC FROM PHONE_FORMAT WHERE COUNTRY_ID=" + p_iCountryId + "  ORDER BY PHONE_FORMAT_ROWID";


        //        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//systemcontrol[@name='PhoneFormat']");
        //        objElm.InnerText = "";
        //        if (objElm != null)
        //        {
        //            objNewElm = p_objXmlDoc.CreateElement("option");
        //            objNewElm.SetAttribute("value", "0");
        //            objNewElm.InnerText = "--SELECT--";
        //            objElm.AppendChild(objNewElm);
        //            objNewElm = null;
        //            objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                  
        //            while (objRdr.Read())
        //            {
        //                objNewElm = p_objXmlDoc.CreateElement("option");
        //                objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString());
        //                objNewElm.InnerText = objRdr.GetString(1);
        //                objElm.AppendChild(objNewElm);
        //                objNewElm = null;
        //            }
        //        }
        //        sSQL = "SELECT DATE_FORMAT_ROWID,DATE_FORMATDESC FROM DATE_FORMAT WHERE COUNTRY_ID=" + p_iCountryId + "  ORDER BY DATE_FORMAT_ROWID";

        //        //Populate country combo
        //        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//systemcontrol[@name='Dateformat']");
        //        objElm.InnerText = "";
        //        if (objElm != null)
        //        {
        //            objNewElm = p_objXmlDoc.CreateElement("option");
        //            objNewElm.SetAttribute("value", "0");
        //            objNewElm.InnerText = "--SELECT--";
        //            objElm.AppendChild(objNewElm);
        //            objNewElm = null;
        //            objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
        //            while (objRdr.Read())
        //            {
        //                objNewElm = p_objXmlDoc.CreateElement("option");
        //                objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString());
        //                objNewElm.InnerText = objRdr.GetString(1);
        //                objElm.AppendChild(objNewElm);
        //                objNewElm = null;
        //            }
        //        }
        //        sSQL = "SELECT TIME_FORMAT_ROWID,TIME_FORMATDESC FROM TIME_FORMAT WHERE COUNTRY_ID=" + p_iCountryId + "  ORDER BY TIME_FORMAT_ROWID";


        //        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//systemcontrol[@name='Timeformat']");
        //        objElm.InnerText = "";
        //        if (objElm != null)
        //        {
        //            objNewElm = p_objXmlDoc.CreateElement("option");
        //            objNewElm.SetAttribute("value", "0");
        //            objNewElm.InnerText = "--SELECT--";
        //            objElm.AppendChild(objNewElm);
        //            objNewElm = null;
        //            objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
        //            while (objRdr.Read())
        //            {
        //                objNewElm = p_objXmlDoc.CreateElement("option");
        //                objNewElm.SetAttribute("value", objRdr.GetInt32(0).ToString());
        //                objNewElm.InnerText = objRdr.GetString(1);
        //                objElm.AppendChild(objNewElm);
        //                objNewElm = null;
        //            }
        //        }
                
        //        objRdr.Dispose();
        //        return p_objXmlDoc;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("SystemParameter.FinHistChange.Err"), p_objEx);
        //    }
          
        //}
	}
}

