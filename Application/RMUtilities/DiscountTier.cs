using System;
using System.Xml; 
using Riskmaster.Db;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 
using Riskmaster.Security;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author	:   Anurag Agarwal
	///Class	:	DiscTier
	///Dated	:   06 April 2005
	///Purpose	:   This class is use Add / Modify the Discounts Tier data
	/// </summary>
	public class DiscTier:UtilitiesUIBase
	{
		private string m_sUserName = "";
		
		/// <summary>
		/// String array for Discount Tier field mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "DISC_TIER_ROWID"},
			{"AddedByUser", "ADDED_BY_USER"},
			{"DttmRcdAdded", "DTTM_RCD_ADDED"},
			{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
			{"UpdatedByUser", "UPDATED_BY_USER"},
			{"TierName", "TIER_NAME"},
			{"UseDiscTier", "IN_USE_FLAG"},
			{"LOBcode", "LINE_OF_BUSINESS"},
			{"Statecode", "STATE"},
			{"EffectiveDate", "EFFECTIVE_DATE"},
			{"ExpirationDate", "EXPIRATION_DATE"},
			{"ParentLevel", "PARENT_LEVEL"}
									  };

		private string m_sConnectionString = "";

		private XmlDocument m_objXMLDocument = null;

        private int m_iClientId = 0;

		/// <summary>
		/// Constructor for Discount Tier
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public DiscTier(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
			m_sConnectionString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public DiscTier(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			m_sUserName = p_sLoginName;
			ConnectString = p_sConnectionString;
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="p_userLogin">User Login</param>
        /// <param name="p_sConnectionString">Connection String</param>
        public DiscTier(UserLogin p_userLogin, string p_sConnectionString, int p_iClientId)
            : base(p_userLogin,p_sConnectionString, p_iClientId)
        {
            m_sUserName = p_userLogin.LoginName;
            ConnectString = p_sConnectionString;
            m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }
		/// <summary>
		/// Initialize private variables and properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_POL_DISC_TIER";
			KeyField = "DISC_TIER_ROWID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// <summary>
		/// Get the data for the Discount Tier form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data structure</param>
		/// <returns>Xml document with data and the form structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XMLDoc = p_objXmlDocument;

			try
			{
				base.Get();
				UpdateParentLevel("get");
				return XMLDoc; 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DiscountTier.Get.Error", m_iClientId), p_objEx);
			}
		}

		/// <summary>
		/// Get the data for the Discount Tier form
		/// </summary>
		/// <param name="p_objXmlDocument">xml document to be saved</param>
		/// <returns>Xml document with data and the form structure</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				m_objXMLDocument = p_objXmlDocument;
				if(ValidateXMLData()) 
				{
					FillXML();
					UpdateParentLevel("save");
					base.Save();
				}
				return XMLDoc;
			}
			catch(RMAppException p_objExp)
			{
				throw p_objExp; 
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DiscountTier.Save.Error", m_iClientId), p_objEx);
			}
		}

		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes Discount Tier record
		/// </summary>
		/// <param name="p_iDiscTierRowId">Discount Tier ID</param>
		/// <returns>True for sucess and false for failure</returns>
		internal bool Delete(int p_iDiscTierRowId)
		{
			string sSQL = "";
			DbConnection objCon = null; 
			DbReader objDbReader = null;
			string sLOB = "";
			string sState = "";

            try
            {
                //Do not allow deletion of the Modified Premium level tier
                if (p_iDiscTierRowId == 1)
                {
                    throw new RMAppException(Globalization.GetString("DiscountTier.Delete.DeletionNotAllowed", m_iClientId));
                }

                //Getting LOB & State value
                sSQL = "SELECT * FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID=" + p_iDiscTierRowId;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        sLOB = objDbReader["LINE_OF_BUSINESS"].ToString();
                        sState = objDbReader["STATE"].ToString();
                    }
                }
                objDbReader.Dispose();

                //Do not allow a discount tier to be deleted if a lower level tier is attached
                sSQL = "SELECT * FROM SYS_POL_DISC_TIER WHERE LINE_OF_BUSINESS=" + sLOB + " AND STATE=" + sState + " AND PARENT_LEVEL=" + p_iDiscTierRowId;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                        throw new RMAppException(Globalization.GetString("DiscountTier.Delete.LowerDiscountAttached", m_iClientId));
                }
                objDbReader.Dispose();

                //A Tier cannot be removed if it has a discount attached to it
                sSQL = "SELECT * FROM SYS_POL_SEL_DISC WHERE PARENT_LEVEL=" + p_iDiscTierRowId;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                        throw new RMAppException(Globalization.GetString("DiscountTier.Delete.DiscountAttached", m_iClientId));
                }
                objDbReader.Dispose();

                if (p_iDiscTierRowId != 0)
                {
                    objCon = DbFactory.GetDbConnection(m_sConnectionString);
                    objCon.Open();
                    sSQL = "DELETE FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + p_iDiscTierRowId;
                    objCon.ExecuteNonQuery(sSQL);
                    objCon.Dispose();
                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("DiscountTier.Delete.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objCon != null)
                    objCon.Dispose();
            }
        }


		/// <summary>
		/// Function to load user details before saving into the database
		/// </summary>
		private void FillXML()
		{
			string sThresholdRowId = "";
			XmlNodeList objNodeList = null;
			string sNodename = string.Empty;

            // npadhy Start MITS 19144 When the Discount Tier is Updated the Date Time Added and Added by User is not passed.
            // Now it getting passed as the same Value as in DB
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sAddedByUser = string.Empty;
            string sDateTimeAdded = string.Empty;

			sThresholdRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;

            if (sThresholdRowId != "")
            {
                sSQL = "SELECT ADDED_BY_USER, DTTM_RCD_ADDED FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID=" + sThresholdRowId;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        sAddedByUser = objDbReader["ADDED_BY_USER"].ToString();
                        sDateTimeAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
                    }
                }
                objDbReader.Dispose();
            }

			objNodeList=m_objXMLDocument.GetElementsByTagName("control");

			foreach(XmlElement objXMLElement in objNodeList)
			{
				sNodename = objXMLElement.GetAttribute("name");  
				switch(sNodename)
				{
					case "AddedByUser":
						if(sThresholdRowId == "")
							objXMLElement.InnerText = m_sUserName; 
                        else
                            objXMLElement.InnerText = sAddedByUser; 
						break;
					case "DttmRcdAdded":
                        if (sThresholdRowId == "")
                            objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
                        else
                            objXMLElement.InnerText = sDateTimeAdded;
						break;
					case "DttmRcdLastUpd":
                        objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
						break;
					case "UpdatedByUser":
						objXMLElement.InnerText = m_sUserName; 
						break;
				}
			}
		}
        // npadhy End MITS 19144 When the Discount Tier is Updated the Date Time Added and Added by User is not passed.
        // Now it getting passed as the same Value as in DB

		/// <summary>
		/// Validates the Input XML
		/// </summary>
		/// <returns></returns>
		private bool ValidateXMLData()
		{
			string sLOBCode = "";
			string sStateCode = "";
			string sEffectiveDate = "";
			string sExpirationDate = "";
			string sParentLevel = "";
			string sSQL = "";
			string sDiscountTierRowId = "";
			string sTierName = "";
			string sUseDiscTier = "";
			DbReader objDBReader = null;
			TimeSpan stTimeSpan;
			string sRowID = "";
			string sDate = "";
			string sName = "";
            string sState = "";
			bool bUse = false;
			string sLOB = "";
			int iCount = 0;

			string sValue=string.Empty;

            SysSettings objSettings = null;

			try
			{
				sDiscountTierRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;
				sLOBCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='LOBcode']")).Attributes["codeid"].Value;
				sStateCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='Statecode']")).Attributes["codeid"].Value;
				sEffectiveDate = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='EffectiveDate']")).InnerText;
				sExpirationDate = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='ExpirationDate']")).InnerText;
				sParentLevel = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='ParentLevel']")).Attributes["codeid"].Value;
				sTierName = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='TierName']")).InnerText;

                objSettings = new SysSettings(m_sConnectionString, m_iClientId);

				//get the value of the checkbox
				sValue=((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='UseDiscTier']")).InnerText;
				if (sValue == "True")
					sUseDiscTier="1";
				else
					sUseDiscTier="0";
				
				if(sParentLevel.Trim() == "")
                    throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.ApplLevelMissing", m_iClientId));
				else
				{
					// 1. first make sure there isn't a conflict b/w states or lob and that aren't attaching to itself
					//   also, if one adding is sel. for use, the one attach to must be as well
					sSQL = "SELECT * FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + sParentLevel;

					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

					if(objDBReader.Read())
					{
						sState = objDBReader["STATE"].ToString();
						sRowID = objDBReader["DISC_TIER_ROWID"].ToString();
						sLOB = objDBReader["LINE_OF_BUSINESS"].ToString();
						bUse = Conversion.ConvertStrToBool(objDBReader["IN_USE_FLAG"].ToString());

						if(!bUse && sUseDiscTier == "1")
                            throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.InvalidLevel", m_iClientId));
						if(sRowID.Trim() == sDiscountTierRowId.Trim())
                            throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.LevelAlreadyExist", m_iClientId));
						if(sLOB.Trim() != string.Empty)
						{
							if(sLOBCode.Trim() == string.Empty)
                                throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.IncorrectLOB", m_iClientId));
							else if(sLOBCode.Trim() != sLOB.Trim())
                                throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.InvalidLOB", m_iClientId));
						}
						if(sState.Trim() != string.Empty)
						{
							if(sStateCode.Trim() == string.Empty)
                                throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.IncorrectState", m_iClientId));
							else if(sStateCode.Trim() != sState.Trim())
                                throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.StateMatchError", m_iClientId));
						}
					}//if(objDBReader.Read())

					if(objDBReader != null)
						objDBReader.Close();

					//2. if already have one at the level for that lob and state, error
					sSQL = "SELECT * FROM SYS_POL_DISC_TIER WHERE (LINE_OF_BUSINESS = " + sLOBCode + 
						" OR LINE_OF_BUSINESS = 0) AND (STATE = " + sStateCode + " ) AND PARENT_LEVEL = " + sParentLevel;

					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

					while(objDBReader.Read())
					{
						sRowID = objDBReader["DISC_TIER_ROWID"].ToString();
						sDate = objDBReader["EXPIRATION_DATE"].ToString();
						bUse = Conversion.ConvertStrToBool(objDBReader["IN_USE_FLAG"].ToString());

						if(sRowID != sDiscountTierRowId)
						{
							if(bUse)
							{
								if(sDate != "")
								{  // Divya 04/20/2007 Conversion.todate does not work for dd/mm/yyyy format
									sDate = Conversion.GetDate(sDate);
									stTimeSpan =  (Convert.ToDateTime(sEffectiveDate))-Conversion.ToDate(sDate);
									if(stTimeSpan.TotalDays <= 0)
                                        throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.AlreadyExistsWithNoExpDate", m_iClientId));
								}
								else
								{
									if(sExpirationDate == "")
                                        throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.AlreadyExists", m_iClientId));
									else
									{
										stTimeSpan = (Convert.ToDateTime(sEffectiveDate))-Convert.ToDateTime(sExpirationDate);
										if(stTimeSpan.Days <= 0)
                                            throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.AlreadyExistsWithNoExpDate", m_iClientId));
									}
								}//if(sDate != "")
							}//if(bUse)
						}//if(sRowID != sDiscountTierRowId)
					}//while(objDBReader.Read())

					if(objDBReader != null)
						objDBReader.Close();

					//do not allow for tiers to have identical names, state, lob and dates
					sSQL = "SELECT * FROM SYS_POL_DISC_TIER WHERE (LINE_OF_BUSINESS = " + sLOBCode + 
						" OR LINE_OF_BUSINESS = 0) AND (STATE=0 OR STATE = " + sStateCode + " )";

					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

					while(objDBReader.Read())
					{
						sName = objDBReader["TIER_NAME"].ToString();
						sRowID = objDBReader["DISC_TIER_ROWID"].ToString();
						sDate = objDBReader["EFFECTIVE_DATE"].ToString();
					
						if(sName == sTierName && sRowID != sDiscountTierRowId)
						{
							if(sDate != "")
							{
								sDate = Conversion.GetDate(sDate);
								stTimeSpan = Convert.ToDateTime(sEffectiveDate)- Conversion.ToDate(sDate) ;
								if(stTimeSpan.TotalDays == 0)
									throw new RMAppException(Globalization.GetString("DiscountTier.ValidateXMLData.TierAlreadyExists", m_iClientId));
							}
						}
					}//while(objDBReader.Read())

					if(objDBReader != null)
						objDBReader.Close();


                    // npadhy Start MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
                    // Instead of hardcoding 15, we will fetch the value for DB
					//have to make sure there aren't more than 5 selected for one LOB and state at one time
					sSQL = "SELECT * FROM SYS_POL_DISC_TIER WHERE LINE_OF_BUSINESS = " + sLOBCode + " OR LINE_OF_BUSINESS = 0";

					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

					while(objDBReader.Read())
					{
						sState = objDBReader["STATE"].ToString();
						sRowID = objDBReader["DISC_TIER_ROWID"].ToString();
						sDate = objDBReader["EXPIRATION_DATE"].ToString();
						bUse = Conversion.ConvertStrToBool(objDBReader["IN_USE_FLAG"].ToString());

						if(sRowID != "1" && sRowID != sDiscountTierRowId)
						{
							if(sState.Trim() == sStateCode.Trim() || sState == string.Empty)
							{
								if(bUse)
								{
									if(sDate != "")
									{
										sDate = Conversion.GetDate(sDate);
										stTimeSpan =  Convert.ToDateTime(sEffectiveDate)-Conversion.ToDate(sDate) ;
										if(stTimeSpan.TotalDays < 1)
											iCount++;	
									}
									else
										iCount++;
								}
							}
						}
					}//while(objDBReader.Read())

					if(objDBReader != null)
						objDBReader.Close();

                    if (iCount >= objSettings.MaxDiscTier)
                        throw new RMAppException(String.Format(Globalization.GetString("DiscountTier.ValidateXMLData.DiscTierLimitCrossed", m_iClientId), objSettings.MaxDiscTier));
                    // npadhy End MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
				}
				return true;
			}
			finally
			{
				if(objDBReader != null)
					objDBReader.Dispose();

                if (objSettings != null)
                    objSettings = null;
			}
		}
		private void UpdateParentLevel(string p_sMode)
		{
			string sParentLevel;
			XmlElement objElement=null;
			DbConnection objConn = null;
			DbReader objDbReader = null;
			string sSQL=string.Empty;
	
			if (p_sMode=="get")
			{
				objElement=(XmlElement)XMLDoc.SelectSingleNode("//control[@name='ParentLevel']");
				sParentLevel=objElement.InnerText;

				sSQL="SELECT TIER_NAME FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID =" + sParentLevel;

				try
				{
					objConn = DbFactory.GetDbConnection(ConnectString);
					objConn.Open(); 
					objDbReader = objConn.ExecuteReader(sSQL); 
			
					if (objDbReader != null)
					{
						if(objDbReader.Read())
						{
							objElement.SetAttribute("codeid",sParentLevel);
							objElement.InnerText=objDbReader["TIER_NAME"].ToString();
						}
						objDbReader.Close();
					}
					objConn.Dispose(); 
				}
				finally
				{
					if(objDbReader != null)
					{
						objDbReader.Dispose();
						objDbReader=null;
					}
					if(objConn!= null)
					{
						objConn.Dispose();
						objConn=null;
					}
				}
			}
			else
			{
				objElement=(XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='ParentLevel']");
				objElement.InnerText=objElement.Attributes["codeid"].Value;
			}
		}
	}

	/// <summary>
	/// Summary description for DiscountTier.
	/// </summary>
	public class DiscountTier
	{
		public DiscountTier()
		{
		}

		/// <summary>
		/// Stores the disctierrowid value
		/// </summary>
		private int m_iDiscTierRowId = 0;

		/// <summary>
		/// Stores the use value
		/// </summary>
		private bool m_bUse = false;

		/// <summary>
		/// Stores the tiername value
		/// </summary>
		private string m_sTierName = string.Empty;

		/// <summary>
		/// Stores the lob value
		/// </summary>
		private int m_iLOB = 0;

		/// <summary>
		/// Stores the state value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the bjparentlevel value
		/// </summary>
		private string m_sParentLevel = string.Empty;

		/// <summary>
		/// Stores the effectivedate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the expirationdate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the addedbyuser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the dttmrcdadded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the updatedbyuser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the dttmrcdlastupd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the datachanged value
		/// </summary>
		private bool m_bDataChanged = false;

		/// <summary>
		/// Read/Write property for disctierrowid
		/// </summary>
		internal int DiscTierRowId
		{
			get
			{
				return m_iDiscTierRowId;
			}
			set
			{
				m_iDiscTierRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for use
		/// </summary>
		internal bool Use
		{
			get
			{
				return m_bUse;
			}
			set
			{
				m_bUse = value;
			}
		}

		/// <summary>
		/// Read/Write property for tiername
		/// </summary>
		internal string TierName
		{
			get
			{
				if(m_sTierName.Length > 30)
					return m_sTierName.Substring(0,29);
				else
					return m_sTierName;
			}
			set
			{
				m_sTierName = value;
			}
		}

		/// <summary>
		/// Read/Write property for lob
		/// </summary>
		internal int LOB
		{
			get
			{
				return m_iLOB;
			}
			set
			{
				m_iLOB = value;
			}
		}

		/// <summary>
		/// Read/Write property for state
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for bjparentlevel
		/// </summary>
		internal string ParentLevel
		{
			get
			{
				return m_sParentLevel;
			}
			set
			{
				m_sParentLevel = value;
			}
		}

		/// <summary>
		/// Read/Write property for effectivedate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for expirationdate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for addedbyuser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for dttmrcdadded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for updatedbyuser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for dttmrcdlastupd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for datachanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
	}
}
