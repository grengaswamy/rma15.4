﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Collections;
using System.Xml.Linq;
namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    /// 
    /// </summary>
    public class HistoryDesigner
    {
        private string m_sConnectionString = string.Empty;
        private string m_sUserName = string.Empty;
        private int m_iClientId = 0;

        public HistoryDesigner(string p_sLoginName, string p_sConnectionstring, int p_iClientId)
        {
            m_sUserName = p_sLoginName;
            m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// This function will get the table id,name and enabled flag from hist_track_tables 
        /// to render only table nodes in the tree
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument GetTables(XmlDocument p_objXmlDocument)
        {
            XmlDocument objXmlResponse = null;
            XmlNode objXmlNode = null;
            XmlNode objRootNode = null;
            XmlNode objTmpNode = null;
            XmlAttribute objXMLAtt = null;

            string sSQL = string.Empty;                       
            String sTableId = String.Empty;
            String sTableName = String.Empty;                     
            
            LocalCache objCache = null;
            Dictionary<string, string> objReportReqFields = null;
            XmlNodeList objNodeList = null;
            int iCount = 1;

            try
            {
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                XmlDocument objDoc = objCache.GetXmlForHistTrack();
                if (objDoc != null)
                {
                    objReportReqFields = new Dictionary<string, string>();
                    objNodeList = objDoc.SelectNodes("//Table");
                    foreach (XmlNode objTable in objNodeList)
                    {                       
                        objReportReqFields.Add("TABLE_" + iCount++, objTable.Attributes["name"].Value);                        
                    }
                }


                objXmlResponse = new XmlDocument();
                objXmlNode = objXmlResponse.CreateElement("Document");
                objXmlResponse.AppendChild(objXmlNode);

                objRootNode = objXmlResponse.CreateElement("Root");                            

                sSQL = "SELECT TABLE_ID,TABLE_NAME,USER_PROMPT, ENABLED_FLAG FROM HIST_TRACK_TABLES ORDER BY USER_PROMPT";
                using (DbReader objDBReaderForTables = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objDBReaderForTables.Read())
                    {
                        sTableName = objDBReaderForTables.GetString("TABLE_NAME");

                      
                        objXmlNode = objXmlResponse.CreateElement("Table");

                        objXMLAtt = objXmlResponse.CreateAttribute("Id");
                        sTableId = objDBReaderForTables.GetInt32("TABLE_ID").ToString();
                        objXMLAtt.Value = sTableId;
                        objXmlNode.Attributes.Append(objXMLAtt);

                        objXMLAtt = objXmlResponse.CreateAttribute("Name");
                        objXMLAtt.Value = objDBReaderForTables.GetString("USER_PROMPT");
                        objXmlNode.Attributes.Append(objXMLAtt);

                        objXMLAtt = objXmlResponse.CreateAttribute("Track_FLAG");
                        objXMLAtt.Value = objDBReaderForTables.GetInt16("ENABLED_FLAG").ToString();
                        objXmlNode.Attributes.Append(objXMLAtt);

                        //If Table is required for reports,set it as true
                        objXMLAtt = objXmlResponse.CreateAttribute("RequiredForReport_FLAG");
                        if (objReportReqFields != null)
                        {
                            objXMLAtt.Value = objReportReqFields.ContainsValue(sTableName).ToString();
                        }
                        else
                        {
                            objXMLAtt.Value = "False";
                        }                      
                        objXmlNode.Attributes.Append(objXMLAtt);

                        objRootNode.AppendChild(objXmlNode);
                    }
                }
                objXmlResponse.FirstChild.AppendChild(objRootNode);
                objXmlNode = p_objXmlDocument.SelectSingleNode("//DataChanged");
                if (objXmlNode != null)
                {
                    objTmpNode = objXmlResponse.CreateElement("DataChanged");
                    objTmpNode.InnerText = objXmlNode.InnerText;
                    objXmlResponse.FirstChild.AppendChild(objTmpNode);
                }
                return objXmlResponse;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.GetTables.Error", m_iClientId), p_objEx);

            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }

        /// <summary>
        /// This function will get the columns of the selected Table
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument GetColumns(XmlDocument p_objXmlDocument)
        {
            XmlDocument objXmlResponse = null;
            XmlNode objXmlNode = null;
            XmlNode objRootNode = null;
            XmlNode objTmpNode = null;
            XmlAttribute objXMLAtt = null;
            XmlNodeList objNodeList = null;
            string sSelectedName = string.Empty;
            string sSelectedTableId = "0";
            string sSQL = string.Empty;

            LocalCache objCache = null;
            Dictionary<string, string> objReportReqFields = null;
            string sTableName = string.Empty;
            string sColumnName = string.Empty;
            int iCount = 1;
            try
            {
                objXmlResponse = new XmlDocument();
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                XmlDocument objDoc = objCache.GetXmlForHistTrack();
                if (objDoc != null)
                {
                    objReportReqFields = new Dictionary<string, string>();
                    objNodeList = objDoc.SelectNodes("//Table");
                    foreach (XmlNode objTable in objNodeList)
                    {
                        sTableName = objTable.Attributes["name"].Value;
                        foreach (XmlNode objColumn in objTable)
                        {
                            sColumnName = objColumn.Attributes["name"].Value;
                            objReportReqFields.Add("COLUMN_" + iCount++, string.Format("{0}.{1}", objTable.Attributes["name"].Value, objColumn.Attributes["name"].Value));
                        }
                    }
                }

                objXmlNode = objXmlResponse.CreateElement("Document");
                objXmlResponse.AppendChild(objXmlNode);

                objRootNode = objXmlResponse.CreateElement("Root");                

                objXmlNode = p_objXmlDocument.SelectSingleNode("//SelectedTableId");
                if (objXmlNode != null && !string.IsNullOrEmpty(objXmlNode.InnerText))
                {
                    sSelectedTableId = objXmlNode.InnerText;
                    sSQL = string.Format(@"SELECT CASE WHEN(HC.COLUMN_ID IS NULL) THEN 0 ELSE -1 END COLUMN_SELECTED,HT.TABLE_NAME, 
                            HD.COLUMN_NAME, HD.COLUMN_ID HD_COLUMN_ID,HD.USER_PROMPT,HD.PRIMARY_KEY_FLAG 
                            FROM HIST_TRACK_DICTIONARY HD 
                            INNER JOIN HIST_TRACK_TABLES HT ON HT.TABLE_ID=HD.TABLE_ID 
                            LEFT OUTER JOIN HIST_TRACK_COLUMNS HC ON HC.COLUMN_ID=HD.COLUMN_ID 
                            WHERE HD.TABLE_ID = {0} ORDER BY HD.USER_PROMPT", sSelectedTableId);

                    using (DbReader objDBReader = DbFactory.ExecuteReader(m_sConnectionString, sSQL))
                    {
                        while (objDBReader.Read())
                        {

                            objXmlNode = objXmlResponse.CreateElement("Column");

                            objXMLAtt = objXmlResponse.CreateAttribute("Id");
                            objXMLAtt.Value = objDBReader.GetInt("HD_COLUMN_ID").ToString();
                            objXmlNode.Attributes.Append(objXMLAtt);

                            objXMLAtt = objXmlResponse.CreateAttribute("Name");
                            objXMLAtt.Value = objDBReader.GetString("USER_PROMPT");
                            objXmlNode.Attributes.Append(objXMLAtt);

                            objXMLAtt = objXmlResponse.CreateAttribute("PrimaryKey");
                            objXMLAtt.Value = objDBReader.GetInt16("PRIMARY_KEY_FLAG").ToString();
                            objXmlNode.Attributes.Append(objXMLAtt);

                            objXMLAtt = objXmlResponse.CreateAttribute("Track_FLAG");
                            objXMLAtt.Value = objDBReader.GetInt("COLUMN_SELECTED").ToString();
                            objXmlNode.Attributes.Append(objXMLAtt);

                            //If Field is required for reports,set it as true
                            objXMLAtt = objXmlResponse.CreateAttribute("RequiredForReport_FLAG");
                            if (objReportReqFields != null)
                            {
                                objXMLAtt.Value = objReportReqFields.ContainsValue(string.Format("{0}.{1}", objDBReader.GetString("TABLE_NAME"), objDBReader.GetString("COLUMN_NAME"))).ToString();
                            }
                            else
                            {
                                objXMLAtt.Value ="False";
                            }                            
                            objXmlNode.Attributes.Append(objXMLAtt);

                            objRootNode.AppendChild(objXmlNode);

                        }
                    }
                }
                objXmlResponse.FirstChild.AppendChild(objRootNode);
                //During PostBack:Value of DataChanged was getting lost
                objXmlNode = p_objXmlDocument.SelectSingleNode("//DataChanged");
                if(objXmlNode != null)
                {
                    objTmpNode = objXmlResponse.CreateElement("DataChanged");
                    objTmpNode.InnerText = objXmlNode.InnerText;
                    objXmlResponse.FirstChild.AppendChild(objTmpNode);
                }
                return objXmlResponse;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.GetColumns.Error", m_iClientId), p_objEx);

            }
            finally
            {
                objReportReqFields = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }

        /// <summary>
        /// This function will save all selection/deselection of tables and columns
        /// </summary>
        /// <param name="p_objDocIn"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objDocIn)
        {
            try
            {
                SaveTables(p_objDocIn);
                SaveColumns(p_objDocIn);
                UpdateChangeFlagOfTables(p_objDocIn);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.Save.Error", m_iClientId), p_objEx);
            }
            return true;
        }

        /// <summary>
        /// This function will get all those tables which are either checked/unchecked
        /// and will update the update and enable flags accordingly in HIST_TRACK_TABLES
        /// </summary>
        /// <param name="p_objDocIn"></param>
        private void SaveTables(XmlDocument p_objDocIn)
        {
            Dictionary<int, string> objReportFields = null;
            string sSelectedTableIds = string.Empty;
            string sDeselectedTableIds = string.Empty;

            try
            {
                XDocument xDocIn = XDocument.Load(new XmlNodeReader(p_objDocIn));                

                #region Updating Seleted Tables
                // Getting those tables which are selected by user and updating enable flag as -1 in HIST_TRACK_TABLES
                //Also Adding Primary columns and required field for reports  in hist_track_columns
                sSelectedTableIds = string.Join(",", (from table in xDocIn.Descendants("Table")
                                                      where table.Attribute("Selected") != null && table.Attribute("Selected").Value == "-1"
                                                      select table.Attribute("id").Value).ToArray<string>());

                if (sSelectedTableIds != string.Empty)
                {
                    UpdateTables(sSelectedTableIds, "-1");
                    AddOrRemovePrimaryColumns(sSelectedTableIds, "Add");                    
                }

                #endregion

                #region Updating Deseleted Tables
                //Getting those tables which are Deselected by user and updating enable flag as 0 in HIST_TRACK_TABLES
                //Also Removing Primary columns and required field for reports from hist_track_columns
                sDeselectedTableIds = string.Join(",", (from table in xDocIn.Descendants("Table")
                                                        where table.Attribute("Selected") != null && table.Attribute("Selected").Value == "0"
                                                        select table.Attribute("id").Value).ToArray<string>());

                if (sDeselectedTableIds != string.Empty)
                {
                    UpdateTables(sDeselectedTableIds, "0");
                    AddOrRemovePrimaryColumns(sDeselectedTableIds, "Remove");                    
                }

                #endregion

            }            
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.SaveTables.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// This function will update the enabled and update flag for input table list
        /// </summary>
        /// <param name="p_sTableIds"></param>
        /// <param name="p_sEnabledFlag"></param>
        public void UpdateTables(string p_sTableIds, string p_sEnabledFlag)
        {
            StringBuilder sbSQL = null;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("UPDATE HIST_TRACK_TABLES SET UPDATED_FLAG = -1");

                if (p_sEnabledFlag != "")
                    sbSQL.AppendFormat(", ENABLED_FLAG = {0} ", p_sEnabledFlag);

                sbSQL.AppendFormat(", UPDATED_BY_USER = '{0}'", m_sUserName);
                sbSQL.AppendFormat(", DTTM_RCD_LAST_UPD ='{0}'", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                sbSQL.AppendFormat(" WHERE TABLE_ID IN ({0})", p_sTableIds);

                //We will not set UPDATED_FLAG for those tables which are already disabled if user is changing just its column selection
                //otherwise it will create problem in transferring data from audit tables
                if (p_sEnabledFlag == "")
                    sbSQL.Append(" AND ENABLED_FLAG <> 0");

                using (DbConnection oConnection = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    oConnection.Open();
                    oConnection.ExecuteNonQuery(sbSQL.ToString());
                    sbSQL.Length = 0;
                    oConnection.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.UpdateTables.Error", m_iClientId), p_objEx);
            }
            finally
            {
            }
        }

        /// <summary>
        /// This function will get and save those columns which are selected 
        /// and update the updated_flag corresponding to their table in hist_track_tables
        /// </summary>
        /// <param name="p_objDocIn"></param>
        private void SaveColumns(XmlDocument p_objDocIn)
        {
            XElement objXmlIn = null;
            string sColumnIds = string.Empty;
            try
            {
                objXmlIn = XElement.Load(new XmlNodeReader(p_objDocIn));
                ArrayList arrColumnIds = new ArrayList((from column in objXmlIn.Descendants("Column")
                                                        where column.Attribute("Selected").Value == "-1"
                                                        select column.Attribute("id").Value).ToArray());

                if (arrColumnIds != null && arrColumnIds.Count != 0)
                {
                    AddColumns(arrColumnIds);
                }

                sColumnIds = string.Join(",", (from column in objXmlIn.Descendants("Column")
                                               where column.Attribute("Selected").Value == "0"
                                               select column.Attribute("id").Value).ToArray());
                if (!string.IsNullOrEmpty(sColumnIds))
                {
                    RemoveColumns(sColumnIds);
                }
            }            
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.SaveColumns.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// This function will insert columns in HIST_TRACK_COLUMNS
        /// </summary>
        /// <param name="p_sArrColumns"></param>
        public void AddColumns(ArrayList p_sArrColumns)
        {
            string sSQL =string.Empty;                       
            try
            {
                using (DbConnection oConnection = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    oConnection.Open();

                    foreach (string sColumn in p_sArrColumns)
                    {
                       sSQL = string.Format(@"INSERT INTO HIST_TRACK_COLUMNS(COLUMN_ID,ADDED_BY_USER,DTTM_RCD_ADDED) VALUES(
                                                   {0},'{1}','{2}')", sColumn, m_sUserName, System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                       oConnection.ExecuteNonQuery(sSQL);                                                    
                    }
                    oConnection.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.AddColumns.Error", m_iClientId), p_objEx);
            }
            finally
            {

            }
        }

        /// <summary>        
        /// This function will delete columns from HIST_TRACK_COLUMNS
        /// </summary>
        /// <param name="p_sColumnsToBeDeleted"></param>
        private void RemoveColumns(string p_sColumnsToBeDeleted)
        {
            string sSql = string.Empty;
            try
            {
                sSql  = string.Format("DELETE FROM HIST_TRACK_COLUMNS WHERE COLUMN_ID IN ({0})", p_sColumnsToBeDeleted);

                using (DbConnection oConnection = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    oConnection.Open();
                    oConnection.ExecuteNonQuery(sSql);                   
                    oConnection.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.RemoveColumns.Error", m_iClientId), p_objEx);
            }
            finally
            {

            }
        }

        /// <summary>
        /// In case Table is neither selected nor deselected but its column selection is changed
        /// Then in that case update flag in hist_track_tables needs to be updated
        /// </summary>
        /// <param name="p_objDocIn"></param>
        private void UpdateChangeFlagOfTables(XmlDocument p_objDocIn)
        {
            string sTableIds = string.Empty;
            XDocument xDocIn = null;
            try
            {
                xDocIn = XDocument.Load(new XmlNodeReader(p_objDocIn));

                sTableIds = string.Join(",", (from table in xDocIn.Descendants("Table")
                                              where table.Attribute("Selected") == null
                                              select table.Attribute("id").Value).ToArray<string>());

                if (sTableIds != string.Empty)
                {
                    UpdateTables(sTableIds, "");
                }
            }         
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.UpdateChangeFlagOfTables.Error", m_iClientId), p_objEx);
            }
        }      

        
        /// <summary>
        /// This function will take list of comma separated table ids and 
        /// add primary column ids of those tables in HIST_TRACK_COLUMNS
        /// </summary>
        /// <param name="p_sTableIds"></param>
        /// <param name="p_sMode"></param>
        public void AddOrRemovePrimaryColumns(string p_sTableIds, string p_sMode)
        {
            string sSql = string.Empty;
            ArrayList arrColumns = null;
            try
            {

                
                arrColumns = new ArrayList();

                sSql =string.Format("SELECT COLUMN_ID FROM HIST_TRACK_DICTIONARY WHERE PRIMARY_KEY_FLAG = -1 AND TABLE_ID IN ({0})", p_sTableIds);
                using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    while (objReader.Read())
                    {
                        arrColumns.Add(objReader.GetInt("COLUMN_ID").ToString());                        
                    }
                }
                if (arrColumns.Count != 0)
                {
                    if (p_sMode == "Add")
                    {
                        AddColumns(arrColumns);
                    }
                    else
                    {
                        RemoveColumns(string.Join(",", (String[])arrColumns.ToArray(typeof(string))));
                    }
                }
            }            
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistoryDesigner.AddOrRemovePrimaryColumns.Error", m_iClientId), p_objEx);
            }
        }       
    }
}
