using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Billing Rule information.
	/// </summary>
	public class BRPayPlan:UtilitiesUIBase
	{
		#region Private Variables
		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields=
		{
			{"RowId","PAY_PLAN_ROWID"},
            {"UsePayPlan", "IN_USE_FLAG"},
			{"PayPlanCode", "PAY_PLAN_CODE"},
			{"BillingRule", "BILLING_RULE_ROWID"},
			{"LOB", "LINE_OF_BUSINESS"},

			{"State", "STATE"},
			{"LateIndicator", "LATE_START_INDIC"},
			{"ApplyAddPremiumto", "EXTRA_AMTS_INDIC"},
			{"InstGenType", "INSTALL_GEN_TYPE"},

			{"OffsetFrom", "OFFSET_INDIC"},
			{"Frequency", "FIXED_FREQUENCY"},
			{"GenDay", "FIXED_GEN_DAY"},
			{"DueDays", "FIXED_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate","FIXED_GEN_DATE"}, 
            {"DueDate","FIXED_DUE_DATE"}, 
            //npadhy RMSC retrofit Ends
            
			{"Install1", "INSTALL1_AMOUNT"},
			{"InstallType1", "INSTALL1_TYPE"},
			{"GenLagDays1", "INSTALL1_LAG_DAYS"},
			{"DueDays1", "INSTALL1_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate1","INSTALL1_GEN_DATE"}, 
            {"DueDate1","INSTALL1_DUE_DATE"}, 
            //npadhy RMSC retrofit Ends

			{"Install2", "INSTALL2_AMOUNT"},
			{"InstallType2", "INSTALL2_TYPE"},
			{"GenLagDays2", "INSTALL2_LAG_DAYS"},
			{"DueDays2", "INSTALL2_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate2","INSTALL2_GEN_DATE"}, 
            {"DueDate2","INSTALL2_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install3", "INSTALL3_AMOUNT"},
			{"InstallType3", "INSTALL3_TYPE"},
			{"GenLagDays3", "INSTALL3_LAG_DAYS"},
			{"DueDays3", "INSTALL3_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate3","INSTALL3_GEN_DATE"}, 
            {"DueDate3","INSTALL3_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install4", "INSTALL4_AMOUNT"},
			{"InstallType4", "INSTALL4_TYPE"},
			{"GenLagDays4", "INSTALL4_LAG_DAYS"},
			{"DueDays4", "INSTALL4_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate4","INSTALL4_GEN_DATE"}, 
            {"DueDate4","INSTALL4_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install5", "INSTALL5_AMOUNT"},
			{"InstallType5", "INSTALL5_TYPE"},
			{"GenLagDays5", "INSTALL5_LAG_DAYS"},
			{"DueDays5", "INSTALL5_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate5","INSTALL5_GEN_DATE"}, 
            {"DueDate5","INSTALL5_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install6", "INSTALL6_AMOUNT"},
			{"InstallType6", "INSTALL6_TYPE"},
			{"GenLagDays6", "INSTALL6_LAG_DAYS"},
			{"DueDays6", "INSTALL6_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate6","INSTALL6_GEN_DATE"}, 
            {"DueDate6","INSTALL6_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install7", "INSTALL7_AMOUNT"},
			{"InstallType7", "INSTALL7_TYPE"},
			{"GenLagDays7", "INSTALL7_LAG_DAYS"},
			{"DueDays7", "INSTALL7_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate7","INSTALL7_GEN_DATE"}, 
            {"DueDate7","INSTALL7_DUE_DATE"},
            //npadhy RMSC retrofit Ends

            {"Install8", "INSTALL8_AMOUNT"},
			{"InstallType8", "INSTALL8_TYPE"},
			{"GenLagDays8", "INSTALL8_LAG_DAYS"},
			{"DueDays8", "INSTALL8_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate8","INSTALL8_GEN_DATE"}, 
            {"DueDate8","INSTALL8_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install9", "INSTALL9_AMOUNT"},
			{"InstallType9", "INSTALL9_TYPE"},
			{"GenLagDays9", "INSTALL9_LAG_DAYS"},
			{"DueDays9", "INSTALL9_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate9","INSTALL9_GEN_DATE"}, 
            {"DueDate9","INSTALL9_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install10", "INSTALL10_AMOUNT"},
			{"InstallType10", "INSTALL10_TYPE"},
			{"GenLagDays10", "INSTALL10_LAG_DAYS"},
			{"DueDays10", "INSTALL10_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate10","INSTALL10_GEN_DATE"}, 
            {"DueDate10","INSTALL10_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install11", "INSTALL11_AMOUNT"},
			{"InstallType11", "INSTALL11_TYPE"},
			{"GenLagDays11", "INSTALL11_LAG_DAYS"},
			{"DueDays11", "INSTALL11_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate11","INSTALL11_GEN_DATE"}, 
            {"DueDate11","INSTALL11_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"Install12", "INSTALL12_AMOUNT"},
			{"InstallType12", "INSTALL12_TYPE"},
			{"GenLagDays12", "INSTALL12_LAG_DAYS"},
			{"DueDays12", "INSTALL12_DUE_DAYS"},
            //npadhy RMSC retrofit Starts
            {"GenDate12","INSTALL12_GEN_DATE"}, 
            {"DueDate12","INSTALL12_DUE_DATE"},
            //npadhy RMSC retrofit Ends

			{"AddedByUser", "ADDED_BY_USER"},
			{"DttmRcdAdded", "DTTM_RCD_ADDED"},
			{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
			{"UpdatedByUser", "UPDATED_BY_USER"}
			
		};
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Xml document object
		/// </summary>
		private XmlDocument m_objXMLDocument = null;

        private int m_iClientId = 0;
		#endregion

        UserLogin m_oUserLogin = null;//vkumar258 ML Chnages
		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public BRPayPlan(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			try
			{
				ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRPayPlan.Constructor.Error", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public BRPayPlan(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			try
			{
				m_sUserName = p_sLoginName;
                m_iClientId = p_iClientId;
				ConnectString = p_sConnectionString;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRPayPlan.Constructor.Error", m_iClientId), p_objEx);
			}
		}
        //vkumar258 RMA-7898 Starts
        public BRPayPlan(UserLogin p_oUserLogin, string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_oUserLogin, p_sConnectionString, p_iClientId)
        {
            try
            {
                m_sUserName = p_sLoginName;
                m_iClientId = p_iClientId;
                ConnectString = p_sConnectionString;
                m_oUserLogin = p_oUserLogin;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BRPayPlan.Constructor.Error", m_iClientId), p_objEx);
            }
        }
        //vkumar258 End
		#endregion

		#region Private Functions
		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			try
			{
				TableName = "SYS_BILL_PAY_PLAN";
				KeyField= "PAY_PLAN_ROWID";
				InitFields(arrFields);
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRPayPlan.Initialize.Error", m_iClientId), p_objEx);
			}
		}
		private void FillXML()
		{
			string sBillingPayPlanRowid = "";
			XmlNodeList objNodeList = null;
			string sNodename = string.Empty;
			XmlElement objNode=null;
			try
			{
				objNode=((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']"));
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sBillingPayPlanRowid = objNode.InnerText;
					}
				}
			
				sBillingPayPlanRowid=sBillingPayPlanRowid.Trim();
				objNodeList=m_objXMLDocument.SelectNodes("//control");
				sBillingPayPlanRowid=sBillingPayPlanRowid.Trim();
				foreach(XmlElement objXMLElement in objNodeList)
				{
					sNodename = objXMLElement.GetAttribute("name");  
					switch(sNodename)
					{
						case "AddedByUser":
							if(sBillingPayPlanRowid == "" || sBillingPayPlanRowid == "-1")
								objXMLElement.InnerText = m_sUserName; 
							break;
						case "DttmRcdAdded":
							if(sBillingPayPlanRowid == "" || sBillingPayPlanRowid == "-1")
								objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToLongDateString()); 
							break;
						case "DttmRcdLastUpd":
							objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToLongDateString());
							break;
						case "UpdatedByUser":
							objXMLElement.InnerText = m_sUserName; 
							break;
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRPayPlan.FillXML.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNodeList = null;
				objNode=null;
			}
		}
		#endregion
		/// <summary>
		/// Key field value
		/// </summary>
		new public string KeyFieldValue
		{
			get
			{
				return Conversion.ConvertObjToStr(base.KeyFieldValue);
			}
		}
		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			string sValue="";
			LocalCache objCache=null;
			string sShortCode="";
			string sShortDesc="";
			DbReader objRead=null;
			string sSql="";
			try
			{
				XMLDoc = p_objXmlDocument;
				Get();
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='PayPlanCode']");			
				/*(if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;
					((XmlElement)objNode).SetAttribute("value",sValue);
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='LOB']");			
				if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;						
					((XmlElement)objNode).SetAttribute("value",sValue);				
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='State']");				
				if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;
					((XmlElement)objNode).SetAttribute("value",sValue);
					objCache.GetStateInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='LateIndicator']");			
				if (objNode.InnerText !=null)
				{
                    sValue=	objNode.InnerText;															
					((XmlElement)objNode).SetAttribute("value",sValue);
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ApplyAddPremiumto']");				
				if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;						
					((XmlElement)objNode).SetAttribute("value",sValue);				
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='InstGenType']");				
				if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;						
					((XmlElement)objNode).SetAttribute("value",sValue);				
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='OffsetFrom']");				
				if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;						
					((XmlElement)objNode).SetAttribute("value",sValue);				
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Frequency']");				
				if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;						
					((XmlElement)objNode).SetAttribute("value",sValue);				
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='GenDay']");				
				if (objNode.InnerText !=null)
				{
					sValue=	objNode.InnerText;						
					((XmlElement)objNode).SetAttribute("value",sValue);				
					objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
					objNode.InnerText=sShortCode+" "+sShortDesc;
				}
				
				for (int i=1;i<=12;i++)
				{
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='InstallType"+i.ToString()+"']");				
					if (objNode.InnerText !=null)
					{
                        sValue=	objNode.InnerText;															
						((XmlElement)objNode).SetAttribute("value",sValue);
						objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
						objNode.InnerText=sShortCode+" "+sShortDesc;
					}
				}
				*/
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='BillingRule']");		
				string sRowId="";
				if (objNode.InnerText !=null)
				{
					if (objNode.InnerText.Trim()!="")
					{
						sRowId=objNode.InnerText;
					}
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='BillingRule']");
				if (sRowId !=null)
				{
					if (sRowId!="")
					{
						sSql="SELECT BILLING_RULE_CODE FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID ="+sRowId;
						objRead=DbFactory.GetDbReader(base.ConnectString,sSql);
						if (objRead.Read())
						{
														
							sValue=Conversion.ConvertObjToStr(objRead.GetValue(0));
							objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
							objNode.InnerText=sShortCode+" - "+sShortDesc;
							objNode = p_objXmlDocument.SelectSingleNode("//internal[@name='BillingRule_cid']");
							objNode.InnerText=sRowId;
						}
					}
				}
						
				objCache.Dispose();
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRPayPlan.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
                if (objRead != null)
                {
                    objRead.Close();
                    objRead.Dispose();
                }
				objNode=null;
				if (objCache!=null)
				{
					objCache.Dispose();
				}
				objCache=null;
			}
			return XMLDoc;
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data to the database
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
			XmlNode objNode=null;
			string sValue="";
			XmlNodeList objXmlNodeList=null;
			string sBillingRuleRowid="";
			int iBillingRuleRowid=0;
			string sLob="";
			string sState="";
			string sPayCode="";
			DbReader objDBReader=null;
			string sInUseFlag="";
			string sSQL="";
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//internal[@name='BillingRule_cid']");
				objXmlNodeList=p_objXmlDocument.SelectNodes("//control[@type='code']");
				sValue=objNode.InnerText;
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='BillingRule']");
				objNode.InnerText=sValue;
				m_objXMLDocument=p_objXmlDocument;
				sBillingRuleRowid = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;
				sLob = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='LOB']")).GetAttribute("codeid");
				sState = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='State']")).GetAttribute("codeid");
				sPayCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='PayPlanCode']")).GetAttribute("codeid");
				iBillingRuleRowid=Conversion.ConvertStrToInteger(sBillingRuleRowid);
				FillXML();
				for (int i=1;i<=12;i++)
				{
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Install"+i.ToString()+"']");				
					if (objNode.InnerText !=null)
					{
						if (objNode.InnerText.Trim().Equals(""))
						{
							objNode.InnerText="0";
						}
					}
				}
				foreach(XmlElement objXmlElement in objXmlNodeList)
				{
					if (objXmlElement.GetAttribute("codeid").Equals(""))
					{
                        //objXmlElement.Attributes["codeid"].Value="null"; Changed by mdhamija MITS 27678
                       objXmlElement.Attributes["codeid"].Value = "0";
					}
				}
				sInUseFlag = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='UsePayPlan']")).InnerText;
				
				if(sInUseFlag == "True")
				{
					if (iBillingRuleRowid>0)
					{
						//Check if the Billing Rule is used by an active pay plan
						sSQL = "SELECT BILLING_RULE_CODE FROM SYS_BILL_BLNG_RULE WHERE IN_USE_FLAG = 0 AND BILLING_RULE_ROWID =" + iBillingRuleRowid.ToString();
						objDBReader = DbFactory.GetDbReader(base.ConnectString,sSQL);
						if(objDBReader.Read())
						{
                            throw new RMAppException(Globalization.GetString("BRPayPlan.ValidateXMLData.InActivePayPlan", m_iClientId));
						}
						objDBReader.Close();
					}
				
					if (iBillingRuleRowid<=0)
					{
						//Check if there is already Billing Rule for the same Billing Code, log error
						sSQL = "SELECT PAY_PLAN_ROWID FROM " +TableName +" WHERE LINE_OF_BUSINESS = "+sLob+
							" AND STATE = "+sState+" AND PAY_PLAN_CODE = "+sPayCode;
						

						objDBReader = DbFactory.GetDbReader(base.ConnectString,sSQL);
						if(objDBReader.Read())
						{
                            throw new RMAppException(Globalization.GetString("BRPayPlan.ValidateXMLData.AlreadyExists", m_iClientId));
						}
						objDBReader.Close();
					}
					else
					{
						//Check if there is already Billing Rule for the same Billing Code, log error
						sSQL = "SELECT PAY_PLAN_ROWID FROM " +TableName +" WHERE LINE_OF_BUSINESS = "+sLob+
							" AND STATE = "+sState+" AND PAY_PLAN_CODE = "+sPayCode+
							 " AND IN_USE_FLAG <> 0 AND PAY_PLAN_ROWID <> "+iBillingRuleRowid.ToString();
								objDBReader = DbFactory.GetDbReader(base.ConnectString,sSQL);
						if(objDBReader.Read())
						{
							if(sInUseFlag == "True")
							{
                                throw new RMAppException(Globalization.GetString("BRPayPlan.ValidateXMLData.AlreadyExists", m_iClientId));
							}
							
						}
						objDBReader.Close();
					}
				}
                else if (sInUseFlag == "")
                {
                    m_objXMLDocument.SelectSingleNode("//control[@name='UsePayPlan']").InnerText = "False";
                }

				XMLDoc=p_objXmlDocument;
				base.Save();
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRPayPlan.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
                if (objDBReader != null)
                {
                    objDBReader.Close();
                    objDBReader.Dispose();
                }
			}
			
		} 
		/// Name		: Delete
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes the Billing Rule information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Delete(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
			try
			{
				XMLDoc=p_objXmlDocument;
				base.Delete();
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRPayPlan.Delete.Error", m_iClientId), p_objEx);
			}
		}
		#endregion
		
	}
}
