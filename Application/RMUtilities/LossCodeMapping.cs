﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Settings;
using System.Xml.Linq;
//abhal3 MITS 36046 start
using System.Text.RegularExpressions; 
using C1.C1PrintDocument;
using System.IO;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections;
using DocumentFormat.OpenXml;
using System.Globalization;
using System.Drawing;
//abhal3 MITS 36046 end
namespace Riskmaster.Application.RMUtilities
{
    public class LossCodeMapping
    {
        string m_sConnString = string.Empty;
        private string m_sUserName = string.Empty;//abhal3 MITS 36046
        private string m_sDsnName = string.Empty;//abhal3 MITS 36046
        private int m_iClientId = 0;
        //Ankit Start : Worked for MITS - 35096
		//abhal3 MITS 36046
        public enum ImportMaping
        {
            SavedSuccessfully = 0,
            DuplicateMappingExists = 1,
            ErrorOccured = 2
        };
        private bool UsePolicyInterface
        {
            get
            {
                Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(m_sConnString,m_iClientId);
                return objSettings.UsePolicyInterface;
            }
        }
        //Ankit End

        public LossCodeMapping(string p_sConnectString)
        {
            m_sConnString = p_sConnectString;
        }
		//abhal3 MITS 36046
        public LossCodeMapping(string p_sConnectString, string sUserName, string sDsnName)
        {
            m_sConnString = p_sConnectString;
            m_sUserName = sUserName;
            m_sDsnName = sDsnName;
        }

        public XmlDocument GetCodeType(XmlDocument p_objXmlDocument, UserLogin objUser)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            StringBuilder sbSQL = null;
            string sSystemTableName = string.Empty;
            //Aman ML Change  --start
            int iUserLangCode = 0;
            int iBaseLangCode = 0;
            bool isBaseLangCode = false;
            bool success;
            //Added By Nitika for FNOL
            SysSettings oSysSettings = null;
            oSysSettings = new SysSettings(m_sConnString,m_iClientId);
            //End : Added By Nitika for FNOL
            try
            {
                iUserLangCode = objUser.objUser.NlsCode;
                iBaseLangCode = Conversion.CastToType<int>(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0], out success);

                if (int.Equals(iUserLangCode, 0) || int.Equals(iBaseLangCode, iUserLangCode))
                    isBaseLangCode = true;
                sbSQL = new StringBuilder();
                sSystemTableName = p_objXmlDocument.SelectSingleNode("SystemTableName").InnerText;

                //sbSQL.Append(" SELECT 'POLICY_CLAIM_LOB' AS SYSTEM_TABLE_NAME, -1 AS CODE_ID, '------SELECT------' AS CODE_DESC, '' AS SHORT_CODE ");
                //if (DbFactory.IsOracleDatabase(m_sConnString))
                //    sbSQL.Append(" FROM DUAL ");
                //sbSQL.Append(" UNION ");
                //sbSQL.Append(" SELECT G.SYSTEM_TABLE_NAME, CT.CODE_ID, CT.CODE_DESC, C.SHORT_CODE ");
                //sbSQL.Append(" FROM CODES C ");
                //sbSQL.Append(" INNER JOIN CODES_TEXT CT On C.CODE_ID = CT.CODE_ID ");
                //sbSQL.Append(" INNER JOIN GLOSSARY G ON G.TABLE_ID = C.TABLE_ID ");
                //sbSQL.Append(" WHERE UPPER(G.SYSTEM_TABLE_NAME) In (" + sSystemTableName + ") ");
                //sbSQL.Append(" And C.DELETED_FLAG <> -1 ");
                //sbSQL.Append(" ORDER BY SYSTEM_TABLE_NAME, CODE_ID, CODE_DESC ");

                sbSQL.Append(" SELECT G.SYSTEM_TABLE_NAME, CT.CODE_ID, CT.CODE_DESC, C.SHORT_CODE,  ");
                if (!isBaseLangCode)
                    sbSQL.Append(" UCT.CODE_DESC USER_CODE_DESC  ");
                else
                    sbSQL.Append(" '' USER_CODE_DESC  ");
                sbSQL.Append(" FROM CODES C ");
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT CT On C.CODE_ID = CT.CODE_ID And CT.LANGUAGE_CODE = " + iBaseLangCode + " ");
                if (!isBaseLangCode)
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCT On C.CODE_ID = UCT.CODE_ID And UCT.LANGUAGE_CODE = " + iUserLangCode + " ");
                sbSQL.Append(" LEFT OUTER JOIN GLOSSARY G ON G.TABLE_ID = C.TABLE_ID ");
                sbSQL.Append(" WHERE UPPER(G.SYSTEM_TABLE_NAME) In (" + sSystemTableName + ") ");
                sbSQL.Append(" And C.DELETED_FLAG <> -1 ");
                sbSQL.Append(" ORDER BY G.SYSTEM_TABLE_NAME, C.SHORT_CODE ");

                objReader = DbFactory.GetDbReader(m_sConnString, sbSQL.ToString());
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("LossCodeMapping");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("CodeTypes");
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("option");
                    objElemTemp.SetAttribute("table_name", Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_TABLE_NAME")));
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")));
                    if (!string.Equals(Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")), "-1", StringComparison.OrdinalIgnoreCase))
                    {
                        if (string.IsNullOrEmpty(Convert.ToString((objReader.GetValue("USER_CODE_DESC")))))
                            objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                        else
                            objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objReader.GetValue("USER_CODE_DESC")));
                        //objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Convert.ToString((objReader.GetValue("USER_CODE_DESC")))))
                            objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                        else
                            objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("USER_CODE_DESC")));
                    }
                    objElemChild.AppendChild(objElemTemp);
                }
                //Aman ML Changes --End
                objDOM.FirstChild.AppendChild(objElemChild);
                //Added By Nitika for FNOL
                if (oSysSettings.MultiCovgPerClm == -1 && (oSysSettings.UsePolicyInterface) && (oSysSettings.FNOLReserve))
                {
                    XmlElement objElemParentFNOL = null;
                    objElemParentFNOL = objDOM.CreateElement("FNOLReserve");
                    objElemParentFNOL.InnerText = "Yes";
                    objElemParent.AppendChild(objElemParentFNOL);
                }
                //Added By Nitika for FNOL
                //Ankit Start : Worked for MITS - 35096
                XmlElement objElemParentUsePI = null;
                objElemParentUsePI = objDOM.CreateElement("UsePI");
                objElemParentUsePI.InnerText = UsePolicyInterface.ToString();
                objElemParent.AppendChild(objElemParentUsePI);
                //Ankit End
                objReader.Close();

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {

                throw new RMAppException(Globalization.GetString("LossCodeMapping.GetCodeType.Error",m_iClientId), p_objEx);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;                
            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }
        }

        public XmlDocument GetLossType(XmlDocument p_objXmlDocument, UserLogin objUser)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            StringBuilder sbSQL = null;
            string sTableId = string.Empty;
            //Ankit Start : Worked on MITS - 34657
            XmlElement objLossSysXMLEle = null;
            bool blnIsRMALossType = false;
            int iMultipleRelationship = 0;
            int iPolicySystemType = 0;
            string sPolicySystemID = "0";
            string sTablePrefix = string.Empty;
            string sPSCodeID = string.Empty;
            string sPolicyLOB = string.Empty;
            string sCvgTypeCode = string.Empty;
            bool success;
            //Ankit End
            //Start Ankit
            bool blnIsMultiDsnSelected = false;
            //End Ankit
            LocalCache objCache = null;
            try
            {
                sbSQL = new StringBuilder();
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("LossCodeMapping");
                objDOM.AppendChild(objElemParent);
                //Ankit Start : Worked on MITS - 34657
                objLossSysXMLEle = (XmlElement)p_objXmlDocument.SelectSingleNode("//LossCodeMappingAdaptor");
                blnIsRMALossType = Convert.ToBoolean(objLossSysXMLEle.GetElementsByTagName("IsRMALossType").Item(0).InnerText);

                sPolicySystemID = objLossSysXMLEle.GetElementsByTagName("PolicySystemID").Item(0).InnerText;
                if (!blnIsRMALossType)
                {
                    iPolicySystemType = Conversion.CastToType<int>(objLossSysXMLEle.GetElementsByTagName("PolicySystemType").Item(0).InnerText, out success);
                }
                else
                {
                    sPolicyLOB = Convert.ToString(objLossSysXMLEle.GetElementsByTagName("PolicyLOB").Item(0).InnerText);
                    sCvgTypeCode = Convert.ToString(objLossSysXMLEle.GetElementsByTagName("CvgTypeCode").Item(0).InnerText);
                    sPSCodeID = Convert.ToString(objLossSysXMLEle.GetElementsByTagName("PSCodeID").Item(0).InnerText);
                }
				
                if (objLossSysXMLEle.GetElementsByTagName("PolicyLOB") != null)
                {
                    objCache = new LocalCache(m_sConnString, m_iClientId);
                    if (objCache.GetShortCode(objCache.GetRelatedCodeId(Conversion.ConvertStrToInteger(objLossSysXMLEle.GetElementsByTagName("PolicyLOB").Item(0).InnerText))) == "WL")
                    {
                        sTableId = Convert.ToString(objCache.GetTableId("DISABILITY_TYPE"));//dbisht6 mits 35554
                        if (!blnIsRMALossType)
                        {
                            if (sPolicySystemID.Contains(","))
                            {
                                sTableId = "";
                                string[] arrsPolicySystemId = sPolicySystemID.Split(',');
                                foreach (string item in arrsPolicySystemId)
                                {
                                    string TempTableid = Convert.ToString(GetTableID("DISABILITY_TYPE", Convert.ToInt32(item), iPolicySystemType, objCache));
                                    if (!string.IsNullOrEmpty(sTableId))
                                    {
                                        sTableId = sTableId + " , " + TempTableid;
                                    }
                                    else
                                    {
                                        sTableId = TempTableid;
                                    }
                                }
                            }
                            else
                                sTableId = Convert.ToString(GetTableID("DISABILITY_TYPE", Convert.ToInt32(sPolicySystemID), iPolicySystemType, objCache));
                        }
                        //Ankit End
                        objElemChild = objDOM.CreateElement("IsWorkersComp");
                        objElemChild.InnerText = "true";
                        objDOM.FirstChild.AppendChild(objElemChild);
                    }
                    else
                    {
                        //Ankit Start : Worked on MITS - 34657
                        sTableId = Convert.ToString(objCache.GetTableId("LOSS_CODES"));
                        if (!blnIsRMALossType)
                        {
                            if (sPolicySystemID.Contains(","))
                            {
                                sTableId = "";
                                string[] arrsPolicySystemId = sPolicySystemID.Split(',');
                                foreach (string item in arrsPolicySystemId)
                                {
                                    string TempTableid = Convert.ToString(GetTableID("LOSS_CODES", Convert.ToInt32(item), iPolicySystemType, objCache));
                                    if (!string.IsNullOrEmpty(sTableId))
                                    {
                                        sTableId = sTableId + " , " + TempTableid;
                                    }
                                    else
                                    {
                                        sTableId = TempTableid;
                                    }
                                }
                            }
                            else
                                sTableId = Convert.ToString(GetTableID("LOSS_CODES", Convert.ToInt32(sPolicySystemID), iPolicySystemType, objCache));
                            // iTableId = GetTableID("LOSS_CODES", iPolicySystemID, iPolicySystemType, objCache);
                        }
                        //Ankit End
                        objElemChild = objDOM.CreateElement("IsWorkersComp");
                        objElemChild.InnerText = "false";
                        objDOM.FirstChild.AppendChild(objElemChild);
                    }
                }
                objElemChild = null;

                if (sPolicySystemID.Contains(","))         //Ankit Start : For MITS - 36046
                {
                    if (blnIsRMALossType)
                    {

                        sbSQL.Append("    SELECT DISTINCT RS1.CODE_DESC,RS1.SHORT_CODE,RS1.CODE_ID  FROM CODES,");
                        sbSQL.Append(" (SELECT CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE,CODES.CODE_ID,  CODES.TABLE_ID  ,  CODES_TEXT.LANGUAGE_CODE");
                        if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                        {
                            //if (blnIsRMALossType && !string.IsNullOrEmpty(sPSCodeID))
                            if (!string.IsNullOrEmpty(sPSCodeID))
                            {
                                sbSQL.Append(" , PCM.ROW_ID");
                            }
                        }
                        sbSQL.Append(" FROM CODES  ");
                        sbSQL.Append(" INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID  ");

                        //AND PCM.PS_CODE_ID IN ( 6811,6871 ) ) RS1");
                        if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                        {
                            //  if (blnIsRMALossType && !string.IsNullOrEmpty(sPSCodeID))
                            if (!string.IsNullOrEmpty(sPSCodeID))
                            {
                                sbSQL.Append(" INNER JOIN POLICY_CODE_MAPPING PCM ON PCM.RMX_CODE_ID = CODES.CODE_ID");
                                sbSQL.Append(" AND PCM.PS_CODE_ID IN ( " + sPSCodeID + " )");
                            }
                        }
                        sbSQL.Append("  AND CODES.DELETED_FLAG = 0");
                        sbSQL.Append(" ) RS1");
                        sbSQL.Append(" ,");
                        sbSQL.Append(" (SELECT CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE,CODES.CODE_ID, CODES.TABLE_ID, CODES_TEXT.LANGUAGE_CODE");
                        if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                        {
                            // if (blnIsRMALossType && !string.IsNullOrEmpty(sPSCodeID))
                            if (!string.IsNullOrEmpty(sPSCodeID))
                            {
                                sbSQL.Append(" , PCM.ROW_ID");
                            }
                        }

                        sbSQL.Append(" FROM CODES  ");
                        sbSQL.Append(" INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID  ");
                        if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                        {
                            //if (blnIsRMALossType && !string.IsNullOrEmpty(sPSCodeID))
                            if (!string.IsNullOrEmpty(sPSCodeID))
                            {
                                sbSQL.Append(" INNER JOIN POLICY_CODE_MAPPING PCM ON PCM.RMX_CODE_ID = CODES.CODE_ID");
                                sbSQL.Append(" AND PCM.PS_CODE_ID IN ( " + sPSCodeID + " )");
                            }
                        }
                        sbSQL.Append("  AND CODES.DELETED_FLAG = 0");
                        sbSQL.Append(" ) RS2");

                        sbSQL.Append(" WHERE RS1.CODE_DESC = RS2.CODE_DESC AND RS1.SHORT_CODE = RS2.SHORT_CODE ");
                        if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                        {
                            //   if (blnIsRMALossType && !string.IsNullOrEmpty(sPSCodeID))
                            if (!string.IsNullOrEmpty(sPSCodeID))
                            {
                                sbSQL.Append(" AND RS1.ROW_ID <> RS2.ROW_ID  ");
                            }
                        }
                        sbSQL.Append(" AND RS1.CODE_ID=CODES.CODE_ID AND RS1.TABLE_ID = RS2.TABLE_ID AND RS1.LANGUAGE_CODE = RS2.LANGUAGE_CODE AND RS1.TABLE_ID  IN (" + sTableId + " ) AND RS1.LANGUAGE_CODE = 1033  ");
                    }
                    else
                    {
                        sbSQL.Append("  SELECT * FROM ( SELECT DISTINCT RS1.* FROM (");
                        int iCounter = 1;
                        foreach (string sPSTableID in sTableId.Split(','))
                        {

                            if (iCounter > 1)
                            {
                                sbSQL.Append(" INNER JOIN ");
                            }

                            sbSQL.Append(" (SELECT CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE,CODES.CODE_ID,  CODES.TABLE_ID  ,  CODES_TEXT.LANGUAGE_CODE FROM CODES   ");
                            sbSQL.Append("    INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID    AND CODES.DELETED_FLAG = 0 WHERE TABLE_ID =" + sPSTableID + ")");
                            sbSQL.Append("RS" + iCounter);

                            if (iCounter > 1)
                            {
                                sbSQL.Append(" ON  RS1.CODE_DESC = RS" + iCounter + ".CODE_DESC AND RS1.SHORT_CODE = RS" + iCounter + ".SHORT_CODE  AND RS1.LANGUAGE_CODE = RS" + iCounter + ".LANGUAGE_CODE");
                            }

                            //inner join will be appended when icounter >1 and on claise all for RS 1
                            iCounter++;

                        }
                        sbSQL.Append("   ))A,CODES WHERE CODES.CODE_ID = A.CODE_ID");
                    }

                }
                //Ankit Start : For MITS - 36046
                else
                {
                    sbSQL.Append(" SELECT CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE,CODES.CODE_ID ");
                    sbSQL.Append(" FROM CODES ");
                    sbSQL.Append(" INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                    {
                        if (blnIsRMALossType && !string.IsNullOrEmpty(sPSCodeID))
                            sbSQL.Append(" INNER JOIN POLICY_CODE_MAPPING PCM ON PCM.RMX_CODE_ID = CODES.CODE_ID AND PCM.PS_CODE_ID = " + sPSCodeID);
                    }
                    sbSQL.Append(" WHERE CODES.TABLE_ID = " + sTableId);
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    sbSQL.Append(" AND CODES.DELETED_FLAG = 0 ");
                }
                //Ankit End
                string sTableName = string.Empty;
                if (sTableId.Contains(","))
                {
                    string[] arrTableid = sTableId.Split(',');
                    foreach (var item in arrTableid)
                    {
                        if (!string.IsNullOrEmpty(sTableName))
                        {
                            sTableName = sTableName + " , '" + objCache.GetTableName(Convert.ToInt32(item)) + "'";
                        }
                        else
                        {
                            sTableName = "'" + objCache.GetTableName(Convert.ToInt32(item)) + "'";
                        }
                        //sTableName = objCache.GetTableName(item);
                    }
                }
                else
                    sTableName = objCache.GetTableName(Convert.ToInt32(sTableId));


                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), sTableName, objUser.objUser.NlsCode);   //Aman ML Change

                //Ankit Start : Worked on MITS - 34657
                if (blnIsRMALossType && UsePolicyInterface)         //Ankit Start : Worked for MITS - 35096
                {
                    bool bFlag = false;
                    foreach (var item in sPSCodeID.Split(','))
                    {
                        bFlag = ShowRecords(sTableId, item, sPolicyLOB, sCvgTypeCode, sPolicySystemID);
                    }
                    if (!string.IsNullOrEmpty(sPSCodeID) && bFlag)
                        objReader = DbFactory.GetDbReader(m_sConnString, sbSQL.ToString());
                }
                else
                    objReader = DbFactory.GetDbReader(m_sConnString, sbSQL.ToString());
                //Ankit End
                objElemChild = objDOM.CreateElement("LossTypes");
                if (objReader != null)			//Ankit Start : Worked on MITS - 34657
                {
                    Dictionary<string, string> dictLossCodes = new Dictionary<string, string>();
                    bool bFlag = false;
                    //Ankit Start : Worked on MITS - 34657
                    while (objReader.Read())
                    {
                        if (!string.IsNullOrEmpty(objReader.GetValue("SHORT_CODE").ToString()))
                        {
                            objElemTemp = objDOM.CreateElement("option");
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")));
                            objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));

                            objElemChild.AppendChild(objElemTemp);
                            if (!dictLossCodes.ContainsKey(Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC"))))
                            {
                                dictLossCodes.Add(Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")), Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")));
                            }
                            else
                            {
                                bFlag = true;
                                dictLossCodes[(Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE"))
                                    + " - "
                                    + Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")))] = dictLossCodes[(Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE"))
                                    + " - "
                                    + Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")))] + "," + Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID"));

                            }
                        }
                    }							//Ankit Start : Worked on MITS - 34657

                    if (dictLossCodes.Count > 0 && bFlag)
                    {
                        XElement result = new XElement("options",
                                     dictLossCodes.Where(j => j.Value.Contains(",")).Select(i => new XElement("option", new XAttribute("value", i.Value), new XAttribute("option_text", i.Key)))
                                     );

                        objElemChild.InnerXml = string.Join(" ", result.DescendantNodes());
                    }

                }
                objDOM.FirstChild.AppendChild(objElemChild);
                if (objReader != null)			//Ankit Start : Worked on MITS - 34657
                    objReader.Close();

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {

                throw new RMAppException(Globalization.GetString("LossCodeMapping.GetCodeType.Error", m_iClientId), p_objEx);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objReader != null)			//Ankit Start : Worked on MITS - 34657
                {								//Ankit Start : Worked on MITS - 34657
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }							//Ankit Start : Worked on MITS - 34657
                    objReader = null;			//Ankit Start : Worked on MITS - 34657
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objReader = null;
            }
        }
        /// <summary>
        /////abhal3 MITS 36046 start
        /// To schedule task programmtically when number of import records exceeed threshold value
        /// </summary>
        /// <param name="iFileContentRowId"></param>
        /// <param name="sPolicySystemIds"></param>
        /// <param name="iReplicateFromPolicySystemId"></param>
        /// <param name="bIsReplication"></param>
        
	private void ScheduleTask(int iFileContentRowId, string sPolicySystemIds, int iReplicateFromPolicySystemId, bool bIsReplication)
        {
            UserLogin objUserLogin = null;
            TaskManager objTaskManager = null;
            string sModule = string.Empty;
            string sReportType = string.Empty;
            string sTMConnectionString = string.Empty;
            int iTimer = 2;

            try
            {
                objUserLogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);
                objTaskManager = new TaskManager(objUserLogin, m_iClientId);
                ScheduleDetails objSchedule;
                string sModulename = string.Empty;
                int iTypeId = 0;
                objSchedule = new ScheduleDetails();
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 1;
                sTMConnectionString = RMConfigurationManager.GetConnectionString("TaskManagerDataSource", m_iClientId);
                string sSql = "SELECT TASK_TYPE_ID  FROM TM_TASK_TYPE  WHERE NAME  = 'Loss Code Mapping' ";

                using (DbReader objRdr = DbFactory.ExecuteReader(sTMConnectionString, sSql))
                {
                    if (objRdr.Read())
                    {
                        iTypeId = Conversion.ConvertObjToInt(objRdr[0], m_iClientId);
                    }
                    else
                    {
                        throw new Exception("TaskNotFound");
                    }
                }
                objSchedule.TaskTypeId = iTypeId;
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";
                objSchedule.FinalRunDTTM = "";
                objSchedule.TaskName = "Loss Code Mapping";
                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;
                objSchedule.Config = "<Task Name=\"Import Loss Code Mapping\" cmdline=\"yes\"><Path>LossCodeMappingImport.exe</Path><Args><arg>"
                          + objUserLogin.objRiskmasterDatabase.DataSourceName
                            + "</arg><arg>"
                            + objUserLogin.LoginName
                            + "</arg><arg>"
                            + bIsReplication.ToString()
                             + "</arg><arg>"
                            + iFileContentRowId
                           + "</arg><arg>"
                            + iReplicateFromPolicySystemId.ToString()
                            + "</arg><arg>"
                            + sPolicySystemIds
                            + "</arg><arg type=\"print\">-j jobid></arg>"
                            + "</Args></Task>";

                #region Duplicacy check

                int iAlreadyScheduledCount = 0;
                Regex regex = new Regex(@"^(?<start>.*){(?<driver>Oracle.*)}(?<end>.*)$", RegexOptions.IgnoreCase);
                if (regex.Match(sTMConnectionString).Success)
                {
                    //for Oracle db
                    sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE TO_CHAR(LOWER(CONFIG)) = TO_CHAR(LOWER('" + objSchedule.Config + "'))";
                }
                else
                {
                    //for sql serevr db
                    sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE LOWER(CONFIG) = LOWER('" + objSchedule.Config + "')";
                }
                using (DbReader objRdr = DbFactory.ExecuteReader(sTMConnectionString, sSql))
                {
                    if (objRdr.Read())
                    {
                        Int32.TryParse(objRdr[0].ToString(), out iAlreadyScheduledCount);
                    }
                }
                if (iAlreadyScheduledCount > 0)
                {
                    // the task is already scheduled. Exiting in which case.
                    return;
                }

                #endregion
                objSchedule.TimeToRun = Conversion.GetTime(DateTime.Now.TimeOfDay.Add(new TimeSpan(0, iTimer, 0)).ToString());
                objSchedule.NextRunDTTM = Conversion.ToDbDateTime(DateTime.Now);
                objSchedule.DayOfMonth = 0;
                objTaskManager.SaveSettings(objSchedule);
                iTimer++;

            }
            catch (Exception e)
            {
                throw new Exception("TaskScheduleException", e.InnerException);
            }
        }
        /// <summary>
        ///Mits 36046
        /// Save mapping for particular policy system in database
        /// </summary>
        /// <param name="iPolicySystemID"></param>
        /// <param name="sPolicyLob"></param>
        /// <param name="sCvgTypeCode"></param>
        /// <param name="sLossCode"></param>
        /// <param name="bSaveType"></param>
        /// <param name="sErrorMsg"></param>
        /// <returns></returns>
        public ImportMaping SaveMapping(int iPolicySystemID, string sPolicyLob, string sCvgTypeCode, string sLossCode, bool bSaveType, ref string sErrorMsg)
        {
            string sSQL = "";
            int iPolicyLob = 0;
            int iCvgTypeCode = 0;
            int iLossCode = 0;
            DbWriter dbWriter = null;
            int iTableId = 0;
            LocalCache objCache = null;
            try
            {
                objCache = new LocalCache(m_sConnString, m_iClientId);
                if (!bSaveType)
                {
                    iPolicyLob = objCache.GetCodeId(sPolicyLob, "POLICY_CLAIM_LOB");
                    if (iPolicyLob == 0)
                    {
                        sErrorMsg = "Policy LOB not found";
                        return ImportMaping.ErrorOccured;
                    }
                    iCvgTypeCode = objCache.GetCodeId(sCvgTypeCode, "COVERAGE_TYPE");
                    if (iCvgTypeCode == 0)
                    {
                        sErrorMsg = "Coverage Type  not found";
                        return ImportMaping.ErrorOccured;
                    }

                    if (objCache.GetRelatedShortCode(iPolicyLob) == "WL")
                    {
                        iLossCode = objCache.GetCodeId(sLossCode, "DISABILITY_TYPE");
                        iTableId = objCache.GetTableId("DISABILITY_TYPE");
                        if (iLossCode == 0)
                        {
                            sErrorMsg = "Loss Code not found";
                            return ImportMaping.ErrorOccured;
                        }

                    }
                    else
                    {
                        iLossCode = objCache.GetCodeId(sLossCode, "LOSS_CODES");
                        iTableId = objCache.GetTableId("LOSS_CODES");
                        if (iLossCode == 0)
                        {
                            sErrorMsg = "Loss Code not found";
                            return ImportMaping.ErrorOccured;
                        }

                    }
                }
                else
                {
                    iPolicyLob = Conversion.ConvertStrToInteger(sPolicyLob);
                    iCvgTypeCode = Conversion.ConvertStrToInteger(sCvgTypeCode);
                    iLossCode = Conversion.ConvertStrToInteger(sLossCode);

                    if (objCache.GetRelatedShortCode(iPolicyLob) == "WL")
                    {
                        iTableId = objCache.GetTableId("DISABILITY_TYPE");
                    }
                    else
                    {
                        iTableId = objCache.GetTableId("LOSS_CODES");

                    }
                } 
                if (!CheckDuplicate(iPolicyLob, iCvgTypeCode, iLossCode, iPolicySystemID, iTableId))
                {
                    dbWriter = DbFactory.GetDbWriter(m_sConnString);
                    dbWriter.Tables.Add("CVG_LOSS_LOB_MAPPING");
                    dbWriter.Fields.Add("ROW_ID", Utilities.GetNextUID(m_sConnString, "CVG_LOSS_LOB_MAPPING", m_iClientId));
                    dbWriter.Fields.Add("POLICY_LOB", iPolicyLob);
                    dbWriter.Fields.Add("CVG_TYPE_CODE", iCvgTypeCode);
                    dbWriter.Fields.Add("LOSS_CODE", iLossCode);
                    dbWriter.Fields.Add("POLICY_SYSTEM_ID", iPolicySystemID);
                    dbWriter.Execute();
                    return ImportMaping.SavedSuccessfully;
                }
                else
                    return ImportMaping.DuplicateMappingExists;
            }
            catch (RMAppException p_objRMExp)
            {
                if (p_objRMExp.InnerException != null)
                    sErrorMsg = p_objRMExp.InnerException.Message;
                else
                    sErrorMsg = p_objRMExp.Message;
                return ImportMaping.ErrorOccured;
            }
            catch (Exception p_objExp)
            {
                if (p_objExp.InnerException != null)
                    sErrorMsg = p_objExp.InnerException.Message;
                else
                    sErrorMsg = p_objExp.Message;
                return ImportMaping.ErrorOccured;
            }
            finally
            {
                if (dbWriter != null)
                {
                    dbWriter = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }
        /// <summary>
        /// Mits 36046
        /// Checks if duplicate mapping exists for imported codes
        /// </summary>
        /// <param name="iPolicyLob"></param>
        /// <param name="iCvgTypeCode"></param>
        /// <param name="iLossCode"></param>
        /// <param name="iPolicySystemID"></param>
        /// <param name="iTableId"></param>
        /// <returns></returns>
        private bool CheckDuplicate(int iPolicyLob, int iCvgTypeCode, int iLossCode, int iPolicySystemID, int iTableId)
        {
            StringBuilder sbSQL = null;
            LocalCache objCache = null;
            bool success;
            Dictionary<string, string> dictParams = null;
            int iRecordCounts = 0;
            bool bReturnValue = false;
            PolicySystemInterface.PolicySystemInterface objPolSystemInterface = null;
            int iPolSysCodeId = 0;
            try
            {
                objPolSystemInterface = new PolicySystemInterface.PolicySystemInterface(m_sConnString, m_iClientId);
                objCache = new LocalCache(m_sConnString, m_iClientId);
                iPolSysCodeId = objPolSystemInterface.GetPSMappedCodeIDFromRMXCodeId(iLossCode, objCache.GetTableName(iTableId), "Policy Code mapping doesnot exists", iPolicySystemID.ToString());
                if ((iPolSysCodeId == 0) || (iPolSysCodeId == iLossCode))
                {
                    throw new Exception("Policy Code mapping for  code:-  " + objCache.GetShortCode(iLossCode) + "  doesnot exists");//change exception message
                }
                sbSQL = new StringBuilder();
                dictParams = new Dictionary<string, string>();
                sbSQL.Append(" SELECT COUNT(ROW_ID) FROM CVG_LOSS_LOB_MAPPING WHERE LOSS_CODE IN ( ");
                sbSQL.Append(" SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE PS_CODE_ID IN(  ");
                sbSQL.Append(" SELECT PS_CODE_ID FROM POLICY_CODE_MAPPING  ");
                sbSQL.AppendFormat(" WHERE RMX_CODE_ID={0} ", "~RMXCODEID~");
                sbSQL.AppendFormat(" AND POLICY_SYSTEM_ID={0} ", "~POLSYSID1~");
                sbSQL.AppendFormat(" AND RMX_TABLE_ID={0} ", "~TABLEID~");
                sbSQL.Append(" )) ");
                sbSQL.AppendFormat(" AND POLICY_LOB={0} ", "~POLLOB~");
                sbSQL.AppendFormat(" AND CVG_TYPE_CODE={0} ", "~CVGCODE~");
                sbSQL.AppendFormat(" AND POLICY_SYSTEM_ID={0} ", "~POLSYSID2~");
                dictParams.Add("RMXCODEID", iLossCode.ToString());
                dictParams.Add("POLSYSID1", iPolicySystemID.ToString());
                dictParams.Add("TABLEID", iTableId.ToString());
                dictParams.Add("POLLOB", iPolicyLob.ToString());
                dictParams.Add("CVGCODE", iCvgTypeCode.ToString());
                dictParams.Add("POLSYSID2", iPolicySystemID.ToString());
                iRecordCounts = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnString, sbSQL.ToString(), dictParams).ToString(), out success);
                if (iRecordCounts > 0)
                    bReturnValue = true;
                else
                    bReturnValue = false;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                dictParams = null;
                sbSQL = null;
            }
            return bReturnValue;
        }
        /// <summary>
        /// Mits 36046
        /// creates new page in pdf
        /// </summary>
        /// <param name="p_objPrintDoc"></param>
        /// <param name="p_objArgs"></param>
        private void NewPage(C1PrintDocument p_objPrintDoc, NewPageStartedEventArgs p_objArgs)
        {
            RenderTable objHeaderTable = null;
            string sHeaderText = string.Empty;

            try
            {
                sHeaderText = "Loss Code Mapping Import Log - Page "
                    + p_objPrintDoc.PageCount + "                             "
                    + DateTime.Now.ToShortDateString();
                objHeaderTable = new RenderTable(p_objPrintDoc);
                objHeaderTable.Style.Borders.AllEmpty = false;
                objHeaderTable.Style.Borders.Left = new LineDef(System.Drawing.Color.Black, 0);
                objHeaderTable.Style.Borders.Right = new LineDef(System.Drawing.Color.Black, 0);
                objHeaderTable.Style.Borders.Bottom = new LineDef(System.Drawing.Color.Black, 0);
                objHeaderTable.Style.Borders.Top = new LineDef(System.Drawing.Color.Black, 0);
                objHeaderTable.StyleTableCell.BorderTableHorz.Empty = true;
                objHeaderTable.StyleTableCell.BorderTableVert.Empty = true;
                objHeaderTable.StyleTableCell.BorderTableHorz.WidthPt = 0;
                objHeaderTable.StyleTableCell.BorderTableVert.WidthPt = 0;
                objHeaderTable.Columns.AddSome(1);
                objHeaderTable.Body.Rows.AddSome(2);
                objHeaderTable.Columns[0].StyleTableCell.TextAlignHorz = AlignHorzEnum.Center;
                objHeaderTable.Columns[0].Width = 11000;
                objHeaderTable.Body.Cell(0, 0).StyleTableCell.Font = new System.Drawing.Font("Arial", 10, FontStyle.Bold);
                objHeaderTable.Body.Cell(0, 0).RenderText.Text = sHeaderText;
                objHeaderTable.Body.Cell(1, 0).StyleTableCell.Font = new System.Drawing.Font("Arial", 10, FontStyle.Bold);
                objHeaderTable.Body.Cell(1, 0).RenderText.Text = "";
                p_objPrintDoc.PageHeader.RenderObject = objHeaderTable;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objHeaderTable != null)
                {
                    objHeaderTable.Dispose();
                    objHeaderTable = null;
                }
            }
        }
        /// <summary>
        /// Mits 36046
        /// to write pdf with logs
        /// </summary>
        /// <param name="sText"></param>
        /// <returns></returns>
        public string Print(string sText)
        {
            C1PrintDocument objPrintDoc = null;
            RenderRichText objRichText = null;
            RenderTable objTable = null;
            C1.C1PrintDocument.TableColumn objTableColumn = null;
            int iRowIndex = 0;
            string sFileContent = string.Empty;
            objPrintDoc = new C1PrintDocument();
            objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
            objPrintDoc.PageSettings.Margins.Top = 45;
            objPrintDoc.PageSettings.Margins.Left = 40;
            objPrintDoc.PageSettings.Margins.Bottom = 40;
            objPrintDoc.PageSettings.Margins.Right = 40;
            objPrintDoc.PageSettings.Landscape = false;
            objPrintDoc.NewPageStarted += new NewPageStartedEventHandler(this.NewPage);
            objPrintDoc.StartDoc();

            objTable = new RenderTable(objPrintDoc);
            objPrintDoc.Style.Borders.AllEmpty = true;
            objTable.Body.StyleTableCell.Font = new System.Drawing.Font("Courier New", 10);

            objTable.Columns.AddSome(1);
            objTableColumn = new C1.C1PrintDocument.TableColumn(objPrintDoc);
            objTableColumn.Width = 10000;
            objTable.Columns[0] = objTableColumn;

            objTable.Body.AutoHeight = true;

            objTable.StyleTableCell.Borders.Bottom = new LineDef(System.Drawing.Color.Black, 0);
            objTable.StyleTableCell.Borders.Top = new LineDef(System.Drawing.Color.Black, 0);
            objTable.StyleTableCell.Borders.Left = new LineDef(System.Drawing.Color.Black, 0);
            objTable.StyleTableCell.Borders.Right = new LineDef(System.Drawing.Color.Black, 0);

            objTable.Style.Borders.Left = new LineDef(System.Drawing.Color.Black, 0);
            objTable.Style.Borders.Right = new LineDef(System.Drawing.Color.Black, 0);
            objTable.Style.Borders.Bottom = new LineDef(System.Drawing.Color.Black, 0);
            objTable.Style.Borders.Top = new LineDef(System.Drawing.Color.Black, 0);

            objTable.StyleTableCell.Padding.Right = 0;
            objTable.StyleTableCell.Padding.Top = 0;
            objTable.StyleTableCell.Padding.Bottom = 0;

            objTable.StyleTableCell.BorderTableHorz.Empty = false;
            objTable.StyleTableCell.BorderTableVert.Empty = false;

            objTable.StyleTableCell.BorderTableHorz.WidthPt = 0;
            objTable.StyleTableCell.BorderTableVert.WidthPt = 0;

            objRichText = new RenderRichText(objPrintDoc);
            objTable.Body.Rows.AddSome(6);

            objTable.Body.Cell(iRowIndex, 0).RenderText.Text = sText;
            objPrintDoc.RenderBlock(objTable);

            // End doc.
            objPrintDoc.EndDoc();

            // Save the PDF.

            string sPDFPath = RMConfigurator.TempPath + "\\LossCodeMappingImport\\LossCodeMappingImport" + DateTime.Now.ToString("yyyyMMddHHmmss") + AppDomain.GetCurrentThreadId() + ".pdf";
            objPrintDoc.ExportToPDF(sPDFPath, false);
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            byte[] arrRet = null;
            objFileStream = new FileStream(sPDFPath, FileMode.Open);
            objBReader = new BinaryReader(objFileStream);
            arrRet = objBReader.ReadBytes((int)objFileStream.Length);
            objMemoryStream = new MemoryStream(arrRet);

            objFileStream.Close();

            // Delete the temp file.

            sFileContent = Convert.ToBase64String(objMemoryStream.ToArray());

            File.Delete(sPDFPath);

            return sFileContent;

        }
        /// <summary>
        /// Mits 36046
        /// replicate mapping of one policy system to another.task is scheduled for this purpose
        /// </summary>
        /// <param name="iParentPolicySystemId"></param>
        /// <param name="sReplicateToPolicySystemIds"></param>
        /// <param name="sFileContent"></param>
        /// <returns></returns>
        public bool ReplicateMapping(int iParentPolicySystemId, string sReplicateToPolicySystemIds, ref string sFileContent)
        {
            bool bTaskScheduled = false;
            string sSQL = string.Empty;
            try
            {
                bTaskScheduled = true;
                ScheduleTask(0, sReplicateToPolicySystemIds, iParentPolicySystemId, true);
            }
            catch (Exception e)
            {
                throw e;
            }
            return bTaskScheduled;
        }
        /// <summary>
        /// Mits 36046
        /// process the mapping defined for save
        /// </summary>
        /// <param name="sPolicySystemIds"></param>
        /// <param name="oDatatable"></param>
        /// <param name="bIsReplication"></param>
        /// <returns></returns>
        private string SaveImportedMapping(string sPolicySystemIds, DataTable oDatatable, bool bIsReplication)
        {

            string[] sPolicySystemIdArray = sPolicySystemIds.Split(',');
            LocalCache objCache = null;
            ImportMaping iReturnValue = 0;
            StringBuilder objStr = null;
            string sErrorMsg = string.Empty;
            string sFileContent = string.Empty;
            try
            {
                objStr = new StringBuilder();
                foreach (string sPolicySystemId in sPolicySystemIdArray)
                {
                    foreach (DataRow dr in oDatatable.Rows)
                    {

                        if (string.IsNullOrEmpty(dr[0].ToString()) && string.IsNullOrEmpty(dr[1].ToString()) && string.IsNullOrEmpty(dr[2].ToString()))
                        {
                            continue;
                        }
                        objCache = new LocalCache(m_sConnString, m_iClientId);
                        iReturnValue = SaveMapping(Conversion.ConvertStrToInteger(sPolicySystemId), dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), bIsReplication, ref sErrorMsg);
                        if (!bIsReplication)
                        {
                            if (iReturnValue == ImportMaping.DuplicateMappingExists)
                            {
                                objStr.AppendLine(" Duplicate mapping exits for Policy LOB :" + dr[0].ToString() + " Coverage type :" + dr[1].ToString() + " Loss code:" + dr[2].ToString() + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId)));
                            }
                            else if (iReturnValue == Riskmaster.Application.RMUtilities.LossCodeMapping.ImportMaping.ErrorOccured)
                            {
                                objStr.AppendLine(" Error occured while saving  mapping  for Policy LOB :" + dr[0].ToString() + " Coverage type :" + dr[1].ToString() + " Loss code:" + dr[2].ToString() + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId)));
                                objStr.AppendLine(" Error message :-");
                                objStr.AppendLine(sErrorMsg);
                           }
                        }
                        else
                        {
                            if (iReturnValue == ImportMaping.DuplicateMappingExists)
                            {
                                objStr.AppendLine(" Duplicate mapping exits for Policy LOB :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[0], m_iClientId)) + ", m_iClientId Coverage type :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[1], m_iClientId)) + " Loss code:" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[2], m_iClientId)) + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId)));
                            }
                            else if (iReturnValue == Riskmaster.Application.RMUtilities.LossCodeMapping.ImportMaping.ErrorOccured)
                            {
                                objStr.AppendLine(" Error occured while saving  mapping  for Policy LOB :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[0], m_iClientId)) + ", m_iClientId Coverage type :" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[1], m_iClientId)) + " Loss code:" + objCache.GetShortCode(Conversion.ConvertObjToInt(dr[2], m_iClientId)) + " for Policy System " + GetPolicySystemName(Conversion.ConvertStrToInteger(sPolicySystemId)));
                                objStr.AppendLine(" Error message :-");
                                objStr.AppendLine(sErrorMsg);
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(objStr.ToString()))
                {
                    sFileContent = Print(objStr.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objStr = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            return sFileContent;
        }
        /// <summary>
        /// Mits 36046
        /// get policy system name
        /// </summary>
        /// <param name="iPolicySystemId"></param>
        /// <returns></returns>
        private string GetPolicySystemName(int iPolicySystemId)
        {
            string spolicySystemName = string.Empty;
            try
            {
                spolicySystemName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnString, "SELECT  POLICY_SYSTEM_NAME FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + iPolicySystemId));
            }
            catch (Exception)
            {
                spolicySystemName = string.Empty;
            }
            return spolicySystemName;
        }
        /// <summary>
        /// Mits 36046
        /// to import mapping
        /// </summary>
        /// <param name="sImportFileContent"></param>
        /// <param name="sPolicySystemIds"></param>
        /// <param name="sFileContent"></param>
        /// <returns></returns>
        public bool ImportMapping(string sImportFileContent, string sPolicySystemIds, ref string sFileContent)
        {

            DataTable oDataTable = null;
            int iImportThreshold = 0;
            bool bTaskScheduled = false;
            int iCount = 0;
            byte[] byteArray = null;
            string xlsxFilePath = string.Empty;
            UserLogin objUserLogin = null;
            string sheetName = "rmALossCodeMapping";
            BinaryWriter Writer = null;
            ITempFileInterface objFile = null;
            FileStorageOutPut objOutPut = null;
            string sFilePath = RMConfigurator.TempPath + "\\LossCodeMappingImport";
            string sFileName = string.Empty;
            try
            {
                objUserLogin = new UserLogin(m_sUserName, m_sDsnName,0);
                sFileName = "ImportFile_" + objUserLogin.UserId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                xlsxFilePath = sFilePath + "\\" + sFileName;
                byteArray = Convert.FromBase64String(sImportFileContent);
                iCount = sPolicySystemIds.Split(',').Count();
                objFile = TempFileStorageFactory.CreateInstance(StorageTypeEnumeration.TempStorageType.FileSystem);
                Dictionary<string, string> parms = new Dictionary<string, string>();
                parms.Add("filepath", sFilePath);
                parms.Add("filename", sFileName);
                objOutPut = objFile.Add(parms, byteArray);
                oDataTable = new DataTable();
                oDataTable = ExtractExcelSheetValuesToDataTable(xlsxFilePath, sheetName);
                iImportThreshold = Conversion.ConvertObjToInt(RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", m_sConnString, m_iClientId)["LossCodeMappingTaskManagerImportCount"], m_iClientId);
                if ((oDataTable != null))
                {
                    if (((oDataTable.Rows.Count) * iCount) > iImportThreshold)
                    {
                        bTaskScheduled = true;
                        ScheduleTask(SaveFileContent(byteArray, objUserLogin.UserId), sPolicySystemIds, 0, false);

                    }
                    else
                    {
                        sFileContent = SaveImportedMapping(sPolicySystemIds, oDataTable, false);
                        bTaskScheduled = false;
                    }

                }

                if (File.Exists(xlsxFilePath))
                    File.Delete(xlsxFilePath);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objUserLogin = null;
                oDataTable = null;
                if (Writer != null)
                {
                    Writer.Flush();
                    Writer.Close();
                    Writer = null;
                }
                objFile = null;

            }
            return bTaskScheduled;


        }
        /// <summary>
        /// Mits 36046
        /// save import file into database
        /// </summary>
        /// <param name="bFileContent"></param>
        /// <param name="iUserId"></param>
        /// <returns></returns>
        private int SaveFileContent(byte[] bFileContent, int iUserId)
        {
            int iRowId = 0;
            ITempFileInterface objFile = null;
            FileStorageOutPut objOutPut = null;
            Dictionary<string, string> parms = null;
            try
            {
                objFile = TempFileStorageFactory.CreateInstance(StorageTypeEnumeration.TempStorageType.Database);

                parms = new Dictionary<string, string>();
                parms.Add("filename", "LossCodeMappingImport\\TaskMansgerImportFile_" + iUserId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                parms.Add("ConnString", m_sConnString);
                objOutPut = objFile.Add(parms, bFileContent);
                if (objOutPut.Success)
                    iRowId = objOutPut.RowId;
            }
            catch (Exception e)
            {
                throw e;
                objFile = null;
                parms = null;
            }

            return iRowId;
        }
        /// <summary>
        /// Mits 36046
        /// export mapping
        /// </summary>
        /// <param name="iPolicySystemId"></param>
        /// <param name="sSessionId"></param>
        /// <returns></returns>
        public string ExportMapping(int iPolicySystemId, string sSessionId)
        {
            string sSQL = string.Empty;
            DataSet objDataSet = null;
            string sFileContent = string.Empty;
            BinaryReader objBReader = null;
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            ArrayList alDataTypes = null;
            string sReportFilePath = string.Empty;
            byte[] excelbytes = null;
            string sReportTitle = "My Report";

            try
            {

                sSQL = " SELECT POL_LOB.SHORT_CODE POLICY_LOB,CVG_CODE.SHORT_CODE COVERAGE_TYPE_CODE,LOSS.SHORT_CODE LOSS_CODE FROM CVG_LOSS_LOB_MAPPING,CODES POL_LOB,CODES CVG_CODE,CODES LOSS WHERE ";
                sSQL = sSQL + " POLICY_LOB=POL_LOB.CODE_ID AND CVG_TYPE_CODE = CVG_CODE.CODE_ID AND LOSS_CODE = LOSS.CODE_ID AND POLICY_SYSTEM_ID=" + iPolicySystemId;

                objDataSet = DbFactory.GetDataSet(m_sConnString, sSQL,m_iClientId);

                if (objDataSet != null)
                {
                    if (objDataSet.Tables.Count > 0)
                    {
                        sReportFilePath = RMConfigurator.BasePath + "\\temp\\LossCodeMappingImport\\ExportLossCodeMapping" + "_" + sSessionId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_temp" + ".xlsx";
                        if (File.Exists(sReportFilePath))
                        {
                            File.Delete(sReportFilePath);
                        }
                        File.Copy(RMConfigurator.BasePath + "\\temp\\LossCodeMappingImport\\" + "ExportLossCodeMappingTemplate.xlsx", sReportFilePath, true);
                        OpenExcelDocument(objDataSet.Tables[0], sReportFilePath, sReportTitle, alDataTypes);
                        objFileStream = new FileStream(sReportFilePath, FileMode.Open);
                        objBReader = new BinaryReader(objFileStream);
                        excelbytes = objBReader.ReadBytes((int)objFileStream.Length);
                        objMemoryStream = new MemoryStream(excelbytes);
                        objFileStream.Close();
                        sFileContent = Convert.ToBase64String(objMemoryStream.ToArray());
                        File.Delete(sReportFilePath);
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                excelbytes = null;

                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }

                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream = null;
                }
                if (objFileStream != null)
                {

                    objFileStream.Close();
                    objFileStream = null;
                }
            }

            return sFileContent;


        }
		
	//abhal3 MITS 36046 end

        public bool AddNewMapping(XmlDocument p_objInputXMLDoc)
        {
            string sSQL = "";
            DbConnection objCon = null;
            XmlElement objLossSysXMLEle = null;
            string sPolLob = string.Empty;
            string sCvgTypeCode = string.Empty;
            string sLossCode = string.Empty;
            //Ankit Start : Worked on MITS - 34657
            string sPolicySystemID = "0";
            bool ReturnFlag = true;
            //Ankit End
            try
            {
                objLossSysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//LossCodeMappingAdaptor/AddCode");
                sPolLob = objLossSysXMLEle.GetElementsByTagName("PolLob").Item(0).InnerText;
                sCvgTypeCode = objLossSysXMLEle.GetElementsByTagName("CvgTypeCode").Item(0).InnerText;
                sLossCode = objLossSysXMLEle.GetElementsByTagName("LossCode").Item(0).InnerText;
                if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                    sPolicySystemID = objLossSysXMLEle.GetElementsByTagName("PolicySystemID").Item(0).InnerText;       //Ankit Start : Worked on MITS - 34657
                if (sPolicySystemID.Contains(","))
                {
                    string[] arrsPolicySystemId = sPolicySystemID.Split(',');
                    foreach (string item in arrsPolicySystemId)
                    {
                        if (!CheckDuplicate(sPolLob, sCvgTypeCode, sLossCode, item))  //Ankit Start : Worked on MITS - 34657
                        {   //Ankit Start : Worked on MITS - 34657
                            //Insertion Logic
                            sSQL = "INSERT INTO CVG_LOSS_LOB_MAPPING(ROW_ID,POLICY_LOB,CVG_TYPE_CODE,LOSS_CODE,POLICY_SYSTEM_ID)" +        //Ankit Start : Worked on MITS - 34657
                                        "VALUES(" + Utilities.GetNextUID(m_sConnString, "CVG_LOSS_LOB_MAPPING", m_iClientId) + "," + sPolLob + "," + sCvgTypeCode + "," + sLossCode + "," + item + ")";        //Ankit Start : Worked on MITS - 34657
                            objCon = DbFactory.GetDbConnection(m_sConnString);
                            objCon.Open();
                            objCon.ExecuteNonQuery(sSQL);
                            ReturnFlag = true;
                        }  //Ankit Start : Worked on MITS - 34657
                        else
                            ReturnFlag = false;
                    }
                }
                else
                {
                    if (!CheckDuplicate(sPolLob, sCvgTypeCode, sLossCode, sPolicySystemID))  //Ankit Start : Worked on MITS - 34657
                    {   //Ankit Start : Worked on MITS - 34657
                        //Insertion Logic
                        sSQL = "INSERT INTO CVG_LOSS_LOB_MAPPING(ROW_ID,POLICY_LOB,CVG_TYPE_CODE,LOSS_CODE,POLICY_SYSTEM_ID)" +        //Ankit Start : Worked on MITS - 34657
                                    "VALUES(" + Utilities.GetNextUID(m_sConnString, "CVG_LOSS_LOB_MAPPING", m_iClientId) + "," + sPolLob + "," + sCvgTypeCode + "," + sLossCode + "," + sPolicySystemID + ")";        //Ankit Start : Worked on MITS - 34657
                        objCon = DbFactory.GetDbConnection(m_sConnString);
                        objCon.Open();
                        objCon.ExecuteNonQuery(sSQL);
                        ReturnFlag = true;
                    }  //Ankit Start : Worked on MITS - 34657
                    else
                        ReturnFlag = false;
                }
            }
            catch (RMAppException p_objRMExp)
            {
                throw new RMAppException(Globalization.GetString("LossCodeMapping.Save.Error",m_iClientId), p_objRMExp);

            }
            catch (Exception p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();

                if (objLossSysXMLEle != null)
                    objLossSysXMLEle = null;
            }
            return ReturnFlag;
        }
        public XmlDocument GetAllMapping(XmlDocument p_objInputXMLDoc, UserLogin objUser)
        {
            string sSQL = "";
            string sHiddenKey = "";
            string sPolicyClaimLOB = "";
            DbReader objDbReader = null;
            XmlElement objElemTemp = null;
            //Aman ML Change --start
            int iUserLangCode = 0;
            int iBaseLangCode = 0;
            bool isBaseLangCode = false;
            //Ankit Start : Worked on MITS - 34657
            string sLossCodeString = string.Empty;
            string sPolicySystemID = "0";
            int iCvgTypeCodeID = 0;
            string sCodeID = "0";        //Ankit Start : Worked for MITS - 35096
            //Ankit End
            bool success;
            try
            {
                iUserLangCode = objUser.objUser.NlsCode;
                iBaseLangCode = Conversion.CastToType<int>(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0], out success);

                if (int.Equals(iUserLangCode, 0) || int.Equals(iBaseLangCode, iUserLangCode))
                    isBaseLangCode = true;

                objElemTemp = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//PolLob");
                if (objElemTemp != null)
                    sPolicyClaimLOB = objElemTemp.InnerText;
                //Ankit Start : Worked on MITS - 34657
                //Ankit Start : Worked for MITS - 35096
                if (UsePolicyInterface)
                {
                    objElemTemp = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//PolicySystemID");
                    if (objElemTemp != null)
                        sPolicySystemID = objElemTemp.InnerText;
                }
                //Ankit End
                objElemTemp = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//CvgTypeCodeID");
                if (objElemTemp != null)
                    iCvgTypeCodeID = Conversion.CastToType<int>(objElemTemp.InnerText, out success);

                objElemTemp = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//CodeID");        //Ankit Start : Worked for MITS - 35096
                if (objElemTemp != null)
                    sCodeID = Convert.ToString(objElemTemp.InnerText);
                //Ankit End
                objElemTemp = null;

                //sSQL = "SELECT CLLM.ROW_ID, PL.CODE_DESC AS POLICY_LOB, CTC.CODE_DESC AS CVG_TYPE_CODE, LC.CODE_DESC AS LOSS_CODE, " +
                //    " CLLM.POLICY_LOB AS POLICY_LOB_ID, CLLM.CVG_TYPE_CODE AS CVG_TYPE_CODE_ID, CLLM.LOSS_CODE AS LOSS_CODE_ID " +
                //    " FROM CVG_LOSS_LOB_MAPPING CLLM " +
                //    " INNER JOIN CODES_TEXT PL ON PL.CODE_ID = CLLM.POLICY_LOB " +
                //    " INNER JOIN CODES_TEXT CTC ON CTC.CODE_ID = CLLM.CVG_TYPE_CODE " +
                //    " INNER JOIN CODES_TEXT LC ON LC.CODE_ID = CLLM.LOSS_CODE " +
                //    " WHERE CLLM.POLICY_LOB = " + sPolicyClaimLOB + " " +
                //    " ORDER BY  PL.CODE_DESC, CTC.CODE_DESC, LC.CODE_DESC";
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("SELECT  ");
                if (UsePolicyInterface)
                    sbSQL.Append("POLICY_SYSTEM_NAME, ");
                else
                    sbSQL.Append(" '' POLICY_SYSTEM_NAME, ");

                sbSQL.Append(" CLLM.ROW_ID, PL.SHORT_CODE AS POLICY_LOB_ST, PL.CODE_DESC AS POLICY_LOB, ");		//Ankit Start : Worked on MITS - 34657
                if (!isBaseLangCode)
                    sbSQL.Append(" UPL.CODE_DESC AS USER_POLICY_LOB,  ");
                else
                    sbSQL.Append(" '' USER_POLICY_LOB,  ");

                sbSQL.Append(" CTC.SHORT_CODE AS CVG_TYPE_CODE_ST, CTC.CODE_DESC AS CVG_TYPE_CODE,");				//Ankit Start : Worked on MITS - 34657
                if (!isBaseLangCode)
                    sbSQL.Append(" UCTC.CODE_DESC AS USER_CVG_TYPE_CODE,  ");
                else
                    sbSQL.Append(" '' USER_CVG_TYPE_CODE, ");
                //Ankit Start : Worked on MITS - 34657
                sbSQL.Append(" LC.SHORT_CODE AS LOSS_CODE_ST, LC.CODE_DESC AS LOSS_CODE, ");
                if (!isBaseLangCode)
                    sbSQL.Append(" ULC.CODE_DESC AS USER_LOSS_CODE, ");
                else
                    sbSQL.Append(" '' USER_LOSS_CODE, ");

                sbSQL.Append(" PCT.SHORT_CODE AS POINT_LOSS_CODE_ST, PCT.CODE_DESC AS POINT_LOSS_CODE, ");
                if (!isBaseLangCode)
                    sbSQL.Append(" UPCT.CODE_DESC AS USER_POINT_LOSS_CODE, ");
                else
                    sbSQL.Append(" '' USER_POINT_LOSS_CODE, ");
                //Ankit End

                sbSQL.Append(" CLLM.POLICY_LOB AS POLICY_LOB_ID, CLLM.CVG_TYPE_CODE AS CVG_TYPE_CODE_ID, CLLM.LOSS_CODE AS LOSS_CODE_ID ");

                sbSQL.Append(" FROM CVG_LOSS_LOB_MAPPING CLLM ");
                if (UsePolicyInterface)
                    sbSQL.Append(" INNER JOIN POLICY_X_WEB PXW ON PXW.POLICY_SYSTEM_ID = CLLM.POLICY_SYSTEM_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT PL ON PL.CODE_ID = CLLM.POLICY_LOB AND PL.LANGUAGE_CODE = " + iBaseLangCode);
                sbSQL.Append(" INNER JOIN CODES_TEXT CTC ON CTC.CODE_ID = CLLM.CVG_TYPE_CODE AND CTC.LANGUAGE_CODE = " + iBaseLangCode);
                sbSQL.Append(" INNER JOIN CODES_TEXT LC ON LC.CODE_ID = CLLM.LOSS_CODE AND LC.LANGUAGE_CODE = " + iBaseLangCode);
                //Ankit Start : Worked on MITS - 34657
                sbSQL.Append(" LEFT OUTER JOIN POLICY_CODE_MAPPING PCM ON PCM.RMX_CODE_ID = LC.CODE_ID");
                if (UsePolicyInterface)
                    sbSQL.Append(" AND CLLM.POLICY_SYSTEM_ID = PCM.POLICY_SYSTEM_ID");
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT PCT ON PCT.CODE_ID = PCM.PS_CODE_ID AND PCT.LANGUAGE_CODE = " + iBaseLangCode);
                //Ankit End
                if (!isBaseLangCode)
                {
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UPL ON UPL.CODE_ID = CLLM.POLICY_LOB AND UPL.LANGUAGE_CODE = " + iUserLangCode);
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCTC ON UCTC.CODE_ID = CLLM.CVG_TYPE_CODE AND UCTC.LANGUAGE_CODE = " + iUserLangCode);
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT ULC ON ULC.CODE_ID = CLLM.LOSS_CODE AND ULC.LANGUAGE_CODE = " + iUserLangCode);
                    //Ankit Start : Worked on MITS - 34657
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UPCT ON UPCT.CODE_ID = PCM.PS_CODE_ID AND UPCT.LANGUAGE_CODE = " + iUserLangCode);
                    //Ankit End
                }
                sbSQL.Append(" WHERE CLLM.POLICY_LOB = " + sPolicyClaimLOB + " ");
                sbSQL.Append(" AND CLLM.POLICY_SYSTEM_ID IN ( " + sPolicySystemID + " ) ");            //Ankit Start : Worked on MITS - 34657

                if (!int.Equals(iCvgTypeCodeID, -1))     //Ankit Start : Worked on MITS - 36045
                    sbSQL.Append(" AND CLLM.CVG_TYPE_CODE = " + iCvgTypeCodeID);            //Ankit Start : Worked on MITS - 34657
                //Ankit Start : Worked for MITS - 35096
                if (UsePolicyInterface)
                {
                    if (!string.Equals(sCodeID, "-1"))        //Ankit Start : Worked on MITS - 36045
                        sbSQL.Append(" AND PCT.CODE_ID IN ( " + sCodeID + " )");            //Ankit Start : Worked on MITS - 34657
                }
                else
                {
                    if (!string.Equals(sCodeID, "-1"))      //Ankit Start : Worked on MITS - 36045
                        sbSQL.Append(" AND LC.CODE_ID IN ( " + sCodeID + " )");
                }
                //sbSQL.Append(" AND CLLM.POLICY_SYSTEM_ID=POLICY_X_WEB.POLICY_SYSTEM_ID");
                //Ankit End
                sbSQL.Append(" ORDER BY PL.CODE_DESC, CTC.CODE_DESC, LC.CODE_DESC");

                objDbReader = DbFactory.GetDbReader(m_sConnString, sbSQL.ToString());
                while (objDbReader.Read())
                {
                    objElemTemp = p_objInputXMLDoc.CreateElement("MappedCodes");
                    //Ankit Start : Worked on MITS - 34657
                    objElemTemp.SetAttribute("RowID", Convert.ToString(objDbReader.GetValue("ROW_ID")));
                    if (string.IsNullOrEmpty(Convert.ToString(objDbReader.GetValue("USER_POLICY_LOB"))))
                        objElemTemp.SetAttribute("PolicyLob", string.Concat(Convert.ToString(objDbReader.GetValue("POLICY_LOB_ST")), " - ", Convert.ToString(objDbReader.GetValue("POLICY_LOB"))));
                    else
                        objElemTemp.SetAttribute("PolicyLob", string.Concat(Convert.ToString(objDbReader.GetValue("POLICY_LOB_ST")), " - ", Convert.ToString(objDbReader.GetValue("USER_POLICY_LOB"))));

                    if (string.IsNullOrEmpty(Convert.ToString(objDbReader.GetValue("USER_CVG_TYPE_CODE"))))
                        objElemTemp.SetAttribute("CvgTypeCode", string.Concat(Convert.ToString(objDbReader.GetValue("CVG_TYPE_CODE_ST")), " - ", Convert.ToString(objDbReader.GetValue("CVG_TYPE_CODE"))));
                    else
                        objElemTemp.SetAttribute("CvgTypeCode", string.Concat(Convert.ToString(objDbReader.GetValue("CVG_TYPE_CODE_ST")), " - ", Convert.ToString(objDbReader.GetValue("USER_CVG_TYPE_CODE"))));


                    if (string.IsNullOrEmpty(Convert.ToString(objDbReader.GetValue("USER_LOSS_CODE"))))
                        sLossCodeString = string.Concat(Convert.ToString(objDbReader.GetValue("LOSS_CODE_ST")), " - ", Convert.ToString(objDbReader.GetValue("LOSS_CODE")));
                    else
                        sLossCodeString = string.Concat(Convert.ToString(objDbReader.GetValue("LOSS_CODE_ST")), " - ", Convert.ToString(objDbReader.GetValue("USER_LOSS_CODE")));

                    objElemTemp.SetAttribute("PolicyName", Conversion.ConvertObjToStr(objDbReader.GetValue("POLICY_SYSTEM_NAME")));

                    //Ankit Start : Worked for MITS - 35096
                    if (UsePolicyInterface)
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(objDbReader.GetValue("USER_POINT_LOSS_CODE"))))
                            sLossCodeString = string.Concat(sLossCodeString, " (", Convert.ToString(objDbReader.GetValue("POINT_LOSS_CODE_ST")), " - ", Convert.ToString(objDbReader.GetValue("POINT_LOSS_CODE")), ")");
                        else
                            sLossCodeString = string.Concat(sLossCodeString, " (", Convert.ToString(objDbReader.GetValue("POINT_LOSS_CODE_ST")), " - ", Convert.ToString(objDbReader.GetValue("USER_POINT_LOSS_CODE")), ")");
                    }
                    objElemTemp.SetAttribute("LossCode", sLossCodeString);
                    //Ankit End
                    sHiddenKey = Convert.ToString(objDbReader.GetValue("POLICY_LOB_ID")) + Convert.ToString(objDbReader.GetValue("CVG_TYPE_CODE_ID")) + Convert.ToString(objDbReader.GetValue("LOSS_CODE_ID"));
                    objElemTemp.SetAttribute("HiddenKey", sHiddenKey);
                    p_objInputXMLDoc.FirstChild.AppendChild(objElemTemp);
                }
                //Aman ML Change --End
                return p_objInputXMLDoc;
            }
            catch (RMAppException p_objRMExp)
            {
                throw new RMAppException(Globalization.GetString("LossCodeMapping.Save.Error",m_iClientId), p_objRMExp);

            }
            catch (Exception p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                objElemTemp = null;
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
            }
        }

        public bool DeleteCodeMapping(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            XmlElement objElm = null;
            DbConnection objConn = null;
            DbConnection objConn1 = null;
            DbReader objReader = null;
            string sSQL = string.Empty;
            string sbSQL = string.Empty;
            string sSQL1 = string.Empty;
            string sLossID = string.Empty;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//DeletedCode");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objConn = DbFactory.GetDbConnection(m_sConnString);
                objConn.Open();

                if (sRowId != "")
                {
                    sSQL = "DELETE FROM CVG_LOSS_LOB_MAPPING  WHERE ROW_ID IN(" + sRowId + ")";
                }
                objConn.ExecuteNonQuery(sSQL);
                if (sRowId != "")
                {
                    sbSQL = "SELECT LOSS_RSV_ROW_ID FROM LOSS_RESERVE_MAPPING WHERE CVG_LOS_LOB_ROWID IN (" + sRowId + ")";
                }

                objReader = DbFactory.GetDbReader(m_sConnString, sbSQL.ToString());
                while (objReader.Read())
                {

                    sLossID = Conversion.ConvertObjToStr(objReader.GetValue("LOSS_RSV_ROW_ID"));
                    if (sLossID != string.Empty)
                    {
                        sSQL1 = "DELETE FROM LOSS_RESERVE_MAPPING  WHERE LOSS_RSV_ROW_ID IN(" + sLossID + ")";
                        objConn1 = DbFactory.GetDbConnection(m_sConnString);
                        objConn1.Open();
                        objConn1.ExecuteNonQuery(sSQL1);
                    }
                }
                return true;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LossCodeMapping.Delete.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
				if (objConn1 != null)
                {
                    objConn1.Dispose();
                    objConn1 = null;
                }
            }
        }

        public bool AddReserveNewMapping(XmlDocument p_objInputXMLDoc, out string sError)
        {
            string sSQL = "";
            string sSQLReserve = "";
            DbConnection objCon = null;
            DbConnection objConn = null;
            XmlElement objLossSysXMLEle = null;
            string sPolLob = string.Empty;
            string sCvgTypeCode = string.Empty;
            string sLossCode = string.Empty;
            string sReserveType = string.Empty;
            string sReserveAmount = string.Empty;
            string sCoverageLossLOBID = string.Empty;
            int iReserveCount = 0;
          
            try
            {
                
                objLossSysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//LossCodeMappingAdaptor/AddCode");
                //spahariya MITS 30911
                sReserveType = objLossSysXMLEle.GetElementsByTagName("ReserveType").Item(0).InnerText;
                sReserveAmount = objLossSysXMLEle.GetElementsByTagName("ReserveAmount").Item(0).InnerText;
                sCoverageLossLOBID = objLossSysXMLEle.GetElementsByTagName("CoverageLossLOBID").Item(0).InnerText;

                if (string.IsNullOrEmpty(sReserveAmount))
                {
                        sReserveAmount = "0.00";
                }

                sSQLReserve = "SELECT COUNT(*) FROM LOSS_RESERVE_MAPPING WHERE CVG_LOS_LOB_ROWID = " + sCoverageLossLOBID + "AND RESERVE_TYPE = " + sReserveType;

                objConn = DbFactory.GetDbConnection(m_sConnString);
                objConn.Open();
                iReserveCount = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQLReserve), m_iClientId);

                if (iReserveCount == 0)
                {
                    sSQL = "INSERT INTO LOSS_RESERVE_MAPPING(LOSS_RSV_ROW_ID,CVG_LOS_LOB_ROWID,RESERVE_TYPE,RESERVE_AMOUNT)" +
                            "VALUES(" + Utilities.GetNextUID(m_sConnString, "LOSS_RESERVE_MAPPING", m_iClientId) + "," + sCoverageLossLOBID + "," + sReserveType + "," + sReserveAmount + ")";
                    objCon = DbFactory.GetDbConnection(m_sConnString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);
                    sError = string.Empty;
                }
                else
                {
                    sError = "Reserve Already Exist";
                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw new RMAppException(Globalization.GetString("LossCodeMapping.Save.Error",m_iClientId), p_objRMExp);

            }
            catch (Exception p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                    if (objConn != null)
                    {
                        objConn.Dispose();
                        objCon = null;
                    }
                    if (objLossSysXMLEle != null)
                    {
                        objLossSysXMLEle = null;
                    }
            }
        }
        public XmlDocument GetReserveMapping(XmlDocument p_objXmlDocument)
        {
            string sSQL = "";
            XmlElement objElm = null;
            XmlElement objReserveNode = null;
            XmlElement objElemTemp = null;
            DbConnection objConn = null;
            DbReader objDbReader = null;
            XmlElement objRootElement = null;
            LocalCache objCache = null;
            
          
            string sCoverageLossLOBID = string.Empty;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//LossCodeMappingAdaptor/AddCode");
                //spahariya MITS 30911
                sCoverageLossLOBID = objElm.GetElementsByTagName("CoverageLossID").Item(0).InnerText;
                objReserveNode = (XmlElement)p_objXmlDocument.CreateNode(XmlNodeType.Element, "ReserveList", "");
                sSQL = "SELECT LOSS_RSV_ROW_ID,CVG_LOS_LOB_ROWID,RESERVE_TYPE,RESERVE_AMOUNT FROM LOSS_RESERVE_MAPPING WHERE CVG_LOS_LOB_ROWID = " + sCoverageLossLOBID;
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                while (objDbReader.Read())
                {
                    objCache = new LocalCache(m_sConnString, m_iClientId);
                    objElemTemp = p_objXmlDocument.CreateElement("Reserve");

                    objElemTemp.SetAttribute("LossReserveID", Conversion.ConvertObjToStr(objDbReader.GetValue("LOSS_RSV_ROW_ID")));
                    objElemTemp.SetAttribute("CovgLossLOBID", Conversion.ConvertObjToStr(objDbReader.GetValue("CVG_LOS_LOB_ROWID")));
                    objElemTemp.SetAttribute("ReserveType", objCache.GetCodeDesc(Conversion.ConvertObjToInt(objDbReader.GetValue("RESERVE_TYPE"), m_iClientId)));
                    objElemTemp.SetAttribute("ReserveAmount", Conversion.ConvertObjToStr(objDbReader.GetValue("RESERVE_AMOUNT")));
                    //spahariya MITS 30911 - end
                    objReserveNode.AppendChild(objElemTemp);
                    
                }

                p_objXmlDocument.FirstChild.AppendChild(objReserveNode);

                return p_objXmlDocument;
            }
            catch (RMAppException p_objRMExp)
            {
                throw new RMAppException(Globalization.GetString("LossCodeMapping.Save.Error",m_iClientId), p_objRMExp);

            }
            catch (Exception p_objExp)
            {
                throw p_objExp;
            }
            finally
            {

                if (objElm != null)
                    objElm = null;
            }
        }

        //Ankit Start : Worked on MITS - 34657
        private bool CheckDuplicate(string sPolicyLob, string sCvgTypeCode, string sLossCode, string iPolicySystemID)
        {
            bool blnReturn = false;
            StringBuilder sbSQL = null;
            int iRecordCounts = 0;
            bool success;
            Dictionary<string, string> dictParams = null;

            try
            {
                sbSQL = new StringBuilder();
                dictParams = new Dictionary<string, string>();
                sbSQL.Append(" SELECT COUNT(*) AS RECORD_COUNTS FROM CVG_LOSS_LOB_MAPPING");
                sbSQL.AppendFormat(" WHERE POLICY_LOB={0} ", "~POLICYLOB~");
                sbSQL.AppendFormat(" AND CVG_TYPE_CODE={0} ", "~CVGTYPECODE~");
                sbSQL.AppendFormat(" AND LOSS_CODE={0} ", "~LOSSCODE~");
                sbSQL.AppendFormat(" AND POLICY_SYSTEM_ID={0} ", "~POLICYSYSTEMID~");

                dictParams.Add("POLICYLOB", sPolicyLob);
                dictParams.Add("CVGTYPECODE", sCvgTypeCode);
                dictParams.Add("LOSSCODE", sLossCode);
                dictParams.Add("POLICYSYSTEMID", iPolicySystemID.ToString());

                iRecordCounts = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnString, sbSQL.ToString(), dictParams).ToString(), out success);
                if (iRecordCounts > 0)
                    blnReturn = true;

                return blnReturn;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LossCodeMapping.Save.Error",m_iClientId), p_objEx);
            }
            finally
            {
                dictParams = null;
            }
        }
        private int GetTableID(string strTableName, int intPolicySystemID, int intPolicySystemTypeID, LocalCache objCache)
        {
            int iTableId = 0;
            int iMultipleRelationship = 0;
            string sTablePrefix = string.Empty;
            bool success;

            Dictionary<string, string> dictParams = null;
            try
            {
                dictParams = new Dictionary<string, string>();
                iTableId = objCache.GetTableId(strTableName);

                if (UsePolicyInterface)          //Ankit Start : Worked for MITS - 35096
                {
                    dictParams.Add("POLICYSYSTEMID", intPolicySystemID.ToString());
                    sTablePrefix = DbFactory.ExecuteScalar(m_sConnString, string.Format("SELECT TABLE_PREFIX FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID={0}", "~POLICYSYSTEMID~"), dictParams).ToString();

                    //MITS 34657 srajindersin 1/16/2014
                    dictParams.Clear();
                    dictParams.Add("TABLEID", iTableId.ToString());
                    dictParams.Add("POLICYSYSTEMTYPEID", intPolicySystemTypeID.ToString());
                    iMultipleRelationship = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnString, string.Format("SELECT MULTIPLE_RELATIONSHIP FROM PS_MAP_TABLES WHERE RMX_TABLE_ID={0} AND POLICY_SYSTEM_TYPE_ID={1}", "~TABLEID~", "~POLICYSYSTEMTYPEID~"), dictParams).ToString(), out success);
                    if ( iMultipleRelationship == 0)
                         //iTableId = objCache.GetTableId(string.Concat(sTablePrefix, "_", strTableName, "(M)"));
                        iTableId = objCache.GetTableId(string.Concat(sTablePrefix, "_", strTableName));
                    else
                        //iTableId = objCache.GetTableId(string.Concat(sTablePrefix, "_", strTableName));
                        iTableId = objCache.GetTableId(string.Concat(sTablePrefix, "_", strTableName, "(M)"));
                }
                return iTableId;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                dictParams = null;
            }
        }
        private bool ShowRecords(string iTableId, string sPSCodeID, string sPolicyLOB, string sCVGTypeCode, string iPolicySystemID)
        {
            bool blnShowRecords = true;
            StringBuilder sbSQL = null;
            int iRecordCounts = 0;
            bool success;
            //Dictionary<string, string> dictParams = null;

            try
            {
                sbSQL = new StringBuilder();
                //dictParams = new Dictionary<string, string>();

                sbSQL.Append(" SELECT COUNT(*) AS RECORD_COUNTS FROM CODES");
                sbSQL.Append(" INNER JOIN CODES_TEXT ON CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                sbSQL.Append(" INNER JOIN POLICY_CODE_MAPPING PCM ON PCM.RMX_CODE_ID = CODES.CODE_ID ");
                sbSQL.AppendFormat(" AND PCM.PS_CODE_ID={0} ", sPSCodeID);
                sbSQL.AppendFormat(" AND PCM.POLICY_SYSTEM_ID IN ( {0} ) ", iPolicySystemID.ToString());
                sbSQL.Append(" INNER JOIN CVG_LOSS_LOB_MAPPING CLLM ON CLLM.LOSS_CODE = CODES.CODE_ID AND ");
                sbSQL.AppendFormat(" CLLM.POLICY_LOB={0} ", sPolicyLOB);
                sbSQL.AppendFormat(" AND CLLM.CVG_TYPE_CODE={0} ", sCVGTypeCode);
                sbSQL.AppendFormat(" AND CLLM.POLICY_SYSTEM_ID IN ( {0} ) ", iPolicySystemID.ToString());
                sbSQL.AppendFormat(" WHERE CODES.TABLE_ID IN ( {0} )", iTableId.ToString());
                sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");

                //dictParams.Add("PSCODEID", sPSCodeID);
                //dictParams.Add("POLICYSYSTEMID", iPolicySystemID.ToString());
                //dictParams.Add("POLICYLOB", sPolicyLOB);
                //dictParams.Add("CVGTYPECODE", sCVGTypeCode);
                //dictParams.Add("POLICYSYSTEMID1", iPolicySystemID.ToString());
                //dictParams.Add("TABLEID", iTableId.ToString());

                iRecordCounts = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnString, sbSQL.ToString()).ToString(), out success);
                if (iRecordCounts > 0)
                    blnShowRecords = false;

                return blnShowRecords;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                //dictParams = null;
            }
        }
        //Ankit End

		//abhal3 MITS 36046 
        #region ExcelWrite
        /// <summary>
        /// Mits 36046
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="xlsxFilePath"></param>
        /// <param name="xlsxReportTitle"></param>
        /// <param name="alDataTypes"></param>
        private void OpenExcelDocument(DataTable dt, string xlsxFilePath, string xlsxReportTitle, ArrayList alDataTypes)
        {
            UInt32Value iCurrencyStyleId;
            using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(xlsxFilePath, true))
            {
                WorkbookPart workbookPart = myWorkbook.WorkbookPart;
                Stylesheet styleSheet = workbookPart.WorkbookStylesPart.Stylesheet;
                iCurrencyStyleId = createCellFormat(styleSheet, null, null, UInt32Value.FromUInt32(7));
                InsertValuesInSheets("rmALossCodeMapping", workbookPart, dt, xlsxReportTitle, iCurrencyStyleId, alDataTypes);
                workbookPart.Workbook.Save();
            }
        }
        private void InsertValuesInSheets(string sheetName, WorkbookPart workbookPart, DataTable dt, string xlsxReportTitle, UInt32Value iCurrencyStyleId, ArrayList alDataTypes)
        {
            WorksheetPart worksheetPart = null;
            if (!string.IsNullOrEmpty(sheetName))
            {
                Sheet ss = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetName).SingleOrDefault<Sheet>();
                worksheetPart = (WorksheetPart)workbookPart.GetPartById(ss.Id);
            }
            else
            {
                worksheetPart = workbookPart.WorksheetParts.FirstOrDefault();
            }
            if (worksheetPart != null)
            {
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                var excelRows = sheetData.Descendants<Row>().ToList();
                int excelRowIndex = 2;
                for (int rowIndexInTable = 0; rowIndexInTable < dt.Rows.Count; rowIndexInTable++)
                {
                    if (excelRowIndex >= excelRows.Count)
                    {
                        Row contentRow = CreateContentRow(excelRowIndex, dt.Rows[rowIndexInTable], dt.Columns.Count, iCurrencyStyleId, alDataTypes);
                        sheetData.AppendChild(contentRow);
                    }
                    else
                    {
                        var cells = excelRows[excelRowIndex].Descendants<Cell>().ToList();
                        for (int colIndexInTable = 0; colIndexInTable < dt.Columns.Count; colIndexInTable++)
                        {
                            Cell cell;
                            if (colIndexInTable >= cells.Count)
                            {
                                cell = new Cell();
                                excelRows[excelRowIndex].AppendChild(AddValueToCell(cell, dt.Rows[rowIndexInTable][colIndexInTable].ToString()));
                            }
                            else
                            {
                                if (cells.Count == 1)
                                {
                                    cell = new Cell();
                                    excelRows[excelRowIndex].InsertAt(AddValueToCell(cell, dt.Rows[rowIndexInTable][colIndexInTable].ToString()), 0);
                                }
                                else
                                {
                                    cell = cells[colIndexInTable];
                                    AddValueToCell(cell, dt.Rows[rowIndexInTable][colIndexInTable].ToString());
                                }
                            }
                        }
                    }
                    excelRowIndex++;
                }
                worksheetPart.Worksheet.Save();
            }
        }
        private Row CreateContentRow(int index, DataRow dr, int headerColumnsCount, UInt32Value iCurrencyFormatId, ArrayList alDataTypes)
        {
            //Create new row
            Row r = new Row();
            r.RowIndex = (UInt32)index;

            for (int i = 0; i < headerColumnsCount; i++)
            {
                Cell c;

                if (alDataTypes == null)
                {
                    //if (dr[i].GetType() == typeof(Int32) || dr[i].GetType() == typeof(Decimal))
                    //{
                    //    c = CreateNumberCell("", dr[i].ToString(), index);
                    //}
                    //else
                    //{
                    c = CreateTextCell("", dr[i].ToString(), index);
                    //}
                }
                else
                {
                    switch (alDataTypes[i].ToString())
                    {
                        case "String":
                            c = CreateTextCell("", dr[i].ToString(), index);
                            break;
                        case "Number":
                            c = CreateNumberCell("", dr[i].ToString(), index);
                            break;
                        case "Currency":
                            c = createValueCell(dr[i], iCurrencyFormatId);
                            break;
                        default:
                            c = CreateTextCell("", dr[i].ToString(), index);
                            break;
                    }
                }
                r.AppendChild(c);
            }
            return r;
        }
        /// <summary>
        /// if needed in future.
        /// </summary>
        /// <param name="header"></param>
        /// <param name="text"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private Cell CreateNumberCell(string header, string text, int index)
        {
            //Create new inline Number cell
            Cell c = new Cell();
            c.DataType = CellValues.Number;
            c.CellValue = new CellValue(text);
            return c;
        }

        private Cell createValueCell(object cellValue, Nullable<uint> styleIndex)
        {
            Cell cell = new Cell();
            CellValue value = new CellValue();
            value.Text = cellValue.ToString();
            //apply the cell style if supplied
            if (styleIndex.HasValue)
                cell.StyleIndex = styleIndex.Value;

            cell.AppendChild(value);

            return cell;
        }

        private UInt32Value createCellFormat(Stylesheet styleSheet, UInt32Value fontIndex, UInt32Value fillIndex, UInt32Value numberFormatId)
        {
            CellFormat cellFormat = new CellFormat();

            if (fontIndex != null)
                cellFormat.FontId = fontIndex;

            if (fillIndex != null)
                cellFormat.FillId = fillIndex;

            if (numberFormatId != null)
            {
                cellFormat.NumberFormatId = numberFormatId;
                cellFormat.ApplyNumberFormat = BooleanValue.FromBoolean(true);
            }

            styleSheet.CellFormats.Append(cellFormat);

            UInt32Value result = styleSheet.CellFormats.Count;
            styleSheet.CellFormats.Count++;
            return result;
        }
        private Cell CreateTextCell(string header, string text, int index)
        {
            //Create new inline string cell
            Cell c = new Cell();
            c.DataType = CellValues.InlineString;

            //Add text to text cell
            InlineString inlineString = new InlineString();
            Text t = new Text();

            //t.Text = text.Replace("| ", "\r\n");
            t.Text = text;
            inlineString.AppendChild(t);
            c.AppendChild(inlineString);
            return c;
        }

        private Cell AddValueToCell(Cell cell, string text)
        {
            cell.DataType = CellValues.InlineString;
            cell.RemoveAllChildren();
            InlineString inlineString = new InlineString();
            Text t = new Text();
            t.Text = text;
            inlineString.AppendChild(t);

            cell.AppendChild(inlineString);

            return cell;
        }

        public DataTable ExtractExcelSheetValuesToDataTable(string xlsxFilePath, string sheetName)
        {
            DataTable dt = new DataTable();
            int iCount = 0;

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(xlsxFilePath, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                }
                foreach (Row row in rows) //this will also include your header row...
                {
                    if (iCount > 0)
                    {
                        DataRow tempRow = dt.NewRow();
                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                        }
                        dt.Rows.Add(tempRow);
                    }
                    iCount++;
                }
            }
            return dt;
        }

        public string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                if (string.IsNullOrEmpty(cell.InnerText))
                    return "";
                else
                    return cell.InnerText;
            }
            string value = cell.CellValue.InnerXml;
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        #endregion

        //abhal3 MITS 36046 end

    }
}
