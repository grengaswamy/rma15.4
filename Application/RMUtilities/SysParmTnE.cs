using System;
using System.Xml;
using System.Data;
using Riskmaster.Db; 
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
//Start - Sumit(06/25/2010)
using System.Collections;
//End - Sumit
  

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   19th,May 2005
	///Purpose :   Time and Expense Module Form Implementation. This opens from System Parameter Setup
	///			   This is for clients who want Time and Expense only.
	/// </summary>
	public class SysParmTnE
	{
		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnStr="";
        /// <summary>
        /// gagnihotri MITS 11995 Changes made for Audit table
        /// User Login Name
        /// </summary>
        private string m_sUserName = "";

        private int m_iClientId = 0;


		/// Name		: SysParmTnE
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
        public SysParmTnE(string p_sConnStr, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sConnStr= p_sConnStr;
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        public SysParmTnE(string p_sConnStr, string p_sUserName, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_sConnStr = p_sConnStr;
            m_sUserName = p_sUserName;
		}

        //Start(07/01/2010) - Sumit
        #region System Parameter Modules Names
        private const string USE_TIME_EXP_KEY = "USETMANDEXP";
        private const string USE_MASTER_KEY = "USEMASTER";
        #endregion
        //End - Sumit

		/// Name		: SavePermission
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/19/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the Module Permission Settings
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Document with data to save</param>
		public XmlDocument SavePermission(XmlDocument p_objXmlDoc)
		{
			int iID=0;
			string sPwd="";
			string sSQL="";
			string sActualPwd="";
			XmlNode objNode=null;
			DbReader objRdr=null;
			DataRow[] rows=null;
			//RMConfigurator objConfig=null;
			DataTable objTbl = null;
			DbConnection objCon = null;
			LocalCache objCache=null;
            //Sumit (06/25/2010) - Start
            SystemParameter objSysParm = null;
            ArrayList AdditionalLicenseParams = new ArrayList();
            //End - Sumit

			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Password']");
				if(objNode!=null)
					sPwd = objNode.InnerText;

				objCon = DbFactory.GetDbConnection(m_sConnStr);
				objCon.Open(); 

				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='Success']");

                //Raman/Shivendu MITS 17856
                //objConfig = new RMConfigurator();
				//sActualPwd = objConfig.GetValue("SysParmModuleAccsPerm");

                //Start(06/23/2010) - Sumit
                AdditionalLicenseParams.Add(USE_TIME_EXP_KEY);
                objSysParm = new SystemParameter(m_sConnStr, m_iClientId);
                sActualPwd = objSysParm.GenerateLicense(AdditionalLicenseParams);
                //sActualPwd = TANDE_ENABLE_KEY;
                //End - Sumit
				if(sPwd == sActualPwd || sPwd==USE_MASTER_KEY)
				{
					objRdr = objCon.ExecuteReader("SELECT * FROM FUNDS_SUPP WHERE TRANS_ID=-1");
					objTbl = objRdr.GetSchemaTable(); 
					objRdr.Dispose();
					rows = objTbl.Select("ColumnName='INT_CLAIM_TEXT'"); 
					if(rows.GetLength(0)==0)
					{
						switch(objCon.DatabaseType)
						{
							case eDatabaseType.DBMS_IS_SQLSRVR:
							case eDatabaseType.DBMS_IS_SYBASE:
								sSQL = "ALTER TABLE FUNDS_SUPP ADD INT_CLAIM_TEXT VARCHAR(21) NULL";
								break;
							case eDatabaseType.DBMS_IS_ORACLE:
								sSQL = "ALTER TABLE FUNDS_SUPP ADD INT_CLAIM_TEXT VARCHAR2(21) NULL";
								break;
							case eDatabaseType.DBMS_IS_INFORMIX :
							case eDatabaseType.DBMS_IS_DB2:
								sSQL = "ALTER TABLE FUNDS_SUPP ADD INT_CLAIM_TEXT VARCHAR(21)";
								break;
							case eDatabaseType.DBMS_IS_ACCESS:
								sSQL = "ALTER TABLE FUNDS_SUPP ADD INT_CLAIM_TEXT TEXT(21)";
								break;
						}
						
						objCon.ExecuteNonQuery(sSQL);
					}
 
					objRdr = objCon.ExecuteReader("SELECT * FROM SUPP_DICTIONARY WHERE SYS_FIELD_NAME='INT_CLAIM_TEXT'");
					if(!objRdr.Read())
					{
                        iID = Utilities.GetNextUID(m_sConnStr, "SUPP_DICTIONARY", m_iClientId);
						objRdr.Dispose(); 
                        //gagnihotri MITS 11995 Changes made for Audit table
						objCon.ExecuteNonQuery("INSERT INTO SUPP_DICTIONARY ( " + 
							"FIELD_ID,SEQ_NUM,SUPP_TABLE_NAME,USER_PROMPT,SYS_FIELD_NAME,FIELD_TYPE," + 
							"FIELD_SIZE,REQUIRED_FLAG,DELETE_FLAG,LOOKUP_FLAG,IS_PATTERNED,CODE_FILE_ID," +
                            "GRP_ASSOC_FLAG,HELP_CONTEXT_ID,NETVISIBLE_FLAG,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES (" + 
							iID + "," + iID + ",'FUNDS_SUPP','Internal Claim','INT_CLAIM_TEXT',10,21,0,0,0,0,0,0,0,0,'" +
                            m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")");
					} 
					//T and E ONLY, Add rows to TRANS_TYPE, RESERVE_TYPE code tables.
					iID = UTILITY.AddCode(m_sConnStr,"NONPOST", 1033, "Non-Postable", "RESERVE_TYPE",0 , m_iClientId);
					if(iID!=0)
					{
                        objCache = new LocalCache(m_sConnStr, m_iClientId);
						if(objCache.GetCodeId("NONPOST", "TRANS_TYPES")>0)
                            iID = UTILITY.AddCode(m_sConnStr, "NONPOST", 1033, "Non-Postable", "TRANS_TYPES", iID, m_iClientId);  
					}
					objNode.InnerText ="-1";
                    p_objXmlDoc.SelectSingleNode("//control[@name='FormMode']").InnerText = "close"; 
				}
				else
				{
					//wrong password
					objNode.InnerText ="0";
				}
	
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SysParmTnE.SavePermission.Err", m_iClientId), p_objEx);  
			}
			finally
			{
				objNode=null;
				if(objCon!=null)
				{
					objCon.Dispose();
				}
				if(objCache!=null)
				{
					objCache.Dispose();
				}
                if (objRdr != null)
                {
                    objRdr.Dispose();
                }
                //objConfig = null;
                rows = null;
                if(objTbl != null)
                    objTbl.Dispose();
               
			}
		}
	}
}