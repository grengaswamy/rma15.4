using System;
using System.Collections ;
using Riskmaster.Common; 
using Riskmaster.Db;
using System.Text;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Tushar Agarwal
	///Dated   :   09,Nov 2009
	///Purpose :   Rate Range List implementation form
    ///MITS    :    18231
	/// </summary>
	public class RateRangeList : CollectionBase
	{
        private string m_sConnectString = string.Empty;
		private int m_iRateRowid = 0;
		private bool m_bDataChanged = false;

		/// <summary>
		/// Stores the pParent value
		/// </summary>
		private ExposureRate  m_objParent = null;

		private const string TABLE_NAME = "SYS_POL_RANGE";

        private Boolean bIsSucess = false;
        private int m_iClientId = 0;

		public RateRangeList(string p_sConnectString, int p_iClientId)
		{
			m_sConnectString = p_sConnectString;
			m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Read property for Count
		/// </summary>
		internal int Count
		{
			get
			{
				return List.Count;
			}
		}

		/// <summary>
		/// Read property for Count
		/// </summary>
		internal int RateRowid
		{
			get
			{
				return m_iRateRowid;
			}
			set
			{
				m_iRateRowid = value;
				for(int i = 0; i < List.Count; i++)
				{
					((RateRange)List[i]).RateRowid = m_iRateRowid;
				}
			}
		}

		/// <summary>
		/// Read/Write property for pParent
		/// </summary>
		internal ExposureRate  Parent
		{
			//IMPORTANT DO NO SET PARENT PROPERTY FROM PARENT'S OBJECT INITIALIZE EVENT !!!!!!
			get
			{
				return m_objParent ;
			}
			set
			{
				m_objParent = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				if(m_bDataChanged)
					return m_bDataChanged;
 
				for(int i = 0; i < List.Count; i++)
				{
					if(((RateRange )List[i]).DataChanged == true)
					{
						m_bDataChanged = true;
						break;
					}

				}
				return m_bDataChanged;
			}
			set
			{
				for(int i = 0; i < List.Count; i++)
				{
					((RateRange )List[i]).DataChanged = value;
				}
				m_bDataChanged = value;
			}
		}

		/// <summary>
		/// Add an item in the list.
		/// </summary>
		/// <param name="p_objRateRange">Financial Summary Item</param>
		/// <returns>Position on which the item is inserted.</returns>
		internal int Add(RateRange p_objRateRange)
		{
            if (p_objRateRange.RangeRowId   == 0)
			{
                p_objRateRange.RangeRowId = Common.Utilities.GetNextUID(m_sConnectString, TABLE_NAME, m_iClientId);
                p_objRateRange.NewRecord = true;
			}

            p_objRateRange.Parent = this;
			m_bDataChanged = true;
            return List.Add(p_objRateRange);			
		}		


		/// <summary>
		/// Remove the item form the list.
		/// </summary>
		/// <param name="p_objRateRange">Financial Summary Item</param>
		internal void Remove( RateRange p_objRateRange )
		{
            List.Remove(p_objRateRange);
		} 

		/// <summary>
		/// Check the existence of the item in the list.
		/// </summary>
		/// <param name="p_objRateRange">Financial Summary Item</param>
		/// <returns>Return the status of the item. "True" if item is in the list.</returns>
		internal bool Contains( RateRange p_objRateRange )
		{
            return List.Contains(p_objRateRange);
		}
		/// <summary>
		/// Get the index of the item in the list.
		/// </summary>
		/// <param name="p_objRateRange">Financial Summary Item</param>
		/// <returns>Return the integer index.</returns>
		internal int IndexOf( RateRange  p_objRateRange )
		{
            return List.IndexOf(p_objRateRange);
		}

		/// <summary>
		/// Indexing for RateRange
		/// </summary>
		internal RateRange this[int p_iIndex]
		{
			get { return ( RateRange  )List[p_iIndex]; }
			set { List[p_iIndex] = value; }
		}

		internal void LoadData()
		{

			DbReader objDbReader = null;
			RateRange objRateRange = null;
            int iRateRange = 0;
            LocalCache objLocalCache=null;

			try
			{
				//pmahli 10/3/2007 Sorted Disocunt Range list
                StringBuilder sSQL = new StringBuilder();
                sSQL.AppendFormat("SELECT * FROM {0} WHERE RATE_ROWID = {1} ORDER BY RANGE_ROWID",TABLE_NAME,m_iRateRowid);

				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL.ToString());

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						objRateRange  = new RateRange ();

						objRateRange.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objRateRange.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objRateRange.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objRateRange.DttmRcdLastUpd  = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
                        //Changed By Anu Tennyson for MITS 18231 STARTS 2/2/2010
                        iRateRange = Conversion.CastToType<int>(Convert.ToString(objDbReader["RANGE_TYPE"]), out bIsSucess);
                        objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
                        objRateRange.RangeType = objLocalCache.GetShortCode(iRateRange);
                        if (objRateRange.RangeType == "D")
                            objRateRange.RangeType = "Deductible";
                        else
                            objRateRange.RangeType = "Range";
                        //Changed By Anu Tennyson for MITS 18231 ENDS
                        objRateRange.Modifier = Conversion.ConvertStrToDouble(objDbReader["MODIFIER"].ToString());
                        objRateRange.Deductible = Conversion.ConvertStrToDouble(objDbReader["DEDUCTIBLE"].ToString());
                        objRateRange.BeginningRange = objDbReader["BEGINNING_RANGE"].ToString();
						objRateRange.RangeRowId = Conversion.ConvertStrToInteger(objDbReader["RANGE_ROWID"].ToString());
						objRateRange.RateRowid = Conversion.ConvertStrToInteger(objDbReader["RATE_ROWID"].ToString());
						objRateRange.EndRange = objDbReader["END_RANGE"].ToString();

						objRateRange.DataChanged = false;
						List.Add(objRateRange);
					}
				}

				m_bDataChanged = false;
			}
			finally
			{
				if(objDbReader != null)
					objDbReader.Dispose();
				if(objRateRange != null)
					objRateRange = null;
                if (objLocalCache != null)
                    objLocalCache = null;
			}
		}

	}
}
