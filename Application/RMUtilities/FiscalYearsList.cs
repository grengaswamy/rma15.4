﻿
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;

using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Fiscal Years List information.
	/// </summary>
	public class FiscalYearsList:UtilityFormBase
	{
		#region XML Format
		/*
	<form name=" FiscalYearsList" title="Fiscal Years List" topbuttons="1" supp="">
	<toolbar>
		<button type="new" title="New" />
		<button type="save" title="Save" />
		<button type="delete" title="Delete Record" />
		<button type="recordsummary" title="Record Summary" />
	</toolbar>
	<group name="FiscalYears" title="Fiscal Years List">
		<displaycolumn>
		<control name="RowId" type="id">
		</control>
		<control name="LineOfBusiness" type="code" firstfield="1" codetable="LINE_OF_BUSINESS" title="Line Of Business" maxlength="50">
		</control>
		<control name="AllLOB" type="button" title="All LOB">
		</control>
		</displaycolumn>
		<displaycolumn>
		<control name="Organization" type="code" firstfield="1" codetable="LINE_OF_BUSINESS" title="Organization" maxlength="50">
		</control>
		<control name="AllOrg" type="button" title="All Org">
		</control>
		</displaycolumn>
		<displaycolumn>
		<control name="TableName" type="radiolist">
			<listhead>
			<rowhead colname="FYear">
			</rowhead>
			<rowhead colname="FiscalYear">Fiscal Year</rowhead>
			<rowhead colname="StartDate">Start Date</rowhead>
			<rowhead colname="EndDate">End Date</rowhead>
			<rowhead colname="Periods">Periods</rowhead>
			<rowhead colname="LOB">Line of Business</rowhead>
			<rowhead colname="Organization">Organization</rowhead>
			</listhead>
			<listrow>
			<rowtext type="radio" name="radiolist0" value="2009" title="" />
			<rowtext type="label" name="WCBenefitType" title="2009" />
			<rowtext type="label" name="StartDate" title="4/1/2009" />
			<rowtext type="label" name="EndDate" title="3/31/2010" />
			<rowtext type="label" name="Periods" title="12" />
			<rowtext type="label" name="LOB" title="All Lines" />
			<rowtext type="label" name="Organization" title="Entire Organization" />
			</listrow>
		</control>
		</displaycolumn>
	</group>
	</form>
		*/
		#endregion

		#region Private Variables

        // Ash - cloud, config settings moved to DB
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;
	
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
        private int iLangCode = 1033;
		/// <summary>
		/// Security DSN
		/// </summary>
		private string m_sSecurityDSN="";

        /// <summary>
        /// ClientID for multi-tenant environment, cloud
        /// </summary>
        private int m_iClientId = 0;

		#endregion

        #region Protected Variables
        //aanandpraka2:Region added to include Dictionary variable
        protected System.Collections.Generic.Dictionary<string, string> dictParams = new System.Collections.Generic.Dictionary<string, string>();
        
        #endregion
        public int LanguageCode
        {
            set
            {
                iLangCode = value;
            }
        }
        
        #region Constructor
        /// <summary>
		/// Default Constructor
		/// </summary>
		/// public FiscalYearsList(){}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public FiscalYearsList(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			try
			{
				ConnectString = p_sConnString;	
				m_sDSN=ConnectString;
                m_iClientId = p_iClientId;
                m_sSecurityDSN = SecurityDatabase.GetSecurityDsn(m_iClientId);
                m_MessageSettings = RMConfigurationSettings.GetRMMessages(ConnectString, m_iClientId); // Ash - cloud, config settings
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FiscalYearsList.Constructor.Error",m_iClientId),p_objEx);//sonali-cloud
			}
		}
		#endregion

		#region Private Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <param name="p_sValue"></param>
        /// <returns></returns>
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//control[@name='"+p_sValue+"']");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}

		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			try
			{
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FiscalYearsList.Initialize.Error", m_iClientId), p_objEx);//sonali
			}
		}

		#endregion

		#region Public Functions

		/// Name		: DeleteFiscalYear
		/// Author		: Parag Sarin
		/// Date Created	: 29 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes fiscal year
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool DeleteFiscalYear(XmlDocument p_objXmlDocument)
		{
			ArrayList arrlstSql=null;
			StringBuilder sbSQL=null;
			bool bReturnValue=false;
			string sLOBValue="";
			string sOrgValue="";
			string sRecord="";            
			try
			{             
				sRecord=GetValue(p_objXmlDocument,"RowId");
				sLOBValue=GetValue(p_objXmlDocument,"LineOfBusiness");
				sOrgValue=GetValue(p_objXmlDocument,"Organization");
				sbSQL=new StringBuilder();
				if (sLOBValue.Trim().Equals(""))
				{
					sLOBValue="0";
				}
				if (sOrgValue.Trim().Equals(""))
				{
					sOrgValue="0";
				}
                //aanandpraka2:Changes for Pen Testing-9/01/2012
				//Delete from table FISCAL_YEAR
                //sbSQL.Append("DELETE FROM FISCAL_YEAR WHERE FISCAL_YEAR ="+sRecord);
                //sbSQL.Append(" AND LINE_OF_BUS_CODE =" + sLOBValue);
                //sbSQL.Append("  AND ORG_EID =" + sOrgValue);                
                sbSQL.Append(string.Format("DELETE FROM FISCAL_YEAR WHERE FISCAL_YEAR = {0}", "~sRecord~"));
                sbSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}", "~sLOBValue~"));
                sbSQL.Append(string.Format(" AND ORG_EID = {0}", "~sOrgValue~"));

                this.dictParams.Add("sRecord",sRecord);
                this.dictParams.Add("sLOBValue", sLOBValue);
                this.dictParams.Add("sOrgValue", sOrgValue);
                //aanandpraka2:end changes

				arrlstSql=new ArrayList();
				arrlstSql.Add(sbSQL.ToString());                
                //Aman MITS 27339
                UTILITY.Save(arrlstSql, m_sDSN.ToString(), dictParams, m_iClientId);
                this.dictParams.Clear();
                arrlstSql.Clear();
				sbSQL=new StringBuilder();

                //aanandpraka2:Changes for Pen Testing-9/01/2012
                //Delete from table FISCAL_PERIOD
				//sbSQL.Append("DELETE FROM FISCAL_YEAR_PERIOD WHERE FISCAL_YEAR ="+sRecord);
				//sbSQL.Append("  AND LINE_OF_BUS_CODE ="+sLOBValue);
				//sbSQL.Append("  AND ORG_EID ="+sOrgValue);
                //Aman MITS 27339-- Changed the table name from FISCAL_YEAR_PERIOD111 to FISCAL_YEAR_PERIOD
                sbSQL.Append(string.Format("DELETE FROM FISCAL_YEAR_PERIOD WHERE FISCAL_YEAR = {0}","~sRecord1~"));
                sbSQL.Append(string.Format(" AND LINE_OF_BUS_CODE = {0}","~sLOBValue1~"));
                sbSQL.Append(string.Format(" AND ORG_EID = {0}","~sOrgValue1~"));
                //add dictionary parameters
                this.dictParams.Add("sRecord1", sRecord);
                this.dictParams.Add("sLOBValue1", sLOBValue);
                this.dictParams.Add("sOrgValue1", sOrgValue);

				arrlstSql.Add(sbSQL.ToString());
				//UTILITY.Save(arrlstSql,m_sDSN);
                UTILITY.Save(arrlstSql, m_sDSN.ToString(), dictParams, m_iClientId);                
                //aanandpraka2:End changes
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FiscalYearsList.DeleteFiscalYear.Error", m_iClientId), p_objEx);	//sonali-cloud		
			}
			finally
			{
				arrlstSql=null;
                sbSQL = null;
			}
			return bReturnValue;
		}

		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created	: 29 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			DbReader objRead=null;
			XmlElement objElement=null;
			XmlElement objRowTxt=null;
			XmlElement objLstRow=null;
			StringBuilder sbSql=null;
			string sLOBValue="";
			string sAbbr="";
			string sName="";
			string sOrgValue="";
			LocalCache objCache=null;
			try
			{
				XMLDoc = p_objXmlDocument;
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sLOBValue=GetValue(p_objXmlDocument,"LineOfBusiness");

				sbSql=new StringBuilder();
				sbSql.Append(" SELECT FISCAL_YEAR, FY_START_DATE, FY_END_DATE, NUM_PERIODS, ORG_EID,LINE_OF_BUS_CODE FROM FISCAL_YEAR ");
				if (sLOBValue!="" && sLOBValue!="0")
				{
                    //aanandpraka2:Start changes for Pen Testing
					//sbSql.Append(" WHERE LINE_OF_BUS_CODE ="+sLOBValue);
                    sbSql.Append(string.Format(" WHERE LINE_OF_BUS_CODE = {0}", "~sLOBValue~"));
                    dictParams.Add("sLOBValue", sLOBValue);
                    //aanandpraka2:End changes
				}
				else
				{
                    sbSql.Append(string.Format(" WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)"));
				}

				sOrgValue=GetValue(p_objXmlDocument,"Organization");
				if (sOrgValue!=""  && sOrgValue!="0")
				{
                    //aanandpraka2:Start changes for Pen Testing
					//sbSql.Append(" AND ORG_EID IN("+sOrgValue+")");
                    sbSql.Append(string.Format(" AND ORG_EID IN({0})", "~sOrgValue~"));
                    dictParams.Add("sOrgValue", sOrgValue);
                    //aanandpraka2:End changes
				}
				else
				{
					sbSql.Append(" AND (ORG_EID = 0 OR ORG_EID IS NULL)");
				}
				sbSql.Append("  ORDER BY LINE_OF_BUS_CODE, ORG_EID, FISCAL_YEAR DESC ");
				//new code
				objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CurrentYear']");  
				objElement.InnerText=DateTime.Now.Year.ToString();
				//new code
				objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FiscalYearList']");  
				
				
				objLstRow = p_objXmlDocument.CreateElement("listhead");

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "FYear");
				objRowTxt.InnerText="";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "FiscalYear");
				objRowTxt.InnerText="Fiscal Year";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "StartDate");
				objRowTxt.InnerText="Start Date";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "EndDate");
				objRowTxt.InnerText="End Date";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Periods");
				objRowTxt.InnerText="Periods";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "LOB");
				objRowTxt.InnerText="Line of Business";
				objLstRow.AppendChild(objRowTxt);


				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Organization");
				objRowTxt.InnerText="Organization";
				objLstRow.AppendChild(objRowTxt);
				objElement.AppendChild(objLstRow);
				

				if (sLOBValue!="" && sLOBValue!="0")
				{
                    objCache = new LocalCache(m_sDSN, m_iClientId);
					sLOBValue=objCache.GetCodeDesc(Conversion.ConvertObjToInt(sLOBValue, m_iClientId));
					objCache.Dispose();
				}
				else
				{
					sLOBValue="All Lines";
				}
				if (sOrgValue!="" && sOrgValue!="0")
				{
                    objCache = new LocalCache(m_sDSN, m_iClientId);
                    objCache.GetOrgInfo(Conversion.ConvertObjToInt(sOrgValue, m_iClientId), ref sAbbr, ref sName);
					sOrgValue=sName;
					objCache.Dispose();
				}
				else
				{
					sOrgValue="Entire Organization";
				}
				//aanandpraka2:Start changes for Pen Testing
                //objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
                objRead = DbFactory.ExecuteReader(base.ConnectString, sbSql.ToString(), dictParams);
                dictParams.Clear();
                //aanandpraka2:End changes
                
				while (objRead.Read())
				{
					
					objLstRow = p_objXmlDocument.CreateElement("listrow");

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "radio");
					objRowTxt.SetAttribute("name" , "radiolist0");
					objRowTxt.SetAttribute("value" , Conversion.ConvertObjToStr(objRead.GetValue("FISCAL_YEAR")));
					objRowTxt.SetAttribute("title", "");
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "FiscalYear");
					objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("FISCAL_YEAR")));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "StartDate");
					objRowTxt.SetAttribute("title", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRead.GetValue("FY_START_DATE")),"d"));
					objLstRow.AppendChild(objRowTxt);
	
					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "EndDate");
					objRowTxt.SetAttribute("title", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRead.GetValue("FY_END_DATE")),"d"));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "Periods");
					objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("NUM_PERIODS")));
					objLstRow.AppendChild(objRowTxt);
					
					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "LOB");
					objRowTxt.SetAttribute("title", sLOBValue);
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "Organization");
					objRowTxt.SetAttribute("title", sOrgValue);
					objLstRow.AppendChild(objRowTxt);
					objElement.AppendChild(objLstRow);
				}
				objCache.Dispose();
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FiscalYearsList.Get.Error", m_iClientId), p_objEx);		//sonali-cloud		
			}
			finally
			{
				objRead=null;
				objElement=null;
				objRowTxt=null;
				objLstRow=null;
				sbSql=null;
				if (objCache!=null)
					objCache.Dispose();
			}
			return XMLDoc;
		}

		/// Name		: GetLobOrg
		/// Author		: Parag Sarin
		/// Date Created	: 29 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument GetLobOrg(XmlDocument p_objXmlDocument)
        {
            #region local variables
            DbReader objRead=null;
			XmlElement objElement=null;
			XmlElement objRowTxt=null;
			XmlElement objLstRow=null;
			StringBuilder sbSql=null;
			string sLOBValue="";
			string sOrgValue="";
			DbConnection objConn=null;
			long lDbMake=-1;
			string sSQL="";
			LocalCache objCache=null;
			string sAbbrev="";
			string sName="";
			long lRow=1;
            #endregion

            try
			{
				XMLDoc = p_objXmlDocument;
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sLOBValue=GetValue(p_objXmlDocument,"LineOfBusiness");
				
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT DISTINCT MIN(FISCAL_YEAR) | MIN_FY, MAX(FISCAL_YEAR) | MAX_FY, ");
				sbSql.Append(" COUNT(FISCAL_YEAR) | CNT_FY, LINE_OF_BUS_CODE, ORG_EID ");
				sbSql.Append(" FROM FISCAL_YEAR ");
				sbSql.Append(" GROUP BY LINE_OF_BUS_CODE, ORG_EID");
				sbSql.Append(" ORDER BY ORG_EID, LINE_OF_BUS_CODE");
				objConn=DbFactory.GetDbConnection(base.ConnectString);
				objConn.Open();
				lDbMake=(int)objConn.DatabaseType;
				objConn.Close();
				sSQL=sbSql.ToString();
				if (lDbMake==0)//ACCESS DB
				{
					sSQL=sSQL.Replace("|","AS");
				}
				else
				{
					sSQL=sSQL.Replace("|","");
				}
				if (sLOBValue!="" && sLOBValue!="0")
				{
					sbSql.Append(" WHERE LINE_OF_BUS_CODE ="+sLOBValue);
				}
				else
				{
					sbSql.Append(" WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)");
				}

				sOrgValue=GetValue(p_objXmlDocument,"Organization");
				if (sOrgValue!=""  && sOrgValue!="0")
				{
					sbSql.Append(" AND ORG_EID IN("+sOrgValue+")");
				}
				else
				{
					sbSql.Append(" AND (ORG_EID = 0 OR ORG_EID IS NULL)");
				}
				sbSql.Append("  ORDER BY LINE_OF_BUS_CODE, ORG_EID, FISCAL_YEAR DESC ");

				objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FiscalYearList']");  
				
				
				objLstRow = p_objXmlDocument.CreateElement("listhead");

			
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Row");
				objRowTxt.InnerText="       ";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "1Year");
				objRowTxt.InnerText="1st Year    ";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "2Year");
				objRowTxt.InnerText="2nd Year     ";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Years");
				objRowTxt.InnerText="Years     ";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "LOB");
				objRowTxt.InnerText="Line of Business          ";
				objLstRow.AppendChild(objRowTxt);


				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Organization       ");
				objRowTxt.InnerText="Organization";
				objLstRow.AppendChild(objRowTxt);
				objElement.AppendChild(objLstRow);
				
				objRead=DbFactory.GetDbReader(base.ConnectString,sSQL);
				while (objRead.Read())
				{
					objLstRow = p_objXmlDocument.CreateElement("listrow");

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "Row");
					objRowTxt.SetAttribute("title", lRow.ToString()+") ");
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "1Year");
					objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("MIN_FY")));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "2Year");
					objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("MAX_FY")));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "Years");
					objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("CNT_FY")));
					objLstRow.AppendChild(objRowTxt);
	
					sLOBValue=Conversion.ConvertObjToStr(objRead.GetValue("LINE_OF_BUS_CODE"));
					if (sLOBValue!="" && sLOBValue!="0")
					{
                        objCache = new LocalCache(m_sDSN, m_iClientId);
						sLOBValue=objCache.GetCodeDesc(Conversion.ConvertObjToInt(sLOBValue, m_iClientId));
						objCache.Dispose();
					}
					else
					{
						sLOBValue="All Lines";
					}
					sOrgValue=Conversion.ConvertObjToStr(objRead.GetValue("ORG_EID"));
					if (sOrgValue!="" && sOrgValue!="0")
					{
                        objCache = new LocalCache(m_sDSN, m_iClientId);
                        objCache.GetOrgInfo(Conversion.ConvertObjToInt(sOrgValue, m_iClientId), ref sAbbrev, ref sName);
						objCache.Dispose();
						sOrgValue=sName;
						sAbbrev="";
						sName="";
					}
					else
					{
						sOrgValue="Entire Organization";
					}
					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "LOB");
					objRowTxt.SetAttribute("title", sLOBValue);
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "Organization");
					objRowTxt.SetAttribute("title", sOrgValue);
					objLstRow.AppendChild(objRowTxt);
					objElement.AppendChild(objLstRow);
					lRow++;

				}
				objCache.Dispose();
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FiscalYearsList.GetLobOrg.Error", m_iClientId), p_objEx);		//sonali-cloud		
			}
			finally
			{
				objElement=null;
				objRowTxt=null;
				objLstRow=null;
				sbSql=null;
				if (objCache!=null)
					objCache.Dispose();
                if (objConn != null)
                    objConn.Dispose();
                if (objRead != null)
                    objRead.Dispose();
             }
			return XMLDoc;
		}
		
		/// Name		: PrintFiscalYearList
		/// Author		: Parag Sarin
		/// Date Created	: 29 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Prints the Fiscal Year List
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param> 
		/// <returns>Stream containing the FiscalYearList report</returns>
		public MemoryStream PrintFiscalYearList(XmlDocument p_objXmlDocument)
        {
            #region local variables
            PrintWrapper objPrintWrapper = null;
			MemoryStream objMemoryStream = null;
			DbReader objDbReader = null;
			StringBuilder sbSql=null;
			string sPath="";
			LocalCache objCache=null;
			string sLOBValue="";
			string sOrgValue="";
			string sAbbr="";
			string sName="";
            #endregion

            try
			{
				objPrintWrapper = new PrintWrapper(m_iClientId);
				objPrintWrapper.StartDoc(false);

				CreateHeader(ref objPrintWrapper);
				
				sLOBValue=GetValue(p_objXmlDocument,"LineOfBusiness");
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT FISCAL_YEAR, FY_START_DATE, FY_END_DATE, NUM_PERIODS, ORG_EID,LINE_OF_BUS_CODE FROM FISCAL_YEAR ");
				if (sLOBValue!="" && sLOBValue!="0")
				{
					sbSql.Append(" WHERE LINE_OF_BUS_CODE ="+sLOBValue);
				}
				else
				{
					sbSql.Append(" WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)");
				}

				sOrgValue=GetValue(p_objXmlDocument,"Organization");
				if (sOrgValue!=""  && sOrgValue!="0")
				{
					sbSql.Append(" AND ORG_EID IN("+sOrgValue+")");
				}
				else
				{
					sbSql.Append(" AND (ORG_EID = 0 OR ORG_EID IS NULL)");
				}
				sbSql.Append("  ORDER BY LINE_OF_BUS_CODE, ORG_EID, FISCAL_YEAR DESC ");
				
				objDbReader = DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());

				objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
				objPrintWrapper.CurrentX = 300;
				objPrintWrapper.SetFontBold(false);
                objPrintWrapper.PrintTextNew(CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Header1", m_iClientId), this.iLangCode.ToString()), true);

				objPrintWrapper.CurrentX = 1675;
				objPrintWrapper.SetFontBold(false);
                objPrintWrapper.PrintTextNew(CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Header2", m_iClientId), this.iLangCode.ToString()), true);

				objPrintWrapper.CurrentX = 3252;
				objPrintWrapper.SetFontBold(false);
                objPrintWrapper.PrintTextNew(CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Header3", m_iClientId), this.iLangCode.ToString()), true);

				objPrintWrapper.CurrentX = 4764;
				objPrintWrapper.SetFontBold(false);
                objPrintWrapper.PrintTextNew(CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Header4", m_iClientId), this.iLangCode.ToString()), true);

				objPrintWrapper.CurrentX = 5986;
				objPrintWrapper.SetFontBold(false);
                objPrintWrapper.PrintTextNew(CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Header5", m_iClientId), this.iLangCode.ToString()), true);

				objPrintWrapper.CurrentX = 8677;
				objPrintWrapper.SetFontBold(false);
                objPrintWrapper.PrintTextNew(CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Header6", m_iClientId), this.iLangCode.ToString()), true);
				
				if (sLOBValue!="" && sLOBValue!="0")
				{
                    objCache = new LocalCache(m_sDSN, m_iClientId);
					sLOBValue=objCache.GetCodeDesc(Conversion.ConvertObjToInt(sLOBValue, m_iClientId));
					objCache.Dispose();
				}
				else
				{
					sLOBValue="All Lines";
				}
				if (sOrgValue!="" && sOrgValue!="0")
				{
                    objCache = new LocalCache(m_sDSN, m_iClientId);
                    objCache.GetOrgInfo(Conversion.ConvertObjToInt(sOrgValue, m_iClientId), ref sAbbr, ref sName);
					sOrgValue=sName;
					objCache.Dispose();
				}
				else
				{
					sOrgValue="Entire Organization";
				}

				objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						if (objPrintWrapper.CurrentY > 14500)
						{
							objPrintWrapper.NewPage();
							CreateHeader(ref objPrintWrapper);
						}
						objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");

						objPrintWrapper.CurrentX = 150;
						objPrintWrapper.SetFontBold(false); 
						objPrintWrapper.PrintTextNew(Conversion.ConvertObjToStr(objDbReader.GetValue("FISCAL_YEAR")),true);
				
						objPrintWrapper.CurrentX = 1675;
						objPrintWrapper.SetFontBold(false); 
						objPrintWrapper.PrintTextNew(Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDbReader.GetValue("FY_START_DATE")),"d"),true);

						objPrintWrapper.CurrentX = 3252;
						objPrintWrapper.SetFontBold(false); 
						objPrintWrapper.PrintTextNew(Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDbReader.GetValue("FY_END_DATE")),"d"),true);

						objPrintWrapper.CurrentX = 4764;
						objPrintWrapper.SetFontBold(false); 
						objPrintWrapper.PrintTextNew(Conversion.ConvertObjToStr(objDbReader.GetValue("NUM_PERIODS")),true);

						objPrintWrapper.CurrentX = 5986;
						objPrintWrapper.SetFontBold(false); 
						objPrintWrapper.PrintTextNew(sLOBValue,true);

						objPrintWrapper.CurrentX = 8677;
						objPrintWrapper.SetFontBold(false); 
						objPrintWrapper.PrintTextNew(sOrgValue,true);
					}
					if(objDbReader != null)
						objDbReader.Close();
					if (((objPrintWrapper.CurrentY)+(objPrintWrapper.GetTextHeight("W")*8)) > 14500)
					{
						objPrintWrapper.NewPage();
						CreateHeader(ref objPrintWrapper);
					}
					CreateFooter(objPrintWrapper,150,objPrintWrapper.CurrentY,objPrintWrapper.GetTextHeight("W"),250);
				}

				objPrintWrapper.EndDoc();

				objMemoryStream = new MemoryStream();
				
                sPath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "FiscalYearList");
				if (!Directory.Exists(sPath))
				{
					Directory.CreateDirectory(sPath);
				}
				sPath=sPath+@"\"+Path.GetFileName(Path.GetTempFileName()).Replace(".tmp",".pdf");;
				objPrintWrapper.GetPDFStream(sPath,ref objMemoryStream); 
				File.Delete(sPath);
				return objMemoryStream;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FiscalYearsList.PrintFiscalYearList.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				objPrintWrapper = null;
				objMemoryStream = null;
				if(objDbReader != null)
					objDbReader.Dispose();
				if (objCache!=null)
					objCache.Dispose();
				sbSql=null;
            }
		}

		#endregion

		#region Report Private Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objPrintWrapper"></param>
		private void CreateHeader(ref PrintWrapper objPrintWrapper)
        {
            #region local variables
            double dBorderWidth=0;
			double dMarginWidth = 0;
			double dPageWidth = 0;
			double dPageHeight = 0;
			double dClientWidth = 0;
			double dClientHeight = 0;
			double dTotalLines = 0;
			double dTextHeight = 0;
			string sDate = "";
			string sCompanyName="";
			string sFooterText = "";
			double dBodyHeight = 0;
			SysParms objSysSetting=null;
            #endregion

            try
			{
				sDate = DateTime.Now.ToShortDateString();
   
				//PrintBorder
				dBorderWidth = 2;
				dMarginWidth = 75;
				dPageWidth = objPrintWrapper.PageWidth - 2 * (dBorderWidth + dMarginWidth);
				dPageHeight = objPrintWrapper.PageHeight - 2 * (dBorderWidth + dMarginWidth);
				dClientWidth = objPrintWrapper.PageWidth - 2 * dMarginWidth;
				dClientHeight = objPrintWrapper.PageHeight - 2 * (dMarginWidth + 50);
				//Moved from top. Referenced ClientHeight before it was set.
				dTotalLines = Math.Abs(dClientHeight / objPrintWrapper.GetTextHeight("W") - 10);  
				objSysSetting = new SysParms( base.ConnectString, m_iClientId);
				sCompanyName=objSysSetting.SysSettings.ClientName;
				objSysSetting=null;
				dTextHeight = objPrintWrapper.GetTextHeight(sCompanyName);
  
				objPrintWrapper.SetFont("Arial",10);
				objPrintWrapper.SetFontBold(true);
				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth,dMarginWidth);

				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth,dMarginWidth, dPageHeight - dMarginWidth);
				objPrintWrapper.PrintLine(dPageWidth - dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth, dPageHeight - dMarginWidth);
				objPrintWrapper.PrintLine(dMarginWidth,dPageHeight - dMarginWidth,dPageWidth - dMarginWidth,dPageHeight - dMarginWidth);
				
				//print header
				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth + 500,dPageWidth - dMarginWidth,dMarginWidth + 500);
				objPrintWrapper.FillRect(dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth,dMarginWidth + 500);


				if(sCompanyName.Length < 36)
				{
					objPrintWrapper.CurrentX = (dPageWidth - objPrintWrapper.GetTextWidth(sCompanyName)) / 2;
					objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
					objPrintWrapper.PrintTextNew(sCompanyName,true);

					objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sDate + "W");
					objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
					objPrintWrapper.PrintTextNew(sDate,true);
				}
				else
				{
					objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sCompanyName + " " + sDate + " ");
					objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
					objPrintWrapper.PrintTextNew(sCompanyName + " " + sDate,true);
				}
				
				//print footer
				objPrintWrapper.PrintLine(dMarginWidth,dPageHeight - dMarginWidth - 500,dPageWidth - dMarginWidth - 500,dPageHeight - dMarginWidth);
				objPrintWrapper.FillRect(dMarginWidth,dPageHeight - dMarginWidth - 500,dPageWidth - dMarginWidth,dPageHeight - dMarginWidth);
                sFooterText = String.Format("{0} - {1}  ", CommonFunctions.FilterBusinessMessage(Globalization.GetString("ORGSEC.PrintOrgSec.ConfidentialData", m_iClientId), this.iLangCode.ToString()), m_MessageSettings["DEF_RMCAPTION"]);
				objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sFooterText) - 1;
				objPrintWrapper.CurrentY = (dPageHeight - 500) + objPrintWrapper.GetTextWidth("W");
				objPrintWrapper.SetFontItalic(true);  
				objPrintWrapper.PrintTextNew(sFooterText,true);
				objPrintWrapper.SetFontItalic(false);
                sFooterText = CommonFunctions.FilterBusinessMessage(Globalization.GetString("ORGSEC.PrintOrgSec.PageX", m_iClientId), this.iLangCode.ToString()) + " [@@PageNo@@]";
				objPrintWrapper.CurrentX = (dPageWidth - objPrintWrapper.GetTextWidth(sFooterText)) / 2;
				objPrintWrapper.SetFontBold(true);  
				objPrintWrapper.PrintTextNew(sFooterText,true);
				objPrintWrapper.SetFontBold(false);

				dBodyHeight = dClientHeight - 2 * 500;
				objPrintWrapper.PageLimit = dBodyHeight;
				objPrintWrapper.CurrentY=400;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("FiscalYearsList.CreateHeader.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				objSysSetting=null;
			}

		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objPrintWrapper"></param>
        /// <param name="p_dCurrentX"></param>
        /// <param name="p_dCurrentY"></param>
        /// <param name="p_dHeight"></param>
        /// <param name="p_dblSpace"></param>
		private void CreateFooter(PrintWrapper p_objPrintWrapper,double p_dCurrentX,double p_dCurrentY,double p_dHeight,double p_dblSpace)
		{
			try
			{
				p_objPrintWrapper.CurrentY+= (p_objPrintWrapper.GetTextHeight("W")*3);
				p_objPrintWrapper.CurrentX=p_dCurrentX;
				p_objPrintWrapper.SetFont("Arial",7.5f);
				p_objPrintWrapper.SetFontBold(true);
				p_objPrintWrapper.SetFontUnderline(true);
                p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_NOTE"].ToString(), true);
				
				p_objPrintWrapper.CurrentX=p_dCurrentX;
				p_objPrintWrapper.CurrentY +=p_dHeight;
				p_objPrintWrapper.SetFont("Arial",7.5f);
				p_objPrintWrapper.SetFontBold(true);
                p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_RMPROPRIETARY"].ToString(), true);
				
				p_objPrintWrapper.CurrentX=p_dCurrentX;
				p_objPrintWrapper.CurrentY +=p_dHeight;
				p_objPrintWrapper.SetFont("Arial",7.5f);
				p_objPrintWrapper.SetFontBold(true);
                p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_NAME"].ToString(), true);

				p_objPrintWrapper.CurrentX=p_dCurrentX;
				p_objPrintWrapper.CurrentY +=p_dHeight;
				p_objPrintWrapper.SetFont("Arial",7.5f);
				p_objPrintWrapper.SetFontBold(true);
                p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_RMCOPYRIGHT"].ToString(), true);
	
				p_objPrintWrapper.CurrentX=p_dCurrentX;
				p_objPrintWrapper.CurrentY +=p_dHeight;
				p_objPrintWrapper.SetFont("Arial",7.5f);
				p_objPrintWrapper.SetFontBold(true);
                p_objPrintWrapper.PrintTextNew(m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString(), true);
				
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FiscalYearsList.CreateFooter.Error", m_iClientId), p_objEx);	//sonali-cloud			
			}
		}

		#endregion
	}
}
