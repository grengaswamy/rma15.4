/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 * 05/14/2015 | RMA-4606         | nshah28   | Import third party Currency Exchange Rates from flat file
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Collections;
using Riskmaster.Security;
using System.Data;
using Riskmaster.Security.Encryption;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Settings;
using System.Linq;

namespace Riskmaster.Application.RMUtilities
{
    public class TaskManager
    {
        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objUserLogin"></param>
        /// <param name="ClientId"></param>
        public TaskManager(UserLogin p_objUserLogin, int p_iClientId)
        {
            if (p_iClientId != 0)
            {
                Dictionary<int, CommonFunctions.MultiClientConnection> dictMultiTenant = CommonFunctions.MultiTenantConnection(p_iClientId);
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[p_iClientId];

                m_sConnectionString = StructConnString.TMConnectionString;
                m_sConnectionStrSecurity = StructConnString.SecConnectionString;
            }
            else
            {

                m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", p_iClientId);//rkaur27
                m_sConnectionStrSecurity = RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId);//rkaur27
            }

            m_sDataSourceName = p_objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sDbUserName = p_objUserLogin.objRiskmasterDatabase.RMUserId;
            m_sLoginName = p_objUserLogin.LoginName;
            m_sPassword = p_objUserLogin.Password;
            m_sDbConnstring = p_objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_iUserId = p_objUserLogin.objUser.UserId;
            m_iClientId = p_iClientId;//Add by kuladeep for Cloud Jira-1531
        }
        public TaskManager(int p_iClientId)
        {
            m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", p_iClientId);
            m_iClientId = p_iClientId;//Add by kuladeep for Cloud Jira-1531
        }
        #endregion

        # region private variables
        //changed by nadim for BES
        string m_sAdminDbConnstring = string.Empty;
        string m_sAdminUserName = string.Empty;
        string m_sAdminPassword = string.Empty;
        string m_sDbConnstring = string.Empty;
        string m_sDbUserName = string.Empty;
        string m_sConnectionStrSecurity = string.Empty;        
        int m_iUserId = 0;        
        //changed by nadim for BES
        string m_sConnectionString = String.Empty;
        string m_sLoginName = String.Empty;
        string m_sPassword = String.Empty;
        string m_sDataSourceName = String.Empty;
        int m_iClientId = 0;//Add by kuladeep for Cloud Jira-1531
        const int TASK_WPA = 1;
        const int TASK_FINHIST = 2;
        const int TASK_BILL = 3;
        const string TASK_PRINT_SCH_CHECK_BATCH = "PrintCheckBatch";
        const string TASK_NOTIFY_DIARY = "ProcessOverDueDiary";  //Added by Amitosh for QBE Enhancement of Auto Diary Notification.
        const string TASK_BES = "BES";
        const string TASK_HIST_TRACK = "HistoryTracking"; //MGaba2:R7:History Tracking
        const string TASK_BATCH_PRINT = "PrintBatchFroi"; //Nadim for batch print
        const string TASK_POLICY_SYSTEM_UPDATE = "PolicySystemUpdate";
        //Anu Tennyson : Added to support Auto Mail Merge Starts
        const string TASK_AUTO_MAIL_MERGE = "AutoMailMerge"; 
        //Anu Tennyson Ends
        
        //JIRA RMA-4606 nshah28 start
        const string TASK_CURRENCY_EXCHANGE_INTERFACE = "CurrencyExchangeInterface";
        //JIRA RMA-4606 nshah28 end
		const string TASK_CLAIM_BALANCING = "ClaimBalancing"; 
		//MITS 22996 set the password in the task to a constant and it can be ignored.
        const string RMX_TM_PWD = "NOPWD4TM";
        const string TASK_FAS = "FASScheduler";//averma62 MITS 32386 rmA-FAS
        // akaushik5 Added for MITS 36381 Starts
        const string TASK_RESERVE_BALANCE = "Resbal";
        // akaushik5 Added for MITS 36381 Ends
		
		const string TASK_OSHA = "OSHA"; //Rakhel Osha Enhancement //33046
        const string TASK_OSHA_REPORTS = "OSHA Reports"; //33046
        const string SCHEDULE_TYPE_DAILY = "Daily";//33046
        # endregion
        string sClmStatusLst = string.Empty;
        string sClmTypeLst = string.Empty;
        #region Properties
        public string ConnectionString
        {
            get
            {
                return m_sConnectionString;
            }
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// This method saves the user entered settings in case of OneTime Schedule Type.
        /// </summary>
        /// <param name="p_objXmlDoc">XmlIn containing user entered values</param>
        public void SaveOneTimeSettings(XmlDocument p_objXmlDoc)
        {
            ScheduleDetails objSchedule;
            DbReader objReader = null;
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objArgElement = null;
            ArrayList objArrToDelete = null;
            string sSQL = "";
            string sConfig = "";
            string[] sArgs = null;
            string sModulename = string.Empty;
            try
            {
                objSchedule = new ScheduleDetails();
                if (p_objXmlDoc.SelectSingleNode("//ScheduleId") != null)
                {
                    objSchedule.ScheduleId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ScheduleId").InnerText);
                }//
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 1;
                objSchedule.TaskTypeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//TaskType").InnerText);
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";
                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.DayOfMonth = 0;
                objSchedule.NextRunDTTM = Conversion.GetDateTime(p_objXmlDoc.SelectSingleNode("//Date").InnerText + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.FinalRunDTTM = "";
                //Appending subtask name with Task name.
                if (p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel") != null && (p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel").InnerText != "None" && p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel").InnerText.Trim() !=""))
                {
                    objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText + " - " + p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel").InnerText;
                }
                else
                {
                    objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText;
                }

                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;

                if (objSchedule.ScheduleId.ToString() == "" || objSchedule.ScheduleId <= 0)
                {
                    sSQL = "SELECT CONFIG FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + objSchedule.TaskTypeId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);
                        
                        // Prepare the arguments based on the Task Type.
                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password -- MITS 33592
                                //psaiteja MITS - 33592 - 08/26/2013
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);
                                
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);
                               
                            //    objSchedule.Config = objXmlDoc.OuterXml;

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                          
                            default:
                                // Check if it a Data Integrator task.
                                //if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING)
                                //RMA-4606 nshah28(Comment above condition and including below condition)
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    int iOptionsetId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText);

                                    objSchedule.OptionsetId = iOptionsetId;
                                    
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "di");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid"; 
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-o " + p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by Iti Puri for WPA-Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    // npadhy - RMACLOUD-2418 - ISO Compatible with Cloud
                                    // We need to pass the Client Id preceded with -c as the exe expects the identifier with single character.
                                    // All the other parameters as above is also of single character
                                    objArgElement.InnerText = "-c " + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by Iti Puri for WPA-Cloud-End
                                }
                                //MITS: 26428
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud
                                    if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText =  p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES || p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)//Mgaba2:R7:Adding Case for History Tracking
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText ="-ds"+ m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru"+m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString( p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);
                                    //Adding argument for subtask . It is applicable only for history tracking.
                                    if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "subTaskType");
                                        objArgElement.InnerText = "-st" + p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    //Add by kuladeep for WPA-Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for WPA-Cloud-End
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "-ru" + m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for Cloud-End

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                m_sPassword = "-rp" + RMX_TM_PWD;
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "ClaimTypeCode");

                                if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                {
                                    if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                    {
                                        objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                    }
                                }
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "ClaimStatusCode");
                                if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                {
                                    if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                    {
                                        objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                    }
                                }
                                objTmpElement.AppendChild(objArgElement);


                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "FromDate");
                                objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "ToDate");
                                objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "UFromDate");
                                objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "UToDate");
                                objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                objTmpElement.AppendChild(objArgElement);
                               
                                objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                                          //Amitosh
                            else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_NOTIFY_DIARY)
                                {
                                  objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                            
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "OverDueDays");
                                objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//OverDueDays").InnerText;
                                objTmpElement.AppendChild(objArgElement);

                                
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "CreateSysDiary");
                                objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//CreateSysDiary").InnerText;
                                objTmpElement.AppendChild(objArgElement);

                                
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "SendEmailNotify");
                                objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmailNotify").InnerText;
                                objTmpElement.AppendChild(objArgElement);

                                
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "BothDiaryAndEmail");
                                objArgElement.InnerText =p_objXmlDoc.SelectSingleNode("//BothDiaryAndEmail").InnerText;
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for Cloud-End

                                objSchedule.Config = objXmlDoc.OuterXml;
                         

                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    //Ankit Start for Point Balancing
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystemsClaimsBalancing").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText))
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "0";
                                        //objArgElement.InnerText = Conversion.GetDate( System.DateTime.Now.ToShortDateString());
                                        objTmpElement.AppendChild(objArgElement);

                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText);
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value))
                                    {

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                              

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystems").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "0";
                                    objTmpElement.AppendChild(objArgElement);
                                  
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "false";
                                    objTmpElement.AppendChild(objArgElement);
                                

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText))
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
								//Anu Tennyson added to support Auto Mail Merge Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_AUTO_MAIL_MERGE)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "AutoMerge");
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                   
                                }
                                //Anu Tennyson Ends
                                // akaushik5 Added for MITS 36381 Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                {
                                    this.CreateResbalOption(objXmlDoc, p_objXmlDoc);
                                }
                                // akaushik5 Added for MITS 36381 Ends
								else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_OSHA)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    object oSMTP = DbFactory.ExecuteScalar(m_sDbConnstring, "SELECT SMTP_SERVER FROM SYS_PARMS");
                                    objArgElement.InnerText = oSMTP.ToString();
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = RMConfigurationManager.GetAppSetting("SMDataPath");
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "connectionstring");
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sConnectionString);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "connectionstring");
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sDbConnstring);
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud
                                }
                                //Rakhel OSHA Enhancement - End

                                else // Not a DI task.
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//bParams").InnerText != "")
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                        }

                    }
                    //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES || (sModulename == TASK_HIST_TRACK && p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText != "2") || string.Equals(sModulename, TASK_CLAIM_BALANCING, StringComparison.OrdinalIgnoreCase)) //Ankit Start for Point Balancing
                    {
                        if (IsLoginAdmin(sModulename))
                        {                           
                            SaveSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error",m_iClientId), sModulename));
                        }
                    }
                    else
                        SaveSettings(objSchedule);
                    //changed by nadim for BES
                }
                else
                {
                    sSQL = "SELECT CONFIG FROM TM_SCHEDULE WHERE SCHEDULE_ID = " + objSchedule.ScheduleId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                //psaiteja MITS - 33592 - 08/26/2013
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                 objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:

                            //    objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objSchedule.Config = objXmlDoc.OuterXml;

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);
                                
                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            default:
                                // Check if it a Data Integrator task.
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText != "-1")
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES || p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)//Mgaba2:R7:Adding Case for History Tracking
                                    {
                                        //objTmpElement = objXmlDoc.CreateElement("Args");
                                        //objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        //// TODO : Try a more elegant approach for the code below.

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = m_sDataSourceName;
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = m_sLoginName;
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.SetAttribute("type", "password");
                                        //objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.SetAttribute("type", "user");
                                        //objArgElement.SetAttribute("role", "admin");
                                        //objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        //m_sAdminUserName = objArgElement.InnerText;//changed by nadim for BES
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.SetAttribute("type", "password");
                                        //objArgElement.SetAttribute("role", "admin");
                                        //objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        //m_sAdminPassword = RMCryptography.DecryptString(objArgElement.InnerText);//changed by nadim for BES
                                        //objTmpElement.AppendChild(objArgElement);

                                        //Mgaba2: MITS 22457:Can not edit the BES job in Taskmanager 
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ru" + m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        m_sPassword = "-rp" + RMX_TM_PWD;
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.SetAttribute("role", "admin");
                                        if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                        {
                                            objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        }
                                        m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.SetAttribute("role", "admin");
                                        if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                        {
                                            objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                        else
                                        {
                                            objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        }
                                        m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);
                                        //Adding argument for subtask . It is applicable only for history tracking.
                                        if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.SetAttribute("type", "subTaskType");
                                            objArgElement.InnerText = "-st" + p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText;
                                            objTmpElement.AppendChild(objArgElement);
                                        }

                                        //Add by kuladeep for WPA-Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by kuladeep for WPA-Cloud-End
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)//FOR FAS 
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ru" + m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);


                                        //Add by kuladeep for Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by kuladeep for Cloud-End

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        m_sPassword = "-rp" + RMX_TM_PWD;
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimTypeCode");

                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimStatusCode");
                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "FromDate");
                                        objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ToDate");
                                        objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UFromDate");
                                        objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UToDate");
                                        objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objSchedule.Config = objXmlDoc.OuterXml;
                                    }
                                    // akaushik5 Added for MITS 36381 Starts
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                    {
                                        this.UpdateResbalOption(objXmlDoc, p_objXmlDoc);
                                    }
                                    // akaushik5 Added for MITS 36381 Ends
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//FOR TASK_BATCH_PRINT
                                    {

                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by rkaur27 for Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by rkaur27 for Cloud-End


                                    }//FOR TASK_BATCH_PRINT
                                    else
                                    {
                                    if (p_objXmlDoc.SelectSingleNode("//Arguments").InnerText != "")
                                    {
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                    }
                                    objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Args");
                                    if (sArgs != null && sArgs.Length != 0 && objTmpElement != null)
                                    {
                                        int count = 0;
                                        objArrToDelete = new ArrayList();
                                        foreach (XmlNode objTmpNode in objTmpElement.ChildNodes)
                                        {
                                            if (count < sArgs.Length)
                                            {
                                                objTmpNode.InnerText = sArgs[count++];
                                            }
                                            else
                                            {
                                                objArrToDelete.Add(objTmpNode);
                                            }
                                        }
                                        for (int i = 0; i < objArrToDelete.Count; i++)
                                        {
                                            objTmpElement.RemoveChild((XmlElement)objArrToDelete[i]);
                                        }
                                        if (count < sArgs.Length)
                                        {
                                            for (int i = count; i < sArgs.Length; i++)
                                            {
                                                objArgElement = objXmlDoc.CreateElement("arg");
                                                objArgElement.InnerText = sArgs[i];
                                                objTmpElement.AppendChild(objArgElement);
                                            }
                                        }
                                    }
                                    else if (objTmpElement != null && (sArgs == null || sArgs.Length == 0))
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objTmpElement);
                                    }
                                    else if (sArgs != null && sArgs.Length != 0 && objTmpElement == null)
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }                                
                                }
                                }
                                //MITS: 26428
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud

                                    if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                        }

                    }
                    //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;

                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES || (sModulename == TASK_HIST_TRACK && p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText != "2"))//Mgaba2:R7:Adding Case for History Tracking
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            UpdateSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        UpdateSettings(objSchedule);
                    //changed by nadim for BES
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveOneTimeSettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
        }

        //Ankit Start for Claim Balancing
        private void SaveClaimBalancingParam(XmlDocument xmlDoc)
        {
            DbConnection objCon = null;
            DbCommand objCmd = null;
            //SysSettings objsettings = null;
            string sSQL = string.Empty;
            string sDateOfClaim = string.Empty;
            string sClaimType = string.Empty;
            int iPolicySystemID = 0;
            //int iClaimBasedDate = -1;
            try
            {
                if (xmlDoc != null)
                {
                    //objsettings = new SysSettings(m_sDbConnstring);
                    
                    //************************Variable Initialization****************************************************************
                    if (xmlDoc.SelectSingleNode("//ClmBalanceDate") != null && !string.IsNullOrEmpty(xmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText.Trim()))
                        sDateOfClaim = xmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText.Trim();
                    if (xmlDoc.SelectSingleNode("//ClaimBalance/ClaimType") != null && xmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"] != null
                        && !string.IsNullOrEmpty(xmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value.Trim()))
                        sClaimType = xmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value.Trim();
                    if (xmlDoc.SelectSingleNode("//PolicySystems") != null && !string.IsNullOrEmpty(xmlDoc.SelectSingleNode("//PolicySystems").InnerText))
                        iPolicySystemID = Convert.ToInt32(xmlDoc.SelectSingleNode("//PolicySystems").InnerText.Trim());
                    //if(int.Equals(objsettings.PolicyCvgType,1))
                    //    iClaimBasedDate = 0;
                    //**********************End Initialization**********************************************************************

                    ////**********************Truncating CLAIM_BALANCING_PARAMS Table***************************************************
                    //objCon = DbFactory.GetDbConnection(m_sDbConnstring);
                    //objCon.Open();
                    //sSQL = " TRUNCATE TABLE CLAIM_BALANCING_PARMS ";

                    //objCmd = objCon.CreateCommand();
                    //objCmd.CommandText = sSQL.ToString();
                    //objCmd.ExecuteNonQuery();
                    //objCon.Close();
                    ////************************End Truncate***************************************************************************

                    ////************************Inserting User Inputs******************************************************************
                    //DbWriter writer = DbFactory.GetDbWriter(m_sDbConnstring);
                    //writer.Tables.Add("CLAIM_BALANCING_PARMS");

                    //if (!string.IsNullOrEmpty(sDateOfClaim))
                    //    writer.Fields.Add("DATE_OF_CLAIM", Conversion.GetDate(sDateOfClaim));
                    //if(!string.IsNullOrEmpty(sClaimType) && !string.Equals(sClaimType,"0"))
                    //    writer.Fields.Add("CLAIM_TYPE", sClaimType);
                    //writer.Fields.Add("POLICY_SYSTEM_ID", Convert.ToInt32(iPolicySystemID));
                    //writer.Fields.Add("CLAIM_BASED_DATE", iClaimBasedDate);

                    //writer.Execute();
                    ////************************End Insertion**************************************************************************
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveClaimBalancingParam.Error",m_iClientId), objException);
            }
            finally
            {

                if (objCon != null)
                {
                    objCon.Close();
                    objCon.Dispose();
                }
                //if (objsettings != null)
                //    objsettings = null;
                if (objCmd != null)
                    objCmd = null;
            }
        }
        //Ankit End

        /// <summary>
        /// This method saves the user entered settings in case of Periodical Schedule Type.
        /// </summary>
        /// <param name="p_objXmlDoc">XmlIn containing user entered values</param>
        public void SavePeriodicalSettings(XmlDocument p_objXmlDoc)
        {
            ScheduleDetails objSchedule;
            DbReader objReader = null;
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objArgElement = null;
            ArrayList objArrToDelete = null;
            string sConfig = "";
            string[] sArgs = null;
            string sSQL = "";
            string sModulename = string.Empty;
            try
            {
                
                objSchedule = new ScheduleDetails();
                if (p_objXmlDoc.SelectSingleNode("//ScheduleId") != null)
                {
                    objSchedule.ScheduleId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ScheduleId").InnerText);
                }
                //objSchedule.ScheduleId = 1;
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 2;
                objSchedule.TaskTypeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//TaskType").InnerText);
                objSchedule.IntervalTypeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//IntervalTypeId").InnerText);
                objSchedule.Interval = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Interval").InnerText);
                objSchedule.TrigDirectory = "";
                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.DayOfMonth = 0;
                objSchedule.NextRunDTTM = Conversion.GetDateTime(p_objXmlDoc.SelectSingleNode("//Date").InnerText + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.FinalRunDTTM = "";

                //Adding subtask name with Task name : ukusvaha
                if (p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel") != null && (p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel").InnerText != "None" && p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel").InnerText.Trim() != ""))
                {
                    objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText + " - " + p_objXmlDoc.SelectSingleNode("//SubTaskNameLabel").InnerText;
                }
                else
                {
                    objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText;
                }
                //objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText;
                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;

                //SaveSettings(objSchedule);
                if (objSchedule.ScheduleId.ToString() == "" || objSchedule.ScheduleId <= 0)
                {
                    sSQL = "SELECT CONFIG FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + objSchedule.TaskTypeId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        // Prepare the arguments based on the Task Type.
                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                               // objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                //psaiteja MITS - 33592 - 08/26/2013
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");//psharma206
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);
                                
                            //    objSchedule.Config = objXmlDoc.OuterXml;
                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for Cloud-End

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                             default:
                                // Check if it a Data Integrator task.
                                //if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING)
                                //RMA-4606 nshah28(Comment above condition and including below condition)
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    int iOptionsetId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText);

                                    objSchedule.OptionsetId = iOptionsetId;

                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "di");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-o " + p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by Iti Puri for WPA-Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    // npadhy - RMACLOUD-2418 - ISO Compatible with Cloud
                                    // We need to pass the Client Id preceded with -c as the exe expects the identifier with single character.
                                    // All the other parameters as above is also of single character
                                    objArgElement.InnerText = "-c " + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by Iti Puri for WPA-Cloud-End
                                }
                                //MITS: 26428
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimTypeCode");

                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimStatusCode");
                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "FromDate");
                                    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ToDate");
                                    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UFromDate");
                                    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UToDate");
                                    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud

                                    if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End
                                }//for TASK_BATCH_PRINT PRINT
                                //changed by nadim-start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES || p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)//Mgaba2:R7:Adding Case for History Tracking
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);
                                    //adding argument for subtask. It is applicable only for History Tracking.
                                    if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "subTaskType");
                                        objArgElement.InnerText = "-st" + p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    //Add by kuladeep for WPA-Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for WPA-Cloud-End

                                }
                                //changed by nadim-end
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_NOTIFY_DIARY)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "OverDueDays");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//OverDueDays").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "CreateSysDiary");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//CreateSysDiary").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "SendEmailNotify");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmailNotify").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "BothDiaryAndEmail");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//BothDiaryAndEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                 

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystems").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "0";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "false";
                                    objTmpElement.AppendChild(objArgElement);
                                

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText))
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
								//Anu Tennyson added to support Auto Mail Merge Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_AUTO_MAIL_MERGE)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "AutoMerge");
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End

                                    objSchedule.Config = objXmlDoc.OuterXml;

                                }
                                //Anu Tennyson Ends

                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    //Ankit Start for Point Balancing
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);
                                    
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystemsClaimsBalancing").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText))
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = Conversion.GetDate(System.DateTime.Now.ToShortDateString());
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);

                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText);
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value))
                                    {

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value;
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
                                // akaushik5 Added for MITS 36381 Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                {
                                    this.CreateResbalOption(objXmlDoc, p_objXmlDoc);
                                }
                                // akaushik5 Added for MITS 36381 Ends
                                else // Not a DI task.
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//bParams").InnerText != "")
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                   //changed by nadim for BES                 
                    if (sModulename == TASK_BES || (sModulename == TASK_HIST_TRACK && p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText != "2") || string.Equals(sModulename,TASK_CLAIM_BALANCING,StringComparison.OrdinalIgnoreCase))   //Ankit Start for Point Balancing
                    {
                        if (IsLoginAdmin(sModulename))
                        {                           
                            SaveSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        SaveSettings(objSchedule);
                    //changed by nadim for BES
                }
                else
                {
                    sSQL = "SELECT CONFIG FROM TM_SCHEDULE WHERE SCHEDULE_ID = " + objSchedule.ScheduleId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //psaiteja MITS - 33592 - 08/26/2013
                               // objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);
                                
                            //    objSchedule.Config = objXmlDoc.OuterXml;
                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for Cloud-End

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            default:
                                // Check if it a Data Integrator task.
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText != "-1")
                                {//changed by nadim-start
                                    if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES || p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)//Mgaba2:R7:Adding Case for History Tracking
                                    {
                                        //objTmpElement = objXmlDoc.CreateElement("Args");
                                        //objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        //// TODO : Try a more elegant approach for the code below.

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = m_sDataSourceName;
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = m_sLoginName;
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.SetAttribute("type", "password");
                                        //objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.SetAttribute("type", "user");
                                        //objArgElement.SetAttribute("role", "admin");
                                        //objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        //m_sAdminUserName = objArgElement.InnerText;//changed by nadim for BES
                                        //objTmpElement.AppendChild(objArgElement);

                                        //objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.SetAttribute("type", "password");
                                        //objArgElement.SetAttribute("role", "admin");
                                        //objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        //m_sAdminPassword = RMCryptography.DecryptString(objArgElement.InnerText);//changed by nadim for BES
                                        //objTmpElement.AppendChild(objArgElement);

                                        //Mgaba2: MITS 22457:Can not edit the BES job in Taskmanager 
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ru" + m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        m_sPassword = "-rp" + RMX_TM_PWD;
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.SetAttribute("role", "admin");
                                        if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                        {
                                            objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        }
                                        m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.SetAttribute("role", "admin");
                                        if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                        {
                                            objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        }
                                        m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);
                                        //Adding argument for subtask . It is applicable only for history tracking.
                                        if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.SetAttribute("type", "subTaskType");
                                            objArgElement.InnerText = "-st" + p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText;
                                            objTmpElement.AppendChild(objArgElement);
                                        }

                                        //Add by kuladeep for WPA-Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by kuladeep for WPA-Cloud-End
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ru" + m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by kuladeep for Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by kuladeep for Cloud-End

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        m_sPassword = "-rp" + RMX_TM_PWD;
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimTypeCode");

                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimStatusCode");
                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "FromDate");
                                        objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ToDate");
                                        objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UFromDate");
                                        objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UToDate");
                                        objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objSchedule.Config = objXmlDoc.OuterXml;
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//FOR TASK_BATCH_PRINT
                                    {

                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by rkaur27 for Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by rkaur27 for Cloud-End


                                    }//FOR TASK_BATCH_PRINT
                                    // akaushik5 Added for MITS 36381 Starts
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                    {
                                        this.UpdateResbalOption(objXmlDoc, p_objXmlDoc);
                                    }
                                    // akaushik5 Added for MITS 36381 Ends
                                    else
                                    {
                                        if (p_objXmlDoc.SelectSingleNode("//Arguments").InnerText != "")
                                        {
                                            sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                        }
                                        objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Args");
                                        if (sArgs != null && sArgs.Length != 0 && objTmpElement != null)
                                        {
                                            int count = 0;
                                            objArrToDelete = new ArrayList();
                                            foreach (XmlNode objTmpNode in objTmpElement.ChildNodes)
                                            {
                                                if (count < sArgs.Length)
                                                {
                                                    objTmpNode.InnerText = sArgs[count++];
                                                }
                                                else
                                                {
                                                    objArrToDelete.Add(objTmpNode);
                                                }
                                            }
                                            for (int i = 0; i < objArrToDelete.Count; i++)
                                            {
                                                objTmpElement.RemoveChild((XmlElement)objArrToDelete[i]);
                                            }
                                            if (count < sArgs.Length)
                                            {
                                                for (int i = count; i < sArgs.Length; i++)
                                                {
                                                    objArgElement = objXmlDoc.CreateElement("arg");
                                                    objArgElement.InnerText = sArgs[i];
                                                    objTmpElement.AppendChild(objArgElement);
                                                }
                                            }
                                        }
                                        else if (objTmpElement != null && (sArgs == null || sArgs.Length == 0))
                                        {
                                            objXmlDoc.DocumentElement.RemoveChild(objTmpElement);
                                        }
                                        else if (sArgs != null && sArgs.Length != 0 && objTmpElement == null)
                                        {
                                            objTmpElement = objXmlDoc.CreateElement("Args");
                                            objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                            for (int count = 0; count < sArgs.Length; count++)
                                            {
                                                objArgElement = objXmlDoc.CreateElement("arg");
                                                objArgElement.InnerText = sArgs[count];
                                                objTmpElement.AppendChild(objArgElement);
                                            }
                                        }
                                    }
                             }
                            //MITS: 26428
                            else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "print");
                                // We will replace the jobid with the actual value from the 
                                // Task Manager Service at runtime, once it is available.
                                objArgElement.InnerText = "-j jobid";
                                objTmpElement.AppendChild(objArgElement);

                                //dvatsa-cloud
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //dvatsa-cloud

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                    objTmpElement.AppendChild(objArgElement);
                                }
                                objSchedule.Config = objXmlDoc.OuterXml;
                            }   
                            //changed by nadim -end
                                        //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                            objSchedule.Config = objXmlDoc.OuterXml;
                            break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES || (sModulename == TASK_HIST_TRACK && p_objXmlDoc.SelectSingleNode("//SubTaskValue").InnerText != "2"))//Mgaba2:R7:Adding Case for History Tracking
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            UpdateSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        UpdateSettings(objSchedule);
                    //changed by nadim for BES
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SavePeriodicalSettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
        }

        /// <summary>
        /// This method saves the user entered settings in case of Weekly Schedule Type.
        /// </summary>
        /// <param name="p_objXmlDoc">XmlIn containing user entered values</param>
        public void SaveWeeklySettings(XmlDocument p_objXmlDoc)
        {
            ScheduleDetails objSchedule;
            DbReader objReader = null;
            DateTime dttm;
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objArgElement = null;
            ArrayList objArrToDelete = null;
            string sConfig = "";
            string[] sArgs = null;
            string sSQL = "";
            bool bError = false;
            string sModulename = string.Empty;
            try
            {
                
                objSchedule = new ScheduleDetails();
                if (p_objXmlDoc.SelectSingleNode("//ScheduleId") != null)
                {
                    objSchedule.ScheduleId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ScheduleId").InnerText);
                }//
               // objSchedule.ScheduleId = 1;
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 3;
                objSchedule.TaskTypeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//TaskType").InnerText);
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";
                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.DayOfMonth = 0;
                objSchedule.NextRunDTTM = Conversion.GetDateTime(p_objXmlDoc.SelectSingleNode("//Date").InnerText + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.FinalRunDTTM = "";
                objSchedule.Mon = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Mon_Run").InnerText);
                objSchedule.Tue = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Tue_Run").InnerText);
                objSchedule.Wed = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Wed_Run").InnerText);
                objSchedule.Thu = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Thu_Run").InnerText);
                objSchedule.Fri = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Fri_Run").InnerText);
                objSchedule.Sat = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Sat_Run").InnerText);
                objSchedule.Sun = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Sun_Run").InnerText);
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;
                objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText;
                if (objSchedule.TaskName == TASK_OSHA_REPORTS) //33046 Rakhel Osha Enhancement
                {
                    objSchedule.ScheduleTypeId = 7; // 6 is used as default value in Taskmanager utility -- Rakhel
                }

                dttm = DateTime.Parse(p_objXmlDoc.SelectSingleNode("//Date").InnerText);//Conversion.ToDate(p_objXmlDoc.SelectSingleNode("//Date").InnerText);

                if (dttm.DayOfWeek == DayOfWeek.Monday && objSchedule.Mon == 0)
                {
                    bError = true;
                    if (objSchedule.Tue == -1)
                    {
                        dttm = dttm.AddDays(1);
                    }
                    else if (objSchedule.Wed == -1)
                    {
                        dttm = dttm.AddDays(2);
                    }
                    else if (objSchedule.Thu == -1)
                    {
                        dttm = dttm.AddDays(3);
                    }
                    else if (objSchedule.Fri == -1)
                    {
                        dttm = dttm.AddDays(4);
                    }
                    else if (objSchedule.Sat == -1)
                    {
                        dttm = dttm.AddDays(5);
                    }
                    else if (objSchedule.Sun == -1)
                    {
                        dttm = dttm.AddDays(6);
                    }
                }
                else if (dttm.DayOfWeek == DayOfWeek.Tuesday && objSchedule.Tue == 0)
                {
                    bError = true;
                    if (objSchedule.Wed == -1)
                    {
                        dttm = dttm.AddDays(1);
                    }
                    else if (objSchedule.Thu == -1)
                    {
                        dttm = dttm.AddDays(2);
                    }
                    else if (objSchedule.Fri == -1)
                    {
                        dttm = dttm.AddDays(3);
                    }
                    else if (objSchedule.Sat == -1)
                    {
                        dttm = dttm.AddDays(4);
                    }
                    else if (objSchedule.Sun == -1)
                    {
                        dttm = dttm.AddDays(5);
                    }
                    else if (objSchedule.Mon == -1)
                    {
                        dttm = dttm.AddDays(6);
                    }
                }
                else if (dttm.DayOfWeek == DayOfWeek.Wednesday && objSchedule.Wed == 0)
                {
                    bError = true;
                    if (objSchedule.Thu == -1)
                    {
                        dttm = dttm.AddDays(1);
                    }
                    else if (objSchedule.Fri == -1)
                    {
                        dttm = dttm.AddDays(2);
                    }
                    else if (objSchedule.Sat == -1)
                    {
                        dttm = dttm.AddDays(3);
                    }
                    else if (objSchedule.Sun == -1)
                    {
                        dttm = dttm.AddDays(4);
                    }
                    else if (objSchedule.Mon == -1)
                    {
                        dttm = dttm.AddDays(5);
                    }
                    else if (objSchedule.Tue == -1)
                    {
                        dttm = dttm.AddDays(6);
                    }

                }
                else if (dttm.DayOfWeek == DayOfWeek.Thursday && objSchedule.Thu == 0)
                {
                    bError = true;
                    if (objSchedule.Fri == -1)
                    {
                        dttm = dttm.AddDays(1);
                    }
                    else if (objSchedule.Sat == -1)
                    {
                        dttm = dttm.AddDays(2);
                    }
                    else if (objSchedule.Sun == -1)
                    {
                        dttm = dttm.AddDays(3);
                    }
                    else if (objSchedule.Mon == -1)
                    {
                        dttm = dttm.AddDays(4);
                    }
                    else if (objSchedule.Tue == -1)
                    {
                        dttm = dttm.AddDays(5);
                    }
                    else if (objSchedule.Wed == -1)
                    {
                        dttm = dttm.AddDays(6);
                    }
                }
                else if (dttm.DayOfWeek == DayOfWeek.Friday && objSchedule.Fri == 0)
                {
                    bError = true;
                    
                    if (objSchedule.Sat == -1)
                    {
                        dttm = dttm.AddDays(1);
                    }
                    else if (objSchedule.Sun == -1)
                    {
                        dttm = dttm.AddDays(2);
                    }
                    else if (objSchedule.Mon == -1)
                    {
                        dttm = dttm.AddDays(3);
                    }
                    else if (objSchedule.Tue == -1)
                    {
                        dttm = dttm.AddDays(4);
                    }
                    else if (objSchedule.Wed == -1)
                    {
                        dttm = dttm.AddDays(5);
                    }
                    else if (objSchedule.Thu == -1)
                    {
                        dttm = dttm.AddDays(6);
                    }
                }
                else if (dttm.DayOfWeek == DayOfWeek.Saturday && objSchedule.Sat == 0)
                {
                    bError = true;
                    if (objSchedule.Sun == -1)
                    {
                        dttm = dttm.AddDays(1);
                    }
                    else if (objSchedule.Mon == -1)
                    {
                        dttm = dttm.AddDays(2);
                    }
                    else if (objSchedule.Tue == -1)
                    {
                        dttm = dttm.AddDays(3);
                    }
                    else if (objSchedule.Wed == -1)
                    {
                        dttm = dttm.AddDays(4);
                    }
                    else if (objSchedule.Thu == -1)
                    {
                        dttm = dttm.AddDays(5);
                    }
                    else if (objSchedule.Fri == -1)
                    {
                        dttm = dttm.AddDays(6);
                    }
                }
                else if (dttm.DayOfWeek == DayOfWeek.Sunday && objSchedule.Sun == 0)
                {
                    bError = true;
                    if (objSchedule.Mon == -1)
                    {
                        dttm = dttm.AddDays(1);
                    }
                    else if (objSchedule.Tue == -1)
                    {
                        dttm = dttm.AddDays(2);
                    }
                    else if (objSchedule.Wed == -1)
                    {
                        dttm = dttm.AddDays(3);
                    }
                    else if (objSchedule.Thu == -1)
                    {
                        dttm = dttm.AddDays(4);
                    }
                    else if (objSchedule.Fri == -1)
                    {
                        dttm = dttm.AddDays(5);
                    }
                    else if (objSchedule.Sat == -1)
                    {
                        dttm = dttm.AddDays(6);
                    }
                }
                else if(objSchedule.Mon == 0 && objSchedule.Tue == 0 && objSchedule.Wed == 0 && objSchedule.Thu == 0 &&
                    objSchedule.Fri == 0 && objSchedule.Sat == 0 && objSchedule.Sun == 0)
                {
                    bError = true;
                    dttm = new DateTime(0, 0, 0);
                }

                if (bError)
                {
                    objSchedule.NextRunDTTM = Conversion.GetDateTime(dttm.ToShortDateString() + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                }

                if (objSchedule.ScheduleId.ToString() == "" || objSchedule.ScheduleId <= 0)
                {
                    sSQL = "SELECT CONFIG FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + objSchedule.TaskTypeId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        // Prepare the arguments based on the Task Type.
                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //psaiteja MITS - 33592 - 08/26/2013
                                //objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi for WPA-Cloud-End


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);
                                
                            //    objSchedule.Config = objXmlDoc.OuterXml;
                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi Cloud-End

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                         
                            default:

                                // Check if it a Data Integrator task.
                                //if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING)
                                //RMA-4606 nshah28(Comment above condition and including below condition)
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    int iOptionsetId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText);

                                    objSchedule.OptionsetId = iOptionsetId;

                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "di");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-o " + p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by Iti Puri for WPA-Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    // npadhy - RMACLOUD-2418 - ISO Compatible with Cloud
                                    // We need to pass the Client Id preceded with -c as the exe expects the identifier with single character.
                                    // All the other parameters as above is also of single character
                                    objArgElement.InnerText = "-c " + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by Iti Puri for WPA-Cloud-End
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimTypeCode");

                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimStatusCode");
                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "FromDate");
                                    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ToDate");
                                    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UFromDate");
                                    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UToDate");
                                    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                                //MITS: 26428
                                // akaushik5 Commneted for RMA-18903 Starts
                                //else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                //{
                                //    objTmpElement = objXmlDoc.CreateElement("Args");
                                //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                //    objTmpElement.AppendChild(objArgElement);

                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.InnerText = "-ru" + m_sLoginName;
                                //    objTmpElement.AppendChild(objArgElement);


                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "password");
                                //    m_sPassword = "-rp" + RMX_TM_PWD;
                                //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                //    objTmpElement.AppendChild(objArgElement);

                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                                //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                //    {
                                //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                //        {
                                //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                //        }
                                //        else
                                //        {
                                //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                //        }
                                //    }
                                //    objTmpElement.AppendChild(objArgElement);
                                //    //dvatsa-cloud
                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "clientid");
                                //    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                //    objTmpElement.AppendChild(objArgElement);
                                //    //dvatsa-cloud
                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                                //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                //    {
                                //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                //        {
                                //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                //        }
                                //        else
                                //        {
                                //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                //        }
                                //    }
                                //    objTmpElement.AppendChild(objArgElement);


                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "FromDate");
                                //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                //    objTmpElement.AppendChild(objArgElement);

                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "ToDate");
                                //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                //    objTmpElement.AppendChild(objArgElement);

                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "UFromDate");
                                //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                //    objTmpElement.AppendChild(objArgElement);

                                //    objArgElement = objXmlDoc.CreateElement("arg");
                                //    objArgElement.SetAttribute("type", "UToDate");
                                //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                //    objTmpElement.AppendChild(objArgElement);

                                //    objSchedule.Config = objXmlDoc.OuterXml;
                                //}
                                // akaushik5 Commneted for RMA-18903 Ends
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud

                                    if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End

                                }//for TASK_BATCH_PRINT PRINT
                                //changed by nadim -start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by prachi Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by prachi Cloud-End
                                }
                                //cahnged by nadim-end
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_NOTIFY_DIARY)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "OverDueDays");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//OverDueDays").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "CreateSysDiary");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//CreateSysDiary").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "SendEmailNotify");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmailNotify").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "BothDiaryAndEmail");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//BothDiaryAndEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End


                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                   

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystems").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "0";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "false";
                                    objTmpElement.AppendChild(objArgElement);
                                  

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText))
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
								//Anu Tennyson added to support Auto Mail Merge Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_AUTO_MAIL_MERGE)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "AutoMerge");
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End

                                    objSchedule.Config = objXmlDoc.OuterXml;

                                }
                                //Anu Tennyson Ends
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    //Ankit Start for Point Balancing
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);
                                                                        
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystemsClaimsBalancing").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                    
                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText))
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = Conversion.GetDate(System.DateTime.Now.ToShortDateString());
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);

                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText);
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value))
                                    {

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value;
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
                                    // akaushik5 Added for MITS 36381 Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                {
                                    this.CreateResbalOption(objXmlDoc, p_objXmlDoc);
                                }
                                // akaushik5 Added for MITS 36381 Ends
								
								  else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_OSHA)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    object oSMTP = DbFactory.ExecuteScalar(m_sDbConnstring, "SELECT SMTP_SERVER FROM SYS_PARMS");
                                    objArgElement.InnerText = oSMTP.ToString();
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = RMConfigurationManager.GetAppSetting("SMDataPath");
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "connectionstring");
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sConnectionString);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "connectionstring");
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sDbConnstring);
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud

                                }
                                //Rakhel OSHA Enhancement - End 

                                else // Not a DI task.
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//bParams").InnerText != "")
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES || string.Equals(sModulename,TASK_CLAIM_BALANCING,StringComparison.OrdinalIgnoreCase)) //Ankit Start for Point Balancing
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            SaveSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error",m_iClientId), sModulename));
                        }
                    }
                    else
                        SaveSettings(objSchedule);
                    //changed by nadim for BES
                }
                else
                {
                    sSQL = "SELECT CONFIG FROM TM_SCHEDULE WHERE SCHEDULE_ID = " + objSchedule.ScheduleId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //psaiteja MITS - 33592 - 08/26/2013
                                //objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi Cloud-End


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objSchedule.Config = objXmlDoc.OuterXml;
                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi Cloud-End

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            default:
                                // Check if it a Data Integrator task.
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText != "-1")
                                {
                                    //changed by nadim-start
                                    if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                    {
                                        //Mgaba2: MITS 22457:Can not edit the BES job in Taskmanager 
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.SetAttribute("role", "admin");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        m_sAdminUserName = objArgElement.InnerText;//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.SetAttribute("role", "admin");
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        m_sAdminPassword = RMCryptography.DecryptString(objArgElement.InnerText);//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by prachi Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by prachi Cloud-End


                                    }
                                    else if ((p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS))
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ru" + m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        m_sPassword = "-rp" + RMX_TM_PWD;
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimTypeCode");

                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimStatusCode");
                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "FromDate");
                                        objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ToDate");
                                        objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UFromDate");
                                        objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UToDate");
                                        objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objSchedule.Config = objXmlDoc.OuterXml;
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//FOR TASK_BATCH_PRINT
                                    {

                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by rkaur27 for Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by rkaur27 for Cloud-End

                                    }//FOR TASK_BATCH_PRINT
                                        // akaushik5 Added for MITS 36381 Starts
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                    {
                                        this.UpdateResbalOption(objXmlDoc, p_objXmlDoc);
                                    }
                                    // akaushik5 Added for MITS 36381 Ends
                                    else
                                    {
                                        if (p_objXmlDoc.SelectSingleNode("//Arguments").InnerText != "")
                                        {
                                            sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                        }
                                        objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Args");
                                        if (sArgs != null && sArgs.Length != 0 && objTmpElement != null)
                                        {
                                            int count = 0;
                                            objArrToDelete = new ArrayList();
                                            foreach (XmlNode objTmpNode in objTmpElement.ChildNodes)
                                            {
                                                if (count < sArgs.Length)
                                                {
                                                    objTmpNode.InnerText = sArgs[count++];
                                                }
                                                else
                                                {
                                                    objArrToDelete.Add(objTmpNode);
                                                }
                                            }
                                            for (int i = 0; i < objArrToDelete.Count; i++)
                                            {
                                                objTmpElement.RemoveChild((XmlElement)objArrToDelete[i]);
                                            }
                                            if (count < sArgs.Length)
                                            {
                                                for (int i = count; i < sArgs.Length; i++)
                                                {
                                                    objArgElement = objXmlDoc.CreateElement("arg");
                                                    objArgElement.InnerText = sArgs[i];
                                                    objTmpElement.AppendChild(objArgElement);
                                                }
                                            }
                                        }
                                        else if (objTmpElement != null && (sArgs == null || sArgs.Length == 0))
                                        {
                                            objXmlDoc.DocumentElement.RemoveChild(objTmpElement);
                                        }
                                        else if (sArgs != null && sArgs.Length != 0 && objTmpElement == null)
                                        {
                                            objTmpElement = objXmlDoc.CreateElement("Args");
                                            objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                            for (int count = 0; count < sArgs.Length; count++)
                                            {
                                                objArgElement = objXmlDoc.CreateElement("arg");
                                                objArgElement.InnerText = sArgs[count];
                                                objTmpElement.AppendChild(objArgElement);
                                            }
                                        }
                                    }
                            }
                            //MITS: 26428
                            else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "print");
                                // We will replace the jobid with the actual value from the 
                                // Task Manager Service at runtime, once it is available.
                                objArgElement.InnerText = "-j jobid";
                                objTmpElement.AppendChild(objArgElement);

                                //dvatsa-cloud
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //dvatsa-cloud

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                    objTmpElement.AppendChild(objArgElement);
                                }
                            }
                            //changed by nadim-end
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                            objSchedule.Config = objXmlDoc.OuterXml;
                            break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES)
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            UpdateSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        UpdateSettings(objSchedule);
                    //changed by nadim for BES
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveWeeklySettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
        }

        /// <summary>
        /// This method saves the user entered settings in case of Monthly Schedule Type.
        /// </summary>
        /// <param name="p_objXmlDoc">XmlIn containing user entered values</param>
        public void SaveMonthlySettings(XmlDocument p_objXmlDoc)
        {
            ScheduleDetails objSchedule;
            DbReader objReader = null;
            DateTime dttm;
            DateTime dttmNow;
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objArgElement = null;
            ArrayList objArrToDelete = null;
            string sConfig = "";
            string[] sArgs = null;
            string sSQL = "";
            string sModulename = string.Empty;
            try
            {
                
                objSchedule = new ScheduleDetails();
                if (p_objXmlDoc.SelectSingleNode("//ScheduleId") != null)
                {
                    objSchedule.ScheduleId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ScheduleId").InnerText);
                }
                //objSchedule.ScheduleId = 1;
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 4;
                objSchedule.TaskTypeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//TaskType").InnerText);
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";
                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.DayOfMonth = 0;
                //objSchedule.NextRunDTTM = Conversion.GetDateTime(p_objXmlDoc.SelectSingleNode("//Date").InnerText + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.FinalRunDTTM = "";
                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;
                objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText;

                dttmNow = DateTime.Now;
                if (dttmNow.Month > Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Month").InnerText))
                {
                    dttm = new DateTime(dttmNow.Year + 1, Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Month").InnerText), Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//DayOfMonth").InnerText));
                }
                else
                {
                    dttm = new DateTime(dttmNow.Year, Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Month").InnerText), Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//DayOfMonth").InnerText));
                }

                objSchedule.NextRunDTTM = Conversion.GetDateTime(dttm.ToShortDateString() + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);

                if (objSchedule.ScheduleId.ToString() == "" || objSchedule.ScheduleId <= 0)
                {
                    sSQL = "SELECT CONFIG FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + objSchedule.TaskTypeId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        // Prepare the arguments based on the Task Type.
                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //psaiteja MITS - 33592 - 08/26/2013
                               // objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi Cloud-End


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);
                                
                            //    objSchedule.Config = objXmlDoc.OuterXml;


                            //    objSchedule.Config = objXmlDoc.OuterXml;

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi Cloud-End

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                           
                            default:
                                // Check if it a Data Integrator task.
                                //if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING)
                                //RMA-4606 nshah28(Comment above condition and including below condition)
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    int iOptionsetId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText);

                                    objSchedule.OptionsetId = iOptionsetId;

                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "di");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-o " + p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    // npadhy - RMACLOUD-2418 - ISO Compatible with Cloud
                                    // We need to pass the Client Id preceded with -c as the exe expects the identifier with single character.
                                    // All the other parameters as above is also of single character
                                    objArgElement.InnerText = "-c " + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud
                                }
                                //MITS: 26428
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimTypeCode");

                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimStatusCode");
                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "FromDate");
                                    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ToDate");
                                    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UFromDate");
                                    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UToDate");
                                    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //dvatsa-cloud

                                    if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }    
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End
                                }//for TASK_BATCH_PRINT PRINT
                                //changed by nadim
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by prachi Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by prachi Cloud-End

                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_NOTIFY_DIARY)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "OverDueDays");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//OverDueDays").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "CreateSysDiary");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//CreateSysDiary").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "SendEmailNotify");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmailNotify").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "BothDiaryAndEmail");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//BothDiaryAndEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                   
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystems").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "0";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "false";
                                    objTmpElement.AppendChild(objArgElement);
                               

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText))
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
								//Anu Tennyson added to support Auto Mail Merge Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_AUTO_MAIL_MERGE)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "AutoMerge");
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End
                                    objSchedule.Config = objXmlDoc.OuterXml;

                                }
                                //Anu Tennyson Ends
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    //Ankit Start for Point Balancing
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);
                                    
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystemsClaimsBalancing").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText))
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = Conversion.GetDate(System.DateTime.Now.ToShortDateString());
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);

                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText);
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value))
                                    {

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value;
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
                                // akaushik5 Added for MITS 36381 Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                {
                                    this.CreateResbalOption(objXmlDoc, p_objXmlDoc);
                                }
                                // akaushik5 Added for MITS 36381 Ends
                                    //changed by nadim
                                else // Not a DI task.
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//bParams").InnerText != "")
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES || string.Equals(sModulename,TASK_CLAIM_BALANCING,StringComparison.OrdinalIgnoreCase))  //Ankit Start for Point Balancing
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            SaveSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        SaveSettings(objSchedule);
                    //changed by nadim for BES
                }
                else
                {
                    sSQL = "SELECT CONFIG FROM TM_SCHEDULE WHERE SCHEDULE_ID = " + objSchedule.ScheduleId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //psaiteja MITS - 33592 - 08/26/2013
                               // objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi Cloud-End

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objSchedule.Config = objXmlDoc.OuterXml;

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                            case TASK_BILL:

                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            default:
                                // Check if it a Data Integrator task.
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText != "-1")
                                {
                                    //changed by nadim
                                    if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                    {
                                        //Mgaba2: MITS 22457:Can not edit the BES job in Taskmanager 
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.SetAttribute("role", "admin");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        m_sAdminUserName = objArgElement.InnerText;//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.SetAttribute("role", "admin");
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        m_sAdminPassword = RMCryptography.DecryptString(objArgElement.InnerText);//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by prachi Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by prachi Cloud-End

                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ru" + m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        m_sPassword = "-rp" + RMX_TM_PWD;
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimTypeCode");

                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimStatusCode");
                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "FromDate");
                                        objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ToDate");
                                        objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UFromDate");
                                        objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UToDate");
                                        objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objSchedule.Config = objXmlDoc.OuterXml;
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//FOR TASK_BATCH_PRINT
                                    {

                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by rkaur27 for Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by rkaur27 for Cloud-End
                                    }//FOR TASK_BATCH_PRINT
                                    // akaushik5 Added for MITS 36381 Starts
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                    {
                                        this.UpdateResbalOption(objXmlDoc, p_objXmlDoc);
                                    }
                                    // akaushik5 Added for MITS 36381 Ends
                                    else
                                    {

                                        //changed by nadim
                                    if (p_objXmlDoc.SelectSingleNode("//Arguments").InnerText != "")
                                    {
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                    }
                                    objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Args");
                                    if (sArgs != null && sArgs.Length != 0 && objTmpElement != null)
                                    {
                                        int count = 0;
                                        objArrToDelete = new ArrayList();
                                        foreach (XmlNode objTmpNode in objTmpElement.ChildNodes)
                                        {
                                            if (count < sArgs.Length)
                                            {
                                                objTmpNode.InnerText = sArgs[count++];
                                            }
                                            else
                                            {
                                                objArrToDelete.Add(objTmpNode);
                                            }
                                        }
                                        for (int i = 0; i < objArrToDelete.Count; i++)
                                        {
                                            objTmpElement.RemoveChild((XmlElement)objArrToDelete[i]);
                                        }
                                        if (count < sArgs.Length)
                                        {
                                            for (int i = count; i < sArgs.Length; i++)
                                            {
                                                objArgElement = objXmlDoc.CreateElement("arg");
                                                objArgElement.InnerText = sArgs[i];
                                                objTmpElement.AppendChild(objArgElement);
                                            }
                                        }
                                    }
                                    else if (objTmpElement != null && (sArgs == null || sArgs.Length == 0))
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objTmpElement);
                                    }
                                    else if (sArgs != null && sArgs.Length != 0 && objTmpElement == null)
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }
                                }
                            }
                            //MITS: 26428
                            else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "print");
                                // We will replace the jobid with the actual value from the 
                                // Task Manager Service at runtime, once it is available.
                                objArgElement.InnerText = "-j jobid";
                                objTmpElement.AppendChild(objArgElement);

                                //dvatsa-cloud
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //dvatsa-cloud

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                    objTmpElement.AppendChild(objArgElement);
                                }
                                objSchedule.Config = objXmlDoc.OuterXml;
                            }
                            //changed by nadim
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                            objSchedule.Config = objXmlDoc.OuterXml;
                            break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES)
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            UpdateSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        UpdateSettings(objSchedule);
                    //changed by nadim for BES
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveMonthlySettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
        }

        /// <summary>
        /// This method saves the user entered settings in case of Yearly Schedule Type.
        /// </summary>
        /// <param name="p_objXmlDoc">XmlIn containing user entered values</param>
        public void SaveYearlySettings(XmlDocument p_objXmlDoc)
        {
            ScheduleDetails objSchedule;
            DbReader objReader = null;
            DateTime dttm;
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objArgElement = null;
            ArrayList objArrToDelete = null;
            string sConfig = "";
            string[] sArgs = null;
            string sSQL = "";
            bool bError = false;
            string sModulename = string.Empty;
            try
            {
                
                objSchedule = new ScheduleDetails();
                if (p_objXmlDoc.SelectSingleNode("//ScheduleId") != null)
                {
                    objSchedule.ScheduleId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ScheduleId").InnerText);
                }
                //objSchedule.ScheduleId = 1;
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 5;
                objSchedule.TaskTypeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//TaskType").InnerText);
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";
                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.DayOfMonth = 0;
                objSchedule.NextRunDTTM = Conversion.GetDateTime(p_objXmlDoc.SelectSingleNode("//Date").InnerText + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.FinalRunDTTM = "";
                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Jan_Run").InnerText);
                objSchedule.Feb = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Feb_Run").InnerText);
                objSchedule.Mar = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Mar_Run").InnerText);
                objSchedule.Apr = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Apr_Run").InnerText);
                objSchedule.May = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//May_Run").InnerText);
                objSchedule.Jun = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Jun_Run").InnerText);
                objSchedule.Jul = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Jul_Run").InnerText);
                objSchedule.Aug = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Aug_Run").InnerText);
                objSchedule.Sep = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Sep_Run").InnerText);
                objSchedule.Oct = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Oct_Run").InnerText);
                objSchedule.Nov = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Nov_Run").InnerText);
                objSchedule.Dec = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Dec_Run").InnerText);
                objSchedule.TaskName = p_objXmlDoc.SelectSingleNode("//TaskName").InnerText;

                dttm = DateTime.Parse(p_objXmlDoc.SelectSingleNode("//Date").InnerText);

                if (dttm.Month == 1 && objSchedule.Jan == 0)
                {
                    bError = true;
                    if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 2 && objSchedule.Feb == 0)
                {
                    bError = true;
                    if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 3 && objSchedule.Mar == 0)
                {
                    bError = true;
                    if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 4 && objSchedule.Apr == 0)
                {
                    bError = true;
                    if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 5 && objSchedule.May == 0)
                {
                    bError = true;
                    if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 6 && objSchedule.Jun == 0)
                {
                    bError = true;
                    if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 7 && objSchedule.Jul == 0)
                {
                    bError = true;
                    if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 8 && objSchedule.Aug == 0)
                {
                    bError = true;
                    if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 9 && objSchedule.Sep == 0)
                {
                    bError = true;
                    if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 10 && objSchedule.Oct == 0)
                {
                    bError = true;
                    if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 11 && objSchedule.Nov == 0)
                {
                    bError = true;
                    if (objSchedule.Dec == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (dttm.Month == 12 && objSchedule.Dec == 0)
                {
                    bError = true;
                    if (objSchedule.Jan == -1)
                    {
                        dttm = dttm.AddMonths(1);
                    }
                    else if (objSchedule.Feb == -1)
                    {
                        dttm = dttm.AddMonths(2);
                    }
                    else if (objSchedule.Mar == -1)
                    {
                        dttm = dttm.AddMonths(3);
                    }
                    else if (objSchedule.Apr == -1)
                    {
                        dttm = dttm.AddMonths(4);
                    }
                    else if (objSchedule.May == -1)
                    {
                        dttm = dttm.AddMonths(5);
                    }
                    else if (objSchedule.Jun == -1)
                    {
                        dttm = dttm.AddMonths(6);
                    }
                    else if (objSchedule.Jul == -1)
                    {
                        dttm = dttm.AddMonths(7);
                    }
                    else if (objSchedule.Aug == -1)
                    {
                        dttm = dttm.AddMonths(8);
                    }
                    else if (objSchedule.Sep == -1)
                    {
                        dttm = dttm.AddMonths(9);
                    }
                    else if (objSchedule.Oct == -1)
                    {
                        dttm = dttm.AddMonths(10);
                    }
                    else if (objSchedule.Nov == -1)
                    {
                        dttm = dttm.AddMonths(11);
                    }
                }
                else if (objSchedule.Jan == 0 && objSchedule.Feb == 0 && objSchedule.Mar == 0 && objSchedule.Apr == 0 &&
                        objSchedule.May == 0 && objSchedule.Jun == 0 && objSchedule.Jul == 0 && objSchedule.Aug == 0 &&
                        objSchedule.Sep == 0 && objSchedule.Oct == 0 && objSchedule.Nov == 0 && objSchedule.Dec == 0)
                {
                    bError = true;
                    dttm = new DateTime(0, 0, 0);
                }

                if (bError)
                {
                    objSchedule.NextRunDTTM = Conversion.GetDateTime(dttm.ToShortDateString() + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                }

                //SaveSettings(objSchedule);
                if (objSchedule.ScheduleId.ToString() == "" || objSchedule.ScheduleId <= 0)
                {
                    sSQL = "SELECT CONFIG FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + objSchedule.TaskTypeId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        // Prepare the arguments based on the Task Type.
                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //psaiteja MITS - 33592 - 08/26/2013
                               // objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);


                                //Add by prachi for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi for WPA-Cloud-End



                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                                   
                            //    objSchedule.Config = objXmlDoc.OuterXml;

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_BILL:  //Code change by kuladeep for Jira-6035- Start
                            case TASK_FINHIST:
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for WPA-Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for WPA-Cloud-End

                               
                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                          
                            default:
                                // Check if it a Data Integrator task.
                                //if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING)
                                //RMA-4606 nshah28(Comment above condition and including below condition)
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_POLICY_SYSTEM_UPDATE && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CLAIM_BALANCING && p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    int iOptionsetId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText);

                                    objSchedule.OptionsetId = iOptionsetId;

                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "di");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-o " + p_objXmlDoc.SelectSingleNode("//OptionsetId").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by Iti Puri for WPA-Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    // npadhy - RMACLOUD-2418 - ISO Compatible with Cloud
                                    // We need to pass the Client Id preceded with -c as the exe expects the identifier with single character.
                                    // All the other parameters as above is also of single character
                                    objArgElement.InnerText = "-c " + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by Iti Puri for WPA-Cloud-End
                                }
                                //MITS: 26428
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimTypeCode");

                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ClaimStatusCode");
                                    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                    {
                                        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                        else
                                        {
                                            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                        }
                                    }
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "FromDate");
                                    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "ToDate");
                                    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UFromDate");
                                    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "UToDate");
                                    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);

                                    //dvatsa-cloud

                                    if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    objSchedule.Config = objXmlDoc.OuterXml;
                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End

                                }//for TASK_BATCH_PRINT PRINT
                                //changed by nadim
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    // TODO : Try a more elegant approach for the code below.

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;//changed by nadim for BES
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by prachi Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by prachi Cloud-End


                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_NOTIFY_DIARY)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "OverDueDays");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//OverDueDays").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "CreateSysDiary");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//CreateSysDiary").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "SendEmailNotify");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmailNotify").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "BothDiaryAndEmail");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//BothDiaryAndEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    //Add by kuladeep for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by kuladeep for Cloud-End

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    //Ankit Start for Point Balancing
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "-ru" + m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    m_sPassword = "-rp" + RMX_TM_PWD;
                                    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                    objTmpElement.AppendChild(objArgElement);
                                    
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystemsClaimsBalancing").InnerText;
                                    objTmpElement.AppendChild(objArgElement);


                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText))
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        //objArgElement.InnerText = Conversion.GetDate(System.DateTime.Now.ToShortDateString());
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);

                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//ClmBalanceDate").InnerText);
                                        objTmpElement.AppendChild(objArgElement);
                                    }

                                    if (string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value))
                                    {

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "0";
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    else
                                    {
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//ClaimBalance/ClaimType").Attributes["codeid"].Value;
                                        objTmpElement.AppendChild(objArgElement);
                                    }
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//SendEmail").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText != "")
                                    {
                                        objArgElement.InnerText = "-au" + p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    }
                                    m_sAdminUserName = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.SetAttribute("role", "admin");
                                    if (p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText != "")
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString("-ap" + p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    else
                                    {
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                    }
                                    m_sAdminPassword = p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;

                                }
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE)
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                  
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//PolicySystems").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "0";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = "false";
                                    objTmpElement.AppendChild(objArgElement);
                                 

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "print");
                                    // We will replace the jobid with the actual value from the 
                                    // Task Manager Service at runtime, once it is available.
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText))
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//dtActivity").InnerText;
                                    objTmpElement.AppendChild(objArgElement);

                                    objSchedule.Config = objXmlDoc.OuterXml;


                                }
								//Anu Tennyson added to support Auto Mail Merge Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_AUTO_MAIL_MERGE)//for TASK_BATCH_PRINT PRINT
                                {
                                    objTmpElement = objXmlDoc.CreateElement("Args");
                                    objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sDataSourceName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.InnerText = m_sLoginName;
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "password");
                                    objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                    objTmpElement.AppendChild(objArgElement);

                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "AutoMerge");
                                    objArgElement.InnerText = "-j jobid";
                                    objTmpElement.AppendChild(objArgElement);

                                    //Add by rkaur27 for Cloud-Start
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "clientid");
                                    objArgElement.InnerText = Convert.ToString(m_iClientId);
                                    objTmpElement.AppendChild(objArgElement);
                                    //Add by rkaur27 for Cloud-End

                                    objSchedule.Config = objXmlDoc.OuterXml;

                                }
                                //Anu Tennyson Ends
                                // akaushik5 Added for MITS 36381 Starts
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                {
                                    this.CreateResbalOption(objXmlDoc, p_objXmlDoc);
                                }
                                // akaushik5 Added for MITS 36381 Ends
                                    //changed by nadim
                                else // Not a DI task.
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//bParams").InnerText != "")
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES || string.Equals(sModulename, TASK_CLAIM_BALANCING, StringComparison.OrdinalIgnoreCase))  //Ankit Start for Point Balancing
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            SaveSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        SaveSettings(objSchedule);
                    //changed by nadim for BES
                }
                else
                {
                    sSQL = "SELECT CONFIG FROM TM_SCHEDULE WHERE SCHEDULE_ID = " + objSchedule.ScheduleId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        sConfig = objReader.GetString("CONFIG");
                        objXmlDoc = new XmlDocument();
                        objXmlDoc.LoadXml(sConfig);

                        switch (objSchedule.TaskTypeId)
                        {
                            case TASK_WPA:
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                //psaiteja MITS - 33592 - 08/26/2013
                              //  objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                //TM Changes for Password
                                objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                // The arg mentioned below makes sure that WPA is not running for the infinite time.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = "1";
                                objTmpElement.AppendChild(objArgElement);

                                //Add by prachi Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by prachi Cloud-End


                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                            //    objTmpElement = objXmlDoc.CreateElement("Args");
                            //    objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ds" + m_sDataSourceName;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.InnerText = "-ru" + m_sLoginName;
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "password");
                            //    m_sPassword = "-rp" + RMX_TM_PWD;
                            //    objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimTypeCode");

                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ClaimStatusCode");
                            //    if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                            //    {
                            //        if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //        else
                            //        {
                            //            objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objArgElement);


                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "FromDate");
                            //    objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "ToDate");
                            //    objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UFromDate");
                            //    objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                            //    objArgElement = objXmlDoc.CreateElement("arg");
                            //    objArgElement.SetAttribute("type", "UToDate");
                            //    objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                            //    objTmpElement.AppendChild(objArgElement);

                                
                            //    objSchedule.Config = objXmlDoc.OuterXml;

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_BILL:  //Code change by kuladeep for Jira-6035- Start
                            case TASK_FINHIST:

                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                // TODO : Try a more elegant approach for the code below.
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                //Add by kuladeep for Cloud-Start
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);
                                //Add by kuladeep for Cloud-End

                                //objArgElement = objXmlDoc.CreateElement("arg");
                                //objArgElement.InnerText = "-log";
                                //objTmpElement.AppendChild(objArgElement);

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText;
                                    objTmpElement.AppendChild(objArgElement);
                                }

                                objSchedule.Config = objXmlDoc.OuterXml;
                                break;
                            default:
                                // Check if it a Data Integrator task.
                                if (p_objXmlDoc.SelectSingleNode("//IsDataIntegratorTask").InnerText != "-1")
                                {
                                    //changed by nadim
                                    if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                    {
                                        //Mgaba2: MITS 22457:Can not edit the BES job in Taskmanager 
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "user");
                                        objArgElement.SetAttribute("role", "admin");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//AdminLogin").InnerText;
                                        m_sAdminUserName = objArgElement.InnerText;//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.SetAttribute("role", "admin");
                                        objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//AdminPassword").InnerText);
                                        m_sAdminPassword = RMCryptography.DecryptString(objArgElement.InnerText);//changed by nadim for BES
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by prachi Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by prachi Cloud-End


                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ds" + m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = "-ru" + m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        m_sPassword = "-rp" + RMX_TM_PWD;
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimTypeCode");

                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ny" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedCTypeComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ClaimStatusCode");
                                        if (p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']") != null)
                                        {
                                            if (!string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText))
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                            else
                                            {
                                                objArgElement.InnerText = "-ns" + p_objXmlDoc.SelectSingleNode("//control[@name='txtRelatedComponents']").InnerText;
                                            }
                                        }
                                        objTmpElement.AppendChild(objArgElement);


                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "FromDate");
                                        objArgElement.InnerText = "-nf" + p_objXmlDoc.SelectSingleNode("//FromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "ToDate");
                                        objArgElement.InnerText = "-nt" + p_objXmlDoc.SelectSingleNode("//ToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UFromDate");
                                        objArgElement.InnerText = "-nm" + p_objXmlDoc.SelectSingleNode("//UFromDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "UToDate");
                                        objArgElement.InnerText = "-nn" + p_objXmlDoc.SelectSingleNode("//UToDate").InnerText;
                                        objTmpElement.AppendChild(objArgElement);


                                        objSchedule.Config = objXmlDoc.OuterXml;
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BATCH_PRINT)//FOR TASK_BATCH_PRINT
                                    {

                                        objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));

                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                        // TODO : Try a more elegant approach for the code below.
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sDataSourceName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = m_sLoginName;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "password");
                                        objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsFroiBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//IsAcordBatch").InnerText;
                                        objTmpElement.AppendChild(objArgElement);

                                        //Add by rkaur27 for Cloud-Start
                                        objArgElement = objXmlDoc.CreateElement("arg");
                                        objArgElement.SetAttribute("type", "clientid");
                                        objArgElement.InnerText = Convert.ToString(m_iClientId);
                                        objTmpElement.AppendChild(objArgElement);
                                        //Add by rkaur27 for Cloud-End

                                    }//FOR TASK_BATCH_PRINT
                                    // akaushik5 Added for MITS 36381 Starts
                                    else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                    {
                                        this.UpdateResbalOption(objXmlDoc, p_objXmlDoc);
                                    }
                                    // akaushik5 Added for MITS 36381 Ends
                                    else
                                    {

                                        //changed by nadim
                                    if (p_objXmlDoc.SelectSingleNode("//Arguments").InnerText != "")
                                    {
                                        sArgs = p_objXmlDoc.SelectSingleNode("//Arguments").InnerText.Trim().Split(' ');
                                    }
                                    objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Args");
                                    if (sArgs != null && sArgs.Length != 0 && objTmpElement != null)
                                    {
                                        int count = 0;

                                        foreach (XmlNode objTmpNode in objTmpElement.ChildNodes)
                                        {
                                            if (count < sArgs.Length)
                                            {
                                                objTmpNode.InnerText = sArgs[count++];
                                            }
                                            else
                                            {
                                                objArrToDelete.Add(objTmpNode);
                                            }
                                        }
                                        for (int i = 0; i < objArrToDelete.Count; i++)
                                        {
                                            objTmpElement.RemoveChild((XmlElement)objArrToDelete[i]);
                                        }
                                        if (count < sArgs.Length)
                                        {
                                            for (int i = count; i < sArgs.Length; i++)
                                            {
                                                objArgElement = objXmlDoc.CreateElement("arg");
                                                objArgElement.InnerText = sArgs[i];
                                                objTmpElement.AppendChild(objArgElement);
                                            }
                                        }
                                    }
                                    else if (objTmpElement != null && (sArgs == null || sArgs.Length == 0))
                                    {
                                        objXmlDoc.DocumentElement.RemoveChild(objTmpElement);
                                    }
                                    else if (sArgs != null && sArgs.Length != 0 && objTmpElement == null)
                                    {
                                        objTmpElement = objXmlDoc.CreateElement("Args");
                                        objXmlDoc.DocumentElement.AppendChild(objTmpElement);
                                        for (int count = 0; count < sArgs.Length; count++)
                                        {
                                            objArgElement = objXmlDoc.CreateElement("arg");
                                            objArgElement.InnerText = sArgs[count];
                                            objTmpElement.AppendChild(objArgElement);
                                        }
                                    }
                                }
                            }
                            //MITS: 26428
                            else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
                                objTmpElement = objXmlDoc.CreateElement("Args");
                                objXmlDoc.DocumentElement.AppendChild(objTmpElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sDataSourceName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.InnerText = m_sLoginName;
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "password");
                                objArgElement.InnerText = RMCryptography.EncryptString(RMX_TM_PWD);
                                objTmpElement.AppendChild(objArgElement);

                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "print");
                                // We will replace the jobid with the actual value from the 
                                // Task Manager Service at runtime, once it is available.
                                objArgElement.InnerText = "-j jobid";
                                objTmpElement.AppendChild(objArgElement);

                                //dvatsa-cloud
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "clientid");
                                objArgElement.InnerText = Convert.ToString(m_iClientId);
                                objTmpElement.AppendChild(objArgElement);

                                //dvatsa-cloud

                                if (p_objXmlDoc.SelectSingleNode("//UserArguments").InnerText != "")
                                {
                                    objArgElement = objXmlDoc.CreateElement("arg");
                                    objArgElement.SetAttribute("type", "user");
                                    objArgElement.InnerXml = p_objXmlDoc.SelectSingleNode("//UserArguments").InnerXml;
                                    objTmpElement.AppendChild(objArgElement);
                                }
                                objSchedule.Config = objXmlDoc.OuterXml;
                            }
                                //JIRA RMA-4606 nshah28 start
                                else if (p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForSaveCurrencyExchangeInterface(p_objXmlDoc, objXmlDoc, ref objTmpElement, ref objArgElement);
                                }
                                //JIRA RMA-4606 nshah28 End
                            objSchedule.Config = objXmlDoc.OuterXml;
                            break;
                        }
                    }
                     //MGaba2:R7:In case of History Tracking,SysAdmin rights are required
                    //while in case of BES,SysAdmin/SysSecurity both are allowed
                    sModulename = p_objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText;
                    //changed by nadim for BES                 
                    if (sModulename == TASK_BES)
                    {
                        if (IsLoginAdmin(sModulename))
                        {
                            UpdateSettings(objSchedule);
                        }
                        else
                        {
                            throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), sModulename));
                        }
                    }
                    else
                        UpdateSettings(objSchedule);
                    //changed by nadim for BES
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveYearlySettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
        }

        public XmlDocument SaveDirTrigSettings(XmlDocument p_objXmlDoc)
        {
            XmlElement objTmpElement = null;
            ScheduleDetails objSchedule;
            DbReader objReader = null;
            string sSQL = "";
            try
            {
                objSchedule = new ScheduleDetails();
                objSchedule.ScheduleId = 1;
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 1;
                objSchedule.TaskTypeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//TaskType").InnerText);
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";
                objSchedule.TimeToRun = Conversion.GetTime(p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.DayOfMonth = 0;
                objSchedule.NextRunDTTM = Conversion.GetDateTime(p_objXmlDoc.SelectSingleNode("//Date").InnerText + " " + p_objXmlDoc.SelectSingleNode("//Time").InnerText);
                objSchedule.FinalRunDTTM = "";

                sSQL = "SELECT CONFIG FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + objSchedule.TaskTypeId.ToString();
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objSchedule.Config = objReader.GetString("CONFIG");
                }

                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveDirTrigSettings.Error", m_iClientId), objException);
            }
            finally
            {
                objTmpElement = null;
                //if (objReader != null)
                //    objReader.Dispose();
            }
            return p_objXmlDoc;
        }

        /// <summary>
        /// This method saves the values entered in all the schedule types to db.
        /// </summary>
        /// <param name="p_objSchedule">A structure which is filled according to the schedule type and passed to
        /// this method to save it to db.</param>
        public void SaveSettings(ScheduleDetails p_objSchedule)
        {
            //DbReader objReader = null;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            //Ashish Ahuja MITS 34537 - Made Parameterized Query
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            string sSQL = "";
            try
            {

                //sSQL = "SELECT * FROM TM_SCHEDULE WHERE TASK_TYPE_ID = " + p_objSchedule.TaskTypeId.ToString();
                //objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                //if (objReader.Read())
                //{
                //    throw new RMAppException("There is one schedule already thr for this task ");
                //}

                objConn = DbFactory.GetDbConnection(ConnectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                //Ashish Ahuja MITS 34537 - Made Parameterized Query Start
                sSQL = string.Format("INSERT INTO TM_SCHEDULE (SCHEDULE_ID, SCHEDULE_STATE, SCHEDULE_TYPE_ID, TASK_TYPE_ID, "
                    + "INTERVAL_TYPE_ID, INTERVAL, TRIGGER_DIRECTORY, TIME_TO_RUN, DAY_OF_MON_TO_RUN, NEXT_RUN_DTTM, "
                    + "FINAL_RUN_DTTM, CONFIG, MON_RUN, TUE_RUN, WED_RUN, THU_RUN, FRI_RUN, SAT_RUN, SUN_RUN, JAN_RUN, "
                    + "FEB_RUN, MAR_RUN, APR_RUN, MAY_RUN, JUN_RUN, JUL_RUN, AUG_RUN, SEP_RUN, OCT_RUN, NOV_RUN, DEC_RUN, TASK_NAME, OPTIONSET_ID,TM_USER,TM_DSN) VALUES(" +
                GetNextUID("TM_SCHEDULE").ToString() + ", {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},"+
                "{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33}",
                "~ScheduleState~", "~ScheduleTypeId~", "~TaskTypeId~", "~IntervalTypeId~", "~Interval~", "~TrigDirectory~", "~TimeToRun~",
                "~DayOfMonth~", "~NextRunDTTM~", "~FinalRunDTTM~", "~Config~", "~Mon~", "~Tue~","~Wed~","~Thu~","~Fri~","~Sat~","~Sun~","~Jan~","~Feb~"
                , "~Mar~", "~Apr~", "~May~", "~Jun~", "~Jul~", "~Aug~", "~Sep~", "~Oct~", "~Nov~", "~Dec~", "~TaskName~", "~OptionsetId~",
                "~m_sLoginName~", "~m_sDataSourceName~"+")");
                dictParams.Add("ScheduleState", p_objSchedule.ScheduleState.ToString());
                dictParams.Add("ScheduleTypeId", p_objSchedule.ScheduleTypeId.ToString());
                dictParams.Add("TaskTypeId", p_objSchedule.TaskTypeId.ToString());
                dictParams.Add("IntervalTypeId", p_objSchedule.IntervalTypeId.ToString());
                dictParams.Add("Interval", p_objSchedule.Interval.ToString());
                dictParams.Add("TrigDirectory", p_objSchedule.TrigDirectory);
                dictParams.Add("TimeToRun", p_objSchedule.TimeToRun);
                dictParams.Add("DayOfMonth", p_objSchedule.DayOfMonth.ToString());
                dictParams.Add("NextRunDTTM", p_objSchedule.NextRunDTTM);
                dictParams.Add("FinalRunDTTM", p_objSchedule.FinalRunDTTM);
                dictParams.Add("Config", p_objSchedule.Config);
                dictParams.Add("Mon", p_objSchedule.Mon);
                dictParams.Add("Tue", p_objSchedule.Tue);
                dictParams.Add("Wed", p_objSchedule.Wed);
                dictParams.Add("Thu", p_objSchedule.Thu);
                dictParams.Add("Fri", p_objSchedule.Fri);
                dictParams.Add("Sat", p_objSchedule.Sat);
                dictParams.Add("Sun", p_objSchedule.Sun);
                dictParams.Add("Jan", p_objSchedule.Jan);
                dictParams.Add("Feb", p_objSchedule.Feb);
                dictParams.Add("Mar", p_objSchedule.Mar);
                dictParams.Add("Apr", p_objSchedule.Apr);
                dictParams.Add("May", p_objSchedule.May);
                dictParams.Add("Jun", p_objSchedule.Jun);
                dictParams.Add("Jul", p_objSchedule.Jul);
                dictParams.Add("Aug", p_objSchedule.Aug);
                dictParams.Add("Sep", p_objSchedule.Sep);
                dictParams.Add("Oct", p_objSchedule.Oct);
                dictParams.Add("Nov", p_objSchedule.Nov);
                dictParams.Add("Dec", p_objSchedule.Dec);
                dictParams.Add("TaskName", p_objSchedule.TaskName.ToString());
                dictParams.Add("OptionsetId", p_objSchedule.OptionsetId.ToString());
                dictParams.Add("m_sLoginName", m_sLoginName);
                dictParams.Add("m_sDataSourceName", m_sDataSourceName);
                
                //p_objSchedule.ScheduleState.ToString() + ", " +
                //p_objSchedule.ScheduleTypeId.ToString() + ", " +
                //p_objSchedule.TaskTypeId.ToString() + ", " +
                //p_objSchedule.IntervalTypeId.ToString() + ", " +
                //p_objSchedule.Interval.ToString() + ", '" +
                //p_objSchedule.TrigDirectory + "', '" +
                //p_objSchedule.TimeToRun + "', " +
                //p_objSchedule.DayOfMonth.ToString() + ", '" +
                //p_objSchedule.NextRunDTTM + "', '" +
                //p_objSchedule.FinalRunDTTM + "', '" +
                //p_objSchedule.Config + "', " +
                //p_objSchedule.Mon.ToString() + ", " +
                //p_objSchedule.Tue.ToString() + ", " +
                //p_objSchedule.Wed.ToString() + ", " +
                //p_objSchedule.Thu.ToString() + ", " +
                //p_objSchedule.Fri.ToString() + ", " +
                //p_objSchedule.Sat.ToString() + ", " +
                //p_objSchedule.Sun.ToString() + ", " +
                //p_objSchedule.Jan.ToString() + ", " +
                //p_objSchedule.Feb.ToString() + ", " +
                //p_objSchedule.Mar.ToString() + ", " +
                //p_objSchedule.Apr.ToString() + ", " +
                //p_objSchedule.May.ToString() + ", " +
                //p_objSchedule.Jun.ToString() + ", " +
                //p_objSchedule.Jul.ToString() + ", " +
                //p_objSchedule.Aug.ToString() + ", " +
                //p_objSchedule.Sep.ToString() + ", " +
                //p_objSchedule.Oct.ToString() + ", " +
                //p_objSchedule.Nov.ToString() + ", " +
                //p_objSchedule.Dec.ToString() + ", '" +
                //p_objSchedule.TaskName.ToString() + "', " +
                //p_objSchedule.OptionsetId.ToString() + ", '" +
                //m_sLoginName + "', '" +            
                //m_sDataSourceName + "')");

                objCommand.CommandText = sSQL;
                //objCommand.ExecuteNonQuery();
                DbFactory.ExecuteNonQuery(objCommand, dictParams);

                //Ashish Ahuja MITS 34537 - Made Parameterized Query End
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.SaveSettings.Error", m_iClientId), objException);
            }
            finally
            {
                //if (objReader != null)
                //    objReader.Dispose();
                if (objConn != null)
                    objConn.Dispose();
            }
        }

        /// <summary>
        /// This method updates the edited values to db.
        /// </summary>
        /// <param name="p_objSchedule">A structure which is filled according to the schedule type and passed to
        /// this method to save it to db.</param>
        private void UpdateSettings(ScheduleDetails p_objSchedule)
        {
            DbReader objReader = null;
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string sSQL = "";
            try
            {

                objConn = DbFactory.GetDbConnection(ConnectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();

                sSQL = "UPDATE TM_SCHEDULE " +
                    "SET SCHEDULE_STATE = " + p_objSchedule.ScheduleState.ToString() + ", " +
                    "SCHEDULE_TYPE_ID = " + p_objSchedule.ScheduleTypeId.ToString() + ", " +
                    "TASK_TYPE_ID = " + p_objSchedule.TaskTypeId.ToString() + ", " +
                    "INTERVAL_TYPE_ID = " + p_objSchedule.IntervalTypeId.ToString() + ", " +
                    "INTERVAL = " + p_objSchedule.Interval.ToString() + ", " +
                    "TRIGGER_DIRECTORY = '" + p_objSchedule.TrigDirectory.ToString() + "', " +
                    "TIME_TO_RUN = '" + p_objSchedule.TimeToRun.ToString() + "', " +
                    "DAY_OF_MON_TO_RUN = " + p_objSchedule.DayOfMonth.ToString() + ", " +
                    "NEXT_RUN_DTTM = '" + p_objSchedule.NextRunDTTM.ToString() + "', " +
                    "FINAL_RUN_DTTM = '" + p_objSchedule.FinalRunDTTM.ToString() + "', " +
                    "CONFIG = '" + p_objSchedule.Config.ToString() + "', " +
                    "MON_RUN = " + p_objSchedule.Mon.ToString() + ", " +
                    "TUE_RUN = " + p_objSchedule.Tue.ToString() + ", " +
                    "WED_RUN = " + p_objSchedule.Wed.ToString() + ", " +
                    "THU_RUN = " + p_objSchedule.Thu.ToString() + ", " +
                    "FRI_RUN = " + p_objSchedule.Fri.ToString() + ", " +
                    "SAT_RUN = " + p_objSchedule.Sat.ToString() + ", " +
                    "SUN_RUN = " + p_objSchedule.Sun.ToString() + ", " +
                    "JAN_RUN = " + p_objSchedule.Jan.ToString() + ", " +
                    "FEB_RUN = " + p_objSchedule.Feb.ToString() + ", " +
                    "MAR_RUN = " + p_objSchedule.Mar.ToString() + ", " +
                    "APR_RUN = " + p_objSchedule.Apr.ToString() + ", " +
                    "MAY_RUN = " + p_objSchedule.May.ToString() + ", " +
                    "JUN_RUN = " + p_objSchedule.Jun.ToString() + ", " +
                    "JUL_RUN = " + p_objSchedule.Jul.ToString() + ", " +
                    "AUG_RUN = " + p_objSchedule.Aug.ToString() + ", " +
                    "SEP_RUN = " + p_objSchedule.Sep.ToString() + ", " +
                    "OCT_RUN = " + p_objSchedule.Oct.ToString() + ", " +
                    "NOV_RUN = " + p_objSchedule.Nov.ToString() + ", " +
                    "DEC_RUN = " + p_objSchedule.Dec.ToString() + ", " +
                    "TASK_NAME = '" + p_objSchedule.TaskName.ToString() + "' " +
                    "WHERE SCHEDULE_ID = " + p_objSchedule.ScheduleId.ToString();

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.UpdateSettings.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objConn != null)
                    objConn.Dispose();
            }
        }

        private bool IsDetailedStatusDisabled(int p_iTaskTypeId)
        {
            using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                objConn.Open();
                return Conversion.ConvertObjToBool(objConn.ExecuteScalar("SELECT DISABLE_DETAILED_STATUS FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + p_iTaskTypeId), m_iClientId);

            }
        }
         //Anu Tennyson : Added for Auto Mail Merge Functionality
        /// <summary>
        /// For getting the System Module Name of the task scheduled.
        /// </summary>
        /// <param name="p_iTaskTypeId">Task Id of the task</param>
        /// <returns>System Module Name of the task</returns>
        private string GetSystemModuleName(int p_iTaskTypeId)
        {
            using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
            {
                objConn.Open();
                return Convert.ToString(objConn.ExecuteScalar("SELECT SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + p_iTaskTypeId));

            }
        }

        /// <summary>
        /// Gets the xml for editing a OneTime schedule.
        /// </summary>
        /// <param name="p_objXmlin">Contains Schedule id</param>
        /// <returns>Xml containing the details of the schedule</returns>
        public XmlDocument EditOneTime(XmlDocument p_objXmlin)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            XmlElement objTmpArgElement = null;
            DbReader objReader = null;
            DbReader objReader2 = null;
            ArrayList objArrayList = null;
            XmlDocument objTmpXmlDoc = null;
            LocalCache objCache = null;
            SysSettings objSysSettings = null;
            const string CLAIM_BASED_FIN_HIST = "-zerobasedfinhist zerobasedcrit=claim";
            //const string EVENT_BASED_FIN_HIST = "-zerobasedfinhist zerobasedcrit=event";
            string sConfig = "";
            string sTaskType = "";
            string sScheduleType = "";
            string sTaskName = string.Empty;
            string sSubTaskName = "None";
            int iSubTaskValue = 0;
            string sSQL = "";
            string sLangCode = string.Empty;
            try
            {
                objSysSettings = new SysSettings(m_sDbConnstring, m_iClientId);
                objArrayList = new ArrayList();
                objXmlDoc = new XmlDocument();
                objTmpElement = objXmlDoc.CreateElement("Details"); //PJS: Now common page for Edit & save so needed common ref
                objXmlDoc.AppendChild(objTmpElement);
                objCache = new LocalCache(m_sDbConnstring, m_iClientId);
                sLangCode = p_objXmlin.SelectSingleNode("//LangCode").InnerText;

                sSQL = "SELECT SCHEDULE_ID, TASK_TYPE_ID, SCHEDULE_TYPE_ID, NEXT_RUN_DTTM, CONFIG, TASK_NAME, OPTIONSET_ID FROM TM_SCHEDULE"
                    + " WHERE SCHEDULE_ID = " + p_objXmlin.SelectSingleNode("//ScheduleId").InnerText;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleId");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("TaskType");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                    sTaskType = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    //seperate Task Nmae and SubTask name.
                    sTaskName = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));

                    iSubTaskValue = this.GetSubTaskId(ref sTaskName, ref sSubTaskName);
                    
                    objTmp = objXmlDoc.CreateElement("TaskName");
                    //objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                    objTmp.InnerText = sTaskName;
                    objTmpElement.AppendChild(objTmp);
                    
                    objTmp = objXmlDoc.CreateElement("TaskNameLabel");
                    //objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                    objTmp.InnerText = sTaskName;
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("SubTaskNameLabel");
                    objTmp.InnerText = sSubTaskName;
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("SubTaskValue");
                    objTmp.InnerText = iSubTaskValue.ToString();
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("ScheduleTypeId");
                    objTmp.InnerText = "1";
                    sScheduleType = "1";
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("Date");
                    objTmp.InnerText = Conversion.GetUIDate(objReader.GetString("NEXT_RUN_DTTM"),sLangCode, m_iClientId);
                    objTmpElement.AppendChild(objTmp);

                    string sNextRunDttm = objReader.GetString("NEXT_RUN_DTTM");

                    sNextRunDttm = Conversion.ToDate(sNextRunDttm).ToShortDateString() + " " + Conversion.ToDate(sNextRunDttm).ToShortTimeString();

                    objTmp = objXmlDoc.CreateElement("Time");
                    //objTmp.InnerText = Conversion.ToDate(objReader.GetString("NEXT_RUN_DTTM")).ToShortTimeString();
                    objTmp.InnerText = Conversion.ToDbTime(Convert.ToDateTime(sNextRunDttm));
                    objTmpElement.AppendChild(objTmp);

                    sConfig = objReader.GetString("CONFIG");
                    objTmpXmlDoc = new XmlDocument();
                    objTmpXmlDoc.LoadXml(sConfig);

                    objTmp = objXmlDoc.CreateElement("OptionsetId");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("OPTIONSET_ID"));
                    objTmpElement.AppendChild(objTmp);

                    bool bIsDataIntegratorTask = IsDetailedStatusDisabled(Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId));
                    sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                    using (objReader2 = DbFactory.GetDbReader(ConnectionString, sSQL))
                    {
                        if (objReader2.Read())
                        {
                            objTmp = objXmlDoc.CreateElement("TaskTypeText");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("NAME"));
                            objTmpElement.AppendChild(objTmp);

                            objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("DISABLE_DETAILED_STATUS"));
                            objTmpElement.AppendChild(objTmp);

                            objTmp = objXmlDoc.CreateElement("SystemModuleName");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("SYSTEM_MODULE_NAME"));
                            objTmpElement.AppendChild(objTmp);
                        }
                    }
                    //objReader2.Close();

                    switch (Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId))
                    {
                        case TASK_WPA:
                            break;

                        case TASK_BILL:
                            objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");
                            if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                            {
                                objTmp = objXmlDoc.CreateElement("BillingOption");
                                objTmp.InnerText = objTmpArgElement.InnerText;
                                objTmpElement.AppendChild(objTmp);
                            }
                            break;
                        //Start averma62 MITS 32386
                        //case TASK_FAS:
                        //    XmlElement objTmpChild = null;
                        //    DataSet objDS = null;
                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        if (objTmpArgElement.InnerText.Contains("-ny"))
                        //        {
                        //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                        //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        //            {
                        //                string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                        //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                        //                {
                        //                    if (Icount.Equals(0))
                        //                    {
                        //                        objTmp.InnerText = lstClmType[Icount];
                        //                    }
                        //                    else
                        //                    {
                        //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                        //                    }

                        //                }
                        //                objTmpElement.AppendChild(objTmp);

                        //                objTmp = null;
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                        //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                        //                {
                        //                    sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                        //                    objTmpChild = objXmlDoc.CreateElement("option");
                        //                    objTmpChild.SetAttribute("value", lstClmType[Icount]);
                        //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim();
                        //                    objTmp.AppendChild(objTmpChild);
                        //                }
                        //            }
                        //            objTmpElement.AppendChild(objTmp);
                        //        }

                        //    }
                        //    //Start Claim Type Not Related List box
                        //    objTmpChild = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                        //    StringBuilder sbSql = new StringBuilder();
                        //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                        //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                        //    sbSql.Append(" AND CODES.TABLE_ID=1023");
                        //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                        //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                        //    foreach (DataRow dr in objDS.Tables[0].Rows)
                        //    {
                        //        if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                        //        {
                        //            objTmpChild = objXmlDoc.CreateElement("option");
                        //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        //            objTmp.AppendChild(objTmpChild);
                        //        }
                        //    }
                        //    objTmpElement.AppendChild(objTmp);
                        //    //End Claim Type Not Related List box

                        //    objTmpChild = null;
                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {

                        //        if (objTmpArgElement.InnerText.Contains("-ns"))
                        //        {
                        //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                        //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        //            {
                        //                string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "txtRelatedComponents");

                        //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                        //                {
                        //                    if (Icount.Equals(0))
                        //                    {
                        //                        objTmp.InnerText = lstClmStatus[Icount];
                        //                    }
                        //                    else
                        //                    {
                        //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                        //                    }

                        //                }
                        //                objTmpElement.AppendChild(objTmp);
                        //                objTmp = null;
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "lstRelatedLossComponents");

                        //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                        //                {
                        //                    sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                        //                    objTmpChild = objXmlDoc.CreateElement("option");
                        //                    objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                        //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim();
                        //                    objTmp.AppendChild(objTmpChild);
                        //                }
                        //                objTmpElement.AppendChild(objTmp);
                        //            }
                        //        }
                        //    }

                        //    //Start Claim Status Not Related List box
                        //    objTmpChild = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "lstAvailableLossComponents");

                        //    sbSql = new StringBuilder();
                        //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                        //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                        //    sbSql.Append(" AND CODES.TABLE_ID=1002");
                        //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                        //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                        //    foreach (DataRow dr in objDS.Tables[0].Rows)
                        //    {
                        //        if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                        //        {
                        //            objTmpChild = objXmlDoc.CreateElement("option");
                        //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        //            objTmp.AppendChild(objTmpChild);
                        //        }
                        //    }
                        //    objTmpElement.AppendChild(objTmp);
                        //    //End Claim Status Not Related List box
                        //     objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASFolder");
                        //    objTmp.InnerXml = objSysSettings.FASFolder;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASServer");
                        //    objTmp.InnerXml = objSysSettings.FASServer;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASPassword");
                        //    objTmp.InnerXml = objSysSettings.FASPassword;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASUserId");
                        //    objTmp.InnerXml = objSysSettings.FASUserId;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("FromDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nf"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("ToDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nt"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("UFromDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nm"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("UToDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nn"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    break;
                        //End averma62 MITS 32386
                        case TASK_FINHIST:
                            objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");
                            
                            if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("zerobasedfinhist"))
                            {
                                objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.ToLower().Contains(CLAIM_BASED_FIN_HIST))
                                {
                                    objTmp = objXmlDoc.CreateElement("ClaimBasedFinHist");
                                    objTmp.InnerText = "1";
                                }
                                else 
                                {
                                    objTmp = objXmlDoc.CreateElement("EventBasedFinHist");
                                    objTmp.InnerText = "2";
                                }
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.Contains("-full"))
                                {
                                    objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                if (objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-full"))
                            {
                                objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-log"))
                            {
                                objTmp = objXmlDoc.CreateElement("CreateLog");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);
                            }
                            else
                            {
                                objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                objTmp.InnerText = "";
                                objTmpElement.AppendChild(objTmp);

                                objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                objTmp.InnerText = "";
                                objTmpElement.AppendChild(objTmp);
                            }
                            
                            break;
                        default:
                            if (bIsDataIntegratorTask && objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE || objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    XmlNodeList objList = objTmpXmlDoc.SelectNodes("//arg");
                                    XmlDocument objdoc = GetPolicySystems();
                                    // XmlNode objPolicyListNode = null;
                                    //  objPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                    XmlNode objOldPolicyListNode = objXmlDoc.SelectSingleNode("//PolicySystemList");
                                    XmlNode objNewPolicyListNode = objXmlDoc.CreateElement("PolicySystemList");

                                    objNewPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));

                                    if (objOldPolicyListNode != null)
                                        objXmlDoc.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                                    else
                                        objXmlDoc.DocumentElement.AppendChild(objNewPolicyListNode);
                                }
                                //JIRA RMA-4606 nshah28 start
                                if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForEditCurrencyExchangeInterface(objXmlDoc, objTmpElement, objTmp, objTmpXmlDoc);
                                }
                                //JIRA RMA-4606 nshah28 End
                            }
                            //MITS: 26428
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                            {
                                XmlElement objTmpChild = null;
                                DataSet objDS = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    if (objTmpArgElement.InnerText.Contains("-ny"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmType[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);

                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmType[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                        }
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                }
                                //Start Claim Type Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                                StringBuilder sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1023");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Type Not Related List box

                                objTmpChild = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {

                                    if (objTmpArgElement.InnerText.Contains("-ns"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmStatus[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);
                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedLossComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                            objTmpElement.AppendChild(objTmp);
                                        }
                                    }
                                }

                                //Start Claim Status Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableLossComponents");

                                sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1002");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Status Not Related List box
                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASFolder");
                                objTmp.InnerXml = objSysSettings.FASFolder;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASServer");
                                objTmp.InnerXml = objSysSettings.FASServer;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASPassword");
                                objTmp.InnerXml = objSysSettings.FASPassword;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASUserId");
                                objTmp.InnerXml = objSysSettings.FASUserId;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASFileLocation");
                                objTmp.InnerXml = objSysSettings.FASFileLocation;
                                objTmpElement.AppendChild(objTmp);


                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("FromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nf"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("ToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nt"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UFromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nm"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nn"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            // akaushik5 Added for MITS 36381 Starts
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                            {
                                this.LoadResbalOptions(objXmlDoc, objTmpElement, objTmpXmlDoc);
                            }
                            // akaushik5 Added for MITS 36381 Ends
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//DocumentElement");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("DocumentElement");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES || objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)//Mgaba2:R7:Adding Case for History Tracking
                            {
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user' and @role='admin']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("AdminLogin");
                                    objTmp.InnerText = objTmpArgElement.InnerText;
                                    if (objTmp.InnerText.Contains("-au"))
                                    objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='password' and @role='admin']");

                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("AdminPassword");
                                    objTmp.InnerText = RMCryptography.DecryptString(objTmpArgElement.InnerText);
                                    if (objTmp.InnerText.Contains("-ap"))
                                    objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else
                            {
                                objTmp = objXmlDoc.CreateElement("Arguments");
                                objTmp.InnerText = "";

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//Args");
                                if (objTmpArgElement != null)
                                {
                                    foreach (XmlNode objTmpNode in objTmpArgElement.ChildNodes)
                                    {
                                        objTmp.InnerText += objTmpNode.InnerText + " ";
                                    }
                                    objTmpElement.AppendChild(objTmp);
                                    objTmp = objXmlDoc.CreateElement("bParams");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                else
                                {
                                    objTmpElement.AppendChild(objTmp);
                                    objTmp = objXmlDoc.CreateElement("bParams");
                                    objTmp.InnerText = "False";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            break;
                    }
                }
                objReader.Close();
                sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("TaskTypeText");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("NAME"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DISABLE_DETAILED_STATUS"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("SystemModuleName");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_MODULE_NAME"));
                    objTmpElement.AppendChild(objTmp);
                }
                objReader.Close();

                sSQL = "SELECT SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE WHERE SCHEDULE_TYPE_ID = " + sScheduleType;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleTypeText");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE"));
                    objTmpElement.AppendChild(objTmp);
                }
                objReader.Close();

                objTmp = objXmlDoc.CreateElement("ScheduleType");
                objTmpElement.AppendChild(objTmp);

                sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                    objTmp.AppendChild(objTmpElement);
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.Edit.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objSysSettings != null)
                    objSysSettings = null;
            }
            return objXmlDoc;
        }

        /// <summary>
        /// Get Sub Task Id
        /// </summary>
        /// <param name="sTaskName"></param>
        /// <param name="sSubTaskName"></param>
        /// <returns></returns>
        private int GetSubTaskId(ref string sTaskName, ref string sSubTaskName)
        {
            int iSubTaskValue = default(int);
            if (sTaskName.Contains("-"))
            {
                string[] sTask = sTaskName.Split('-');
                string originalTaskName = sTaskName;
                sTaskName = string.Join("-", sTask, 0, sTask.Length - 1);
                sSubTaskName = sTask.LastOrDefault();
                switch (sSubTaskName.Trim())
                {
                    case "Setup and Populate Data":
                        iSubTaskValue = 1;
                        break;
                    case "Purge Data":
                        iSubTaskValue = 2;
                        break;
                    case "Setup,Populate and Purge":
                        iSubTaskValue = 3;
                        break;
                    default:
                        sTaskName = originalTaskName;
                        sSubTaskName = "None";
                        break;
                }
            }
            return iSubTaskValue;
        }

       
        /// <summary>
        /// Gets the xml for editing a Weekly schedule.
        /// </summary>
        /// <param name="p_objXmlin">Contains Schedule id</param>
        /// <returns>Xml containing the details of the schedule</returns>
        public XmlDocument EditWeekly(XmlDocument p_objXmlin)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            DbReader objReader = null;
            DbReader objReader2 = null;
            ArrayList objArrayList = null;
            ScheduleDetails objScheduleDetails;
            XmlElement objTmpArgElement = null;
            XmlDocument objTmpXmlDoc = null;
            LocalCache objCache = null;//averma62 MITS 32386
            SysSettings objSysSettings = null;
            string sConfig = "";
            string sTaskType = "";
            string sScheduleType = "";
            string sSQL = "";
            const string CLAIM_BASED_FIN_HIST = "-zerobasedfinhist zerobasedcrit=claim";
            string sLangCode = string.Empty;
            try
            {
                objArrayList = new ArrayList();
                objXmlDoc = new XmlDocument();
                objScheduleDetails = new ScheduleDetails();
                objTmpElement = objXmlDoc.CreateElement("Details"); //PJS: Now common page for Edit & save so needed common ref
                objXmlDoc.AppendChild(objTmpElement);
                objCache = new LocalCache(m_sDbConnstring, m_iClientId);//averma62 MITS 32386
                objSysSettings = new SysSettings(m_sDbConnstring, m_iClientId);
                sLangCode = p_objXmlin.SelectSingleNode("//LangCode").InnerText;
                if (p_objXmlin.SelectSingleNode("//ScheduleId") != null && p_objXmlin.SelectSingleNode("//ScheduleId").InnerText != "")
                {
                    sSQL = "SELECT SCHEDULE_ID, TASK_TYPE_ID, SCHEDULE_TYPE_ID, NEXT_RUN_DTTM,"
                        + " MON_RUN, TUE_RUN, WED_RUN, THU_RUN, FRI_RUN, SAT_RUN, SUN_RUN, CONFIG, TASK_NAME, OPTIONSET_ID FROM TM_SCHEDULE"
                        + " WHERE SCHEDULE_ID = " + p_objXmlin.SelectSingleNode("//ScheduleId").InnerText;
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("ScheduleId");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_ID"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("TaskType");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                        sTaskType = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("TaskName");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("TaskNameLabel");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("ScheduleTypeId");
                        objTmp.InnerText = "3";//Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                        sScheduleType = "3";//Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Date");
                        objTmp.InnerText = Conversion.GetUIDate(objReader.GetString("NEXT_RUN_DTTM"), sLangCode, m_iClientId);
                        objTmpElement.AppendChild(objTmp);

                        string sNextRunDttm = objReader.GetString("NEXT_RUN_DTTM");

                        sNextRunDttm = Conversion.ToDate(sNextRunDttm).ToShortDateString() + " " + Conversion.ToDate(sNextRunDttm).ToShortTimeString();

                        objTmp = objXmlDoc.CreateElement("Time");
                        //objTmp.InnerText = Conversion.ToDate(objReader.GetString("NEXT_RUN_DTTM")).ToShortTimeString();
                        objTmp.InnerText = Conversion.ToDbTime(Convert.ToDateTime(sNextRunDttm));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Mon_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("MON_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Tue_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("TUE_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Wed_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("WED_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Thu_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("THU_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Fri_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("FRI_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Sat_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("SAT_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Sun_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("SUN_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Arguments");
                        objTmp.InnerText = "";

                        sConfig = objReader.GetString("CONFIG");
                        objTmpXmlDoc = new XmlDocument();
                        objTmpXmlDoc.LoadXml(sConfig);

                        objTmp = objXmlDoc.CreateElement("OptionsetId");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("OPTIONSET_ID"));
                        objTmpElement.AppendChild(objTmp);

                        bool bIsDataIntegratorTask = IsDetailedStatusDisabled(Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId));
                        //changed by nadim
                        sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                        using (objReader2 = DbFactory.GetDbReader(ConnectionString, sSQL))
                        {
                            if (objReader2.Read())
                            {
                                objTmp = objXmlDoc.CreateElement("TaskTypeText");
                                objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("NAME"));
                                objTmpElement.AppendChild(objTmp);

                                objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                                objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("DISABLE_DETAILED_STATUS"));
                                objTmpElement.AppendChild(objTmp);

                                objTmp = objXmlDoc.CreateElement("SystemModuleName");
                                objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("SYSTEM_MODULE_NAME"));
                                objTmpElement.AppendChild(objTmp);
                            }

                        }
                        //objReader2.Close();
                        //changed by nadim
                        switch (Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId))
                        {
                            case TASK_WPA:
                                break;

                            case TASK_BILL:
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("BillingOption");
                                    objTmp.InnerText = objTmpArgElement.InnerText;
                                    objTmpElement.AppendChild(objTmp);
                                }
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    XmlElement objTmpChild = null;
                            //    DataSet objDS = null;
                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        if (objTmpArgElement.InnerText.Contains("-ny"))
                            //        {
                            //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                            //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                            //            {
                            //                string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                            //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                            //                {
                            //                    if (Icount.Equals(0))
                            //                    {
                            //                        objTmp.InnerText = lstClmType[Icount];
                            //                    }
                            //                    else
                            //                    {
                            //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                            //                    }

                            //                }
                            //                objTmpElement.AppendChild(objTmp);

                            //                objTmp = null;
                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                            //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                            //                {
                            //                    sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                            //                    objTmpChild = objXmlDoc.CreateElement("option");
                            //                    objTmpChild.SetAttribute("value", lstClmType[Icount]);
                            //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim();
                            //                    objTmp.AppendChild(objTmpChild);
                            //                }
                            //            }
                            //            objTmpElement.AppendChild(objTmp);
                            //        }

                            //    }
                            //    //Start Claim Type Not Related List box
                            //    objTmpChild = null;
                            //    objTmp = objXmlDoc.CreateElement("control");
                            //    objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                            //    StringBuilder sbSql = new StringBuilder();
                            //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                            //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                            //    sbSql.Append(" AND CODES.TABLE_ID=1023");
                            //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                            //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                            //    foreach (DataRow dr in objDS.Tables[0].Rows)
                            //    {
                            //        if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                            //        {
                            //            objTmpChild = objXmlDoc.CreateElement("option");
                            //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                            //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                            //            objTmp.AppendChild(objTmpChild);
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objTmp);
                            //    //End Claim Type Not Related List box

                            //    objTmpChild = null;
                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {

                            //        if (objTmpArgElement.InnerText.Contains("-ns"))
                            //        {
                            //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                            //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                            //            {
                            //                string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "txtRelatedComponents");

                            //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                            //                {
                            //                    if (Icount.Equals(0))
                            //                    {
                            //                        objTmp.InnerText = lstClmStatus[Icount];
                            //                    }
                            //                    else
                            //                    {
                            //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                            //                    }

                            //                }
                            //                objTmpElement.AppendChild(objTmp);
                            //                objTmp = null;
                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "lstRelatedLossComponents");

                            //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                            //                {
                            //                    sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                            //                    objTmpChild = objXmlDoc.CreateElement("option");
                            //                    objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                            //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim();
                            //                    objTmp.AppendChild(objTmpChild);
                            //                }
                            //                objTmpElement.AppendChild(objTmp);
                            //            }
                            //        }
                            //    }

                            //    //Start Claim Status Not Related List box
                            //    objTmpChild = null;
                            //    objTmp = objXmlDoc.CreateElement("control");
                            //    objTmp.SetAttribute("name", "lstAvailableLossComponents");

                            //    sbSql = new StringBuilder();
                            //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                            //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                            //    sbSql.Append(" AND CODES.TABLE_ID=1002");
                            //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                            //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                            //    foreach (DataRow dr in objDS.Tables[0].Rows)
                            //    {
                            //        if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                            //        {
                            //            objTmpChild = objXmlDoc.CreateElement("option");
                            //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                            //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                            //            objTmp.AppendChild(objTmpChild);
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objTmp);
                            //    //End Claim Status Not Related List box
                            //    objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASFolder");
                            //objTmp.InnerXml = objSysSettings.FASFolder;
                            //objTmpElement.AppendChild(objTmp);

                            //objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASServer");
                            //objTmp.InnerXml = objSysSettings.FASServer;
                            //objTmpElement.AppendChild(objTmp);

                            //objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASPassword");
                            //objTmp.InnerXml = objSysSettings.FASPassword;
                            //objTmpElement.AppendChild(objTmp);

                            //objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASUserId");
                            //objTmp.InnerXml = objSysSettings.FASUserId;
                            //objTmpElement.AppendChild(objTmp);

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("FromDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nf"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("ToDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nt"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("UFromDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nm"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("UToDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nn"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");

                                if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("zerobasedfinhist"))
                                {
                                    objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);

                                    if (objTmpArgElement.InnerText.ToLower().Contains(CLAIM_BASED_FIN_HIST))
                                    {
                                        objTmp = objXmlDoc.CreateElement("ClaimBasedFinHist");
                                        objTmp.InnerText = "1";
                                    }
                                    else
                                    {
                                        objTmp = objXmlDoc.CreateElement("EventBasedFinHist");
                                        objTmp.InnerText = "2";
                                    }
                                    objTmpElement.AppendChild(objTmp);

                                    if (objTmpArgElement.InnerText.Contains("-full"))
                                    {
                                        objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                    if (objTmpArgElement.InnerText.Contains("-log"))
                                    {
                                        objTmp = objXmlDoc.CreateElement("CreateLog");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-full"))
                                {
                                    objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);

                                    if (objTmpArgElement.InnerText.Contains("-log"))
                                    {
                                        objTmp = objXmlDoc.CreateElement("CreateLog");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                else
                                {
                                    objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                    objTmp.InnerText = "";
                                    objTmpElement.AppendChild(objTmp);

                                    objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                    objTmp.InnerText = "";
                                    objTmpElement.AppendChild(objTmp);
                                }

                                break;
                            default:
                                if (bIsDataIntegratorTask && objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE || objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                    {
                                        XmlNodeList objList = objTmpXmlDoc.SelectNodes("//arg");
                                        XmlDocument objdoc = GetPolicySystems();
                                        // XmlNode objPolicyListNode = null;
                                        //  objPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                        XmlNode objOldPolicyListNode = objXmlDoc.SelectSingleNode("//PolicySystemList");
                                        XmlNode objNewPolicyListNode = objXmlDoc.CreateElement("PolicySystemList");
                                        objNewPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                        if (objOldPolicyListNode != null)
                                            objXmlDoc.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                                        else
                                            objXmlDoc.DocumentElement.AppendChild(objNewPolicyListNode);

                                    }
                                    //JIRA RMA-4606 nshah28 start
                                    else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                    {
                                        SettingsForEditCurrencyExchangeInterface(objXmlDoc, objTmpElement, objTmp, objTmpXmlDoc);
                                    }
                                    //JIRA RMA-4606 nshah28 End
                                }
                                //MITS: 26428
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                {
                                    XmlElement objTmpChild = null;
                                    DataSet objDS = null;
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {
                                        if (objTmpArgElement.InnerText.Contains("-ny"))
                                        {
                                            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                            {
                                                string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                                                objTmp = objXmlDoc.CreateElement("control");
                                                objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                                                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                                {
                                                    if (Icount.Equals(0))
                                                    {
                                                        objTmp.InnerText = lstClmType[Icount];
                                                    }
                                                    else
                                                    {
                                                        objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                                                    }

                                                }
                                                objTmpElement.AppendChild(objTmp);

                                                objTmp = null;
                                                objTmp = objXmlDoc.CreateElement("control");
                                                objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                                                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                                {
                                                    sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                                                    objTmpChild = objXmlDoc.CreateElement("option");
                                                    objTmpChild.SetAttribute("value", lstClmType[Icount]);
                                                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim();
                                                    objTmp.AppendChild(objTmpChild);
                                                }
                                            }
                                            objTmpElement.AppendChild(objTmp);
                                        }

                                    }
                                    //Start Claim Type Not Related List box
                                    objTmpChild = null;
                                    objTmp = objXmlDoc.CreateElement("control");
                                    objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                                    StringBuilder sbSql = new StringBuilder();
                                    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                    sbSql.Append(" AND CODES.TABLE_ID=1023");
                                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                    foreach (DataRow dr in objDS.Tables[0].Rows)
                                    {
                                        if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                        {
                                            objTmpChild = objXmlDoc.CreateElement("option");
                                            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                            objTmp.AppendChild(objTmpChild);
                                        }
                                    }
                                    objTmpElement.AppendChild(objTmp);
                                    //End Claim Type Not Related List box

                                    objTmpChild = null;
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {

                                        if (objTmpArgElement.InnerText.Contains("-ns"))
                                        {
                                            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                            {
                                                string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                                                objTmp = objXmlDoc.CreateElement("control");
                                                objTmp.SetAttribute("name", "txtRelatedComponents");

                                                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                                {
                                                    if (Icount.Equals(0))
                                                    {
                                                        objTmp.InnerText = lstClmStatus[Icount];
                                                    }
                                                    else
                                                    {
                                                        objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                                                    }

                                                }
                                                objTmpElement.AppendChild(objTmp);
                                                objTmp = null;
                                                objTmp = objXmlDoc.CreateElement("control");
                                                objTmp.SetAttribute("name", "lstRelatedLossComponents");

                                                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                                {
                                                    sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                                                    objTmpChild = objXmlDoc.CreateElement("option");
                                                    objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                                                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim();
                                                    objTmp.AppendChild(objTmpChild);
                                                }
                                                objTmpElement.AppendChild(objTmp);
                                            }
                                        }
                                    }

                                    //Start Claim Status Not Related List box
                                    objTmpChild = null;
                                    objTmp = objXmlDoc.CreateElement("control");
                                    objTmp.SetAttribute("name", "lstAvailableLossComponents");

                                    sbSql = new StringBuilder();
                                    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                    sbSql.Append(" AND CODES.TABLE_ID=1002");
                                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                    foreach (DataRow dr in objDS.Tables[0].Rows)
                                    {
                                        if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                        {
                                            objTmpChild = objXmlDoc.CreateElement("option");
                                            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                            objTmp.AppendChild(objTmpChild);
                                        }
                                    }
                                    objTmpElement.AppendChild(objTmp);
                                    //End Claim Status Not Related List box
                                    objTmp = null;
                                    objTmp = objXmlDoc.CreateElement("control");
                                    objTmp.SetAttribute("name", "FASFolder");
                                    objTmp.InnerXml = objSysSettings.FASFolder;
                                    objTmpElement.AppendChild(objTmp);

                                    objTmp = null;
                                    objTmp = objXmlDoc.CreateElement("control");
                                    objTmp.SetAttribute("name", "FASServer");
                                    objTmp.InnerXml = objSysSettings.FASServer;
                                    objTmpElement.AppendChild(objTmp);

                                    objTmp = null;
                                    objTmp = objXmlDoc.CreateElement("control");
                                    objTmp.SetAttribute("name", "FASPassword");
                                    objTmp.InnerXml = objSysSettings.FASPassword;
                                    objTmpElement.AppendChild(objTmp);

                                    objTmp = null;
                                    objTmp = objXmlDoc.CreateElement("control");
                                    objTmp.SetAttribute("name", "FASUserId");
                                    objTmp.InnerXml = objSysSettings.FASUserId;
                                    objTmpElement.AppendChild(objTmp);

                                    objTmp = null;
                                    objTmp = objXmlDoc.CreateElement("control");
                                    objTmp.SetAttribute("name", "FASFileLocation");
                                    objTmp.InnerXml = objSysSettings.FASFileLocation;
                                    objTmpElement.AppendChild(objTmp);


                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("FromDate");
                                        objTmp.InnerXml = objTmpArgElement.InnerXml;
                                        if (objTmp.InnerText.Contains("-nf"))
                                            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("ToDate");
                                        objTmp.InnerXml = objTmpArgElement.InnerXml;
                                        if (objTmp.InnerText.Contains("-nt"))
                                            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("UFromDate");
                                        objTmp.InnerXml = objTmpArgElement.InnerXml;
                                        if (objTmp.InnerText.Contains("-nm"))
                                            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("UToDate");
                                        objTmp.InnerXml = objTmpArgElement.InnerXml;
                                        if (objTmp.InnerText.Contains("-nn"))
                                            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                    // akaushik5 Added for MITS 36381 Starts
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                {
                                    this.LoadResbalOptions(objXmlDoc, objTmpElement, objTmpXmlDoc);
                                }
                                // akaushik5 Added for MITS 36381 Ends
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//DocumentElement");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("DocumentElement");
                                        objTmp.InnerXml = objTmpArgElement.InnerXml;
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                //changed by nadim
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                {
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user' and @role='admin']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("AdminLogin");
                                        objTmp.InnerText = objTmpArgElement.InnerText;
                                        if (objTmp.InnerText.Contains("-au"))
                                            objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='password' and @role='admin']");

                                    if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("AdminPassword");
                                        objTmp.InnerText = RMCryptography.DecryptString(objTmpArgElement.InnerText);
                                        if (objTmp.InnerText.Contains("-ap"))
                                            objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                //changed by nadim
                                else
                                {
                                    objTmp = objXmlDoc.CreateElement("Arguments");
                                    objTmp.InnerText = "";

                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//Args");
                                    if (objTmpArgElement != null)
                                    {
                                        foreach (XmlNode objTmpNode in objTmpArgElement.ChildNodes)
                                        {
                                            objTmp.InnerText += objTmpNode.InnerText + " ";
                                        }
                                        objTmpElement.AppendChild(objTmp);
                                        objTmp = objXmlDoc.CreateElement("bParams");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                    else
                                    {
                                        objTmpElement.AppendChild(objTmp);
                                        objTmp = objXmlDoc.CreateElement("bParams");
                                        objTmp.InnerText = "False";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                break;
                        }

                    }
                    objReader.Close();

                    sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("TaskTypeText");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("NAME"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DISABLE_DETAILED_STATUS"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("SystemModuleName");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_MODULE_NAME"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();
                    
                    sSQL = "SELECT SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE WHERE SCHEDULE_TYPE_ID = " + sScheduleType;
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("ScheduleTypeText");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();

                    objTmp = objXmlDoc.CreateElement("ScheduleType");
                    objTmpElement.AppendChild(objTmp);

                    sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        objTmpElement = objXmlDoc.CreateElement("option");
                        objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                        objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                        objTmp.AppendChild(objTmpElement);
                    }
                }
                else
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleTypeText");
                    objTmp.InnerText = "Weekly";
                    objTmpElement.AppendChild(objTmp);
                    objReader.Close();

                    objTmp = objXmlDoc.CreateElement("ScheduleType");
                    objTmpElement.AppendChild(objTmp);

                    sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        objTmpElement = objXmlDoc.CreateElement("option");
                        objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                        objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                        objTmp.AppendChild(objTmpElement);
                    }
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.Edit.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();

                if (objSysSettings != null)
                    objSysSettings = null;
            }
            return objXmlDoc;
        }

        /// <summary>
        /// Gets the xml for editing an Yearly schedule.
        /// </summary>
        /// <param name="p_objXmlin">Contains Schedule id</param>
        /// <returns>Xml containing the details of the schedule</returns>
        public XmlDocument EditYearly(XmlDocument p_objXmlin)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            DbReader objReader = null;
            DbReader objReader2 = null;
            ArrayList objArrayList = null;
            ScheduleDetails objScheduleDetails;
            XmlElement objTmpArgElement = null;
            XmlDocument objTmpXmlDoc = null;
            LocalCache objCache = null;
            SysSettings objSysSettings = null;
            string sConfig = "";
            string sTaskType = "";
            string sScheduleType = "";
            string sSQL = "";
            string sLangCode = string.Empty;
            const string CLAIM_BASED_FIN_HIST = "-zerobasedfinhist zerobasedcrit=claim";
            try
            {
                objArrayList = new ArrayList();
                objXmlDoc = new XmlDocument();
                objScheduleDetails = new ScheduleDetails();
                objTmpElement = objXmlDoc.CreateElement("Details"); //PJS: Now common page for Edit & save so needed common ref                
                objXmlDoc.AppendChild(objTmpElement);
                objCache = new LocalCache(m_sDbConnstring, m_iClientId); //averma62 MITS 32386
                objSysSettings = new SysSettings(m_sDbConnstring, m_iClientId);
                sLangCode = p_objXmlin.SelectSingleNode("//LangCode").InnerText;

                if (p_objXmlin.SelectSingleNode("//ScheduleId") != null && p_objXmlin.SelectSingleNode("//ScheduleId").InnerText != "")
                {
                    sSQL = "SELECT SCHEDULE_ID, TASK_TYPE_ID, SCHEDULE_TYPE_ID, NEXT_RUN_DTTM,"
                        + " JAN_RUN, FEB_RUN, MAR_RUN, APR_RUN, MAY_RUN, JUN_RUN, JUL_RUN, "
                        + " AUG_RUN, SEP_RUN, OCT_RUN, NOV_RUN, DEC_RUN, CONFIG, TASK_NAME, OPTIONSET_ID FROM TM_SCHEDULE"
                        + " WHERE SCHEDULE_ID = " + p_objXmlin.SelectSingleNode("//ScheduleId").InnerText;
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("ScheduleId");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_ID"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("TaskType");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                        sTaskType = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("TaskName");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("TaskNameLabel");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("ScheduleTypeId");
                        objTmp.InnerText = "5";//Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                        sScheduleType = "5";// Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Date");
                        objTmp.InnerText = Conversion.GetUIDate(objReader.GetString("NEXT_RUN_DTTM"), sLangCode, m_iClientId);
                        objTmpElement.AppendChild(objTmp);

                        string sNextRunDttm = objReader.GetString("NEXT_RUN_DTTM");

                        sNextRunDttm = Conversion.ToDate(sNextRunDttm).ToShortDateString() + " " + Conversion.ToDate(sNextRunDttm).ToShortTimeString();

                        objTmp = objXmlDoc.CreateElement("Time");
                        //objTmp.InnerText = Conversion.ToDate(objReader.GetString("NEXT_RUN_DTTM")).ToShortTimeString();
                        objTmp.InnerText = Conversion.ToDbTime(Convert.ToDateTime(sNextRunDttm));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Jan_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("JAN_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Feb_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("FEB_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Mar_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("MAR_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Apr_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("APR_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("May_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("MAY_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Jun_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("JUN_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Jul_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("JUL_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Aug_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("AUG_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Sep_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("SEP_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Oct_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("OCT_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Nov_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("NOV_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);
                        objTmp = objXmlDoc.CreateElement("Dec_Run");
                        objTmp.InnerText = Conversion.ConvertObjToBool(objReader.GetValue("DEC_RUN"), m_iClientId).ToString();
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("Arguments");
                        objTmp.InnerText = "";

                        sConfig = objReader.GetString("CONFIG");
                        objTmpXmlDoc = new XmlDocument();
                        objTmpXmlDoc.LoadXml(sConfig);

                        objTmp = objXmlDoc.CreateElement("OptionsetId");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("OPTIONSET_ID"));
                        objTmpElement.AppendChild(objTmp);

                        bool bIsDataIntegratorTask = IsDetailedStatusDisabled(Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId));
                        //changed by nadim
                        sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                        using (objReader2 = DbFactory.GetDbReader(ConnectionString, sSQL))
                        {
                            if (objReader2.Read())
                            {
                                objTmp = objXmlDoc.CreateElement("TaskTypeText");
                                objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("NAME"));
                                objTmpElement.AppendChild(objTmp);

                                objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                                objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("DISABLE_DETAILED_STATUS"));
                                objTmpElement.AppendChild(objTmp);

                                objTmp = objXmlDoc.CreateElement("SystemModuleName");
                                objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("SYSTEM_MODULE_NAME"));
                                objTmpElement.AppendChild(objTmp);
                            }

                        }
                        //objReader2.Close();
                        //changed by nadim
                        switch (Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId))
                        {
                            case TASK_WPA:
                                break;

                            case TASK_BILL:
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("BillingOption");
                                    objTmp.InnerText = objTmpArgElement.InnerText;
                                    objTmpElement.AppendChild(objTmp);
                                }
                                break;
                            //Start averma62 MITS 32386
                            //case TASK_FAS:
                            //    XmlElement objTmpChild = null;
                            //    DataSet objDS = null;
                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        if (objTmpArgElement.InnerText.Contains("-ny"))
                            //        {
                            //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                            //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                            //            {
                            //                string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                            //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                            //                {
                            //                    if (Icount.Equals(0))
                            //                    {
                            //                        objTmp.InnerText = lstClmType[Icount];
                            //                    }
                            //                    else
                            //                    {
                            //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                            //                    }

                            //                }
                            //                objTmpElement.AppendChild(objTmp);

                            //                objTmp = null;
                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                            //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                            //                {
                            //                    sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                            //                    objTmpChild = objXmlDoc.CreateElement("option");
                            //                    objTmpChild.SetAttribute("value", lstClmType[Icount]);
                            //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim();
                            //                    objTmp.AppendChild(objTmpChild);
                            //                }
                            //            }
                            //            objTmpElement.AppendChild(objTmp);
                            //        }

                            //    }
                            //    //Start Claim Type Not Related List box
                            //    objTmpChild = null;
                            //    objTmp = objXmlDoc.CreateElement("control");
                            //    objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                            //    StringBuilder sbSql = new StringBuilder();
                            //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                            //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                            //    sbSql.Append(" AND CODES.TABLE_ID=1023");
                            //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                            //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                            //    foreach (DataRow dr in objDS.Tables[0].Rows)
                            //    {
                            //        if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                            //        {
                            //            objTmpChild = objXmlDoc.CreateElement("option");
                            //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                            //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                            //            objTmp.AppendChild(objTmpChild);
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objTmp);
                            //    //End Claim Type Not Related List box

                            //    objTmpChild = null;
                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {

                            //        if (objTmpArgElement.InnerText.Contains("-ns"))
                            //        {
                            //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                            //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                            //            {
                            //                string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "txtRelatedComponents");

                            //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                            //                {
                            //                    if (Icount.Equals(0))
                            //                    {
                            //                        objTmp.InnerText = lstClmStatus[Icount];
                            //                    }
                            //                    else
                            //                    {
                            //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                            //                    }

                            //                }
                            //                objTmpElement.AppendChild(objTmp);
                            //                objTmp = null;
                            //                objTmp = objXmlDoc.CreateElement("control");
                            //                objTmp.SetAttribute("name", "lstRelatedLossComponents");

                            //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                            //                {
                            //                    sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                            //                    objTmpChild = objXmlDoc.CreateElement("option");
                            //                    objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                            //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim();
                            //                    objTmp.AppendChild(objTmpChild);
                            //                }
                            //                objTmpElement.AppendChild(objTmp);
                            //            }
                            //        }
                            //    }

                            //    //Start Claim Status Not Related List box
                            //    objTmpChild = null;
                            //    objTmp = objXmlDoc.CreateElement("control");
                            //    objTmp.SetAttribute("name", "lstAvailableLossComponents");

                            //    sbSql = new StringBuilder();
                            //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                            //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                            //    sbSql.Append(" AND CODES.TABLE_ID=1002");
                            //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                            //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                            //    foreach (DataRow dr in objDS.Tables[0].Rows)
                            //    {
                            //        if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                            //        {
                            //            objTmpChild = objXmlDoc.CreateElement("option");
                            //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                            //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                            //            objTmp.AppendChild(objTmpChild);
                            //        }
                            //    }
                            //    objTmpElement.AppendChild(objTmp);
                            //    //End Claim Status Not Related List box
                                
                            //     objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASFolder");
                            //objTmp.InnerXml = objSysSettings.FASFolder;
                            //objTmpElement.AppendChild(objTmp);

                            //objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASServer");
                            //objTmp.InnerXml = objSysSettings.FASServer;
                            //objTmpElement.AppendChild(objTmp);

                            //objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASPassword");
                            //objTmp.InnerXml = objSysSettings.FASPassword;
                            //objTmpElement.AppendChild(objTmp);

                            //objTmp = null;
                            //objTmp = objXmlDoc.CreateElement("control");
                            //objTmp.SetAttribute("name", "FASUserId");
                            //objTmp.InnerXml = objSysSettings.FASUserId;
                            //objTmpElement.AppendChild(objTmp);

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("FromDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nf"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("ToDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nt"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("UFromDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nm"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                            //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                            //    {
                            //        objTmp = objXmlDoc.CreateElement("UToDate");
                            //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                            //        if (objTmp.InnerText.Contains("-nn"))
                            //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                            //        objTmpElement.AppendChild(objTmp);
                            //    }

                            //    break;
                            //End averma62 MITS 32386
                            case TASK_FINHIST:
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");

                                if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("zerobasedfinhist"))
                                {
                                    objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);

                                    if (objTmpArgElement.InnerText.ToLower().Contains(CLAIM_BASED_FIN_HIST))
                                    {
                                        objTmp = objXmlDoc.CreateElement("ClaimBasedFinHist");
                                        objTmp.InnerText = "1";
                                    }
                                    else
                                    {
                                        objTmp = objXmlDoc.CreateElement("EventBasedFinHist");
                                        objTmp.InnerText = "2";
                                    }
                                    objTmpElement.AppendChild(objTmp);

                                    if (objTmpArgElement.InnerText.Contains("-full"))
                                    {
                                        objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                    if (objTmpArgElement.InnerText.Contains("-log"))
                                    {
                                        objTmp = objXmlDoc.CreateElement("CreateLog");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-full"))
                                {
                                    objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);

                                    if (objTmpArgElement.InnerText.Contains("-log"))
                                    {
                                        objTmp = objXmlDoc.CreateElement("CreateLog");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                else
                                {
                                    objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                    objTmp.InnerText = "";
                                    objTmpElement.AppendChild(objTmp);

                                    objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                    objTmp.InnerText = "";
                                    objTmpElement.AppendChild(objTmp);
                                }

                                break;
                            default:
                                if (bIsDataIntegratorTask && objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE || objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                    {
                                        XmlNodeList objList = objTmpXmlDoc.SelectNodes("//arg");
                                        XmlDocument objdoc = GetPolicySystems();
                                        // XmlNode objPolicyListNode = null;
                                        //  objPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                        XmlNode objOldPolicyListNode = objXmlDoc.SelectSingleNode("//PolicySystemList");
                                        XmlNode objNewPolicyListNode = objXmlDoc.CreateElement("PolicySystemList");
                                        objNewPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                        if (objOldPolicyListNode != null)
                                            objXmlDoc.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                                        else
                                            objXmlDoc.DocumentElement.AppendChild(objNewPolicyListNode);

                                    }
                                    //JIRA RMA-4606 nshah28 start
                                    else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                    {
                                        SettingsForEditCurrencyExchangeInterface(objXmlDoc, objTmpElement, objTmp, objTmpXmlDoc);
                                    }
                                    //JIRA RMA-4606 nshah28 End
                                }
                                //MITS: 26428
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                                {
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//DocumentElement");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("DocumentElement");
                                        objTmp.InnerXml = objTmpArgElement.InnerXml;
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                                {
                                    XmlElement objTmpChild = null;
                                DataSet objDS = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    if (objTmpArgElement.InnerText.Contains("-ny"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmType[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);

                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmType[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                        }
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                }
                                //Start Claim Type Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                                StringBuilder sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1023");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Type Not Related List box

                                objTmpChild = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {

                                    if (objTmpArgElement.InnerText.Contains("-ns"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmStatus[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);
                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedLossComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                            objTmpElement.AppendChild(objTmp);
                                        }
                                    }
                                }

                                //Start Claim Status Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableLossComponents");

                                sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1002");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Status Not Related List box
                                
                                 objTmp = null;
                            objTmp = objXmlDoc.CreateElement("control");
                            objTmp.SetAttribute("name", "FASFolder");
                            objTmp.InnerXml = objSysSettings.FASFolder;
                            objTmpElement.AppendChild(objTmp);

                            objTmp = null;
                            objTmp = objXmlDoc.CreateElement("control");
                            objTmp.SetAttribute("name", "FASServer");
                            objTmp.InnerXml = objSysSettings.FASServer;
                            objTmpElement.AppendChild(objTmp);

                            objTmp = null;
                            objTmp = objXmlDoc.CreateElement("control");
                            objTmp.SetAttribute("name", "FASPassword");
                            objTmp.InnerXml = objSysSettings.FASPassword;
                            objTmpElement.AppendChild(objTmp);

                            objTmp = null;
                            objTmp = objXmlDoc.CreateElement("control");
                            objTmp.SetAttribute("name", "FASUserId");
                            objTmp.InnerXml = objSysSettings.FASUserId;
                            objTmpElement.AppendChild(objTmp);

                            objTmp = null;
                            objTmp = objXmlDoc.CreateElement("control");
                            objTmp.SetAttribute("name", "FASFileLocation");
                            objTmp.InnerXml = objSysSettings.FASFileLocation;
                            objTmpElement.AppendChild(objTmp);


                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("FromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nf"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("ToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nt"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UFromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nm"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nn"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                                }
                                    // akaushik5 Added for MITS 36381 Starts
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                                {
                                    this.LoadResbalOptions(objXmlDoc, objTmpElement, objTmpXmlDoc);
                                }
                                // akaushik5 Added for MITS 36381 Ends
                                //changed by nadim
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                                {
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user' and @role='admin']");
                                    if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("AdminLogin");
                                        objTmp.InnerText = objTmpArgElement.InnerText;
                                        if (objTmp.InnerText.Contains("-au"))
                                            objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='password' and @role='admin']");

                                    if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                    {
                                        objTmp = objXmlDoc.CreateElement("AdminPassword");
                                        objTmp.InnerText = RMCryptography.DecryptString(objTmpArgElement.InnerText);
                                        if (objTmp.InnerText.Contains("-ap"))
                                            objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }

                                    //changed by nadim
                                else
                                {
                                    objTmp = objXmlDoc.CreateElement("Arguments");
                                    objTmp.InnerText = "";

                                    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//Args");
                                    if (objTmpArgElement != null)
                                    {
                                        foreach (XmlNode objTmpNode in objTmpArgElement.ChildNodes)
                                        {
                                            objTmp.InnerText += objTmpNode.InnerText + " ";
                                        }
                                        objTmpElement.AppendChild(objTmp);
                                        objTmp = objXmlDoc.CreateElement("bParams");
                                        objTmp.InnerText = "True";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                    else
                                    {
                                        objTmpElement.AppendChild(objTmp);
                                        objTmp = objXmlDoc.CreateElement("bParams");
                                        objTmp.InnerText = "False";
                                        objTmpElement.AppendChild(objTmp);
                                    }
                                }
                                break;
                        }


                    }
                    objReader.Close();
                    sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("TaskTypeText");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("NAME"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DISABLE_DETAILED_STATUS"));
                        objTmpElement.AppendChild(objTmp);

                        objTmp = objXmlDoc.CreateElement("SystemModuleName");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_MODULE_NAME"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();

                    sSQL = "SELECT SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE WHERE SCHEDULE_TYPE_ID = " + sScheduleType;
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("ScheduleTypeText");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();

                    objTmp = objXmlDoc.CreateElement("ScheduleType");
                    objTmpElement.AppendChild(objTmp);

                    sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        objTmpElement = objXmlDoc.CreateElement("option");
                        objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                        objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                        objTmp.AppendChild(objTmpElement);
                    }
                }
                else
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleTypeText");
                    objTmp.InnerText = "Weekly";
                    objTmpElement.AppendChild(objTmp);
                    objReader.Close();

                    objTmp = objXmlDoc.CreateElement("ScheduleType");
                    objTmpElement.AppendChild(objTmp);

                    sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        objTmpElement = objXmlDoc.CreateElement("option");
                        objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                        objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                        objTmp.AppendChild(objTmpElement);
                    }
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.Edit.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();

                if (objSysSettings != null)
                    objSysSettings = null;
            }
            return objXmlDoc;
        }

        /// <summary>
        /// Gets the xml for editing a Monthly schedule.
        /// </summary>
        /// <param name="p_objXmlin">Contains Schedule id</param>
        /// <returns>Xml containing the details of the schedule</returns>
        public XmlDocument EditMonthly(XmlDocument p_objXmlin)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            DbReader objReader = null;
            DbReader objReader2 = null;
            ArrayList objArrayList = null;
            ScheduleDetails objScheduleDetails;
            XmlElement objTmpArgElement = null;
            XmlDocument objTmpXmlDoc = null;
            //Start averma62 MITS 32386
            LocalCache objCache = null;
            SysSettings objSysSettings = null;
            //End averma62 MITS 32386
            string sConfig = "";
            string sTaskType = "";
            string sScheduleType = "";
            string sSQL = "";
            const string CLAIM_BASED_FIN_HIST = "-zerobasedfinhist zerobasedcrit=claim";
            try
            {
                objArrayList = new ArrayList();
                objXmlDoc = new XmlDocument();
                objScheduleDetails = new ScheduleDetails();
                objTmpElement = objXmlDoc.CreateElement("Details"); //PJS: Now common page for Edit & save so needed common ref                
                objXmlDoc.AppendChild(objTmpElement);
                objCache = new LocalCache(m_sDbConnstring, m_iClientId);//averma62 MITS 32386
                objSysSettings = new SysSettings(m_sDbConnstring, m_iClientId);

                sSQL = "SELECT SCHEDULE_ID, TASK_TYPE_ID, SCHEDULE_TYPE_ID, NEXT_RUN_DTTM, CONFIG, TASK_NAME, OPTIONSET_ID FROM TM_SCHEDULE"
                    + " WHERE SCHEDULE_ID = " + p_objXmlin.SelectSingleNode("//ScheduleId").InnerText;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleId");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("TaskType");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                    sTaskType = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("TaskName");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("TaskNameLabel");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("ScheduleTypeId");
                    objTmp.InnerText = "4";//Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                    sScheduleType = "4";//Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("Month");
                    objTmp.InnerText = Conversion.ToDate(objReader.GetString("NEXT_RUN_DTTM")).Month.ToString();
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("DayOfMonth");
                    objTmp.InnerText = Conversion.ToDate(objReader.GetString("NEXT_RUN_DTTM")).Day.ToString();
                    objTmpElement.AppendChild(objTmp);

                    string sNextRunDttm = objReader.GetString("NEXT_RUN_DTTM");

                    sNextRunDttm = Conversion.ToDate(sNextRunDttm).ToShortDateString() + " " + Conversion.ToDate(sNextRunDttm).ToShortTimeString();

                    objTmp = objXmlDoc.CreateElement("Time");
                    //objTmp.InnerText = Conversion.ToDate(objReader.GetString("NEXT_RUN_DTTM")).ToShortTimeString();
                    objTmp.InnerText = Conversion.ToDbTime(Convert.ToDateTime(sNextRunDttm));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("Arguments");
                    objTmp.InnerText = "";

                    sConfig = objReader.GetString("CONFIG");
                    objTmpXmlDoc = new XmlDocument();
                    objTmpXmlDoc.LoadXml(sConfig);

                    objTmp = objXmlDoc.CreateElement("OptionsetId");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("OPTIONSET_ID"));
                    objTmpElement.AppendChild(objTmp);

                    bool bIsDataIntegratorTask = IsDetailedStatusDisabled(Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId));
                    //changed by nadim
                    sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                    using (objReader2 = DbFactory.GetDbReader(ConnectionString, sSQL))
                    {
                        if (objReader2.Read())
                        {
                            objTmp = objXmlDoc.CreateElement("TaskTypeText");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("NAME"));
                            objTmpElement.AppendChild(objTmp);

                            objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("DISABLE_DETAILED_STATUS"));
                            objTmpElement.AppendChild(objTmp);

                            objTmp = objXmlDoc.CreateElement("SystemModuleName");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("SYSTEM_MODULE_NAME"));
                            objTmpElement.AppendChild(objTmp);
                        }
                    }
                    //objReader2.Close();
                    //changed by nadim
                    switch (Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId))
                    {
                        case TASK_WPA:
                            break;
                        case TASK_BILL:
                            objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");
                            if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                            {
                                objTmp = objXmlDoc.CreateElement("BillingOption");
                                objTmp.InnerText = objTmpArgElement.InnerText;
                                objTmpElement.AppendChild(objTmp);
                            }
                            break;
                        //Start averma62 MITS 32386
                        //case TASK_FAS:
                        //    XmlElement objTmpChild = null;
                        //    DataSet objDS = null;
                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        if (objTmpArgElement.InnerText.Contains("-ny"))
                        //        {
                        //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                        //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        //            {
                        //                string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                        //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                        //                {
                        //                    if (Icount.Equals(0))
                        //                    {
                        //                        objTmp.InnerText = lstClmType[Icount];
                        //                    }
                        //                    else
                        //                    {
                        //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                        //                    }

                        //                }
                        //                objTmpElement.AppendChild(objTmp);

                        //                objTmp = null;
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                        //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                        //                {
                        //                    sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                        //                    objTmpChild = objXmlDoc.CreateElement("option");
                        //                    objTmpChild.SetAttribute("value", lstClmType[Icount]);
                        //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim();
                        //                    objTmp.AppendChild(objTmpChild);
                        //                }
                        //            }
                        //            objTmpElement.AppendChild(objTmp);
                        //        }

                        //    }
                        //    //Start Claim Type Not Related List box
                        //    objTmpChild = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                        //    StringBuilder sbSql = new StringBuilder();
                        //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                        //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                        //    sbSql.Append(" AND CODES.TABLE_ID=1023");
                        //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                        //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                        //    foreach (DataRow dr in objDS.Tables[0].Rows)
                        //    {
                        //        if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                        //        {
                        //            objTmpChild = objXmlDoc.CreateElement("option");
                        //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        //            objTmp.AppendChild(objTmpChild);
                        //        }
                        //    }
                        //    objTmpElement.AppendChild(objTmp);
                        //    //End Claim Type Not Related List box

                        //    objTmpChild = null;
                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {

                        //        if (objTmpArgElement.InnerText.Contains("-ns"))
                        //        {
                        //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                        //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        //            {
                        //                string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "txtRelatedComponents");

                        //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                        //                {
                        //                    if (Icount.Equals(0))
                        //                    {
                        //                        objTmp.InnerText = lstClmStatus[Icount];
                        //                    }
                        //                    else
                        //                    {
                        //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                        //                    }

                        //                }
                        //                objTmpElement.AppendChild(objTmp);
                        //                objTmp = null;
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "lstRelatedLossComponents");

                        //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                        //                {
                        //                    sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                        //                    objTmpChild = objXmlDoc.CreateElement("option");
                        //                    objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                        //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim();
                        //                    objTmp.AppendChild(objTmpChild);
                        //                }
                        //                objTmpElement.AppendChild(objTmp);
                        //            }
                        //        }
                        //    }

                        //    //Start Claim Status Not Related List box
                        //    objTmpChild = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "lstAvailableLossComponents");

                        //    sbSql = new StringBuilder();
                        //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                        //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                        //    sbSql.Append(" AND CODES.TABLE_ID=1002");
                        //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                        //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                        //    foreach (DataRow dr in objDS.Tables[0].Rows)
                        //    {
                        //        if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                        //        {
                        //            objTmpChild = objXmlDoc.CreateElement("option");
                        //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        //            objTmp.AppendChild(objTmpChild);
                        //        }
                        //    }
                        //    objTmpElement.AppendChild(objTmp);
                        //    //End Claim Status Not Related List box

                            
                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASFolder");
                        //    objTmp.InnerXml = objSysSettings.FASFolder;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASServer");
                        //    objTmp.InnerXml = objSysSettings.FASServer;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASPassword");
                        //    objTmp.InnerXml = objSysSettings.FASPassword;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASUserId");
                        //    objTmp.InnerXml = objSysSettings.FASUserId;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("FromDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nf"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("ToDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nt"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("UFromDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nm"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("UToDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nn"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    break;
                        //End averma62 MITS 32386
                        case TASK_FINHIST:
                            objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");

                            if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("zerobasedfinhist"))
                            {
                                objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.ToLower().Contains(CLAIM_BASED_FIN_HIST))
                                {
                                    objTmp = objXmlDoc.CreateElement("ClaimBasedFinHist");
                                    objTmp.InnerText = "1";
                                }
                                else
                                {
                                    objTmp = objXmlDoc.CreateElement("EventBasedFinHist");
                                    objTmp.InnerText = "2";
                                }
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.Contains("-full"))
                                {
                                    objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                if (objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-full"))
                            {
                                objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-log"))
                            {
                                objTmp = objXmlDoc.CreateElement("CreateLog");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);
                            }
                            else
                            {
                                objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                objTmp.InnerText = "";
                                objTmpElement.AppendChild(objTmp);

                                objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                objTmp.InnerText = "";
                                objTmpElement.AppendChild(objTmp);
                            }

                            break;
                        default:
                            if (bIsDataIntegratorTask && objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE || objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    XmlNodeList objList = objTmpXmlDoc.SelectNodes("//arg");
                                    XmlDocument objdoc = GetPolicySystems();
                                    // XmlNode objPolicyListNode = null;
                                    //  objPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                    XmlNode objOldPolicyListNode = objXmlDoc.SelectSingleNode("//PolicySystemList");
                                    XmlNode objNewPolicyListNode = objXmlDoc.CreateElement("PolicySystemList");
                                    objNewPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                    if (objOldPolicyListNode != null)
                                        objXmlDoc.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                                    else
                                        objXmlDoc.DocumentElement.AppendChild(objNewPolicyListNode);

                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForEditCurrencyExchangeInterface(objXmlDoc, objTmpElement, objTmp, objTmpXmlDoc);
                                }
                                //JIRA RMA-4606 nshah28 End
                            }
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                            {
                                XmlElement objTmpChild = null;
                                DataSet objDS = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    if (objTmpArgElement.InnerText.Contains("-ny"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmType[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);

                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmType[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                        }
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                }
                                //Start Claim Type Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                                StringBuilder sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1023");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Type Not Related List box

                                objTmpChild = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {

                                    if (objTmpArgElement.InnerText.Contains("-ns"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmStatus[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);
                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedLossComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                            objTmpElement.AppendChild(objTmp);
                                        }
                                    }
                                }

                                //Start Claim Status Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableLossComponents");

                                sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1002");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Status Not Related List box


                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASFolder");
                                objTmp.InnerXml = objSysSettings.FASFolder;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASServer");
                                objTmp.InnerXml = objSysSettings.FASServer;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASPassword");
                                objTmp.InnerXml = objSysSettings.FASPassword;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASUserId");
                                objTmp.InnerXml = objSysSettings.FASUserId;
                                objTmpElement.AppendChild(objTmp);

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("FromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nf"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("ToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nt"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UFromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nm"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nn"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            //MITS: 26428
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                XmlElement objTmpChild = null;
                                DataSet objDS = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    if (objTmpArgElement.InnerText.Contains("-ny"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmType[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);

                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmType[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                        }
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                }
                                //Start Claim Type Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                                StringBuilder sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1023");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Type Not Related List box

                                objTmpChild = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {

                                    if (objTmpArgElement.InnerText.Contains("-ns"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmStatus[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);
                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedLossComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                            objTmpElement.AppendChild(objTmp);
                                        }
                                    }
                                }

                                //Start Claim Status Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableLossComponents");

                                sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1002");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Status Not Related List box


                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASFolder");
                                objTmp.InnerXml = objSysSettings.FASFolder;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASServer");
                                objTmp.InnerXml = objSysSettings.FASServer;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASPassword");
                                objTmp.InnerXml = objSysSettings.FASPassword;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASUserId");
                                objTmp.InnerXml = objSysSettings.FASUserId;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASFileLocation");
                                objTmp.InnerXml = objSysSettings.FASFileLocation;
                                objTmpElement.AppendChild(objTmp);


                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("FromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nf"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("ToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nt"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UFromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nm"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nn"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                            }
                             // akaushik5 Added for MITS 36381 Starts
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                            {
                                this.LoadResbalOptions(objXmlDoc, objTmpElement, objTmpXmlDoc);
                            }
                            // akaushik5 Added for MITS 36381 Ends
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//DocumentElement");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("DocumentElement");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            //changed by nadim
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES)
                            {
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user' and @role='admin']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("AdminLogin");
                                    objTmp.InnerText = objTmpArgElement.InnerText;
                                    if(objTmp.InnerText.Contains("-au"))
                                        objTmp.InnerText=objTmp.InnerText.Remove(0,3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='password' and @role='admin']");

                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("AdminPassword");
                                    objTmp.InnerText = RMCryptography.DecryptString(objTmpArgElement.InnerText);
                                    if(objTmp.InnerText.Contains("-ap"))
                                        objTmp.InnerText=objTmp.InnerText.Remove(0,3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }

                                //changed by nadim
                            else
                            {
                                objTmp = objXmlDoc.CreateElement("Arguments");
                                objTmp.InnerText = "";

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//Args");
                                if (objTmpArgElement != null)
                                {
                                    foreach (XmlNode objTmpNode in objTmpArgElement.ChildNodes)
                                    {
                                        objTmp.InnerText += objTmpNode.InnerText + " ";
                                    }
                                    objTmpElement.AppendChild(objTmp);
                                    objTmp = objXmlDoc.CreateElement("bParams");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                else
                                {
                                    objTmpElement.AppendChild(objTmp);
                                    objTmp = objXmlDoc.CreateElement("bParams");
                                    objTmp.InnerText = "False";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            break;
                    }

                }
                objReader.Close();
                sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("TaskTypeText");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("NAME"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DISABLE_DETAILED_STATUS"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("SystemModuleName");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_MODULE_NAME"));
                    objTmpElement.AppendChild(objTmp);
                }
                objReader.Close();

                sSQL = "SELECT SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE WHERE SCHEDULE_TYPE_ID = " + sScheduleType;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleTypeText");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE"));
                    objTmpElement.AppendChild(objTmp);
                }
                objReader.Close();

                objTmp = objXmlDoc.CreateElement("ScheduleType");
                objTmpElement.AppendChild(objTmp);

                sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                    objTmp.AppendChild(objTmpElement);
                }
                objReader.Close();

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.Edit.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objSysSettings != null)
                    objSysSettings = null;
            }
            return objXmlDoc;
        }

        /// <summary>
        /// Gets the xml for editing a Periodical schedule.
        /// </summary>
        /// <param name="p_objXmlin">Contains Schedule id</param>
        /// <returns>Xml containing the details of the schedule</returns>
        public XmlDocument EditPeriodical(XmlDocument p_objXmlin)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            DbReader objReader = null;
            DbReader objReader2 = null;
            ArrayList objArrayList = null;
            ScheduleDetails objScheduleDetails;
            XmlElement objTmpArgElement = null;
            XmlDocument objTmpXmlDoc = null;
            LocalCache objCache = null;//averma62 MITS 32386
            SysSettings objSysSettings = null;
            string sConfig = "";
            string sTaskType = "";
            string sScheduleType = "";
            string sTaskName = string.Empty;
            string sSubTaskName = "None";
            int iSubTaskValue = 0;
            string sSQL = "";
            const string CLAIM_BASED_FIN_HIST = "-zerobasedfinhist zerobasedcrit=claim";
            string sLangCode = string.Empty;
            try
            {
                objArrayList = new ArrayList();
                objXmlDoc = new XmlDocument();
                objScheduleDetails = new ScheduleDetails();
                objTmpElement = objXmlDoc.CreateElement("Details"); //PJS: Now common page for Edit & save so needed common ref
                objXmlDoc.AppendChild(objTmpElement);
                objCache = new LocalCache(m_sDbConnstring, m_iClientId);//averma62 MITS 32386
                objSysSettings = new SysSettings(m_sDbConnstring, m_iClientId);
                sLangCode = p_objXmlin.SelectSingleNode("//LangCode").InnerText;
                sSQL = "SELECT SCHEDULE_ID, TASK_TYPE_ID, SCHEDULE_TYPE_ID, NEXT_RUN_DTTM, INTERVAL_TYPE_ID, INTERVAL, CONFIG, TASK_NAME, OPTIONSET_ID FROM TM_SCHEDULE"
                    + " WHERE SCHEDULE_ID = " + p_objXmlin.SelectSingleNode("//ScheduleId").InnerText;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleId");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("TaskType");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                    sTaskType = Conversion.ConvertObjToStr(objReader.GetValue("TASK_TYPE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    //seperate Task Nmae and SubTask name.
                    sTaskName = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));

                    //if (sTaskName.Contains("-"))
                    //{
                    //    string[] sTask = sTaskName.Split('-');
                    //    sTaskName = sTask[0];
                    //    sSubTaskName = sTask[1];
                    //    switch (sSubTaskName.Trim())
                    //    {
                    //        case "Setup and Populate Data":
                    //            iSubTaskValue = 1;
                    //            break;
                    //        case "Purge Data":
                    //            iSubTaskValue = 2;
                    //            break;
                    //        case "Setup,Populate and Purge":
                    //            iSubTaskValue = 3;
                    //            break;
                    //    }
                    //}
                    
                    iSubTaskValue = this.GetSubTaskId(ref sTaskName, ref sSubTaskName);

                    objTmp = objXmlDoc.CreateElement("TaskName");
                    //objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                    objTmp.InnerText = sTaskName;
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("TaskNameLabel");
                   // objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                    objTmp.InnerText = sTaskName;
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("SubTaskNameLabel");
                    objTmp.InnerText = sSubTaskName;
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("SubTaskValue");
                    objTmp.InnerText = iSubTaskValue.ToString();
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("ScheduleTypeId");
                    objTmp.InnerText = "2";//Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                    sScheduleType = "2";//Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("Date");
                    objTmp.InnerText = Conversion.GetUIDate(objReader.GetString("NEXT_RUN_DTTM"), sLangCode, m_iClientId);
                    objTmpElement.AppendChild(objTmp);

                    string sNextRunDttm = objReader.GetString("NEXT_RUN_DTTM");

                    sNextRunDttm = Conversion.ToDate(sNextRunDttm).ToShortDateString() + " " + Conversion.ToDate(sNextRunDttm).ToShortTimeString();

                    objTmp = objXmlDoc.CreateElement("Time");
                    //objTmp.InnerText = Conversion.ToDate(objReader.GetString("NEXT_RUN_DTTM")).ToShortTimeString();
                    objTmp.InnerText = Conversion.ToDbTime(Convert.ToDateTime(sNextRunDttm));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("IntervalTypeId");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("INTERVAL_TYPE_ID"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("Interval");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("INTERVAL"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("Arguments");
                    objTmp.InnerText = "";

                    sConfig = objReader.GetString("CONFIG");
                    objTmpXmlDoc = new XmlDocument();
                    objTmpXmlDoc.LoadXml(sConfig);

                    objTmp = objXmlDoc.CreateElement("OptionsetId");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("OPTIONSET_ID"));
                    objTmpElement.AppendChild(objTmp);

                    bool bIsDataIntegratorTask = IsDetailedStatusDisabled(Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId));
                    //changed by nadim
                    sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                    using (objReader2 = DbFactory.GetDbReader(ConnectionString, sSQL))
                    {
                        if (objReader2.Read())
                        {
                            objTmp = objXmlDoc.CreateElement("TaskTypeText");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("NAME"));
                            objTmpElement.AppendChild(objTmp);

                            objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("DISABLE_DETAILED_STATUS"));
                            objTmpElement.AppendChild(objTmp);

                            objTmp = objXmlDoc.CreateElement("SystemModuleName");
                            objTmp.InnerText = Conversion.ConvertObjToStr(objReader2.GetValue("SYSTEM_MODULE_NAME"));
                            objTmpElement.AppendChild(objTmp);
                        }
                    }
                    //objReader2.Close();
                    //changed by nadim
                    switch (Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId))
                    {
                        case TASK_WPA:
                            break;

                        case TASK_BILL:
                            objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");
                            if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                            {
                                objTmp = objXmlDoc.CreateElement("BillingOption");
                                objTmp.InnerText = objTmpArgElement.InnerText;
                                objTmpElement.AppendChild(objTmp);
                            }
                            break;
                        //Start averma62 MITS 32386
                        //case TASK_FAS:
                        //    XmlElement objTmpChild = null;
                        //    DataSet objDS = null;
                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        if (objTmpArgElement.InnerText.Contains("-ny"))
                        //        {
                        //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                        //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        //            {
                        //                string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                        //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                        //                {
                        //                    if (Icount.Equals(0))
                        //                    {
                        //                        objTmp.InnerText = lstClmType[Icount];
                        //                    }
                        //                    else
                        //                    {
                        //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                        //                    }

                        //                }
                        //                objTmpElement.AppendChild(objTmp);

                        //                objTmp = null;
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                        //                for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                        //                {
                        //                    sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                        //                    objTmpChild = objXmlDoc.CreateElement("option");
                        //                    objTmpChild.SetAttribute("value", lstClmType[Icount]);
                        //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount])).Trim();
                        //                    objTmp.AppendChild(objTmpChild);
                        //                }
                        //            }
                        //            objTmpElement.AppendChild(objTmp);
                        //        }

                        //    }
                        //    //Start Claim Type Not Related List box
                        //    objTmpChild = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                        //    StringBuilder sbSql = new StringBuilder();
                        //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                        //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                        //    sbSql.Append(" AND CODES.TABLE_ID=1023");
                        //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                        //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                        //    foreach (DataRow dr in objDS.Tables[0].Rows)
                        //    {
                        //        if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                        //        {
                        //            objTmpChild = objXmlDoc.CreateElement("option");
                        //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        //            objTmp.AppendChild(objTmpChild);
                        //        }
                        //    }
                        //    objTmpElement.AppendChild(objTmp);
                        //    //End Claim Type Not Related List box

                        //    objTmpChild = null;
                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {

                        //        if (objTmpArgElement.InnerText.Contains("-ns"))
                        //        {
                        //            objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                        //            if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        //            {
                        //                string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "txtRelatedComponents");

                        //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                        //                {
                        //                    if (Icount.Equals(0))
                        //                    {
                        //                        objTmp.InnerText = lstClmStatus[Icount];
                        //                    }
                        //                    else
                        //                    {
                        //                        objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                        //                    }

                        //                }
                        //                objTmpElement.AppendChild(objTmp);
                        //                objTmp = null;
                        //                objTmp = objXmlDoc.CreateElement("control");
                        //                objTmp.SetAttribute("name", "lstRelatedLossComponents");

                        //                for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                        //                {
                        //                    sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                        //                    objTmpChild = objXmlDoc.CreateElement("option");
                        //                    objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                        //                    objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount])).Trim();
                        //                    objTmp.AppendChild(objTmpChild);
                        //                }
                        //                objTmpElement.AppendChild(objTmp);
                        //            }
                        //        }
                        //    }

                        //    //Start Claim Status Not Related List box
                        //    objTmpChild = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "lstAvailableLossComponents");

                        //    sbSql = new StringBuilder();
                        //    sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                        //    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                        //    sbSql.Append(" AND CODES.TABLE_ID=1002");
                        //    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                        //    objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString());
                        //    foreach (DataRow dr in objDS.Tables[0].Rows)
                        //    {
                        //        if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                        //        {
                        //            objTmpChild = objXmlDoc.CreateElement("option");
                        //            objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        //            objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                        //                + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        //            objTmp.AppendChild(objTmpChild);
                        //        }
                        //    }
                        //    objTmpElement.AppendChild(objTmp);
                        //    //End Claim Status Not Related List box
                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASFolder");
                        //    objTmp.InnerXml = objSysSettings.FASFolder;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASServer");
                        //    objTmp.InnerXml = objSysSettings.FASServer;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASPassword");
                        //    objTmp.InnerXml = objSysSettings.FASPassword;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmp = null;
                        //    objTmp = objXmlDoc.CreateElement("control");
                        //    objTmp.SetAttribute("name", "FASUserId");
                        //    objTmp.InnerXml = objSysSettings.FASUserId;
                        //    objTmpElement.AppendChild(objTmp);

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("FromDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nf"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("ToDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nt"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("UFromDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nm"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                        //    if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                        //    {
                        //        objTmp = objXmlDoc.CreateElement("UToDate");
                        //        objTmp.InnerXml = objTmpArgElement.InnerXml;
                        //        if (objTmp.InnerText.Contains("-nn"))
                        //            objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                        //        objTmpElement.AppendChild(objTmp);
                        //    }

                        //    break;
                        //End averma62 MITS 32386
                        case TASK_FINHIST:
                            objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user']");

                            if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("zerobasedfinhist"))
                            {
                                objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.ToLower().Contains(CLAIM_BASED_FIN_HIST))
                                {
                                    objTmp = objXmlDoc.CreateElement("ClaimBasedFinHist");
                                    objTmp.InnerText = "1";
                                }
                                else
                                {
                                    objTmp = objXmlDoc.CreateElement("EventBasedFinHist");
                                    objTmp.InnerText = "2";
                                }
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.Contains("-full"))
                                {
                                    objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                if (objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-full"))
                            {
                                objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);

                                if (objTmpArgElement.InnerText.Contains("-log"))
                                {
                                    objTmp = objXmlDoc.CreateElement("CreateLog");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            else if (objTmpArgElement != null && objTmpArgElement.InnerText.Contains("-log"))
                            {
                                objTmp = objXmlDoc.CreateElement("CreateLog");
                                objTmp.InnerText = "True";
                                objTmpElement.AppendChild(objTmp);
                            }
                            else
                            {
                                objTmp = objXmlDoc.CreateElement("ZeroBasedFinHist");
                                objTmp.InnerText = "";
                                objTmpElement.AppendChild(objTmp);

                                objTmp = objXmlDoc.CreateElement("RecreateFinHist");
                                objTmp.InnerText = "";
                                objTmpElement.AppendChild(objTmp);
                            }

                            break;
                        default:
                            if (bIsDataIntegratorTask && objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText != TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_POLICY_SYSTEM_UPDATE || objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CLAIM_BALANCING)
                                {
                                    XmlNodeList objList = objTmpXmlDoc.SelectNodes("//arg");
                                    XmlDocument objdoc = GetPolicySystems();
                                    // XmlNode objPolicyListNode = null;
                                    //  objPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                    XmlNode objOldPolicyListNode = objXmlDoc.SelectSingleNode("//PolicySystemList");
                                    XmlNode objNewPolicyListNode = objXmlDoc.CreateElement("PolicySystemList");
                                    objNewPolicyListNode.AppendChild(objXmlDoc.ImportNode(objdoc.SelectSingleNode("//PolicySystems"), true));
                                    if (objOldPolicyListNode != null)
                                        objXmlDoc.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                                    else
                                        objXmlDoc.DocumentElement.AppendChild(objNewPolicyListNode);

                                }
                                //JIRA RMA-4606 nshah28 start
                                else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_CURRENCY_EXCHANGE_INTERFACE)
                                {
                                    SettingsForEditCurrencyExchangeInterface(objXmlDoc, objTmpElement, objTmp, objTmpXmlDoc);
                                }
                                //JIRA RMA-4606 nshah28 End
                            }
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_FAS)
                            {
                                XmlElement objTmpChild = null;
                                DataSet objDS = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimTypeCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    if (objTmpArgElement.InnerText.Contains("-ny"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmType = objTmpArgElement.InnerText.Split(',');

                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmType[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmType[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);

                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedCTypeComponents");

                                            for (int Icount = 0; Icount < lstClmType.Length; Icount++)
                                            {
                                                sClmTypeLst = sClmTypeLst + "," + lstClmType[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmType[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmType[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                        }
                                        objTmpElement.AppendChild(objTmp);
                                    }

                                }
                                //Start Claim Type Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableCTypeComponents");

                                StringBuilder sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1023");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmTypeLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Type Not Related List box

                                objTmpChild = null;
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimStatusCode']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {

                                    if (objTmpArgElement.InnerText.Contains("-ns"))
                                    {
                                        objTmpArgElement.InnerXml = objTmpArgElement.InnerXml.Remove(0, 3);
                                        if (!string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                                        {
                                            string[] lstClmStatus = objTmpArgElement.InnerText.Split(',');
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "txtRelatedComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                if (Icount.Equals(0))
                                                {
                                                    objTmp.InnerText = lstClmStatus[Icount];
                                                }
                                                else
                                                {
                                                    objTmp.InnerText = objTmp.InnerText + "," + lstClmStatus[Icount];
                                                }

                                            }
                                            objTmpElement.AppendChild(objTmp);
                                            objTmp = null;
                                            objTmp = objXmlDoc.CreateElement("control");
                                            objTmp.SetAttribute("name", "lstRelatedLossComponents");

                                            for (int Icount = 0; Icount < lstClmStatus.Length; Icount++)
                                            {
                                                sClmStatusLst = sClmStatusLst + "," + lstClmStatus[Icount] + ",";
                                                objTmpChild = objXmlDoc.CreateElement("option");
                                                objTmpChild.SetAttribute("value", lstClmStatus[Icount]);
                                                objTmpChild.InnerText = objCache.GetShortCode(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim() + " - " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(lstClmStatus[Icount], m_iClientId)).Trim();
                                                objTmp.AppendChild(objTmpChild);
                                            }
                                            objTmpElement.AppendChild(objTmp);
                                        }
                                    }
                                }

                                //Start Claim Status Not Related List box
                                objTmpChild = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "lstAvailableLossComponents");

                                sbSql = new StringBuilder();
                                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                                sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                                sbSql.Append(" AND CODES.TABLE_ID=1002");
                                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");

                                objDS = DbFactory.GetDataSet(m_sDbConnstring, sbSql.ToString(), m_iClientId);
                                foreach (DataRow dr in objDS.Tables[0].Rows)
                                {
                                    if (!sClmStatusLst.Contains("," + Conversion.ConvertObjToStr(dr["CODE_ID"]) + ","))
                                    {
                                        objTmpChild = objXmlDoc.CreateElement("option");
                                        objTmpChild.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                        objTmpChild.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                        objTmp.AppendChild(objTmpChild);
                                    }
                                }
                                objTmpElement.AppendChild(objTmp);
                                //End Claim Status Not Related List box
                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASFolder");
                                objTmp.InnerXml = objSysSettings.FASFolder;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASServer");
                                objTmp.InnerXml = objSysSettings.FASServer;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASPassword");
                                objTmp.InnerXml = objSysSettings.FASPassword;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASUserId");
                                objTmp.InnerXml = objSysSettings.FASUserId;
                                objTmpElement.AppendChild(objTmp);

                                objTmp = null;
                                objTmp = objXmlDoc.CreateElement("control");
                                objTmp.SetAttribute("name", "FASFileLocation");
                                objTmp.InnerXml = objSysSettings.FASFileLocation;
                                objTmpElement.AppendChild(objTmp);



                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("FromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nf"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("ToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nt"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UFromDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UFromDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nm"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='UToDate']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("UToDate");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    if (objTmp.InnerText.Contains("-nn"))
                                        objTmp.InnerXml = objTmp.InnerXml.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                                // akaushik5 Added for MITS 36381 Starts
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_RESERVE_BALANCE)
                            {
                                this.LoadResbalOptions(objXmlDoc, objTmpElement, objTmpXmlDoc);
                            }
                            // akaushik5 Added for MITS 36381 Ends
                            //MITS: 26428
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_PRINT_SCH_CHECK_BATCH)
                            {
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//DocumentElement");
                                if (objTmpArgElement != null && objTmpArgElement.InnerXml != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("DocumentElement");
                                    objTmp.InnerXml = objTmpArgElement.InnerXml;
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            //changed by nadim-start
                            else if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_BES || objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)//Mgaba2:R7:Adding Case for History Tracking
                            {
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='user' and @role='admin']");
                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("AdminLogin");
                                    objTmp.InnerText = objTmpArgElement.InnerText;
                                    if (objTmp.InnerText.Contains("-au"))
                                        objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='password' and @role='admin']");

                                if (objTmpArgElement != null && objTmpArgElement.InnerText != string.Empty)
                                {
                                    objTmp = objXmlDoc.CreateElement("AdminPassword");
                                    objTmp.InnerText = RMCryptography.DecryptString(objTmpArgElement.InnerText);
                                    if (objTmp.InnerText.Contains("-ap"))
                                        objTmp.InnerText = objTmp.InnerText.Remove(0, 3);
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }

                                //changed by nadim-end
                            else
                            {
                                objTmp = objXmlDoc.CreateElement("Arguments");
                                objTmp.InnerText = "";

                                objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//Args");
                                if (objTmpArgElement != null)
                                {
                                    foreach (XmlNode objTmpNode in objTmpArgElement.ChildNodes)
                                    {
                                        objTmp.InnerText += objTmpNode.InnerText + " ";
                                    }
                                    objTmpElement.AppendChild(objTmp);
                                    objTmp = objXmlDoc.CreateElement("bParams");
                                    objTmp.InnerText = "True";
                                    objTmpElement.AppendChild(objTmp);
                                }
                                else
                                {
                                    objTmpElement.AppendChild(objTmp);
                                    objTmp = objXmlDoc.CreateElement("bParams");
                                    objTmp.InnerText = "False";
                                    objTmpElement.AppendChild(objTmp);
                                }
                            }
                            break;
                    }

                }
                objReader.Close();
                sSQL = "SELECT NAME,DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + sTaskType;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("TaskTypeText");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("NAME"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("IsDataIntegratorTask");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DISABLE_DETAILED_STATUS"));
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("SystemModuleName");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_MODULE_NAME"));
                    objTmpElement.AppendChild(objTmp);
                }
                objReader.Close();

                sSQL = "SELECT SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE WHERE SCHEDULE_TYPE_ID = " + sScheduleType;
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTmp = objXmlDoc.CreateElement("ScheduleTypeText");
                    objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE"));
                    objTmpElement.AppendChild(objTmp);
                }
                objReader.Close();

                objTmp = objXmlDoc.CreateElement("ScheduleType");
                objTmpElement.AppendChild(objTmp);

                sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                    objTmp.AppendChild(objTmpElement);
                }
                objReader.Close();

                objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Details");
                objTmp = objXmlDoc.CreateElement("IntervalType");
                objTmpElement.AppendChild(objTmp);

                sSQL = "SELECT INTERVAL_TYPE_ID, INTERVAL_TYPE FROM TM_SCHEDULE_INTERVAL";
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                    objTmp.AppendChild(objTmpElement);
                }
                objReader.Close();
                //MGaba2:R7:In Case of History Tracking,Maximum limit for interval is fetched from web.config
                if (objXmlDoc.SelectSingleNode("//SystemModuleName").InnerText == TASK_HIST_TRACK)
                {
                    objXmlDoc = GetMaxIntervalLimit(objXmlDoc);
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.Edit.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objSysSettings != null)
                    objSysSettings = null;
            }
            return objXmlDoc;
        }

        //JIRA RMA-4606 nshah28 start
        /// <summary>
        /// This function takes argument from in XML and generate XML for Save
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <param name="objXmlDoc"></param>
        /// <param name="objTmpElement"></param>
        /// <param name="objArgElement"></param>
        private void SettingsForSaveCurrencyExchangeInterface(XmlDocument p_objXmlDoc, XmlDocument objXmlDoc, ref XmlElement objTmpElement, ref XmlElement objArgElement)
        {
            string sDefaultPort = "21";

            if (objXmlDoc.SelectSingleNode("//Args") != null)
            {
                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
            }
            objTmpElement = objXmlDoc.CreateElement("Args");
            objXmlDoc.DocumentElement.AppendChild(objTmpElement);


            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.InnerText = "-ds" + m_sDataSourceName;
            objTmpElement.AppendChild(objArgElement);

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.InnerText = "-ru" + m_sLoginName;
            objTmpElement.AppendChild(objArgElement);

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.SetAttribute("type", "password");
            m_sPassword = "-rp" + RMX_TM_PWD;
            objArgElement.InnerText = RMCryptography.EncryptString(m_sPassword);
            objTmpElement.AppendChild(objArgElement);

            //For Cloud-Start
            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.SetAttribute("type", "clientid");
            objArgElement.InnerText = "-ci" + Convert.ToString(m_iClientId);
            objTmpElement.AppendChild(objArgElement);
            //For Cloud-End

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.SetAttribute("type", "currencyexchange");
            objArgElement.InnerText = "-j jobid";
            objTmpElement.AppendChild(objArgElement);

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.SetAttribute("type", "filesource");
            string sFileSource = "";
            if (p_objXmlDoc.SelectSingleNode("//FileSource") != null)
            {
                sFileSource = p_objXmlDoc.SelectSingleNode("//FileSource").InnerText;
                objArgElement.InnerText = sFileSource;
            }
            objTmpElement.AppendChild(objArgElement);

            //New argument for file path and File name
            if (sFileSource == "1")
            {
                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "filepath");
                if (p_objXmlDoc.SelectSingleNode("//FilePath") != null)
                {
                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//FilePath").InnerText;
                }
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "filename");
                if (p_objXmlDoc.SelectSingleNode("//FileName") != null)
                {
                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//FileName").InnerText;
                }
                objTmpElement.AppendChild(objArgElement);
            }

            //New argument for FTP Server Name, Port, uname and pass
            if (sFileSource == "2")
            {
                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "ftpservername");
                if (p_objXmlDoc.SelectSingleNode("//FTPServerName") != null)
                {
                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//FTPServerName").InnerText;
                }
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "ftpserverport");
                
                if (p_objXmlDoc.SelectSingleNode("//FTPPort") != null)
                {
                    objArgElement.InnerText = string.IsNullOrEmpty(p_objXmlDoc.SelectSingleNode("//FTPPort").InnerText) ? sDefaultPort : p_objXmlDoc.SelectSingleNode("//FTPPort").InnerText;
                }
                else
                {
                    objArgElement.InnerText = sDefaultPort;
                }
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "ftpserverusername");
                if (p_objXmlDoc.SelectSingleNode("//FTPUserName") != null)
                {
                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//FTPUserName").InnerText;
                }
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "ftpserverpassword");
                if (p_objXmlDoc.SelectSingleNode("//FTPPassword") != null)
                {
                    objArgElement.InnerText = RMCryptography.EncryptString(p_objXmlDoc.SelectSingleNode("//FTPPassword").InnerText);
                }
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "ftpfilepath");
                if (p_objXmlDoc.SelectSingleNode("//FTPFilePath") != null)
                {
                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//FTPFilePath").InnerText;
                }
                objTmpElement.AppendChild(objArgElement);

                objArgElement = objXmlDoc.CreateElement("arg");
                objArgElement.SetAttribute("type", "ftpfilename");
                if (p_objXmlDoc.SelectSingleNode("//FTPFileName") != null)
                {
                    objArgElement.InnerText = p_objXmlDoc.SelectSingleNode("//FTPFileName").InnerText;
                }
                objTmpElement.AppendChild(objArgElement);
            }

        }

        /// <summary>
        /// This function takes argument from in XML and generate XML for to show on UI
        /// </summary>
        /// <param name="objXmlDoc"></param>
        /// <param name="objTmpElement"></param>
        /// <param name="objTmp"></param>
        /// <param name="objTmpXmlDoc"></param>
        /// <returns></returns>
        private void SettingsForEditCurrencyExchangeInterface(XmlDocument objXmlDoc, XmlElement objTmpElement, XmlElement objTmp, XmlDocument objTmpXmlDoc)
        {
            XmlNodeList objXmlNodeList = objTmpXmlDoc.SelectNodes("//arg");

            if (objXmlNodeList != null)
            {
                foreach (XmlNode objXmlNode in objXmlNodeList)
                {
                    if (objXmlNode.Attributes["type"] != null)
                    {
                        switch (objXmlNode.Attributes["type"].Value.ToString())
                        {
                            case "filesource":
                                objTmp = objXmlDoc.CreateElement("FileSource");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            case "filepath":
                                objTmp = objXmlDoc.CreateElement("FilePath");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            case "filename":
                                objTmp = objXmlDoc.CreateElement("FileName");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            //For FTP
                            case "ftpservername":
                                objTmp = objXmlDoc.CreateElement("FTPServerName");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            case "ftpserverport":
                                objTmp = objXmlDoc.CreateElement("FTPPort");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            case "ftpserverusername":
                                objTmp = objXmlDoc.CreateElement("FTPUserName");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            case "ftpserverpassword":
                                objTmp = objXmlDoc.CreateElement("FTPPassword");
                                if(!string.IsNullOrEmpty(objXmlNode.InnerText))
                                {
                                objTmp.InnerText = RMCryptography.DecryptString(objXmlNode.InnerText);
                                }
                                objTmpElement.AppendChild(objTmp);
                                break;
                            case "ftpfilepath":
                                objTmp = objXmlDoc.CreateElement("FTPFilePath");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            case "ftpfilename":
                                objTmp = objXmlDoc.CreateElement("FTPFileName");
                                objTmp.InnerText = objXmlNode.InnerText;
                                objTmpElement.AppendChild(objTmp);
                                break;
                            default:
                                break;
                        }

                    }
                }
            }

        }
        //JIRA RMA-4606 nshah28 end
        /// <summary>
        /// Deletes the selected schedule.
        /// </summary>
        /// <param name="p_objXmlin">contains Schedule id</param>
        public void DeleteSchedule(XmlDocument p_objXmlin)
        {
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string sSQL = "";
            try
            {
                objConn = DbFactory.GetDbConnection(ConnectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                //smahajan6 - 06/22/2010 - MITS# 21191 : Start
                DeleteDAStagingDatabaseEntries(p_objXmlin.SelectSingleNode("//ScheduleId").InnerText, objCommand);
                //smahajan6 - 06/22/2010 - MITS# 21191 : End

                sSQL = "DELETE FROM TM_SCHEDULE WHERE SCHEDULE_ID = " + p_objXmlin.SelectSingleNode("//ScheduleId").InnerText;
                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.DeleteSchedule.Error", m_iClientId), objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }

        //sanoopsharma - Delete task schedule 
        /// <summary>
        /// Deletes scheduled task.
        /// </summary>
        /// <param name="Task Name"></param>
        /// <param name="Policy System ID"></param>
        /// <param name="Interval Type"></param>
        /// <param name="interval"></param>
        /// <param name="Data Source Nanme"</param>
        /// <returns></returns>

        public void DeleteSchedule(string TaskName, string PolicySystemID, string intervaltype, string interval, string DSN)
        {
            string sSQL = string.Empty;
            string sConfig = string.Empty;
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            DbConnection objConn = null;
            eDatabaseType m_sDatabaseType = 0;

            objConn = DbFactory.GetDbConnection(m_sDbConnstring);
            objConn.Open();
            m_sDatabaseType = objConn.DatabaseType;
            DbReader objReader;
            
            try
            {
                if (m_sDatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    sSQL = string.Format("DELETE FROM TM_SCHEDULE WHERE TASK_NAME = {0} AND INTERVAL_TYPE_ID = {1} AND INTERVAL <= {2} AND TM_DSN = {3}" +
                       " AND cast(config as XML).value('(/Task/Args/arg)[3]','VARCHAR(100)')=  {4}"
                       , "~TaskName~", "~INTERVAL_TYPE_ID~", "~INTERVAL~", "~DSN~", "~POLICYSYSTEMID~");
                    dictParams.Add("TaskName", TaskName);
                    dictParams.Add("INTERVAL_TYPE_ID", intervaltype);
                    dictParams.Add("INTERVAL", interval);
                    dictParams.Add("DSN", DSN);
                    dictParams.Add("POLICYSYSTEMID", PolicySystemID);

                    DbFactory.ExecuteNonQuery(ConnectionString, sSQL, dictParams);
                }
                else if(m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sSQL = string.Format("DELETE FROM TM_SCHEDULE WHERE TASK_NAME = {0} AND INTERVAL_TYPE_ID = {1}"
                   + " AND INTERVAL <= {2} AND TM_DSN = {3}"
                   + " AND extractvalue(xmltype(config), '(/Task/Args/arg)[3]') = {4}"
                   , "~TaskName~", "~INTERVAL_TYPE_ID~", "~INTERVAL~", "~DSN~", "~POLICYSYSTEMID~");

                    dictParams.Add("TaskName", TaskName);
                    dictParams.Add("INTERVAL_TYPE_ID", intervaltype);
                    dictParams.Add("INTERVAL", interval);
                    dictParams.Add("DSN", DSN);
                    dictParams.Add("POLICYSYSTEMID", PolicySystemID);

                    DbFactory.ExecuteNonQuery(ConnectionString, sSQL, dictParams);
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.DeleteSchedule.Error", m_iClientId), objException);
            }
        }
        //sanoopsharma end 
        //smahajan6 - 06/22/2010 - MITS# 21191 : Start
        private void DeleteDAStagingDatabaseEntries(string sScheduleId, DbCommand objCommand)
        {
            List<string> TableNames = new List<string>();
            int iOptionSetID = 0;
            string sSQL = string.Empty;
            string sAccessStagingDataSource = string.Empty;

            try
            {
                sAccessStagingDataSource = RMConfigurationManager.GetConfigConnectionString("AccessStagingDataSource", m_iClientId);//rkaur27

                sSQL = "SELECT OPTIONSET_ID FROM TM_SCHEDULE WHERE (TASK_TYPE_ID = (SELECT TASK_TYPE_ID FROM TM_TASK_TYPE WHERE NAME = 'DIS' ))"
                       + " AND SCHEDULE_ID = " + sScheduleId;

                objCommand.CommandText = sSQL;

                iOptionSetID = Conversion.ConvertStrToInteger(objCommand.ExecuteScalar().ToString());

                if (iOptionSetID > 0)
                {
                    sSQL = "SELECT NAME FROM SYS.TABLES";

                    using (DbReader objDbreader = DbFactory.ExecuteReader(sAccessStagingDataSource, sSQL))
                    {
                        while (objDbreader.Read())
                        {
                            sSQL = string.Format(@"DELETE FROM [{0}] WHERE OPTIONSET_ID = {1}", objDbreader.GetString("NAME"), iOptionSetID);
                            try
                            {
                                DbFactory.ExecuteNonQuery(sAccessStagingDataSource, sSQL);
                            }
                            catch (Exception ex) { }
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }
        //smahajan6 - 06/22/2010 - MITS# 21191 : Start

        /// <summary>
        /// This method gets the available task types and schedule types from db.
        /// </summary>
        /// <returns>The xml containing task type and schedule type options.</returns>
        public XmlDocument GetDetails(string sPageId)
        {
            XmlDocument objXmlDoc=null;
			XmlElement objTmpElement=null;
            XmlElement objTmp = null;
			DbReader objReader=null;
            DbReader objRdr = null; //33046
			string sSQL="";
            string sDisableStatusFlag = string.Empty;
            string sSystemModuleName = string.Empty;
            ArrayList objRemoveJobsList = null; //This arraylist will contain Job names that should not be displayed in Task Type dropdown
            SysSettings objsettings = null;
            try
            {
                objXmlDoc = new XmlDocument();
                objTmpElement = objXmlDoc.CreateElement("Details");
                objXmlDoc.AppendChild(objTmpElement);

                objTmp = objXmlDoc.CreateElement("TaskType");
                objTmpElement.AppendChild(objTmp);

                objTmpElement = objXmlDoc.CreateElement("option");
                objTmpElement.InnerText = "";
                objTmpElement.SetAttribute("value", "");    //PJS changes made as Databinding helper expect value 
                objTmp.AppendChild(objTmpElement);
                //Added by Amitosh for mits 25163 
                objRemoveJobsList = new ArrayList();
                objsettings = new SysSettings(m_sDbConnstring,m_iClientId);//ClientId add by kuladeep for Jira-1531
                if (!objsettings.UsePolicyInterface)
                {
                    objRemoveJobsList.Add("PolicySystemUpdate");
                    //rupal:mits 33290
                    objRemoveJobsList.Add("ClaimBalancing");
                    //End amitosh
                }
                objRemoveJobsList.Add("LossCodeMappingImport");//abhal3 MITS 36046
                //Aman mits 27791 --Start                
                objRemoveJobsList.Add("Generate Executive Summary Report");
                //Aman mits 27791 --End
                objRemoveJobsList.Add("OSHAReports");

                //JIRA RMA-4606 nshah28 start
                //Changes If Multicurrency is set to off, then this should not display.
                if (objsettings.UseMultiCurrency == 0)
                {
                    objRemoveJobsList.Add("CurrencyExchangeInterface");
                }
                //JIRA RMA-4606 nshah28 end
                sSQL = "SELECT TASK_TYPE_ID, NAME, DISABLE_DETAILED_STATUS, SYSTEM_MODULE_NAME FROM TM_TASK_TYPE";
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    //Added by Amitosh for mits 25163 
                    if (objRemoveJobsList.Contains(Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_MODULE_NAME"))))
                    {
                        continue;
                    }
                    //End amitosh
                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                    objTmp.AppendChild(objTmpElement);

                    if (String.IsNullOrEmpty(sDisableStatusFlag))
                    {
                        sDisableStatusFlag = Conversion.ConvertObjToStr(objReader.GetValue(2));
                    }
                    else
                    {
                        sDisableStatusFlag = sDisableStatusFlag + "^*^*^" + Conversion.ConvertObjToStr(objReader.GetValue(2));
                    }
                    
                    if (String.IsNullOrEmpty(sSystemModuleName))
                    {
                        if (String.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader.GetValue(3))))
                        {
                            sSystemModuleName = "NoSystemModuleName";
                        }
                        else
                        {
                            sSystemModuleName = Conversion.ConvertObjToStr(objReader.GetValue(3));
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader.GetValue(3))))
                        {
                            sSystemModuleName = sSystemModuleName + "^*^*^" + "NoSystemModuleName";
                        }
                        else
                        {
                            sSystemModuleName = sSystemModuleName + "^*^*^" + Conversion.ConvertObjToStr(objReader.GetValue(3));
                        }
                    }
                }

                objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Details");

                objTmp = objXmlDoc.CreateElement("ScheduleType");
                objTmpElement.AppendChild(objTmp);

                objTmpElement = objXmlDoc.CreateElement("option");
                objTmpElement.InnerText = "";
                objTmpElement.SetAttribute("value", "");
                objTmp.AppendChild(objTmpElement);

                sSQL = "SELECT SCHEDULE_TYPE_ID, SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE";
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmpElement.InnerText = Globalization.GetLocalResourceString("li" + Conversion.ConvertObjToStr(objReader.GetValue(1)), 0, sPageId,m_iClientId);//ClientId add by kuladeep for Jira-1531
                    objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                    objTmp.AppendChild(objTmpElement);
                }

                objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Details");
                objTmp = objXmlDoc.CreateElement("DisableDetailedStatus");
                objTmp.InnerText = sDisableStatusFlag;
                objTmpElement.AppendChild(objTmp);

                objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Details");
                objTmp = objXmlDoc.CreateElement("SystemModuleName");
                objTmp.InnerText = sSystemModuleName;
                objTmpElement.AppendChild(objTmp);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetDetails.Error", m_iClientId), objException);
            }
            finally
            {
               if(objReader != null)
                  objReader.Dispose();
               objsettings = null;
            }
            return objXmlDoc;
        }

        /// <summary>
        /// This method gets the interval types from db
        /// in case of schedule type Periodical.
        /// </summary>
        /// <returns>A node containing interval type options</returns>
        public XmlNode GetIntervalType()
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            DbReader objReader = null;
            string sSQL = "";
            try
            {
                objXmlDoc = new XmlDocument();
                objTmpElement = objXmlDoc.CreateElement("Data");
                objXmlDoc.AppendChild(objTmpElement);

                objTmp = objXmlDoc.CreateElement("IntervalType");
                objTmpElement.AppendChild(objTmp);

                objTmpElement = objXmlDoc.CreateElement("option");
                objTmpElement.InnerText = "";
                objTmpElement.SetAttribute("value", ""); //PJS changes made as Databinding helper expect value
                objTmp.AppendChild(objTmpElement);

                sSQL = "SELECT INTERVAL_TYPE_ID, INTERVAL_TYPE FROM TM_SCHEDULE_INTERVAL";
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));

                    objTmp.AppendChild(objTmpElement);
                }

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetIntervalType.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
            return objXmlDoc.SelectSingleNode("//IntervalType");
        }

        /// <summary>
        /// MGaba2:R7:In Case of History Tracking,Maximum limit for interval is fetched from web.config
        /// </summary>
        /// <param name="p_objXml"></param>
        /// <returns></returns>
        public XmlDocument GetMaxIntervalLimit(XmlDocument p_objXml)
        {           
            XmlNode objNode = null;
            try
            {
                objNode = p_objXml.CreateElement("MaxIntervalLimit");
                if (objNode != null)
                {
                    objNode.InnerText = UTILITY.GetConfigUtilitySettings(m_sDbConnstring, m_iClientId)["TMMaxIntervalHistoryTracking"].ToString() ;
                }
                p_objXml.FirstChild.AppendChild(objNode);
                return p_objXml;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetMaxIntervalLimit.Error", m_iClientId), p_objEx);                
            }
    }

        //changed by nadim for BES-start
        /// <summary>
        /// Create Admin connection string and admin user id.
        /// </summary>
        /// <param name="p_sAdminUserName">Admin DbUser Name</param>
        /// <param name="p_sAdminPassword">Admin DbUser Password</param>
        private void SetAdminParams(string p_sAdminUserName, string p_sAdminPassword)
        {
            string sBaseConnstring = string.Empty;
            if (!string.IsNullOrEmpty(p_sAdminUserName) && !string.IsNullOrEmpty(p_sAdminPassword))
            {

                sBaseConnstring = GetBaseConnectionString(m_sDbConnstring);
                m_sAdminDbConnstring = sBaseConnstring + "UID=" + p_sAdminUserName + ";PWD=" + p_sAdminPassword + ";";

            }
            else
            {
                m_sAdminDbConnstring = m_sDbConnstring;
                m_sAdminUserName = m_sDbUserName;
            }


        }
        //changed by nadim for BES-End
        //changed by nadim for BES-start
        /// <summary>
        /// Get Base connection string by removing UID and PWD
        /// </summary>
        /// <param name="strConnectionString">Connection String</param>
        /// <returns>Connection String without UID and PWD </returns>
        private string GetBaseConnectionString(string strConnectionString)
        {
            string strRegExpr = "^(?<CONN_STR_VALUE>[^;].+)UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";
            const string GROUP_NAME = "CONN_STR_VALUE";

            string strMatchValue = string.Empty;

            //Get the Match for the string
            Match regExMatch = Regex.Match(strConnectionString, strRegExpr, RegexOptions.IgnoreCase);

            //Get the matching value for the specified group name
            strMatchValue = regExMatch.Groups[GROUP_NAME].Value;

            return strMatchValue;
        }
        //changed by nadim for BES-End
        //changed by nadim for BES-start
        /// <summary>
        /// Checks the User for database access permissions. For SQL Server it 
        /// checks for dbo rola and for Oracle it checks for DBA role 
        /// </summary>
        /// <returns>True if User is dbo/DBA else otherwise</returns>
        private bool IsLoginAdmin(string p_sModuleName)//MGaba2:R7:In case of History Tracking,only SysAdmin is allowed.Therefore needs Module Name
        {
            DbConnection objConn = null;
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            string sOraBESRole = string.Empty;
            string strDbConnString = string.Empty;
            const string DB_OWNER_ROLE = "db_owner";
            const string SYSADMIN_ROLE = "sysadmin";
            bool m_bIsAdmin = false;
            eDatabaseType m_sDatabaseType = 0;
            try
            {

                objConn = DbFactory.GetDbConnection(m_sDbConnstring);
                objConn.Open();
                m_sDatabaseType = objConn.DatabaseType;


                SetAdminParams(m_sAdminUserName, m_sAdminPassword);

                if (m_sDatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    //        //Check if the user is a member of the db_owner role
                    if (IsSQLRoleMember(DB_OWNER_ROLE, p_sModuleName))//MGaba2:R7:In case of History Tracking,only SysAdmin is allowed
                     //   if (IsSQLRoleMember(DB_OWNER_ROLE))
                    {
                        m_bIsAdmin = true;
                    }
                    else
                    {
                        m_bIsAdmin = false;
                    }
                }
                else if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {

                    sSQL = "SELECT USERNAME,GRANTED_ROLE FROM USER_ROLE_PRIVS WHERE GRANTED_ROLE = 'DBA'";
                    objDbReader = DbFactory.GetDbReader(m_sAdminDbConnstring, sSQL);

                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            if (objDbReader["USERNAME"].ToString().Trim().Length > 0)
                            {
                                m_bIsAdmin = true;
                            }
                        }
                        else
                            m_bIsAdmin = false;


                    }


                }
                else if (m_sDatabaseType == eDatabaseType.DBMS_IS_DB2)
                {
                    m_bIsAdmin = true;
                }

               
                    return m_bIsAdmin;              

            }
            //mkaran2 : MITS 33856 : Start
            // Oracle Login Exception message does not expose Credentials details ; So handle only SQL one.
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 18456)
                {
                    throw new RMAppException(string.Format(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.IsloginAdmin.Error", m_iClientId), p_sModuleName));                 
                }
                else
                {
                    throw (sqlEx);                      
                }
            }
            //mkaran2 : MITS 33856 : End
            catch (Exception exc)
            {

                throw (exc);
            }
            
            
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                    if (objConn != null)
                        objConn.Dispose();
                }

            }
        }
        //changed by nadim for BES-End
        //changed by nadim for BES-start
        /// <summary>
        /// Determines whether the user is a member of a specified SQL
        /// Server role
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strRoleName">string containing the name of the role to verify membership</param>
        /// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal bool IsSQLRoleMember(string strRoleName,string p_sModuleName)
        {
            string SQL_ROLE_NAME = "sp_helprolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsDBO = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;
            const string SECURITYADMIN_ROLE = "securityadmin";
            const string SYSADMIN_ROLE = "sysadmin";
            DbReader objDbReader = null;


            try
            {

                using (objDbReader = DbFactory.GetDbReader(m_sAdminDbConnstring, SQL_ROLE_NAME))
                {

                    while (objDbReader.Read())
                    {
                        string strMemberName = objDbReader[MEMBER_NAME_COLUMN].ToString();
                        strUserName = m_sAdminUserName;
                        //in case of sa user , member name will be dbo
                        if (strUserName.ToLower() == "sa" && strMemberName.ToLower() == "dbo")
                        {
                            blnIsDBO = true;
                            break;
                        }
                        else
                        {

                            if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                            {
                                blnIsDBO = true;
                                break;
                            }
                        }
                    }
                }


                if (blnIsDBO)
                {
                    if (IsSQLServerRoleMember(SYSADMIN_ROLE))
                    {
                        blnIsDBO = true;
                    }
                    else if (p_sModuleName != TASK_HIST_TRACK && IsSQLServerRoleMember(SECURITYADMIN_ROLE))//MGaba2:R7:In case of History Tracking,only SysAdmin is allowed
                    //  else if (IsSQLServerRoleMember(SECURITYADMIN_ROLE))
                    {
                        blnIsDBO = true;
                    }
                    else
                    {
                        blnIsDBO = false;
                    }
                }
                //    //Nadim-To check if the user is having security admin or sysadmin permission-end
                return blnIsDBO;
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }

            }
        }
        //changed by nadim for BES-End
        //changed by nadim for BES-start
        ///// <summary>
        ///// Determines whether the user is a member of a specified SQL
        ///// Server server-level role
        ///// </summary>     
        ///// <param name="strRoleName">string containing the name of the role to verify membership</param>
        ///// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal bool IsSQLServerRoleMember(string strRoleName)
        {
            string SQL_ROLE_NAME = "sp_helpsrvrolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsSysAdmin = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;
            DbReader objDbReader = null;

            //Get a DataReader object
            try
            {
                using (objDbReader = DbFactory.GetDbReader(m_sAdminDbConnstring, SQL_ROLE_NAME))
                {
                    while (objDbReader.Read())
                    {
                        string strMemberName = objDbReader[MEMBER_NAME_COLUMN].ToString();
                        strUserName = m_sAdminUserName;
                        if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                        {
                            blnIsSysAdmin = true;
                            break;
                        }
                    }
                }

                return blnIsSysAdmin;
            }
            catch (Exception exc)
            {

                throw (exc);
            }
            finally
            {
                if (objDbReader != null)
                {
                    if (!objDbReader.IsClosed)
                    {
                        objDbReader.Close();
                    }
                    objDbReader.Dispose();
                    objDbReader = null;
                }

            }
        }
        //changed by nadim for BES-End

        /// <summary>
        /// This method gets all the tasks that are running or completed.
        /// </summary>
        /// <returns>A node containing interval type options</returns>
        public XmlDocument GetJobs(int p_iPageNumber, string p_sPageId, string p_sLangCode)//ksahu5 - ML Changes - MITS 33942 -PageId and Lang ID added
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            DbReader objReader = null;
			DbReader objRdr = null;//33046
            DataSet objDataset = null;
            string sSQL = "";
            int iTaskTypeId = 0;

            int iPageSize = 25;
            //Anu Tennyson : Added for Auto Mail Merge Functionality
            string sSystemModuleName = string.Empty;
            try
            {
                //ksahu5 - ML Changes - MITS 33942 -PageId and Lang ID added
                string sPageID = string.Empty;
                string sLangCode = string.Empty;
                sPageID = p_sPageId;
                sLangCode = p_sLangCode;
                //MITS 33942 end

                objXmlDoc = new XmlDocument();
                objTmpElement = objXmlDoc.CreateElement("Document");
                objXmlDoc.AppendChild(objTmpElement);

                objTmp = objXmlDoc.CreateElement("TaskInfoList");
                objTmpElement.AppendChild(objTmp);

                objTmpElement = objXmlDoc.CreateElement("listhead");
                objTmp.AppendChild(objTmpElement);
                
                objTmp = objXmlDoc.CreateElement("taskname");
                // objTmp.InnerText = "Job Name"; //MITS 33942
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrJobName", 0, sPageID,m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("description");
               // objTmp.InnerText = "Description";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDescription", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("taskstate");
                //objTmp.InnerText = "Job State";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrJobState", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("startdttm");
                //objTmp.InnerText = "Start Date/Time";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrStartdttm", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("enddttm");
                //objTmp.InnerText = "End Date/Time";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrEnddttm", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("jobstateid");
                objTmp.InnerText = "Job State Id";
                objTmpElement.AppendChild(objTmp);

                // This HIDDEN column will be used to determine whether it is a Data Integrator job or not.
                //objTmp = objXmlDoc.CreateElement("dataintegratorjob");
                //objTmp.InnerText = "Data Integrator Job";
                //objTmpElement.AppendChild(objTmp);

                sSQL = "SELECT JOB_ID, TASK_TYPE_ID, JOB_STATE_ID, START_DTTM, END_DTTM, JOB_NAME FROM TM_JOBS ORDER BY START_DTTM DESC";
                objDataset = DbFactory.GetDataSet(ConnectionString, sSQL, m_iClientId);
                

                for (int i = 0; i < objDataset.Tables[0].Rows.Count; i++)
                {
                    objTmp = (XmlElement)objXmlDoc.SelectSingleNode("//TaskInfoList");

                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmp.AppendChild(objTmpElement);

                    objTmp = objXmlDoc.CreateElement("taskname");
                    objTmp.InnerText = objDataset.Tables[0].Rows[i]["JOB_NAME"].ToString();
                    objTmpElement.AppendChild(objTmp);

                    sSQL = "SELECT DESCRIPTION FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = "
                       + objDataset.Tables[0].Rows[i]["TASK_TYPE_ID"].ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("description");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DESCRIPTION"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();


                    sSQL = "SELECT JOB_STATE FROM TM_JOB_STATE WHERE JOB_STATE_ID = "
                       + objDataset.Tables[0].Rows[i]["JOB_STATE_ID"].ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("taskstate");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("JOB_STATE"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();
                    //PJS, MITS 16324 - checked for null date
                    objTmp = objXmlDoc.CreateElement("startdttm");
                    if (objDataset.Tables[0].Rows[i]["START_DTTM"].ToString() != "")
                    {
                        objTmp.InnerText = Conversion.ToDate(objDataset.Tables[0].Rows[i]["START_DTTM"].ToString()).ToShortDateString() +
                                        " " + Conversion.ToDate(objDataset.Tables[0].Rows[i]["START_DTTM"].ToString()).ToShortTimeString();
                    }
                    else
                        objTmp.InnerText = "";
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("enddttm");
                    if (objDataset.Tables[0].Rows[i]["END_DTTM"].ToString() != "")
                    {
                        objTmp.InnerText = Conversion.ToDate(objDataset.Tables[0].Rows[i]["END_DTTM"].ToString()).ToShortDateString() +
                                            " " + Conversion.ToDate(objDataset.Tables[0].Rows[i]["END_DTTM"].ToString()).ToShortTimeString();
                    }
                    else
                        objTmp.InnerText = "";
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("jobid");
                    objTmp.InnerText = objDataset.Tables[0].Rows[i]["JOB_ID"].ToString();
                    objTmpElement.AppendChild(objTmp);

                    // This HIDDEN column will be used to determine whether it is a Data Integrator job or not.
                    //objTmp = objXmlDoc.CreateElement("dataintegratorjob");
                    //iTaskTypeId = Conversion.ConvertObjToInt(objDataset.Tables[0].Rows[i]["TASK_TYPE_ID"]);
                    //objTmp.InnerText = Conversion.ConvertObjToStr(IsDetailedStatusDisabled(iTaskTypeId));
                    //objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("jobstateid");
                    objTmp.InnerText = objDataset.Tables[0].Rows[i]["JOB_STATE_ID"].ToString();
                    objTmpElement.AppendChild(objTmp);
                }

                //XML for Archived jobs

                objTmpElement = (XmlElement)objXmlDoc.SelectSingleNode("//Document");

                objTmp = objXmlDoc.CreateElement("ArchivedTaskInfoList");
                objTmp.SetAttribute("rowstart", "");
                objTmp.SetAttribute("rowend", "");
                objTmp.SetAttribute("totalrecords", "");
                objTmp.SetAttribute("pagesize", "");
                objTmp.SetAttribute("pagenumber", "");
                objTmp.SetAttribute("totalpages", "");
                objTmpElement.AppendChild(objTmp);

                objTmpElement = objXmlDoc.CreateElement("listhead");
                objTmp.AppendChild(objTmpElement);
                
                objTmp = objXmlDoc.CreateElement("taskname");
                //objTmp.InnerText = "Job Name";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrJobName", 0, sPageID,m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531 
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("description");
                //objTmp.InnerText = "Description";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDescription", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("taskstate");
                //objTmp.InnerText = "Job State";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrJobState", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("startdttm");
                //objTmp.InnerText = "Start Date/Time";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrStartdttm", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("enddttm");
               // objTmp.InnerText = "End Date/Time";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrEnddttm", 0, sPageID, m_iClientId), sLangCode); //MITS 33942 //Add ClientId by kuladeep for Cloud Jira-1531
                objTmpElement.AppendChild(objTmp);
                // This HIDDEN column will be used to determine whether it is a Data Integrator job or not.
                objTmp = objXmlDoc.CreateElement("dataintegratorjob");
                objTmp.InnerText = "Data Integrator Job";
                objTmpElement.AppendChild(objTmp);

                objTmp = objXmlDoc.CreateElement("jobstateid");
                objTmp.InnerText = "Job State Id";
                objTmpElement.AppendChild(objTmp);

                sSQL = "SELECT JOB_ID, TASK_TYPE_ID, JOB_STATE_ID, START_DTTM, END_DTTM, JOB_NAME FROM TM_JOBS_HIST ORDER BY START_DTTM DESC";
                objDataset = DbFactory.GetDataSet(ConnectionString, sSQL, m_iClientId);

                iPageSize = Conversion.ConvertObjToInt(UTILITY.GetConfigUtilitySettings(m_sDbConnstring, m_iClientId)["TMPagingRecords"], m_iClientId);
                
                int  iTotalRecords = objDataset.Tables[0].Rows.Count;

                if (p_iPageNumber == 0)
                {
                    p_iPageNumber = 1;
                }

                int iTotalPages = ((iTotalRecords - 1) / iPageSize)+ 1;

                int iRowStart = (p_iPageNumber - 1) * iPageSize;
                int iRowEnd = iRowStart + iPageSize;
                if (iRowEnd > (iTotalRecords - 1))
                {
                    iRowEnd = iTotalRecords;
                }


                XmlNode objNode = objXmlDoc.SelectSingleNode("//ArchivedTaskInfoList");

                if (objNode != null)
                {
                    objNode.Attributes["rowstart"].Value = iRowStart.ToString();
                    objNode.Attributes["rowend"].Value = iRowEnd.ToString();
                    objNode.Attributes["totalrecords"].Value = iTotalRecords.ToString();
                    objNode.Attributes["pagesize"].Value = iPageSize.ToString();
                    objNode.Attributes["pagenumber"].Value = p_iPageNumber.ToString();
                    objNode.Attributes["totalpages"].Value = iTotalPages.ToString();
                }


                for (int i = iRowStart; i < iRowEnd; i++)
                {
                    objTmp = (XmlElement)objXmlDoc.SelectSingleNode("//ArchivedTaskInfoList");

                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmp.AppendChild(objTmpElement);


                    objTmp = objXmlDoc.CreateElement("taskname");
                    objTmp.InnerText = objDataset.Tables[0].Rows[i]["JOB_NAME"].ToString();
                    objTmpElement.AppendChild(objTmp);

                    sSQL = "SELECT DESCRIPTION FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = "
                       + objDataset.Tables[0].Rows[i]["TASK_TYPE_ID"].ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("description");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DESCRIPTION"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();

                    sSQL = "SELECT JOB_STATE FROM TM_JOB_STATE WHERE JOB_STATE_ID = "
                       + objDataset.Tables[0].Rows[i]["JOB_STATE_ID"].ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("taskstate");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("JOB_STATE"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();
                    //PJS, MITS 16324 - checked for null date
                    objTmp = objXmlDoc.CreateElement("startdttm");
                    if (objDataset.Tables[0].Rows[i]["START_DTTM"].ToString() != "")
                    {
                        objTmp.InnerText = Conversion.ToDate(objDataset.Tables[0].Rows[i]["START_DTTM"].ToString()).ToShortDateString() +
                                            " " + Conversion.ToDate(objDataset.Tables[0].Rows[i]["START_DTTM"].ToString()).ToShortTimeString();
                    }
                    else
                        objTmp.InnerText = "";
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("enddttm");
                    if (objDataset.Tables[0].Rows[i]["END_DTTM"].ToString() != "")
                    {
                        objTmp.InnerText = Conversion.ToDate(objDataset.Tables[0].Rows[i]["END_DTTM"].ToString()).ToShortDateString() +
                                            " " + Conversion.ToDate(objDataset.Tables[0].Rows[i]["END_DTTM"].ToString()).ToShortTimeString();
                    }
                    else
                        objTmp.InnerText = "";
                    objTmpElement.AppendChild(objTmp);

                    objTmp = objXmlDoc.CreateElement("jobid");
                    objTmp.InnerText = objDataset.Tables[0].Rows[i]["JOB_ID"].ToString();
                    objTmpElement.AppendChild(objTmp);

                    // This HIDDEN column will be used to determine whether it is a Data Integrator job or not.
                    objTmp = objXmlDoc.CreateElement("dataintegratorjob");
                    iTaskTypeId = Conversion.ConvertObjToInt(objDataset.Tables[0].Rows[i]["TASK_TYPE_ID"], m_iClientId);
					//Anu Tennyson : Added for showing the archived files on the grid.
                    sSystemModuleName = GetSystemModuleName(iTaskTypeId);
                    if (string.Compare(sSystemModuleName, TASK_AUTO_MAIL_MERGE, true) == 0)
                    {
                        objTmp.InnerText = "-1";
                    }
                    else
                    {
                        objTmp.InnerText = Conversion.ConvertObjToStr(IsDetailedStatusDisabled(iTaskTypeId));
                    }
                    
                    objTmpElement.AppendChild(objTmp);
                    
                    objTmp = objXmlDoc.CreateElement("jobstateid");
                    objTmp.InnerText = objDataset.Tables[0].Rows[i]["JOB_STATE_ID"].ToString();
                    objTmpElement.AppendChild(objTmp);
                }

            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetJobs.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
            return objXmlDoc;
        }

        /// <summary>
        /// This method gets all the tasks that are scheduled.
        /// </summary>
        /// <returns>XML document containing the scheduled task info.</returns>
        public XmlDocument GetScheduledJobs(string p_sPageId, string p_sLangCode)//ksahu5 - ML Changes - MITS 33897 -PageId and Lang ID added
        {
            XmlDocument objXmlDoc = null;
            XmlElement objTmpElement = null;
            XmlElement objTmp = null;
            DbReader objReader = null;
            ArrayList objArrayList = null;
            ScheduleDetails objScheduleDetails;
            string sSQL = "";
            try
            {
                objScheduleDetails = new ScheduleDetails();
                objXmlDoc = new XmlDocument();
                objTmpElement = objXmlDoc.CreateElement("Document");
                objXmlDoc.AppendChild(objTmpElement);

                objTmp = objXmlDoc.CreateElement("TaskInfoList");
                objTmpElement.AppendChild(objTmp);

                objTmpElement = objXmlDoc.CreateElement("listhead");
                objTmp.AppendChild(objTmpElement);
                objTmp = objXmlDoc.CreateElement("taskname");
                //ksahu5 - ML Changes - MITS 33897-start
                //objTmp.InnerText = "Task Name";12206
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrTaskName", 0, p_sPageId,m_iClientId), p_sLangCode);//ClientId add by kuladeep for Jira-1531
                //ksahu5 - ML Changes - MITS 33897-End
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("scheduletype");
                //ksahu5 - ML Changes - MITS 33897-start
                //objTmp.InnerText = "Schedule Type";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrScheduleType", 0, p_sPageId, m_iClientId), p_sLangCode);//ClientId add by kuladeep for Jira-1531
                //ksahu5 - ML Changes - MITS 33897-End
                objTmpElement.AppendChild(objTmp);
                
                objTmp = objXmlDoc.CreateElement("nextrundttm");
                //ksahu5 - ML Changes - MITS 33897-start
                //objTmp.InnerText = "Next Run Date/Time";
                objTmp.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrNextrundttm", 0, p_sPageId, m_iClientId), p_sLangCode);//ClientId add by kuladeep for Jira-1531
                //ksahu5 - ML Changes - MITS 33897-End
                objTmpElement.AppendChild(objTmp);
                

                objArrayList = new ArrayList();
                //sSQL = "SELECT SCHEDULE_ID, TASK_TYPE_ID, SCHEDULE_TYPE_ID, NEXT_RUN_DTTM, TASK_NAME FROM TM_SCHEDULE";
				sSQL = "SELECT SCHEDULE_ID, TASK_TYPE_ID, SCHEDULE_TYPE_ID, NEXT_RUN_DTTM, TASK_NAME FROM TM_SCHEDULE ORDER BY TASK_NAME";//Deb : MITS 32214
                objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objScheduleDetails.ScheduleId = Conversion.ConvertObjToInt(objReader.GetValue("SCHEDULE_ID"), m_iClientId);
                    objScheduleDetails.TaskTypeId = Conversion.ConvertObjToInt(objReader.GetValue("TASK_TYPE_ID"), m_iClientId);
                    objScheduleDetails.ScheduleTypeId = Conversion.ConvertObjToInt(objReader.GetValue("SCHEDULE_TYPE_ID"), m_iClientId);
                    if (objReader.GetString("NEXT_RUN_DTTM") != "")
                    {
                        string NextRunDate = objReader.GetString("NEXT_RUN_DTTM");
                        objScheduleDetails.NextRunDTTM = Conversion.GetUIDate(NextRunDate, p_sLangCode,m_iClientId) + " " + Conversion.GetUITime(NextRunDate,p_sLangCode,m_iClientId);//Add clientId by kuladeep for Cloud
                    }
                    else
                    {
                        objScheduleDetails.NextRunDTTM = "";
                    }


                    objScheduleDetails.TaskName = objReader.GetString("TASK_NAME");
                    objArrayList.Add(objScheduleDetails);
                }
                if (objReader != null)
                    objReader.Close();

                for (int i = 0; i < objArrayList.Count; i++)
                {
                    objTmp = (XmlElement)objXmlDoc.SelectSingleNode("//TaskInfoList");

                    objTmpElement = objXmlDoc.CreateElement("option");
                    objTmp.AppendChild(objTmpElement);             

                    objTmp = objXmlDoc.CreateElement("taskname");
                    objTmp.InnerText = ((ScheduleDetails)objArrayList[i]).TaskName.ToString();
                    objTmpElement.AppendChild(objTmp);

                    sSQL = "SELECT SCHEDULE_TYPE FROM TM_SCHEDULE_TYPE WHERE SCHEDULE_TYPE_ID = "
                       + ((ScheduleDetails)objArrayList[i]).ScheduleTypeId.ToString();
                    objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objTmp = objXmlDoc.CreateElement("scheduletype");
                        objTmp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_TYPE"));
                        objTmpElement.AppendChild(objTmp);
                    }
                    objReader.Close();
                    
                    objTmp = objXmlDoc.CreateElement("nextrundttm");
                    objTmp.InnerText = ((ScheduleDetails)objArrayList[i]).NextRunDTTM;
                    objTmpElement.AppendChild(objTmp);
                    objTmp = objXmlDoc.CreateElement("scheduletypeId");
                    objTmp.InnerText = ((ScheduleDetails)objArrayList[i]).ScheduleTypeId.ToString();
                    objTmpElement.AppendChild(objTmp);
                    objTmp = objXmlDoc.CreateElement("scheduleid");
                    objTmp.InnerText = ((ScheduleDetails)objArrayList[i]).ScheduleId.ToString();
                    objTmpElement.AppendChild(objTmp);
                }
                //PJS:Added Blank row for Grid
                objTmp = (XmlElement)objXmlDoc.SelectSingleNode("//TaskInfoList");
                objTmpElement = objXmlDoc.CreateElement("option");
                objTmp.AppendChild(objTmpElement);
                objTmp = objXmlDoc.CreateElement("taskname");
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("scheduletype");
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("nextrundttm");
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("scheduletypeId");
                objTmpElement.AppendChild(objTmp);
                objTmp = objXmlDoc.CreateElement("scheduleid");
                objTmpElement.AppendChild(objTmp);


            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetScheduledJobs.Error", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
            return objXmlDoc;
        }

        /// <summary>
        /// This method updates the tm_jobs table and sets the state to Schedule to Run.
        /// </summary>
        /// <param name="p_objXmlin">Xml document containing the schedule id.</param>
        public void KillTask(XmlDocument p_objXmlin)
        {
            string sJobId = "";
            DbCommand objCommand = null;
            DbConnection objConn = null;
            string sSQL = "";

            try
            {
                sJobId = p_objXmlin.SelectSingleNode("//SelectedRow").InnerText;

                objConn = DbFactory.GetDbConnection(ConnectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();

                sSQL = "UPDATE TM_JOBS SET JOB_STATE_ID = 5"
                + " WHERE JOB_ID = " + sJobId;

                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.KillTask.Error", m_iClientId), objException);
            }
            finally
            {
                if (objConn != null)
                    objConn.Dispose();
            }
        }

        /// <summary>
        /// This method gets the Status Details for a specific job id.
        /// </summary>
        /// <param name="p_objXmlin">Contains the job id for which the status is to be retrieved.</param>
        /// <returns>An Xml Document containing the Status Details</returns>
        public XmlDocument GetStatusDetails(XmlDocument p_objXmlin)
        {
            string sJobId = "";
            //string sSQL = ""; Commented by bsharma33 not being used anywhere.
            string sStatus = "";
            string[] objArrDelimiter = { "^*^*^" }; 
            XmlDocument objDoc = null;
            XmlElement objElement = null;
            DbReader objReader = null;
            StringBuilder sbStatus = null;
            //Added by bsharma33 for PenTesting MITS 26917 
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            StringBuilder sbSQL = null;
            //End Changes by bsharma33 for PenTesting MITS 26917

            try
            {
                sbStatus = new StringBuilder();
                objDoc = new XmlDocument();
				//Added by bsharma33 for PenTesting MITS 26917 
                sbSQL = new System.Text.StringBuilder();
				//End Changes by bsharma33 for PenTesting MITS 26917
                sJobId = p_objXmlin.SelectSingleNode("//JobId").InnerText;
                
                //Changed by bsharma33 for PenTesting MITS 26917 
                //sSQL = "SELECT MESSAGE FROM TM_JOB_LOG WHERE JOB_ID ={0} " + sJobId;
                sbSQL.Append(string.Format("SELECT MESSAGE FROM TM_JOB_LOG WHERE JOB_ID ={0} ", "~JOB_ID~")); 
                //sbSQL.Append(string.Format("  WHERE EFFECTIVE_DATE >= '{0}'", "~EFFECTIVE_DATE~"));
                dictParams.Add("JOB_ID", sJobId);
                objReader = DbFactory.ExecuteReader(ConnectionString,sbSQL.ToString(),dictParams);
                //objReader = DbFactory.GetDbReader(ConnectionString, sSQL);
                //End Changes by bsharma33 for PenTesting MITS 26917
                if (objReader.Read())
                {
                    sStatus = objReader.GetString("MESSAGE");
                }

                if (!sStatus.Equals(string.Empty))
                {
                    objDoc.LoadXml(sStatus);

                    objElement = (XmlElement)objDoc.SelectSingleNode("//MessageList");
                    foreach (XmlNode objTmpNode in objElement.ChildNodes)
                    {
                        if (objTmpNode.InnerText.IndexOf("^*^*^") > 0)
                        {
                        if (Conversion.ConvertStrToLong(((string[])objTmpNode.InnerText.Split(objArrDelimiter, StringSplitOptions.None))[0]) > 0)
                        {
                            sbStatus.Append(Conversion.ToDate(objTmpNode.Attributes["date"].Value).ToShortDateString() + " " + Conversion.GetTimeAMPM(objTmpNode.Attributes["time"].Value)
                                + " - Error : " + ((string[])objTmpNode.InnerText.Split(objArrDelimiter, StringSplitOptions.None))[1] + Environment.NewLine + Environment.NewLine);
                        }
                        else
                        {
                            sbStatus.Append(Conversion.ToDate(objTmpNode.Attributes["date"].Value).ToShortDateString() + " " + Conversion.GetTimeAMPM(objTmpNode.Attributes["time"].Value)
                                + " - " + ((string[])objTmpNode.InnerText.Split(objArrDelimiter, StringSplitOptions.None))[1] + Environment.NewLine + Environment.NewLine);
                            }
                        }
                        else
                        {
                            sbStatus.Append(Conversion.ToDate(objTmpNode.Attributes["date"].Value).ToShortDateString() + " " + Conversion.GetTimeAMPM(objTmpNode.Attributes["time"].Value)
                                    + " - " + (objTmpNode.InnerText) + Environment.NewLine + Environment.NewLine);
                        }
                    }
                    p_objXmlin.SelectSingleNode("//StatusDetails").InnerText = AddAmparsand(sbStatus.ToString());
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetStatusDetails.Error", m_iClientId), objException);
            }
            finally
            {   
                if (objReader != null)
                    objReader.Dispose();
            }
            return p_objXmlin;
        }
        #endregion

        /// <summary>
        /// Replace &amp; , &gt;, etc. from string
        /// </summary>
        /// <param name="p_sValue">string</param>
        /// <param name="p_iLength">length</param>
        /// <returns>string</returns>
        private static string AddAmparsand(string p_sValue)
        {
            p_sValue = p_sValue.Replace("&amp;", "&");
            p_sValue = p_sValue.Replace("&gt;", ">");
            p_sValue = p_sValue.Replace("&lt;", "<");
            p_sValue= p_sValue.Replace("&quot;", "-");
            p_sValue = p_sValue.Replace("&apos;", "'");
            return p_sValue;
        }

        /// <summary>
        /// Gets the next UID from the TM_IDs table.
        /// </summary>
        /// <param name="p_sTableName"></param>
        /// <returns></returns>
        private int GetNextUID(string p_sTableName)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            DbReader objReader = null;
            int iOrigUID = 0;
            int iNextUID = 0;
            int iCollisionRetryCount = 0;
            int iErrRetryCount = 0;
            int iRows = 0;

            const int COLLISION_RETRY_COUNT = 1000;
            const int ERROR_RETRY_COUNT = 5;
            try
            {
                do
                {
                    sSQL = "SELECT NEXT_ID FROM TM_IDS WHERE TABLE_NAME = '" + p_sTableName + "'";

                    objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (!objReader.Read())
                        {
                            objReader.Close();
                            objReader = null;
                            throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.NoSuchTable", m_iClientId));
                        }

                        iNextUID = objReader.GetInt("NEXT_ID");
                        objReader.Close();
                        objReader = null;

                        // Compute next id
                        iOrigUID = iNextUID;
                        if (iOrigUID != 0)
                            iNextUID++;
                        else
                            iNextUID = 2;

                        // try to reserve id (searched update)
                        sSQL = "UPDATE TM_IDS SET NEXT_ID = " + iNextUID + " WHERE TABLE_NAME = '" + p_sTableName + "'";

                        // only add searched clause if no chance of a null originally
                        // in row (only if no records ever saved against table)   
                        if (iOrigUID != 0)
                            sSQL += " AND NEXT_ID = " + iOrigUID;

                        // Try update
                        try
                        {
                            iRows = objConn.ExecuteNonQuery(sSQL);
                        }
                        catch (Exception e)
                        {
                            iErrRetryCount++;
                            if (iErrRetryCount >= 5)
                            {
                                objConn.Close();
                                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.ErrorModifyingDB", m_iClientId), e);
                            }
                        }
                        // if success, return
                        if (iRows == 1)
                        {
                            objConn.Close();
                            return iNextUID - 1;
                        }
                        else // collided with another user - try again (up to 1000 times)
                            iCollisionRetryCount++;

                    }

                } while ((iErrRetryCount < ERROR_RETRY_COUNT) && (iCollisionRetryCount < COLLISION_RETRY_COUNT));

                if (iCollisionRetryCount >= COLLISION_RETRY_COUNT)
                {
                    objConn.Close();
                    throw new RMAppException
                        (Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.CollisionTimeout", m_iClientId));
                }
                objConn.Close();
                //shouldn't get here under normal conditions.
                return 0;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetNextUID.Error", m_iClientId), p_objEx);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the existing tasks,dataSources and assigned users 
        /// </summary>
        /// <param name="p_objXmlin"></param>
        /// <returns></returns>
 
        public XmlDocument GetTasksForTaskMgrDiary(XmlDocument p_objXmlin, out bool p_blnIsTaskExist)
        {
            DbReader objReader = null;
            int iCounter = 0;
            string sTaskName = string.Empty;
            string sLName = string.Empty;
            string sName = string.Empty;
            string sDSNID = string.Empty;
            string sFName = string.Empty;
            string sDSNIdSelected = string.Empty;
            string sUserId = string.Empty;
            XmlDocument objDOM = null;
            StringBuilder sbSql = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            XmlNode objNode = null;

            try
            {
                //Get existing tasks from the Task Manager Database
                p_blnIsTaskExist = false;
                sbSql = new StringBuilder();
                sbSql.Append("SELECT SCHEDULE_ID,TASK_NAME FROM TM_SCHEDULE WHERE TM_USER IS NULL AND TM_DSN IS NULL");
                objDOM = new XmlDocument();

                objElemParent = objDOM.CreateElement("ScheduledTasks");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("Tasks");
                objDOM.FirstChild.AppendChild(objElemChild);
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                while (objReader.Read())
                {
                    p_blnIsTaskExist = true;
                    objElemTemp = objDOM.CreateElement("option");
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("SCHEDULE_ID")));
                    sTaskName = Conversion.ConvertObjToStr(objReader.GetValue("TASK_NAME"));
                    objElemTemp.InnerText = sTaskName;
                    objElemChild.AppendChild(objElemTemp);
                }
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                }


                //Get the list of dataSources from the Riskmaster Security Database
                if (p_objXmlin != null)
                {
                    //sbSql = new StringBuilder();
                    sbSql.Length = 0;
                    sbSql.Append("SELECT DSNID,DSN FROM DATA_SOURCE_TABLE ORDER BY DSN");

                    objElemChild = objDOM.CreateElement("control");
                    objElemChild.SetAttribute("name", "DataSources");
                    objElemChild.SetAttribute("type", "combobox");
                    objDOM.FirstChild.AppendChild(objElemChild);

                    objReader = DbFactory.GetDbReader(m_sConnectionStrSecurity, sbSql.ToString());
                    while (objReader.Read())
                    {
                        sName = Conversion.ConvertObjToStr(objReader.GetValue("DSN"));
                        sDSNID = Conversion.ConvertObjToStr(objReader.GetValue("DSNID"));

                        if (iCounter == 0)
                        {
                            sDSNIdSelected = sDSNID;
                        }
                        objElemTemp = objDOM.CreateElement("option");
                        objElemTemp.SetAttribute("value", sDSNID);
                        objElemTemp.InnerText = sName;
                        objElemChild.AppendChild(objElemTemp);
                        iCounter++;
                    }
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }

                    objNode = p_objXmlin.SelectSingleNode("/ScheduledTasks/DataSourceID");
                    if (objNode != null)
                    {
                        //if (objNode.InnerText != "")
                        if (!string.IsNullOrEmpty(objNode.InnerText))
                        {
                            sDSNIdSelected = objNode.InnerText;
                        }
                    }

                    //if (sDSNID != "")
                    if (!string.IsNullOrEmpty(sDSNID))
                    {

                        //Get users assigned to selected datasource
                        //sbSql = new StringBuilder();
                        sbSql.Length = 0;
                        sbSql.Append("SELECT DISTINCT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME,USER_DETAILS_TABLE.DSNID");
                        sbSql.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = " + sDSNIdSelected);
                        sbSql.Append(" ORDER BY USER_DETAILS_TABLE.LOGIN_NAME");

                        objElemChild = objDOM.CreateElement("control");
                        objElemChild.SetAttribute("name", "Users");
                        objElemChild.SetAttribute("type", "combobox");
                        objDOM.FirstChild.AppendChild(objElemChild);

                        objReader = DbFactory.GetDbReader(m_sConnectionStrSecurity, sbSql.ToString());
                        while (objReader.Read())
                        {
                            sLName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                            sFName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                            sLName = sFName + " " + sLName;
                            sName = Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
                            sName = sName + " (" + sLName + ")";
                            sUserId = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));

                            objElemTemp = objDOM.CreateElement("option");
                            objElemTemp.SetAttribute("value", sUserId);
                            objElemTemp.InnerText = sName;
                            objElemChild.AppendChild(objElemTemp);
                        }
                        if (!objReader.IsClosed)
                        {
                            objReader.Close();
                        }
                    }
                }
                              
                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetTasksForTaskMgrDiary.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                objDOM = null;
                sbSql = null;            
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
            }
        }

        //sanoopsharma - integral policy update task scheduling 
        /// <summary>
        /// Checks if duplicate task exists.
        /// </summary>
        /// <param name="TaskName"></param>
        /// <param name="PolicySystemID"></param>
        /// <param name="intervaltype"></param>
        /// <param name="interval"></param>
        /// <param name="DataSourceName"></param>
        /// <returns></returns>

        public bool CheckForDuplicateSchedule(string TaskName,string PolicySystemID,string intervaltype, string interval, string DSN)
        {            
            string sSQL = string.Empty;
            string sConfig = string.Empty;
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            DbConnection objConn = null;
            eDatabaseType m_sDatabaseType = 0;

            objConn = DbFactory.GetDbConnection(m_sDbConnstring);
            objConn.Open();
            m_sDatabaseType = objConn.DatabaseType;
            DbReader objReader;
            try
            {
                if (m_sDatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    sSQL = string.Format("SELECT CONFIG FROM TM_SCHEDULE WHERE TASK_NAME = {0} AND INTERVAL_TYPE_ID = {1} AND INTERVAL <= {2} AND TM_DSN = {3}" +
                        " AND cast(config as XML).value('(/Task/Args/arg)[3]','VARCHAR(100)')=  {4}"
                        , "~TaskName~", "~INTERVAL_TYPE_ID~", "~INTERVAL~", "~DSN~", "~POLICYSYSTEMID~");
                    dictParams.Add("TaskName", TaskName);
                    dictParams.Add("INTERVAL_TYPE_ID", intervaltype);
                    dictParams.Add("INTERVAL", interval);
                    dictParams.Add("DSN", DSN);
                    dictParams.Add("POLICYSYSTEMID", PolicySystemID);

                    objReader = DbFactory.ExecuteReader(ConnectionString, sSQL, dictParams);
                    if (objReader.Read())
                        return false;
                    else
                        return true;
                }
                else if (m_sDatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sSQL = string.Format("SELECT SCHEDULE_ID FROM TM_SCHEDULE WHERE TASK_NAME = {0} AND INTERVAL_TYPE_ID = {1}"
                    + " AND INTERVAL <= {2} AND TM_DSN = {3}"
                    + " AND extractvalue(xmltype(config), '(/Task/Args/arg)[3]') = {4}"
                    , "~TaskName~", "~INTERVAL_TYPE_ID~", "~INTERVAL~", "~DSN~", "~POLICYSYSTEMID~");

                    dictParams.Add("TaskName", TaskName);
                    dictParams.Add("INTERVAL_TYPE_ID", intervaltype);
                    dictParams.Add("INTERVAL", interval);
                    dictParams.Add("DSN", DSN);
                    dictParams.Add("POLICYSYSTEMID", PolicySystemID);

                    objReader = DbFactory.ExecuteReader(ConnectionString, sSQL, dictParams);
                    if (objReader.Read())
                        return false;
                    else
                        return true;
                }

                //while (objReader.Read())
                //{
                //    sConfig = objReader.GetString("CONFIG");
                //    if (sConfig.Contains(PolicySystemID))
                //        return false;
                //}
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            finally
            {
                objReader = null;
            }

            return true;
            
        }
        //sanoopsharma end 
        /// <summary>
        /// Update Existing Tasks for Task Manager Diary.
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        public void UpdateTasksForTaskMgrDiary(XmlDocument p_objXmlDocument)
        {  
            ArrayList objQuery = null;                        
            XmlNode objTempNode = null;         
            string[] arrTasks= null;
            string sUser = "";
            string sDataSource = "";
            string sSQL = "";
            
            try
            {
                objQuery = new ArrayList();
                objTempNode = p_objXmlDocument.SelectSingleNode("/ScheduledTasks/Tasks");
                if (objTempNode != null)
                {
                    if (objTempNode.InnerText != null)
                    {
                        arrTasks = objTempNode.InnerText.Split(" ".ToCharArray());
                    }
                }
                objTempNode = p_objXmlDocument.SelectSingleNode("/ScheduledTasks/User");
                
                if (objTempNode != null)
                {
                    sUser = objTempNode.InnerText;                 
                }

                objTempNode = p_objXmlDocument.SelectSingleNode("/ScheduledTasks/DataSourceName");
                if (objTempNode != null)
                {
                    sDataSource = objTempNode.InnerText;
                }           

                for (int i = 0; i < arrTasks.Length; i++)
                {
                    sSQL = "UPDATE TM_SCHEDULE SET TM_USER = '" + sUser + "'" + ",TM_DSN = '" + sDataSource + "'"+
                           " WHERE SCHEDULE_ID = " + arrTasks[i];

                    objQuery.Add(sSQL);
                }

                UTILITY.Save(objQuery, m_sConnectionString, m_iClientId);
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.UpdateTasksForTaskMgrDiary.Error", m_iClientId), objException);
            }
            finally
            {
                objQuery = null;
                objTempNode = null;
                arrTasks = null;                
            }
        }
        public XmlDocument GetAccList(XmlDocument p_objXmlDocument)
        {  
            XmlNode objTempNode = null;         
            string sSQL = string.Empty;
            int iAccId = 0;
            int iEFTCodeId = 0;
            bool bInSuccess = false;
            DbReader objReader = null;
            XmlNode objNode = null; 
            XmlElement objNewElm = null;
            XmlElement objTempElm = null;
            try
            {
                objTempNode = p_objXmlDocument.SelectSingleNode("//AccountId");
                if (objTempNode != null)
                {
                     iAccId = Conversion.CastToType<int>(objTempNode.InnerText, out bInSuccess);
                }
                objNode = p_objXmlDocument.SelectSingleNode("//Account");
                
                //sSQL = "SELECT ACCOUNT_ID, ACCOUNT_NAME FROM ACCOUNT ORDER BY ACCOUNT_NAME";
                sSQL = "SELECT ACCOUNT_ID, ACCOUNT_NAME,IS_EFT_ACCOUNT FROM ACCOUNT ORDER BY ACCOUNT_NAME";//JIRA:438 : ajohari2
                objReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL.ToString());
                while (objReader.Read())
                {
                    objTempElm = p_objXmlDocument.CreateElement("AccountList");
                    objNewElm = p_objXmlDocument.CreateElement("ACCOUNT_ID");
                    objNewElm.InnerText = objReader.GetInt32("ACCOUNT_ID").ToString();
                    if(iAccId == 0) iAccId = objReader.GetInt32("ACCOUNT_ID");
                    objTempElm.AppendChild(objNewElm);
                    objNewElm = p_objXmlDocument.CreateElement("ACCOUNT_NAME");
                    objNewElm.InnerText =  objReader.GetString("ACCOUNT_NAME");
                    objTempElm.AppendChild(objNewElm);
                    //JIRA:438 START: ajohari2
                    objNewElm = p_objXmlDocument.CreateElement("IS_EFT_ACCOUNT");
                    objNewElm.InnerText = objReader.GetBoolean("IS_EFT_ACCOUNT").ToString();
                    objTempElm.AppendChild(objNewElm);
                    //JIRA:438 End:
                    objNode.AppendChild(objTempElm);
                }
                objReader.Close();
                objNode = p_objXmlDocument.SelectSingleNode("//Stock");
                sSQL = "SELECT STOCK_ID, STOCK_NAME FROM CHECK_STOCK WHERE ACCOUNT_ID = " + iAccId + " ORDER BY STOCK_NAME";
                objReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL.ToString());
                while (objReader.Read())
                {
                    objTempElm = p_objXmlDocument.CreateElement("StockList");
                    objNewElm = p_objXmlDocument.CreateElement("STOCK_ID");
                    objNewElm.InnerText = objReader.GetInt32("STOCK_ID").ToString();
                    objTempElm.AppendChild(objNewElm);
                    objNewElm = p_objXmlDocument.CreateElement("STOCK_NAME");
                    objNewElm.InnerText = objReader.GetString("STOCK_NAME");
                    objTempElm.AppendChild(objNewElm);
                    objNode.AppendChild(objTempElm);
                }
                objReader.Close();

                // npadhy JIRA 6418 Starts - While Invoking the Batch Check Print from Task Manager we need to have the Distribution Type there as well
                // We retrieve only those Distribution Type for which we have the settings in CHECK_PRINT_OPTIONS.
                // Only exception is EFT as we do not have any mapping for EFT
                objNode = p_objXmlDocument.SelectSingleNode("//DistributionTypes");
                sSQL = " Select C.CODE_ID, CT.CODE_DESC  from CHECK_PRINT_OPTIONS CPO inner join CODES C on CPO.DSTRBN_TYPE_CODE = C.CODE_ID iNNER JOIN CODES_TEXT CT on C.CODE_ID = CT.CODE_ID ORDER BY C.CODE_ID";
                objReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL);
                while (objReader.Read())
                {
                    objTempElm = p_objXmlDocument.CreateElement("DistributionType");
                    objNewElm = p_objXmlDocument.CreateElement("DistributionTypeId");
                    objNewElm.InnerText = objReader.GetInt32("CODE_ID").ToString();
                    objTempElm.AppendChild(objNewElm);
                    objNewElm = p_objXmlDocument.CreateElement("DistributionTypeDesc");
                    objNewElm.InnerText = objReader.GetString("CODE_DESC");
                    objTempElm.AppendChild(objNewElm);
                    objNode.AppendChild(objTempElm);
                }
                using (LocalCache objCache = new LocalCache(m_sDbConnstring, m_iClientId))
                {
                    iEFTCodeId = objCache.GetCodeId("EFT", "DISTRIBUTION_TYPE");

                    objTempElm = p_objXmlDocument.CreateElement("DistributionType");
                    objNewElm = p_objXmlDocument.CreateElement("DistributionTypeId");
                    objNewElm.InnerText = iEFTCodeId.ToString();
                    objTempElm.AppendChild(objNewElm);
                    objNewElm = p_objXmlDocument.CreateElement("DistributionTypeDesc");
                    objNewElm.InnerText = objCache.GetCodeDesc(iEFTCodeId);
                    objTempElm.AppendChild(objNewElm);
                    objNode.AppendChild(objTempElm);
                }
                objReader.Close();
                objNode = p_objXmlDocument.SelectSingleNode("//EFTDistributionType");
                objNode.InnerText = iEFTCodeId.ToString();
                // npadhy JIRA 6418 Ends 
                return p_objXmlDocument;
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetAccList.Error", m_iClientId), objException);
            }
            finally
            {
                objTempNode = null;
                objNode = null;
                objNewElm = null;
                objReader.Dispose();
            }
        }
        public XmlDocument GetPolicySystems()
        {
           XmlDocument objXmlDocument = null;
            XmlElement objOptionXmlElement = null;
            XmlElement objRootElement = null;
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            bool isRecordsFound = false;
            XmlElement objChild = null;
            XmlElement objChild1 = null;
            try
            {
                objXmlDocument = new XmlDocument();
                sSQL = "SELECT POLICY_SYSTEM_ID,POLICY_SYSTEM_NAME FROM POLICY_X_WEB WHERE FINANCIAL_UPD_FLAG = -1 ORDER BY POLICY_SYSTEM_NAME ";
                objDbReader = DbFactory.GetDbReader(m_sDbConnstring, sSQL);
               
                objRootElement = objXmlDocument.CreateElement("Details");
                objXmlDocument.AppendChild(objRootElement);

                objChild1 = objXmlDocument.CreateElement("PolicySystemList");
                objRootElement.AppendChild(objChild1);

                objChild = objXmlDocument.CreateElement("PolicySystems");
                objChild1.AppendChild(objChild);
             

                while (objDbReader.Read())
                {
                    isRecordsFound = true;
                    objOptionXmlElement = objXmlDocument.CreateElement("option");
                    objOptionXmlElement.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue(0)));
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue(1));
                    objChild.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                }

                if (!isRecordsFound)
                {
                    throw new RMAppException(Globalization.GetString("TaskManager.GetPolicySystem.NoPolicySystemsDefined", m_iClientId));
                }
            }

            catch (RMAppException e)
            {
                throw e;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("TaskManager.GetPolicySystem.PolicySysFetchingError", m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                objOptionXmlElement = null;
                objRootElement = null;
                 objChild = null;
          
            }
            return objXmlDocument;
        }

        // akaushik5 Added for MITS 36381 Starts
        /// <summary>
        /// Updates the resbal option.
        /// </summary>
        /// <param name="objXmlDoc">The object XML document.</param>
        private void UpdateResbalOption(XmlDocument objXmlDoc, XmlDocument p_objXmlDoc)
        {
            if (!object.ReferenceEquals(objXmlDoc.SelectSingleNode("//Args"), null))
            {
                objXmlDoc.DocumentElement.RemoveChild(objXmlDoc.SelectSingleNode("//Args"));
            }
         
            this.CreateResbalOption(objXmlDoc, p_objXmlDoc);
        }

        /// <summary>
        /// Creates the resbal option.
        /// </summary>
        /// <param name="objXmlDoc">The object XML document.</param>
        private void CreateResbalOption(XmlDocument objXmlDoc, XmlDocument p_objXmlDoc)
        {
            XmlElement objTmpElement = null;
            XmlElement objArgElement = null;
            objTmpElement = objXmlDoc.CreateElement("Args");
            objXmlDoc.DocumentElement.AppendChild(objTmpElement);

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.InnerText = string.Format("-ds {0}", m_sDataSourceName);
            objTmpElement.AppendChild(objArgElement);

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.InnerText = string.Format("-ru {0}", m_sLoginName);
            objTmpElement.AppendChild(objArgElement);

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.SetAttribute("type", "password");
            objArgElement.InnerText = RMCryptography.EncryptString(string.Format("-rp {0}", RMX_TM_PWD));
            objTmpElement.AppendChild(objArgElement);

            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.SetAttribute("type", "Resbal");
            objArgElement.InnerText = "-j jobid";
            objTmpElement.AppendChild(objArgElement);

            XmlNode objNode = p_objXmlDoc.SelectSingleNode("//ResbalOption");
            if (objNode != null)
            {
                string resBalOption = objNode.InnerText;
                if (!string.IsNullOrEmpty(resBalOption))
                {
                    objArgElement = objXmlDoc.CreateElement("arg");
                    objArgElement.SetAttribute("type", "ResbalOption");
                    objArgElement.InnerText = string.Format("-op {0}", resBalOption);
                    objTmpElement.AppendChild(objArgElement);

                    switch (resBalOption)
                    {
                        case "1":
                            break;
                        case "2":
                            objNode = p_objXmlDoc.SelectSingleNode("//ResbalClaimNumber");
                            if (objNode != null)
                            {
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "ClaimNumber");
                                objArgElement.InnerText = string.Format("-cn {0}", objNode.InnerText);
                                objTmpElement.AppendChild(objArgElement);
                            }
                            break;

                        case "3":
                            objNode = p_objXmlDoc.SelectSingleNode("//FromDate");
                            if (objNode != null)
                            {
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "FromDate");
                                objArgElement.InnerText = string.Format("-fd {0}", objNode.InnerText);
                                objTmpElement.AppendChild(objArgElement);
                            }

                            objNode = p_objXmlDoc.SelectSingleNode("//ToDate");
                            if (objNode != null)
                            {
                                objArgElement = objXmlDoc.CreateElement("arg");
                                objArgElement.SetAttribute("type", "ToDate");
                                objArgElement.InnerText = string.Format("-td {0}", objNode.InnerText);
                                objTmpElement.AppendChild(objArgElement);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            //Add by kuladeep for Cloud-Start
            objArgElement = objXmlDoc.CreateElement("arg");
            objArgElement.SetAttribute("type", "clientid");
            objArgElement.InnerText = string.Format("-ci {0}", Convert.ToString(m_iClientId));
            objTmpElement.AppendChild(objArgElement);
            //Add by kuladeep for Cloud-End

        }

        /// <summary>
        /// Loads the resbal options.
        /// </summary>
        /// <param name="objXmlDoc">The object XML document.</param>
        /// <param name="objTmpElement">The object temporary element.</param>
        /// <param name="objTmpXmlDoc">The object temporary XML document.</param>
        private void LoadResbalOptions(XmlDocument objXmlDoc, XmlElement objTmpElement, XmlDocument objTmpXmlDoc)
        {
            XmlElement objTmp = null;
            XmlElement objTmpArgElement = null;

            objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ResbalOption']");
            if (objTmpArgElement != null && !string.IsNullOrEmpty(objTmpArgElement.InnerXml))
            {
                string resbalOption = objTmpArgElement.InnerText.Contains("-op") ? objTmpArgElement.InnerText.Remove(0, 3).Trim() : objTmpArgElement.InnerText;
                objTmp = objXmlDoc.CreateElement("ResbalOption");
                objTmp.InnerXml = resbalOption;
                objTmpElement.AppendChild(objTmp);

                switch (resbalOption)
                {
                    case "1":
                        break;
                    case "2":
                        objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ClaimNumber']");
                        if (objTmpArgElement != null && !string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        {
                            objTmp = objXmlDoc.CreateElement("ResbalClaimNumber");
                            objTmp.InnerXml = objTmpArgElement.InnerText.Contains("-cn") ? objTmpArgElement.InnerText.Remove(0, 3).Trim() : objTmpArgElement.InnerText;
                            objTmpElement.AppendChild(objTmp);
                        }
                        break;
                    case "3":
                        objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='FromDate']");
                        if (objTmpArgElement != null && !string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        {
                            objTmp = objXmlDoc.CreateElement("FromDate");
                            objTmp.InnerXml = objTmpArgElement.InnerText.Contains("-fd") ? objTmpArgElement.InnerText.Remove(0, 3).Trim() : objTmpArgElement.InnerText;
                            objTmpElement.AppendChild(objTmp);
                        }

                        objTmpArgElement = (XmlElement)objTmpXmlDoc.SelectSingleNode("//arg[@type='ToDate']");
                        if (objTmpArgElement != null && !string.IsNullOrEmpty(objTmpArgElement.InnerXml))
                        {
                            objTmp = objXmlDoc.CreateElement("ToDate");
                            objTmp.InnerXml = objTmpArgElement.InnerText.Contains("-td") ? objTmpArgElement.InnerText.Remove(0, 3).Trim() : objTmpArgElement.InnerText;
                            objTmpElement.AppendChild(objTmp);
                        }
                        break;
                }
            }
        }
// akaushik5 Added for MITS 36381 Ends
    }

    public struct ScheduleDetails
    {
        private int m_iScheduleId;
        private int m_iScheduleState;
        private int m_iTaskTypeId;
        private int m_iScheduleTypeId;
        private int m_iIntervalTypeId;
        private int m_iInterval;
        private string m_sTrigDirectory;
        private string m_sTimeToRun ;
        private int m_iDayOfMonth;
        private string m_sNextRunDTTM;
        private string m_sFinalRunDTTM;
        private string m_sConfig;
        private int m_bMon;
        private int m_bTue;
        private int m_bWed;
        private int m_bThu;
        private int m_bFri;
        private int m_bSat;
        private int m_bSun;
        private int m_bJan;
        private int m_bFeb;
        private int m_bMar;
        private int m_bApr;
        private int m_bMay;
        private int m_bJun;
        private int m_bJul;
        private int m_bAug;
        private int m_bSep;
        private int m_bOct;
        private int m_bNov;
        private int m_bDec;
        private string m_sTaskName;
        private int m_iOptionsetId;

        public int ScheduleId { get { return m_iScheduleId; } set { m_iScheduleId = value; } }
        public int ScheduleState { get { return m_iScheduleState; } set { m_iScheduleState = value; } }
        public int TaskTypeId { get { return m_iTaskTypeId; } set { m_iTaskTypeId = value; } }
        public int ScheduleTypeId { get { return m_iScheduleTypeId; } set { m_iScheduleTypeId = value; } }
        public int IntervalTypeId { get { return m_iIntervalTypeId; } set { m_iIntervalTypeId = value; } }
        public int Interval { get { return m_iInterval; } set { m_iInterval = value; } }
        public string TrigDirectory { get { return m_sTrigDirectory; } set { m_sTrigDirectory = value; } }
        public string TimeToRun { get { return m_sTimeToRun; } set { m_sTimeToRun = value; } }
        public int DayOfMonth { get { return m_iDayOfMonth; } set { m_iDayOfMonth = value; } }
        public string NextRunDTTM { get { return m_sNextRunDTTM; } set { m_sNextRunDTTM = value; } }
        public string FinalRunDTTM { get { return m_sFinalRunDTTM; } set { m_sFinalRunDTTM = value; } }
        public string Config { get { return m_sConfig; } set { m_sConfig = value; } }
        public int Mon { get { return m_bMon; } set { m_bMon = value; } }
        public int Tue { get { return m_bTue; } set { m_bTue = value; } }
        public int Wed { get { return m_bWed; } set { m_bWed = value; } }
        public int Thu { get { return m_bThu; } set { m_bThu = value; } }
        public int Fri { get { return m_bFri; } set { m_bFri = value; } }
        public int Sat { get { return m_bSat; } set { m_bSat = value; } }
        public int Sun { get { return m_bSun; } set { m_bSun = value; } }
        public int Jan { get { return m_bJan; } set { m_bJan = value; } }
        public int Feb { get { return m_bFeb; } set { m_bFeb = value; } }
        public int Mar { get { return m_bMar; } set { m_bMar = value; } }
        public int Apr { get { return m_bApr; } set { m_bApr = value; } }
        public int May { get { return m_bMay; } set { m_bMay = value; } }
        public int Jun { get { return m_bJun; } set { m_bJun = value; } }
        public int Jul { get { return m_bJul; } set { m_bJul = value; } }
        public int Aug { get { return m_bAug; } set { m_bAug = value; } }
        public int Sep { get { return m_bSep; } set { m_bSep = value; } }
        public int Oct { get { return m_bOct; } set { m_bOct = value; } }
        public int Nov { get { return m_bNov; } set { m_bNov = value; } }
        public int Dec { get { return m_bDec; } set { m_bDec = value; } }
        public string TaskName { get { return m_sTaskName; } set { m_sTaskName = value; } }
        public int OptionsetId { get { return m_iOptionsetId; } set { m_iOptionsetId = value; } }
    }

    public struct JobDetails
    {
        private int m_iJobId;
        private int m_iTaskTypeId;
        private int m_iJobStateId;
        private string m_sStartDTTM;
        private string m_sEndDTTM;
        public int JobId { get { return m_iJobId; } set { m_iJobId = value; } }
        public int TaskTypeId { get { return m_iTaskTypeId; } set { m_iTaskTypeId = value; } }
        public int JobStateId { get { return m_iJobStateId; } set { m_iJobStateId = value; } }
        public string StartDTTM { get { return m_sStartDTTM; } set { m_sStartDTTM = value; } }
        public string EndDTTM { get { return m_sEndDTTM; } set { m_sEndDTTM = value; } }
    }
}
