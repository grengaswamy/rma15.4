﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Text;
using System.Collections;
using Riskmaster.DataModel;
using System.Collections.Generic;
using System.Linq;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Neha Goel MITS# 33409 WWIG Gap 10
    ///Dated   :   03 March 2014
    ///Purpose :  Functions for Restrcited claim sysytem wide users
    /// </summary>
   public class RestrictedClaimsUserOverride
    {
        /// <summary>
        /// Dsn ID
        /// </summary>
        int m_iDSNId = 0;

       

        /// <summary>
        /// Connection String
        /// </summary>
        string m_sConnStr = "";

        /// <summary>
        /// Security Dsn
        /// </summary>
        string m_sSecDsn = "";

        /// <summary>
        /// User Login Id
        /// </summary>
        int m_iUserId = 0;

        
       /// <summary>
       /// UserName
       /// </summary>
       string m_sUserName = null;
       /// <summary>
       /// DSN Name
       /// </summary>
       string m_sDsnName = null;
       /// <summary>
       /// User Login Password
       /// </summary>
       string m_sPassword = null;
       /// <summary>
       /// User's First name
       /// </summary>
       string m_sUserFirstName = null;
       /// <summary>
       /// users Last name
       /// </summary>
       string m_sUserLastName = null;
       /// <summary>
       /// Document Storage Path
       /// </summary>
       string m_sDocumentPath = null;
       /// <summary>
       /// Group Id
       /// </summary>
       string m_sGroupId = null;       
   
       /// <summary>
       /// Initialize 
       /// </summary>
       /// <param name="p_sUserName"></param>
       /// <param name="p_sPassword"></param>
       /// <param name="p_sDatabaseName"></param>
        public RestrictedClaimsUserOverride(string p_sUserName, string p_sPassword, string p_sDatabaseName)
        {
            UserLogin objUserLogin = new UserLogin(p_sUserName, p_sPassword, p_sDatabaseName,0);
            m_iDSNId = objUserLogin.objRiskmasterDatabase.DataSourceId;
            m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sSecDsn = Security.SecurityDatabase.GetSecurityDsn(0);
            m_iUserId = objUserLogin.UserId;    
            m_sUserName = p_sUserName;
            m_sUserFirstName = objUserLogin.objUser.FirstName;
            m_sUserLastName = objUserLogin.objUser.LastName;
            m_sDsnName = objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sDocumentPath= objUserLogin.DocumentPath;
            m_sPassword = p_sPassword;
            m_sGroupId = objUserLogin.GroupId.ToString();
           
            objUserLogin = null;
        }       

       /// <summary>
       /// For saving the system wide users of restricted claim
       /// </summary>
       /// <param name="p_objXmlDoc"></param>
       /// <returns></returns>
        public XmlDocument Save(XmlDocument p_objXmlDoc)
        {
            string sSql = null;
            StringBuilder sbSql = null;            
            DbConnection objConn = null;
            string[] arrUsers ;
            string sSelectedUsers = null;

            //int irowId = 0; //neha goel - 04182014 Swiss re commenting below code as row_id is now an identity column
            try
            {
             
                if (p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsersID") != null)
                {
                    sSelectedUsers = p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsersID").InnerText;
                }

                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();

                sSql = "DELETE FROM CLM_RSTRCTD_USR_OVRD WHERE CLAIM_ID = 0"; //Neha goel MITS# 34287 for Swiss re gap 29: Fetching only system wide users for restricted claims by using Claim_id = 0 in where clause
                objConn.ExecuteNonQuery(sSql);

                //neha goel - 04182014 MITS# 34287 Swiss re: commenting below code as row_id is now an identity column
                //sSql = string.Empty;
                //sSql = "SELECT MAX(ROW_ID) FROM CLM_RSTRCTD_USR_OVRD";
                //irowId = objConn.ExecuteInt(sSql);
                arrUsers = sSelectedUsers.Split(' ');
               
                //HashSet<string> hash = new HashSet<string>(arrUsers);
                //arrUsers = hash.ToArray();                
                sbSql = new StringBuilder();
                if (arrUsers.Length > 0)
                    {
                        for (int i = 0; i < arrUsers.Length; i++)
                        {
                            if (arrUsers[i].ToString().Trim() != "")
                            {
                                //irowId++; //neha goel - 04182014 Swiss re commenting below code as row_id is now an identity column
                                sbSql.Length = 0;
                                sbSql.Append("INSERT INTO CLM_RSTRCTD_USR_OVRD VALUES (");
                                //neha goel - 04182014 Swiss re MITS# 34287: commenting below code as row_id is now an identity column
                                    //sbSql.Append(irowId + "," + arrUsers[i].ToString()  + ",0)");
                                sbSql.Append(arrUsers[i].ToString() + ",0)");
                                //objConn.ExecuteNonQuery(sbSql.ToString());
                                DbFactory.ExecuteNonQuery(m_sConnStr,sbSql.ToString());
                            }
                        }
                    }

                if (p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsersID") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsersID").InnerText = "";
                }

                if (p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsers") != null)
                {
                    p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsers").InnerText = "";
                }

                p_objXmlDoc = Get(p_objXmlDoc);

                return p_objXmlDoc;

            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("RestrictedClaimsUsersOverride.Save.Error"), p_objEx);
            }
            finally
            {
                if(objConn != null)
                     objConn.Dispose();
            }

           
        }

       /// <summary>
       /// For fetching the saved system wide users of restricted claim
       /// </summary>
       /// <param name="p_objXmlDoc"></param>
       /// <returns></returns>
        public XmlDocument Get(XmlDocument p_objXmlDoc)
        {
       
            string sSql = null;
            XmlElement objSelectElement = null;
            XmlElement objRowTxt = null;
            DbReader objReader = null;
            LocalCache objCache= null;
            string sUserIds = string.Empty;
            string sUserName = string.Empty;

            try
            {
                objCache =   new LocalCache(m_sConnStr);
                objSelectElement = (XmlElement)p_objXmlDoc.SelectSingleNode("/form/group/displaycolumn/control/UserList");
                sSql = "SELECT USER_ID FROM CLM_RSTRCTD_USR_OVRD WHERE CLAIM_ID = 0"; //Neha goel MITS# 34287 for Swiss re gap 29: Fetching only system wide users for restricted claims by using Claim_id = 0 in where clause
                objReader = DbFactory.GetDbReader(m_sConnStr, sSql);
                while (objReader.Read())
                {
                    objRowTxt = p_objXmlDoc.CreateElement("UserListoption");
                    objRowTxt.SetAttribute("value", objReader["USER_ID"].ToString());
                    objRowTxt.InnerText = UTILITY.GetUser((objReader.GetInt("USER_ID")), Convert.ToString(m_iDSNId), m_sSecDsn);

                         if (string.IsNullOrEmpty(sUserIds))
                             sUserIds = objReader["USER_ID"].ToString();
                         else
                             sUserIds = sUserIds + " " + objReader["USER_ID"].ToString();

                         if (string.IsNullOrEmpty(sUserName))
                             //sUserName = objCache.GetSystemLoginName(objReader.GetInt("USER_ID"), m_iDSNId, m_sSecDsn);
                             sUserName = sUserName + " " + UTILITY.GetUser((objReader.GetInt("USER_ID")), Convert.ToString(m_iDSNId), m_sSecDsn);
                         else
                             //sUserName = sUserName + " " + objCache.GetSystemLoginName(objReader.GetInt("USER_ID"), m_iDSNId, m_sSecDsn);
                             sUserName = sUserName + " " + UTILITY.GetUser((objReader.GetInt("USER_ID")), Convert.ToString(m_iDSNId), m_sSecDsn);                                                  
                    
                    objSelectElement.AppendChild((XmlNode)objRowTxt);
                }

                p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsersID").InnerText = sUserIds;

                p_objXmlDoc.SelectSingleNode("/form/group/RestrictedClaimsUsersOverride/SelectedUsers").InnerText = sUserName;
                                             
                return p_objXmlDoc;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("RestrictedClaimsUsersOverride.GetData.Error"), p_objEx);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if(objCache != null)
                {
                    objCache.Dispose();
                }
            }

        }
    }
}
