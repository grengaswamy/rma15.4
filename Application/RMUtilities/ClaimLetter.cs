﻿
using System;
using System.Xml;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
    /// Name		: ClaimLetter
    /// Author		: Mridul Bansal
    /// Date Created: 06/19/2009
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// (Application Layer) Stores data from Claim Letter Setup in Utilties
    /// </summary>
    public class ClaimLetter: UtilitiesBase 
    {

            /// <summary>
            /// Two dimensional array for the fields mapping
            /// </summary>
            private string[,] arrFields = {			    
			    {"RowId","ROW_ID"},						
			    {"Adjusters","ADJUSTERS"},									
                {"EmailFrom","EMAILFROIACORDFROM"},	
	        };
            private string m_sUserName = "";
            private int m_iClientId = 0; //rkaur27
            /// <summary>
            /// 
            /// </summary>
            /// <param name="p_sConnString"></param>
            public ClaimLetter(string p_sConnString, int p_iClientId):base(p_sConnString, p_iClientId) //rkaur27
		    {
			    ConnectString = p_sConnString;
                m_iClientId = p_iClientId; //rkaur27
			    this.Initialize(); 
		    }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="p_sConnString"></param>
            /// <param name="p_sUserName"></param>
            public ClaimLetter(string p_sConnString, string p_sUserName, int p_iClientId)//rkaur27
                : base(p_sConnString, p_sUserName, p_iClientId)
            {
                ConnectString = p_sConnString;
                m_sUserName = p_sUserName;
                m_iClientId = p_iClientId; //rkaur27
                this.Initialize();
            }

            /// <summary>
            /// Initialize the variables
            /// </summary>
            new private void Initialize()
            {
                TableName = "EMAIL_FROI_ACORD";
                KeyField = "ROW_ID";
                base.InitFields(arrFields);
                base.Initialize();
            }

            /// <summary>
            /// Get method returns the data
            /// </summary>
            /// <param name="p_objDoc">Input xml document with form structure</param>
            /// <returns>Xml document with data and structure</returns>
            public XmlDocument Get(XmlDocument p_objDoc)
            {
                LocalCache objCache = null;
                XmlNode objNode = null;
                XmlNode objIncNode = null;
                DbReader objReader = null;                
                string sAdjusterList = "";                
                int iTableId = 0 ;                
                XmlElement objElement=null;
                XmlElement objIncElement = null;			    
			    DataSet objDS=null;			    
			    StringBuilder sbSql=null;			    
			    XmlDocument objXmlDocument=null;

                string sPrinterName = string.Empty;
                string sClosedStatus = string.Empty;
                string sXClaimType = string.Empty;
                string sShortCode = string.Empty;
                string sCodeDesc = string.Empty;
                string sACKLtrTmplId = "-1";
                string sCLLtrTmplId = "-1";
                string sTmp = string.Empty;
                string sXClaimTypeList = string.Empty;
                string sAdjustersList = string.Empty;
                string sClaimStatusList = string.Empty;
                SysSettings objSettings = null;

                try
                {
                    objCache = new LocalCache(ConnectString, m_iClientId);
                    iTableId = objCache.GetTableId("ADJUSTERS");
                    objSettings = new SysSettings(ConnectString, m_iClientId);
                    objXmlDocument = new XmlDocument();
                    sbSql = new StringBuilder();
                    sbSql.Append("<Adjuster>");
                    sbSql.Append("<control name='ExcAdjusters' type='combobox' title='Excluded Adjusters:'> </control>");
                    sbSql.Append("<control name='IncAdjusters' type='combobox' title='Included Adjusters:'> </control>");
                    sbSql.Append("<control name='EmailFrom' type='text' title='Sender Email Address'/>");
                    sbSql.Append("<control name='XClaimTypes' type='' codeid='' />");
                    sbSql.Append("<control name='ClaimStatus' type='' codeid=''/>");
                    sbSql.Append("<control name='ACKTemplates' type='combobox' title='ACK Letter' value=''/>");
                    sbSql.Append("<control name='CLTemplates' type='combobox' title='Closed Letter' value=''/>");
                    sbSql.Append("<control name='PrinterName' type='text' title='Printer'/>");
                    sbSql.Append("</Adjuster>");
                    objXmlDocument.LoadXml(sbSql.ToString());

                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT ADJUSTERS, PRINTER_SETTING, CLOSED_STATUS, X_CLAIM_TYPE_CODE, ACKLTR_TMPL_ID, CLLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR");
                    using (objReader = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString()))
                    {
                        if (objReader.Read())
                        {
                            sAdjusterList = Conversion.ConvertObjToStr(objReader.GetString("ADJUSTERS"));
                            sPrinterName = Conversion.ConvertObjToStr(objReader.GetString("PRINTER_SETTING"));
                            sClosedStatus = Conversion.ConvertObjToStr(objReader.GetString("CLOSED_STATUS"));
                            sXClaimType = Conversion.ConvertObjToStr(objReader.GetString("X_CLAIM_TYPE_CODE"));
                            sACKLtrTmplId = Conversion.ConvertObjToStr(objReader.GetInt("ACKLTR_TMPL_ID"));
                            sCLLtrTmplId = Conversion.ConvertObjToStr(objReader.GetInt("CLLTR_TMPL_ID"));
                        }
                    }

                    //Printer
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='PrinterName']");
                    objNode.InnerText = sPrinterName;

                    //Excluded Claim Types
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='XClaimTypes']");

                    //Modified as Table changed to SYS_UTIL_CLM_LTR
                    //Data now not stored as string, stored as IDs
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT CODE_EID FROM SYS_UTIL_CLM_LTR,SYS_PARMS_CLM_LTR WHERE SYS_UTIL_CLM_LTR.ROW_ID = " + Conversion.ConvertStrToInteger(sXClaimType));
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        sXClaimTypeList = sXClaimTypeList + "," + Conversion.ConvertObjToStr(dr["CODE_EID"]) + ",";
                    }
                    //Modified as Table changed to SYS_UTIL_CLM_LTR
                    //Data now not stored as string, stored as IDs
                    //string[] sCtypes = sXClaimType.Split(',');
                    string[] sCtypes = sXClaimTypeList.Split(',');
                    foreach (string sCType in sCtypes)
                    {
                        if (sCType != "")
                        {
                            objIncElement = objXmlDocument.CreateElement("option");
                            objIncElement.SetAttribute("value", sCType);
                            objNode.Attributes.GetNamedItem("codeid").Value = objNode.Attributes.GetNamedItem("codeid").Value + " " + sCType;
                            objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sCType), ref sShortCode, ref sCodeDesc);
                            objIncElement.InnerText = sShortCode + " " + sCodeDesc;
                            objNode.AppendChild(objIncElement);
                        }
                    }

                    //Claim Status
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='ClaimStatus']");

                    //Modified as Table changed to SYS_UTIL_CLM_LTR
                    //Data now not stored as string, stored as IDs
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT CODE_EID FROM SYS_UTIL_CLM_LTR,SYS_PARMS_CLM_LTR WHERE SYS_UTIL_CLM_LTR.ROW_ID = " + Conversion.ConvertStrToInteger(sClosedStatus));
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        sClaimStatusList = sClaimStatusList + "," + Conversion.ConvertObjToStr(dr["CODE_EID"]) + ",";
                    }

                    //Commented as Table has been modified
                    //string[] sCStatuses = sClosedStatus.Split(',');
                    string[] sCStatuses = sClaimStatusList.Split(',');
                    foreach (string sCStatus in sCStatuses)
                    {
                        if (sCStatus != "")
                        {
                            objIncElement = objXmlDocument.CreateElement("option");
                            objIncElement.SetAttribute("value", sCStatus);
                            objNode.Attributes.GetNamedItem("codeid").Value = objNode.Attributes.GetNamedItem("codeid").Value + " " + sCStatus;
                            objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sCStatus), ref sShortCode, ref sCodeDesc);
                            objIncElement.InnerText = sShortCode + " " + sCodeDesc;
                            objNode.AppendChild(objIncElement);
                        }
                    }

                    //Adjusters
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='IncAdjusters']");
                    objIncNode = objXmlDocument.SelectSingleNode("//control[@name='ExcAdjusters']");

                    //Modified as Table changed to SYS_UTIL_CLM_LTR
                    //Data now not stored as string, stored as IDs
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT CODE_EID FROM SYS_UTIL_CLM_LTR,SYS_PARMS_CLM_LTR WHERE SYS_UTIL_CLM_LTR.ROW_ID = " + Conversion.ConvertStrToInteger(sAdjusterList));
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        sAdjustersList = sAdjustersList + "," + Conversion.ConvertObjToStr(dr["CODE_EID"]) + ",";
                    }
                    //End

                    sbSql = new StringBuilder();
                    if (objSettings.UseEntityRole)
                    {
                        sbSql.Append("SELECT FIRST_NAME, LAST_NAME, ENTITY_ID FROM ENTITY WHERE   ENTITY_ID IN (SELECT DISTINCT ADJUSTER_EID FROM CLAIM_ADJUSTER) AND ");
                    }
                    else
                    { 
                        sbSql.Append("SELECT FIRST_NAME, LAST_NAME, ENTITY_ID FROM ENTITY WHERE ENTITY_TABLE_ID = " + iTableId + " AND "); 
                    }
                    sbSql.Append(" DELETED_FLAG=0");//IJHA mits : 23164
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        //Modified the List
                        //if (sAdjusterList.Contains("," + Conversion.ConvertObjToStr(dr["ENTITY_ID"]) + ","))
                        if (sAdjustersList.Contains("," + Conversion.ConvertObjToStr(dr["ENTITY_ID"]) + ","))
                        {
                            objElement = objXmlDocument.CreateElement("option");
                            objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["ENTITY_ID"]));
                            objElement.InnerText = Conversion.ConvertObjToStr(dr["LAST_NAME"]) + "," + Conversion.ConvertObjToStr(dr["FIRST_NAME"]);
                            objNode.AppendChild(objElement);
                        }
                        else
                        {
                            objIncElement = objXmlDocument.CreateElement("option");
                            objIncElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["ENTITY_ID"]));
                            objIncElement.InnerText = Conversion.ConvertObjToStr(dr["LAST_NAME"]) + "," + Conversion.ConvertObjToStr(dr["FIRST_NAME"]);
                            objIncNode.AppendChild(objIncElement);
                        }
                    }

                    //Merge Templates for ACK Claim Letter
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='ACKTemplates']");
                    objNode.Attributes.GetNamedItem("value").Value = sACKLtrTmplId;
                    objElement = objXmlDocument.CreateElement("option");
                    objElement.SetAttribute("value", "");
                    objElement.InnerText = "";
                    objNode.AppendChild(objElement);
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT FORM_ID, FORM_NAME FROM MERGE_FORM WHERE CAT_ID = 1 ORDER BY FORM_NAME ASC");
                    using (objReader = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString()))
                    {
                        while (objReader.Read())
                        {
                            objElement = objXmlDocument.CreateElement("option");
                            sTmp = Conversion.ConvertObjToStr(objReader.GetValue("FORM_ID"));
                            objElement.SetAttribute("value", sTmp);
                            objElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME"));
                            objNode.AppendChild(objElement);
                            
                        }
                    }

                    //Merge Templates for Closed Claim Letter
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='CLTemplates']");
                    objNode.Attributes.GetNamedItem("value").Value = sCLLtrTmplId;
                    objElement = objXmlDocument.CreateElement("option");
                    objElement.SetAttribute("value", "");
                    objElement.InnerText = "";
                    objNode.AppendChild(objElement);
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT FORM_ID, FORM_NAME FROM MERGE_FORM WHERE CAT_ID = 1 ORDER BY FORM_NAME ASC");
                    using (objReader = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString()))
                    {
                        while (objReader.Read())
                        {
                            objElement = objXmlDocument.CreateElement("option");
                            sTmp = Conversion.ConvertObjToStr(objReader.GetValue("FORM_ID"));
                            objElement.SetAttribute("value", sTmp);
                            objElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME"));
                            objNode.AppendChild(objElement);
                        }
                    }
                    return objXmlDocument;
                }
                catch (Exception p_objEx)
                {
                    throw new RMAppException(Globalization.GetString("ClaimLetter.Get.Err", m_iClientId), p_objEx);
                }
                finally
                {                    
                    if (objCache != null)
                        objCache.Dispose();
                    if (sbSql != null)
                        sbSql = null;
                    if (objReader != null)
                        objReader = null;
                }
            }

            /// Name		: Save
            /// Author		: Mridul Bansal
            /// Date Created: 06/19/2009
            ///************************************************************
            /// Amendment History
            ///************************************************************
            /// Date Amended   *   Amendment   *    Author
            ///************************************************************
            /// <summary>
            /// Saves the xml document with data and the form structure
            /// </summary>
            /// <param name="p_objXmlDocument">Input xml document with data</param>
            /// <returns>Saved data</returns>
            public XmlDocument Save(XmlDocument p_objXmlDocument)
            {
                DbConnection objCon = null;
                XmlElement objElm = null;
                string[] arrIds = null;
                StringBuilder sbSql = null;
                StringBuilder sbTmp = null;
                int iCtr = 0;
                string[] sAdjusterList;
                string[] sXClaimTypeList;
                string[] sXClaimStatusList;
                string sSQL = string.Empty;

                try
                {
                    XMLDoc = p_objXmlDocument;                    
                    
                    //iCtr = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(ConnectString, "SELECT COUNT(ADJUSTERS), m_iClientId FROM SYS_PARMS_CLM_LTR"));                                        
                    iCtr = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(ConnectString, "SELECT COUNT(*) FROM SYS_PARMS_CLM_LTR"), m_iClientId);
                    
                    sbSql = new StringBuilder();
                    if (iCtr == 0)
                    {
                        sbSql.Append("INSERT INTO SYS_PARMS_CLM_LTR (DTTM_RCD_LAST_UPD, UPDATED_BY_USER) VALUES ('");
                        sbSql.Append(Conversion.ToDbDateTime(DateTime.Now) + "', '" + m_sUserName + "')");
                        DbFactory.ExecuteScalar(ConnectString, sbSql.ToString());
                    }

                    sbSql = new StringBuilder();
                    sbSql.Append("UPDATE SYS_PARMS_CLM_LTR SET ADJUSTERS = ");
                    //Adjuster

                    //Commented By NAvdeep - Only ID will be stored in SYS_PARMS_CLM_LTR
                    ////objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ExcAdjusters']");
                    //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='IncAdjusters']");
                    //if (objElm != null)
                    //    sbSql.Append("'," + objElm.InnerText.Replace(" ", ",") + ",'");                    
                    
                    //Adjuster ID is 1 which will be mapped with SYS_UTIL_CLM_LTR table
                    sbSql.Append("'1'");

                    //Printer
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PrinterName']");
                    if (objElm != null)
                        sbSql.Append(", PRINTER_SETTING = '" + objElm.InnerText.Replace(" ", ",") + "'");

                    //ACK Letter Template
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ACKTemplates']");
                    if (objElm != null)
                        sbSql.Append(", ACKLTR_TMPL_ID = '" + objElm.GetAttribute("value").ToString().Replace(" ", ",") + "'");

                    //Closed Letter Template
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CLTemplates']");
                    if (objElm != null)
                        sbSql.Append(", CLLTR_TMPL_ID = '" + objElm.GetAttribute("value").ToString().Replace(" ", ",") + "'");

                    //Claim Type
                    //Commented By NAvdeep - Only ID will be stored in SYS_PARMS_CLM_LTR

                    //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='XClaimTypes']");
                    //if (objElm != null)
                    //{
                    //    if (!objElm.GetAttribute("codeid").Trim().Equals(""))
                    //    {
                    //        arrIds = objElm.GetAttribute("codeid").Split(' '.ToString().ToCharArray());
                    //        sbTmp = new StringBuilder();
                    //        sbTmp.Append(",");
                    //        foreach (string sTemp in arrIds)
                    //        {
                    //            if (!sTemp.Trim().Equals(""))
                    //                sbTmp.Append(sTemp + ",");
                    //        }
                    //        sbSql.Append(", X_CLAIM_TYPE_CODE = '" + sbTmp + "'");
                    //    }
                    //    else //Append Blank if its No Claim Type Code is selected                        
                    //        sbSql.Append(", X_CLAIM_TYPE_CODE = ''");
                        
                    //}

                    //Claim Type ID is 2 which will be mapped with SYS_UTIL_CLM_LTR table
                    sbSql.Append(",X_CLAIM_TYPE_CODE = '2'");

                    //Claim Status
                    
                    //Commented By NAvdeep - Only ID will be stored in SYS_PARMS_CLM_LTR
                    //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ClaimStatus']");
                    //if (objElm != null)
                    //{
                    //    if (!objElm.GetAttribute("codeid").Trim().Equals(""))
                    //    {
                    //        sbTmp = new StringBuilder();
                    //        sbTmp.Append(",");
                    //        arrIds = objElm.GetAttribute("codeid").Split(' '.ToString().ToCharArray());
                    //        foreach (string sTemp in arrIds)
                    //        {
                    //            if (!sTemp.Trim().Equals(""))
                    //                sbTmp.Append(sTemp + ",");
                    //        }
                    //        sbSql.Append(", CLOSED_STATUS = '" + sbTmp + "'");
                    //    }
                    //    else //Append Blank if its No Claim Status is selected
                    //        sbSql.Append(", CLOSED_STATUS = ''");
                    //}
                    
                    //CLaim Status ID is 3 which will be mapped with SYS_UTIL_CLM_LTR table
                    sbSql.Append(",CLOSED_STATUS = '3'");

                    using (objCon = DbFactory.GetDbConnection(ConnectString))
                    {
                        objCon.Open();
                        sbSql.Append(", DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(DateTime.Now) + "', UPDATED_BY_USER = '" + m_sUserName + "'");
                        objCon.ExecuteNonQuery(sbSql.ToString());
                        objCon.Close();
                    }

                    //ADDING The adjuster EIDs value in table "SYS_UTIL_CLM_LTR"
                    using (objCon = DbFactory.GetDbConnection(ConnectString))
                    {
                        objCon.Open();
                        objCon.ExecuteNonQuery("DELETE FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 1");
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='IncAdjusters']");
                        if (objElm != null)
                        {
                            objElm.InnerText = objElm.InnerText.Trim();
                            sAdjusterList = objElm.InnerText.Split(' ');
                            foreach (string sAdjusterEid in sAdjusterList)
                            {
                                if (!string.IsNullOrEmpty(sAdjusterEid))
                                {
                                    sSQL = "INSERT INTO SYS_UTIL_CLM_LTR VALUES(1, 'ADJUSTER'," + sAdjusterEid + " )";
                                    objCon.ExecuteNonQuery(sSQL);
                                }
                            }
                        }
                    }


                    // //ADDING The Claim Type IDs value in table "SYS_UTIL_CLM_LTR" 
                    using (objCon = DbFactory.GetDbConnection(ConnectString))
                    {
                        objCon.Open();
                        objCon.ExecuteNonQuery("DELETE FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 2");
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='XClaimTypes']");
                        if (objElm != null)
                        {
                            if (!objElm.GetAttribute("codeid").Trim().Equals(""))
                            {
                                arrIds = objElm.GetAttribute("codeid").Split(' '.ToString().ToCharArray());
                                foreach (string sTemp in arrIds)
                                {
                                    if (!string.IsNullOrEmpty(sTemp))
                                    {
                                        sSQL = "INSERT INTO SYS_UTIL_CLM_LTR VALUES(2, 'CLAIM TYPE'," + sTemp + " )";
                                        objCon.ExecuteNonQuery(sSQL);
                                    }
                                }

                            }
                        }
                    }

                    //Claim Status
                    // //ADDING The Closed Claim Status IDs value in table "SYS_UTIL_CLM_LTR" 
                    using (objCon = DbFactory.GetDbConnection(ConnectString))
                    {
                        objCon.Open();
                        objCon.ExecuteNonQuery("DELETE FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 3");
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ClaimStatus']");
                        if (objElm != null)
                        {
                            if (!objElm.GetAttribute("codeid").Trim().Equals(""))
                            {
                                arrIds = objElm.GetAttribute("codeid").Split(' '.ToString().ToCharArray());
                                foreach (string sTemp in arrIds)
                                {
                                    if (!string.IsNullOrEmpty(sTemp))
                                    {
                                        sSQL = "INSERT INTO SYS_UTIL_CLM_LTR VALUES(3, 'CLOSED Status'," + sTemp + " )";
                                        objCon.ExecuteNonQuery(sSQL);
                                    }
                                }

                            }
                        }
                    }
                    

                    return XMLDoc;
                }
                catch (Exception p_objEx)
                {
                    throw new RMAppException(Globalization.GetString("ClaimLetter.Save.Err", m_iClientId), p_objEx);
                }
                finally
                {
                    if (sbSql != null)
                        sbSql = null;
                    if (sbTmp != null)
                        sbTmp = null;
                }
            }
    }
}
