﻿
/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 06/04/2014 | 33371   | ajohari2   | Added new method for GetUserList by GroupId
**********************************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using System.Data;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{

    public class CustomizeUserList : UtilityFormBase
    {
        #region Private variables
        /// <summary>
        /// Connection string
        /// </summary>
        private string m_sDSN = "";
        /// <summary>
        /// Secure DB Connection string
        /// </summary>
        private string m_sSecDSN = "";
        /// <summary>
        /// DSN id
        /// </summary>
        private string m_sDsnId = "";
        /// <summary>
        /// DB Reader for retriving group names
        /// </summary>
        DbReader m_objReader = null;
        /// <summary>
        /// User Login
        /// </summary>
        private int m_iUserId = 0;

        //Added by Nitin for R6 merging 
        string m_sManagerIds = string.Empty;

        private int m_iClientId = 0;

        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// public CustomizeUserList() { }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>

        public CustomizeUserList(string p_sDSN, string p_sDsnId, string p_sSecureDSN, int p_iUserId, int p_iClientId):base(p_sDSN, p_iClientId)
        {
            m_sDSN = p_sDSN;
            m_sDsnId = p_sDsnId;
            m_sSecDSN = p_sSecureDSN;
            m_iUserId = p_iUserId;
            m_iClientId = p_iClientId;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Initialize the properties
        /// </summary>
        new private void Initialize()
        {
            try
            {
                base.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Initialize.Error", m_iClientId), p_objEx);
            }
        }
        private string GetValue(XmlDocument p_objXmlDocument, string p_sValue)
        {
            XmlNode objNode = null;
            objNode = p_objXmlDocument.SelectSingleNode("//control[@name='" + p_sValue + "']");
            if (objNode != null)
            {
                if (objNode.InnerText != null)
                {
                    return objNode.InnerText;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        #region Added by for R5 DiaryCalendar Module by Nitin

        /// <summary>
        /// Created by Nitin fetch chain of subordinate to subordinates , headed by current user
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        private string GetSupervisoryChain(string user_id)
        {
            string queryString = string.Empty;
            DbConnection objRMConn = null;
            DbReader objReader = null;
            ArrayList subordinateUserList = null;
            string users = string.Empty;
            string tempString = string.Empty;
            try
            {
                objRMConn = DbFactory.GetDbConnection(m_sSecDSN);
                objRMConn.Open();
                queryString = "SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID ='" + user_id + "'";
                objReader = objRMConn.ExecuteReader(queryString);
                subordinateUserList = new ArrayList();

                while (objReader.Read())
                {
                    if (user_id != m_iUserId.ToString())
                    {
                        m_sManagerIds += user_id + ",";
                    }
                    tempString = objReader.GetValue("USER_ID").ToString();
                    users += tempString;
                    if (!string.IsNullOrEmpty(users))
                    {
                        users += ",";
                    }
                    {
                        string[] managers = m_sManagerIds.Split(',');
                        for (int iterator = 0; iterator < managers.Length; iterator++)
                        {
                            if (tempString.Trim() == managers[iterator].Trim())
                            {
                                throw new RMAppException(Globalization.GetString("CustomizeUserList.IncorrentManagerRelation", m_iClientId), new Exception());
                            }
                        }
                    }
                    users += GetSupervisoryChain(tempString);
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objRMConn != null)
                {
                    objRMConn.Close();
                    objRMConn = null;
                }
            }
            return users;
        }

        /// <summary>
        ///  Created by Nitin to get the list of users present in same processing office       
        /// </summary>
        /// <param name="user_id"></param>
        /// <returns></returns>
        private string GetProcessingOfficeUserList(string user_id)
        {
            string queryString = string.Empty;
            DbConnection objRMConn = null;
            DbReader objReader = null;
            StringBuilder users = null;
            try
            {
                objRMConn = DbFactory.GetDbConnection(m_sDSN);
                objRMConn.Open();

                queryString = "SELECT DISTINCT USER_ID FROM ORG_SECURITY_USER WHERE GROUP_ID IN "
                    + "(SELECT GROUP_ID FROM ORG_SECURITY_USER WHERE USER_ID = " + user_id + ") AND USER_ID <> " + user_id;

                objReader = objRMConn.ExecuteReader(queryString);

                users = new StringBuilder();
                string tempString = string.Empty;
                while (objReader.Read())
                {

                    if (objReader.GetValue("USER_ID") != null)
                    {
                        tempString = objReader.GetValue("USER_ID").ToString();
                        users.Append(tempString + ",");
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objRMConn != null)
                {
                    objRMConn.Close();
                    objRMConn = null;
                }
            }
            return users.ToString();
        }

        #endregion

        #endregion

        #region Public Functions

        /// Name		: GetUserList
        /// Author		: Rahul Solanki
        /// Date Created: 10/10/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function returns the list of system users 
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public XmlDocument GetUserList(XmlDocument p_objXmlDocument)
        {
            XmlElement objXmlElement = null;
            XmlElement objRowTxt = null;
            XmlElement objRowChild = null;
            XmlElement objLstRow = null;
            XmlNode objTempNode = null;
            XmlNode objOptionFlagNode = null;
            StringBuilder sbSql = null;
            DbConnection objConn = null;
            LocalCache objCache = null;
            XmlDocument objXmlDocument = null;
            XmlElement objParentElement = null;
            XmlElement objChildElement = null;
            XMLDoc = p_objXmlDocument;

            string sDSNno = string.Empty;
            string sDatabaseName = string.Empty;
            string sSecDSN = SecurityDatabase.GetSecurityDsn(m_iClientId);

            string sSQL = "";
            string sGroupId = "";
            string sGroupName = "";
            string sName = "";
            string sFirstName = "";
            string sLastName = "";
            string sParentForm = "";

            //peek diary
            string sTmpUserId = "";
            string sValToCompare = "";
            string sTmpUserIdPrev = "";
            int iCount = 0;
            bool bAllowGlobalPeek = false;
            string users = string.Empty;

            try
            {
                objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");
                objOptionFlagNode = p_objXmlDocument.SelectSingleNode("//OptionFlag");
                if (objTempNode != null && objOptionFlagNode != null)
                {
                    sParentForm = objTempNode.InnerText;

                    //check for email-doc has been added by Nitin for Mits  15565 on 20-Apr-2009
                    if (sParentForm == "creatediary" || sParentForm == "autodiarysetup" || sParentForm == "enhancednotes" || sParentForm == "emaildocs" || sParentForm == "email-doc" || sParentForm == "weblinksetup")//Added by Amitosh for R8 enhancement of WebLinks
                    {
                        objOptionFlagNode.InnerText = "1";
                    }
                    else
                    {
                        objOptionFlagNode.InnerText = "0";
                    }
                }

                objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");

                sbSql = new StringBuilder();

                if (sParentForm == "peekdiary")
                {
                    sTmpUserId = " " + m_iUserId + " ";
                    bAllowGlobalPeek = Conversion.ConvertLongToBool(GetSingleLong("ALLOW_GLOBAL_PEEK", "SYS_PARMS", "", m_sDSN), m_iClientId);
                    iCount = 0;

                    sbSql = new StringBuilder();

                    sbSql.Append("SELECT DISTINCT USER_DETAILS_TABLE.USER_ID,LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR ");
                    sbSql.Append("FROM USER_DETAILS_TABLE,USER_TABLE");
                    sbSql.Append(" WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                    sbSql.Append(" AND USER_DETAILS_TABLE.DSNID = (");
                    sbSql.Append(m_sDsnId);
                    sbSql.Append(")");

                    if (!bAllowGlobalPeek)
                    {
                        //sbSql.Append(" AND USER_TABLE.MANAGER_ID IN (" + sTmpUserId + ")");

                        //added by Nitin for R6 merging in order to fetch userlist for supervisory chain
                        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Peek Changes
                        users = GetSupervisoryChain(m_iUserId.ToString());
                        if (users[users.Length - 1] == ',')
                        {
                            users = users.Substring(0, (users.Length - 1));
                        }
                        sbSql.Append(" AND USER_TABLE.USER_ID IN (" + users);
                        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Peek Changes - end
                        //sbSql.Append(m_iUserId );
                        sbSql.Append(")");
                    }
                }
                else
                {
                    sbSql = new StringBuilder();
                    sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR FROM USER_DETAILS_TABLE, USER_TABLE");
                    sbSql.Append(" WHERE DSNID= ");
                    sbSql.Append(m_sDsnId);
                    sbSql.Append(" AND USER_TABLE.USER_ID <> 0 AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                }

                //added by Nitin for Mits  15565 on 20-Apr-2009
                if (sParentForm == "email-doc" || sParentForm == "emaildocs")
                {
                    if (DbFactory.IsOracleDatabase(m_sSecDSN))
                    {
                        sbSql.Append(" AND USER_TABLE.EMAIL_ADDR is not null");
                    }
                    else
                    {
                        sbSql.Append(" AND USER_TABLE.EMAIL_ADDR <> ''");
                    }
                }

                sbSql.Append(" ORDER BY USER_DETAILS_TABLE.USER_ID");
                //ended by Nitin for Mits  15565 on 20-Apr-2009

                objXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//SystemUserList");

                //Put USER_ID into a list, so we can retrive group info in much less trips to database
                StringBuilder sbUserIDs = new StringBuilder();

                //Oracle has limit of command text length, we want to split into multiple commands
                List<string> oListUserIDs = new List<string>();
                int iNumber = 0;
                using (DbReader objReader = DbFactory.ExecuteReader(m_sSecDSN, sbSql.ToString()))
                {
                    while (objReader.Read())
                    {
                        objLstRow = p_objXmlDocument.CreateElement("SystemUser");
                        sFirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                        sLastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));

                        //sName = sFirstName + "," + sLastName;
                        sName = sLastName + "," + sFirstName;
                        if (sFirstName == "")
                        {
                            sName = sLastName;
                        }
                        else if (sLastName == "")
                        {
                            sName = sFirstName;
                        }

                        string sUserID = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));

                        iNumber++;
                        if (iNumber % 200 == 0)
                        {
                            oListUserIDs.Add(sbUserIDs.ToString());
                            sbUserIDs.Length = 0;
                        }

                        if (sbUserIDs.Length > 0)
                            sbUserIDs.Append("," + sUserID);
                        else
                            sbUserIDs.Append(sUserID);

                        objRowChild = p_objXmlDocument.CreateElement("UserID");
                        objRowChild.InnerText = sUserID;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("LoginName");
                        objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("Name");
                        objRowChild.InnerText = sName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("FirstName");
                        objRowChild.InnerText = sFirstName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("LastName");
                        objRowChild.InnerText = sLastName;
                        objLstRow.AppendChild(objRowChild);

                        //objRowChild = p_objXmlDocument.CreateElement("UserType");
                        //objRowChild.InnerText = GetGroupType(Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId));
                        //objLstRow.AppendChild(objRowChild);

                        //Added By Amitosh for MITS 24236 (03/09/2011)
                        objRowChild = p_objXmlDocument.CreateElement("UserType");
                        objLstRow.AppendChild(objRowChild);
                        //end Amitosh

                        objRowChild = p_objXmlDocument.CreateElement("Email");
                        objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("EMAIL_ADDR"));
                        objLstRow.AppendChild(objRowChild);

                       
                        objXmlElement.AppendChild(objLstRow);
                    }
                }

                if (sbUserIDs.Length > 0)
                {
                    oListUserIDs.Add(sbUserIDs.ToString());
                }

                //Get User Group information
                GetGroupType(p_objXmlDocument, oListUserIDs);

                //getting user type list info from the main database.
                objXmlDocument = new XmlDocument();
                objParentElement = objXmlDocument.CreateElement("SystemUserGroups");
                objXmlDocument.AppendChild(objParentElement);

                objChildElement = objXmlDocument.CreateElement("UserTypes");
                objParentElement.AppendChild(objChildElement);

                sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS";

                using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSQL))
                {
                    while (objReader.Read())
                    {
                        iCount = iCount + 1;
                        sGroupId = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_ID")).Trim();
                        sGroupName = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_NAME")).Trim();

                        objXmlElement = objXmlDocument.CreateElement("GroupName");

                        objXmlElement.SetAttribute("groupid", sGroupId);
                        objXmlElement.InnerText = sGroupName;

                        objChildElement.AppendChild(objXmlElement);
                    }
                }
                XmlDocumentFragment objDocFrag = XMLDoc.CreateDocumentFragment();

                objDocFrag.InnerXml = objXmlDocument.InnerXml;

                XmlNode InsertLoc = XMLDoc.DocumentElement.SelectSingleNode("SystemUserList");
                XMLDoc.DocumentElement.InsertAfter(objDocFrag, InsertLoc);

            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objXmlElement = null;
                objRowTxt = null;
                objLstRow = null;
                sbSql = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                objCache = null;
            }
            return XMLDoc;
        }

        //MITS:33371 ajohari2: Start

        /// Name		: GetUserListByGroup
        /// Author		: ajohari2
        /// Date Created: 20/05/2014		
        /// <summary>
        /// Function returns the list of system users based on groupId
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <param name="gruopId">This contain groupId of loggedinuser</param>
        /// <returns>True/False</returns>
        public XmlDocument GetUserListByGroup(XmlDocument p_objXmlDocument, int groupId)
        {
            XmlElement objXmlElement = null;
            XmlElement objRowChild = null;
            XmlElement objLstRow = null;
            XmlNode objTempNode = null;
            XmlNode objOptionFlagNode = null;
            StringBuilder sbSql = null;
            DbConnection objConn = null;
            LocalCache objCache = null;
            XmlDocument objXmlDocument = null;
            XmlElement objParentElement = null;
            XmlElement objChildElement = null;
            XMLDoc = p_objXmlDocument;

            string sDSNno = string.Empty;
            string sDatabaseName = string.Empty;
            string sSecDSN = SecurityDatabase.GetSecurityDsn(m_iClientId);

            string sSQL = "";
            string sGroupId = "";
            string sGroupName = "";
            string sName = "";
            string sFirstName = "";
            string sLastName = "";
            string sParentForm = "";
            string sTmpUserId = "";
            string sValToCompare = "";
            string sTmpUserIdPrev = "";
            int iCount = 0;
            bool bAllowGlobalPeek = false;
            string users = string.Empty;

            try
            {
                objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");
                objOptionFlagNode = p_objXmlDocument.SelectSingleNode("//OptionFlag");
                if (objTempNode != null && objOptionFlagNode != null)
                {
                    sParentForm = objTempNode.InnerText;

                    if (sParentForm == "creatediary" || sParentForm == "autodiarysetup" || sParentForm == "enhancednotes" || sParentForm == "emaildocs" || sParentForm == "email-doc" || sParentForm == "weblinksetup")//Added by Amitosh for R8 enhancement of WebLinks
                    {
                        objOptionFlagNode.InnerText = "1";
                    }
                    else
                    {
                        objOptionFlagNode.InnerText = "0";
                    }
                }

                objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");

                sbSql = new StringBuilder();

                if (sParentForm == "peekdiary")
                {
                    sTmpUserId = " " + m_iUserId + " ";
                    bAllowGlobalPeek = Conversion.ConvertLongToBool(GetSingleLong("ALLOW_GLOBAL_PEEK", "SYS_PARMS", "", m_sDSN), m_iClientId);
                    iCount = 0;

                    sbSql = new StringBuilder();

                    sbSql.Append("SELECT DISTINCT USER_DETAILS_TABLE.USER_ID,LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR ");
                    sbSql.Append("FROM USER_DETAILS_TABLE,USER_TABLE");
                    sbSql.Append(" WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                    sbSql.Append(" AND USER_DETAILS_TABLE.DSNID = (");
                    sbSql.Append(m_sDsnId);
                    sbSql.Append(")");

                    if (!bAllowGlobalPeek)
                    {
                        users = GetSupervisoryChain(m_iUserId.ToString());
                        if (users[users.Length - 1] == ',')
                        {
                            users = users.Substring(0, (users.Length - 1));
                        }
                        sbSql.Append(" AND USER_TABLE.USER_ID IN (" + users);

                        sbSql.Append(")");
                    }
                }
                else
                {
                    sbSql = new StringBuilder();
                    sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR FROM USER_DETAILS_TABLE, USER_TABLE");
                    sbSql.Append(" WHERE DSNID= ");
                    sbSql.Append(m_sDsnId);
                    sbSql.Append(" AND USER_TABLE.USER_ID <> 0 AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                }

                if (sParentForm == "email-doc" || sParentForm == "emaildocs")
                {
                    if (DbFactory.IsOracleDatabase(m_sSecDSN))
                    {
                        sbSql.Append(" AND USER_TABLE.EMAIL_ADDR is not null");
                    }
                    else
                    {
                        sbSql.Append(" AND USER_TABLE.EMAIL_ADDR <> ''");
                    }
                }

                sbSql.Append(" ORDER BY USER_DETAILS_TABLE.USER_ID");

                objXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//SystemUserList");

                //Put USER_ID into a list, so we can retrive group info in much less trips to database
                StringBuilder sbUserIDs = new StringBuilder();

                //Oracle has limit of command text length, we want to split into multiple commands
                List<string> oListUserIDs = new List<string>();
                int iNumber = 0;
                using (DbReader objReader = DbFactory.ExecuteReader(m_sSecDSN, sbSql.ToString()))
                {
                    while (objReader.Read())
                    {
                        objLstRow = p_objXmlDocument.CreateElement("SystemUser");
                        sFirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                        sLastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));

                        sName = sLastName + "," + sFirstName;
                        if (sFirstName == "")
                        {
                            sName = sLastName;
                        }
                        else if (sLastName == "")
                        {
                            sName = sFirstName;
                        }

                        string sUserID = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));

                        iNumber++;
                        if (iNumber % 200 == 0)
                        {
                            oListUserIDs.Add(sbUserIDs.ToString());
                            sbUserIDs.Length = 0;
                        }

                        if (sbUserIDs.Length > 0)
                            sbUserIDs.Append("," + sUserID);
                        else
                            sbUserIDs.Append(sUserID);

                        objRowChild = p_objXmlDocument.CreateElement("UserID");
                        objRowChild.InnerText = sUserID;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("LoginName");
                        objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("Name");
                        objRowChild.InnerText = sName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("FirstName");
                        objRowChild.InnerText = sFirstName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("LastName");
                        objRowChild.InnerText = sLastName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("UserType");
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("Email");
                        objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("EMAIL_ADDR"));
                        objLstRow.AppendChild(objRowChild);

                        objXmlElement.AppendChild(objLstRow);
                    }
                }

                if (sbUserIDs.Length > 0)
                {
                    oListUserIDs.Add(sbUserIDs.ToString());
                }

                //Get User Group information
                GetGroupType(p_objXmlDocument, oListUserIDs);

                //getting user type list info from the main database.
                objXmlDocument = new XmlDocument();
                objParentElement = objXmlDocument.CreateElement("SystemUserGroups");
                objXmlDocument.AppendChild(objParentElement);

                objChildElement = objXmlDocument.CreateElement("UserTypes");
                objParentElement.AppendChild(objChildElement);

                sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS";
                sSQL += " where group_id=" + groupId;//MITS:33371 ajohari2

                using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSQL))
                {
                    while (objReader.Read())
                    {
                        iCount = iCount + 1;
                        sGroupId = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_ID")).Trim();
                        sGroupName = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_NAME")).Trim();

                        objXmlElement = objXmlDocument.CreateElement("GroupName");

                        objXmlElement.SetAttribute("groupid", sGroupId);
                        objXmlElement.InnerText = sGroupName;

                        objChildElement.AppendChild(objXmlElement);
                    }
                }
                XmlDocumentFragment objDocFrag = XMLDoc.CreateDocumentFragment();

                objDocFrag.InnerXml = objXmlDocument.InnerXml;

                XmlNode InsertLoc = XMLDoc.DocumentElement.SelectSingleNode("SystemUserList");
                XMLDoc.DocumentElement.InsertAfter(objDocFrag, InsertLoc);
                //MITS:33371 ajohari2:Start
                XmlNodeList objNodLst = null;
                objNodLst = XMLDoc.SelectNodes("//SystemUser");
                XmlNodeList objNodLstTemp = XMLDoc.SelectNodes("//SystemUser");
                int i = 0;
                foreach (XmlElement objElm in objNodLstTemp)
                {
                    foreach (XmlElement objChildElements in objElm.ChildNodes)
                    {
                        if (objChildElements.Name == "UserType")
                        {
                            if (objChildElements.InnerText.Trim() != sGroupName.Trim()) // As assumed we will always have only sGroupName in this case
                            {
                                objNodLst[i].ParentNode.RemoveChild(objNodLst[i]);
                            }
                        }
                    }

                    i++;
                }
                //MITS:33371 ajohari2:End

            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objXmlElement = null;
                objLstRow = null;
                sbSql = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                objCache = null;
            }
            return XMLDoc;
        }

        //MITS:33371 ajohari2: End

        /// Name		: GetUserListForMobileAdjuster
        /// Author		: Amandeep Kaur
        /// Date Created: 06/27/2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function returns the list of system users 
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        //public XmlDocument GetUserListForMobileAdjuster(XmlDocument p_objXmlDocument)
        //{

        //    XmlElement objXmlElement = null;
        //    XmlElement objRowTxt = null;
        //    XmlElement objRowChild = null;
        //    XmlElement objLstRow = null;
        //    XmlNode objTempNode = null;
        //    XmlNode objOptionFlagNode = null;
        //    StringBuilder sbSql = null;
        //    DbConnection objConn = null;
        //    LocalCache objCache = null;
        //    XmlDocument objXmlDocument = null;
        //    XmlElement objParentElement = null;
        //    XmlElement objChildElement = null;
        //    XMLDoc = p_objXmlDocument;

        //    string sDSNno = string.Empty;
        //    string sDatabaseName = string.Empty;
        //    string sSecDSN = SecurityDatabase.GetSecurityDsn(0);

        //    string sSQL = "";
        //    string sGroupId = "";
        //    string sGroupName = "";
        //    string sName = "";
        //    string sFirstName = "";
        //    string sLastName = "";
        //    string sParentForm = "";

        //    //peek diary
        //    string sTmpUserId = "";
        //    string sValToCompare = "";
        //    string sTmpUserIdPrev = "";
        //    int iCount = 0;
        //    bool bAllowGlobalPeek = false;
        //    string users = string.Empty;
        //    string sMobileGroupName = string.Empty;
        //    XmlElement objMobileGroup = null;
        //    try
        //    {
        //        objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");
        //        objOptionFlagNode = p_objXmlDocument.SelectSingleNode("//OptionFlag");
        //        if (objTempNode != null && objOptionFlagNode != null)
        //        {
        //            sParentForm = objTempNode.InnerText;

        //            //check for email-doc has been added by Nitin for Mits  15565 on 20-Apr-2009
        //            if (sParentForm == "creatediary" || sParentForm == "autodiarysetup" || sParentForm == "enhancednotes" || sParentForm == "emaildocs" || sParentForm == "email-doc" || sParentForm == "weblinksetup")//Added by Amitosh for R8 enhancement of WebLinks
        //            {
        //                objOptionFlagNode.InnerText = "1";
        //            }
        //            else
        //            {
        //                objOptionFlagNode.InnerText = "0";
        //            }
        //        }

        //        objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");

        //        sbSql = new StringBuilder();

        //        if (sParentForm == "peekdiary")
        //        {
        //            sTmpUserId = " " + m_iUserId + " ";
        //            bAllowGlobalPeek = Conversion.ConvertLongToBool(GetSingleLong("ALLOW_GLOBAL_PEEK", "SYS_PARMS", "", m_sDSN));
        //            iCount = 0;

        //            sbSql = new StringBuilder();

        //            sbSql.Append("SELECT DISTINCT USER_DETAILS_TABLE.USER_ID,LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR ");
        //            sbSql.Append("FROM USER_DETAILS_TABLE,USER_TABLE");
        //            sbSql.Append(" WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
        //            sbSql.Append(" AND USER_DETAILS_TABLE.DSNID = (");
        //            sbSql.Append(m_sDsnId);
        //            sbSql.Append(")");

        //            if (!bAllowGlobalPeek)
        //            {
        //                //sbSql.Append(" AND USER_TABLE.MANAGER_ID IN (" + sTmpUserId + ")");

        //                //added by Nitin for R6 merging in order to fetch userlist for supervisory chain
        //                //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Peek Changes
        //                users = GetSupervisoryChain(m_iUserId.ToString());
        //                if (users[users.Length - 1] == ',')
        //                {
        //                    users = users.Substring(0, (users.Length - 1));
        //                }
        //                sbSql.Append(" AND USER_TABLE.USER_ID IN (" + users);
        //                //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Peek Changes - end
        //                //sbSql.Append(m_iUserId );
        //                sbSql.Append(")");
        //            }
        //        }
        //        else
        //        {
        //            sbSql = new StringBuilder();
        //            sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR FROM USER_DETAILS_TABLE, USER_TABLE");
        //            sbSql.Append(" WHERE DSNID= ");
        //            sbSql.Append(m_sDsnId);
        //            sbSql.Append(" AND USER_TABLE.USER_ID <> 0 AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
        //        }

        //        //added by Nitin for Mits  15565 on 20-Apr-2009
        //        if (sParentForm == "email-doc" || sParentForm == "emaildocs")
        //        {
        //            if (DbFactory.IsOracleDatabase(m_sSecDSN))
        //            {
        //                sbSql.Append(" AND USER_TABLE.EMAIL_ADDR is not null");
        //            }
        //            else
        //            {
        //                sbSql.Append(" AND USER_TABLE.EMAIL_ADDR <> ''");
        //            }
        //        }

        //        sbSql.Append(" ORDER BY USER_DETAILS_TABLE.USER_ID");
        //        //ended by Nitin for Mits  15565 on 20-Apr-2009

        //        objXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//SystemUserList");

        //        //Put USER_ID into a list, so we can retrive group info in much less trips to database
        //        StringBuilder sbUserIDs = new StringBuilder();

        //        //Oracle has limit of command text length, we want to split into multiple commands
        //        List<string> oListUserIDs = new List<string>();
        //        int iNumber = 0;
        //        using (DbReader objReader = DbFactory.ExecuteReader(m_sSecDSN, sbSql.ToString()))
        //        {

        //            while (objReader.Read())
        //            {
        //                sMobileGroupName = GetGroupTypeForMobileAdjuster(Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId));
        //                sMobileGroupName = sMobileGroupName.Trim();
        //                objMobileGroup = (XmlElement)p_objXmlDocument.SelectSingleNode("//MobileGroup");
        //                if (objMobileGroup != null)
        //                {
        //                    if (!sMobileGroupName.Equals(objMobileGroup.InnerText.Trim()))
        //                    {
        //                        continue;
        //                    }
        //                    objLstRow = p_objXmlDocument.CreateElement("SystemUser");
        //                    sFirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
        //                    sLastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));

        //                    //sName = sFirstName + "," + sLastName;
        //                    sName = sLastName + "," + sFirstName;
        //                    if (sFirstName == "")
        //                    {
        //                        sName = sLastName;
        //                    }
        //                    else if (sLastName == "")
        //                    {
        //                        sName = sFirstName;
        //                    }

        //                    objRowChild = p_objXmlDocument.CreateElement("UserID");
        //                    objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));
        //                    objLstRow.AppendChild(objRowChild);

        //                    objRowChild = p_objXmlDocument.CreateElement("LoginName");
        //                    objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
        //                    objLstRow.AppendChild(objRowChild);

        //                    objRowChild = p_objXmlDocument.CreateElement("Name");
        //                    objRowChild.InnerText = sName;
        //                    objLstRow.AppendChild(objRowChild);

        //                    objRowChild = p_objXmlDocument.CreateElement("FirstName");
        //                    objRowChild.InnerText = sFirstName;
        //                    objLstRow.AppendChild(objRowChild);

        //                    objRowChild = p_objXmlDocument.CreateElement("LastName");
        //                    objRowChild.InnerText = sLastName;
        //                    objLstRow.AppendChild(objRowChild);

        //                    objRowChild = p_objXmlDocument.CreateElement("UserType");
        //                    objRowChild.InnerText = sMobileGroupName;
        //                    objLstRow.AppendChild(objRowChild);

        //                    objRowChild = p_objXmlDocument.CreateElement("Email");
        //                    objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("EMAIL_ADDR"));
        //                    objLstRow.AppendChild(objRowChild);

        //                    objXmlElement.AppendChild(objLstRow);
        //                }
        //            }
        //        }

        //        //getting user type list info from the main database.

        //        objXmlDocument = new XmlDocument();
        //        objParentElement = objXmlDocument.CreateElement("SystemUserGroups");
        //        objXmlDocument.AppendChild(objParentElement);

        //        objChildElement = objXmlDocument.CreateElement("UserTypes");
        //        objParentElement.AppendChild(objChildElement);

        //        sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS";

        //        using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSQL))
        //        {
        //            while (objReader.Read())
        //            {
        //                iCount = iCount + 1;
        //                sGroupId = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_ID")).Trim();
        //                sGroupName = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_NAME")).Trim();

        //                objXmlElement = objXmlDocument.CreateElement("GroupName");

        //                objXmlElement.SetAttribute("groupid", sGroupId);
        //                objXmlElement.InnerText = sGroupName;

        //                objChildElement.AppendChild(objXmlElement);
        //            }

        //        }
        //        XmlDocumentFragment objDocFrag = XMLDoc.CreateDocumentFragment();

        //        objDocFrag.InnerXml = objXmlDocument.InnerXml;

        //        XmlNode InsertLoc = XMLDoc.DocumentElement.SelectSingleNode("SystemUserList");
        //        XMLDoc.DocumentElement.InsertAfter(objDocFrag, InsertLoc);

        //    }

        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error"), p_objEx);
        //    }
        //    finally
        //    {
        //        objXmlElement = null;
        //        objRowTxt = null;
        //        objLstRow = null;
        //        sbSql = null;
        //        if (objCache != null)
        //        {
        //            objCache.Dispose();
        //        }
        //        if (objConn != null)
        //        {
        //            objConn.Dispose();
        //        }
        //        objCache = null;
        //    }
        //    return XMLDoc;
        //}

        /// Name		: GetUserListForMobileAdjuster
        /// Author		: Amandeep Kaur
        /// Date Created: 06/27/2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function returns the list of system users 
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public XmlDocument GetUserListForMobileAdjuster(XmlDocument p_objXmlDocument)
        {
            string sSQL = string.Empty;
            int iTableId = 0;
            LocalCache objCache = null;
            StringBuilder sbSql = null;
            StringBuilder sbTemp = null;

            XmlElement objXmlElement = null;           
            XmlElement objRowChild = null;
            XmlElement objLstRow = null;                  
            DbConnection objConn = null;
            XmlNode objTempNode = null;
            XmlNode objOptionFlagNode = null;
            XmlDocument objXmlDocument = null;
            XmlElement objParentElement = null;
            XmlElement objChildElement = null;
           
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sGroupId = "";
            string sGroupName = "";
            string sName = string.Empty;
            string sParentForm = string.Empty;

            string sTmpUserId = "";           
            int iCount = 0;
            bool bAllowGlobalPeek = false;
            string users = string.Empty;
            SysSettings objSettings = null;
            try
            {               
                sbTemp = new StringBuilder();
                objCache = new LocalCache(m_sDSN, m_iClientId);
                iTableId = objCache.GetTableId("ADJUSTERS");
                XMLDoc = p_objXmlDocument;
                objSettings = new SysSettings(m_sDSN, m_iClientId);
                objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");
                objOptionFlagNode = p_objXmlDocument.SelectSingleNode("//OptionFlag");
                if (objTempNode != null && objOptionFlagNode != null)
                {
                    sParentForm = objTempNode.InnerText;

                    //check for email-doc has been added by Nitin for Mits  15565 on 20-Apr-2009
                    if (sParentForm == "creatediary" || sParentForm == "autodiarysetup" || sParentForm == "enhancednotes" || sParentForm == "emaildocs" || sParentForm == "email-doc" || sParentForm == "weblinksetup")//Added by Amitosh for R8 enhancement of WebLinks
                    {
                        objOptionFlagNode.InnerText = "1";
                    }
                    else
                    {
                        objOptionFlagNode.InnerText = "0";
                    }
                }
                objTempNode = p_objXmlDocument.SelectSingleNode("//Parentform");
                if (objSettings.UseEntityRole)
                {
                    sSQL = "SELECT DISTINCT RM_USER_ID from ENTITY where RM_USER_ID <> 0";
                }
                else
                {
                    sSQL = "SELECT DISTINCT RM_USER_ID from ENTITY where ENTITY_TABLE_ID = " + iTableId + " AND RM_USER_ID <> 0";
                }
                using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSQL))
                {
                    while (objReader.Read())
                    {
                        if (sbTemp.Length > 0)
                            sbTemp.Append(",");
                        sbTemp.Append(objReader.GetInt(0).ToString());
                    }
                }
               
                sbSql = new StringBuilder();

                if (sParentForm == "peekdiary")
                {
                    sTmpUserId = " " + m_iUserId + " ";
                    bAllowGlobalPeek = Conversion.ConvertLongToBool(GetSingleLong("ALLOW_GLOBAL_PEEK", "SYS_PARMS", "", m_sDSN), m_iClientId);
                    iCount = 0;

                    sbSql = new StringBuilder();

                    sbSql.Append("SELECT DISTINCT USER_DETAILS_TABLE.USER_ID,LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR ");
                    sbSql.Append("FROM USER_DETAILS_TABLE,USER_TABLE");
                    sbSql.Append(" WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                    sbSql.Append(" AND USER_DETAILS_TABLE.DSNID = (");
                    sbSql.Append(m_sDsnId);
                    sbSql.Append(")");

                    if (!bAllowGlobalPeek)
                    {
                        //sbSql.Append(" AND USER_TABLE.MANAGER_ID IN (" + sTmpUserId + ")");

                        //added by Nitin for R6 merging in order to fetch userlist for supervisory chain
                        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Peek Changes
                        users = GetSupervisoryChain(m_iUserId.ToString());
                        if (users[users.Length - 1] == ',')
                        {
                            users = users.Substring(0, (users.Length - 1));
                        }
                        sbSql.Append(" AND USER_TABLE.USER_ID IN (" + users);
                        //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Peek Changes - end
                        //sbSql.Append(m_iUserId );
                        sbSql.Append(")");
                    }
                }
                else
                {
                    sbSql = new StringBuilder();
                    sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,EMAIL_ADDR FROM USER_DETAILS_TABLE, USER_TABLE");
                    sbSql.Append(" WHERE DSNID= ");
                    sbSql.Append(m_sDsnId);
                    sbSql.Append(" AND USER_TABLE.USER_ID IN (" + sbTemp.ToString() + ") AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                   
                    
                }

                //added by Nitin for Mits  15565 on 20-Apr-2009
                if (sParentForm == "email-doc" || sParentForm == "emaildocs")
                {
                    if (DbFactory.IsOracleDatabase(m_sSecDSN))
                    {
                        sbSql.Append(" AND USER_TABLE.EMAIL_ADDR is not null");
                    }
                    else
                    {
                        sbSql.Append(" AND USER_TABLE.EMAIL_ADDR <> ''");
                    }
                }
                sbSql.Append(" ORDER BY USER_DETAILS_TABLE.USER_ID");
                objXmlElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//SystemUserList");

                //Put USER_ID into a list, so we can retrive group info in much less trips to database
                StringBuilder sbUserIDs = new StringBuilder();

                //Oracle has limit of command text length, we want to split into multiple commands
                List<string> oListUserIDs = new List<string>();
                int iNumber = 0;

                using (DbReader objReader = DbFactory.ExecuteReader(m_sSecDSN, sbSql.ToString()))
                {
                    while (objReader.Read())
                    {

                        objLstRow = p_objXmlDocument.CreateElement("SystemUser");
                        sFirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                        sLastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));

                        //sName = sFirstName + "," + sLastName;
                        sName = sLastName + "," + sFirstName;
                        if (sFirstName == "")
                        {
                            sName = sLastName;
                        }
                        else if (sLastName == "")
                        {
                            sName = sFirstName;
                        }

                        string sUserID = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));

                        iNumber++;
                        if (iNumber % 200 == 0)
                        {
                            oListUserIDs.Add(sbUserIDs.ToString());
                            sbUserIDs.Length = 0;
                        }

                        if (sbUserIDs.Length > 0)
                            sbUserIDs.Append("," + sUserID);
                        else
                            sbUserIDs.Append(sUserID);


                        objRowChild = p_objXmlDocument.CreateElement("UserID");
                        objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("LoginName");
                        objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("Name");
                        objRowChild.InnerText = sName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("FirstName");
                        objRowChild.InnerText = sFirstName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("LastName");
                        objRowChild.InnerText = sLastName;
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("UserType");
                        objLstRow.AppendChild(objRowChild);

                        objRowChild = p_objXmlDocument.CreateElement("Email");
                        objRowChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("EMAIL_ADDR"));
                        objLstRow.AppendChild(objRowChild);

                        objXmlElement.AppendChild(objLstRow);
                    }
                }                
                //getting user type list info from the main database.
                if (sbUserIDs.Length > 0)
                {
                    oListUserIDs.Add(sbUserIDs.ToString());
                }

                //Get User Group information
                GetGroupType(p_objXmlDocument, oListUserIDs);

                objXmlDocument = new XmlDocument();
                objParentElement = objXmlDocument.CreateElement("SystemUserGroups");
                objXmlDocument.AppendChild(objParentElement);

                objChildElement = objXmlDocument.CreateElement("UserTypes");
                objParentElement.AppendChild(objChildElement);

                sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS";

                using (DbReader objReader = DbFactory.GetDbReader(m_sDSN, sSQL))
                {
                    while (objReader.Read())
                    {
                        iCount = iCount + 1;
                        sGroupId = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_ID")).Trim();
                        sGroupName = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_NAME")).Trim();

                        objXmlElement = objXmlDocument.CreateElement("GroupName");

                        objXmlElement.SetAttribute("groupid", sGroupId);
                        objXmlElement.InnerText = sGroupName;

                        objChildElement.AppendChild(objXmlElement);
                    }

                }
                XmlDocumentFragment objDocFrag = XMLDoc.CreateDocumentFragment();

                objDocFrag.InnerXml = objXmlDocument.InnerXml;

                XmlNode InsertLoc = XMLDoc.DocumentElement.SelectSingleNode("SystemUserList");
                XMLDoc.DocumentElement.InsertAfter(objDocFrag, InsertLoc);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objXmlElement = null;                
                objLstRow = null;
                sbSql = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                objCache = null;
            }
            return XMLDoc;
        }
        /// <summary>
        ///  Created by Nitin to fetch list of chain subordinates of current user
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public DataSet GetUserListForDiaryCalendar(string action)
        {
            StringBuilder sbSql = null;
            DbConnection objRMConn;
            DbReader objReader = null;
            Hashtable hstModuleGroups = new Hashtable();
            string sGrpName = string.Empty;
            DataSet objDsUsers;

            //logDebug("RMXPortalCustomization.GetUserList", "initializing");
            string subordinateList = string.Empty;

            try
            {
                objRMConn = DbFactory.GetDbConnection(m_sDSN);

                sbSql = new StringBuilder();

                if (action == "peek")
                {
                    subordinateList = GetSupervisoryChain(m_iUserId.ToString());
                }
                else if (action == "processingoffice")
                {
                    subordinateList = GetProcessingOfficeUserList(m_iUserId.ToString());
                }
                else
                {
                    subordinateList = string.Empty;
                }

                if (!string.IsNullOrEmpty(subordinateList))
                {
                    subordinateList = subordinateList.Substring(0, subordinateList.Length - 1);
                }
                else
                {
                    return null;
                }


                //CUL for diary module
                sbSql.Append("SELECT DISTINCT USER_DETAILS_TABLE.LOGIN_NAME,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ");
                sbSql.Append("FROM USER_DETAILS_TABLE,USER_TABLE");
                sbSql.Append(" WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID");
                sbSql.Append(string.Format(" AND USER_DETAILS_TABLE.DSNID = ({0})", m_sDsnId));


                sbSql.Append(String.Format("AND USER_TABLE.USER_ID IN ({0})", subordinateList));
                sbSql.Append(" ORDER BY USER_DETAILS_TABLE.LOGIN_NAME");

                objDsUsers = DbFactory.GetDataSet(m_sSecDSN, sbSql.ToString(), m_iClientId);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return objDsUsers;
        }

        /// Name		: GetGroupTypeForMObileAdjuster
        /// Author		: Amandeep Kaur
        /// Date Created: 03/23/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function returns the Module Group Name 
        /// </summary>

        private string GetGroupTypeForMobileAdjuster(int p_iUserId)
        {
            string sSQL = "";
            string sGroupName = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT GROUP_NAME FROM USER_GROUPS,USER_MEMBERSHIP WHERE USER_MEMBERSHIP.GROUP_ID= USER_GROUPS.GROUP_ID AND USER_MEMBERSHIP.USER_ID =" + p_iUserId;

                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);

                if (objReader.Read())
                {
                    sGroupName = Conversion.ConvertObjToStr(objReader.GetValue(0));
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return sGroupName;

        }
        private void GetGroupType(XmlDocument p_objXmlDocument, List<string> p_oListUserIDs)
        {
            string sSQL = string.Empty;
            string sActualSQL = string.Empty;
            string sGroupName = string.Empty;
            string sUserID = string.Empty;
            XmlNode objRowChild = null;

            // XmlNode objListRow = null;

            try
            {
                sSQL = "SELECT GROUP_NAME, USER_MEMBERSHIP.USER_ID FROM USER_GROUPS,USER_MEMBERSHIP WHERE USER_MEMBERSHIP.GROUP_ID= USER_GROUPS.GROUP_ID AND USER_MEMBERSHIP.USER_ID IN (";

                // Changed by Amitosh for Mits 24236 (03/14/2011)
                // Removed foreach because list's count was always 1 so no loop is required.
                //    foreach (string sUserIDs in p_oListUserIDs)
                //  {
                // sActualSQL = string.Format("{0} {1} )", sSQL, sUserIDs);
                // mkaran2 MITS 32487 code merged for mkaran2 - MITS 32543 - start
                foreach (string sSequenceListUserIDs in p_oListUserIDs)
                {
                    //sActualSQL = string.Format("{0} {1} )", sSQL, p_oListUserIDs[0]);
                    sActualSQL = string.Format("{0} {1} )", sSQL, sSequenceListUserIDs);
                    using (DbReader objReader = DbFactory.ExecuteReader(m_sDSN, sActualSQL))
                    {
                        while (objReader.Read())
                        {
                            sGroupName = Conversion.ConvertObjToStr(objReader.GetValue("GROUP_NAME"));
                            sUserID = Conversion.ConvertObjToStr(objReader.GetValue("USER_ID"));

                            //Changed By Amitosh for MITS 24236 (03/09/2011)
                            //objListRow = p_objXmlDocument.SelectSingleNode("//SystemUserList/SystemUser[UserID='" + sUserID + "']");
                            objRowChild = p_objXmlDocument.SelectSingleNode("//SystemUserList/SystemUser[UserID='" + sUserID + "']/UserType");
                            // objRowChild = p_objXmlDocument.CreateElement("UserType");
                            objRowChild.InnerText = sGroupName;
                            //objListRow.AppendChild(objRowChild);

                        }
                    }
                }  // mkaran2 MITS 32487 code merged for mkaran2 - MITS 32543 - end
            }
            //}
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("UserList.GetGroupType.Error", m_iClientId), p_objException);
            }
            finally
            {

            }

        }

        #endregion

        #region Get Single Long
        /// <summary>
        /// Creates a dynamic query and executes it against the database and returns result as a Long value
        /// </summary>
        /// <param name="p_sFieldName">Field Name</param>
        /// <param name="p_sTableName">Table Name</param>
        /// <param name="p_sCriteria">Criteria ie. where clause</param>
        /// <param name="p_sConnectString">Connection string</param>
        /// <returns>long</returns>
        private long GetSingleLong(string p_sFieldName, string p_sTableName, string p_sCriteria, string p_sConnectString)
        {
            long lGetSingleLong = 0;
            string sSql = "";
            DbReader objReader = null;
            try
            {
                if (p_sCriteria.Trim().Length == 0)
                {
                    p_sCriteria = "";
                }
                else
                {
                    p_sCriteria = " WHERE " + p_sCriteria;
                }
                sSql = "SELECT " + p_sFieldName + " FROM " + p_sTableName + p_sCriteria;
                objReader = DbFactory.GetDbReader(p_sConnectString, sSql);

                if (objReader != null)
                    if (objReader.Read())
                    {
                        if (objReader.GetValue(0) is System.DBNull)
                        {
                            lGetSingleLong = 0;
                        }
                        else
                        {
                            lGetSingleLong = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }

                    }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.GetSingleLong.GeneralError", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (lGetSingleLong);
        }
        #endregion
    }
}
