﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{

	/// <summary>
	///Author	:   Anurag Agarwal
	///Class	:	DiscountParms
	///Dated	:   06 April 2005
	///Purpose	:   This class is use Add / Modify the Discounts data
	/// </summary>
	public class DiscountParms
	{
		/// <summary>
		/// Private variable to store Connection String
		/// </summary>
		private string m_sConnectString = "";

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "";
		
		/// <summary>
		/// Private constant variable to store TableName
		/// </summary>
		private const string TABLENAME = "SYS_POL_DISCOUNTS";

		/// <summary>
		/// Private constant variable to store TableName
		/// </summary>
		private const string RANGE_TABLENAME = "SYS_POL_DISC_RANGE";
        private int m_iClientId = 0;
        private UserLogin m_UserLogin = null;//vkumar258
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public DiscountParms(string p_sLoginName, string p_sConnectionString, int p_iClientId)
		{
			m_sUserName = p_sLoginName;
			m_sConnectString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}

        public DiscountParms(UserLogin oUserLogin, string p_sLoginName, string p_sConnectionString, int p_iClientId = 0)//vkumar258 ML chnages
        {
            m_UserLogin = oUserLogin;
            m_sUserName = m_UserLogin.LoginName;
            m_sConnectString = p_sConnectionString;
            m_iClientId = p_iClientId;
        }

		/// Name		: LoadData
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Loads Discount record on the basis of DiscountRowId passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">The DiscountRowId is passed in the XML
		///		The sample input XML will be
		///				<Document>
		///					<discount>
		///						<discountname></discountname>
		///						<lob></lob>
		///						<state></state>
		///						<discounttype></discounttype>
		///						<amount></amount>
		///						<expirationdate></expirationdate>
		///						<effectivedate></effectivedate>
		///						<discountrowid>4</discountrowid>
		///					</discount>					
		///				</Document>
		/// </param>
		/// <returns>The XML with data is returned 
		///		The sample output XML will be 
		///				<Document>
		///					<discount>
		///						<discountname></discountname>
		///						<lob></lob>
		///						<state></state>
		///						<discounttype></discounttype>
		///						<amount></amount>
		///						<expirationdate></expirationdate>
		///						<effectivedate></effectivedate>
		///						<discountrowid>4</discountrowid>
		///						<discountrangelist>
		///							<discountrange>
		///								<addedbyuser></addedbyuser>
		///								<dttmrcdadded></dttmrcdadded>
		///								<updatedbyuser></updatedbyuser>
		///								<dttmrcdlastupd></dttmrcdlastupd>
		///								<amount></amount>
		///								<beginningrange></beginningrange>
		///								<discountrangerowid></discountrangerowid>
		///								<endrange></endrange>
		///							</discountrange>
		///						</discountrangelist>
		///					</discount>
		///				</Document>
		///</returns>
		public XmlDocument LoadData(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbReader objDbReader = null;
			string sDiscountRowId = "";
			XmlElement objDiscountXMLEle = null;
			Discount objDiscount = null;
			XmlElement objDiscountRangeListXMLEle = null;
			XmlElement objDiscountRangeXMLEle = null;

			string sValue=string.Empty;
			string sShortCode=string.Empty;
			string sDesc=string.Empty;
			XmlElement objElement=null;
			LocalCache objLocalCache=null;
            //rupal:start,multicurrency enh
            Riskmaster.Settings.SysSettings objSysSettings = null;
            XmlElement oElement = null;
            //rupal:end
			try
			{
                //rupal:start,multicurrency enh
                objSysSettings = new Settings.SysSettings(m_sConnectString,m_iClientId);
                //rupal:end
				objDiscountXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/discount");

				sDiscountRowId = objDiscountXMLEle.GetElementsByTagName("discountrowid").Item(0).InnerText;

                //added by pmahli MITS 8770 Header list moved out of the if statement as list Header should be populated ireespective of mode Edit or Add
                objDiscountRangeListXMLEle = (XmlElement)objDiscountXMLEle.SelectSingleNode("//Document/discount/discountrangelist");
                if (objDiscountRangeListXMLEle != null)
                    objDiscountRangeListXMLEle.InnerXml = "";
                else
                {
                    objDiscountRangeListXMLEle = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("discountrangelist"), false);
                    objDiscountXMLEle.AppendChild(objDiscountRangeListXMLEle);
                }
                //add a header row
                //Added By: Nikhil Garg		Dated: 23/Jun/2005
                objDiscountRangeXMLEle = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("listhead"), false);

                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("addedbyuser", "Added By User"), true));
                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdadded", "Dttm Rcd Added"), true));
                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("updatedbyuser", "Updated By User"), true));
                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdlastupd", "Dttm Rcd Last Upd"), true));
                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("amount", "Amount"), true));
                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("beginningrange", "Beginning Range"), true));
                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("discountrangerowid", "Discount Range Row Id"), true));
                objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange", "End Range"), true));

                objDiscountRangeListXMLEle.AppendChild(objDiscountRangeXMLEle);
                //added by pmahli MITS 8770 - End

                //In case of Edit
				if(sDiscountRowId != "")
				{
					sSQL = "SELECT * FROM " + TABLENAME + " WHERE DISCOUNT_ROWID= " + sDiscountRowId;
					objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
				
					if(objDbReader != null)
					{
						if(objDbReader.Read())
						{
                            // Naresh Volume Discount Implementation
                            objDiscountXMLEle.GetElementsByTagName("UseVolDisc").Item(0).InnerText = Conversion.ConvertObjToInt(objDbReader["USE_VOLUME_DISCOUNT"], m_iClientId)==-1?"True":"False";
							objDiscountXMLEle.GetElementsByTagName("amount").Item(0).InnerText = objDbReader["AMOUNT"].ToString();
							objDiscountXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText = Common.Conversion.GetUIDate(objDbReader["EFFECTIVE_DATE"].ToString(),m_UserLogin.objUser.NlsCode.ToString(),m_iClientId);
							objDiscountXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText = Common.Conversion.GetUIDate(objDbReader["EXPIRATION_DATE"].ToString(),m_UserLogin.objUser.NlsCode.ToString(),m_iClientId);
							objDiscountXMLEle.GetElementsByTagName("discountname").Item(0).InnerText = objDbReader["DISCOUNT_NAME"].ToString();

                            objLocalCache = new LocalCache(m_sConnectString, m_iClientId);

							objElement=(XmlElement)objDiscountXMLEle.GetElementsByTagName("discounttype").Item(0);
							sValue= objDbReader["FLAT_OR_PERCENT"].ToString();
							objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);

							objElement=(XmlElement)objDiscountXMLEle.GetElementsByTagName("lob").Item(0);
							sValue= objDbReader["LINE_OF_BUSINESS"].ToString();
							objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);

							objElement=(XmlElement)objDiscountXMLEle.GetElementsByTagName("state").Item(0);
							sValue= objDbReader["STATE"].ToString();
							objLocalCache.GetStateInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);
						}
						if(objDbReader != null)
							objDbReader.Close();
					}
					else
                        throw new RMAppException(Globalization.GetString("DiscountParms.LoadData.NoRecordsFound", m_iClientId));

                    objDiscount = new Discount(m_iClientId);
					objDiscount.ConnectionString = m_sConnectString;
					objDiscount.DiscountRangeList.DiscountRowid = Conversion.ConvertStrToInteger(sDiscountRowId);
					objDiscount.DiscountRangeList.LoadData();
                    

                    //commented by pmahli MITS 8770 Header list moved out of the if statement as list Header should be populated ireespective of mode Edit or Add
                    //objDiscountRangeListXMLEle = (XmlElement)objDiscountXMLEle.SelectSingleNode("//Document/discount/discountrangelist");
 
                    //if(objDiscountRangeListXMLEle != null)
                    //    objDiscountRangeListXMLEle.InnerXml = "";
                    //else
                    //{
                    //    objDiscountRangeListXMLEle = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("discountrangelist"),false);
                    //    objDiscountXMLEle.AppendChild(objDiscountRangeListXMLEle);
                    //}

                    ////add a header row
                    ////Added By: Nikhil Garg		Dated: 23/Jun/2005
                    //objDiscountRangeXMLEle = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("listhead"),false);
							
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("addedbyuser","Added By User"),true));
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdadded","Dttm Rcd Added"),true));
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("updatedbyuser","Updated By User"),true));
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdlastupd","Dttm Rcd Last Upd"),true));
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("amount","Amount"),true));
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("beginningrange","Beginning Range"),true));
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("discountrangerowid","Discount Range Row Id"),true));
                    //objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange","End Range"),true));
							
                    //objDiscountRangeListXMLEle.AppendChild(objDiscountRangeXMLEle); 
                    //commented by pmahli  MITS 8770 Header list moved out of the if statement as list Header should be populated ireespective of mode Edit or Add
//					if(objDiscount.DiscountRangeList.Count > 0)
//					{
						foreach(DiscountRange objDiscountRange in objDiscount.DiscountRangeList)
						{
							objDiscountRangeXMLEle = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("discountrange"),false);
							
							objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("addedbyuser",objDiscountRange.AddedByUser),true));
							objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdadded",objDiscountRange.DttmRcdAdded),true));
							objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("updatedbyuser",objDiscountRange.UpdatedByUser),true));
							objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("dttmrcdlastupd",objDiscountRange.DttmRcdLastUpd),true));
                            //Geeta 06/21/07 : MITS 9813
                            //rupal:start, multicurrency
                            if (objDiscountXMLEle.GetElementsByTagName("discounttype").Item(0).InnerText == "F Flat")
                            {
                                if (objSysSettings.UseMultiCurrency != 0)
                                {
                                    oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("amount", CommonFunctions.ConvertCurrency(0, objDiscountRange.Amount, CommonFunctions.NavFormType.None, m_sConnectString,m_iClientId)), true);
                                    oElement.SetAttribute("Amount", objDiscountRange.Amount.ToString());
                                    objDiscountRangeXMLEle.AppendChild(oElement);                                    
                                }
                                else
                                {
                                    oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("amount", string.Format("{0:C}", objDiscountRange.Amount)), true);
                                    oElement.SetAttribute("Amount", objDiscountRange.Amount.ToString());
                                    objDiscountRangeXMLEle.AppendChild(oElement);                                    
                                }

                            }
                            else
                            {
                                oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("amount", objDiscountRange.Amount.ToString() + "%"), true);
                                oElement.SetAttribute("Amount", objDiscountRange.Amount.ToString());
                                objDiscountRangeXMLEle.AppendChild(oElement);
                            }
                            if (objSysSettings.UseMultiCurrency != 0)
                            {
                                oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("beginningrange", CommonFunctions.ConvertCurrency(0, objDiscountRange.BeginningRange, CommonFunctions.NavFormType.None, m_sConnectString,m_iClientId)), true);
                                oElement.SetAttribute("Amount", objDiscountRange.BeginningRange.ToString());
                                objDiscountRangeXMLEle.AppendChild(oElement);   
                            }
                            else
                            {
                                oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("beginningrange", string.Format("{0:C}", objDiscountRange.BeginningRange)), true);
                                oElement.SetAttribute("Amount", objDiscountRange.BeginningRange.ToString());
                                objDiscountRangeXMLEle.AppendChild(oElement);                                
                            }
                            objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("discountrangerowid", objDiscountRange.DiscountRangeRowId.ToString()), true));
                            
							//pmahli 10/3/2007  Dispalying data blank when End Range is 0.0 - start 
                            if (Conversion.ConvertObjToDouble(objDiscountRange.EndRange, m_iClientId) == 0.0)
                            {
                                if (objSysSettings.UseMultiCurrency != 0)
                                {
                                    oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange", ""), true);
                                    oElement.SetAttribute("Amount", "");
                                    objDiscountRangeXMLEle.AppendChild(oElement);
                                }
                                else
                                {
                                    oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange", string.Format("{0:C}", "")), true);
                                    oElement.SetAttribute("Amount", "");
                                    objDiscountRangeXMLEle.AppendChild(oElement);
                                }
                            }
                            //pmahli 10/3/2007  Dispalying data blank when End Range is 0.0 - End
                            else 
                            {
                                if (objSysSettings.UseMultiCurrency != 0)
                                {
                                    oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange", CommonFunctions.ConvertCurrency(0, objDiscountRange.EndRange, CommonFunctions.NavFormType.None, m_sConnectString,m_iClientId)), true);
                                    oElement.SetAttribute("Amount", objDiscountRange.EndRange.ToString());
                                    objDiscountRangeXMLEle.AppendChild(oElement);
                                }
                                else
                                {
                                    oElement = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewEleWithValue("endrange", string.Format("{0:C}", objDiscountRange.EndRange)), true);
                                    oElement.SetAttribute("Amount", objDiscountRange.EndRange.ToString());
                                    objDiscountRangeXMLEle.AppendChild(oElement);
                                }
                            }
                            //rupal:end
							objDiscountRangeListXMLEle.AppendChild(objDiscountRangeXMLEle); 
						}
//					}
//					else	//return blank Tags for discount range
//					{
//						objDiscountRangeXMLEle = (XmlElement)p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("discountrange"),false);
//
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("addedbyuser"),true));
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("dttmrcdadded"),true));
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("updatedbyuser"),true));
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("dttmrcdlastupd"),true));
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("amount"),true));
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("beginningrange"),true));
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("discountrangerowid"),true));
//						objDiscountRangeXMLEle.AppendChild(p_objInputXMLDoc.ImportNode(UTILITY.GetNewElement("endrange"),true));
//							
//						objDiscountRangeListXMLEle.AppendChild(objDiscountRangeXMLEle); 
//					}
				}
	
				return p_objInputXMLDoc;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("DiscountParms.LoadData.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				if(objDbReader != null)
					objDbReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();

				objDiscountXMLEle = null;
				if(objDiscount != null)
					objDiscount = null;
				objDiscountRangeListXMLEle = null;
				objDiscountRangeXMLEle = null;
                //rupal:multicurrency enh
                if (objSysSettings != null)
                    objSysSettings = null;
			}
		}

        // Pankaj Volume Discount Implementation
        public XmlDocument VolumeDiscount(XmlDocument p_objInputXMLDoc)
        {
            XmlDocument objDOM = null;
            XmlElement objElement = null;
            
            Riskmaster.Settings.CCacheFunctions objSettings = null;
            LocalCache objLocalCache = null;
            
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sDiscountNameTemp = string.Empty;
            string sAmountTemp = string.Empty;
            string sLobCodeidTemp = string.Empty;
            string sLobTemp = string.Empty;
            string sDiscountTypeCodeidTemp = string.Empty;

            // Start Naresh 10/02/2007 Removed the Hard Coded Code Id Values and now Fetching from the Code Cache
            int iDiscountType = 0;
            int iLob = 0;

            try
            {
                objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
                objDOM = p_objInputXMLDoc;

                //DiscountName
                objElement = (XmlElement)objDOM.GetElementsByTagName("discountname").Item(0);
                sDiscountNameTemp = objElement.InnerText;
                objElement.InnerText = "Volume Discount";
                objElement = (XmlElement)objDOM.GetElementsByTagName("discountnametemp").Item(0);
                objElement.InnerText = sDiscountNameTemp;

                //DisocuntType
                objElement = (XmlElement)objDOM.GetElementsByTagName("discounttype").Item(0);
                sDiscountTypeCodeidTemp = objElement.GetAttribute("codeid").ToString();
                // Start Naresh 10/02/2007 Removed the Hard Coded Code Id Values and now Fetching from the Code Cache
                iDiscountType = objLocalCache.GetCodeId("P", "DISCOUNT_TYPE");
                objLocalCache.GetCodeInfo(iDiscountType, ref sShortCode, ref sDesc);
                objElement.InnerText = sShortCode + " " + sDesc;
                objElement.SetAttribute("codeid", iDiscountType.ToString());
                objElement = (XmlElement)objDOM.GetElementsByTagName("disocunttypecodeidtemp").Item(0);
                objElement.InnerText = sDiscountTypeCodeidTemp;
                if (sDiscountTypeCodeidTemp != "")
                {
                    objElement = (XmlElement)objDOM.GetElementsByTagName("disocunttypetemp").Item(0);
                    objLocalCache.GetCodeInfo(Convert.ToInt16(sDiscountTypeCodeidTemp), ref sShortCode, ref sDesc);
                    objElement.InnerText = sShortCode + " " + sDesc;
                }
                //LOB 
                objElement = (XmlElement)objDOM.GetElementsByTagName("lob").Item(0);
                sLobCodeidTemp = objElement.GetAttribute("codeid").ToString();
                // Start Naresh 10/02/2007 Removed the Hard Coded Code Id Values and now Fetching from the Code Cache
                iLob = objLocalCache.GetCodeId("WC", "POLICY_LOB");
                objLocalCache.GetCodeInfo(iLob, ref sShortCode, ref sDesc);
                objElement.InnerText = sShortCode + " " + sDesc;
                objElement.SetAttribute("codeid", iLob.ToString());
                objElement = (XmlElement)objDOM.GetElementsByTagName("lobCodeidtemp").Item(0);
                objElement.InnerText = sLobCodeidTemp;
                if (sLobCodeidTemp != "")
                {
                    objElement = (XmlElement)objDOM.GetElementsByTagName("lobtemp").Item(0);
                    objLocalCache.GetCodeInfo(Convert.ToInt16(sLobCodeidTemp), ref sShortCode, ref sDesc);
                    objElement.InnerText = sShortCode + " " + sDesc;
                }
                //Amount
                objElement = (XmlElement)objDOM.GetElementsByTagName("amount").Item(0);
                sAmountTemp = objElement.InnerText;
                objElement.InnerText = "0";
                objElement = (XmlElement)objDOM.GetElementsByTagName("amounttemp").Item(0);
                objElement.InnerText = sAmountTemp;

                return p_objInputXMLDoc;
            }
            catch (RMAppException p_objRMExp)
            {
                //TODO
            }
            catch (Exception p_objExp)
            {
               //TODO
            }
            finally
            {
                
            }
            return p_objInputXMLDoc;
            
        }
        //pmahli


		/// Name		: Save
		/// Author		: Anurag Agarwal
		/// Date Created	: 6 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves Discount record on the basis of DiscountRowId passed
		/// If new record then DiscountRowId will be empty string
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<discount>
		///						<discountname></discountname>
		///						<lob></lob>
		///						<state></state>
		///						<discounttype></discounttype>
		///						<amount></amount>
		///						<expirationdate></expirationdate>
		///						<effectivedate></effectivedate>
		///						<discountrowid>4</discountrowid>
		///					</discount>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		public bool Save(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbConnection objCon = null; 
			string sDiscountRowId = "";
			string sDiscountRangeRowId = "";
            // Naresh Volume Discount Implementation
            string sUseVolumeDiscount = "";
			Discount objDiscount = null;
			DiscountRange objDiscountRange = null;
			bool bIsNew = false;
			XmlElement objDiscountXMLEle = null;
			XmlNodeList objDiscRangeNodeList = null;
            string sTempAmount = string.Empty ;

			try
			{
                if (Validate(p_objInputXMLDoc)) 
				{
					objDiscountXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/discount"); 	

					sDiscountRowId = objDiscountXMLEle.GetElementsByTagName("discountrowid").Item(0).InnerText;
                    
                    // Naresh Volume Discount Implementation
                    sUseVolumeDiscount = objDiscountXMLEle.GetElementsByTagName("UseVolDisc").Item(0).InnerText;
                    objDiscount = new Discount(m_iClientId);

					if(sDiscountRowId == "")
					{
						bIsNew = true;
                        objDiscount.DiscountRowId = Utilities.GetNextUID(m_sConnectString, TABLENAME, m_iClientId);
						objDiscount.AddedByUser = m_sUserName;
                        objDiscount.DttmRcdAdded = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
					}
					else
						objDiscount.DiscountRowId = Conversion.ConvertStrToInteger(sDiscountRowId);

                    // Naresh Volume Discount Implementation
                    objDiscount.UseVolumeDiscount = sUseVolumeDiscount.ToLower() == "true" ? -1 : 0;
					objDiscount.UpdatedByUser = m_sUserName;
                    objDiscount.DttmRcdLastUpd = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
					objDiscount.State = Conversion.ConvertStrToInteger(objDiscountXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value);
					objDiscount.Amount = Conversion.ConvertStrToDouble(objDiscountXMLEle.GetElementsByTagName("amount").Item(0).InnerText);
					objDiscount.DiscountType = Conversion.ConvertStrToInteger(objDiscountXMLEle.GetElementsByTagName("discounttype").Item(0).Attributes["codeid"].Value);
					objDiscount.EffectiveDate = objDiscountXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
					objDiscount.ExpirationDate = objDiscountXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
					objDiscount.DiscountName = objDiscountXMLEle.GetElementsByTagName("discountname").Item(0).InnerText;
					objDiscount.LOB = Conversion.ConvertStrToInteger(objDiscountXMLEle.GetElementsByTagName("lob").Item(0).Attributes["codeid"].Value);

                    // Naresh Volume Discount Implementation
					if(bIsNew)
					{
                        //Parijat : Mits 11446
                        if (objDiscount.ExpirationDate.Trim() == "" || objDiscount.ExpirationDate == "0")
                        {
                            sSQL = "INSERT INTO " + TABLENAME + " (DISCOUNT_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," +
                                "UPDATED_BY_USER,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,DISCOUNT_NAME," +
                                "LINE_OF_BUSINESS,RANGE,USE_VOLUME_DISCOUNT) VALUES(" + objDiscount.DiscountRowId + ",'" + objDiscount.AddedByUser + "'," + objDiscount.DttmRcdAdded + "," +
                                objDiscount.DttmRcdLastUpd + ",'" + objDiscount.UpdatedByUser + "'," +
                                objDiscount.State + "," + objDiscount.Amount + "," + objDiscount.DiscountType + ",'" + UTILITY.FormatDate(objDiscount.EffectiveDate, true) + "','"  + objDiscount.DiscountName + "'," + objDiscount.LOB + ",0.0, " + objDiscount.UseVolumeDiscount + " )";
                        
                        }
                        else
                        {
                            sSQL = "INSERT INTO " + TABLENAME + " (DISCOUNT_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," +
                                "UPDATED_BY_USER,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,EXPIRATION_DATE,DISCOUNT_NAME," +
                                "LINE_OF_BUSINESS,RANGE,USE_VOLUME_DISCOUNT) VALUES(" + objDiscount.DiscountRowId + ",'" + objDiscount.AddedByUser + "'," + objDiscount.DttmRcdAdded + "," +
                                objDiscount.DttmRcdLastUpd + ",'" + objDiscount.UpdatedByUser + "'," +
                                objDiscount.State + "," + objDiscount.Amount + "," + objDiscount.DiscountType + ",'" + UTILITY.FormatDate(objDiscount.EffectiveDate, true) + "','" +
                                UTILITY.FormatDate(objDiscount.ExpirationDate, true) + "','" + objDiscount.DiscountName + "'," + objDiscount.LOB + ",0.0, " + objDiscount.UseVolumeDiscount + " )";
                        }
                    }
                    else
                    {
                        //Parijat : Mits 11446
                        if (objDiscount.ExpirationDate.Trim() == "" || objDiscount.ExpirationDate == "0")
                        {
                            sSQL = "UPDATE " + TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objDiscount.DttmRcdLastUpd + "," +
                                "UPDATED_BY_USER='" + objDiscount.UpdatedByUser + "'," + "LINE_OF_BUSINESS=" + objDiscount.LOB + "," +
                                "STATE=" + objDiscount.State + "," + "AMOUNT=" + objDiscount.Amount + "," + "FLAT_OR_PERCENT=" + objDiscount.DiscountType + "," +
                                "EFFECTIVE_DATE='" + UTILITY.FormatDate(objDiscount.EffectiveDate, true) + "'," + "EXPIRATION_DATE= NULL" +"," +
                                "DISCOUNT_NAME='" + objDiscount.DiscountName + "', USE_VOLUME_DISCOUNT= " + objDiscount.UseVolumeDiscount + " WHERE DISCOUNT_ROWID=" + objDiscount.DiscountRowId;
                        
                        }
                        else
                        {
                            sSQL = "UPDATE " + TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objDiscount.DttmRcdLastUpd + "," +
                                "UPDATED_BY_USER='" + objDiscount.UpdatedByUser + "'," + "LINE_OF_BUSINESS=" + objDiscount.LOB + "," +
                                "STATE=" + objDiscount.State + "," + "AMOUNT=" + objDiscount.Amount + "," + "FLAT_OR_PERCENT=" + objDiscount.DiscountType + "," +
                                "EFFECTIVE_DATE='" + UTILITY.FormatDate(objDiscount.EffectiveDate, true) + "'," + "EXPIRATION_DATE='" + UTILITY.FormatDate(objDiscount.ExpirationDate, true) + "'," +
                                "DISCOUNT_NAME='" + objDiscount.DiscountName + "', USE_VOLUME_DISCOUNT= " + objDiscount.UseVolumeDiscount + " WHERE DISCOUNT_ROWID=" + objDiscount.DiscountRowId;
                        }
                    }

					objCon = DbFactory.GetDbConnection(m_sConnectString);
					objCon.Open();
					objCon.ExecuteNonQuery(sSQL);

					//code for saving the discount range data
					objDiscRangeNodeList = p_objInputXMLDoc.GetElementsByTagName("discountrange");  //(XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/discount/discountrangelist/discountrange"); 	

					foreach(XmlElement objDiscountRangeXMLEle in objDiscRangeNodeList)
					{
						objDiscountRange = new DiscountRange();

						sDiscountRangeRowId = objDiscountRangeXMLEle.GetElementsByTagName("discountrangerowid").Item(0).InnerText;
						bIsNew = false;
						if(sDiscountRangeRowId == "")
						{
							bIsNew = true;
							if(objDiscountRange.DiscountRangeRowId == 0)
                                objDiscountRange.DiscountRangeRowId = Utilities.GetNextUID(m_sConnectString, RANGE_TABLENAME, m_iClientId);
							
							objDiscountRange.AddedByUser = m_sUserName;
                            objDiscountRange.DttmRcdAdded = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
						}
						else
							objDiscountRange.DiscountRangeRowId=Conversion.ConvertStrToInteger(sDiscountRangeRowId);

						objDiscountRange.UpdatedByUser = m_sUserName;
                        objDiscountRange.DttmRcdLastUpd = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
						objDiscountRange.DiscountRowid = objDiscount.DiscountRowId;
                        //Geeta 06/21/07 : MITS 9813
                        sTempAmount = objDiscountRangeXMLEle.GetElementsByTagName("amount").Item(0).InnerText;
                        if (sTempAmount.IndexOf('%') >= 0)
                        {
                            objDiscountRangeXMLEle.GetElementsByTagName("amount").Item(0).InnerText = sTempAmount.Remove(sTempAmount.IndexOf('%'));
                        }
					
						//rupal:start,multicurrency enh
                        //objDiscountRange.Amount = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("amount").Item(0).InnerText);
                        objDiscountRange.Amount = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("amount").Item(0).Attributes["Amount"].InnerText);
                        //rupal:end
						//rupal:start, multicurrency
                        //objDiscountRange.BeginningRange = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("beginningrange").Item(0).InnerText);
						//objDiscountRange.EndRange = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("endrange").Item(0).InnerText);
                        if (objDiscountRangeXMLEle.GetElementsByTagName("beginningrange").Item(0).Attributes.Count > 0 && objDiscountRangeXMLEle.GetElementsByTagName("beginningrange").Item(0).Attributes["Amount"] != null)
                        {
                            objDiscountRange.BeginningRange = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("beginningrange").Item(0).Attributes["Amount"].InnerText);
                            objDiscountRange.EndRange = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("endrange").Item(0).Attributes["Amount"].InnerText);
                        }
                        else
                        {
                            objDiscountRange.BeginningRange = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("beginningrange").Item(0).InnerText);
                            objDiscountRange.EndRange = Conversion.ConvertStrToDouble(objDiscountRangeXMLEle.GetElementsByTagName("endrange").Item(0).InnerText);
                        }
                        //rupal:end

						if(bIsNew)
						{
							sSQL = "INSERT INTO " + RANGE_TABLENAME + " (DISC_RANGE_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," + 
								"UPDATED_BY_USER,AMOUNT,DISCOUNT_ROWID,BEGINNING_RANGE,END_RANGE) VALUES(" 
								+ objDiscountRange.DiscountRangeRowId + ",'" + objDiscountRange.AddedByUser + "'," + objDiscountRange.DttmRcdAdded + "," + 
								objDiscountRange.DttmRcdLastUpd + ",'" + objDiscountRange.UpdatedByUser + "'," + objDiscountRange.Amount + "," 
								+ objDiscountRange.DiscountRowid + "," + objDiscountRange.BeginningRange + "," + objDiscountRange.EndRange + ")";
						}
						else
						{
							sSQL = "UPDATE " + RANGE_TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objDiscountRange.DttmRcdLastUpd + "," + 
								"UPDATED_BY_USER='" + objDiscountRange.UpdatedByUser + "'," + "DISCOUNT_ROWID=" + objDiscountRange.DiscountRowid + "," + 
								"AMOUNT=" + objDiscountRange.Amount + "," + "BEGINNING_RANGE=" + objDiscountRange.BeginningRange + "," + 
								"END_RANGE=" + objDiscountRange.EndRange + " WHERE DISC_RANGE_ROWID=" + objDiscountRange.DiscountRangeRowId; 
						}
						objCon.ExecuteNonQuery(sSQL);

						objDiscountRange = null;
					}

					return true;
				}
				else
					return false;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("DiscountParms.Save.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				if(objCon != null)
					objCon.Dispose();

				if(objDiscount != null)
					objDiscount = null;
	
				if(objDiscountXMLEle != null)
					objDiscountXMLEle = null;

				if(objDiscRangeNodeList != null)
					objDiscRangeNodeList = null;

				if(objDiscountRange != null)
					objDiscountRange = null; 
			}
		}
	

		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 6 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves Discount record on the basis of DiscountRowId passed
		/// If new record then DiscountRowId will be empty string
		/// </summary>
		/// <param name="p_iDiscountRangeRowId">Discount Range Row ID</param>
		/// <returns>True for sucess and false for failure</returns>
		public bool Delete(int p_iDiscountRangeRowId)
		{
			string sSQL = "";
			DbConnection objCon = null;

			try
			{
				sSQL = "DELETE FROM " + RANGE_TABLENAME + " WHERE DISC_RANGE_ROWID = " + p_iDiscountRangeRowId;
				objCon = DbFactory.GetDbConnection(m_sConnectString);
				objCon.Open();
				objCon.ExecuteNonQuery(sSQL);
				objCon.Dispose();

				return true;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("DiscountParms.Delete.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				if(objCon != null)
					objCon.Dispose();
			}
		}

		/// Name		: Validate
		/// Author		: Anurag Agarwal
		/// Date Created	: 6 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Validate Discount data for the values passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<discount>
		///						<discountname></discountname>
		///						<lob></lob>
		///						<state></state>
		///						<discounttype></discounttype>
		///						<amount></amount>
		///						<expirationdate></expirationdate>
		///						<effectivedate></effectivedate>
		///						<discountrowid>4</discountrowid>
		///					</discount>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		private bool Validate(XmlDocument p_objInputXMLDoc)
		{
            // Naresh Volume Discount Implementation
            string sUseVolumeDiscount = "";
			string sDiscountRowId = "";
			string sLOB = "";
			string sState = "";
			string sEffectiveDate = "";
			string sExpirationDate = "";
			string sDiscountName = "";
			string sSQL = "";
			DbReader objDBReader = null;
			XmlElement objDiscountXMLEle = null;
            XmlNodeList objDiscountRangeXMLNode = null;

            double dBegRange = 0;
            string sEndRange = "";
            string sAmount = "";

            double dBegRangePrev = 0;
            double dEndRangePrev = 0;
            double dAmountPrev = 0;
            string sSortedListValuePrev = "";

            double dBegRangeNext = 0;
            double dEndRangeNext = 0;
            double dAmountNext = 0;
            string sSortedListValueNext = "";

            // Start Naresh 9/20/2007 MITS 10403
            int iDecimalPos = 0;
            int iCharAfterDecimal = 0;
            // End Naresh 9/20/2007 MITS 10403

            double dEndRange = 0;

            SortedList slRanges = null;

			try
			{
				objDiscountXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/discount");

                // Naresh Volume Discount Implementation
                sUseVolumeDiscount = objDiscountXMLEle.GetElementsByTagName("UseVolDisc").Item(0).InnerText;
				sDiscountRowId = objDiscountXMLEle.GetElementsByTagName("discountrowid").Item(0).InnerText;
				sLOB = objDiscountXMLEle.GetElementsByTagName("lob").Item(0).Attributes["codeid"].Value;
				sState = objDiscountXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value;
				sEffectiveDate = objDiscountXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
				sExpirationDate = objDiscountXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
				sDiscountName = objDiscountXMLEle.GetElementsByTagName("discountname").Item(0).InnerText;

				 //abansal23: MITS 15213 Starts
                
                if (!string.IsNullOrEmpty(sExpirationDate))
                {
                    sSQL = "SELECT DISCOUNT_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + sLOB +
                        " AND STATE = " + sState + " AND DISCOUNT_NAME='" + sDiscountName + "' AND EXPIRATION_DATE = '" +
                        Conversion.GetDate(sExpirationDate) + "' AND EFFECTIVE_DATE = '" + Conversion.GetDate(sEffectiveDate) + "'";
                }
                else
                {
                    sSQL = "SELECT DISCOUNT_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + sLOB +
                        " AND STATE = " + sState + " AND DISCOUNT_NAME='" + sDiscountName + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE = '' ))   AND EFFECTIVE_DATE = '" + Conversion.GetDate(sEffectiveDate) + "'";
                }
                //abansal23: MITS 15213 Ends
			
				objDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

				if(objDBReader != null)
				{
					while(objDBReader.Read())
					{
						if(objDBReader["DISCOUNT_ROWID"].ToString() != sDiscountRowId)
                            throw new RMAppException(Globalization.GetString("DiscountParms.Validate.AlreadyExistsWithNoExpDate", m_iClientId));
					}
					objDBReader.Close();
				}

                // Naresh Volume Discount Implementation
                if (sUseVolumeDiscount.ToLower() == "true")
                {
                    // Start Naresh 9/20/2007 Fixed with MITS 10403
                    // Check That this is the only Volume Discount for the Selected State and Period
                    sSQL = "SELECT DISCOUNT_ROWID,EFFECTIVE_DATE,EXPIRATION_DATE FROM " + TABLENAME + " WHERE STATE = " + sState + " AND USE_VOLUME_DISCOUNT = -1 ";
                    objDBReader = DbFactory.GetDbReader(m_sConnectString, sSQL);

                    if (objDBReader != null)
                    {
                        while (objDBReader.Read())
                        {
                            if (objDBReader["DISCOUNT_ROWID"].ToString() != sDiscountRowId)
                            {
                                if (objDBReader["EXPIRATION_DATE"].ToString() == "" &&
                                    sExpirationDate == "")
                                {
                                    throw new RMAppException(Globalization.GetString("DiscountParms.Validate.OnlyOneVolumeDiscount", m_iClientId));
                                }
                                else if (objDBReader["EXPIRATION_DATE"].ToString() != "" &&
                                    sExpirationDate == "")
                                {
                                    if(DateTime.Parse(sEffectiveDate) < Conversion.ToDate(objDBReader["EXPIRATION_DATE"].ToString()))
                                    {
                                        throw new RMAppException(Globalization.GetString("DiscountParms.Validate.OnlyOneVolumeDiscount", m_iClientId));
                                    }
                                }
                                else if (objDBReader["EXPIRATION_DATE"].ToString() == "" &&
                          sExpirationDate != "")
                                {
                                    if (DateTime.Parse(sExpirationDate) > Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString()))
                                    {
                                        throw new RMAppException(Globalization.GetString("DiscountParms.Validate.OnlyOneVolumeDiscount", m_iClientId));
                                    }
                                }
                                else
                                {
                                    if (!((DateTime.Parse(sExpirationDate) < Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString())) ||
                                        (DateTime.Parse(sEffectiveDate) > Conversion.ToDate(objDBReader["EXPIRATION_DATE"].ToString()))))
                                    {
                                        throw new RMAppException(Globalization.GetString("DiscountParms.Validate.OnlyOneVolumeDiscount", m_iClientId));
                                    }
                                }
                            }
                                
                        }
                        objDBReader.Close();
                    }
                    // End Naresh 9/20/2007 Fixed with MITS 10403

                    // Check if there are Ranges with the Volume Discount or Not
                    objDiscountRangeXMLNode = p_objInputXMLDoc.SelectNodes("//Document/discount/discountrangelist/discountrange");
                    if (objDiscountRangeXMLNode == null)
                    {
                        throw new RMAppException(Globalization.GetString("DiscountParms.Validate.RangeExistence", m_iClientId));
                    }
                    else if (objDiscountRangeXMLNode.Count == 0)
                    {
                        throw new RMAppException(Globalization.GetString("DiscountParms.Validate.RangeExistence", m_iClientId));
                    }
                    else
                    {

                        slRanges = new SortedList();

                        objDiscountRangeXMLNode = p_objInputXMLDoc.SelectNodes("//Document/discount/discountrangelist/discountrange");

                        foreach (XmlNode objNode in objDiscountRangeXMLNode)
                        {
                            //rupal:start, multicurrency
                            /*
                            dBegRange = Conversion.ConvertStrToDouble(((XmlElement)objNode).GetElementsByTagName("beginningrange").Item(0).InnerText);
                            sEndRange = ((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).InnerText;
                            sAmount = ((XmlElement)objNode).GetElementsByTagName("amount").Item(0).InnerText;
                             * */
                            dBegRange = Conversion.ConvertStrToDouble(((XmlElement)objNode).GetElementsByTagName("beginningrange").Item(0).Attributes["Amount"].InnerText);
                            sEndRange = ((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).Attributes["Amount"].InnerText;
                            sAmount = ((XmlElement)objNode).GetElementsByTagName("amount").Item(0).Attributes["Amount"].InnerText;
                            //rupal:end
                            if (sAmount.IndexOf('%') >= 0)
                                sAmount = sAmount.Remove(sAmount.IndexOf('%'));

                            slRanges.Add(dBegRange, sEndRange + "|" + sAmount);
                        }

                        for (int iCount = 0; iCount < slRanges.Count - 1; iCount++)
                        {
                            sSortedListValuePrev = slRanges.GetByIndex(iCount).ToString();
                            sSortedListValueNext = slRanges.GetByIndex(iCount + 1).ToString();
                            dEndRangePrev = Conversion.ConvertStrToDouble(sSortedListValuePrev.Substring(0, sSortedListValuePrev.IndexOf('|')));
                            dBegRangeNext = Conversion.ConvertObjToDouble(slRanges.GetKey(iCount + 1), m_iClientId);

                            // Start Naresh 9/20/2007 MITS 10403
                            iDecimalPos = dEndRangePrev.ToString().IndexOf('.');
                            if (iDecimalPos != -1)
                                iCharAfterDecimal = dEndRangePrev.ToString().Substring(iDecimalPos + 1).Length;
                            else
                                iCharAfterDecimal = 0;

                            double dblAmountAdd = 1 / Math.Pow(10,iCharAfterDecimal);

                            double dblAmount = Convert.ToDouble((dEndRangePrev + dblAmountAdd).ToString());

                            // End Naresh 9/20/2007 MITS 10403
                            if (dblAmount != dBegRangeNext)
                                throw new RMAppException(Globalization.GetString("DiscountParms.Validate.RangeContinuous", m_iClientId));

                            dAmountPrev = Conversion.ConvertStrToDouble(sSortedListValuePrev.Substring(sSortedListValuePrev.IndexOf('|') + 1));
                            dAmountNext = Conversion.ConvertStrToDouble(sSortedListValueNext.Substring(sSortedListValueNext.IndexOf('|') + 1));

                            if (dAmountPrev > dAmountNext)
                            {
                                throw new RMAppException(Globalization.GetString("DiscountParms.Validate.AmountNotInSync", m_iClientId));
                            }
                            // Naresh -- If the Upper Bound for the Last Range is not defined Show Message
                            // Dont allow the User to Save
                            // Start Naresh 9/20/2007 fixed with MITS 10403
                            if (iCount == slRanges.Count - 2)
                            {
                                dEndRange = Conversion.ConvertStrToDouble(sSortedListValueNext.Substring(0, sSortedListValueNext.IndexOf('|')));
                                if (dEndRange != 0)
                                {
                                    throw new RMAppException(Globalization.GetString("DiscountParms.Validate.LastRangeNotBlank", m_iClientId));
                                }
                            }
                            // End Naresh 9/20/2007 MITS 10403
                        }
                        // If there is only one Range then For loop will not be Executed.
                        // Checking whether the Last Range has Blank Uppr Bound or Not
                        // Start Naresh 9/20/2007 fixed with MITS 10403
                        if (slRanges.Count == 1)
                        {
                            sSortedListValuePrev = slRanges.GetByIndex(0).ToString();
                            dEndRange = Conversion.ConvertStrToDouble(sSortedListValuePrev.Substring(0, sSortedListValuePrev.IndexOf('|')));
                            if (dEndRange != 0)
                            {
                                throw new RMAppException(Globalization.GetString("DiscountParms.Validate.LastRangeNotBlank", m_iClientId));
                            }
                        }
                        // End Naresh 9/20/2007 fixed with MITS 10403
                    }
                }
                 // If the Discount Getting Saved is not Volume Discount 
                // Then there should Not be any range without Upper Bound
                else
                {
                     objDiscountRangeXMLNode = p_objInputXMLDoc.SelectNodes("//Document/discount/discountrangelist/discountrange");

                     foreach (XmlNode objNode in objDiscountRangeXMLNode)
                     {
                         //rupal:multicurrency
                         //sEndRange = ((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).InnerText;
                         if (((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).Attributes.Count >= 1 && ((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).Attributes["Amount"] != null)
                         {
                             sEndRange = ((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).Attributes["Amount"].InnerText;
                         }
                         else
                         {
                             sEndRange = ((XmlElement)objNode).GetElementsByTagName("endrange").Item(0).InnerText;
                         }
                         //rupal:end
                         dEndRange = Conversion.ConvertStrToDouble(sEndRange);

                         if (dEndRange == 0)
                         {
                             throw new RMAppException(Globalization.GetString("DiscountParms.Validate.UpperBoundBlank", m_iClientId));
                         }
                     }
                }

				return true;
			}

			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("DiscountParms.Validate.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				if(objDBReader != null)
					objDBReader.Dispose();
				objDiscountXMLEle = null;
			}
		}

	}

	/// <summary>
	/// Summary description for Discount.
	/// </summary>
	public class Discount
	{
        private int m_iClientId = 0;

        public Discount(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}


		/// <summary>
		/// Stores the Connection string
		/// </summary>
		private string m_sConnectString = "";

		/// <summary>
		/// Stores the DiscountRowid value
		/// </summary>
		private int m_iDiscountRowId = 0;

        // Naresh Volume Discount Implementation
        /// <summary>
        /// Stores Whether the Discount getting Saved is a Volume Discount or not.
        /// </summary>
        private int m_iUseVolumeDiscount = 0;

        /// <summary>
		/// Stores the DiscountName value
		/// </summary>
		private string m_sDiscountName = string.Empty;

		/// <summary>
		/// Stores the LOB value
		/// </summary>
		private int m_iLOB = 0;

		/// <summary>
		/// Stores the State value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the Amount value
		/// </summary>
		private double m_dAmount = 0.0;

		/// <summary>
		/// Stores the DiscountType value
		/// </summary>
		private int m_iDiscountType = 0;

		/// <summary>
		/// Stores the Range value
		/// </summary>
		private double m_dRange = 0.0;

		/// <summary>
		/// Stores the EffectiveDate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the ExpirationDate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the DiscountRangeList value
		/// </summary>
		private DiscountRangeList m_objDiscountRangeList = null;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

		/// <summary>
		/// Read/Write property for DiscountName
		/// </summary>
		internal string ConnectionString
		{
			get
			{
				return m_sConnectString;
			}
			set
			{
				m_sConnectString = value;
			}
		}

		/// <summary>
		/// Read/Write property for DiscountRowid
		/// </summary>
		internal int DiscountRowId
		{
			get
			{
				return m_iDiscountRowId;
			}
			set
			{
				m_iDiscountRowId = value;
			}
		}

        // Naresh Volume Discount Implementation
        /// <summary>
        /// Read/Write property for Use Volume Discount
        /// </summary>
        internal int UseVolumeDiscount
        {
            get
            {
                return m_iUseVolumeDiscount;
            }
            set
            {
                m_iUseVolumeDiscount = value;
            }
        }

		/// <summary>
		/// Read/Write property for DiscountName
		/// </summary>
		internal string DiscountName
		{
			get
			{
				return m_sDiscountName;
			}
			set
			{
				m_sDiscountName = value;
			}
		}

		/// <summary>
		/// Read/Write property for LOB
		/// </summary>
		internal int LOB
		{
			get
			{
				return m_iLOB;
			}
			set
			{
				m_iLOB = value;
			}
		}

		/// <summary>
		/// Read/Write property for State
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for Amount
		/// </summary>
		internal double Amount
		{
			get
			{
				return m_dAmount;
			}
			set
			{
				m_dAmount = value;
			}
		}

		/// <summary>
		/// Read/Write property for DiscountType
		/// </summary>
		internal int DiscountType
		{
			get
			{
				return m_iDiscountType;
			}
			set
			{
				m_iDiscountType = value;
			}
		}

		/// <summary>
		/// Read/Write property for Range
		/// </summary>
		internal double Range
		{
			get
			{
				return m_dRange;
			}
			set
			{
				m_dRange = value;
			}
		}

		/// <summary>
		/// Read/Write property for EffectiveDate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpirationDate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read property for DiscountRangeList
		/// </summary>
		internal DiscountRangeList DiscountRangeList
		{
			get
			{
				if(m_objDiscountRangeList == null)
				{
					m_objDiscountRangeList = new DiscountRangeList(m_sConnectString, m_iClientId);
					m_objDiscountRangeList.Parent = this;
					m_objDiscountRangeList.DiscountRowid = m_iDiscountRowId;
					if(m_iDiscountRowId != 0)
						m_objDiscountRangeList.LoadData(); 
				}
				return m_objDiscountRangeList;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
	}
}
