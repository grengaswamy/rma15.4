﻿
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;
using System.Text;
using System.Collections.Generic;
using Riskmaster.DataModel; //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
using Riskmaster.Settings;  //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)


namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Animesh Sahai 
    ///Dated   :   2/Nov/2007
    ///Purpose :   AdjusterScreen form
    /// </summary>
    public class AdjusterScreen : UtilitiesUIBase
    {
        /// <summary>
        /// Database fields mapping string array
        /// </summary>
        private string[,] arrFields = {
			{"RowId", "AUTO_ASSIGN_ID"},
            {"AutoAssignID","AUTO_ASSIGN_ID"},
			{"AdjusterEID","ADJ_EID"},
            {"AddedByUser","ADDED_BY_USER"},
            {"UpdatedByUser","UPDATED_BY_USER"},
            {"DateTimeRecordAdded","DTTM_RCD_ADDED"},
            {"DateTimeRecordLastUpdated","DTTM_RCD_LAST_UPD"},
            {"SkipWorkItems","SKIP_WORK_ITEMS"},
            {"ForceWorkItems","FORCE_WORK_ITEMS"},
            {"DeptEIds","DEPT_EIDS"},
            {"LOBCodes","LOB_CODES"},
            {"ClaimTypeCodes","CLAIM_TYPE_CODES"},
            {"JurisdictionCodes","JURISDICTION_CODES"}
		  };
        //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
        private string DsnName = "";
        private string m_Password = "";
        private string m_UserName = "";         
        private DataModelFactory m_objDataModelFactory = null;
        Riskmaster.Settings.SysSettings objSysSettings = null;
        //Rijul : Worked for 7573 (Issue of 4634 - Epic 340) end
        private int m_iClientId = 0;//Add by kuladeep for Cloud. Jira-58

        /// Name		: AdjusterScreen
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default constructor with connection string
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>
        public AdjusterScreen(string p_UserName, string p_Password, string p_sDatabaseName, string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)  //Rijul : Worked for 7573 (Issue of 4634 - Epic 340)
        {
            //Rijul start: Worked for 7573 (Issue of 4634 - Epic 340)
            m_UserName = p_UserName;
            m_Password = p_Password;
            DsnName = p_sDatabaseName;
            //Rijul : Worked for 7573 (Issue of 4634 - Epic 340) end
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;//Add by kuladeep for Cloud. Jira-58
            this.Initialize();
        }

        /// Name		: Initialize
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            TableName = "AUTO_ASSIGN_ADJ";
            KeyField = "AUTO_ASSIGN_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        /// Name		: New
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Ge the structure for a new record
        /// </summary>
        /// <param name="p_objXMLDocument">XML Doc</param>
        /// <returns>Xmldocument with structure</returns>
        public XmlDocument New(XmlDocument p_objXMLDocument)
        {
            try
            {
                return p_objXMLDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NewError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
        }

        /// Name		: Get
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlNodeList objNodeList = null;
            string sControlType = "";
            string sValue = "";
            try
            {
                XMLDoc = p_objXmlDocument;
                base.Get();
                objNodeList = p_objXmlDocument.GetElementsByTagName("control");
                foreach(XmlElement objXmlElement in objNodeList)
                {
                    if(objXmlElement.GetAttribute("name") == "AdjusterEID")
	                {
		                sValue=objXmlElement.InnerText; 
		                sControlType=objXmlElement.GetAttribute("type").ToLower();
		                switch(sControlType)
		                {
			                case "entitylookup":
				                objXmlElement.SetAttribute("codeid",sValue);
                                objXmlElement.InnerText = UTILITY.GetEmpDetails(sValue, base.ConnectString, m_iClientId);
				                break;
		                }
	                }
                }
                p_objXmlDocument = ReloadNonAvailPlan(p_objXmlDocument);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjusterGetError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
            finally
            {
                objNodeList = null;
            }

        }

        /// Name		: Delete
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes a particular record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>next record if any</returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                XMLDoc = p_objXmlDocument;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
                if (objElm != null)
                {
                    base.WhereClause = "ORG_EID=" + objElm.InnerText;
                }
                base.Delete();
                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjusterDeleteError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
            finally
            {
                objElm = null;
            }
        }

        /// Name		: Save
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <param name="strLoggedInUser">Logged in user name</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument, string strLoggedInUser)
        {
            string sAutoAdjID = string.Empty;
            string sOrgId = string.Empty;
            string sSkipWorkItems = string.Empty;
            string sForceWorkItems = string.Empty;
            string sAdjEId = string.Empty;
            string sDeptEIds = string.Empty;
            string sLOBCodes = string.Empty;
            string sClaimTypeCodes = string.Empty;
            string sJurisdictionCodes = string.Empty;
            StringBuilder strSQL = new StringBuilder();
            string sSQL = string.Empty;
            XmlElement objElm = null;
            string[] splitItems = null;
            bool bIsOracle = DbFactory.IsOracleDatabase(ConnectString);
            //Rijul start: Worked for 7573 (Issue of 4634 - Epic 340)
            LocalCache objCache = null;
            objSysSettings = new Settings.SysSettings(ConnectString, m_iClientId);
            objCache = new LocalCache(ConnectString, m_iClientId);     
            m_objDataModelFactory = new DataModelFactory(DsnName, m_UserName, m_Password, m_iClientId);
            //Rijul : Worked for 7573 (Issue of 4634 - Epic 340) end
            DbConnection objConn;
            DbReader objDbReader = null;

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AutoAssignID']");
            if (objElm != null)
                sAutoAdjID = objElm.InnerText;

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AdjusterEID']");
            if (objElm != null)
                sAdjEId = objElm.GetAttribute("codeid");

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SkipWorkItems']");
            if (objElm != null)
                sSkipWorkItems = objElm.InnerText;

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ForceWorkItems']");
            if (objElm != null)
                sForceWorkItems = objElm.InnerText;

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name ='LOBCodes']");
            if (objElm != null && objElm.Attributes["codeid"] != null)
                sLOBCodes = objElm.Attributes["codeid"].Value;

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name ='DeptEIds']");
            if (objElm != null && objElm.Attributes["codeid"] != null)
                sDeptEIds = objElm.Attributes["codeid"].Value;

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name ='ClaimTypeCodes']");
            if (objElm != null && objElm.Attributes["codeid"] != null)
                sClaimTypeCodes = objElm.Attributes["codeid"].Value;

            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name ='JurisdictionCodes']");
            if (objElm != null && objElm.Attributes["codeid"] != null)
                sJurisdictionCodes = objElm.Attributes["codeid"].Value;

            if (sSkipWorkItems == String.Empty)
            {
                sSkipWorkItems = "0";
            }

            if (sForceWorkItems == String.Empty)
            {
                sForceWorkItems = "0";
            }

            if (string.IsNullOrEmpty(sAdjEId) || sAdjEId == "0")
            {
                throw new RMAppException(Globalization.GetString("NoAdjusterSaveError", m_iClientId));
            }

            //PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
            //using (objConn = DbFactory.GetDbConnection(ConnectString))
            //{
            //    objConn.Open();

                
                Dictionary<string, object> dictParams = new Dictionary<string, object>();
                
                strSQL.Append(String.Format("SELECT AUTO_ASSIGN_ID FROM AUTO_ASSIGN_ADJ WHERE ADJ_EID = {0}", "~ADJ_EID~"));
                dictParams.Add("ADJ_EID", sAdjEId);

                if (sAutoAdjID != string.Empty)
                {
                    strSQL.Append(String.Format(" AND AUTO_ASSIGN_ID <> {0}", "~AUTO_ASSIGN_ID~"));
                    dictParams.Add("AUTO_ASSIGN_ID", sAutoAdjID);
                }

                //strSQL.Append(String.Format("SELECT AUTO_ASSIGN_ID FROM AUTO_ASSIGN_ADJ WHERE ADJ_EID = {0}", sAdjEId));
                //if (sAutoAdjID != string.Empty)
                //{
                //    strSQL.Append(String.Format(" AND AUTO_ASSIGN_ID <> {0}", sAutoAdjID));
                //}
                //END PenTesting - MITS 26941 - srajindersin - 11th Jan 2012

                if (sDeptEIds != string.Empty)
                {
                    strSQL.Append(String.Format(" AND (DEPT_EIDS IS NULL OR {0})", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("DEPT_EIDS", sDeptEIds, bIsOracle)));
                }
                if (sLOBCodes != string.Empty)
                {
                    strSQL.Append(String.Format(" AND (LOB_CODES IS NULL OR {0})", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("LOB_CODES", sLOBCodes, bIsOracle)));
                }
                if (sClaimTypeCodes != string.Empty)
                {
                    strSQL.Append(String.Format(" AND (CLAIM_TYPE_CODES IS NULL OR {0})", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("CLAIM_TYPE_CODES", sClaimTypeCodes, bIsOracle)));
                }
                if (sJurisdictionCodes != string.Empty)
                {
                    strSQL.Append(String.Format(" AND (JURISDICTION_CODES IS NULL OR {0})", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("JURISDICTION_CODES", sJurisdictionCodes, bIsOracle)));
                }

                //PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
                objDbReader = DbFactory.ExecuteReader(ConnectString, strSQL.ToString(), dictParams);
                if (objDbReader != null && objDbReader.Read())
                {
                    throw new RMAppException(Globalization.GetString("AutoAdjNotUniqueSaveError", m_iClientId));//Add by kuladeep for Cloud. Jira-58
                }
                
                //using (objDbReader = objConn.ExecuteReader(strSQL.ToString()))
                //{
                //    if (objDbReader != null && objDbReader.Read())
                //    {
                //        throw new RMAppException(Globalization.GetString("AutoAdjNotUniqueSaveError"));
                //    }
                //}
                //END PenTesting - MITS 26941 - srajindersin - 11th Jan 2012

                //Deb Commented exexuted below
                //sDeptEIds = SortCodes(sDeptEIds);
                //sLOBCodes = SortCodes(sLOBCodes);
                //sClaimTypeCodes = SortCodes(sClaimTypeCodes);
                //sJurisdictionCodes = SortCodes(sJurisdictionCodes);
                //Deb Commented exexuted below
                //Deb Added Extra Condition to Check the sAutoAdjID Can't be "0 and add check before null entry
                if (sAutoAdjID != string.Empty && sAutoAdjID != "0") //updating an existing record
                {
                    //PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
                    DbWriter writer = DbFactory.GetDbWriter(ConnectString);
                    writer.Tables.Add("AUTO_ASSIGN_ADJ");
                    writer.Where.Add(String.Format("AUTO_ASSIGN_ID={0}",sAutoAdjID));

                    writer.Fields.Add("ADJ_EID", sAdjEId);
                    writer.Fields.Add("SKIP_WORK_ITEMS", sSkipWorkItems);
                    writer.Fields.Add("UPDATED_BY_USER", strLoggedInUser);
                    writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                    writer.Fields.Add("DEPT_EIDS", " " + SortCodes(sDeptEIds)+" ");
                    writer.Fields.Add("LOB_CODES", " "+SortCodes(sLOBCodes)+" ");
                    writer.Fields.Add("FORCE_WORK_ITEMS", sForceWorkItems);
                    writer.Fields.Add("CLAIM_TYPE_CODES", " "+SortCodes(sClaimTypeCodes)+" ");
                    writer.Fields.Add("JURISDICTION_CODES", " "+SortCodes(sJurisdictionCodes)+" ");
                    writer.Execute();

                    //sSQL = string.Format("UPDATE AUTO_ASSIGN_ADJ SET ADJ_EID = {0}, SKIP_WORK_ITEMS = {1}, UPDATED_BY_USER = '{2}', DTTM_RCD_LAST_UPD = '{3}', DEPT_EIDS = {4}, LOB_CODES = {5}, FORCE_WORK_ITEMS = {7} , CLAIM_TYPE_CODES = {8} , JURISDICTION_CODES = {9} WHERE AUTO_ASSIGN_ID = {6}", sAdjEId, sSkipWorkItems, strLoggedInUser, Conversion.ToDbDateTime(DateTime.Now), sDeptEIds, sLOBCodes, sAutoAdjID, sForceWorkItems, sClaimTypeCodes, sJurisdictionCodes);
                    //objConn.ExecuteNonQuery(sSQL);
                    //END PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
                }
                else //a new record, first validate; if ok then create the new record
                {
                    long lngAutoAssignID = Utilities.GetNextUID(ConnectString, "AUTO_ASSIGN_ADJ", m_iClientId);
                    //PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
                    DbWriter writer = DbFactory.GetDbWriter(ConnectString);
                    writer.Tables.Add("AUTO_ASSIGN_ADJ");

                    writer.Fields.Add("AUTO_ASSIGN_ID", lngAutoAssignID);
                    writer.Fields.Add("ADJ_EID", sAdjEId);
                    writer.Fields.Add("SKIP_WORK_ITEMS", sSkipWorkItems);
                    writer.Fields.Add("ADDED_BY_USER", strLoggedInUser);
                    writer.Fields.Add("UPDATED_BY_USER", strLoggedInUser);
                    writer.Fields.Add("DTTM_RCD_ADDED", Conversion.ToDbDateTime(DateTime.Now));
                    writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                    writer.Fields.Add("DEPT_EIDS", " "+SortCodes(sDeptEIds)+" ");
                    writer.Fields.Add("LOB_CODES", " " + SortCodes(sLOBCodes) + " ");
                    writer.Fields.Add("FORCE_WORK_ITEMS", sForceWorkItems);
                    writer.Fields.Add("CLAIM_TYPE_CODES", " " + SortCodes(sClaimTypeCodes) + " ");
                    writer.Fields.Add("JURISDICTION_CODES", " " + SortCodes(sJurisdictionCodes) + " ");
                    writer.Execute();


                    //sSQL = string.Format("INSERT INTO AUTO_ASSIGN_ADJ(AUTO_ASSIGN_ID, ADJ_EID, SKIP_WORK_ITEMS, ADDED_BY_USER, UPDATED_BY_USER, DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, DEPT_EIDS, LOB_CODES, FORCE_WORK_ITEMS, CLAIM_TYPE_CODES, JURISDICTION_CODES) VALUES ({0}, {1}, {2}, '{3}', '{4}', '{5}', '{6}', {7}, {8}, {9}, {10}, {11})", lngAutoAssignID, sAdjEId, sSkipWorkItems, strLoggedInUser, strLoggedInUser, Conversion.ToDbDateTime(DateTime.Now), Conversion.ToDbDateTime(DateTime.Now), sDeptEIds, sLOBCodes, sForceWorkItems, sClaimTypeCodes, sJurisdictionCodes);
                    //objConn.ExecuteNonQuery(sSQL);
                    //END PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
                }
            // PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
            //}
            //END PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
            return XMLDoc;
        }

        /// Name		: ReloadNonAvailPlan
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>
        public XmlDocument ReloadNonAvailPlan(XmlDocument p_objXmlDocument)
        {

            string sAdjusterID = string.Empty;
            DataSet objDs = null;
            XmlDocument subDoc = null;
            XmlElement objLstRow = null;
            //XmlElement objMainRow = null;
            XmlElement objRowTxt = null;
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            XmlElement objElm = null;
            LocalCache objLocalCache = null;
            string sSQL = "";
            string sDeptEIds = string.Empty;
            string sLOBCodes = string.Empty;
            string sClaimTypeCodes = string.Empty;
            string sJurisdictionCodes = string.Empty;
            try
            {
                objLocalCache = new LocalCache(ConnectString, m_iClientId);
                subDoc = new XmlDocument();
                subDoc.LoadXml("<NonAvailPlan><listhead><RowId>Rowid</RowId><StartDateTime type='Date'>Start Date Time</StartDateTime><EndDateTime type='Date'>End Date Time</EndDateTime></listhead></NonAvailPlan>");

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AdjusterEID']");
                if (objElm != null)
                {
                    sAdjusterID = objElm.GetAttribute("codeid");
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DeptEIds']");
                if (objElm != null && objElm.ChildNodes.Count == 0)
                {
                    sDeptEIds = objElm.GetAttribute("codeid");
                    UTILITY.GetXmlMultiCodeInfo(ref objElm, ref p_objXmlDocument, sDeptEIds, ref objLocalCache, m_iClientId);
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LOBCodes']");
                if (objElm != null && objElm.ChildNodes.Count == 0)
                {
                    sLOBCodes = objElm.GetAttribute("codeid");
                    UTILITY.GetXmlMultiCodeInfo(ref objElm, ref p_objXmlDocument, sLOBCodes, ref objLocalCache, m_iClientId);
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ClaimTypeCodes']");
                if (objElm != null && objElm.ChildNodes.Count == 0)
                {
                    sClaimTypeCodes = objElm.GetAttribute("codeid");
                    UTILITY.GetXmlMultiCodeInfo(ref objElm, ref p_objXmlDocument, sClaimTypeCodes, ref objLocalCache, m_iClientId);
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='JurisdictionCodes']");
                if (objElm != null && objElm.ChildNodes.Count == 0)
                {
                    sJurisdictionCodes = objElm.GetAttribute("codeid");
                    UTILITY.GetXmlMultiCodeInfo(ref objElm, ref p_objXmlDocument, sJurisdictionCodes, ref objLocalCache, m_iClientId);
                }

                if (sAdjusterID != "")
                {
                    //PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
                    sSQL = string.Format("SELECT ROW_ID, ENTITY_NON_AVL_START, ENTITY_NON_AVL_END FROM ENTITY_NON_AVL WHERE ENTITY_ID = {0}", "~ENTITY_ID~");
                    Dictionary<string, object> dictParams = new Dictionary<string, object>();
                    dictParams.Add("ENTITY_ID", sAdjusterID);

                    objDs = DbFactory.ExecuteDataSet(ConnectString, sSQL, dictParams, m_iClientId);

                    //sSQL = "SELECT ROW_ID, ENTITY_NON_AVL_START, ENTITY_NON_AVL_END FROM ENTITY_NON_AVL WHERE" +
                    //       " ENTITY_ID = " + sAdjusterID;
                    //objDs = DbFactory.GetDataSet(ConnectString, sSQL);
                    //END PenTesting - MITS 26941 - srajindersin - 11th Jan 2012
                    

                    objNode = p_objXmlDocument.SelectSingleNode("//*[name()='NonAvailPlan']");
                    //objNodLst = objNode.ChildNodes.Item(0).ChildNodes;
                    //objMainRow= subDoc.CreateElement("NonAvailPlan");  
                    //subDoc.InnerXml = objNode.InnerXml;
                    objNodLst = objNode.ChildNodes;
                    int i = 0;
                    while (i < objNodLst.Count)
                    //foreach (XmlNode objTempNode in objNodLst)
                    {
                        XmlNode objTempNode = objNodLst[i];
                        if (objTempNode.Name == "listrow")
                        {
                            objTempNode.ParentNode.RemoveChild(objTempNode);
                            i = i - 1;
                        }
                        i = i + 1;
                    }


                    subDoc.DocumentElement.InnerXml = objNode.InnerXml;




                    foreach (DataRow objRow in objDs.Tables[0].Rows)
                    {
                        objLstRow = subDoc.CreateElement("listrow");
                        objRowTxt = subDoc.CreateElement("RowId");
                        objRowTxt.InnerText = objRow["ROW_ID"].ToString();
                        objLstRow.AppendChild(objRowTxt);
                        objRowTxt = subDoc.CreateElement("StartDateTime");
                        objRowTxt.InnerText = objRow["ENTITY_NON_AVL_START"].ToString();
                        objRowTxt.InnerText = Conversion.ToDate(objRowTxt.InnerText).ToString();
                        objLstRow.AppendChild(objRowTxt);
                        objRowTxt = subDoc.CreateElement("EndDateTime");
                        objRowTxt.InnerText = objRow["ENTITY_NON_AVL_END"].ToString();
                        objRowTxt.InnerText = Conversion.ToDate(objRowTxt.InnerText).ToString();
                        objLstRow.AppendChild(objRowTxt);
                        subDoc.DocumentElement.AppendChild((XmlNode)objLstRow);
                    }

                    objDs.Dispose();
                }
                XmlDocumentFragment objDocFrag = p_objXmlDocument.CreateDocumentFragment();
                objDocFrag.InnerXml = subDoc.SelectSingleNode("//*[name()='NonAvailPlan']").InnerXml;

                XmlNode InsertLoc = p_objXmlDocument.DocumentElement.SelectSingleNode("NonAvailPlan");
                //p_objXmlDocument.DocumentElement.InsertAfter(objDocFrag, InsertLoc);
                InsertLoc.InnerXml = objDocFrag.InnerXml;
                return p_objXmlDocument;
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjusterReloadError", m_iClientId), p_objEx);//Add by kuladeep for Cloud. Jira-58
            }
            finally
            {
                if (objDs != null)
                {
                    objDs.Dispose();
                    objDs = null;
                }
                objLstRow = null;
                objRowTxt = null;
                objNode = null;
                objNodLst = null;
                objElm = null;
                subDoc = null;
            }
        }

        private void GetXmlMultiCodeInfo(ref XmlElement objXmlElement, ref XmlDocument objXmlDoc, string sValue, ref LocalCache objLocalCache)
        {
            StringBuilder serializedStr = new StringBuilder();
            XmlElement objTempXmlElement = null;
            string[] splitItems = null;
            string sType = string.Empty;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            if (!string.IsNullOrEmpty(sValue))
            {
                sType = objXmlElement.GetAttribute("type").ToLower();
                objXmlElement.SetAttribute("codeid", sValue);
                splitItems = sValue.Split((char)32);
                for (int i = 0; i < splitItems.Length; i++)
                {
                    if (serializedStr.Length > 0)
                    {
                        serializedStr.Remove(0, serializedStr.Length);
                    }
                    switch (sType)
                    {
                        case "multilob":
                        case "multiclaimtype":
                            objLocalCache.GetCodeInfo(Riskmaster.Common.Conversion.ConvertObjToInt(splitItems[i], m_iClientId), ref sShortCode, ref sDesc);
                            serializedStr.Append(String.Format("{0} {1}", sShortCode, sDesc));
                            break;
                        case "multijurisdiction":
                            objLocalCache.GetStateInfo(Riskmaster.Common.Conversion.ConvertObjToInt(splitItems[i], m_iClientId), ref sShortCode, ref sDesc);
                            serializedStr.Append(String.Format("{0} {1}", sShortCode, sDesc));
                            break;
                        case "multiorg":
                            serializedStr.Append(UTILITY.GetOrgName(splitItems[i], objLocalCache.DbConnectionString, m_iClientId));
                            break;
                        case "default":
                        case "":
                        default:
                            serializedStr.Append(splitItems[i]);
                            break;
                    }

                    objTempXmlElement = objXmlDoc.CreateElement("Types");
                    //objTempXmlElement.SetAttribute("name", sShortCode);
                    objTempXmlElement.SetAttribute("value", splitItems[i]);
                    objTempXmlElement.InnerText = serializedStr.ToString();

                    objXmlElement.AppendChild(objTempXmlElement);

                }
            }
        }
        //Deb Changed Return type object
        private object SortCodes(string sCodes)
        {
            string[] splitItems = null;
            if (string.IsNullOrEmpty(sCodes))
            {
                //psarin2 - Added DBNULL instead of NULL 
                //Deb Return DBNull is value is null or empty
               return  System.DBNull.Value;
            }
            else
            {
                splitItems = sCodes.Split(' ');
                Array.Sort(splitItems);
                for (int i = 0; i < splitItems.Length; i++)
                {
                    if (i == 0)
                    {
                        sCodes = splitItems[i];
                    }
                    else
                    {
                        sCodes = sCodes + ' ' + splitItems[i];
                    }
                }
                //psarin2 - No need now
                //sCodes = "'" + sCodes + "'";
            }
            return sCodes;
        }
    }
}
