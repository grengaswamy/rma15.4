using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common; 
 
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   04th,May 2005
	///Purpose :   BRS CPT Extended Code Details Form 
	/// </summary>
	public class BRSCPTExtendedCodeDetails:UtilitiesUIBase 
	{
		/// <summary>
		/// String array for database fields mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "CPT"},
			{"CPTCode", "CPT"},
			{"CodeDesc", "CPT_DESC"},
			{"Amount", "AMOUNT"},
			{"ProfComp", "PROF_COMPONENT"},
			{"FollowUp", "FOLLUP"},
			{"Strrd", "STARRED"},
			{"AsstAtSurgery", "ASSISTSURG"},
			{"AnsthValUnit", "ANESTHESIA_VU"},
			{"AnsthByRep", "ANES_BR"},
			{"ByRep", "BYREPORT"},
			{"AnsthNotAppl", "ANES_NA"},
			{"TableId", "TABLE_ID"}
			};

        private int m_iClientId = 0;
		/// Name		: BRSCPTExtendedCodeDetails
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string parameter</param>
        public BRSCPTExtendedCodeDetails(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			ConnectString = p_sConnString;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Initialize variables and properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "WRCMP_CPT";
			KeyField = "CPT";
			base.InitFields(arrFields);
			base.Initialize();
		}		

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Gets the CPT Codes details
		/// </summary>
		/// <param name="p_objXmlDocument">Input Xml document for structure</param>
		/// <returns>Xml document with data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			XMLDoc = p_objXmlDocument;
			objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
			if(objNode!=null)
                base.WhereClause = "TABLE_ID=" + objNode.InnerText; 
			base.Get();
			return XMLDoc; 
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Saves the data for the form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data and structure</param>
		/// <returns>xml document with saved data</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			int iTableId=0;
			string sCPT="";
			string sSQL="";
			XmlNode objNode=null;
			DbReader objRdr=null;
			string sCodeDesc="";
			double dAmount=0;
			double dProfComp=0;
			int iFollowUp=0;
			string sStrrd="";
			string sAsstAtSurgery="";
			double dAnsthValUnit=0;
			string sAnsthByRep="";
			string sByRep="";
			string sAnsthNotAppl="";
			DbConnection objCn=null;
			try
			{
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
				if(objNode!=null)
					iTableId = Conversion.ConvertStrToInteger(objNode.InnerText);   
				
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CPTCode']");
				if(objNode!=null)
					sCPT = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CodeDesc']");
				if(objNode!=null)
					sCodeDesc = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Amount']");
				if(objNode!=null)
					dAmount = Conversion.ConvertStrToDouble(objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ProfComp']");
				if(objNode!=null)
					dProfComp = Conversion.ConvertStrToDouble(objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='FollowUp']");
				if(objNode!=null)
					iFollowUp  = Conversion.ConvertStrToInteger(objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Strrd']");
				if(objNode!=null)
					sStrrd = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AsstAtSurgery']");
				if(objNode!=null)
					sAsstAtSurgery = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AnsthValUnit']");
				if(objNode!=null)
					dAnsthValUnit  = Conversion.ConvertStrToDouble(objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AnsthByRep']");
				if(objNode!=null)
					sAnsthByRep = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ByRep']");
				if(objNode!=null)
					sByRep = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AnsthNotAppl']");
				if(objNode!=null)
					sAnsthNotAppl = objNode.InnerText;


				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
				if(objNode!=null)
				{
					if(objNode.InnerText=="")
					{
						objRdr=DbFactory.GetDbReader(ConnectString,"SELECT * FROM WRCMP_CPT WHERE TABLE_ID=" +
								iTableId + " AND CPT='" + sCPT + "'");
						if(objRdr.Read())
						{
                            throw new RMAppException(Globalization.GetString("BRSCPTExtendedCodeDetails.Save.DupErr", m_iClientId));  
						}  
						sSQL = "INSERT INTO WRCMP_CPT(TABLE_ID,CPT,CPT_DESC,AMOUNT,PROF_COMPONENT,FOLLUP,STARRED,ASSISTSURG,ANESTHESIA_VU,ANES_BR,BYREPORT,ANES_NA) " + 
							"VALUES( " + iTableId + ",'" + sCPT + "','" + sCodeDesc + "'," + dAmount  + "," + dProfComp + "," + iFollowUp  + ",'" + sStrrd + "','" + sAsstAtSurgery + "'," + 
							dAnsthValUnit  + ",'" + sAnsthByRep + "','" + sByRep + "','" + sAnsthNotAppl + "')" ;
					}
					else
						sSQL = "UPDATE WRCMP_CPT SET CPT_DESC='" + sCodeDesc + "', AMOUNT=" + dAmount + ", PROF_COMPONENT=" + dProfComp +
								", FOLLUP= " + iFollowUp  + ",STARRED='" + sStrrd +  "',ASSISTSURG='" + sAsstAtSurgery + "',ANESTHESIA_VU='" +
							dAnsthValUnit + "',ANES_BR='" + sAnsthByRep + "',BYREPORT='" + sByRep + "',ANES_NA='" + sAnsthNotAppl + "' WHERE" +
							" TABLE_ID=" + iTableId + " AND CPT='" + sCPT + "'" ;
				}
				
				objCn= DbFactory.GetDbConnection(ConnectString);
				objCn.Open();
				objCn.ExecuteNonQuery(sSQL); 
				return p_objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSCPTExtendedCodeDetails.Save.Err", m_iClientId), p_objEx);
			}
			finally
			{
                if (objRdr != null)
                {
                    objRdr.Dispose();
                }
				objNode=null;
				if(objCn!=null)
				{
					objCn.Dispose();
				}
			}
		}

		/// Name		: New
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Get the structure for a new record.
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>Xmldocument with structure</returns>
		public XmlDocument New(XmlDocument p_objXMLDocument)
		{
			try
			{
				return p_objXMLDocument; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSCPTExtendedCodeDetails.New.Err", m_iClientId), p_objEx);
			}
		}
	}
}