﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Xml;
using System.Text;
using System.Web;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Collections;
using System.Xml.Linq;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>	
	///	which implements the functionality of Navigation Tree Customization.
	/// </summary>
    public class NavigationTreeCustomization
    {
        string m_connectionString = "";
        private Hashtable CustomOptions;
        SysSettings objSysSettings = null;   //ngupta73 : MITS- 34260/RMA-4362
        private int m_iClientId = 0;  //ngupta73 : MITS- 34260/RMA-4362
       
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string parameter</param>
        public NavigationTreeCustomization(string p_sConnString)
        {
            objSysSettings = new SysSettings(p_sConnString, m_iClientId);//ngupta73 : MITS- 34260/RMA-4362
            CustomOptions = new Hashtable();
            m_connectionString = p_sConnString;
            //Bkuzhanthaim : MITS- 36027/RMA-342: ADJ_AUTO_EXPANSTION is PARM_NAME Value of PARMS_NAME_VALUE DBTable, AdjusterExpansion is  rmxref of aspx Page Control 
            CustomOptions.Add("ADJ_AUTO_EXPANSION", "AdjusterExpansion");
            //ngupta73 : MITS- 34260/RMA-4362: POL_AUTO_EXPANSION is PARM_NAME Value of PARMS_NAME_VALUE DBTable, PolicyExpansion is  rmxref of aspx Page Control           
            CustomOptions.Add("POL_AUTO_EXPANSION", "PolicyExpansion");
        }

		/// <summary>
        /// Gets the Auto Assign Adjuster Customization data.
		/// </summary>
        public XDocument GetCustomization(XDocument XDoc)
		{
            DbConnection objCon = null;
            XElement XElm = null;
            String sSQL = String.Empty;
            try
            {
                sSQL = "SELECT PARM_NAME, STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME IN ('DUMMY_ITEM'";
                foreach (DictionaryEntry entry in CustomOptions)
                {
                    sSQL += ", '" + entry.Key.ToString() + "'";
                }
                sSQL += ")";
                using (objCon = DbFactory.GetDbConnection(m_connectionString))
                {
                    objCon.Open();
                    using (Riskmaster.Db.DbReader rdr = objCon.ExecuteReader(sSQL))
                    {
                        while (rdr.Read())
                        {
                            XElm = XDoc.Root.Element(CustomOptions[rdr["PARM_NAME"]].ToString());
                            if (XElm != null)
                            {
                                if (rdr["STR_PARM_VALUE"].ToString() == (-1).ToString() || rdr["STR_PARM_VALUE"].ToString() == (0).ToString())
                                {
                                    XElm.Value = rdr["STR_PARM_VALUE"].ToString();
                                }
                            }
                        }
                   }
                   
                }
                //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim. Start
                if (objSysSettings.MultiCovgPerClm != 0)
                {                   
                    XDoc.Root.Element("CheckCarrierClaim").Value= "True";
                }
                else
                {
                    XDoc.Root.Element("CheckCarrierClaim").Value = "False";
                }
                //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim. END
                return XDoc;
            }
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("NavigationTreeCustomization.GetCustomization.Err", m_iClientId), p_objEx); 
			}
			finally
			{
			}
		}

		/// <summary>
		/// Saves the Auto Assign Adjuster Customization to the database
		/// </summary>
		public void SaveCustomization(XDocument XDoc)
		{
            DbConnection objCon = null;
            XElement XElm = null;
            String sSQL = String.Empty;
            String sInClause = String.Empty;
			try
			{
                using (objCon = DbFactory.GetDbConnection(m_connectionString))
                {
                    objCon.Open();
                    sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = CASE PARM_NAME";
                    foreach (DictionaryEntry entry in CustomOptions)
                    {
                        XElm = XDoc.Root.Element(entry.Value.ToString());

                        if (XElm != null)
                        {
                            if (XElm.Value == (-1).ToString() || XElm.Value == (0).ToString())
                            {
                                sSQL += " WHEN '" + entry.Key.ToString() + "' THEN '" + XElm.Value + "'";
                                if (sInClause == String.Empty)
                                {
                                    sInClause = "('" + entry.Key.ToString() + "'";
                                }
                                else
                                {
                                    sInClause += ",'" + entry.Key.ToString() + "'";
                                }
                            }
                        }
                    }
                    if (sInClause != String.Empty)
                    {
                        sSQL = sSQL + " END WHERE PARM_NAME IN " + sInClause + ")";
                        objCon.ExecuteNonQuery(sSQL); //update a column (for multiple records) using one query!
                        //Bkuzhanthaim : RMA-3754 / RMA-2718 : The changes will be reflect after few minutes or after the iis reset : Started
                        SysSettings objSys = new SysSettings(m_connectionString, m_iClientId);
                        objSys.SaveSystemSettings();
                        //Bkuzhanthaim : RMA-3754 / RMA-2718 : End
                    }

                }
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("NavigationTreeCustomization.SaveCustomization.Err", m_iClientId), p_objEx); 
			}
		}
	}
}
