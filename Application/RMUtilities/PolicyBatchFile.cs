﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SecurityManagement;
using System.IO;
using Riskmaster.Application.PolicySystemInterface;


namespace Riskmaster.Application.RMUtilities
{
    public class PolicyBatchFile 
    {
        private string m_sUserName = string.Empty;
        private string m_sConnectString = string.Empty;
        private int m_iClientId = 0;
     public PolicyBatchFile(string conn)
        {
            m_sConnectString = conn;
        }
     struct RealTimeUpdate
     {
         private string sPolicyNumber;
         private string sPolicyName;
         private string sPolicyType;
         private string sVoidDate;
         private string sClaimNumber;
         private string sControlNumber;
         private string sVoidFlag;
         private string sDateOfCheck;
         private string sTransactionNumber;
         private string sTransactionDate;



         private string sClearedFlag;
         private string sPaymentFlag;
         private string sCollectionFlag;
         private string sStatus;
         private string sReserveType;
         private string sTransactionType;
         private string sSplitAmount;
         private string sTotalAmount;
         private string sUnit;
         private string sCoverage;
         internal string Coverage
         {
             get
             {
                 return (sCoverage);
             }
             set
             {
                 sCoverage = value;
             }
         }
         internal string Unit
         {
             get
             {
                 return (sUnit);
             }
             set
             {
                 sUnit = value;
             }
         }
         internal string PolicyNumber
         {
             get
             {
                 return (sPolicyNumber);
             }
             set
             {
                 sPolicyNumber = value;
             }
         }
         internal string PolicyName
         {
             get
             {
                 return (sPolicyName);
             }
             set
             {
                 sPolicyName = value;
             }
         }
         internal string PolicyType
         {
             get
             {
                 return (sPolicyType);
             }
             set
             {
                 sPolicyType = value;
             }
         }
         internal string VoidDate
         {
             get
             {
                 return (sVoidDate);
             }
             set
             {
                 sVoidDate = value;
             }
         }
         internal string ClaimNumber
         {
             get
             {
                 return (sClaimNumber);
             }
             set
             {
                 sClaimNumber = value;
             }
         }
         internal string ControlNumber
         {
             get
             {
                 return (sControlNumber);
             }
             set
             {
                 sControlNumber = value;
             }
         }
         internal string VoidFlag
         {
             get
             {
                 return (sVoidFlag);
             }
             set
             {
                 sVoidFlag = value;
             }
         }
         internal string DateOfCheck
         {
             get
             {
                 return (sDateOfCheck);
             }
             set
             {
                 sDateOfCheck = value;
             }
         }
         internal string TransactionNumber
         {
             get
             {
                 return (sTransactionNumber);
             }
             set
             {
                 sTransactionNumber = value;
             }
         }
         internal string TransactionDate
         {
             get
             {
                 return (sTransactionDate);
             }
             set
             {
                 sTransactionDate = value;
             }
         }
         internal string ClearedFlag
         {
             get
             {
                 return (sClearedFlag);
             }
             set
             {
                 sClearedFlag = value;
             }
         }
         internal string PaymentFlag
         {
             get
             {
                 return (sPaymentFlag);
             }
             set
             {
                 sPaymentFlag = value;
             }
         }
         internal string CollectionFlag
         {
             get
             {
                 return (sCollectionFlag);
             }
             set
             {
                 sCollectionFlag = value;
             }
         }
         internal string Status
         {
             get
             {
                 return (sStatus);
             }
             set
             {
                 sStatus = value;
             }
         }
         internal string ReserveType
         {
             get
             {
                 return (sReserveType);
             }
             set
             {
                 sReserveType = value;
             }
         }
         internal string TransactionType
         {
             get
             {
                 return (sTransactionType);
             }
             set
             {
                 sTransactionType = value;
             }
         }
         internal string SplitAmount
         {
             get
             {
                 return (sSplitAmount);
             }
             set
             {
                 sSplitAmount = value;
             }
         }
         internal string TotalAmount
         {
             get
             {
                 return (sTotalAmount);
             }
             set
             {
                 sTotalAmount = value;
             }
         }


     }
    
        public XmlDocument GetPolicySystems(XmlDocument p_objXmlDocument)
        {
            

            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sApplicableLevel = string.Empty;
            string sDiscountName = string.Empty;

            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            
            XmlElement objOptionXmlElement = null;
            XmlElement objNewPolicyListNode = null;
            XmlNode objOldPolicyListNode = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLChildElement = null;
            string sDefaultPolId = string.Empty;
            LocalCache objCache = null;
            int iCounter = 0;
            string sSplitIds = string.Empty;
            PolicySystemInterface.PolicySystemInterface objPolicySystem = null;
            int iPolicyId =0;
            bool bSuccess = false;
            try
            {
                objOldPolicyListNode = p_objXmlDocument.SelectSingleNode("//PolicyList");
                objNewPolicyListNode = p_objXmlDocument.CreateElement("PolicyList");
                objPolicySystem = new PolicySystemInterface.PolicySystemInterface(m_sConnectString, m_iClientId);
                XmlDocument objdoc = objPolicySystem.GetPolicySystemList(ref iPolicyId);
                objNewPolicyListNode.AppendChild(p_objXmlDocument.ImportNode(objdoc.SelectSingleNode("//PolicySystemList"),true));
                if (objOldPolicyListNode != null)
                    p_objXmlDocument.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                else
                    p_objXmlDocument.DocumentElement.AppendChild(objNewPolicyListNode);

                if (iPolicyId == 0)
                    iPolicyId =Conversion.CastToType<int>(objdoc.SelectSingleNode("//PolicySystemList/option").Attributes["value"].Value,out bSuccess);

                p_objXmlDocument = GetTransactionXML(iPolicyId, p_objXmlDocument);

                return p_objXmlDocument;
            }
                  catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicyBatchFile.LoadTransaction.Err",m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();


            }

            
        }

        private XmlDocument GetTransactionXML(int p_iPolicyId, XmlDocument p_objXmlDocument)
    {
        string sSQL = string.Empty;
        LocalCache objCache = null;
        XmlNode objOldPolicyListNode = null;
        XmlElement objXMLElement = null;
        XmlElement objXMLChildElement = null;
        DbReader objDbReader = null;
        int iCounter = 0;
        string sSplitIds = string.Empty;
        try
        {
                sSQL = "SELECT FIRST_NAME,LAST_NAME,CTL_NUMBER,FUNDS_TRANS_SPLIT.AMOUNT,TRANS_TYPE_CODE,RESERVE_TYPE_CODE,SPLIT_ROW_ID ";
                //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID :Start
                //sSQL = sSQL + " FROM FUNDS,FUNDS_TRANS_SPLIT,POLICY_X_UNIT,POLICY_X_CVG_TYPE,POLICY";
                sSQL = sSQL + " FROM FUNDS,FUNDS_TRANS_SPLIT,POLICY_X_UNIT,POLICY_X_CVG_TYPE,POLICY,COVERAGE_X_LOSS,RESERVE_CURRENT";
                //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID :End
                sSQL = sSQL + " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID";
                //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID :Start
                //sSQL = sSQL + " AND FUNDS_TRANS_SPLIT.UPDATE_POL_SYS = -1 AND FUNDS_TRANS_SPLIT.POLCVG_ROW_ID= POLICY_X_CVG_TYPE.POLCVG_ROW_ID";
                sSQL = sSQL + " AND FUNDS_TRANS_SPLIT.UPDATE_POL_SYS = -1 ";
                sSQL = sSQL + " AND FUNDS_TRANS_SPLIT.RC_ROW_ID=RESERVE_CURRENT.RC_ROW_ID AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND COVERAGE_X_LOSS.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID ";
                //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID :End
                sSQL = sSQL + " AND  POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID";
                sSQL = sSQL + " AND POLICY_X_UNIT.POLICY_ID = POLICY.POLICY_ID  ";
                sSQL = sSQL + " AND FUNDS_TRANS_SPLIT.POLICY_SYSTEM_ID =" + p_iPolicyId;
       
                objXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("PolicySystems");

                objXMLChildElement = (XmlElement)p_objXmlDocument.ImportNode(UTILITY.GetNewElement("TransactionList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("TransactionList");

                objXMLChildElement = (XmlElement)p_objXmlDocument.ImportNode(UTILITY.GetNewElement("listhead"), false);

                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("controlnumber", "Control Number"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("lastname", "Last Name"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("firstname", "First Name"), true));

                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("transtype", "Transaction Type"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("reservetype", "Reserve Type"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", "Amount"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("transid", "Transaction Id"), true));
                objXMLElement.AppendChild(objXMLChildElement);

                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);

                objCache = new LocalCache(m_sConnectString, m_iClientId);
                while (objDbReader.Read())
                {

                    objXMLChildElement = (XmlElement)p_objXmlDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/PolicySystemList/option[" + (++iCounter).ToString() + "]");

                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("controlnumber", Conversion.ConvertObjToStr(objDbReader.GetValue("CTL_NUMBER"))), true));
                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("lastname", Conversion.ConvertObjToStr(objDbReader.GetValue("LAST_NAME"))), true));
                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("firstname", Conversion.ConvertObjToStr(objDbReader.GetValue("FIRST_NAME"))), true));

                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("transtype", objCache.GetCodeDesc(Conversion.ConvertObjToInt(objDbReader.GetValue("TRANS_TYPE_CODE"), m_iClientId))), true));
                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("reservetype", objCache.GetCodeDesc(Conversion.ConvertObjToInt(objDbReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId))), true));
                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", Conversion.ConvertObjToStr(objDbReader.GetValue("AMOUNT"))), true));

                    string sTag = Conversion.ConvertObjToStr(objDbReader.GetValue("CTL_NUMBER")) + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("LAST_NAME")) + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("FIRST_NAME")) +
                         "," + objCache.GetCodeDesc(Conversion.ConvertObjToInt(objDbReader.GetValue("TRANS_TYPE_CODE"), m_iClientId)) + "," + objCache.GetCodeDesc(Conversion.ConvertObjToInt(objDbReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId)) + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("AMOUNT")) +
                         "," + Conversion.ConvertObjToStr(objDbReader.GetValue("SPLIT_ROW_ID"));

                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag), true));
                    objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("transid", Conversion.ConvertObjToStr(objDbReader.GetValue("SPLIT_ROW_ID"))), true));
                    objXMLElement.AppendChild(objXMLChildElement);
                    if (string.IsNullOrEmpty(sSplitIds))
                        sSplitIds = Conversion.ConvertObjToStr(objDbReader.GetValue("SPLIT_ROW_ID"));
                    else
                        sSplitIds = sSplitIds + "," + Conversion.ConvertObjToStr(objDbReader.GetValue("SPLIT_ROW_ID"));
                }

                objXMLChildElement = (XmlElement)p_objXmlDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/PolicySystems/TransactionList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("controlnumber"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("lastname"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("firstname"), false));

                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("transtype"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("reservetype"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("transid"), false));
                objXMLElement.AppendChild(objXMLChildElement);

                objXMLElement = null;
                objXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//TransSplitIds");
                objXMLElement.InnerText = sSplitIds;
                return p_objXmlDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicyBatchFile.LoadData.Err",m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
               
              
            }
        }


        public XmlDocument GetTransactions(XmlDocument p_objXmlDocument)
        {

            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sApplicableLevel = string.Empty;
            string sDiscountName = string.Empty;

            string sShortCode = string.Empty;
            string sDesc = string.Empty;

            XmlElement objOptionXmlElement = null;
            XmlElement objNewPolicyListNode = null;
            XmlNode objPolicySystemList = null;
            XmlNode objOldPolicyListNode = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLChildElement = null;
            string sPolId = string.Empty;
            XmlDocument objdoc = null;
            string sSplitIds = string.Empty;
            int iPolicyId = 0;
            PolicySystemInterface.PolicySystemInterface objPolicySystem = null;
            bool bSuccess = false;
            try
            {
                sPolId = p_objXmlDocument.SelectSingleNode("//PolicySystems/PolicyList").InnerText;
                objOldPolicyListNode = p_objXmlDocument.SelectSingleNode("//PolicyList");
            
         objNewPolicyListNode = p_objXmlDocument.CreateElement("PolicyList");

                objOldPolicyListNode = p_objXmlDocument.SelectSingleNode("//PolicyList");
                objNewPolicyListNode = p_objXmlDocument.CreateElement("PolicyList");
                objPolicySystem = new PolicySystemInterface.PolicySystemInterface(m_sConnectString, m_iClientId);
                 objdoc = objPolicySystem.GetPolicySystemList(ref iPolicyId);
                 objNewPolicyListNode.InnerXml= objdoc.SelectSingleNode("//PolicySystemList").InnerXml;
                // objNewPolicyListNode.AppendChild(objPolicySystemList);
                if (objOldPolicyListNode != null)
                    p_objXmlDocument.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                else
                    p_objXmlDocument.DocumentElement.AppendChild(objNewPolicyListNode);

                p_objXmlDocument = GetTransactionXML(Conversion.CastToType<int>(sPolId,out bSuccess), p_objXmlDocument);

                return p_objXmlDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicyBatchFile.LoadTransaction.Err",m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();


            }
        }


        public XmlDocument CreateFile(XmlDocument p_objXmlDocument)
        {

            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sApplicableLevel = string.Empty;
            string sDiscountName = string.Empty;

            string sShortCode = string.Empty;
            string sDesc = string.Empty;

            XmlElement objOptionXmlElement = null;
            XmlElement objNewPolicyListNode = null;
            XmlNode objOldPolicyListNode = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLChildElement = null;
            string sSelectedSplitIds = string.Empty;
            DbConnection objCon = null;
            string sFilePath = string.Empty;
            string sPolId = string.Empty;
            string[] arrSelectedIds;
            
            string sTransId = string.Empty;
           StringBuilder sbSQL = null;
            string sControlNumber = string.Empty;
            string sFileName = string.Empty;
            string sAmount = string.Empty;
            string sPolicySystemName = string.Empty;
            string sPolicySystemId = string.Empty;
            bool bUpdateFinancial = false;
            string sTransType = string.Empty;
            string sReserveType = string.Empty;
            try
            {
                sPolId = p_objXmlDocument.SelectSingleNode("//PolicySystems/PolicyList").InnerText;
          sSelectedSplitIds = p_objXmlDocument.SelectSingleNode("//PolicySystems/TransSplitIds").InnerText;

              //  sFilePath = RMConfigurator.BasePath + "/userdata/PolicyBatchFile/";

                arrSelectedIds = sSelectedSplitIds.Split(',');

                foreach (string sSplitId in arrSelectedIds)
                {
                    //sSQL = "SELECT FUNDS_TRANS_SPLIT.SPLIT_ROW_ID,FUNDS_TRANS_SPLIT.POLICY_SYSTEM_ID,CTL_NUMBER,FINANCIAL_UPD_FLAG,POLICY_SYSTEM_NAME,FUNDS_TRANS_SPLIT.AMOUNT SPLIT_AMOUNT,RESERVE_TYPE_CODE,TRANS_TYPE_CODE FROM FUNDS,FUNDS_TRANS_SPLIT,POLICY_X_WEB ";
                    //sSQL = sSQL + "WHERE  FUNDS_TRANS_SPLIT.TRANS_ID = FUNDS.TRANS_ID AND FUNDS_TRANS_SPLIT.POLICY_SYSTEM_ID=POLICY_X_WEB.POLICY_SYSTEM_ID";
                    //sSQL = sSQL + " AND FUNDS_TRANS_SPLIT.SPLIT_ROW_ID = " + sSplitId;
                    sbSQL = new StringBuilder();
                    sbSQL.Append(" SELECT FUNDS.CTL_NUMBER,FUNDS.PAYMENT_FLAG,FUNDS.COLLECTION_FLAG,  FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.DATE_OF_CHECK,");
                    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT  FAMT,CLAIM_NUMBER,  ");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.AMOUNT  FTSAMT,");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC,POLICY_X_WEB.FINANCIAL_UPD_FLAG");


                    sbSQL.Append(" ,POL.CODE_DESC COVERAGETYPE,POLICY.POLICY_NAME,POLICY.POLICY_NUMBER,TRANS_TYPE_CODE,RESERVE_TYPE_CODE ");
                    sbSQL.Append(" , CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT VIN FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID) END UNIT_TYPE");
                    sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT,CODES_TEXT,FUNDS,POLICY_X_WEB");
                    sbSQL.Append(" ,POLICY_X_CVG_TYPE,CODES_TEXT POL,POLICY, POLICY_X_UNIT");

                    //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                    sbSQL.Append(" ,COVERAGE_X_LOSS,RESERVE_CURRENT");

                    sbSQL.Append(" WHERE  FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");

                    //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                   // sbSQL.Append(" AND  FUNDS_TRANS_SPLIT.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                    sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RC_ROW_ID=RESERVE_CURRENT.RC_ROW_ID AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND COVERAGE_X_LOSS.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");

                    sbSQL.Append("   AND POL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE AND POLICY.POLICY_ID =POLICY_X_CVG_TYPE.POLICY_ID ");
                    sbSQL.Append("  AND   POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                    sbSQL.Append(" AND CODES_TEXT.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE ");
                    sbSQL.Append("AND FUNDS_TRANS_SPLIT.SPLIT_ROW_ID = " + sSplitId);
                    sbSQL.Append(" AND POLICY_X_WEB.POLICY_SYSTEM_ID =FUNDS_TRANS_SPLIT.POLICY_SYSTEM_ID");
                    sbSQL.Append(" ORDER BY FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID ");

                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString()))
                    {
                        while (objReader.Read())
                        {
                            RealTimeUpdate objRealTimeUpdate = new RealTimeUpdate();

                            //sPolicySystemId =Conversion.ConvertObjToStr(objReader.GetValue("POLICY_SYSTEM_ID"));
                            //sControlNumber = Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER"));
                            //sAmount = Conversion.ConvertObjToStr(objReader.GetValue("SPLIT_AMOUNT"));
                            //sPolicySystemName = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_SYSTEM_NAME"));
                            //bUpdateFinancial = Conversion.ConvertObjToBool(objReader.GetValue("FINANCIAL_UPD_FLAG"));
                            //sReserveType = Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE"));
                            //sTransType = Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE"));
                            //sSplitId = Conversion.ConvertObjToStr(objReader.GetValue("SPLIT_ROW_ID"));
                            objRealTimeUpdate.ControlNumber = Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER"));
                            objRealTimeUpdate.TransactionDate = Conversion.ConvertObjToStr(objReader.GetValue("TRANS_DATE"));
                            objRealTimeUpdate.VoidFlag = Conversion.ConvertObjToStr(objReader.GetValue("VOID_FLAG"));
                            objRealTimeUpdate.DateOfCheck = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CHECK"));
                            objRealTimeUpdate.TransactionType = Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE"));
                            objRealTimeUpdate.TransactionNumber = Conversion.ConvertObjToStr(objReader.GetValue("TRANS_NUMBER"));
                            objRealTimeUpdate.ReserveType = Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE"));
                            objRealTimeUpdate.ClearedFlag = Conversion.ConvertObjToStr(objReader.GetValue("CLEARED_FLAG"));
                            objRealTimeUpdate.TotalAmount = Conversion.ConvertObjToStr(objReader.GetValue("FAMT"));
                            objRealTimeUpdate.SplitAmount = Conversion.ConvertObjToStr(objReader.GetValue("FTSAMT"));
                            objRealTimeUpdate.Coverage = Conversion.ConvertObjToStr(objReader.GetValue("COVERAGETYPE"));
                            objRealTimeUpdate.PaymentFlag = Conversion.ConvertObjToStr(objReader.GetValue("PAYMENT_FLAG"));
                            objRealTimeUpdate.CollectionFlag = Conversion.ConvertObjToStr(objReader.GetValue("COLLECTION_FLAG"));
                            objRealTimeUpdate.PolicyName = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_NAME"));
                            objRealTimeUpdate.Unit = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_TYPE"));
                            objRealTimeUpdate.TotalAmount = Conversion.ConvertObjToStr(objReader.GetValue("FAMT"));
                            objRealTimeUpdate.SplitAmount = Conversion.ConvertObjToStr(objReader.GetValue("FTSAMT"));
                            objRealTimeUpdate.Coverage = Conversion.ConvertObjToStr(objReader.GetValue("COVERAGETYPE"));
                            objRealTimeUpdate.ClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));
                            objRealTimeUpdate.PolicyNumber = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_NUMBER"));
                            bUpdateFinancial = Conversion.ConvertObjToBool(objReader.GetValue("FINANCIAL_UPD_FLAG"), 0);



                            if (bUpdateFinancial)
                            {
                                sFileName = GetFileName(objRealTimeUpdate.ControlNumber) + ".xml";
                                WriteFile(sFileName, GetXML(objRealTimeUpdate));
                            }
                        }
                    }

                }
                sSQL = "UPDATE FUNDS_TRANS_SPLIT  SET UPDATE_POL_SYS = 0 WHERE SPLIT_ROW_ID IN (" + sSelectedSplitIds + ")";

                objCon = DbFactory.GetDbConnection(m_sConnectString);
                objCon.Open();
                objCon.ExecuteNonQuery(sSQL);
                objCon.Close();


               // sPolId = p_objXmlDocument.SelectSingleNode("//PolicySystems/PolicyList").InnerText;
                objOldPolicyListNode = p_objXmlDocument.SelectSingleNode("//PolicyList");
                //p_objXmlDocument.RemoveChild("//PolicyList");
                objNewPolicyListNode = p_objXmlDocument.CreateElement("PolicyList");
                //  objNewPolicyListNode.InnerText = p_objXmlDocument.SelectSingleNode("//PolicySystems/PolicyList").InnerText;
                sSQL = "SELECT POLICY_SYSTEM_ID,POLICY_SYSTEM_NAME,DEFAULT_POL_FLAG FROM POLICY_X_WEB ORDER BY  DEFAULT_POL_FLAG";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                while (objDbReader.Read())
                {
                    objOptionXmlElement = p_objXmlDocument.CreateElement("option");
                    objOptionXmlElement.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue(0)));
                    objOptionXmlElement.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue(1));
                    objNewPolicyListNode.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;
                    //if (Conversion.ConvertObjToBool(objDbReader.GetValue(2)))
                    //{
                    //    sDefaultPolId = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                    //}

                }



                if (objOldPolicyListNode != null)
                    p_objXmlDocument.DocumentElement.ReplaceChild(objNewPolicyListNode, objOldPolicyListNode);
                else
                    p_objXmlDocument.DocumentElement.AppendChild(objNewPolicyListNode);


                objXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("PolicySystems");

                objXMLChildElement = (XmlElement)p_objXmlDocument.ImportNode(UTILITY.GetNewElement("TransactionList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("TransactionList");

                objXMLChildElement = (XmlElement)p_objXmlDocument.ImportNode(UTILITY.GetNewElement("listhead"), false);

                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("controlnumber", "Control Number"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("lastname", "Last Name"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("firstname", "First Name"), true));
                
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("transtype", "Transaction Type"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("reservetype", "Reserve Type"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", "Amount"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewEleWithValue("transid", "Transaction Id"), true));
                objXMLElement.AppendChild(objXMLChildElement);


                //    sSQL = "SELECT FIRST_NAME,LAST_NAME,CTL_NUMBER,AMOUNT, FROM FUNDS WHERE POLICY_SYSTEM_ID =" + sDefaultPolId;


                objXMLChildElement = (XmlElement)p_objXmlDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/PolicySystems/TransactionList/option[1]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("controlnumber"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("lastname"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("firstname"), false));
                
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("transtype"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("reservetype"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(p_objXmlDocument.ImportNode(UTILITY.GetNewElement("transid"), false));
                objXMLElement.AppendChild(objXMLChildElement);

                return p_objXmlDocument;
            }
            
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicyBatchFile.FileWrite.Err",m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();


            }
        }

       
        private  void WriteFile(string p_sFileName, string sData)
        {

            string sSQL = string.Empty;
     
            string sApplicableLevel = string.Empty;
            string sDiscountName = string.Empty;

            string sShortCode = string.Empty;
            string sDesc = string.Empty;

       
            string sSelectedSplitIds = string.Empty;
  

            string sPolId = string.Empty;

            StreamWriter objWriter = null;
            string sFilePath = string.Empty;
            try
            {
                sFilePath = RMConfigurator.BasePath + "/userdata/PolicyBatchFile/" + p_sFileName;
                objWriter = new StreamWriter(sFilePath);
                objWriter.WriteLine(sData);
            }

           
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objWriter != null)
                {
                    objWriter.Close();
                }


            }
        }
        private  string GetFileName(string sControlNumber)
        {
            string sFileName = string.Empty;
            string sSQL = string.Empty;
            try
            {
                sFileName = sControlNumber + " " + System.DateTime.Now.ToString("yyyyMMddHHmmss");


            }
            catch (Exception e)
            {
                throw e;

            }
            return sFileName;
        }
        private string GetXML(RealTimeUpdate objRealTime)
        {

            StringBuilder sXml = new StringBuilder("<Message>");
            sXml = sXml.Append("<Data><Document>");
            sXml.Append("<PolicyNumber>" + objRealTime.PolicyNumber + "<PolicyNumber>");
            sXml.Append("<PolicyName>" + objRealTime.PolicyName + "</PolicyName>");
            sXml.Append("<ControlNumber>" + objRealTime.ControlNumber + "</ControlNumber>");
            sXml.Append("<VoidFlag>" + objRealTime.VoidFlag + "</VoidFlag>");
            sXml.Append("<DateOfCheck>" + objRealTime.DateOfCheck + "</DateOfCheck>");
            sXml.Append("<TransactionNumber>" + objRealTime.TransactionNumber + "</TransactionNumber>");
            sXml.Append("<TransactionDate>" + objRealTime.TransactionDate + "</TransactionDate>");
            sXml.Append("<ClearedFlag>" + objRealTime.ClearedFlag + "</ClearedFlag>");
            sXml.Append("<PaymentFlag>" + objRealTime.PaymentFlag + "</PaymentFlag>");
            sXml.Append("<CollectionFlag>" + objRealTime.CollectionFlag + "</CollectionFlag>");
            sXml.Append("<ReserveType>" + objRealTime.ReserveType + "</ReserveType>");
            sXml.Append("<TransactionType>" + objRealTime.TransactionType + "</TransactionType>");
            sXml.Append("<SplitAmount>" + objRealTime.SplitAmount + "</SplitAmount>");
            sXml.Append("<TotalAmount>" + objRealTime.TotalAmount + "</TotalAmount>");
            sXml.Append("<Coverage>" + objRealTime.Coverage + "</Coverage>");
            sXml.Append("<Unit>" + objRealTime.Unit + "</Unit>");
            sXml.Append("<ClaimNumber>" + objRealTime.ClaimNumber + "</ClaimNumber>");



            return sXml.ToString();


        }

          
    }
}
