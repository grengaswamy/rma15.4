using System;
using System.Xml; 
using Riskmaster.Db; 
using System.Data;
using Riskmaster.Common;
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Nikhil Garg	
	///Dated   :   22 Jun 2005
	///Purpose :   Base Class for List Type Forms
	/// </summary>
	public class UtilitiesUIListBase
	{
		/// <summary>
		/// Key Field
		/// </summary>
		private string m_sKeyField="";

		/// <summary>
		/// Key Field Value
		/// </summary>
		private string m_sKeyFieldValue="";

		/// <summary>
		/// Table Name
		/// </summary>
		private string m_sTableName="";

		/// <summary>
		/// Radio Name
		/// </summary>
		private string m_sRadioName="";

		/// <summary>
		/// Where Clause
		/// </summary>
		private string m_sWhereClause="";

		/// <summary>
		/// Order By Clause
		/// </summary>
		private string m_sOrderByClause="";

		/// <summary>
		/// Xml Document
		/// </summary>
		private XmlDocument m_objXmlDoc=null;

		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnString="";
        
        /// <summary>
        /// Client ID
        /// </summary>
        private int m_iClientId = 0;

        /// <summary>
        /// userLogin
        /// </summary>
        UserLogin m_oUserLogin = null;//vkumar258 ML Changes

		/// <summary>
		/// Internal Datafield List variable
		/// </summary>
		internal DataFieldList m_FieldList=new DataFieldList();

		/// <summary>
		/// Child array variable
		/// </summary>
		protected string[,] m_arrChild =  new string[10,5];
        ///
        /// <summary>
        /// Parameter listing
        /// </summary>
        //Added by bsharma33 for MITS 26942
        protected System.Collections.Generic.Dictionary<string, string> dictParams = new System.Collections.Generic.Dictionary<string, string>();
        //End additons by bsharma33 for MITS 26942
		#region "Properties"

		/// <summary>
		/// TableName
		/// </summary>
		protected string TableName
		{
			get{return m_sTableName;}
			set{m_sTableName = value;}
		}

		/// <summary>
		/// KeyField
		/// </summary>
		protected string KeyField
		{
			get{return m_sKeyField;}
			set{m_sKeyField = value;}
		}

		/// <summary>
		/// KeyFieldValue
		/// </summary>
		protected string KeyFieldValue
		{
			get{return m_sKeyFieldValue;}
			set{m_sKeyFieldValue = value;}
		}

		/// <summary>
		/// RadioName
		/// </summary>
		protected string RadioName
		{
			get{return m_sRadioName;}
			set{m_sRadioName = value;}
		}

		/// <summary>
		/// WhereClause
		/// </summary>
		protected string WhereClause
		{
			get{return m_sWhereClause;}
			set{m_sWhereClause = value;}
		}

		/// <summary>
		/// OrderByClause
		/// </summary>
		protected string OrderByClause
		{
			get{return m_sOrderByClause;}
			set{m_sOrderByClause = value;}
		}

		/// <summary>
		/// XMLDoc
		/// </summary>
		protected XmlDocument XMLDoc
		{
			get{return m_objXmlDoc;}
			set{m_objXmlDoc = value;}
		}

		/// <summary>
		/// ConnectString
		/// </summary>
		public string ConnectString
        //internal string ConnectString
		{
			get{return m_sConnString;}
			set{m_sConnString = value;}
		}

		/// <summary>
		/// Child Array
		/// </summary>
		protected string[,] ChildArray
		{
			set{m_arrChild = value;}
		}

		#endregion

		/// Name		: UtilitiesUIListBase
		/// Author		: Nikhil Garg
		/// Date Created: 22-Jun-2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
		internal UtilitiesUIListBase(string p_sConnString, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			Initialize();
		}

		/// Name		: Initialize
		/// Author		: Nikhil Garg
		/// Date Created: 22-Jun-2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize
		/// </summary>
		protected void Initialize(){}

        /// <summary>
        /// //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | Start
        /// </summary>
        public UtilitiesUIListBase(int p_iClientId)
        {
            m_iClientId = p_iClientId;
            Initialize();
        }

        public UtilitiesUIListBase(UserLogin p_oUserLogin, int p_iClientId)//vkumar258 ML Changes
        {
            m_iClientId = p_iClientId;
            m_oUserLogin = p_oUserLogin;
            Initialize();
        }
        //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | End

		/// Name		: InitFields
		/// Author		: Nikhil Garg
		/// Date Created: 22-Jun-2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Add all obect Fields into the Field Collection from our Field List.
		/// </summary>
		/// <param name="p_sFields">Fields Collection</param>
		protected virtual void InitFields(string[,] p_sFields)
		{ 
			for(int i =0; i< p_sFields.GetLength(0);i++)
			{
				this.m_FieldList[p_sFields[i,0]] = p_sFields[i,1];
			}
		}

		/// Name		: FieldsToSelect
		/// Author		: Nikhil Garg
		/// Date Created: 22-Jun-2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// FieldsToSelect
		/// </summary>
		/// <returns>string list</returns>
		private string FieldsToSelect()
		{
			string sFieldsToSelect="";

			foreach(string key in m_FieldList.Keys)
			{
				if (m_FieldList[key].ToString() != m_sKeyField)   // don't include key field again since it is automatically included. Oracle doesn't like it appearing twice.
				{
					if( sFieldsToSelect=="")
						sFieldsToSelect = m_FieldList[key].ToString() ;
					else
						sFieldsToSelect += "," + m_FieldList[key].ToString() ;
				}
			}
			return sFieldsToSelect;
		}

		/// Name		: Get
		/// Author		: Nikhil Garg
		/// Date Created: 22-Jun-2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Base Get Implementation
		/// </summary>
		protected virtual void Get()
		{
			DataSet objDs = null;
			XmlElement objLstRow = null;
			XmlElement objRowTxt = null;
			XmlNode objNode = null;
			XmlNodeList objNodLst = null;
			XmlElement objElm =null;
			string sColumnName="";
			string sSQL = "";

			LocalCache objCache = null;
			string sShortCode="";
			string sShortDesc="";
            //Added by bsharma33 for MITS 26942
              System.Text.StringBuilder sbSQL = null;
           
            //bsharma33
			
			try
			{
                objCache = new LocalCache(this.ConnectString, m_iClientId);
                //Changed by bsharma33 Regions PanTesting MITS 26942
                sbSQL = new System.Text.StringBuilder();
				//sSQL = String.Format("SELECT {0},{1} FROM {2}",m_sKeyField, FieldsToSelect() , m_sTableName);
                sbSQL.Append(String.Format("SELECT {0},{1} FROM {2}", m_sKeyField, FieldsToSelect(), m_sTableName));
                if (m_sWhereClause != null && m_sWhereClause != "")
                { //sSQL += " WHERE " + m_sWhereClause; 
                    sbSQL.Append(string.Format(" WHERE {0}", m_sWhereClause));
                }
                //MITS 33793 asingh263 Starts
                if (string.IsNullOrEmpty(m_sOrderByClause) && m_sTableName == "WPA_AUTO_SET")
                {
                    m_sOrderByClause = "AUTO_NAME";
                }
                //MITS 33793 asingh263 Ends
                if (m_sOrderByClause != null && m_sOrderByClause != "")
                { //sSQL += " ORDER BY " + m_sOrderByClause; 
                    sbSQL.Append(string.Format(" ORDER BY {0}", m_sOrderByClause));
                }

                objDs = DbFactory.ExecuteDataSet(m_sConnString, sbSQL.ToString(), dictParams,m_iClientId);//clear the keys in dictParams, in the functions where keys are added,i.e., in the calling function.
               // objDs = DbFactory.GetDataSet(m_sConnString, sSQL);
                //End changes by bsharma33 Regions Pan Testing MITS 26942
				objNode = m_objXmlDoc.SelectSingleNode("//*[name()='" + m_sRadioName +  "']");  
				objNodLst  = objNode.ChildNodes.Item(0).ChildNodes;
			
				foreach(DataRow objRow in objDs.Tables[0].Rows )
				{
					objLstRow = m_objXmlDoc.CreateElement("listrow");
					
					for(int i = 0; i< objNodLst.Count-1;i++)  
					{
						objElm = (XmlElement)objNodLst[i]; 
						objRowTxt = m_objXmlDoc.CreateElement(objNodLst[i].Name);
						if(m_FieldList.ContainsField(objElm.Name))//Fill data only when its in the column list
						{
							if(objNodLst[i].Attributes["type"] != null)
							{
								if (objNodLst[i].Attributes["type"].Value.ToLower()=="date")
								{
									objRowTxt.InnerText=objRow[m_FieldList[objElm.Name].ToString()].ToString();
									objRowTxt.InnerText=UTILITY.FormatDate(objRowTxt.InnerText,false);
								}
								else if (objNodLst[i].Attributes["type"].Value.ToLower()=="code")
								{
									objCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRow[m_FieldList[objElm.Name].ToString()].ToString()),ref sShortCode,ref sShortDesc);
									objRowTxt.InnerText= sShortCode + " " + sShortDesc;
                                    objRowTxt.SetAttribute("codeid", objRow[m_FieldList[objElm.Name].ToString()].ToString());//rsushilaggar : Set the codeid attribute in the xml tag
								}
								else if (objNodLst[i].Attributes["type"].Value.ToLower()=="bool")
								{
									objRowTxt.InnerText=objRow[m_FieldList[objElm.Name].ToString()].ToString();
									if(objRowTxt.InnerText=="-1")
										objRowTxt.InnerText ="Yes";
									else
										objRowTxt.InnerText ="No";
								}
                                else
                                    objRowTxt.InnerText = objRow[m_FieldList[objElm.Name].ToString()].ToString();
							}
                            else
                                objRowTxt.InnerText = objRow[m_FieldList[objElm.Name].ToString()].ToString();
							
						}
						objLstRow.AppendChild(objRowTxt);
					}

                    if (m_sKeyField.IndexOf('.') > 0)
                        sColumnName = (m_sKeyField.ToString().Split('.'))[1].ToString();
                    else
                        sColumnName = m_sKeyField.ToString();

                    //objLstRow.SetAttribute("rowid",objRow[sColumnName].ToString()); 
                    objRowTxt = m_objXmlDoc.CreateElement("RowId");
                    objRowTxt.InnerText = objRow[sColumnName].ToString();
                    objLstRow.AppendChild(objRowTxt);

					objNode.AppendChild((XmlNode)objLstRow);
				}
				objDs.Dispose();
			}
			finally
			{
				if (objDs != null)
				{
					objDs.Dispose();
					objDs=null;
				}
				if(objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				objLstRow = null;
				objRowTxt = null;
				objNode = null;
				objNodLst = null;
				objElm =null;
			}
		}

		/// Name		: Delete
		/// Author		: Nikhil Garg
		/// Date Created: 22-Jun-2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete method
		/// </summary>
		/// <returns>boolean success</returns>
		protected virtual bool Delete()
		{
			DbConnection objConn = null;
			string sSQL = "";
			try
			{
				sSQL = String.Format("DELETE FROM {0} WHERE {1} = {2}",m_sTableName,m_sKeyField,m_sKeyFieldValue);
				if (m_sWhereClause!="")
					sSQL += " AND " + m_sWhereClause;
				objConn = DbFactory.GetDbConnection(m_sConnString);
				objConn.Open(); 
				objConn.ExecuteNonQuery(sSQL); 
				objConn.Dispose();
				return true;
			}
			finally
			{
				if(objConn != null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}
	}	
}