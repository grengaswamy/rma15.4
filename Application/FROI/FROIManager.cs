﻿
using System;
using System.IO ;
using System.Xml ;
using System.Text ;
using System.Drawing ;
using System.Collections ;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Application.FileStorage ;
using Riskmaster.Application.ExtenderLib ;
using Riskmaster.Application.FormProcessor ;
using Riskmaster.Application.DocumentManagement ;
using C1.C1PrintDocument ;


namespace Riskmaster.Application.FROI
{
	/**************************************************************
	 * $File		: FROIManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 02/02/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: Class exposes various methods for Printing FROI( First Report Of Injury ). 
	 * $Source		:  	
	**************************************************************/
	/*
	*	The Client-Custom  overlay logic...
	* 
	*		PDS File Path For FROI :  
	*									..\\ 
	*									..\\Client-Custom\Froi\ 					( For Client-Custom )	
	*		
	*		PDS File Path For WC : 		
	*									..\\State\
	*				 					..\\Client-Custom\Workcomp\State\			( For Client-Custom )
	*
	*		PDS File Path For Claim :
	*									..\\ClaimForms\State\
	*									..\\Client-Custom\Claim-Forms\State\		( For Client-Custom )
	*					
	*		Base folder path( ..\\ ) will be read from configuration file( Ex : "C:\Wizard\pdf-forms" ).
	*		
	*
	*/	
	public class FROIManager
	{
		#region Constants
		/// <summary>
		/// Constant string contains a dot.
		/// </summary>
		private const string DOTT_STRING = "." ;	
		/// <summary>
		/// Constant for PdfTemplateFormsPath node in configuration file.
		/// </summary>
		internal const string PDF_TEMPLATE_FORMS_PATH = "FROI_PdfTemplateFormsPath" ;
		/// <summary>
		/// Constant for PdfUrlPath node in configuration file.
		/// </summary>
		internal const string PDF_URL_PATH = "FROI_PdfUrlPath" ;
		/// <summary>
		/// Constant for PdfOutPath node in configuration file.
		/// </summary>
		internal const string PDF_OUT_PATH = "FROI_PdfOutPath" ;
		/// <summary>
		/// Constant for FailedFroiPdfPath name in configuration file.
		/// </summary>
		internal const string FAILED_FROI_PDF_PATH = "FROI_FailedFroiPdfPath" ;
		#endregion 

		#region Variable Declaration 
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of data model factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;		
		/// <summary>
		/// Private variable to store Base path of the PDS and PDF file Path.
		/// </summary>
		private string m_sPdfPath = "" ;
		/// <summary>
		/// Private variable for Pdf Url Path.
		/// </summary>
		private string m_sPdfUrl = "" ;
		/// <summary>
		/// Private variable for Output FDF File Path
		/// </summary>
		private string m_sOutputPath = "" ;		
		/// <summary>
		/// Enumeration for FormType.
		/// </summary>
		private enum FormType : int
		{
			FROIForm = 0,
			WCClaimForm = 1,
			ClaimForm = 2
		}
		#endregion 

		#region Structure Declaration 
		/// Name		: InvokeFROIInfo
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// InvokeFROIInfo contains the basic information required to call InvokeFROI function. 
		/// </summary>
		private struct InvokeFROIInfo
		{
			/// <summary>
			/// Private variable for StateId.
			/// </summary>
			private string sStateId ;		
			/// <summary>
			/// Private variable for ClaimId.
			/// </summary>
			private int iClaimId ;	
			/// <summary>
			/// Read/Write property for StateId.
			/// </summary>
			internal string StateId 
			{
				get
				{
					return( sStateId ) ;
				}
				set
				{
					sStateId = value ;
				}
			}
			/// <summary>
			/// Read/Write property for ClaimId.
			/// </summary>
			internal int ClaimId 
			{
				get
				{
					return( iClaimId ) ;
				}
				set
				{
					iClaimId = value ;
				}
			}
		}

		/// Name		: ErrorInfo
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// ErrorInfo contains the basic information required to print failed FROI report. 
		/// </summary>		
		private struct ErrorInfo
		{
			/// <summary>
			/// Private variable for Error.
			/// </summary>
			private string sError ;		
			/// <summary>
			/// Private variable for ClaimNumber.
			/// </summary>
			private string sClaimNumber ;	
			/// <summary>
			/// Read/Write property for Error.
			/// </summary>
			internal string Error 
			{
				get
				{
					return( sError ) ;
				}
				set
				{
					sError = value ;
				}
			}
			/// <summary>
			/// Read/Write property for ClaimNumber.
			/// </summary>
			internal string ClaimNumber 
			{
				get
				{
					return( sClaimNumber ) ;
				}
				set
				{
					sClaimNumber = value ;
				}
			}
		}
		/// Name		: BatchFROIInfo
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// BatchFROIInfo contains the basic information of a FROI. 
		/// </summary>		
		private struct BatchFROIInfo
		{
			/// <summary>
			/// Private variable for Claim Number.
			/// </summary>
			private string sClaimNumber ;
			/// <summary>
			/// Private variable for Claim Date.
			/// </summary>
			private string sClaimDate ;
			/// <summary>
			/// Private variable for First Name.
			/// </summary>
			private string sFirstName ;
			/// <summary>
			/// Private variable for Last Name.
			/// </summary>
			private string sLastName ;
			/// <summary>
			/// Private variable for StateId.
			/// </summary>
			private string sStateId ;
			/// <summary>
			/// Private variable for State Name.
			/// </summary>
			private string sStateName ;
			/// <summary>
			/// Private variable for OrgA Last Name.
			/// </summary>
			private string sOrgALastName ;
			/// <summary>
			/// Private variable for OrgB Last Name.
			/// </summary>
			private string sOrgBLastName ;
			/// <summary>
			/// Private variable for ClaimId.
			/// </summary>
			private int iClaimId ;
			/// <summary>
			/// Read/Write property for ClaimNumber.
			/// </summary>
			internal string ClaimNumber 
			{
				get
				{
					return( sClaimNumber ) ;
				}
				set
				{
					sClaimNumber = value ;
				}
			}
			/// <summary>
			/// Read/Write property for Claim Date.
			/// </summary>
			internal string ClaimDate 
			{
				get
				{
					return( sClaimDate ) ;
				}
				set
				{
					sClaimDate = value ;
				}
			}
			/// <summary>
			/// Read/Write property for First Name.
			/// </summary>
			internal string FirstName 
			{
				get
				{
					return( sFirstName ) ;
				}
				set
				{
					sFirstName = value ;
				}
			}
			/// <summary>
			/// Read/Write property for Last Name.
			/// </summary>
			internal string LastName 
			{
				get
				{
					return( sLastName ) ;
				}
				set
				{
					sLastName = value ;
				}
			}
			/// <summary>
			/// Read/Write property for StateId.
			/// </summary>
			internal string StateId 
			{
				get
				{
					return( sStateId ) ;
				}
				set
				{
					sStateId = value ;
				}
			}
			/// <summary>
			/// Read/Write property for State Name.
			/// </summary>
			internal string StateName 
			{
				get
				{
					return( sStateName ) ;
				}
				set
				{
					sStateName = value ;
				}
			}
			/// <summary>
			/// Read/Write property for OrgA Last Name.
			/// </summary>
			internal string OrgALastName 
			{
				get
				{
					return( sOrgALastName ) ;
				}
				set
				{
					sOrgALastName = value ;
				}
			}
			/// <summary>
			/// Read/Write property for OrgB Last Name.
			/// </summary>
			internal string OrgBLastName 
			{
				get
				{
					return( sOrgBLastName ) ;
				}
				set
				{
					sOrgBLastName = value ;
				}
			}
			/// <summary>
			/// Read/Write property for ClaimId.
			/// </summary>
			internal int ClaimId 
			{
				get
				{
					return( iClaimId ) ;
				}
				set
				{
					iClaimId = value ;
				}
			}
		}


		#endregion 

		#region Constructor
		/// Name		: FROIManager
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>				
		public FROIManager( string p_sDsnName , string p_sUserName , string p_sPassword )
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword );	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
			Constants.g_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() ;
		}

		#endregion 

		#region Get Forms information for FROI , Claim Or WC in XML format
		/// Name		: GetFROIForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get FROI info XML for the given Claim Id.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <returns>XML String, contains the FROI Info.</returns>
		public string GetFROIForms( int p_iClaimId )
		{
			Claim objClaim = null ;
			DbReader objReader = null ;
			StringBuilder sbSQL = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			XmlElement objFroiFormNode = null ;

			string sFormName = "" ;
			int iFormId = 0 ;

			try
			{
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimId > 0 )
					objClaim.MoveTo ( p_iClaimId );
				else
				{
					objClaim = null;
					throw new RMAppException(Globalization.GetString("FROIManager.InvalidClaimId" ) );
				}			
			 
				sbSQL = new StringBuilder();
				sbSQL.Append( " SELECT * FROM JURIS_FORMS WHERE STATE_ROW_ID = " + objClaim.FilingStateId );
				sbSQL.Append( " ORDER BY FORM_ID " );

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
				objXmlDocument = new XmlDocument();
				this.StartDocument( ref objXmlDocument , ref objRootNode , "FROIForms" );

				if( objReader != null )
				{
					while( objReader.Read() ) 
					{
						sFormName = objReader.GetString( "FORM_NAME" );
						iFormId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FORM_ID" ), m_iClientId );
						this.CreateElement( objRootNode , "FROIForm" , ref objFroiFormNode );					
						objFroiFormNode.SetAttribute( "Name" , sFormName );
						objFroiFormNode.SetAttribute( "Id" , iFormId.ToString() );
					}
					objReader.Close();
				}
				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetFROIForms.Error") , p_objEx );				
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				sbSQL = null ;
				objRootNode = null ;
				objFroiFormNode = null ;
				objXmlDocument = null ;
			}
		}

		/// Name		: GetClaimForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get claim FROI info XML for the given Claim Id.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <returns>XML String, contains the claim FROI Info.</returns>
		public string GetClaimForms( int p_iClaimId )
		{
			Claim objClaim = null ;
			Event objEvent = null ;
			DbReader objReader = null ;
			StringBuilder sbSQL = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			XmlElement objClaimFormNode = null ;

			string sFormName = "" ;
			string sTitle = "" ;
			int iFormId = 0 ;
			int iStateId = 0 ;

			try
			{
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimId > 0 )
					objClaim.MoveTo ( p_iClaimId );
				else
				{
					objClaim = null;
					throw new RMAppException(Globalization.GetString("FROIManager.InvalidClaimId" ) );
				}
				objEvent = ( Event ) objClaim.Parent ; 					
				iStateId = objEvent.StateId ;

				sbSQL = new StringBuilder();
				sbSQL.Append( " SELECT FORM_ID, FORM_NAME, FORM_TITLE FROM CL_FORMS WHERE CL_FORMS.STATE_ROW_ID = " );
				sbSQL.Append( iStateId + " ORDER BY CL_FORMS.FORM_NAME" );

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
				objXmlDocument = new XmlDocument();
				this.StartDocument( ref objXmlDocument , ref objRootNode , "ClaimForms" );

				if( objReader != null )
				{
					while( objReader.Read() ) 
					{
						sFormName = objReader.GetString( "FORM_NAME" );
						sTitle = objReader.GetString( "FORM_TITLE" );
						iFormId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FORM_ID" ), m_iClientId );
						
						this.CreateElement( objRootNode , "ClaimForm" , ref objClaimFormNode );					
						objClaimFormNode.SetAttribute( "Name" , sFormName );
						objClaimFormNode.SetAttribute( "Title" , sTitle );
						objClaimFormNode.SetAttribute( "Id" , iFormId.ToString() );
					}
					objReader.Close();
				}
				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetClaimForms.Error") , p_objEx );				
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objEvent != null )
				{
					objEvent.Dispose();
					objEvent = null ;
				}
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				sbSQL = null ;
				objRootNode = null ;
				objClaimFormNode = null ;
				objXmlDocument = null ;
			}
		}

		/// Name		: GetWCForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get WC FROI info XML for the given Claim Id.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <returns>XML String, contains the WC FROI Info.</returns>
		public string GetWCForms( int p_iClaimId )
		{
			Claim objClaim = null ;
			DbReader objReader = null ;
			StringBuilder sbSQL = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			XmlElement objWCFormNode = null ;

			string sFormName = "" ;
			string sTitle = "" ;
			string sFormCatDesc = "" ;
			int iFormId = 0 ;
			int iStateId = 0 ;

			try
			{
				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );
				if( p_iClaimId > 0 )
					objClaim.MoveTo ( p_iClaimId );
				else
				{
					objClaim = null;
					throw new RMAppException(Globalization.GetString("FROIManager.InvalidClaimId" ) );
				}								
				iStateId = objClaim.FilingStateId ;

				sbSQL = new StringBuilder();
				sbSQL.Append( " SELECT WCP_FORMS_CAT_LKUP.FORM_CAT_DESC, WCP_FORMS.FORM_ID, WCP_FORMS.FORM_NAME, ");
				sbSQL.Append( " WCP_FORMS.FORM_TITLE " );   
				sbSQL.Append( " From WCP_FORMS, WCP_FORMS_CAT_LKUP" );
				sbSQL.Append( " WHERE (WCP_FORMS.FORM_CATEGORY = WCP_FORMS_CAT_LKUP.FORM_CAT)" );
				sbSQL.Append( " AND (WCP_FORMS.STATE_ROW_ID = WCP_FORMS_CAT_LKUP.STATE_ROW_ID)" );
				sbSQL.Append( " AND (WCP_FORMS.STATE_ROW_ID = " + iStateId + ")" );
				sbSQL.Append( " AND (WCP_FORMS.PRIMARY_FORM_FLAG = -1)" );
				sbSQL.Append( " ORDER BY WCP_FORMS_CAT_LKUP.FORM_CAT_DESC," );
				sbSQL.Append( " WCP_FORMS.FORM_NAME , WCP_FORMS.FORM_TITLE" );

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
				objXmlDocument = new XmlDocument();
				this.StartDocument( ref objXmlDocument, ref objRootNode , "ClaimForms" );

				if( objReader != null )
				{
					while( objReader.Read() ) 
					{
						sFormName = objReader.GetString( "FORM_NAME" );
						sTitle = objReader.GetString( "FORM_TITLE" );
						sFormCatDesc = objReader.GetString( "FORM_CAT_DESC" );
						iFormId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FORM_ID" ), m_iClientId );
						
						this.CreateElement( objRootNode , "ClaimForm" , ref objWCFormNode );					
						objWCFormNode.SetAttribute( "Name" , sFormName );
						objWCFormNode.SetAttribute( "Title" , sTitle );
						objWCFormNode.SetAttribute( "FormCatDesc" , sFormCatDesc );
						objWCFormNode.SetAttribute( "Id" , iFormId.ToString() );
					}
					objReader.Close();
				}
				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetWCForms.Error") , p_objEx );				
			}
			finally
			{
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				sbSQL = null ;
				objRootNode = null ;
				objWCFormNode = null ;
				objXmlDocument = null ;
			}
		}

		#endregion 

		#region Get Forms information for Batch FROI in XML format.
		/// Name		: GetBatchFROIForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the Batch FROI information for the criteria selected.
		/// </summary>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_bClaimByDateOfClaim">Claims By Date Of Claim Flag</param>
		/// <param name="p_iIncludeIndex">Include Index.( 0 - None, 1 - Batch , 2- Single, 3 - Both. )</param>
		/// <returns>XML contains the Batch Info.</returns>
		public string GetBatchFROIForms( string p_sFromDate , string p_sToDate , bool p_bClaimByDateOfClaim , int p_iIncludeIndex )
		{
			DbReader objReader = null ;
			ArrayList arrlstForms = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			XmlElement objBatchFROIForm = null ;
			BatchFROIInfo structBatchFROIInfo ;
			
			string sSQL = "" ;
			int iHighLevel = 0 ;
			int iLowLevel = 0 ;
			int iExClosedClaim = 0 ;
			int iIndex = 0 ;

			try
			{
				this.GetExistingOrgHierarchyData( ref iHighLevel , ref iLowLevel , ref iExClosedClaim );

				sSQL = this.BuildFROISQL( p_sFromDate , p_sToDate , p_bClaimByDateOfClaim , p_iIncludeIndex 
					, iHighLevel , iLowLevel , iExClosedClaim );

				arrlstForms = new ArrayList();
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader != null )
				{
					while( objReader.Read() )
					{
						structBatchFROIInfo = new BatchFROIInfo();
						structBatchFROIInfo.ClaimNumber = objReader.GetString( "CLAIM_NUMBER" );
						structBatchFROIInfo.ClaimDate = Conversion.GetDBDateFormat( objReader.GetString( "DATE_OF_CLAIM" ) , "d");
						structBatchFROIInfo.LastName = objReader.GetString( "PI_LAST_NAME" );
						structBatchFROIInfo.FirstName = objReader.GetString( "PI_FIRST_NAME" );
						structBatchFROIInfo.StateId = objReader.GetString( "STATE_ID" );
						structBatchFROIInfo.StateName = objReader.GetString( "STATE_NAME" );
						structBatchFROIInfo.OrgBLastName = objReader.GetString( "ORG_A_LAST_NAME" );
						structBatchFROIInfo.OrgALastName = objReader.GetString( "ORG_B_LAST_NAME" );
						structBatchFROIInfo.ClaimId = Common.Conversion.ConvertObjToInt(objReader.GetValue( "CLAIM_ID" ), m_iClientId );
						arrlstForms.Add( structBatchFROIInfo );
					}
					objReader.Close();
				}
			
				objXmlDocument = new XmlDocument();
				this.StartDocument( ref objXmlDocument , ref objRootNode , "BatchFROIForms" );
				this.CreateAndSetElement( objRootNode , "HighLevel" , ( iHighLevel > 1004 ).ToString() );
				this.CreateAndSetElement( objRootNode , "LowLevel" , ( iLowLevel > 1004 ).ToString() );

				this.DeleteStateJobs( arrlstForms , "WA" , "Washington" , objRootNode );

				for( iIndex = 0 ; iIndex < arrlstForms.Count ; iIndex++ )
				{
					structBatchFROIInfo = ( BatchFROIInfo )arrlstForms[iIndex] ;
					this.CreateElement( objRootNode , "BatchFROIForm" , ref objBatchFROIForm );
					this.CreateAndSetElement( objBatchFROIForm , "ClaimNumber" , structBatchFROIInfo.ClaimNumber );
					this.CreateAndSetElement( objBatchFROIForm , "DateOfClaim" , structBatchFROIInfo.ClaimDate );
					this.CreateAndSetElement( objBatchFROIForm , "LastName" , structBatchFROIInfo.LastName );
					this.CreateAndSetElement( objBatchFROIForm , "FirstName" , structBatchFROIInfo.FirstName );
					this.CreateAndSetElement( objBatchFROIForm , "StateId" , structBatchFROIInfo.StateId );
					this.CreateAndSetElement( objBatchFROIForm , "StateName" , structBatchFROIInfo.StateName );
					this.CreateAndSetElement( objBatchFROIForm , "OrgALastName" , structBatchFROIInfo.OrgALastName );
					this.CreateAndSetElement( objBatchFROIForm , "OrgBLastName" , structBatchFROIInfo.OrgBLastName );
					this.CreateAndSetElement( objBatchFROIForm , "ClaimId" , structBatchFROIInfo.ClaimId.ToString() );
				}
				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetBatchFROIForms.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();				
				arrlstForms = null ;
				objXmlDocument = null ;
				objRootNode = null ;
				objBatchFROIForm = null ;
			}			
		}

		/// Name		: BuildFROISQL
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Build Batch FROI SQL
		/// </summary>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_bClaimByDateOfClaim">Claims By Date Of Claim Flag</param>
		/// <param name="p_iIncludeIndex">Include Index.( 0 - None, 1 - Batch , 2- Single, 3 - Both.</param>
		/// <param name="p_iHighLevel">High Level</param>
		/// <param name="p_iLowLevel">Low Level</param>
		/// <param name="p_iExClosedClaim">Ex Closed Claim</param>
		/// <returns>Batch FROI SQL for selected criteria</returns>
		private string BuildFROISQL( string p_sFromDate , string p_sToDate , bool p_bClaimByDateOfClaim , 
			int p_iIncludeIndex, int p_iHighLevel , int p_iLowLevel , int p_iExClosedClaim )
		{
			DbReader objReaderClosedStatus = null ;

			StringBuilder sbSQLFields = null ;
			StringBuilder sbSQLWhere = null ;
			StringBuilder sbSQLClosedStatus = null ;
			StringBuilder sbClosedStatus = null ;
			
			string sAlias = "" ;			
			bool bFirstItem = true ;

			try
			{
				if ( Constants.g_sDBType == Constants.DB_ACCESS )
					sAlias = " AS " ;
				else
					sAlias = " " ;
				
				sbSQLFields = new StringBuilder();
				sbSQLFields.Append( " SELECT DISTINCT CLAIM_NUMBER, DATE_OF_CLAIM" );
				sbSQLFields.Append( " , PI.LAST_NAME " + sAlias + " PI_LAST_NAME, PI.FIRST_NAME " + sAlias + " PI_FIRST_NAME" );
				sbSQLFields.Append( " , STATES.STATE_ID, STATE_NAME, CLAIM.CLAIM_ID " );
				if( p_iHighLevel > 1004 )
					sbSQLFields.Append( " , ORG_A.LAST_NAME " + sAlias + " ORG_A_LAST_NAME" );			
				if( p_iLowLevel > 1004 )
					sbSQLFields.Append( " , ORG_B.LAST_NAME " + sAlias + " ORG_B_LAST_NAME" );
			
				sbSQLFields.Append( " FROM CLAIM,CLAIMANT,STATES,ENTITY " + sAlias + " PI" );
				if( p_iHighLevel > 1004 )
					sbSQLFields.Append( " ,ORG_HIERARCHY,PERSON_INVOLVED,ENTITY " + sAlias + " ORG_A" );			
				if( p_iLowLevel > 1004 )
					sbSQLFields.Append( " , ENTITY " + sAlias + " ORG_B" );

				sbSQLWhere = new StringBuilder();
				sbSQLWhere.Append( " WHERE CLAIM.LINE_OF_BUS_CODE = 243" );
				sbSQLWhere.Append( " AND CLAIM.FILING_STATE_ID = STATES.STATE_ROW_ID" );
				sbSQLWhere.Append( " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID" );
				sbSQLWhere.Append( " AND CLAIMANT.CLAIMANT_EID = PI.ENTITY_ID" );
				if( p_iHighLevel > 1004 )
				{
					sbSQLWhere.Append( " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID" );
					sbSQLWhere.Append( " AND CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID" );
					sbSQLWhere.Append( " AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = ORG_HIERARCHY.DEPARTMENT_EID" );
					sbSQLWhere.Append( " AND ORG_A.ENTITY_ID = ORG_HIERARCHY." );
					sbSQLWhere.Append( Conversion.EntityTableIdToOrgTableName( p_iHighLevel ).ToUpper() + "_EID" );
				}
				if( p_iLowLevel > 1004 )
				{
					sbSQLWhere.Append( " AND ORG_B.ENTITY_ID = ORG_HIERARCHY." );
					sbSQLWhere.Append( Conversion.EntityTableIdToOrgTableName( p_iLowLevel ).ToUpper() + "_EID" );
				}

				if( p_iExClosedClaim == 1 )
				{
					sbSQLClosedStatus = new StringBuilder();
					sbSQLClosedStatus.Append( " SELECT CODES_B.CODE_ID" );
					sbSQLClosedStatus.Append( " FROM CODES " + sAlias + " CODES_A,CODES " + sAlias + " CODES_B" );
					sbSQLClosedStatus.Append( " WHERE CODES_A.TABLE_ID = " + this.GetTableId( "STATUS" ) );
					sbSQLClosedStatus.Append( " AND CODES_A.SHORT_CODE = 'C'" );
					sbSQLClosedStatus.Append( " AND CODES_A.CODE_ID = CODES_B.RELATED_CODE_ID" );
					sbSQLClosedStatus.Append( " AND CODES_B.TABLE_ID = " + this.GetTableId( "CLAIM_STATUS" ) );				
					
					objReaderClosedStatus = DbFactory.GetDbReader( m_sConnectionString , sbSQLClosedStatus.ToString() );
					sbSQLClosedStatus = null ;

					if( objReaderClosedStatus != null )
					{
						sbClosedStatus = new StringBuilder();
						while( objReaderClosedStatus.Read() )
						{
							if( bFirstItem )
							{
								sbClosedStatus.Append( Conversion.ConvertObjToStr( objReaderClosedStatus.GetValue( "CODE_ID" ) ) );
								bFirstItem = false ;
							}
							else
								sbClosedStatus.Append( " , " + Conversion.ConvertObjToStr( objReaderClosedStatus.GetValue( "CODE_ID" ) ) );
						}
						objReaderClosedStatus.Close();
						if( sbClosedStatus.ToString().Trim() != "" )
							sbSQLWhere.Append( " AND CLAIM_STATUS_CODE NOT IN (" + sbClosedStatus.ToString().Trim() + ")" );					

						sbClosedStatus = null ;
					}					
				}
			
				sbSQLFields.Append( sbSQLWhere.ToString() );
				sbSQLFields.Append( this.ClaimSelectOptionsSQL( "JURIS_FORMS_HIST" , p_sFromDate , p_sToDate , p_bClaimByDateOfClaim , p_iIncludeIndex ) );

				return( sbSQLFields.ToString() );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.BuildFROISQL.SQLBuildError") , p_objEx );				
			}
			finally
			{
				if( objReaderClosedStatus != null )
				{
					objReaderClosedStatus.Close();
					objReaderClosedStatus.Dispose();
				}
				sbSQLFields = null ;
				sbSQLWhere = null ;
				sbSQLClosedStatus = null ;
				sbClosedStatus = null ;
			}
		}

		/// Name		: ClaimSelectOptionsSQL
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Advanced the SQL using Include Index( None , Batch , Single, Both ). 
		/// </summary>
		/// <param name="sTableName">Table Name</param>
		/// <param name="p_sFromDate"> From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_bClaimByDateOfClaim">Claims By Date Of Claim Flag</param>
		/// <param name="p_iIncludeIndex">Include Index.( 0 - None, 1 - Batch , 2- Single, 3 - Both.</param>
		/// <returns>SQL WHERE string</returns>
		private string ClaimSelectOptionsSQL( string sTableName , string p_sFromDate , string p_sToDate ,
			bool p_bClaimByDateOfClaim , int p_iIncludeIndex )
		{
			StringBuilder sbSQL = null ;

			try
			{
				sbSQL = new StringBuilder();
				switch( p_iIncludeIndex )
				{
					case -1 :
						// this means no printed claims will be included in the selection list
						sbSQL.Append( " AND CLAIM.CLAIM_ID NOT IN" );
						sbSQL.Append( " (SELECT DISTINCT " + sTableName + ".CLAIM_ID FROM " + sTableName + ")" );
						break;
					case 0 :
						// this means no printed claims will be included in the selection list
						sbSQL.Append( " AND CLAIM.CLAIM_ID NOT IN" );
						sbSQL.Append( " (SELECT DISTINCT " + sTableName + ".CLAIM_ID FROM " + sTableName + ")" );
						break;
					case 1 :
						// include only BATCH printed Claims.
						// HOW_PRINTED is Compared with 'SINGL'. It Should be 'SINGLE'. Subject of change.
						sbSQL.Append( " AND CLAIM.CLAIM_ID NOT IN" );
						sbSQL.Append( " (SELECT DISTINCT " + sTableName + ".CLAIM_ID" );
						sbSQL.Append( " FROM " + sTableName );
						sbSQL.Append( " WHERE HOW_PRINTED = 'SINGL')" );
						break;
					case 2 :
						// include only SINGLE printed Claims
						sbSQL.Append( " AND CLAIM.CLAIM_ID NOT IN" );
						sbSQL.Append( " (SELECT DISTINCT " + sTableName + ".CLAIM_ID" );
						sbSQL.Append( " FROM " + sTableName );
						sbSQL.Append( " WHERE HOW_PRINTED = 'BATCH')" );
						break;
					case 3 :
						break;
					default :
						throw new InvalidValueException( Globalization.GetString("FROIManager.ClaimSelectOptionsSQL.InvalidValue") );						
				}
			
				if( p_bClaimByDateOfClaim )
				{
					sbSQL.Append( " AND DATE_OF_CLAIM BETWEEN '" + p_sFromDate );
					sbSQL.Append( "' AND '" + p_sToDate + "'" );
				}
				else
				{
					sbSQL.Append( " AND CLAIM.DTTM_RCD_ADDED BETWEEN '" + p_sFromDate );
					sbSQL.Append( "000000' AND '" + p_sToDate + "240000'" );
				}

				sbSQL.Append( " ORDER BY DATE_OF_CLAIM, CLAIM_NUMBER" );
				return( sbSQL.ToString() );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.ClaimSelectOptionsSQL.SQLBuildError") , p_objEx );				
			}
			finally
			{
				sbSQL = null ;
			}
		}

		/// Name		: GetExistingOrgHierarchyData
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Org Hierarchy Data
		/// </summary>
		/// <param name="p_iHighLevel">High Level</param>
		/// <param name="p_iLowLevel">Low Level</param>
		/// <param name="p_iExClosedClaim">Ex Closed Claim</param>
		private void GetExistingOrgHierarchyData( ref int p_iHighLevel , ref int p_iLowLevel , ref int p_iExClosedClaim )
		{
			DbReader objReader = null ;
			string sSQL = "" ;

			try
			{
				sSQL = "SELECT * FROM SYS_PARMS " ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					p_iHighLevel = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FROI_BAT_HIGH_LVL" ), m_iClientId );
					p_iLowLevel = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FROI_BAT_LOW_LVL" ), m_iClientId );
					p_iExClosedClaim = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FROI_BAT_CLD_CLM" ), m_iClientId );
					if( p_iExClosedClaim < 0 )
						p_iExClosedClaim = - p_iExClosedClaim ;
				}
				else
				{
					p_iHighLevel = -1 ;
					p_iLowLevel = -1 ;
					p_iExClosedClaim = 0 ;				
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetExistingOrgHierarchyData.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}
		
		/// Name		: DeleteStateJobs
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete the FROI's from the Batch if PDS Template form does not exist.
		/// </summary>
		/// <param name="p_arrlstForms">Array List of forms</param>
		/// <param name="p_sStateId">State Id</param>
		/// <param name="p_sStateName">State Name</param>
		/// <param name="p_objRootNode">Root Node of XML Document, for Error Message </param>
		private void DeleteStateJobs( ArrayList p_arrlstForms , string p_sStateId , string p_sStateName , XmlElement p_objRootNode )
		{
			DbReader objReader = null ;
			BatchFROIInfo structBatchFROIInfo ;

			int iIndex = 0 ;
			int iFROIFormId = 0 ;
			int iDotPosition = -1 ;
			bool bKillPrintJobs = false ;
			string sSQL = "" ;
			string sPDFFileName = "" ;
			string sPDSFileName = "" ;
			string sJurisName = "" ;
			string sMessage = "" ;
			string sPdfCustomPath = "" ;

			try
			{
				if( p_arrlstForms.Count != 0 )
				{
					for( iIndex = 0 ; iIndex < p_arrlstForms.Count ; iIndex++ )
					{
						structBatchFROIInfo = ( BatchFROIInfo )p_arrlstForms[iIndex] ;
						if( structBatchFROIInfo.StateId == p_sStateId && structBatchFROIInfo.StateName == p_sStateName )
						{
							iFROIFormId = this.GetFROIFormId( p_sStateId );
							if( iFROIFormId < 1 )
								bKillPrintJobs = true ;
							break;
						}
					}

					sSQL = " SELECT PDF_FILE_NAME,FORM_NAME, STATE_ROW_ID "
						+	" FROM JURIS_FORMS WHERE FORM_ID = " + iFROIFormId ;
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader != null )
					{
						if( objReader.Read() )
							sPDFFileName = objReader.GetString( "PDF_FILE_NAME" );
						else
							bKillPrintJobs = true ;
						objReader.Close();
					}
					iDotPosition = sPDFFileName.IndexOf( DOTT_STRING );
					if( iDotPosition > 0 )
						sJurisName = sPDFFileName.Substring( 0 , iDotPosition );
					sPDFFileName = sJurisName + ".PDF" ;
					sPDSFileName = sJurisName + ".PDS" ;

					// Read the Template PDFs Path from the configuration file.
					m_sPdfPath = this.ReadConfigFile( PDF_TEMPLATE_FORMS_PATH , true );
					
					// Make the Custom PDF Template path.
					sPdfCustomPath = m_sPdfPath + @"client-custom\froi\" ;

					if( !( ( File.Exists( sPdfCustomPath + sPDFFileName ) && File.Exists( sPdfCustomPath + sPDSFileName ) )
						||	( File.Exists( m_sPdfPath + sPDFFileName ) && File.Exists( m_sPdfPath + sPDSFileName ) ) ))
						bKillPrintJobs = true ;

					if( bKillPrintJobs )
					{
						sMessage = Globalization.GetString("FROIManager.DeleteStateJobs.WAError");
						this.CreateAndSetElement( p_objRootNode , "Message" , sMessage );

						for( iIndex = 0 ; iIndex < p_arrlstForms.Count ; iIndex++ )
						{
							structBatchFROIInfo = ( BatchFROIInfo )p_arrlstForms[iIndex] ;
							if( structBatchFROIInfo.StateId == p_sStateId && structBatchFROIInfo.StateName == p_sStateName )
							{
								p_arrlstForms.RemoveAt( iIndex );
								iIndex-- ; 
							}
						}
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.DeleteStateJobs.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();				
			}
		}

		/// Name		: GetFROIFormId
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Form Id for given StateId.
		/// </summary>
		/// <param name="p_sStateId">State Id</param>
		/// <returns>Form Id</returns>
		private int GetFROIFormId( string p_sStateId )
		{
			StringBuilder sbSQL = null ;
			DbReader objReader = null ;

			int iFROIFormId = 0 ;
			
			try
			{
				sbSQL = new StringBuilder();
				sbSQL.Append( "SELECT FORM_ID FROM JURIS_FORMS, STATES " );
				sbSQL.Append( " WHERE JURIS_FORMS.STATE_ROW_ID = STATES.STATE_ROW_ID " );
				sbSQL.Append( " AND STATES.STATE_ID = '" + p_sStateId + "' AND PRIMARY_FORM_FLAG = -1" );
				
				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
				if( objReader != null )
				{
					if( objReader.Read() )
						iFROIFormId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FORM_ID" ), m_iClientId );
					objReader.Close();
				}				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetFROIFormId.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				sbSQL = null ;			
			}			
			return( iFROIFormId );
		}

		
		#endregion 

		#region Invoke FROI , WCForms , ClaimForms
		/// Name		: InvokeFROIForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Invoke FROI.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iFormId">Form Id</param>
		/// <param name="p_bAttachForm">Attach Document Flag</param>
		/// <param name="p_sDestinationStoragePath">Document Storage Path.( File System Path OR Connection String )</param>
		/// <param name="p_enumStorageType">Storage Type</param>
		/// <param name="p_sFDFFilePath">Return FDF File Path </param>
		/// <param name="p_sErrorText">Return Error Text</param>
		/// <returns>Return true if success </returns>
		public bool InvokeFROIForms( int p_iClaimId , int p_iFormId , bool p_bAttachForm , string p_sDestinationStoragePath 
			, StorageType p_enumStorageType , ref string p_sFDFFilePath , ref string p_sErrorText )
		{
			bool bReturnValue = false ;
			try
			{
				// Read The PDF paths from the configuration file and validate them
				this.ReadPathsFromConfigFile();

				bReturnValue = this.InvokeFROI( p_iClaimId , p_iFormId , p_bAttachForm , p_sDestinationStoragePath, 
					p_enumStorageType , ref p_sFDFFilePath , ref p_sErrorText );
				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("FROIManager.InvokeFROIForms.Error"),p_objException);
			}
			return( bReturnValue );
		}

		/// Name		: InvokeFROI
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Invoke FROI.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iFormId">Form Id</param>
		/// <param name="p_bAttachForm">Attach Document Flag</param>
		/// <param name="p_sDestinationStoragePath">Document Storage Path.( File System Path OR Connection String )</param>
		/// <param name="p_enumStorageType">Storage Type</param>
		/// <param name="p_sFDFFilePath">Return FDF File Path </param>
		/// <param name="p_sErrorText">Return Error Text</param>
		/// <returns>Return true if success </returns>
		private bool InvokeFROI( int p_iClaimId , int p_iFormId , bool p_bAttachForm , string p_sDestinationStoragePath,
			StorageType p_enumStorageType , ref string p_sFDFFilePath , ref string p_sErrorText )
		{
			FROIExtender objFROIExtender = null ;
			Populater objPopulator = null ;
			FormManager objFormManager = null ;
			            
			bool bReturnValue = false ;
			string sPDFLink = "" ;
			string sPDFFileName = "" ;
			string sPDSFilePath = "" ;
			string sFDFFileName = "" ;
			string sFDFFilePath = "" ;

			int iErrorCode = 0 ;		

			try
			{				
				// Fill the FROIExtender object.
				objPopulator = new Populater( m_sUserName , m_sPassword , m_sDsnName );
				objFROIExtender = objPopulator.InvokeFROI( p_iClaimId , ref iErrorCode );
				
				if( iErrorCode <= 0 )
				{
					// Read the PDF file name. 
					sPDFFileName = this.GetPDFName( p_iFormId , (int)FormType.FROIForm );	
					sPDFFileName = sPDFFileName.ToLower();

					// Set the PDFLink path, PDS file path and FDF file path.
					this.SetFilePath( (int)FormType.FROIForm , objFROIExtender.objClaim.ClaimNumber , "" , 
						sPDFFileName , ref sPDFLink , ref sPDSFilePath , ref sFDFFileName ,ref sFDFFilePath );
					
					// Call the Form Processer
					objFormManager = new FormManager( m_sConnectionString );
					objFormManager.ProcessForm( sPDFLink , sPDSFilePath , sFDFFilePath , objFROIExtender ); 
					
					// Attach Form
					if( p_bAttachForm )
					{
						this.AttachForm( p_sDestinationStoragePath , p_enumStorageType , sFDFFileName , sFDFFilePath , 
							p_iClaimId );
					}
					
					// Log operation in JURIS_FORMS_HIST
					this.UpdateFormsTable( p_iFormId , objFROIExtender.ClaimID );
					
					p_sFDFFilePath = sFDFFilePath ;
					bReturnValue = true ;					
				}
				else
				{
					p_sErrorText = this.GetErrorText( iErrorCode );
					bReturnValue = false ;
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("FROIManager.InvokeFROI.Error"),p_objException);
			}
			finally
			{
				if( objPopulator != null )
					objPopulator.Dispose();	
				if( objFormManager != null )
					objFormManager.Dispose();
				objFROIExtender = null ;				
			}
			return( bReturnValue );
		}
				
		/// Name		: InvokeWCForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Invoke WC FROI.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iFormId">Form Id</param>
		/// <param name="p_sState">State Id</param>
		/// <param name="p_sDestinationStoragePath">Document Storage Path.( File System Path OR Connection String )</param>
		/// <param name="p_enumStorageType">Storage Type</param>
		/// <param name="p_sFDFFilePath">Return FDF File Path </param>
		public void InvokeWCForms( int p_iClaimId , int p_iFormId , string p_sState ,  
			string p_sDestinationStoragePath, StorageType p_enumStorageType , ref string p_sFDFFilePath )
		{
			JURISExtender objJURISExtender = null ;
			Populater objPopulator = null ;
			FormManager objFormManager = null ;
					            
			string sPDFLink = "" ;
			string sPDFFileName = "" ;
			string sPDSFilePath = "" ;
			string sFDFFileName = "" ;
			string sFDFFilePath = "" ;

			try
			{
				// Read The PDF paths from the configuration file and validate them
				this.ReadPathsFromConfigFile();

				// Fill the FROIExtender object.
				objPopulator = new Populater( m_sUserName , m_sPassword , m_sDsnName );
				objJURISExtender = objPopulator.InvokeWorkComp( p_iClaimId );
				
				// Read the PDF file name. 
				sPDFFileName = this.GetPDFName( p_iFormId , (int)FormType.WCClaimForm );
				sPDFFileName = sPDFFileName.ToLower();

				// Set the PDFLink path, PDS file path and FDF file path.
				this.SetFilePath( (int)FormType.WCClaimForm , objJURISExtender.objClaim.ClaimNumber , p_sState , 
					sPDFFileName , ref sPDFLink , ref sPDSFilePath , ref sFDFFileName, ref sFDFFilePath );					

				// Call the Form Processer
				objFormManager = new FormManager( m_sConnectionString );
				objFormManager.ProcessForm( sPDFLink , sPDSFilePath , sFDFFilePath , objJURISExtender ); 

				// Attach Form
				this.AttachForm( p_sDestinationStoragePath , p_enumStorageType , sFDFFileName , sFDFFilePath , 
					p_iClaimId );

				p_sFDFFilePath = sFDFFilePath ;												
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("FROIManager.InvokeWCForms.Error"),p_objException);
			}
			finally
			{
				if( objPopulator != null )
					objPopulator.Dispose();	
				if( objFormManager != null )
					objFormManager.Dispose();
				objJURISExtender = null ;							
			}			
		}
				
		/// Name		: InvokeClaimForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Invoke Claim FROI.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iFormId">Form Id</param>
		/// <param name="p_sState"> State Id</param>
		/// <param name="p_sFDFFilePath">Return FDF File Path </param>
		/// <param name="p_sErrorText">Return Error Text</param>
		/// <returns>Return true if success </returns>
		public bool InvokeClaimForms( int p_iClaimId , int p_iFormId , string p_sState ,
			ref string p_sFDFFilePath , ref string p_sErrorText )
		{
			FROIExtender objFROIExtender = null ;
			Populater objPopulator = null ;
			FormManager objFormManager = null ;
			            
			bool bReturnValue = false ;
			string sPDFLink = "" ;
			string sPDFFileName = "" ;
			string sPDSFilePath = "" ;
			string sFDFFileName = "" ;
			string sFDFFilePath = "" ;

			int iErrorCode = 0 ;		

			try
			{
				// Read The PDF paths from the configuration file and validate them
				this.ReadPathsFromConfigFile();

				// Fill the FROIExtender object.
				objPopulator = new Populater( m_sUserName , m_sPassword , m_sDsnName );
				objFROIExtender = objPopulator.InvokeClaimVehAccNewYork( p_iClaimId , ref iErrorCode );
				
				if( iErrorCode <= 0 )
				{
					// Read the PDF file name. 
					sPDFFileName = this.GetPDFName( p_iFormId , (int)FormType.ClaimForm );	
					sPDFFileName = sPDFFileName.ToLower();

					// Set the PDFLink path, PDS file path and FDF file path.
					this.SetFilePath( (int)FormType.ClaimForm , objFROIExtender.objClaim.ClaimNumber , p_sState , 
						sPDFFileName , ref sPDFLink , ref sPDSFilePath , ref sFDFFileName, ref sFDFFilePath );

					// Call the Form Processer
					objFormManager = new FormManager( m_sConnectionString );
					objFormManager.ProcessForm( sPDFLink , sPDSFilePath , sFDFFilePath , objFROIExtender ); 

					p_sFDFFilePath = sFDFFilePath ;
					bReturnValue = true ;					
				}
				else
				{
					p_sErrorText = this.GetErrorText( iErrorCode );
					bReturnValue = false ;
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("FROIManager.InvokeClaimForms.Error"),p_objException);
			}
			finally
			{
				if( objPopulator != null )
					objPopulator.Dispose();	
				if( objFormManager != null )
					objFormManager.Dispose();
				objFROIExtender = null ;				
			}
			return( bReturnValue );
		}
				
		
		#endregion 

		#region Invoke Batch FROI
		/// Name		: InvokeBatchFROI
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Invoke Batch FROI.
		/// </summary>
		/// <param name="p_sInputString">Input XML String, contains the FROI's info.</param>
		/// <param name="p_sDestinationStoragePath">Document Storage Path.( File System Path OR Connection String )</param>		
		/// <param name="p_enumStorageType">Storage Type</param>
		/// <param name="p_sXMLPdfsPath">Return XML, contains the FDF file paths for all succeeded FROI</param>
		/// <param name="p_sFailedFROIPdfPath">Return String, contains the Failed FROI PDF file path </param>
		/// <returns>Return true if no FROI has failed </returns>
		public bool InvokeBatchFROI( string p_sInputString , string p_sDestinationStoragePath, StorageType p_enumStorageType , 
			ref string p_sXMLPdfsPath , ref string p_sFailedFROIPdfPath )
		{
			XmlDocument objInputDoc = null ;
			ArrayList arrlstInvokeBatchFROI = null ;
			ArrayList arrlstErrors = null ;
			ArrayList arrlstFDFFilePath = null ;
			Claim objClaim = null ;

			InvokeFROIInfo structInvokeFROIInfo ;
			ErrorInfo structErrorInfo ;

            int iClaimId = 0 ;
			int iFormId = 0 ;
			string sFDFFilePath = "" ;
			string sErrorText = "" ;
			bool bReturnValue = false ;
			
			try
			{
				arrlstErrors = new ArrayList();
				arrlstFDFFilePath = new ArrayList();

				objInputDoc = new XmlDocument();
				objInputDoc.LoadXml( p_sInputString );

				arrlstInvokeBatchFROI = this.GetBatchFROIList( objInputDoc );	
			
				// Read The PDF paths from the configuration file and validate them
				this.ReadPathsFromConfigFile();

				objClaim = ( Claim ) m_objDataModelFactory.GetDataModelObject( "Claim" , false );

				for( int iIndex = 0 ; iIndex < arrlstInvokeBatchFROI.Count ; iIndex++ )
				{
					structInvokeFROIInfo = ( InvokeFROIInfo ) arrlstInvokeBatchFROI[iIndex];
				
					iClaimId = structInvokeFROIInfo.ClaimId ;
					iFormId = this.GetFROIFormId( structInvokeFROIInfo.StateId );

					// Invoke FROI.
					bReturnValue = this.InvokeFROI( iClaimId , iFormId , true , p_sDestinationStoragePath, 
						p_enumStorageType , ref sFDFFilePath, ref sErrorText );  

					if( bReturnValue )
						arrlstFDFFilePath.Add( sFDFFilePath );
					else
					{	
						objClaim.MoveTo( iClaimId );
						
						// Add error info in error list.
						structErrorInfo = new ErrorInfo();
						structErrorInfo.Error = sErrorText ;
						structErrorInfo.ClaimNumber = objClaim.ClaimNumber ;
						arrlstErrors.Add( structErrorInfo );
					}
				}

				if( arrlstErrors.Count > 0 )
				{
					bReturnValue = false ;
					p_sFailedFROIPdfPath = this.PrintFailedFROIReport( arrlstErrors );
				}
				else
					bReturnValue = true ;

				// Return XML contains the FDF file paths.
				p_sXMLPdfsPath = this.GetFDFFilesXML( arrlstFDFFilePath );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.InvokeBatchFROI.Error") , p_objEx );				
			}
			finally
			{
				objInputDoc = null ;
				arrlstInvokeBatchFROI = null ;
				arrlstErrors = null ;
				arrlstFDFFilePath = null ;
				if( objClaim != null )
				{
					objClaim.Dispose();
					objClaim = null ;
				}
			}
			return( bReturnValue );
		}

		/// Name		: PrintFailedFROIReport
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Failed FROI Report.
		/// </summary>
		/// <param name="p_arrlstErrors">ArrayList contains Failed FROI's info.</param>
		/// <returns>Return String, contains the Failed FROI PDF file path</returns>
		private string PrintFailedFROIReport( ArrayList p_arrlstErrors )
		{
			C1PrintDocument objPrintDoc = null ;
			RenderTable objTable = null ;
			TableColumn objTableColumn = null ;
			ErrorInfo structErrorInfo ;
 
			int iNumberOfRows = 0 ;
			string sHeaderText = "" ;
			string sFailedFROIPath = "" ;			

			try
			{
				sFailedFROIPath = this.ReadConfigFile( FAILED_FROI_PDF_PATH , true );

				#region Initialize document and set the layout properties.
				objPrintDoc = new C1PrintDocument();
				objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;
				objPrintDoc.PageSettings.Margins.Top = 45 ;
				objPrintDoc.PageSettings.Margins.Left = 50 ;
				objPrintDoc.PageSettings.Margins.Bottom = 40 ;
				objPrintDoc.PageSettings.Margins.Right = 50 ;	
				objPrintDoc.PageSettings.Landscape = false ;
				objPrintDoc.PageHeader.Height = 0 ;
				objPrintDoc.PageFooter.Height = 0 ;
				objPrintDoc.StartDoc();

				objTable = new RenderTable( objPrintDoc );
				objPrintDoc.Style.Borders.AllEmpty = true;
				objTable.Body.StyleTableCell.Font = new Font( "Arial" , 10 );
			
				objTable.Columns.AddSome( 2 );
				objTableColumn = new TableColumn( objPrintDoc );
				objTableColumn.Width = 2700 ;
				objTable.Columns[0] = objTableColumn ;
				objTableColumn.Width = 8100 ;
				objTable.Columns[1] = objTableColumn ;

				iNumberOfRows = p_arrlstErrors.Count + 1 ; 
				objTable.Body.Rows.AddSome( iNumberOfRows );
				objTable.Body.AutoHeight = true ;	
			
				objTable.StyleTableCell.Borders.Bottom = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Top = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Left = new LineDef( Color.Black , 0 );
				objTable.StyleTableCell.Borders.Right = new LineDef( Color.Black , 0 );
					
				objTable.Style.Borders.Left = new LineDef( Color.Black , 1 );
				objTable.Style.Borders.Right = new LineDef( Color.Black , 1 );
				objTable.Style.Borders.Bottom = new LineDef( Color.Black , 1 );
				objTable.Style.Borders.Top = new LineDef( Color.Black , 1 );
			
				objTable.StyleTableCell.Padding.Right = 0 ;
				objTable.StyleTableCell.Padding.Top = 0 ;
				objTable.StyleTableCell.Padding.Bottom = 0 ;			

				objTable.StyleTableCell.BorderTableHorz.Empty = false;
				objTable.StyleTableCell.BorderTableVert.Empty = false;			
				#endregion 

				// Add table columns header text.
				objTable.Body.Cell( 0 , 0 ).RenderText.Text = "Claim Number";
				objTable.Body.Cell( 0 , 1 ).RenderText.Text = "Not Printed Reason(s)";
			
				// Set cell text.
				for( int iIndex = 1 ; iIndex < iNumberOfRows ; iIndex++ )
				{
					structErrorInfo = ( ErrorInfo ) p_arrlstErrors[iIndex-1] ;
					objTable.Body.Cell( iIndex , 0 ).RenderText.Text = structErrorInfo.ClaimNumber ;
					objTable.Body.Cell( iIndex , 1 ).RenderText.Text = structErrorInfo.Error;
				}
			
				// Print the header string.
				sHeaderText = Globalization.GetString("FROIManager.PrintFailedFROIReport.HeaderText") 
					+ System.DateTime.Now.ToString("d") + "\n" ;
				objPrintDoc.RenderBlockText( sHeaderText , new Font( "Arial" , 20 ) );
			
				// For blank line.
				objPrintDoc.RenderBlockText( "\n" , new Font( "Arial" , 10 ) );
			
				// Render the table in block flow.
				objPrintDoc.RenderBlock( objTable );

				// End doc.
				objPrintDoc.EndDoc();
			
				// Save the PDF.
				sFailedFROIPath += this.GetUniqueFileName( "FROI" ) + ".pdf" ;
				objPrintDoc.ExportToPDF( sFailedFROIPath ,false );
				return( sFailedFROIPath );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.PrintFailedFROIReport.Error") , p_objEx );				
			}
			finally
			{
				if( objPrintDoc != null )
				{
					objPrintDoc.Dispose();
					objPrintDoc = null ;
				}
				if( objTable != null )
				{
					objTable.Dispose();
					objTable = null ;
				}
				if( objTableColumn != null )
				{
					objTableColumn.Dispose();
					objTableColumn = null ;
				}				 
			}
		}

		/// Name		: GetFDFFilesXML
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Generate the XML, which contains the FDF file paths for all succeeded FROI.
		/// </summary>
		/// <param name="p_arrlstFDFFilePath">ArrayList contains FDF file paths for all succeeded FROI.</param>
		/// <returns>The XML string, contains the FDF file paths for all succeeded FROI.</returns>
		private string GetFDFFilesXML( ArrayList p_arrlstFDFFilePath )
		{
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;

			int iIndex = 0 ;			
			
			try
			{
				this.StartDocument( ref objXmlDocument , ref objRootNode , "FROIPdfPaths" );
				for( iIndex = 0 ; iIndex < p_arrlstFDFFilePath.Count ; iIndex++ )
					this.CreateAndSetElement( objRootNode , "FROIPdfPath" , p_arrlstFDFFilePath[iIndex].ToString() );

				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetFDFFilesXML.Error") , p_objEx );				
			}
			finally
			{
				objXmlDocument = null ;
				objRootNode = null ;
			}
		}

		/// Name		: GetBatchFROIList
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the FROI's Info from XML Document.
		/// </summary>
		/// <param name="p_objDocument">XML Document.</param>
		/// <returns>The array list of the FROI's, which are in the FROI Batch.</returns>
		private ArrayList GetBatchFROIList( XmlDocument p_objDocument )
		{
			XmlNodeList objNodeListFROI = null ;
			XmlElement objStateNode = null ;
			XmlElement objClaimIdNode = null ;
			ArrayList arrlstReturn = null ;
			InvokeFROIInfo structInvokeFROIInfo ;

			string sStateId = "" ;
			string sClaimId = "" ;

			try
			{
				arrlstReturn = new ArrayList();

				objNodeListFROI = p_objDocument.SelectNodes( "//" + "BatchFROIForm" );
				foreach( XmlNode objTempNodeFROI in objNodeListFROI )
				{
					objStateNode = (XmlElement)objTempNodeFROI.SelectSingleNode( "StateId" );
					if( objStateNode != null )
						sStateId = objStateNode.InnerText ;
					else
						throw new RMAppException(Globalization.GetString("FROIManager.GetBatchFROIList.InvalidXMLFormat") );

					objClaimIdNode = (XmlElement)objTempNodeFROI.SelectSingleNode( "ClaimId" );
					if( objClaimIdNode != null )
						sClaimId = objClaimIdNode.InnerText ;
					else
						throw new RMAppException(Globalization.GetString("FROIManager.GetBatchFROIList.InvalidXMLFormat") );
 
					structInvokeFROIInfo = new InvokeFROIInfo();
					structInvokeFROIInfo.ClaimId = Conversion.ConvertStrToInteger( sClaimId );
					structInvokeFROIInfo.StateId = sStateId ;
					arrlstReturn.Add( structInvokeFROIInfo );
				}
				return( arrlstReturn );
			}
			catch(Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetBatchFROIList.Error") , p_objEx );
			}
			finally
			{
				objNodeListFROI = null ;
				objStateNode = null ;
				objClaimIdNode = null ;
				arrlstReturn = null ;
			}	
		}

		#endregion 

		#region Invoke supportive functions.
		/// Name		: AttachForm
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Attach Document.
		/// </summary>
		/// <param name="p_sDestinationStoragePath">Document Storage Path.( File System Path OR Connection String )</param>
		/// <param name="p_enumStorageType">Storage Type</param>
		/// <param name="p_sAttachDocName">Document Name</param>
		/// <param name="p_sAttachDocPath">Document Path</param>
		/// <param name="p_iClaimId">Claim Id</param>
		private void AttachForm( string p_sDestinationStoragePath, StorageType p_enumStorageType , string p_sAttachDocName
			, string p_sAttachDocPath , int p_iClaimId )
		{
			DocumentManager objDocManager = null ;
			StringBuilder sbDocumentXml = null ;
			MemoryStream objAttachDocStream = null ;
			
			long lDocumentId = 0 ;

			try
			{
				objDocManager = new DocumentManager();
				objDocManager.ConnectionString = m_sConnectionString;
				objDocManager.UserLoginName = m_sUserName;

				objDocManager.DocumentStorageType = p_enumStorageType ;
				objDocManager.DestinationStoragePath = p_sDestinationStoragePath ;
						
				objAttachDocStream = this.CreateStream( p_sAttachDocPath );
						
				sbDocumentXml = new StringBuilder();
				sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
				sbDocumentXml.Append( "<CreateDate>"+ Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
				sbDocumentXml.Append( "<Category></Category><Name>FROI</Name><Class></Class><Subject></Subject>" );
				sbDocumentXml.Append( "<Type></Type>" );
				sbDocumentXml.Append( "<Notes></Notes>" );
				sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
				sbDocumentXml.Append( "<FileName>" + p_sAttachDocName + "</FileName>" );
				sbDocumentXml.Append( "<FilePath>" + p_sAttachDocPath + "</FilePath>");
				sbDocumentXml.Append( "<Keywords>Key</Keywords>" );
				sbDocumentXml.Append( "<AttachTable>CLAIM</AttachTable>" );
				sbDocumentXml.Append( "<AttachRecordId>" + p_iClaimId.ToString() + "</AttachRecordId>" );
				sbDocumentXml.Append( "</Document></data>" ); 

						 	
				objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream , out lDocumentId );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.AttachForm.Error") , p_objEx );				
			}	
			finally
			{
				objDocManager = null ;
				sbDocumentXml = null ;
				if( objAttachDocStream != null )
				{
					objAttachDocStream.Close();
					objAttachDocStream = null ;
				}
			}
		}
		/// Name		: UpdateFormsTable
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Update JURIS_FORMS_HIST table for the printed FROI.
		/// </summary>
		/// <param name="p_iFormId">Form Id</param>
		/// <param name="p_iClaimId">Claim Id</param>
		private void UpdateFormsTable( int p_iFormId , int p_iClaimId )
		{
			DbConnection objConn = null ; 
			StringBuilder sbSQL = null ;

			try
			{
				sbSQL = new StringBuilder();
				sbSQL.Append( " INSERT INTO JURIS_FORMS_HIST(FORM_ID,DATE_PRINTED,TIME_PRINTED,CLAIM_ID, " );
				sbSQL.Append( " USER_ID) VALUES ( " + p_iFormId + ", " );
				sbSQL.Append( " '" + Conversion.ToDbDate( DateTime.Now ) + "', " );
				sbSQL.Append( " '" + Conversion.GetTime( DateTime.Now.ToString() ) + "', " );  
				sbSQL.Append( p_iClaimId + ",'" + m_sUserName + "')" );

				objConn = DbFactory.GetDbConnection( m_sConnectionString );
				objConn.Open();
				objConn.ExecuteNonQuery( sbSQL.ToString() );
				objConn.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.UpdateFormsTable.Error") , p_objEx );				
			}	
			finally
			{
				if( objConn != null )
					objConn.Dispose();
				sbSQL = null ;
			}
		}

		/// Name		: SetFilePath
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Set PDFLink , PDS and FDF file paths.
		/// </summary>
		/// <param name="p_iFormType">Form Type</param>
		/// <param name="p_sClaimNumber">Claim Number</param>
		/// <param name="p_sState">State</param>
		/// <param name="p_sPDFFileName">PDF File Name</param>
		/// <param name="p_sPDFLink">Return PDF Link</param>
		/// <param name="p_sPDSFilePath">Return PDS File Path</param>
		/// <param name="p_sFDFFileName">Return FDF File Name</param>
		/// <param name="p_sFDFFilePath">Return FDF File Path</param>
		private void SetFilePath( int p_iFormType, string p_sClaimNumber , string p_sState , string p_sPDFFileName, 
			ref string p_sPDFLink , ref string p_sPDSFilePath , ref string p_sFDFFileName, ref string p_sFDFFilePath )
		{
			FileStream objFS = null ;

			string sPDFFilePath = "" ;										

			try
			{
				// Set the PDF file name and path.
				sPDFFilePath = this.GetPDFFilePath( p_iFormType , p_sPDFFileName , p_sState );		
				
				// Set the PDF link.
				p_sPDFLink = sPDFFilePath.Replace( m_sPdfPath , m_sPdfUrl ).Replace( @"\\", "/" ).Replace( @"\", "/" );
				
				// Set the PDS file path.
				p_sPDSFilePath = sPDFFilePath.Replace( ".pdf" , ".pds" );
				
				// Set the FDF file name and path.
				p_sFDFFileName = p_sClaimNumber + p_sPDFFileName.Replace( ".pdf" , ".fdf" );
				p_sFDFFileName = p_sFDFFileName.Replace( " " , "" );
				p_sFDFFilePath = m_sOutputPath + p_sFDFFileName ;
				
				// If FDF file name exist create a new file name by adding an index.
				if( File.Exists( p_sFDFFilePath ) )
				{
					p_sFDFFileName = this.GetUniqueFileName( m_sOutputPath , p_sFDFFileName );
					p_sFDFFilePath = m_sOutputPath + p_sFDFFileName ;						
				}
				
				// Create Zero size file.
				objFS = new FileStream( p_sFDFFilePath , FileMode.OpenOrCreate , FileAccess.ReadWrite );
				objFS.Close();
				objFS = null ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.SetFilePath.Error") , p_objEx );				
			}	
			finally
			{
				if( objFS != null )
				{
					objFS.Close();
					objFS = null ;
				}
			}
		}

		/// Name		: GetPDFFilePath
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the PDF file path, specific to the FormType.
		/// </summary>
		/// <param name="p_iFormType">Form Type</param>
		/// <param name="p_sPDFFileName">PDF File Name</param>
		/// <param name="p_sState">State Id</param>
		/// <returns></returns>
		private string GetPDFFilePath( int p_iFormType , string p_sPDFFileName, string p_sState )
		{
			string sPathSegment = "" ;
			string sCustomPathSegment = "" ;
			string sPDFFilePath = "" ;
			try
			{
				switch( p_iFormType )
				{
					case (int)FormType.FROIForm :
						sPathSegment = "" ;
						sCustomPathSegment = @"client-custom\froi\" ;
						break;
					case (int)FormType.WCClaimForm :
						sPathSegment = p_sState + @"\" ;
						sCustomPathSegment = @"Client-Custom\workcomp\" + p_sState + @"\" ;
						break;
					case (int)FormType.ClaimForm :
						sPathSegment = @"ClaimForms\" + p_sState + @"\" ;
						sCustomPathSegment = @"client-custom\claim-forms\" + p_sState + @"\" ;
						break;
					default :
						throw new RMAppException(Globalization.GetString("FROIManager.InvalidFormType"));						
				}

				if( File.Exists( m_sPdfPath + sCustomPathSegment + p_sPDFFileName ) )
					sPDFFilePath = m_sPdfPath + sCustomPathSegment + p_sPDFFileName ;
				else
					sPDFFilePath = m_sPdfPath + sPathSegment + p_sPDFFileName ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetPDFFilePath.Error") , p_objEx );				
			}
			return( sPDFFilePath );
		}

		/// Name		: ReadConfigFile
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Read the Path form configuration file and validate.
		/// </summary>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_bValidate">Validate Flag</param>
		/// <returns>The configuration file value</returns>
		private string ReadConfigFile( string p_sNodeName , bool p_bValidate )
		{
			RMConfigurator objConfig = null;
			string sReturnValue = "" ;
			
			try
			{
				objConfig = new RMConfigurator();
				sReturnValue = objConfig.GetValue( p_sNodeName );
				if( sReturnValue != null )
				{
					if( p_bValidate )
					{
						if(!sReturnValue.EndsWith(@"\"))
							sReturnValue += @"\";

						if( !Directory.Exists( sReturnValue ) )
							throw new RMAppException( Globalization.GetString( "FROIManager.ReadConfigFile.InvalidDirectoryPath" ) );
					}
				}
				else
					throw new RMAppException( Globalization.GetString( "FROIManager.ReadConfigFile.ConfigEntryNotFound" ) );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.ReadConfigFile.Error") , p_objEx );				
			}
			finally
			{
				objConfig = null ;
			}
			return( sReturnValue );
		}

		/// Name		: ReadPathsFromConfigFile
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Read the paths from configuration file.
		/// </summary>
		private void ReadPathsFromConfigFile()
		{			
			try
			{
				// Read the Template PDFs Path from the configuration file.				
				m_sPdfPath = this.ReadConfigFile( PDF_TEMPLATE_FORMS_PATH , true );

				// Read the Out PDf's Path from the configuration file.
				m_sOutputPath = this.ReadConfigFile( PDF_OUT_PATH , true );
				
				// Read the PDF url Path from the configuration file.
				m_sPdfUrl = this.ReadConfigFile( PDF_URL_PATH , false );
												
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.ReadPathsFromConfigFile.Error") , p_objEx );				
			}			
		}

		/// Name		: GetErrorText
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the Error Text for the given Error Code.
		/// </summary>
		/// <param name="p_iErrorCode">Error Code.</param>
		/// <returns>Error Text</returns>
		private string GetErrorText( int p_iErrorCode )
		{
			StringBuilder sbErrorText ;
			
			try
			{
				sbErrorText = new StringBuilder();
				switch( p_iErrorCode )
				{
					case  (int)PopulateExtender.FROIFails.FF_NCCIBodyParts :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIBodyParts") );
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIBodyParts 
						+ (int)PopulateExtender.FROIFails.FF_NCCICauseCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIBodyParts") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCICause") );
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIBodyParts 
						+ (int)PopulateExtender.FROIFails.FF_NCCICauseCode 
						+ (int)PopulateExtender.FROIFails.FF_NCCIIllness :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIBodyParts") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCICause") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIBodyParts 
						+ (int)PopulateExtender.FROIFails.FF_NCCICauseCode 
						+ (int)PopulateExtender.FROIFails.FF_NCCIIllness 
						+ (int)PopulateExtender.FROIFails.FF_NonNumericClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIBodyParts") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCICause") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NonNumericClassCode"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIBodyParts 
						+ (int)PopulateExtender.FROIFails.FF_NCCIIllness :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIBodyParts") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIBodyParts 
						+ (int)PopulateExtender.FROIFails.FF_NCCIIllness 
						+ (int)PopulateExtender.FROIFails.FF_NonNumericClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIBodyParts") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NonNumericClassCode"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIBodyParts 
						+ (int)PopulateExtender.FROIFails.FF_NonNumericClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIBodyParts") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NonNumericClassCode"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCICauseCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCICause") );
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCICauseCode 
						+ (int)PopulateExtender.FROIFails.FF_NCCIIllness :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCICause") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCICauseCode 
						+ (int)PopulateExtender.FROIFails.FF_NCCIIllness 
						+ (int)PopulateExtender.FROIFails.FF_NonNumericClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCICause") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NonNumericClassCode"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCICauseCode 
						+ (int)PopulateExtender.FROIFails.FF_NonNumericClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCICause") );
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NonNumericClassCode"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIIllness :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NCCIIllness 
						+ (int)PopulateExtender.FROIFails.FF_NonNumericClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIInjuryIllness"));
						sbErrorText.Append( "\n" + Globalization.GetString("FROIManager.GetErrorText.NonNumericClassCode"));
						break;
					case  (int)PopulateExtender.FROIFails.FF_NonNumericClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NonNumericClassCode"));
						break;
					case (int)PopulateExtender.FROIFails.FF_NCCIClassCode :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NCCIClass"));
						break ;
					case (int)PopulateExtender.FROIFails.FF_NoDepartmentAssigned :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NoDepartmentAssigned"));
						break ;
					case (int)PopulateExtender.FROIFails.FF_NoClaimant :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.NoClaimant"));
						break;
					case (int)PopulateExtender.FROIFails.FF_LoadFROIOptionsFailed :
						sbErrorText.Append( Globalization.GetString("FROIManager.GetErrorText.LoadFROIOptionsFailed"));
						break;
					default :
						sbErrorText.Append( "General Failure" );
						break ;
				}
				return( sbErrorText.ToString() );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetErrorText.Error") , p_objEx );				
			}
			finally
			{
				sbErrorText = null ;
			}
		}

		/// Name		: GetPDFName
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the PDF name for the given FormId.
		/// </summary>
		/// <param name="p_iFormId">Form Id</param>
		/// <param name="p_iFormType">Form Type</param>
		/// <returns>PDF File Name</returns>
		private string GetPDFName( int p_iFormId , int p_iFormType )
		{
			DbReader objReader = null ;
			string sSQL = "" ;
			string sPDFFileName = "" ;			

			try
			{
				switch( p_iFormType )
				{
					case (int)FormType.FROIForm :
						sSQL = " SELECT PDF_FILE_NAME,FORM_NAME FROM JURIS_FORMS WHERE FORM_ID = " + p_iFormId ;
						break ;
					case (int)FormType.WCClaimForm :
						sSQL = " SELECT FILE_NAME,FORM_NAME FROM WCP_FORMS WHERE FORM_ID = " + p_iFormId ;
						break;
					case (int)FormType.ClaimForm :
						sSQL = " SELECT FILE_NAME,FORM_NAME FROM CL_FORMS WHERE FORM_ID = " + p_iFormId ;
						break ;
					default :
						throw new RMAppException(Globalization.GetString("FROIManager.InvalidFormType"));						
				}
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				
				if( objReader != null )
				{
					if( objReader.Read() )
						sPDFFileName = objReader.GetString( 0 );										
					else
						throw new RMAppException(Globalization.GetString("FROIManager.GetPdfName.InvalidFromId"));
					objReader.Close();
				}				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetPDFName.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();
			}
			return( sPDFFileName );
		}

		/// Name		: GetUniqueFileName
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get unique file name by adding an index.
		/// </summary>
		/// <param name="p_sFilePath">File Path</param>
		/// <param name="p_sFileNameWithExtension">File Name</param>
		/// <returns>Unique File Name</returns>
		private string GetUniqueFileName( string p_sFilePath , string p_sFileNameWithExtension )
		{
			const int NO_OF_ITERATION = 2000 ;

			int	iIndex = 1 ;
			int iPos = -1 ;
			bool bUniqueFile=false;
			string sFileName="";
			string sFileExtension = "" ;
			string sTempFileName="";
			string sUniqueFileName = "" ;
			
			try
			{
				// Get the file name and file extension.
				if( p_sFileNameWithExtension != null )
					iPos = p_sFileNameWithExtension.LastIndexOf( DOTT_STRING );

				if( iPos != -1 )
				{
					sFileName = p_sFileNameWithExtension.Substring( 0 , iPos );
					sFileExtension = p_sFileNameWithExtension.Substring( iPos );					
				}				
				
				// If file name does not exist then set it to "fdf".
				if( sFileName == "" )
					sFileName = "fdf" ;
				
				// Get next file name by adding { "-" + iIndex } to the file name.
				do
				{
					sTempFileName = sFileName + "-" + iIndex.ToString() ;
					// Test if the file name is unique
					if( !File.Exists( p_sFilePath + sTempFileName + sFileExtension ) )
					{
						bUniqueFile = true ;
						break ;
					}
					iIndex++;
				}
				while( iIndex < NO_OF_ITERATION );
				// If unique name is found
				if( bUniqueFile )
					sUniqueFileName = sTempFileName + sFileExtension ;				
				else
				{
					// If unique file name not found then create the file name using time. 
					sUniqueFileName = this.GetUniqueFileName( sFileName + "-" ) + sFileExtension ;									
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetUniqueFileName.Error"),p_objException);
			}				
			return( sUniqueFileName );
		}

		/// Name		: GetUniqueFileName
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get unique file name by using time string.
		/// </summary>
		/// <param name="p_sPrefix">Prefix string</param>
		/// <returns>Unique File Name</returns>
		private string GetUniqueFileName( string p_sPrefix )
		{
			string sTempPath = "" ;
			
			try
			{
				sTempPath = p_sPrefix + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
					+ System.AppDomain.GetCurrentThreadId() ; 			
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetUniqueFileName.Error") , p_objEx );				
			}
			return sTempPath ;
		}
		#endregion 

		#region Integrity Check Functions
		/// Name		: GetIntegrityCheckFROIForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Check the existence of the PDS files for FROI states.
		/// </summary>
		/// <returns>XML string contains PDS file paths for which PDS file does not exist</returns>
		public string GetIntegrityCheckFROIForms()
		{
			DbReader objReader = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			StringBuilder sbSQL = null ;
            			
			string sFileName = "" ;
			string sFilePath = "" ;

			try
			{
				m_sPdfPath = this.ReadConfigFile( PDF_TEMPLATE_FORMS_PATH , true );
				this.StartDocument( ref objXmlDocument , ref objRootNode , "IntChkFROIForms" );

				sbSQL = new StringBuilder();			
				sbSQL.Append( " SELECT STATE_ID, [PDF_FILE_NAME] FROM JURIS_FORMS FRM, STATES " );
				sbSQL.Append( " WHERE FRM.PRIMARY_FORM_FLAG <> 0 AND " );
				sbSQL.Append( " STATES.STATE_ROW_ID = FRM.STATE_ROW_ID " );
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );

				if( objReader != null )
				{
					while( objReader.Read() )
					{
						sFileName = objReader.GetString( 1 );
						sFilePath = m_sPdfPath + sFileName ;

						if( ! File.Exists( sFilePath ) )
							this.CreateAndSetElement( objRootNode , "IntChkFROIFormsPath" , sFilePath );

					}
					objReader.Close();
				}
			
				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetIntegrityCheckFROIForms.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();

				objXmlDocument = null ;
				objRootNode = null ;
				sbSQL = null ;
			}
		}

		/// Name		: GetIntegrityCheckWCForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Check the existence of the PDS files for WC states.
		/// </summary>
		/// <returns>XML string contains PDS file paths for which PDS file does not exist</returns>
		public string GetIntegrityCheckWCForms()
		{
			DbReader objReader = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			StringBuilder sbSQL = null ;
            		
			string sStateId = "" ;
			string sFileName = "" ;
			string sFilePath = "" ;

			try
			{
				m_sPdfPath = this.ReadConfigFile( PDF_TEMPLATE_FORMS_PATH , true );
				this.StartDocument( ref objXmlDocument , ref objRootNode , "IntChkWCForms" );

				sbSQL = new StringBuilder();	
				sbSQL.Append( " SELECT STATE_ID, [FILE_NAME] FROM WCP_FORMS FRM, STATES " );
				sbSQL.Append( " WHERE FRM.PRIMARY_FORM_FLAG <> 0 AND " );
				sbSQL.Append( " STATES.STATE_ROW_ID = FRM.STATE_ROW_ID" );
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );

				if( objReader != null )
				{
					while( objReader.Read() )
					{
						sStateId = objReader.GetString( "STATE_ID" );
						sFileName = objReader.GetString( 1 );
						sFilePath = m_sPdfPath + sStateId + @"\" + sFileName ;

						if( ! File.Exists( sFilePath ) )
							this.CreateAndSetElement( objRootNode , "IntChkWCFormsPath" , sFilePath );

					}
					objReader.Close();
				}
			
				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetIntegrityCheckWCForms.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();

				objXmlDocument = null ;
				objRootNode = null ;
				sbSQL = null ;
			}
		}

		/// Name		: IntegrityCheckClaimForms
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Check the existence of the PDS files for claim states.
		/// </summary>
		/// <returns>XML string contains PDS file paths for which PDS file does not exist</returns>
		public string GetIntegrityCheckClaimForms()
		{
			DbReader objReader = null ;
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			StringBuilder sbSQL = null ;
            		
			string sStateId = "" ;
			string sFileName = "" ;
			string sFilePath = "" ;

			try
			{
				m_sPdfPath = this.ReadConfigFile( PDF_TEMPLATE_FORMS_PATH , true );
				this.StartDocument( ref objXmlDocument , ref objRootNode , "IntChkClaimForms" );

				sbSQL = new StringBuilder();	
				sbSQL.Append( "SELECT STATE_ID, [FILE_NAME] FROM CL_FORMS FRM, STATES " );
				sbSQL.Append( " WHERE FRM.PRIMARY_FORM_FLAG <> 0 AND " );
				sbSQL.Append( " STATES.STATE_ROW_ID = FRM.STATE_ROW_ID" );
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );

				if( objReader != null )
				{
					while( objReader.Read() )
					{
						sStateId = objReader.GetString( "STATE_ID" );
						sFileName = objReader.GetString( 1 );
						sFilePath = m_sPdfPath + @"ClaimForms\" + sStateId + @"\" + sFileName ;

						if( ! File.Exists( sFilePath ) )
							this.CreateAndSetElement( objRootNode , "IntChkClaimFormsPath" , sFilePath );

					}
					objReader.Close();
				}
			
				return( objXmlDocument.OuterXml );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetIntegrityCheckClaimForms.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
					objReader.Dispose();

				objXmlDocument = null ;
				objRootNode = null ;
				sbSQL = null ;
			}
		}


		#endregion 

		#region Common XML functions
		/// Name		: StartDocument
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the XML document
		/// </summary>
		/// <param name="p_objXmlDocument">XML document</param>
		/// <param name="p_objRootNode">Root Node</param>
		/// <param name="p_sRootNodeName">Root Node Name</param>
		private void StartDocument( ref XmlDocument p_objXmlDocument, ref XmlElement p_objRootNode  , string p_sRootNodeName )
		{
			try
			{
				p_objXmlDocument = new XmlDocument();
				p_objRootNode = p_objXmlDocument.CreateElement( p_sRootNodeName );
				p_objXmlDocument.AppendChild( p_objRootNode );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.StartDocument.Error") , p_objEx );				
			}
		}

		/// Name		: CreateElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Node Name</param>	
		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName )
		{
			XmlElement objChildNode = null ;
			try
			{
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.CreateElement.Error") , p_objEx );
			}
			finally
			{
				objChildNode = null ;
			}

		}	
	
		/// Name		: CreateElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Child Node Name</param>
		/// <param name="p_objChildNode">Child Node</param>
		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode )
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.CreateElement.Error") , p_objEx );
			}

		}
		
		/// Name		: CreateAndSetElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sText">Text</param>	
		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText )
		{
			try
			{
				XmlElement objChildNode = null ;
				this.CreateAndSetElement( p_objParentNode , p_sNodeName , p_sText , ref objChildNode );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.CreateAndSetElement.Error") , p_objEx );
			}
		}
		/// Name		: CreateAndSetElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sText">Text</param>
		/// <param name="p_objChildNode">Child Node</param>
		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode )
		{
			try
			{
				CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode );
				p_objChildNode.InnerText = p_sText ;			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.CreateAndSetElement.Error") , p_objEx );
			}
		}

		#endregion 		

		#region Dispose
		/// Name		: Dispose
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Dispose the objects.
		/// </summary>
		public void Dispose()
		{
			try
			{
				if( m_objDataModelFactory != null )
				{
					m_objDataModelFactory.UnInitialize();
					m_objDataModelFactory = null ;	
				}
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}		

		#endregion

		#region Misc.
		/// Name		: GetTableId
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************		
		/// <summary>
		/// Get Table Id for the given Table Name.
		/// </summary>
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>Table Id </returns>
		private int GetTableId(string p_sTableName )
		{
			DbReader objReader = null ;

			int iTableId = 0 ;
			string sSQL = "" ;
			
			try
			{
				if( p_sTableName == "" )
					return(0) ;

				sSQL = " SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + p_sTableName + "'" ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if ( objReader != null )
					if( objReader.Read() )				
						iTableId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TABLE_ID" ), m_iClientId );					
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.GetTableId.Error") , p_objEx );				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( iTableId );				
		} 

		/// Name		: CreateStream
		/// Author		: Vaibhav Kaushik
		/// Date Created: 02/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************						
		/// <summary>
		/// Creates a memory stream of given file
		/// </summary>
		/// <param name="p_sFilePath">Complete File Path</param>
		/// <returns>Memory stream of the given file</returns>
		private MemoryStream CreateStream( string p_sFilePath )
		{
			MemoryStream objMemoryStream = null;
			FileStream objFileStream = null;
			BinaryReader objBinaryReader = null ;
			Byte[] arrByte = null;

			try
			{
				if( !File.Exists( p_sFilePath ) )
					throw new FileNotFoundException(Globalization.GetString("FROIManager.CreateStream.FileNotFound"));
				objFileStream = new FileStream( p_sFilePath , FileMode.Open , FileAccess.Read );
				objMemoryStream = new MemoryStream( (int)objFileStream.Length );
				objBinaryReader = new BinaryReader( objFileStream );
                arrByte = objBinaryReader.ReadBytes( (int)objFileStream.Length );
				objMemoryStream.Write( arrByte , 0 , (int)objFileStream.Length );
				return( objMemoryStream );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FROIManager.CreateStream.Error") , p_objEx );				
			}
			finally
			{
				if( objFileStream != null )
				{
					objFileStream.Close();
					objFileStream = null ;
				}
				if( objBinaryReader != null )
				{
					objBinaryReader.Close();
					objBinaryReader = null ;
				}
				arrByte = null;
				if( objMemoryStream != null )
				{
					objMemoryStream.Close();
					objMemoryStream = null ;
				}
			}			
		}
		
		#endregion 
	}
}
