using System;
using System.Xml ;
using Riskmaster.DataModel ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;
using System.Collections ;
using System.Data;
using System.Text ;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.DocumentManagement;
using System.IO;  
using Riskmaster.Common.Win32;
using Riskmaster.Application.SecurityManagement;
using SMENGINELib; 
using Riskmaster.Application.OSHALib;
using Riskmaster.Application.ExecutiveSummaryReport;
using SMInterface;

namespace Riskmaster.Application.ReportAdmin
{
	/// <summary>
	/// Summary description for ReportAdmin.
	/// </summary>
	public class ReportAdmin
	{
 
		# region Class Level Declarations
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";
		/// <summary>
		/// Private variable to store the instance of XmlDocument object
		/// </summary>
		private XmlDocument m_objDocument = null ;

		/// <summary>
		/// 
		/// </summary>

		#endregion

		# region constructor
		public ReportAdmin()
		{
			//
			// TODO: Add constructor logic here
			//


		}
		public ReportAdmin(string sConnectionString)
		{
			m_sConnectionString=sConnectionString;
		}
		#endregion

		# region Get Connection for SMSever
		private string GetSMServerConn()
		{
			XmlDocument xmlConfig=new XmlDocument();
			XmlNode xmlNodeSM=null;
			string strSMConn="";
			try
			{
				xmlNodeSM=RMConfigurator.NamedNode("SORTMASTERServerSettings/Host/ConnectionString");
				strSMConn=xmlNodeSM.InnerXml;
			}
			catch(Exception ex)
			{
				string ss="";
				ss=ex.Message;
			}
			return strSMConn;
		}
		#endregion

		# region Get Schedule Reports
		/// Name		: GetScheduleReports
		/// Author		: Rahul Sharma
		/// Date Created: 08/02/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// 
		///*************************************************************************
		/// <summary>
		/// Get the Schedule Report Listing information from the database.
		/// </summary>
		/// <param name="p_sInputXml">XML string containing report information to be retrieved.</param>
		/// 
		/// <returns>Data in form XML.</returns>
		
		public string GetScheduleReports(string p_sInputXml)
		{
			DbConnection connSM=null;
			DbReader objReader=null;
			string sSQL="";
			string sMode="";
			string sOrderBy="";
			XmlDocument xmlInput=new XmlDocument();
            XmlElement objRootNode=null;
			XmlElement objRowNode = null ;
			try
			{
				connSM=DbFactory.GetDbConnection(GetSMServerConn());
				connSM.Open();
				xmlInput.LoadXml(p_sInputXml);				
				sMode=xmlInput.SelectSingleNode("//Mode").InnerXml;
				sOrderBy=xmlInput.SelectSingleNode("//OrderBy").InnerXml;
				sSQL="SELECT SCHEDULE_ID, SM_SCHEDULE.REPORT_ID, SM_REPORTS.REPORT_NAME, SCHEDULE_TYPE, LAST_RUN_DTTM, NEXT_RUN_DATE, START_TIME FROM SM_SCHEDULE,SM_REPORTS ";
				sSQL=sSQL+" WHERE SM_SCHEDULE.REPORT_ID=SM_REPORTS.REPORT_ID ";
				if(sOrderBy=="")
				{
					sSQL=sSQL+" ORDER BY SM_REPORTS.REPORT_NAME " +  sMode;
				}
				else
				{
					sSQL=sSQL+" ORDER BY "+ sOrderBy + " " + sMode;
				}
				objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
				objRootNode = null ;
				if(objReader!=null)
				{
					this.StartDocument( ref objRootNode , "NewDataSet" );
					while(objReader.Read())
					{
						this.CreateElement( objRootNode , "Table" , ref objRowNode );
						this.CreateAndSetElement( objRowNode , "SCHEDULE_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("SCHEDULE_ID")));
						this.CreateAndSetElement( objRowNode , "REPORT_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("REPORT_ID")));
						this.CreateAndSetElement( objRowNode , "REPORT_NAME" , objReader.GetString("REPORT_NAME"));
						this.CreateAndSetElement( objRowNode , "SCHEDULE_TYPE" , Conversion.ConvertObjToStr(objReader.GetInt32("SCHEDULE_TYPE")));
						this.CreateAndSetElement( objRowNode , "LAST_RUN_DTTM" , Conversion.GetDBDTTMFormat(objReader.GetString("LAST_RUN_DTTM"),"D","T"));
						this.CreateAndSetElement( objRowNode , "NEXT_RUN_DATE" , Conversion.GetDBDateFormat(objReader.GetString("NEXT_RUN_DATE"),"D"));
						this.CreateAndSetElement( objRowNode , "START_TIME" , Conversion.GetDBTimeFormat(objReader.GetString("START_TIME"),"T"));
					}
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetScheduleReports") , p_objException );				
			}
			finally
			{
				connSM.Close();
				connSM=null;
				objReader=null;
				objRowNode=null;
				objRootNode=null;
			}
			return (m_objDocument.InnerXml);
		}
		#endregion

		# region Get Queue Report
		/// Name		: GetQueueReports
		/// Author		: Rahul Sharma
		/// Date Created: 08/02/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// 
		///*************************************************************************
		/// <summary>
		/// Get the GetQueue Reports Listing information from the database.
		/// </summary>
		/// <param name="p_sInputXml">XML string containing report information to be retrieved.</param>
		/// 
		/// <returns>Data in form XML.</returns>
		public string GetQueueReports(string p_sInputXml,string sLoginName,int iDSNid)
		{
			DbConnection connSM=null;
			DbReader objReader=null;
			DbReader objReaderMSG=null;
			//DbReader objReaderMSG1=null;
			string sSQL="";
			XmlElement objRootNode=null;
			XmlElement objRowNode = null ;
			DataSet objDsMsg;
			string sMsgSQL="";
			string sUserName="";
			string sMsg="";
			try
			{
				connSM=DbFactory.GetDbConnection(GetSMServerConn());
				connSM.Open();
				sSQL="SELECT JOB_ID,JOB_NAME,JOB_DESC,OUTPUT_PATH_URL,START_DTTM,ASSIGNED,ASSIGNED_DTTM,COMPLETE,COMPLETE_DTTM,ERROR_FLAG,USER_ID, OUTPUT_TYPE,OUTPUT_PATH FROM SM_JOBS WHERE ";
				sSQL=sSQL+ "(ARCHIVED = 0 OR ARCHIVED IS NULL)";
				sSQL=sSQL+ " ORDER BY JOB_ID DESC";
				objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
				objRootNode = null ;
				if(objReader!=null)
				{
					this.StartDocument( ref objRootNode , "NewDataSet" );
					while(objReader.Read())
					{
						this.CreateElement( objRootNode , "Table" , ref objRowNode );
						this.CreateAndSetElement( objRowNode , "JOB_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("JOB_ID")));
						this.CreateAndSetElement( objRowNode , "JOB_NAME" , objReader.GetString("JOB_NAME"));
						this.CreateAndSetElement( objRowNode , "JOB_DESC" , objReader.GetString("JOB_DESC"));
						this.CreateAndSetElement( objRowNode , "OUTPUT_PATH_URL" , objReader.GetString("OUTPUT_PATH_URL"));
						if(objReader.GetString("START_DTTM")=="")
						{
							this.CreateAndSetElement( objRowNode , "START_DTTM" , "Run Immediately");
						}
						else
						{
							this.CreateAndSetElement( objRowNode , "START_DTTM" , Conversion.GetDBDateFormat(objReader.GetString("START_DTTM"),"d"));
						}
						sMsgSQL="";
						this.CreateAndSetElement( objRowNode , "ASSIGNED" , Conversion.ConvertObjToStr(objReader.GetInt16("ASSIGNED")));
						this.CreateAndSetElement( objRowNode , "ASSIGNED_DTTM" , Conversion.GetDBDTTMFormat(objReader.GetString("ASSIGNED_DTTM"),"d","T"));
						this.CreateAndSetElement( objRowNode , "COMPLETE" , Conversion.ConvertObjToStr(objReader.GetInt16("COMPLETE")));
						this.CreateAndSetElement( objRowNode , "COMPLETE_DTTM" , Conversion.GetDBDateFormat(objReader.GetString("COMPLETE_DTTM"),"d"));
						if(Conversion.ConvertObjToStr(objReader.GetInt16("ASSIGNED"))=="-1" && Conversion.ConvertObjToStr(objReader.GetInt16("COMPLETE"))=="0")
						{
							objDsMsg=new DataSet();
							sMsgSQL="SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = "+Conversion.ConvertObjToStr(objReader.GetInt32("JOB_ID"))+" AND MSG_TYPE = 'progress' ORDER BY LOG_ID DESC";
							objDsMsg=DbFactory.GetDataSet(GetSMServerConn(),sMsgSQL);
							//objReaderMSG=DbFactory.GetDbReader(GetSMServerConn(),sMsgSQL);
							if(objDsMsg.Tables[0].Rows.Count==0)
							{
								sMsg="";
							}
							else
							{
								sMsg=objDsMsg.Tables[0].Rows[0]["MSG"].ToString();
							}
							//							if(objReaderMSG!=null)
							//								sMsg=objReaderMSG.GetString(0);
							this.CreateAndSetElement( objRowNode , "MSG" ,sMsg);
						}
						else
						{
							if(Conversion.ConvertObjToStr(objReader.GetInt16("ASSIGNED"))=="-1" && Conversion.ConvertObjToStr(objReader.GetInt16("COMPLETE"))=="-1")
							{
								if(Conversion.ConvertObjToStr(objReader.GetInt16("ERROR_FLAG"))=="-1")
								{
									objDsMsg=new DataSet();
									sMsgSQL="SELECT MSG FROM SM_JOB_LOG WHERE JOB_ID = "+Conversion.ConvertObjToStr(objReader.GetInt32("JOB_ID"))+" AND MSG_TYPE = 'error' ORDER BY LOG_ID DESC";
									objDsMsg=DbFactory.GetDataSet(GetSMServerConn(),sMsgSQL);
									//objReaderMSG=DbFactory.GetDbReader(GetSMServerConn(),sMsgSQL);
									if(objDsMsg.Tables[0].Rows.Count==0)
									{
										sMsg="";
									}
									else
									{
										sMsg=objDsMsg.Tables[0].Rows[0]["MSG"].ToString();
									}
									//if(objReaderMSG!=null)
									//	sMsg=objReaderMSG.GetString(0);
									this.CreateAndSetElement( objRowNode , "MSG" ,sMsg);
								}
							}

						}
						objReaderMSG=null;
						//objReaderMSG1=null;
						UserLogins objUser=null;
						User obju=null;
						
						this.CreateAndSetElement( objRowNode , "ERROR_FLAG" , Conversion.ConvertObjToStr(objReader.GetInt16("ERROR_FLAG")));
						this.CreateAndSetElement( objRowNode , "USER_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("USER_ID")));
						try
						{
							objUser=new UserLogins(objReader.GetInt32("USER_ID"),iDSNid);
							sUserName=objUser.LoginName+"("+objUser.LoginName+")";
						}
						catch
						{
							sUserName="";
						}
						this.CreateAndSetElement( objRowNode , "USER_NAME" , sUserName);
						this.CreateAndSetElement( objRowNode , "OUTPUT_TYPE" ,objReader.GetString("OUTPUT_TYPE"));
						this.CreateAndSetElement( objRowNode , "OUTPUT_PATH" , objReader.GetString("OUTPUT_PATH"));
					}
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetQueueReports") , p_objException );				
			}
			finally
			{
				connSM.Close();
				connSM=null;
				objReader=null;
				objRowNode=null;
				objRootNode=null;
			}
			return (m_objDocument.InnerXml);
		}
		#endregion
		
		# region Post New Report
		public bool PostNewReport(string p_sInputXml,UserLogin objUser,string sConnectionStr)
		{
			m_sConnectionString = sConnectionStr;
			MemoryStream objDocumentManagementFile=null;
			XmlDocument xmlInput=new XmlDocument();
			string sFile = string.Empty;
			string sReportName=string.Empty;
			string sReportDesc=string.Empty;
			string sSQL=string.Empty;
			XmlNode objXmlNode = null;
			FileStorage.FileStorageManager objFileStorage=null;
			string sFilePath=string.Empty;
			string sFileName=string.Empty;
			bool bReturnValue = false;
			string [] arrsFileNameParts = null;
			int iReport_ID=0;
			DbCommand objCmd=null;
			DbConnection objConn=null;
			DbTransaction objTrans=null;
			XmlDocument objXMLReport=null;
			SMEngineClass smi = new SMEngineClass();
			try
			{
				xmlInput.LoadXml(p_sInputXml);
				objXmlNode = null;
				objXmlNode = xmlInput.SelectSingleNode("//FileDetails/file");
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					throw new InvalidValueException
						(Globalization.GetString("ReportAdmin.PostNewReport.MissingFileNode"));
					return bReturnValue;
				}
				sFilePath=objUser.DocumentPath;
				if(sFilePath=="" || sFilePath==null)
				{
					sFilePath=RMConfigurator.NamedNode("StorageDestination").InnerText;
				}
				if((sFilePath == null) || (sFilePath.Trim() == ""))
					throw new InvalidValueException
						(Globalization.GetString("ReportAdmin.PostNewReport.DefaultDestinationFilePathNotSpecified"));
				sFile = objXmlNode.InnerText;
				objXmlNode = xmlInput.SelectSingleNode("//FileDetails/Filename");
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					throw new InvalidValueException
						(Globalization.GetString("ReportAdmin.PostNewReport.MissingFileName"));
					return bReturnValue;
				}
				sFileName=objXmlNode.InnerText;
				
				//string sDelimiter = "\";
				if (sFileName != "")
				{
					arrsFileNameParts = sFileName.Split("\\".ToCharArray());
					foreach(string sFName in arrsFileNameParts)
					{
						if (sFName.IndexOf('.') > 0)
						{
							sFileName = sFName;
						}
					}					
				}
				objFileStorage=new FileStorageManager();
				if ( !Directory.Exists( sFilePath ) )
				{
				 Directory.CreateDirectory( sFilePath );
				}
				sFilePath=sFilePath+"\\"+sFileName;
				objDocumentManagementFile = new MemoryStream(Convert.FromBase64String(sFile));
				char[] b64Data=sFile.ToCharArray();
				byte[] decodeData=Convert.FromBase64CharArray(b64Data,0,b64Data.Length);

				int i=objFileStorage.StoreFile(objDocumentManagementFile,sFilePath);
//				FileStream fs = new FileStream(sFilePath, FileMode.Create, FileAccess.Write, FileShare.None);
//				fs.Write(decodeData, 0, decodeData.Length);
//				fs.Close();
//				char[] charArr = new char[decodeData.Length];
//				decodeData.CopyTo( charArr,0);
//				StringBuilder sb= new StringBuilder();
//				sb.Append( charArr);
				iReport_ID=GetNextID("SM_REPORTS",GetSMServerConn());
				//objXMLReport.LoadXml(b64Data.ToString());
				
				
				smi.InitEngine( m_sConnectionString );
				if ( smi.SMRConvert( sFilePath,sFilePath) )
				{
					objXMLReport=new XmlDocument();
					objXMLReport.Load(sFilePath);
					objXmlNode=null;
					objXmlNode = xmlInput.SelectSingleNode("//FileDetails/ReportName");
					if(objXmlNode==null)
					{ 
						bReturnValue = false;
						throw new InvalidValueException
							(Globalization.GetString("ReportAdmin.PostNewReport.MissingReportName"));
						//return bReturnValue;
					}
					sReportName=objXmlNode.InnerText;
					objXmlNode=null;
					objXmlNode = xmlInput.SelectSingleNode("//FileDetails/ReportDesc");
					if(objXmlNode==null)
					{ 
						bReturnValue = false;
						throw new InvalidValueException
							(Globalization.GetString("ReportAdmin.PostNewReport.MissingReportDescription"));
						//	return bReturnValue;
					}
					sReportDesc=objXmlNode.InnerText;

					objConn=DbFactory.GetDbConnection(GetSMServerConn());
					objConn.Open();
					objTrans = objConn.BeginTransaction();
					objCmd = objConn.CreateCommand();
					objCmd.Connection = objConn;
					objCmd.Transaction = objTrans;
				

					objCmd.CommandText = "INSERT INTO SM_REPORTS (REPORT_ID,REPORT_NAME,REPORT_DESC,REPORT_XML) VALUES ('" + iReport_ID + "','"+sReportName+"','"+sReportDesc+"','"+objXMLReport.InnerXml+"')";
					objCmd.ExecuteNonQuery();
					objCmd.CommandText = "INSERT INTO SM_REPORTS_PERM(REPORT_ID,USER_ID) VALUES('" + iReport_ID +"','"+ objUser.UserId + "')" ;
					objCmd.ExecuteNonQuery();
					objTrans.Commit();
				
					//				sSQL="INSERT INTO SM_REPORTS (REPORT_ID,REPORT_NAME,REPORT_DESC,REPORT_XML) VALUES ('" + iUID + "','"+sReportName+"','"+sReportDesc+"','"+objXMLReport.InnerXml+"')";
					//				objConn.ExecuteNonQuery(sSQL);
					//				objConn.Close();
					//objCmd.CommandText=sSQL;
					//objCmd.Connection=objConn;
					//objCmd.ExecuteNonQuery();
					//SMENGINELib.SMEngineClass objSM=new SMEngineClass();
					//objSM.
					return true;
				}
				else
				{
					//todo exception handling
					throw new InvalidValueException
						(Globalization.GetString("ReportAdmin.PostNewReport.MissingFileName"));
				}
			}
			catch(RMAppException p_objException)
			{
				if ( objTrans != null )
				{
					objTrans.Rollback();
				}
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetScheduleDetail") , p_objException );				
			}
			finally 
			{
				objCmd = null;
				objTrans =null;
				objConn.Close();
				smi = null;
			}
		}

		#endregion

		#region Get Next Id for Table
		/// <summary>
		/// Fetches the current ID and sets next ID as incremented by 1
		/// </summary>
		/// <param name="p_sTableName">Table Name</param>
		/// <param name="p_sConnectStr">Connection string</param>
		/// <returns>Next Id of the Table; -1 if failed</returns>
		public int GetNextID(string p_sTableName,string p_sConnectStr) 
		{ 
			int iNextUID = 0;		/* Initialize, if next id couldn't be retrieved */
			int iOrigUID = 0;		/* Initialize */
			int iRows = 0;

			DbConnection oCnn = null;
			DbReader oReader = null;

			try
			{
				/* Obtain the connection for Sortmaster database */
				oCnn = DbFactory.GetDbConnection(p_sConnectStr);
				oCnn.Open();
 
				string sSql = "SELECT NEXT_ID FROM SM_IDS"
					+ " WHERE TABLE_NAME = '" + p_sTableName + "'";
				
				/* Fire the query */
				oReader = oCnn.ExecuteReader(sSql);
				
				if(oReader.Read())
				{
					iNextUID = oReader.GetInt32(0);
				}
				else
				{
					return -1;
				}

				oReader.Close();
 
				/* Compute next id */
				iOrigUID = iNextUID; 
				if (iOrigUID != 0) 
					iNextUID++; 
				else 
					iNextUID = 2; 

				/* Try to reserve id (searched update) */
				sSql  = "UPDATE SM_IDS SET NEXT_ID = " + iNextUID
					+ " WHERE TABLE_NAME = '" + p_sTableName + "'";
			
				/* Only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96 */
				if (iOrigUID != 0) 
					sSql = sSql + " AND NEXT_ID = " + iOrigUID; 
			
				/* Try update */
				iRows = oCnn.ExecuteNonQuery(sSql);

				if (iRows == 1)
					return --iNextUID;
				else
					return -1;
			}
			catch(Exception p_oException)
			{
				/* Raise the exception if failure */
				throw new RMAppException(Globalization.GetString("Generic.GetNextID.NextIdException"), p_oException);
			}
			finally
			{
				/* Release the resources */
				if(oReader != null)
					oReader.Dispose();

				if(oCnn != null)
					oCnn.Dispose();
			}
		}
		#endregion

		#region Get Schedule Detail
		/// Name		: GetScheduleDetail
		/// Author		: Rahul Sharma
		/// Date Created: 08/02/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// 
		///*************************************************************************
		/// <summary>
		/// Get the Get Schedule Detail from the database.
		/// </summary>
		/// <param name="p_sInputXml">XML string containing report information to be retrieved.</param>
		/// <param name="iUserId">XML string containing report information to be retrieved.</param>
		/// 
		/// <returns>Data in form XML.</returns>
		public string GetScheduleDetail(string p_sInputXml,int iUserId)
		{
			DbConnection connSM=null;
			XmlElement objRootNode=null;
			XmlElement objRowNode = null ;
			XmlNode objReportNode=null;
			string sSQL="";
			string sOutputTypeSQL="";
			string sOutputType="";
			int iReportType=0;
			XmlDocument xmlInput=new XmlDocument();
			XmlDocument xmlRptXML=new XmlDocument();
			string sScheduleId="";
			string sReportId="";
			DbReader objReader=null;
			string sUserId="";
			DataSet objdsRptXML=new DataSet();
			//	LoginAdaptor objLogin=new LoginAdaptor();
			
			try
			{
				xmlInput.LoadXml(p_sInputXml);
				
				sScheduleId=xmlInput.SelectSingleNode("//Id").InnerXml;
				sReportId=xmlInput.SelectSingleNode("//ReportId").InnerXml;
				connSM=DbFactory.GetDbConnection(GetSMServerConn());
				connSM.Open();
				sUserId=iUserId.ToString();
				sSQL="SELECT SM_SCHEDULE.*, SM_REPORTS.REPORT_NAME FROM SM_SCHEDULE,SM_REPORTS WHERE SM_SCHEDULE.REPORT_ID=SM_REPORTS.REPORT_ID AND SCHEDULE_ID="+sScheduleId;
				sOutputTypeSQL="SELECT REPORT_XML FROM SM_REPORTS,SM_REPORTS_PERM WHERE SM_REPORTS_PERM.USER_ID = " +sUserId+ " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS.REPORT_ID = "+sReportId;
				objdsRptXML=DbFactory.GetDataSet(GetSMServerConn(),sOutputTypeSQL);
				string strInnerXml="";
				if(objdsRptXML.Tables[0].Rows.Count==0)
				{
					sOutputType="Error: Selected report could not be found on the server.";
				}
				else
				{
					xmlRptXML.LoadXml(objdsRptXML.GetXml());
					objReportNode=xmlRptXML.SelectSingleNode("//REPORT_XML");
					strInnerXml = objReportNode.InnerXml.Replace("&lt;","<" ).Replace("&gt;",">").Replace("\r","").Replace("\n","").Replace(@"\","");
					xmlRptXML.LoadXml( strInnerXml);
					objReportNode = xmlRptXML.SelectSingleNode("//report");
					if(objReportNode==null)
					{
						sOutputType="Error: Selected report does not have a top level report element with the 'type' attribute.  Please try uploading it again.";
					}
//					else
//					{
//						//iReportType=Convert.ToInt32(objReportNode.Attributes["selecttype"].Value);
//						if( objReportNode.Attributes["selecttype"].Value.ToUpper().Equals( "PDF" ))
//						{
//							sOutputType="PDF";
//						}
//						else
//						{
//							sOutputType="All";
//						}
//					}
				}
//				objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
//				if(objReader!=null)
//				{
//					this.StartDocument( ref objRootNode , "NewDataSet" );
//					while(objReader.Read())
//					{
//						this.CreateElement( objRootNode , "Table" , ref objRowNode );
//						this.CreateAndSetElement( objRowNode , "OUTPUT_TYPE_OPTIONS" , sOutputType);//Output Format Options
//						this.CreateAndSetElement( objRowNode , "SCHEDULE_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("SCHEDULE_ID")));
//						this.CreateAndSetElement( objRowNode , "REPORT_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("REPORT_ID")));
//						this.CreateAndSetElement( objRowNode , "REPORT_NAME" , objReader.GetString("REPORT_NAME"));
//						this.CreateAndSetElement( objRowNode , "SCHEDULE_TYPE" , Conversion.ConvertObjToStr(objReader.GetInt32("SCHEDULE_TYPE")));
//						this.CreateAndSetElement( objRowNode , "LAST_RUN_DTTM" , Conversion.GetDBDTTMFormat(objReader.GetString("LAST_RUN_DTTM"),"D","T"));
//						this.CreateAndSetElement( objRowNode , "NEXT_RUN_DATE" , Conversion.GetDBDateFormat(objReader.GetString("NEXT_RUN_DATE"),"D"));
//						this.CreateAndSetElement( objRowNode , "START_TIME" , Conversion.GetDBTimeFormat(objReader.GetString("START_TIME"),"T"));
//						this.CreateAndSetElement( objRowNode , "START_DATE" , Conversion.GetDBDateFormat(objReader.GetString("START_DATE"),"d"));
//						this.CreateAndSetElement( objRowNode , "MON_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("MON_RUN"))));
//						this.CreateAndSetElement( objRowNode , "TUE_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("TUE_RUN"))));
//						this.CreateAndSetElement( objRowNode , "WED_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("WED_RUN"))));
//						this.CreateAndSetElement( objRowNode , "THU_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("THU_RUN"))));
//						this.CreateAndSetElement( objRowNode , "FRI_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("FRI_RUN"))));
//						this.CreateAndSetElement( objRowNode , "SAT_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("SAT_RUN"))));
//						this.CreateAndSetElement( objRowNode , "SUN_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("SUN_RUN"))));
//						this.CreateAndSetElement( objRowNode , "DAYOFMONTH_RUN" , Conversion.ConvertObjToStr(objReader.GetInt32("DAYOFMONTH_RUN")));
//						this.CreateAndSetElement( objRowNode , "JAN_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("JAN_RUN"))));
//						this.CreateAndSetElement( objRowNode , "FEB_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("FEB_RUN"))));
//						this.CreateAndSetElement( objRowNode , "MAR_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("MAR_RUN"))));
//						this.CreateAndSetElement( objRowNode , "APR_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("APR_RUN"))));
//						this.CreateAndSetElement( objRowNode , "MAY_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("MAY_RUN"))));
//						this.CreateAndSetElement( objRowNode , "JUN_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("JUN_RUN"))));
//						this.CreateAndSetElement( objRowNode , "JUL_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("JUL_RUN"))));
//						this.CreateAndSetElement( objRowNode , "AUG_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("AUG_RUN"))));
//						this.CreateAndSetElement( objRowNode , "SEP_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("SEP_RUN"))));
//						this.CreateAndSetElement( objRowNode , "OCT_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("OCT_RUN"))));
//						this.CreateAndSetElement( objRowNode , "NOV_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("NOV_RUN"))));
//						this.CreateAndSetElement( objRowNode , "DEC_RUN" , Conversion.ConvertObjToStr(Conversion.ConvertObjToBool(objReader.GetInt16("DEC_RUN"))));
//						this.CreateAndSetElement( objRowNode , "JOB_NAME" , objReader.GetString("JOB_NAME"));
//						this.CreateAndSetElement( objRowNode , "JOB_DESC" , objReader.GetString("JOB_DESC"));
//						this.CreateAndSetElement( objRowNode , "DSN" , objReader.GetString("DSN"));
//						this.CreateAndSetElement( objRowNode , "OUTPUT_TYPE" , objReader.GetString("OUTPUT_TYPE"));
//						this.CreateAndSetElement( objRowNode , "OUTPUT_PATH" , objReader.GetString("OUTPUT_PATH"));
//						this.CreateAndSetElement( objRowNode , "OUTPUT_PATH_URL" , objReader.GetString("OUTPUT_PATH_URL"));
//						this.CreateAndSetElement( objRowNode , "OUTPUT_OPTIONS" , objReader.GetString("OUTPUT_OPTIONS"));
//						this.CreateAndSetElement( objRowNode , "OUTPUT_OPTIONS" , objReader.GetString("OUTPUT_OPTIONS"));
//						this.CreateAndSetElement( objRowNode , "REPORT_XML" , objReader.GetString("REPORT_XML"));
//						this.CreateAndSetElement( objRowNode , "NOTIFICATION_TYPE" , objReader.GetString("NOTIFICATION_TYPE"));
//						this.CreateAndSetElement( objRowNode , "NOTIFY_EMAIL" , objReader.GetString("NOTIFY_EMAIL"));
//						this.CreateAndSetElement( objRowNode , "NOTIFY_MSG" , objReader.GetString("NOTIFY_MSG"));
//						this.CreateAndSetElement( objRowNode , "USER_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("USER_ID")));
//					}
//				}
				return "<NewDataSet><Table><REPORT_XML>" +strInnerXml+ "</REPORT_XML></Table></NewDataSet>";
			}
			catch(RMAppException p_objException)
			{
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetScheduleDetail") , p_objException );				
			}
			finally
			{
				connSM.Close();
				connSM=null;
				objReader=null;
				xmlInput=null;
				objRowNode=null;
				objRootNode=null;

			}
			//return (m_objDocument.InnerXml);
			
		}
		#endregion

		#region Get Report XML
		public string GetReportXML(string p_sInputXml,int iUserId)
		{
			DbConnection connSM=null;
			DbReader objReader=null;
			string sSQL="";
			XmlElement objRootNode=null;
			XmlElement objRowNode = null ;
			try
			{
				connSM=DbFactory.GetDbConnection(GetSMServerConn());
				connSM.Open();
				sSQL="SELECT * FROM SM_REPORTS,SM_REPORTS_PERM WHERE ";
				sSQL=sSQL+" SM_REPORTS_PERM.USER_ID =  " + iUserId + " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID";
				sSQL=sSQL+" ORDER BY SM_REPORTS.REPORT_NAME";
				objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
				objRootNode = null ;
				if(objReader!=null)
				{
					this.StartDocument( ref objRootNode , "NewDataSet" );
					while(objReader.Read())
					{
						this.CreateElement( objRootNode , "Table" , ref objRowNode );
						this.CreateAndSetElement( objRowNode , "REPORT_ID" , Conversion.ConvertObjToStr(objReader.GetInt32("REPORT_ID")));
						this.CreateAndSetElement( objRowNode , "REPORT_NAME" , objReader.GetString("REPORT_NAME"));
						this.CreateAndSetElement( objRowNode , "REPORT_DESC" , objReader.GetString("REPORT_DESC"));
					
					}
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetReportXML") , p_objException );				
			}
			finally
			{
				connSM.Close();
				connSM=null;
				objReader=null;
				objRowNode=null;
				objRootNode=null;
			}
			return (m_objDocument.InnerXml);
		}
		#endregion

		#region Get Users List
		public string GetUsersList(int iDSNid)
		{
			DbConnection connSM=null;
			XmlElement objRootNode=null;
			XmlElement objRowNode = null ;
			string sSQL="";
			XmlDocument xmlInput=new XmlDocument();
			XmlDocument xmlRptXML=new XmlDocument();
			DbReader objReader=null;
			string sOption="";
			DataSet objdsRptXML=new DataSet();

			try
			{
				connSM=DbFactory.GetDbConnection(GetSMServerConn());
				connSM.Open();
				sSQL="SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + iDSNid + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";
				objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
				if(objReader!=null)
				{
					this.StartDocument( ref objRootNode , "NewDataSet" );
					this.CreateElement( objRootNode , "Table" , ref objRowNode );
					while(objReader.Read())
					{
						sOption=objReader.GetString("FIRST_NAME")+ " " + objReader.GetString("LAST_NAME")+ " (" + objReader.GetString("LOGIN_NAME") + ") ";
						this.CreateAndSetElement( objRowNode , "option" , sOption);
						objRowNode.SetAttribute( "value" , Conversion.ConvertObjToStr(objReader.GetInt32("USER_ID")));
					}
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetUsersList") , p_objException );				
			}
			finally
			{
				connSM.Close();
				connSM=null;
				objReader=null;
				objRowNode=null;
				objRootNode=null;
			}
			return (m_objDocument.InnerXml);
		
		}
		#endregion

		#region Create XML
		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_objRootNode">Root Node</param>
		/// <param name="p_sRootNodeName">Root Node Name</param>
		private void StartDocument( ref XmlElement p_objRootNode  , string p_sRootNodeName )
		{
			try
			{
				m_objDocument = new XmlDocument();
				p_objRootNode = m_objDocument.CreateElement( p_sRootNodeName );
				m_objDocument.AppendChild( p_objRootNode );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.StartDocument.ErrorDocInit") , p_objEx );				
			}
		}

		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Node Name</param>		
		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName )
		{
			XmlElement objChildNode = null ;
			try
			{
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.CreateElement.Error") , p_objEx );
			}
			finally
			{
				objChildNode = null ;
			}

		}	
	
		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_objChildNode">Child Node Name</param>
		/// <param name="p_objChildNode">Child Node</param>
		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode )
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.CreateElement.Error") , p_objEx );
			}

		}
		
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="sText">Text</param>		
		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string sText )
		{
			try
			{
				XmlElement objChildNode = null ;
				this.CreateAndSetElement( p_objParentNode , p_sNodeName , sText , ref objChildNode );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.CreateAndSetElement.Error") , p_objEx );
			}
		}
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="sText">Text</param>
		/// <param name="p_objChildNode">Child Node</param>
		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string sText , ref XmlElement p_objChildNode )
		{
			try
			{
				CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode );
				p_objChildNode.InnerText = sText ;			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.CreateAndSetElement.Error") , p_objEx );
			}
		}

		#endregion

		#region Delete Schedule Report
		public bool DeleteReport(string p_sInputXml)
		{
			XmlDocument objXML=new XmlDocument();
			string sLevel="";
			string sParam="";
			int iFlag=0;
			string sSQL="";
			string sReportName="";
			DbReader objReader=null;
			objXML = new XmlDocument();
			objXML.LoadXml(p_sInputXml);
			sParam="(";
			sLevel=Conversion.ConvertObjToStr(objXML.SelectSingleNode("//SelectedIds").InnerXml.ToString());
			string [] sChildRow=sLevel.Split('|');
			if(sChildRow.Length==1 ) 
			{
				sParam=sParam + sChildRow[0].ToString();
			}
			else
			{
				if(sChildRow.Length==0)
				{
					sParam=sParam + sLevel;
				}
				else
				{
					iFlag=1;
					for(int i=0;i<sChildRow.Length;i++)
					{
						if(sChildRow[i]!="")
							sParam=sParam+sChildRow[i]+",";
					}
					sParam=sParam.Remove(sParam.Length-1,1);
				}
			}
			sParam=sParam+")";
			sReportName=Conversion.ConvertObjToStr(objXML.SelectSingleNode("//Calling").InnerXml.ToString());
			if(sReportName=="SR")
			{
				sSQL="DELETE FROM SM_SCHEDULE WHERE SCHEDULE_ID IN " + sParam;
			}
			else
			{
				if(sReportName=="ALL")
				{
					sSQL="DELETE FROM SM_REPORTS_PERM WHERE REPORT_ID IN " + sParam;
					objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
					sSQL="DELETE FROM SM_REPORTS WHERE REPORT_ID IN "+sParam;
					objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
					sSQL="DELETE FROM SM_SCHEDULE WHERE REPORT_ID IN "+sParam;
					objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
				}
				else
				{
					if(sReportName=="QR")
					{
						sSQL="DELETE FROM SM_JOBS WHERE JOB_ID IN " + sParam;
					}
				}
			}
			objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
			return true;
		}
		#endregion

		#region Get Report XML of Jobs
		public string GetXMLDetail(string p_sInputXml)
		{
			DbConnection connSM=null;
			//XmlElement objRootNode=null;
			XmlElement objRoot=null;
			XmlNode objRootNode=null;
			string sSQL="";
			string sOutput="";
			XmlDocument xmlInput=new XmlDocument();
			XmlDocument xmlRptXML=new XmlDocument();
			XmlDocument xmlResult=new XmlDocument();
			string sReportId="";
			DbReader objReader=null;
			DataSet objdsRptXML=new DataSet();
			try
			{
				xmlInput.LoadXml(p_sInputXml);
				
				//sScheduleId=xmlInput.SelectSingleNode("//Id").InnerXml;
				sReportId=xmlInput.SelectSingleNode("//ReportId").InnerXml;
				connSM=DbFactory.GetDbConnection(GetSMServerConn());
				connSM.Open();
				sSQL="SELECT REPORT_XML FROM SM_REPORTS WHERE REPORT_ID = "+sReportId;					
				objReader=DbFactory.GetDbReader(GetSMServerConn(),sSQL);
				if(objReader!=null)
				{
					//this.StartDocument( ref objRootNode , "NewDataSet" );
					if(objReader.Read())
					{
						//this.CreateAndSetElement( objRootNode , "REPORT_XML" , objReader.GetString("REPORT_XML"));
						sOutput=objReader.GetString("REPORT_XML");
					}
				}
				
				sOutput=sOutput.Replace("&lt;","<");
				sOutput=sOutput.Replace("&gt;",">");
				xmlRptXML.LoadXml(sOutput);
				objRoot=xmlResult.CreateElement("NewDataSet");
				xmlResult.AppendChild(objRoot);
				objRootNode=xmlRptXML.SelectSingleNode("//report");
				//xmlResult.AppendChild(objRootNode);
				//objRoot.AppendChild(objRootNode);
				objRoot.InnerXml=objRootNode.OuterXml;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetScheduleDetail") , p_objException );				
			}
			finally
			{
				connSM.Close();
				connSM=null;
				objReader=null;
				xmlInput=null;
				objRootNode=null;
			}
			return (xmlResult.InnerXml);
		}
		#endregion 

		# region CriteriaEdit()
		private string CriteriaEdit(string sReportId, string sReportXML, string sReportType)
		{
			string sCriteriaHTML=string.Empty;
			switch(sReportType)
			{
				case "1":
				case "3":
				case "4":
				case "5":
					sCriteriaHTML= GenerateOSHACriteriaHTML(sReportType,sReportXML);
					break;
				//case "2": ///ToDO
					//sCriteriaHTML= GenerateExecCriteriaHTML();
				default:break;
					//TODO: Genreate new Criteria HTML.
			}
			return sCriteriaHTML;
			
		}
		private string GenerateOSHACriteriaHTML(string sReportType,string sReportXML)
		{
			int i;
			int iSelected=0;
			OshaManager objMgr=null;
			XmlNode objFrm = null;
			XmlElement objOpt = null;
			XmlAttribute objAttr = null;
			XmlNode objNode = null;
			XmlDocument objReportXML=new XmlDocument();
			objReportXML.LoadXml(sReportXML);
			objMgr=new OshaManager(m_sConnectionString,Conversion.ConvertStrToInteger(sReportType));
			objMgr.Init();
			objMgr.CriteriaXmlDom=objReportXML;
			objFrm=objMgr.CriteriaXmlDom.GetElementsByTagName("div").Item(0);

			objNode = objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='false']");
			if(objNode!=null)
			{
				objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("onClick"));
				objNode.Attributes["onClick"].Value="byEstablishmentFlagChanged();";
			}
			
			objNode=objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='true']");
			if(objNode!=null)
			{
				objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("onClick"));
				objNode.Attributes["onClick"].Value="byEstablishmentFlagChanged();";
			}
			//ReportOn Validate Handler (Clear the selected entities if changed)
			objNode=objMgr.CriteriaXmlDom.SelectSingleNode("//select[@name = 'reportlevel']");
			objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("onClick"));
			objNode.Attributes["onClick"].Value="reportlevelChanged();";
			//Orgh Code Lookup by Level (Add indirection layer to onClick so that we can divert to an OSHA Establishment List if needed.)
			objNode=objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'selectedentitiesbtn']");
			objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("onClick"));
			objNode.Attributes["onClick"].Value="selectReportTarget();";
			//Add Year of Report options
			objNode=objMgr.CriteriaXmlDom.SelectSingleNode("//select[@name = 'yearofreport']");
			i=2002;
			if(objNode.Attributes["codeid"].Value!="")
				iSelected=Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].Value);
			if(iSelected==0)
				iSelected=DateTime.Today.Year;
			for(i=2002;i<=DateTime.Today.Year;i++)
			{
				objOpt=objMgr.CriteriaXmlDom.CreateElement("option");
				objOpt.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("value"));
				objOpt.Attributes["value"].Value=i.ToString();
				if(i==iSelected)
				{
					objOpt.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("selected"));
					objOpt.Attributes["selected"].Value="";
				}
				objOpt.Value=i.ToString();
				objNode.AppendChild(objOpt);
			}
			objNode = objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'byoshaestablishmentflag' && @value='false']");
			if(objNode!=null)
			{
				objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("checked"));
				objNode.Attributes["checked"].Value="";
			}
			objNode = objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'reporttype']");
			objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("value"));
			objNode.Attributes["value"].Value=sReportType;
			//Set to "AllEntities"
			objNode = objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'allentitiesflag' && @value = 'true']");
			objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("checked"));
			objNode.Attributes["checked"].Value="";
			//Set to "UseYear" 
			objNode = objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'datemethod' && @value = 'useyear']");
			objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("checked"));
			objNode.Attributes["checked"].Value="";
			//Set to "ShowSoftErrors" 
			objNode = objMgr.CriteriaXmlDom.SelectSingleNode("//input[@name = 'printsofterrlog']");
			objNode.Attributes.Append(objMgr.CriteriaXmlDom.CreateAttribute("checked"));
			objNode.Attributes["checked"].Value="";

			return objMgr.CriteriaXmlDom.InnerXml;
		}
//		private string GenerateExecCriteriaHTML(string sReportType,string sReportXML)
//		{
////			ExecutiveSummary	objExecSumm = null;
////			objExecSumm.
//			
//		}
		#endregion
		#region Report Scheduling Report Info
		/// Name		: GetReportInfo
		/// Author		: Rahul Sharma
		/// Date Created: 08/02/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// 
		///*************************************************************************
		/// <summary>
		/// Get the Get Schedule Detail from the database.
		/// </summary>
		/// <param name="p_sInputXml">XML string containing report information to be retrieved.</param>
		/// <param name="iUserId">XML string containing report information to be retrieved.</param>
		/// 
		/// <returns>Data in form XML.</returns>
		public string GetReportInfo(string p_sInputXml,int iUserId, string sEmailid)
		{
			DbConnection connSM=null;
			XmlNode objReportNode=null;
			string sReportXML= string.Empty;
			string sWhere=string.Empty;
			XmlDocument xmlInput=new XmlDocument();
			XmlDocument xmlRptXML=new XmlDocument();
			string sReportId="";
			DataSet objdsRptXML=new DataSet();
			string sCriteriaHTML=string.Empty;
			//	LoginAdaptor objLogin=new LoginAdaptor();
			try
			{
				xmlInput.LoadXml(p_sInputXml);
				sReportId=xmlInput.SelectSingleNode("//ReportId").InnerXml;
				connSM=DbFactory.GetDbConnection(GetSMServerConn());
				connSM.Open();
				sWhere="SM_REPORTS_PERM.USER_ID = " + iUserId.ToString() + " AND SM_REPORTS.REPORT_ID = SM_REPORTS_PERM.REPORT_ID AND SM_REPORTS.REPORT_ID = " + sReportId;
				sReportXML=GetSingleSMValue("REPORT_XML,REPORT_NAME","SM_REPORTS,SM_REPORTS_PERM",sWhere);
				if(sReportXML=="")
					throw new RMAppException("Error: Selected report could not be found on the server.");
				else
				{
					xmlRptXML=null;
					xmlRptXML.LoadXml(sReportXML);
					if(xmlRptXML==null)
					{
						throw new RMAppException("Error: Selected report could not be loaded.  Please try uploading it again.");
					}
					else
					{
						objReportNode=xmlRptXML.SelectSingleNode("//report");
						if(objReportNode==null)
						{
							throw new RMAppException("Error: Selected report does not have a top level report element with the \'type\' attribute.  Please try uploading it again.");
						}
						else
						{
							if(objReportNode.Attributes["type"].Value=="1" || objReportNode.Attributes["type"].Value=="2" || objReportNode.Attributes["type"].Value=="3" || objReportNode.Attributes["type"].Value=="4" || objReportNode.Attributes["type"].Value=="5")
							{
								sCriteriaHTML=CriteriaEdit(sReportId, sReportXML, objReportNode.Attributes["type"].Value);
							}
							else
							{
								SMIClass objSMI=new SMIClass();
								objSMI.NotifyAttach=true;
								objSMI.NotifyEmbed_Text="Send Email with Report Output Attached";
								objSMI.NotifyDefault=2;
								objSMI.NotifyEmail=true;
								objSMI.NotifyLink_Text="Send Email with Link to Report Output";
								objSMI.NotifyLink=true;
								objSMI.NotifyOnly_Text="Send Email Only (no report output attached or linked)";
								objSMI.NotifyNone_Text="None";
								objSMI.RunDTTM=true;
								objSMI.RunImmediate_Text="Immediately";
								objSMI.RunDTTM_Text="At Specific Date/Time ->";
								objSMI.RunDefault=0;
								sCriteriaHTML=objSMI.generateReportPage(Conversion.ConvertStrToInteger(sReportId),iUserId,sEmailid,m_sConnectionString,GetSMServerConn());

							}
						}
					}
				}
				return sCriteriaHTML;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ReportAdmin.GetScheduleDetail") , p_objException );				
			}
		}
		#endregion
		# region GetSingleSMValue
		private string GetSingleSMValue(string sFieldName, string sTableName, string sCriteria)
		{
			string sSQL=string.Empty;
			string sReportXML=string.Empty;
			if(sFieldName=="" || sTableName=="")
			{
				throw new RMAppException("Table Name and Field Name of Report is missing!!!");
			}
			else
			{
				sSQL = "SELECT " + sFieldName + " FROM " + sTableName;
				if(sCriteria != "")
					sSQL += " WHERE " + sCriteria;
				DataSet dsReportXML= DbFactory.GetDataSet(GetSMServerConn(),sSQL);
				if(dsReportXML.Tables[0].Rows.Count!=0)
					sReportXML = dsReportXML.Tables[0].Rows[0][0].ToString();
				else
					sReportXML = "";	
				dsReportXML=null;
				dsReportXML.Dispose();
			}
			return sReportXML;
		}
		#endregion
	}
}
