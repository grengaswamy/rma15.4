using System;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/// <summary>
	/// Summary description for PolBillingConstants.
	/// </summary>
	public class PolBillingConstants
	{
		/// <summary>
		/// Represents the Percent String.
		/// </summary>
		public const string PERCENT = "%" ;
		/// <summary>
		/// Represents the "Flat" String.
		/// </summary>
		public const string FLAT = "flat" ;


		
        public const string BILL_ITEM_ROW_ID = "BillItemRowId";
		public const string BILL_ITEM_POLICY_ID = "PolicyId" ;
		public const string BILL_ITEM_POLICY_NAME = "PolicyName" ;
		public const string BILL_ITEM_POLICY_NUMBER = "PolicyNumber" ;
		public const string BILL_ITEM_DATE_ENTERED = "DateEntered" ;
		public const string BILL_ITEM_INVOICE_NUMBER = "InvoiceNumber" ;
		public const string BILL_ITEM_NOTICE_NUMBER = "NoticeNumber" ;
		public const string BILL_ITEM_DATE_RECONCILED = "DateReconciled" ;
		public const string BILL_ITEM_TERM_NUMBER = "TermNumber" ;
		public const string BILL_ITEM_ENTRY_BATCH_NUMBER = "EntryBatchNumber" ;
		public const string BILL_ITEM_ENTRY_BATCH_TYPE = "EntryBatchType" ;
		public const string BILL_ITEM_FINAL_BATCH_NUMBER = "FinalBatchNumber" ;
		public const string BILL_ITEM_FINAL_BATCH_TYPE = "FinalBatchType" ;
		public const string BILL_ITEM_COMMENT = "Comment" ;
		public const string BILL_ITEM_STATUS = "Status" ;

        public const string CODE_ID_ATTRIBUTE_NAME = "codeid";
		public const string SHORT_CODE_ATTRIBUTE_NAME = "ShortCode" ;
		public const string CODE_DESC_ATTRIBUTE_NAME = "CodeDesc" ;
	
		public const string FILTER_STATUS = "FilterStatus" ;
		public const string FILTER_TERM_NUMBER = "FilterTermNumber" ;
		public const string FILTER_FORM_DATE = "FilterFromDate" ;
		public const string FILTER_TO_DATE = "FilterToDate" ;



		public PolBillingConstants()
		{			
		}

       

	}
}
