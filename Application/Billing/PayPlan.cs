using System;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: PayPlan.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	/// <summary>
	/// TODO
	/// </summary>
	public class PayPlan
	{
		#region Member Variables
		/// <summary>
		/// Private variable for DttmRcdLastUpd.
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty ;
		/// <summary>
		/// Private variable for m_sUpdatedByUser.
		/// </summary>
		private string m_sUpdatedByUser = string.Empty ;
		/// <summary>
		/// Private variable for m_sDttmRcdAdded.
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty ;
		/// <summary>
		/// Private variable for m_sAddedByUser.
		/// </summary>
		private string m_sAddedByUser = string.Empty ;
		/// <summary>
		/// Private variable for m_iPayPlanRowid.
		/// </summary>
		private int m_iPayPlanRowid = 0 ;
		/// <summary>
		/// Private variable for m_iPayPlanCode.
		/// </summary>
		private int m_iPayPlanCode = 0 ;
		/// <summary>
		/// Private variable for m_iBillingRuleRowid.
		/// </summary>
		private int m_iBillingRuleRowid = 0 ;
		/// <summary>
		/// Private variable for m_iInUseFlag.
		/// </summary>
		private int m_iInUseFlag = 0 ;
		/// <summary>
		/// Private variable for m_iLineOfBusiness.
		/// </summary>
		private int m_iLineOfBusiness = 0 ;
		/// <summary>
		/// Private variable for m_iState.
		/// </summary>
		private int m_iState = 0 ;
		/// <summary>
		/// Private variable for m_iInstallGenType.
		/// </summary>
		private int m_iInstallGenType = 0 ;
		/// <summary>
		/// Private variable for m_iFixedFrequency.
		/// </summary>
		private int m_iFixedFrequency = 0 ;
		/// <summary>
		/// Private variable for m_iFixedGenDay.
		/// </summary>
		private int m_iFixedGenDay = 0 ;
		/// <summary>
		/// Private variable for m_iFixedDueDays.
		/// </summary>
		private int m_iFixedDueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall1Amount.
		/// </summary>
		private double m_dblInstall1Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall1Type.
		/// </summary>
		private int m_iInstall1Type = 0 ;
		/// <summary>
		/// Private variable for DttmRcdLastUpd.
		/// </summary>
		private int m_iInstall1LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall1DueDays.
		/// </summary>
		private int m_iInstall1DueDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall1DueDays.
		/// </summary>
		private double m_dblInstall2Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall2Type.
		/// </summary>
		private int m_iInstall2Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall2LagDays.
		/// </summary>
		private int m_iInstall2LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall2DueDays.
		/// </summary>
		private int m_iInstall2DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall3Amount.
		/// </summary>
		private double m_dblInstall3Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall3Type.
		/// </summary>
		private int m_iInstall3Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall3LagDays.
		/// </summary>
		private int m_iInstall3LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall3DueDays.
		/// </summary>
		private int m_iInstall3DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall4Amount.
		/// </summary>
		private double m_dblInstall4Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall4Type.
		/// </summary>
		private int m_iInstall4Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall4LagDays.
		/// </summary>
		private int m_iInstall4LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall4DueDays.
		/// </summary>
		private int m_iInstall4DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall5Amount.
		/// </summary>
		private double m_dblInstall5Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall5Type.
		/// </summary>
		private int m_iInstall5Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall5LagDays.
		/// </summary>
		private int m_iInstall5LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall5DueDays.
		/// </summary>
		private int m_iInstall5DueDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall6Amount.
		/// </summary>
		private double m_dblInstall6Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall6Type.
		/// </summary>
		private int m_iInstall6Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall6LagDays.
		/// </summary>
		private int m_iInstall6LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall6DueDays.
		/// </summary>
		private int m_iInstall6DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall7Amount.
		/// </summary>
		private double m_dblInstall7Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall7Type.
		/// </summary>
		private int m_iInstall7Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall7LagDays.
		/// </summary>
		private int m_iInstall7LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall7DueDays.
		/// </summary>
		private int m_iInstall7DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall8Amount.
		/// </summary>
		private double m_dblInstall8Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall8Type.
		/// </summary>
		private int m_iInstall8Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall8LagDays.
		/// </summary>
		private int m_iInstall8LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall8DueDays.
		/// </summary>
		private int m_iInstall8DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall9Amount.
		/// </summary>
		private double m_dblInstall9Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall9Type.
		/// </summary>
		private int m_iInstall9Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall9LagDays.
		/// </summary>
		private int m_iInstall9LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall9DueDays.
		/// </summary>
		private int m_iInstall9DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall10Amount.
		/// </summary>
		private double m_dblInstall10Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall10Type.
		/// </summary>
		private int m_iInstall10Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall10LagDays.
		/// </summary>
		private int m_iInstall10LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall10DueDays.
		/// </summary>
		private int m_iInstall10DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall11Amount.
		/// </summary>
		private double m_dblInstall11Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall11Type.
		/// </summary>
		private int m_iInstall11Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall11LagDays.
		/// </summary>
		private int m_iInstall11LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall11DueDays.
		/// </summary>
		private int m_iInstall11DueDays = 0 ;
		/// <summary>
		/// Private variable for m_dblInstall12Amount.
		/// </summary>
		private double m_dblInstall12Amount = 0.0 ;
		/// <summary>
		/// Private variable for m_iInstall12Type.
		/// </summary>
		private int m_iInstall12Type = 0 ;
		/// <summary>
		/// Private variable for m_iInstall12LagDays.
		/// </summary>
		private int m_iInstall12LagDays = 0 ;
		/// <summary>
		/// Private variable for m_iInstall12DueDays.
		/// </summary>
		private int m_iInstall12DueDays = 0 ;
		/// <summary>
		/// Private variable for m_iExtraAmtsIndic.
		/// </summary>
		private int m_iExtraAmtsIndic = 0 ;
		/// <summary>
		/// Private variable for m_iLateStartIndic.
		/// </summary>
		private int m_iLateStartIndic = 0 ;
		/// <summary>
		/// Private variable for m_iOffsetIndic.
		/// </summary>
		private int m_iOffsetIndic = 0 ;
		/// <summary>
		/// Private variable for DttmRcdLastUpd.
		/// </summary>
		private bool m_bNewRecord = false ;
		/// <summary>
		/// Private variable for m_bDataChanged.
		/// </summary>
		private bool m_bDataChanged = false ;

        //npadhy RMSC retrofit Starts
        private string m_sFixedGenDate = string.Empty;
        private string m_sFixedDueDate = string.Empty;

        private string m_sInstall1GenDate=string.Empty;
        private string m_sInstall1DueDate = string.Empty;

        private string m_sInstall2GenDate = string.Empty;
        private string m_sInstall2DueDate = string.Empty;

        private string m_sInstall3GenDate = string.Empty;
        private string m_sInstall3DueDate = string.Empty;

        private string m_sInstall4GenDate = string.Empty;
        private string m_sInstall4DueDate = string.Empty;

        private string m_sInstall5GenDate = string.Empty;
        private string m_sInstall5DueDate = string.Empty;

        private string m_sInstall6GenDate = string.Empty;
        private string m_sInstall6DueDate = string.Empty;

        private string m_sInstall7GenDate = string.Empty;
        private string m_sInstall7DueDate = string.Empty;

        private string m_sInstall8GenDate = string.Empty;
        private string m_sInstall8DueDate = string.Empty;

        private string m_sInstall9GenDate = string.Empty;
        private string m_sInstall9DueDate = string.Empty;

        private string m_sInstall10GenDate = string.Empty;
        private string m_sInstall10DueDate = string.Empty;

        private string m_sInstall11GenDate = string.Empty;
        private string m_sInstall11DueDate = string.Empty;

        private string m_sInstall12GenDate = string.Empty;
        private string m_sInstall12DueDate = string.Empty;

        //npadhy RMSC retrofit Ends
		#endregion 
		
		#region Member Properties
		/// <summary>
		/// Read/Write property for DttmRcdLastUpd.
		/// </summary>
		public string DttmRcdLastUpd 
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value ;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser.
		/// </summary>
		public string UpdatedByUser 
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value ;
			}
		}
		/// <summary>
		/// Read/Write property for DttmRcdAdded.
		/// </summary>
		public string DttmRcdAdded 
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value ;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser.
		/// </summary>
		public string AddedByUser 
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value ;
			}
		}

		/// <summary>
		/// Read/Write property for PayPlanRowid.
		/// </summary>
		public int PayPlanRowid 
		{
			get
			{
				return m_iPayPlanRowid;
			}
			set
			{
				m_iPayPlanRowid = value ;
			}
		}
		/// <summary>
		/// Read/Write property for PayPlanCode.
		/// </summary>
		public int PayPlanCode 
		{
			get
			{
				return m_iPayPlanCode;
			}
			set
			{
				m_iPayPlanCode = value ;
			}
		}
		/// <summary>
		/// Read/Write property for BillingRuleRowid.
		/// </summary>
		public int BillingRuleRowid 
		{
			get
			{
				return m_iBillingRuleRowid;
			}
			set
			{
				m_iBillingRuleRowid = value ;
			}
		}
		/// <summary>
		/// Read/Write property for InUseFlag.
		/// </summary>
		public int InUseFlag 
		{
			get
			{
				return m_iInUseFlag;
			}
			set
			{
				m_iInUseFlag = value ;
			}
		}
		/// <summary>
		/// Read/Write property for LineOfBusiness.
		/// </summary>
		public int LineOfBusiness 
		{
			get
			{
				return m_iLineOfBusiness;
			}
			set
			{
				m_iLineOfBusiness = value ;
			}
		}
		/// <summary>
		/// Read/Write property for State.
		/// </summary>
		public int State 
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value ;
			}
		}
		/// <summary>
		/// Read/Write property for InstallGenType.
		/// </summary>
		public int InstallGenType 
		{
			get
			{
				return m_iInstallGenType;
			}
			set
			{
				m_iInstallGenType = value ;
			}
		}
		/// <summary>
		/// Read/Write property for FixedFrequency.
		/// </summary>
		public int FixedFrequency 
		{
			get
			{
				return m_iFixedFrequency;
			}
			set
			{
				m_iFixedFrequency = value ;
			}
		}
		/// <summary>
		/// Read/Write property for FixedGenDay.
		/// </summary>
		public int FixedGenDay 
		{
			get
			{
				return m_iFixedGenDay;
			}
			set
			{
				m_iFixedGenDay = value ;
			}
		}
		/// <summary>
		/// Read/Write property for FixedDueDays.
		/// </summary>
		public int FixedDueDays 
		{
			get
			{
				return m_iFixedDueDays;
			}
			set
			{
				m_iFixedDueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install1Amount.
		/// </summary>
		public double Install1Amount 
		{
			get
			{
				return m_dblInstall1Amount;
			}
			set
			{
				m_dblInstall1Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install1Type.
		/// </summary>
		public int Install1Type 
		{
			get
			{
				return m_iInstall1Type;
			}
			set
			{
				m_iInstall1Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install1LagDays.
		/// </summary>
		public int Install1LagDays 
		{
			get
			{
				return m_iInstall1LagDays;
			}
			set
			{
				m_iInstall1LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install1DueDays.
		/// </summary>
		public int Install1DueDays 
		{
			get
			{
				return m_iInstall1DueDays;
			}
			set
			{
				m_iInstall1DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install2Amount.
		/// </summary>
		public double Install2Amount 
		{
			get
			{
				return m_dblInstall2Amount;
			}
			set
			{
				m_dblInstall2Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install2Type.
		/// </summary>
		public int Install2Type 
		{
			get
			{
				return m_iInstall2Type;
			}
			set
			{
				m_iInstall2Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install2LagDays.
		/// </summary>
		public int Install2LagDays 
		{
			get
			{
				return m_iInstall2LagDays;
			}
			set
			{
				m_iInstall2LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install2DueDays.
		/// </summary>
		public int Install2DueDays 
		{
			get
			{
				return m_iInstall2DueDays;
			}
			set
			{
				m_iInstall2DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install3Amount.
		/// </summary>
		public double Install3Amount 
		{
			get
			{
				return m_dblInstall3Amount;
			}
			set
			{
				m_dblInstall3Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install3Type.
		/// </summary>
		public int Install3Type 
		{
			get
			{
				return m_iInstall3Type;
			}
			set
			{
				m_iInstall3Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install3LagDays.
		/// </summary>
		public int Install3LagDays 
		{
			get
			{
				return m_iInstall3LagDays;
			}
			set
			{
				m_iInstall3LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install3DueDays.
		/// </summary>
		public int Install3DueDays 
		{
			get
			{
				return m_iInstall3DueDays;
			}
			set
			{
				m_iInstall3DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install4Amount.
		/// </summary>
		public double Install4Amount 
		{
			get
			{
				return m_dblInstall4Amount;
			}
			set
			{
				m_dblInstall4Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install4Type.
		/// </summary>
		public int Install4Type 
		{
			get
			{
				return m_iInstall4Type;
			}
			set
			{
				m_iInstall4Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install4LagDays.
		/// </summary>
		public int Install4LagDays 
		{
			get
			{
				return m_iInstall4LagDays;
			}
			set
			{
				m_iInstall4LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install4DueDays.
		/// </summary>
		public int Install4DueDays 
		{
			get
			{
				return m_iInstall4DueDays;
			}
			set
			{
				m_iInstall4DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install5Amount.
		/// </summary>
		public double Install5Amount 
		{
			get
			{
				return m_dblInstall5Amount;
			}
			set
			{
				m_dblInstall5Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install5Type.
		/// </summary>
		public int Install5Type 
		{
			get
			{
				return m_iInstall5Type;
			}
			set
			{
				m_iInstall5Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install5LagDays.
		/// </summary>
		public int Install5LagDays 
		{
			get
			{
				return m_iInstall5LagDays;
			}
			set
			{
				m_iInstall5LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install5DueDays.
		/// </summary>
		public int Install5DueDays 
		{
			get
			{
				return m_iInstall5DueDays;
			}
			set
			{
				m_iInstall5DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install6Amount.
		/// </summary>
		public double Install6Amount 
		{
			get
			{
				return m_dblInstall6Amount;
			}
			set
			{
				m_dblInstall6Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install6Type.
		/// </summary>
		public int Install6Type 
		{
			get
			{
				return m_iInstall6Type;
			}
			set
			{
				m_iInstall6Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install6LagDays.
		/// </summary>
		public int Install6LagDays 
		{
			get
			{
				return m_iInstall6LagDays;
			}
			set
			{
				m_iInstall6LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install6DueDays.
		/// </summary>
		public int Install6DueDays 
		{
			get
			{
				return m_iInstall6DueDays;
			}
			set
			{
				m_iInstall6DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install7Amount.
		/// </summary>
		public double Install7Amount 
		{
			get
			{
				return m_dblInstall7Amount;
			}
			set
			{
				m_dblInstall7Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install7Type.
		/// </summary>
		public int Install7Type 
		{
			get
			{
				return m_iInstall7Type;
			}
			set
			{
				m_iInstall7Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install7LagDays.
		/// </summary>
		public int Install7LagDays 
		{
			get
			{
				return m_iInstall7LagDays;
			}
			set
			{
				m_iInstall7LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install7DueDays.
		/// </summary>
		public int Install7DueDays 
		{
			get
			{
				return m_iInstall7DueDays;
			}
			set
			{
				m_iInstall7DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install8Amount.
		/// </summary>
		public double Install8Amount 
		{
			get
			{
				return m_dblInstall8Amount;
			}
			set
			{
				m_dblInstall8Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install8Type.
		/// </summary>
		public int Install8Type 
		{
			get
			{
				return m_iInstall8Type;
			}
			set
			{
				m_iInstall8Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install8LagDays.
		/// </summary>
		public int Install8LagDays 
		{
			get
			{
				return m_iInstall8LagDays;
			}
			set
			{
				m_iInstall8LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install8DueDays.
		/// </summary>
		public int Install8DueDays 
		{
			get
			{
				return m_iInstall8DueDays;
			}
			set
			{
				m_iInstall8DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install9Amount.
		/// </summary>
		public double Install9Amount 
		{
			get
			{
				return m_dblInstall9Amount;
			}
			set
			{
				m_dblInstall9Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install9Type.
		/// </summary>
		public int Install9Type 
		{
			get
			{
				return m_iInstall9Type;
			}
			set
			{
				m_iInstall9Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install9LagDays.
		/// </summary>
		public int Install9LagDays 
		{
			get
			{
				return m_iInstall9LagDays;
			}
			set
			{
				m_iInstall9LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install9DueDays.
		/// </summary>
		public int Install9DueDays 
		{
			get
			{
				return m_iInstall9DueDays;
			}
			set
			{
				m_iInstall9DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install10Amount.
		/// </summary>
		public double Install10Amount 
		{
			get
			{
				return m_dblInstall10Amount;
			}
			set
			{
				m_dblInstall10Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install10Type.
		/// </summary>
		public int Install10Type 
		{
			get
			{
				return m_iInstall10Type;
			}
			set
			{
				m_iInstall10Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install10LagDays.
		/// </summary>
		public int Install10LagDays 
		{
			get
			{
				return m_iInstall10LagDays;
			}
			set
			{
				m_iInstall10LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install10DueDays.
		/// </summary>
		public int Install10DueDays 
		{
			get
			{
				return m_iInstall10DueDays;
			}
			set
			{
				m_iInstall10DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install11Amount.
		/// </summary>
		public double Install11Amount 
		{
			get
			{
				return m_dblInstall11Amount;
			}
			set
			{
				m_dblInstall11Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install11Type.
		/// </summary>
		public int Install11Type 
		{
			get
			{
				return m_iInstall11Type;
			}
			set
			{
				m_iInstall11Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install11LagDays.
		/// </summary>
		public int Install11LagDays 
		{
			get
			{
				return m_iInstall11LagDays;
			}
			set
			{
				m_iInstall11LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install11DueDays.
		/// </summary>
		public int Install11DueDays 
		{
			get
			{
				return m_iInstall11DueDays;
			}
			set
			{
				m_iInstall11DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install12Amount.
		/// </summary>
		public double Install12Amount 
		{
			get
			{
				return m_dblInstall12Amount;
			}
			set
			{
				m_dblInstall12Amount = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install12Type.
		/// </summary>
		public int Install12Type 
		{
			get
			{
				return m_iInstall12Type;
			}
			set
			{
				m_iInstall12Type = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install12LagDays.
		/// </summary>
		public int Install12LagDays 
		{
			get
			{
				return m_iInstall12LagDays;
			}
			set
			{
				m_iInstall12LagDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for Install12DueDays.
		/// </summary>
		public int Install12DueDays 
		{
			get
			{
				return m_iInstall12DueDays;
			}
			set
			{
				m_iInstall12DueDays = value ;
			}
		}
		/// <summary>
		/// Read/Write property for ExtraAmtsIndic.
		/// </summary>
		public int ExtraAmtsIndic 
		{
			get
			{
				return m_iExtraAmtsIndic;
			}
			set
			{
				m_iExtraAmtsIndic = value ;
			}
		}
		/// <summary>
		/// Read/Write property for LateStartIndic.
		/// </summary>
		public int LateStartIndic 
		{
			get
			{
				return m_iLateStartIndic;
			}
			set
			{
				m_iLateStartIndic = value ;
			}
		}
		/// <summary>
		/// Read/Write property for OffsetIndic.
		/// </summary>
		public int OffsetIndic 
		{
			get
			{
				return m_iOffsetIndic;
			}
			set
			{
				m_iOffsetIndic = value ;
			}
		}
		/// <summary>
		/// Read/Write property for NewRecord.
		/// </summary>
		public bool NewRecord 
		{
			get
			{
				return m_bNewRecord;
			}
			set
			{
				m_bNewRecord = value ;
			}
		}
		/// <summary>
		/// Read/Write property for DataChanged.
		/// </summary>
		public bool DataChanged 
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value ;
			}
		}
        //npadhy RMSC retrofit Starts
        public string FixedGenDate
        {
            get
            {
                return m_sFixedGenDate; 
            }
            set
            {
                m_sFixedGenDate = value;
            }
        }
        public string FixedDueDate
        {
            get
            {
                return m_sFixedDueDate;
            }
            set
            {
                m_sFixedDueDate = value;
            }
        }
        public string Install1GenDate
        {
            get
            {
                return m_sInstall1GenDate; 
            }
            set
            {
                m_sInstall1GenDate = value;
            }
        }
        public string Install1DueDate
        {
            get
            {
                return m_sInstall1DueDate;
            }
            set
            {
                m_sInstall1DueDate = value;
            }
        }
        public string Install2GenDate
        {
            get
            {
                return m_sInstall2GenDate;
            }
            set
            {
                m_sInstall2GenDate = value;
            }
        }
        public string Install2DueDate
        {
            get
            {
                return m_sInstall2DueDate;
            }
            set
            {
                m_sInstall2DueDate = value;
            }
        }
        public string Install3GenDate
        {
            get
            {
                return m_sInstall3GenDate;
            }
            set
            {
                m_sInstall3GenDate = value;
            }
        }
        public string Install3DueDate
        {
            get
            {
                return m_sInstall3DueDate;
            }
            set
            {
                m_sInstall3DueDate = value;
            }
        }
        public string Install4GenDate
        {
            get
            {
                return m_sInstall4GenDate;
            }
            set
            {
                m_sInstall4GenDate = value;
            }
        }
        public string Install4DueDate
        {
            get
            {
                return m_sInstall4DueDate;
            }
            set
            {
                m_sInstall4DueDate = value;
            }
        }
        public string Install5GenDate
        {
            get
            {
                return m_sInstall5GenDate;
            }
            set
            {
                m_sInstall5GenDate = value;
            }
        }
        public string Install5DueDate
        {
            get
            {
                return m_sInstall5DueDate;
            }
            set
            {
                m_sInstall5DueDate = value;
            }
        }
        public string Install6GenDate
        {
            get
            {
                return m_sInstall6GenDate;
            }
            set
            {
                m_sInstall6GenDate = value;
            }
        }
        public string Install6DueDate
        {
            get
            {
                return m_sInstall6DueDate;
            }
            set
            {
                m_sInstall6DueDate = value;
            }
        }
        public string Install7GenDate
        {
            get
            {
                return m_sInstall7GenDate;
            }
            set
            {
                m_sInstall7GenDate = value;
            }
        }
        public string Install7DueDate
        {
            get
            {
                return m_sInstall7DueDate;
            }
            set
            {
                m_sInstall7DueDate = value;
            }
        }
        public string Install8GenDate
        {
            get
            {
                return m_sInstall8GenDate;
            }
            set
            {
                m_sInstall8GenDate = value;
            }
        }
        public string Install8DueDate
        {
            get
            {
                return m_sInstall8DueDate;
            }
            set
            {
                m_sInstall8DueDate = value;
            }
        }
        public string Install9GenDate
        {
            get
            {
                return m_sInstall9GenDate;
            }
            set
            {
                m_sInstall9GenDate = value;
            }
        }
        public string Install9DueDate
        {
            get
            {
                return m_sInstall9DueDate;
            }
            set
            {
                m_sInstall9DueDate = value;
            }
        }
        public string Install10GenDate
        {
            get
            {
                return m_sInstall10GenDate;
            }
            set
            {
                m_sInstall10GenDate = value;
            }
        }
        public string Install10DueDate
        {
            get
            {
                return m_sInstall10DueDate;
            }
            set
            {
                m_sInstall10DueDate = value;
            }
        }
        public string Install11GenDate
        {
            get
            {
                return m_sInstall11GenDate;
            }
            set
            {
                m_sInstall11GenDate = value;
            }
        }
        public string Install11DueDate
        {
            get
            {
                return m_sInstall11DueDate;
            }
            set
            {
                m_sInstall11DueDate = value;
            }
        }
        public string Install12GenDate
        {
            get
            {
                return m_sInstall12GenDate;
            }
            set
            {
                m_sInstall12GenDate = value;
            }
        }
        public string Install12DueDate
        {
            get
            {
                return m_sInstall12DueDate;
            }
            set
            {
                m_sInstall12DueDate = value;
            }
        }
        //npadhy RMSC retrofit Ends

		#endregion 

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value.
		/// </summary>
		public PayPlan()
		{			
		}
		#endregion 
	}
}
