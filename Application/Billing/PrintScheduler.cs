using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /**************************************************************
	 * $File		: PrintScheduler.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Gagan Bhatnagar
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
    public class PrintScheduler
    {

        #region "Member Variables"

        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;

        /// <summary>
        /// Private variable to track Invoice serial no.
        /// </summary>
        private int m_iInvoiceCount = 0;

        /// <summary>
        /// Private variable to track Notice serial number
        /// </summary>
        private int m_iNoticeCount = 0;

        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDSN_Name = "";

        private int m_iClientId = 0;//psharma206

        # endregion


		#region Constructor

		/// <summary>
		/// Constructor, initializes the variables to the default value .
		/// </summary>		
        public PrintScheduler()
		{			
		}

        /// <summary>
        /// Constructor, initializes the variables to the default value .
        /// </summary>		             
        public PrintScheduler(string p_sDSN_Name, int p_iClientId)
        {
            m_sDSN_Name = p_sDSN_Name;
            m_iClientId = p_iClientId;//psharma206
        }

        # endregion


        # region Constants

        private const string ROOT_NODE_NAME = "BillingScheduler";
        private const string NOTICES_NODE_NAME = "Notices";
        private const string NOTICE_NODE_NAME = "Notice";
        private const string INVOICES_NODE_NAME = "Invoices";
        private const string INVOICE_NODE_NAME = "Invoice";
        private const string FILENAME_NODE_NAME = "Filename";
        private const string DATE_NODE_NAME = "Date";
        private const string TIME_NODE_NAME = "Time";
        private const string S_NO_NAME = "S_NO";

        # endregion


        #region "Public Methods"

        /// <summary>
        /// GetInvoicesandNoticesList
        /// </summary>
        /// <returns>Xml contianing list of invoices and notices</returns>
        public XmlDocument GetInvoicesandNoticesList()
        {
            XmlElement objRootNode = null;
            XmlElement objNoticesNode = null;
            XmlElement objInvoicesNode = null;
            DirectoryInfo objFolder = null;
            string[] strArray;
            string strFileName;
            string strFileType;
            string strDate;
            string strTime;
            //Changed by Gagan for MITS 18499 : start
            string sDSN_Name;
            //Changed by Gagan for MITS 18499 : End

            try
            {               
                Function.StartDocument(ref m_objDocument, ref objRootNode, ROOT_NODE_NAME, m_iClientId);
                Function.CreateElement(objRootNode, NOTICES_NODE_NAME, ref objNoticesNode, m_iClientId);
                Function.CreateElement(objRootNode, INVOICES_NODE_NAME, ref objInvoicesNode, m_iClientId);                

                objFolder = new DirectoryInfo(Function.GetSavePath());

                if (objFolder.Exists)
                {
                    foreach (FileInfo objFileInfo in objFolder.GetFiles())
                    {
                        strFileName = objFileInfo.Name;
                        strArray = objFileInfo.Name.Split('-');
                        //Changed by Gagan for MITS 18499 : start
                        sDSN_Name = "";
                        //Changed by Gagan for MITS 18499 : end

                        //Changed by Gagan for MITS 18499 : start
                        if (strArray.Length != 3 && strArray.Length !=4) 
                            continue;

                        //Skip all files that are not invoices and notices pdf
                        if (strArray[0].Trim() != "Invoices" && strArray[0].Trim() != "Notices")
                            continue;
                        

                        strFileType = strArray[0].Trim();
                        if (strArray.Length == 4)
                        {
                            sDSN_Name = strArray[3].Trim();
                            sDSN_Name = sDSN_Name.Substring(0, sDSN_Name.LastIndexOf("."));
                            sDSN_Name = sDSN_Name.Trim();
                        }
                        //Changed by Gagan for MITS 18499 : end

                        strDate =  strArray[1].Trim();
                        strDate = strDate.Substring(4,2) + "/" + strDate.Substring(6,2) +
                                   "/" + strDate.Substring(0,4);                         

                        strTime = strArray[2].Substring(1, 6);
                        strTime = strTime.Substring(0, 2) + ":" + strTime.Substring(2, 2) + 
                                  ":" + strTime.Substring(4, 2);
                        //Changed by Gagan for MITS 18499 : start
                        if (sDSN_Name == "" || m_sDSN_Name == sDSN_Name)
                        {
                            if (strFileType == "Notices")
                                CreateNoticesXml(objNoticesNode, strFileName, strDate, strTime);

                            else if (strFileType == "Invoices")
                                CreateInvoicesXml(objInvoicesNode, strFileName, strDate, strTime);
                        }
                        //Changed by Gagan for MITS 18499 : end
                    }
                }            
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintScheduler.GetInvoicesandNoticesList.Error", m_iClientId), p_objEx);//psharma206
            }
            finally
            {
                objRootNode = null;
                objNoticesNode = null;
                objInvoicesNode = null;
                objFolder = null;
            }

            return (m_objDocument);
        }


        /// <summary>        
        /// Creates Xml for Notices
        /// </summary>            
        /// <param name="p_objParentNode">Xml node</param>
        /// <param name="strFileName">File Name</param>
        /// <param name="strDate">Date</param>
        /// <param name="strTime">Time</param>
        private void CreateNoticesXml(XmlElement p_objParentNode, string strFileName, string strDate,
                                      string strTime)
        {   
            XmlElement objNoticeNode = null;

            try
            {
                m_iNoticeCount = m_iNoticeCount + 1;

                Function.CreateElement(p_objParentNode, NOTICE_NODE_NAME, ref objNoticeNode, m_iClientId);

                Function.CreateAndSetElement(objNoticeNode, S_NO_NAME, m_iNoticeCount.ToString(), m_iClientId);
                Function.CreateAndSetElement(objNoticeNode, FILENAME_NODE_NAME, strFileName, m_iClientId);
                Function.CreateAndSetElement(objNoticeNode, DATE_NODE_NAME, strDate, m_iClientId);
                Function.CreateAndSetElement(objNoticeNode, TIME_NODE_NAME, strTime, m_iClientId);
                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {                
                throw new RMAppException(Globalization.GetString("PrintScheduler.CreateNoticesXml.Error", m_iClientId), p_objEx);//psharma206
            }
            finally
            {
                objNoticeNode = null;
            }
        }



        /// <summary>        
        /// Creates Xml for Invoices
        /// </summary>            
        /// <param name="p_objParentNode">Xml node</param>
        /// <param name="strFileName">File Name</param>
        /// <param name="strDate">Date</param>
        /// <param name="strTime">Time</param>
        private void CreateInvoicesXml(XmlElement p_objParentNode, string strFileName, string strDate,
                                      string strTime)
        {
            XmlElement objInvoiceNode = null;

            try
            {
                m_iInvoiceCount = m_iInvoiceCount + 1;
                Function.CreateElement(p_objParentNode, INVOICE_NODE_NAME, ref objInvoiceNode, m_iClientId);

                Function.CreateAndSetElement(objInvoiceNode, S_NO_NAME, m_iInvoiceCount.ToString(), m_iClientId);
                Function.CreateAndSetElement(objInvoiceNode, FILENAME_NODE_NAME, strFileName, m_iClientId);
                Function.CreateAndSetElement(objInvoiceNode, DATE_NODE_NAME, strDate, m_iClientId);
                Function.CreateAndSetElement(objInvoiceNode, TIME_NODE_NAME, strTime, m_iClientId);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintScheduler.CreateInvoicesXml.Error", m_iClientId), p_objEx);//psharma206
            }
            finally
            {
                objInvoiceNode = null;
            }
        }




        #endregion 

    }
}
