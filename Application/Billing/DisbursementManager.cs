﻿
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;

using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using Riskmaster.Application.FundManagement;
namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /**************************************************************
     * $File		: DisbursementManager.cs
     * $Revision	: 1.0.0.0
     * $Date		: 
     * $Author		: Nitesh Deedwania
     * $Comment		:  
     * $Source		:  	
    **************************************************************/

    public class DisbursementManager : IDisposable
    {
        #region Member Variables
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Private variable to store the instance of Local Cache object
        /// </summary>
        private LocalCache m_objLocalCache = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;
        /// <summary>
        /// ClientID for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;

        private const string DISBURSEMENT_NODE_NAME = "Disbursements";
        private const string DISBURSEMENT_PAYMENT_METHOD = "PaymentMethod";
        private const string DISBURSEMENT_CHECK_NUMBER = "CheckNumber";
        private const string DISBURSEMENT_DATE_PRINTED = "DatePrinted";
        private const string DISBURSEMENT_PRINTED_FLAG = "PrintedFlag";
        private const string DISBURSEMENT_PAYEE = "Payee";
        private const string DISBURSEMENT_PAYEE_NAME = "PayeeName";
        private const string DISBURSEMENT_TRANSACTION_TYPE = "TransactionType";
        private const string DISBURSEMENT_BANK_ACCOUNT = "BankAccount";
        private const string DISBURSEMENT_AMOUNT = "Amount";
        private const string DISBURSEMENT_DISABLEALL = "DisableControls";


        #endregion
        #region Constructor
        /// <summary>
        /// Constructor, initializes the variables to the default value .
        /// </summary>
        /// <param name="p_objDataModelFactory">DataModelFactory Object</param>
        public DisbursementManager(DataModelFactory p_objDataModelFactory)
        {
            m_objDataModelFactory = p_objDataModelFactory;
            m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
            m_iClientId = m_objDataModelFactory.Context.ClientId; //Ash - cloud
        }
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>	

        public DisbursementManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId; //Ash - cloud
            m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);            
        }
        #endregion

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }

        public XmlDocument GetDisbursementData(int p_iBillItemRowId)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootNode = null;

            try
            {
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                if (p_iBillItemRowId != 0)
                    objBillXBillItem.MoveTo(p_iBillItemRowId);
                else
                    throw new RMAppException("DisbursementManager.GetDisbursementData.NoBillingSelected");

                Function.StartDocument(ref m_objDocument, ref objRootNode, DISBURSEMENT_NODE_NAME, m_iClientId);
                this.CreateDisbursementItemXml(objRootNode, objBillXBillItem);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DisbursementManager.GetDisbursementData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objRootNode = null;
            }
            return (m_objDocument);
        }

        public XmlDocument GetNewDisbursementData(int p_PolicyId, int p_iTermNumber, string p_sPolicyNumber)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootNode = null;

            try
            {
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                if (p_PolicyId != 0)
                {
                    objBillXBillItem.PolicyId = p_PolicyId;
                    objBillXBillItem.PolicyNumber = p_sPolicyNumber;
                    objBillXBillItem.TermNumber = p_iTermNumber;
                    objBillXBillItem.BillingItemType = m_objLocalCache.GetCodeId("D", "BILLING_ITEM_TYPES");
                }
                else
                    throw new RMAppException("DisbursementManager.GetNewDisbursementData.NoPolicySelect");

                Function.StartDocument(ref m_objDocument, ref objRootNode, DISBURSEMENT_NODE_NAME, m_iClientId);
                this.CreateDisbursementItemXml(objRootNode, objBillXBillItem);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DisbursementManager.GetNewDisbursementData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objRootNode = null;
            }
            return (m_objDocument);
        }
        private void AppendBankAccountList(XmlElement p_objParentNode)
        {
            //Variable declarations
            ArrayList arrAcctList = new ArrayList();
            XmlDocument objXML = p_objParentNode.OwnerDocument ;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;
            try
            {
                int iUserId = m_objDataModelFactory.Context.RMUser.UserId;
                int iGroupId = m_objDataModelFactory.Context.RMUser.GroupId;
                bool bUseFundsSubAcc = m_objDataModelFactory.Context.InternalSettings.SysSettings.UseFundsSubAcc;

                //Create and return the instance of the FundManager object
                using (FundManager FundManagerObject = new FundManager(m_sDsnName, m_sUserName, m_sPassword, iUserId, iGroupId, m_iClientId))
                {
                    //Retrieve the complete Bank Account List and store it in the ArrayList
                    arrAcctList = FundManagerObject.GetAccounts(bUseFundsSubAcc);
                }

                //Create the necessary Parent to be used in ref binding
                objOld = objXML.SelectSingleNode("//AccountList");
                objNew = objXML.CreateElement("AccountList");

                XmlElement xmlOption = null;
                XmlAttribute xmlOptionAttrib = null;

                // Blank value for the combo box
                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;

                //Loop through and create all the option values for the combobox control
                foreach (FundManager.AccountDetail item in arrAcctList)
                {
                    xmlOption = objXML.CreateElement("option");
                    objCData = objXML.CreateCDataSection(item.AccountName);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXML.CreateAttribute("value");

                    if (bUseFundsSubAcc)
                        xmlOptionAttrib.Value = item.SubRowId.ToString();
                    else
                        xmlOptionAttrib.Value = item.AccountId.ToString();

                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                }//end foreach

                //Add the XML to the ParentNode 
                if (objOld != null)
                    objXML.DocumentElement.ReplaceChild(objNew, objOld);
                else
                    objXML.DocumentElement.AppendChild(objNew);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DisbursementManager.AppendBankAccountList.Erro", m_iClientId), p_objEx);
            }
            finally
            {
                //Clean up
                arrAcctList = null;
                objOld = null;
                objNew = null;
                objCData = null;
                objXML = null;
            }
            
        }

        private void CreateDisbursementItemXml(XmlElement p_objParentNode, BillXBillItem p_objBillXBillItem)
        {
            SysSettings objSysSettings = null;
            XmlElement objNew = null;
            XmlDocument objXML = p_objParentNode.OwnerDocument;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            XmlCDataSection objCData = null;

            //Manish Multicurrency
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString, m_iClientId);

            objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), ref sShortCode, ref sDesc);
            sCurrency = sDesc.Split('|')[1];
            try
            {
                objSysSettings = new SysSettings(m_sConnectionString,m_iClientId); //Ash - cloud
                //Function.CreateNodeWithCodeValues(p_objParentNode, DISBURSEMENT_PAYMENT_METHOD, p_objBillXBillItem.BillXDsbrsmnt.DisbursementType, m_sConnectionString);
                Function.CreateAndSetElement(p_objParentNode, "BaseCurrencyType", sCurrency, m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_PAYMENT_METHOD, Conversion.ConvertObjToStr(p_objBillXBillItem.BillXDsbrsmnt.DisbursementType), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_CHECK_NUMBER, p_objBillXBillItem.BillXDsbrsmnt.CheckNumber.ToString(), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_DATE_PRINTED, Conversion.GetDBDateFormat(p_objBillXBillItem.BillXDsbrsmnt.DatePrinted, "d"), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_PRINTED_FLAG, p_objBillXBillItem.BillXDsbrsmnt.PrintedFlag.ToString(), m_iClientId);
                if (p_objBillXBillItem.BillXDsbrsmnt.Payee == 0)
                    p_objBillXBillItem.BillXDsbrsmnt.Payee = Function.GetBillToPayeeEid(p_objBillXBillItem.PolicyId, p_objBillXBillItem.TermNumber, m_sConnectionString, m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_PAYEE_NAME, Function.GetEntityName(p_objBillXBillItem.BillXDsbrsmnt.Payee, m_sConnectionString, m_iClientId), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_PAYEE, p_objBillXBillItem.BillXDsbrsmnt.Payee.ToString(), m_iClientId);
                //Function.CreateNodeWithCodeValues(p_objParentNode, DISBURSEMENT_TRANSACTION_TYPE, p_objBillXBillItem.BillXDsbrsmnt.TransactionType, m_sConnectionString);
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_TRANSACTION_TYPE, Conversion.ConvertObjToStr(p_objBillXBillItem.BillXDsbrsmnt.TransactionType), m_iClientId);
                if (objSysSettings.UseFundsSubAcc)
                    Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_BANK_ACCOUNT, p_objBillXBillItem.BillXDsbrsmnt.SubAcctCode.ToString(), m_iClientId);
                else
                    Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_BANK_ACCOUNT, p_objBillXBillItem.BillXDsbrsmnt.BankAcctCode.ToString(), m_iClientId);
                //Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_AMOUNT, string.Format("{0:C}", p_objBillXBillItem.Amount)); Changes made for R5 : csingh7
                //Changed by Gagan for MITS 16399 : Start
                Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_AMOUNT, Convert.ToString(p_objBillXBillItem.Amount), m_iClientId);
                //Changed by Gagan for MITS 16399 : End
                Function.SetBillItemXml(p_objBillXBillItem, p_objParentNode, m_sConnectionString, m_iClientId);
                AppendBankAccountList(p_objParentNode);

                if (p_objBillXBillItem.Status != m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT") && p_objBillXBillItem.Status != 0)
                {
                    //Function.CreateAndSetElement(p_objParentNode, DISBURSEMENT_DISABLEALL, "True");
                    XmlElement objTempElement = p_objParentNode.OwnerDocument.CreateElement("DisableControls");

                    XmlElement objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "PaymentMethod";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Amount";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "BankAccount";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "TransactionType";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "CheckNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "InvoiceNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "NoticeNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Comments";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Ok";
                    objTempElement.AppendChild(objChildElement);

                    p_objParentNode.AppendChild(objTempElement);
                }

                string sCodevalues = Function.GetCodes("RECEIPT_TYPES", m_sConnectionString, m_iClientId);
                //rupal:start,mits 27977
                //string[] sCodeArr = sCodevalues.Split('^');
                string[] sCodeArr = sCodevalues.Split(Function.NewCodeSeperator,StringSplitOptions.RemoveEmptyEntries);
                //rupal:end
                objNew = objXML.CreateElement("CodeList");

                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;

                for (int i = 0; i < sCodeArr.Length; i++)
                {
                    //rupal:start,mits 27977
                    //string[] sCodes = sCodeArr[i].Split('~');
                    string[] sCodes = sCodeArr[i].Split(Function.CodeIdSeperator, StringSplitOptions.RemoveEmptyEntries);
                    //rupal:end
                    xmlOption = objXML.CreateElement("option");
                    objCData = objXML.CreateCDataSection(sCodes[1]);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXML.CreateAttribute("value");

                    xmlOptionAttrib.Value = sCodes[0];

                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                }

                objXML.DocumentElement.AppendChild(objNew);

                sCodevalues = Function.GetCodes("TRANS_TYPES", m_sConnectionString, m_iClientId);                
                //rupal:start,mits 27977
                //sCodeArr = sCodevalues.Split('^');                
                sCodeArr = sCodevalues.Split(Function.NewCodeSeperator, StringSplitOptions.RemoveEmptyEntries);
                //rupal:end

                objNew = objXML.CreateElement("TransTypeCodes");

                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;

                for (int i = 0; i < sCodeArr.Length; i++)
                {
                    //rupal:start,mits 27977
                    //string[] sCodes = sCodeArr[i].Split('~');
                    string[] sCodes = sCodeArr[i].Split(Function.CodeIdSeperator, StringSplitOptions.RemoveEmptyEntries);
                    //rupal:end
                    xmlOption = objXML.CreateElement("option");
                    objCData = objXML.CreateCDataSection(sCodes[1]);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXML.CreateAttribute("value");

                    xmlOptionAttrib.Value = sCodes[0];

                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                }

                objXML.DocumentElement.AppendChild(objNew);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DisbursementManager.CreateDisbursementItemXml.Erro", m_iClientId), p_objEx);
            }
            finally
            {
                objSysSettings = null;
                objNew = null;
                objXML = null;
                xmlOption = null;
                xmlOptionAttrib = null;
                objCData = null;
            }
        }


        public void SaveDisbursement(XmlDocument p_objInputDoc, int p_iBillItemRowId)
        {
            BillXBillItem objBillXBillItem = null;
            SysSettings objSysSettings = null;
            //policy Billing 
            double dblAmount = 0.0;
            string sDateReconciled = string.Empty;
            int iEntryBatchNumber = 0;
            int iEntryBatchType = 0;
            int iFinalBatchNumber = 0;
            int iFinalBatchType = 0;
            int iInvoiceNumber = 0;
            int iNoticeNumber = 0;
            int iBillingItemType = 0;
            int iStatus = 0;
            int iTermNumber = 0;
            int iPolicyId = 0;
            string sPolicyNumber = string.Empty;
            string sComment = string.Empty;
            bool bDisbursementPrintedFlag = false;
            int iDisbursementTransType = 0;
            int iDisbursementBankAccount = 0;
            int iDisbursementPayee = 0;
            string sDisbursementDatePrinted = string.Empty;
            //int iDisbursementCheckNumber = 0;
            long lDisbursementCheckNumber = 0; //pmittal5 Mits 14848
            int iDisbursementType = 0;
            try
            {
                objSysSettings = new SysSettings(m_sConnectionString,m_iClientId); //Ash - cloud
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);

                //Save Edited Data else save new record
                if (p_iBillItemRowId != 0)
                    objBillXBillItem.MoveTo(p_iBillItemRowId);

                //Function.GetBillItemXml(p_objInputDoc, sDateEntered, sDateReconciled, iEntryBatchNumber, iEntryBatchType, iFinalBatchNumber, iFinalBatchType, iInvoiceNumber, iNoticeNumber, iStatus, iTermNumber, sComment);
                Function.GetBillItemXml(p_objInputDoc, ref sPolicyNumber, ref iPolicyId, ref sDateReconciled, ref iEntryBatchNumber, ref iEntryBatchType, ref iFinalBatchNumber, ref iFinalBatchType, ref iInvoiceNumber, ref iNoticeNumber, ref iStatus, ref iTermNumber, ref sComment, m_sConnectionString, m_iClientId);
                dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, DISBURSEMENT_AMOUNT, m_iClientId));
                iBillingItemType = m_objLocalCache.GetCodeId("D", "BILLING_ITEM_TYPES");
                //iDisbursementTransType = Conversion.ConvertObjToInt(Function.GetCodeValue(p_objInputDoc, DISBURSEMENT_TRANSACTION_TYPE), m_iClientId);
                iDisbursementTransType = Conversion.ConvertObjToInt(Function.GetValue(p_objInputDoc, DISBURSEMENT_TRANSACTION_TYPE, m_iClientId), m_iClientId);
                iDisbursementBankAccount = Conversion.ConvertObjToInt(Function.GetValue(p_objInputDoc, DISBURSEMENT_BANK_ACCOUNT, m_iClientId), m_iClientId);
                iDisbursementPayee = Conversion.ConvertObjToInt(Function.GetValue(p_objInputDoc, DISBURSEMENT_PAYEE, m_iClientId), m_iClientId);
                sDisbursementDatePrinted = Function.GetValue(p_objInputDoc, DISBURSEMENT_DATE_PRINTED, m_iClientId);
                //pmittal5 Mits 14848 04/25/09 - Check Number is of 64 bits
                //iDisbursementCheckNumber = Conversion.ConvertObjToInt(Function.GetValue(p_objInputDoc, DISBURSEMENT_CHECK_NUMBER), m_iClientId);
                lDisbursementCheckNumber = Conversion.ConvertObjToInt64(Function.GetValue(p_objInputDoc, DISBURSEMENT_CHECK_NUMBER, m_iClientId), m_iClientId);
                //iDisbursementType = Conversion.ConvertObjToInt(Function.GetCodeValue(p_objInputDoc, DISBURSEMENT_PAYMENT_METHOD), m_iClientId);
                iDisbursementType = Conversion.ConvertObjToInt(Function.GetValue(p_objInputDoc, DISBURSEMENT_PAYMENT_METHOD, m_iClientId), m_iClientId);
                bDisbursementPrintedFlag = Conversion.ConvertObjToBool(Function.GetValue(p_objInputDoc, DISBURSEMENT_PRINTED_FLAG, m_iClientId), m_iClientId);
                objBillXBillItem.Amount = dblAmount;
                objBillXBillItem.DateEntered = Conversion.GetDate(DateTime.Now.ToString("d")); 
                objBillXBillItem.DateReconciled = sDateReconciled;
                objBillXBillItem.EntryBatchNum = iEntryBatchNumber;
                objBillXBillItem.EntryBatchType = iEntryBatchType;
                objBillXBillItem.FinalBatchNum = iFinalBatchNumber;
                objBillXBillItem.FinalBatchType = iFinalBatchType;
                objBillXBillItem.InvoiceId = iInvoiceNumber;
                objBillXBillItem.NoticeId = iNoticeNumber;
                objBillXBillItem.PolicyId = iPolicyId;
                objBillXBillItem.PolicyNumber = sPolicyNumber;
                objBillXBillItem.BillingItemType = iBillingItemType;
                objBillXBillItem.Status = iStatus;
                objBillXBillItem.TermNumber = iTermNumber;
                objBillXBillItem.Comments = sComment;
                objBillXBillItem.BillXDsbrsmnt.TransactionType = iDisbursementTransType;
                if (objSysSettings.UseFundsSubAcc)
                {
                    objBillXBillItem.BillXDsbrsmnt.SubAcctCode = iDisbursementBankAccount;
                    objBillXBillItem.BillXDsbrsmnt.BankAcctCode = 0;
                }
                else
                {
                    objBillXBillItem.BillXDsbrsmnt.BankAcctCode = iDisbursementBankAccount;
                    objBillXBillItem.BillXDsbrsmnt.SubAcctCode = 0;
                }
                objBillXBillItem.BillXDsbrsmnt.Payee = iDisbursementPayee;
                objBillXBillItem.BillXDsbrsmnt.DatePrinted = sDisbursementDatePrinted;
                objBillXBillItem.BillXDsbrsmnt.CheckNumber = lDisbursementCheckNumber;
                objBillXBillItem.BillXDsbrsmnt.DisbursementType = iDisbursementType;
                objBillXBillItem.BillXDsbrsmnt.PrintedFlag = bDisbursementPrintedFlag;
                objBillXBillItem.Save();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DisbursementManager.SaveDisbursement.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objSysSettings = null;
            }

        }

    }
}
