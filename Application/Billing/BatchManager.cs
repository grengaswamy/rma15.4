using System;
using System.IO ;
using System.Xml ;
using System.Text ;
using System.Collections ;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: BatchManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class BatchManager : IDisposable
	{
		#region Member Variables
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty ;	
		/// <summary>
		/// Private variable to store the instance of Local Cache object
		/// </summary>
		private LocalCache m_objLocalCache = null ;
		/// <summary>
		/// Private variable to store the instance of XmlDocument object
		/// </summary>
		private XmlDocument m_objDocument = null ;
        /// <summary>
        /// Private variable to store the Batch Total
        /// </summary>
        private double dblBatchTotal = 0;

        private int m_iClientId = 0;

        #endregion

        # region Constants

		private const string ROOT_NODE_NAME = "Batches" ;
		private const string USERS_GROUP_NODE_NAME = "UsersList" ;
		private const string USER_NODE_NAME = "option" ;
        private const string BATCH_STATUS_GROUP_NODE_NAME = "BatchStatusList";
        private const string BATCH_STATUS_NODE_NAME = "option";
        private const string BATCH_TYPE_GROUP_NODE_NAME = "BatchTypeList";
        private const string BATCH_TYPE_NODE_NAME = "option";
		private const string BATCH_GROUP_NODE_NAME = "Batch" ;
        private const string BATCH_HEAD_NODE_NAME = "listhead";
		private const string BATCH_NODE_NAME = "listrow" ;
		private const string BATCH_ROW_ID = "BatchRowId" ;
		private const string BATCH_STATUS = "Status" ;
		private const string BATCH_TYPE = "Type" ;
		private const string BATCH_DATE_OPENED = "DateOpened" ;
		private const string BATCH_AMOUNT = "Amount";
		private const string BATCH_COUNT = "BatchCount";
		private const string BATCH_DATE_CLOSED = "DateClosed" ;
		private const string BATCH_ADDED_BY_USER = "AddedByUser" ;
		private const string BILL_ITEMS_GROUP_NODE_NAME = "BillingItems" ;
        private const string BILL_ITEM_HEAD_NODE_NAME = "listhead";
		private const string BILL_ITEM_NODE_NAME = "listrow" ;
		private const string BILL_ITEM_DATE_ENTERED = "DateEntered" ;
		private const string BILL_ITEM_TYPE = "Type" ;
		private const string BILL_ITEM_STATUS = "Status" ;
		private const string BILL_ITEM_AMOUNT = "Amount" ;
		private const string BILL_ITEM_DATE_RECONCILE = "DateReconcile" ;
		private const string BILL_ITEM_FINAL_BATCH_NUM = "FinalBatchNum" ;
		private const string BILL_ITEM_FINAL_BATCH_TYPE = "FinalBatchType" ;
		private const string BILL_ITEM_ENTRY_BATCH_NUM = "EntryBatchNum" ;
		private const string BILL_ITEM_ENTRY_BATCH_TYPE = "EntryBatchType" ;
		private const string BILL_ITEM_INVOICE_ID = "InvoiceId" ;
		private const string BILL_ITEM_ROW_ID = "RowId" ;
        private const string SELECT = "Select";
		#endregion 		

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value .
		/// </summary>
		/// <param name="p_objDataModelFactory">DataModelFactory Object</param>
        public BatchManager(DataModelFactory p_objDataModelFactory, int p_iClientId)
		{
			m_objDataModelFactory = p_objDataModelFactory ;
			m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString ;
            m_iClientId = p_iClientId;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
		}
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>		
        public BatchManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString ;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);												
		}
		#endregion 

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }

		#region Abort Batch

        /// <summary>
        /// Aborts the batch
        /// </summary>
        /// <param name="p_iBatchRowId">Batch Row ID</param>        
        public void OnAbortBatch(int p_iBatchRowId)
        {
            BillXBatch objBillXBatch = null;
            BillingMaster objBillingMaster = null;
            BillingManager objBillingManager = null;
            ArrayList arrlstBillingItems = null;

            string sSQL = string.Empty;

            try
            {
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);
                objBillingMaster = new BillingMaster(m_objDataModelFactory, m_iClientId);
                arrlstBillingItems = new ArrayList();

                objBillXBatch = (BillXBatch)m_objDataModelFactory.GetDataModelObject("BillXBatch", false);
                objBillXBatch.MoveTo(p_iBatchRowId);

                sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE FINAL_BATCH_NUM = " + p_iBatchRowId;
                objBillingManager.FillBillingItemLoop(arrlstBillingItems, sSQL, true);

                objBillingMaster.Batches.Add(objBillXBatch.BatchRowid, objBillXBatch);

                // Change the Status of the Bill Items in the Batch
                foreach (BillXBillItem objBillXBillItem in arrlstBillingItems)
                {
                    if (objBillXBillItem.FinalBatchNum == p_iBatchRowId)
                    {
                        objBillXBillItem.Status = m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT");
                        objBillXBillItem.FinalBatchNum = 0;
                        objBillXBillItem.FinalBatchType = 0;
                        objBillXBillItem.DateReconciled = "";
                        objBillingMaster.BillingItems.Add(objBillXBillItem.BillingItemRowid, objBillXBillItem);
                    }
                }

                // Delete Batch
                objBillXBatch = (BillXBatch)objBillingMaster.Batches[p_iBatchRowId];
                if (objBillXBatch != null)
                {
                    objBillingMaster.DeleteBatch(p_iBatchRowId);
                }

                // Save.
                objBillingMaster.Save();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BatchManager.OnAbortBatch.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();
                    objBillXBatch = null;
                }
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
                arrlstBillingItems = null;
            }
        }

        #endregion 

		
		#region OnLoad Function


        /// <summary>
        /// Refreshes/Loads the Batches screen 
        /// </summary>
        /// <param name="p_objXmlFilter">Filter criteria</param>
        /// <returns>Output XML</returns>
        public XmlDocument OnLoad(XmlNode p_objXmlFilter)
		{			
			BillingManager objBillingManager = null ;
			ArrayList arrlstBatch = null ;
			ArrayList arrlstUsers = null ;
			DbReader objReader = null ;
			XmlElement objRootNode = null ;
            XmlElement objBillItemNode = null;
            XmlNode objNode = null;
            XmlNode objNewNode = null; 
            
			string sSQL = string.Empty ;
            string sStatus = string.Empty ;
            string sType = string.Empty;                      
			
			try
            {
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);
                arrlstBatch = new ArrayList();
                arrlstUsers = new ArrayList();
                

                // Read all the system batches
                sSQL = " SELECT * FROM BILL_X_BATCH ";
                objBillingManager.FillBatchLoop(arrlstBatch, sSQL);

                // Read for all users that have batches
                sSQL = "SELECT DISTINCT(ADDED_BY_USER) FROM BILL_X_BATCH";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                while (objReader.Read())
                {
                    arrlstUsers.Add(objReader.GetString("ADDED_BY_USER"));
                }
                objReader.Close();

                Function.StartDocument(ref m_objDocument, ref objRootNode, ROOT_NODE_NAME, m_iClientId);

                //Generate lookup xmls
                this.CreateUsersXml(objRootNode, arrlstUsers,
                                    p_objXmlFilter.SelectSingleNode("//SelectedUser").InnerText);
                this.CreateBatchXml(objRootNode, arrlstBatch,
                                    p_objXmlFilter.SelectSingleNode("//SelectedUser").InnerText,
                                    p_objXmlFilter.SelectSingleNode("//SelectedBatchStatus").InnerText,
                                    p_objXmlFilter.SelectSingleNode("//SelectedBatchType").InnerText,
                                    p_objXmlFilter.SelectSingleNode("//ToDate").InnerText,
                                    p_objXmlFilter.SelectSingleNode("//FromDate").InnerText);
                this.CreateBatchStatusXml(objRootNode, 
                                          p_objXmlFilter.SelectSingleNode("//SelectedBatchStatus").InnerText);
                this.CreateBatchTypeXml(objRootNode,
                                        p_objXmlFilter.SelectSingleNode("//SelectedBatchType").InnerText );


                Function.CreateElement(objRootNode, BILL_ITEMS_GROUP_NODE_NAME, ref objBillItemNode, m_iClientId);
                CreateBillItemsHeader(objBillItemNode);                            
                
                //Restore filter criteria to maintain state of controls
                objNode = m_objDocument.SelectSingleNode("//Batches");
                objNewNode = m_objDocument.CreateElement("Filter");
                objNewNode.InnerXml = p_objXmlFilter.SelectSingleNode("//Filter").InnerXml;
                //Clear batch id if action performed is not select batch since
                //page needs to be refreshed
                objNewNode.SelectSingleNode("//BatchId").InnerXml = "";
                objNode.AppendChild(objNewNode);                
            }
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.OnLoad.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
                objBillItemNode = null;
				arrlstBatch = null ;
				arrlstUsers = null ;
				objRootNode = null ;
                objNode = null;
                objNewNode = null;
			}
			return( m_objDocument );
        }

        # endregion


        #region OnSelect Function
        
        /// <summary>
        /// Perform action based on selection of a batch
        /// </summary>
        /// <param name="p_objXmlFilter">Filter criteria</param>
        /// <returns>Response XML</returns>
        public XmlDocument OnSelect(XmlNode p_objXmlFilter)
        {           
           int iBatchId = 0;         
           XmlElement objBillItemNode = null;
           bool bPrintDisb;

           try
            {
                //Invoke Onload to generate all Xmls
                OnLoad(p_objXmlFilter);

                if (p_objXmlFilter.SelectSingleNode("//SelectedBatch/BatchId") != null)
                    iBatchId = int.Parse(p_objXmlFilter.SelectSingleNode("//SelectedBatch/BatchId").InnerText);
                else
                    throw new RMAppException("Batch Id is blank");

                objBillItemNode = (XmlElement) m_objDocument.SelectSingleNode("//" + BILL_ITEMS_GROUP_NODE_NAME);

                // Verify there is no reconciled disbursement in the batch
                bPrintDisb = this.NoReconciledDisbursement(iBatchId);
                Function.CreateAndSetElement(objBillItemNode, "PrintDisbursement", bPrintDisb.ToString(), m_iClientId);                                              

                //Fetch billing items for the batch
                this.GetBillingItemsForBatch(objBillItemNode, iBatchId);
                Function.CreateAndSetElement(objBillItemNode, "BatchTotal", dblBatchTotal.ToString(), m_iClientId);

                m_objDocument.SelectSingleNode("//BatchId").InnerText = iBatchId.ToString();                
                
            }
           catch( RMAppException p_objEx )
			{
				throw p_objEx;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.OnSelect.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                objBillItemNode = null;
			
			}
			return( m_objDocument );
        }

        # endregion



        #region XML Methods

        /// <summary>
		/// Creates XML for Batches
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_arrlstBatch">Array list of Batches</param>
		/// <param name="p_sUser">User List</param>
        /// <param name="p_sBatchStatus">Batch Status</param>
		/// <param name="p_sBatchType">Batch Type</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_sFromDate">From Date</param>
		private void CreateBatchXml( XmlElement p_objParentNode, ArrayList p_arrlstBatch , string p_sUser , string p_sBatchStatus , string p_sBatchType , string p_sToDate, string p_sFromDate )
		{
			BillXBatch objBillXBatch = null ;
			XmlElement objBatchsNode = null ;            

			int iMaxDate = 99991231 ;
			int iMinDate = 00000000 ;
			int iFromDate = 0 ;
			int iToDate = 0 ;
			int iIndex = 0 ;			

			try
			{
				p_sFromDate = Conversion.GetDate( p_sFromDate );
				p_sToDate = Conversion.GetDate( p_sToDate );

                Function.CreateElement(p_objParentNode, BATCH_GROUP_NODE_NAME, ref objBatchsNode, m_iClientId);
                CreateBatchHeader(objBatchsNode);                                                           

				if( p_sFromDate == "" )
					iFromDate = iMinDate ;
				else
					iFromDate = Conversion.ConvertStrToInteger( p_sFromDate );

				if( p_sToDate == "" )
					iToDate = iMaxDate ;
				else
					iToDate = Conversion.ConvertStrToInteger( p_sToDate );
				
				for( iIndex = 0 ; iIndex < p_arrlstBatch.Count ; iIndex++ )
				{
					objBillXBatch = ( BillXBatch ) p_arrlstBatch[ iIndex ];
					if( ( objBillXBatch.BatchType.ToString() == p_sBatchType || p_sBatchType == "All" || p_sBatchType == "" )
                          && ( objBillXBatch.Status.ToString() == p_sBatchStatus || p_sBatchStatus == "All" || p_sBatchStatus == "") 
                          && ( p_sUser == "All" || p_sUser == "" || objBillXBatch.AddedByUser == p_sUser ) )
					{														
					   if( objBillXBatch.DateOpened != "" )
						{
							if( Conversion.ConvertStrToInteger( objBillXBatch.DateOpened ) >= iFromDate && Conversion.ConvertStrToInteger( objBillXBatch.DateOpened ) <= iToDate )
								this.CreateBatchItemXml( objBatchsNode , objBillXBatch );
						}
						else
						{
							this.CreateBatchItemXml( objBatchsNode , objBillXBatch );
						}
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.CreateBatchXml.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if( objBillXBatch != null )
				{
					objBillXBatch.Dispose();
					objBillXBatch = null ;
				}
                objBatchsNode = null;
			}
		}



        /// <summary>
        /// Creates XML for Batch grid Header
        /// </summary>
        /// <param name="objBatchsNode"></param>
        private void CreateBatchHeader(XmlElement objBatchsNode)
        {
            XmlElement objBatchHeaderNode = null;

            try
            {
                Function.CreateElement(objBatchsNode, BATCH_HEAD_NODE_NAME, ref objBatchHeaderNode, m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_ROW_ID, "Batch Number", m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_STATUS, "Status", m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_TYPE, "Batch Type", m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_DATE_OPENED, "Date Opened", m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_COUNT, "Batch Count", m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_AMOUNT, "Batch Amount", m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_DATE_CLOSED, "Date Closed", m_iClientId);
                Function.CreateAndSetElement(objBatchHeaderNode, BATCH_ADDED_BY_USER, "User Id", m_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BatchManager.CreateBatchHeader.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objBatchHeaderNode = null;
            }
        }





        /// <summary>
        /// Creates XML for Batch Items
        /// </summary>
        /// <param name="p_objParentNode"></param>
        /// <param name="p_objBillXBatch"></param>
		private void CreateBatchItemXml( XmlElement p_objParentNode , BillXBatch p_objBillXBatch )
		{
			XmlElement objBatchNode = null ;
			try
			{
                Function.CreateElement(p_objParentNode, BATCH_NODE_NAME, ref objBatchNode, m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_ROW_ID, p_objBillXBatch.BatchRowid.ToString(), m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_STATUS, m_objLocalCache.GetCodeDesc(p_objBillXBatch.Status), m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_TYPE, m_objLocalCache.GetCodeDesc(p_objBillXBatch.BatchType), m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_DATE_OPENED, Conversion.GetDBDateFormat(p_objBillXBatch.DateOpened, "d"), m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_COUNT, p_objBillXBatch.BatchCount.ToString(), m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_AMOUNT, string.Format("{0:C}", p_objBillXBatch.BatchAmount), m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_DATE_CLOSED, Conversion.GetDBDateFormat(p_objBillXBatch.DateClosed, "d"), m_iClientId);
                Function.CreateAndSetElement(objBatchNode, BATCH_ADDED_BY_USER, p_objBillXBatch.AddedByUser, m_iClientId);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.CreateBatchItemXml.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objBatchNode = null ;
			}
		}


        /// <summary>
        /// Creates XML for Batch Status
        /// </summary>
        /// <param name="p_objParentNode"></param>
        /// <param name="p_sSelectedBatchStatusId"></param>
        private void CreateBatchStatusXml(XmlElement p_objParentNode, string p_sSelectedBatchStatusId)
        {
            XmlElement objBatchStatusGroupNode = null ;
            XmlElement objBatchStatusNode = null;
            DbReader objReader = null;

			string sBatchStatusID = string.Empty ;
            string sBatchStatus = string.Empty;
            string sSQL = string.Empty;
			
			try
			{             
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT, GLOSSARY " +
                    " WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'BATCH_STATUS' " +
                    " AND CODES.CODE_ID = CODES_TEXT.CODE_ID " +
                    " AND CODES.TABLE_ID = GLOSSARY.TABLE_ID " +
                    " AND (CODES.DELETED_FLAG=0 OR CODES.DELETED_FLAG IS NULL)" +
                    " AND (GLOSSARY.DELETED_FLAG=0 OR GLOSSARY.DELETED_FLAG IS NULL) " +
                    " ORDER BY CODES_TEXT.CODE_DESC";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                Function.CreateElement(p_objParentNode, BATCH_STATUS_GROUP_NODE_NAME, ref objBatchStatusGroupNode, m_iClientId);

                Function.CreateAndSetElement(objBatchStatusGroupNode, BATCH_STATUS_NODE_NAME, "All", ref objBatchStatusNode, m_iClientId);
                objBatchStatusNode.SetAttribute("value", "All");

                while (objReader.Read())
                {                    
                    sBatchStatusID = objReader.GetInt32( "CODE_ID" ).ToString();
                    sBatchStatus = objReader.GetString("SHORT_CODE") + " - " + objReader.GetString("CODE_DESC");

                    Function.CreateAndSetElement(objBatchStatusGroupNode, BATCH_STATUS_NODE_NAME, sBatchStatus, ref objBatchStatusNode, m_iClientId);
                    objBatchStatusNode.SetAttribute("value", sBatchStatusID);                                     
                }

                objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.CreateBatchStatusXml.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                objBatchStatusGroupNode = null;
                objBatchStatusNode= null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
		}


        /// <summary>
        /// Creates XML for Batch type
        /// </summary>
        /// <param name="p_objParentNode"></param>
        /// <param name="p_sSelectedBatchTypeId"></param>
        private void CreateBatchTypeXml(XmlElement p_objParentNode, string p_sSelectedBatchTypeId)
        {
            XmlElement objBatchTypeGroupNode = null;
            XmlElement objBatchTypeNode = null;
            DbReader objReader = null;

            string sBatchTypeID = string.Empty;
            string sBatchType = string.Empty;
            string sSQL = string.Empty;

            try
            {
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT, GLOSSARY " +
                    " WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'BATCH_TYPES' " +
                    " AND CODES.CODE_ID = CODES_TEXT.CODE_ID " +
                    " AND CODES.TABLE_ID = GLOSSARY.TABLE_ID " +
                    " AND (CODES.DELETED_FLAG=0 OR CODES.DELETED_FLAG IS NULL)" +
                    " AND (GLOSSARY.DELETED_FLAG=0 OR GLOSSARY.DELETED_FLAG IS NULL) " +
                    " ORDER BY CODES_TEXT.CODE_DESC";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                Function.CreateElement(p_objParentNode, BATCH_TYPE_GROUP_NODE_NAME, ref objBatchTypeGroupNode, m_iClientId);

                Function.CreateAndSetElement(objBatchTypeGroupNode, BATCH_TYPE_NODE_NAME, "All", ref objBatchTypeNode, m_iClientId);
                objBatchTypeNode.SetAttribute("value", "All");

                while (objReader.Read())
                {
                    sBatchTypeID = objReader.GetInt32("CODE_ID").ToString();
                    sBatchType = objReader.GetString("SHORT_CODE") + "-" + objReader.GetString("CODE_DESC");

                    Function.CreateAndSetElement(objBatchTypeGroupNode, BATCH_TYPE_NODE_NAME, sBatchType, ref objBatchTypeNode, m_iClientId);
                    objBatchTypeNode.SetAttribute("value", sBatchTypeID);                    
                }

                objReader.Close();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BatchManager.CreateBatchTypeXml.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objBatchTypeGroupNode = null;
                objBatchTypeNode = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }
        
		
        /// <summary>
        /// Create Users XML
        /// </summary>
        /// <param name="p_objParentNode"></param>
        /// <param name="p_arrlstUsers"></param>
        /// <param name="p_sSelectedUser"></param>
		private void CreateUsersXml( XmlElement p_objParentNode , ArrayList p_arrlstUsers , string p_sSelectedUser )
		{
			XmlElement objUserGroupNode = null ;
			XmlElement objUserNode = null ;
			
			int iIndex = 0 ;
			string sUserName = string.Empty ;
			
			try
			{
                Function.CreateElement(p_objParentNode, USERS_GROUP_NODE_NAME, ref objUserGroupNode, m_iClientId);

                Function.CreateAndSetElement(objUserGroupNode, USER_NODE_NAME, "All", ref objUserNode, m_iClientId);
                objUserNode.SetAttribute("value", "All");
            

				for( iIndex = 0 ; iIndex < p_arrlstUsers.Count ; iIndex++ )
				{
					sUserName = ( string ) p_arrlstUsers[ iIndex ] ;
                    Function.CreateAndSetElement(objUserGroupNode, USER_NODE_NAME, sUserName, ref objUserNode, m_iClientId);
                    objUserNode.SetAttribute("value", sUserName);
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.CreateUsersXml.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				objUserGroupNode = null ;
				objUserNode = null ;
			}
        }

        #endregion

        #region Get Billing Items for the Batch




        /// <summary>
        /// Gets the Billing Items for the batch and generates the XML
        /// </summary>
        /// <param name="p_objParentNode">Output XML node</param>
        /// <param name="p_iBatchRowId">Batch Row ID</param>        
		public void GetBillingItemsForBatch(XmlElement p_objParentNode, int p_iBatchRowId )
		{
			BillingManager objBillingManager = null ;
			ArrayList arrlstBillingItems = null ;			
			string sSQL = string.Empty ;

			try
			{
				arrlstBillingItems = new ArrayList();

				sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE FINAL_BATCH_NUM =" + p_iBatchRowId + " OR ENTRY_BATCH_NUM =" + p_iBatchRowId ;
                using (objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId))
                {
				    objBillingManager.FillBillingItemLoop( arrlstBillingItems , sSQL , true );			
                }
			
                this.CreateBillingItemsXml(p_objParentNode, arrlstBillingItems);                
            }
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.GetBillingItemsForBatch.Error", m_iClientId), p_objEx);				
			}
			finally
			{				
				arrlstBillingItems = null ;
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
			}            
		}

		
        
        /// <summary>
        /// Creates XML of billing Items
        /// </summary>
        /// <param name="p_objParentNode">Output XML</param>
        /// <param name="p_arrlstBillingItems">billing Items</param>
		private void CreateBillingItemsXml( XmlElement p_objParentNode, ArrayList p_arrlstBillingItems )
		{
			BillXBillItem objBillXBillItem = null;
            XmlElement objBillItemNode = null;            
			int iIndex = 0 ;
			
			try
            {

              this.dblBatchTotal = 0;

			  for( iIndex = 0 ; iIndex < p_arrlstBillingItems.Count ; iIndex++ )
				{
					objBillXBillItem = ( BillXBillItem ) p_arrlstBillingItems[ iIndex ];
                    Function.CreateElement(p_objParentNode, BILL_ITEM_NODE_NAME, ref objBillItemNode, m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_DATE_ENTERED, Conversion.GetDBDateFormat(objBillXBillItem.DateEntered, "d"), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_TYPE, m_objLocalCache.GetCodeDesc(objBillXBillItem.BillingItemType), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_STATUS, m_objLocalCache.GetShortCode(objBillXBillItem.Status), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_AMOUNT, string.Format("{0:C}", objBillXBillItem.Amount), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_DATE_RECONCILE, Conversion.GetDBDateFormat(objBillXBillItem.DateReconciled, "d"), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_FINAL_BATCH_NUM, objBillXBillItem.FinalBatchNum.ToString(), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_FINAL_BATCH_TYPE, m_objLocalCache.GetCodeDesc(objBillXBillItem.FinalBatchType), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_ENTRY_BATCH_NUM, objBillXBillItem.EntryBatchNum.ToString(), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_ENTRY_BATCH_TYPE, m_objLocalCache.GetCodeDesc(objBillXBillItem.EntryBatchType), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_INVOICE_ID, objBillXBillItem.InvoiceId.ToString(), m_iClientId);
                    Function.CreateAndSetElement(objBillItemNode, BILL_ITEM_ROW_ID, objBillXBillItem.BillingItemRowid.ToString(), m_iClientId);

                   this.dblBatchTotal = this.dblBatchTotal + objBillXBillItem.Amount;
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.CreateBillingItemsXml.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
                objBillItemNode = null;
			}
		}


        /// <summary>
        /// Creates Grid Header elements
        /// </summary>
        /// <param name="objBillItemsNode"></param>   
        private void CreateBillItemsHeader(XmlElement objBillItemsNode)
        {
            XmlElement objBillHeaderNode = null;

            try
            {
                Function.CreateElement(objBillItemsNode, BILL_ITEM_HEAD_NODE_NAME, ref objBillHeaderNode, m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_DATE_ENTERED, "Date Entered", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_TYPE, "Billing Item Type", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_STATUS, "Status", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_AMOUNT, "Amount", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_DATE_RECONCILE, "Date Reconciled", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_FINAL_BATCH_NUM, "Final Batch Number", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_FINAL_BATCH_TYPE, "Final Batch Type", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_ENTRY_BATCH_NUM, "Entry Batch Number", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_ENTRY_BATCH_TYPE, "Entry Batch Type", m_iClientId);
                Function.CreateAndSetElement(objBillHeaderNode, BILL_ITEM_INVOICE_ID, "Invoice Number", m_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BatchManager.CreateBillItemsHeader.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objBillHeaderNode = null;
            }
        }



 
                    
		#endregion 

		#region Reverse Batch
		
        /// <summary>
        /// Reverses the batch
        /// </summary>
        /// <param name="p_iBatchRowId">BatchRowId</param>        
		public void OnReverseBatch( int p_iBatchRowId )
		{
			BillingManager objBillingManager = null ;
			BillingMaster objBillingMaster = null ;
			ArrayList arrlstBillingItems = null ;
			BillXBatch objBillXBatch = null;
			BillXBillItem objBillXBillItem = null ;
			DiaryManager objDiaryManager = null ;

			string sSQL = string.Empty ;
			
			try
			{
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);
                objDiaryManager = new DiaryManager(m_objDataModelFactory, m_iClientId);
				arrlstBillingItems = new ArrayList();

				sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE FINAL_BATCH_NUM <> 0 OR ENTRY_BATCH_NUM <> 0" ;
				objBillingManager.FillBillingItemLoop( arrlstBillingItems , sSQL , true );				

				// Add the batch in Billing Master.
                objBillingMaster = new BillingMaster(m_objDataModelFactory, m_iClientId);
				objBillXBatch = ( BillXBatch ) m_objDataModelFactory.GetDataModelObject( "BillXBatch" , false );
				objBillXBatch.MoveTo( p_iBatchRowId );
				objBillingMaster.Batches.Add( objBillXBatch.BatchRowid , objBillXBatch );

                
				if( this.ReverseBatch( objBillingMaster , arrlstBillingItems , p_iBatchRowId ) )
				{
					objBillingMaster.Save();                    
				}
				
				// Create the Credit Diary.
				if( arrlstBillingItems.Count > 0 )
				{
					objBillXBillItem = ( BillXBillItem ) arrlstBillingItems[0] ;
					objDiaryManager.ScheduleCreditDiaries( objBillXBillItem.PolicyId , objBillXBillItem.TermNumber );
				}				
			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.OnReverseBatch.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				arrlstBillingItems = null ;
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();
                }
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                }
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                }
				
			}			
		}


        /// <summary>
        /// Reverses the batch
        /// </summary>
        /// <param name="p_objBillingMaster"></param>
        /// <param name="p_arrlstBillingItems">Array of billing Items</param>
        /// <param name="p_iBatchRowId">Batch Row Id</param>        
		private bool ReverseBatch( BillingMaster p_objBillingMaster, ArrayList p_arrlstBillingItems, int p_iBatchRowId )
		{
			BillXBatch objBillXBatch = null ;
			BillXBillItem objBillItem = null ;
			BillingManager objBillingManager = null ;

			string sSQL = string.Empty ;
			int iIndex = 0 ;

			try
			{				
				/* 
					This is going to be a recursive routine
					Find all the transactions that are in the batch.
					If there is one that was added as result of this batch and it has been reconciled,
					We must reverse that reconciliation as well and on down the line recursively 
				*/
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);

				foreach( BillXBillItem objBillXBillItem in p_arrlstBillingItems )
				{
					if( ( objBillXBillItem.EntryBatchNum == p_iBatchRowId ) && ( ( objBillXBillItem.FinalBatchNum != 0 ) && ( objBillXBillItem.FinalBatchNum != p_iBatchRowId ) ) && ( objBillXBillItem.Status == m_objLocalCache.GetCodeId( "RC", "BILLING_ITEM_STAT" ) )) 
					{
						sSQL = " SELECT * FROM BILL_X_BATCH WHERE BATCH_ROWID=" + objBillXBillItem.FinalBatchNum ;
						objBillXBatch = ( BillXBatch ) m_objDataModelFactory.GetDataModelObject( "BillXBatch" , false );
						objBillingManager.FillBatch( objBillXBatch , sSQL );
                        
                        // Verify there is no reconciled disbursement in the batch
                        if(this.NoReconciledDisbursement(objBillXBatch.BatchRowid))
                            throw new RMAppException("There is a printed disbursement in a related batch - batch cannot be reversed.");
												
						// Add the batch in Billing Master.
						p_objBillingMaster.Batches.Add( objBillXBatch.BatchRowid , objBillXBatch );
						
						// Recursive call.
						if( !this.ReverseBatch( p_objBillingMaster, p_arrlstBillingItems, objBillXBatch.BatchRowid ) )
						{
							return( false );
						}
					}
				}

				// If there are any that were added as result of this batch (at this point they won't be reconciled) then delete them.
				for( iIndex = 0 ; iIndex < p_arrlstBillingItems.Count ; iIndex++ )
				{
					objBillItem = ( BillXBillItem ) p_arrlstBillingItems[iIndex] ;
					if( objBillItem.EntryBatchNum == p_iBatchRowId )
					{
						p_objBillingMaster.BillingItems.Add( objBillItem.BillingItemRowid , objBillItem );
						p_objBillingMaster.DeleteBillingItem( objBillItem.BillingItemRowid );
						p_arrlstBillingItems.RemoveAt( iIndex );
						iIndex-- ;
					}
				}

				foreach( BillXBillItem objBillXBillItem in p_arrlstBillingItems )
				{
					if( objBillXBillItem.FinalBatchNum == p_iBatchRowId )
					{
						objBillXBillItem.Status = m_objLocalCache.GetCodeId( "OK", "BILLING_ITEM_STAT" );
						objBillXBillItem.DateReconciled = "" ;
						objBillXBillItem.FinalBatchNum = 0 ;
						objBillXBillItem.FinalBatchType = 0 ;

                        //changed for mits 16338 : Start
						//objBillXBillItem.NoticeId = 0 ;
						//objBillXBillItem.InvoiceId = 0 ;
                        //changed for mits 16338 : End
						p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
					}
				}

				objBillXBatch = ( BillXBatch ) p_objBillingMaster.Batches[ p_iBatchRowId ];
				if( objBillXBatch != null )
				{
					objBillXBatch.Status = m_objLocalCache.GetCodeId( "R" , "BATCH_STATUS" );
				}
                return true;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.ReverseBatch.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBatch != null )
				{
					objBillXBatch.Dispose();
					objBillXBatch = null ;
				}
                if (objBillItem != null)
                {
                    objBillItem.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
			}            
        }

        #endregion

        # region No Reconciled Disbursement

        /// <summary>
        /// Checks whether there is a printed disbursement in the batch
        /// </summary>
        /// <param name="p_iBatchRowId">Batch Row ID</param>
        /// <returns>True/False</returns>    
        private bool NoReconciledDisbursement( int p_iBatchRowId )
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;

			try
			{
				sSQL = " SELECT * FROM BILL_X_BILL_ITEM,BILL_X_DSBRSMNT WHERE BILL_X_BILL_ITEM.FINAL_BATCH_NUM = " 
					+ p_iBatchRowId + " AND BILL_X_DSBRSMNT.BILLING_ITEM_ROWID = BILL_X_BILL_ITEM.BILLING_ITEM_ROWID AND BILL_X_DSBRSMNT.PRINTED_FLAG <>0" ;
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                if (objReader.Read())
                {
                    return true;
                }

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.NoReconciledDisbursement.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
            
            return false;
		}

        #endregion		
		
		#region Close Batch
		
		/// <summary>
		/// Closes the batch
		/// </summary>
        /// <param name="p_iBatchRowId">BatchRowId</param>
        /// <param name="p_iReconcileType">Reconciliation Type</param>		
        /// <param name="bCancelPayPlan">Cancel Pay Plan</param>		
        public void OnCloseBatch(int p_iBatchRowId, int p_iReconcileType, bool bCancelPayPlan)
		{
			BillingManager objBillingManager = null ;
			BillingMaster objBillingMaster = null ;
            //Changed for MITS 9775 by Gagan : Start 
            ArrayList arrlstAccountItems = null;
            //Changed for MITS 9775 by Gagan : End
			ArrayList arrlstInstallItems = null ;
			ArrayList arrlstBillingItems = null ;
			BillXBatch objBillXBatch = null;
			
			string sSQL = string.Empty ;
			
			try
			{
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);
                //Changed for MITS 9775 by Gagan : Start 
                arrlstAccountItems = new ArrayList();
                //Changed for MITS 9775 by Gagan : End
				arrlstInstallItems = new ArrayList();
				arrlstBillingItems = new ArrayList();

				// Add the batch in Billing Master.
                objBillingMaster = new BillingMaster(m_objDataModelFactory, m_iClientId);
				objBillXBatch = ( BillXBatch ) m_objDataModelFactory.GetDataModelObject( "BillXBatch" , false );
				objBillXBatch.MoveTo( p_iBatchRowId );
				objBillingMaster.Batches.Add( objBillXBatch.BatchRowid , objBillXBatch );

                //Changed for MITS 9775 by Gagan : Start 
                // Fill bill-x-account records.
                sSQL = "SELECT * from BILL_X_ACCOUNT WHERE BILL_X_ACCOUNT.POLICY_ID = " + objBillXBatch.PolicyId;
                objBillingManager.FillBillAccount(arrlstAccountItems, sSQL);
                //Changed for MITS 9775 by Gagan : End

				// Fill bill-x-instlmnt records
				sSQL = "SELECT * from BILL_X_INSTLMNT WHERE BILL_X_INSTLMNT.POLICY_ID = " + objBillXBatch.PolicyId ;
				objBillingManager.FillBillInstall( arrlstInstallItems , sSQL );
				
				// Fill Bill Items
				sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE FINAL_BATCH_NUM <> 0 OR ENTRY_BATCH_NUM <> 0" ;
				objBillingManager.FillBillingItemLoop( arrlstBillingItems , sSQL , true );

                //Changed for MITS 9775 by Gagan : Start 
                objBillingManager.CheckPaidInFull(arrlstBillingItems, arrlstAccountItems, arrlstInstallItems, objBillingMaster, p_iBatchRowId);
                //Changed for MITS 9775 by Gagan : End

                //If customer has paid premium in full and confirmation to cancel pay plan
                if (bCancelPayPlan)
				{
					objBillingManager.CancelPayPlanPaid( arrlstBillingItems , arrlstInstallItems , objBillingMaster , p_iBatchRowId );

					// Get Latest List of Billing Items.
					arrlstBillingItems.Clear();
					sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE FINAL_BATCH_NUM <> 0 OR ENTRY_BATCH_NUM <> 0" ;
					objBillingManager.FillBillingItemLoop( arrlstBillingItems , sSQL , true );
				}
							
				objBillingManager.Reconcile( arrlstBillingItems, p_iReconcileType , objBillingMaster );
				objBillingMaster.Save();			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.OnCloseBatch.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				arrlstBillingItems = null ;
                arrlstAccountItems = null;
                arrlstInstallItems = null;
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();
                }
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
			}			
		}

        #endregion 
        
        #region IsCheckPaidInFull

                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_iBatchRowId"> Batch RowID</param>
        /// <returns>True/ False whether 
        /// customer has paid premium in full</returns>
        public bool IsCheckPaidInFull(int p_iBatchRowId)
        {
        	BillingManager objBillingManager = null ;
			BillingMaster objBillingMaster = null ;
			ArrayList arrlstAccountItems = null ;
			ArrayList arrlstInstallItems = null ;
			ArrayList arrlstBillingItems = null ;
			BillXBatch objBillXBatch = null;
			
			string sSQL = string.Empty ;
			
			try
			{
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);	
				arrlstAccountItems = new ArrayList();
				arrlstInstallItems = new ArrayList();
				arrlstBillingItems = new ArrayList();

				// Add the batch in Billing Master.
                objBillingMaster = new BillingMaster(m_objDataModelFactory, m_iClientId);
				objBillXBatch = ( BillXBatch ) m_objDataModelFactory.GetDataModelObject( "BillXBatch" , false );
				objBillXBatch.MoveTo( p_iBatchRowId );
				objBillingMaster.Batches.Add( objBillXBatch.BatchRowid , objBillXBatch );
				
				// Fill bill-x-account records.
				sSQL = "SELECT * from BILL_X_ACCOUNT WHERE BILL_X_ACCOUNT.POLICY_ID = " + objBillXBatch.PolicyId ;
				objBillingManager.FillBillAccount( arrlstAccountItems , sSQL );
				
				// Fill bill-x-instlmnt records
				sSQL = "SELECT * from BILL_X_INSTLMNT WHERE BILL_X_INSTLMNT.POLICY_ID = " + objBillXBatch.PolicyId ;
				objBillingManager.FillBillInstall( arrlstInstallItems , sSQL );
				
				// Fill Bill Items
				//sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE FINAL_BATCH_NUM <> 0 OR ENTRY_BATCH_NUM <> 0" ;
                sSQL = "Select * from BILL_X_BILL_ITEM where POLICY_ID = " + objBillXBatch.PolicyId;
				objBillingManager.FillBillingItemLoop( arrlstBillingItems , sSQL , true );
                
				return objBillingManager.CheckPaidInFull( arrlstBillingItems , arrlstAccountItems , arrlstInstallItems, objBillingMaster , p_iBatchRowId );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BatchManager.IsCheckPaidInfull.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
				arrlstBillingItems = null ;
                arrlstAccountItems = null;
                arrlstInstallItems = null;
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();
                }
			}

			
		}

		
		#endregion 
	}
}
