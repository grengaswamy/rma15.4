﻿
using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Drawing;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Application.ReportInterfaces;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /**************************************************************
     * $File		: PCFirstInvoice.cs
     * $Revision	: 1.0.0.0
     * $Date		: 
     * $Author		: Nitin Goel
     * $Comment		: Template for Property (similar to GL)   
     * $Source		:  	
    **************************************************************/
    public class PCFirstInvoice : DataDynamics.ActiveReports.ActiveReport3   
	{
		#region Member Variables
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty ;
		/// <summary>
		/// Private variable to store Insured Entity
		/// </summary>		
		private Entity m_objInsuredEntity = null ;
		/// <summary>
		/// Private variable to store Insurer Entity
		/// </summary>
		private Entity m_objInsurerEntity = null ;
		/// <summary>
		/// Private variable to store Invoice
		/// </summary>
		private BillXInvoice m_objBillXInvoice = null ; 
		/// <summary>
		/// Private variable to store Effective Date
		/// </summary>		
		private string m_sEffDate = string.Empty ;
		/// <summary>
		/// Private variable to store Exp. Date
		/// </summary>
		private string m_sExpDate = string.Empty ;
		/// <summary>
		/// Private variable to store Early Pay Date
		/// </summary>
		private string m_sEarlyPayDate = string.Empty ;
		/// <summary>
		/// Private variable to store Early Pay Discount(string representation)
		/// </summary>
		private string m_sEarlyPayDiscount = string.Empty ;
		/// <summary>
		/// Private variable to store Early Pay Discount
		/// </summary>
		private double m_dblEarlyPayDiscount = 0.0 ;
		/// <summary>
		/// Private variable to store Early Pay Discount Amount
		/// </summary>
		private double m_dblEarlyPayDiscountAmount = 0.0 ;
		/// <summary>
		/// Private variable to store Premium After Discount
		/// </summary>
		private double m_dblPremiumAfterDiscount = 0.0 ;
		/// <summary>
		/// Private variable to store Total Premium
		/// </summary>
		private double m_dblTotalPremium = 0.0 ;
		/// <summary>
		/// Private variable to store Discount Type
		/// </summary>
		private int m_iDiscountType = 0 ;		
		/// <summary>
		/// Private variable to store Fetch Count
		/// </summary>
		private int m_iFetchCount = 0 ;
        private string m_sPolicy_Name = string.Empty; // csingh7  : MITS 12456 
        private int m_iClientId = 0;
		#endregion 		

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value, and Initialize the Report.
		/// </summary>
		/// <param name="p_objDataModelFactory">DataModelFactory Object</param>
        public PCFirstInvoice(DataModelFactory p_objDataModelFactory, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_objDataModelFactory = p_objDataModelFactory ;
			m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString ;
			
			InitializeComponent();
		}
		#endregion 

		#region Properties
		/// <summary>
		/// Read propertie for the Invoice collection.
		/// </summary>
		internal BillXInvoice Invoice 
		{
			get
			{				
				return m_objBillXInvoice;
			}			
		}

		#endregion 

		#region DataInitialize, FetchData, ReportStart Functions bind to respective events of the report.
		/// <summary>
		/// This method would invoke the method of base class to initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void PolicyBilling_DataInitialize( object p_objsender, System.EventArgs p_objeArgs )
		{ 
			try
			{
				m_iFetchCount = 0 ;

				Fields.Add("INSURED_NAME");
				Fields.Add("INSURED_ATTN");
				Fields.Add("INSURED_ADDR1");
				Fields.Add("INSURED_ADDR2");
				Fields.Add("INSURED_CITY");
				Fields.Add("INSURED_STATE");
				Fields.Add("INSURED_ZIPCODE");
				Fields.Add("INSURER_NAME");
				Fields.Add("INSURER_ADDR1");
				Fields.Add("INSURER_ADDR2");
				Fields.Add("INSURER_ADDR3");
                Fields.Add("POLICY_NAME");  //csingh7 : MITS 12456
				Fields.Add("DATE");
				Fields.Add("INVOICE_NUMBER");
				Fields.Add("DUE_DATE");
				Fields.Add("TERM_DATES");
				Fields.Add("AMOUNT1");
				Fields.Add("AMOUNT2");
				Fields.Add("AMOUNT3");					
				Fields.Add("EARLY_PAY_DATE1");
				Fields.Add("EARLY_PAY_DISCOUNT");
				Fields.Add("DISCOUNT_AMOUNT");
				Fields.Add("EARLY_PAY_DATE2");								
				Fields.Add("EARLY_PAY_PREMIUM");
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// This method would invoke the method of base class to fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void PolicyBilling_FetchData( object p_objsender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs p_objeArgs)
		{
			LocalCache objLocalCache = null ;
            DateTime objDate;

			string sMonth = string.Empty ;
			string sDay = string.Empty ;
			string sYear = string.Empty ;
			
			try
			{
				m_iFetchCount++ ;

				if( m_iFetchCount >=2 )
				{
					p_objeArgs.EOF = true;
					return;
				}
				else
					p_objeArgs.EOF = false;

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                m_objInsuredEntity.MoveTo(m_objBillXInvoice.HierarchyLevel);        // csingh7 : R5 changes MITS 16417
				// Supply data
				Fields["INSURED_NAME"].Value = m_objInsuredEntity.LastName ;
				Fields["INSURED_ATTN"].Value = m_objInsuredEntity.Contact ;
				Fields["INSURED_ADDR1"].Value = m_objInsuredEntity.Addr1 ;
				Fields["INSURED_ADDR2"].Value = m_objInsuredEntity.Addr2 ;
				Fields["INSURED_CITY"].Value = m_objInsuredEntity.City ;
				Fields["INSURED_STATE"].Value = objLocalCache.GetStateCode( m_objInsuredEntity.StateId );
				Fields["INSURED_ZIPCODE"].Value = m_objInsuredEntity.ZipCode ;
		    
				Fields["INSURER_NAME"].Value = m_objInsurerEntity.LastName ;
				Fields["INSURER_ADDR1"].Value = m_objInsurerEntity.Addr1 ;
				Fields["INSURER_ADDR2"].Value = m_objInsurerEntity.Addr2 ;
				Fields["INSURER_ADDR3"].Value = m_objInsurerEntity.City 
					+ ", " + objLocalCache.GetStateCode( m_objInsurerEntity.StateId ) 
					+ "   " + m_objInsurerEntity.ZipCode ;
                Fields["POLICY_NAME"].Value = m_sPolicy_Name;   //csingh7  : MITS 12456

                

				sMonth = m_objBillXInvoice.BillDate.Substring( 4 , 2 ) ;
				sDay = m_objBillXInvoice.BillDate.Substring( 6 , 2 ) ;
				sYear = m_objBillXInvoice.BillDate.Substring( 0 , 4 ) ;

                objDate = new DateTime(Conversion.ConvertStrToInteger(sYear),
                                                Conversion.ConvertStrToInteger(sMonth),
                                                Conversion.ConvertStrToInteger(sDay));


                Fields["DATE"].Value = objDate.ToString("MMMM") + " " + sDay + ", " + sYear;                
				Fields["INVOICE_NUMBER"].Value = m_objBillXInvoice.InvoiceRowid ;
				Fields["DUE_DATE"].Value = Common.Conversion.GetDBDateFormat( m_objBillXInvoice.DueDate , "d" ) ;
				Fields["TERM_DATES"].Value = m_sEffDate + " - " + m_sExpDate ;
				Fields["AMOUNT1"].Value = string.Format("{0:C}" , m_dblTotalPremium ) ;
				Fields["AMOUNT2"].Value = string.Format("{0:C}" , m_objBillXInvoice.Amount ) ;
				Fields["AMOUNT3"].Value = string.Format("{0:C}" , m_objBillXInvoice.Amount ) ;
				if( m_sEarlyPayDate != "(NONE)" )
				{
					if( m_sEarlyPayDate.Substring( 1 , 1) == "/" || m_sEarlyPayDate.Substring( 2, 1) == "/"  )
						m_sEarlyPayDate = Common.Conversion.GetDate( m_sEarlyPayDate );
				
					//sMonth = MonthName( m_sEarlyPayDate.Substring( 4 , 2 ) ) ;
					sDay = m_sEarlyPayDate.Substring( 6 , 2 ) ;
					sYear = m_sEarlyPayDate.Substring( 0 , 4 ) ;
					Fields["EARLY_PAY_DATE1"].Value = sMonth + " " + sDay + ", " + sYear ;
					Fields["EARLY_PAY_DATE2"].Value = sMonth + " " + sDay + ", " + sYear ;
				}
				else
				{
					Fields["EARLY_PAY_DATE1"].Value = m_sEarlyPayDate ;
					Fields["EARLY_PAY_DATE2"].Value = m_sEarlyPayDate ;
				}
				Fields["EARLY_PAY_DISCOUNT"].Value = m_sEarlyPayDiscount ;
			
				if( m_dblEarlyPayDiscountAmount == 0.0 )
					Fields["DISCOUNT_AMOUNT"].Value = string.Format("{0:C}" , m_dblEarlyPayDiscount ) ;
				else
					Fields["DISCOUNT_AMOUNT"].Value = string.Format("{0:C}" , m_dblEarlyPayDiscount ) ;
			
				Fields["EARLY_PAY_PREMIUM"].Value = string.Format("{0:C}" , m_dblPremiumAfterDiscount ) ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
			}
			finally
			{
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
			}
		}

		/// <summary>
		/// This method will do the page setting for the EOB Report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event</param>
		private void PolicyBilling_ReportStart( object p_objsender,  System.EventArgs p_objeArgs)
		{
			try
			{
				this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PCFirstInvoice.PolicyBilling_ReportStart.Error", m_iClientId), p_objEx);				
			}
		}

		#endregion 

		#region GatherData Method 
		/// <summary>
		/// This method will collect the data for the report.
		/// </summary>
		/// <param name="p_iInvoiceId">Invoice Id</param>
		/// <param name="p_objBillingMaster">Billing Master Object</param>
		public void GatherData( int p_iInvoiceId, BillingMaster p_objBillingMaster )
		{
			PolicyEnh objPolicyEnh = null ;
			DbReader objReader = null ;
			LocalCache objLocalCache = null ;
							
			string sSQL = string.Empty ;
			bool bRoundFlag = false ;

			try
			{
				m_objInsuredEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
			
				m_objBillXInvoice = (BillXInvoice) p_objBillingMaster.Invoices[ p_iInvoiceId ] ;
				if( m_objBillXInvoice != null )
				{
					//m_objInsuredEntity.EntityId = m_objBillXInvoice.HierarchyLevel ;
                    m_objInsuredEntity.MoveTo(m_objBillXInvoice.HierarchyLevel);
					objPolicyEnh = (PolicyEnh) p_objBillingMaster.Policies[ m_objBillXInvoice.PolicyId ];
					if( objPolicyEnh != null )
					{
                        m_sPolicy_Name = objPolicyEnh.PolicyName;   //csingh7  : MITS 12456
						m_objInsurerEntity = objPolicyEnh.InsurerEntity ;
						foreach( PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList )
						{
							if( objPolicyXTermEnh.TermNumber == m_objBillXInvoice.TermNumber )
							{
								m_sEffDate = Common.Conversion.GetDBDateFormat( objPolicyXTermEnh.EffectiveDate , "d" ); 
								m_sExpDate = Common.Conversion.GetDBDateFormat( objPolicyXTermEnh.ExpirationDate , "d" );
								break;
							}
						}
					}
				}
			
				// Retrieve the address
				m_objInsuredEntity.Refresh();

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

				// Early pay info
				sSQL = " SELECT * FROM SYS_ERLY_PAY_DSCNT WHERE LINE_OF_BUSINESS =" 
					+	objPolicyEnh.PolicyType + " AND IN_USE_FLAG <>0 " ;

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					m_sEarlyPayDate = objReader.GetString( "DEADLINE_DATE" );
					m_iDiscountType = Common.Conversion.ConvertObjToInt( objReader.GetValue( "DISCOUNT_TYPE" ), m_iClientId );
					m_dblEarlyPayDiscount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
					if( objLocalCache.GetShortCode( m_iDiscountType ) == "P" )
						m_sEarlyPayDiscount = m_dblEarlyPayDiscount.ToString() + PolBillingConstants.PERCENT ;
					else
						m_sEarlyPayDiscount = PolBillingConstants.FLAT ;
				}
				else
				{
					m_sEarlyPayDate = "(NONE)" ;
					m_sEarlyPayDiscount = "0%" ;
					m_dblEarlyPayDiscountAmount = 0 ;
					m_dblEarlyPayDiscount = 0 ;
				}
				objReader.Close();

				sSQL = " SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID=" 
					+ objPolicyEnh.PolicyId + " AND TERM_NUMBER=" + m_objBillXInvoice.TermNumber ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );			
				if( objReader.Read() ) 
					m_dblTotalPremium = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT_DUE" ) ));
				objReader.Close();

				sSQL = " SELECT * FROM SYS_POL_OPTIONS " ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );	
				if( objReader.Read() ) 
					bRoundFlag = objReader.GetBoolean( "ROUND_AMTS_FLAG" );

				if( m_dblEarlyPayDiscount != 0 )
				{
					if( objLocalCache.GetShortCode( m_iDiscountType ) == "P" )
					{
						if( bRoundFlag )
                            m_dblEarlyPayDiscount = Math.Round((m_dblTotalPremium * (m_dblEarlyPayDiscount / 100)), 0, MidpointRounding.AwayFromZero);
						else
                            m_dblEarlyPayDiscount = Math.Round((m_dblTotalPremium * (m_dblEarlyPayDiscount / 100)), 2, MidpointRounding.AwayFromZero);

                        m_dblPremiumAfterDiscount = Math.Round((m_dblTotalPremium - m_dblEarlyPayDiscountAmount), 2, MidpointRounding.AwayFromZero);
					}
					else
					{
                        m_dblPremiumAfterDiscount = Math.Round((m_dblTotalPremium - m_dblEarlyPayDiscount), 2, MidpointRounding.AwayFromZero);
					}
				}
				else
				{
					m_dblPremiumAfterDiscount = m_dblTotalPremium ;
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PCFirstInvoice.GatherData.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
				if( objReader != null )
				{
					objReader.Dispose() ;
					objReader = null ;
				}
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
			}
		}

		#endregion 

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.TextBox Field28 = null;
		private DataDynamics.ActiveReports.TextBox Field29 = null;
		private DataDynamics.ActiveReports.TextBox Field30 = null;
		private DataDynamics.ActiveReports.TextBox Field33 = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.TextBox Field34 = null;
		private DataDynamics.ActiveReports.TextBox Field47 = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.Label Label2 = null;
		private DataDynamics.ActiveReports.Label Label3 = null;
		private DataDynamics.ActiveReports.Label Label4 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.Label Label6 = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.TextBox Field35 = null;
		private DataDynamics.ActiveReports.Label Label8 = null;
		private DataDynamics.ActiveReports.Label Label9 = null;
		private DataDynamics.ActiveReports.TextBox Field36 = null;
		private DataDynamics.ActiveReports.Label Label10 = null;
		private DataDynamics.ActiveReports.TextBox Field37 = null;
		private DataDynamics.ActiveReports.Label Label11 = null;
		private DataDynamics.ActiveReports.TextBox Field38 = null;
		private DataDynamics.ActiveReports.Label Label12 = null;
		private DataDynamics.ActiveReports.Label Label13 = null;
		private DataDynamics.ActiveReports.Label Label14 = null;
		private DataDynamics.ActiveReports.Label Label15 = null;
		private DataDynamics.ActiveReports.Label Label16 = null;
		private DataDynamics.ActiveReports.TextBox Field39 = null;
		private DataDynamics.ActiveReports.TextBox Field40 = null;
		private DataDynamics.ActiveReports.Label Label17 = null;
		private DataDynamics.ActiveReports.Label Label18 = null;
		private DataDynamics.ActiveReports.Label Label19 = null;
		private DataDynamics.ActiveReports.Label Label20 = null;
		private DataDynamics.ActiveReports.Label Label21 = null;
		private DataDynamics.ActiveReports.Label Label22 = null;
		private DataDynamics.ActiveReports.Label Label23 = null;
		private DataDynamics.ActiveReports.TextBox Field42 = null;
		private DataDynamics.ActiveReports.TextBox Field43 = null;
		private DataDynamics.ActiveReports.TextBox Field44 = null;
		private DataDynamics.ActiveReports.TextBox Field45 = null;
		private DataDynamics.ActiveReports.TextBox Field46 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.TextBox Field1 = null;
		private DataDynamics.ActiveReports.TextBox Field2 = null;
		private DataDynamics.ActiveReports.TextBox Field3 = null;
		private DataDynamics.ActiveReports.TextBox Field4 = null;
		private DataDynamics.ActiveReports.TextBox Field10 = null;
        private DataDynamics.ActiveReports.TextBox Field11 = null;
		private DataDynamics.ActiveReports.TextBox Field41 = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PCFirstInvoice));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.Label2 = new DataDynamics.ActiveReports.Label();
            this.Label3 = new DataDynamics.ActiveReports.Label();
            this.Label4 = new DataDynamics.ActiveReports.Label();
            this.Label5 = new DataDynamics.ActiveReports.Label();
            this.Label6 = new DataDynamics.ActiveReports.Label();
            this.Label7 = new DataDynamics.ActiveReports.Label();
            this.Field35 = new DataDynamics.ActiveReports.TextBox();
            this.Label8 = new DataDynamics.ActiveReports.Label();
            this.Label9 = new DataDynamics.ActiveReports.Label();
            this.Field36 = new DataDynamics.ActiveReports.TextBox();
            this.Label10 = new DataDynamics.ActiveReports.Label();
            this.Field37 = new DataDynamics.ActiveReports.TextBox();
            this.Label11 = new DataDynamics.ActiveReports.Label();
            this.Field38 = new DataDynamics.ActiveReports.TextBox();
            this.Label12 = new DataDynamics.ActiveReports.Label();
            this.Label13 = new DataDynamics.ActiveReports.Label();
            this.Label14 = new DataDynamics.ActiveReports.Label();
            this.Label15 = new DataDynamics.ActiveReports.Label();
            this.Label16 = new DataDynamics.ActiveReports.Label();
            this.Field39 = new DataDynamics.ActiveReports.TextBox();
            this.Field40 = new DataDynamics.ActiveReports.TextBox();
            this.Label17 = new DataDynamics.ActiveReports.Label();
            this.Label18 = new DataDynamics.ActiveReports.Label();
            this.Label19 = new DataDynamics.ActiveReports.Label();
            this.Label20 = new DataDynamics.ActiveReports.Label();
            this.Label21 = new DataDynamics.ActiveReports.Label();
            this.Label22 = new DataDynamics.ActiveReports.Label();
            this.Label23 = new DataDynamics.ActiveReports.Label();
            this.Field42 = new DataDynamics.ActiveReports.TextBox();
            this.Field43 = new DataDynamics.ActiveReports.TextBox();
            this.Field44 = new DataDynamics.ActiveReports.TextBox();
            this.Field45 = new DataDynamics.ActiveReports.TextBox();
            this.Field46 = new DataDynamics.ActiveReports.TextBox();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.Field28 = new DataDynamics.ActiveReports.TextBox();
            this.Field29 = new DataDynamics.ActiveReports.TextBox();
            this.Field30 = new DataDynamics.ActiveReports.TextBox();
            this.Field33 = new DataDynamics.ActiveReports.TextBox();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.Field34 = new DataDynamics.ActiveReports.TextBox();
            this.Field47 = new DataDynamics.ActiveReports.TextBox();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.Field1 = new DataDynamics.ActiveReports.TextBox();
            this.Field2 = new DataDynamics.ActiveReports.TextBox();
            this.Field3 = new DataDynamics.ActiveReports.TextBox();
            this.Field4 = new DataDynamics.ActiveReports.TextBox();
            this.Field10 = new DataDynamics.ActiveReports.TextBox();
            this.Field11 = new DataDynamics.ActiveReports.TextBox();
            this.Field41 = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.Label7,
            this.Field35,
            this.Label8,
            this.Label9,
            this.Field36,
            this.Label10,
            this.Field37,
            this.Label11,
            this.Field38,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Field39,
            this.Field40,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Label21,
            this.Label22,
            this.Label23,
            this.Field42,
            this.Field43,
            this.Field44,
            this.Field45,
            this.Field46});
            this.Detail.Height = 6.322222F;
            this.Detail.Name = "Detail";
            // 
            // Label2
            // 
            this.Label2.Border.BottomColor = System.Drawing.Color.Black;
            this.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.LeftColor = System.Drawing.Color.Black;
            this.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.RightColor = System.Drawing.Color.Black;
            this.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.TopColor = System.Drawing.Color.Black;
            this.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = "";
            this.Label2.Left = 0.125F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label2.Text = "Invoice Number:";
            this.Label2.Top = 0.5F;
            this.Label2.Width = 1.375F;
            // 
            // Label3
            // 
            this.Label3.Border.BottomColor = System.Drawing.Color.Black;
            this.Label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Border.LeftColor = System.Drawing.Color.Black;
            this.Label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Border.RightColor = System.Drawing.Color.Black;
            this.Label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Border.TopColor = System.Drawing.Color.Black;
            this.Label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = "";
            this.Label3.Left = 2F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label3.Text = "Total Billed Premium:";
            this.Label3.Top = 1.6875F;
            this.Label3.Width = 1.6875F;
            // 
            // Label4
            // 
            this.Label4.Border.BottomColor = System.Drawing.Color.Black;
            this.Label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Border.LeftColor = System.Drawing.Color.Black;
            this.Label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Border.RightColor = System.Drawing.Color.Black;
            this.Label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Border.TopColor = System.Drawing.Color.Black;
            this.Label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = "";
            this.Label4.Left = 1F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label4.Text = "Property Coverage Period:";
            this.Label4.Top = 1.0625F;
            this.Label4.Width = 5F;
            // 
            // Label5
            // 
            this.Label5.Border.BottomColor = System.Drawing.Color.Black;
            this.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Border.LeftColor = System.Drawing.Color.Black;
            this.Label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Border.RightColor = System.Drawing.Color.Black;
            this.Label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Border.TopColor = System.Drawing.Color.Black;
            this.Label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = "";
            this.Label5.Left = 4F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label5.Text = "Due Date:";
            this.Label5.Top = 0.5F;
            this.Label5.Width = 0.875F;
            // 
            // Label6
            // 
            this.Label6.Border.BottomColor = System.Drawing.Color.Black;
            this.Label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Border.LeftColor = System.Drawing.Color.Black;
            this.Label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Border.RightColor = System.Drawing.Color.Black;
            this.Label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Border.TopColor = System.Drawing.Color.Black;
            this.Label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = "";
            this.Label6.Left = 2F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold; font-size: 12pt; ";
            this.Label6.Text = "TOTAL DUE :";
            this.Label6.Top = 2.0625F;
            this.Label6.Width = 1.75F;
            // 
            // Label7
            // 
            this.Label7.Border.BottomColor = System.Drawing.Color.Black;
            this.Label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Border.LeftColor = System.Drawing.Color.Black;
            this.Label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Border.RightColor = System.Drawing.Color.Black;
            this.Label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Border.TopColor = System.Drawing.Color.Black;
            this.Label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = "";
            this.Label7.Left = 0.125F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 12pt; ";
            this.Label7.Text = "PLEASE PAY THIS AMOUNT => => => =>";
            this.Label7.Top = 3.125F;
            this.Label7.Width = 2.75F;
            // 
            // Field35
            // 
            this.Field35.Border.BottomColor = System.Drawing.Color.Black;
            this.Field35.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field35.Border.LeftColor = System.Drawing.Color.Black;
            this.Field35.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field35.Border.RightColor = System.Drawing.Color.Black;
            this.Field35.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field35.Border.TopColor = System.Drawing.Color.Black;
            this.Field35.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field35.DataField = "AMOUNT3";
            this.Field35.Height = 0.1875F;
            this.Field35.Left = 4F;
            this.Field35.Name = "Field35";
            this.Field35.Style = "ddo-char-set: 0; text-align: right; font-size: 12pt; font-family: Arial; ";
            this.Field35.Text = "AMOUNT3";
            this.Field35.Top = 3.125F;
            this.Field35.Width = 1.375F;
            // 
            // Label8
            // 
            this.Label8.Border.BottomColor = System.Drawing.Color.Black;
            this.Label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Border.LeftColor = System.Drawing.Color.Black;
            this.Label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Border.RightColor = System.Drawing.Color.Black;
            this.Label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Border.TopColor = System.Drawing.Color.Black;
            this.Label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Height = 0.25F;
            this.Label8.HyperLink = "";
            this.Label8.Left = 0F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "text-decoration: underline; font-weight: bold; font-style: italic; font-size: 12p" +
                "t; ";
            this.Label8.Text = "PLEASE NOTE:";
            this.Label8.Top = 3.8125F;
            this.Label8.Width = 2.75F;
            // 
            // Label9
            // 
            this.Label9.Border.BottomColor = System.Drawing.Color.Black;
            this.Label9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Border.LeftColor = System.Drawing.Color.Black;
            this.Label9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Border.RightColor = System.Drawing.Color.Black;
            this.Label9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Border.TopColor = System.Drawing.Color.Black;
            this.Label9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = "";
            this.Label9.Left = 0.1875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 10pt; ";
            this.Label9.Text = "If payment is made before";
            this.Label9.Top = 4.1875F;
            this.Label9.Width = 1.6875F;
            // 
            // Field36
            // 
            this.Field36.Border.BottomColor = System.Drawing.Color.Black;
            this.Field36.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field36.Border.LeftColor = System.Drawing.Color.Black;
            this.Field36.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field36.Border.RightColor = System.Drawing.Color.Black;
            this.Field36.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field36.Border.TopColor = System.Drawing.Color.Black;
            this.Field36.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field36.DataField = "EARLY_PAY_DATE1";
            this.Field36.Height = 0.1875F;
            this.Field36.Left = 1.875F;
            this.Field36.Name = "Field36";
            this.Field36.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; font-family: Arial; ";
            this.Field36.Text = "EARLY_PAY_DATE1";
            this.Field36.Top = 4.1875F;
            this.Field36.Width = 1.25F;
            // 
            // Label10
            // 
            this.Label10.Border.BottomColor = System.Drawing.Color.Black;
            this.Label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label10.Border.LeftColor = System.Drawing.Color.Black;
            this.Label10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label10.Border.RightColor = System.Drawing.Color.Black;
            this.Label10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label10.Border.TopColor = System.Drawing.Color.Black;
            this.Label10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = "";
            this.Label10.Left = 3.125F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 10pt; ";
            this.Label10.Text = ", you will be eligible for a";
            this.Label10.Top = 4.1875F;
            this.Label10.Width = 1.5625F;
            // 
            // Field37
            // 
            this.Field37.Border.BottomColor = System.Drawing.Color.Black;
            this.Field37.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field37.Border.LeftColor = System.Drawing.Color.Black;
            this.Field37.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field37.Border.RightColor = System.Drawing.Color.Black;
            this.Field37.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field37.Border.TopColor = System.Drawing.Color.Black;
            this.Field37.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field37.DataField = "EARLY_PAY_DISCOUNT";
            this.Field37.Height = 0.1875F;
            this.Field37.Left = 4.6875F;
            this.Field37.Name = "Field37";
            this.Field37.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; font-family: Arial; ";
            this.Field37.Text = "EARLY_PAY_DISCOUNT";
            this.Field37.Top = 4.1875F;
            this.Field37.Width = 0.6875F;
            // 
            // Label11
            // 
            this.Label11.Border.BottomColor = System.Drawing.Color.Black;
            this.Label11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label11.Border.LeftColor = System.Drawing.Color.Black;
            this.Label11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label11.Border.RightColor = System.Drawing.Color.Black;
            this.Label11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label11.Border.TopColor = System.Drawing.Color.Black;
            this.Label11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = "";
            this.Label11.Left = 5.375F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 10pt; ";
            this.Label11.Text = "discount of";
            this.Label11.Top = 4.1875F;
            this.Label11.Width = 0.75F;
            // 
            // Field38
            // 
            this.Field38.Border.BottomColor = System.Drawing.Color.Black;
            this.Field38.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field38.Border.LeftColor = System.Drawing.Color.Black;
            this.Field38.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field38.Border.RightColor = System.Drawing.Color.Black;
            this.Field38.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field38.Border.TopColor = System.Drawing.Color.Black;
            this.Field38.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field38.DataField = "DISCOUNT_AMOUNT";
            this.Field38.Height = 0.1875F;
            this.Field38.Left = 2.5F;
            this.Field38.Name = "Field38";
            this.Field38.Style = "ddo-char-set: 0; text-align: center; font-weight: bold; font-size: 10pt; font-fam" +
                "ily: Arial; ";
            this.Field38.Text = "DISCOUNT_AMOUNT";
            this.Field38.Top = 4.375F;
            this.Field38.Width = 1.375F;
            // 
            // Label12
            // 
            this.Label12.Border.BottomColor = System.Drawing.Color.Black;
            this.Label12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label12.Border.LeftColor = System.Drawing.Color.Black;
            this.Label12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label12.Border.RightColor = System.Drawing.Color.Black;
            this.Label12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label12.Border.TopColor = System.Drawing.Color.Black;
            this.Label12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label12.Height = 0.1875F;
            this.Label12.HyperLink = "";
            this.Label12.Left = 0.0625F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "";
            this.Label12.Text = "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" +
                "$";
            this.Label12.Top = 4.75F;
            this.Label12.Width = 6.375F;
            // 
            // Label13
            // 
            this.Label13.Border.BottomColor = System.Drawing.Color.Black;
            this.Label13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label13.Border.LeftColor = System.Drawing.Color.Black;
            this.Label13.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label13.Border.RightColor = System.Drawing.Color.Black;
            this.Label13.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label13.Border.TopColor = System.Drawing.Color.Black;
            this.Label13.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label13.Height = 0.1875F;
            this.Label13.HyperLink = "";
            this.Label13.Left = 0.0625F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "";
            this.Label13.Text = "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" +
                "$";
            this.Label13.Top = 5.5F;
            this.Label13.Width = 6.375F;
            // 
            // Label14
            // 
            this.Label14.Border.BottomColor = System.Drawing.Color.Black;
            this.Label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label14.Border.LeftColor = System.Drawing.Color.Black;
            this.Label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label14.Border.RightColor = System.Drawing.Color.Black;
            this.Label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label14.Border.TopColor = System.Drawing.Color.Black;
            this.Label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = "";
            this.Label14.Left = 0.375F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-weight: bold; font-style: italic; font-size: 12pt; ";
            this.Label14.Text = "The amount due if payment is made before";
            this.Label14.Top = 4.9375F;
            this.Label14.Width = 3.5F;
            // 
            // Label15
            // 
            this.Label15.Border.BottomColor = System.Drawing.Color.Black;
            this.Label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label15.Border.LeftColor = System.Drawing.Color.Black;
            this.Label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label15.Border.RightColor = System.Drawing.Color.Black;
            this.Label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label15.Border.TopColor = System.Drawing.Color.Black;
            this.Label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label15.Height = 0.125F;
            this.Label15.HyperLink = "";
            this.Label15.Left = 0.0625F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "";
            this.Label15.Text = "*********************************************************************************" +
                "************************************";
            this.Label15.Top = 3F;
            this.Label15.Width = 6.375F;
            // 
            // Label16
            // 
            this.Label16.Border.BottomColor = System.Drawing.Color.Black;
            this.Label16.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label16.Border.LeftColor = System.Drawing.Color.Black;
            this.Label16.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label16.Border.RightColor = System.Drawing.Color.Black;
            this.Label16.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label16.Border.TopColor = System.Drawing.Color.Black;
            this.Label16.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label16.Height = 0.125F;
            this.Label16.HyperLink = "";
            this.Label16.Left = 0.0625F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "";
            this.Label16.Text = "*********************************************************************************" +
                "************************************";
            this.Label16.Top = 3.3125F;
            this.Label16.Width = 6.375F;
            // 
            // Field39
            // 
            this.Field39.Border.BottomColor = System.Drawing.Color.Black;
            this.Field39.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field39.Border.LeftColor = System.Drawing.Color.Black;
            this.Field39.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field39.Border.RightColor = System.Drawing.Color.Black;
            this.Field39.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field39.Border.TopColor = System.Drawing.Color.Black;
            this.Field39.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field39.DataField = "EARLY_PAY_DATE2";
            this.Field39.Height = 0.1875F;
            this.Field39.Left = 3.875F;
            this.Field39.Name = "Field39";
            this.Field39.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-style: italic; font-si" +
                "ze: 12pt; font-family: Arial; ";
            this.Field39.Text = "EARLY_PAY_DATE2";
            this.Field39.Top = 4.9375F;
            this.Field39.Width = 2.3125F;
            // 
            // Field40
            // 
            this.Field40.Border.BottomColor = System.Drawing.Color.Black;
            this.Field40.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field40.Border.LeftColor = System.Drawing.Color.Black;
            this.Field40.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field40.Border.RightColor = System.Drawing.Color.Black;
            this.Field40.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field40.Border.TopColor = System.Drawing.Color.Black;
            this.Field40.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field40.DataField = "EARLY_PAY_PREMIUM";
            this.Field40.Height = 0.1875F;
            this.Field40.Left = 2.375F;
            this.Field40.Name = "Field40";
            this.Field40.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 12pt; font-famil" +
                "y: Arial; ";
            this.Field40.Text = "EARLY_PAY_PREMIUM";
            this.Field40.Top = 5.3125F;
            this.Field40.Width = 1.375F;
            // 
            // Label17
            // 
            this.Label17.Border.BottomColor = System.Drawing.Color.Black;
            this.Label17.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label17.Border.LeftColor = System.Drawing.Color.Black;
            this.Label17.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label17.Border.RightColor = System.Drawing.Color.Black;
            this.Label17.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label17.Border.TopColor = System.Drawing.Color.Black;
            this.Label17.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = "";
            this.Label17.Left = 0.0625F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "";
            this.Label17.Text = "$$";
            this.Label17.Top = 4.9375F;
            this.Label17.Width = 0.25F;
            // 
            // Label18
            // 
            this.Label18.Border.BottomColor = System.Drawing.Color.Black;
            this.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.LeftColor = System.Drawing.Color.Black;
            this.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.RightColor = System.Drawing.Color.Black;
            this.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.TopColor = System.Drawing.Color.Black;
            this.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = "";
            this.Label18.Left = 0.0625F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "";
            this.Label18.Text = "$$";
            this.Label18.Top = 5.125F;
            this.Label18.Width = 0.25F;
            // 
            // Label19
            // 
            this.Label19.Border.BottomColor = System.Drawing.Color.Black;
            this.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.LeftColor = System.Drawing.Color.Black;
            this.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.RightColor = System.Drawing.Color.Black;
            this.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.TopColor = System.Drawing.Color.Black;
            this.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = "";
            this.Label19.Left = 0.0625F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "";
            this.Label19.Text = "$$";
            this.Label19.Top = 5.3125F;
            this.Label19.Width = 0.25F;
            // 
            // Label20
            // 
            this.Label20.Border.BottomColor = System.Drawing.Color.Black;
            this.Label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.LeftColor = System.Drawing.Color.Black;
            this.Label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.RightColor = System.Drawing.Color.Black;
            this.Label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.TopColor = System.Drawing.Color.Black;
            this.Label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = "";
            this.Label20.Left = 6.25F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "";
            this.Label20.Text = "$$";
            this.Label20.Top = 5.125F;
            this.Label20.Width = 0.25F;
            // 
            // Label21
            // 
            this.Label21.Border.BottomColor = System.Drawing.Color.Black;
            this.Label21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label21.Border.LeftColor = System.Drawing.Color.Black;
            this.Label21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label21.Border.RightColor = System.Drawing.Color.Black;
            this.Label21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label21.Border.TopColor = System.Drawing.Color.Black;
            this.Label21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label21.Height = 0.1875F;
            this.Label21.HyperLink = "";
            this.Label21.Left = 6.25F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "";
            this.Label21.Text = "$$";
            this.Label21.Top = 5.3125F;
            this.Label21.Width = 0.25F;
            // 
            // Label22
            // 
            this.Label22.Border.BottomColor = System.Drawing.Color.Black;
            this.Label22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label22.Border.LeftColor = System.Drawing.Color.Black;
            this.Label22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label22.Border.RightColor = System.Drawing.Color.Black;
            this.Label22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label22.Border.TopColor = System.Drawing.Color.Black;
            this.Label22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label22.Height = 0.1875F;
            this.Label22.HyperLink = "";
            this.Label22.Left = 6.25F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "";
            this.Label22.Text = "$$";
            this.Label22.Top = 4.9375F;
            this.Label22.Width = 0.25F;
            // 
            // Label23
            // 
            this.Label23.Border.BottomColor = System.Drawing.Color.Black;
            this.Label23.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Border.LeftColor = System.Drawing.Color.Black;
            this.Label23.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Border.RightColor = System.Drawing.Color.Black;
            this.Label23.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Border.TopColor = System.Drawing.Color.Black;
            this.Label23.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Height = 0.1875F;
            this.Label23.HyperLink = "";
            this.Label23.Left = 0.0625F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-size: 10pt; ";
            this.Label23.Text = "To insure proper handling, please return a copy of this invoice with your check.";
            this.Label23.Top = 5.8125F;
            this.Label23.Width = 5.3125F;
            // 
            // Field42
            // 
            this.Field42.Border.BottomColor = System.Drawing.Color.Black;
            this.Field42.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.Border.LeftColor = System.Drawing.Color.Black;
            this.Field42.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.Border.RightColor = System.Drawing.Color.Black;
            this.Field42.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.Border.TopColor = System.Drawing.Color.Black;
            this.Field42.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.DataField = "DUE_DATE";
            this.Field42.Height = 0.1875F;
            this.Field42.Left = 4.875F;
            this.Field42.Name = "Field42";
            this.Field42.Style = "ddo-char-set: 0; text-align: left; font-size: 12pt; font-family: Arial; ";
            this.Field42.Text = "DUE_DATE";
            this.Field42.Top = 0.5F;
            this.Field42.Width = 1.375F;
            // 
            // Field43
            // 
            this.Field43.Border.BottomColor = System.Drawing.Color.Black;
            this.Field43.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.Border.LeftColor = System.Drawing.Color.Black;
            this.Field43.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.Border.RightColor = System.Drawing.Color.Black;
            this.Field43.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.Border.TopColor = System.Drawing.Color.Black;
            this.Field43.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.DataField = "TERM_DATES";
            this.Field43.Height = 0.1875F;
            this.Field43.Left = 3F;
            this.Field43.Name = "Field43";
            this.Field43.Style = "ddo-char-set: 0; text-align: left; font-size: 12pt; font-family: Arial; ";
            this.Field43.Text = "TERM_DATES";
            this.Field43.Top = 1.0625F;
            this.Field43.Width = 2F;
            // 
            // Field44
            // 
            this.Field44.Border.BottomColor = System.Drawing.Color.Black;
            this.Field44.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.Border.LeftColor = System.Drawing.Color.Black;
            this.Field44.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.Border.RightColor = System.Drawing.Color.Black;
            this.Field44.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.Border.TopColor = System.Drawing.Color.Black;
            this.Field44.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.DataField = "INVOICE_NUMBER";
            this.Field44.Height = 0.1875F;
            this.Field44.Left = 1.5F;
            this.Field44.Name = "Field44";
            this.Field44.Style = "ddo-char-set: 0; text-align: left; font-size: 12pt; font-family: Arial; ";
            this.Field44.Text = "INVOICE_NUMBER";
            this.Field44.Top = 0.5F;
            this.Field44.Width = 1.375F;
            // 
            // Field45
            // 
            this.Field45.Border.BottomColor = System.Drawing.Color.Black;
            this.Field45.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field45.Border.LeftColor = System.Drawing.Color.Black;
            this.Field45.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field45.Border.RightColor = System.Drawing.Color.Black;
            this.Field45.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field45.Border.TopColor = System.Drawing.Color.Black;
            this.Field45.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field45.DataField = "AMOUNT1";
            this.Field45.Height = 0.1875F;
            this.Field45.Left = 4F;
            this.Field45.Name = "Field45";
            this.Field45.Style = "ddo-char-set: 0; text-align: right; font-size: 12pt; font-family: Arial; ";
            this.Field45.Text = "AMOUNT1";
            this.Field45.Top = 1.6875F;
            this.Field45.Width = 1.375F;
            // 
            // Field46
            // 
            this.Field46.Border.BottomColor = System.Drawing.Color.Black;
            this.Field46.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.Border.LeftColor = System.Drawing.Color.Black;
            this.Field46.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.Border.RightColor = System.Drawing.Color.Black;
            this.Field46.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.Border.TopColor = System.Drawing.Color.Black;
            this.Field46.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.DataField = "AMOUNT2";
            this.Field46.Height = 0.1875F;
            this.Field46.Left = 4F;
            this.Field46.Name = "Field46";
            this.Field46.Style = "ddo-char-set: 0; text-align: right; font-weight: bold; font-size: 12pt; font-fami" +
                "ly: Arial; ";
            this.Field46.Text = "AMOUNT2";
            this.Field46.Top = 2.0625F;
            this.Field46.Width = 1.375F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Field28,
            this.Field29,
            this.Field30,
            this.Field33,
            this.Label1,
            this.Field34,
            this.Field47});
            this.PageHeader.Height = 1.790972F;
            this.PageHeader.Name = "PageHeader";
            // 
            // Field28
            // 
            this.Field28.Border.BottomColor = System.Drawing.Color.Black;
            this.Field28.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.Border.LeftColor = System.Drawing.Color.Black;
            this.Field28.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.Border.RightColor = System.Drawing.Color.Black;
            this.Field28.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.Border.TopColor = System.Drawing.Color.Black;
            this.Field28.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.DataField = "INSURER_NAME";
            this.Field28.Height = 0.1875F;
            this.Field28.Left = 0.9375F;
            this.Field28.Name = "Field28";
            this.Field28.Style = "ddo-char-set: 178; text-align: center; font-weight: bold; font-size: 12pt; font-f" +
                "amily: Arial; ";
            this.Field28.Text = "INSURER_NAME";
            this.Field28.Top = 0.375F;
            this.Field28.Width = 4.5625F;
            // 
            // Field29
            // 
            this.Field29.Border.BottomColor = System.Drawing.Color.Black;
            this.Field29.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.Border.LeftColor = System.Drawing.Color.Black;
            this.Field29.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.Border.RightColor = System.Drawing.Color.Black;
            this.Field29.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.Border.TopColor = System.Drawing.Color.Black;
            this.Field29.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.DataField = "INSURER_ADDR1";
            this.Field29.Height = 0.1875F;
            this.Field29.Left = 0.9375F;
            this.Field29.Name = "Field29";
            this.Field29.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field29.Text = "INSURER_ADDR1";
            this.Field29.Top = 0.5625F;
            this.Field29.Width = 4.5625F;
            // 
            // Field30
            // 
            this.Field30.Border.BottomColor = System.Drawing.Color.Black;
            this.Field30.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.Border.LeftColor = System.Drawing.Color.Black;
            this.Field30.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.Border.RightColor = System.Drawing.Color.Black;
            this.Field30.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.Border.TopColor = System.Drawing.Color.Black;
            this.Field30.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.DataField = "INSURER_ADDR3";
            this.Field30.Height = 0.1875F;
            this.Field30.Left = 0.9375F;
            this.Field30.Name = "Field30";
            this.Field30.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field30.Text = "INSURER_ADDR3";
            this.Field30.Top = 0.9375F;
            this.Field30.Width = 4.5625F;
            // 
            // Field33
            // 
            this.Field33.Border.BottomColor = System.Drawing.Color.Black;
            this.Field33.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.Border.LeftColor = System.Drawing.Color.Black;
            this.Field33.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.Border.RightColor = System.Drawing.Color.Black;
            this.Field33.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.Border.TopColor = System.Drawing.Color.Black;
            this.Field33.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.DataField = "INSURER_ADDR2";
            this.Field33.Height = 0.1875F;
            this.Field33.Left = 0.9375F;
            this.Field33.Name = "Field33";
            this.Field33.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field33.Text = "INSURER_ADDR2";
            this.Field33.Top = 0.75F;
            this.Field33.Width = 4.5625F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.3125F;
            this.Label1.HyperLink = "";
            this.Label1.Left = 0.9375F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "text-align: center; font-weight: bold; font-size: 18pt; ";
            this.Label1.Text = "** INVOICE **";
            this.Label1.Top = 0F;
            this.Label1.Width = 4.5625F;
            // 
            // Field34
            // 
            this.Field34.Border.BottomColor = System.Drawing.Color.Black;
            this.Field34.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.Border.LeftColor = System.Drawing.Color.Black;
            this.Field34.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.Border.RightColor = System.Drawing.Color.Black;
            this.Field34.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.Border.TopColor = System.Drawing.Color.Black;
            this.Field34.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.DataField = "DATE";
            this.Field34.Height = 0.188F;
            this.Field34.Left = 2.1875F;
            this.Field34.Name = "Field34";
            this.Field34.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field34.Text = "DATE";
            this.Field34.Top = 1.125F;
            this.Field34.Width = 2F;
            // 
            // Field47
            // 
            this.Field47.Border.BottomColor = System.Drawing.Color.Black;
            this.Field47.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.Border.LeftColor = System.Drawing.Color.Black;
            this.Field47.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.Border.RightColor = System.Drawing.Color.Black;
            this.Field47.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.Border.TopColor = System.Drawing.Color.Black;
            this.Field47.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.DataField = "POLICY_NAME";
            this.Field47.Height = 0.1875F;
            this.Field47.Left = 0.9375F;
            this.Field47.Name = "Field47";
            this.Field47.Style = "ddo-char-set: 178; text-align: center; font-weight: bold; font-size: 12pt; font-f" +
                "amily: Arial; ";
            this.Field47.Text = "POLICY_NAME";
            this.Field47.Top = 1.3125F;
            this.Field47.Width = 4.5625F;
            // 
            // PageFooter
            // 
            this.PageFooter.CanGrow = false;
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field10,
            this.Field11,
            this.Field41});
            this.PageFooter.Height = 1.239583F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Field1
            // 
            this.Field1.Border.BottomColor = System.Drawing.Color.Black;
            this.Field1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.Border.LeftColor = System.Drawing.Color.Black;
            this.Field1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.Border.RightColor = System.Drawing.Color.Black;
            this.Field1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.Border.TopColor = System.Drawing.Color.Black;
            this.Field1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.DataField = "INSURED_NAME";
            this.Field1.Height = 0.125F;
            this.Field1.Left = 0.25F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "ddo-char-set: 0; font-weight: bold; font-size: 8.5pt; font-family: Microsoft Sans" +
                " Serif; ";
            this.Field1.Text = "INSURED_NAME";
            this.Field1.Top = 0.188F;
            this.Field1.Width = 3.0625F;
            // 
            // Field2
            // 
            this.Field2.Border.BottomColor = System.Drawing.Color.Black;
            this.Field2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.Border.LeftColor = System.Drawing.Color.Black;
            this.Field2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.Border.RightColor = System.Drawing.Color.Black;
            this.Field2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.Border.TopColor = System.Drawing.Color.Black;
            this.Field2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.DataField = "INSURED_ATTN";
            this.Field2.Height = 0.125F;
            this.Field2.Left = 0.25F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field2.Text = "INSURED_ATTN";
            this.Field2.Top = 0.313F;
            this.Field2.Width = 3.0625F;
            // 
            // Field3
            // 
            this.Field3.Border.BottomColor = System.Drawing.Color.Black;
            this.Field3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.Border.LeftColor = System.Drawing.Color.Black;
            this.Field3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.Border.RightColor = System.Drawing.Color.Black;
            this.Field3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.Border.TopColor = System.Drawing.Color.Black;
            this.Field3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.DataField = "INSURED_ADDR1";
            this.Field3.Height = 0.125F;
            this.Field3.Left = 0.25F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field3.Text = "INSURED_ADDR1";
            this.Field3.Top = 0.438F;
            this.Field3.Width = 3.0625F;
            // 
            // Field4
            // 
            this.Field4.Border.BottomColor = System.Drawing.Color.Black;
            this.Field4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.Border.LeftColor = System.Drawing.Color.Black;
            this.Field4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.Border.RightColor = System.Drawing.Color.Black;
            this.Field4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.Border.TopColor = System.Drawing.Color.Black;
            this.Field4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.DataField = "INSURED_CITY";
            this.Field4.Height = 0.125F;
            this.Field4.Left = 0.25F;
            this.Field4.Name = "Field4";
            this.Field4.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field4.Text = "INSURED_CITY";
            this.Field4.Top = 0.688F;
            this.Field4.Width = 1.25F;
            // 
            // Field10
            // 
            this.Field10.Border.BottomColor = System.Drawing.Color.Black;
            this.Field10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.Border.LeftColor = System.Drawing.Color.Black;
            this.Field10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.Border.RightColor = System.Drawing.Color.Black;
            this.Field10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.Border.TopColor = System.Drawing.Color.Black;
            this.Field10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.DataField = "INSURED_STATE";
            this.Field10.Height = 0.125F;
            this.Field10.Left = 1.563F;
            this.Field10.Name = "Field10";
            this.Field10.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field10.Text = "INSURED_STATE";
            this.Field10.Top = 0.688F;
            this.Field10.Width = 0.375F;
            // 
            // Field11
            // 
            this.Field11.Border.BottomColor = System.Drawing.Color.Black;
            this.Field11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.Border.LeftColor = System.Drawing.Color.Black;
            this.Field11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.Border.RightColor = System.Drawing.Color.Black;
            this.Field11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.Border.TopColor = System.Drawing.Color.Black;
            this.Field11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.DataField = "INSURED_ZIPCODE";
            this.Field11.Height = 0.125F;
            this.Field11.Left = 2F;
            this.Field11.Name = "Field11";
            this.Field11.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field11.Text = "INSURED_ZIPCODE";
            this.Field11.Top = 0.688F;
            this.Field11.Width = 0.875F;
            // 
            // Field41
            // 
            this.Field41.Border.BottomColor = System.Drawing.Color.Black;
            this.Field41.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.Border.LeftColor = System.Drawing.Color.Black;
            this.Field41.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.Border.RightColor = System.Drawing.Color.Black;
            this.Field41.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.Border.TopColor = System.Drawing.Color.Black;
            this.Field41.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.DataField = "INSURED_ADDR2";
            this.Field41.Height = 0.125F;
            this.Field41.Left = 0.25F;
            this.Field41.Name = "Field41";
            this.Field41.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field41.Text = "INSURED_ADDR2";
            this.Field41.Top = 0.563F;
            this.Field41.Width = 3.0625F;
            // 
            // PCFirstInvoice
            // 
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.PolicyBilling_FetchData);
            this.ReportStart += new System.EventHandler(this.PolicyBilling_ReportStart);
            this.DataInitialize += new System.EventHandler(this.PolicyBilling_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		 }

		#endregion
	}
}
