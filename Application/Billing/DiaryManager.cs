﻿
using System;
using System.IO ;
using System.Xml ;
using System.Text ;
using System.Collections ;
using Riskmaster.Common ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.DataModel ;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/// <summary>
	/// Summary description for DiaryManager.
	/// </summary>
	public class DiaryManager : IDisposable
	{
		#region Member Variables
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty ;	
		/// <summary>
		/// Private variable to store the instance of Local Cache object
		/// </summary>
		private LocalCache m_objLocalCache = null ;
        private int m_iClientId = 0;
		#endregion 		

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value, and Initialize the Report.
		/// </summary>
		/// <param name="p_objDataModelFactory">DataModelFactory Object</param>
        public DiaryManager(DataModelFactory p_objDataModelFactory, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_objDataModelFactory = p_objDataModelFactory ;
			m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString ;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
		}
		#endregion 
		
        public void Dispose()
        {
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }

        private WpaDiaryEntry AddWpaDiaryEntry(string p_sEntryName, string p_sEntryNotes, string p_sCompleteDate, int p_iAttachRecordId, int p_iAttSecRecId, string p_sRegarding, string p_sActShortCode, string p_sAttachTable, BillingMaster p_objBillingMaster)
		{
			WpaDiaryEntry objWpaDiaryEntry = null;
			WpaDiaryAct objWpaDiaryAct = null ;
			
			string sActTextCodeDesc = string.Empty ;
			string sActTextShortCode = string.Empty ;

			try
			{
				objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
			
				objWpaDiaryEntry.EntryName = p_sEntryName ;
				objWpaDiaryEntry.EntryNotes = p_sEntryNotes ;
                objWpaDiaryEntry.CreateDate = Conversion.GetDateTime(DateTime.Now.ToString("d")); // TODO
			
				if( p_sCompleteDate.Substring( 1 , 1) == "/" || p_sCompleteDate.Substring( 2, 1) == "/"  )
					objWpaDiaryEntry.CompleteDate = Common.Conversion.GetDate( p_sCompleteDate );
				else
					objWpaDiaryEntry.CompleteDate = p_sCompleteDate ;

				objWpaDiaryEntry.Priority = 2 ;
				objWpaDiaryEntry.StatusOpen = true ;
				objWpaDiaryEntry.AutoConfirm = false ;
				objWpaDiaryEntry.AssignedUser = "csc" ; 
				objWpaDiaryEntry.AssigningUser = "SYSTEM" ;
				objWpaDiaryEntry.IsAttached = true ;
				objWpaDiaryEntry.TeTracked = false ;						
				objWpaDiaryEntry.AttachRecordid = p_iAttachRecordId ;
				objWpaDiaryEntry.AttachTable = p_sAttachTable ;
				objWpaDiaryEntry.AttSecRecId = p_iAttSecRecId ;
				objWpaDiaryEntry.Regarding = p_sRegarding;
                //Indu - Adding att_parent_code for Diary enhancement - Mits 33843
                string scSQL = string.Empty;
                DbReader objRead = null;
                scSQL = "SELECT POLICY_ID FROM POLICY_ENH WHERE POLICY_ID =" + p_iAttachRecordId;
                objRead = DbFactory.GetDbReader(m_sConnectionString, scSQL);
                if (objRead != null)
                {
                    while (objRead.Read())
                    {
                        objWpaDiaryEntry.AttParentCode = Conversion.ConvertObjToInt(objRead.GetInt32("POLICY_ID"), m_iClientId);
                    }
                    objRead.Close();
                    objRead.Dispose();
                }
				//End of Mits 33843		
				objWpaDiaryAct = ( WpaDiaryAct ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryAct" , false ); 
				objWpaDiaryAct.ActCode = m_objLocalCache.GetCodeId( p_sActShortCode , "AUTODIARY_ACT_CODE" );
			
				m_objLocalCache.GetCodeInfo( objWpaDiaryAct.ActCode , ref sActTextShortCode , ref sActTextCodeDesc );
				objWpaDiaryAct.ActText = p_sActShortCode + " - " + sActTextCodeDesc ;

                // Naresh Changed as per RMWorld
                objWpaDiaryEntry.WpaDiaryActList.Add( objWpaDiaryAct );     
				
				if( p_objBillingMaster == null )
				{
					// For Schedule Credit Diaries.
					objWpaDiaryEntry.Save();
				}
				else
				{
                    // Naresh Changed as Per RMWorld
                    objWpaDiaryEntry.EntryId = Utilities.GetNextUID(m_sConnectionString, "WPA_DIARY_ENTRY", m_iClientId);
                    
					p_objBillingMaster.Diaries.Add( objWpaDiaryEntry.EntryId , objWpaDiaryEntry );
				}
                return objWpaDiaryEntry;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.AddWpaDiaryEntry.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				if( objWpaDiaryAct != null )
				{
					objWpaDiaryAct.Dispose();
					objWpaDiaryAct = null ;
				}
			}
            return null;
		}

		
		private string GetDiaryCompletionDate( int p_iIndex, string p_sCompletionDate, int p_iMaxInstallNum, PayPlan p_objPayPlan, BillingRule p_objBillingRule, BillXAccount p_objBillXAccount )
		{
			string sCompletionDate = string.Empty ;

			try
			{
				if( p_objBillXAccount.PayPlanRowid == 0 || p_iIndex > 1 )
					sCompletionDate = Function.AddDays( p_sCompletionDate , p_objBillingRule.DueDays );							
				else
				{
					if( m_objLocalCache.GetShortCode( p_objPayPlan.InstallGenType ) == "F" )
						sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.FixedDueDays );	
					else
					{
						switch( p_iMaxInstallNum )
						{
							case 1 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install1DueDays );
								break ;
							case 2 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install2DueDays );
								break ;
							case 3 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install3DueDays );
								break ;
							case 4 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install4DueDays );
								break ;
							case 5 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install5DueDays );
								break ;
							case 6 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install6DueDays );
								break ;
							case 7 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install7DueDays );
								break ;
							case 8 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install8DueDays );
								break ;
							case 9 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install9DueDays );
								break ;
							case 10 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install10DueDays );
								break ;
							case 11 :
								sCompletionDate = Function.AddDays( p_sCompletionDate, p_objPayPlan.Install11DueDays );
								break ;
							case 12 :
								sCompletionDate = Function.AddDays( p_sCompletionDate , p_objPayPlan.Install12DueDays );
								break ;									
						}
					}
				}
				return( sCompletionDate );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.GetDiaryCompletionDate.Error", m_iClientId), p_objEx);				
			}					
		}

		
		private void FillNoticeDiaryDetails( BillingMaster p_objBillingMaster , int p_iPolicyId , int p_iTermNumber, string p_sDate , int p_iBiRowId , string p_sName )
		{
			PolicyEnh objPolicyEnh = null;
			WpaDiaryEntry objWpaDiaryEntry = null ;
			DbReader objReader = null ;
			bool bPolicyFoundFlag = false ;
			string sSQL = string.Empty ;
			string sActTextCodeDesc = string.Empty ;
			string sActTextShortCode = string.Empty ;
			string sRegarding = string.Empty ;
			string sEntryName = string.Empty ;
			string sEntryNotes = string.Empty ;

			try
			{
				sEntryName = "Print Notice" ;
				sEntryNotes = "Auto-Generate/Print Notice" ;
				if( p_sName == "" )
				{
					foreach( int iKey in p_objBillingMaster.Policies.Keys )
					{
						objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
						if( objPolicyEnh.PolicyId == p_iPolicyId )
						{
							p_sName = objPolicyEnh.PolicyName ;
							bPolicyFoundFlag = true ;
							break ;
						}
					}
				}				
				if( !bPolicyFoundFlag )
				{
					sSQL = " SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID=" + p_iPolicyId ;
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader.Read() )
					{
						p_sName = objReader.GetString( "POLICY_NAME" ) ;
					}
					objReader.Close();
				}
				sRegarding = "Notice due for policy:" + p_sName + " Term Number: " + p_iTermNumber ;
				
				this.AddWpaDiaryEntry( sEntryName , sEntryNotes , p_sDate , p_iPolicyId , p_iBiRowId , sRegarding , "PN" , "POLICY_ENH", p_objBillingMaster );							
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.FillNoticeDiaryDetails.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}				
			}
		}


		private void FillNoticeDiaryDetails( BillingMaster p_objBillingMaster , int p_iPolicyId , int p_iTermNumber, string p_sDate , int p_iBiRowId )
		{
			this.FillNoticeDiaryDetails( p_objBillingMaster , p_iPolicyId , p_iTermNumber , p_sDate , p_iBiRowId , "" );
		}


		#region Functions to Schedule Diaries ( Cancel , CancelPending , Notice , Rebill, Invoice, Installment , Credit )
		
		public void ScheduleCancelDiary( BillXBillItem  p_objBillXBillItem, BillingRule p_objBillingRule, BillingMaster p_objBillingMaster )
		{
			WpaDiaryEntry objWpaDiaryEntry = null ;
			PolicyEnh objPolicyEnh = null ;
			WpaDiaryAct objWpaDiaryAct = null ;

			string sDueDate = string.Empty ;
			string sName = string.Empty ;
			string sActTextCodeDesc = string.Empty ;
			string sActTextShortCode = string.Empty ;
			string sRegarding = string.Empty ;
			string sEntryNotes = string.Empty ;

			try
			{
				// Calcuate when the complete date (date diary will run) based on CANCEL_PEND_DIARY code
				//switch( m_objLocalCache.GetShortCode( p_objBillingRule.CancelPendDiary ) )     csingh7 for MITS 12578
				switch( m_objLocalCache.GetShortCode( p_objBillingRule.CancelDiary ) )
                {
					case "1" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate, Conversion.ConvertStrToInteger( p_objBillingRule.FirstNoticeDays ) );
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.FirstNoticeDays ) ) ;
						break;
					case "2" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger( p_objBillingRule.SecondNoticeDays ) );
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.SecondNoticeDays ) ) ;
						break;
					case "3" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger( p_objBillingRule.ThirdNoticeDays ) );
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.ThirdNoticeDays ) ) ;
						break;
					case "4" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger( p_objBillingRule.FourthNoticeDays ) );
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.FourthNoticeDays ) ) ;
						break;
					default :
						sDueDate = Common.Conversion.GetDate( DateTime.Now.ToString("d") ); // TODO
						break;
				}

				sEntryNotes = "Premium is past due for the policy; please review policy and cancel, if necessary" ;
				foreach( int iKey in p_objBillingMaster.Policies.Keys )
				{
					objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
					if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
					{
						sName = objPolicyEnh.PolicyName ;
						break;
					}
				}

				if( sName == "" )
					sName = Function.GetPolicyName( p_objBillXBillItem.PolicyId , m_sConnectionString,m_iClientId) ;
				sRegarding = "Past due amount for Policy:" + sName ;
				
				this.AddWpaDiaryEntry( "Cancel Policy" , sEntryNotes , sDueDate , p_objBillXBillItem.PolicyId , p_objBillXBillItem.BillingItemRowid , sRegarding , "PCP" , "POLICY_ENH", p_objBillingMaster );				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.ScheduleCancelDiary.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objWpaDiaryAct != null )
				{
					objWpaDiaryAct.Dispose();
					objWpaDiaryAct = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
			}
		}


		public void ScheduleCancelPendingDiary( BillXBillItem  p_objBillXBillItem, BillingRule p_objBillingRule, BillingMaster p_objBillingMaster )
		{
			WpaDiaryEntry objWpaDiaryEntry = null ;
			PolicyEnh objPolicyEnh = null ;
			WpaDiaryAct objWpaDiaryAct = null ;

			string sDueDate = string.Empty ;
			string sName = string.Empty ;
			string sRegarding = string.Empty ;
			string sEntryNotes = string.Empty ;

			try
			{
				// Calcuate when the complete date (date diary will run) based on CANCEL_PEND_DIARY code
				switch( m_objLocalCache.GetShortCode( p_objBillingRule.CancelPendDiary ) )
				{
					case "1" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate, Conversion.ConvertStrToInteger( p_objBillingRule.FirstNoticeDays ));
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger(p_objBillingRule.FirstNoticeDays )) ;
						break;
					case "2" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger(p_objBillingRule.SecondNoticeDays ));
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger(p_objBillingRule.SecondNoticeDays )) ;
						break;
					case "3" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger(p_objBillingRule.ThirdNoticeDays ));
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger(p_objBillingRule.ThirdNoticeDays )) ;
						break;
					case "4" :
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sDueDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger(p_objBillingRule.FourthNoticeDays ));
						else
							sDueDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.FourthNoticeDays ) ) ;
						break;
					default :
						sDueDate = Common.Conversion.GetDate( DateTime.Now.ToString("d") ); // TODO
						break;
				}

				foreach( int iKey in p_objBillingMaster.Policies.Keys )
				{
					objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
					if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
					{
						sName = objPolicyEnh.PolicyName ;
						break;
					}
				}
				if( sName == "" )
					sName = Function.GetPolicyName( p_objBillXBillItem.PolicyId , m_sConnectionString,m_iClientId ) ;
				sRegarding = "Past due amount for Policy:" + sName ;		
						
				this.AddWpaDiaryEntry( "Provisionally Cancel Policy" , sEntryNotes , sDueDate , p_objBillXBillItem.PolicyId , p_objBillXBillItem.BillingItemRowid , sRegarding , "PCP" , "POLICY_ENH", p_objBillingMaster );				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.ScheduleCancelPendingDiary.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objWpaDiaryAct != null )
				{
					objWpaDiaryAct.Dispose();
					objWpaDiaryAct = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
			}
		}

		
		public void ScheduleNoticeDiaries( BillXBillItem  p_objBillXBillItem, BillingRule p_objBillingRule, BillingMaster p_objBillingMaster )
		{
			bool bBiFoundFlag = false ;
			int iTermNumber = 0 ;
			int iBiRowId = 0 ;
			int iPolicyId = 0 ;
			string sInstallDate = string.Empty ;
            
			try
			{
				// We should only be printing if there is a premium item or underpayment on the policy
				if( p_objBillXBillItem.Amount > 0 &&  m_objLocalCache.GetShortCode( p_objBillXBillItem.BillingItemType ) == "P" 
					&& (m_objLocalCache.GetShortCode( p_objBillXBillItem.BillXPremItem.PremiumItemType ) == "FB"  
					|| m_objLocalCache.GetShortCode( p_objBillXBillItem.BillXPremItem.PremiumItemType ) == "IN" ))
				{
					bBiFoundFlag = true ;
					iTermNumber = p_objBillXBillItem.TermNumber ;
					iBiRowId = p_objBillXBillItem.BillingItemRowid ;
					iPolicyId = p_objBillXBillItem.PolicyId ;
				}
				
				if( bBiFoundFlag )
				{
					if( p_objBillingRule.FirstNoticeDays != "" )
					{
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sInstallDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger( p_objBillingRule.FirstNoticeDays ) ) ;
						else
							sInstallDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.FirstNoticeDays ) ) ;
						this.FillNoticeDiaryDetails( p_objBillingMaster, iPolicyId, iTermNumber, sInstallDate, iBiRowId );
					}
					if( p_objBillingRule.SecondNoticeDays != "" )
					{
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sInstallDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger( p_objBillingRule.SecondNoticeDays ) ) ;
						else
							sInstallDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.SecondNoticeDays ) ) ;
						this.FillNoticeDiaryDetails( p_objBillingMaster, iPolicyId, iTermNumber, sInstallDate, iBiRowId );
					}
					if( p_objBillingRule.ThirdNoticeDays != "" )
					{
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sInstallDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger( p_objBillingRule.ThirdNoticeDays ) ) ;
						else
							sInstallDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.ThirdNoticeDays ) ) ;
						this.FillNoticeDiaryDetails( p_objBillingMaster, iPolicyId, iTermNumber, sInstallDate, iBiRowId );
					}
					if( p_objBillingRule.FourthNoticeDays != "" )
					{
						if( m_objLocalCache.GetShortCode( p_objBillingRule.NoticeOffsetInd ) == "D" )
							sInstallDate = Function.AddDays( p_objBillXBillItem.DueDate , Conversion.ConvertStrToInteger( p_objBillingRule.FourthNoticeDays ) ) ;
						else
							sInstallDate = Function.AddDays( p_objBillXBillItem.DateEntered , Conversion.ConvertStrToInteger( p_objBillingRule.FourthNoticeDays ) ) ;
						this.FillNoticeDiaryDetails( p_objBillingMaster, iPolicyId, iTermNumber, sInstallDate, iBiRowId );
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.ScheduleNoticeDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
								
			}
		}


		public void ScheduleRebillDiaries( WpaDiaryEntry p_objInvoiceDiary , BillXInstlmnt p_objBillXInstlmnt , BillingMaster p_objBillingMaster )
		{
			// The Term Number will be stored in the AttSecRecId field on the table.
			
			PolicyEnh objPolicyEnh = null ;
			BillXBillItem objBillXBillItem = null ;
			BillXAccount objBillXAccount = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			PayPlan objPayPlan = null ;
			BillingRule objBillingRule = null; 
			WpaDiaryEntry objWpaDiaryEntry = null ;
			DbReader objReader = null ;
			WpaDiaryAct objWpaDiaryAct = null ;
			BillingManager objBillingManager = null ;


			int iPolicyId = 0 ;
			int iTermNumber = 0 ;
			string sName = string.Empty ;
			bool bBiFound = false ;
			int iBiRowId = 0 ;
			bool bAccountFound = false ;
			string sSQL = string.Empty ;
			int iMaxInstallNum = 0 ;
			bool bContinue = false ;
			int iIndex = 0 ;
			string sRegarding = string.Empty ;
			string sCompletionDate = string.Empty ;

			try
			{
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);
				objBillXInstlmnt = ( BillXInstlmnt ) m_objDataModelFactory.GetDataModelObject( "BillXInstlmnt" , false );
                //pmahli MITS 9679  Added as otherwise it was giving error "object instance not set to an instance of the object"
                objBillXAccount = (BillXAccount)m_objDataModelFactory.GetDataModelObject("BillXAccount", false);
                objPayPlan = new PayPlan();

				if( p_objBillXInstlmnt != null )
				{
					iPolicyId = p_objBillXInstlmnt.PolicyId ;
					iTermNumber = p_objBillXInstlmnt.TermNumber ;
				}
				else
				{
					foreach( int iKey in p_objBillingMaster.Policies.Keys )
					{
						objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
						if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
						{
							iPolicyId = objPolicyEnh.PolicyId ;
							sName = objPolicyEnh.PolicyName ;
							break;
						}
					}
				}

				foreach( int iKey in p_objBillingMaster.BillingItems.Keys )
				{
					objBillXBillItem = ( BillXBillItem ) p_objBillingMaster.BillingItems[iKey] ;
                    //Merging changed done by Shruti for Rebill diaires mits 11619
                    //dont know why this check was here, rebill diaries are meant for the case
                    //where there is no PP.In this case the whole amount comes under a single billing item,
                    //and the billing item is of type 'P' always.
                    //if( m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P" && objBillXBillItem.PolicyId == iPolicyId && ( m_objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "FB" || m_objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "IN" || m_objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "EN" || m_objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "RP" ) )
                    //    return ;
                    //else
                    //{
						if( objBillXBillItem.PolicyId == iPolicyId && objBillXBillItem.Amount > 0 && (( m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P" ) || (( m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "A" ) && ( m_objLocalCache.GetShortCode( objBillXBillItem.BillXAdjstmnt.AdjustmentType ) == "UP")))) 
						{
							bBiFound = true ;
							iBiRowId = objBillXBillItem.BillingItemRowid ;
							iTermNumber = objBillXBillItem.TermNumber ;
							break;
						}
					//}
				}

				if( !bBiFound )
					return ;

                // Naresh Changed as Per RMWorld
				if( p_objBillingMaster.Account.BillAccountRowid != 0 )
				{
					if( p_objBillingMaster.Account.PolicyId == iPolicyId && p_objBillingMaster.Account.TermNumber == iTermNumber )
					{
						bAccountFound = true ;
						objBillXAccount = p_objBillingMaster.Account ;
					}
				}

                // Naresh Changed as per RMWorld
				if( !bAccountFound )
				{
					sSQL = " SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID =" + iPolicyId + " AND TERM_NUMBER =" + iTermNumber ;
					objBillingManager.FillAccount( objBillXAccount , sSQL );
				}

				if( objBillXAccount.PayPlanRowid != 0 )
				{
					sSQL = " SELECT MAX(INSTALLMENT_NUM) FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER=" + iTermNumber ;
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader.Read() )
					{
						iMaxInstallNum = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId ); ;
					}
					objReader.Close();

					if( iMaxInstallNum != 0 )
					{
						sSQL = "SELECT * FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + iPolicyId + 
							" AND TERM_NUMBER=" + iTermNumber + " AND INSTALLMENT_NUM=" + iMaxInstallNum ;
						
						objBillingManager.FillInstallment( objBillXInstlmnt , sSQL );
						if( objBillXInstlmnt.GeneratedFlag )
						{
							sSQL = " SELECT * FROM SYS_BILL_PAY_PLAN WHERE PAY_PLAN_ROWID =" + objBillXAccount.PayPlanRowid ;
							objBillingManager.FillPayPlan( objPayPlan, sSQL );
							bContinue = true ;
						}
						else
							return ;
					}
				}

				if( objBillXAccount.PayPlanRowid == 0 || bContinue )
				{
					// Read for the rebill settings
					objBillingRule = new BillingRule();

					sSQL = " SELECT * FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID = " + objBillXAccount.BillingRuleRowid ;
					objBillingManager.FillBillingRule( objBillingRule , sSQL );
					sCompletionDate = p_objInvoiceDiary.CompleteDate ;

					for( iIndex = 1 ; iIndex <= objBillingRule.MaxNumRebills ; iIndex++ )
					{
						// Create the diaries						
						
						if( sName == "" )
							sName = Function.GetPolicyName( iPolicyId , m_sConnectionString,m_iClientId );
						sRegarding = "Rebill due for policy:" + sName + " Term Number: " + iTermNumber ;						
						sCompletionDate = this.GetDiaryCompletionDate( iIndex , sCompletionDate , iMaxInstallNum , objPayPlan , objBillingRule , objBillXAccount );
						
						this.AddWpaDiaryEntry( "Print Notice" , "Auto-Generate/Print Notice" , sCompletionDate , iPolicyId , iBiRowId , sRegarding , "PN" , "POLICY_ENH" , p_objBillingMaster );
					}
					
					// Now create the final diary to alert the user that the amount is still unpaid
					
					if( sName == "" )
						sName = Function.GetPolicyName( iPolicyId , m_sConnectionString,m_iClientId);
					sRegarding = "Rebilling is complete for policy:" + sName + " Term Number: " + iTermNumber  ;											
					sCompletionDate = this.GetDiaryCompletionDate( iIndex , sCompletionDate , iMaxInstallNum , objPayPlan , objBillingRule , objBillXAccount );

					this.AddWpaDiaryEntry( "Unpaid Balance" , "Rebilling Complete with Unpaid Balance Remaining" , sCompletionDate , iPolicyId , iBiRowId , sRegarding , "PN" , "POLICY_ENH" , p_objBillingMaster );					
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.ScheduleRebillDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				if( objWpaDiaryAct != null )
				{
					objWpaDiaryAct.Dispose();
					objWpaDiaryAct = null ;
				}	
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
				if( objBillXAccount != null )
				{
					objBillXAccount.Dispose();
					objBillXAccount = null ;					
				}
				objPayPlan = null ;
				objBillingRule = null; 
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
			}

		}
		

		public void ScheduleInvoiceDiaries( BillingMaster p_objBillingMaster )
		{
			this.ScheduleInvoiceDiaries( null , p_objBillingMaster , false );			
		}


		public void ScheduleInvoiceDiaries( BillXInstlmnt  p_objBillXInstlmnt, BillingMaster p_objBillingMaster )
		{
			this.ScheduleInvoiceDiaries( p_objBillXInstlmnt , p_objBillingMaster , false );			
		}


		public void ScheduleInvoiceDiaries( BillXInstlmnt  p_objBillXInstlmnt, BillingMaster p_objBillingMaster, bool p_bInvoiceNotPrinted )
		{
			// The Billing Item Rowid will be stored in the AttSecRecId field on the table, the Regarding will hold
			// the Term Number and the Attach Record Id holds the Policy Id
			PolicyEnh objPolicyEnh = null ;
			BillXBillItem objBillXBillItem = null ;
			DbReader objReader = null ;
			DbReader objReaderDiary = null ;
			WpaDiaryEntry objWpaDiaryEntry = null ;
			WpaDiaryEntry objWpaDiaryEntryTemp = null ;
			
			string sName = string.Empty ;
			string sInstallDate = string.Empty ;
			string sSQL = string.Empty ;
			string sSQLDiary = string.Empty ;
			bool bAuditItemFound = false ;
			bool bDiaryEntryFound = false ;
			bool bHaveDiary = false ; 
			bool bRebildDiary = true ;
			bool bPolicyFound = false ;
			bool bBiFound = false ;
			int iPolicyId = 0 ;
			int iBiRowId = 0 ;
			int iTermNumber = 0 ;
            int iEntryId = 0;
			string sRegarding = string.Empty ;
			

			try
			{
				if( p_objBillXInstlmnt != null )
				{
					iPolicyId = p_objBillXInstlmnt.PolicyId ;
					iTermNumber = p_objBillXInstlmnt.TermNumber ;
					sInstallDate = p_objBillXInstlmnt.InstallmentDate ; 
				}
				else
				{
					foreach( int iKey in p_objBillingMaster.Policies.Keys )
					{
						objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
						if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
						{
							iPolicyId = objPolicyEnh.PolicyId ;
							sName = objPolicyEnh.PolicyName ;
							break ;
						}
					}
				}

				// We should only be printing if there is a premium item or underpayment on the policy
				foreach( int iKey in p_objBillingMaster.BillingItems.Keys )
				{
					objBillXBillItem = ( BillXBillItem ) p_objBillingMaster.BillingItems[iKey] ;
					// TODO Delete Flag Implementation.
					if( objBillXBillItem.PolicyId == iPolicyId  &&   
						(	m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType) == "P" || 
						(	m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType) == "A" && 
						m_objLocalCache.GetShortCode( objBillXBillItem.BillXAdjstmnt.AdjustmentType ) == "UP" ) )	) 
					{
						bBiFound = true ;
						iTermNumber = objBillXBillItem.TermNumber ;
						iBiRowId = objBillXBillItem.BillingItemRowid ;
						sInstallDate = objBillXBillItem.DateEntered ;
						if( m_objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "P" )
							bAuditItemFound = true ;
						break ;
					}
				}

				if( bBiFound ) 
				{
					if( !p_bInvoiceNotPrinted )
					{
						sSQL = " SELECT * FROM WPA_DIARY_ENTRY WHERE ATTACH_TABLE = 'POLICY_ENH' AND ATTACH_RECORDID = " + iPolicyId + 
							" AND STATUS_OPEN <>0" ;
						
						objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
						while( objReader.Read() )
						{
							sSQLDiary = " SELECT * FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " 
								+ Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId );
							objReaderDiary = DbFactory.GetDbReader( m_sConnectionString , sSQLDiary );
							if( objReaderDiary.Read() )
							{
								if( m_objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReaderDiary.GetValue( "ACT_CODE" ), m_iClientId ) ) == "PI" )
								{
									bDiaryEntryFound = true ;
									objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
                                    //objWpaDiaryEntry.EntryId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTRY_ID"), m_iClientId);
                                    // Naresh Changed as per RMWorld
                                    iEntryId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTRY_ID"), m_iClientId);
                                    //objWpaDiaryEntry.MoveTo(Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId ));
								}
							}
							objReaderDiary.Close();
						}
						objReader.Close();

						if( !bDiaryEntryFound )
						{
							foreach( int iKey in p_objBillingMaster.Diaries.Keys )
							{
								objWpaDiaryEntryTemp = ( WpaDiaryEntry ) p_objBillingMaster.Diaries[iKey] ;
								if( objWpaDiaryEntryTemp.EntryName == "Print Invoice" && objWpaDiaryEntryTemp.AttachRecordid == iPolicyId )
								{
									bDiaryEntryFound = true ;
									objWpaDiaryEntry = objWpaDiaryEntryTemp ;
									bHaveDiary = true ;
									break;
								}
							}	
						}

						if( bDiaryEntryFound )
						{
							if( !bHaveDiary )
							{
								objWpaDiaryEntry.MoveTo(iEntryId);
                                bRebildDiary = false;
							}
							
						}
					}

					if( bRebildDiary )
					{
						// Create the diary
						if( sName == "" )
						{
							sSQL = " SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + iPolicyId ;
							objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
							while( objReader.Read() )
							{
								sName = objReader.GetString( "POLICY_NAME" );
								bPolicyFound = true ;
							}
							objReader.Close();

							if( !bPolicyFound )
							{
								objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iPolicyId] ;
								if( objPolicyEnh != null )
								{
									sName = objPolicyEnh.PolicyName ;
								}
							}
						}
						
						if( bAuditItemFound )
							sRegarding = "Audit invoice due for policy:" + sName + " Term Number: " + iTermNumber ;
						else
							sRegarding = "Invoice due for policy:" + sName + " Term Number: " + iTermNumber ;
						
						objWpaDiaryEntry = this.AddWpaDiaryEntry( "Print Invoice" , "Auto-Generate/Print Invoice" , sInstallDate , iPolicyId , iBiRowId , sRegarding , "PI" , "POLICY_ENH", p_objBillingMaster );						
					}

					this.ScheduleRebillDiaries( objWpaDiaryEntry, p_objBillXInstlmnt , p_objBillingMaster );
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.ScheduleInvoiceDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
				if( objReaderDiary != null )
				{
					objReaderDiary.Close();
					objReaderDiary.Dispose();
					objReaderDiary = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				if( objWpaDiaryEntryTemp != null )
				{
					objWpaDiaryEntryTemp.Dispose();
					objWpaDiaryEntryTemp = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
			}
		}


		public void ScheduleInstallmentDiary( BillingMaster p_objBillingMaster , int p_iPolicyId, int p_iInstallmentNumber )
		{
			PolicyEnh objPolicyEnh = null ;
			BillXInstlmnt objBillXInstlmntTemp = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			WpaDiaryEntry objWpaDiaryEntry = null ;
			WpaDiaryAct objWpaDiaryAct = null ;
			DbReader objReader = null ;
			BillingManager objBillingManager = null ;

			bool bInstallFound = false ;
			int iInstallNumber = 12 ;
			int iIndex = 0 ;
			string sSQL = string.Empty ;
			int iMinInstallmentNumber = 0 ;
			bool bFlag = false ;
			string sName = string.Empty ;
			string sRegarding = string.Empty ;
			string sEntryName = string.Empty ;
			string sEntryNotes = string.Empty ;
			
			try
			{
				// TODO check this function flow again.........a lot GOTO in this...

                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);

				if( p_objBillingMaster.Installments.Count != 0 )
				{
					for( iIndex = 0 ; iIndex < p_objBillingMaster.Installments.Count ; iIndex++ )
					{
						objBillXInstlmntTemp = ( BillXInstlmnt ) p_objBillingMaster.Installments[ iIndex ] ;
                        if (objBillXInstlmntTemp.GeneratedFlag == false && objBillXInstlmntTemp.InstallmentNum < iInstallNumber)
						{
							iInstallNumber = objBillXInstlmntTemp.InstallmentNum ;
							objBillXInstlmnt = objBillXInstlmntTemp ;
							bInstallFound = true ;
						}
					}
				}

				if( p_objBillingMaster.Installments.Count == 0 || !bInstallFound )
				{
					if( p_iPolicyId == 0 )
						p_iPolicyId = p_objBillingMaster.Account.PolicyId ;

					sSQL = " SELECT MIN(INSTALLMENT_NUM) FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_iPolicyId + " AND GENERATED_FLAG = 0" ;
					if( p_iInstallmentNumber != 0 )
						sSQL += " AND INSTALLMENT_NUM <> " + p_iInstallmentNumber + " AND INSTALLMENT_NUM >" + p_iInstallmentNumber ;
					
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader.Read() )
					{
						iMinInstallmentNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId ); ;
					}
					objReader.Close();
					
					if( iMinInstallmentNumber != 0 )
					{
						objBillXInstlmnt = ( BillXInstlmnt ) m_objDataModelFactory.GetDataModelObject( "BillXInstlmnt" , false );
						sSQL = " SELECT * FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_iPolicyId + " AND INSTALLMENT_NUM = " + iMinInstallmentNumber + " AND GENERATED_FLAG =0" ;
						objBillingManager.FillInstallment( objBillXInstlmnt, sSQL );
					}
					else
					{
						bFlag = true ;
					}
				}
				
				if( !bFlag )
				{
					// TODO
					if( Conversion.ConvertStrToInteger( objBillXInstlmnt.InstallmentDate ) <= Conversion.ConvertStrToInteger( Conversion.GetDate( DateTime.Now.ToString("d") ) ) ) 
					{
                        
						// Create charge item
						objBillingManager.CreateChargeItemInstallment( p_objBillingMaster , objBillXInstlmnt );
                        //Shruti for MITS 9527
                        p_objBillingMaster.Installments.Add(objBillXInstlmnt);
					}
					else
					{
						// create the diary
						
						sEntryName = "Next Installment Due" ;
						sEntryNotes = "Auto-Generate Next Pay Plan Installment" ;
						sName = Function.GetPolicyName( objBillXInstlmnt.PolicyId , m_sConnectionString ,m_iClientId);						
						if( sName == "" )
						{
							foreach( int iKey in p_objBillingMaster.Policies.Keys )
							{
								objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
								if( objPolicyEnh.PolicyId == objBillXInstlmnt.PolicyId )
								{
									sName = objPolicyEnh.PolicyName ;
									break;
								}
							}
						}

						sRegarding = "Next installment due for policy:" + sName ;
						
						this.AddWpaDiaryEntry( sEntryName , sEntryNotes , objBillXInstlmnt.InstallmentDate , objBillXInstlmnt.InstallmentRowid , objBillXInstlmnt.PolicyId , sRegarding , "GNI" , "BILL_X_INSTLMNT" , p_objBillingMaster );						
					}
				}																
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.ScheduleInstallmentDiary.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXInstlmntTemp != null )
				{
					objBillXInstlmntTemp.Dispose();
					objBillXInstlmntTemp = null ;
				}
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				if( objWpaDiaryAct != null )
				{
					objWpaDiaryAct.Dispose();
					objWpaDiaryAct = null ;
				}
                if (objPolicyEnh != null)
                {
                    objPolicyEnh.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
			}
		}


		public void ScheduleInstallmentDiary( BillingMaster p_objBillingMaster )
		{
			this.ScheduleInstallmentDiary( p_objBillingMaster , 0 , 0 );
		}

		
		public void ScheduleCreditDiaries( int p_iPolicyId, int p_iTermNumber )
		{
			ArrayList arrlstBillingItems = null ;
			ArrayList arrlstAccount = null ; 
			BillXBillItem objBillXBillItem = null ;
			BillXAccount objBillXAccount = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			DbReader objReader = null ;
			BillingManager objBillingManager = null ;

			int iIndex = 0 ;
			int iPayPlanCode = 0 ;
			int iMaxInstall = 0 ;
			double dblTermBalance = 0.0 ;
			bool bCreateDiary = false ;
			string sSQL = string.Empty ;
			string sEntryName = string.Empty ;
			string sRegarding = string.Empty ;
			string sEntryNotes = string.Empty ;
			string sComleteDate = string.Empty ;



			try
			{
				arrlstBillingItems = new ArrayList();
				arrlstAccount = new ArrayList();
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);

				sSQL = " Select * from BILL_X_BILL_ITEM where POLICY_ID = " + p_iPolicyId + "  AND TERM_NUMBER = " + p_iTermNumber ;				
				objBillingManager.FillBillingItemLoop( arrlstBillingItems , sSQL , true );

				for( iIndex = 0 ; iIndex < arrlstBillingItems.Count ; iIndex++ )
				{
					objBillXBillItem = ( BillXBillItem ) arrlstBillingItems[ iIndex ];
					dblTermBalance += objBillXBillItem.Amount ;
				}
				
				sSQL = " SELECT * from BILL_X_ACCOUNT WHERE BILL_X_ACCOUNT.POLICY_ID = " + p_iPolicyId + " AND TERM_NUMBER = " + p_iTermNumber ;
				objBillingManager.FillBillAccount( arrlstAccount , sSQL );

				for( iIndex = 0 ; iIndex < arrlstAccount.Count ; iIndex++ )
				{
					objBillXAccount = ( BillXAccount ) arrlstAccount[ iIndex ] ;
					if( objBillXAccount.TermNumber == p_iTermNumber )
					{
						sSQL = " SELECT PAY_PLAN_CODE FROM SYS_BILL_PAY_PLAN where PAY_PLAN_ROWID = " + objBillXAccount.PayPlanRowid ;
						objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
						if( objReader.Read() )
						{
							iPayPlanCode = Common.Conversion.ConvertObjToInt( objReader.GetValue( "PAY_PLAN_CODE" ), m_iClientId );	
						}
						objReader.Close();	
					}
				}

				if( dblTermBalance < 0 )
				{
					// Check whether PayPlan is present for the term
					if( iPayPlanCode == 0 )
					{
						bCreateDiary = true ;						
					}
					else
					{
						// Check whether max installment is generated
						sSQL = " Select max(INSTALLMENT_NUM) FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_iPolicyId + " AND TERM_NUMBER = " + p_iTermNumber ;
						objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
						if( objReader.Read() )
						{
							iMaxInstall = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );	
						}
						objReader.Close();
	
						objBillXInstlmnt = ( BillXInstlmnt ) m_objDataModelFactory.GetDataModelObject( "BillXInstlmnt" , false );
						sSQL = " SELECT * FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_iPolicyId + " AND TERM_NUMBER = " + p_iTermNumber + " AND INSTALLMENT_NUM=" + iMaxInstall ;
						objBillingManager.FillInstallment( objBillXInstlmnt, sSQL );

						if( objBillXInstlmnt.GeneratedFlag )
							bCreateDiary = true ;
						else
							return ;

						sSQL = " SELECT * FROM WPA_DIARY_ENTRY A,WPA_DIARY_ACT B WHERE A.ATTACH_TABLE = 'BILL_X_BILL_ITEM' "
							+	" AND A.ATTACH_RECORDID = " + p_iPolicyId + " AND A.ATT_SEC_REC_ID = " + p_iTermNumber 
							+	" AND B.PARENT_ENTRY_ID = A.ENTRY_ID AND ACT_CODE = " + m_objLocalCache.GetCodeId( "CD", "AUTODIARY_ACT_CODE" ) ;
						objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
						if( objReader.Read() )
						{
							bCreateDiary = false ;	
						}
						objReader.Close();
					}

					if( bCreateDiary )
					{ 
						sEntryName = "Credit Balance" ;
						sEntryNotes = "Credit exists for the policy. Please review and generate a disbursement check if necessary." ;
						sRegarding = "Credit exists on the policy:" + Function.GetPolicyName( p_iPolicyId , m_sConnectionString,m_iClientId ) + ", Term #:" + p_iTermNumber ;
                        sComleteDate=Conversion.GetDate(DateTime.Now.ToString("d"));
						this.AddWpaDiaryEntry( sEntryName , sEntryNotes , sComleteDate , p_iPolicyId , p_iTermNumber , sRegarding , "CD" , "BILL_X_BILL_ITEM" , null );						
					}
				}
				else
				{
					this.DeleteCreditDiaries( p_iPolicyId, p_iTermNumber );
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.ScheduleCreditDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
				if( objBillXAccount != null )
				{
					objBillXAccount.Dispose();
					objBillXAccount = null ;
				}
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
                arrlstBillingItems = null;
                arrlstAccount = null;

			}
		}

		
		#endregion 

		#region Functions to Delete Diaries ( Cancel , CancelPending , Notice , UnpaidBalance , Rebill, Credit )
		
		public void DeleteCancelDiaries( int p_iPolicyId , int p_iBillingItemRowId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteDiaries( p_iPolicyId , p_iBillingItemRowId , p_objBillingMaster , "Cancel" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteCancelDiaries.Error", m_iClientId), p_objEx);				
			}						
		}

		
		public void DeleteCancelPendingDiaries( int p_iPolicyId , int p_iBillingItemRowId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteDiaries( p_iPolicyId , p_iBillingItemRowId , p_objBillingMaster , "CancelPending" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteCancelPendingDiaries.Error", m_iClientId), p_objEx);				
			}						
		}

		
		public void DeleteNoticeDiaries( int p_iPolicyId , int p_iBillingItemRowId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteDiaries( p_iPolicyId , p_iBillingItemRowId , p_objBillingMaster , "Notice" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteNoticeDiaries.Error", m_iClientId), p_objEx);				
			}						
		}

		
		public void DeleteUnpaidBalanceDiary( int p_iPolicyId , int p_iBillingItemRowId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteDiaries( p_iPolicyId , p_iBillingItemRowId , p_objBillingMaster , "UnpaidBalance" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteUnpaidBalanceDiary.Error", m_iClientId), p_objEx);				
			}						
		}


		public void DeleteCancelRebillDiaries( int p_iPolicyId , int p_iBillingItemRowId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteDiaries( p_iPolicyId , p_iBillingItemRowId , p_objBillingMaster , "CancelRebill" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteCancelRebillDiaries.Error", m_iClientId), p_objEx);				
			}						
		}

		
		public void DeleteCreditDiaries( int p_iPolicyId, int p_iTermNumber )
		{
			ArrayList arrlstDiariesToDelete = null ;
			DbReader objReader = null ;
			DbConnection objConn = null ;
			StringBuilder sbSQL = null ;

			int iEntryId = 0 ;
			int iIndex = 0 ;
			string sSQL = string.Empty ;
			
			try
			{
				arrlstDiariesToDelete = new ArrayList();
				sbSQL = new StringBuilder();

				sbSQL.Append( " SELECT * FROM WPA_DIARY_ENTRY A,WPA_DIARY_ACT B WHERE A.ATTACH_TABLE " );
				sbSQL.Append( "= 'BILL_X_BILL_ITEM' AND A.ATTACH_RECORDID = " + p_iPolicyId );
				sbSQL.Append( " AND A.ATT_SEC_REC_ID = " + p_iTermNumber + " AND B.PARENT_ENTRY_ID = A.ENTRY_ID AND " );
				sbSQL.Append( " ACT_CODE = " + m_objLocalCache.GetCodeId( "CD", "AUTODIARY_ACT_CODE" ) );
				sbSQL.Append( " AND DIARY_VOID = 0 AND DIARY_DELETED = 0" );

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
				while( objReader.Read() )
				{
					iEntryId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId );
					arrlstDiariesToDelete.Add( iEntryId );
				}
				objReader.Close();
				
				objConn = DbFactory.GetDbConnection( m_sConnectionString );
				objConn.Open();

				for( iIndex = 0 ; iIndex < arrlstDiariesToDelete.Count ; iIndex++ )
				{
					iEntryId = ( int ) arrlstDiariesToDelete[ iIndex ];
					
					sSQL = " DELETE FROM WPA_DIARY_ENTRY WHERE ENTRY_ID = " + iEntryId ;					
					objConn.ExecuteNonQuery( sSQL );						

					sSQL = " DELETE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + iEntryId ;
					objConn.ExecuteNonQuery( sSQL );
				}

				objConn.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteCreditDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
				if ( objConn != null )
				{
					objConn.Close();
					objConn.Dispose();					
				}				
				arrlstDiariesToDelete = null ;
				sbSQL = null ;
			}
		}

		
		private void DeleteDiaries( int p_iPolicyId , int p_iBillingItemRowId , BillingMaster p_objBillingMaster , string p_sDiaryType )
		{
			ArrayList arrlstDiariesToDelete = null ;
			DbReader objReader = null ;
			DbReader objReaderDiaryAct = null ;
			WpaDiaryEntry objWpaDiaryEntry = null ;
			StringBuilder sbSQL = null ;

			int iEntryId = 0 ;
			int iIndex = 0 ;
			string sActCode = string.Empty ;
			bool bSkip = false ;
			string sRegarding = string.Empty ;

			try
			{
				arrlstDiariesToDelete = new ArrayList();

				switch( p_sDiaryType )
				{
					case "Cancel" :
						sActCode = "CP" ;
						break ;
					case "CancelPending" :
						sActCode = "PCP" ;
						break ;
					case "Notice" :
						sActCode = "PN" ;
						break ;
					case "UnpaidBalance" :
						sActCode = "RC" ;
						break ;
					case "CancelRebill" :
						sActCode = "PN" ;
						break ;
				}

				sbSQL = new StringBuilder();
				sbSQL.Append( "SELECT * FROM WPA_DIARY_ENTRY WHERE ATTACH_TABLE = 'POLICY_ENH' AND ATTACH_RECORDID = " );
				sbSQL.Append( p_iPolicyId + " AND ATT_SEC_REC_ID = " + p_iBillingItemRowId + " AND STATUS_OPEN <>0" );
				sbSQL.Append( " AND DIARY_VOID = 0 AND DIARY_DELETED = 0" );

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
				while( objReader.Read() )
				{
					iEntryId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId );
					sbSQL.Remove( 0 , sbSQL.Length );
					
					if( sActCode == "PN" )
					{
						sRegarding = objReader.GetString( "REGARDING" ) ;
						if( sRegarding.Length > 6  && ( sRegarding.Substring( 0 , 6 )== "Rebill" || sRegarding.Substring( 0 , 6 )== "Notice" ) )
							bSkip = false ;						
						else
							bSkip = true ;
					}
					else
						bSkip = false ;
					
					if( !bSkip )
					{
						sbSQL.Append( " SELECT ACT_CODE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + iEntryId );
						objReaderDiaryAct = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
						if( objReaderDiaryAct.Read() )
						{
							if( m_objLocalCache.GetShortCode( Conversion.ConvertObjToInt( objReaderDiaryAct.GetValue( "ACT_CODE" ), m_iClientId ) ) == sActCode )
							{
								arrlstDiariesToDelete.Add( iEntryId );
							}					
						}
						objReaderDiaryAct.Close();
					}
				}
				objReader.Close();
			
				for( iIndex = 0 ; iIndex < arrlstDiariesToDelete.Count ; iIndex++ )
				{
					iEntryId = ( int ) arrlstDiariesToDelete[ iIndex ];
					
					objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
					objWpaDiaryEntry.MoveTo( iEntryId );
                    if(!(p_objBillingMaster.DiariesToDelete.Contains(iEntryId)))
					    p_objBillingMaster.DiariesToDelete.Add( iEntryId , objWpaDiaryEntry ); // TODO
				}				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
				if( objReaderDiaryAct != null )
				{
					objReaderDiaryAct.Close();
					objReaderDiaryAct.Dispose();
					objReaderDiaryAct = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				arrlstDiariesToDelete = null ;
				sbSQL = null ;
			}			
		}

		
		#endregion 

		#region Functions to Delete All Diaries ( Cancel , CancelPending , Notice )

		public void DeleteAllCancelDiaries( int p_iPolicyId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteAllDiaries( p_iPolicyId , p_objBillingMaster , "Cancel" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteAllCancelDiaries.Error", m_iClientId), p_objEx);				
			}						
		}


		public void DeleteAllCancelPendingDiaries( int p_iPolicyId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteAllDiaries( p_iPolicyId , p_objBillingMaster , "CancelPending" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteAllCancelPendingDiaries.Error", m_iClientId), p_objEx);				
			}						
		}


		public void DeleteAllNoticeDiaries( int p_iPolicyId , BillingMaster p_objBillingMaster )
		{
			try
			{
				DeleteAllDiaries( p_iPolicyId , p_objBillingMaster , "Notice" );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteAllNoticeDiaries.Error", m_iClientId), p_objEx);				
			}						
		}

		
		private void DeleteAllDiaries( int p_iPolicyId , BillingMaster p_objBillingMaster , string p_sDiaryType )
		{
			ArrayList arrlstDiariesToDelete = null ;
			DbReader objReader = null ;
			DbReader objReaderDiaryAct = null ;
			WpaDiaryEntry objWpaDiaryEntry = null ;
			StringBuilder sbSQL = null ;

			int iEntryId = 0 ;
			int iIndex = 0 ;
			string sActCode = string.Empty ;
			bool bSkip = false ;
			string sRegarding = string.Empty ;

			try
			{
				arrlstDiariesToDelete = new ArrayList();

				switch( p_sDiaryType )
				{
					case "Cancel" :
						sActCode = "CP" ;
						break ;
					case "CancelPending" :
						sActCode = "PCP" ;
						break ;
					case "Notice" :
						sActCode = "PN" ;
						break ;					
				}

				sbSQL = new StringBuilder();
				sbSQL.Append( "SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE ATTACH_TABLE = 'POLICY_ENH' AND ATTACH_RECORDID = " );
				sbSQL.Append( p_iPolicyId + " AND STATUS_OPEN <>0" );
				sbSQL.Append( " AND DIARY_VOID = 0 AND DIARY_DELETED = 0" );

				objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
				while( objReader.Read() )
				{
					iEntryId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId );
					sbSQL.Remove( 0 , sbSQL.Length );
					
					if( sActCode == "PN" )
					{
						sRegarding = objReader.GetString( "REGARDING" ) ;
						if( sRegarding.Length > 6  && sRegarding.Substring( 0 , 6 )== "Notice" )
							bSkip = false ;						
						else
							bSkip = true ;
					}
					else
						bSkip = false ;
					
					if( !bSkip )
					{
						sbSQL.Append( " SELECT ACT_CODE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID = " + iEntryId );
						objReaderDiaryAct = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
						if( objReaderDiaryAct.Read() )
						{
							if( m_objLocalCache.GetShortCode( Conversion.ConvertObjToInt( objReaderDiaryAct.GetValue( "ACT_CODE" ), m_iClientId ) ) == sActCode )
							{
								arrlstDiariesToDelete.Add( iEntryId );
							}					
						}
						objReaderDiaryAct.Close();
					}
				}
				objReader.Close();
			
				for( iIndex = 0 ; iIndex < arrlstDiariesToDelete.Count ; iIndex++ )
				{
					iEntryId = ( int ) arrlstDiariesToDelete[ iIndex ];
					
					objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
					objWpaDiaryEntry.MoveTo( iEntryId );
					p_objBillingMaster.DiariesToDelete.Add( iEntryId , objWpaDiaryEntry ); // TODO
				}				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteAllDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
				if( objReaderDiaryAct != null )
				{
					objReaderDiaryAct.Close();
					objReaderDiaryAct.Dispose();
					objReaderDiaryAct = null ;
				}
				if( objWpaDiaryEntry != null )
				{
					objWpaDiaryEntry.Dispose();
					objWpaDiaryEntry = null ;
				}
				arrlstDiariesToDelete = null ;
				sbSQL = null ;
			}			
		}

		
		public void DeleteAllInstallmentDiaries( int p_iPolicyId , int p_iTermNumber, BillingMaster p_objBillingMaster )
		{
			DbReader objReader = null ;
			WpaDiaryEntry objWpaDiaryEntry = null ;
			ArrayList arrlstDiariesToDelete = null ;

			string sSQL = string.Empty ;
			int iMinInstallNumber = 0 ;
			int iRowId = 0 ;
			int iIndex = 0 ;
			int iEntryId = 0 ;
			
			try
			{
				arrlstDiariesToDelete = new ArrayList();

				sSQL = " SELECT MIN(INSTALLMENT_NUM) FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_iPolicyId + " AND TERM_NUMBER = " + p_iTermNumber + " AND GENERATED_FLAG = 0" ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					iMinInstallNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );				
				}
				objReader.Close();

				if( iMinInstallNumber == 0 )
					return ;

				sSQL = " SELECT INSTALLMENT_ROWID FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_iPolicyId + " AND TERM_NUMBER = " + p_iTermNumber + " AND INSTALLMENT_NUM=" + iMinInstallNumber + " AND GENERATED_FLAG = 0" ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					iRowId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALLMENT_ROWID" ), m_iClientId );				
				}
				objReader.Close();

				sSQL = " SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE ATTACH_TABLE = 'BILL_X_INSTLMNT' AND ATTACH_RECORDID = " + iRowId
					+	" AND STATUS_OPEN <>0 AND DIARY_VOID = 0 AND DIARY_DELETED = 0" ;

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				while( objReader.Read() )
				{
					iEntryId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId );
					arrlstDiariesToDelete.Add( iEntryId );
				}
				objReader.Close();
			
				for( iIndex = 0 ; iIndex < arrlstDiariesToDelete.Count ; iIndex++ )
				{
					iEntryId = ( int ) arrlstDiariesToDelete[ iIndex ];
					
					objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
					objWpaDiaryEntry.MoveTo( iEntryId );
                    p_objBillingMaster.DiariesToDelete.Add(iEntryId, objWpaDiaryEntry); // TODO
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("DiaryManager.DeleteAllInstallmentDiaries.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
		}
		
		#endregion 		
	}
}
 
