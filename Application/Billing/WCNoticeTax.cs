using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.ReportInterfaces;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /// <summary>
    /// Summary description for RMSCWCNotice.
    /// </summary>
    public partial class WCNoticeTax : DataDynamics.ActiveReports.ActiveReport3
    {
        #region Member Variables
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Private variable to store Insured Entity
        /// </summary>		
        private Entity m_objInsuredEntity = null;
        /// <summary>
        /// Private variable to store Insurer Entity
        /// </summary>
        private Entity m_objInsurerEntity = null;
        /// <summary>
        /// Private variable to store Insurer Entity
        /// </summary>
        private Entity m_objBrokerEntity = null;
        /// <summary>
        /// Private variable to store Invoice
        /// </summary>
        private BillXInvoice m_objBillXInvoice = null;
        /// <summary>
        /// Private variable to store Invoice details
        /// </summary>
        private BillXNotice  m_objBillXNotice = null;
        /// <summary>
        /// Private variable to store Invoice details
        /// </summary>
        private BillXNoticeDet m_objBillXNoticeDet = null;
        /// <summary>
        /// Private variable to store Effective Date
        /// </summary>		
        private string m_sEffDate = string.Empty;
        /// <summary>
        /// Private variable to store Exp. Date
        /// </summary>
        private string m_sExpDate = string.Empty;
        /// <summary>
        /// Private variable to store Fetch Count
        /// </summary>
        private int m_iFetchCount = 0;
        private string m_sPolicy_Name = string.Empty;
        private double dblTotalDividendCredit = 0;
        private double dblNetPremium = 0;
        private string strServicedBy = string.Empty;
        private string strPastDueDate = string.Empty;
        private int m_iClientId = 0;
        #endregion 		

        #region Properties
        /// <summary>
        /// Read propertie for the Invoice collection.
        /// </summary>
        internal BillXInvoice Invoice
        {
            get
            {
                return m_objBillXInvoice;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor, initializes the variables to the default value, and Initialize the Report.
        /// </summary>
        /// <param name="p_objDataModelFactory">DataModelFactory Object</param>
        public WCNoticeTax(DataModelFactory p_objDataModelFactory, int p_iClientId)
        {
            m_objDataModelFactory = p_objDataModelFactory;
            m_iClientId = p_iClientId;
            m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString;
            InitializeComponent();
        }

        #endregion
        //*******************************************************
        #region DataInitialize, FetchData, ReportStart Functions bind to respective events of the report.
        
        /// <summary>
        /// This method would invoke the method of base class to initialize the fields of the report.
        /// </summary>
        /// <param name="p_objsender">Sender of the event.</param>
        /// <param name="p_objeArgs">Information about the event.</param>
        private void PolicyBilling_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
        {
            try
            {
                m_iFetchCount = 0;
                Fields.Add("INSURER_NAME");
                Fields.Add("INSURER_ADDR1");
                Fields.Add("INSURER_ADDR2");
                Fields.Add("INSURER_ADDR3");
                Fields.Add("INSURER_PHONE");
                Fields.Add("INSURED_NAME");
                Fields.Add("INSURED_ADDR1");
                Fields.Add("INSURED_ADDR2");
                Fields.Add("INSURED_ADDR3");
                Fields.Add("NOTICE_DATE");
                Fields.Add("POLICY_NUMBER");
                Fields.Add("TERM_DATES");
                Fields.Add("AMOUNT_DUE");
                Fields.Add("DUE_DATE");
                Fields.Add("PAST_DUE_DATE");
                Fields.Add("BALANCE");
                Fields.Add("BILLED_PREM");
                Fields.Add("STATE_TAX");
                Fields.Add("TOTAL_DUE");
                Fields.Add("SERVICED_BY");
                Fields.Add("BROKER_NAME");
                Fields.Add("BROKER_ADDR1");
                Fields.Add("BROKER_ADDR2");
                Fields.Add("BROKER_ADDR3");
                Fields.Add("BROKER_PHONE");
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
            }
        }

        
        

        /// <summary>
        /// This method will do the page setting for the EOB Report to be printed.
        /// </summary>
        /// <param name="p_objsender">Sender of the event.</param>
        /// <param name="p_objeArgs">Information about the event</param>
        private void PolicyBilling_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
        {
            try
            {
                this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("RMSCWCNotice.PolicyBilling_ReportStart.Error", m_iClientId), p_objEx);
            }
        }

        #endregion

        #region GatherData Method
        /// <summary>
        /// This method will collect the data for the report.
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_objBillingMaster">Billing Master Object</param>
        public void GatherData(int p_iNoticeId, BillingMaster p_objBillingMaster , ref bool blnGenerateNotice)
        {
            PolicyEnh objPolicyEnh = null;
            DbReader objReader = null;
            LocalCache objLocalCache = null;

            string sSQL = string.Empty;
            bool bRoundFlag = false;
            string strFieldName = string.Empty;
            int intTopInsurerId = 0;

            try
            {
                blnGenerateNotice=true; 
                m_objInsuredEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                m_objBrokerEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                m_objBillXNotice = (BillXNotice)p_objBillingMaster.Notices[p_iNoticeId];
                if (m_objBillXNotice != null)
                {
                    sSQL = "SELECT DUE_DATE FROM BILL_X_INVOICE WHERE INVOICE_ROWID = " + m_objBillXNotice.PrevInvoiceRowid;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        strPastDueDate = Conversion.ConvertObjToStr(objReader.GetValue("DUE_DATE"));
                    }
                    objReader.Close();
                    if (Conversion.ConvertObjToInt(m_objBillXNotice.DueDate, m_iClientId) < Conversion.ConvertObjToInt(strPastDueDate, m_iClientId))
                    {
                        blnGenerateNotice = false;
                    }

                    else
                    {
                        m_objInsuredEntity.MoveTo(m_objBillXNotice.HierarchyLevel);
                        objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[m_objBillXNotice.PolicyId];
                        if (objPolicyEnh != null)
                        {
                            m_sPolicy_Name = objPolicyEnh.PolicyName;
                            m_objInsurerEntity = objPolicyEnh.InsurerEntity;
                            m_objBrokerEntity.MoveTo(objPolicyEnh.BrokerEntity.ParentEid);
                            if(objPolicyEnh.PolicyXInsuredEnhList.Count>0)
                            {
                                //Change by kuladeep for Audit Issue MITS:24736
                                //m_objInsuredEntity.EntityId = Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objPolicyEnh.PolicyXInsuredEnh));
                                foreach (PolicyXInsuredEnh objInsured in objPolicyEnh.PolicyXInsuredEnhList)
                                {
                                    m_objInsuredEntity.EntityId = objInsured.InsuredEid;
                                    break;
                                }
                            }

                            foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                            {
                                if (objPolicyXTermEnh.TermNumber == m_objBillXNotice.TermNumber)
                                {
                                    m_sEffDate = Common.Conversion.GetDBDateFormat(objPolicyXTermEnh.EffectiveDate, "d");
                                    m_sExpDate = Common.Conversion.GetDBDateFormat(objPolicyXTermEnh.ExpirationDate, "d");
                                    break;
                                }
                            }
                        }
                    }
                }

                strFieldName = "";
                sSQL = "SELECT ENTITY.ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY.ENTITY_ID = " + m_objInsuredEntity.EntityId + "ORDER BY ENTITY.ENTITY_TABLE_ID ASC ";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    strFieldName = GetOrgHierarchyFieldName(Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId));
                }
                objReader.Close();
                if (strFieldName != "")
                {
                    sSQL = "SELECT ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM ENTITY INNER JOIN ORG_HIERARCHY ON "
                            + " ENTITY.ENTITY_ID = ORG_HIERARCHY.CLIENT_EID WHERE ORG_HIERARCHY." + strFieldName + " = " + m_objInsuredEntity.EntityId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        strServicedBy = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                        if (strServicedBy.Trim() != "")
                            strServicedBy = strServicedBy + " " + Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                        else
                            strServicedBy = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                    }
                    objReader.Close();

                }
                // Retrieve the address
                m_objInsuredEntity.Refresh();
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("RMSCWCNotice.GatherData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objPolicyEnh != null)
                {
                    objPolicyEnh.Dispose();
                    objPolicyEnh = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
            }
        }
        public string GetOrgHierarchyFieldName(int intOrgLevel)
        {
            string strReturn = string.Empty;
            try
            {
                strReturn = "CLIENT_EID";
                switch (intOrgLevel)
                {
                    case 1005:
                        strReturn = "CLIENT_EID";
                        break;
                    case 1006:
                        strReturn = "COMPANY_EID";
                        break;
                    case 1007:
                        strReturn = "OPERATION_EID";
                        break;
                    case 1008:
                        strReturn = "REGION_EID";
                        break;
                    case 1009:
                        strReturn = "DIVISION_EID";
                        break;
                    case 1010:
                        strReturn = "LOCATION_EID";
                        break;
                    case 1011:
                        strReturn = "FACILITY_EID";
                        break;
                    case 1012:
                        strReturn = "DEPARTMENT_EID";
                        break;
                }
            }
            catch
            {
                strReturn = "";
            }
            finally
            {

            }
            return strReturn;
        }

        /// <summary>
        /// This method would invoke the method of base class to fill data in the fields of the report.
        /// </summary>
        /// <param name="p_objsender">Sender of the event.</param>
        /// <param name="p_objeArgs">Information about the event.</param>
        private void PolicyBilling_FetchData(object p_objsender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs p_objeArgs)
        {
            LocalCache objLocalCache = null;
            DateTime objDate;

            string sMonth = string.Empty;
            string sDay = string.Empty;
            string sYear = string.Empty;
            string strTemp = string.Empty;
            //Start:Add by kuladeep for Past Due Date MITS:24765/24301
			string strPastDueDate = string.Empty;
            string sSQL=string.Empty;
            //End:Add by kuladeep for Past Due Date MITS:24765/24301

            try
            {
                m_iFetchCount++;

                if (m_iFetchCount >= 2)
                {
                    p_objeArgs.EOF = true;
                    return;
                }
                else
                    p_objeArgs.EOF = false;

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                m_objInsuredEntity.MoveTo(m_objBillXNotice.HierarchyLevel);
                // Supply data
                Fields["INSURED_NAME"].Value = m_objInsuredEntity.LastName;
                Fields["INSURED_ADDR1"].Value = m_objInsuredEntity.Addr1;
                if (m_objInsuredEntity.Addr1.Trim() == "")
                {
                    Fields["INSURED_ADDR1"].Value = m_objInsuredEntity.Addr2;
                }
                else
                {
                    Fields["INSURED_ADDR2"].Value = m_objInsuredEntity.Addr2;
                }

                if (m_objInsuredEntity.City.Trim() == "")
                {
                    strTemp = objLocalCache.GetStateCode(m_objInsuredEntity.StateId) + "  " + m_objInsuredEntity.ZipCode;
                }
                else
                {
                    strTemp = m_objInsuredEntity.City + ", " + objLocalCache.GetStateCode(m_objInsuredEntity.StateId) + "  " + m_objInsuredEntity.ZipCode;
                }

                if (m_objInsuredEntity.Addr1.Trim() == "")
                {
                    if (m_objInsuredEntity.Addr2.Trim() == "")
                    {
                        Fields["INSURED_ADDR1"].Value = strTemp;
                    }
                    else
                    {
                        Fields["INSURED_ADDR2"].Value = strTemp;
                    }
                }
                else
                {
                    if (m_objInsuredEntity.Addr2.Trim() == "")
                    {
                        Fields["INSURED_ADDR2"].Value = strTemp;
                    }
                    else
                    {
                        Fields["INSURED_ADDR3"].Value = strTemp;
                    }
                }
                if (m_objInsurerEntity != null)
                {

                    Fields["INSURER_NAME"].Value = m_objInsurerEntity.LastName;
                    Fields["INSURER_ADDR1"].Value = m_objInsurerEntity.Addr1;
                    Fields["INSURER_ADDR2"].Value = m_objInsurerEntity.Addr2;
                    if (m_objInsurerEntity.City.Trim() == "")
                    {
                        strTemp = objLocalCache.GetStateCode(m_objInsurerEntity.StateId) + "  " + m_objInsurerEntity.ZipCode;
                    }
                    else
                    {
                        strTemp = m_objInsurerEntity.City + ", " + objLocalCache.GetStateCode(m_objInsurerEntity.StateId) + "  " + m_objInsurerEntity.ZipCode;
                    }

                    if (m_objInsurerEntity.Addr1.Trim() == "")
                    {
                        if (m_objInsurerEntity.Addr2.Trim() == "")
                        {
                            Fields["INSURER_ADDR1"].Value = strTemp;
                        }
                        else
                        {
                            Fields["INSURER_ADDR2"].Value = strTemp;
                        }
                    }
                    else
                    {
                        if (m_objInsurerEntity.Addr2.Trim() == "")
                        {
                            Fields["INSURER_ADDR2"].Value = strTemp;
                        }
                        else
                        {
                            Fields["INSURER_ADDR3"].Value = strTemp;
                        }
                    }

                    if (m_objInsurerEntity.Phone1.Trim() == "")
                    {
                        Fields["INSURER_PHONE"].Value = m_objInsurerEntity.Phone2;
                    }
                    else
                    {
                        Fields["INSURER_PHONE"].Value = m_objInsurerEntity.Phone1;
                    }
                }
                sMonth = m_objBillXNotice.BillDate.Substring(4, 2);
                sDay = m_objBillXNotice.BillDate.Substring(6, 2);
                sYear = m_objBillXNotice.BillDate.Substring(0, 4);

                objDate = new DateTime(Conversion.ConvertStrToInteger(sYear),
                                                Conversion.ConvertStrToInteger(sMonth),
                                                Conversion.ConvertStrToInteger(sDay));


                Fields["NOTICE_DATE"].Value = objDate.ToString("MMMM") + " " + sDay + ", " + sYear;
                Fields["POLICY_NUMBER"].Value = m_objBillXNotice.PolicyNumber;
                Fields["DUE_DATE"].Value = Common.Conversion.GetDBDateFormat(m_objBillXNotice.DueDate, "d");
                Fields["TERM_DATES"].Value = m_sEffDate + " - " + m_sExpDate;
                Fields["BILLED_PREM"].Value = string.Format("{0:C}", m_objBillXNotice.CurrentCharges);
                Fields["STATE_TAX"].Value = string.Format("{0:C}", m_objBillXNotice.CurrentTaxCharges);
                Fields["BALANCE"].Value = string.Format("{0:C}", m_objBillXNotice.PreviousBalance);
                Fields["TOTAL_DUE"].Value = string.Format("{0:C}", m_objBillXNotice.Amount);
                Fields["AMOUNT_DUE"].Value = string.Format("{0:C}", m_objBillXNotice.Amount);

                if (m_objBrokerEntity != null)
                {
                    Fields["BROKER_NAME"].Value = m_objBrokerEntity.LastName;
                    Fields["BROKER_ADDR1"].Value = m_objBrokerEntity.Addr1;
                    Fields["BROKER_ADDR2"].Value = m_objBrokerEntity.Addr2;
                    if (m_objBrokerEntity.City.Trim() == "")
                    {
                        strTemp = objLocalCache.GetStateCode(m_objBrokerEntity.StateId) + "  " + m_objBrokerEntity.ZipCode;
                    }
                    else
                    {
                        strTemp = m_objBrokerEntity.City + ", " + objLocalCache.GetStateCode(m_objBrokerEntity.StateId) + "  " + m_objBrokerEntity.ZipCode;
                    }

                    if (m_objBrokerEntity.Addr2.Trim() == "")
                    {
                        Fields["BROKER_ADDR2"].Value = strTemp;
                    }
                    else
                    {
                        Fields["BROKER_ADDR3"].Value = strTemp;
                    }

                    if (m_objBrokerEntity.Addr2.Trim() == "")
                    {
                        if (m_objBrokerEntity.Phone1.Trim() == "")
                        {
                            Fields["BROKER_ADDR3"].Value = m_objBrokerEntity.Phone2;
                        }
                        else
                        {
                            Fields["BROKER_ADDR3"].Value = m_objBrokerEntity.Phone1;
                        }
                    }
                    else
                    {
                        if (m_objBrokerEntity.Phone1.Trim() == "")
                        {
                            Fields["BROKER_PHONE"].Value = m_objBrokerEntity.Phone2;
                        }
                        else
                        {
                            Fields["BROKER_PHONE"].Value = m_objBrokerEntity.Phone1;
                        }
                    }
                }
                Fields["SERVICED_BY"].Value = strServicedBy;

                //Add by kuladeep for MITS:24765/24301 Start
				sSQL = "SELECT DUE_DATE FROM BILL_X_INVOICE WHERE INVOICE_ROWID = " + m_objBillXNotice.PrevInvoiceRowid;
                    using(DbReader objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL))
                    {
                        while (objReader.Read())
                        {
                            strPastDueDate = Common.Conversion.GetDBDateFormat(Convert.ToString(objReader.GetValue("DUE_DATE")), "d");
                        }
                    }
                    Fields["PAST_DUE_DATE"].Value = strPastDueDate;
                //Add by kuladeep for MITS:24765/24301 End
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
            }
        }
        #endregion


    }
}
