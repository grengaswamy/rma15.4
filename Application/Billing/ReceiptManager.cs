﻿
using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;

using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /**************************************************************
     * $File		: ReceiptManager.cs
     * $Revision	: 1.0.0.0
     * $Date		: 
     * $Author		: Vaibav Kaushik/Nitesh Deedwania
     * $Comment		:  
     * $Source		:  	
    **************************************************************/
    public class ReceiptManager : IDisposable
    {
        #region Member Variables
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Private variable to store the instance of Local Cache object
        /// </summary>
        private LocalCache m_objLocalCache = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;

        private int m_iClientId = 0;

        private const string RECEIPT_NODE_NAME = "Receipts";
        private const string RECEIPT_PAYMENT_METHOD = "PaymentMethod";
        //npadhy RMSC retrofit Starts
        private const string RECEIPT_RCPT_TYPE = "RcptType";
        //npadhy RMSC retrofit Ends
        private const string RECEIPT_DATE_RECEIVED = "DateReceived";
        private const string RECEIPT_CHECK_NUMBER = "CheckNumber";
        private const string RECEIPT_CHECK_DATE = "CheckDate";
        private const string RECEIPT_AMOUNT = "Amount";
        private const string RECEIPT_DISABLEALL = "DisableControls";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor, initializes the variables to the default value .
        /// </summary>
        /// <param name="p_objDataModelFactory">DataModelFactory Object</param>
        public ReceiptManager(DataModelFactory p_objDataModelFactory,int p_iClientId)
        {
            m_objDataModelFactory = p_objDataModelFactory;
            m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString;
            m_iClientId = p_iClientId;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
        }
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>	
        public ReceiptManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
        {

            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
        }
        #endregion

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }

        public  XmlDocument GetReceiptData(int p_iBillItemRowId)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootNode = null;

            try
            {
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                if (p_iBillItemRowId != 0)
                    objBillXBillItem.MoveTo(p_iBillItemRowId);
                else
                    throw new RMAppException("ReceiptManager.GetReceiptData.NoBillingSelected");

                Function.StartDocument(ref m_objDocument, ref objRootNode, RECEIPT_NODE_NAME, m_iClientId);
                this.CreateReceiptItemXml(objRootNode, objBillXBillItem);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReceiptManager.GetReceiptData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objRootNode = null;
            }
            return (m_objDocument);
        }

        public XmlDocument GetNewReceiptData(int p_PolicyId, int p_iTermNumber,string p_sPolicyNumber)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootNode = null;
            try
            {

                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                if (p_PolicyId != 0)
                {
                    objBillXBillItem.PolicyId = p_PolicyId;
                    objBillXBillItem.PolicyNumber = p_sPolicyNumber;
                    objBillXBillItem.TermNumber = p_iTermNumber;
                    objBillXBillItem.BillingItemType = m_objLocalCache.GetCodeId("R", "BILLING_ITEM_TYPES");
                }
                else
                    throw new RMAppException("ReceiptManager.GetNewReceiptData.NoPolicySelect");

                Function.StartDocument(ref m_objDocument, ref objRootNode, RECEIPT_NODE_NAME, m_iClientId);
                this.CreateReceiptItemXml(objRootNode, objBillXBillItem);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReceiptManager.GetNewReceiptData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objRootNode = null;
            }
            return (m_objDocument);
        }
        private void CreateReceiptItemXml(XmlElement p_objParentNode, BillXBillItem p_objBillXBillItem)
        {
            XmlElement objNew = null;
            XmlDocument objXML = p_objParentNode.OwnerDocument;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            XmlCDataSection objCData = null;

            //Manish Multicurrency
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString, m_iClientId);

            objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), ref sShortCode, ref sDesc);
            sCurrency = sDesc.Split('|')[1];
            //XmlElement elem = objDOM.CreateElement("BaseCurrencyType");
            //elem.InnerText = sCurrency;
            //objDOM.DocumentElement.AppendChild(elem);

            try
            {


                //Function.CreateNodeWithCodeValues(p_objParentNode, RECEIPT_PAYMENT_METHOD, p_objBillXBillItem.BillXReceipt.ReceiptType, m_sConnectionString);
                Function.CreateAndSetElement(p_objParentNode, RECEIPT_PAYMENT_METHOD, Conversion.ConvertObjToStr(p_objBillXBillItem.BillXReceipt.ReceiptType), m_iClientId);
                //npadhy RMSC retrofit Starts

                Function.CreateAndSetElement(p_objParentNode, RECEIPT_RCPT_TYPE, Conversion.ConvertObjToStr(p_objBillXBillItem.BillXReceipt.RcptType), m_iClientId);
                //npadhy RMSC retrofit Ends
                Function.CreateAndSetElement(p_objParentNode, RECEIPT_DATE_RECEIVED, p_objBillXBillItem.BillXReceipt.DateReceived, m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, RECEIPT_CHECK_NUMBER, p_objBillXBillItem.BillXReceipt.CheckNumber.ToString(), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, RECEIPT_CHECK_DATE, Conversion.GetDBDateFormat(p_objBillXBillItem.BillXReceipt.CheckDate, "d"), m_iClientId);
                //Function.CreateAndSetElement(p_objParentNode, RECEIPT_AMOUNT, string.Format("{0:C}", Math.Abs(p_objBillXBillItem.Amount) * -1));Changes made for R5 : csingh7
                Function.CreateAndSetElement(p_objParentNode, RECEIPT_AMOUNT, Convert.ToString(p_objBillXBillItem.Amount * -1), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, "BaseCurrencyType", sCurrency, m_iClientId);
                Function.SetBillItemXml(p_objBillXBillItem, p_objParentNode, m_sConnectionString, m_iClientId);
                if (p_objBillXBillItem.Status != m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT") && p_objBillXBillItem.Status != 0)
                {
                    //Function.CreateAndSetElement(p_objParentNode, RECEIPT_DISABLEALL, "True");

                    XmlElement objTempElement = p_objParentNode.OwnerDocument.CreateElement("DisableControls");

                    XmlElement objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "PaymentMethod";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Amount";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "CheckNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "DateReceived";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "CheckDate";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "InvoiceNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "NoticeNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Comments";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Ok";
                    objTempElement.AppendChild(objChildElement);

                    p_objParentNode.AppendChild(objTempElement);
                }
                string sCodevalues = Function.GetCodes("RECEIPT_TYPES", m_sConnectionString, m_iClientId);
                //rupal:start,mits 27977
                //string[] sCodeArr = sCodevalues.Split('^');
                string[] sCodeArr = sCodevalues.Split(Function.NewCodeSeperator, StringSplitOptions.RemoveEmptyEntries);
                //rupal:end

                objNew = objXML.CreateElement("CodeList");

                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;

                for (int i = 0; i < sCodeArr.Length; i++)
                {
                    //rupal:start,mits 27977
                    //string[] sCodes = sCodeArr[i].Split('~');
                    string[] sCodes = sCodeArr[i].Split(Function.CodeIdSeperator, StringSplitOptions.RemoveEmptyEntries);
                    //rupal:end
                    xmlOption = objXML.CreateElement("option");
                    objCData = objXML.CreateCDataSection(sCodes[1]);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXML.CreateAttribute("value");

                    xmlOptionAttrib.Value = sCodes[0];

                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                }

                objXML.DocumentElement.AppendChild(objNew);
                //npadhy RMSC retrofit Starts
                sCodevalues = "";
                sCodeArr = null;
                sCodevalues = Function.GetCodes("RCPT_TYPE", m_sConnectionString, m_iClientId);
                //rupal:start, mits 27977
                //sCodeArr = sCodevalues.Split('^');
                sCodeArr = sCodevalues.Split(Function.NewCodeSeperator, StringSplitOptions.RemoveEmptyEntries);
                //rupal:end
                objNew = objXML.CreateElement("CodeListRcpt");
                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;
                for (int i = 0; i < sCodeArr.Length; i++)
                {
                    //rupal:start,mits 27977
                    //string[] sCodes = sCodeArr[i].Split('~');
                    string[] sCodes = sCodeArr[i].Split(Function.CodeIdSeperator, StringSplitOptions.RemoveEmptyEntries);
                    //rupal:end
                    xmlOption = objXML.CreateElement("option");
                    objCData = objXML.CreateCDataSection(sCodes[1]);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXML.CreateAttribute("value");
                    xmlOptionAttrib.Value = sCodes[0];
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                }
                objXML.DocumentElement.AppendChild(objNew);
                //npadhy RMSC retrofit Ends

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReceiptManager.CreateReceiptItemXml.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNew = null;
                objXML = null;
                xmlOption = null;
                xmlOptionAttrib = null;
                objCData = null;
            }
        }


        public void SaveReceipt(XmlDocument p_objInputDoc,int p_iBillItemRowId)
        {
            BillXBillItem objBillXBillItem = null;
            //PolicyBilling objPolicyBilling = null;

            string sDateReconciled = string.Empty;
            string sComment = string.Empty;
            string sReceiptDateReceived = string.Empty;
            string sReceiptCheckDate = string.Empty;
            string sPolicyNumber = string.Empty;
            int iPolicyId = 0;
            int iEntryBatchNumber = 0;
            int iEntryBatchType = 0;
            int iFinalBatchNumber = 0;
            int iFinalBatchType = 0;
            int iInvoiceNumber = 0;
            int iNoticeNumber = 0;
            int iBillingItemType = 0;
            int iStatus = 0;
            int iTermNumber = 0;
            int iReceiptCheckNumber = 0;
            int iReceiptType = 0;
            double dblAmount = 0.0;
            //npadhy RMSC retrofit Starts
            int iRcptType = 0;
            //npadhy RMSC retrofit Ends

            //string sStatus = string.Empty;
            //string sTermNumber = string.Empty;
            //string sFromDate = string.Empty;
            //string sToDate = string.Empty;

            try
            {
                //objPolicyBilling = new PolicyBilling(m_objDataModelFactory);
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                
                //Save Edited Data else save new record
                if (p_iBillItemRowId != 0)
                    objBillXBillItem.MoveTo(p_iBillItemRowId);

                //Shruti
                Function.GetBillItemXml(p_objInputDoc, ref sPolicyNumber, ref iPolicyId, ref sDateReconciled, ref iEntryBatchNumber, ref iEntryBatchType, ref iFinalBatchNumber, ref iFinalBatchType, ref iInvoiceNumber, ref iNoticeNumber, ref iStatus, ref iTermNumber, ref sComment, m_sConnectionString, m_iClientId);
                if (Function.GetValue(p_objInputDoc, RECEIPT_AMOUNT, m_iClientId).StartsWith(@"("))
                {
                    dblAmount = Math.Abs(Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, RECEIPT_AMOUNT, m_iClientId).Replace("(", "").Replace(")", ""))) * -1;
                }
                else
                {
                    dblAmount = Math.Abs(Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, RECEIPT_AMOUNT, m_iClientId))) * -1;
                }
                iBillingItemType = m_objLocalCache.GetCodeId("R", "BILLING_ITEM_TYPES");
                sReceiptDateReceived = Conversion.GetDate(Function.GetValue(p_objInputDoc, RECEIPT_DATE_RECEIVED, m_iClientId));
                //Shruti
                sReceiptCheckDate = Conversion.GetDate(Function.GetValue(p_objInputDoc, RECEIPT_CHECK_DATE, m_iClientId));
                iReceiptCheckNumber = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, RECEIPT_CHECK_NUMBER, m_iClientId));
                iReceiptType = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, RECEIPT_PAYMENT_METHOD, m_iClientId));
                //npadhy RMSC retrofit Starts
                iRcptType = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, RECEIPT_RCPT_TYPE, m_iClientId));
                //npadhy RMSC retrofit Ends
                //iReceiptType= Conversion.ConvertStrToInteger(Function.GetCodeValue(p_objInputDoc, RECEIPT_PAYMENT_METHOD));
                objBillXBillItem.Amount = dblAmount;
                objBillXBillItem.DateEntered = Conversion.GetDate(DateTime.Now.ToString("d"));
                objBillXBillItem.DateReconciled = sDateReconciled;
                objBillXBillItem.EntryBatchNum = iEntryBatchNumber;
                objBillXBillItem.EntryBatchType = iEntryBatchType;
                objBillXBillItem.FinalBatchNum = iFinalBatchNumber;
                objBillXBillItem.FinalBatchType = iFinalBatchType;
                objBillXBillItem.InvoiceId = iInvoiceNumber;
                objBillXBillItem.NoticeId = iNoticeNumber;
                objBillXBillItem.PolicyId = iPolicyId;
                objBillXBillItem.PolicyNumber = sPolicyNumber;
                objBillXBillItem.BillingItemType = iBillingItemType;
                objBillXBillItem.Status = iStatus;
                objBillXBillItem.TermNumber = iTermNumber;
                objBillXBillItem.Comments = sComment;
                objBillXBillItem.BillXReceipt.DateReceived = sReceiptDateReceived;
                objBillXBillItem.BillXReceipt.CheckNumber = iReceiptCheckNumber;
                objBillXBillItem.BillXReceipt.CheckDate = sReceiptCheckDate;
                objBillXBillItem.BillXReceipt.ReceiptType = iReceiptType;
                //npadhy RMSC retrofit Starts
                objBillXBillItem.BillXReceipt.RcptType = iRcptType;
                //npadhy RMSC retrofit Ends
                objBillXBillItem.Save();

                CreateAdjustment(objBillXBillItem);

                // Get the Updated XML for the Billing Screen.
                //m_objDocument = objPolicyBilling.GetPolicyBillingXml(iPolicyId, sStatus, sTermNumber, sFromDate, sToDate);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReceiptManager.SaveReceipt.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
            }
            //return (m_objDocument);
        }

        private void CreateAdjustment(BillXBillItem p_objBillXBillItem)
        {
            int i = 0;
            string sSql = "";
            double dDueAmt = 0;
            double dDisAmt = 0;
            string sDisType = "";
            double dAmount = 0;
            double dBAmount = 0;
            double dAdjAmount = 0;
            double dPremAmount = 0;
            int iTerm = 0;
            long lPId = 0;
            bool bRound = false;

            DbReader objReader = null;
            DbReader objReaderTmp = null;
            //LocalCache objLocalCache = null;
            BillXBillItem objBillItem = null;
            try
            {
                //objLocalCache = new LocalCache(m_sConnectionString);
                sSql = "Select AMOUNT_DUE, TERM_NUMBER, POLICY_ID from BILL_X_ACCOUNT where POLICY_ID = " + p_objBillXBillItem.PolicyId.ToString() + " and TERM_NUMBER = " + p_objBillXBillItem.TermNumber.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader.Read())
                {
                    dDueAmt = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT_DUE"), m_iClientId);
                    iTerm = Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), m_iClientId);
                    lPId = Conversion.ConvertObjToInt64(objReader.GetValue("POLICY_ID"), m_iClientId);
                }
                objReader.Close();

                bRound = Function.GetRoundAmountFlag(m_sConnectionString, m_iClientId);

                sSql = "Select POLICY_TYPE from POLICY_ENH where POLICY_ID  = " + p_objBillXBillItem.PolicyId.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader.Read())
                {
                    sSql = "Select * from SYS_ERLY_PAY_DSCNT where IN_USE_FLAG <> 0 and LINE_OF_BUSINESS  = " + Conversion.ConvertObjToStr(objReader.GetValue("POLICY_TYPE"));
                    objReaderTmp = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReaderTmp.Read())
                    {
                        dDisAmt = Conversion.ConvertObjToDouble(objReaderTmp.GetValue("AMOUNT"), m_iClientId);
                        sDisType = Conversion.ConvertObjToStr(objReaderTmp.GetValue("DISCOUNT_TYPE"));

                        if (m_objLocalCache.GetShortCode(Conversion.ConvertStrToInteger(sDisType)) == "P")
                        {
                            dAmount = 0.01 * dDisAmt * dDueAmt;
                            dBAmount = 0.01 * (100 - dDisAmt) * dDueAmt;
                        }
                        else
                        {
                            dAmount = dDisAmt;
                            dBAmount = dDueAmt - dDisAmt;
                        }

                        if (bRound)
                        {
                            dAdjAmount = Math.Round(dAmount, 0, MidpointRounding.AwayFromZero);
                            dPremAmount = Math.Round(dBAmount, 0, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            dAdjAmount = Math.Round(dAmount, 2, MidpointRounding.AwayFromZero);
                            dPremAmount = Math.Round(dBAmount, 2, MidpointRounding.AwayFromZero);
                        }

                        if (-(p_objBillXBillItem.Amount) >= dPremAmount)
                        {
                            if (Conversion.ConvertStrToInteger(p_objBillXBillItem.DateEntered) <= Conversion.ConvertObjToInt(objReaderTmp.GetString("DEADLINE_DATE"), m_iClientId))
                            {
                                objBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                                objBillItem.Amount = dAdjAmount * -1;
                                objBillItem.BillingItemType = m_objLocalCache.GetCodeId("A", "BILLING_ITEM_TYPES");
                                objBillItem.DateEntered = Conversion.GetDate(DateTime.Now.ToString("d"));
                                objBillItem.FinalBatchNum = p_objBillXBillItem.FinalBatchNum;
                                objBillItem.FinalBatchType = p_objBillXBillItem.FinalBatchType;
                                objBillItem.EntryBatchNum = p_objBillXBillItem.EntryBatchNum;
                                objBillItem.EntryBatchType = p_objBillXBillItem.EntryBatchType;
                                objBillItem.InvoiceId = p_objBillXBillItem.InvoiceId;
                                objBillItem.NoticeId = p_objBillXBillItem.NoticeId;
                                objBillItem.PolicyId = p_objBillXBillItem.PolicyId;
                                objBillItem.PolicyNumber = p_objBillXBillItem.PolicyNumber;
                                objBillItem.BillXAdjstmnt.AdjustmentType = m_objLocalCache.GetCodeId("ED", "ADJUSTMENT_TYPES");
                                objBillItem.Status = m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT");
                                objBillItem.TermNumber = iTerm;
                                objBillItem.Save();
                            }
                            else
                            {
                                if (p_objBillXBillItem.BillXReceipt.DateReceived != "")
                                {
                                    if (Conversion.ConvertStrToInteger(p_objBillXBillItem.BillXReceipt.DateReceived) <= Conversion.ConvertObjToInt(objReaderTmp.GetString("DEADLINE_DATE"), m_iClientId))
                                    {
                                        objBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                                        objBillItem.Amount = dAdjAmount * -1;
                                        objBillItem.BillingItemType = m_objLocalCache.GetCodeId("A", "BILLING_ITEM_TYPES");
                                        objBillItem.DateEntered = Conversion.GetDate(DateTime.Now.ToString("d"));
                                        objBillItem.FinalBatchNum = p_objBillXBillItem.FinalBatchNum;
                                        objBillItem.FinalBatchType = p_objBillXBillItem.FinalBatchType;
                                        objBillItem.EntryBatchNum = p_objBillXBillItem.EntryBatchNum;
                                        objBillItem.EntryBatchType = p_objBillXBillItem.EntryBatchType;
                                        objBillItem.InvoiceId = p_objBillXBillItem.InvoiceId;
                                        objBillItem.NoticeId = p_objBillXBillItem.NoticeId;
                                        objBillItem.PolicyId = p_objBillXBillItem.PolicyId;
                                        objBillItem.PolicyNumber = p_objBillXBillItem.PolicyNumber;
                                        objBillItem.BillXAdjstmnt.AdjustmentType = m_objLocalCache.GetCodeId("ED", "ADJUSTMENT_TYPES");
                                        objBillItem.Status = m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT");
                                        objBillItem.TermNumber = iTerm;
                                        objBillItem.Save();
                                    }
                                }
                            }

                        }

                    }
                    objReaderTmp.Close();
                }
                objReader.Close();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReceiptManager.SaveEPDAdjustment.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillItem != null)
                {
                    objBillItem.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objReaderTmp != null)
                {
                    objReaderTmp.Dispose();
                }
            }
            

        }
    }
}
