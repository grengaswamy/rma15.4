﻿

using System;
using System.IO ;
using System.Xml ;
using System.Text ;
using System.Collections;

using Riskmaster.Common ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.DataModel ;
using Riskmaster.Application.DocumentManagement ;
using Riskmaster.Application.FileStorage ;
using Riskmaster.Security ;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export.Pdf;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: InvoiceManger.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class InvoiceManager : IDisposable
	{
		#region "Member Variables"
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = string.Empty;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = string.Empty;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>		
		private string m_sDsnName = string.Empty;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty;
		/// <summary>
		/// Represents the save path for Printed Invoices PDF .
		/// </summary>
		private string m_sPdfSavePath = string.Empty ;        
        /// <summary>
        /// Represents the dummy Active Report used to merge invoices
        /// </summary>
        public Test objActiveReport;

        private int m_iClientId = 0;

		#endregion
		
        
		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value.
		/// </summary>
		/// <param name="p_sDsnName">DSN Name</param>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
        public InvoiceManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
            try
            {
                m_sUserName = p_sUserName;
                m_sPassword = p_sPassword;
                m_sDsnName = p_sDsnName;
                m_iClientId = p_iClientId;
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
                m_sPdfSavePath = Function.GetSavePath();
                objActiveReport = new Riskmaster.Application.EnhancePolicy.Billing.Test();
                objActiveReport.Run();
            }
            catch (Exception exc)
            {
                throw exc;
            }
		}
		#endregion

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (objActiveReport != null)
            {
                objActiveReport.Dispose();
            }
        }


        # region  Export Invoice
        
        /// <summary>
        /// Exports Invoices to PDF
        /// </summary>
        public void ExportInvoice()
        {
            PdfExport objPdfExport = new PdfExport();
            try
            {
                //Removing first page from the dummy Active report used for merging invoices
                objActiveReport.Document.Pages.Remove(objActiveReport.Document.Pages[0]);
                objPdfExport.Export(objActiveReport.Document, m_sPdfSavePath + "\\Invoices - "
                                    + Common.Conversion.ToDbDate(DateTime.Now) + " - "
                                    + DateTime.Now.TimeOfDay.ToString().Substring(0, 2)
                                    + DateTime.Now.TimeOfDay.ToString().Substring(3, 2)
                                    + DateTime.Now.TimeOfDay.ToString().Substring(6, 2)
                                    + " - " + m_sDsnName
                                    + ".pdf");
            }
            finally
            {
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                }
            }
        }

        # endregion       


        #region GL First, GL Other, WC Full Pay, WC Audit, WC Pay Plan First, WC Pay Plan Other :: Methods To Print Report

        private void PrintGLFirstInvoice( int p_iInvoiceId, BillingMaster p_objBillingMaster)
		{
			GLFirstInvoice objGLFirstInvoice = null ;
			PdfExport objPdfExport = null ;
			DocumentManager objDocManager = null ;
			StringBuilder sbDocumentXml = null ;
			MemoryStream objAttachDocStream = null ;
			long lDocumentId = 0 ;
			string sPdfFileName = string.Empty ;

			try
			{
				// Create Instance Of the Report Class.
				objGLFirstInvoice = new GLFirstInvoice( m_objDataModelFactory,m_iClientId); 
				
				// Gather Data.					
				objGLFirstInvoice.GatherData( p_iInvoiceId , p_objBillingMaster );

				// Run the report.
				objGLFirstInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objGLFirstInvoice.Document.Pages);

                // PDF File Name.
                //sPdfFileName = "GLFirstInvoice" + objGLFirstInvoice.Invoice.BillDate + "PolicyID" + objGLFirstInvoice.Invoice.PolicyId + ".pdf";
                //p_sPDFSaveFilePath = m_sPdfSavePath + sPdfFileName;

                // Export the report to the PDF file.
                //objPdfExport = new PdfExport();
                //objPdfExport.Export(objGLFirstInvoice.Document, p_sPDFSaveFilePath);

                //// Attach the PDF to the Invoice.				
                //objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser);
                //objDocManager.ConnectionString = m_sConnectionString;
                //objDocManager.UserLoginName = m_sUserName;
                //objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
                //objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
                //objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
                //sbDocumentXml = new StringBuilder();			
                //sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
                //sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
                //sbDocumentXml.Append( "<Category></Category><Name>GL First Invoice</Name><Class></Class><Subject>Invoice</Subject>" );
                //sbDocumentXml.Append( "<Type></Type>" );
                //sbDocumentXml.Append( "<Notes>Policy Id: " + objGLFirstInvoice.Invoice.PolicyId.ToString() + "</Notes>" );
                //sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
                //sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
                //sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
                //sbDocumentXml.Append( "<Keywords></Keywords>" );
                //sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
                //sbDocumentXml.Append( "<AttachRecordId>" + objGLFirstInvoice.Invoice.PolicyId.ToString() + "</AttachRecordId>" );
                //sbDocumentXml.Append( "</Document></data>" ); 						 	
                //objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintGLFirstInvoice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objGLFirstInvoice != null)
                {
                    objGLFirstInvoice.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}
				objDocManager = null ;
				sbDocumentXml = null ;
				objAttachDocStream = null ;
			}
		}


		private void PrintGLOtherInvoices( int p_iInvoiceId, BillingMaster p_objBillingMaster)
		{
			GLSecondInvoice objGLSecondInvoice = null ;
			PdfExport objPdfExport = null ;

			string sPdfFileName = string.Empty ;

			try
			{
				// Create Instance Of the Report Class.
				objGLSecondInvoice = new GLSecondInvoice( m_objDataModelFactory,m_iClientId ); 
				
				// Gather Data.					
				objGLSecondInvoice.GatherData( p_iInvoiceId , p_objBillingMaster );

				// Run the report.
				objGLSecondInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objGLSecondInvoice.Document.Pages);             				
				
				// PDF File Name.
				//sPdfFileName = "GLOtherInvoice" + objGLSecondInvoice.Invoice.BillDate + "PolicyID" + objGLSecondInvoice.Invoice.PolicyId + ".pdf" ;
				//p_sPDFSaveFilePath = m_sPdfSavePath +  sPdfFileName ;

                // Export the report to the PDF file.
                //objPdfExport = new PdfExport();
                //objPdfExport.Export( objGLSecondInvoice.Document, p_sPDFSaveFilePath );
				
                //// Attach the PDF to the Invoice.				
                //objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser);
                //objDocManager.ConnectionString = m_sConnectionString;
                //objDocManager.UserLoginName = m_sUserName;
                //objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
                //objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
                //objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
                //sbDocumentXml = new StringBuilder();			
                //sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
                //sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
                //sbDocumentXml.Append( "<Category></Category><Name>GL Additional Invoice</Name><Class></Class><Subject>Invoice</Subject>" );
                //sbDocumentXml.Append( "<Type></Type>" );
                //sbDocumentXml.Append( "<Notes>Policy Id: " + objGLSecondInvoice.Invoice.PolicyId.ToString() + "</Notes>" );
                //sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
                //sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
                //sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
                //sbDocumentXml.Append( "<Keywords></Keywords>" );
                //sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
                //sbDocumentXml.Append( "<AttachRecordId>" + objGLSecondInvoice.Invoice.PolicyId.ToString() + "</AttachRecordId>" );
                //sbDocumentXml.Append( "</Document></data>" ); 						 	
                //objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintGLOtherInvoices.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objGLSecondInvoice != null)
                {
                    objGLSecondInvoice.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}
			}
		}

        //Start:Added by Nitin Goel:To generate Invoice for vehicle and property ,06/10/2010
        /// <summary>
        /// Method to generate AL first Invoice
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_objBillingMaster">Object of Billing Master</param>
        private void PrintALFirstInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
        {
            ALFirstInvoice objALFirstInvoice = null;
            PdfExport objPdfExport = null;
            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objALFirstInvoice = new ALFirstInvoice(m_objDataModelFactory, m_iClientId);

                // Gather Data.					
                objALFirstInvoice.GatherData(p_iInvoiceId, p_objBillingMaster);

                // Run the report.
                objALFirstInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objALFirstInvoice.Document.Pages);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintALFirstInvoice.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objALFirstInvoice != null)
                {
                    objALFirstInvoice.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }
            }
        }
        /// <summary>
        /// Method to generate AL other Invoices
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_objBillingMaster">Object of Billing Master</param>

        private void PrintALOtherInvoices(int p_iInvoiceId, BillingMaster p_objBillingMaster)
        {
            ALSecondInvoice objALSecondInvoice = null;
            PdfExport objPdfExport = null;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objALSecondInvoice = new ALSecondInvoice(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objALSecondInvoice.GatherData(p_iInvoiceId, p_objBillingMaster);

                // Run the report.
                objALSecondInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objALSecondInvoice.Document.Pages);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintALOtherInvoices.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objALSecondInvoice != null)
                {
                    objALSecondInvoice.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }
            }
        }

        /// <summary>
        /// Method to generate PC First Invoice
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_objBillingMaster">Object of Billing Master</param>
        private void PrintPCFirstInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
        {
            PCFirstInvoice objPCFirstInvoice = null;
            PdfExport objPdfExport = null;
            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objPCFirstInvoice = new PCFirstInvoice(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objPCFirstInvoice.GatherData(p_iInvoiceId, p_objBillingMaster);

                // Run the report.
                objPCFirstInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objPCFirstInvoice.Document.Pages);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrinPCFirstInvoice.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objPCFirstInvoice != null)
                {
                    objPCFirstInvoice.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }
            }
        }

        /// <summary>
        /// Method to generate PC other Invoices
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_objBillingMaster">Object of Billing Master</param>
        private void PrintPCOtherInvoices(int p_iInvoiceId, BillingMaster p_objBillingMaster)
        {
            PCSecondInvoice objPCSecondInvoice = null;
            PdfExport objPdfExport = null;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objPCSecondInvoice = new PCSecondInvoice(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objPCSecondInvoice.GatherData(p_iInvoiceId, p_objBillingMaster);

                // Run the report.
                objPCSecondInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objPCSecondInvoice.Document.Pages);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintPCOtherInvoices.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objPCSecondInvoice != null)
                {
                    objPCSecondInvoice.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }
            }
        }
        //End:Nitin Goel:To generate Invoice for vehicle and property,06/10/2010
        private void PrintWCFullPayInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
		{
			WCFullPayInvoice objWCFullPayInvoice = null ;
			PdfExport objPdfExport = null ;

			string sPdfFileName = string.Empty ;

			try
			{
				// Create Instance Of the Report Class.
				objWCFullPayInvoice = new WCFullPayInvoice( m_objDataModelFactory,m_iClientId ); 
				
				// Gather Data.					
				objWCFullPayInvoice.GatherData( p_iInvoiceId , p_objBillingMaster );

				// Run the report.
				objWCFullPayInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objWCFullPayInvoice.Document.Pages);				
				
				// PDF File Name.
				//sPdfFileName = "WCFullPayInvoice" + objWCFullPayInvoice.Invoice.BillDate + "PolicyID" + objWCFullPayInvoice.Invoice.PolicyId + ".pdf" ;
				//p_sPDFSaveFilePath = m_sPdfSavePath +  sPdfFileName ;

				// Export the report to the PDF file.
                //objPdfExport = new PdfExport();
                //objPdfExport.Export( objWCFullPayInvoice.Document, p_sPDFSaveFilePath );
				
                //// Attach the PDF to the Invoice.				
                //objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser);
                //objDocManager.ConnectionString = m_sConnectionString;
                //objDocManager.UserLoginName = m_sUserName;
                //objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
                //objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
                //objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
                //sbDocumentXml = new StringBuilder();			
                //sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
                //sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
                //sbDocumentXml.Append( "<Category></Category><Name>WC Full Pay Invoice</Name><Class></Class><Subject>Invoice</Subject>" );
                //sbDocumentXml.Append( "<Type></Type>" );
                //sbDocumentXml.Append( "<Notes>Policy Id: " + objWCFullPayInvoice.Invoice.PolicyId.ToString() + "</Notes>" );
                //sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
                //sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
                //sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
                //sbDocumentXml.Append( "<Keywords></Keywords>" );
                //sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
                //sbDocumentXml.Append( "<AttachRecordId>" + objWCFullPayInvoice.Invoice.PolicyId.ToString() + "</AttachRecordId>" );
                //sbDocumentXml.Append( "</Document></data>" ); 						 	
                //objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintWCFullPayInvoice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objWCFullPayInvoice != null)
                {
                    objWCFullPayInvoice.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}
			}
		}


        private void PrintWCPPFirstInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
		{
			WCPPFirstInvoice objWCPPFirstInvoice = null ;
			PdfExport objPdfExport = null ;

			string sPdfFileName = string.Empty ;

			try
			{
				// Create Instance Of the Report Class.
				objWCPPFirstInvoice = new WCPPFirstInvoice( m_objDataModelFactory,m_iClientId); 
				
				// Gather Data.					
				objWCPPFirstInvoice.GatherData( p_iInvoiceId , p_objBillingMaster );

				// Run the report.
				objWCPPFirstInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objWCPPFirstInvoice.Document.Pages);              
				
				// PDF File Name.
				//sPdfFileName = "WCPayPlanFirstInvoice" + objWCPPFirstInvoice.Invoice.BillDate + "PolicyID" + objWCPPFirstInvoice.Invoice.PolicyId + ".pdf" ;
				//p_sPDFSaveFilePath = m_sPdfSavePath +  sPdfFileName ;

				// Export the report to the PDF file.
                //objPdfExport = new PdfExport();
                //objPdfExport.Export( objWCPPFirstInvoice.Document, p_sPDFSaveFilePath );
				
                //// Attach the PDF to the Invoice.				
                //objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser);
                //objDocManager.ConnectionString = m_sConnectionString;
                //objDocManager.UserLoginName = m_sUserName;
                //objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
                //objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
                //objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
                //sbDocumentXml = new StringBuilder();			
                //sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
                //sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
                //sbDocumentXml.Append( "<Category></Category><Name>WC Pay Plan First Invoice</Name><Class></Class><Subject>Invoice</Subject>" );
                //sbDocumentXml.Append( "<Type></Type>" );
                //sbDocumentXml.Append( "<Notes>Policy Id: " + objWCPPFirstInvoice.Invoice.PolicyId.ToString() + "</Notes>" );
                //sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
                //sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
                //sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
                //sbDocumentXml.Append( "<Keywords></Keywords>" );
                //sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
                //sbDocumentXml.Append( "<AttachRecordId>" + objWCPPFirstInvoice.Invoice.PolicyId.ToString() + "</AttachRecordId>" );
                //sbDocumentXml.Append( "</Document></data>" ); 						 	
                //objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintWCPPFirstInvoice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objWCPPFirstInvoice != null)
                {
                    objWCPPFirstInvoice.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}

			}
		}


        private void PrintWCPPOtherInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
		{
			WCPayPlanExtraInvoices objWCPayPlanExtraInvoices = null ;
			PdfExport objPdfExport = null ;

			string sPdfFileName = string.Empty ;

			try
			{
				// Create Instance Of the Report Class.
				objWCPayPlanExtraInvoices = new WCPayPlanExtraInvoices( m_objDataModelFactory,m_iClientId ); 
				
				// Gather Data.					
				objWCPayPlanExtraInvoices.GatherData( p_iInvoiceId , p_objBillingMaster );

				// Run the report.
				objWCPayPlanExtraInvoices.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objWCPayPlanExtraInvoices.Document.Pages);

                //objPdfExport = new PdfExport();
                // objPdfExport.Export(objActiveReport.Document, @"C:\DotNet\Riskmaster\UI\riskmaster\userdata\Billing\Invoice1.pdf");

                // objWCPayPlanExtraInvoices = null ;

              	//PDF File Name.
				//sPdfFileName = "WCAdditionalInvoice" + objWCPayPlanExtraInvoices.Invoice.BillDate + "PolicyID" + objWCPayPlanExtraInvoices.Invoice.PolicyId + ".pdf" ;
				//p_sPDFSaveFilePath = m_sPdfSavePath +  sPdfFileName ;
                //p_sPDFSaveFilePath = sPdfFileName; ;

				// Export the report to the PDF file.
                // objPdfExport = new PdfExport();
                // objPdfExport.Export( objWCPayPlanExtraInvoices.Document, p_sPDFSaveFilePath );

                // objPdfExport.Export(objActiveReport.Document, @"C:\DotNet\Riskmaster\UI\riskmaster\userdata\Billing\a.pdf");

                //// Attach the PDF to the Invoice.				
                //objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser);
                //objDocManager.ConnectionString = m_sConnectionString;
                //objDocManager.UserLoginName = m_sUserName;
                //objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
                //objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
                //objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
                //sbDocumentXml = new StringBuilder();			
                //sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
                //sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
                //sbDocumentXml.Append( "<Category></Category><Name>WC Additional Invoice</Name><Class></Class><Subject>Invoice</Subject>" );
                //sbDocumentXml.Append( "<Type></Type>" );
                //sbDocumentXml.Append( "<Notes>Policy Id: " + objWCPayPlanExtraInvoices.Invoice.PolicyId.ToString() + "</Notes>" );
                //sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
                //sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
                //sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
                //sbDocumentXml.Append( "<Keywords></Keywords>" );
                //sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
                //sbDocumentXml.Append( "<AttachRecordId>" + objWCPayPlanExtraInvoices.Invoice.PolicyId.ToString() + "</AttachRecordId>" );
                //sbDocumentXml.Append( "</Document></data>" ); 						 	
                //objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("InvoiceManager.PriPrintWCPPOtherInvoice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objWCPayPlanExtraInvoices != null)
                {
                    objWCPayPlanExtraInvoices.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}

			}
		}


        private void PrintWCAuditInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
		{
            WCAuditInvoice objWCAuditInvoice = null;
            PdfExport objPdfExport = null;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objWCAuditInvoice = new WCAuditInvoice(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objWCAuditInvoice.GatherData(p_iInvoiceId, p_objBillingMaster);

                // Run the report.
                objWCAuditInvoice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objWCAuditInvoice.Document.Pages);

                // PDF File Name.
                //sPdfFileName = "WCAuditInvoice" + objWCAuditInvoice.Invoice.BillDate + "PolicyID" + objWCAuditInvoice.Invoice.PolicyId + ".pdf";
                //p_sPDFSaveFilePath = m_sPdfSavePath + sPdfFileName;

                // Export the report to the PDF file.
                //objPdfExport = new PdfExport();
                //objPdfExport.Export( objWCAuditInvoice.Document, p_sPDFSaveFilePath );
                
				
            //    // Attach the PDF to the Invoice.				
            //    objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser);
            //    objDocManager.ConnectionString = m_sConnectionString;
            //    objDocManager.UserLoginName = m_sUserName;
            //    objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
            //    objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
            //    objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
            //    sbDocumentXml = new StringBuilder();			
            //    sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
            //    sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
            //    sbDocumentXml.Append( "<Category></Category><Name>WC Audit Invoice</Name><Class></Class><Subject>Invoice</Subject>" );
            //    sbDocumentXml.Append( "<Type></Type>" );
            //    sbDocumentXml.Append( "<Notes>Policy Id: " + objWCAuditInvoice.Invoice.PolicyId.ToString() + "</Notes>" );
            //    sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
            //    sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
            //    sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
            //    sbDocumentXml.Append( "<Keywords></Keywords>" );
            //    sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
            //    sbDocumentXml.Append( "<AttachRecordId>" + objWCAuditInvoice.Invoice.PolicyId.ToString() + "</AttachRecordId>" );
            //    sbDocumentXml.Append( "</Document></data>" ); 						 	
            //    objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintWCAuditInvoice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objWCAuditInvoice != null)
                {
                    objWCAuditInvoice.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}

			}
		}

        //npadhy RMSC retrofit Starts
        public void PrintWCPremiumInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
        {
            //************************************************
            PremiumInvoiceTax objPremiumInvoiceTax = null;
            PdfExport objPdfExport = null;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objPremiumInvoiceTax = new PremiumInvoiceTax(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objPremiumInvoiceTax.GatherData(p_iInvoiceId, p_objBillingMaster);

                // Run the report.
                objPremiumInvoiceTax.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objPremiumInvoiceTax.Document.Pages);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintWCPremiumInvoice.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objPremiumInvoiceTax != null)
                {
                    objPremiumInvoiceTax.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }

            }

            //************************************************
        }

        private void PrintWCAuditInvoiceTax(int p_iInvoiceId, BillingMaster p_objBillingMaster)
        {
            AuditInvoiceTax objWCAuditInvoiceTax = null;
            PdfExport objPdfExport = null;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objWCAuditInvoiceTax = new AuditInvoiceTax(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objWCAuditInvoiceTax.GatherData(p_iInvoiceId, p_objBillingMaster);

                // Run the report.
                objWCAuditInvoiceTax.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objWCAuditInvoiceTax.Document.Pages);
                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintWCAuditInvoiceTax.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objWCAuditInvoiceTax != null)
                {
                    objWCAuditInvoiceTax.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }

            }
        }

        //Animesh Insertion ends

		#endregion 

		#region Print Invoice
        public void PrintInvoice(int p_iInvoiceId, BillingMaster p_objBillingMaster)
        {
            BillXInvoice objBillXInvoice = null;
            BillXAccount objBillXAccount = null;
            PolicyEnh objPolicyEnh = null;
            LocalCache objLocalCache = null;

            string sPolicyType = string.Empty;
            bool bInvoiceFound = false;

            // Changes because of RMSC Retrofit
            string seff = string.Empty;
            string sexp = string.Empty;
            string sSQL = string.Empty;

            DbReader objReader = null;
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                objBillXInvoice = (BillXInvoice)p_objBillingMaster.Invoices[p_iInvoiceId];
                if (objBillXInvoice != null)
                {
                    objBillXAccount = (BillXAccount)m_objDataModelFactory.GetDataModelObject("BillXAccount", false);
                    objBillXAccount.MoveTo(objBillXInvoice.BillAccountRowid);
                    //objBillXAccount.BillAccountRowid = objBillXInvoice.BillAccountRowid ;

                    objPolicyEnh = (PolicyEnh)p_objBillingMaster.Policies[objBillXInvoice.PolicyId];
                    if (objPolicyEnh != null)
                    {
                        sPolicyType = objLocalCache.GetShortCode(objPolicyEnh.PolicyType);
                        bInvoiceFound = true;
                    }
                }

                if (!bInvoiceFound)
                {
                    objBillXInvoice = (BillXInvoice)m_objDataModelFactory.GetDataModelObject("BillXInvoice", false);
                    //objBillXInvoice.InvoiceRowid = p_iInvoiceId ;
                    objBillXInvoice.MoveTo(p_iInvoiceId);
                    //objBillXInvoice.Refresh();
                    p_objBillingMaster.Invoices.Add(p_iInvoiceId, objBillXInvoice);

                    objBillXAccount = (BillXAccount)m_objDataModelFactory.GetDataModelObject("BillXAccount", false);
                    //objBillXAccount.BillAccountRowid = objBillXInvoice.BillAccountRowid ;
                    objBillXAccount.MoveTo(objBillXInvoice.BillAccountRowid);

                    objPolicyEnh = (PolicyEnh)m_objDataModelFactory.GetDataModelObject("PolicyEnh", false);
                    objPolicyEnh.MoveTo(objBillXInvoice.PolicyId);
                    //objPolicyEnh.PolicyId = objBillXInvoice.PolicyId ;
                    //objPolicyEnh.Refresh();				
                    p_objBillingMaster.Policies.Add(objBillXInvoice.PolicyId, objPolicyEnh);

                    sPolicyType = objLocalCache.GetShortCode(objPolicyEnh.PolicyType);
                }

                //changed by gagan for mits 18853 : Start
                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                {
                    foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                    {
                        if (objPolicyXTermEnh.TermNumber == objPolicyXTransEnh.TermNumber && objPolicyXTermEnh.SequenceAlpha == objPolicyXTransEnh.TermSeqAlpha)
                        {
                            seff = objPolicyXTermEnh.EffectiveDate;
                            sexp = objPolicyXTermEnh.ExpirationDate;
                            sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")"
                                    + "AND EFFECTIVE_DATE <= '" + seff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + seff + "') AND IN_USE_FLAG <> 0";

                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objReader.Read())
                            {
                                p_objBillingMaster.Account.UseTax = true;
                                p_objBillingMaster.Account.TaxRate = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                                p_objBillingMaster.Account.TaxType = Conversion.ConvertObjToInt(objReader.GetValue("FLAT_OR_PERCENT"), m_iClientId);
                            }
                            break;
                        }
                    }
                }

                if (objReader != null)
                    objReader.Close();

                //changed by gagan for mits 18853 : End

                //objBillXAccount.Refresh();

                if (sPolicyType == "GL")
                {
                    if (objBillXInvoice.PrevInvoiceRowid == 0 && objBillXInvoice.PrevNoticeRowid == 0)
                    {
                        this.PrintGLFirstInvoice(p_iInvoiceId, p_objBillingMaster);
                    }
                    else
                    {
                        this.PrintGLOtherInvoices(p_iInvoiceId, p_objBillingMaster);
                    }
                }
                //Start:Added by Nitin Goel:To generate Invoice for vehicle and property ,06/10/2010
                else if (sPolicyType == "AL")
                {
                    if (objBillXInvoice.PrevInvoiceRowid == 0 && objBillXInvoice.PrevNoticeRowid == 0)
                    {
                        this.PrintALFirstInvoice(p_iInvoiceId, p_objBillingMaster);
                    }
                    else
                    {
                        this.PrintALOtherInvoices(p_iInvoiceId, p_objBillingMaster);
                    }
                }
                else if (sPolicyType == "PC")
                {
                    if (objBillXInvoice.PrevInvoiceRowid == 0 && objBillXInvoice.PrevNoticeRowid == 0)
                    {
                        this.PrintPCFirstInvoice(p_iInvoiceId, p_objBillingMaster);
                    }
                    else
                    {
                        this.PrintPCOtherInvoices(p_iInvoiceId, p_objBillingMaster);
                    }
                }
                //End:Nitin Goel:To generate Invoice for vehicle and property,06/10/2010
                else
                {
                    if (objBillXInvoice.PolicyTransId == 0)
                    {
                        if (objBillXInvoice.PrevInvoiceRowid == 0 && objBillXInvoice.PrevNoticeRowid == 0)
                        {
                            if (objBillXAccount.PayPlanRowid == 0)
                            {
                                this.PrintWCFullPayInvoice(p_iInvoiceId, p_objBillingMaster);
                            }
                            else
                            {
                                //npadhy RMSC retrofit Starts
                                // We will Print the Invoices corresponding to Tax if UseTax is True.
                                // Otherwise we will print the Legacy Invoice
                                if (p_objBillingMaster.Account.UseTax)
                                {
                                    this.PrintWCPremiumInvoice(p_iInvoiceId, p_objBillingMaster);
                                }
                                else
                                {
                                    this.PrintWCPPFirstInvoice(p_iInvoiceId, p_objBillingMaster);
                                }
                                //npadhy RMSC retrofit Ends
                            }
                        }
                        else
                        {
                            //npadhy RMSC retrofit Starts
                            // We will Print the Invoices corresponding to Tax if UseTax is True.
                            // Otherwise we will print the Legacy Invoice
                            if (p_objBillingMaster.Account.UseTax)
                            {
                                this.PrintWCPremiumInvoice(p_iInvoiceId, p_objBillingMaster);
                            }
                            else
                            {
                                this.PrintWCPPOtherInvoice(p_iInvoiceId, p_objBillingMaster);
                            }
                            //npadhy RMSC retrofit Ends
                        }
                    }
                    else
                    {
                        // Print the audit
                        //npadhy RMSC retrofit Starts
                        // We will Print the Invoices corresponding to Tax if UseTax is True.
                        // Otherwise we will print the Legacy Invoice
                        if (p_objBillingMaster.Account.UseTax)
                        {
                            this.PrintWCAuditInvoiceTax(p_iInvoiceId, p_objBillingMaster);
                        }
                        else
                        {
                            this.PrintWCAuditInvoice(p_iInvoiceId, p_objBillingMaster);
                        }
                        //npadhy RMSC retrofit Ends
                    }
                    Console.WriteLine("0 ^*^*^ Invoice Generated for Invoice Id {0} and Policy Id {1}", p_iInvoiceId, objBillXInvoice.PolicyId);
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceManager.PrintInvoice.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXInvoice != null)
                {
                    objBillXInvoice.Dispose();
                    objBillXInvoice = null;
                }
                if (objBillXAccount != null)
                {
                    objBillXAccount.Dispose();
                    objBillXAccount = null;
                }
                if (objPolicyEnh != null)
                {
                    objPolicyEnh.Dispose();
                    objPolicyEnh = null;
                }
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
                if (objReader != null)
                    objReader.Close();

                //changed by gagan for mits 18853 : End

            }
        }



        
        /// <summary>
        /// Generates single batch pdf for Invoices
        /// </summary>
        public void PrintInvoice()
        {
            bool bPrinting;
            string ssql = String.Empty;

            BillingMaster objBillingMaster = null;    
            WpaDiaryEntry objWpaDiaryEntry = null;
            Db.DbReader objReader = null;
            Db.DbReader objTempReader = null;
            string sPdfOutputPath;
            ArrayList objDiaries = new ArrayList();
            ArrayList objInvoicesToPrint = new ArrayList();
            bool bError = false;
         
            string strConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

            try
            {
                ssql = "SELECT ENTRY_ID , ATTACH_RECORDID FROM WPA_DIARY_ENTRY WHERE ENTRY_NAME = 'Print Invoice'"
                        + " AND COMPLETE_DATE <= '" + DateTime.Now.ToString("yyyyMMdd")
                        + "' AND STATUS_OPEN <>0 AND DIARY_VOID = 0 AND DIARY_DELETED = 0"
                        + " ORDER BY COMPLETE_DATE";
                
                objReader = DbFactory.GetDbReader(strConnectionString, ssql);

                while (objReader.Read())
                {
                            ssql = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + objReader.GetInt(1);

                            objTempReader = DbFactory.GetDbReader(strConnectionString, ssql);

                            if (objTempReader.Read())
                            {
                                objDiaries.Add(objReader.GetInt(0));
                            }
                            else
                            {
                                objWpaDiaryEntry = (WpaDiaryEntry)m_objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);
                                objWpaDiaryEntry.MoveTo(objReader.GetInt(0));
                                objWpaDiaryEntry.StatusOpen = false;
                                objWpaDiaryEntry.Save();
                            }
                            objTempReader.Close();
                        }

                objBillingMaster = new BillingMaster(m_objDataModelFactory, m_iClientId);
                //objInvoiceManager = new InvoiceManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password);

                //changed for mits 12454 : start


                //Removing diairies that are duplicate so that only one clubbed invoice is generated for all similar diaires
                //Storing only one copy of duplicate diaries
                ArrayList objWPADiaries = new ArrayList();
                ArrayList objDiariesToRemove = new ArrayList();
                bool bFound = false;
                foreach (object objlngEntryId in objDiaries)
                {
                    bFound = false;

                    objWpaDiaryEntry = (WpaDiaryEntry)m_objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);
                    objWpaDiaryEntry.MoveTo((int)objlngEntryId);

                    foreach (object objWpaDiary in objWPADiaries)
                    {
                        if (objWpaDiaryEntry.CompleteDate == ((WpaDiaryEntry)objWpaDiary).CompleteDate &&
                            (objWpaDiaryEntry.AttachRecordid == ((WpaDiaryEntry)objWpaDiary).AttachRecordid))
                        {
                            bFound = true;
                            break;
                        }
                    }

                    if (!bFound)
                    {
                        objWPADiaries.Add(objWpaDiaryEntry);
                    }
                    else
                    {
                        //objDiaries.Remove(objlngEntryId);
                        objDiariesToRemove.Add(objlngEntryId);
                        objWpaDiaryEntry.StatusOpen = false;
                        objWpaDiaryEntry.Save();
                    }
                }

                //Remove duplicate diaries
                foreach (object objlngEntryId in objDiariesToRemove)
                {
                    objDiaries.Remove(objlngEntryId);
                }

                //changed for mits 12454 : end

                //Create Invoice and add to print invoice collection
                //for each entry
                foreach (object objlngEntryId in objDiaries)
                {
                    bError = false;

                    objWpaDiaryEntry = (WpaDiaryEntry)m_objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);
                    objWpaDiaryEntry.MoveTo((int)objlngEntryId);
                    objBillingMaster.Diaries.Add(objWpaDiaryEntry.EntryId, objWpaDiaryEntry);
                    //objInvoiceManager.CreateInvoice(objWpaDiaryEntry, objBillingMaster);
                    try
                    {
                        CreateInvoice(objWpaDiaryEntry, objBillingMaster);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("1001 ^*^*^ {0} ", ex.Message);
                        Log.Write(ex.Message, "CommonWebServiceLog", m_iClientId);                        
                        //TO DO
                        //Log.Write(ex.Message, "Default");
                        bError = true;
                    }

                    objWpaDiaryEntry.StatusOpen = false;
                    objBillingMaster.Save();

                    if (!bError)
                    {
                        foreach (int objKey in objBillingMaster.Invoices.Keys)
                        {
                            if (!objInvoicesToPrint.Contains(((BillXInvoice)objBillingMaster.Invoices
                                [objKey]).InvoiceRowid))    //csingh7 06/04/2010 : MITS 18321 
                            {        
                            objInvoicesToPrint.Add(((BillXInvoice)objBillingMaster.Invoices
                                [objKey]).InvoiceRowid);
                            break;
                            }         
                        }
                    }
                }

                //Check how many invoices need to be printed
                if (objInvoicesToPrint.Count == 0)
                {
                    Console.WriteLine("0 ^*^*^ There were no invoices to be printed");
                    return;                    
                }

                //Generate/Print  report for each invoice
                foreach (object objInVoiceRowId in objInvoicesToPrint)
                {
                    bPrinting = true;
                    //objInvoiceManager.PrintInvoice((int)objInVoiceRowId, objBillingMaster);
                    try
                    {
                        this.PrintInvoice((int)objInVoiceRowId, objBillingMaster);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("1001 ^*^*^ {0} ", ex.Message);
                        Log.Write(ex.Message, "CommonWebServiceLog", m_iClientId);                        
                        //TO DO
                        //Log.Write(ex.Message, "Default");
                    }

                }

                //Export invoices to a single batch pdf
                //objInvoiceManager.ExportInvoice();
                this.ExportInvoice();
                
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objWpaDiaryEntry != null)
                {
                    objWpaDiaryEntry.Dispose();
                }
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                }

                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDiaries = null;
                objInvoicesToPrint = null;
            }
        }



		
        //public void PrintInvoice( int p_DiaryEntryId , out string p_sPDFSaveFilePath )
        //{
        //    BillingMaster objBillingMaster = null ;
        //    WpaDiaryEntry objWpaDiaryEntry = null ;
        //    BillXInvoice objInvoice = null ;		

        //    try
        //    {
        //        objBillingMaster = new BillingMaster( m_objDataModelFactory );
        //        objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
        //        objWpaDiaryEntry.MoveTo(p_DiaryEntryId);
        //        //objWpaDiaryEntry.EntryId = p_DiaryEntryId ;
        //        //objWpaDiaryEntry.Refresh();

        //        objBillingMaster.Diaries.Add( objWpaDiaryEntry.EntryId , objWpaDiaryEntry );
        //        this.CreateInvoice( objWpaDiaryEntry , objBillingMaster );
        //        objWpaDiaryEntry.StatusOpen = false ;
        //        // TODO
        //        //objBillingMaster.Save();

        //        foreach( string sKey in objBillingMaster.Invoices.Keys )
        //        {
        //            objInvoice = (BillXInvoice)objBillingMaster.Invoices[sKey] ;
        //            break;
        //        }

        //        this.PrintInvoice(  objInvoice.InvoiceRowid , objBillingMaster , out p_sPDFSaveFilePath );
        //    }
        //    catch( RMAppException p_objEx )
        //    {
        //        throw p_objEx ;
        //    }
        //    catch( Exception p_objEx )
        //    {
        //        throw new RMAppException(Globalization.GetString("InvoiceManager.PrintInvoice.Error"), p_objEx);				
        //    }
        //}

		#endregion 

		#region Create Invoice
		public void CreateInvoice( WpaDiaryEntry p_objDiary , BillingMaster p_objBillingMaster )
		{
			BillingManager objBillingManager = null ;
			BillXNotice objLastNotice = null ;
			BillXInvoice objLastInvoice = null ;
			BillXInvoice objNewInvoice = null ;
			BillXAccount objAccount = null ;
			PolicyEnh objPolicyEnh = null ;
			BillXBillItem objBillXBillItem = null ;
			DbReader objReader = null ;
			DbReader objReaderNext = null ;
			LocalCache objLocalCache = null ;
			StringBuilder sbSQL = null ;
			BillXInvoiceDet objBillXInvoiceDet = null ;
			BillingRule objBillingRule = null ;

			string sSQL = string.Empty ;
			string sBillItemType = string.Empty ;
			string sAdjustmentType = string.Empty ;
			string sLastDate = string.Empty ;
			string sDueDate = string.Empty ;
 
			int iPolicyId = 0 ;
			int iBIRowId = 0 ;
			int iTermNumber = 0 ;
			int iTransactionNumber = 0 ;
			int iLastInvoiceRowid = 0 ;
			int iLastNoticeRowid = 0 ;
            //changed by gagan for mits 18853 : Start
			bool bAudit = false ;
            //changed by gagan for mits 18853 : End
			bool bHoldAmt = false ;
			double dblHoldAmt = 0.0 ;
			double dblTotalCredits = 0.0 ;
			double dblTotalOutstandingInvoiceBalance = 0.0  ;
			double dblCurrentCharges = 0.0 ;
			double dblPolicyTotal = 0.0 ;


            //csingh7 : RMSC changes: Start

            double dBilledPremium = 0;
            double dPreviousBalance = 0;
            string sDateEntered = "";
            int iBillingItemId = 0;
            long lInvId = 0;
            ArrayList collBatchId = new ArrayList();
            //changed by Gagan for mits 17551/17552 : Start
            long lLastInvoiceDate = 0;
            long lDateAdded = 0;
            //changed by Gagan for mits 17551/17552 : End
            string sStatus = "";
            long lEntryBatchNum = 0;
            long lFinalBatchNum = 0;
            bool bFound = false;
            double dCredits = 0;
            //changed by Gagan for mits 17551/17552 : Start
            double dCreditsActual = 0;
            //changed by Gagan for mits 17551/17552 : End

            //Added for Mits 22323:Start
            int iAdjustmentType=0;
            double dWriteOffAdjustment = 0.0;
            //Added for Mits 22323:End

            //csingh7 : RMSC changes: End
            //npadhy RMSC retrofit Starts
            double dCurrentTax = 0;
            double dTotalStateTax = 0;
            ArrayList collTransId = new ArrayList();
            //npadhy RMSC retrofit Ends


            // MITS 19364 : Start
            int iCancelledTaxBillingItemRowId = 0;
            int iMaxTransId = 0;
            bool bCancel = false;
            // MITS 19364 : End

			try
			{
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);

				objLastNotice = ( BillXNotice ) m_objDataModelFactory.GetDataModelObject( "BillXNotice" , false );
				objLastInvoice = ( BillXInvoice ) m_objDataModelFactory.GetDataModelObject( "BillXInvoice" , false );
				objNewInvoice = ( BillXInvoice ) m_objDataModelFactory.GetDataModelObject( "BillXInvoice" , false );
				objAccount = ( BillXAccount ) m_objDataModelFactory.GetDataModelObject( "BillXAccount" , false );
				objPolicyEnh = ( PolicyEnh ) m_objDataModelFactory.GetDataModelObject( "PolicyEnh" , false );
			
				iPolicyId = p_objDiary.AttachRecordid ;
				iBIRowId = p_objDiary.AttSecRecId ;


                // csingh7 : MITS 19351 commented
                //if (p_objDiary.Regarding.Length > 1 && Common.Conversion.IsNumeric(p_objDiary.Regarding.Substring(p_objDiary.Regarding.Length - 2)))
                //{
                //    iTermNumber = Common.Conversion.ConvertStrToInteger(p_objDiary.Regarding.Substring(p_objDiary.Regarding.Length - 2));
                //}
                //else
                //{
                //    sSQL = " SELECT TERM_NUMBER FROM BILL_X_BILL_ITEM WHERE BILLING_ITEM_ROWID=" + iBIRowId;
                //    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //    if (objReader.Read())
                //    {
                //        iTermNumber = Common.Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), m_iClientId);
                //    }
                //    objReader.Close();
                //}
                // csingh7 : MITS 19351 Added
                sSQL = " SELECT TERM_NUMBER FROM BILL_X_BILL_ITEM WHERE BILLING_ITEM_ROWID=" + iBIRowId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    iTermNumber = Common.Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), m_iClientId);
                }
                objReader.Close();

                if (iTermNumber == 0)   // if term number is not available from Bill_X_Bill_Item ,it will be extracted from 'Regarding' field. 
                {
                    if (p_objDiary.Regarding.Length > 1 )
                    {       
                        if (p_objDiary.Regarding.Contains("Term Number") && Common.Conversion.IsNumeric(p_objDiary.Regarding.Substring(p_objDiary.Regarding.Length - 2)))
                        {
                            iTermNumber = Common.Conversion.ConvertStrToInteger(p_objDiary.Regarding.Substring(p_objDiary.Regarding.Length - 2));
                        }
                        else    // Regarding field length is 70 , if term number not found even in 'Regarding' field , throw new exception.
                        {
                            throw new RMAppException(Globalization.GetString("InvoiceManager.CreateInvoice.InvalidTermNumber", m_iClientId));
                        }
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("InvoiceManager.CreateInvoice.InvalidTermNumber", m_iClientId));
                    }
                }
                // csingh7 : MITS 19351 End

                //changed by Gagan for mits 17645 : Start
                sSQL = " SELECT DUE_DATE FROM BILL_X_BILL_ITEM WHERE BILLING_ITEM_ROWID=" + iBIRowId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    sDueDate = Common.Conversion.ConvertObjToStr(objReader.GetValue("DUE_DATE"));
                }
                objReader.Close();
                //changed by Gagan for mits 17645 : End

                objNewInvoice.InvoiceRowid = Common.Utilities.GetNextUID(m_sConnectionString, "BILL_X_INVOICE", m_iClientId);
				p_objBillingMaster.Invoices.Add( objNewInvoice.InvoiceRowid , objNewInvoice );

				objNewInvoice.PolicyId = iPolicyId ;
				objNewInvoice.TermNumber = iTermNumber ;
				//objNewInvoice.BillDate = Common.Conversion.GetDate( DateTime.Now.ToString("d") );//TODO
                objNewInvoice.BillDate = DateTime.Now.ToString("yyyyMMdd");
			
				// Retrieve the billing address entity id

//				objPolicyEnh.PolicyId = iPolicyId ;
                objPolicyEnh.MoveTo(iPolicyId);
                if (!p_objBillingMaster.Policies.ContainsKey(objPolicyEnh.PolicyId))
				    p_objBillingMaster.Policies.Add( objPolicyEnh.PolicyId , objPolicyEnh );

				foreach( PolicyXBillEnh objPolicyXBillEnh in objPolicyEnh.PolicyXBillEnhList )
				{
					if( objPolicyXBillEnh.TermNumber == iTermNumber )
					{
						if( objPolicyXBillEnh.BillToOverride )
							objNewInvoice.HierarchyLevel = objPolicyXBillEnh.NewBillToEid ;
						else
							objNewInvoice.HierarchyLevel = objPolicyXBillEnh.BillToEid ;
						break;
					}
				}
            // MITS 19364 : Start

                foreach (PolicyXTransEnh objPolicyXTransEnhd in objPolicyEnh.PolicyXTransEnhList)
                {
                    if(iMaxTransId < objPolicyXTransEnhd.TransactionId )
                        iMaxTransId = objPolicyXTransEnhd.TransactionId;
                }
                PolicyXTransEnh objPolicyTrans = (PolicyXTransEnh)m_objDataModelFactory.GetDataModelObject("PolicyXTransEnh", false); ;
                objPolicyTrans.MoveTo(iMaxTransId);
                if (objLocalCache.GetShortCode(objPolicyTrans.TransactionType) == "CF" || objLocalCache.GetShortCode(objPolicyTrans.TransactionType) == "CPR")
                {
                    bCancel = true;
                }
            // MITS 19364 : End
				// Retrieve the billing item associated with this
				objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
                objBillXBillItem.MoveTo(iBIRowId);
				//objBillXBillItem.BillingItemRowid = iBIRowId ;
				//objBillXBillItem.Refresh();

				if( objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P" && objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "AP" )  
				{
					bAudit = true ;
					foreach( PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList )
					{
						if( objPolicyXTransEnh.TermNumber == iTermNumber && objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) == "AU" )
						{
							iTransactionNumber = objPolicyXTransEnh.TransactionId ;
							break;
						}
					}
				}

				// This will be set only for audits
				if( bAudit )
					objNewInvoice.PolicyTransId = iTransactionNumber ;

				// Find the last invoice sent
				sSQL = " SELECT MAX(INVOICE_ROWID) FROM BILL_X_INVOICE WHERE POLICY_ID = " + iPolicyId + 
					" AND TERM_NUMBER = " + iTermNumber ;

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					iLastInvoiceRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );
				}
				objReader.Close();

				if( iLastInvoiceRowid != 0 )
				{
					sSQL = "SELECT * FROM BILL_X_INVOICE WHERE INVOICE_ROWID = " + iLastInvoiceRowid ;
					objBillingManager.FillInvoice( objLastInvoice , sSQL );
				}
			
				// Find the last notice sent
				sSQL = " SELECT MAX(NOTICE_ROWID) FROM BILL_X_NOTICE WHERE POLICY_ID = " 
					+ iPolicyId + " AND TERM_NUMBER = " + iTermNumber ;
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					iLastNoticeRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );
				}
				objReader.Close();

				if( iLastNoticeRowid != 0 )
				{
					sSQL = " SELECT * FROM BILL_X_NOTICE WHERE NOTICE_ROWID = " + iLastNoticeRowid ;
					objBillingManager.FillNotice( objLastNotice , sSQL );
				}
			
				// Compare the dates of the notice and invoice to see which to use
				if( objLastNotice.NoticeRowid == 0 || Conversion.ConvertStrToInteger( objLastInvoice.DttmRcdAdded ) > Conversion.ConvertStrToInteger( objLastNotice.DttmRcdAdded ) )
					objNewInvoice.PrevInvoiceRowid = objLastInvoice.InvoiceRowid ;
				else
					objNewInvoice.PrevNoticeRowid = objLastNotice.NoticeRowid ;
			

				// Get the bill account record
				sSQL = " SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + iTermNumber ;
				objBillingManager.FillAccount( objAccount , sSQL );
		    
				objNewInvoice.BillAccountRowid = objAccount.BillAccountRowid ;

                // csingh7 : RMSC changes - Commenting the code

                //// Find the total of the credits
                //sbSQL = new StringBuilder();
                //sbSQL.Append( " SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER=" + iTermNumber );
                //sbSQL.Append( " AND INVOICE_ID=0 AND NOTICE_ID=0" );
                //sbSQL.Append( " AND BILLING_ITEM_TYPE <>" + objLocalCache.GetCodeId( "P", "BILLING_ITEM_TYPES") );
                //sbSQL.Append( " AND BILLING_ITEM_TYPE <>" + objLocalCache.GetCodeId( "D", "BILLING_ITEM_TYPES") );
                //sbSQL.Append( " AND AMOUNT <0" );

                //objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
                //while( objReader.Read() )
                //{
                //    sBillItemType = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_TYPE" ), m_iClientId ) );
                //    if( sBillItemType == "A" )
                //    {
                //        bHoldAmt = false ;
                //        sSQL = "SELECT * FROM BILL_X_ADJSTMNT WHERE BILLING_ITEM_ROWID=" 
                //            + Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
					
                //        objReaderNext = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                //        if( objReaderNext.Read() )
                //        {
                //            sAdjustmentType = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReaderNext.GetValue("ADJUSTMENT_TYPE"), m_iClientId));
                //            if( sAdjustmentType != "OP" && sAdjustmentType != "BI" )
                //            {
                //                if( sAdjustmentType == "DV" )
                //                {
                //                    bHoldAmt = true ;
                //                    dblHoldAmt = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //                }
                //                else
                //                {
                //                    dblTotalCredits += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //                    objBillXInvoiceDet = ( BillXInvoiceDet ) m_objDataModelFactory.GetDataModelObject( "BillXInvoiceDet" , false );
                //                    objBillXInvoiceDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //                    objNewInvoice.BillXInvoiceDetList.Add( objBillXInvoiceDet );
                //                }
                //            }
                //        }
                //        objReaderNext.Close();

                //        if( bHoldAmt )
                //        {
                //            sSQL = " SELECT * FROM BILL_X_INVOICE_DET WHERE BILL_ITEM_ID=" 
                //                + Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
						
                //            objReaderNext = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                //            if( objReaderNext.Read() )
                //            {
                //                dblHoldAmt = 0.0 ;
                //            }
                //            else
                //            {
                //                objBillXInvoiceDet = ( BillXInvoiceDet ) m_objDataModelFactory.GetDataModelObject( "BillXInvoiceDet" , false );
                //                objBillXInvoiceDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //                objNewInvoice.BillXInvoiceDetList.Add( objBillXInvoiceDet );
                //            }
                //            objReaderNext.Close();
                //        }
                //        dblTotalCredits += dblHoldAmt ;
                //        dblHoldAmt = 0.0 ;
                //    }
                //    else
                //    {
                //        dblTotalCredits += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //        objBillXInvoiceDet = ( BillXInvoiceDet ) m_objDataModelFactory.GetDataModelObject( "BillXInvoiceDet" , false );
                //        objBillXInvoiceDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //        objNewInvoice.BillXInvoiceDetList.Add( objBillXInvoiceDet );
                //    }
                //}
                //objReader.Close();

                //if( objNewInvoice.PrevInvoiceRowid != 0 )
                //{
                //    dblTotalOutstandingInvoiceBalance = objLastInvoice.Amount + dblTotalCredits ;
                //    objNewInvoice.PreviousBalance = objLastInvoice.Amount ;
                //}
                //else
                //{
                //    if( objNewInvoice.PrevNoticeRowid != 0 )
                //    {
                //        dblTotalOutstandingInvoiceBalance = objLastNotice.Amount + dblTotalCredits ;
                //        objNewInvoice.PreviousBalance = objLastNotice.Amount ;
                //    }
                //}
			
                //// Find the current charges
                //sbSQL.Remove(0,sbSQL.Length);
                //sbSQL.Append( " SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId );
                //sbSQL.Append( " AND TERM_NUMBER = " + iTermNumber );
                //sbSQL.Append( " AND (AMOUNT > 0 OR BILLING_ITEM_TYPE=" + objLocalCache.GetCodeId("P", "BILLING_ITEM_TYPES") + ")" );
                //sbSQL.Append( " AND INVOICE_ID =0 AND NOTICE_ID=0" );

                //objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
                //while( objReader.Read() )
                //{
                //    sBillItemType = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_TYPE" ), m_iClientId ) );
                //    if( sBillItemType == "A" )
                //    {
                //        bHoldAmt = false ;
                //        sSQL = "SELECT * FROM BILL_X_ADJSTMNT WHERE BILLING_ITEM_ROWID=" 
                //            + Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
					
                //        objReaderNext = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                //        if( objReaderNext.Read() )
                //        {
                //            sAdjustmentType = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReaderNext.GetValue( "ADJUSTMENT_TYPE" ), m_iClientId ) );
                //            if( sAdjustmentType != "UP" && sAdjustmentType != "BI" )
                //            {
                //                dblCurrentCharges += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //                objBillXInvoiceDet = ( BillXInvoiceDet ) m_objDataModelFactory.GetDataModelObject( "BillXInvoiceDet" , false );
                //                objBillXInvoiceDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //                objNewInvoice.BillXInvoiceDetList.Add( objBillXInvoiceDet );							
                //            }
                //        }
                //        objReaderNext.Close();					
                //    }
                //    else
                //    {
                //        dblCurrentCharges += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //        objBillXInvoiceDet = ( BillXInvoiceDet ) m_objDataModelFactory.GetDataModelObject( "BillXInvoiceDet" , false );
                //        objBillXInvoiceDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //        objNewInvoice.BillXInvoiceDetList.Add( objBillXInvoiceDet );
                //    }
                //}
                //objReader.Close();

                //if( objNewInvoice.PrevInvoiceRowid == 0 && objNewInvoice.PrevNoticeRowid == 0 )
                //    dblPolicyTotal = dblCurrentCharges + dblTotalCredits ;
                //else
                //    dblPolicyTotal = dblCurrentCharges + dblTotalOutstandingInvoiceBalance ;

                //Moving RMSC changes to Base
                sbSQL = new StringBuilder();
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append("SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId.ToString() + " AND TERM_NUMBER=" + iTermNumber.ToString());
                sbSQL.Append(" AND BILLING_ITEM_TYPE = " + objLocalCache.GetCodeId("P", "BILLING_ITEM_TYPES"));
                sbSQL.Append(" ORDER BY DATE_ENTERED ASC");

                string sMaxDate = "";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                while (objReader.Read())
                {                   
                    sMaxDate = Common.Conversion.ConvertObjToStr(objReader.GetValue("DATE_ENTERED"));
                }

                objReader.Close();

                //Find the final batch numbers for the latest premium
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append("SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId.ToString() + " AND TERM_NUMBER=" + iTermNumber.ToString());
                sbSQL.Append(" ORDER BY BILLING_ITEM_ROWID");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());

                while (objReader.Read())
                {
                    sBillItemType = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_TYPE"), m_iClientId));

                    sDateEntered = Common.Conversion.ConvertObjToStr(objReader.GetValue("DATE_ENTERED"));
                    iBillingItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId);


                    sStatus = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("STATUS"), m_iClientId));

                    lEntryBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("ENTRY_BATCH_NUM"), m_iClientId);

                    lFinalBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("FINAL_BATCH_NUM"), m_iClientId);

                    lInvId = Common.Conversion.ConvertObjToInt64(objReader.GetValue("INVOICE_ID"), m_iClientId);
				//npadhy RMSC retrofit Starts
                    if ((sBillItemType == "P" && sDateEntered == sMaxDate) && (lInvId == 0 || iBillingItemId == iBIRowId))
				//npadhy RMSC retrofit Ends
                    {
                        if (sStatus == "RC")
                        {
                            bFound = false;
                            foreach (object v in collBatchId)
                            {
                                if (lFinalBatchNum == Convert.ToInt64(v))
                                {
                                    bFound = true;
                                }
                            }

                            if (bFound == false)
                            {
                                collBatchId.Add(lFinalBatchNum);
                            }
                        }
                    }
                    //npadhy RMSC retrofit Starts
                    else if ((sBillItemType == "TAX" && sDateEntered == sMaxDate) && (lInvId == 0 || iBillingItemId == iBIRowId + 1))
                    {
                        if (sStatus == "RC")
                        {
                            bFound = false;
                            foreach (object v in collBatchId)
                            {
                                if (lFinalBatchNum == Convert.ToInt64(v))
                                {
                                    bFound = true;
                                }
                            }
                            if (bFound == false)
                            {
                                collBatchId.Add(lFinalBatchNum);
                            }
                        }
                    }
                    //npadhy RMSC retrofit Ends
                }

                objReader.Close();
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append("SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId.ToString() + " AND TERM_NUMBER=" + iTermNumber.ToString());
                sbSQL.Append(" ORDER BY BILLING_ITEM_ROWID");

                dBilledPremium = 0;
                dPreviousBalance = 0;
				
				//changed by Gagan for mits 17551/17552 : Start
                //Date of generation for last Invoice
                string sLastInvoiceDate = objLastInvoice.DttmRcdAdded;
                lLastInvoiceDate = Common.Conversion.ConvertStrToLong(sLastInvoiceDate);
                //changed by Gagan for mits 17551/17552 : End
				
                //npadhy RMSC retrofit Starts
                dTotalStateTax = 0;
                //npadhy RMSC retrofit Ends

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());

                while (objReader.Read())
                {
                    sBillItemType = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_TYPE"), m_iClientId));

                    sDateEntered = Common.Conversion.ConvertObjToStr(objReader.GetValue("DATE_ENTERED"));

                    iBillingItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId);

                    sStatus = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("STATUS"), m_iClientId));

                    lEntryBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("ENTRY_BATCH_NUM"), m_iClientId);

                    lFinalBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("FINAL_BATCH_NUM"), m_iClientId);

                    lInvId = Common.Conversion.ConvertObjToInt64(objReader.GetValue("INVOICE_ID"), m_iClientId);

                    //changed by Gagan for mits 17551/17552 : Start
                    lDateAdded = Common.Conversion.ConvertStrToLong(Common.Conversion.ConvertObjToStr(objReader.GetValue("DTTM_RCD_ADDED")));
                    //changed by Gagan for mits 17551/17552 : End
                    
                    // csingh7 : TO-DO
                    ////Separate case for audit invoices --
                    ////Audit invoices should only include those premium or tax items which are not reconciled,
                    ////and are generated as a result of audit on the policy
                    if (bAudit)
                    {
                        objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                        objBillXBillItem.MoveTo(iBillingItemId);

                        if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "P" && objLocalCache.GetShortCode(objBillXBillItem.BillXPremItem.PremiumItemType) == "AP" && sStatus != "RC" && lInvId == 0)
                        {
                            dBilledPremium = dBilledPremium + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            collBatchId.Add(objBillXBillItem.BillXPremItem.PolicyTranId);
                        }
                        else if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "TAX" && sStatus != "RC" && lInvId == 0)
                        {
                            bFound = false;
                            foreach (object v in collTransId)
                            {
                                if (objBillXBillItem.BillXPremItem.PolicyTranId == Convert.ToInt64(v))
                                {
                                    bFound = true;
                                }
                            }

                            if (bFound == true)
                            {
                                dTotalStateTax = dTotalStateTax + Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            }
                        }
                    }
            // MITS 19364 : Start

                    else if (bCancel)
                    {
                        //int ibillitemrowid = 302;
                        //objBillXBillItem.MoveTo(ibillitemrowid);
                        //objBillXBillItem.MoveTo(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId));

                        iBillingItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId);
                        objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                        objBillXBillItem.MoveTo(iBillingItemId);

                        if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "P" && objLocalCache.GetShortCode(objBillXBillItem.BillXPremItem.PremiumItemType) == "CP")
                        {
                            dBilledPremium = dBilledPremium + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            iCancelledTaxBillingItemRowId = iBillingItemId + 1;  // for calculating cancelled premium tax                         
                        }
                         else if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "TAX" && sStatus != "RC" && lEntryBatchNum == 0 && lInvId == 0)
                            {
                                dTotalStateTax = dTotalStateTax + Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            }
                            //Add by kuladeep for Previous Balance:End MITS 24743

                       // else if ((sBillItemType == "P" || sBillItemType == "TAX") && sStatus != "RC")
                        else if ((objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "P" || objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "TAX") && sStatus != "RC")
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        else if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "A" && lEntryBatchNum != 0 && sStatus != "RC")
                        {
                            bFound = false;
                            foreach (object v in collBatchId)
                            {
                                if (lFinalBatchNum == Convert.ToInt64(v))
                                {
                                    bFound = true;
                                }
                            }

                            if (bFound == false)
                            {
                                dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            }
                        }
                        //Any unreconciled adjustment that has been manually entered in the system
                        //by the user needs to be reflected as previous balance        
                        else if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "A" && lEntryBatchNum == 0 && sStatus != "RC")
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }

                    }
            // MITS 19364 : End
                    else  //If not Audit and Cancel
                    {
                        //Latest Premium Items with Date Entered among the latest generated premiums
                        // and (whose invoice hasnt been generated or have billing item rowid same as
                        //billing item for which this installment diary was generated)       
                        if (sBillItemType == "P" && sDateEntered == sMaxDate)
                        {
                            dBilledPremium = dBilledPremium + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }

                        //Latest Tax Items with Date Entered among the latest generated taxes
                        //and (whose invoice hasnt been generated or have billing item rowid same as
                        //billing item for which this installment diary was generated)
                        //changed by gagan for mits 18853 : Start
                        else if (sBillItemType == "TAX" && sDateEntered == sMaxDate)
                        {
                            dTotalStateTax = dTotalStateTax + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        //changed by gagan for mits 18853 : End

                        //changed by Gagan for mits 17551/17552 : Start

                        // any premium item(s) whether reconciled or not and generated after last invoice date
                        // is added to current billed premium
                        else if (sBillItemType == "P" && lDateAdded > lLastInvoiceDate && sStatus!="RC")
                        {
                            dBilledPremium = dBilledPremium + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }

                        // any premium item(s) whether reconciled or not and generated after last invoice date
                        // is added to current billed premium
                        //changed by gagan for mits 18853 : Start
                        else if (sBillItemType == "TAX" && lDateAdded > lLastInvoiceDate && sStatus!="RC")
                        {
                            dTotalStateTax = dTotalStateTax + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        //changed by gagan for mits 18853 : End

                         //changed by Gagan for mits 17551/17552 : End   

                         //Any premium item that hasn't been reconciled yet comes as a previous balance
                        //changed by gagan for mits 18853 : Start
                        else if ((sBillItemType == "P" || sBillItemType == "TAX") && sStatus != "RC")
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        //changed by gagan for mits 18853 : End


                        //do not include adjustments in Previous Balance generated as a result of
                        //reconciliation of premium items shown in billed Premium. Include those which
                        //are generated as a result of previous reconciliation
                        else if (sBillItemType == "A" && lEntryBatchNum != 0 && sStatus != "RC")
                        {
                            bFound = false;
                            foreach (object v in collBatchId)
                            {
            // MITS 19364 : Start

                                //if (lEntryBatchNum == Convert.ToInt64(v))
                                if (lFinalBatchNum == Convert.ToInt64(v))                                
                                {                                    
            // MITS 19364 : End

                                    bFound = true;
                                }
                            }

                            if (bFound == false)
                            {
                                dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            }
                        }


                        //Any unreconciled adjustment that has been manually entered in the system
                        //by the user needs to be reflected as previous balance        
                        else if (sBillItemType == "A" && lEntryBatchNum == 0 && sStatus != "RC")
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }

                        //changed by Gagan for mits 17551/17552 : Start
                        //Calculating Credits
                        //Credits include all reconciled items which are in the latest batch containing current premium or tax items
                        //Adjustments - if entered manually
                        //Adjustments - generated as a result of previous reconciliation
                        //Reciepts
                        //Disbursements
                        //'Note: System generated Adjustments of type Balance Item (where entry batch num = final batch num) should not
                        //be included in the credits.


                        //else if (sStatus == "RC" && (sBillItemType != "P" || (sBillItemType == "P" && sDateEntered != sMaxDate)) && lEntryBatchNum != lFinalBatchNum)
                        //{

                        //    bFound = false;
                        //    foreach (object v in collBatchId)
                        //    {
                        //        if (lFinalBatchNum == Convert.ToInt64(v))
                        //        {
                        //            bFound = true;
                        //        }
                        //    }

                        //    if (bFound == true)
                        //    {
                        //        dCredits = dCredits + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        //    }
                        //}
                    }

                    objBillXInvoiceDet = (BillXInvoiceDet)m_objDataModelFactory.GetDataModelObject("BillXInvoiceDet", false);
                    objBillXInvoiceDet.BillItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId);
                    objNewInvoice.BillXInvoiceDetList.Add(objBillXInvoiceDet);                   

                }
                objReader.Close();






                //Finding credits
                //Credits are all reconciled receipts added in the system since last invoice was generated
                dCreditsActual = 0;
                dWriteOffAdjustment = 0.0; //Mits 22323
                lDateAdded = 0;
                sbSQL = new StringBuilder();
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append("SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId.ToString() + " AND TERM_NUMBER=" + iTermNumber.ToString());
                sbSQL.Append(" AND ( BILLING_ITEM_TYPE = " + objLocalCache.GetCodeId("R", "BILLING_ITEM_TYPES"));
                sbSQL.Append(" OR BILLING_ITEM_TYPE = " + objLocalCache.GetCodeId("D", "BILLING_ITEM_TYPES")); //Mits 22328
                sbSQL.Append(" OR BILLING_ITEM_TYPE = " + objLocalCache.GetCodeId("A", "BILLING_ITEM_TYPES" )+")"); //Mits 22327
                //sbSQL.Append(" AND DTTM_RCD_ADDED = " > sLastInvoiceDate);                
                //sbSQL.Append(" AND BILLING_ITEM_TYPE = " + objLocalCache.GetCodeId("R", "STATUS"));                                                

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                while (objReader.Read())
                {
                    sBillItemType = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_TYPE"), m_iClientId)); //Mits 22327
                    sStatus = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("STATUS"), m_iClientId));
                    lDateAdded = Common.Conversion.ConvertStrToLong(Common.Conversion.ConvertObjToStr(objReader.GetValue("DTTM_RCD_ADDED")));
                    lEntryBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("ENTRY_BATCH_NUM"), m_iClientId); //Mits 22327
                    lFinalBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("FINAL_BATCH_NUM"), m_iClientId); //Mits 22327
                    iBillingItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId);

                    if (sStatus == "RC" && lDateAdded > lLastInvoiceDate)
                    {
                        //Added If clause for Mits 22327:Add adjustment and reconcile  total due premium is wrong
                        //Past due amount is wrong when Adjusted with balance
                        if(sBillItemType == "A")
                        {
                            if (lEntryBatchNum != lFinalBatchNum) //For the new Adjustment added by us(not generated as a result of reconcilation)
                            {
                                dCreditsActual = dCreditsActual + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);//Taking the payment coming in(Adjustment) as Credit.
                            }
                            else if (lEntryBatchNum == lFinalBatchNum) //Added Else if for Mits 22323:Total due premium is wrong in invoice when recoinciled write offf
                            {
                                sbSQL.Length = 0;
                                sbSQL.Append("SELECT ADJUSTMENT_TYPE FROM BILL_X_ADJSTMNT WHERE BILLING_ITEM_ROWID = ");
                                sbSQL.Append(iBillingItemId.ToString());

                                using(objReaderNext = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                                {
                                    if (objReaderNext.Read())
                                    {
                                        iAdjustmentType = Common.Conversion.ConvertObjToInt(objReaderNext.GetValue("ADJUSTMENT_TYPE"), m_iClientId);

                                        if (objLocalCache == null)
                                        {
                                            objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                                        }

                                        if (iAdjustmentType == objLocalCache.GetCodeId("WO", "ADJUSTMENT_TYPES"))
                                        {
                                            dWriteOffAdjustment += Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                                        }
                                    }
                                }                               
                            }//end else if
                        }
                        //Added If clause for Mits 22327:Add adjustment and reconcile  total due premium is wrong
                        else if (sBillItemType == "D") //Added else If clause for Mits 22328:Past due amount is wrong after adding disbursement
                        {
                            if (lEntryBatchNum != lFinalBatchNum) 
                            {
                                dCreditsActual = dCreditsActual + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);//Taking the payment coming in(disbursement) as Credit.
                            }
                        }
                        else
                        {
                            dCreditsActual = dCreditsActual + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                    }

                }
                objReader.Close();

                //changed by Gagan for mits 17551/17552 : End

				// Find all the billing items that are on this invoice
				foreach( BillXInvoiceDet objBillXInvoiceDetTemp in objNewInvoice.BillXInvoiceDetList )
				{
					objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
					sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE BILLING_ITEM_ROWID=" + objBillXInvoiceDetTemp.BillItemId ;
					objBillingManager.FillBillingItem( objBillXBillItem , sSQL );

					if( objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P" )
					{
                        //Commneted by gagan for mits  17645
						//if( sLastDate == "" || Common.Conversion.ConvertStrToInteger( objBillXBillItem.DateEntered ) > Common.Conversion.ConvertStrToInteger( sLastDate ) )
						//{
                        //changed by Gagan for mits 17645 : Start
							//sDueDate = objBillXBillItem.DueDate ;
                        //changed by Gagan for mits 17645 : End
							sLastDate = objBillXBillItem.DueDate ;
						//}
					}
                    //Dont set invoice id of Audit premium items while printing premium invoice
                    if (!bAudit && (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "P") && (objLocalCache.GetShortCode(objBillXBillItem.BillXPremItem.PremiumItemType) == "AP") && sStatus != "RC")
                    {
                        collTransId.Add(objBillXBillItem.BillXPremItem.PolicyTranId);
                    }
                    else if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "TAX" && sStatus != "RC")
                    {
                        //To find the tax items which are generated because of Audit on the Policy
                        bFound = false;
                        foreach (object v in collTransId)
                        {
                            if (objBillXBillItem.BillXPremItem.PolicyTranId == Convert.ToInt64(v))
                            {
                                bFound = true;
                            }
                        }
                        if (!(bAudit == false && bFound == true))
                        {
                            if (objBillXBillItem.InvoiceId == 0)  //gbhatnagar for MITS 20768
                                objBillXBillItem.InvoiceId = objNewInvoice.InvoiceRowid;
                        }
                    }
                    else if (objBillXBillItem.InvoiceId == 0)  //gbhatnagar for MITS 20768
                    {
                        objBillXBillItem.InvoiceId = objNewInvoice.InvoiceRowid;
                    }
                     //npadhy RMSC retrofit Starts
                    //if(objBillXBillItem.InvoiceId == 0)         //csingh7 : Wrong Invoice number getting generated - fixed. 
                    //    objBillXBillItem.InvoiceId = objNewInvoice.InvoiceRowid ;
                    //npadhy RMSC retrofit Ends
                    //if (!p_objBillingMaster.BillingItems.Contains(objBillXInvoiceDetTemp.BillItemId))
					p_objBillingMaster.BillingItems.Add( objBillXInvoiceDetTemp.BillItemId , objBillXBillItem );
				}
	
                //ArrayList objInvoiceList = new ArrayList(); //csingh7 for MITS 12503
                //BillXInvoice objInvoice;
                //BillXBillItem objBillItem;

                //foreach( Int32 iKey in p_objBillingMaster.Invoices.Keys )
                //{
                //    objInvoice = (BillXInvoice)p_objBillingMaster.Invoices[iKey] ;
                //    objInvoiceList.Add(objInvoice.InvoiceRowid);
                //}
                //int iCount = 0 ;
                //foreach (Int32 iKey1 in p_objBillingMaster.BillingItems.Keys)
                //{
                //    objBillItem = (BillXBillItem)p_objBillingMaster.BillingItems[iKey1];
                //    if (iCount < objInvoiceList.Count)
                //    {
                //        objBillItem.InvoiceId = (int)objInvoiceList[iCount];
                //    }
                //    iCount++;
                //}
				if( sDueDate == "" )
				{
					objBillingRule = new BillingRule();
					sSQL = " SELECT * FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID =" + objAccount.BillingRuleRowid ;
					objBillingManager.FillBillingRule( objBillingRule, sSQL );
					//sDueDate = Conversion.GetDate( Conversion.ToDate( Conversion.GetDBDateFormat( objNewInvoice.BillDate , "d" ) ).AddDays( objBillingRule.DueDays ).ToString("d" ));
                    sDueDate = Conversion.GetDate(Conversion.ToDate(objNewInvoice.BillDate).AddDays(objBillingRule.DueDays).ToString("d"));                    
				}
                //changed by Gagan for mits 17551/17552 : Start
            // MITS 19364 : Start

        //        if(!bCancel )  //MITS 19364
          //          dPreviousBalance = objLastInvoice.Amount;
                //changed by Gagan for mits 17551/17552 : End
            // MITS 19364 : End


				objNewInvoice.DueDate = sDueDate ;
				//objNewInvoice.Amount = dblPolicyTotal ;
				//objNewInvoice.Payments = dblTotalCredits ;
				//objNewInvoice.OutstandingBalance = dblTotalOutstandingInvoiceBalance ;
				//objNewInvoice.CurrentCharges = dblCurrentCharges ;
                objNewInvoice.PreviousBalance = objLastInvoice.Amount;
                //changed by Gagan for mits 17551/17552 : Start
                objNewInvoice.Payments = dCreditsActual;
                objNewInvoice.CurrentCharges = dBilledPremium;
                objNewInvoice.CurrentTaxCharges = dTotalStateTax;
                //objNewInvoice.OutstandingBalance = dPreviousBalance + dCreditsActual + dWriteOffAdjustment;//Mits 22323:Added dWriteOffAdjustment
                objNewInvoice.OutstandingBalance = dPreviousBalance;
                //changed by Gagan for mits 17551/17552 : End
                //objNewInvoice.Amount = Math.Round(dPreviousBalance, 2) + Math.Round(dBilledPremium, 2) + Math.Round(dCredits, 2);
                //changed by gagan for mits 18853 : Start
            // MITS 19364 : Start

                if (bCancel)    //MITS 19364
                {
                    objNewInvoice.Amount = Math.Round(dPreviousBalance, 2, MidpointRounding.AwayFromZero) + Math.Round(dBilledPremium, 2, MidpointRounding.AwayFromZero) + Math.Round(dTotalStateTax, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    objNewInvoice.Amount = Math.Round(dPreviousBalance, 2, MidpointRounding.AwayFromZero) + Math.Round(dBilledPremium, 2, MidpointRounding.AwayFromZero) + Math.Round(dTotalStateTax, 2, MidpointRounding.AwayFromZero) + Math.Round(dWriteOffAdjustment, 2, MidpointRounding.AwayFromZero);//Mits 22323:Added dWriteOffAdjustment
                    //objNewInvoice.Amount = Math.Round(dPreviousBalance, 2) + Math.Round(dBilledPremium, 2) + Math.Round(dCreditsActual, 2) + Math.Round(dTotalStateTax, 2) + Math.Round(dWriteOffAdjustment, 2);//Mits 22323:Added dWriteOffAdjustment

                }
            // MITS 19364 : End

                //changed by gagan for mits 18853 : End
                objNewInvoice.PolicyNumber = objAccount.PolicyNumber ;

                //csingh7 : RMSC changes: End
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("InvoiceManager.CreateInvoice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objLastNotice != null )
				{
					objLastInvoice.Dispose();
					objLastInvoice = null ;
				}
				if( objLastInvoice != null )
				{
					objLastInvoice.Dispose();
					objLastInvoice = null ;
				}
				if( objNewInvoice != null )
				{
					objNewInvoice.Dispose();
					objNewInvoice = null ;
				}
				if( objAccount != null )
				{
					objAccount.Dispose();
					objAccount = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
				if( objReaderNext != null )
				{
					objReaderNext.Close();
					objReaderNext.Dispose();
					objReaderNext = null ;
				}
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
				if( objBillXInvoiceDet != null )
				{
					objBillXInvoiceDet.Dispose();
					objBillXInvoiceDet = null ;
				}
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
				sbSQL = null ;
				objBillingRule = null ;
			}
		}

		#endregion 

		


	}
}
