using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Text;
using System.Xml;
using Riskmaster.Application.PrintChecks;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.ZipUtil;
using Riskmaster.Security;

namespace Riskmaster.Application.CheckStockSetup
{
	///************************************************************** 
	///* $File				: CheckStockSetup.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 30-Dec-2004
	///* $Author			: Neelima Dabral
	///***************************************************************	
	
	/// <summary>	
	///	This class contains methods to configure the way check look and the information on them.	
	/// </summary>
	public class CheckStockSetup:IDisposable
	{
		#region Member Variables
		
		/// <summary>
		/// DSN
		/// </summary>
		private string m_sDatabaseName = "";

		/// <summary>
		/// User Id
		/// </summary>
		private string m_sUserName = "";

		/// <summary>
		/// Password of the user
		/// </summary>
		private string m_sPassword = "";
		private bool m_Clone = false;
		private bool m_New = false;

		/// <summary>
		/// DataModelFactory object
		/// </summary>
		DataModelFactory  m_objDMF = null;
        /// <summary>
        /// ClientID for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;
		#endregion

		#region Constructor

		/// Name			: CheckStockSetup
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sDatabaseName">DSN</param>
		/// <param name="p_sUserName">Application user</param>
		/// <param name="p_sPassword">Password of application user</param>
		public CheckStockSetup(string p_sDatabaseName,string p_sUserName,string p_sPassword, int p_iClientId)
		{
			m_sDatabaseName = p_sDatabaseName;
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
            m_iClientId = p_iClientId;
		}	

		#endregion

        #region Destructor
        ~CheckStockSetup()
        {
            Dispose();
        }
        #endregion

        #region Methods

        /// Name			: GetCheckStockDetails
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will return the XML containing the setup related 
		///		details for a selected check stock. 		
		/// </summary>
		/// <param name="p_iStockId">Stock Id</param>		
		/// <returns>XML containing the setup related details for specified Stock Id</returns>		
		public XmlDocument GetCheckStockDetails (int p_iStockId)    
		{
			CheckStock        objCheckStock = null;
			XmlElement        objXMLElem	= null;
			XmlDocument       objOutXML		= null;	
			string			  sUnitValue	= "";
			CheckStockFormsList objCheckStockFormsList=null;
			XmlElement objXmlChildElement=null;
			XmlElement objXml2LevelChild=null;
			string sFormSource=string.Empty;
			// Added by Ratheen 4th August 2005
			bool	bSoftForm = false;
			//XmlDocument		  objReturnXML	= null;
			try
			{	
				InitializeDataModel();
				objCheckStock = (CheckStock)m_objDMF.GetDataModelObject("CheckStock",false);				
				try
				{
					objCheckStock.MoveTo(p_iStockId);
				}
				catch (RecordNotFoundException p_objException)
				{
					throw new InvalidValueException(Globalization.GetString
						("CheckStockSetup.GetCheckStockDetails.StockIdNotFound", m_iClientId),p_objException);
				}
				
				objOutXML= new XmlDocument();

				objXMLElem=objOutXML.CreateElement("CheckStock");
				objOutXML.AppendChild(objXMLElem);			
				objXMLElem = null;

				objXMLElem = objOutXML.CreateElement("StockId");
				objXMLElem.InnerText = objCheckStock.StockId.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	

				objXMLElem = objOutXML.CreateElement("AccountId");
				objXMLElem.InnerText = objCheckStock.AccountId.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				#region Adding the information in the XML displayed in Basic Options tab

				objXMLElem = objOutXML.CreateElement("StockName");
				objXMLElem.InnerText = objCheckStock.StockName;
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("FontName");
				objXMLElem.InnerText = objCheckStock.FontName;
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("FontSize");
				objXMLElem.InnerText = objCheckStock.FontSize.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
				
				objXMLElem = objOutXML.CreateElement("MICRFlag");
				objXMLElem.InnerText = objCheckStock.ChkMicrFlag.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MICRSequence");
				objXMLElem.InnerText = objCheckStock.ChkMicrSequence;
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MICRType");
				objXMLElem.InnerText = objCheckStock.MicrType.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MICRFont");
				objXMLElem.InnerText = objCheckStock.MicrFont;
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MICRFontSize");
				objXMLElem.InnerText = objCheckStock.MicrFontSize.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				#endregion

				#region Adding the information in the XML displayed in Check Image Tab

                //For Visio 2007 support
                objXMLElem = objOutXML.CreateElement("NewVisioVersion");
                objXMLElem.InnerText = objCheckStock.NewVisioVersion.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //For Visio 2007 support

				objXMLElem = objOutXML.CreateElement("RXLaserFlag");
				objXMLElem.InnerText = objCheckStock.ChkRxlaserFlag.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("SoftForm");
				objXMLElem.InnerText = objCheckStock.ChkSoftForm.ToString();				
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
				// Addded by Ratheen on 4th August 2005
				bSoftForm = objCheckStock.ChkSoftForm;
				
				objXMLElem = objOutXML.CreateElement("RXLaserForm");
				objXMLElem.InnerText = objCheckStock.ChkRxlaserForm.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("RXVoidForm");
				objXMLElem.InnerText = objCheckStock.RxVoidForm.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("RXSpecialForm");
				objXMLElem.InnerText = objCheckStock.RxSpecialForm.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
			
				objXMLElem = objOutXML.CreateElement("SpecialFlag");
				objXMLElem.InnerText = objCheckStock.SpecialFlag.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("SpecialAmount");
				objXMLElem.InnerText = objCheckStock.SpecialAmount.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				#endregion

				#region Adding the information in the XML displayed in Alignment Option Tab
				
				objXMLElem = objOutXML.CreateElement("DateX");
				objXMLElem.InnerText = objCheckStock.ChkDateX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("DateY");
				objXMLElem.InnerText = objCheckStock.ChkDateY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //asingh263 mits 32712 starts
                objXMLElem = objOutXML.CreateElement("EventDateX");
                objXMLElem.InnerText = objCheckStock.ChkEventDateX.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("EventDateY");
                objXMLElem.InnerText = objCheckStock.ChkEventDateY.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("ClaimDateX");
                objXMLElem.InnerText = objCheckStock.ChkClaimDateX.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("ClaimDateY");
                objXMLElem.InnerText = objCheckStock.ChkClaimDateY.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("InsuredNameX");
                objXMLElem.InnerText = objCheckStock.ChkInsurerX.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("InsuredNameY");
                objXMLElem.InnerText = objCheckStock.ChkInsurerY.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PolicyNumberX");
                objXMLElem.InnerText = objCheckStock.ChkPolicyX.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PolicyNumberY");
                objXMLElem.InnerText = objCheckStock.ChkPolicyY.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PayeeAddressX");
                objXMLElem.InnerText = objCheckStock.PayeeAddrX.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PayeeAddressY");
                objXMLElem.InnerText = objCheckStock.PayeeAddrY.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("AgentAddressX");
                objXMLElem.InnerText = objCheckStock.AgentAddrX.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("AgentAddressY");
                objXMLElem.InnerText = objCheckStock.AgentAddrY.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PayeeAddOnCheckX");
                objXMLElem.InnerText = objCheckStock.PayeeAddrOnCheckX.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PayeeAddOnCheckY");
                objXMLElem.InnerText = objCheckStock.PayeeAddrOnCheckY.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //asingh263 mits 32712 ends

				objXMLElem = objOutXML.CreateElement("CheckNumberX");
				objXMLElem.InnerText = objCheckStock.ChkNumberX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckNumberY");
				objXMLElem.InnerText = objCheckStock.ChkNumberY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckAmountX");
				objXMLElem.InnerText = objCheckStock.ChkAmountX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckAmountY");
				objXMLElem.InnerText = objCheckStock.ChkAmountY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckAmountTextX");
				objXMLElem.InnerText = objCheckStock.ChkAmountTextX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckAmountTextY");
				objXMLElem.InnerText = objCheckStock.ChkAmountTextY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("ClaimNumberOffsetX");
				objXMLElem.InnerText = objCheckStock.ClmNumOffsetX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("ClaimNumberOffsetY");
				objXMLElem.InnerText = objCheckStock.ClmNumOffsetY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MemoX");
				objXMLElem.InnerText = objCheckStock.ChkMemoX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MemoY");
				objXMLElem.InnerText = objCheckStock.ChkMemoY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckDtlX");
				objXMLElem.InnerText = objCheckStock.ChkDetailX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckDtlY");
				objXMLElem.InnerText = objCheckStock.ChkDetailY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckPayerX");
				objXMLElem.InnerText = objCheckStock.ChkPayerX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckPayerY");
				objXMLElem.InnerText = objCheckStock.ChkPayerY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckPayeeX");
				objXMLElem.InnerText = objCheckStock.PayeeX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckPayeeY");
				objXMLElem.InnerText = objCheckStock.PayeeY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckOffsetX");
				objXMLElem.InnerText = objCheckStock.ChkOffsetX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckOffsetY");
				objXMLElem.InnerText = objCheckStock.ChkOffsetY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckMICRX");
				objXMLElem.InnerText = objCheckStock.ChkMicrX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckMICRY");
				objXMLElem.InnerText = objCheckStock.ChkMicrY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("SecondOffsetX");
				objXMLElem.InnerText = objCheckStock.SecdOffsetX.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("SecondOffsetY");
				objXMLElem.InnerText = objCheckStock.SecdOffsetY.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                //dvatsa - cloud
                //sUnitValue = RMConfigurationManager.GetDictionarySectionSettings("CheckStock")["CheckStockUnits"].ToString();
                sUnitValue = RMConfigurationManager.GetDictionarySectionSettings("CheckStock", m_objDMF.Context.DbConn.ConnectionString, m_iClientId)["CheckStockUnits"].ToString();
				if (sUnitValue == null)
					sUnitValue = "";
				objXMLElem = objOutXML.CreateElement("Unit");
				if (!(sUnitValue.ToLower() == "false"))
					objXMLElem.InnerText = "Inches";
				else
					objXMLElem.InnerText = "Centimeters";
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				#endregion

				#region Adding the information in the XML displayed in Advanced Options Tab
				
				objXMLElem = objOutXML.CreateElement("DollarSign");
				objXMLElem.InnerText = objCheckStock.DolSignFlag.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("PrintAddress");
				objXMLElem.InnerText = (objCheckStock.PrintAddrFlag).ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
				
				objXMLElem = objOutXML.CreateElement("PrintTaxIDOnStub");
				objXMLElem.InnerText = objCheckStock.PrntTaxidOnStub.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //asingh263 mits 32712 starts
                objXMLElem = objOutXML.CreateElement("PrintAgentNameAddressOnStub");
                objXMLElem.InnerText = objCheckStock.PrntAgentOnStub.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PrintDateofClaim");
                objXMLElem.InnerText = objCheckStock.PrntClaimDateOnCheck.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PrintClaimDateOnStub");
                objXMLElem.InnerText = objCheckStock.PrntClaimDateOnStub.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PrintFullPolicyNumberonStub");
                objXMLElem.InnerText = objCheckStock.PrntPolicyOnStub.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PrintFullPolicyNumber");
                objXMLElem.InnerText = objCheckStock.PrntPolicyOnCheck.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PrintInsuredNameOnCheck");
                objXMLElem.InnerText = objCheckStock.PrntInsurerOnCheck.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PrintDateofEvent");
                objXMLElem.InnerText = objCheckStock.PrntEventDateOnCheck.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PrintPayeeAddressOnStub");
                objXMLElem.InnerText = objCheckStock.PrintAddrOnStubFlag.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                //asingh263 mits 32712 ends

                //MITS 26614 mcapps2 start
                objXMLElem = objOutXML.CreateElement("FormatDateOfCheck");
                objXMLElem.InnerText = objCheckStock.FormatDateOfCheck.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //MITS 26614 mcapps2 end

				objXMLElem = objOutXML.CreateElement("PrintAdjNamePhoneOnStub");
				objXMLElem.InnerText = objCheckStock.PrntAdjOnStub.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("PreNumStock");
				objXMLElem.InnerText = objCheckStock.PreNumStock.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("PrintMemo");				
				objXMLElem.InnerText = objCheckStock.PrintMemoFlag.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //asharma326 jira 9288 starts
                objXMLElem = objOutXML.CreateElement("PayeeAddressFont");
                objXMLElem.InnerText = objCheckStock.PayeeAddressFont.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //asharma326 jira 9288 Ends
				objXMLElem = objOutXML.CreateElement("PrntEventOnStub");
				objXMLElem.InnerText = objCheckStock.PrntEventOnStub.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("PrintInsurer");
				objXMLElem.InnerText = objCheckStock.PrintInsurer.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("PrintClaimNumberOnCheck");
				objXMLElem.InnerText = objCheckStock.PrntClmNumCheck.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("PrintCtlNumberOnStub");
				objXMLElem.InnerText = objCheckStock.PrntCtlOnStub.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
				
				objXMLElem = objOutXML.CreateElement("PrintCheckDateOnStub");
				objXMLElem.InnerText = (objCheckStock.PrntChkDtOnStub).ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("PayerLevel");
				objXMLElem.InnerText = objCheckStock.PayerLevel.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("CheckDateFormat"); //MITS 33324 mcapps2
                objXMLElem.InnerText = objCheckStock.CheckDateFormat.ToString(); //MITS 33324 mcapps2
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem); //MITS 33324 mcapps2

				objXMLElem = objOutXML.CreateElement("MemoFontName");
				objXMLElem.InnerText = objCheckStock.MemoFontName;
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MemoFontSize");
				objXMLElem.InnerText = objCheckStock.MemoFontSize.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MemoRows");
				objXMLElem.InnerText = objCheckStock.MemoRows.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("MemoRowLength");
				objXMLElem.InnerText = objCheckStock.MemoRowLength.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
                //Ashish Ahuja - Pay To The Order Config starts
                objXMLElem = objOutXML.CreateElement("PayToOrderFontName");
                objXMLElem.InnerText = objCheckStock.PayToOrderFontName;
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PayToOrderFontSize");
                objXMLElem.InnerText = objCheckStock.PayToOrderFontSize.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PayToOrderRows");
                objXMLElem.InnerText = objCheckStock.PayToOrderRows.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = objOutXML.CreateElement("PayToOrderRowLength");
                objXMLElem.InnerText = objCheckStock.PayToOrderRowLength.ToString();
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                //Ashish Ahuja - Pay To The Order Config ends

				objXMLElem = objOutXML.CreateElement("AddressFlag");
				objXMLElem.InnerText = objCheckStock.AddressFlag.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

				objXMLElem = objOutXML.CreateElement("CheckLayout");
				objXMLElem.InnerText = objCheckStock.ChkLayout.ToString();
				objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);			
				
				if ((bSoftForm)&&(!m_Clone))
				{
					//The code to fetch the EMFs from CHECK_STOCK_FORMS table for a stock id 
					//was not written in Datamodel.
					//Done By: Nikhil Garg		Dated: 11-Apr-2005
					objCheckStockFormsList=objCheckStock.CheckStockFormsList;
					objXMLElem = objOutXML.CreateElement("CheckStockForms");
					foreach(CheckStockForms objCheckStockForms in objCheckStockFormsList)
					{
						objXmlChildElement=objOutXML.CreateElement("CheckStockForm");						

						objXml2LevelChild=objOutXML.CreateElement("StockId");
						objXml2LevelChild.InnerText = objCheckStockForms.StockId.ToString();
						objXmlChildElement.AppendChild(objXml2LevelChild);

						objXml2LevelChild=objOutXML.CreateElement("ImageIdx");
						objXml2LevelChild.InnerText = objCheckStockForms.ImageIdx.ToString();
						objXmlChildElement.AppendChild(objXml2LevelChild);

						objXml2LevelChild=objOutXML.CreateElement("FormType");
						objXml2LevelChild.InnerText = objCheckStockForms.FormType.ToString();
						objXmlChildElement.AppendChild(objXml2LevelChild);

						objXml2LevelChild=objOutXML.CreateElement("FormSource");
						sFormSource=objCheckStockForms.FormSource;
						//remove imageidx and | char from the beginning
						if(sFormSource!="")
						{
							sFormSource=sFormSource.Substring(2);
							/* 
							 * Vaibhav 8 Sep 2006 :
							 * No More File content would render to the screen. 
							 * File would be render on demand. i.e. Only on View Image. 
							 * 
							 * DBVALUE constant string would suggest that Imgae content has not changed.
							 */
							objXml2LevelChild.InnerText = "DBVALUE" ;
							objXmlChildElement.AppendChild(objXml2LevelChild);
						}

						objXMLElem.AppendChild(objXmlChildElement);		
					}
					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				#endregion

				objXMLElem = null;
				return objOutXML;
			}		
			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
                    ("CheckStockSetup.GetCheckStockDetails.Error", m_iClientId), p_objException);
			}	

			finally
			{
				if (objCheckStock != null)
					objCheckStock.Dispose();								
				objXMLElem =null;
				objOutXML=null;			
				CleanDataModel();
			}
			//return objReturnXML;
		}
		
		/// <summary>
		/// Vaibhav 8 Sep 2006 :
		/// On SetImage, this function saved the Image content to file. Contents of the file wud be read when 
		/// check stock is about to save. 
		/// </summary>
		/// <param name="p_sFileContentAsBase64String">binary content of the image</param>
		/// <returns>The temprary file name</returns>
		public string SaveToLocalServer(string p_sFileContentAsBase64String )
		{
			StreamWriter objStream = null ;				
			string sFilePath = string.Empty ;
			string sFileName = string.Empty ;

			try
			{
				sFilePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "CheckStock");

				if( sFilePath.Substring( sFilePath.Length -1 ) != @"\" )
					sFilePath += @"\" ;

				if( ! Directory.Exists( sFilePath ) )
					Directory.CreateDirectory( sFilePath );

				sFileName = "CheckStock" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + System.AppDomain.GetCurrentThreadId() + ".txt" ; 
				
				objStream = new StreamWriter( sFilePath + sFileName, false );
				objStream.Write( p_sFileContentAsBase64String );
				objStream.Close();				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("CheckStockSetup.SaveToLocalServer.Error", m_iClientId), p_objException);
			}	
			finally
			{
                if (objStream != null)
                {
                    objStream.Close();
                    objStream.Dispose();
                }
			}

			return sFileName ;
		}

		/// Name			: NewCheckStockDetails
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will add a new check stock. 		
		///		The details for the new check stock will be fetched from the XML string passed to the method.
		/// </summary>		
		/// <param name="p_sCheckStockDetail">
		///		XML Containing the setup related details 
		///		for a check stock that needs to be added in database.
		/// </param>			
		/// <returns>
		/// The XML containing the setup related data after inserting it into the database.
		/// </returns>
		public XmlDocument NewCheckStockDetails(string p_sCheckStockDetail)
		{
			XmlDocument      objOutXML=null;
			CheckStockList   objCheckStockList=null;
			CheckStock       objCheckStock=null;
			//			DataModelFactory objDMF=null;
			XmlDocument      objReturnXML=null;
			int				 iNewStockId=0;
			try
			{
				m_New=true;
				if ((p_sCheckStockDetail == null) || (p_sCheckStockDetail.Trim() == ""))
					throw new InvalidValueException(Globalization.GetString
                        ("CheckStockSetup.NewCheckStockDetails.InvalidXML", m_iClientId));

				objOutXML = new XmlDocument();				
				objOutXML.LoadXml(p_sCheckStockDetail);
				InitializeDataModel();
				objCheckStockList = (CheckStockList)m_objDMF.GetDataModelObject("CheckStockList",false);					
				objCheckStock = objCheckStockList.AddNew();
				ReadStockDetailXML(objOutXML, ref objCheckStock);
				objOutXML = null;
				objCheckStock.Save();
				iNewStockId = objCheckStock.StockId;

				#region Disposing Objects
				
				if (objCheckStock !=null)
					objCheckStock.Dispose();				
				if (objCheckStockList !=null)
					objCheckStockList.Dispose();								
				
				#endregion

				objReturnXML = this.GetCheckStockDetails(iNewStockId);				 
			}

			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
                    ("CheckStockSetup.NewCheckStockDetails.Error", m_iClientId), p_objException);
			}	
			finally
			{
				objOutXML=null;
				if (objCheckStockList !=null)
					objCheckStockList.Dispose();
				if (objCheckStock!=null)
					objCheckStock.Dispose();
				CleanDataModel();				
			}
			return objReturnXML;
		}

		/// Name			: UpdateCheckStockDetails
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will update setup related details for an existing check stock. 		
		///		The details will be fetched from the XML string passed to the method.
		/// </summary>		
		/// <param name="p_iStockId">Stock Id whose details needs to be updated</param>
		/// <param name="p_sCheckStockDetail">
		///		XML containing the setup related details 
		///		for a check stock that needs to be saved in database.
		/// </param>			
		/// <returns>
		/// The XML containing the setup related data after updating the details into the database.
		/// </returns>
		public XmlDocument UpdateCheckStockDetails(int p_iStockId,string p_sCheckStockDetail)
		{
			XmlDocument      objOutXML=null;
			CheckStock       objCheckStock=null;			
			XmlDocument		 objReturnXML=null;
			try
			{
				if ((p_sCheckStockDetail == null) || (p_sCheckStockDetail.Trim() == ""))
					throw new InvalidValueException(Globalization.GetString
                        ("CheckStockSetup.UpdateCheckStockDetails.InvalidXML", m_iClientId));

				objOutXML = new XmlDocument();
				objOutXML.LoadXml(p_sCheckStockDetail);
				InitializeDataModel();
				objCheckStock = (CheckStock)m_objDMF.GetDataModelObject("CheckStock",false);		
				try
				{
					objCheckStock.MoveTo(p_iStockId);				
				}
				catch (RecordNotFoundException p_objException)
				{
					throw new InvalidValueException(Globalization.GetString
                        ("CheckStockSetup.UpdateCheckStockDetails.StockIdNotFound", m_iClientId), p_objException);
				}
								
				
				ReadStockDetailXML(objOutXML, ref objCheckStock);
				objOutXML = null;
				objCheckStock.Save();

				#region Disposing Object

				if (objCheckStock!=null)
					objCheckStock.Dispose();								

				#endregion

				objReturnXML = this.GetCheckStockDetails(p_iStockId);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
                    ("CheckStockSetup.UpdateCheckStockDetails.Error", m_iClientId), p_objException);
			}	
			finally
			{
				objOutXML=null;				
				if (objCheckStock!=null)
					objCheckStock.Dispose();
				CleanDataModel();				
			}
			return objReturnXML;
		}

		/// Name			: PrintCheckStockSample
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will generate the sample for the selected check stock.		
		/// </summary>	
		/// <param name="p_iStockId">Stock Id</param>
		/// <returns>
		///     Binary stream corresponding to sample check generated for the specified stock.
		/// </returns>
		public MemoryStream PrintCheckStockSample (int p_iStockId,string p_sSampleText)    
		{
			string					sReturnFileName="";
			byte[]					arrRet  = null;			
			FileStream				objFStream=null;		
			BinaryReader			objBReader=null;
			MemoryStream			objMemoryStream=null;
			CheckManager			objPrintCheck=null;
			
			try
			{
				objPrintCheck = new CheckManager(m_sDatabaseName,m_sUserName,m_sPassword, m_iClientId );
                //Ashish Ahuja - Pay To The Order Config
				objPrintCheck.PrintSampleCheck(p_iStockId,p_sSampleText,ref sReturnFileName);		

				objFStream = new FileStream(sReturnFileName,FileMode.Open);
				objBReader = new BinaryReader(objFStream);
				arrRet = objBReader.ReadBytes((int)objFStream.Length);
				objMemoryStream = new MemoryStream(arrRet);			
				return objMemoryStream;			
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
                    ("CheckStockSetup.PrintCheckStockSample.Error", m_iClientId), p_objException);
			}
			finally
			{
				if( objPrintCheck != null )
				{
					//objPrintCheck.UnInitialize();
					objPrintCheck.Dispose();
                    objPrintCheck = null;	
				}

				if( objFStream != null) 
				{
					objFStream.Close();
                    objFStream.Dispose();
				}
				if( objBReader != null) 
				{
					objBReader.Close();
					objBReader = null;					
				}
				objMemoryStream = null;
			}
			
		}

		/// Name			: CloneCheckStockDetails 
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will create a copy of the current check stock to use for 
		///		any existing bank account i.e. cloning.		
		/// </summary>	
		/// <param name="p_iStockId">Stock Id whose copy will be created</param>
		/// <param name="p_bCurBankAccount">
		///      Flag to identify whether copy of specified check stock needs to be 
		///      created for the current bank
		///      account or for the bank account passed as the input parameter.
		/// </param>
		/// <param name="p_iAccountId">
		///      Bank Account for which the copy of a specified check stock will be created
		/// </param>
		/// <returns> The XML containing the data related to new check stock after cloning.</returns>	
		public XmlDocument CloneCheckStockDetails  (int p_iStockId,bool p_bCurBankAccount,int p_iAccountId)    
		{
			CheckStockList   objCheckStockList=null;
			CheckStock       objOldCheckStock=null;
			CheckStock       objNewCheckStock=null;
            //Changed by Gagan for MITS 15984 : Start
            CheckStockForms  objNewCheckStockForms = null;
            //Changed by Gagan for MITS 15984 : End
			XmlDocument      objOldCheckStockXML = null;
			XmlDocument      objReturnXML=null;
			//XmlDocument		 objOldCheckStockXML=null;
			int				 iNewCheckStockId=0;
            UserLogin objUserLogin = null;//Deb : MITS 31263
			try
			{
				m_Clone=true;
				//If cloning of Check Stock needs to be done for a bank account different from
				//current bank account (i.e. Account Id for the check stock which needs to be cloned)
				if (!p_bCurBankAccount)
					if (p_iAccountId == 0)
						throw new InvalidValueException(Globalization.GetString
                            ("CheckStockSetup.CloneCheckStockDetails.InvalidBankAcc", m_iClientId));
				
				// Getting the XML containing the data corresponding to the check stock that will be cloned.
				objOldCheckStockXML = this.GetCheckStockDetails(p_iStockId);
				//objOldCheckStockXML = new XmlDocument();
				//objOldCheckStockXML.LoadXml(sOldCheckStockXML);

                //Raman Bhatia 06/27/2007 : m_objDMF has been cleaned up so needs to be initialized again
                
                InitializeDataModel();
				objOldCheckStock = (CheckStock)m_objDMF.GetDataModelObject("CheckStock",false);
                
				// Building the Check Stock Object corresponding to the stock that will be cloned.
				ReadStockDetailXML(objOldCheckStockXML, ref objOldCheckStock);
				objOldCheckStockXML = null;									
				
                objCheckStockList = (CheckStockList)m_objDMF.GetDataModelObject("CheckStockList",false);					
				objNewCheckStock = objCheckStockList.AddNew();
                
				#region Updating the values in the new check stock object 
				
				// Some of the fields are not copied while cloning.
				// This is as per the code in RMWorld.

				objNewCheckStock.DolSignFlag = objOldCheckStock.DolSignFlag;
				objNewCheckStock.MemoFontName = objOldCheckStock.MemoFontName;
				objNewCheckStock.MemoFontSize = objOldCheckStock.MemoFontSize;
				objNewCheckStock.MemoRowLength = objOldCheckStock.MemoRowLength;
				objNewCheckStock.MemoRows = objOldCheckStock.MemoRows;
                //Ashish Ahuja - Pay To The Order Config starts
                objNewCheckStock.PayToOrderFontName = objOldCheckStock.PayToOrderFontName;
                objNewCheckStock.PayToOrderFontSize = objOldCheckStock.PayToOrderFontSize;
                objNewCheckStock.PayToOrderRowLength = objOldCheckStock.PayToOrderRowLength;
                objNewCheckStock.PayToOrderRows = objOldCheckStock.PayToOrderRows;
                //Ashish Ahuja - Pay To The Order Config ends
				objNewCheckStock.PrintAddrFlag = objOldCheckStock.PrintAddrFlag;
				objNewCheckStock.PrintMemoFlag = objOldCheckStock.PrintMemoFlag;	
				objNewCheckStock.AccountId = objOldCheckStock.AccountId;
				objNewCheckStock.ChkDateX = objOldCheckStock.ChkDateX;
				objNewCheckStock.ChkDateY = objOldCheckStock.ChkDateY;
				objNewCheckStock.ChkNumberX = objOldCheckStock.ChkNumberX;
				objNewCheckStock.ChkNumberY = objOldCheckStock.ChkNumberY;
				objNewCheckStock.ChkAmountX = objOldCheckStock.ChkAmountX;
				objNewCheckStock.ChkAmountY = objOldCheckStock.ChkAmountY;
				objNewCheckStock.ChkAmountTextX = objOldCheckStock.ChkAmountTextX;
				objNewCheckStock.ChkAmountTextY = objOldCheckStock.ChkAmountTextY;
				objNewCheckStock.PayeeX = objOldCheckStock.PayeeX;
				objNewCheckStock.PayeeY = objOldCheckStock.PayeeY;
				objNewCheckStock.ChkMemoX = objOldCheckStock.ChkMemoX;
				objNewCheckStock.ChkMemoY = objOldCheckStock.ChkMemoY;
				objNewCheckStock.ChkDetailX = objOldCheckStock.ChkDetailX;
				objNewCheckStock.ChkDetailY = objOldCheckStock.ChkDetailY;
				objNewCheckStock.ChkPayerX = objOldCheckStock.ChkPayerX;
				objNewCheckStock.ChkPayerY = objOldCheckStock.ChkPayerY;
				objNewCheckStock.FontName = objOldCheckStock.FontName;
				objNewCheckStock.FontSize = objOldCheckStock.FontSize;
				objNewCheckStock.ChkOffsetX = objOldCheckStock.ChkOffsetX;
				objNewCheckStock.ChkOffsetY = objOldCheckStock.ChkOffsetY;
				objNewCheckStock.ChkMicrX = objOldCheckStock.ChkMicrX;
				objNewCheckStock.ChkMicrY = objOldCheckStock.ChkMicrY;
				objNewCheckStock.ChkRxlaserFlag = objOldCheckStock.ChkRxlaserFlag;
				objNewCheckStock.ChkMicrFlag = objOldCheckStock.ChkMicrFlag;
				objNewCheckStock.ChkMicrSequence = objOldCheckStock.ChkMicrSequence;
				objNewCheckStock.ChkRxlaserForm = objOldCheckStock.ChkRxlaserForm;
				objNewCheckStock.PreNumStock = objOldCheckStock.PreNumStock;
				objNewCheckStock.SpecialFlag = objOldCheckStock.SpecialFlag;
				objNewCheckStock.SpecialAmount = objOldCheckStock.SpecialAmount;
				objNewCheckStock.RxVoidForm = objOldCheckStock.RxVoidForm;
				objNewCheckStock.RxSpecialForm = objOldCheckStock.RxSpecialForm;
				objNewCheckStock.PayerLevel = objOldCheckStock.PayerLevel;
                objNewCheckStock.CheckDateFormat = objOldCheckStock.CheckDateFormat; //MITS 33324 mcapps2
                objNewCheckStock.MicrFont = objOldCheckStock.MicrFont;
                objNewCheckStock.MicrFontSize = objOldCheckStock.MicrFontSize;
                objNewCheckStock.MicrType = objOldCheckStock.MicrType;
                //asingh263 mits 32712 starts 
                objNewCheckStock.PrntAgentOnStub = objOldCheckStock.PrntAgentOnStub;
                objNewCheckStock.PrntPolicyOnStub = objOldCheckStock.PrntPolicyOnStub;
                objNewCheckStock.PrntClaimDateOnStub = objOldCheckStock.PrntClaimDateOnStub;
                objNewCheckStock.PrntPolicyOnCheck = objOldCheckStock.PrntPolicyOnCheck;
                objNewCheckStock.PrntClaimDateOnCheck = objOldCheckStock.PrntClaimDateOnCheck;
                objNewCheckStock.PrntEventDateOnCheck = objOldCheckStock.PrntEventDateOnCheck;
                objNewCheckStock.PrntInsurerOnCheck = objOldCheckStock.PrntInsurerOnCheck;
                objNewCheckStock.PrintAddrOnStubFlag = objOldCheckStock.PrintAddrOnStubFlag;

                objNewCheckStock.ChkEventDateX = objOldCheckStock.ChkEventDateX;
                objNewCheckStock.ChkEventDateY = objOldCheckStock.ChkEventDateY;
                objNewCheckStock.ChkClaimDateX = objOldCheckStock.ChkClaimDateX;
                objNewCheckStock.ChkClaimDateY = objOldCheckStock.ChkClaimDateY;
                objNewCheckStock.ChkInsurerX = objOldCheckStock.ChkInsurerX;
                objNewCheckStock.ChkInsurerY = objOldCheckStock.ChkInsurerY;
                objNewCheckStock.ChkPolicyX = objOldCheckStock.ChkPolicyX;
                objNewCheckStock.ChkPolicyY = objOldCheckStock.ChkPolicyY;
                objNewCheckStock.PayeeAddrX = objOldCheckStock.PayeeAddrX;
                objNewCheckStock.PayeeAddrY = objOldCheckStock.PayeeAddrY;

                objNewCheckStock.AgentAddrX = objOldCheckStock.AgentAddrX;
                objNewCheckStock.AgentAddrY = objOldCheckStock.AgentAddrY;
                objNewCheckStock.PayeeAddrOnCheckX = objOldCheckStock.PayeeAddrOnCheckX;
                objNewCheckStock.PayeeAddrOnCheckY = objOldCheckStock.PayeeAddrOnCheckY;

                //asingh263 mits 32712 ends

                //Changed by Gagan for MITS 15984 : Start
                //Start by Shivendu for MITS 12058
                objNewCheckStock.DolSignFlag = objOldCheckStock.DolSignFlag;
                objNewCheckStock.PrintInsurer = objOldCheckStock.PrintInsurer;
                objNewCheckStock.PrintAddrFlag = objOldCheckStock.PrintAddrFlag;
                objNewCheckStock.PrntClmNumCheck = objOldCheckStock.PrntClmNumCheck;
                objNewCheckStock.PrntTaxidOnStub = objOldCheckStock.PrntTaxidOnStub;
                objNewCheckStock.FormatDateOfCheck = objOldCheckStock.FormatDateOfCheck; // MITS 26614 mcapps2
                objNewCheckStock.PrntCtlOnStub = objOldCheckStock.PrntCtlOnStub;
                objNewCheckStock.PreNumStock = objOldCheckStock.PreNumStock;
                objNewCheckStock.PrntAdjOnStub = objOldCheckStock.PrntAdjOnStub;
                objNewCheckStock.PrintMemoFlag = objOldCheckStock.PrintMemoFlag;
                objNewCheckStock.PrntChkDtOnStub = objOldCheckStock.PrntChkDtOnStub;
                objNewCheckStock.PrntEventOnStub = objOldCheckStock.PrntEventOnStub;
                objNewCheckStock.AddressFlag = objOldCheckStock.AddressFlag;
                objNewCheckStock.ChkLayout = objOldCheckStock.ChkLayout;

                objNewCheckStock.SecdOffsetX = objOldCheckStock.SecdOffsetX; 
                objNewCheckStock.SecdOffsetY = objOldCheckStock.SecdOffsetY;

                objNewCheckStock.ClmNumOffsetX = objOldCheckStock.ClmNumOffsetX;
                objNewCheckStock.ClmNumOffsetY = objOldCheckStock.ClmNumOffsetY;


                
                //MITS 12254 - 05/07/2008 Raman Bhatia
                //When a check stock is cloned,the check image is not copied to cloned check stock.

                objNewCheckStock.ChkSoftForm = objOldCheckStock.ChkSoftForm;

                //End by Shivendu for MITS 12058
                //Deb : MITS 31263
                objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDatabaseName, m_iClientId);
                objNewCheckStock.StockName = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckStockSetup.CloneCheckStockDetails.CopyOf", m_iClientId), objUserLogin.objUser.NlsCode.ToString()) + " " + objOldCheckStock.StockName;//rkaur27
                //Deb : MITS 31263
                if (!p_bCurBankAccount)
                    objNewCheckStock.AccountId = p_iAccountId;

                #endregion

                objNewCheckStock.Save();
                iNewCheckStockId = objNewCheckStock.StockId;

                //MITS 12254 - 05/07/2008 Raman Bhatia
                //When a check stock is cloned,the check image is not copied to cloned check stock.
                objOldCheckStock.MoveTo(p_iStockId);

                foreach (CheckStockForms objOldCheckStockForms in objOldCheckStock.CheckStockFormsList)
                {
                    objNewCheckStockForms = (CheckStockForms)m_objDMF.GetDataModelObject("CheckStockForms", false);
                    objNewCheckStockForms.StockId = iNewCheckStockId;
                    objNewCheckStockForms.ImageIdx = objOldCheckStockForms.ImageIdx;
                    objNewCheckStockForms.FormType = objOldCheckStockForms.FormType;
                    objNewCheckStockForms.FormSource = objOldCheckStockForms.FormSource;
                    objNewCheckStockForms.Save();

                }

                //Changed by Gagan for MITS 15984 : End

				#region Disposing Objects

				if (objOldCheckStock !=null)
					objOldCheckStock.Dispose();
				if (objCheckStockList !=null)
					objCheckStockList.Dispose();			
				if (objNewCheckStock !=null)
					objNewCheckStock.Dispose();

				#endregion
			
				objReturnXML = this.GetCheckStockDetails(iNewCheckStockId);				 
			}

			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
					("CheckStockSetup.CloneCheckStockDetails.Error", m_iClientId), p_objException);
			}	
			finally
			{
				if (objNewCheckStock !=null)
					objNewCheckStock.Dispose();
				if (objOldCheckStock !=null)
					objOldCheckStock.Dispose();
				if (objCheckStockList !=null)
					objCheckStockList.Dispose();
			
				objOldCheckStockXML = null;
                CleanDataModel();
                //Deb : MITS 31263
                if (objUserLogin != null)
                {
                    objUserLogin = null;
                }
                //Deb : MITS 31263
			}
			return objReturnXML;			
		}

		/// Name			: DeleteStockDetails
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will delete details related to a specific check stock.		
		/// </summary>	
		/// <param name="p_iStockId">Stock Id to be deleted</param>			
		public void DeleteStockDetails  (int p_iStockId)    
		{
			CheckStock       objCheckStock=null;			
			try
			{
				InitializeDataModel();
				objCheckStock = (CheckStock)m_objDMF.GetDataModelObject("CheckStock",false);		
				try
				{
					objCheckStock.MoveTo(p_iStockId);				
				}
				catch (RecordNotFoundException p_objException)
				{
					throw new RMAppException(Globalization.GetString
						("CheckStockSetup.DeleteStockDetails.StockIdNotFound", m_iClientId), p_objException);
				}	
								
				objCheckStock.Delete();
			}
			catch(RMAppException p_objException)			
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
					("CheckStockSetup.DeleteStockDetails.Error", m_iClientId), p_objException);
			}	
			finally
			{
				if (objCheckStock!=null)
					objCheckStock.Dispose();
				CleanDataModel();
			}
		}
		
		/// Name			: MoveAndGetStockDetails
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will move to a specified check stock record and return back the details
		///		related to that check stock.
		/// </summary>	
		/// <param name="p_iAccountId">Bank Account Id</param>
		/// <param name="p_iStockId">Stock Id</param>	
		/// <param name="p_iNavigation">Navigation Direction ex - Move First, Move Last etc</param>		
		/// <returns>The XML containing the details of the check stock</returns>	
		public XmlDocument MoveAndGetStockDetails(int p_iAccountId,int p_iStockId,int p_iNavigation)    
		{
			DbReader        objReader= null;
			StringBuilder   sbSQL=null;
			int             iMoveToStockId=0;
			XmlDocument     objReturnXML=null;
			string			sConnString = "";
			try
			{
				InitializeDataModel();
				sConnString = m_objDMF.Context.DbConn.ConnectionString;    		

				if(p_iAccountId == 0)
					throw new InvalidValueException(Globalization.GetString
						("CheckStockSetup.MoveAndGetStockDetails.InvalidAccId", m_iClientId));

				sbSQL = new StringBuilder();
				switch (p_iNavigation)
				{
					case (int)Common.Constants.NavDir.NavigationFirst:						
						sbSQL.Append("SELECT MIN(STOCK_ID) FROM CHECK_STOCK "); 
						sbSQL.Append("WHERE ACCOUNT_ID = " + p_iAccountId);
						break;

					case (int) Common.Constants.NavDir.NavigationLast:
						sbSQL.Append("SELECT MAX(STOCK_ID) FROM CHECK_STOCK "); 
						sbSQL.Append("WHERE ACCOUNT_ID = " + p_iAccountId);
						break;

					case (int) Common.Constants.NavDir.NavigationNext:
                        //Rsolanki2 (2 nov 07) - updates for mits 10166 and 10733                         
                        //if(p_iStockId == 0)
                        //    throw new InvalidValueException(Globalization.GetString
                        //        ("CheckStockSetup.MoveAndGetStockDetails.InvalidStockId"));

                        sbSQL.Append("SELECT CASE WHEN MAX(STOCK_ID) > ");
                        sbSQL.Append(p_iStockId);
                        sbSQL.Append(" THEN MIN(STOCK_ID)");
                        sbSQL.Append(" ELSE NULL END ");
                        sbSQL.Append(" FROM CHECK_STOCK ");                        
						sbSQL.Append(" WHERE STOCK_ID> " + p_iStockId + " AND ACCOUNT_ID = " + p_iAccountId);
						break;

					case (int) Common.Constants.NavDir.NavigationPrev:												
                        //if(p_iStockId == 0)
                        //    throw new InvalidValueException(Globalization.GetString
                        //        ("CheckStockSetup.MoveAndGetStockDetails.InvalidStockId"));

                        sbSQL.Append("SELECT CASE WHEN MIN(STOCK_ID) <");
                        sbSQL.Append(p_iStockId);
                        sbSQL.Append(" THEN MAX(STOCK_ID)");
                        sbSQL.Append(" ELSE NULL END ");
                        sbSQL.Append(" FROM CHECK_STOCK ");                        
						sbSQL.Append(" WHERE STOCK_ID< " + p_iStockId + " AND ACCOUNT_ID = " + p_iAccountId);						
						break;
                        // rsolanki2 - end updates
					case (int) Common.Constants.NavDir.NavigationNone:												
							iMoveToStockId=p_iStockId;
						break;

					default :
						throw new InvalidValueException(Globalization.GetString
                            ("CheckStockSetup.MoveAndGetStockDetails.InvalidNavigation", m_iClientId));

				}				

				//Fetch the Stock Id
				if (p_iNavigation!=(int)Common.Constants.NavDir.NavigationNone)
				{                    

					objReader = DbFactory.GetDbReader(sConnString,sbSQL.ToString());
					if (objReader.Read())
						if (objReader.GetValue(0) != System.DBNull.Value)						
							iMoveToStockId = objReader.GetInt(0);
				
					objReader.Dispose();

                    if (p_iStockId != 0 && iMoveToStockId == 0)
                    {
                        iMoveToStockId = p_iStockId;                        
                    }
				}
                
				//Only if New Stock Id is not 0 
                if (iMoveToStockId != 0)
                    objReturnXML = this.GetCheckStockDetails(iMoveToStockId);
                else
                    objReturnXML = this.GetCheckStockUnit();
			}
			catch(RMAppException p_objException)			
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
					("CheckStockSetup.MoveAndGetStockDetails.Error", m_iClientId), p_objException);
			}			
			finally
			{
				if(objReader !=null)
					objReader.Dispose();				
				sbSQL = null;
				CleanDataModel();
			}
			return objReturnXML;
		}

        /// <summary>
        /// Gets a list of all of the installed fonts on the system
        /// </summary>
        /// <returns>Generic Dictionary collection which enumerates all
        /// of the installed fonts</returns>
        public Dictionary<string, string> GetInstalledFonts()
        {
            Dictionary<string, string> dictInstalledFonts = new Dictionary<string, string>();
            int intFontIndex = 0;

            InstalledFontCollection objInstalledFonts = new InstalledFontCollection();

            foreach (FontFamily fontFamily in objInstalledFonts.Families)
            {
                dictInstalledFonts.Add(fontFamily.Name, intFontIndex.ToString());

                //Increment the font index value
                intFontIndex++;
            }//foreach

            return dictInstalledFonts;
        } // method: GetInstalledFonts

		public XmlDocument GetAccountNameAndId()
		{
            string sConnString = string.Empty, sSQL = string.Empty, sAccountName = string.Empty;
            string sAccountId = string.Empty;
			DbReader objReader = null;
			XmlDocument objXmlDocument = null;
			XmlElement objXmlElementParent = null;
			XmlElement objParentElement = null;
			XmlElement objChildElement = null;
            XmlAttribute objAttribute = null;

			try
			{
				objXmlDocument = new XmlDocument ();
				objParentElement =  objXmlDocument.CreateElement("CheckStockSetup");
				objXmlDocument.AppendChild(objParentElement);

				objChildElement =  objXmlDocument.CreateElement("Accounts");
				objParentElement.AppendChild(objChildElement);

				sSQL = "SELECT ACCOUNT_ID , ACCOUNT_NAME FROM ACCOUNT";
				

				InitializeDataModel();
				sConnString = m_objDMF.Context.DbConn.ConnectionString;    				

				objReader = DbFactory.GetDbReader(sConnString,sSQL);
					
				if (objReader!=null)
				{
					while (objReader.Read())
					{
						sAccountName= Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("Account_Name")).Trim();
						sAccountId= Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("Account_Id")).Trim();
						//objXmlElementParent = objXmlDocument.CreateElement("Account");
                        objXmlElementParent = objXmlDocument.CreateElement("option");
                        objChildElement.AppendChild(objXmlElementParent);
                        objAttribute = objXmlDocument.CreateAttribute("value");
                        objAttribute.Value = sAccountId;
                        objXmlElementParent.Attributes.Append(objAttribute);
                        objXmlElementParent.InnerText = sAccountName;
                        //objXmlElementParent = objXmlDocument.CreateElement("option");
                        //objXmlElementParent = objXmlDocument.CreateElement("value");
						//objXmlElementChild1 = objXmlDocument.CreateElement("value");
						//objXmlElementChild1.InnerText = sAccountName;
						//objXmlElementParent.AppendChild(objXmlElementChild1);
						//objXmlElementChild2 = objXmlDocument.CreateElement("AccountId");
						//objXmlElementChild2.InnerText = sAccountId;
						//objXmlElementParent.AppendChild(objXmlElementChild2);
						//objChildElement.AppendChild(objXmlElementParent);				
					}
					objReader.Close();
				}
			}

			
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("CheckstockSetUp.GetAcccountNameAndId.Error", m_iClientId), p_objException);
			}
			finally 
			{
				if (objReader != null) 
				{
					objReader.Close();
					objReader.Dispose();
				}
				objChildElement = null;
				objParentElement = null;
				CleanDataModel();
				
			}
			return(objXmlDocument);				

			
		
		}

		/// Name			: ViewImage 
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		/// 
		///	Vaibhav 8 Sep 2006
		///	
		///	If TempFile is a valid file then return the content of that temp file or 
		///	else return the contents from the DB. 
		/// ************************************************************
		/// <summary>
		///		This method will return binary data for the requested image corresponding
		///		to a specified check stock.
		/// </summary>		
		/// <param name="p_iStockId">Stock Id</param>
		/// <param name="p_iImageId">Image id to be fetched</param>
		/// <returns>Binary data for the requested image</returns>		
		public string ViewImage (int p_iStockId, int p_iImageId, string sTempFileName )    
		{
			MemoryStream  objStockBackImage = null;
			string sFilePath = string.Empty ;
			StreamReader objStreamReader = null ;
			string sFileContent = string.Empty ;
			CheckStock objCheckStock=null;
			string sFormSource=string.Empty;
			string sReturnValue = string.Empty ;
			try
			{
				/*
				 * Vaibhav 8 Sep 2006
				 * 
				 * If sTempFileName value is "DBVALUE", It represent that file content has not been modified. 
				 */ 
				if( sTempFileName == "DBVALUE" )
				{
				//The collection for holding the EMFs corresponding to a check stock was not
				//implemented in CheckStock class of Datamodel.
				//Done By: Nikhil Garg		Dated: 11-Apr-2005
				InitializeDataModel();
				objCheckStock = (CheckStock)m_objDMF.GetDataModelObject("CheckStock",false);				
				try
				{
					objCheckStock.MoveTo(p_iStockId);
				}
				catch (RecordNotFoundException p_objException)
				{
					throw new InvalidValueException(Globalization.GetString
						("CheckStockSetup.GetCheckStockDetails.StockIdNotFound", m_iClientId), p_objException);
				}

				foreach(CheckStockForms objCheckStockForms in objCheckStock.CheckStockFormsList)
				{
					if (objCheckStockForms.ImageIdx==p_iImageId)
					{
						sFormSource=objCheckStockForms.FormSource;
						//remove imageidx and | char from the beginning
						sFormSource=sFormSource.Substring(2);
						ZipUtility objZipUtility=new ZipUtility(m_iClientId);
						objStockBackImage=objZipUtility.UnZIPBufferToMemoryStream(ref sFormSource);
							sReturnValue = Convert.ToBase64String( objStockBackImage.ToArray() ) ;
					}
				}
				}
				else
				{
                    sFilePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "CheckStock");

					if( sFilePath.Substring( sFilePath.Length -1 ) != @"\" )
						sFilePath += @"\" ;	

					objStreamReader = new StreamReader( sFilePath + @"\" + sTempFileName );
					sFileContent = objStreamReader.ReadToEnd();
					objStreamReader.Close();

					sReturnValue = sFileContent;

				}				
				return( sReturnValue );
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                //sgoel6 MITS 15359 04/17/2009 
                //The message has been corrected for condition when no image is available
                if (sTempFileName != "DBVALUE" && sTempFileName == "")
                    throw new RMAppException(Globalization.GetString
                    ("CheckStockSetup.ViewImage.NoImageMessage", m_iClientId), p_objException);
                else
				throw new RMAppException(Globalization.GetString
					("CheckStockSetup.ViewImage.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objCheckStock != null)
					objCheckStock.Dispose();
				objStockBackImage=null;	
				if( objStreamReader != null )
					objStreamReader.Close();
				CleanDataModel();
			}
		}

		/// Name			: ReadStockDetailXML
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will read the setup details from XML and update the CheckStock Object
		///		with those values.
		/// </summary>		
		/// <param name="p_objXMLDoc">
		///		The XML containing the details related to a stock id.
		///		The values in the XML are read and these values are saved in database.
		///	</param>
		/// <param name="p_objCheckStock">
		///     An object containing the details related to a stock id. 
		///     The values are read from XML and are assigned to the object.
		/// </param>	
		private void ReadStockDetailXML(XmlDocument p_objXMLDoc, ref CheckStock p_objCheckStock)
		{
			string sValue="";
			bool bRxLaserFlag = false;
			bool bSoftForm = false;
            ZipUtility objZipUtility = new ZipUtility(m_iClientId);
			try
			{
				
				#region Assigning values from XML to the CheckStock Object

				sValue = p_objXMLDoc.SelectSingleNode("//" + "StockName").InnerText;				
				if (sValue != null)
					p_objCheckStock.StockName = sValue;

				sValue = p_objXMLDoc.SelectSingleNode("//" + "FontName").InnerText;								
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.FontName = "Arial";
				else
					p_objCheckStock.FontName = sValue;

				sValue = p_objXMLDoc.SelectSingleNode("//" + "AccountId").InnerText;								
				if (sValue != null)
					p_objCheckStock.AccountId = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//" + "FontSize").InnerText;															
				p_objCheckStock.FontSize =  Conversion.ConvertStrToDouble(sValue);
				if ((p_objCheckStock.FontSize < 4) || (p_objCheckStock.FontSize > 40))
					p_objCheckStock.FontSize  = 10;

				sValue = p_objXMLDoc.SelectSingleNode("//" + "MICRFlag").InnerText;			
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.ChkMicrFlag = false;
				else
					p_objCheckStock.ChkMicrFlag = Convert.ToBoolean(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MICRSequence").InnerText;				
				if (sValue != null)
					p_objCheckStock.ChkMicrSequence = sValue;
				

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MICRType").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.MicrType = 0;
				else
				{
					if(Convert.ToBoolean(Common.Conversion.ConvertStrToInteger(sValue)) == true)
					{
						p_objCheckStock.MicrType = 1;
						sValue = p_objXMLDoc.SelectSingleNode("//"+ "MICRFont").InnerText;				
						if (sValue != null)
							p_objCheckStock.MicrFont = sValue;

						sValue = p_objXMLDoc.SelectSingleNode("//"+ "MICRFontSize").InnerText;								
						p_objCheckStock.MicrFontSize = Common.Conversion.ConvertStrToDouble(sValue);
					}
					else															
						p_objCheckStock.MicrType = 0;				
				}

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "RXLaserFlag").InnerText;				
				if ((sValue == null) || (sValue.Trim() == ""))
					bRxLaserFlag = false;
				else
					bRxLaserFlag = Convert.ToBoolean(sValue);					
				p_objCheckStock.ChkRxlaserFlag = bRxLaserFlag;

				// The following line were commented because ChkRxlaserForm property in CheckStock class
				// of Datamodel should return an integer value instead of boolean
				// Removed the commenting of the lines after the change in DataModel
				//Done By: Nikhil Garg		Dated: 14-Mar-2005
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "RXLaserForm").InnerText;								
				p_objCheckStock.ChkRxlaserForm =Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "RXVoidForm").InnerText;								
				p_objCheckStock.RxVoidForm =Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "RXSpecialForm").InnerText;								
				p_objCheckStock.RxSpecialForm = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "SoftForm").InnerText;			
				if ((sValue == null) || (sValue.Trim() == ""))	
					bSoftForm = false;
				else
					bSoftForm = Convert.ToBoolean(sValue);
				p_objCheckStock.ChkSoftForm = bSoftForm;

				if ((bRxLaserFlag) || (bSoftForm))
				{
					sValue = p_objXMLDoc.SelectSingleNode("//"+ "SpecialFlag").InnerText;		
					if ((sValue == null) || (sValue.Trim() == ""))
					{
						p_objCheckStock.SpecialFlag = false;
						p_objCheckStock.SpecialAmount = 0;
					}
					else
					{	
						p_objCheckStock.SpecialFlag = Convert.ToBoolean(sValue);
						if (p_objCheckStock.SpecialFlag)
						{
							sValue = p_objXMLDoc.SelectSingleNode("//"+ "SpecialAmount").InnerText;								
							//p_objCheckStock.SpecialAmount = Common.Conversion.ConvertStrToInteger(sValue);
                            //deb:MITS 24624
                            p_objCheckStock.SpecialAmount = Common.Conversion.ConvertObjToDouble(sValue, m_iClientId);
						}
						else
							p_objCheckStock.SpecialAmount = 0;
					}

				}				
				else
				{
					p_objCheckStock.SpecialFlag = false;
					p_objCheckStock.SpecialAmount = 0;
				}
				
				if (bRxLaserFlag)
				{
					p_objCheckStock.ChkRxlaserFlag= true;
					p_objCheckStock.ChkSoftForm= false;
				}
				else if (bSoftForm)
				{
					p_objCheckStock.ChkRxlaserFlag= false;
					p_objCheckStock.ChkSoftForm= true;
				}
				else
				{
					p_objCheckStock.ChkRxlaserFlag= false;
					p_objCheckStock.ChkSoftForm= false;					
				}

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintClaimNumberOnCheck").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
				{
					p_objCheckStock.PrntClmNumCheck = false;
					p_objCheckStock.PrntClmNumCheck = false;
					p_objCheckStock.ClmNumOffsetX = 0;
					p_objCheckStock.ClmNumOffsetY = 0;
				}
				else
				{
					p_objCheckStock.PrntClmNumCheck = Convert.ToBoolean(sValue);
					if (p_objCheckStock.PrntClmNumCheck)
					{
						sValue = p_objXMLDoc.SelectSingleNode("//"+ "ClaimNumberOffsetX").InnerText;		
						p_objCheckStock.ClmNumOffsetX = Common.Conversion.ConvertStrToInteger(sValue);

						sValue = p_objXMLDoc.SelectSingleNode("//"+ "ClaimNumberOffsetY").InnerText;								
						p_objCheckStock.ClmNumOffsetY = Common.Conversion.ConvertStrToInteger(sValue);
					}
					else
					{
						p_objCheckStock.PrntClmNumCheck = false;
						p_objCheckStock.ClmNumOffsetX = 0;
						p_objCheckStock.ClmNumOffsetY = 0;
					}
				}
                //For Visio 2007 support
                if (p_objXMLDoc.SelectSingleNode("//" + "NewVisioVersion") != null)
                {
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "NewVisioVersion").InnerText;
                    if ((sValue == null) || (sValue.Trim() == ""))
                        p_objCheckStock.NewVisioVersion = false;
                    else
                        p_objCheckStock.NewVisioVersion = Convert.ToBoolean(sValue);
                }
                //For Visio 2007 support

                //MITS 26614 mcapps2 start

                sValue = p_objXMLDoc.SelectSingleNode("//" + "FormatDateOfCheck").InnerText;
                if ((sValue == null) || (sValue.Trim() == ""))
                {
                    p_objCheckStock.FormatDateOfCheck = false;
                }
                else
                {
                    p_objCheckStock.FormatDateOfCheck = Convert.ToBoolean(sValue);
                }
                //MITS 26614 mcapps2 End

                //asingh263 mits 32712 starts
                if (p_objXMLDoc.SelectSingleNode("//" + "PrintDateofClaim") != null)
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintDateofClaim").InnerText;
                else if (p_objXMLDoc.SelectSingleNode("//" + "PrntClaimDateOnCheck") != null)
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "PrntClaimDateOnCheck").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrntClaimDateOnCheck = false;
                }
                else
                {
                    p_objCheckStock.PrntClaimDateOnCheck = Convert.ToBoolean(sValue);
                }
                if (p_objXMLDoc.SelectSingleNode("//" + "PrintAgentNameAddressOnStub")!=null)
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintAgentNameAddressOnStub").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrntAgentOnStub = false;
                }
                else
                {
                    p_objCheckStock.PrntAgentOnStub = Convert.ToBoolean(sValue);
                }
                if (p_objXMLDoc.SelectSingleNode("//" + "PrintFullPolicyNumberonStub") != null)
                   sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintFullPolicyNumberonStub").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrntPolicyOnStub = false;
                }
                else
                {
                    p_objCheckStock.PrntPolicyOnStub = Convert.ToBoolean(sValue);
                }
               
                if(p_objXMLDoc.SelectSingleNode("//" + "PrintFullPolicyNumber")!=null)
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintFullPolicyNumber").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrntPolicyOnCheck = false;
                }
                else
                {
                    p_objCheckStock.PrntPolicyOnCheck = Convert.ToBoolean(sValue);
                }

                if(p_objXMLDoc.SelectSingleNode("//" + "PrintClaimDateOnStub")!=null)
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintClaimDateOnStub").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrntClaimDateOnStub = false;
                }
                else
                {
                    p_objCheckStock.PrntClaimDateOnStub = Convert.ToBoolean(sValue);
                }
               
                if(p_objXMLDoc.SelectSingleNode("//" + "PrintDateofEvent")!=null)
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintDateofEvent").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrntEventDateOnCheck = false;
                }
                else
                {
                    p_objCheckStock.PrntEventDateOnCheck = Convert.ToBoolean(sValue);
                }
               
                if(p_objXMLDoc.SelectSingleNode("//" + "PrintInsuredNameOnCheck")!=null)
                    sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintInsuredNameOnCheck").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrntInsurerOnCheck = false;
                }
                else
                {
                    p_objCheckStock.PrntInsurerOnCheck = Convert.ToBoolean(sValue);
                }

                if(p_objXMLDoc.SelectSingleNode("//" + "PrintPayeeAddressOnStub")!=null)
                sValue = p_objXMLDoc.SelectSingleNode("//" + "PrintPayeeAddressOnStub").InnerText;
                if (string.IsNullOrEmpty(sValue))
                {
                    p_objCheckStock.PrintAddrOnStubFlag = false;
                }
                else
                {
                    p_objCheckStock.PrintAddrOnStubFlag = Convert.ToBoolean(sValue);
                }
                //asingh263 mits 32712 ends

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintTaxIDOnStub").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.PrntTaxidOnStub = false;
				else
					p_objCheckStock.PrntTaxidOnStub = Convert.ToBoolean(sValue);
				
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintAdjNamePhoneOnStub").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.PrntAdjOnStub = false;
				else
					p_objCheckStock.PrntAdjOnStub = Convert.ToBoolean(sValue);
				
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintCtlNumberOnStub").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.PrntCtlOnStub = false;
				else
					p_objCheckStock.PrntCtlOnStub = Convert.ToBoolean(sValue);
				
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrntEventOnStub").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.PrntEventOnStub = false;				
				else
					p_objCheckStock.PrntEventOnStub = Convert.ToBoolean(sValue);				

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrntEventOnStub").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.PrntEventOnStub =false;
				else
					p_objCheckStock.PrntEventOnStub = Convert.ToBoolean(sValue);
				

				//TODO 
				//Property to access PRNT_CHKDT_ON_STUB field in CHECK_STOCK table is not implemented 
				//in CheckStock class of Datamodel.
				//Following lines will be uncommented after the property is implemented.
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintCheckDateOnStub").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.PrntChkDtOnStub= false;
				else
				{
					p_objCheckStock.PrntChkDtOnStub = (Convert.ToBoolean(sValue));
				}

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "DateX").InnerText;						
				p_objCheckStock.ChkDateX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "DateY").InnerText;						
				p_objCheckStock.ChkDateY = Common.Conversion.ConvertStrToInteger(sValue);
                //asingh263 mits 32712 starts
                sValue = p_objXMLDoc.SelectSingleNode("//" + "EventDateX").InnerText;
                p_objCheckStock.ChkEventDateX = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "EventDateY").InnerText;
                p_objCheckStock.ChkEventDateY = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "ClaimDateX").InnerText;
                p_objCheckStock.ChkClaimDateX = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "ClaimDateY").InnerText;
                p_objCheckStock.ChkClaimDateY = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "PolicyNumberX").InnerText;
                p_objCheckStock.ChkPolicyX = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "PolicyNumberY").InnerText;
                p_objCheckStock.ChkPolicyY = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "InsuredNameX").InnerText;
                p_objCheckStock.ChkInsurerX = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "InsuredNameY").InnerText;
                p_objCheckStock.ChkInsurerY = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "PayeeAddressX").InnerText;
                p_objCheckStock.PayeeAddrX = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "PayeeAddressY").InnerText;
                p_objCheckStock.PayeeAddrY = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "AgentAddressX").InnerText;
                p_objCheckStock.AgentAddrX = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "AgentAddressY").InnerText;
                p_objCheckStock.AgentAddrY = Common.Conversion.ConvertStrToInteger(sValue);


                sValue = p_objXMLDoc.SelectSingleNode("//" + "PayeeAddOnCheckX").InnerText;
                p_objCheckStock.PayeeAddrOnCheckX = Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "PayeeAddOnCheckY").InnerText;
                p_objCheckStock.PayeeAddrOnCheckY = Common.Conversion.ConvertStrToInteger(sValue);

                //asingh263 mits 32712 ends

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckNumberX").InnerText;						
				p_objCheckStock.ChkNumberX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckNumberY").InnerText;						
				p_objCheckStock.ChkNumberY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckAmountX").InnerText;						
				p_objCheckStock.ChkAmountX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckAmountY").InnerText;						
				p_objCheckStock.ChkAmountY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckAmountTextX").InnerText;						
				p_objCheckStock.ChkAmountTextX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckAmountTextY").InnerText;						
				p_objCheckStock.ChkAmountTextY = Common.Conversion.ConvertStrToInteger(sValue);
				
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MemoX").InnerText;						
				p_objCheckStock.ChkMemoX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MemoY").InnerText;						
				p_objCheckStock.ChkMemoY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckDtlX").InnerText;						
				p_objCheckStock.ChkDetailX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckDtlY").InnerText;						
				p_objCheckStock.ChkDetailY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckPayerX").InnerText;						
				p_objCheckStock.ChkPayerX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckPayerY").InnerText;						
				p_objCheckStock.ChkPayerY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckPayeeX").InnerText;						
				p_objCheckStock.PayeeX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckPayeeY").InnerText;						
				p_objCheckStock.PayeeY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckOffsetX").InnerText;						
				p_objCheckStock.ChkOffsetX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckOffsetY").InnerText;						
				p_objCheckStock.ChkOffsetY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "SecondOffsetX").InnerText;						
				p_objCheckStock.SecdOffsetX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "SecondOffsetY").InnerText;						
				p_objCheckStock.SecdOffsetY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckMICRX").InnerText;						
				p_objCheckStock.ChkMicrX = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckMICRY").InnerText;						
				p_objCheckStock.ChkMicrY = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "DollarSign").InnerText;						
				if ((sValue == null) || (sValue.Trim() == ""))			
					p_objCheckStock.DolSignFlag = false;
				else					
					p_objCheckStock.DolSignFlag = Convert.ToBoolean(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintAddress").InnerText;						
				if ((sValue == null) || (sValue.Trim() == ""))			
					p_objCheckStock.PrintAddrFlag = false;					
				else
					p_objCheckStock.PrintAddrFlag = Convert.ToBoolean(sValue);
					
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintMemo").InnerText;						
				if ((sValue == null) || (sValue.Trim() == ""))	
					p_objCheckStock.PrintMemoFlag = false;
				else
					p_objCheckStock.PrintMemoFlag = Convert.ToBoolean(sValue);

                //asharma326 jira 9288
                sValue = p_objXMLDoc.SelectSingleNode("//" + "PayeeAddressFont").InnerText;
                p_objCheckStock.PayeeAddressFont = Conversion.ConvertStrToDouble(sValue);
                if ((p_objCheckStock.PayeeAddressFont < 4) || (p_objCheckStock.PayeeAddressFont > 40))
                    p_objCheckStock.PayeeAddressFont = 10;
                //asharma326 jira 9288


				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PreNumStock").InnerText;						
				if ((sValue == null) || (sValue.Trim() == ""))	
					p_objCheckStock.PreNumStock = false;
				else
					p_objCheckStock.PreNumStock = Convert.ToBoolean(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PrintInsurer").InnerText;						
				if ((sValue == null) || (sValue.Trim() == ""))	
					p_objCheckStock.PrintInsurer= false;
				else
					p_objCheckStock.PrintInsurer= Convert.ToBoolean(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MemoFontName").InnerText;						
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.MemoFontName = "Arial";
				else
					p_objCheckStock.MemoFontName = sValue;
				
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MemoFontSize").InnerText;						
				p_objCheckStock.MemoFontSize= Conversion.ConvertStrToDouble(sValue);
				if ((p_objCheckStock.MemoFontSize < 4) || (p_objCheckStock.MemoFontSize > 40))
					p_objCheckStock.MemoFontSize = 10;
				
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MemoRows").InnerText;										
				if ((sValue == null) || (sValue.Trim() == ""))					
					p_objCheckStock.MemoRows= 1;
				else
					p_objCheckStock.MemoRows= Common.Conversion.ConvertStrToInteger(sValue);
					

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "MemoRowLength").InnerText;										
				if ((sValue == null) || (sValue.Trim() == ""))					
					p_objCheckStock.MemoRowLength=30;
				else
					p_objCheckStock.MemoRowLength= Common.Conversion.ConvertStrToInteger(sValue);					

                //Ashish Ahuja - Pay To The Order Config starts
                sValue = p_objXMLDoc.SelectSingleNode("//PayToOrderFontName").InnerText;
                //if ((sValue == null) || (sValue.Trim() == ""))
                //    p_objCheckStock.PayToOrderFontName = "Arial";
                //else
                    p_objCheckStock.PayToOrderFontName = sValue;

                sValue = p_objXMLDoc.SelectSingleNode("//PayToOrderFontSize").InnerText;
                p_objCheckStock.PayToOrderFontSize = Conversion.ConvertStrToDouble(sValue);
                //if ((p_objCheckStock.PayToOrderFontSize < 4) || (p_objCheckStock.PayToOrderFontSize > 40))
                //    p_objCheckStock.PayToOrderFontSize = 10;

                sValue = p_objXMLDoc.SelectSingleNode("//PayToOrderRows").InnerText;
                if ((sValue == null) || (sValue.Trim() == ""))
                    p_objCheckStock.PayToOrderRows = 2;
                else
                    //p_objCheckStock.PayToOrderRows = Common.Conversion.ConvertStrToInteger(sValue);
                    p_objCheckStock.PayToOrderRows = Convert.ToInt32(Convert.ToDouble(sValue));

                sValue = p_objXMLDoc.SelectSingleNode("//PayToOrderRowLength").InnerText;
                //if ((sValue == null) || (sValue.Trim() == ""))
                if(string.IsNullOrEmpty(sValue))  
                    p_objCheckStock.PayToOrderRowLength = 60;
                else
                    //p_objCheckStock.PayToOrderRowLength = Common.Conversion.ConvertStrToInteger(sValue);
                p_objCheckStock.PayToOrderRowLength = Convert.ToInt32(Convert.ToDouble(sValue));

                //Ashish Ahuja - Pay To The Order Config ends
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "PayerLevel").InnerText;										
				p_objCheckStock.PayerLevel= Common.Conversion.ConvertStrToInteger(sValue);

                sValue = p_objXMLDoc.SelectSingleNode("//" + "CheckDateFormat").InnerText;
                p_objCheckStock.CheckDateFormat = Common.Conversion.ConvertStrToInteger(sValue);

				sValue = p_objXMLDoc.SelectSingleNode("//"+ "AddressFlag").InnerText;										
				p_objCheckStock.AddressFlag= Common.Conversion.ConvertStrToInteger(sValue);	
			
				//Added by Ratheen 4 August 2005

				string[] arrFormSource = { "" , "" , "" }; 
				foreach( CheckStockForms objStockFormDB in p_objCheckStock.CheckStockFormsList )
					arrFormSource[objStockFormDB.ImageIdx] = objStockFormDB.FormSource ;				
				//Delete all the soft forms for that stock id
				p_objCheckStock.CheckStockFormsList.Delete();
				p_objCheckStock.CheckStockFormsList.Refresh();
				if ((bSoftForm)&&(!m_Clone))
				{
                    string sFilePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "CheckStock");
					string sNewFile=string.Empty;
					string sFileContent=string.Empty;
					string sFileName = string.Empty;
					Random objRandom=new Random();
					int iRandom=objRandom.Next();
					bool bFileTransfer = false;

					//The collection for holding the EMFs corresponding to a check stock was not
					//implemented in CheckStock class of Datamodel.
					//Done By: Nikhil Garg		Dated: 11-Apr-2005
					XmlNodeList objXmlNodeList=null;					

					objXmlNodeList=p_objXMLDoc.SelectNodes("//CheckStockForm");
					foreach(XmlNode objXmlNode in objXmlNodeList)
					{
						if(objXmlNode.ChildNodes[3].InnerText.Trim()=="")//Check for "FormSource".
							continue;
													
						CheckStockForms objCheckStockForms = p_objCheckStock.CheckStockFormsList.AddNew();
						if(!m_New)
						{
							string sStockId =objXmlNode.SelectSingleNode("//StockId").InnerXml;
							objCheckStockForms.StockId = int.Parse(sStockId);
						}
						
						foreach(XmlNode objChildNode in objXmlNode.ChildNodes)
						{
							bFileTransfer = false;
							switch(objChildNode.Name.ToLower())
							{
								case "imageidx":
									objCheckStockForms.ImageIdx=Conversion.ConvertStrToInteger(objChildNode.InnerText);									
									break;
								case "formtype":
									objCheckStockForms.FormType=Conversion.ConvertStrToInteger(objChildNode.InnerText);
									break;
								case "formsource":
									/*
									 *Vaibhav 8 Sep 2006 :
									 * 
									 * DBVALUE represents that file content has not changed. 
									 * 
									 */
									string sImageContentFileName = objChildNode.InnerText ;
									
									if( sImageContentFileName == "DBVALUE" )
									{
										/* Vaibhav 8 Sep 2006 : Read the original values */
										objCheckStockForms.FormSource = arrFormSource[ objCheckStockForms.ImageIdx ] ;
										continue ;
									}
									else
									{
										/* Vaibhav 8 Sep 2006 : Read the temp file content */
										StreamReader objStreamReader = null ;
										try
										{
											objStreamReader = new StreamReader( sFilePath + @"\" + sImageContentFileName );
											sFileContent = objStreamReader.ReadToEnd();										
										}
										finally
										{
											if( objStreamReader != null )
												objStreamReader.Close() ;
										}
									if(sFileContent=="")
										break;
									sFileName="image_"+iRandom.ToString()+".jpg";
									DirectoryInfo sDir = new DirectoryInfo(sFilePath);
									if(!sDir.Exists)
										sDir.Create();
									sNewFile = sFilePath + "\\"+ sFileName;
                                    bFileTransfer = Utilities.FileTranfer(sFileContent, sNewFile, m_iClientId);
									if(!bFileTransfer)
										break;
									objCheckStockForms.FormSource=objCheckStockForms.ImageIdx + "|" +objZipUtility.ZipFile(sNewFile);
									FileInfo sFile=new FileInfo(sNewFile);
									sFile.Delete();
									break;
									}
							}
							
						}						
						objCheckStockForms.Dispose();
                        objCheckStockForms = null;
					}
				}
				sValue = p_objXMLDoc.SelectSingleNode("//"+ "CheckLayout").InnerText;		
				if ((sValue == null) || (sValue.Trim() == ""))
					p_objCheckStock.ChkLayout =false;
				else
					p_objCheckStock.ChkLayout = Convert.ToBoolean(sValue);

				#endregion
				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}	
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString
					("CheckStockSetup.ReadStockDetailXML.Error", m_iClientId), p_objException);
			}		
		}		

		/// Name			: InitializeDataModel
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method initialize DataModelFactory object.
		/// </summary>
		private void InitializeDataModel()
		{
			if(m_objDMF == null)			
				m_objDMF =	new DataModelFactory(m_sDatabaseName,m_sUserName,m_sPassword, m_iClientId);				
		}

		//BSB 10.18.2006 Clean up non-deterministic connection leak found at TASB
		private void CleanDataModel()
		{
			if(m_objDMF != null)			
			{
				m_objDMF.UnInitialize();
				m_objDMF.Dispose();
			}
			m_objDMF=null;	
		}

        // PJS -MITS 20011 : Added to get Unit Setting from Config file for launching new check stock
        private XmlDocument GetCheckStockUnit()
        {
            CheckStock objCheckStock = null;
            XmlElement objXMLElem = null;
            XmlDocument objOutXML = null;
            string sUnitValue = "";

            string sFormSource = string.Empty;
            try
            { 
                objOutXML = new XmlDocument();

                objXMLElem = objOutXML.CreateElement("CheckStock");
                objOutXML.AppendChild(objXMLElem);
                objXMLElem = null;
          
                //dvatsa - cloud
                sUnitValue = RMConfigurationManager.GetDictionarySectionSettings("CheckStock", m_objDMF.Context.DbConn.ConnectionString, m_iClientId)["CheckStockUnits"].ToString();
                if (sUnitValue == null)
                    sUnitValue = "";
                objXMLElem = objOutXML.CreateElement("Unit");
                if (!(sUnitValue.ToLower() == "false"))
                    objXMLElem.InnerText = "Inches";
                else
                    objXMLElem.InnerText = "Centimeters";
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objXMLElem = null;
                return objOutXML;
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString
                    ("CheckStockSetup.GetCheckStockUnit.Error", m_iClientId), p_objException);
            }

            finally
            {
                if (objCheckStock != null)
                    objCheckStock.Dispose();
                objXMLElem = null;
                objOutXML = null;

            }
            //return objReturnXML;
        }
        #endregion
        
#region IDisposable Method
        /// Name			: Dispose
		/// Author			: Neelima Dabral
		/// Date Created	: 30-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		///		This method will be called by the caller of this library to nullify class level object.		
		/// </summary>				
		public void Dispose()
		{
            if (m_objDMF != null)
            {
                m_objDMF.UnInitialize();
                m_objDMF.Dispose();
            }
                
			
		}

		#endregion
	}
}
