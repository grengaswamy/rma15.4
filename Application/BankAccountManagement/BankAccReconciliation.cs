﻿using System;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Collections;
using C1.C1PrintDocument;
using System.Globalization;
using System.Text;

using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;


namespace Riskmaster.Application.BankAccountManagement
{
	///************************************************************** 
	///* $File		: BankAccReconciliation.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 13-Sep-2005
	///* $Author	: Neelima Dabral
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class contains method related to Bank Account Reconciliation.
	/// </summary>
	
	public class BankAccReconciliation
	{
		#region Member Variables

		/// <summary>
		/// Database connection string
		/// </summary>
		string m_sConnectionString="";

        private int m_iClientId = 0;
		
		#endregion

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnectionString">Database connection string</param>
		public BankAccReconciliation(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId ;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// This method will save balance statement details for a bank account in the database.
		/// </summary>
		/// <param name="p_iAccountId">Bank Account Id</param>
		/// <param name="p_sBeginDate">Statement Begin Date</param>
		/// <param name="p_sEndDate">Statement End Date</param>
		/// <param name="p_dblBalance">Statement Balance</param>
		public void SaveNewBalanceStatementInformation(int p_iAccountId,string p_sBeginDate,string p_sEndDate,double p_dblBalance)
		{
			DbWriter objDbWriter = null;
			try
			{
				objDbWriter = DbFactory.GetDbWriter( m_sConnectionString );
				objDbWriter.Tables.Add("BANK_ACC_RECON");
                objDbWriter.Fields.Add("RECORD_ID", (long)Utilities.GetNextUID(m_sConnectionString, "BANK_ACC_RECON", m_iClientId));
				objDbWriter.Fields.Add("ACCOUNT_ID",p_iAccountId);
				objDbWriter.Fields.Add("BEGIN_DATE",p_sBeginDate);
				objDbWriter.Fields.Add("END_DATE",p_sEndDate);
				objDbWriter.Fields.Add("BALANCE",p_dblBalance);
				objDbWriter.Execute();
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.SaveNewBalanceStatementInformation.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				objDbWriter = null;
			}
		}		

		
		/// <summary>
		/// This method will read a file and mark the checks as cleared.
		/// </summary>
		/// <param name="p_iLayout">Value indicating account number length</param>
		/// <param name="p_sFileName">Name of the balance file to be processed.</param>
		/// <returns>File containing the Bank Account Reconciliation report</returns>
		public string ProcessBalanceFileFromBank(int p_iLayout,string p_sFileName)
		{
			StreamReader	objStream = null;
			string			sLine="";
			string			sAccount="";
			string			sCheckNumber="";
			string			sAmount="";
            string          sAmountBeforeDec = string.Empty;
            string          sDate = "";
			bool			bSkip=false;
			string			sAccountIds="";
			string			sSql="";
			DbReader		objReader = null;
			ArrayList		arrlstErrorInfo=null;
			ArrayList		arrlstCleared=null;
			string			sError="";
			DbReader		objReaderSumAmt = null;
			double			dblAmount=0;
			double			dblSumTotal=0;
			string			sTodaysdate="";
			double			dblTotalCleared=0;			
			long			lNumErrorRecords=0;
			long			lNumCleared=0;
            long			lNumChecks=0;    //csingh7 MITS 17530
			string		    sRptFileName="";
            string          sAmountAfterDec="0";
            int             iResult = 0;
            DbConnection    objConn = null;

			try
			{
				//The file layout is as follows:
				// Account #     1-8   length 8
				// Check Number  9-16  length 8
				// filler        17    length 1
				// check amt     18-29 length 12
				// cleared date  30-35 length 6
                //MITS :12767 Umesh
                //New file layout is as follows:
                // Account #     1-19   length 19
                // Check Number  20-29  length 10
                // filler        30    length 1
                // check amt     31-42 length 12
                // cleared date  43-48 length 6
				
				sTodaysdate = DateTime.Now.ToString("yyyyMMdd");
				try
				{
					objStream = new StreamReader(p_sFileName);				
				}
				catch(FileNotFoundException p_objException)
				{
					throw p_objException;
				}

				arrlstErrorInfo= new ArrayList();
				arrlstCleared= new ArrayList();
				while ((sLine = objStream.ReadLine()) != null)
				{
                    sAccount = GetLeftString(sLine.Trim(), 19 + p_iLayout);

                    sCheckNumber = GetSubString(sLine, 20 + p_iLayout, 10).Trim();

                    sAmount = GetSubString(sLine, 31, 12).Trim();

                    sDate = GetSubString(sLine, 43, 6).Trim();

					if (sDate.Length >= 2)
					{
						if (Val(sDate.Substring(sDate.Length-2,2)) > 50)							
							sDate = "19" +  sDate.Substring(sDate.Length-2,2) + GetLeftString(sDate,4);
						else							
							sDate = "20" +  sDate.Substring(sDate.Length-2,2) + GetLeftString(sDate,4);
					}

                    //Geeta 03/31/08 : Updated for Mits 11900
                    sAmountAfterDec = sAmount.Substring(sAmount.Length - 2, 2);                    
                   // sAmount = GetLeftString(sAmount,10) ;
                    //zmohammad 04/26/2013 MITS 25238 Fixing for the decimal character repeatition.
                    sAmountBeforeDec = GetLeftString(sAmount, sAmount.Length - 2);
                    if (sAmount.Length >= 2)
                        sAmount = sAmountBeforeDec + "." + sAmountAfterDec;
						//sAmount = sAmount + "." + sAmount.Substring(sAmount.Length-2,2);

					bSkip = false;
					sAccountIds = "";
                    sAccount = sAccount.TrimStart('0');   //rsharma220 MITS 36803
					sSql = "SELECT ACCOUNT_ID FROM ACCOUNT WHERE ACCOUNT_NUMBER='" + sAccount + "'";
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if(!objReader.Read())
					{

                        sError = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.AccountNotFound", m_iClientId));//dvatsa-cloud
                        sError = sError + " " + sCheckNumber + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.CheckAmt", m_iClientId));//dvatsa-cloud
                        sError = sError + " " + string.Format("{0:C}", Conversion.ConvertStrToDouble(sAmount)) + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ClearedDate",m_iClientId)); //csingh7 MITS 13656
                        sError = sError + " " + Conversion.GetDBDateFormat(sDate, "d") + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Acct", m_iClientId)); //dvatsa-cloud
						sError = sError + " " + sAccount;							
						arrlstErrorInfo.Add(sError);
						++lNumErrorRecords;
						bSkip = true;
					}
					else
					{
						do
						{
							if (sAccountIds == "")
								sAccountIds = Conversion.ConvertObjToStr(objReader.GetValue("ACCOUNT_ID")) + "";
							else
								sAccountIds = sAccountIds + "," + Conversion.ConvertObjToStr(objReader.GetValue("ACCOUNT_ID")) + "";                
						}while(objReader.Read());
					}
					objReader.Close();
    
					if(! bSkip)
					{
                        sSql = "SELECT CTL_NUMBER,VOID_FLAG,AMOUNT,TRANS_ID FROM FUNDS WHERE TRANS_NUMBER =" + Conversion.ConvertObjToInt64(Val(sCheckNumber), m_iClientId) + " AND ACCOUNT_ID IN (" + sAccountIds + ")";          
						objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
						if(objReader.Read())
						{
                            sSql = "SELECT SUM(AMOUNT) FROM FUNDS WHERE TRANS_NUMBER =" + Conversion.ConvertObjToInt64(Val(sCheckNumber), m_iClientId) + " AND ACCOUNT_ID IN (" + sAccountIds + ") AND VOID_FLAG = 0";
							objReaderSumAmt = DbFactory.GetDbReader(m_sConnectionString,sSql);
							if(objReaderSumAmt.Read())
                                dblSumTotal = Conversion.ConvertObjToDouble(objReaderSumAmt.GetValue(0), m_iClientId);
							objReaderSumAmt.Close();


                            if (Math.Abs(dblSumTotal - Conversion.ConvertStrToDouble(sAmount)) > 0.009) //csingh7 MITS 13656
							{

                                sError = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.AmountsDontMatch", m_iClientId));//dvatsa-cloud
                                sError = sError + " " + sCheckNumber + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Amount", m_iClientId));//dvatsa-cloud
                                sError = sError + " " + string.Format("{0:C}", Conversion.ConvertStrToDouble(sAmount)) + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Riskmaster",m_iClientId)); // csingh7 MITS 13656
								sError = sError + " " + string.Format("{0:C}",dblSumTotal);
								do
								{
                                    sError = sError + "; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ControlNumAmount", m_iClientId));//dvatsa-cloud
									sError = sError + " " + Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")) ;
                                    sError = sError + " , " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.PaymentAmount", m_iClientId));//dvatsa-cloud
                                    sError = sError + " " + string.Format("{0:C}", Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId));
								}while(objReader.Read());
                                sError = sError + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ClearedDate", m_iClientId));//dvatsa-cloud
								sError = sError + " " +  Conversion.GetDBDateFormat(sDate,"d");
                                sError = sError + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Acct", m_iClientId));//dvatsa-cloud
								sError = sError + " " + sAccount;
								arrlstErrorInfo.Add(sError);
								++lNumErrorRecords;
							}
							else
							{
								do
								{
									sError="";
                                    if (Conversion.ConvertObjToInt(objReader.GetValue("VOID_FLAG"), m_iClientId) != 0)
									{
                                        sError = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.PaymentVoidedCheckClearedBank", m_iClientId));//dvatsa-cloud
                                        sError = sError + " " + sCheckNumber + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.CheckAmt", m_iClientId));//dvatsa-cloud
                                        sError = sError + " " + string.Format("{0:C}", Conversion.ConvertStrToDouble(sAmount)) + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ClearedDate",m_iClientId)); //csingh7 MITS 13656                                         
                                        sError = sError + " " + Conversion.GetDBDateFormat(sDate, "d") + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Acct", m_iClientId));//dvatsa-cloud
                                        sError = sError + " " + sAccount + " ; " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ControlNumAmount", m_iClientId));//dvatsa-cloud
										sError = sError + " " + Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")) ;
										arrlstErrorInfo.Add(sError);
										++lNumErrorRecords;
									}
									else
									{
                                        dblAmount = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                                        sError = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.CheckClearedData", m_iClientId));//dvatsa-cloud
                                        sError = sError + " " + sCheckNumber + " , " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ControlNumAmount", m_iClientId));//dvatsa-cloud
                                        sError = sError + " " + Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")) + " , " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.CheckAmt", m_iClientId));//dvatsa-cloud
                                        sError = sError + " " + string.Format("{0:C}", Conversion.ConvertStrToDouble(sAmount)) + " , " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ClearedDate",m_iClientId)); //csingh7 MITS 13656                                        
                                        sError = sError + " " + Conversion.GetDBDateFormat(sDate, "d") + " , " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Acct", m_iClientId));//dvatsa-cloud
										sError = sError + " " + sAccount;
										arrlstCleared.Add(sError);
										++lNumCleared;
										dblTotalCleared = dblTotalCleared + dblAmount;
                                        sSql = "UPDATE FUNDS SET CLEARED_FLAG=-1, VOID_DATE='" + sTodaysdate + "'";

                                        // npadhy JIRA 10675 Starts Last Updated User information is lost in case of Bank Account Reconciliation
                                        // As discussed with Raman, this is a bug, overriding this behavior without any setting
                                        //if (!objSysSettings.DoNotOverrideUserInfoBankReconcile)
                                        //{
                                        //    sSql = sSql + " , UPDATED_BY_USER='BANKFILE',DTTM_RCD_LAST_UPD='" + DateTime.Now.ToString("yyyyMMddHHmmss") + "'";
                                        //}
                                        // npadhy JIRA 10675 Ends Last Updated User information is lost in case of Bank Account Reconciliation
                                        sSql = sSql + " WHERE TRANS_ID=" + Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
                                        //Geeta 11/29/07 Mits 10745 : Cannot Process Bank Rec. File
                                        objConn = DbFactory.GetDbConnection(m_sConnectionString);
                                        objConn.Open();
                                        iResult = objConn.ExecuteNonQuery(sSql);
                                        objConn.Close();										
									}
								}while(objReader.Read());
                                ++lNumChecks;         //csingh7 MITS 17530
							}							
						}
						else
						{
                            sError = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.CouldNotLocateCheck", m_iClientId));//dvatsa-cloud
							sError = sError + " " + sCheckNumber  + " ; " ;
                            sError = sError + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.CheckAmt",m_iClientId));
                            sError = sError + " " + string.Format("{0:C}", Conversion.ConvertStrToDouble(sAmount)) + " ; ";  //csingh7 MITS 13656                            
                            sError = sError + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.ClearedDate",m_iClientId));
							sError = sError + " " + Conversion.GetDBDateFormat(sDate,"d")  + " ; " ;
                            sError = sError + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Acct",m_iClientId));
							sError = sError + " " + sAccount ;
							arrlstErrorInfo.Add(sError);		
							++lNumErrorRecords;
						}
						objReader.Close();						
					}					
				}	
				objStream.Close();
                //sRptFileName =  PrtBalFileFromBankProcReport(arrlstErrorInfo, lNumErrorRecords, lNumCleared,p_sFileName, dblTotalCleared);  //csingh7 MITS 17530
                sRptFileName = PrtBalFileFromBankProcReport(arrlstErrorInfo, lNumErrorRecords, lNumChecks, lNumCleared, p_sFileName, dblTotalCleared);
				return sRptFileName;
			}
			
			catch(FileNotFoundException p_objException)
			{
				throw p_objException;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.ProcessBalanceFileFromBank.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				if(objStream != null)
				{
					objStream.Close();
					objStream = null;
				}
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}	
				if(objReaderSumAmt != null)
				{
					objReaderSumAmt.Dispose();
					objReaderSumAmt = null;
				}			
                if (objConn != null)
                {                    
                    objConn.Close();
                    objConn.Dispose();
                }
                
				arrlstErrorInfo=null;
				arrlstCleared=null;
			
			}
		}
		/// <summary>
		/// This method will load account balance information.
		/// </summary>
		/// <param name="p_iAccountId">Bank Account Id</param>
		/// <param name="p_iRecordId">Record Id</param>
        public XmlDocument LoadAccountBalanceInfo(int p_iAccountId, int p_iRecordId)
		{
			DbReader objReader = null;
			int iCounter=0;
            string sAccountName = string.Empty; //MITS 27587 hlv 7/25/12
			string sSQL = "";
			string sEndDate = "";
			string sBeginDate = "";
			string sBalancePerBankStatement = "";
			double dBalance = 0.00;
			double dPayAmount = 0.00;
			double dDeposit = 0.00;
			double dTotalBank = 0;
			double dTotalBooks = 0;
			double dClearedDeposits=0;;
			double dTemp = 0;
			long lNumChecks = 0;
			long lNumDeposits = 0;
			long lNumCollect = 0;
			long lNumAdjust = 0;
			double dcollections = 0.00;
			double dAdjustments = 0;
			XmlDocument objDOM=null;
			XmlElement objElem=null;
			XmlElement objChildElem=null;
			XmlElement objTempElem=null;
			int iFundsAccountBalanceDateCriteria = 0;
           
			SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
		
			try
			{
				objDOM =new XmlDocument();
				objElem = (XmlElement) objDOM.CreateNode(XmlNodeType.Element, "AccountBalanceInfo", "");

                //MITS 27587 hlv 7/25/12 begin
                objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + p_iAccountId.ToString());

                if (objReader.Read())
                {
                    sAccountName = objReader.GetString(0);
                }
                else
                {
                    sAccountName = "";
                }
                objElem.SetAttribute("BankAccountName", sAccountName);
                objReader.Close();
                //MITS 27587 hlv 7/25/12 end

                iFundsAccountBalanceDateCriteria = objSysSettings.FundsAcountBalanceDateCriteria;
				sSQL = "SELECT END_DATE,BALANCE,BEGIN_DATE FROM BANK_ACC_RECON WHERE ";
				if(p_iRecordId > 0)
					sSQL = sSQL + " RECORD_ID=" + p_iRecordId;
				else
					sSQL = sSQL + " ACCOUNT_ID=" + p_iAccountId + " ORDER BY END_DATE DESC";
				
				objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
				if(!(objReader.Read()))
				{
					objElem.SetAttribute("PriorStatement" , "false");
					if(iFundsAccountBalanceDateCriteria == 0)
					{
						sBeginDate = "N/A";
						sEndDate = "N/A";
						sBalancePerBankStatement = "$0.00";
					}
					else
					{
						sBeginDate = DateTime.Now.Year + DateTime.Now.Month + "01";
						sEndDate = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Date.ToString();
						sBalancePerBankStatement = "N/A";
					}

					objElem.SetAttribute("StatementBeginDate" , sBeginDate);
					objElem.SetAttribute("StatementEndDate" , sEndDate);
					objElem.SetAttribute("BalancePerBankStatement" , sBalancePerBankStatement);
					
					dBalance = 0;

					objElem.SetAttribute("NumDeposits" , "0");
					objElem.SetAttribute("NumChecks" , "0");
					objElem.SetAttribute("ClearedChecks" , "$0.00");
					objElem.SetAttribute("ClearedDeposits" , "$0.00");
		
					objDOM.AppendChild(objElem);
				
				}
				else
				{
					objElem.SetAttribute("PriorStatement" , "true");
					sBeginDate = objReader.GetString("BEGIN_DATE");
					sEndDate = objReader.GetString("END_DATE");
					CultureInfo us = new CultureInfo("en-US");
					dBalance = objReader.GetDouble("BALANCE");
					sBalancePerBankStatement = String.Format("{0:$0.00;($0.00)}", dBalance);
					
					objReader.Close();
								
					objElem.SetAttribute("StatementBeginDate" , Conversion.GetDBDateFormat(sBeginDate , "MM/dd/yyyy"));
					objElem.SetAttribute("StatementEndDate" , Conversion.GetDBDateFormat(sEndDate , "MM/dd/yyyy"));
					objElem.SetAttribute("BalancePerBankStatement" , sBalancePerBankStatement);
					
					sSQL = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS WHERE ACCOUNT_ID = " + p_iAccountId;
				    sSQL = sSQL + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
					sSQL = sSQL + " AND VOID_DATE >= '" + sBeginDate + "'" ;
					
					objReader = DbFactory.GetDbReader(m_sConnectionString , (sSQL + " AND DATE_OF_CHECK <= '" + sEndDate + "' AND PAYMENT_FLAG <> 0"));
					if(objReader.Read())
					{
                        lNumChecks = Conversion.ConvertObjToInt64(objReader.GetValue(1), m_iClientId);
						objElem.SetAttribute("NumChecks" , lNumChecks.ToString());
						objElem.SetAttribute("ClearedChecks" , String.Format("{0:$0.00;($0.00)}", objReader.GetDouble(0)));//"$" + objReader.GetDouble(0).ToString("N2"));
					}

					objReader.Close();
					//************* NOW DO COLLECTIONS
					if(iFundsAccountBalanceDateCriteria != 2)
					{
						objReader = DbFactory.GetDbReader(m_sConnectionString , (sSQL + " AND TRANS_DATE <= '" + sEndDate + "' AND PAYMENT_FLAG = 0"));
						if(objReader.Read())
						{
                            dcollections = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                            lNumCollect = Conversion.ConvertObjToInt64(objReader.GetValue(1), m_iClientId);							
						}
                        objReader.Close();
					}

					//now do deposits
					//MJH 0 DEPOSIT 1 ADJUSTMENT 2 MM DEPOSIT 3 MM TRANSFER 4 MM ADJUSTMENT
					//for cleared deposits go to collections also

					sSQL = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID = " + p_iAccountId.ToString();
				    sSQL = sSQL + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
				    sSQL = sSQL + " AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE <= '" + sEndDate + "'";
				    sSQL = sSQL + " AND DEPOSIT_TYPE IN (0,2) ";
					objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
		
					if(objReader.Read())
					{
                        dClearedDeposits = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId) + dcollections;
                        dTemp = Conversion.ConvertObjToInt64(objReader.GetValue(1), m_iClientId) + lNumDeposits;
					}
					objElem.SetAttribute("NumDeposits" , dTemp.ToString());
					objElem.SetAttribute("ClearedDeposits" , String.Format("{0:$0.00;($0.00)}", dClearedDeposits)); 
					objReader.Close();

					//mjh now get cleared adjustments
					sSQL = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID = " + p_iAccountId.ToString();
					sSQL = sSQL + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
				    sSQL = sSQL + " AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE <= '" + sEndDate + "'";
					sSQL = sSQL + " AND DEPOSIT_TYPE IN (1,4) ";
        
					objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
					if(objReader.Read())
					{
                        lNumAdjust = Conversion.ConvertObjToInt64(objReader.GetValue(1), m_iClientId);
						dAdjustments = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
					}
					objElem.SetAttribute("NumAdjust" , lNumAdjust.ToString());
					objElem.SetAttribute("Adjustments" , String.Format("{0:$0.00;($0.00)}", dAdjustments)); // Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId).ToString("N2"));
					objReader.Close();

					sSQL = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS WHERE ACCOUNT_ID = " + p_iAccountId.ToString();
				    sSQL = sSQL + " AND CLEARED_FLAG = 0 AND VOID_FLAG = 0"; //'mjh  AND TRANS_DATE <= '" & sEndDate & "'"
					objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL + " AND DATE_OF_CHECK <= '" + sEndDate + "' AND PAYMENT_FLAG <> 0 AND STATUS_CODE = 1054");
					if(objReader.Read())
					{
						dPayAmount = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);				
						lNumChecks = Conversion.ConvertObjToInt64(objReader.GetValue(1), m_iClientId); //mjh 11/18/98						
					}
                    objReader.Close();
					objElem.SetAttribute("OutstandingChecks" , String.Format("{0:$0.00;($0.00)}", dPayAmount));  //"$" + dPayAmount.ToString("N2"));
					objElem.SetAttribute("NumOC" , lNumChecks.ToString() + ""); 
	
					//collections are deposits
					if(iFundsAccountBalanceDateCriteria != 2)
					{
						objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL + " AND TRANS_DATE <= '" + sEndDate + "' AND PAYMENT_FLAG = 0");
						if(objReader.Read())
						{
							dcollections = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
							lNumCollect = Conversion.ConvertObjToInt64(objReader.GetValue(1), m_iClientId);							
						}
                        objReader.Close();
					}
					
					//DEPOSITs IN TRANSIT
					sSQL = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID = " + p_iAccountId;
					sSQL = sSQL + " AND VOID_FLAG = 0 AND CLEARED_FLAG = 0";
					sSQL = sSQL + " AND DEPOSIT_TYPE <> 3 AND TRANS_DATE <= '" + sEndDate + "'"; //MJH TRANSFER
					objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
					if(objReader.Read())
					{
						dDeposit = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
						lNumDeposits = Conversion.ConvertObjToInt64(objReader.GetValue(1), m_iClientId);						
					}
                    objReader.Close();

					double dDepositsInTransit = dDeposit + dcollections;
					long lNumDT = lNumDeposits + lNumCollect;

					objElem.SetAttribute("DepositsInTransit" , String.Format("{0:$0.00;($0.00)}", dDepositsInTransit));//"$" + dDepositsInTransit.ToString("N2"));
					objElem.SetAttribute("NumDT" , lNumDT.ToString());// "$" + lNumDT.ToString("N2"));
					dTotalBank = dBalance + dDeposit - dPayAmount + dcollections;
					objElem.SetAttribute("SubTotal" , String.Format("{0:$0.00;($0.00)}", dTotalBank));//"$" + dTotalBank.ToString("N2"));

					//********************************************************
					//balance per books
					//this mean everything as of the final date
					//****************************************************

					sSQL = "SELECT SUM(AMOUNT) FROM FUNDS WHERE ACCOUNT_ID = " + p_iAccountId.ToString();
					sSQL = sSQL + " AND VOID_FLAG = 0"; //mjh  AND TRANS_DATE <= '" & sEndDate & "'"
					objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL + " AND DATE_OF_CHECK <= '" + sEndDate + "' AND PAYMENT_FLAG <> 0 AND (STATUS_CODE = 1054 OR CLEARED_FLAG <> 0)");
					if(objReader.Read())
					{
						dPayAmount = objReader.GetDouble(0);						
					}
                    objReader.Close();
					//collections
					if(iFundsAccountBalanceDateCriteria != 2)
					{
						objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL + " AND TRANS_DATE <= '" + sEndDate + "' AND PAYMENT_FLAG = 0");
						if(objReader.Read())
						{
							dcollections = objReader.GetDouble(0);							
						}
                        objReader.Close();
					}
					else
					{
						dcollections = 0;
					}
					
					//DEPOSITS
					sSQL = "SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID = " + p_iAccountId.ToString();
					sSQL = sSQL + " AND VOID_FLAG = 0 AND DEPOSIT_TYPE <> 3 AND TRANS_DATE <= '" + sEndDate + "'"; //TRANSFER WITHIN SUB ACCOUNT
					objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
					if(objReader.Read())
					{
						dDeposit = objReader.GetDouble(0);						
					}
                    objReader.Close();
					dTotalBooks = dDeposit - dPayAmount + dcollections;
					objElem.SetAttribute("BalancePerBooks" , String.Format("{0:$0.00;($0.00)}", dTotalBooks));
					objElem.SetAttribute("OutOfBalance" , String.Format("{0:$0.00;($0.00)}", dTotalBank - dTotalBooks));
					
					objDOM.AppendChild(objElem);
				
				}

				//now loading various statement dates and corresponding record id's
				objElem = (XmlElement) objDOM.SelectSingleNode("//AccountBalanceInfo");
				objChildElem = (XmlElement) objDOM.CreateNode(XmlNodeType.Element , "Statements" , "");
				
				sSQL = "SELECT * FROM BANK_ACC_RECON WHERE ACCOUNT_ID = " + p_iAccountId + " ORDER BY BEGIN_DATE";
				objReader = DbFactory.GetDbReader(m_sConnectionString , sSQL);
				while(objReader.Read())
				{
					iCounter++;
					objTempElem = (XmlElement) objDOM.CreateNode(XmlNodeType.Element , "Statement" , "");
					sBeginDate = objReader.GetString("BEGIN_DATE");
					sEndDate = objReader.GetString("END_DATE");
					dBalance = objReader.GetDouble("BALANCE");
					sBalancePerBankStatement = String.Format("{0:$0.00;($0.00)}", dBalance);
					
					objTempElem.SetAttribute("StatementBeginDate" , Conversion.GetDBDateFormat(sBeginDate , "MM/dd/yyyy"));
					objTempElem.SetAttribute("StatementEndDate" , Conversion.GetDBDateFormat(sEndDate , "MM/dd/yyyy"));
					objTempElem.SetAttribute("BalancePerBankStatement" , sBalancePerBankStatement);
					objTempElem.InnerText = objReader.GetValue("RECORD_ID").ToString();

					objChildElem.AppendChild(objTempElem);

				}
                objReader.Close();
				objChildElem.SetAttribute("Count" , iCounter.ToString());
				objElem.AppendChild(objChildElem);

                //Manish multicurrency

                string sShortCode = string.Empty;
                string sDesc = string.Empty;
                string sCurrency = string.Empty;
                LocalCache objCache = null;
                objCache = new LocalCache(m_sConnectionString,m_iClientId);

                objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), ref sShortCode, ref sDesc);
                sCurrency = sDesc.Split('|')[1];
                XmlElement elem = objDOM.CreateElement("BaseCurrencyType");
                elem.InnerText = sCurrency;
                objDOM.DocumentElement.AppendChild(elem);
				
    		}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.LoadAccountBalanceInfo.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
                if (objReader != null)
                {                    
                    objReader.Dispose();
                }				
			}
			return(objDOM);
		}		

		
		/// Name			: LoadSubBankDisbursementAccount
		/// Author			: Neelima Dabral
		/// Date Created	: Jan-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will load the information for the disbursement account for a selected sub bank account.
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_iSubAccountId">Sub Bank Account Id</param>
		/// <param name="p_iRecordId">Record Id</param>
		/// <returns>XML Document containing details for the disbursement account for a selected sub bank account.</returns>
		public XmlDocument LoadSubBankDisbursementAccount(int p_iAccountId,int p_iSubAccountId,int p_iRecordId)
		{
			XmlDocument objXMLDocument=null;
			DbReader	objReader = null;
			DbReader	objAmountReader = null;
			int			iParentAccountId=0;
			XmlElement  objElem=null;
			int			iFundsAccountBalanceDateCriteria=0;
			string		sSql = "";
			double		dblPrior = 0;
			double		dblCollections = 0;
			double      dblPayments = 0;
			string		sBeginDate="";
			string		sEndDate="";
			double		dblDDSAdjustment=0;
			int			iNumChecks=0;
			int			iNumCollect=0;
			int			iNumDeposits=0;
			int			iNumAdjustments=0;
			double		dblTotalRecon = 0;
			double		dblTemp=0;
			double		dblPriorBalance=0;
			SysSettings objSysSettings = null;
			double		dblPayAmount = 0;
			int			iNumOC=0;
			double		dblDeposits=0;
			double		dblTotalBank=0;
			double		dblTotal1=0;
			XmlElement  objChildElem = null;
			int			iCounter=0;
			XmlElement  objTempElem=null;
			double		dblBalance=0;

			try
			{
				objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
				objXMLDocument = new XmlDocument();
				objElem = (XmlElement) objXMLDocument.CreateNode(XmlNodeType.Element, "DisbursementAccountInfo", "");
				if (p_iSubAccountId != 0)
					iParentAccountId = GetParentAccountID(p_iSubAccountId);
				else
					iParentAccountId = 0;
				if((p_iSubAccountId != 0)  && (iParentAccountId != 0 ))
				{
					iFundsAccountBalanceDateCriteria = objSysSettings.FundsAcountBalanceDateCriteria;
					sSql="SELECT END_DATE,BEGIN_DATE FROM BANK_ACC_RECON WHERE ";
					
					if (p_iRecordId > 0 )
						sSql = sSql + " RECORD_ID=" + p_iRecordId;
					else
						sSql = sSql + " ACCOUNT_ID=" + iParentAccountId + " ORDER BY END_DATE DESC";

					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if(!objReader.Read())
					{
						dblPrior= 0;
						if(iFundsAccountBalanceDateCriteria != 0)
						{
							#region if(iFundsAccountBalanceDateCriteria != 0)
							sBeginDate = DateTime.Now.Year + DateTime.Now.Month + "01";
							sEndDate = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Date.ToString();
							
							objElem.SetAttribute("StatementBeginDate" , sBeginDate);
							objElem.SetAttribute("StatementEndDate" , sEndDate);

							sSql = "SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
							sSql = sSql + " AND VOID_FLAG = 0 AND DATE_OF_CHECK < '" + sBeginDate + "'";
					        
							// Collections and payments
							if (iFundsAccountBalanceDateCriteria != 2 )
							{
								objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql + " AND PAYMENT_FLAG = 0");
								if (objAmountReader.Read())
                                    dblCollections = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
								objAmountReader.Close();
							}
							if (iFundsAccountBalanceDateCriteria == 2 )
								sSql = sSql + " AND STATUS_CODE=1054 ";
							objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql + " AND PAYMENT_FLAG <> 0");
							if (objAmountReader.Read())
                                dblPayments = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
							objAmountReader.Close();
							
							//Deposits  
        
							sSql = "SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
							sSql = sSql + " AND VOID_FLAG = 0 AND TRANS_DATE < '" + sBeginDate + "'";
							objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql+" AND DEPOSIT_TYPE IN (0,1)");
							if (objAmountReader.Read())
								dblPrior = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
							objAmountReader.Close();											
					        
							dblPriorBalance= dblPrior + dblCollections + dblPayments;							
							objElem.SetAttribute("PriorBalance" , string.Format("{0:C}",dblPriorBalance));	

							#endregion
						}												
					}
					else
					{
						sBeginDate = objReader.GetString("BEGIN_DATE");
						sEndDate = objReader.GetString("END_DATE");

						objElem.SetAttribute("StatementBeginDate" , Conversion.GetDBDateFormat(sBeginDate,"d") );
						objElem.SetAttribute("StatementEndDate" , Conversion.GetDBDateFormat(sEndDate,"d") );
						// Get prior balance before begin date
        
						sSql = "SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
						sSql = sSql + " AND DATE_OF_CHECK < '" + sBeginDate + "' AND VOID_FLAG = 0 AND (STATUS_CODE = 1054 OR CLEARED_FLAG <> 0)" ;

						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql + " AND PAYMENT_FLAG <> 0");
						if (objAmountReader.Read())
                            dblPrior = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
						objAmountReader.Close();


						// Do Collections
						if (iFundsAccountBalanceDateCriteria != 2 )
							sSql = "SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
						sSql = sSql + " AND TRANS_DATE < '" + sBeginDate + "' AND VOID_FLAG = 0 " ;
						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql + " AND PAYMENT_FLAG = 0");
						if (objAmountReader.Read())
                            dblPrior = dblPrior - Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
						objAmountReader.Close();
				        

						// Deposits to Disbursement Account
						sSql = "SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
						sSql = sSql + " AND TRANS_DATE < '" + sBeginDate + "' AND VOID_FLAG = 0 ";
						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql + " AND DEPOSIT_TYPE IN (0,1,3)");
						if (objAmountReader.Read())
                            dblPrior = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId) - dblPrior;
						objAmountReader.Close();		
				
						objElem.SetAttribute("PriorBalance" , string.Format("{0:C}",dblPrior));	

						if (iFundsAccountBalanceDateCriteria == 2)
						{
							sSql = "SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
							sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
							sSql = sSql + " AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'" ;
							sSql = sSql + " AND DEPOSIT_TYPE = 9 " ;
							objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
							if (objAmountReader.Read())
                                dblDDSAdjustment = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
							objAmountReader.Close();
						}
						sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
						sSql = sSql + " AND VOID_DATE >= '" + sBeginDate + "' AND DATE_OF_CHECK BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'";
						sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql + " AND PAYMENT_FLAG <> 0");
						if (objAmountReader.Read())
						{
                            dblPayments = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
                            iNumChecks = Conversion.ConvertObjToInt(objAmountReader.GetValue(1), m_iClientId);
						}
						objAmountReader.Close();

						objElem.SetAttribute("ReconcilitaionNumChecks" , iNumChecks.ToString());	
						if (iFundsAccountBalanceDateCriteria == 2) 
							dblPayments = (dblPayments - dblDDSAdjustment);

						objElem.SetAttribute("ClearedChecks" , string.Format("{0:C}",dblPayments));	

						//DO COLLECTIONS
						if (iFundsAccountBalanceDateCriteria != 2 )
						{
							sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
							sSql = sSql + " AND VOID_DATE >= '" + sBeginDate + "' AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'" ;
							sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
							objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql + " AND PAYMENT_FLAG = 0");
							if (objAmountReader.Read())
							{
                                dblCollections = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
                                iNumCollect = Conversion.ConvertObjToInt(objAmountReader.GetValue(1), m_iClientId);
							}
							objAmountReader.Close();
						}

						sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
						sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
						sSql = sSql + " AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'" ;
						sSql = sSql + " AND DEPOSIT_TYPE IN (0,3) ";
						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
						if (objAmountReader.Read())
						{
                            dblCollections = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId) + dblCollections;
                            iNumDeposits = Conversion.ConvertObjToInt(objAmountReader.GetValue(1), m_iClientId) + iNumCollect;
						}
						objAmountReader.Close();
						objElem.SetAttribute("ReconcilitaionNumDeposits" ,iNumDeposits.ToString() );	
						objElem.SetAttribute("ClearedDeposits" ,string.Format("{0:C}",dblCollections) );	
   
						sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
						sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
						sSql = sSql + " AND VOIDCLEAR_DATE >= '" + sBeginDate + "' AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'" ;
						sSql = sSql + " AND DEPOSIT_TYPE =1 " ;
						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
						if (objAmountReader.Read())
						{
                            dblTemp = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
                            iNumAdjustments = Conversion.ConvertObjToInt(objAmountReader.GetValue(1), m_iClientId);
						}
						objAmountReader.Close();
						objElem.SetAttribute("Adjustments" ,string.Format("{0:C}",dblTemp) );
						objElem.SetAttribute("ReconcilitaionNumAdjustments" ,iNumAdjustments.ToString() );	
						objAmountReader.Close();

						dblTotalRecon = dblCollections - dblPayments + dblTemp;
						objElem.SetAttribute("ReconciledItem" ,string.Format("{0:C}",dblTotalRecon) );
					}					
					objReader.Close();
					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
					sSql = sSql + " AND CLEARED_FLAG = 0 AND VOID_FLAG = 0 AND DATE_OF_CHECK BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'" ;
					objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql+" AND PAYMENT_FLAG <> 0 AND STATUS_CODE = 1054");
					if (objAmountReader.Read())
					{
                        dblPayAmount = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
                        iNumOC = Conversion.ConvertObjToInt(objAmountReader.GetValue(1), m_iClientId);
					}
					objAmountReader.Close();
					if (iFundsAccountBalanceDateCriteria == 2 )
						dblPayAmount = dblPayAmount + dblDDSAdjustment;
					objElem.SetAttribute("NoOfChecks" ,iNumOC.ToString() );
					objElem.SetAttribute("OutstandingChecks" ,string.Format("{0:C}",dblPayAmount) );
					
					if (iFundsAccountBalanceDateCriteria != 2 )
					{
						sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
						sSql = sSql + " AND CLEARED_FLAG = 0 AND VOID_FLAG = 0 AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'";
						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql+" AND PAYMENT_FLAG = 0");
						if (objAmountReader.Read())
						{
                            dblCollections = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
                            iNumCollect = Conversion.ConvertObjToInt(objAmountReader.GetValue(1), m_iClientId);
						}
						objAmountReader.Close();
					}
					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;			
					sSql = sSql + " AND VOID_FLAG = 0 AND CLEARED_FLAG = 0 AND TRANS_DATE BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'";
					sSql = sSql + " AND DEPOSIT_TYPE IN (0,1,3)" ;
					objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objAmountReader.Read())
					{
                        dblDeposits = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);
                        iNumDeposits = Conversion.ConvertObjToInt(objAmountReader.GetValue(1), m_iClientId);
					}
					objAmountReader.Close();
					objElem.SetAttribute("TransitDeposits" ,string.Format("{0:C}",dblDeposits+dblCollections) );
					objElem.SetAttribute("NumOfDeposits",(iNumDeposits+iNumCollect).ToString());

					dblTotalBank = dblPrior + dblDeposits - dblPayAmount + dblCollections + dblTotalRecon;
					objElem.SetAttribute("SubTotal" ,string.Format("{0:C}",dblTotalBank) );
				
					sSql = "SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
					sSql = sSql + " AND VOID_FLAG = 0 AND DATE_OF_CHECK <= '" + sEndDate + "'";
					objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql+" AND PAYMENT_FLAG <> 0 AND (STATUS_CODE = 1054 OR CLEARED_FLAG <> 0)");
					if (objAmountReader.Read())
                        dblPayAmount = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);							
					objAmountReader.Close();

					if (iFundsAccountBalanceDateCriteria != 2)
					{
						sSql = "SELECT SUM(AMOUNT) FROM FUNDS WHERE SUB_ACCOUNT_ID = " + p_iSubAccountId;
						sSql = sSql + " AND VOID_FLAG = 0 AND TRANS_DATE <= '" + sEndDate + "'";
						objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql+" AND PAYMENT_FLAG = 0");
						if (objAmountReader.Read())
                            dblCollections = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);													
						objAmountReader.Close();
					}

					sSql = "SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND VOID_FLAG = 0 AND TRANS_DATE <= '" + sEndDate + "'";
					objAmountReader = DbFactory.GetDbReader(m_sConnectionString,sSql+ " AND DEPOSIT_TYPE IN (0,1,3)");
					if (objAmountReader.Read())
                        dblDeposits = Conversion.ConvertObjToDouble(objAmountReader.GetValue(0), m_iClientId);													
					objAmountReader.Close();
    
					dblTotal1 = dblDeposits - dblPayAmount + dblCollections;

					objElem.SetAttribute("CurrentBalance" ,string.Format("{0:C}",dblTotal1) );					      
					objElem.SetAttribute("OutOfBalance" ,string.Format("{0:C}",dblTotalBank - dblTotal1) );					      												

					objXMLDocument.AppendChild(objElem);

					//now loading various statement dates and corresponding record id's
					objElem = (XmlElement) objXMLDocument.SelectSingleNode("//DisbursementAccountInfo");
					objChildElem = (XmlElement) objXMLDocument.CreateNode(XmlNodeType.Element , "Statements" , "");
				
					sSql = "SELECT * FROM BANK_ACC_RECON WHERE ACCOUNT_ID = " + p_iAccountId + " ORDER BY BEGIN_DATE";
					objReader = DbFactory.GetDbReader(m_sConnectionString , sSql);
					while(objReader.Read())
					{
						iCounter++;
						objTempElem = (XmlElement) objXMLDocument.CreateNode(XmlNodeType.Element , "Statement" , "");
						sBeginDate = Conversion.ConvertObjToStr(objReader.GetValue("BEGIN_DATE"));
						sEndDate = Conversion.ConvertObjToStr(objReader.GetValue("END_DATE"));
                        dblBalance = Conversion.ConvertObjToDouble(objReader.GetValue("BALANCE"), m_iClientId);
						
						objTempElem.SetAttribute("StatementBeginDate" , Conversion.GetDBDateFormat(sBeginDate,"d"));
						objTempElem.SetAttribute("StatementEndDate" ,Conversion.GetDBDateFormat(sEndDate,"d"));
						objTempElem.SetAttribute("BalancePerBankStatement" , string.Format("{0:C}",dblBalance));
						objTempElem.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("RECORD_ID")) ;

						objChildElem.AppendChild(objTempElem);

					}
					objChildElem.SetAttribute("Count" , iCounter.ToString());
					objElem.AppendChild(objChildElem);
                    objReader.Close();

				} 				

				
				return objXMLDocument;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.LoadSubBankDisbursementAccount.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				objXMLDocument=null;
				objSysSettings = null;

				if (objReader != null)
				{
					objReader.Dispose();
					objReader=null;
				}
				if (objAmountReader != null)
				{
					objAmountReader.Dispose();
					objAmountReader=null;
				}
				
				objElem=null;
				objChildElem = null;				
				objTempElem=null;
			}
		}


		/// Name			: LoadMoneyMarketAccountBalance
		/// Author			: Neelima Dabral
		/// Date Created	: Jan-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will load the information for the money market account for a selected sub bank account.
		/// </summary>		
		/// <param name="p_iSubAccountId">Sub Bank Account Id</param>
		/// <param name="p_iMoneyMktAcc">Value of Money Market Account (0/1)</param>
		/// <param name="p_sStartDate">Selected Start Date</param>		
		/// <param name="p_sEndDate">Selected End Date</param>
		/// <returns>XML Document containing details for the disbursement account for a selected sub bank account.</returns>
		public XmlDocument LoadMoneyMarketAccountBalance(int p_iSubAccountId,int p_iMoneyMktAcc,string p_sStartDate,string p_sEndDate)
		{
			XmlDocument objXMLDocument = null;
			XmlElement  objElem =null;
			string		sSql="";
			DbReader	objReader = null;
			double		dblDeposit=0;
			double		dblTransfer=0;
			double		dblPrior=0;
			double		dblAdjust=0;
			int			iCount=0;
			double		dblTotalRecon=0;
			double		dblTotalBank=0;
			double		dblTemp=0;
			double		dblTotal=0;
			try
			{
				objXMLDocument = new XmlDocument();
				objElem = (XmlElement) objXMLDocument.CreateNode(XmlNodeType.Element, "MoneyMarketAccountBalanceInfo", "");

				if(p_iMoneyMktAcc ==0)
				{
					objElem.SetAttribute("Current" , string.Format("{0:C}",0));	
					objElem.SetAttribute("OutOfBalance" , string.Format("{0:C}",0));	
					objElem.SetAttribute("RecAdjustmentAmt" , string.Format("{0:C}",0));	
					objElem.SetAttribute("RecDepositAmt" , string.Format("{0:C}",0));	
					objElem.SetAttribute("RecTransferAmt" , string.Format("{0:C}",0));	
					objElem.SetAttribute("RecAdjustmentNum" , "0");	
					objElem.SetAttribute("RecDepositNum" , "0");	
					objElem.SetAttribute("RecTransferNum" , "0");	
					objElem.SetAttribute("NoOfTrans" , "0");	
					objElem.SetAttribute("StartDate" ,  "N/A");	
					objElem.SetAttribute("EndDate" ,  "N/A");
					objElem.SetAttribute("NoOfDeposit" , "0");	
					objElem.SetAttribute("PriorBalance" , string.Format("{0:C}",0));	
					objElem.SetAttribute("TransitDeposit" , string.Format("{0:C}",0));	
					objElem.SetAttribute("TransferNotCleared" , string.Format("{0:C}",0));	
					objElem.SetAttribute("SubTotal" , string.Format("{0:C}",0));	
					objElem.SetAttribute("ReconciledItems" , string.Format("{0:C}",0));	           
					objXMLDocument.AppendChild(objElem);
				}
				else
				{

					objElem.SetAttribute("StartDate" , Conversion.GetDBDateFormat(p_sStartDate,"d"));	
					objElem.SetAttribute("EndDate" ,  Conversion.GetDBDateFormat(p_sEndDate,"d"));

					sSql = "SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
					sSql = sSql + " AND TRANS_DATE <= '" + p_sStartDate + "'";
					sSql = sSql + " AND DEPOSIT_TYPE IN (2,4)"  ;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objReader.Read())
                        dblDeposit = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);					
					objReader.Close();


					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
					sSql = sSql + " AND TRANS_DATE <= '" + p_sStartDate + "'";
					sSql = sSql + " AND DEPOSIT_TYPE =3 " ;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objReader.Read())
                        dblTransfer = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);					
					objReader.Close();

					dblPrior = dblDeposit - dblTransfer;					
					objElem.SetAttribute("PriorBalance" , string.Format("{0:C}",dblPrior));	

					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
					sSql = sSql + " AND VOIDCLEAR_DATE >= '" + p_sStartDate + "' AND TRANS_DATE BETWEEN '" + p_sStartDate + "' AND '" + p_sEndDate + "'" ;
					sSql = sSql + " AND DEPOSIT_TYPE = 4  " ;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objReader.Read())
					{
                        dblAdjust = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        iCount = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);					
					}
					objReader.Close();

					objElem.SetAttribute("RecAdjustmentNum" , iCount.ToString());	
					objElem.SetAttribute("RecAdjustmentAmt" , string.Format("{0:C}",dblAdjust));
					dblDeposit=0;

					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
					sSql = sSql + " AND VOIDCLEAR_DATE >= '" + p_sStartDate + "' AND TRANS_DATE BETWEEN '" + p_sStartDate + "' AND '" + p_sEndDate + "'";
					sSql = sSql + " AND DEPOSIT_TYPE = 2  " ;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objReader.Read())
					{
                        dblDeposit = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        iCount = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);					
					}
					objReader.Close();
					objElem.SetAttribute("RecDepositNum" , iCount.ToString());	
					objElem.SetAttribute("RecDepositAmt" , string.Format("{0:C}",dblDeposit));

					dblTransfer=0;
					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND CLEARED_FLAG <> 0 AND VOID_FLAG = 0";
					sSql = sSql + " AND VOIDCLEAR_DATE >= '" + p_sStartDate + "' AND TRANS_DATE BETWEEN '" + p_sStartDate + "' AND '" + p_sEndDate + "'" ;
					sSql = sSql + " AND DEPOSIT_TYPE =3 " ;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objReader.Read())
					{
                        dblTransfer = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        iCount = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);					
					}
					objReader.Close();

					objElem.SetAttribute("RecTransferNum" , iCount.ToString());	
					objElem.SetAttribute("RecTransferAmt" , string.Format("{0:C}",dblTransfer));
					dblTotalRecon = dblDeposit + dblAdjust - dblTransfer;
					objElem.SetAttribute("ReconciledItems" , string.Format("{0:C}",dblTotalRecon));	    
					dblDeposit=0;

					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND VOID_FLAG = 0 AND CLEARED_FLAG = 0 AND TRANS_DATE <= '" + p_sEndDate + "'";
					sSql = sSql + " AND DEPOSIT_TYPE IN (2,4)" ;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objReader.Read())
					{
                        dblDeposit = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        iCount = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);					
					}
					objReader.Close();

					objElem.SetAttribute("NoOfDeposit" , iCount.ToString());	
					objElem.SetAttribute("TransitDeposit" , string.Format("{0:C}",dblDeposit));					
					
					dblTransfer=0;
					sSql = "SELECT SUM(AMOUNT),COUNT(*) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND VOID_FLAG = 0 AND CLEARED_FLAG = 0 AND TRANS_DATE <= '" + p_sEndDate + "'";
					sSql = sSql + " AND DEPOSIT_TYPE =3 " ;
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					if (objReader.Read())
					{
                        dblTransfer = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                        iCount = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);					
					}
					objReader.Close();

					objElem.SetAttribute("NoOfTrans" , iCount.ToString());	
					objElem.SetAttribute("TransferNotCleared" , string.Format("{0:C}",dblTransfer));			
					dblTotalBank = dblPrior + dblDeposit - dblTransfer + dblTotalRecon;
					objElem.SetAttribute("SubTotal" , string.Format("{0:C}",dblTotalBank));			
   
					sSql = "SELECT SUM(AMOUNT) FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId;
					sSql = sSql + " AND VOID_FLAG = 0 AND TRANS_DATE <= '" + p_sEndDate + "'";
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql+" AND DEPOSIT_TYPE IN (2,4)");
					if (objReader.Read())
                        dblTemp = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);															
					objReader.Close();

					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql+" AND DEPOSIT_TYPE = 3");
					if (objReader.Read())
                        dblTotal = dblTemp - Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);															
					objReader.Close();

					objElem.SetAttribute("Current" , string.Format("{0:C}",dblTotal));			
					objElem.SetAttribute("OutOfBalance" , string.Format("{0:C}",dblTotalBank - dblTotal));			

					objXMLDocument.AppendChild(objElem);				   
				}
				return objXMLDocument;
			}	
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.LoadMoneyMarketAccountBalance.Error", m_iClientId), p_objException);//dvatsa-cloud
			}	
			finally
			{
				objXMLDocument = null;
				objElem =null;
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
			}

		}


		/// Name			: GetDeposits
		/// Author			: Neelima Dabral
		/// Date Created	: Feb-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will load the deposits for the selected bank account.
		/// </summary>				
		/// <param name="p_iAccountId">Bank Account Id</param>
		/// <param name="p_iSubAccountId">Bank Sub-Account Id</param>
		/// <returns>XML Document containing deposits for the selected bank account.</returns>
		public XmlDocument GetDeposits(int p_iSubAccountId,int p_iAccountId)
		{
			SysSettings objSysSettings = null;
			bool		bUseFundsSubAcc=false;
			string		sSql="";
			DbReader	objReader = null;
			XmlDocument objOutXML=null;
			XmlElement  objXMLElem=null;
			int			iDepositType=0;
			string		sDepositType="";

			try
			{
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//dvatsa-cloud
				bUseFundsSubAcc =objSysSettings.UseFundsSubAcc;

				if (bUseFundsSubAcc)
					sSql = "SELECT * FROM FUNDS_DEPOSIT WHERE SUB_ACC_ID = " + p_iSubAccountId + " AND CLEARED_FLAG = 0 AND VOID_FLAG = 0";
				else
					sSql = "SELECT * FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID = " + p_iAccountId + " AND CLEARED_FLAG = 0 AND VOID_FLAG = 0";
				
				objOutXML = new XmlDocument();	
				objXMLElem=objOutXML.CreateElement("Deposits");
				objXMLElem.SetAttribute("AccountName",GetAccountName(p_iAccountId));													
				objOutXML.AppendChild(objXMLElem);

				if (p_iAccountId != 0)
				{
					objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
					while(objReader.Read())
					{
						objXMLElem = objOutXML.CreateElement("Deposit");
						objXMLElem.SetAttribute("ControlNo",Common.Conversion.ConvertObjToStr(objReader.GetValue("CTL_NUMBER")));					
						objXMLElem.SetAttribute("TransDate",Common.Conversion.GetDBDateFormat(Common.Conversion.ConvertObjToStr(objReader.GetValue("TRANS_DATE")),"d"));					
						objXMLElem.SetAttribute("Amount",string.Format("{0:C}",objReader.GetValue("AMOUNT")));
                        iDepositType = Common.Conversion.ConvertObjToInt(objReader.GetValue("DEPOSIT_TYPE"), m_iClientId);					
						switch(iDepositType)
						{
							case 0 :
								sDepositType ="Deposit";
								break;
							case 1 :
								sDepositType ="Adjustment";
								break;
							case 2 :
								sDepositType ="Money Market Deposit";
								break;
							case 3 :
								sDepositType ="Money Market Transfer";
								break;
							case 4 :
								sDepositType ="Money Market Adjustment";
								break;
						}
						objXMLElem.SetAttribute("DepositType",sDepositType);
						objXMLElem.SetAttribute("DepositId",Common.Conversion.ConvertObjToStr(objReader.GetValue("DEPOSIT_ID")));

						objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
					}
					objReader.Close();   
				}
				return objOutXML;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("BankAccReconciliation.GetDeposits.Error",m_iClientId),p_objException);//dvatsa-cloud
			}	
			finally
			{
				objSysSettings=null;
				if (objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
				objOutXML=null;
				objXMLElem=null;
			}

		}


		/// Name			: ClearDeposits
		/// Author			: Neelima Dabral
		/// Date Created	: Feb-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************	
		/// <summary>
		/// This method will clear multiple deposits.
		/// </summary>			
		public void ClearDeposits(string p_sDepositId,string p_sUserLoginName)
		{
			string			sSql="";
			DbConnection	objConn = null;
			int				iResult=0;
			try
			{
				if((p_sDepositId != null) && p_sDepositId.Trim() != "")
				{
					sSql = "UPDATE FUNDS_DEPOSIT SET CLEARED_FLAG = -1,VOIDCLEAR_DATE = '" + Common.Conversion.GetDate(DateTime.Now.ToString()) + "',DTTM_RCD_LAST_UPD = '" + Common.Conversion.GetDateTime(DateTime.Now.ToString()) + "'," ;
					sSql = sSql + "UPDATE_BY_USER = '" + p_sUserLoginName + "' WHERE DEPOSIT_ID in (" + p_sDepositId + ")";

					objConn = DbFactory.GetDbConnection(m_sConnectionString);
					objConn.Open();
					iResult = objConn.ExecuteNonQuery(sSql);					
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("BankAccReconciliation.ClearDeposits.Error",m_iClientId),p_objException);//dvatsa-cloud
			}	
			finally
			{				
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}	

			}

		}
				
		

		#endregion

		#region Private Methods

		/// <summary>
		/// This method will return the numeric part from the start of input string.
		/// If the string does not start with a number or does not have numeric
		/// value at all then this method returns 0.
		/// </summary>
		/// <param name="p_sInput">Input string</param>
		/// <returns>integer</returns>
		private int Val(string p_sInput)
		{
			int iReturnValue = 0;
			int iCnt = 0;
			string sTemp = "";
			try
			{

				if(p_sInput == null || p_sInput == "")
					return 0;

				for(iCnt=0;iCnt<p_sInput.Length;iCnt++)
				{
					if(Conversion.IsNumeric(p_sInput.Substring(iCnt,1)))
						sTemp = sTemp + p_sInput.Substring(iCnt,1);
					else
					{
						//If the first character is non-numeric then -1 is returned.
						if(iCnt == 0)
							sTemp = "0";
						//exit
						break;
					}
				}//end for
				iReturnValue = Conversion.ConvertStrToInteger(sTemp);
				return iReturnValue;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}	
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.Val.Err", m_iClientId), p_objException);//dvatsa-cloud
			}			
		}


		/// <summary>
		/// This method will generate the bank account reconciliation audit report.
		/// </summary>
		/// <param name="p_arrlstErrorInfo">List containing the error records found while processing</param>
		/// <param name="p_lNumErrorRecords">Number of error records</param>
		/// <param name="p_lNumChecks">Number of checks that are cleared as a result of processing including Roll up checks</param>
		/// <param name="p_lNumCleared">Number of checks that are cleared as a result of processing</param>
		/// <param name="p_sFileName">File that is being processed</param>
		/// <param name="p_dblTotalCleared">Total amount of the checks that gets cleared.</param>
		/// <returns>File containing the Bank Account Reconciliation report</returns>
		//private string PrtBalFileFromBankProcReport(ArrayList p_arrlstErrorInfo, long p_lNumErrorRecords,long p_lNumCleared,string p_sFileName, double p_dblTotalCleared)
        private string PrtBalFileFromBankProcReport(ArrayList p_arrlstErrorInfo, long p_lNumErrorRecords,long p_lNumChecks,long p_lNumCleared,string p_sFileName, double p_dblTotalCleared)
		{
			C1PrintDocument objReport = new C1PrintDocument();			
			string			sFileName="";			
			RenderTable		objHeaderTable=null;
			RenderTable		objFooterTable=null;			
			LocalCache      objCache = null;
			string			sCompanyName="";
			RenderText		objText=null;	
			RenderTable		objTable=null;
			int				iRowcounter =0;			
			try
			{
				objCache = new LocalCache(m_sConnectionString,m_iClientId);
				objCache.GetSysInfo("CLIENT_NAME",ref sCompanyName);				

				objReport.DefaultUnit = UnitTypeEnum.Twip ;
				objReport.PageSettings.Margins.Top = 20 ;
				objReport.PageSettings.Margins.Left = 20 ;
				objReport.PageSettings.Margins.Bottom = 20 ;
				objReport.PageSettings.Margins.Right =20 ;					
				objReport.PageSettings.Landscape = false ;					
				
				objReport.NewPageStarted += new NewPageStartedEventHandler( NewPageStarted );
				
				objReport.StartDoc();			
			

				#region Setting Page Header

				objHeaderTable = new RenderTable( objReport );	
				objHeaderTable.Style.Borders.AllEmpty = false;
				objHeaderTable.StyleTableCell.BorderTableHorz.Empty = true;
				objHeaderTable.StyleTableCell.BorderTableVert.Empty = true;				

				objHeaderTable.Columns.AddSome(3);
				objHeaderTable.Body.Rows.AddSome(1);

				objHeaderTable.Columns[0].StyleTableCell.TextAlignHorz = AlignHorzEnum.Left;
				objHeaderTable.Columns[0].WidthStr = "40%";
				objHeaderTable.Body.Cell( 0 , 0).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
				objHeaderTable.Columns[1].StyleTableCell.TextAlignHorz = AlignHorzEnum.Left;
				objHeaderTable.Columns[1].WidthStr = "50%";
				objHeaderTable.Body.Cell( 0 , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
				objHeaderTable.Columns[2].StyleTableCell.TextAlignHorz = AlignHorzEnum.Right;
				objHeaderTable.Columns[2].WidthStr = "10%";				
				objHeaderTable.Body.Cell( 0 , 2).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );

				// assign texts to the 3 parts of the header			
				objHeaderTable.Body.Cell(0, 0).StyleTableCell.BackColor = Color.LightGray;
				objHeaderTable.Body.Cell(0, 1).StyleTableCell.BackColor = Color.LightGray;
				objHeaderTable.Body.Cell(0, 2).StyleTableCell.BackColor = Color.LightGray;
                objHeaderTable.Body.Cell(0, 0).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.BankAuditReport", m_iClientId));//dvatsa-cloud
				objHeaderTable.Body.Cell(0, 1).RenderText.Text = sCompanyName;
				objHeaderTable.Body.Cell(0, 2).RenderText.Text = System.DateTime.Now.ToShortDateString();

				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
				objReport.PageHeader.RenderObject=objHeaderTable;

				objHeaderTable = null;
				#endregion

				#region	Setting Page Footer

				objFooterTable = new RenderTable( objReport );	
				objFooterTable.Style.Borders.AllEmpty = false;
				objFooterTable.StyleTableCell.BorderTableHorz.Empty = true;
				objFooterTable.StyleTableCell.BorderTableVert.Empty = true;				

				objFooterTable.Columns.AddSome(3);
				objFooterTable.Body.Rows.AddSome(1);

				objFooterTable.Columns[0].StyleTableCell.TextAlignHorz = AlignHorzEnum.Left;
				objFooterTable.Columns[0].WidthStr = "20%";
				objFooterTable.Body.Cell( 0 , 0).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
				objFooterTable.Columns[1].StyleTableCell.TextAlignHorz = AlignHorzEnum.Center;
				objFooterTable.Columns[1].WidthStr = "55%";
				objFooterTable.Body.Cell( 0 , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
				objFooterTable.Columns[2].StyleTableCell.TextAlignHorz = AlignHorzEnum.Left;
				objFooterTable.Columns[2].WidthStr = "25%";				
				objFooterTable.Body.Cell( 0 , 2).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );

				// assign texts to the 3 parts of the header
				objFooterTable.Body.Cell(0, 0).StyleTableCell.BackColor = Color.LightGray;
				objFooterTable.Body.Cell(0, 1).StyleTableCell.BackColor = Color.LightGray;
				objFooterTable.Body.Cell(0, 2).StyleTableCell.BackColor = Color.LightGray;
				objFooterTable.Body.Cell(0, 0).RenderText.Text = " ";
				objFooterTable.Body.Cell(0, 1).RenderText.Text = "Page [@@PageNo@@]";
				objFooterTable.Body.Cell(0, 2).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.ConfidentialData",m_iClientId))
                    + " - " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.RiskMaster", m_iClientId));	//dvatsa-cloud

				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
				objReport.PageFooter.RenderObject=objFooterTable;

				#endregion				

				#region Printing the audit report				
		
				objText = new RenderText( objReport );

				objText.Text=" ";
				objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold);
				objText.Style.WordWrap = false ;
				objText.AutoWidth = true ;	
				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
				objReport.RenderBlock( objText );

                objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.ProcessingDetailInformation", m_iClientId));//dvatsa-cloud
				objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Underline);
				objText.Style.WordWrap = false ;
				objText.AutoWidth = true ;	
				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
				objReport.RenderBlock( objText );

				objTable = new RenderTable( objReport );				
				objTable.Style.Borders.AllEmpty=true;
				objTable.Body.StyleTableCell.BorderTableHorz.Empty= true ;
				objTable.Body.StyleTableCell.BorderTableVert.Empty= true ;								
				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
			
				objTable.Columns.AddSome(3);	
				objTable.Columns[0].WidthStr = "1%";
				objTable.Columns[1].WidthStr = "98%";
				objTable.Columns[2].WidthStr = "1%";

				if(p_lNumCleared == 0)
				{
					objTable.Body.Rows.AddSome(1);										
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.ZeroChecksCleared", m_iClientId));//dvatsa-cloud
					++iRowcounter;
					
					objTable.Body.Rows.AddSome(1);										
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.TotalRecordsInError", m_iClientId)) + " " + Convert.ToString(p_lNumErrorRecords);//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);										
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage((Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.TotalRecordsProcessed", m_iClientId)) + " " + Convert.ToString(p_lNumCleared + p_lNumErrorRecords));//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);					
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage((Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.Date/timeProcessed", m_iClientId) + " " + DateTime.Now.ToString("G")));//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);										
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.FileProcessed", m_iClientId)) + " " + p_sFileName;//dvatsa-cloud
					++iRowcounter;		

				}
				else
				{
					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );																							
					//objTable.Body.Cell( iRowcounter , 1 ).RenderText.Text = p_lNumCleared + " " + Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.XChecksCleared"); csingh7    MITS 17530
                    if (p_lNumChecks == 1)  // Modified by csingh7    MITS 17530
                        objTable.Body.Cell(iRowcounter, 1).RenderText.Text = p_lNumChecks + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.OneCheckCleared", m_iClientId));//dvatsa-cloud
                    else
                        objTable.Body.Cell(iRowcounter, 1).RenderText.Text = p_lNumChecks + " " + CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.XChecksCleared", m_iClientId));//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.TotalAmountCleared", m_iClientId)) + " " + string.Format("{0:C}", p_dblTotalCleared);//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.TotalRecordsInError", m_iClientId)) + " " + Convert.ToString(p_lNumErrorRecords);//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.TotalRecordsProcessed", m_iClientId)) + " " + Convert.ToString(p_lNumCleared + p_lNumErrorRecords);//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.Date/timeProcessed", m_iClientId) + " " + DateTime.Now.ToString("G"));//dvatsa-cloud
					++iRowcounter;

					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.FileProcessed", m_iClientId)) + " " + p_sFileName;//dvatsa-cloud
					++iRowcounter;
				}	
				

				objReport.RenderBlock(objTable); 

				objText.Text=" ";
				objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold);
				objText.Style.WordWrap = false ;
				objText.AutoWidth = true ;	
				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
				objReport.RenderBlock( objText );

                objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.ErrorRecordsInformation", m_iClientId));//dvatsa-cloud
				objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold|FontStyle.Underline);
				objText.Style.WordWrap = false ;
				objText.AutoWidth = true ;	
				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
				objReport.RenderBlock( objText );

				objText.Text=" ";
				objText.Style.Font = new Font("Arial" , 12,FontStyle.Bold);
				objText.Style.WordWrap = false ;
				objText.AutoWidth = true ;	
				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;
				objReport.RenderBlock( objText );
				
				iRowcounter=0;
				objTable.Dispose();
				objTable = new RenderTable( objReport );		
			
				objTable.Style.Borders.AllEmpty=true;
				objTable.Body.StyleTableCell.BorderTableHorz.Empty= true ;
				objTable.Body.StyleTableCell.BorderTableVert.Empty= true ;								
				objReport.Style.AlignChildrenHorz = AlignHorzEnum.Center ;

				objTable.Columns.AddSome(3);								
				objTable.Columns[0].WidthStr = "1%";
				objTable.Columns[1].WidthStr = "98%";
				objTable.Columns[2].WidthStr = "1%";
				
				if (p_lNumErrorRecords == 0)
				{
					objTable.Body.Rows.AddSome(1);
					objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );
                    objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.ProcessCompletedNoErrors", m_iClientId));//dvatsa-cloud
					++iRowcounter;					
				}
				else
				{
					for(int iCounter=0;iCounter <= p_lNumErrorRecords -1;++iCounter)
					{
						objTable.Body.Rows.AddSome(1);
						objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );																		
						objTable.Body.Rows[iRowcounter].CanSplit=true;							
						objTable.Body.Cell( iRowcounter , 1 ).RenderText.Text = Convert.ToString(p_arrlstErrorInfo[iCounter]); 																
						++iRowcounter;						
					}											
				}

				#endregion
				
				#region Printing Copyright

				objTable.Body.Rows.AddSome(1);
				objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Regular );																							
				objTable.Body.Cell( iRowcounter , 1 ).RenderText.Text = " ";
				iRowcounter++;

				objTable.Body.Rows.AddSome(1);
				objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold|FontStyle.Underline );
                objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.Note", m_iClientId));//dvatsa-cloud
				iRowcounter++;

				objTable.Body.Rows.AddSome(1);
				objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.ProprietaryConfidentialData", m_iClientId));//dvatsa-cloud
				iRowcounter++;

				objTable.Body.Rows.AddSome(1);
				objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.RiskMasterReg", m_iClientId));//dvatsa-cloud
				iRowcounter++;

				objTable.Body.Rows.AddSome(1);
				objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.Copyright", m_iClientId));//dvatsa-cloud
				iRowcounter++;

				objTable.Body.Rows.AddSome(1);
				objTable.Body.Cell( iRowcounter , 1).StyleTableCell.Font= new Font( "Arial" , 9,FontStyle.Bold );
                objTable.Body.Cell(iRowcounter, 1).RenderText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.AllRightsReservedWorldwide", m_iClientId));//dvatsa-cloud
				
				iRowcounter++;			
				#endregion

				objReport.RenderBlock(objTable ); 
				objReport.EndDoc();				
				SaveToFile(objReport,ref sFileName);
				return sFileName;
				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}	
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.PrtBalFileFromBankProcReport.Err", m_iClientId), p_objException);//dvatsa-cloud
			}		
			finally
			{
				
				#region releasing objects
				
				if(objReport != null)
				{ 
					objReport.Dispose();
					objReport = null;
				}
				if(objCache != null)
				{ 
					objCache.Dispose();
					objCache = null;
				}
				if(objHeaderTable != null)
				{ 
					objHeaderTable.Dispose();
					objHeaderTable = null;
				}
				if(objFooterTable != null)
				{ 
					objFooterTable.Dispose();
					objFooterTable = null;
				}	
				if(objText != null)
				{ 
					objText.Dispose();
					objText = null;
				}	
				if(objTable != null)
				{ 
					objTable.Dispose();
					objTable = null;
				}	
				#endregion
			}
		}


		/// <summary>
		/// This method will print the page border whenever a new page is added in the report.
		/// </summary>
		/// <param name="p_objReport">Bank Account Reconciliation Audit Report</param>
		/// <param name="arg">Event Arguments</param>
		private void NewPageStarted( C1PrintDocument p_objReport , NewPageStartedEventArgs arg )
		{
			p_objReport.RenderDirectLine(0,0,0,p_objReport.PrintableAreaSize.Height - p_objReport.PageFooter.SizeInfo.ActualContentHeight);
			p_objReport.RenderDirectLine(p_objReport.PrintableAreaSize.Width,0,p_objReport.PrintableAreaSize.Width,p_objReport.PrintableAreaSize.Height - p_objReport.PageFooter.SizeInfo.ActualContentHeight);
		}


		/// <summary>
		/// This method will write the contents of the report to a file.
		/// </summary>
		/// <param name="p_objPrintDoc">Bank Account Reconciliation Audit Report</param>
		/// <param name="p_sFileName">File Name</param>
		private void SaveToFile(C1PrintDocument p_objPrintDoc,ref string p_sFileName)
		{
            string sStoragePath = string.Empty;

            sStoragePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "BankAccountReconRpt");

            p_sFileName = String.Format(@"{0}\{1}", sStoragePath, GetUniqueFileName());
			p_objPrintDoc.ExportToPDF(p_sFileName,false);
		}


		/// <summary>
		/// This method will generate the unique name for the file containing Bank Account Reconciliation Audit Report.
		/// </summary>
		/// <returns>Unique File Name</returns>
		private string GetUniqueFileName()
		{
			string sTempPath = "" ;			
			try
			{
				sTempPath = "BankAccRecAuditRpt" + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
					+ System.AppDomain.GetCurrentThreadId() + ".PDF" ; 			
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.GetUniqueFileName.Error", m_iClientId), p_objEx);//dvatsa-cloud				
			}
			return sTempPath ;
		}		


		/// <summary>
		/// This method will return the specified number of characters from a string starting from Left.
		/// </summary>
		/// <param name="p_sInputStr">Input String</param>
		/// <param name="p_iNoOfChars">Number of Characters to be read from the left of the string</param>
		/// <returns>String</returns>
		private string GetLeftString(string p_sInputStr, int p_iNoOfChars)
		{
			int iStrLength =0;
			try
			{
				iStrLength = p_sInputStr.Length;
				if(iStrLength < p_iNoOfChars)
					p_sInputStr = p_sInputStr.Substring(0,iStrLength);
				else
					p_sInputStr = p_sInputStr.Substring(0,p_iNoOfChars);
				return (p_sInputStr);
			}			
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.GetLeftString.Error", m_iClientId), p_objException);//dvatsa-cloud
			}				
		}

		
		/// <summary>
		/// This method will return the specified number of characters from a string starting from specified location.
		/// </summary>
		/// <param name="p_sInputStr">Input String</param>
		/// <param name="p_iStartPos">Start Position</param>
		/// <param name="p_iNoOfChars">Number of Characters to be read from the left of the string</param>
		/// <returns>string</returns>
		private string GetSubString(string p_sInputStr, int p_iStartPos, int p_iNoOfChars)
		{
			int iStrLength =0;
			try
			{
				iStrLength = p_sInputStr.Length;
				if(iStrLength < p_iStartPos)
					p_sInputStr = "";
				else 
				{
					if(p_sInputStr.Substring(p_iStartPos-1).Length < p_iNoOfChars)
						p_sInputStr = p_sInputStr.Substring(p_iStartPos-1);
					else
						p_sInputStr = p_sInputStr.Substring(p_iStartPos-1,p_iNoOfChars);
				}
				return (p_sInputStr);
			}					
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BankAccReconciliation.GetSubString.Error", m_iClientId), p_objException);//dvatsa-cloud
			}				
		}

		/// <summary>
		/// This method returns the parent account id for the selected sub account.
		/// </summary>
		/// <param name="p_iAccountId">Sub Account Id</param>		
		/// <returns>Parent Account Id</returns>
		private int GetParentAccountID(int p_iAccountId)
		{
			string			sSQL=string.Empty;
			DbReader		objReader=null;
			int				iAccountId=0;			
			try
			{
				sSQL = "SELECT ACCOUNT_ID FROM BANK_ACC_SUB WHERE SUB_ROW_ID=" + p_iAccountId;
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
				if (objReader.Read())
					iAccountId=Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), m_iClientId);				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(String.Format(Globalization.GetString("BankAccReconciliation.GetParentAccountID.Error", m_iClientId), p_iAccountId.ToString()), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
				
			}
			return iAccountId;
		}


		/// <summary>
		/// This method returns the account name for a bank account id.
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>		
		/// <returns>Bank Account Name</returns>		
		private string GetAccountName(int p_iAccountId)
		{
			string			sSQL=string.Empty;
			DbReader		objReader=null;
			string			sAccountName="";
			try
			{
				sSQL = "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID=" + p_iAccountId;
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
				if (objReader.Read())
					sAccountName=Conversion.ConvertObjToStr(objReader.GetValue("ACCOUNT_NAME"));
				else
					sAccountName="not known";

			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(String.Format(Globalization.GetString("BankAccReconciliation.GetAccountName.Error", m_iClientId), p_iAccountId.ToString()), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objReader != null)
				{
					objReader.Dispose();
					objReader = null;
				}
			
			}
			return sAccountName;
		}
	
		#endregion
		

	}
}
