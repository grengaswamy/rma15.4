﻿namespace Riskmaster.Application.FROINet6
{
    using System.Linq;
    using System.Xml.Linq;
    using Riskmaster.Application.ExtenderLib;
    using Riskmaster.BusinessAdaptor.Common;
    using Riskmaster.Db;
    using Riskmaster.Security;

    /// <summary>
    /// Preparer Info Class.
    /// </summary>
    public class PreparerInfo : BusinessAdaptorBase
    {
        #region Private Members

        /// <summary>
        /// The m_e source
        /// </summary>
        private ePreparerSource m_eSource;

        private int m_iClientId = 0;
        #endregion

        /// <summary>
        /// ePreparerSource
        /// </summary>
        public enum ePreparerSource
        {
            /// <summary>
            /// The none
            /// </summary>
            None = 2,
            /// <summary>
            /// The registry
            /// </summary>
            Registry = 4,
            /// <summary>
            /// The user XML
            /// </summary>
            UserXML = 8,
            /// <summary>
            /// The system XML
            /// </summary>
            SystemXML = 12
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public ePreparerSource Source
        {
            get { return m_eSource; }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        /// <value>
        /// The phone.
        /// </value>
        public string Phone { get; set; }

        /// <summary>
        /// Gets a value indicating whether [skip prompt].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [skip prompt]; otherwise, <c>false</c>.
        /// </value>
        public bool SkipPrompt { get; set; }

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PreparerInfo"/> class.
        /// </summary>
        /// <param name="userLogin">The user login.</param>
        /// <param name="p_iClientId">cloud clientId</param>
        public PreparerInfo(UserLogin userLogin, int p_iClientId)
            : base()
        {
            base.SetSecurityInfo(userLogin, p_iClientId);
            this.m_eSource = ePreparerSource.None;
            this.LoadPreparerInfoXML();
            this.m_iClientId = p_iClientId;
        }
        #endregion

        /// <summary>
        /// Skips the juris preparer prompt.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <returns></returns>
        public bool SkipJurisPreparerPrompt(ref int lClaimId)
        {
            return SkipPreparerPrompt(lClaimId, true);
        }

        /// <summary>
        /// Skips the froi preparer prompt.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <returns></returns>
        public bool SkipFroiPreparerPrompt(ref int lClaimId)
        {
            return SkipPreparerPrompt(lClaimId, false);
        }

        /// <summary>
        /// Loads the preparer information XML.
        /// </summary>
        /// <returns></returns>
        private int LoadPreparerInfoXML()
        {
            int functionReturnValue = default(int);
            bool success = default(bool);
            DbReader objReader = null;
            string sUSER_ID = string.Empty;
            string sPREF_XML = string.Empty;

            this.Name = string.Empty;
            this.Title = string.Empty;
            this.Phone = string.Empty;
            this.SkipPrompt = false;

            try
            {
                string sSQL = string.Format("SELECT USER_ID, PREF_XML FROM USER_PREF_XML WHERE USER_ID = {0} ORDER BY USER_ID DESC", this.userLogin.UserId);

                using (objReader = DbFactory.GetDbReader(this.connectionString, sSQL))
                {
                    if (!object.ReferenceEquals(objReader, null) && objReader.Read())
                    {
                        sUSER_ID = objReader.GetValue("USER_ID").ToString();
                        sPREF_XML = objReader.GetString("PREF_XML");
                    }
                    objReader.Close();
                }

                m_eSource = sUSER_ID.Equals("-1") ? ePreparerSource.SystemXML : ePreparerSource.UserXML;

                if (string.IsNullOrEmpty(sPREF_XML))
                {
                    return 0;
                }

                XDocument xDocument = XDocument.Parse(sPREF_XML);

                if (object.ReferenceEquals(xDocument, null))
                {
                    return 0;
                }

                XElement node = xDocument.Descendants("FROIOptions").FirstOrDefault();

                if (object.ReferenceEquals(node, null))
                {
                    return 0;
                }

                // Pick out Values
                if (!object.ReferenceEquals(node.Attribute("phone"), null))
                {
                    this.Phone = node.Attribute("phone").Value;
                }

                if (!object.ReferenceEquals(node.Attribute("name"), null))
                {
                    this.Name = node.Attribute("name").Value;
                }

                if (!object.ReferenceEquals(node.Attribute("title"), null))
                {
                    this.Title = node.Attribute("title").Value;
                }

                if (!object.ReferenceEquals(node.Attribute("skipprompt"), null))
                {
                    this.SkipPrompt = Common.Conversion.CastToType<bool>(node.Attribute("skipprompt").Value, out success);
                }

                functionReturnValue = 1;
            }
            catch
            {
                functionReturnValue = 0;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Stores the preparer information XML.
        /// </summary>
        /// <returns></returns>
        private int StorePreparerInfoXML()
        {
            int functionReturnValue = 0;
            string sSql = string.Empty;
            string sXML = string.Empty;
            XDocument xDocument = null;
            XElement rootElement = null;
            try
            {
                sSql = string.Format("SELECT * FROM USER_PREF_XML WHERE USER_ID = {0}", this.userLogin.UserId);
                using (DbReader objDbReader = DbFactory.GetDbReader(this.connectionString, sSql))
                {
                    if (objDbReader != null && objDbReader.Read())
                    {
                        sXML = objDbReader.GetString("PREF_XML");
                    }
                }

                //****************** Set up\Modify the xmldoc ********************************
                if (string.IsNullOrEmpty(sXML))
                {
                    rootElement = new XElement("setting");
                }
                else
                {
                    xDocument = XDocument.Parse(sXML);
                    rootElement = xDocument.Root;
                }

                XElement nodeFROIOptions = new XElement("FROIOptions"
                    , new XAttribute("name", this.Name)
                    , new XAttribute("title", this.Title)
                    , new XAttribute("phone", this.Phone)
                    , new XAttribute("skipprompt", this.SkipPrompt.ToString())
                    );
                if (rootElement.Element("FROIOptions") == null)
                {
                    rootElement.Add(nodeFROIOptions);
                }
                else
                {
                    rootElement.Element("FROIOptions").ReplaceWith(nodeFROIOptions);
                }

                //****************** End Set up\Modify the xmldoc ********************************
                DbWriter objWriter = DbFactory.GetDbWriter(this.connectionString);
                objWriter.Tables.Add("USER_PREF_XML");
                objWriter.Fields.Add("USER_ID", this.userLogin.UserId);
                objWriter.Fields.Add("PREF_XML", rootElement.ToString());
                objWriter.Where.Add(string.Format("USER_ID = {0}", this.userLogin.UserId));
                objWriter.Execute();
                objWriter = null;
                functionReturnValue = 1;
            }
            catch
            {
                functionReturnValue = 0;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Skips the preparer prompt.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <param name="bIsJuris">if set to <c>true</c> [b is juris].</param>
        /// <returns></returns>
        private bool SkipPreparerPrompt(int lClaimId, bool bIsJuris)
        {
            bool functionReturnValue = false;
            Populater objPopulater = default(Populater);
            int lPreparerSourceSetting = 0;
            bool bRet = false;

            bRet = true;

            //Fetch Froi Source Setting
            objPopulater = new Populater(this.userLogin.LoginName, this.userLogin.Password, this.userLogin.objRiskmasterDatabase.DataSourceName, this.m_iClientId);

            lPreparerSourceSetting = bIsJuris ? objPopulater.GetJURISPreparerSource(lClaimId) : objPopulater.GetFROIPreparerSource(lClaimId);

            objPopulater = null;

            //Decide about the Prompt...
            //Current User Setting selected on Froi Options Screen
            if (lPreparerSourceSetting == 1)
            {
                //Stored User Preference
                if (!this.SkipPrompt)
                {
                    if (this.Source == ePreparerSource.UserXML || this.Source == ePreparerSource.Registry)
                        bRet = false;
                    //Where did we get this info?
                }
            }

            functionReturnValue = bRet;
            return functionReturnValue;
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        internal void Save()
        {
            StorePreparerInfoXML();
        }
    }
}