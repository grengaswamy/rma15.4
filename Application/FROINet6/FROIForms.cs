﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Xml;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.Application.ExtenderLib;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.FormProcessor;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;
using DBConn = Riskmaster.Db.DbConnection;
using DBFactory = Riskmaster.Db.DbFactory;
using DBReader = Riskmaster.Db.DbReader;
using DBCommand = Riskmaster.Db.DbCommand;
using FormProcessors = Riskmaster.Application.FormProcessor.FormProcessor;
using Conversions = Riskmaster.Common.Conversion;


namespace Riskmaster.Application.FROINet6
{
    /// <summary>
    /// FROI forms class.
    /// </summary>
    public class FROIForms
    {
        #region Private Varaibles

        /// <summary>
        /// The m_ output path
        /// </summary>
        private string m_OutputPath;

        /// <summary>
        /// The M_L storage identifier
        /// </summary>
        private int m_lStorageId;

        /// <summary>
        /// The preparer information object.
        /// </summary>
        private PreparerInfo m_objPreparerInfo;

        /// <summary>
        /// The m_ insured identifier
        /// </summary>
        private int m_InsuredID;

        /// <summary>
        /// The m_ document path
        /// </summary>
        private string m_DocumentPath;

        /// <summary>
        /// The m_ document storage type
        /// </summary>
        private StorageType m_DocStorageType;

        /// <summary>
        /// The m_ froi print type
        /// </summary>
        private string m_FroiPrintType;

        /// <summary>
        /// The m_ attach form
        /// </summary>
        private bool m_AttachForm;

        /// <summary>
        /// The connection string
        /// </summary>
        private string ConnectionString = string.Empty;

        /// <summary>
        /// The m_obj DMF
        /// </summary>
        private DataModelFactory m_objDmf = null;

        /// <summary>
        /// The fun
        /// </summary>
        Functions fun = new Functions();

        /// <summary>
        /// The user login
        /// </summary>
        private UserLogin userLogin;
        
        private int m_iClientid = 0;//sharishkumar Jira 827
        #endregion Private Varaibles

        #region Enumerators
        /// <summary>
        /// Froi fail enums.
        /// </summary>
        public enum FROIFails
        {
            /// <summary>
            /// The f f_ no error
            /// </summary>
            FF_NoError = 0,

            /// <summary>
            /// The f f_ ncci body parts
            /// </summary>
            FF_NCCIBodyParts = 2,

            /// <summary>
            /// The f f_ ncci cause code
            /// </summary>
            FF_NCCICauseCode = 4,

            /// <summary>
            /// The f f_ ncci class code
            /// </summary>
            FF_NCCIClassCode = 8,

            /// <summary>
            /// The f f_ ncci illness
            /// </summary>
            FF_NCCIIllness = 16,

            /// <summary>
            /// The f f_ no department assigned
            /// </summary>
            FF_NoDepartmentAssigned = 32,

            /// <summary>
            /// The f f_ no claimant
            /// </summary>
            FF_NoClaimant = 64,

            /// <summary>
            /// The f f_ load froi options failed
            /// </summary>
            FF_LoadFROIOptionsFailed = 128,

            /// <summary>
            /// The f f_ non numeric class code
            /// </summary>
            FF_NonNumericClassCode = 256,

            /// <summary>
            /// The f f_ incomplete missing class code
            /// </summary>
            FF_IncompleteMissingClassCode = 512
        }

        /// <summary>
        /// Supported form types.
        /// </summary>
        private enum FormType
        {
            /// <summary>
            /// The froi form
            /// </summary>
            FROIForm = 1,

            /// <summary>
            /// The claim form
            /// </summary>
            ClaimForm = 2,

            /// <summary>
            /// The wc claim form
            /// </summary>
            WCClaimForm = 3,

            /// <summary>
            /// The event form
            /// </summary>
            EventForm = 4,

            /// <summary>
            /// The person involved form
            /// </summary>
            PersonInvolvedForm = 5,

            /// <summary>
            /// The acord form
            /// </summary>
            AcordForm = 6,

            /// <summary>
            /// The ad forms
            /// </summary>
            ADForms = 7
        }

        # endregion Enumerators

        #region Properties

        /// <summary>
        /// Gets or sets the storage identifier.
        /// </summary>
        /// <value>
        /// The storage identifier.
        /// </value>
        public int StorageId { get; set; }

        /// <summary>
        /// Gets the object preparer information.
        /// </summary>
        /// <value>
        /// The object preparer information.
        /// </value>
        public PreparerInfo objPreparerInfo
        {
            get
            {
                if (m_objPreparerInfo == null)
                {
                    m_objPreparerInfo = new PreparerInfo(this.userLogin, m_iClientid);
                }

                return m_objPreparerInfo;
            }
        }

        /// <summary>
        /// Gets or sets the type of the document storage.
        /// </summary>
        /// <value>
        /// The type of the document storage.
        /// </value>
        public short DocStorageType
        {
            get { return Convert.ToInt16(m_DocStorageType); }
            set { m_DocStorageType = (StorageType)value; }
        }

        /// <summary>
        /// Gets or sets the PDF URL.
        /// </summary>
        /// <value>
        /// The PDF URL.
        /// </value>
        public string PDFUrl { get; set; }

        /// <summary>
        /// Gets or sets the PDF path.
        /// </summary>
        /// <value>
        /// The PDF path.
        /// </value>
        public string PDFPath { get; set; }

        /// <summary>
        /// Gets or sets the DSN.
        /// </summary>
        /// <value>
        /// The DSN.
        /// </value>
        public string DSN { get; set; }

        /// <summary>
        /// Gets or sets the name of the login.
        /// </summary>
        /// <value>
        /// The name of the login.
        /// </value>
        public string LoginName { get; set; }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        /// <value>
        /// The output path.
        /// </value>
        public string OutputPath
        {
            get { return m_OutputPath; }
            set
            {
                m_OutputPath = value;
                if (!string.IsNullOrEmpty(m_OutputPath) && m_OutputPath.Substring(m_OutputPath.Length - 1) != "\\")
                {
                    m_OutputPath = m_OutputPath + "\\";
                }
            }
        }

        /// <summary>
        /// Gets or sets the document path.
        /// </summary>
        /// <value>
        /// The document path.
        /// </value>
        public string DocumentPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [attach form].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [attach form]; otherwise, <c>false</c>.
        /// </value>
        public bool AttachForm
        {
            get { return m_AttachForm; }
            set { m_AttachForm = value; }
        }

        /// <summary>
        /// Gets or sets the type of the froi print.
        /// </summary>
        /// <value>
        /// The type of the froi print.
        /// </value>
        public string FroiPrintType
        {
            get { return m_FroiPrintType; }
            set { m_FroiPrintType = value; }
        }

        /// <summary>
        /// Sets the login identifier.
        /// </summary>
        /// <value>
        /// The login identifier.
        /// </value>
        public int LoginId
        {
            set { fun.g_LoginId = value; }
        }

        /// <summary>
        /// Gets a value indicating whether [is initialized].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is initialized]; otherwise, <c>false</c>.
        /// </value>
        public bool IsInitialized
        {
            get { return (!string.IsNullOrEmpty(this.DSN) & fun.g_LoginId != 0 & !string.IsNullOrEmpty(fun.g_LoginName)); }
        }

        /// <summary>
        /// Gets or sets the sel insured identifier.
        /// </summary>
        /// <value>
        /// The sel insured identifier.
        /// </value>
        public int SelInsuredID
        {
            get { return m_InsuredID; }
            set { m_InsuredID = value; }
        }
        
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes a new instance of the <see cref="FROIForms"/> class.
        /// </summary>
        /// <param name="userLogin">The user login.</param>
        public FROIForms(UserLogin userLogin,int p_iClientId)//sharishkumar Jira 827
        {
            m_iClientid = p_iClientId;//sharishkumar Jira 827
            this.userLogin = userLogin;
            this.m_objDmf = new DataModel.DataModelFactory(this.userLogin.objRiskmasterDatabase.DataSourceName, this.userLogin.LoginName, this.userLogin.Password, m_iClientid);//sharishkumar Jira 827
            this.ConnectionString = m_objDmf.Context.DbConn.ConnectionString;
            this.AttachForm = true;
        }

        /// <summary>
        /// Sets the preparer information.
        /// </summary>
        /// <param name="sName">Name of the s.</param>
        /// <param name="sTitle">The s title.</param>
        /// <param name="sPhone">The s phone.</param>
        public void SetPreparerInfo(string sName, ref string sTitle, ref string sPhone)
        {
            //if (!this.IsInitialized)
                // Err().Raise(3000, "FROINET6.FROIForms.SetPreparerInfo()", "Component not fully initialized. Check that you have set the DSN, UserId and UserLoginName properties before calling this function.");
            //fun.OpenDatabase();

            if (m_objPreparerInfo == null)
            {
                m_objPreparerInfo = new PreparerInfo(this.userLogin, m_iClientid);//sharishkumar Jira 827
            }

            m_objPreparerInfo.Name = sName;
            m_objPreparerInfo.Phone = sPhone;
            m_objPreparerInfo.Title = sTitle;
            m_objPreparerInfo.Save();
        }

        /// <summary>
        /// Gets the acord forms.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <returns></returns>
        public object GetAcordForms(int lClaimId)
        {
            int claimTypeCode = 0;
            int lStateID = 0;
            int iCounter = 0;
            string category = string.Empty;
            string sSQL = null;
            var formMappings = FROIForms.MakeList(new { FormId = string.Empty, Category = string.Empty, FormTitle = string.Empty });
            object[,] vRet = new string[3, formMappings.Count];

            try
            {
                using (Claim objClaim = (Claim)m_objDmf.GetDataModelObject("Claim", false))
                {
                    if (lClaimId > 0)
                    {
                        objClaim.MoveTo(lClaimId);
                        claimTypeCode = objClaim.ClaimTypeCode;
                    }
                }

                sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID= 'AC'";

                using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sSQL))
                {
                    if (!object.ReferenceEquals(objReader, null) && objReader.Read())
                    {
                        lStateID = objReader.GetInt("STATE_ROW_ID");
                    }
                }

                if (lStateID > 0 && claimTypeCode > 0)
                {
                    sSQL = string.Format(@"SELECT FORM_CATEGORY, FORM_ID, FORM_TITLE
                                            FROM CL_FORMS, CL_FORMS_CAT_LKUP, CLAIM_ACCORD_MAPPI 
                                            WHERE (CL_FORMS.FORM_CATEGORY = CL_FORMS_CAT_LKUP.FORM_CAT) 
                                            AND (CL_FORMS.STATE_ROW_ID = CL_FORMS_CAT_LKUP.STATE_ROW_ID) 
                                            AND (CL_FORMS.STATE_ROW_ID = {0}) 
                                            AND (CL_FORMS.FORM_ID = CLAIM_ACCORD_MAPPI.ACORD_FORM_ID)
                                            AND (CLAIM_ACCORD_MAPPI.CLAIM_TYPE_CODE = {1}) 
                                            ORDER BY CL_FORMS_CAT_LKUP.FORM_CAT_DESC, CL_FORMS.FORM_NAME", lStateID, claimTypeCode);

                    using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sSQL))
                    {
                        if (!object.ReferenceEquals(objReader, null))
                        {
                            while (objReader.Read())
                            {
                                category = Conversion.ConvertObjToStr(objReader.GetValue("FORM_CATEGORY"));

                                if (formMappings.Exists(x => x.Category.Equals(category)))
                                {
                                    category = string.Empty;
                                }

                                formMappings.Add(new
                                {
                                    FormId = objReader.GetInt("FORM_ID").ToString(),
                                    Category = category,
                                    FormTitle = objReader.GetString("FORM_TITLE")
                                });
                            }
                        }
                    }
                }
                vRet = new string[3, formMappings.Count];
                foreach (var form in formMappings)
                {
                    vRet[0, iCounter] = form.FormId;
                    vRet[1, iCounter] = form.Category;
                    vRet[2, iCounter] = form.FormTitle;
                    iCounter++;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.GetAcordForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {

            }
            return vRet;
        }

        /// <summary>
        /// Gets the wc forms.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <returns></returns>
        public object GetWCForms(int lClaimId)
        {
            int lStateID = 0;
            string[,] vRet = null;
            int iCounter = 0;
            var formMappings = FROIForms.MakeList(new { Category = string.Empty, FormId = string.Empty, FormNameAndTitle = string.Empty });
            string category = string.Empty;
            string sSQL = string.Empty;

            try
            {
                using (Claim objClaim = (Claim)m_objDmf.GetDataModelObject("Claim", false))
                {
                    if (lClaimId > 0)
                    {
                        objClaim.MoveTo(lClaimId);
                        lStateID = objClaim.FilingStateId;
                    }
                }

                if (lStateID != 0)
                {
                    sSQL = string.Format(@"SELECT WCP_FORMS_CAT_LKUP.FORM_CAT_DESC as FORMCATDESC, WCP_FORMS.FORM_ID as FORMID,
            WCP_FORMS.FORM_NAME as FORMNAME, WCP_FORMS.FORM_TITLE as FORMTITLE From WCP_FORMS, WCP_FORMS_CAT_LKUP
            WHERE (WCP_FORMS.FORM_CATEGORY = WCP_FORMS_CAT_LKUP.FORM_CAT) AND (WCP_FORMS.STATE_ROW_ID = WCP_FORMS_CAT_LKUP.STATE_ROW_ID) 
            AND (WCP_FORMS.STATE_ROW_ID = {0}) AND (WCP_FORMS.PRIMARY_FORM_FLAG = -1) ORDER BY WCP_FORMS_CAT_LKUP.FORM_CAT_DESC, 
            WCP_FORMS.FORM_NAME , WCP_FORMS.FORM_TITLE", lStateID);

                    using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sSQL))
                    {
                        if (!object.ReferenceEquals(objReader, null))
                        {
                            while (objReader.Read())
                            {
                                category = objReader.GetString("FORMCATDESC");

                                if (formMappings.Exists(x => x.Category.Equals(category)))
                                {
                                    category = string.Empty;
                                }

                                formMappings.Add(new
                                {
                                    Category = category,
                                    FormId = objReader.GetInt("FORMID").ToString(),
                                    FormNameAndTitle = string.Format("{0}:{1}", objReader.GetString("FORMNAME"), objReader.GetString("FORMTITLE"))
                                });
                            }
                        }
                    }
                }

                vRet = new string[3, formMappings.Count];
                foreach (var form in formMappings)
                {
                    vRet[0, iCounter] = form.Category;
                    vRet[1, iCounter] = form.FormId;
                    vRet[2, iCounter] = form.FormNameAndTitle;
                    iCounter++;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.GetWCForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
            }
            return vRet;
        }

        /// <summary>
        /// Gets the froi forms.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <returns></returns>
        public object GetFROIForms(int lClaimId)
        {
            string[,] vRet = null;
            int iCounter = 0;
            Claim objClaim = null;
            int filingStateId = default(int);
            var formMappings = FROIForms.MakeList(new { FormId = string.Empty, FormName = string.Empty });

            try
            {
                using (objClaim = (Claim)m_objDmf.GetDataModelObject("Claim", false))
                {
                    if (lClaimId > 0)
                    {
                        objClaim.MoveTo(lClaimId);
                        filingStateId = objClaim.FilingStateId;
                    }
                }

                if (filingStateId != default(int))
                {
                    string sSQL = string.Format("SELECT FORM_ID, FORM_NAME FROM JURIS_FORMS WHERE STATE_ROW_ID = {0} ORDER BY FORM_ID", filingStateId);

                    using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            formMappings.Add(new
                            {
                                FormId = objReader.GetValue("FORM_ID").ToString(),
                                FormName = objReader.GetString("FORM_NAME")
                            });
                        }
                    }
                }

                vRet = new string[2, formMappings.Count];
                foreach (var form in formMappings)
                {
                    vRet[0, iCounter] = form.FormId;
                    vRet[1, iCounter] = form.FormName;
                    iCounter++;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.GetFROIForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                
            }
            return vRet;
        }

        /// <summary>
        /// Gets the claim forms.
        /// </summary>
        /// <param name="p_iClaimId">The p_i claim identifier.</param>
        /// <returns></returns>
        /// <exception cref="Riskmaster.ExceptionTypes.RMAppException">
        /// </exception>
        public object GetClaimForms(int p_iClaimId)
        {
            string sbSQL = null;
            var formMappings = FROIForms.MakeList(new { FormId = string.Empty, FormNameAndTitle = string.Empty });
            int iStateId = 0;
            string[,] vRet = null;
            int iCounter = 0;
            try
            {
                using (Claim objClaim = this.m_objDmf.GetDataModelObject("Claim", false) as Claim)
                {
                    if (p_iClaimId > 0)
                    {
                        objClaim.MoveTo(p_iClaimId);

                        using (Event objEvent = objClaim.Parent as Event)
                        {
                            iStateId = objEvent.StateId;
                        }
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("FROIManager.InvalidClaimId", m_iClientid));//sharishkumar Jira 827
                    }
                }

                sbSQL = string.Format(" SELECT FORM_ID, FORM_NAME, FORM_TITLE FROM CL_FORMS WHERE CL_FORMS.STATE_ROW_ID = {0} ORDER BY CL_FORMS.FORM_NAME", iStateId);

                using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            formMappings.Add(new
                            {
                                FormId = objReader.GetInt("FORMID").ToString(),
                                FormNameAndTitle = string.Format("{0}:{1}", objReader.GetString("FORMNAME"), objReader.GetString("FORMTITLE"))
                            });
                        }
                    }
                }

                vRet = new string[3, formMappings.Count];
                foreach (var form in formMappings)
                {
                    vRet[0, iCounter] = form.FormId;
                    vRet[1, iCounter] = form.FormNameAndTitle;
                    iCounter++;
                }

                return vRet;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.GetClaimsForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
            }
        }

        /// <summary>
        /// Saves the wc forms.
        /// </summary>
        /// <param name="sFDFFilename">The s FDF filename.</param>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <param name="lStorageId">The l storage identifier.</param>
        /// <returns></returns>
        public bool SaveWCForms(string sFDFFilename, int lClaimId, ref int lStorageId)
        {
            m_lStorageId = lStorageId;
            string sPDFFileName = string.Empty;
            string sClaimNumber = string.Empty;
            AttachOutput(ref sPDFFileName, ref sFDFFilename, ref  sClaimNumber, ref  lClaimId);
            return true;
        }

        /// <summary>
        /// Attaches the output.
        /// </summary>
        /// <param name="sPDFFileName">Name of the s PDF file.</param>
        /// <param name="sFDFFilename">The s FDF filename.</param>
        /// <param name="sClaimNumber">The s claim number.</param>
        /// <param name="lClaimId">The l claim identifier.</param>
        public void AttachOutput( ref string sPDFFileName, ref string sFDFFilename,ref  string sClaimNumber,ref  int lClaimId)
        {
            string sDocFile = string.Empty;
            DocumentManager objNewDoc = null;
            DocumentManager objDocumentManager = null;

            try
            {
                objDocumentManager = new DocumentManager(userLogin, m_iClientid);//sharishkumar Jira 827
                objDocumentManager.ConnectionString = ConnectionString;
                objDocumentManager.UserLoginName = userLogin.LoginName;

                if (!string.IsNullOrEmpty(this.DocumentPath))
                {
                    sDocFile = sClaimNumber + sPDFFileName.Replace(".pdf", ".fdf");

                    if (m_DocStorageType == StorageType.FileSystemStorage)
                    {
                        if (this.DocumentPath.Substring(this.DocumentPath.Length - 1) != "\\")
                        {
                            this.DocumentPath = this.DocumentPath + "\\";
                        }

                        sDocFile = this.DocumentPath + sDocFile;
                        if (File.Exists(sDocFile))
                        {
                            sDocFile = fun.GetUniqueFileName(sDocFile);
                            File.Copy(sFDFFilename, sDocFile);
                        }

                        objDocumentManager.DestinationStoragePath = this.DocumentPath;
                    }
                    else
                    {
                        sDocFile = sFDFFilename;
                        objDocumentManager.DestinationStoragePath = fun.GetDocPath(sFDFFilename);
                        objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;//nanda Legacy Migration
                        objDocumentManager.ConnectionString = this.ConnectionString;

                        //default behavior of .AddDocument is to delete the file after streaming into blob
                        //-- ABhateja 05/24/2006
                        //-- Update the BLOB stored at this STORAGE_ID
                        objDocumentManager.StorageId = m_lStorageId;
                    }

                    //Commented by Nanda ,need to analysis atlast.

                    //Get the DSN
                    objDocumentManager.ConnectionString = this.ConnectionString;
                    objDocumentManager.UserLoginName = this.LoginName;
                    objNewDoc = new DocumentManager(this.userLogin, m_iClientid);//sharishkumar Jira 827

                    var _with3 = objNewDoc;

                    //objNewDoc.AddDocument(

                    //_with3.AttachTable = "CLAIM",
                    //_with3.AttachRecordId = lClaimId,
                    //    //-- mwalia2 11/24/2009 to fix the issue of MITS:15417
                    //    //.Title = "FROI"
                    //_with3.Title = "SROI",
                    //_with3.FileName = fun.GetFileName(sDocFile));
                    //objDocumentManager.AddDocument(objNewDoc);

                    //-- ABhateja 05/24/2006
                    //-- This id will be available in wcforms.asp file and later used by
                    //-- froipdf.asp while saving the pdf form.
                    m_lStorageId = objDocumentManager.StorageId;
                }
                else
                {
                    //El.LogError("InvokeFroi", 0, 0, "FROIForms.InvokeFroi", "Document path not set. Cannot attach FDF form to claim.");
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.AttachOutput.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                objNewDoc = null;
                objDocumentManager = null;
            }
        }
       
        /// <summary>
        /// Invoke AcordForms.
        /// </summary>
        /// <param name="lClaimId">Name of the s PDF file.</param>
        /// <param name="lformID">The s FDF filename.</param>
        public string InvokeAcordForms(int lClaimId, int lformID)
        {
            string functionReturnValue = null;
            string sSQL = string.Empty;
            string sPDFFileName = null;
            string sPDSFilePath = null;
            string sFDFFilename = null;
            string sSubject = null;
            string sPDFLink = null;
            string sPDFFilePath = null;
            Populater objPopulater = null;
            FROIExtender objExt = null;
            FormProcessors objFormProcessor = null;

            try
            {
                objPopulater = new Populater(this.userLogin.LoginName, this.userLogin.Password, this.userLogin.objRiskmasterDatabase.DataSourceName, m_iClientid);//sharishkumar Jira 827
                objExt = objPopulater.InvokeACORD(lClaimId);
                objFormProcessor = new FormProcessors();

                // Run script and generate output
                this.GetFileAndFormName(lformID, "CL_FORMS", out sPDFFileName, out sSubject);

                sPDFFilePath = GetPDFFullPath(sPDFFileName, (FormType.AcordForm));
                sPDFLink = sPDFFilePath.Replace(this.PDFPath, this.PDFUrl).Replace("\\", "/").Replace("'\'", "/");

                sPDSFilePath = sPDFFilePath.Replace(".pdf", ".pds");
                sFDFFilename = m_OutputPath + objExt.objClaim.ClaimNumber + sPDFFileName.Replace(".pdf", ".fdf").Replace(" ", "");

                if (File.Exists(sFDFFilename))
                {
                    sFDFFilename = fun.GetUniqueFileName(sFDFFilename);
                }

                //Crank the Script
                objFormProcessor.ProcessForm(sPDFLink, sPDSFilePath, sFDFFilename, objExt);
                functionReturnValue = sFDFFilename;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.InvokeAcordForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                objExt = null;
                objFormProcessor = null;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Invoke the PolicyEnh
        /// </summary>
        /// <param name="lClaimId">Claim ID.</param>
        /// <param name="lformID">PDF form ID.</param>
        public string InvokePolicyEnh(int lPolicyId, int lformID)
        {
            string functionReturnValue = string.Empty;
            string sDocFile  = string.Empty;
            string sSQL =  string.Empty;
            string sPDFFileName =  string.Empty;
            string sPDSFilePath  = string.Empty;
            string sFDFFilename  = string.Empty;
            string sSubject  = string.Empty;
            string sPDFLink  = string.Empty;
            string sPDFFilePath  = string.Empty;

            DBConn objConn = null;
            DBCommand objCommand = null;
            Populater objPopulater = null;
            PolicyEnhExtender objExt = null;
            FormProcessors objFormProcessor = null;

            try
            {

                objPopulater = new Populater(this.userLogin.LoginName, this.userLogin.Password, this.userLogin.objRiskmasterDatabase.DataSourceName, m_iClientid);//sharishkumar Jira 827
                objExt = objPopulater.InvokePolicyEnh(lPolicyId);
                objFormProcessor = new FormProcessors();

               
                //if (objPopulater.InitobjExtender(fun.g_objApp, fun.g_DSN, fun.g_LoginId, fun.g_LoginName) == -1)
                //{
                //    switch (objPopulater.InvokePolicyEnh(lPolicyId, objExt))
                //    {
                //        case -1:
                //            objExt.InsuredID = SelInsuredID;
                //            break;
                //        default:
                //            break;
                //    }
                //}

                this.GetFileAndFormName(lformID, "AD_FORMS", out sPDFFileName, out sSubject);

                //Locate PDF on Disk (for both local and web)
                sPDFFileName = sPDFFileName.ToLower();
                sPDFFilePath = GetPDFFullPath(sPDFFileName, (FormType.ADForms));
                sPDFLink = sPDFFilePath.Replace(this.PDFPath, this.PDFUrl).Replace("\\", "/").Replace("'\'", "/");

                sPDSFilePath = sPDFFilePath.Replace(".pdf", ".pds");
                sFDFFilename = lPolicyId + sPDFFileName.Replace(".pdf", ".fdf");

                sDocFile = sFDFFilename;
                if (m_DocStorageType == StorageType.FileSystemStorage)
                {
                    if (this.DocumentPath.Substring(this.DocumentPath.Length - 1) != "'\'")
                    {
                        this.DocumentPath = this.DocumentPath + "'\'";
                    }

                    sDocFile = this.DocumentPath + sDocFile;
                    if (File.Exists(sDocFile))
                    {
                        sDocFile = fun.GetUniqueFileName(sDocFile, false);
                    }

                    sDocFile = fun.GetFileName(sDocFile);
                }
                else
                {
                    sDocFile = m_OutputPath + sFDFFilename;
                    if (File.Exists(sDocFile))
                        sDocFile = fun.GetUniqueFileName(sDocFile, false);
                    sDocFile = fun.GetFileName(sDocFile);
                }

                objExt.FDFDocFile = sDocFile + "||" + sPDFLink;

                sFDFFilename = m_OutputPath + sFDFFilename.Replace(" ", "");
                if (File.Exists(sFDFFilename))
                    sFDFFilename = fun.GetUniqueFileName(sFDFFilename);

                //Crank the Script
                objFormProcessor.ProcessForm(sPDFLink, sPDSFilePath, sFDFFilename, objExt);

                //**********************************
                //**********************************
                //-- End; MITS 6578;

                //*******************************
                //*******************************
                AttachOutputPolicy(sPDFFileName, sFDFFilename, lPolicyId);

                sSQL = "INSERT INTO AD_FORMS_HIST(FORM_ID,DATE_PRINTED,TIME_PRINTED,USER_ID, HOW_PRINTED) VALUES (";
                sSQL = sSQL + lformID + ",'" + DateTime.Now.ToString("YYYYMMDD") + "','" + DateTime.Now.ToString("HHMMSS") + "','" + fun.g_LoginName + "','SINGL')";

                //Initializing Connection and Command Objects****************************************************
                objConn = DBFactory.GetDbConnection(this.ConnectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                //End Initialization***

                objCommand.Parameters.Clear();
                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery();

                functionReturnValue = sFDFFilename;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.InvokePolicyEnh.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                objConn.Close();
                objExt = null;
                objFormProcessor = null;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Invokes the claim forms.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <param name="lformID">The lform identifier.</param>
        /// <param name="sState">State of the s.</param>
        /// <returns></returns>
        public string InvokeClaimForms(int lClaimId, int lformID, string sState)
        {
            string functionReturnValue = string.Empty;
            string sPDFFileName = string.Empty;
            string sPDSFilePath = string.Empty;
            string sFDFFilename = string.Empty;
            string sSubject = string.Empty;
            string sPDFLink = string.Empty;
            string sPDFFilePath = string.Empty;
            int errorCode = 0;
            FROIExtender objExt = null;
            FormProcessors objFormProcessor = null;

            try
            {
                Populater objPopulater = new Populater(this.userLogin.LoginName, this.userLogin.Password, this.userLogin.objRiskmasterDatabase.DataSourceName, m_iClientid);//sharishkumar Jira 827
                objPopulater.InvokeClaimVehAccNewYork(lClaimId, ref errorCode);
                objFormProcessor = new FormProcessors();

			  if (!errorCode.Equals(0))
              {
                  functionReturnValue = Convert.ToString(errorCode);
                    return functionReturnValue;
                }
                this.GetFileAndFormName(lformID, "CL_FORMS", out sPDFFileName, out sSubject);

                sPDFFileName = sPDFFileName.ToLower();
                sPDFFilePath = GetPDFFullPath(sPDFFileName, FormType.WCClaimForm, sState);
                sPDFLink = sPDFFilePath.Replace(this.PDFPath, this.PDFUrl).Replace("\\", "/").Replace("'\'", "/");

                sPDSFilePath = sPDFFilePath.Replace(".pdf", ".pds");

                sFDFFilename = objExt.objClaim.ClaimNumber + sPDFFileName.Replace(".pdf", ".fdf");
                sFDFFilename = m_OutputPath + sFDFFilename.Replace(" ", "");
                if (File.Exists(sFDFFilename))
                {
                    sFDFFilename = fun.GetUniqueFileName(sFDFFilename);
                }

                objFormProcessor.ProcessForm(sPDFLink, sPDSFilePath, sFDFFilename, objExt);
                functionReturnValue = sFDFFilename;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.InvokeClaimForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                objExt = null;
                objFormProcessor = null;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Invokes the wc forms.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <param name="lformID">The lform identifier.</param>
        /// <param name="sState">State of the s.</param>
        /// <returns></returns>
        public string InvokeWCForms(int lClaimId, int lformID, string sState)
        {
            string sPDFFileName = string.Empty;
            string sPDSFilePath = string.Empty;
            string sFDFFilename = string.Empty;
            string sSubject = string.Empty;
            string sPDFLink = string.Empty;
            string sPDFFilePath = string.Empty;
            string sDocFile = string.Empty;
            FormProcessors objFormProcessor = null;
            Populater objPopulater = null;
            JURISExtender objExt = null;

            try
            {
                objPopulater = new Populater(this.userLogin.LoginName, this.userLogin.Password, this.userLogin.objRiskmasterDatabase.DataSourceName, m_iClientid);//sharishkumar Jira 827
                objExt = objPopulater.InvokeWorkComp(lClaimId);
                objFormProcessor = new FormProcessors();

                this.GetFileAndFormName(lformID, "WCP_FORMS", out sPDFFileName, out sSubject);
                sPDFFilePath = GetPDFFullPath(sPDFFileName, (FormType.WCClaimForm), sState);
                sPDFLink = sPDFFilePath.Replace(this.PDFPath, this.PDFUrl).Replace("\\", "/").Replace("'\'", "/");
                sPDSFilePath = sPDFFilePath.Replace(".pdf", ".pds");
                sFDFFilename = objExt.objClaim.ClaimNumber + sPDFFileName.Replace(".pdf", ".fdf");

                sDocFile = sFDFFilename;

                if (m_DocStorageType == StorageType.FileSystemStorage)
                {
                    if (!string.IsNullOrEmpty(this.DocumentPath))
                    {
                        if (this.DocumentPath.Substring(this.DocumentPath.Length - 1) != "\\")
                        {
                            this.DocumentPath = this.DocumentPath + "\\";
                        }
                        sDocFile = this.DocumentPath + sDocFile;
                        if (File.Exists(sDocFile))
                        {
                            sDocFile = fun.GetUniqueFileName(sDocFile, false);
                        }
                        sDocFile = fun.GetFileName(sDocFile);
                    }
                }
                else
                {
                    //-- Need to write to the output path (RMNetUserData folder) which will hold
                    //-- the changes made to the pdf. These changes will be written to the DB
                    //-- when "Save Only" button is clicked.
                    sDocFile = m_OutputPath + sFDFFilename;
                    if (File.Exists(sDocFile))
                    {
                        sDocFile = fun.GetUniqueFileName(sDocFile, false);
                    }
                    sDocFile = fun.GetFileName(sDocFile);
                    ///      objDoc.DocPath = GetDocPath(sFDFFilename)
                    ///      objDoc.DocPathType = CSCStoreDatabase
                    ///      objDoc.DocStorageCnnStr = m_DocumentPath
                    ///      objDoc.DeleteDoc = False   'default behavior of .AddDocument is to delete the file after streaming into blob
                }

                objExt.FDFDocFile = string.Format("{0}||{1}", sDocFile, sPDFLink);
                //-- End; MITS 6578;

                sFDFFilename = m_OutputPath + sFDFFilename;
                if (File.Exists(sFDFFilename))
                {
                    sFDFFilename = fun.GetUniqueFileName(sFDFFilename);
                }

                objFormProcessor.ProcessForm(sPDFLink, sPDSFilePath, sFDFFilename, objExt);

                if (m_AttachForm)
                {
                    AttachDocument(sPDFFileName, sFDFFilename, objExt.objClaim, "SROI"); ////MITS 33585 ID 1570
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.InvokeWCForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                objExt = null;
                objFormProcessor = null;
                objPopulater = null;
            }
            return sFDFFilename;
        }

        /// <summary>
        /// Invokes the med watch.
        /// </summary>
        /// <param name="lEventID">The l event identifier.</param>
        /// <param name="lformID">The lform identifier.</param>
        /// <returns></returns>
        public string InvokeMedWatch(int lEventID, int lformID)
        {
            string functionReturnValue = null;
            string sPDFFileName = null;
            string sPDSFilePath = null;
            string sFDFFilename = null;
            string sSubject = null;
            string sPDFLink = null;
            string sPDFFilePath = null;
            Populater objPopulater = null;
            FROIExtender objExt = null;
            FormProcessors objFormProcessor = null;
            
            try
            {
                objFormProcessor = new FormProcessors();
                objPopulater = new Populater(this.userLogin.LoginName, this.userLogin.Password, this.userLogin.objRiskmasterDatabase.DataSourceName, m_iClientid);//sharishkumar Jira 827
                objExt = objPopulater.InvokeEventMedWatch(lEventID);

                this.GetFileAndFormName(lformID, "EV_FORMS", out sPDFFileName, out sSubject);

                sPDFFileName = sPDFFileName.ToLower();
                sPDFFilePath = GetPDFFullPath(sPDFFileName, (FormType.EventForm), "FDA");
                sPDFLink = sPDFFilePath.Replace(this.PDFPath, this.PDFUrl).Replace("\\", "/").Replace("'\'", "/");

                sPDSFilePath = sPDFFilePath.Replace(".pdf", ".pds");

                sFDFFilename = objExt.objEvent.EventNumber + sPDFFileName.Replace(".pdf", ".fdf");
                sFDFFilename = m_OutputPath + sFDFFilename.Replace(" ", "");

                if (File.Exists(sFDFFilename))
                {
                    sFDFFilename = fun.GetUniqueFileName(sFDFFilename);
                }

                //Crank the Script
                objFormProcessor.ProcessForm(sPDFLink, sPDSFilePath, sFDFFilename, objExt);
                functionReturnValue = sFDFFilename;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.InvokeMedWatch.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                objExt = null;
                objFormProcessor = null;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Invokes the froi.
        /// </summary>
        /// <param name="lClaimId">The l claim identifier.</param>
        /// <param name="lformID">The lform identifier.</param>
        /// <returns></returns>
        public string InvokeFROI(int lClaimId, int lformID)
        {
            string functionReturnValue = null;
            string sSQL = null;
            string sPDFFileName = null;
            string sPDSFilePath = null;
            string sFDFFilename = null;
            string sSubject = null;
            string sPDFLink = null;
            string sPDFFilePath = null;
            int lErrorType = 0;
            string sHowPrintType = null;
            int iRowCount = 0;
            string sFilterType = null;
            string sDocFile = null; 

            FormProcessors objFormProcessor = null;
            FROIExtender objExt = null;
            Populater objPopulater = null;
          
            try
            {
                objFormProcessor = new FormProcessors();
                objExt = default(FROIExtender);
                objPopulater = new Populater(this.userLogin.LoginName, this.userLogin.Password, this.userLogin.objRiskmasterDatabase.DataSourceName, m_iClientid);//sharishkumar Jira 827

                objExt = objPopulater.InvokeFROI(lClaimId, ref lErrorType);
               
                if (!lErrorType.Equals(0))
                {
                    functionReturnValue = Convert.ToString(lErrorType);
                    return functionReturnValue;
                }


                sSQL = string.Format("SELECT PDF_FILE_NAME,FORM_NAME FROM JURIS_FORMS WHERE FORM_ID = {0}", lformID);

                using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        sPDFFileName = objReader.GetString("PDF_FILE_NAME");
                        sSubject = objReader.GetString("FORM_NAME");
                    }
                }

                sPDFFileName = sPDFFileName.ToLower();
                sPDFFilePath = GetPDFFullPath(sPDFFileName, (FormType.FROIForm));
                sPDFLink = sPDFFilePath.Replace(this.PDFPath, this.PDFUrl);
                sPDFLink = sPDFLink.Replace("\\", "/");
                sPDFLink = sPDFLink.Replace("'\'", "/");

                //Locate PDS on Disk (for local only)
                sPDSFilePath = sPDFFilePath.Replace(".pdf", ".pds");

                sFDFFilename = objExt.objClaim.ClaimNumber + sPDFFileName.Replace(".pdf", ".fdf");
                
                if (m_DocStorageType == StorageType.FileSystemStorage)
                {
                    if (!string.IsNullOrEmpty(this.DocumentPath))
                    {
                        if (this.DocumentPath.Substring(this.DocumentPath.Length - 1) != "\\")
                        {
                            this.DocumentPath = this.DocumentPath + "\\";
                        }
                        sDocFile = this.DocumentPath + sDocFile;
                        if (File.Exists(sDocFile))
                        {
                            sDocFile = fun.GetUniqueFileName(sDocFile, false);
                        }
                        sDocFile = fun.GetFileName(sDocFile);
                    }
                }
                else
                {
                    sFDFFilename=sDocFile = sFDFFilename.Replace(" ", ""); //Changed for Unit testing issue nnithiyanand for db attachment
                    if (File.Exists(sDocFile))
                    {
                        sDocFile = fun.GetUniqueFileName(sDocFile, false);
                    }
                    sDocFile = fun.GetFileName(sDocFile);
                 
                }

                objExt.FDFDocFile = string.Format("{0}||{1}", sDocFile, sPDFLink);
                sFDFFilename = m_OutputPath + sFDFFilename;
                if (File.Exists(sFDFFilename))
                {
                    sFDFFilename = fun.GetUniqueFileName(sFDFFilename);
                }

                //Crank the Script
                objFormProcessor.ProcessForm(sPDFLink, sPDSFilePath, sFDFFilename, objExt);

                if (m_AttachForm)
                {
                    AttachDocument(sPDFFileName, sFDFFilename, objExt.objClaim, "FROI"); ////MITS 33585 ID 1570
                }

                //Add by kuladeep for froi issue 4/4/2012 Start mits:28100
                //Initial set the value for HOW_PRINTED field
                if (m_FroiPrintType == "SINGL")
                {
                    sHowPrintType = "SINGL";
                    sFilterType = "BATCH";
                }
                else if (m_FroiPrintType == "BATCH")
                {
                    sHowPrintType = "BATCH";
                    sFilterType = "SINGL";
                }

                //Alter the value of HOW_PRINTED on the basis of condition.
                bool success = default(bool);

                sSQL = string.Format("SELECT COUNT(DISTINCT HOW_PRINTED) TOTALTYPE FROM JURIS_FORMS_HIST WHERE (HOW_PRINTED = '{0}' OR HOW_PRINTED='BOTH') AND CLAIM_ID = {1}", sFilterType, objExt.ClaimID);
                using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iRowCount = Conversion.CastToType<int>(objReader.GetValue("TOTALTYPE").ToString(), out success);
                    }
                }

                if ((iRowCount == 1 | iRowCount > 1))
                {
                    sHowPrintType = "BOTH";
                }
                //Add by kuladeep for froi issue 4/4/2012 End mits:28100

                //Log operation in JURIS_FORMS_HIST
                sSQL = "INSERT INTO JURIS_FORMS_HIST(FORM_ID,DATE_PRINTED,TIME_PRINTED,CLAIM_ID,USER_ID,HOW_PRINTED) VALUES (";
                sSQL = sSQL + lformID + ",'" + DateTime.Now.ToString("yyyyMMdd") + "','" + DateTime.Now.ToString("HHmmss") + "'," + objExt.ClaimID + ",'" + fun.g_LoginName + "','" + sHowPrintType + "')";

                using (DBConn objDbConnection = DBFactory.GetDbConnection(this.ConnectionString))
                {
                    objDbConnection.Open();
                    objDbConnection.ExecuteNonQuery(sSQL);
                    objDbConnection.Close(); //MITS: 33585 Defect 1569 
                }

                functionReturnValue = sFDFFilename;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.InvokeFROI.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
               objFormProcessor = null;
               objExt = null;
               objPopulater = null;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Files the exists.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public bool FileExists(string fileName)
        {
            return File.Open(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite) != null;
        }

        #endregion Public Methods

        # region Private Methods
        /// <summary>
        /// Gets the name of the file and form.
        /// </summary>
        /// <param name="formId">The form identifier.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="formName">Name of the form.</param>
        private void GetFileAndFormName(int formId, string tableName, out string fileName, out string formName)
        {
            string sSQL = string.Format("SELECT FILE_NAME,FORM_NAME FROM {0} WHERE FORM_ID = {1}", tableName, formId);
            fileName = string.Empty;
            formName = string.Empty;

            try
            {
                using (DBReader objReader = DBFactory.GetDbReader(this.ConnectionString, sSQL))
                {
                    if (!object.ReferenceEquals(objReader, null))
                    {
                        if (objReader.Read())
                        {
                            fileName = objReader.GetString("FILE_NAME").ToLower();
                            formName = objReader.GetString("FORM_NAME");
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROIManager.GetClaimForms.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
            }
        }

        /// <summary>
        /// Attaches the output policy.
        /// </summary>
        /// <param name="sPDFFileName">Name of the s PDF file.</param>
        /// <param name="sFDFFilename">The s FDF filename.</param>
        /// <param name="lPolicyId">The l policy identifier.</param>
        private void AttachOutputPolicy(string sPDFFileName, string sFDFFilename, int lPolicyId)
        {
            string sDocFile = string.Empty;
            DocumentManager objDoc = null;

            try
            {
                objDoc = new DocumentManager(userLogin, m_iClientid);//sharishkumar Jira 827
                if (!string.IsNullOrEmpty(this.DocumentPath))
                {
                    sDocFile = Convert.ToString(lPolicyId) + sPDFFileName.Replace(".pdf", ".fdf");
                    if (m_DocStorageType == StorageType.FileSystemStorage)
                    {
                        if (this.DocumentPath.Substring(this.DocumentPath.Length - 1) != "'\'")
                            this.DocumentPath = this.DocumentPath + "'\'";
                        sDocFile = this.DocumentPath + sDocFile;

                        if (File.Exists(sDocFile))
                        {
                            sDocFile = fun.GetUniqueFileName(sDocFile);
                        }

                        File.Copy(sFDFFilename, sDocFile);
                        objDoc.DestinationStoragePath = this.DocumentPath;
                    }
                    else
                    {
                        sDocFile = sFDFFilename;
                        objDoc.DestinationStoragePath = fun.GetDocPath(sFDFFilename);
                        objDoc.DocumentStorageType = StorageType.DatabaseStorage; 
                        objDoc.ConnectionString = this.DocumentPath;

                        //default behavior of .AddDocument is to delete the file after streaming into blob
                        //-- ABhateja 05/24/2006
                        //-- Update the BLOB stored at this STORAGE_ID
                        objDoc.StorageId = m_lStorageId;
                    }
                    //Nanda
                    //objDoc.DSN = fun.g_DSN;
                    //objDoc.UserLoginName = fun.g_LoginName;
                    //objNewDoc = new DocumentManager();
                    //var _with4 = objNewDoc;
                    //_with4.AttachTable = "POLICYENH";
                    //_with4.AttachRecordId = lPolicyId;
                    //_with4.Title = "POLICYENH";
                    //_with4.FileName = fun.GetFileName(sDocFile);
                    //objDoc.AddDocument(objNewDoc);

                    //-- ABhateja 05/24/2006
                    //-- This id will be available in wcforms.asp file and later used by
                    //-- froipdf.asp while saving the pdf form.
                    m_lStorageId = objDoc.StorageId;
                    //UPGRADE_NOTE: Object objNewDoc may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                   
                   
                }
                else
                {
                    throw new Exception("Document path not set. Cannot attach FDF form to policy.");
                    //El.LogError("InvokePolicyEnh", 0, 0, "FROIForms.InvokePolicyEnh", "Document path not set. Cannot attach FDF form to policy.");
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.AttachOutputPolicy.Error", m_iClientid), p_objEx);////sharishkumar Jira 827
            }
            finally
            {
                objDoc = null;
            }
        }

        /// <summary>
        /// Makes the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemOftype">The item oftype.</param>
        /// <returns>Returns a list of annonimous type.</returns>
        private static List<T> MakeList<T>(T itemOftype)
        {
            List<T> newList = new List<T>();
            return newList;
        }

        /// <summary>
        /// Gets the PDF full path.
        /// </summary>
        /// <param name="sFileName">Name of the s file.</param>
        /// <param name="PDFType">Type of the PDF.</param>
        /// <param name="sState">State of the s.</param>
        /// <returns></returns>
        private string GetPDFFullPath(string sFileName, FormType PDFType, string sState = "")
        {
            string functionReturnValue = null;
            string sPathSegment = null;
            string sCustomPathSegment = null;
            sPathSegment = "";
            sCustomPathSegment = "";

            try
            {
                switch (PDFType)
                {
                    case FormType.FROIForm:
                        sPathSegment = "";
                        sCustomPathSegment = "Client-Custom\\FROI\\";
                        break;
                    case FormType.WCClaimForm:
                        sPathSegment = sState + "\\";
                        sCustomPathSegment = "Client-Custom\\workcomp\\" + sState + "\\";
                        break;
                    case FormType.ClaimForm:
                        sPathSegment = Convert.ToString(Convert.ToDouble("ClaimForms\\sState & ") / Convert.ToDouble(""));
                        sCustomPathSegment = "Client-Custom\\Claim-Forms\\" + sState + "\\";
                        break;
                    case FormType.EventForm:
                        sPathSegment = "EventForms\\" + sState + "\\";
                        sCustomPathSegment = "Client-Custom\\Event-Forms\\" + sState + "\\";
                        break;
                    case FormType.PersonInvolvedForm:
                        sCustomPathSegment = "Client-Custom\\person-involved\\misc\\";
                        break;
                    case FormType.AcordForm:
                        sPathSegment = "";
                        sCustomPathSegment = "ClaimForms\\";
                        break;
                    case FormType.ADForms:
                        sPathSegment = "\\";
                        sCustomPathSegment = "\\";
                        break;
                }

                // If Custom Form Exists Use Custom else Use Standard.
                if (File.Exists(this.PDFPath + sCustomPathSegment + sFileName))
                {
                    functionReturnValue = this.PDFPath + sCustomPathSegment + sFileName;
                }
                else
                {
                    functionReturnValue = this.PDFPath + sPathSegment + sFileName;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.GetPDFFullPath.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Attaches the document.
        /// </summary>
        /// <param name="sPDFFileName">Name of the s PDF file.</param>
        /// <param name="sFDFFilename">The s FDF filename.</param>
        /// <param name="objClaim">The object claim.</param>
        /// <param name="FormType">The Form type.</param>
        private void AttachDocument(string sPDFFileName, string sFDFFilename, Claim objClaim,string FormType)
        {
            long lDocumentId = 0;
            int iPSID = 0;

            string p_sSubject = string.Empty;
            string p_sFileName = string.Empty;
            string sAppExcpXml = string.Empty;
            string sDocFile = string.Empty;
            string Filename = string.Empty;

            XmlDocument objDOM = null;
            DocumentManager objDocumentManager = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DataModelFactory objDataModelFactory = null;
            
            try
            {
                if (!string.IsNullOrEmpty(this.DocumentPath))
                {
                    sDocFile = objClaim.ClaimNumber + sPDFFileName.Replace(".pdf", ".fdf");
                    if (m_DocStorageType == StorageType.FileSystemStorage)
                    {
                        if (this.DocumentPath.Substring(this.DocumentPath.Length - 1) != "\\")
                        {
                            this.DocumentPath = this.DocumentPath + "\\";
                        }
                        sDocFile = this.DocumentPath + sDocFile;
                        if (File.Exists(sDocFile))
                        {
                            sDocFile = fun.GetUniqueFileName(sDocFile);
                        }
                        File.Copy(sFDFFilename, sDocFile);
                    }
                    else
                    {
                        sDocFile = sFDFFilename;
                    }


                    MemoryStream objMemory = new MemoryStream();
                    //get the binary format of the file to be attached to Claim
                    GetFileStream(sDocFile, ref objMemory);
                    if (m_DocStorageType == StorageType.FileSystemStorage)
                    {
                        if (File.Exists(sDocFile))
                        {
                            File.Delete(sDocFile);
                        }
                    }
                    objDocumentManager = new DocumentManager(objClaim.Context.RMUser, m_iClientid);//sharishkumar Jira 827
                    objDocumentManager.ConnectionString = objClaim.Context.DbConnLookup.ConnectionString;
                    objDocumentManager.UserLoginName = objClaim.Context.RMUser.LoginName;
                    objDocumentManager.SecurityConnectionString = Security.SecurityDatabase.GetSecurityDsn(m_iClientid);//sharishkumar Jira 827
                    if (objClaim.Context.RMUser.DocumentPath.Length > 0)
                    {
                        objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                        objDocumentManager.DestinationStoragePath = objClaim.Context.RMUser.DocumentPath;
                    }
                    else
                    {
                        objDocumentManager.DocumentStorageType = (StorageType)objClaim.Context.RMUser.objRiskmasterDatabase.DocPathType;
                        objDocumentManager.DestinationStoragePath = objClaim.Context.RMUser.objRiskmasterDatabase.GlobalDocPath;
                    }

                    objDOM = new XmlDocument();
                    objElemParent = objDOM.CreateElement("data");
                    objDOM.AppendChild(objElemParent);

                    objElemChild = objDOM.CreateElement("Document");

                    objElemTemp = objDOM.CreateElement("DocumentId");
                    objElemTemp.InnerText = "0";
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("FolderId");
                    objElemTemp.InnerText = "0";
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("Notes");
                    objElemTemp.InnerText = "";
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("Class");
                    objElemTemp.InnerText = "0";
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("Category");
                    objElemTemp.InnerText = "0";
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("Type");
                    objElemTemp.InnerText = "0";
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("CreateDate");
                    objElemTemp.InnerText = Conversions.ToDbDateTime(DateTime.Now);
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("Name");
                    objElemTemp.InnerText =  FormType;
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("Subject");
                    objElemTemp.InnerText = string.Format("{0} Form", FormType);

                    p_sSubject = objElemTemp.InnerText;
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("UserLoginName");
                    objElemTemp.InnerText = objClaim.Context.RMUser.LoginName;
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("FileName");
                    Filename = fun.GetFileName(sDocFile).Replace(".pdf", ".fdf");
                    objElemTemp.InnerText = Filename;

                    p_sFileName = objElemTemp.InnerText;
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("FilePath");
                    objElemTemp.InnerText = string.Empty;
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("Keywords");
                    objElemTemp.InnerText = string.Empty;
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("AttachTable");
                    objElemTemp.InnerText = "CLAIM";
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("AttachRecordId");
                    objElemTemp.InnerText = objClaim.ClaimId.ToString();
                    objElemChild.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("FormName");
                    objElemTemp.InnerText = "Claim";
                    objElemChild.AppendChild(objElemTemp);

                    objDOM.FirstChild.AppendChild(objElemChild);
                    iPSID = 3000;
                    objDocumentManager.AddDocument(objDOM.InnerXml, objMemory, iPSID, out lDocumentId);

                    this.StorageId = (int)lDocumentId;
                }
            }
           
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROINet6.FROIForms.AttachDocument.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                }
                objDOM = null;
                objDocumentManager = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets the file stream.
        /// </summary>
        /// <param name="p_sPath">The P_S path.</param>
        /// <param name="p_objMemoryStream">The p_obj memory stream.</param>
        /// <exception cref="Riskmaster.ExceptionTypes.RMAppException"></exception>
        private void GetFileStream(string p_sPath, ref MemoryStream p_objMemoryStream)
        {
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            Byte[] arrRet = null;
            try
            {
                objFileStream = new FileStream(p_sPath, FileMode.Open);
                objBReader = new BinaryReader(objFileStream);
                arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                p_objMemoryStream = new MemoryStream(arrRet);
                objFileStream.Close();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("GetFileStream.Error", m_iClientid), p_objEx);//sharishkumar Jira 827
            }
        }

        #endregion Private Methods
    }
}
