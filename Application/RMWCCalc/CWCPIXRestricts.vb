Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPIXRestricts
    Implements System.Collections.IEnumerable

    Const sClassName As String = "CWCPIXRestricts"
    Const m_TableName As String = "PI_X_RESTRICT"

    Private m_DateFirstRestrct As String
    Private m_DateLastRestrct As String
    Private m_Duration As Integer
    Private m_PercentDisabled As Integer
    Private m_PiRestrictRowId As Integer
    Private m_PiRowId As Integer
    Private m_PositionCode As Integer

    'local variable to hold collection
    Private mCol As Collection
    Private Function ClearObject() As Integer



    End Function
    Public Function LoadData(ByRef lPIRowID As Integer) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadData = 0

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " PI_X_RESTRICT.DATE_FIRST_RESTRCT"
            sSQL = sSQL & ",PI_X_RESTRICT.DATE_LAST_RESTRCT"
            sSQL = sSQL & ",PI_X_RESTRICT.DURATION"
            sSQL = sSQL & ",PI_X_RESTRICT.PERCENT_DISABLED"
            sSQL = sSQL & ",PI_X_RESTRICT.PI_RESTRICT_ROW_ID"
            sSQL = sSQL & ",PI_X_RESTRICT.PI_ROW_ID"
            sSQL = sSQL & ",PI_X_RESTRICT.POSITION_CODE"

            sSQL2 = ""
            sSQL2 = sSQL2 & " FROM " & m_TableName
            sSQL2 = sSQL2 & " WHERE PI_ROW_ID = " & lPIRowID
            sSQL2 = sSQL2 & " ORDER BY PI_RESTRICT_ROW_ID"

            If lPIRowID = 0 Then
                sSQL2 = ""
                sSQL2 = sSQL2 & " FROM PI_X_RESTRICT, PERSON_INVOLVED, CLAIM, CLAIMANT"
                sSQL2 = sSQL2 & " WHERE CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID"
                sSQL2 = sSQL2 & " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
                sSQL2 = sSQL2 & " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID"
                sSQL2 = sSQL2 & " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_RESTRICT.PI_ROW_ID"
                sSQL2 = sSQL2 & " AND CLAIM.CLAIM_ID = " & g_objXClaim.ClaimID
                sSQL2 = sSQL2 & " ORDER BY PI_RESTRICT_ROW_ID"
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL & sSQL2)

            While objReader.Read()
                m_DateFirstRestrct = objReader.GetString("DATE_FIRST_RESTRCT")
                m_DateLastRestrct = objReader.GetString("DATE_LAST_RESTRCT")
                m_Duration = objReader.GetInt32("DURATION")
                m_PercentDisabled = objReader.GetInt32("PERCENT_DISABLED")
                m_PiRestrictRowId = objReader.GetInt32("PI_RESTRICT_ROW_ID")
                m_PiRowId = objReader.GetInt32("PI_ROW_ID")
                m_PositionCode = objReader.GetInt32("POSITION_CODE")

                Add(m_DateFirstRestrct, m_DateLastRestrct, m_Duration, m_PercentDisabled, m_PiRestrictRowId, m_PiRowId, m_PositionCode)
            End While

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function Add(ByRef DateFirstRestrct As String, ByRef DateLastRestrct As String, ByRef Duration As Integer, ByRef PercentDisabled As Integer, ByRef PiRestrictRowId As Integer, ByRef PiRowId As Integer, ByRef PositionCode As Integer) As CWCPiXRestrict
        Const sFunctionName As String = "Add"
        Try
            'create a new object
            Dim objNewMember As CWCPiXRestrict
            objNewMember = New CWCPiXRestrict

            'set the properties passed into the method
            objNewMember.DateFirstRestrct = DateFirstRestrct
            objNewMember.DateLastRestrct = DateLastRestrct
            objNewMember.Duration = Duration
            objNewMember.PercentDisabled = PercentDisabled
            objNewMember.PiRestrictRowId = PiRestrictRowId
            objNewMember.PiRowId = PiRowId
            objNewMember.PositionCode = PositionCode

            mCol.Add(objNewMember)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            Dim ErrNum As Integer
            Dim ErrDesc, ErrSrc As String
            ErrNum = Err.Number
            ErrDesc = Err.Description
            ErrSrc = Err.Source
            Err.Raise(ErrNum, sClassName & "." & sFunctionName & "." & ErrSrc, ErrDesc)
            Exit Function


        Finally
        End Try

    End Function
    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CWCPiXRestrict
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

