Option Strict Off
Option Explicit On
Imports Riskmaster.Db
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
Public Class CWCPermTotalDis
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "PTD"
    Const sClassName As String = "CWCPermTotlDis"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRulePTD"
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objClaimBenefitRecent As Object
    Dim objSpendableIncome As Object
    Public objTTD As Object

    Public BeginPrntDate_Effective As String
    Public BenefitDays As Integer
    Public BenefitEndDate As String
    Public BenefitStartDate As String
    Public BenefitRate_Effective As Double

    Public CalculatedPayment As Double

    Public LastPayableDate As String
    Public PayLateCharge As Short
    Public PermStatDate_DTG As String

    Public TotalCost As Double
    Public WeeksPayable_Effective As Double

    Public BenefitRate_PTD As Double

    Private Function dGetEffectiveRate() As Double
        Const sFunctionName As String = "dGetEffectiveRate"
        Dim dRate As Double
        Try

            dGetEffectiveRate = 0

            'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dRate = objTTD.Standard.dGetBasicRate(g_objXClaim.ClaimantOriginalAWW, g_objXClaim.ClaimantTTDRate, g_objXClaim.JurisTaxExemptions, g_objXClaim.JurisTaxStatusCode, 0, g_objXClaim.ClaimantHourlyPayRate)
            'UPGRADE_WARNING: Couldn't resolve default property of object objClaimBenefitRecent.BenefitRateEffective. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objClaimBenefitRecent.BenefitRateEffective > 0 And objClaimBenefitRecent.BenefitRateEffective <> dRate Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objClaimBenefitRecent.BenefitRateEffective. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dRate = objClaimBenefitRecent.BenefitRateEffective
            End If

            dGetEffectiveRate = dRate
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    'note: if bSaveDateAndPercent, user must pass in PermStatDate (dis rate will be 100%)
    '       if bCalculate, user must pass in dPermStatDate, sBirthDate, iSexCode
    'note: this function will return calculated payments that cannot be saved
    'note: call this function before saving a payment:
    '       -set bCalculate true and bSaveDateAndPercent false.
    '       -reject save if .ErrorMask > 0
    Public Function PutClaimData() As Boolean
        Const sFunctionName As String = "PutClaimData"

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String

        Dim sNow As String
        Dim lRowID As Integer
        Try
            lRowID = -1

            sNow = Format(Now, "yyyymmddhhnnss")
            sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID & " AND BENEFIT_TYPE = 'PTD'" & " ORDER BY ROW_ID DESC"

            'if the record exists, update it.  if it does not exist, create it

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                lRowID = objReader.GetInt32("ROW_ID")
                objReader.Close()

                sSQL = "SELECT * FROM WCP_CLAIMS WHERE ROW_ID = " & lRowID
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objReader.Close()
                objWriter.Fields("DISABILITY_RATE").Value = 100
                objWriter.Fields("BENEFIT_TYPE").Value = "PTD"
                objWriter.Fields("BENEFIT_RATE").Value = Me.BenefitRate_Effective
                objWriter.Fields("CLAIM_ID").Value = g_objXClaim.ClaimID
                objWriter.Fields("BEGIN_PRNT_DATE").Value = Me.BeginPrntDate_Effective
                objWriter.Fields("ADDED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("DTTM_RCD_ADDED").Value = sNow
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = sNow
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lRowID = lGetNextUID("WCP_CLAIMS")
                objReader.Close()
                objWriter.Fields.Add("ROW_ID", lRowID)

                objWriter.Fields.Add("DISABILITY_RATE", 100)
                objWriter.Fields.Add("BENEFIT_TYPE", "PTD")
                objWriter.Fields.Add("BENEFIT_RATE", Me.BenefitRate_Effective)
                objWriter.Fields.Add("CLAIM_ID", g_objXClaim.ClaimID)
                objWriter.Fields.Add("BEGIN_PRNT_DATE", Me.BeginPrntDate_Effective)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", sNow)
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", sNow)

            End If

            objWriter.Execute()
            objReader.Close()

            PutClaimData = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)

            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objTTD may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objTTD = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 1/8/2005 15:25
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : California Permanent Total Disability and Temperatory Total Disability use the same data
    ' ..........: table and the same calculations EXCEPT for the two year rule
    '***********************************************************************************************************

    '---------------------------------------------------------------------------------------
    '
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"

        Dim objReader As DbReader

        Dim dblMinBenefit, dblMaxBenefit, dblMaxAWW As Double
        Dim dblLifeExpectancy, dblMod As Double
        Dim lReturn As Integer
        Dim dEndDate, dDateOfEvent, dLastPayableDate As Date
        Dim sPermStatDate As String
        Dim sSQL As String
        Dim sSQL2 As String
        Dim sAbbr As String
        Dim sWarning As String
        Dim iLifeExpectancyDays As Short
        Try
            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()


            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objCalcBenefitRule.JurisWorkWeek = 0 Then
                m_Warning = ""
                m_Warning = m_Warning & "The Jurisdictionally defined work week is not set correctly." & vbCrLf
                m_Warning = m_Warning & "It is set to zero when the expected value should be 5,6 or 7." & vbCrLf & vbCrLf
                m_Warning = m_Warning & "The jurisdiction is:  " & modFunctions.GetStateName_SQL((g_objXClaim.FilingStateID)) & vbCrLf
                m_Warning = m_Warning & "The Event Date is:  " & g_objXClaim.DateOfEventCalar & "." & vbCrLf
                m_Warning = m_Warning & "Please inform the System Administrator of this issue."
                Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseTTDRateCode = g_lYesCodeID Then
                sAbbr = modFunctions.GetTempTotalAbbreviation(m_Warning, (g_objXClaim.FilingStateID))
                If m_Warning > "" Then
                    Exit Function
                End If
                lReturn = modFunctions.GetCalculator(objTTD, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function

                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lReturn = objTTD.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                m_ErrorMask = objTTD.Standard.ErrorMask
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                m_ErrorMaskSAWW = objTTD.Standard.ErrorMaskSAWW
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                m_ErrorMaskCalcSetup = objTTD.Standard.ErrorMaskCalcSetup
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If (lReturn + objTTD.Standard.ErrorMask + objTTD.Standard.ErrorMaskSAWW + objTTD.Standard.ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then
                    Exit Function
                End If
            End If

            'jtodd22 --get most recent WCP_CLAIMS record for this claim
            objClaimBenefitRecent = New CWCWCPClaim
            'UPGRADE_WARNING: Couldn't resolve default property of object objClaimBenefitRecent.LoadDataLastRecordByRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lReturn = objClaimBenefitRecent.LoadDataLastRecordByRowID(m_sBenefitTypeAbbr)
            If Not (lReturn = m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then
                Exit Function
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            lLoadData = g_lErrNum

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:35
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        '***********************************************************************************************************
        '*****California Permanent Total Disability and Temperatory Total Disability use the same data table and
        '*****the same calculations EXCEPT for the two year rule
        '***********************************************************************************************************
        Dim dMMIDisabilityRate As Double
        Dim sMMIDateDTG As String

        Dim dRate As Double
        Dim dLastPayableDate As Date
        Dim dblLifeExpectancy As Double
        Dim dblMod As Double
        Dim dEndDate As Date
        Dim iLifeExpectancyDays As Short
        Dim lReturn As Integer

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'If objBenefitRule.AllwaysRequireMMIDate = g_lYesCodeID Then
            lReturn = modFunctions.GetMMIImpairment(dMMIDisabilityRate, sMMIDateDTG)
            If lReturn = 0 Then
                If sMMIDateDTG = "" Then
                    If modFunctions.GetStatePostalCode_SQL((g_objXClaim.FilingStateID)) = "CA" Then
                        m_Note = "There is no 'Permanent and Stationary Date'.  Weeks payable and Reserves will not be calculated."
                    Else
                        m_Note = "There is no 'Maximum Medical Improvement Date'.  Weeks payable and Reserves will not be calculated."
                    End If
                    'jtodd22 07/17/2006 this is a note not a warning and not a fatal error--If m_Note > "" Then Exit Function
                End If

                If sMMIDateDTG > g_objXPaymentParms.BenefitStartDate Then
                    m_Warning = "First Date for payment can not be before the MMI date."
                    Exit Function
                End If
            End If
            'End If

            'build top of work sheet
            If Trim(g_objXPaymentParms.BenefitStartDate & "") = "" Then
                m_Warning = "The Benefit Start Date is missing."
                Exit Function
            End If
            If Trim(g_objXPaymentParms.BenefitEndDate & "") = "" Then
                m_Warning = "The Benefit End Date is missing."
                Exit Function
            End If
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail
            colWorkSheet.Add("|Date of Injury                                    |" & g_objXClaim.DateOfEventCalar)
            colWorkSheet.Add("|Jurisdiction                                      |" & g_objXClaim.FilingStatePostalCode)
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Benefit Payment Start Date                        |" & CDate(g_objXPaymentParms.BenefitStartDate))
            colWorkSheet.Add("|Benefit Payment End Date                          |" & CDate(g_objXPaymentParms.BenefitEndDate))
            colWorkSheet.Add("|Days in Payment                                   |" & g_objXPaymentParms.BenefitDays)
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Claimant's Original AWW|" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseTTDRateCode = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.JurisdictionMaxAww. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Me.objTTD.JurisdictionMaxAww > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.JurisdictionMaxAww. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|Jurisdiction Imposed Max AWW Limit                |" & Format(Me.objTTD.JurisdictionMaxAww, "Currency"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.ClaimantJurisAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|Claimant AWW As Capped (State Imposed Limit)      |" & Format(Me.objTTD.ClaimantJurisAWW, "Currency"))
                End If
            End If
            Select Case g_objXClaim.FilingStatePostalCode
                Case "CA"
                    If g_objXClaim.DateOfEventDTG > "20021231" Then
                        colWorkSheet.Add("|California imposed a minimum PTD Rate of $126 per week for events after 12/31/2002.  For calculations this is the same as a minimum AWW of $189 per Week.|")
                        'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.ClaimantJurisAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        colWorkSheet.Add("|Claimant AWW (After Imposing State Cap and Floor Limits)      |" & Format(Me.objTTD.ClaimantJurisAWW, "Currency"))
                    End If
                Case Else
            End Select
            If Trim(Me.PermStatDate_DTG & "") <> "" Then
                dblLifeExpectancy = 0
                dblLifeExpectancy = GetYearsLifeExpectancy(Me.PermStatDate_DTG)
                If dblLifeExpectancy = 0 Then
                    dblLifeExpectancy = GetYearsLifeExpectancyB(Me.PermStatDate_DTG)
                End If
                dEndDate = DateAdd(Microsoft.VisualBasic.DateInterval.Year, Int(dblLifeExpectancy), CDate(GetFormattedDate(Me.PermStatDate_DTG)))
                dblMod = modFunctions.RoundStandard(dblLifeExpectancy - Int(dblLifeExpectancy), 4)
                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                iLifeExpectancyDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(GetFormattedDate(Me.PermStatDate_DTG)), dEndDate) + (dblMod * 365)
                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.WeeksPayable_Effective = modFunctions.RoundStandard(iLifeExpectancyDays / objCalcBenefitRule.JurisWorkWeek, 4)
                Me.TotalCost = Me.WeeksPayable_Effective * CDbl(Me.BenefitRate_Effective)
                dLastPayableDate = DateAdd(Microsoft.VisualBasic.DateInterval.Day, iLifeExpectancyDays, CDate(GetFormattedDate(Me.PermStatDate_DTG)))
                Me.LastPayableDate = Format(dLastPayableDate, "yyyymmdd")
                m_Note = "Based on life expectancy, the estimated weeks payable is " & Me.WeeksPayable_Effective & " with a cost of " & Format(Me.TotalCost, "Currency") & ".  " & "Benefit time frame begins on " & GetFormattedDate(Me.PermStatDate_DTG) & " though " & GetFormattedDate(Me.LastPayableDate) & "."
            End If

            dRate = dGetBasicRate(g_objXClaim.ClaimantOriginalAWW, (g_objXClaim.ClaimantTTDRate), (g_objXClaim.JurisTaxExemptions), (g_objXClaim.JurisTaxStatusCode), 0, (g_objXClaim.ClaimantHourlyPayRate))
            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lCalculatePayment = g_lErrNum

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim lReturn As Integer
        Dim bRateLessThanMin As Boolean

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If


            'jtodd22  The creation of objTTD is part of the LoadData function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseTTDRateCode = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.dGetBasicRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dBasicRate = objTTD.dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.NonTTDRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.NonTTDRate > 1 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.NonTTDRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = g_objXClaim.ClaimantOriginalAWW * (objBenefitRule.NonTTDRate / 100)
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.NonTTDRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = g_objXClaim.ClaimantOriginalAWW * objBenefitRule.NonTTDRate
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.MaxCompRateWeekly > 0 And dBasicRate > objBenefitRule.MaxCompRateWeekly Then dBasicRate = objBenefitRule.MaxCompRateWeekly
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If dBasicRate < objBenefitRule.MinCompRate Then
                    dBasicRate = AverageWage
                    bRateLessThanMin = True
                End If
            End If

            If bRateLessThanMin Then
                Select Case g_objXClaim.FilingStatePostalCode

                    Case "IA" 'Plug the minimum amount (which is found in BenefitRate_PPD variable here)
                        'into the spendable income chart with the exemptions and marital status
                        'and get the basic rate
                        lReturn = modFunctions.GetRateSpendable(dBasicRate, m_ErrorMaskSpendData, AverageWage)
                        If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSAWW + m_ErrorMaskSpendData) > g_objXErrorMask.cSuccess Then Exit Function
                End Select
            End If

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            If bRuleIsLocal Then
                'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objBenefitRule = Nothing
                'UPGRADE_NOTE: Object objTTD may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objTTD = Nothing
            End If

        End Try

    End Function




    Private Function ClearObject() As Object
        m_CheatSheetTitle = "Permanent Total"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

