Option Strict Off
Option Explicit On
Public Class CWCBenefitInCalc
    '---------------------------------------------------------------------------------------
    ' Module    : CWCBenefitInCalc
    ' DateTime  : 12/17/2003 10:22
    ' Author    : jlt
    ' Purpose   : data structure for benefit data
    '---------------------------------------------------------------------------------------
    Const sClassName As String = "CWCBenefitInCalc"
    Private m_sBenefitDesc As String
    Private m_sAbbreviation As String
    Private m_lDatabaseLookupID As Integer
    Public Property BenefitDesc() As String
        Get
            BenefitDesc = m_sBenefitDesc
        End Get
        Set(ByVal Value As String)
            m_sBenefitDesc = Value
        End Set
    End Property
    Public Property Abbreviation() As String
        Get
            Abbreviation = m_sAbbreviation
        End Get
        Set(ByVal Value As String)
            m_sAbbreviation = Value
        End Set
    End Property
    Public Property DatabaseLookupID() As Integer
        Get
            DatabaseLookupID = m_lDatabaseLookupID
        End Get
        Set(ByVal Value As Integer)
            m_lDatabaseLookupID = Value
        End Set
    End Property
End Class

