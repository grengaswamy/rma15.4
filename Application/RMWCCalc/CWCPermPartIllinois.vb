Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPermPartIllinois
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "PPD"
    Const sClassName As String = "CWCPermPartIllinois"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleSPPDIL"
    Const m_MinimumSwitchDateDBFormat As String = "20060102"

    Dim objWCPClaim As CWCWCPClaim
    Private colClaimantBodyMembers As New CWCPIXBodyMembers
    Public colBodyMembers As Object
    Public objBenefitRule As Object
    Public objBenefitSubRuleAmount As Object
    Public objCalcBenefitRule As Object
    Public objSAWW As Object

    Private m_lRowID As Integer 'jtodd22 processing varible


    'these members represent fields in WCP_CLAIMS
    Public BenefitRate_Original As Double 'pre-commutation rate, if record was commutated
    Public BenefitRate_PostComm As Double 'only exists if a commutation
    Public BenefitRate_Effective As Double
    Public CalculatedPayment As Double
    Public Commutation As Double
    Public DisabilityRate_Original As Double
    Public DisabilityRate_PostComm As Double
    Public DisabilityRate_Effective As Double
    Public EstCost_Original As Double
    Public EstCost_PostComm As Double
    Public EstCost_Effective As Double
    Public Liability As Double
    Public LiabilityPaid As Double
    Public WeeksPayable_Original As Double
    Public WeeksPayable_PostComm As Double
    Public WeeksPayable_Effective As Double
    Public BeginPrntDate_Original As String
    Public BeginPrntDate_PostComm As String
    Public BeginPrntDate_Effective As String
    Public EndRecdDate_Original As String
    Public EndRecdDate_PostComm As String
    'end members linked to WCP_CLAIMS

    Public LastCommutationDate As String
    Public LastCommutationDate_DTG As String
    Public LastDayPPDPayable As Date
    Public LastDayPPDPaid As String
    Public WeeksPayable As Double

    Public BenefitDays As Integer
    Public RecordCount As Integer


    Public JurisMaxBenRateDay As Double
    Public JurisMaxBenRateWeek As Double
    Public MinBenRateDay As Double
    Public MinBenRateWeek As Double

    Public BenefitEndDate As String
    Public BenefitStartDate As String
    Public objTTD As Object
    Public PayLateCharge As Short

    Dim objSpendableIncome As Object

    Public BenefitRate_PPD As Double

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object colBodyMembers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        colBodyMembers = Nothing
        'UPGRADE_NOTE: Object colClaimantBodyMembers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        colClaimantBodyMembers = Nothing
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objBenefitSubRuleAmount may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitSubRuleAmount = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSAWW = Nothing
        'UPGRADE_NOTE: Object objSpendableIncome may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncome = Nothing
        'UPGRADE_NOTE: Object objWCPClaim may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objWCPClaim = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Public Function PutClaimBenefitData() As Boolean
        Const sFunctionName As String = "PutClaimBenefitData"
        Try
            Dim lRowID As Integer
            Dim sSQL As String

            sSQL = ""
            sSQL = sSQL & "SELECT MIN(ROW_ID) FROM WCP_CLAIM"
            sSQL = sSQL & " WHERE BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'"
            sSQL = sSQL & " AND CLAIM_ID = " & g_objXClaim.ClaimID

            lRowID = DbFactory.ExecuteScalar(g_ConnectionString, sSQL)
            sSQL = ""
            sSQL = sSQL & "SELECT * FROM WCP_CLAIM"
            sSQL = sSQL & " WHERE BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'"
            sSQL = sSQL & " AND CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND ROW_ID = " & lRowID
            If lRowID = 0 Then 'new record
                objWCPClaim.LoadDataOriginalRecord(sSQL)
                With objWCPClaim
                    'not used here--.AttyFeeCode
                    'not used here--.AttyFeePercent
                    .BeginPrintDateEffective = Me.BeginPrntDate_Effective
                    .BenefitRateEffective = Me.BenefitRate_Effective
                    .BenefitType = m_sBenefitTypeAbbr
                    'not used here--.Commutation
                    .DisabilityRateEffective = Me.DisabilityRate_Effective
                    'not used here--.EndRecvdDateEffective
                    'calculated in object--.EstCostEffective
                    'not used here--.FormID
                    'not used here--.Paid
                    .WeeksPayableEffective = Me.WeeksPayable_Effective
                End With
                objWCPClaim.SaveDataOriginalRecord()
            Else
                'have multiple records
                'jtodd22 do not change data
            End If
            PutClaimBenefitData = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"

        Dim dTemp As Double
        Dim objReader As DbReader
        Dim iTemp As Short
        Dim lReturn As Integer
        Dim lRowID As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Dim sSQL3 As String

        Dim dblCommutation, dblBenefitNew As Double
        Dim dblOverPayment, dblDays As Double
        Dim lRecordCount As Integer
        Dim sExistingCommutationRate As String
        Dim sTemp As String
        Dim dDateOfEvent As Date
        Dim sAbbr As String
        Dim sWarning As String
        Try
            lLoadData = g_objXErrorMask.cSuccess

            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinPercentOfSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxPercentOfSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MaxPercentOfSAWW + objBenefitRule.MinPercentOfSAWW + objBenefitRule.LesserPercentSAWW > 0 Then
                objSAWW = New RMJuRuLib.CJRSAWW()
                'UPGRADE_WARNING: Couldn't resolve default property of object objSAWW.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lReturn = objSAWW.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then
                    'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objSAWW = Nothing
                    Exit Function
                End If
            Else
                'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objSAWW = Nothing
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseBodyMembersCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_UseBodyMembersCode = objBenefitRule.UseBodyMembersCode

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.objBenefitRule.UseBodyMembersCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Me.objBenefitRule.UseBodyMembersCode = g_lYesCodeID Then
                'load colBodyMembers
                Me.colBodyMembers = New RMJuRuLib.CJRBodyMembers()
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.colBodyMembers.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Me.colBodyMembers.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG) = -1 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.colBodyMembers.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If Me.colBodyMembers.Count = 0 Then
                        m_ErrorMask = g_objXErrorMask.cNoBodyMemberData
                    End If
                End If
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function
            End If
            'jtodd22 presume that objBenefitRule.UseTTDRateCode = g_lYesCodeID
            sAbbr = modFunctions.GetTempTotalAbbreviation(sWarning, (g_objXClaim.FilingStateID))
            If sAbbr > "" Then
                lReturn = modFunctions.GetCalculator(objTTD, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lReturn = objTTD.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function
            End If
            objWCPClaim = New CWCWCPClaim
            m_lRowID = objWCPClaim.GetRowIDMaximum(m_sBenefitTypeAbbr)

            'jtodd22 If m_lRowID = 0 then we have loaded all data
            'jtodd22 To calculate a payment we will need to write the first record of claim related data

            If m_lRowID = 0 Then Exit Function

            lReturn = objWCPClaim.LoadDataByRowID(m_lRowID, m_sBenefitTypeAbbr)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function

            If objWCPClaim.Commutation > 0 Then
                Me.LastCommutationDate_DTG = objWCPClaim.EndRecvdDateEffective
            End If

            'fetch the minimum and maximum benefits allowed for this state, event date, and effective dis rate
            lReturn = GetPPDLimits(Me.DisabilityRate_Effective)
            Select Case lReturn
                Case 0 'expected
                    Me.JurisMaxBenRateWeek = g_dblMaxBenefit
                    Me.MinBenRateWeek = g_dblMinBenefit
                Case Else 'bad thing
                    lLoadData = lReturn
                    Exit Function
            End Select

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.MinBenRateDay = Me.MinBenRateWeek / objCalcBenefitRule.JurisWorkWeek
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.JurisMaxBenRateDay = Me.JurisMaxBenRateWeek / objCalcBenefitRule.JurisWorkWeek
            If Trim(Me.BeginPrntDate_Effective & "") = "" Then
                m_PaidPendingWeeks = 0
            Else
                m_PaidPendingWeeks = GetBenefitAmountPaidOrPending2("4,5,6,16", Me.BeginPrntDate_Effective)
            End If
            Me.LastDayPPDPayable = f_dtGetLastDayPermPartialPayable((Me.WeeksPayable_Original), (Me.BeginPrntDate_Original))
            iTemp = 0
            If IsDate(Me.BeginPrntDate_Effective) Then
                sTemp = Mid(Me.BeginPrntDate_Effective, 5, 2) & "/" & Mid(Me.BeginPrntDate_Effective, 7, 2) & "/" & Mid(Me.BeginPrntDate_Effective, 1, 4)
                If Me.BenefitRate_Effective > 0 Then
                    iTemp = ((m_PaidPendingWeeks * 7) / Me.BenefitRate_Effective)
                    Me.LastDayPPDPaid = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, iTemp - 1, CDate(sTemp)))
                    'UPGRADE_WARNING: Couldn't resolve default property of object PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.LastDayPPDPaid = PadDateWithZeros(Me.LastDayPPDPaid)
                End If
            End If
            'days paid must include the Me.BeginPrntDate_Effective as it is the intitlement day jtodd22
            If Len(Me.LastCommutationDate_DTG) = 8 Then
                sTemp = Me.LastCommutationDate_DTG
                sTemp = Mid(sTemp, 5, 2) & "/" & Mid(sTemp, 7, 2) & "/" & Mid(sTemp, 1, 4)
                Me.LastCommutationDate = sTemp
            End If
            Me.Liability = (Me.BenefitRate_Effective * Me.WeeksPayable_Effective)

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:36
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Double
        Const sFunctionName As String = "lCalculatePayment"
        Dim bRateChanged As Boolean

        Dim dDateOfEvent As Date

        Dim dBasicRate As Double
        Dim dMonthsToPay As Double
        Dim dRate As Double
        Dim dWeeksToPay As Double

        Dim i2 As Short
        Dim i3 As Short

        Dim lOrderType As Integer
        Dim lPIRowID As Integer
        Dim lReturn As Integer

        Dim dblOverPayment As Double
        Dim dblDays As Double
        Dim dMaxRate As Double

        Dim dMMIRate As Double

        Dim sOrderNumber As String
        Dim sMMIDateDBFormat As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            dBasicRate = 0
            lOrderType = 0
            sOrderNumber = ""

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MMIDateRequiredCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MMIDateRequiredCode = g_lYesCodeID Then
                lReturn = modFunctions.GetMMIImpairment(dMMIRate, sMMIDateDBFormat)
                If Trim(sMMIDateDBFormat & "") = "" Then
                    m_Warning = ""
                    m_Warning = m_Warning & "A MMI date is required and was not found." & vbCrLf
                    m_Warning = m_Warning & "Please insure the Claimant has an MMI date."
                    Exit Function
                End If
            End If

            modFunctions.BuildTopOfWorkSheet(colWorkSheet)

            'jtodd22 --this is permanent partial not temporary partial
            If g_objXClaim.ClaimantOriginalAWW > 0 Then
                g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
            End If

            'jtodd22 --get rate first
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseTTDRateCode = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dRate = objTTD.Standard.GetBasicRate(g_objXClaim.AWWToCompensate, g_objXClaim.ClaimantTTDRate, g_objXClaim.JurisTaxExemptions, g_objXClaim.JurisTaxStatusCode, g_objXClaim.ClaimantTTDRate, g_objXClaim.ClaimantHourlyPayRate)
                colWorkSheet.Add("|Rate based on Temporary Total Rate|" & "Yes")
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FixedPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.FixedPercentage > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FixedPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dRate = modFunctions.RoundStandard(g_objXClaim.ClaimantTTDRate * objBenefitRule.FixedPercentage, 2)
                End If
                colWorkSheet.Add("|Rate based on Temporary Total Rate|" & "No")
            End If

            colWorkSheet.Add("|Payment Rate|" & Format(dRate, "Currency"))

            'Fetch Maximum rate
            dMaxRate = 0
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.LesserFixedAmount > 0 And objBenefitRule.LesserPercentSAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dMaxRate = objBenefitRule.LesserFixedAmount
                If Not objSAWW Is Nothing Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object objSAWW.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (objSAWW.SAWWAmount * objBenefitRule.LesserPercentSAWW) < dMaxRate Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object objSAWW.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dMaxRate = (objSAWW.SAWWAmount * objBenefitRule.LesserPercentSAWW)
                    End If
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ElseIf objBenefitRule.LesserFixedAmount = 0 And objBenefitRule.LesserPercentSAWW <> 0 Then
                If Not objSAWW Is Nothing Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object objSAWW.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dMaxRate = (objSAWW.SAWWAmount * objBenefitRule.LesserPercentSAWW)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ElseIf objBenefitRule.LesserPercentSAWW = 0 And objBenefitRule.LesserFixedAmount <> 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dMaxRate = objBenefitRule.LesserFixedAmount
            End If

            'Apply Maximum rate Cap
            If dRate > dMaxRate Then
                dRate = dMaxRate
                colWorkSheet.Add("The Maximum Rate is|" & Format(dMaxRate, "Currency"))
                colWorkSheet.Add("|Payment Rate is|" & Format(dRate, "Currency"))
            End If

            'Apply Miniumn rates
            bRateChanged = False
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EffectiveDate < m_MinimumSwitchDateDBFormat Then
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child1
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child1 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with One Child|" & Format(objBenefitRule.Child1, "Currency"))
                            End If
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child2
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child2 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with Two Children|" & Format(objBenefitRule.Child2, "Currency"))
                            End If
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child3
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child3 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with Three Children|" & Format(objBenefitRule.Child3, "Currency"))
                            End If
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child4 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child4
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child4 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with Four Children|" & Format(objBenefitRule.Child4, "Currency"))
                            End If
                        End If
                    Case Else
                        Select Case Trim(UCase(GetCodeDesc_SQL(modFunctions.GetMartialStatusCode)))
                            Case "MARRIED"
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.MarriedValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.MarriedValue
                                    bRateChanged = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If g_objXClaim.AWWToCompensate < objBenefitRule.MarriedValue Then
                                        dRate = g_objXClaim.AWWToCompensate
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        colWorkSheet.Add("|Minimum Rate Married Status|" & Format(objBenefitRule.MarriedValue, "Currency"))
                                    End If
                                End If
                            Case Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.SingleValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.SingleValue
                                    bRateChanged = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If g_objXClaim.AWWToCompensate < objBenefitRule.SingleValue Then
                                        dRate = g_objXClaim.AWWToCompensate
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        colWorkSheet.Add("|Minimum Rate Single Status|" & Format(objBenefitRule.SingleValue, "Currency"))
                                    End If
                                End If
                        End Select
                End Select
                If bRateChanged = True Then
                    colWorkSheet.Add("|Payment Rate after Minimum is|" & Format(dRate, "Currency"))
                End If
            Else
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID) + modFunctions.GetDependentSpouseCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse0
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse0 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with zero Child/Spouse|" & Format(objBenefitRule.ChildSpouse0, "Currency"))
                            End If
                        End If
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse1
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse1 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with One Child/Spouse|" & Format(objBenefitRule.ChildSpouse1, "Currency"))
                            End If
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse2
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse2 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with Two Children/Spouse|" & Format(objBenefitRule.ChildSpouse2, "Currency"))
                            End If
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse3
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse3 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with Three Children/Spouse|" & Format(objBenefitRule.ChildSpouse3, "Currency"))
                            End If
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse4P Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse4P
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse4P Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with four or more Children/Spouse|" & Format(objBenefitRule.ChildSpouse4P, "Currency"))
                            End If
                        End If
                End Select
                If bRateChanged = True Then
                    colWorkSheet.Add("|Payment Rate after Minimum is|" & Format(dRate, "Currency"))
                End If

            End If

            If m_UseBodyMembersCode = g_lYesCodeID Then
                'get collection of Claimant body members--not body parts
                lPIRowID = modBenefitFunctions.f_lGetPIRowID(g_objXClaim.ClaimID)
                lReturn = colClaimantBodyMembers.LoadData(lPIRowID)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If colClaimantBodyMembers.Count < 1 Then
                    m_Warning = "There are no Body Members for the Claimant to do the calculations."
                    'UPGRADE_NOTE: Object colClaimantBodyMembers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    colClaimantBodyMembers = Nothing
                    Exit Function
                End If
                For i2 = 1 To colClaimantBodyMembers.Count
                    'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    For i3 = 1 To colBodyMembers.Count
                        'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If colBodyMembers.Item(i3).TableRowID = colClaimantBodyMembers.Item(i2).WCPBodyMemberID Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            colBodyMembers.Item(i3).ImpairmentPercentage = colClaimantBodyMembers.Item(i2).ImpairmentPercent
                            'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If colBodyMembers.Item(i3).MaxMonths > 0 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colBodyMembers.Item(i3).LiabilityAmount = g_objXClaim.ClaimantTTDRate * (colClaimantBodyMembers.Item(i2).ImpairmentPercent * colBodyMembers.Item(i3).MaxMonths)
                            End If
                            'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If colBodyMembers.Item(i3).MaxWeeks > 0 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colBodyMembers.Item(i3).LiabilityAmount = g_objXClaim.ClaimantTTDRate * (colClaimantBodyMembers.Item(i2).ImpairmentPercent * colBodyMembers.Item(i3).MaxWeeks)
                            End If
                        End If
                    Next i3
                Next i2

                'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                For i3 = 1 To colBodyMembers.Count
                    'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If colBodyMembers.Item(i3).MaxMonths > dMonthsToPay Then dMonthsToPay = colBodyMembers.Item(i3).MaxMonths
                    'UPGRADE_WARNING: Couldn't resolve default property of object colBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If colBodyMembers.Item(i3).MaxWeeks > dWeeksToPay Then dWeeksToPay = colBodyMembers.Item(i3).MaxWeeks
                Next i3


                'payment is ttdrate for (MMI *weeks)

                ''MsgBox "Coding is not complete.", vbOKOnly, Me.Name

            Else
                'If m_UseBodyMembersCode = g_lNoCodeID Then

            End If

            If objWCPClaim.RowID = 0 Then
                'we need to create a new record
                dBasicRate = dGetBasicRate(g_objXClaim.ClaimantOriginalAWW, 0, 0, 0, 0, (g_objXClaim.ClaimantHourlyPayRate))
                dBasicRate = modFunctions.RoundStandard(dBasicRate, 2)
                If modFunctions.PutOrginalClaimBenefitData(m_sBenefitTypeAbbr, dBasicRate, sOrderNumber, lOrderType) = True Then
                    lReturn = objWCPClaim.LoadDataByRowID(objWCPClaim.GetRowIDMaximum(m_sBenefitTypeAbbr), m_sBenefitTypeAbbr)
                Else
                    m_ErrorMask = g_objXErrorMask.cNoRecord
                End If
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function
            End If


            dDateOfEvent = CDate(GetFormattedDate(g_objXClaim.DateOfEventDTG))

            'build benefit type detail
            colWorkSheet.Add("|Percentage Disability                             |" & Format(Me.DisabilityRate_Effective / 100, "Percent"))
            colWorkSheet.Add("||")
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Claimant's Original AWW|" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Me.objTTD.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                colWorkSheet.Add("|Jurisdiction Imposed Max AWW Limit|" & Format(Me.objTTD.MaxAWW, "Currency"))
            Else
                colWorkSheet.Add("|Jurisdiction Imposed Max AWW Limit|(None)")
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.ClaimantJurisAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            colWorkSheet.Add("|Claimant's AWW (After Jurisdiction Imposed Limit)|" & Format(Me.objTTD.ClaimantJurisAWW, "Currency"))
            colWorkSheet.Add("||")
            colWorkSheet.Add("|The Weekly Benefit Rate is |" & Format(Me.BenefitRate_Effective, "Currency"))
            colWorkSheet.Add("||")

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.CalculatedPayment = (Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays
            Select Case Me.RecordCount
                Case 0
                Case Else
                    dblOverPayment = Me.CalculatedPayment + m_PaidPendingWeeks - Me.EstCost_Effective
                    If dblOverPayment > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dblDays = dblOverPayment / Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek
                        If dblDays >= 1 Then
                            m_Note = m_Note & vbCrLf & "The ending payment date will result in an overpayment of liability by " & Format(System.Math.Round(dblOverPayment, 2), "#.00") & "." & "Please change the through payment date backwards by " & Int(dblDays) & " days and recalculate the payment." & "No calculation is possible."
                            Me.CalculatedPayment = 0
                        Else
                            m_Note = m_Note & vbCrLf & "There is an overpayment of liability on the though payment date." & "The payment will be reduced to avoid an overpayment."
                            Me.CalculatedPayment = Me.Liability - Me.LiabilityPaid
                        End If
                    End If
            End Select
            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
hSkip:
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function

    Private Function ClearObject() As Integer
        Const sFunctionName As String = "ClearObject"
        Try

            BenefitRate_Original = 0 'pre-commutation rate, if record was commutated
            BenefitRate_PostComm = 0 'only exists if a commutation
            BenefitRate_Effective = 0 'zero as start of LoadData
            CalculatedPayment = 0
            Commutation = 0
            DisabilityRate_Original = 0
            DisabilityRate_PostComm = 0
            DisabilityRate_Effective = 0
            EstCost_Original = 0
            EstCost_PostComm = 0
            EstCost_Effective = 0
            Liability = 0
            LiabilityPaid = 0
            WeeksPayable_Original = 0
            WeeksPayable_PostComm = 0
            WeeksPayable_Effective = 0
            BeginPrntDate_Original = vbNullString
            BeginPrntDate_PostComm = vbNullString
            BeginPrntDate_Effective = vbNullString
            EndRecdDate_Original = vbNullString
            EndRecdDate_PostComm = vbNullString
            ''end members linked to WCP_CLAIMS
            '
            LastDayPPDPayable = CDate("12/31/1899")
            WeeksPayable = 0
            BenefitDays = 0
            RecordCount = 0
            m_CheatSheetTitle = "Permanent Partial"
            m_EarningsRequiredCode = g_lNoCodeID
            m_ErrorMask = g_objXErrorMask.cSuccess
            m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
            m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
            m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
            m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
            m_MMIDateRequiredCode = g_lNoCodeID
            m_Note = ""
            m_PaidPendingMonths = 0
            m_PaidPendingWeeks = 0
            m_PayPeriodName = ""
            m_RuleTotalMonths = 0
            m_RuleTotalWeeks = 0
            m_UseBodyMembersCode = g_lNoCodeID
            m_Warning = ""

            m_CalculatedPaymentAuto = 0
            m_CalculatedPaymentCatchUp = 0
            m_CalculatedPaymentRegular = 0
            m_CalculatedPaymentWaitingPeriod = 0
            '
            JurisMaxBenRateDay = 0
            JurisMaxBenRateWeek = 0
            MinBenRateDay = 0
            MinBenRateWeek = 0


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            ClearObject = g_lErrNum

        End Try

    End Function
    Public Function GetExistingWeeklyRate_PPDBenefit() As Double
        Const sFunctionName As String = "GetExistingWeeklyRate_PPDBenefit"

        'Employee has a Permanent Partial Disability
        'The Weekly Rate can change during a Commutation
        'RiskMaster stores a rate in the table WCP_CLAIMS by CLAIM_ID and by ROW_ID
        Dim dTest As Double
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL_2 As String
        Dim rs As Short
        Try
            '***Two conditions to check
            '***1)existing record, with out commutation
            '******disability rate can change, most common
            '******may not have a 'P & S Date'
            '******beware that a claim can have multiple commutations
            '***2)existing record, with commutation
            '******disability rate does not change
            '******must have a 'P & S Date' for commutations
            '******beware that a claim can have multiple commutations
            '**********************************************************************************
            '***On each commutation an existing record is updated and a new record is created with
            '***the benefit base data.
            GetExistingWeeklyRate_PPDBenefit = 0
            '***
            Select Case Me.RecordCount
                Case 0
                    'never happens, me.objidscalc.ppd.loaddata forces first record
                Case 1
                    'have record, no commutation
                    'disability rate can change
                    dTest = 0
                    sSQL_2 = SQLSelectClaimDataWithOrderBy(m_sBenefitTypeAbbr, "DESC")
                    objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL_2)
                    If Not objReader.Read() Then
                        dTest = objReader.GetInt32("BENEFIT_RATE")
                        If dTest = 0 Then
                            dTest = GetExistingWeeklyRate_PPDBenefit
                            Call PutClaimBenefitData()
                        Else
                            GetExistingWeeklyRate_PPDBenefit = dTest
                        End If
                        GetExistingWeeklyRate_PPDBenefit = dTest
                    Else
                        'should not happen
                    End If
                Case Else
                    'have commutation, rate does not change from screen inputs
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function GetPPDWeeks() As Double
        Const sFunctionName As String = "GetPPDWeeks"

        '**************************************************************************************************
        '***note:  do not call this if commutation > 0
        '***If Me.RecordCount > 1 the percentage must not change in calling application.
        '**************************************************************************************************
        Dim sSQL As String
        Dim objReader As DbReader
        Dim dblBenefitWeeks, dblDistractor As Double
        Try
            If Me.RecordCount > 1 Then Exit Function

            sSQL = "SELECT * FROM WCP_PPD_WEEKS" & " WHERE BEGIN_DATE <= '" & g_objXClaim.DateOfEventDTG & "'" & " AND STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND (" & Me.DisabilityRate_Effective & " BETWEEN BEGIN_PERCENT AND END_PERCENT)" & " ORDER BY BEGIN_DATE DESC"

            'jlt, would expect 7 records or less to be returned, we want the one with the BEGIN_DATE
            'closest to the Date_of_Event and before the Date of Event
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                dblDistractor = objReader.GetInt32("PD_DISTRACTOR")
                GetPPDWeeks = objReader.GetInt32("BASE_WEEKS") + objReader.GetInt32("PD_MULTIPLIER") * (Me.DisabilityRate_Effective - dblDistractor)
            Else
                GetPPDWeeks = 0
            End If



        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)

            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function SQLSelectPPD_BenefitWeeks() As String
        Dim sSQL As String

        sSQL = "SELECT * FROM WCP_PPD_WEEKS" & " WHERE BEGIN_DATE <= '" & g_objXClaim.DateOfEventDTG & "'" & " AND STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND (" & Me.DisabilityRate_Effective & " BETWEEN BEGIN_PERCENT AND END_PERCENT)" & " ORDER BY BEGIN_DATE DESC"
        'jlt, would expect 7 records or less to be returned, we want the one with the BEGIN_DATE
        'closest to the Date_of_Event and before the Date of Event
        SQLSelectPPD_BenefitWeeks = sSQL



    End Function

    Public Function SQLSelectPPDClaimDataWithOrderBy(ByVal sSortOrder As String) As String
        Dim sSQL As String

        sSortOrder = Trim(UCase(sSortOrder))
        sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID & " AND BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'" & " ORDER BY ROW_ID " & sSortOrder

        SQLSelectPPDClaimDataWithOrderBy = sSQL



    End Function

    Public Function GetWeeklyRate_PPDBenefit_Calcu() As Double
        Const sFunctionName As String = "GetWeeklyRate_PPDBenefit"

        'Employee has a Permanent Partial Disability
        'The Weekly Rate can change during a Commutation
        'RiskMaster stores a rate in the table WCP_CLAIMS by CLAIM_ID and by ROW_ID
        Dim dBenefitAmount As Double
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim sSQL As String
        Try
            '***Two conditions to check
            '***1)existing record, with out commutation
            '******disability rate can change, most common
            '******may not have a 'P & S Date'
            '******beware that a claim can have multiple commutations
            '***2)existing record, with commutation
            '******disability rate does not change
            '******must have a 'P & S Date' for commutations
            '******beware that a claim can have multiple commutations
            '**********************************************************************************
            '***On each commutation an existing record is updated and a new record is created with
            '***the benefit base data.
            GetWeeklyRate_PPDBenefit_Calcu = 0
            '***
            Select Case Me.RecordCount
                Case Is < 2
                    'have record, no commutation
                    'disability rate can change
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.objTTD.ClaimantJurisAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBenefitAmount = modFunctions.RoundStandard(CDbl((Me.objTTD.ClaimantJurisAWW * 2) / 3), 2)
                    'fetch the minimum and maximum benefits allowed for this state, event date, and effective dis rate
                    lReturn = GetPPDLimits(Me.DisabilityRate_Effective)
                    Select Case lReturn
                        Case 0 'expected
                            Me.JurisMaxBenRateWeek = g_dblMaxBenefit
                            Me.MinBenRateWeek = g_dblMinBenefit
                        Case Else 'bad thing
                    End Select

                    Select Case dBenefitAmount
                        Case Is < Me.MinBenRateWeek
                            dBenefitAmount = Me.MinBenRateWeek
                        Case Is > Me.JurisMaxBenRateWeek
                            dBenefitAmount = Me.JurisMaxBenRateWeek
                        Case Else
                            dBenefitAmount = dBenefitAmount
                    End Select
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.JurisMaxBenRateDay = Me.JurisMaxBenRateWeek / objCalcBenefitRule.JurisWorkWeek
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.MinBenRateDay = Me.MinBenRateWeek / objCalcBenefitRule.JurisWorkWeek

                    Me.BenefitRate_Effective = modFunctions.RoundStandard(dBenefitAmount, 2)
                    GetWeeklyRate_PPDBenefit_Calcu = modFunctions.RoundStandard(dBenefitAmount, 2)
                Case Else
                    'have commutation, rate does not change from screen inputs
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePPDSLibility
    ' DateTime  : 1/29/2005 12:16
    ' Author    : jtodd22
    ' Purpose   : To return an object that lists the Body Member related libility for the event
    '---------------------------------------------------------------------------------------
    Public Function CalculatePPDSLiability(ByRef objBodyMembers As Object) As Integer
        Const sFunctionName As String = "CalculatePPDSLiability"
        Dim i As Short
        Dim lTest As Integer
        'UPGRADE_NOTE: Rate was upgraded to Rate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Rate_Renamed As Double
        Try
            CalculatePPDSLiability = 0
            Rate_Renamed = GetPPDSRate()
            If Rate_Renamed = 0 Then
                'puke
                Exit Function
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseImpairPercentCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseImpairPercentCode = g_lYesCodeID Then
                With objBodyMembers
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    For i = 1 To objBodyMembers.Count
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If .Item(i).MaxMonths = 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Item(i).LiabilityAmount = .Item(i).ImpairmentPercentage * .Item(i).MaxWeeks * Rate_Renamed
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Item(i).LiabilityAmount = .Item(i).ImpairmentPercentage * .Item(i).MaxMonths * Rate_Renamed
                        End If
                    Next i
                End With
            End If
            CalculatePPDSLiability = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            CalculatePPDSLiability = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Private Function GetPPDSRate() As Double
        Const sFunctionName As String = "GetPPDSRate"
        Dim lTest As Integer
        Dim objRule2 As Object
        Dim objRule3 As Object
        'Dim objBenefitRule As RMJuRuLib.CJRBenefitRuleSPPDIL
        'Dim objRule2 As RMJuRuLib.CJRSAWW
        'Dim objRule3 As RMJuRuLib.CJRBenefitRuleTTD
        'UPGRADE_NOTE: Rate was upgraded to Rate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Rate_Renamed As Double
        Try
            GetPPDSRate = 0
            objBenefitRule = New RMJuRuLib.CJRBenefitRuleSPPDIL()
            'Set objBenefitRule = New RMJuRuLib.CJRBenefitRuleSPPDIL
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lTest = objBenefitRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            If lTest <> -1 Then
                'puke
                Exit Function
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseTTDRateCode = g_lYesCodeID Then
                GetPPDSRate = g_objXClaim.ClaimantTTDRate
                Exit Function
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FixedPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.FixedPercentage > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FixedPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                GetPPDSRate = modFunctions.RoundStandard(g_objXClaim.ClaimantTTDRate * (objBenefitRule.FixedPercentage / 100), 2)
                Exit Function
            End If
            'Private m_LesserFixedAmount As Double
            'Private m_LesserPercentSAWW As Double
            'Private m_PercentOfMaxTTD As Double
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.LesserFixedAmount > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Rate_Renamed > objBenefitRule.LesserFixedAmount Then Rate_Renamed = objBenefitRule.LesserFixedAmount
            End If
            objRule2 = New RMJuRuLib.CJRSAWW()
            'Set objRule2 = New RMJuRuLib.CJRSAWW
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.LesserPercentSAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lTest = objRule2.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
                If lTest <> -1 Then
                    'puke
                    Exit Function
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If Rate_Renamed > objRule2.SAWWAmount Then Rate_Renamed = objRule2.SAWWAmount
                End If
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PercentOfMaxTTD. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PercentOfMaxTTD = g_lYesCodeID Then
                objRule3 = New RMJuRuLib.CJRBenefitRuleTTDAA()
                'Set objRule3 = New RMJuRuLib.CJRBenefitRuleTTD
                'UPGRADE_WARNING: Couldn't resolve default property of object objRule3.LoadData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lTest = objRule3.LoadData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
                If lTest <> -1 Then
                    'puke
                    Exit Function
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objRule3.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objRule3.MaxCompRate > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objRule3.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Rate_Renamed = objRule3.MaxCompRate
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object objRule3.UseSAWWMaximumCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objRule3.UseSAWWMaximumCode = g_lYesCodeID Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.MaxAmountAsPublished. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If objRule2.MaxAmountAsPublished > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.MaxAmountAsPublished. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Rate_Renamed = objRule2.MaxAmountAsPublished
                        End If
                        'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.PercentMax. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If objRule2.PercentMax > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.PercentMax. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Rate_Renamed = objRule2.PercentMax * objRule2.SAWWAmount
                        End If
                    End If
                End If
            End If
            GetPPDSRate = modFunctions.RoundStandard(Rate_Renamed, 4)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objBenefitRule = Nothing
            'UPGRADE_NOTE: Object objRule2 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objRule2 = Nothing
            'UPGRADE_NOTE: Object objRule3 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objRule3 = Nothing


        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dMaxRate As Double
        Dim dRate As Double
        Dim lReturn As Integer
        Dim lReturn2 As Integer

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            dMaxRate = 0

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseTTDRateCode = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dRate = objTTD.Standard.GetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FixedPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.FixedPercentage > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FixedPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dRate = modFunctions.RoundStandard(TempTotalRate * objBenefitRule.FixedPercentage, 2)
                End If
            End If

            'Fetch Maximum rate
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.LesserFixedAmount > 0 And objBenefitRule.LesserPercentSAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dMaxRate = objBenefitRule.LesserFixedAmount
                If Not objSAWW Is Nothing Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object objSAWW.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (objSAWW.SAWWAmount * objBenefitRule.LesserPercentSAWW) < dMaxRate Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object objSAWW.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dMaxRate = (objSAWW.SAWWAmount * objBenefitRule.LesserPercentSAWW)
                    End If
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ElseIf objBenefitRule.LesserFixedAmount = 0 And objBenefitRule.LesserPercentSAWW <> 0 Then
                If Not objSAWW Is Nothing Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object objSAWW.SAWWAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dMaxRate = (objSAWW.SAWWAmount * objBenefitRule.LesserPercentSAWW)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserPercentSAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            ElseIf objBenefitRule.LesserPercentSAWW = 0 And objBenefitRule.LesserFixedAmount <> 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LesserFixedAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dMaxRate = objBenefitRule.LesserFixedAmount
            End If

            'Apply Maximum rate Cap
            If dRate > dMaxRate Then
                dRate = dMaxRate
            End If

            If g_objXClaim.ClaimantOriginalAWW > 0 Then
                g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
            Else
                g_objXClaim.AWWToCompensate = AverageWage
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EffectiveDate < m_MinimumSwitchDateDBFormat Then
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child1
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child1 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child2
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child2 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child3
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child3 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child4 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child4
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child4 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case Else
                        Select Case Trim(UCase(GetCodeDesc_SQL(modFunctions.GetMartialStatusCode)))
                            Case "MARRIED"
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.MarriedValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.MarriedValue
                                    If g_objXClaim.AWWToCompensate < dRate Then dRate = g_objXClaim.AWWToCompensate
                                End If
                            Case Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.SingleValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.SingleValue
                                    If g_objXClaim.AWWToCompensate < dRate Then dRate = g_objXClaim.AWWToCompensate
                                End If
                        End Select
                End Select
            Else
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID) + modFunctions.GetDependentSpouseCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse0
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse0 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse1
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse1 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse2
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse2 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse3
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse3 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse4P Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse4P
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse4P Then dRate = g_objXClaim.AWWToCompensate
                        End If
                End Select
            End If

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function GetBasicRateTemp(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer) As Double
        Const sFunctionName As String = "GetBasicRateTemp"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dMaxRate As Double
        Dim lReturn As Integer
        Dim objSpendable As Object

        Try

            GetBasicRateTemp = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
                If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            End If

            lReturn = GetSpendableIncomeRule()
            Select Case lReturn
                Case -1 'spendable income jurisdiction
                    lReturn = modFunctions.GetRateSpendable(dBasicRate, m_ErrorMaskSpendData, AverageWage)
                    If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSAWW + m_ErrorMaskSpendData) > g_objXErrorMask.cSuccess Then Exit Function

                Case 0 'discounted jurisdiction
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = modFunctions.RoundStandard(g_objXClaim.ClaimantOriginalAWW * objBenefitRule.PrimeRate, 2)
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.FloorAmount > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If objBenefitRule.DollarForDollar <> g_lYesCodeID Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If dBasicRate < objBenefitRule.FloorAmount Then Me.BenefitRate_Effective = objBenefitRule.FloorAmount
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If objBenefitRule.MaxCompRate > 0 And dBasicRate > objBenefitRule.MaxCompRate Then dBasicRate = objBenefitRule.MaxCompRate
                            Exit Function
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Select Case objBenefitRule.FloorAmount
                                Case Is >= g_objXClaim.ClaimantOriginalAWW
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                                        dBasicRate = modFunctions.RoundStandard((g_objXClaim.ClaimantOriginalAWW), 2)
                                    End If
                                Case Is >= (g_objXClaim.ClaimantOriginalAWW * objBenefitRule.PrimeRate)
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dBasicRate = modFunctions.RoundStandard(g_objXClaim.ClaimantOriginalAWW * objBenefitRule.PrimeRate, 2)
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If dBasicRate < objBenefitRule.FloorAmount Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        dBasicRate = objBenefitRule.FloorAmount
                                    End If
                            End Select
                        End If
                    End If
                    With objBenefitRule
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If .MaxCompRate > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If dBasicRate > .MaxCompRate Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dBasicRate = .MaxCompRate
                            End If
                        End If
                    End With

            End Select


            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If (dBasicRate < objBenefitRule.FloorAmount) Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = objBenefitRule.FloorAmount
                End If
            End If

            'Added for discounted jurisdiction.
            'Apply Maximum check for discounted jurisdiction..Tanuj
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If (dBasicRate > objBenefitRule.MaxCompRate And objBenefitRule.MaxCompRate > 0) Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dBasicRate = objBenefitRule.MaxCompRate
            End If

            'Apply more checks on above calculated rate..
            'goes as follows..
            GetBasicRateTemp = dBasicRate
        Catch ex As TypeLoadException
            'hLoadDataError:
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing



        End Try

    End Function

    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

