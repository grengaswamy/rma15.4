Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCTempTotalTEFlorida
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "TPD"
    Const m_sBenefittypeAbbr2 As String = "TTE"
    Const sClassName As String = "CWCTempTotalTEFlorida"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleTTDFl"
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objSpendableIncome As Object
    Dim objSpendableIncomeRule As Object

    Private m_BenefitRate_Effective As Double
    Private m_ClaimantJurisAWW As Double
    Private m_ClaimantOriginalAWW As Double

    Public BenefitRate_Original As Double


    Private m_JurisMaxBenRateWeek As Double
    Private m_JurisMaxBenRateDay As Double
    Private m_JurisMinBenRateWeek As Double
    Private m_JurisMinBenRateDay As Double

    Public PayLateCharge As Double
    Public TwoYearStartDateCalendar As String
    Public TwoYearStartDateDTG As String
    Public BenefitRate_TTD As Double
    Public BenefitRate_TTD_CA_TwoYear As Double
    Public BenefitRate_TPD As Double
    Private m_WaitingPeriodIsPaid As Short

    Public Property WaitingPeriodIsPaid() As Short
        Get
            WaitingPeriodIsPaid = m_WaitingPeriodIsPaid
        End Get
        Set(ByVal Value As Short)
            m_WaitingPeriodIsPaid = Value
        End Set
    End Property

    Public Property BenefitRate_Effective() As Double
        Get
            BenefitRate_Effective = m_BenefitRate_Effective
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_Effective = Value
        End Set
    End Property


    Public Property ClaimantJurisAWW() As Double
        Get
            ClaimantJurisAWW = m_ClaimantJurisAWW
        End Get
        Set(ByVal Value As Double)
            m_ClaimantJurisAWW = Value
        End Set
    End Property

    Public Property ClaimantOriginalAWW() As Double
        Get
            ClaimantOriginalAWW = m_ClaimantOriginalAWW
        End Get
        Set(ByVal Value As Double)
            m_ClaimantOriginalAWW = Value
        End Set
    End Property

    Public Property JurisMaxBenRateDay() As Double
        Get
            JurisMaxBenRateDay = m_JurisMaxBenRateDay
        End Get
        Set(ByVal Value As Double)
            m_JurisMaxBenRateDay = Value
        End Set
    End Property

    Public Property JurisMaxBenRateWeek() As Double
        Get
            JurisMaxBenRateWeek = m_JurisMaxBenRateWeek
        End Get
        Set(ByVal Value As Double)
            m_JurisMaxBenRateWeek = Value
        End Set
    End Property

    Public Property JurisMinBenRateDay() As Double
        Get
            JurisMinBenRateDay = m_JurisMinBenRateDay
        End Get
        Set(ByVal Value As Double)
            m_JurisMinBenRateDay = Value
        End Set
    End Property

    Public Property JurisMinBenRateWeek() As Double
        Get
            JurisMinBenRateWeek = m_JurisMinBenRateWeek
        End Get
        Set(ByVal Value As Double)
            m_JurisMinBenRateWeek = Value
        End Set
    End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property
    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property

    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property


    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Temporary Total"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim dRate As Double
        Dim lReturn As Integer
        Dim sTemp As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            dRate = 0

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            If m_EarningsRequiredCode = g_lYesCodeID Then
                If g_objXPaymentParms.EarningsPerWeek = 0 And g_objXPaymentParms.EarningsPerMonth = 0 Then
                    m_Warning = "Claimant does not have earnings and earnings are required."
                    Exit Function
                End If
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = "Claimant has been paid or has pending payment " & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks of TTP or TPD." & vbCrLf & "There is no Temporary liability left to pay."
                    Exit Function
                End If
            End If

            'build top of work sheet as needed
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            If colWorkSheet.Count() = 0 Then
                'build top of work sheet
                'build benefit type detail
            Else
                colWorkSheet.Add("||")
            End If


            g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                colWorkSheet.Add("|State Imposed Max AWW Limit                       |" & Format(objBenefitRule.MaxAWW, "Currency"))
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.AWWToCompensate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                g_objXClaim.AWWToCompensate = objBenefitRule.AWWToCompensate
                colWorkSheet.Add("|AWW to compensate      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If

            If g_objXPaymentParms.EarningsPerWeek > 0 Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|The Earnings per week are | " & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))
                g_objXClaim.AWWToCompensate = g_objXClaim.AWWToCompensate - g_objXPaymentParms.EarningsPerWeek
                colWorkSheet.Add("|AWW to compensate      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.DollarForDollar <> g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDateDTG. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                colWorkSheet.Add("|Jurisdiction imposed a minimum TTD Rate of " & Format(objBenefitRule.FloorAmount, "Currency") & " per week for events after " & sDBDateFormat(objBenefitRule.EffectiveDateDTG, "Short Date") & ".  For calculations this is the same as a minimum AWW of " & Format(objBenefitRule.FloorAmount / objBenefitRule.PrimeRate, "Currency") & " per Week.|")

                colWorkSheet.Add("|Claimant AWW (After Imposing State Cap and Floor Limits)      |" & Format(Me.ClaimantJurisAWW, "Currency"))
            Else
                colWorkSheet.Add("|Claimant AWW As Capped (State Imposed Limit)      |" & Format(Me.ClaimantJurisAWW, "Currency"))
            End If

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function


            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim lReturn As Integer

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If


            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MaxCompRate > 0 And dBasicRate > objBenefitRule.MaxCompRate Then dBasicRate = objBenefitRule.MaxCompRate
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If dBasicRate < objBenefitRule.MinCompRate Then dBasicRate = objBenefitRule.MinCompRate

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function

    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Try
            Dim objReader As DbReader
            Dim dblNewClaimantOriginalAWW As Double
            Dim sStartDate As String
            Dim dbl As Double
            Dim dDateOfEvent As Date
            Dim lReturn As Integer
            Dim sDateOfEvent_DTG As String
            Dim sDummyDate_DTG As String
            Dim sSQL As String
            Dim sTemp As String

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            m_EarningsRequiredCode = g_lYesCodeID
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)

            Me.ClaimantOriginalAWW = g_objXClaim.ClaimantOriginalAWW
            Me.ClaimantJurisAWW = g_objXClaim.ClaimantOriginalAWW
            'jlt 07/29/2003  cap aww if over the jurisdiction max aww
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If g_objXClaim.ClaimantOriginalAWW > objBenefitRule.MaxAWW And objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Me.ClaimantJurisAWW > objBenefitRule.MaxAWW Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.ClaimantJurisAWW = objBenefitRule.MaxAWW
                End If
            End If

            GrateBeneRates()

            'jtodd22 beware the two year rule applies to payments, not to rates in general
            'jtodd22 reload the rates only if the whole payment is affected
            'jtodd22 if a payment is split the CalculatePayment function must handle it
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayCurrentRateAfterTwoYears. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PayCurrentRateAfterTwoYears = g_lYesCodeID Then
                GetTwoYearStartDateCalendar()
                sTemp = GetTwoYearStartDateCalendar()
                Me.TwoYearStartDateCalendar = sTemp
                Me.TwoYearStartDateDTG = Mid(sTemp, 7, 4) & Mid(sTemp, 1, 2) & Mid(sTemp, 4, 2)

                'UPGRADE_WARNING: Couldn't resolve default property of object PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sDummyDate_DTG = PadDateWithZeros(g_objXPaymentParms.BenefitStartDate)
                sDummyDate_DTG = Mid(sDummyDate_DTG, 7, 4) & Mid(sDummyDate_DTG, 1, 2) & Mid(sDummyDate_DTG, 4, 2)
                If sDummyDate_DTG >= Me.TwoYearStartDateDTG Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    objBenefitRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, Me.TwoYearStartDateDTG)
                    GrateBeneRates()
                End If
            End If
            Me.JurisMinBenRateWeek = g_dblMinBenefit
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.JurisMinBenRateDay = modFunctions.RoundStandard(g_dblMinBenefit / objCalcBenefitRule.JurisWorkWeek, 2)

            sTemp = modFunctions.GetBenefitLookUpIDs("TTE", "TPD")
            If Len(sTemp) > 1 Then
                m_PaidPendingWeeks = modBenefitFunctions.GetBenefitWeeksPaidOrPending(sTemp)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function TwoYearPaymentRule() As Short
        Const sFunctionName As String = "TwoYearPaymentRule"
        Try
            'jlt, 03/23/2001 California has a rule where payments made two (2) years after an event
            'date are calculated at current rates
            Dim sTemp As String

            m_Note = ""
            Select Case CDate(Me.TwoYearStartDateCalendar)
                Case Is > CDate(g_objXPaymentParms.BenefitEndDate)
                    'two year rule does not apply to whole payment period
                    'whole payment period is before the two year anniversary
                    TwoYearPaymentRule = g_objXErrorMask.cPreCATwoYear
                Case Is <= CDate(g_objXPaymentParms.BenefitStartDate)
                    'two year rule does apply to whole payment period
                    'whole payment period is past the two year anniversary
                    If Me.BenefitRate_Effective = Me.BenefitRate_TTD_CA_TwoYear Then
                        TwoYearPaymentRule = g_objXErrorMask.cNoRateChange
                        m_Note = "Dates of Payment are over two years from Date of Event.  There was no change in Payment Rates."
                    Else
                        TwoYearPaymentRule = g_objXErrorMask.cRateChanged
                        m_Note = "Dates of Payment are over two years from Date of Event.  There is a change in Payment Rates, the change was used in payment calculations"
                    End If
                Case Else
                    'two year rule does apply to part of payment period
                    If Me.BenefitRate_TTD_CA_TwoYear = Me.BenefitRate_Effective Then
                        m_Note = "Dates of Payment include two year anniversary.  There was no change in Payment Rates."
                        TwoYearPaymentRule = g_objXErrorMask.cNoRateChange + g_objXErrorMask.cSplitPayment
                    Else
                        m_Note = "Dates of Payment include two year anniversary." & vbCrLf & "There is a change in Payment Rates."
                        TwoYearPaymentRule = g_objXErrorMask.cSplitPayment
                    End If

            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function ReLoadData_TwoYearRule() As Integer
        Const sFunctionName As String = "ReLoadData_TwoYearRule"
        Try
            Dim objReader As DbReader
            Dim sSQL As String
            Dim g_dblMaxBenefit As Double
            Dim dblMaxAWW As Double
            Dim dblMinBenefit As Double
            Dim sStartDate As String
            Dim dblCalculatedPayment As Double
            Dim dbl As Double
            Dim dDateOfEvent As Date

            sSQL = "SELECT MAX_BENEFIT, MAX_AWW, MIN_BENEFIT FROM WCP_RULE_TTD" & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND '" & Me.TwoYearStartDateDTG & "' >= BEGIN_DATE" & " ORDER BY BEGIN_DATE DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                g_dblMaxBenefit = objReader.GetInt32("MAX_BENEFIT")
                dblMaxAWW = objReader.GetInt32("MAX_AWW")
                dblMinBenefit = objReader.GetInt32("MIN_BENEFIT")
            Else
                Err.Raise(vbObjectError + 7000, "ReLoadData_TwoYearRule", "Unable to retrieve TTD Rates")
            End If


            Me.ClaimantJurisAWW = f_dGetClaimantWeeklyWage((g_objXClaim.ClaimID), 0)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objBenefitRule.MaxAWW = dblMaxAWW

            Select Case UCase(g_objXClaim.FilingStatePostalCode)
                Case "CA"
                    'jlt, 03/23/2001 California uses the employee AWW (m_AWW_WeeklyRate) directly in
                    'calculation where the AWW is <= a state set value (m_dttdJurisdictionMaxAww) otherwise the
                    'state set max value (m_dttdJurisdictionMaxAww) is used
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If Me.ClaimantJurisAWW > objBenefitRule.MaxAWW Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Me.ClaimantJurisAWW = objBenefitRule.MaxAWW
                    Else
                    End If
                    Me.BenefitRate_Effective = modFunctions.RoundStandard(Me.ClaimantJurisAWW * 2 / 3, 2)
                    'jlt 10/09/2002 California says if the Claimant's AWW is below the min. rate use the AWW
                    ' as the rate, in the calculation of payment insure that a Claimant's payment does not go below the
                    ' min rate if it calculates below the min.
                    Select Case Me.ClaimantJurisAWW
                        Case Is <= dblMinBenefit
                            Me.BenefitRate_Effective = modFunctions.RoundStandard((Me.ClaimantJurisAWW), 2)
                        Case Is <= (dblMinBenefit * 1.5)
                            Me.BenefitRate_Effective = modFunctions.RoundStandard(Me.ClaimantJurisAWW * 2 / 3, 2)
                            If Me.BenefitRate_Effective < dblMinBenefit Then
                                Me.BenefitRate_Effective = dblMinBenefit
                            End If
                        Case Else
                    End Select
                Case Else
            End Select
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            ReLoadData_TwoYearRule = g_lErrNum

        End Try

    End Function

    Private Sub GrateBeneRates()
        Const sRoutineName As String = "GrateBeneRates"
        Dim lReturn As Integer
        Try

            lReturn = GetSpendableIncomeRule()
            If lReturn = -1 Then GrateBeneRatesSpendableIncome()
            If lReturn = 0 Then
                lReturn = modFunctions.GrateBeneRatesDiscountedAWW(Me)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Sub

    Public Function GrateBeneRatesSpendableIncome(Optional ByRef AWW As Integer = 0, Optional ByRef TaxExemptions As Integer = 0, Optional ByRef TaxStatusCode As Integer = 0) As Integer
        Const sFunctionName As String = "GrateBeneRatesSpendableIncome"
        Dim dRate As Double
        Dim lReturn As Integer

        Try

            GrateBeneRatesSpendableIncome = 0

            If objBenefitRule Is Nothing Then
                lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTE", objBenefitRule, sDLLClassNameJRRule)
                'error messages are generated in GetBenefitRuleTT
                If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            End If

            If TaxStatusCode > 0 Then
                g_objXClaim.JurisTaxStatusCode = TaxStatusCode
                g_objXClaim.JurisTaxExemptions = TaxExemptions
                g_objXClaim.ClaimantOriginalAWW = AWW
            End If

            lReturn = modFunctions.GetRateSpendable(dRate, m_ErrorMaskSpendData, g_objXClaim.ClaimantOriginalAWW)
            If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSAWW + m_ErrorMaskSpendData) > g_objXErrorMask.cSuccess Then Exit Function
            BenefitRate_TTD = dRate

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.JurisMaxBenRateWeek = objBenefitRule.MaxCompRate
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.JurisMaxBenRateDay = modFunctions.RoundStandard(objBenefitRule.MaxCompRate / objCalcBenefitRule.JurisWorkWeek, 2)
            Me.JurisMinBenRateWeek = 0
            Me.JurisMinBenRateDay = 0
            Me.BenefitRate_Effective = BenefitRate_TTD

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Me.BenefitRate_TTD > objBenefitRule.MaxCompRate And objBenefitRule.MaxCompRate > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.BenefitRate_Effective = objBenefitRule.MaxCompRate
            End If

            GrateBeneRatesSpendableIncome = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GrateBeneRatesSpendableIncome = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function


    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objSpendableIncome may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncome = Nothing
        'UPGRADE_NOTE: Object objSpendableIncomeRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncomeRule = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

