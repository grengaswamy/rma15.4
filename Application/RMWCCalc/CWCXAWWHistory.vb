Option Strict Off
Option Explicit On
Public Class CWCXAWWHistory
    '---------------------------------------------------------------------------------------
    ' Module    : CWCXAWWHistory
    ' DateTime  : 11/10/2006 08:25
    ' Author    : jtodd22
    ' Purpose   : A base class for the CWCXAWWHistories Class
    '---------------------------------------------------------------------------------------


    Private m_AWW As Double
    Private m_DateTimeStamp As String
    Private m_TableRowID As Integer
    Private m_TTDRate As Double

    Public Function SaveNewOnly() As Integer



    End Function

    Public Property AWW() As Double
        Get
            AWW = m_AWW
        End Get
        Set(ByVal Value As Double)
            m_AWW = Value
        End Set
    End Property

    Public Property DateTimeStamp() As String
        Get
            DateTimeStamp = m_DateTimeStamp
        End Get
        Set(ByVal Value As String)
            m_DateTimeStamp = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property TTDRate() As Double
        Get
            TTDRate = m_TTDRate
        End Get
        Set(ByVal Value As Double)
            m_TTDRate = Value
        End Set
    End Property
End Class

