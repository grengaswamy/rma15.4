Option Strict Off
Option Explicit On
Public Class CWCEnumPPDMask
    Public cppdBenefitRateChanged As Short
    Public cppdLiabilityPaid As Short
    Public cppdLiabilityOverPaid As Short
    Public cppdInvalidDisPercent As Short
    Public cppdPermStationaryDateChanged As Short
    Public cppdDisabilityRateChanged As Short


    Public Function SetConstantValues() As Integer
        cppdBenefitRateChanged = 1
        cppdLiabilityPaid = 2
        cppdLiabilityOverPaid = 4
        cppdInvalidDisPercent = 8
        cppdPermStationaryDateChanged = 16
        cppdDisabilityRateChanged = 32


    End Function
End Class

