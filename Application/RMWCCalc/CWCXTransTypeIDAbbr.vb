Option Strict Off
Option Explicit On
Public Class CWCXTransTypeIDAbbr
    Const sClassName As String = "CWCXTransTypeIDAbbr"

    Dim m_TransTypeCodeID As Integer
    Dim m_Abbreviation As String

    Public Property TransTypeCodeID() As Integer
        Get
            TransTypeCodeID = m_TransTypeCodeID
        End Get
        Set(ByVal Value As Integer)
            m_TransTypeCodeID = Value
        End Set
    End Property

    Public Property Abbreviation() As String
        Get
            Abbreviation = m_Abbreviation
        End Get
        Set(ByVal Value As String)
            m_Abbreviation = Value
        End Set
    End Property
End Class

