Option Strict Off
Option Explicit On
Public Class CWCXPaymentParm
    Const sClassName As String = "CWCXPaymentParm"
    Private m_AutoDays As Integer
    Private m_AutoMonths As Integer
    Private m_AutoEndDate As String
    Private m_AutoEndDateFirstPayt As String

    Private m_AutoEndDate02 As String
    Private m_AutoNumberOfPayments As Integer
    Private m_AutoNumberOfPayments02 As Integer
    Private m_AutoStartDate As String
    Private m_AutoStartDate02 As String
    Private m_BenefitDays As Integer
    Private m_BenefitMonths As Integer

    Private m_BenefitEndDate As String
    Private m_BenefitStartDate As String
    Private m_CatchUpDays As Integer
    Private m_CatchUpMonths As Integer
    Private m_CatchUpEndDate As String
    Private m_CatchUpEndDate02 As String
    Private m_CatchUpNumberOfPayments As Integer
    Private m_CatchUpNumberOfPayments02 As Integer
    Private m_CatchUpStartDate As String
    Private m_CatchUpStartDate02 As String
    Private m_EarningsPerWeek As Double
    Private m_EarningsPerMonth As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentLateCharge02 As Double
    Private m_PayTypeShortCode As String

    Private m_PenaltyDays As Integer
    Private m_PenaltyMonths As Integer
    Private m_PenaltyEndDate As String
    Private m_PenaltyEndDate02 As String
    Private m_PenaltyNumberOfPayments As Integer
    Private m_PenaltyNumberOfPayments02 As Integer
    Private m_PenaltyStartDate As String
    Private m_PenaltyStartDate02 As String

    Private m_RegularDays As Integer
    Private m_RegularMonths As Integer
    Private m_RegularEndDate As String
    Private m_RegularEndDate02 As String
    Private m_RegularNumberOfPayments As Integer
    Private m_RegularNumberOfPayments02 As Integer
    Private m_RegularStartDate As String
    Private m_RegularStartDate02 As String
    Private m_TotalNumberofPayments As Integer
    Private m_WaitingDays As Integer
    Private m_WaitingMonths As Integer
    Private m_WaitingEndDate As String
    Private m_WaitingNumberOfPayments As Integer
    Private m_WaitingStartDate As String

    Public Function GetAutoDays() As Integer
        GetAutoDays = 0
        Select Case Trim(m_PayTypeShortCode)
            Case "BI"
                GetAutoDays = 14 'Bi - Weekly
            Case "PD" 'Day
                GetAutoDays = 1
            Case "PW" 'Week
                GetAutoDays = 7
            Case "PM" 'Month
                GetAutoDays = 0 'jtodd22 -Month does not convert to days
            Case "PQ" 'Quarter
                GetAutoDays = 0 'jtodd22 -Quarter does not convert to days
            Case "PY" 'Year
                GetAutoDays = 0 'jtodd22 -Year does not convert to days (Remember leap year)
            Case "PB" 'Bi - Weekly
                GetAutoDays = 14
        End Select


    End Function
    Private Function GetAutoEndDate() As String
        GetAutoEndDate = ""
        If Trim(m_AutoStartDate) > "" Then
            If m_AutoDays = 0 Then
                m_AutoDays = GetAutoDays()
            End If
            If m_AutoDays > 0 Then
                GetAutoEndDate = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, m_AutoDays - 1, CDate(m_AutoStartDate)))
            End If
        End If


    End Function
    Private Function GetAutoEndDate02() As String
        GetAutoEndDate02 = ""
        If Trim(m_AutoStartDate) > "" Then
            If m_AutoDays = 0 Then
                m_AutoDays = GetAutoDays()
            End If
            If m_AutoDays > 0 Then
                GetAutoEndDate02 = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, m_AutoDays - 1, CDate(m_AutoStartDate)))
            End If
        End If


    End Function

    Public Function ClearObject() As Integer
        m_AutoDays = 0
        m_AutoMonths = 0
        m_AutoEndDate = vbNullString
        m_AutoEndDate02 = vbNullString
        m_AutoNumberOfPayments = 0
        m_AutoNumberOfPayments02 = 0
        m_AutoStartDate = vbNullString
        m_AutoStartDate02 = vbNullString
        m_BenefitDays = 0
        m_BenefitMonths = 0
        m_CatchUpDays = 0
        m_CatchUpMonths = 0
        m_CatchUpEndDate = vbNullString
        m_CatchUpEndDate02 = vbNullString
        m_CatchUpNumberOfPayments = 0
        m_CatchUpNumberOfPayments02 = 0
        m_CatchUpStartDate = vbNullString
        m_CatchUpStartDate02 = vbNullString
        m_EarningsPerMonth = 0
        m_EarningsPerWeek = 0
        m_CalculatedPaymentLateCharge = 0
        m_PayTypeShortCode = vbNullString
        m_RegularDays = 0
        m_RegularMonths = 0
        m_RegularEndDate = vbNullString
        m_RegularEndDate02 = vbNullString
        m_RegularNumberOfPayments = 0
        m_RegularNumberOfPayments02 = 0
        m_RegularStartDate = vbNullString
        m_RegularStartDate02 = vbNullString
        m_WaitingDays = 0
        m_WaitingMonths = 0
        m_WaitingEndDate = vbNullString
        m_WaitingNumberOfPayments = 0
        m_WaitingStartDate = vbNullString



    End Function
    Public Property BenefitDays() As Integer
        Get
            BenefitDays = m_BenefitDays
        End Get
        Set(ByVal Value As Integer)
            m_BenefitDays = Value
        End Set
    End Property

    Public Property BenefitMonths() As Integer
        Get
            BenefitMonths = m_BenefitMonths
        End Get
        Set(ByVal Value As Integer)
            m_BenefitMonths = Value
        End Set
    End Property

    Public Property BenefitEndDate() As String
        Get
            BenefitEndDate = m_BenefitEndDate
        End Get
        Set(ByVal Value As String)
            m_BenefitEndDate = Value
        End Set
    End Property

    Public Property BenefitStartDate() As String
        Get
            BenefitStartDate = m_BenefitStartDate
        End Get
        Set(ByVal Value As String)
            m_BenefitStartDate = Value
        End Set
    End Property

    Public Property AutoDays() As Integer
        Get
            If m_AutoDays = 0 And m_AutoNumberOfPayments > 0 Then
                AutoDays = GetAutoDays()
            Else
                AutoDays = m_AutoDays
            End If
        End Get
        Set(ByVal Value As Integer)
            m_AutoDays = Value
        End Set
    End Property


    Public Property AutoMonths() As Integer
        Get
            AutoMonths = m_AutoMonths
        End Get
        Set(ByVal Value As Integer)
            m_AutoMonths = Value
        End Set
    End Property

    Public Property AutoEndDate() As String
        Get
            If Not Trim(m_PayTypeShortCode) = "PM" Then
                AutoEndDate = GetAutoEndDate()
            Else
                AutoEndDate = m_AutoEndDate
            End If
        End Get
        Set(ByVal Value As String)
            m_AutoEndDate = Value
        End Set
    End Property

    Public Property AutoEndDate02() As String
        Get
            AutoEndDate02 = GetAutoEndDate02()
        End Get
        Set(ByVal Value As String)
            m_AutoEndDate02 = Value
        End Set
    End Property


    Public Property AutoEndDateFirstPayt() As String
        Get
            AutoEndDateFirstPayt = m_AutoEndDateFirstPayt
        End Get
        Set(ByVal Value As String)
            m_AutoEndDateFirstPayt = Value
        End Set
    End Property

    Public Property AutoStartDate() As String
        Get
            AutoStartDate = m_AutoStartDate
        End Get
        Set(ByVal Value As String)
            m_AutoStartDate = Value
        End Set
    End Property

    Public Property AutoStartDate02() As String
        Get
            AutoStartDate02 = m_AutoStartDate02
        End Get
        Set(ByVal Value As String)
            m_AutoStartDate02 = Value
        End Set
    End Property

    Public Property AutoNumberOfPayments() As Integer
        Get
            AutoNumberOfPayments = m_AutoNumberOfPayments
        End Get
        Set(ByVal Value As Integer)
            m_AutoNumberOfPayments = Value
        End Set
    End Property

    Public Property AutoNumberOfPayments02() As Integer
        Get
            AutoNumberOfPayments02 = m_AutoNumberOfPayments02
        End Get
        Set(ByVal Value As Integer)
            m_AutoNumberOfPayments02 = Value
        End Set
    End Property

    Public Property CatchUpDays() As Integer
        Get
            If m_CatchUpDays = 0 Then
                If IsDate(m_CatchUpEndDate) And IsDate(m_CatchUpStartDate) Then
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    m_CatchUpDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(m_CatchUpStartDate), CDate(m_CatchUpEndDate)) + 1
                    CatchUpDays = m_CatchUpDays
                End If
            Else
                CatchUpDays = m_CatchUpDays
            End If
        End Get
        Set(ByVal Value As Integer)
            m_CatchUpDays = Value
        End Set
    End Property


    Public Property CatchUpMonths() As Integer
        Get
            CatchUpMonths = m_CatchUpMonths
        End Get
        Set(ByVal Value As Integer)
            m_CatchUpMonths = Value
        End Set
    End Property

    Public Property CatchUpEndDate() As String
        Get
            CatchUpEndDate = m_CatchUpEndDate
        End Get
        Set(ByVal Value As String)
            m_CatchUpEndDate = Value
        End Set
    End Property

    Public Property CatchUpEndDate02() As String
        Get
            CatchUpEndDate02 = m_CatchUpEndDate02
        End Get
        Set(ByVal Value As String)
            m_CatchUpEndDate02 = Value
        End Set
    End Property

    Public Property CatchUpStartDate() As String
        Get
            CatchUpStartDate = m_CatchUpStartDate
        End Get
        Set(ByVal Value As String)
            m_CatchUpStartDate = Value
        End Set
    End Property

    Public Property CatchUpStartDate02() As String
        Get
            CatchUpStartDate02 = m_CatchUpStartDate02
        End Get
        Set(ByVal Value As String)
            m_CatchUpStartDate02 = Value
        End Set
    End Property

    Public Property CatchUpNumberOfPayments() As Integer
        Get
            CatchUpNumberOfPayments = m_CatchUpNumberOfPayments
        End Get
        Set(ByVal Value As Integer)
            m_CatchUpNumberOfPayments = Value
            If m_CatchUpNumberOfPayments < 0 Then m_CatchUpNumberOfPayments = 0
            If m_CatchUpNumberOfPayments > 1 Then m_CatchUpNumberOfPayments = 1
        End Set
    End Property

    Public Property CatchUpNumberOfPayments02() As Integer
        Get
            CatchUpNumberOfPayments02 = m_CatchUpNumberOfPayments02
        End Get
        Set(ByVal Value As Integer)
            m_CatchUpNumberOfPayments02 = Value
            If m_CatchUpNumberOfPayments02 < 0 Then m_CatchUpNumberOfPayments02 = 0
            If m_CatchUpNumberOfPayments02 > 1 Then m_CatchUpNumberOfPayments02 = 1
        End Set
    End Property

    Public Property EarningsPerMonth() As Double
        Get
            EarningsPerMonth = m_EarningsPerMonth
        End Get
        Set(ByVal Value As Double)
            m_EarningsPerMonth = Value
        End Set
    End Property

    Public Property EarningsPerWeek() As Double
        Get
            EarningsPerWeek = m_EarningsPerWeek
        End Get
        Set(ByVal Value As Double)
            m_EarningsPerWeek = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge02() As Double
        Get
            CalculatedPaymentLateCharge02 = m_CalculatedPaymentLateCharge02
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge02 = Value
        End Set
    End Property

    Public Property PayTypeShortCode() As String
        Get
            PayTypeShortCode = m_PayTypeShortCode
        End Get
        Set(ByVal Value As String)
            m_PayTypeShortCode = Value
        End Set
    End Property


    Public Property PenaltyDays() As Integer
        Get
            If m_PenaltyDays = 0 Then
                If m_PenaltyStartDate > "" And m_PenaltyEndDate > "" Then
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    RegularDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(m_PenaltyStartDate), CDate(m_PenaltyEndDate)) + 1
                End If
            Else
                PenaltyDays = m_PenaltyDays
            End If
        End Get
        Set(ByVal Value As Integer)
            m_PenaltyDays = Value
        End Set
    End Property


    Public Property PenaltyMonths() As Integer
        Get
            PenaltyMonths = m_PenaltyMonths
        End Get
        Set(ByVal Value As Integer)
            m_PenaltyMonths = Value
        End Set
    End Property

    Public Property PenaltyEndDate() As String
        Get
            PenaltyEndDate = m_PenaltyEndDate
        End Get
        Set(ByVal Value As String)
            m_PenaltyEndDate = Value
        End Set
    End Property

    Public Property PenaltyEndDate02() As String
        Get
            PenaltyEndDate02 = m_PenaltyEndDate02
        End Get
        Set(ByVal Value As String)
            m_PenaltyEndDate02 = Value
        End Set
    End Property

    Public Property PenaltyStartDate() As String
        Get
            PenaltyStartDate = m_PenaltyStartDate
        End Get
        Set(ByVal Value As String)
            m_PenaltyStartDate = Value
        End Set
    End Property

    Public Property PenaltyStartDate02() As String
        Get
            PenaltyStartDate02 = m_PenaltyStartDate02
        End Get
        Set(ByVal Value As String)
            m_PenaltyStartDate02 = Value
        End Set
    End Property

    Public Property PenaltyNumberOfPayments() As Integer
        Get
            PenaltyNumberOfPayments = m_PenaltyNumberOfPayments
        End Get
        Set(ByVal Value As Integer)
            m_PenaltyNumberOfPayments = Value
            If m_PenaltyNumberOfPayments < 0 Then m_PenaltyNumberOfPayments = 0
            If m_PenaltyNumberOfPayments > 1 Then m_PenaltyNumberOfPayments = 1
        End Set
    End Property

    Public Property PenaltyNumberOfPayments02() As Integer
        Get
            PenaltyNumberOfPayments02 = m_PenaltyNumberOfPayments02
        End Get
        Set(ByVal Value As Integer)
            m_PenaltyNumberOfPayments02 = Value
            If m_PenaltyNumberOfPayments02 < 0 Then m_PenaltyNumberOfPayments02 = 0
            If m_PenaltyNumberOfPayments02 > 1 Then m_PenaltyNumberOfPayments02 = 1
        End Set
    End Property

    Public Property RegularDays() As Integer
        Get
            If m_RegularDays = 0 Then
                If m_RegularStartDate > "" And m_RegularEndDate > "" Then
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    RegularDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(m_RegularStartDate), CDate(m_RegularEndDate)) + 1
                End If
            Else
                RegularDays = m_RegularDays
            End If
        End Get
        Set(ByVal Value As Integer)
            m_RegularDays = Value
        End Set
    End Property


    Public Property RegularMonths() As Integer
        Get
            RegularMonths = m_RegularMonths
        End Get
        Set(ByVal Value As Integer)
            m_RegularMonths = Value
        End Set
    End Property

    Public Property RegularEndDate() As String
        Get
            RegularEndDate = m_RegularEndDate
        End Get
        Set(ByVal Value As String)
            m_RegularEndDate = Value
        End Set
    End Property

    Public Property RegularEndDate02() As String
        Get
            RegularEndDate02 = m_RegularEndDate02
        End Get
        Set(ByVal Value As String)
            m_RegularEndDate02 = Value
        End Set
    End Property

    Public Property RegularStartDate() As String
        Get
            RegularStartDate = m_RegularStartDate
        End Get
        Set(ByVal Value As String)
            m_RegularStartDate = Value
        End Set
    End Property

    Public Property RegularStartDate02() As String
        Get
            RegularStartDate02 = m_RegularStartDate02
        End Get
        Set(ByVal Value As String)
            m_RegularStartDate02 = Value
        End Set
    End Property

    Public Property RegularNumberOfPayments() As Integer
        Get
            RegularNumberOfPayments = m_RegularNumberOfPayments
        End Get
        Set(ByVal Value As Integer)
            m_RegularNumberOfPayments = Value
            If m_RegularNumberOfPayments < 0 Then m_RegularNumberOfPayments = 0
            If m_RegularNumberOfPayments > 1 Then m_RegularNumberOfPayments = 1
        End Set
    End Property

    Public Property RegularNumberOfPayments02() As Integer
        Get
            RegularNumberOfPayments02 = m_RegularNumberOfPayments02
        End Get
        Set(ByVal Value As Integer)
            m_RegularNumberOfPayments02 = Value
            If m_RegularNumberOfPayments02 < 0 Then m_RegularNumberOfPayments02 = 0
            If m_RegularNumberOfPayments02 > 1 Then m_RegularNumberOfPayments02 = 1
        End Set
    End Property

    Public Property TotalNumberofPayments() As Integer
        Get
            TotalNumberofPayments = m_TotalNumberofPayments
        End Get
        Set(ByVal Value As Integer)
            m_TotalNumberofPayments = Value
        End Set
    End Property

    Public Property WaitingDays() As Integer
        Get
            WaitingDays = m_WaitingDays
        End Get
        Set(ByVal Value As Integer)
            m_WaitingDays = Value
        End Set
    End Property

    Public Property WaitingMonths() As Integer
        Get
            WaitingMonths = m_WaitingMonths
        End Get
        Set(ByVal Value As Integer)
            m_WaitingMonths = Value
        End Set
    End Property

    Public Property WaitingEndDate() As String
        Get
            WaitingEndDate = m_WaitingEndDate
        End Get
        Set(ByVal Value As String)
            m_WaitingEndDate = Value
        End Set
    End Property

    Public Property WaitingNumberOfPayments() As Integer
        Get
            WaitingNumberOfPayments = m_WaitingNumberOfPayments
        End Get
        Set(ByVal Value As Integer)
            m_WaitingNumberOfPayments = Value
        End Set
    End Property

    Public Property WaitingStartDate() As String
        Get
            WaitingStartDate = m_WaitingStartDate
        End Get
        Set(ByVal Value As String)
            m_WaitingStartDate = Value
        End Set
    End Property
End Class

