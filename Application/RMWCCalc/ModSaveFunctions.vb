Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Module modSaveFunctions
	Const sModuleName As String = "modSaveFunctions"
	
	Public Function Save_Payment_Splits(ByRef BenefitType As String, ByRef dblPayment_1 As Double, ByRef sBenefitStartDateDTG_1 As String, ByRef sBenefitEndDateDTG_1 As String, ByRef lTransTypeCode_1 As Integer, ByRef dblPayment_2 As Double, ByRef sBenefitStartDateDTG_2 As String, ByRef sBenefitEndDateDTG_2 As String, ByRef lTransTypeCode_2 As Integer, ByRef dblPayment_3 As Double, ByRef sBenefitStartDateDTG_3 As String, ByRef sBenefitEndDateDTG_3 As String, ByRef lTransTypeCode_3 As Integer, ByRef sCheckDateDTG As String, ByRef lBankAccId As Integer, ByRef lPaymentStatus As Integer, ByRef PayeeEID As Integer, ByRef PayeeTypeCode As Integer, ByRef dblEarnings As Double) As Integer
		
		'Const sFunctionName As String = "Save_Payment_Splits"
        'try
        '    Dim iRdSet As Integer
        '    Dim sSQL As String
        '    Dim lReserveTypeCode As Long, lAccountId As Long, lSubAccountID As Long
        '    Dim sBenefitStartDate As String, sBenefitEndDate As String, sCheckDate As String
        '    Dim sNow As String
        '    Dim bSysParmUseSubAcc As Boolean
        '    Dim sCtlNumber As String
        '    Dim s_Now As String
        '    Dim sTmp As String
        '    Dim objXEntity As CWCXEntity
        '    Dim objFunds As CFunds
        '    Dim objTransSplit_1 As CFundsTransSplit
        '    Dim objTransSplit_2 As CFundsTransSplit
        '    Dim objTransSplit_3 As CFundsTransSplit
        '    Dim iPer As IPersistence
        '    Dim inav As INavigation
        '
        '    Save_Payment_Splits = -1
        '
        '    'bank account ids
        '    sSQL = "SELECT USE_SUB_ACCOUNT FROM SYS_PARMS"
        '    iRdSet = g_DBObject.DB_CreateRecordset(g_dbConn1, sSQL, DB_FORWARD_ONLY, 0)
        '    If (objReader.Read()) Then bSysParmUseSubAcc = objReader.GetInt32( "USE_SUB_ACCOUNT")
        '    SafeDropRecordset iRdSet
        '
        '    lAccountId = lBankAccId
        '    If lBankAccId > 0 And bSysParmUseSubAcc Then
        '        lSubAccountID = lBankAccId
        '        lAccountId = 0
        '        sSQL = "SELECT CODES.RELATED_CODE_ID FROM CODES WHERE CODE_ID = " & lBankAccId
        '        iRdSet = g_DBObject.DB_CreateRecordset(g_dbConn1, sSQL, DB_FORWARD_ONLY, 0)
        '        If (objReader.Read()) Then
        '            lAccountId = objReader.GetInt32(1)
        '        End If
        '        SafeDropRecordset iRdSet
        '    End If
        '
        '    s_Now = Format$(Now, "yyyymmddhhnnss")
        '
        '    Set objFunds = Nothing
        '    Set objFunds = New CFunds
        '    Set inav = objFunds
        '    inav.AddNew
        '
        '    Set objTransSplit_1 = Nothing
        '    Set objTransSplit_1 = New CFundsTransSplit
        '    With objTransSplit_1
        '        .Amount =modfunctions.roundstandard(dblPayment_1, 2)
        '        .DataChanged = True
        '        .ReserveTypeCode = GetReserveTypeCode(lTransTypeCode_1)
        '        .SumAmount =modfunctions.roundstandard(dblPayment_1, 2)
        '        .FromDate = sBenefitStartDateDTG_1
        '        .ToDate = sBenefitEndDateDTG_1
        '        .TransTypeCode = lTransTypeCode_1
        '    End With
        '    Set objTransSplit_2 = Nothing
        '    If lTransTypeCode_2 > 0 Then
        '        Set objTransSplit_2 = New CFundsTransSplit
        '        With objTransSplit_2
        '            .Amount =modfunctions.roundstandard(dblPayment_2, 2)
        '            .DataChanged = True
        '            .ReserveTypeCode = GetReserveTypeCode(lTransTypeCode_2)
        '            .SumAmount =modfunctions.roundstandard(dblPayment_2, 2)
        '            .FromDate = sBenefitStartDateDTG_2
        '            .ToDate = sBenefitEndDateDTG_2
        '            .TransTypeCode = lTransTypeCode_2
        '        End With
        '    End If
        '    Set objTransSplit_3 = Nothing
        '    If lTransTypeCode_3 > 0 Then
        '        Set objTransSplit_3 = New CFundsTransSplit
        '        With objTransSplit_3
        '            .Amount =modfunctions.roundstandard(dblPayment_3, 2)
        '            .DataChanged = True
        '            .ReserveTypeCode = GetReserveTypeCode(lTransTypeCode_3)
        '            .SumAmount =modfunctions.roundstandard(dblPayment_3, 2)
        '            .FromDate = sBenefitStartDateDTG_3
        '            .ToDate = sBenefitEndDateDTG_3
        '            .TransTypeCode = lTransTypeCode_3
        '        End With
        '    End If
        '    With objFunds
        '        If bSysParmUseSubAcc Then
        '           If lBankAccId >= 0 Then
        '              .SubAccountId = lBankAccId
        '              .AccountId = GetParentAccountID(.SubAccountId)
        '           Else
        '              .SubAccountId = 0
        '              .AccountId = 0
        '           End If
        '        Else
        '           If lBankAccId >= 0 Then
        '              .AccountId = lBankAccId
        '           Else
        '              .AccountId = 0
        '           End If
        '           .SubAccountId = 0
        '        End If
        '        Set objXEntity = Nothing
        '        objXEntity.LoadData (PayeeEID)
        '        .Addr1 = objXEntity.Addr1
        '        .Addr2 = objXEntity.Addr2
        '        .City = objXEntity.City
        '        .CountryCode = objXEntity.CountryCode
        '        .FirstName = objXEntity.FirstName
        '        .LastName = objXEntity.LastName
        '        .StateId = objXEntity.StateId
        '        .ZipCode = objXEntity.ZipCode
        '
        '        'error--ReadOnly--.AddedByUser = g_objUser.LoginName
        '        .Amount =modfunctions.roundstandard(Abs(dblPayment_1) + Abs(dblPayment_2) + Abs(dblPayment_3), 2)
        '        '.ApproveUser
        '        '.AutoCheckDetail
        '        '.AutoCheckFlag
        '        '.BatchNumber
        '        '.CheckMemo
        '        .ClaimantEID = g_objXClaim.ClaimantEID
        '        .ClaimID = g_objXClaim.ClaimID
        '        .ClaimNumber = g_objXClaim.ClaimNumber
        '        '.ClearedFlag
        '        '.ClearObject
        '        '.CollectionFlag
        '        '.Comments
        '        '.CRC
        '        '.ctlnumber
        '        '.DateOfCheck
        '        '.DttmApproval
        '        'error--readOnly--.DttmRcdAdded = s_Now
        '        'error--ReadOnly--.DttmRcdLastUpd = s_Now
        '        '.EnclosureFlag
        '        '.Filed1099Flag
        '        'error--ReadOnly--.LineOfBusCode = 243
        '        '.Notes
        '        '.parent
        '        .PayeeEID = PayeeEID
        '        '.PayeeEntity.
        '        .PayeeTypeCode = 466
        '        .PaymentFlag = True
        '        '.PrecheckFlag
        '        '.RollupId
        '        '.SettlementFlag
        '        .StatusCode = lPaymentStatus   'jlt 12/23/2002 Normally status is released, may be hold for court order or taxes
        '        'error--ReadOnly--.Supplementals.IsAnyRequired = False
        '        .TransDate = sCheckDateDTG
        '        .DateOfCheck = sCheckDateDTG
        '
        '        'defaults value--.TransId = l_TransID
        '        '.TransNumber
        '        .TransSplitsList.Add objTransSplit_1
        '        If (Not objTransSplit_2 Is Nothing) Then
        '            .TransSplitsList.Add objTransSplit_2
        '        End If
        '        If (Not objTransSplit_3 Is Nothing) Then
        '            .TransSplitsList.Add objTransSplit_3
        '        End If
        '        '.UnitId
        '        'error--ReadOnly--.UpdatedByUser = g_objUser.LoginName
        '        '.VoidDate
        '        .AutoCheckFlag = False
        '        .PrecheckFlag = False
        '        .VoidFlag = False
        '        .ClearedFlag = False
        '        .PaymentFlag = True
        '        .CollectionFlag = False
        '        .BatchNumber = 0
        '
        '    End With
        '    Set iPer = objFunds
        '    iPer.Save
        '
        '    sCtlNumber = BenefitType & "-" & Format(objFunds.TransId, "0000000")
        '
        '    g_DBObject.DB_SQLExecute g_dbConn1, "UPDATE FUNDS SET CTL_NUMBER = '" & sCtlNumber & "' WHERE TRANS_ID = " & objFunds.TransId
        '    DoEvents
        '    Save_Payment_Splits = objFunds.TransId
        '
        'hExit:
        '    Set objFunds = Nothing
        '    Set objTransSplit_1 = Nothing
        '    Exit Function
        '
        'hError:
        '    With Err
        '        g_lErrNum = .Number
        '        g_sErrSrc = .Source
        '        g_sErrDescription = .Description
        '    End With
        '    g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".Save_Payment_Splits|"
        '    g_lErrLine = Erl
        '    SafeDropRecordset iRdSet
        '    LogError g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription
        '    Err.Raise g_lErrNum, g_sErrProcedure, g_sErrDescription
        '    Resume hExit:
        '    Resume


    End Function
    Public Function lGetNextUID(ByVal sTableName As String) As Integer
        Const sFunctionName As String = "lGetNextUID"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim lTableID As Integer
        Dim lNextUID As Integer
        Dim lOrigUID As Integer
        Dim lRows As Integer
        Dim lCollisionRetryCount As Integer
        Dim lErrRetryCount As Integer
        Dim sSaveError As String
        Try
            Do
                objReader = DbFactory.GetDbReader(g_ConnectionString, "SELECT TABLE_ID,NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'")
                If Not objReader.Read() Then
                    Err.Raise(32001, sModuleName & ".lGetNextUID(" & sTableName & ")", "Specified table does not exist in glossary.")
                    lGetNextUID = 0
                    Exit Function
                End If

                lTableID = objReader.GetInt32(0)
                lNextUID = objReader.GetInt32(1)
                objReader.Close()

                ' Compute next id
                lOrigUID = lNextUID

                If lOrigUID Then lNextUID = lNextUID + 1 Else lNextUID = 2

                ' try to reserve id (searched update)
                sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & lNextUID & " WHERE TABLE_ID = " & lTableID

                ' only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
                If lOrigUID Then sSQL = sSQL & " AND NEXT_UNIQUE_ID = " & lOrigUID

                ' Try update
                Try
                    lRows = DbFactory.ExecuteNonQuery(g_ConnectionString, sSQL)
                    If lRows = 1 Then
                        lGetNextUID = lNextUID - 1
                        Exit Function ' success
                    Else
                        lCollisionRetryCount = lCollisionRetryCount + 1 ' collided with another user - try again (up to 1000 times)
                    End If
                Catch ex As Exception
                    sSaveError = Err.Description
                    lErrRetryCount = lErrRetryCount + 1
                    If lErrRetryCount >= 5 Then
                        Err.Raise(29999, sModuleName & ".lGetNextUID(" & sTableName & ")", sSaveError)
                        Exit Function
                    End If

                End Try

            Loop While (lErrRetryCount < 5) And (lCollisionRetryCount < 1000)

            If lCollisionRetryCount >= 1000 Then
                Err.Raise(32001, sModuleName & ".lGetNextUID(" & sTableName & ")", "Collision timeout. Server load too high. Please wait and try again.")
                Exit Function
            End If

            lGetNextUID = 0

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".lGetNextUID(" & sTableName & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Private Function GetReserveTypeCode(ByRef lTransTypeCode As Object) As Integer
        Const sFunctionName As String = "GetReserveTypeCode"

        Dim objReader As dbReader
        Dim sSQL As String
        Try
            'reserve type code
            'UPGRADE_WARNING: Couldn't resolve default property of object lTransTypeCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sSQL = "SELECT CODES.RELATED_CODE_ID FROM CODES WHERE CODE_ID = " & lTransTypeCode
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetReserveTypeCode = objReader.GetInt32(0)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function
End Module

