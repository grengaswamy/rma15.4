Option Strict Off
Option Explicit On
Public Class CWCEnumWaitPeriodPayStatus
    '123456789012345678901234567890123456789
    'RMWCCalc.CWCEnumWaitPeriodPayStatus
    Public IsApplicable As Integer
    Public NotApplicable As Integer
    Public NotPaid As Integer
    Public NotPayableAtThisTime As Integer
    Public Paid As Integer
    'jtodd22 10/10/2006 only one value applies to the Waiting Period
    Public Function LoadData() As Integer
        IsApplicable = 1
        NotApplicable = 2
        NotPaid = 3
        NotPayableAtThisTime = 4
        Paid = 5


    End Function
End Class

