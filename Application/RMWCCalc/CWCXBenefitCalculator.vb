Option Strict Off
Option Explicit On
Public Class CWCXBenefitCalculator
    Const sClassName As String = "CWCXBenefitCalculator"
    Public UserTransactionTypes As New CWCXUserTransactionTypes

    Protected Overrides Sub Finalize()
        UserTransactionTypes = Nothing
        MyBase.Finalize()
    End Sub

End Class

