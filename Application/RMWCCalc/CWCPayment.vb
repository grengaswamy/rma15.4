Option Strict Off
Option Explicit On
Public Class CWCPayment
    Const sClassName As String = "CWCPayment"
    Private m_Amount As Double
    Private m_Days As Integer
    Private m_FromDateCalder As String
    Private m_ToDateCalder As String
    Private m_TransactionCodeID As String

    Public Property Amount() As Double
        Get
            Amount = m_Amount
        End Get
        Set(ByVal Value As Double)
            m_Amount = Value
        End Set
    End Property

    Public Property Days() As Integer
        Get
            Days = m_Days
        End Get
        Set(ByVal Value As Integer)
            m_Days = Value
        End Set
    End Property

    Public Property FromDateCalder() As String
        Get
            FromDateCalder = m_FromDateCalder
        End Get
        Set(ByVal Value As String)
            m_FromDateCalder = Value
        End Set
    End Property

    Public Property ToDateCalder() As String
        Get
            ToDateCalder = m_ToDateCalder
        End Get
        Set(ByVal Value As String)
            m_ToDateCalder = Value
        End Set
    End Property

    Public Property TransactionCodeID() As String
        Get
            TransactionCodeID = m_TransactionCodeID
        End Get
        Set(ByVal Value As String)
            m_TransactionCodeID = Value
        End Set
    End Property
End Class

