Option Strict Off
Option Explicit On
Imports System.Reflection.Assembly
Imports Riskmaster.Db

Module modFunctions
    Const sModuleName As String = "modFunctions"
    Public Function GetFormattedDate(ByVal sDateDBFormat As String) As String
        'takes a DTG style date (YYYYMMDD) and returns a string (as "Short Date")
        Dim sFmtDate As String
        On Error Resume Next

        GetFormattedDate = ""

        If sDateDBFormat = "" Then Exit Function

        sFmtDate = Format(DateSerial(Val(Left(sDateDBFormat, 4)), Val(Mid(sDateDBFormat, 5, 2)), Val(Mid(sDateDBFormat, 7, 2))), "Short Date")

        GetFormattedDate = sFmtDate



    End Function
    'e.g. return what goes in parens in sql statement like "...WHERE CODE_ID IN(15,78,84,87)..."
    Public Sub GetCodeList(ByRef sSQL As String, ByRef sList As String)
        Dim objReader As DbReader

        objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
        While (objReader.Read())
            sList = sList & "," & objReader.GetInt32(0)
        End While
        If Len(sList) > 0 Then sList = Mid(sList, 2)
    End Sub

    Public Function GetStateName_SQL(ByRef lStateID As Integer) As String
        Dim objReader As DbReader
        Dim sSQL As String
        GetStateName_SQL = ""
        sSQL = "SELECT STATE_NAME FROM STATES WHERE STATE_ROW_ID = " & lStateID
        objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
        If (objReader.Read()) Then
            GetStateName_SQL = objReader.GetString(0)
        End If
    End Function

    Public Function GetStatePostalCode_SQL(ByRef lStateID As Integer) As String
        Dim objReader As DbReader
        Dim sSQL As String
        GetStatePostalCode_SQL = ""
        sSQL = "SELECT STATE_ID FROM STATES WHERE STATE_ROW_ID = " & lStateID
        objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
        If (objReader.Read()) Then
            GetStatePostalCode_SQL = objReader.GetString(0)
        End If
    End Function

    Function sDBDateFormat(ByRef sDate As String, ByRef sFormat As String) As String

        Dim vTmp As Object
        sDBDateFormat = ""
        If Trim(sDate) <> "" Then
            'UPGRADE_WARNING: Couldn't resolve default property of object vTmp. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            vTmp = DateSerial(Val(Left(sDate, 4)), Val(Mid(sDate, 5, 2)), Val(Mid(sDate, 7, 2)))
            'UPGRADE_WARNING: Couldn't resolve default property of object vTmp. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sDBDateFormat = Format(vTmp, sFormat)
        End If


    End Function

    Public Function GetBenefitDescription(ByRef lBenefitID As Integer, iClientId As Integer) As String
        Dim objDS As DataSet
        Dim sSQL As String
        GetBenefitDescription = ""

        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " JURIS_BENEFIT_DESC"
        sSQL = sSQL & " FROM WCP_BENEFIT_LKUP"
        sSQL = sSQL & " WHERE BENEFIT_LKUP_ID = " & lBenefitID

        objDS = DbFactory.GetDataSet(g_ConnectionString, sSQL, iClientId)
        If (objDS.Tables.Count > 0) Then
            If Not IsNothing(objDS.Tables(0).Columns("JURIS_BENEFIT_DESC")) Then
                If (objDS.Tables(0).Rows.Count > 0) Then
                    GetBenefitDescription = (objDS.Tables(0).Rows(0)("JURIS_BENEFIT_DESC")).ToString()
                End If
            End If
        End If
    End Function


    Public Function GetBenefitLookUpIDs(ByRef sAbbr1 As String, ByRef sAbbr2 As String) As String
        Const sFunctionName As String = "GetBenefitLookUpIDs"
        Dim objReader As DbReader
        Dim sAbbr As String
        Dim sSQL As String
        Dim sTemp As String

        Try

            GetBenefitLookUpIDs = ""

            If Trim(sAbbr1 & sAbbr2) = "" Then Exit Function

            sTemp = ""

            sSQL = ""
            sSQL = sSQL & "SELECT BENEFIT_LKUP_ID FROM WCP_BENEFIT_LKUP"
            If Trim(sAbbr2 & "") > "" Then
                sAbbr = "'" & sAbbr1 & "','" & sAbbr2 & "'"
            Else
                sAbbr = "'" & sAbbr1 & "'"
            End If
            sSQL = sSQL & " WHERE ABBREVIATION IN (" & sAbbr & ")"
            sSQL = sSQL & " AND JURIS_ROW_ID = " & g_objXClaim.FilingStateID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                If Len(Trim(sTemp)) = 0 Then
                    sTemp = CStr(objReader.GetInt32("BENEFIT_LKUP_ID"))
                Else
                    sTemp = sTemp & "," & CStr(objReader.GetInt32("BENEFIT_LKUP_ID"))
                End If
            End While

            GetBenefitLookUpIDs = sTemp
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            GetBenefitLookUpIDs = ""
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    Public Function GetCatchupPaymentMonths(ByRef sMessage As String, ByRef oUserInputs As CWCXUserInputs) As Integer
        Dim sDateCalendar As String
        GetCatchupPaymentMonths = 0
        sMessage = ""

        'remember that; for payments dates are inclusive, for DateDiff dates are exclusive
        With oUserInputs
            If CDate(.LastPaymentDateCalendar) < Today Then
                sDateCalendar = CStr(DateAdd(INTERVALDAY, 1, CDate(.LastPaymentDateCalendar)))
                GetCatchupPaymentMonths = DateDiff(INTERVALMONTH, CDate(.FirstPaymentDateCalendar), CDate(sDateCalendar))
            Else
                If CDate(oUserInputs.FirstPaymentDateCalendar) < Today Then
                    sDateCalendar = CStr(DateAdd(INTERVALDAY, 1, Today))
                    GetCatchupPaymentMonths = DateDiff(INTERVALMONTH, CDate(oUserInputs.FirstPaymentDateCalendar), CDate(sDateCalendar))
                End If
            End If
        End With

        If GetCatchupPaymentMonths < 0 Then GetCatchupPaymentMonths = 0



    End Function

    Public Function GetClientDictionaryPath(iClientId As Integer) As String
        Dim objDS As DataSet
        Dim sSQL As String
        g_sDictPath = ""
        sSQL = "SELECT * FROM SYS_PARMS"
        objDS = DbFactory.GetDataSet(g_ConnectionString, sSQL, iClientId)
        If (objDS.Tables.Count > 0) Then
            If Not IsNothing(objDS.Tables(0).Columns("USER_DICT_PATH")) Then
                If (objDS.Tables(0).Rows.Count > 0) Then
                    g_sDictPath = (objDS.Tables(0).Rows(0)("USER_DICT_PATH")).ToString()
                End If
            End If
        End If
    End Function

    Public Function GetCodeDesc_SQL(ByRef lCodeID As Integer) As String
        Const sFunctionName As String = "GetCodeDesc_SQL"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetCodeDesc_SQL = ""
            sSQL = "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = " & lCodeID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetCodeDesc_SQL = objReader.GetString(0)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_lErrLine = Erl()
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally

        End Try

    End Function

    Public Sub SafeCloseRecordset(ByVal objReader As DbReader)
        If Not objReader Is Nothing Then
            objReader.Close()
            objReader.Dispose()
        End If
    End Sub

    'Public Sub SafeDropRecordset(ByRef iRdSet As Short)
    '    If iRdSet <> 0 Then
    '        On Error Resume Next
    '        g_DBObject.DB_CloseRecordset(iRdSet, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP)
    '        On Error GoTo 0
    '    End If
    'End Sub

    Public Function OverRideRateRead(ByRef sBenefitAbbr As String) As Double
        Const sFunctionName As String = "OverRideRateRead"
        Dim objRate As CWCWCPClaim
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim sSQL2 As String
        Dim sSQL3 As String

        Try

            OverRideRateRead = 0

            sBenefitAbbr = UCase(sBenefitAbbr)

            objRate = New CWCWCPClaim

            lReturn = objRate.LoadDataLastRecordByRowID(sBenefitAbbr)

            If Not objRate Is Nothing Then
                OverRideRateRead = objRate.BenefitRateEffective
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            'UPGRADE_NOTE: Object objRate may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objRate = Nothing

        End Try

    End Function

    Public Function PadDateWithZeros(ByVal DateToPad As String) As Object
        'expected mm/dd/yyyy
        'will fix m/d/yyyy
        PadDateWithZeros = Trim(DateToPad)
        If Trim(DateToPad) = "" Then
            'string is empty
            Exit Function
        End If
        If Len(DateToPad) = 10 Then
            Exit Function
        End If
        If InStr(1, DateToPad, "/") = 2 Then
            DateToPad = "0" & DateToPad
            ' is 0m/d/yyyy or 0m/dd/yyyy
        End If
        PadDateWithZeros = Trim(DateToPad)
        If Len(DateToPad) = 10 Then
            Exit Function
        End If
        If InStr(4, DateToPad, "/") = 5 Then
            DateToPad = Left(DateToPad, 3) & "0" & Mid(DateToPad, 4)
        End If
        PadDateWithZeros = Trim(DateToPad)



    End Function
    '               123456789012345678901234567890
    Public Function PutOrginalClaimBenefitData(ByVal sBenefitType As String, ByVal dPaymentRate As Double, ByVal sOrderNumber As String, ByVal lOrderType As Integer) As Boolean
        '*************************************************************
        '*** find the record
        '*** if the record is missing (new benefit) then add a record
        '*************************************************************
        Const sFunctionName As String = "PutOrginalClaimBenefitData"


        Dim dMMIDisabilityRate As Double
        Dim sMMIDateDTG As String

        Dim dPermDisRate As Double
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim iRdSetUpdate As Short
        Dim lReturn As Integer
        Dim lTemp As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Dim sSQL3 As String

        Dim sSQLSelect As String
        Dim s_Now As String
        Try
            PutOrginalClaimBenefitData = False

            If g_objXClaim Is Nothing Then Exit Function

            lReturn = modFunctions.GetMMIImpairment(dMMIDisabilityRate, sMMIDateDTG)

            lTemp = -1
            s_Now = Format(Now, "yyyymmddhhnnss")

            sBenefitType = Trim(UCase(sBenefitType))
            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(ROW_ID)"
            sSQL2 = sSQL2 & " FROM " & g_sWCPClaimsTableName
            sSQL2 = sSQL2 & " WHERE BENEFIT_TYPE = '" & sBenefitType & "'"
            sSQL2 = sSQL2 & " AND CLAIM_ID = " & g_objXClaim.ClaimID

            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT * "
            sSQL3 = sSQL3 & " FROM " & g_sWCPClaimsTableName
            sSQL3 = sSQL3 & " WHERE BENEFIT_TYPE = '" & sBenefitType & "'"
            sSQL3 = sSQL3 & " AND CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL3 = sSQL3 & " AND ROW_ID = (" & sSQL2 & ")"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If Not (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objReader.Close()

                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("BEGIN_PRNT_DATE", sMMIDateDTG)
                objWriter.Fields.Add("BENEFIT_RATE", dPaymentRate)
                objWriter.Fields.Add("BENEFIT_TYPE", UCase(sBenefitType))
                objWriter.Fields.Add("CLAIM_ID", g_objXClaim.ClaimID)
                objWriter.Fields.Add("DISABILITY_RATE", dMMIDisabilityRate)
                objWriter.Fields.Add("DTTM_RCD_ADDED", s_Now)
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", s_Now)
                objWriter.Fields.Add("ROW_ID", lGetNextUID("WCP_CLAIMS"))
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Execute()
            End If

            PutOrginalClaimBenefitData = True

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function GetTransTypeCodeIDsIndemnity() As String
        Const sFunctionName As String = "GetTransTypeCodeIDsIndemnity"
        Dim objReader As DbReader
        Dim lType As Integer
        Dim sOut As String
        Dim sSQL As String

        Try

            GetTransTypeCodeIDsIndemnity = ""
            sOut = ""

            lType = GetIndemnityBenefitTypeNumber()
            If lType = 0 Then
                'jtodd this is fatal to fetching the transaction code IDs
                Exit Function
            End If

            sSQL = ""
            sSQL = sSQL & "SELECT DISTINCT"
            sSQL = sSQL & " WCP_TRANS_TYPES.CODE_ID "
            sSQL = sSQL & " FROM WCP_BENEFIT_LKUP, WCP_TRANS_TYPES"
            sSQL = sSQL & " WHERE WCP_BENEFIT_LKUP.BENEFIT_LKUP_ID = WCP_TRANS_TYPES.BENEFIT_LKUP_ID"
            sSQL = sSQL & " AND WCP_BENEFIT_LKUP.JURIS_ROW_ID = " & g_objXClaim.FilingStateID
            sSQL = sSQL & " AND WCP_BENEFIT_LKUP.TYPE_DESC_ROW_ID = " & lType

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                If sOut = "" Then
                    sOut = CStr(objReader.GetInt32(0))
                Else
                    sOut = sOut & "," & CStr(objReader.GetInt32(0))
                End If
            End While

            GetTransTypeCodeIDsIndemnity = sOut

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    Public Function GetTwoYearStartDateCalendar() As String
        Dim sTemp As String
        sTemp = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Year, 2, CDate(g_objXClaim.DateOfEventCalar)))
        'UPGRADE_WARNING: Couldn't resolve default property of object PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        sTemp = PadDateWithZeros(sTemp)
        GetTwoYearStartDateCalendar = sTemp


    End Function
    Public Function GetNoCodeID(ByRef lErrNumber As Integer) As Integer
        Const sFunctionName As String = "GetNoCodeID"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetNoCodeID = 0
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " CODES.CODE_ID"
            sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY_TEXT"
            sSQL = sSQL & " WHERE TABLE_NAME = 'Yes/No'"
            sSQL = sSQL & " AND GLOSSARY_TEXT.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES_TEXT.CODE_DESC = 'No'"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetNoCodeID = objReader.GetInt32("CODE_ID")
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lErrNumber = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    Public Function GetYesCodeID(ByRef lErrNumber As Integer) As Integer
        Const sFunctionName As String = "GetYesCodeID"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetYesCodeID = 0
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " CODES.CODE_ID"
            sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY_TEXT"
            sSQL = sSQL & " WHERE TABLE_NAME = 'Yes/No'"
            sSQL = sSQL & " AND GLOSSARY_TEXT.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES_TEXT.CODE_DESC = 'Yes'"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetYesCodeID = objReader.GetInt32("CODE_ID")
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lErrNumber = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function
    Public Function iCalculateAge(ByRef sDate1 As String, ByRef sDate2 As String) As Short
        Dim lTmp As Integer

        If IsDate(sDate1) And IsDate(sDate2) Then
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            lTmp = Int(DateDiff(Microsoft.VisualBasic.DateInterval.DayOfYear, CDate(sDate1), CDate(sDate2)) / 365.25)
            If lTmp > 32000 Then lTmp = 0
            If lTmp < 0 Then lTmp = 0
            iCalculateAge = lTmp
        Else
            iCalculateAge = 0
        End If


    End Function


    Public Function ValidateBenefitDates(ByRef StartDate As String, ByRef EndDate As String) As Boolean
        Const sFunctionName As String = "ValidateBenefitDates"
        Try
            ValidateBenefitDates = False
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(StartDate), CDate(g_objXClaim.DateOfEventCalar)) > 0 Then
                Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit start date(" & StartDate & ") cannot precede event date(" & g_objXClaim.DateOfEventCalar & ")")
            End If
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(EndDate), CDate(StartDate)) > 0 Then
                Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit end date(" & EndDate & ") cannot precede the benefit start date date(" & StartDate & ")")
            End If
            ValidateBenefitDates = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function CalculateBenefitDays(ByRef StartDate As String, ByRef EndDate As String) As Integer
        Const sFunctionName As String = "CalculateBenefitDays"
        Try
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(StartDate), CDate(g_objXClaim.DateOfEventCalar)) >= 0 Then Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit start date(" & CDate(StartDate) & ") cannot precede event date(" & g_objXClaim.DateOfEventCalar & ")")
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(EndDate), CDate(StartDate)) >= 0 Then Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit end date(" & CDate(EndDate) & ") cannot precede benefitstartdate date(" & CDate(StartDate) & ")")

            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            CalculateBenefitDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(StartDate), CDate(EndDate)) + 1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function


    Public Function BuildTopOfWorkSheet(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "BuildTopOfWorkSheet"
        Try

            BuildTopOfWorkSheet = g_objXErrorMask.cSplitPayment

            With g_objXPaymentParms
                .TotalNumberofPayments = .AutoNumberOfPayments + .CatchUpNumberOfPayments + .RegularNumberOfPayments + .WaitingNumberOfPayments
                .BenefitDays = .AutoDays + .CatchUpDays + .RegularDays + .WaitingDays
                .BenefitMonths = .AutoMonths + .CatchUpMonths + .RegularMonths + .WaitingMonths
            End With
            'build top of work sheet
            If colWorkSheet.Count() = 0 Then
                colWorkSheet.Add("|Claim Number                     |" & g_objXClaim.ClaimNumber)
                colWorkSheet.Add("|Claimant Name                    |" & g_objXClaim.ClaimantNameLFM)
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Date of Injury                   |" & g_objXClaim.DateOfEventCalar)
                colWorkSheet.Add("|Jurisdiction                     |" & g_objXClaim.FilingStatePostalCode)
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Benefit Start Date               |" & CDate(g_objXPaymentParms.BenefitStartDate))
                colWorkSheet.Add("|Benefit End Date                 |" & CDate(g_objXPaymentParms.BenefitEndDate))
                colWorkSheet.Add("|Payments to be made              |" & g_objXPaymentParms.TotalNumberofPayments)

                If g_objXPaymentParms.BenefitDays > 0 Then
                    colWorkSheet.Add("|    Auto Check Days                    |" & g_objXPaymentParms.AutoDays)
                    colWorkSheet.Add("|    Catch Up Days                      |" & g_objXPaymentParms.CatchUpDays)
                    colWorkSheet.Add("|    Regular Days                       |" & g_objXPaymentParms.RegularDays)
                    colWorkSheet.Add("|    Waiting Period Days                |" & g_objXPaymentParms.WaitingDays)
                    colWorkSheet.Add("|Days in Payment(s)                     |" & g_objXPaymentParms.BenefitDays)
                    colWorkSheet.Add("||")
                    colWorkSheet.Add("|Claimant's Original Average Weekly Wage|" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))
                End If

                If g_objXPaymentParms.BenefitMonths > 0 Then
                    colWorkSheet.Add("|    Auto Check Months              |" & g_objXPaymentParms.AutoMonths)
                    colWorkSheet.Add("|    Catch Up Months                |" & g_objXPaymentParms.CatchUpMonths)
                    colWorkSheet.Add("|    Regular Months                 |" & g_objXPaymentParms.RegularMonths)
                    colWorkSheet.Add("|    Waiting Period Months          |" & g_objXPaymentParms.WaitingMonths)
                    colWorkSheet.Add("|Months in Payment(s)               |" & g_objXPaymentParms.BenefitMonths)
                    colWorkSheet.Add("||")
                    colWorkSheet.Add("|Claimant's Original AWW          |" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))
                End If

            Else
                colWorkSheet.Add("||")
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            BuildTopOfWorkSheet = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '               123456789012345678901234567890
    Public Function bCheckAnniversaryInCalrDateRange(ByRef sBeginCalrDate As String, ByRef sEndCalrDate As String) As Boolean
        Const sFunctionName As String = "bCheckAnniversaryInCalrDateRange"
        Dim sBeginDBFormat As String
        Dim sBeginYear As String
        Dim sEndDBFormat As String
        Dim sEvent As String
        Dim sEventAnniversary As String
        Dim sEventAnniversaryNext As String
        Dim sNextYear As String
        Try
            bCheckAnniversaryInCalrDateRange = False

            sEvent = Mid(g_objXClaim.DateOfEventDTG, 5)
            sBeginDBFormat = modFunctions.sGetDBDateFormat(sBeginCalrDate)
            sBeginYear = Mid(sBeginDBFormat, 1, 4)
            sEventAnniversary = sBeginYear & sEvent
            sNextYear = Format(Val(sBeginYear) + 1, "0")
            sEventAnniversaryNext = sNextYear & sEvent

            sEndDBFormat = modFunctions.sGetDBDateFormat(sEndCalrDate)


            Select Case sBeginDBFormat
                Case Is < sEventAnniversary
                    If sEndDBFormat >= sEventAnniversary Then
                        bCheckAnniversaryInCalrDateRange = True
                    End If

                Case Is = sEventAnniversary
                    If sEndDBFormat >= sEventAnniversaryNext Then
                        bCheckAnniversaryInCalrDateRange = True
                    End If
                Case Is > sEventAnniversary
                    If sEndDBFormat >= sEventAnniversaryNext Then
                        bCheckAnniversaryInCalrDateRange = True
                    End If
            End Select


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function
    Public Function GetAssemblyType(sDLLClass As String) As Type

        Dim a As System.Reflection.Assembly

        a = System.Reflection.Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + "Riskmaster.Application.RMJuRuLib.dll")

        Dim t As Type = a.GetType("Riskmaster.Application." & sDLLClass)

        Return t

    End Function


    Public Function GetBenefitRule(ByRef m_ErrorMask As Integer, ByRef m_ErrorMaskSAWW As Integer, ByRef sAbbv As String, ByRef objRule As Object, ByRef sDLLClass As String, Optional ByRef bUseTwoYearRule As Boolean = False) As Integer
        Const sFunctionName As String = "GetBenefitRule"
        Dim lReturn As Integer
        Try
            'presume no errors
            GetBenefitRule = g_objXErrorMask.cSuccess

            objRule = Activator.CreateInstance(GetAssemblyType(sDLLClass))

            If bUseTwoYearRule = False Then
                lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            Else
                lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventTwoYearDTG)
            End If
            If objRule.ErrorMaskSAWW > 0 Then
                m_ErrorMaskSAWW = objRule.ErrorMaskSAWW
                Exit Function
            End If

            Select Case lReturn
                Case -1 'normal, expected
                    If objRule.TableRowID = 0 Then
                        m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                        Exit Function
                    End If
                Case 0
                    g_sErrDescription = "Jurisdictional Benefit rule (" & sAbbv & ") was not found for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80000
                    m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)
                    Exit Function
                    'jtodd22 is not a true application error, is a true missing data error
                    'jtodd22 01/13/2005  Err.Raise g_lErrNum, g_sErrProcedure, g_sErrDescription
                Case Else
                    g_sErrDescription = "There was an error fetching the Jurisdictional Benefit rule (" & sAbbv & ") for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80001
                    GetBenefitRule = g_lErrNum
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)
                    Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetBenefitRule = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "(" & sDLLClass & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function GetBenefitRuleWA(ByRef m_ErrorMask As Integer, ByRef m_ErrorMaskSAWW As Integer, ByRef sAbbv As String, ByRef objRule As Object, ByRef sDLLClass As String, Optional ByRef bUseTwoYearRule As Boolean = False) As Integer
        Const sFunctionName As String = "GetBenefitRuleWA"
        Dim lReturn As Integer
        Try

            'presume no errors
            GetBenefitRuleWA = g_objXErrorMask.cSuccess

            objRule = Activator.CreateInstance(System.Type.GetType(sDLLClass))

            If bUseTwoYearRule = False Then
                lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.JurisTaxExemptions)
            Else
                lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventTwoYearDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.JurisTaxExemptions)
            End If

            If objRule.ErrorMaskSAWW > 0 Then
                m_ErrorMaskSAWW = objRule.ErrorMaskSAWW
                Exit Function
            End If

            Select Case lReturn
                Case -1 'normal, expected
                    If objRule.TableRowID = 0 Then
                        m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                        Exit Function
                    End If
                Case 0
                    g_sErrDescription = "Jurisdictional Benefit rule (" & sAbbv & ") was not found for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80000
                    m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)
                    Exit Function
                    'jtodd22 is not a true application error, is a true missing data error
                    'jtodd22 01/13/2005  Err.Raise g_lErrNum, g_sErrProcedure, g_sErrDescription
                Case Else
                    g_sErrDescription = "There was an error fetching the Jurisdictional Benefit rule (" & sAbbv & ") for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80001
                    GetBenefitRuleWA = g_lErrNum
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)
                    Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetBenefitRuleWA = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "(" & sDLLClass & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function GetBenefitRuleLIB(ByRef m_ErrorMask As Integer, ByRef m_ErrorMaskSAWW As Integer, ByRef sAbbv As String, ByRef objRule As Object, ByRef sDLLClass As String, Optional ByRef bUseTwoYearRule As Boolean = False) As Integer
        Const sFunctionName As String = "GetBenefitRuleLIB"
        Dim lReturn As Integer
        Try

            'presume no errors
            GetBenefitRuleLIB = g_objXErrorMask.cSuccess

            objRule = Activator.CreateInstance(System.Type.GetType(sDLLClass))
            Select Case UCase(g_objXClaim.FilingStatePostalCode)
                Case "NH"
                    lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.JurisTaxExemptions, g_objXClaim.ClaimantOriginalAWW)
                Case "WA"
                    lReturn = objRule.LoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.JurisTaxExemptions)
                Case Else
                    If bUseTwoYearRule = False Then
                        lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
                        ''                If objRule.ErrorMaskSAWW = g_objXErrorMask.cNoJurisdictionalRule Then
                        ''                    m_ErrorMaskSAWW = objRule.ErrorMaskSAWW
                        ''                    Exit Function
                        ''                End If
                    Else
                        lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventTwoYearDTG)
                        ''                If objRule.ErrorMaskSAWW = g_objXErrorMask.cNoJurisdictionalRule Then
                        ''                    m_ErrorMaskSAWW = objRule.ErrorMaskSAWW
                        ''                    Exit Function
                        ''                End If
                    End If
            End Select
            Select Case lReturn
                Case -1 'normal, expected
                    If objRule.TableRowID = 0 Then
                        m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                        Exit Function
                    End If
                Case 0

                    g_sErrDescription = "Jurisdictional Benefit rule (" & sAbbv & ") was not found for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80000
                    m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)

                    Exit Function
                    'jtodd22 is not a true application error, is a true missing data error
                    'jtodd22 01/13/2005  Err.Raise g_lErrNum, g_sErrProcedure, g_sErrDescription
                Case Else
                    g_sErrDescription = "There was an error fetching the Jurisdictional Benefit rule (" & sAbbv & ") for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80001
                    GetBenefitRuleLIB = g_lErrNum
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)
                    Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetBenefitRuleLIB = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "(" & sDLLClass & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function GetBenefitRuleTT(ByRef m_ErrorMask As Integer, ByRef m_ErrorMaskSAWW As Integer, ByRef sAbbv As String, ByRef objRule As Object, ByRef sDLLClass As String, Optional ByRef bUseTwoYearRule As Boolean = False) As Integer
        Const sFunctionName As String = "GetBenefitRuleTT"
        Dim lReturn As Integer
        Try

            'presume no errors
            GetBenefitRuleTT = g_objXErrorMask.cSuccess

            objRule = Activator.CreateInstance(System.Type.GetType(sDLLClass))
            Select Case UCase(g_objXClaim.FilingStatePostalCode)
                Case "NH"
                    lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.JurisTaxExemptions, g_objXClaim.ClaimantOriginalAWW)
                Case "WA"
                    lReturn = objRule.LoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.JurisTaxExemptions)
                Case Else
                    If bUseTwoYearRule = False Then
                        lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
                        If objRule.ErrorMaskSAWW = g_objXErrorMask.cNoJurisdictionalRule Then
                            m_ErrorMaskSAWW = objRule.ErrorMaskSAWW
                            Exit Function
                        End If
                    Else
                        lReturn = objRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventTwoYearDTG)
                        If objRule.ErrorMaskSAWW = g_objXErrorMask.cNoJurisdictionalRule Then
                            m_ErrorMaskSAWW = objRule.ErrorMaskSAWW
                            Exit Function
                        End If
                    End If
            End Select
            Select Case lReturn
                Case -1 'normal, expected
                    If objRule.TableRowID = 0 Then
                        m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                        Exit Function
                    End If
                Case 0

                    g_sErrDescription = "Jurisdictional Benefit rule (" & sAbbv & ") was not found for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80000
                    m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)

                    Exit Function
                    'jtodd22 is not a true application error, is a true missing data error
                    'jtodd22 01/13/2005  Err.Raise g_lErrNum, g_sErrProcedure, g_sErrDescription
                Case Else
                    g_sErrDescription = "There was an error fetching the Jurisdictional Benefit rule (" & sAbbv & ") for " & g_objXClaim.FilingStatePostalCode & "."
                    g_sErrProcedure = sDLLClass & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    g_lErrNum = 80001
                    GetBenefitRuleTT = g_lErrNum
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, "objRule.LoadDataByEventDate", g_sErrDescription)
                    Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetBenefitRuleTT = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "(" & sDLLClass & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function GetSpendableIncomeRule() As Integer
        Const sFunctionName As String = "GetSpendableIncomeRule"
        Dim lErrNumber As Integer
        Dim lReturn As Integer
        Dim lSpendable As Integer

        Dim objSpendableIncomeRule As Object

        Try
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin SubRoutine"
#End If

            GetSpendableIncomeRule = 0

            lSpendable = modFunctions.IsSpendableJurisdiction(lErrNumber)
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin SubRoutine"
#End If

            Select Case lSpendable
                Case g_lNoCodeID 'normal, expected
                    Exit Function
                Case g_lYesCodeID 'normal, expected
                    'do nothing
                Case Else 'probable errors
                    If lErrNumber = g_objXErrorMask.cNoValidAWWCalSetup Then
                        ''                sWarning = sWarning & "No valid AWW Calculation was found for this claim." & vbCrLf
                        ''                sWarning = sWarning & "Jurisdiction is " & g_objXClaim.FilingStatePostalCode & "." & vbCrLf
                        ''                sWarning = sWarning & "Event Date is " & g_objXClaim.DateOfEventCalar & "." & vbCrLf
                        g_sErrProcedure = "RMWCCalc|" & sModuleName & "." & sFunctionName & "|"
                        g_sErrDescription = "RMJuRuLib.CJRCalcAWW.LoadDataByJurisRowIDEffectiveDate|RMWCCalc." & sModuleName & "." & sFunctionName & "Data fetch failed."
                        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                        Exit Function
                    Else
                        Err.Raise(vbObjectError + 80000, "RMJuRuLib.CJRCalcAWW.LoadDataByJurisRowIDEffectiveDate|RMWCCalc." & sModuleName & "." & sFunctionName, "Data fetch failed.")
                        Exit Function
                    End If
            End Select

            objSpendableIncomeRule = New RMJuRuLib.CJRSpendableIncomeRule()
            lReturn = objSpendableIncomeRule.LoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            Select Case lReturn
                Case -1 'expected and normal
                    'jtodd22 02/09/2006  The LoadDataByClaimData function completed normally
                    If objSpendableIncomeRule.TableRowID = 0 Then
                        'jtodd22 02/09/2006 The LoadDataByClaimData did not return data, therefore no rule
                        GetSpendableIncomeRule = 0
                        objSpendableIncomeRule = Nothing
                        Exit Function
                    End If
                Case 0 'not expected, may not be an error
                    'jtodd22 02/09/2006 The LoadDataByClaimData function did not complete normally and did not generate an error.
                    objSpendableIncomeRule = Nothing

                Case Else 'not expected, is an error
                    Err.Raise(vbObjectError + 80000, "RMJuRuLib.CJRSpendableIncomeRule.LoadDataByClaimData|RMWCCalc." & sModuleName & ".GetSpendableIncomeRule", "Data fetch failed.")
                    objSpendableIncomeRule = Nothing
                    Exit Function
            End Select

            objSpendableIncomeRule = Nothing

            GetSpendableIncomeRule = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetSpendableIncomeRule = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            objSpendableIncomeRule = Nothing

        End Try

    End Function
    Public Function GrateBeneRatesSpendableIncome(ByRef objSpendableIncome As Object, ByRef AWW As Double, ByRef TaxExemptions As Integer, ByRef TaxStatusCode As Integer) As Double

        Const sFunctionName As String = "GrateBeneRatesSpendableIncome"
        Dim lReturn As Integer

        Try

            GrateBeneRatesSpendableIncome = 0

            If TaxStatusCode > 0 Then
                g_objXClaim.JurisTaxStatusCode = TaxStatusCode
                g_objXClaim.JurisTaxExemptions = TaxExemptions
                g_objXClaim.ClaimantOriginalAWW = AWW
            End If

            lReturn = GetSpendableIncomeData(objSpendableIncome)

            Select Case g_objXClaim.JurisTaxExemptions
                Case 0
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption00
                Case 1
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption01
                Case 2
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption02
                Case 3
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption03
                Case 4
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption04
                Case 5
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption05
                Case 6
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption06
                Case 7
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption07
                Case 8
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption08
                Case 9
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption09
                Case 10
                    GrateBeneRatesSpendableIncome = objSpendableIncome.Exemption10orMore
                Case Else
                    'Rhode Island--maybe
                    GrateBeneRatesSpendableIncome = -10
            End Select


            'let error handler reset GrateBeneRatesSpendableIncome value

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GrateBeneRatesSpendableIncome = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GrateBeneRatesDiscountedAWW(ByRef objCalculator As Object) As Integer
        Const sFunctionName As String = "GrateBeneRatesDiscountedAWW"
        Dim dRate As Double
        Try

            GrateBeneRatesDiscountedAWW = 0

            '    With g_objXClaim
            '        dRate = objCalculator.dGetBasicRate(.ClaimantOriginalAWW, .ClaimantTTDRate, .JurisTaxExemptions, .JurisTaxStatusCode, 0, .ClaimantHourlyPayRate)
            '    End With
            '    Me.JurisMaxBenRateWeek = objCalculator.objBenefitRule.MaxCompRate
            '    Me.JurisMaxBenRateDay = modFunctions.RoundStandard(objCalculator.objBenefitRule.MaxCompRate / objCalculator.objCalcBenefitRule.JurisWorkWeek, 2)
            '    Me.JurisMinBenRateWeek = 0
            '    Me.JurisMinBenRateDay = 0
            '
            '    If objCalculator.objBenefitRule.FloorAmount > 0 Then
            '        If objCalculator.objBenefitRule.DollarForDollar <> g_lYesCodeID Then
            '            Me.JurisMinBenRateWeek = objCalculator.objBenefitRule.FloorAmount
            '            Me.JurisMinBenRateDay = modFunctions.RoundStandard(objCalculator.objBenefitRule.FloorAmount / objCalculator.objCalcBenefitRule.JurisWorkWeek, 2)
            '            Exit Function
            '        Else
            '            Select Case objCalculator.objBenefitRule.FloorAmount
            '                Case Is >= Me.ClaimantJurisAWW
            '                    If objCalculator.objBenefitRule.DollarForDollar = g_lYesCodeID Then
            '                        Me.JurisMinBenRateWeek = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate, 2)
            '                        Me.JurisMinBenRateDay = modFunctions.RoundStandard(modFunctions.RoundStandard(g_objXClaim.AWWToCompensate, 2) / objCalculator.objCalcBenefitRule.JurisWorkWeek, 2)
            '                    End If
            '                Case Is >= (g_objXClaim.AWWToCompensate * objBenefitRule.PrimeRate)
            '                    Me.JurisMinBenRateWeek = modFunctions.RoundStandard(dRate, 2)
            '                    Me.JurisMinBenRateDay = modFunctions.RoundStandard(dRate / objCalculator.objCalcBenefitRule.JurisWorkWeek, 2)
            '                Case Else
            '            End Select
            '        End If
            '    End If

            GrateBeneRatesDiscountedAWW = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GrateBeneRatesDiscountedAWW = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function


    Public Function GetSpendableIncomeData(ByRef objSpendableIncome As Object) As Integer
        Const sFunctionName As String = "GetSpendableIncomeData"
        Dim lReturn As Integer
        Try

            GetSpendableIncomeData = 0

            objSpendableIncome = New RMJuRuLib.CJRSpendableIncome()
            lReturn = objSpendableIncome.LoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.ClaimantOriginalAWW)
            Select Case lReturn
                Case -1 'expected and normal
                    If objSpendableIncome.TableRowID = 0 Then GetSpendableIncomeData = 0
                Case 0 'not expected, may not be an error
                Case Else
                    Err.Raise(vbObjectError + 80000, "RMJuRuLib.CJRSpendableIncomeRule.LoadDataByClaimData|RMWCCalc.CWCTempTotlDis.GetSpendableIncomeRule", "Data fetch failed.")
                    Exit Function
            End Select
            GetSpendableIncomeData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetSpendableIncomeData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function ApplyMinMaxLimits(ByRef dBenefitRate As Double, ByRef dMinRate As Double, ByRef dMaxRate As Double) As Double

        Select Case dBenefitRate
            Case Is < dMinRate
                ApplyMinMaxLimits = dMinRate
            Case Is > dMaxRate
                ApplyMinMaxLimits = dMaxRate
            Case Else
                ApplyMinMaxLimits = dBenefitRate
        End Select



    End Function

    Public Function GetSpousalEID() As Integer
        Const sFunctionName As String = "GetSpousalEID"
        Dim objReader As DbReader
        Dim sSQL2 As String
        Dim sSQL3 As String
        Dim sSQL4 As String
        Dim sSQL5 As String
        Try
            GetSpousalEID = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'RELATION_CODE'"

            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT CODES.*, CODES_TEXT.CODE_DESC"
            sSQL3 = sSQL3 & " FROM CODES, CODES_TEXT"
            sSQL3 = sSQL3 & " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL3 = sSQL3 & " AND CODES.TABLE_ID = (" & sSQL2 & ")"
            sSQL3 = sSQL3 & " AND"
            sSQL3 = sSQL3 & " (CODES_TEXT.CODE_DESC LIKE '%wife%' OR"
            sSQL3 = sSQL3 & " CODES_TEXT.CODE_DESC LIKE '%Wife%' OR"
            sSQL3 = sSQL3 & " CODES_TEXT.CODE_DESC LIKE '%WIFE%' OR"
            sSQL3 = sSQL3 & " CODES_TEXT.CODE_DESC LIKE '%husband%' OR"
            sSQL3 = sSQL3 & " CODES_TEXT.CODE_DESC LIKE '%Husband%' OR"
            sSQL3 = sSQL3 & " CODES_TEXT.CODE_DESC LIKE '%HUSBAND%')"

            sSQL4 = ""
            sSQL4 = sSQL4 & "SELECT PERSON_INVOLVED.PI_ROW_ID"
            sSQL4 = sSQL4 & " FROM PERSON_INVOLVED, CLAIM, CLAIMANT"
            sSQL4 = sSQL4 & " WHERE CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
            sSQL4 = sSQL4 & " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID"
            sSQL4 = sSQL4 & " AND CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID"
            sSQL4 = sSQL4 & " AND CLAIM.CLAIM_ID = " & g_objXClaim.ClaimID

            sSQL5 = ""
            sSQL5 = sSQL5 & "SELECT DEPENDENT_EID"
            sSQL5 = sSQL5 & " FROM PI_X_DEPENDENT"
            sSQL5 = sSQL5 & " WHERE PI_ROW_ID = (" & sSQL4 & ")"
            sSQL5 = sSQL5 & " AND RELATION_CODE IN (" & sSQL3 & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL5)
            If (objReader.Read()) Then
                GetSpousalEID = objReader.GetInt32(0)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function sAddDTGDate(ByRef sFromDateDTG As String, ByRef lDays As Integer) As String
        Dim sDateCal As String
        If Len(sFromDateDTG) <> 8 Then Exit Function
        sDateCal = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, lDays, CDate(sDBDateFormat(sFromDateDTG, "Short Date"))))
        sDateCal = PadDateWithZeros(sDateCal)
        sAddDTGDate = Mid(sDateCal, 7) & Mid(sDateCal, 1, 2) & Mid(sDateCal, 4, 2)


    End Function

    Public Function sGetDBDateFormat(ByRef sDateCal As String) As String
        If Not IsDate(sDateCal) Then Exit Function
        sDateCal = PadDateWithZeros(sDateCal)
        sGetDBDateFormat = Mid(sDateCal, 7) & Mid(sDateCal, 1, 2) & Mid(sDateCal, 4, 2)
    End Function

    Public Function CheckBenefitTransactionMapping(ByRef sWarning As String, ByRef lBenefitID As Integer) As Integer



    End Function
    Public Function CheckDatesAreMonthsApart(ByRef sMessage As String, ByRef oWorkingInputs As CWCXUserInputs) As Boolean
        Const sFunctionName As String = "CheckDatesAreMonthsApart"
        Dim lMonthsDiff As Integer

        Try

            CheckDatesAreMonthsApart = False

            If Not (IsDate(oWorkingInputs.FirstPaymentDateCalendar) And IsDate(oWorkingInputs.LastPaymentDateCalendar)) Then
                oWorkingInputs.Warning = "One or both payment date(s) is/are invalid."
                Exit Function
            End If

            'jtodd22 --find if months are whole, not partial
            lMonthsDiff = GetNumberOfMonths((oWorkingInputs.FirstPaymentDateDBFormat), (oWorkingInputs.LastPaymentDateDBFormat))

            If System.DateTime.FromOADate(DateAdd(Microsoft.VisualBasic.DateInterval.Month, lMonthsDiff, CDate(oWorkingInputs.FirstPaymentDateCalendar)).ToOADate - 1) <> CDate(oWorkingInputs.LastPaymentDateCalendar) Then
                sMessage = ""
                sMessage = sMessage & "The benefit with earnings must cover whole months." & vbCrLf
                sMessage = sMessage & "because you are paying monthly." & vbCrLf
                sMessage = sMessage & "Generally the last date is one day less than the first date." & vbCrLf
                sMessage = sMessage & "Remember that payment dates are inclusive." & vbCrLf
                sMessage = sMessage & "Please adjust the Benefit End Date."
                Exit Function
            Else
                oWorkingInputs.NumberOfTimeUnits = lMonthsDiff
            End If

            CheckDatesAreMonthsApart = True

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function CheckEarningsVersusMonthsIsGood(ByRef sMessage As String, ByVal WorkingInputs As CWCXUserInputs) As Boolean
        Const sFunctionName As String = "CheckEarningsVersusMonthsIsGood"
        Dim lFirstDateMonth As Integer
        Dim lLastDateMonth As Integer
        Dim lMonthsDiff As Integer
        Dim sFirstDateDBFormat As String
        Dim sLastDateDBFormat As String
        Dim sTmp As String

        Try

            CheckEarningsVersusMonthsIsGood = False
            sMessage = ""
            If WorkingInputs.Earnings = 0 Then
                CheckEarningsVersusMonthsIsGood = True
                Exit Function
            End If

            If CheckDatesAreMonthsApart(sMessage, WorkingInputs) = False Then
                sMessage = sMessage & "The benefit with earnings must cover whole months." & vbCrLf
                sMessage = sMessage & "because you are paying monthly." & vbCrLf
                sMessage = sMessage & "Generally the last date is one day less than the first date." & vbCrLf
                sMessage = sMessage & "Remember that payment dates are inclusive." & vbCrLf
                sMessage = sMessage & "Please adjust the Number of Days or the Last Date."
                Exit Function
            End If

            CheckEarningsVersusMonthsIsGood = True

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function CheckEarningsVersusTimeIsGood(ByRef sMessage As String, ByVal WorkingInputs As CWCXUserInputs) As Boolean

        CheckEarningsVersusTimeIsGood = False
        sMessage = ""

        If UCase(Trim(Left(WorkingInputs.BenefitTypeText, 3))) = "MO" Or UCase(Trim(Left(WorkingInputs.BenefitTypeText, 3))) = "PM" Then
            CheckEarningsVersusTimeIsGood = modFunctions.CheckEarningsVersusMonthsIsGood(sMessage, WorkingInputs)
        Else
            CheckEarningsVersusTimeIsGood = modFunctions.CheckEarningsVersusWeeksIsGood(sMessage, WorkingInputs)
        End If
        If sMessage > "" Then
            Exit Function
        End If

        CheckEarningsVersusTimeIsGood = True



    End Function
    Public Function CheckEarningsVersusWeeksIsGood(ByRef sMessage As String, ByVal WorkingInputs As CWCXUserInputs) As Boolean
        Const sFunctionName As String = "CheckEarningsVersusWeeksIsGood"

        Try

            CheckEarningsVersusWeeksIsGood = True
            sMessage = ""

            If WorkingInputs.Earnings > 0 Then
                If WorkingInputs.NumberOfTimeUnits Mod 7 <> 0 Then
                    sMessage = sMessage & "Temporary benefits with earnings must cover whole weeks." & vbCrLf
                    sMessage = sMessage & "Please add " & 7 - (WorkingInputs.NumberOfTimeUnits Mod 7) & " days" & vbCrLf
                    sMessage = sMessage & "or substract " & (WorkingInputs.NumberOfTimeUnits Mod 7) & " days." & vbCrLf
                    sMessage = sMessage & "Please adjust Number of Days and Last Date."
                    CheckEarningsVersusWeeksIsGood = False
                End If
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function DayCounterDayKiller(ByRef sFromDateCalr As String, ByRef sToDateCalr As String, ByRef iExcludeSunday As Short, ByRef iExcludeMonday As Short, ByRef iExcludeTuesday As Short, ByRef iExcludeWednesday As Short, ByRef iExcludeThursday As Short, ByRef iExcludeFriday As Short, ByRef iExcludeSaturday As Short) As Integer
        Const sFunctionName As String = "DayCounterDayKiller"
        Dim iArray(7) As Short
        Dim i As Short
        Dim iKillDay As Short
        Dim lDayCount As Integer
        Dim sDateCalr As String

        Try

            DayCounterDayKiller = 0

            If Not IsDate(sFromDateCalr) Or Not IsDate(sToDateCalr) Then Exit Function

            lDayCount = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sFromDateCalr), CDate(sToDateCalr)) + 1

            'load array
            iArray(1) = iExcludeSunday
            iArray(2) = iExcludeMonday
            iArray(3) = iExcludeTuesday
            iArray(4) = iExcludeWednesday
            iArray(5) = iExcludeThursday
            iArray(6) = iExcludeFriday
            iArray(7) = iExcludeSaturday



            For i = 1 To 7

                iKillDay = 0

                If iArray(i) = 1 Then iKillDay = FirstDayOfWeek.Sunday : iArray(i) = 0
                If iArray(i) = 1 Then iKillDay = FirstDayOfWeek.Monday : iArray(i) = 0
                If iArray(i) = 1 Then iKillDay = FirstDayOfWeek.Tuesday : iArray(i) = 0
                If iArray(i) = 1 Then iKillDay = FirstDayOfWeek.Wednesday : iArray(i) = 0
                If iArray(i) = 1 Then iKillDay = FirstDayOfWeek.Thursday : iArray(i) = 0
                If iArray(i) = 1 Then iKillDay = FirstDayOfWeek.Friday : iArray(i) = 0
                If iArray(i) = 1 Then iKillDay = FirstDayOfWeek.Saturday : iArray(i) = 0

                sDateCalr = sFromDateCalr

                While CDate(sDateCalr) < DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, CDate(sToDateCalr))
                    If Weekday(CDate(sDateCalr)) = iKillDay Then lDayCount = lDayCount - 1
                    sDateCalr = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, CDate(sDateCalr)))
                End While

            Next

            DayCounterDayKiller = lDayCount
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function
    Public Function CheckTemporaryTotalIsGood(ByRef sMessage As String, ByRef lJurisRowID As Integer, ByRef sDateOfEventDB As String, ByRef dHourlyRate As Double) As Integer
        Const sFunctionName As String = "CheckTemporaryTotalIsGood"
        Dim lReturn As Integer
        Dim objCalculator As Object
        Dim objErrorMask As New CWCEnumErrorMask
        Dim sAbbr As String
        Dim sSQL As String
        Try
            CheckTemporaryTotalIsGood = -1

            sAbbr = GetTempTotalAbbreviation(sMessage, lJurisRowID)
            If Trim(sMessage) = "" Then
                lReturn = GetCalculator(objCalculator, sAbbr, lJurisRowID, sDateOfEventDB)
                If lReturn <> 0 Then
                    sMessage = ErrorMaskProcessing(sAbbr, (objErrorMask.cNoJurisdictionalRule), 0, 0, 0)
                Else
                    lReturn = objCalculator.Standard.LoadData(dHourlyRate)
                    If lReturn <> 0 Then
                        sMessage = ErrorMaskProcessing(sAbbr, (objErrorMask.cNoJurisdictionalRule), 0, 0, 0)
                    Else
                        CheckTemporaryTotalIsGood = 0
                    End If
                End If
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            objCalculator = Nothing
            objErrorMask = Nothing

        End Try

    End Function
    Public Function ErrorMaskProcessing(ByRef sAbbreviation As String, ByRef lErrorMask As Integer, ByRef lErrorMaskSAWW As Integer, ByRef lErrorMaskCalcSetup As Integer, ByRef lErrorMaskSpendData As Integer) As String
        Dim sJurisEvent As String
        Dim sMissingRule As String
        Dim sTmp As String

        ErrorMaskProcessing = ""
        sJurisEvent = ""
        sJurisEvent = sJurisEvent & vbCrLf & vbCrLf
        sJurisEvent = sJurisEvent & "Jurisdiction is:  " & GetStateName_SQL((g_objXClaim.FilingStateID)) & "." & vbCrLf
        sJurisEvent = sJurisEvent & "Date of Event:  " & sDBDateFormat((g_objXClaim.DateOfEventDTG), "Short Date") & "."
        sMissingRule = ""

        Select Case lErrorMaskCalcSetup
            Case 0 'normal, expected
            Case g_objXErrorMask.cNoJurisdictionalRule
                sMissingRule = sMissingRule & "The Benefit Calculator Rule was not found."

            Case g_objXErrorMask.cNoTransTypeCode
                sMissingRule = sMissingRule & "The Transactions Type Code was not found."

            Case Else 'data issue
                sMissingRule = sMissingRule & "A data issue occurred with Calculation Setup, " & lErrorMaskCalcSetup & "."
        End Select

        Select Case lErrorMaskSAWW
            Case 0 'normal, expected

            Case g_objXErrorMask.cNoDependentsOnRecord
                sMissingRule = sMissingRule & "There are no Dependents on record."

            Case g_objXErrorMask.cNoEarnings
                sMissingRule = sMissingRule & "Earnings are required for this Benefit Type."

            Case g_objXErrorMask.cNoImpairment
                sMissingRule = sMissingRule & "An Impairment Rating is required for rate/payment calculation." & vbCrLf
                sMissingRule = sMissingRule & "Please return to the Claim screen and enter an Impairment Rating."

            Case g_objXErrorMask.cSplitPayment
                sMissingRule = sMissingRule & "Payments can not be made across the rate change date."

            Case g_objXErrorMask.cNoJurisdictionalRule
                sMissingRule = sMissingRule & "Supporting SAWW data was not found."

            Case Else 'data issue
                sMissingRule = sMissingRule & "A data issue occurred in SAWW, " & lErrorMaskSAWW & "."
        End Select

        Select Case lErrorMaskSpendData
            Case 0 'normal, expected
            Case Else
                sMissingRule = sMissingRule & "Supporting Spendable Income data was not found."
        End Select

        Select Case lErrorMask
            Case g_objXErrorMask.cSuccess '0      'normal, expected

            Case g_objXErrorMask.cMissingTaxStatus
                sMissingRule = sMissingRule & GetStateName_SQL((g_objXClaim.FilingStateID)) & " is a Tax Status/Spendable Income jurisdiction." & vbCrLf
                sMissingRule = sMissingRule & "A tax status was not found for this claim, " & g_objXClaim.ClaimNumber & "." & vbCrLf & vbCrLf
                sMissingRule = sMissingRule & "Please return to the AWW Calculator and re-calculated the AWW."

            Case g_objXErrorMask.cNoDateOfDeath '262144
                sMissingRule = sMissingRule & "There is no Date of Death."

            Case g_objXErrorMask.cNoDependentsOnRecord
                sMissingRule = sMissingRule & "There are no Dependents on record."

            Case g_objXErrorMask.cNoEarnings
                sMissingRule = sMissingRule & "Earnings are required for this Benefit Type."

            Case g_objXErrorMask.cNoImpairment
                sMissingRule = sMissingRule & "An Impairment Rating is required for rate/payment calculation." & vbCrLf & vbCrLf
                sMissingRule = sMissingRule & "Please return to the Claim screen and enter an Impairment Rating."

            Case g_objXErrorMask.cNoImpairmentDate '4194304
                sMissingRule = sMissingRule & "An Impairment (MMI) Date is required for payment date validation." & vbCrLf & vbCrLf
                sMissingRule = sMissingRule & "Please return to the Claim screen and enter an Impairment (MMI) Date."

            Case g_objXErrorMask.cNoJurisdictionalRule '8192   'No Jurisdictional Rule Found
                sMissingRule = sMissingRule & "The Benefit, ('" & UCase(sAbbreviation) & "',) Rule was not found."

            Case g_objXErrorMask.cNoSpendableIncomeData
                sMissingRule = sMissingRule & "The Spendable Income/Tax Table data was not found."

            Case g_objXErrorMask.cNoBodyMemberData '524288
                'missing body member data
                sMissingRule = sMissingRule & "The Scheduled Loss data from Jurisdictional Rules was not found." & vbCrLf & vbCrLf
                sMissingRule = sMissingRule & "Please have the Scheduled Loss data for Body Members added to Jurisdictional Rules."

            Case g_objXErrorMask.cNoTransTypeCode
                sMissingRule = sMissingRule & "No Transaction Type Code was found."

            Case Else 'data issue
                sMissingRule = sMissingRule & "A data issue occurred, " & lErrorMask & "."
        End Select

        If sMissingRule > "" Then
            ErrorMaskProcessing = sMissingRule & vbCrLf & vbCrLf & sJurisEvent
        End If



    End Function


    Public Function GetBodyMemberJurisdictionIndor() As Integer


    End Function

    Public Function GetRateSpendable(ByRef dRate As Double, ByRef lErrorMaskSpendData As Integer, ByVal AWW As Double) As Integer

        Const sFunctionName As String = "GetRateSpendable"
        Dim lReturn As Integer
        Dim objSpendableIncome As Object

        Try

            GetRateSpendable = g_objXErrorMask.cSuccess
            lErrorMaskSpendData = g_objXErrorMask.cSuccess

            objSpendableIncome = New RMJuRuLib.CJRSpendableIncome()
            'LoadDataByClaimData(objUser As RMUser,lFilingStateID As Long, sDateOfEventDTG As String,lTaxStatusCode As Long,lAWW As Long
            lReturn = objSpendableIncome.LoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, CInt(AWW))
            Select Case lReturn
                Case -1 'expected and normal
                    If objSpendableIncome.TableRowID = 0 Then
                        lReturn = 0
                        lErrorMaskSpendData = g_objXErrorMask.cNoSpendableIncomeData
                        objSpendableIncome = Nothing
                        Exit Function
                    End If
                Case 0 'not expected, may not be an error
                    lErrorMaskSpendData = g_objXErrorMask.cNoSpendableIncomeData
                    g_lErrLine = Erl()
                    g_lErrNum = vbObjectError + 8000
                    g_sErrDescription = ""
                    g_sErrDescription = g_sErrDescription & vbCrLf
                    g_sErrDescription = g_sErrDescription & "Failed to load CJRSpendableIncome." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "Jurisdiction is; " & g_objXClaim.FilingStatePostalCode & "." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "Date of Event is; " & g_objXClaim.DateOfEventCalar & "." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "Tax Status Is; " & modFunctions.GetCodeDesc_SQL((g_objXClaim.JurisTaxStatusCode)) & ".  AWW is " & Format(AWW, "Currency")

                    g_sWarning = g_sErrDescription

                    g_sErrProcedure = "RMWCCalc." & sModuleName & "." & sFunctionName
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                    objSpendableIncome = Nothing
                    Exit Function

                Case Else
                    Err.Raise(vbObjectError + 80000, "RMJuRuLib.CJRSpendableIncolreturn =lLoadData(dEmployeeHourlyRate)ByClaimData|RMWCCalc.ModFunctions.GetRateSpendable", "Data fetch failed.")
                    objSpendableIncome = Nothing
                    Exit Function
            End Select

            Select Case g_objXClaim.JurisTaxExemptions
                Case 0
                    dRate = objSpendableIncome.Exemption00
                Case 1
                    dRate = objSpendableIncome.Exemption01
                Case 2
                    dRate = objSpendableIncome.Exemption02
                Case 3
                    dRate = objSpendableIncome.Exemption03
                Case 4
                    dRate = objSpendableIncome.Exemption04
                Case 5
                    dRate = objSpendableIncome.Exemption05
                Case 6
                    dRate = objSpendableIncome.Exemption06
                Case 7
                    dRate = objSpendableIncome.Exemption07
                Case 8
                    dRate = objSpendableIncome.Exemption08
                Case 9
                    dRate = objSpendableIncome.Exemption09
                Case 10
                    dRate = objSpendableIncome.Exemption10orMore
            End Select

            'let the error handler reset GetRateSpendable return value
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            GetRateSpendable = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetRateWashingtonState(ByRef dRate As Double, ByRef lErrorMaskSpendData As Integer, ByVal AWW As Double) As Integer

        Const sFunctionName As String = "GetRateWashingtonState"
        Const sRuleClassName As String = "CJRBenefitRuleTTDWA"
        Const sTableName As String = "WCP_RULE_TTD_WA"

        Dim lReturn As Integer
        Dim objSpendableIncome As Object

        Try

            GetRateWashingtonState = g_objXErrorMask.cSuccess
            lErrorMaskSpendData = g_objXErrorMask.cSuccess

            objSpendableIncome = New RMJuRuLib.CJRBenefitRuleTTDWA()

            lReturn = objSpendableIncome.lLoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, g_objXClaim.JurisTaxStatusCode, g_objXClaim.JurisTaxExemptions)
            Select Case lReturn
                Case -1 'expected and normal
                    If objSpendableIncome.TableRowID = 0 Then
                        lReturn = 0
                        lErrorMaskSpendData = g_objXErrorMask.cNoSpendableIncomeData
                        objSpendableIncome = Nothing
                        Exit Function
                    End If
                Case 0 'not expected, may not be an error
                    lErrorMaskSpendData = g_objXErrorMask.cNoSpendableIncomeData
                    g_lErrLine = Erl()
                    g_lErrNum = vbObjectError + 8000
                    g_sErrDescription = "Failed to load" & sRuleClassName & ". Jurisdiction is; " & g_objXClaim.FilingStatePostalCode & ".  Tax Status Is; " & modFunctions.GetCodeDesc_SQL((g_objXClaim.JurisTaxStatusCode)) & ".  AWW is " & Format(AWW, "Currency")
                    g_sErrProcedure = "RMWCCalc." & sModuleName & "." & sFunctionName
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                    objSpendableIncome = Nothing
                    Exit Function

                Case Else
                    Err.Raise(vbObjectError + 80000, "RMJuRuLib." & sRuleClassName & ".LoadDataByClaimData|RMWCCalc.ModFunctions." & sFunctionName, "Data fetch failed.")
                    objSpendableIncome = Nothing
                    Exit Function
            End Select

            dRate = g_objXClaim.AWWToCompensate * objSpendableIncome.BenefitPercentage
            If dRate > objSpendableIncome.MaxCompRate Then dRate = objSpendableIncome.MaxCompRate

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            GetRateWashingtonState = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetRegularChecksPendingAmount() As Double
        Const subRoutineName As String = "GetAutoChecksPendingAmount"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Dim sTransTypeCodeIDs As String

        Try

            GetRegularChecksPendingAmount = 0

            sTransTypeCodeIDs = GetTransTypeCodeIDsIndemnity()

            sSQL2 = ""
            sSQL2 = sSQL2 & ModSQLStatements.SelectCheckStatusCodeIds
            sSQL2 = sSQL2 & " AND CODES_TEXT.CODE_DESC <> 'Printed'"

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " SUM(FUNDS_TRANS_SPLIT.AMOUNT)"
            sSQL = sSQL & " FROM FUNDS, FUNDS_TRANS_SPLIT"
            sSQL = sSQL & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
            sSQL = sSQL & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL = sSQL & " AND FUNDS.STATUS_CODE IN (" & sSQL2 & ")"
            sSQL = sSQL & " AND FUNDS.VOID_FLAG = 0"
            sSQL = sSQL & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeIDs & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetRegularChecksPendingAmount = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function
    Public Function CalculateFirstDateMonths(ByRef oUserInputs As CWCXUserInputs) As Integer

        With oUserInputs
            If IsDate(oUserInputs.LastPaymentDateCalendar) Then
                If IsNumeric(oUserInputs.NumberOfTimeUnits) Then
                    If oUserInputs.NumberOfPayments > 0 Then
                        oUserInputs.FirstPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Month, -oUserInputs.NumberOfTimeUnits, CDate(oUserInputs.LastPaymentDateCalendar)))
                        oUserInputs.FirstPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, CDate(oUserInputs.FirstPaymentDateCalendar)))
                        If Trim(oUserInputs.FirstPaymentDateDBFormat & "") = "" Then
                            oUserInputs.FirstPaymentDateDBFormat = modFunctions.sGetDBDateFormat((oUserInputs.FirstPaymentDateCalendar))
                        End If
                    End If
                Else
                    If IsNumeric(oUserInputs.NumberOfPayments) Then
                        If oUserInputs.NumberOfPayments > 0 Then
                            oUserInputs.FirstPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Month, oUserInputs.NumberOfPayments, CDate(oUserInputs.LastPaymentDateCalendar)))
                            oUserInputs.FirstPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, CDate(oUserInputs.FirstPaymentDateCalendar)))
                            If Trim(oUserInputs.FirstPaymentDateDBFormat & "") = "" Then
                                oUserInputs.FirstPaymentDateDBFormat = modFunctions.sGetDBDateFormat((oUserInputs.FirstPaymentDateCalendar))
                            End If
                        End If
                    End If
                End If
            End If
        End With

        CalculateFirstDateMonths = 0



    End Function
    Public Function CalculateLastDateMonths(ByRef oUserInputs As CWCXUserInputs) As Integer

        With oUserInputs
            If IsDate(oUserInputs.FirstPaymentDateCalendar) Then
                If IsNumeric(oUserInputs.NumberOfTimeUnits) Then
                    If oUserInputs.NumberOfPayments > 0 Then
                        oUserInputs.LastPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Month, oUserInputs.NumberOfTimeUnits, CDate(oUserInputs.FirstPaymentDateCalendar)))
                        oUserInputs.LastPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, CDate(oUserInputs.LastPaymentDateCalendar)))
                        If Trim(oUserInputs.LastPaymentDateDBFormat & "") = "" Then
                            oUserInputs.LastPaymentDateDBFormat = modFunctions.sGetDBDateFormat((oUserInputs.LastPaymentDateCalendar))
                        End If
                    End If
                Else
                    If IsNumeric(oUserInputs.NumberOfPayments) Then
                        If oUserInputs.NumberOfPayments > 0 Then
                            oUserInputs.LastPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Month, oUserInputs.NumberOfPayments, CDate(oUserInputs.FirstPaymentDateCalendar)))
                            oUserInputs.LastPaymentDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, CDate(oUserInputs.LastPaymentDateCalendar)))
                            If Trim(oUserInputs.LastPaymentDateDBFormat & "") = "" Then
                                oUserInputs.LastPaymentDateDBFormat = modFunctions.sGetDBDateFormat((oUserInputs.LastPaymentDateCalendar))
                            End If
                        End If
                    End If
                End If
            End If
        End With

        CalculateLastDateMonths = 0



    End Function
    Public Function GetMartialStatusCode() As Integer
        Const sFunctionName As String = "GetMartialStatusCode"
        Dim objReader As DbReader
        Dim sSQL2 As String
        Try

            GetMartialStatusCode = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT "
            sSQL2 = sSQL2 & " MARITAL_STAT_CODE"
            sSQL2 = sSQL2 & " FROM PERSON_INVOLVED"
            sSQL2 = sSQL2 & " WHERE EVENT_ID = " & g_objXClaim.EventID
            sSQL2 = sSQL2 & " AND PI_EID = " & g_objXClaim.ClaimantEID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL2)
            If (objReader.Read()) Then
                GetMartialStatusCode = objReader.GetInt32("MARITAL_STAT_CODE")
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function GetMMIImpairment(ByRef dMMIRate As Double, ByRef sMMIDateDTG As String) As Integer
        Const sFunctionName As String = "GetMMIImpairment"
        Dim objReader As DbReader
        Dim sSQL2 As String
        Dim sSQL3 As String
        Dim sSQL4 As String
        Try

            GetMMIImpairment = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT "
            sSQL2 = sSQL2 & " PI_ROW_ID"
            sSQL2 = sSQL2 & " FROM PERSON_INVOLVED"
            sSQL2 = sSQL2 & " WHERE EVENT_ID = " & g_objXClaim.EventID
            sSQL2 = sSQL2 & " AND PI_EID = " & g_objXClaim.ClaimantEID

            sSQL3 = sSQL3 & "SELECT "
            sSQL3 = sSQL3 & " MAX(PI_X_MMIROW_ID)"
            sSQL3 = sSQL3 & " FROM PI_X_MMI_HIST"
            sSQL3 = sSQL3 & " WHERE PI_ROW_ID = (" & sSQL2 & ")"

            sSQL4 = ""
            sSQL4 = sSQL4 & "SELECT"
            sSQL4 = sSQL4 & " MMI_DATE,PERCENT_DISABILITY"
            sSQL4 = sSQL4 & " FROM PI_X_MMI_HIST"
            sSQL4 = sSQL4 & " WHERE PI_ROW_ID = (" & sSQL2 & ")"
            sSQL4 = sSQL4 & " AND PI_X_MMIROW_ID = (" & sSQL3 & ")"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL4)
            If (objReader.Read()) Then
                sMMIDateDTG = objReader.GetString("MMI_DATE")
                dMMIRate = objReader.GetDouble("PERCENT_DISABILITY")    ''RMA-1046 Issue Fixed
            End If
            'let the error handler reset GetMMIImpairment
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            GetMMIImpairment = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    '               123456789012345678901234567890
    Public Function GetIndemnityBenefitTypeNumber() As Integer
        Const sFunctionName As String = "GetIndemnityBenefitTypeNumber"
        Dim cBenefitTypes As Object
        Dim lReturn As Integer
        Dim lTypes As Integer
        Try
            GetIndemnityBenefitTypeNumber = 0
            lTypes = 0

            cBenefitTypes = New RMJuRuLib.CJRBenefitTypes()
            lReturn = cBenefitTypes.LoadData(g_objUser)
            If cBenefitTypes.Count = 0 Then
                g_sWarning = "There are no Work Comp Benefit Types defined in this datasource." & "Please contact your System Administrator."
                g_sErrSrc = "RMJuRuLib"
                g_sErrDescription = g_sWarning
                g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
                g_lErrLine = Erl()
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
                Exit Function
            End If

            For lTypes = 1 To cBenefitTypes.Count
                If InStr(1, UCase(cBenefitTypes.Item(lTypes).TypeDesc), UCase("Indemnity"), CompareMethod.Text) > 0 And InStr(1, UCase(cBenefitTypes.Item(lTypes).TypeDesc), UCase("Commutation"), CompareMethod.Text) = 0 Then
                    Exit For
                End If
            Next lTypes

            GetIndemnityBenefitTypeNumber = lTypes

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            cBenefitTypes = Nothing

        End Try

    End Function
    Public Function GetIndemnityAmountPending() As Double
        Const sFunctionName As String = "GetIndemnityAmountPending"
        Dim dSubTotal As Double
        Dim objReader As DbReader
        Dim sSQL2 As String
        Dim sSQL3 As String
        Dim sTransTypeCodeIDs As String

        Try

            GetIndemnityAmountPending = 0

            dSubTotal = 0
            sTransTypeCodeIDs = GetTransTypeCodeIDsIndemnity()

            sSQL2 = ""
            sSQL2 = sSQL2 & ModSQLStatements.SelectCheckStatusCodeIds
            sSQL2 = sSQL2 & " AND (CODES_TEXT.CODE_DESC <> 'Printed' AND CODES_TEXT.CODE_DESC <> 'PRINTED')"

            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) AS S_AMOUNT"
            sSQL3 = sSQL3 & " FROM FUNDS, FUNDS_TRANS_SPLIT"
            sSQL3 = sSQL3 & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
            sSQL3 = sSQL3 & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL3 = sSQL3 & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL3 = sSQL3 & " AND FUNDS.VOID_FLAG = 0"
            sSQL3 = sSQL3 & " AND FUNDS.STATUS_CODE IN (" & sSQL2 & ")"
            sSQL3 = sSQL3 & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeIDs & ")"

            If modGlobals.g_dbMake <> Riskmaster.Db.eDatabaseType.DBMS_IS_ACCESS Then sSQL3 = Replace(sSQL3, " AS ", " ") 'Remove alias As(notneeded except for access)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If (objReader.Read()) Then
                dSubTotal = objReader.GetInt32("S_AMOUNT")
            End If


            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT SUM(FUNDS_AUTO_SPLIT.AMOUNT) AS S_AMOUNT"
            sSQL3 = sSQL3 & " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT"
            sSQL3 = sSQL3 & " WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID"
            sSQL3 = sSQL3 & " AND FUNDS_AUTO.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL3 = sSQL3 & " AND FUNDS_AUTO.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL3 = sSQL3 & " AND FUNDS_AUTO.STATUS_CODE IN (" & sSQL2 & ")"
            sSQL3 = sSQL3 & " AND FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeIDs & ")"

            If modGlobals.g_dbMake <> Riskmaster.Db.eDatabaseType.DBMS_IS_ACCESS Then sSQL3 = Replace(sSQL3, " AS ", " ") 'Remove alias As(notneeded except for access)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If (objReader.Read()) Then
                dSubTotal = dSubTotal + objReader.GetInt32("S_AMOUNT")
            End If


            GetIndemnityAmountPending = dSubTotal
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function
    Public Function GetJurisdefinedWorkWeek() As Integer
        Const sFunctionName As String = "GetJurisDefinedWorkWeek"

        Dim objRule As Object
        Dim lReturn As Integer

        Try

            GetJurisdefinedWorkWeek = 0

            objRule = New RMJuRuLib.CJRCalcBenefit()
            lReturn = objRule.LoadDataByJurisRowIDEffectiveDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            Select Case lReturn
                Case -1 'expected and normal
                    If objRule.TableRowID = 0 Then
                        GetJurisdefinedWorkWeek = g_objXErrorMask.cNoJurisdictionalRule
                        objRule = Nothing
                        Exit Function
                    End If
                Case Else 'error
                    Err.Raise(vbObjectError + 80000, "RMJuRuLib.CJRCalcAWW.LoadDataByJurisRowIDEffectiveDate|RMWCCalc." & sModuleName & "." & sFunctionName, "Data fetch failed.")
                    objRule = Nothing
                    Exit Function
            End Select

            GetJurisdefinedWorkWeek = objRule.JurisWorkWeek
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            GetJurisdefinedWorkWeek = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            objRule = Nothing

        End Try

    End Function
    Public Function IsPureCatchupPayment(ByRef sMessage As String, ByRef oUserInputs As CWCXUserInputs) As Boolean
        IsPureCatchupPayment = False
        sMessage = ""

        With oUserInputs
            If CDate(.LastPaymentDateCalendar) < Today Then
                IsPureCatchupPayment = True
            End If
        End With



    End Function
    Public Function IsSpendableJurisdiction(ByRef lErrorNumber As Integer) As Integer
        Const sFunctionName As String = "IsSpendableJurisdiction"
        Dim lReturn As Integer
        Dim objRuleJurisType As Object
        Try
            IsSpendableJurisdiction = 0

            objRuleJurisType = New RMJuRuLib.CJRCalcAWW()
            lReturn = objRuleJurisType.LoadDataByJurisRowIDEffectiveDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            Select Case lReturn
                Case 0
                    'jtodd22 02/12/2008 This is the 'LoadDataByJurisRowIDEffectiveDate' default value
                    'jtodd22 02/12/2008 'LoadDataByJurisRowIDEffectiveDate' should NEVER return a 0 (zero)
                    IsSpendableJurisdiction = 0
                    Exit Function
                Case -1
                    'jtodd22 02/12/2008 This is the 'LoadDataByJurisRowIDEffectiveDate' exit value
                    If objRuleJurisType.TableRowID = 0 Then
                        'jtodd22 02/12/2008 There was no rule for "RMJuRuLib.CJRCalcAWW"
                        lErrorNumber = g_objXErrorMask.cNoValidAWWCalSetup
                        IsSpendableJurisdiction = 0
                        Exit Function
                    End If
                Case Else
                    'error--should trigger sub routines error handler
            End Select

            IsSpendableJurisdiction = objRuleJurisType.TaxStatusSpendableIncome


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            lErrorNumber = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            objRuleJurisType = Nothing
        End Try

    End Function

    Public Function GetCalculator(ByRef objReturned As Object, ByRef sAbbreviation As String, ByRef lJurisRowID As Integer, ByRef sDateOfEvent As String) As Integer
        Dim lErrorNumber As Integer
        Dim lSpendableStatus As Integer

        Const sFunctionName As String = "GetCalculator"

        Try
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin SubRoutine"
#End If

            GetCalculator = 0
            lErrorNumber = 0

            lSpendableStatus = IsSpendableJurisdiction(lErrorNumber)
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "lSpendableStatus value is:  " & lSpendableStatus
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "g_lYesCodeID value is:  " & g_lYesCodeID
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "g_lNoCodeID value is:  " & g_lNoCodeID
#End If

            Select Case lSpendableStatus
                Case g_lYesCodeID
                    'Rule for setup of tax tables was found.
                Case g_lNoCodeID
                    'Rule for setup of tax tables was not found.
                Case Else
                    'Fatal error, tax table status was not resolved.
                    lSpendableStatus = lSpendableStatus + 0
                    g_sErrDescription = ""
                    g_sErrDescription = g_sErrDescription & "Failed to resolved tax table status." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "An unexpected value of,'" & lSpendableStatus & "', was returned." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "Jurisdiction is:  " & modFunctions.GetStateName_SQL(lJurisRowID) & "." & vbCrLf
                    If (lSpendableStatus = 0) Then
                        g_sErrDescription = g_sErrDescription & "The Jurisdictional Rules Set Up may be missing."
                    End If
                    g_lErrNum = vbObjectError + 500
                    g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, vbCrLf & g_sErrDescription)
                    Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
                    Exit Function
            End Select

#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin 'Select Case UCase$(sAbbreviation)'"
#End If
            Select Case UCase(sAbbreviation) 'Master select statement
                Case "BBS"
                    objReturned = New CWCDeathBurial

                Case "DB"
                    objReturned = New CWCDeathBurial

                Case "DBB", "DBF"
                    objReturned = New CWCDeathBurial

                Case "DBS"
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    objReturned = New CWCDeathSuv
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    objReturned = New CWCDeathSuv
                            End Select
                    End Select

                Case "DC" 'Dependency Coverage
                    objReturned = New CWCDependencyCoverage

                Case "DF", "DFB" 'Disfigurement Benefit
                    objReturned = New CWCDisFig

                Case "DIBS" 'Death Income Benefits--Texas only
                    objReturned = New CWCDeathIncomeTexas

                Case "HP"
                    objReturned = New CWCHealPeriod

                Case "IB" 'FL, Impairment Income Benefits
                    objReturned = New CWCImpairIncome

                Case "IIBS" 'TX, Impairment Income Benefits
                    objReturned = New CWCImpairIncome

                Case "LIBS" 'TX, Lifetime Income Benefits
                    'TX Lifetime Income is the same as Permanent Partial
                    objReturned = New CWCLifeTimeIncomeTexas

                Case "LOEP" 'Washington, State Of
                    objReturned = New CWCLossOfEarningPower

                Case "LP" 'no rate, CA, Life Pension, no detail
                    objReturned = New CWCLifePen

                Case "LPCOM" 'no rate, CA, commutation, no detail
                    objReturned = New CWCCommLife

                Case "MIB"
                    'Colorado Medical Impairment Benefit
                    objReturned = New CWCMedImpair

                Case "O" 'LA, Indemnity-Other Benefits
                    'jtodd22 not a real benefit, carried from FROI
                Case "PBA" 'no rate, CA, Permanent Benefit Advance
                    objReturned = New CWCPermPartAdvance

                Case "PD" 'Partial Disability, Section 35, Mass
                    objReturned = New cwcPartialDisabilitySec35Mass

                Case "PI"
                    objReturned = New CWCStatPermImpairFlorida

                Case "PIB" 'no rate, NOT USED

                Case "PID"
                    'AK, Permanent Impairment Benefits
                    'AK PID is same as PPD
                    'Alaska's PID is normally paid as a lump sum
                    objReturned = New CWCPermPartSpendAK


                Case "PPD"
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "AK"
                                    objReturned = New CWCPermPartSpendAK
                                Case Else
                                    objReturned = New CWCPermPartSpendable
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "CA"
                                    'jtodd22 California is a freak
                                    'jtodd22 California Permanent Partial is Non-Scheduled but called Permanent
                                    'jtodd22 California rules are unique to California
                                    objReturned = New CWCPermPartCalifornia

                                Case Else
                                    objReturned = New CWCPermPartDis

                            End Select
                        Case Else
                    End Select

                Case "PPDCOM" 'no rate, commutation, no detail
                    Select Case GetStatePostalCode_SQL(lJurisRowID)
                        Case "CA"
                            objReturned = New CWCCommPermPartCA
                        Case Else
                            objReturned = New CWCCommPermPartAA
                    End Select

                Case "PPD-S", "S-PPD"
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "AK"
                                    objReturned = New CWCPermPartSpendAK
                                Case Else
                                    objReturned = New CWCPermPartSpendable

                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "CA"
                                    'jtodd22 California is a freak
                                    'jtodd22 California Permanent Partial is Non-Scheduled but called Permanent
                                    'jtodd22 California rules are unique to California
                                    objReturned = New CWCPermPartCalifornia

                                Case Else
                                    objReturned = New CWCPermPartDis

                            End Select
                        Case Else
                    End Select

                Case "PPD-U", "U-PPD"
                    'Jurisdictional Rules Coding for this type of Benifit is not completed.
                    'This is whole body impairment

                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "AK"
                                    objReturned = New CWCPermPartSpendAK
                                Case Else
                                    objReturned = New CWCPermPartSpendable
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "CA"
                                    'jtodd22 California is a freak
                                    'jtodd22 California Permanent Partial is Non-Scheduled but called Permanent
                                    'jtodd22 California rules are unique to California
                                    objReturned = New CWCPermPartCalifornia

                                Case "IL" 'jtodd22 03/21/2010
                                    objReturned = New CWCPermPartIllinois

                                Case Else
                                    'jtodd22 12/18/2007 this is "PPD"--Set objReturned = New CWCPermPartDis
                                    objReturned = New CWCPermPartDisctNotSched
                            End Select
                        Case 0
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "CA"
                                    'jtodd22 California is a freak
                                    'jtodd22 California Permanent Partial is Non-Scheduled but called Permanent
                                    'jtodd22 California rules are unique to California
                                    objReturned = New CWCPermPartCalifornia

                                Case Else
                                    'jtodd22 12/18/2007 this is "PPD"--Set objReturned = New CWCPermPartDis
                                    objReturned = New CWCPermPartDisctNotSched
                            End Select

                    End Select

                Case "PRB" 'KS, Physical Rehabilitation Benefits
                    objReturned = New CWCPhysicalRehabilitation

                Case "PTD"
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    objReturned = New CWCPermTotalDis
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    objReturned = New CWCPermTotalDis
                            End Select
                    End Select
                Case "PTS" 'FL, Permanent Total Supplemental
                    objReturned = New CWCPermImpairSupp

                Case "RM" 'CO, Rehabilitation Maintenance
                    objReturned = New CWCVocRehab

                Case "SB" 'FL, Supplemental Income Benefits
                    objReturned = New CWCSuppEarningsLA

                Case "SDB" 'MD, Serious Disability Benefits
                    'MD SDB is a sub class of Permanent Partial
                    objReturned = New CWCPermPartDis

                Case "SE" 'LA, Indemnity-Supplemental Earnings
                    'http://www.lwcc.com/articles_legal.cfm?A=142&C=3
                    ''Set objReturned = New CWCSuppEarningsLA
                    objReturned = New CWCTempPartNtSDiscountAA
                Case "SIBS" 'TX, Supplemental Income Benefits
                    objReturned = New CWCSuppIncomeTexas

                Case "SPI" 'FL, Satuatory Permanent Impairment
                    objReturned = New CWCStatPermImpairFlorida

                Case "TIB-P"
                    objReturned = New CWCTempPartNtSDiscountAA

                Case "TIB-T"
                    objReturned = New CWCTempTotalNtSDiscountAA

                Case "TIB"
                    'Texas Tempory Income Benefits
                    objReturned = New CWCTempIncomeTexas

                Case "TIBS"
                    'Texas Tempory Income Benefits
                    objReturned = New CWCTempIncomeTexas

                Case "TDB" 'Total Disability Benefits, Indiana
                    objReturned = New CWCTotalDisabilityIndiana

                Case "TP"
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    objReturned = New CWCTempPartNtSSpendAA
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "NY"
                                    objReturned = New CWCTempPartNtSDiscountNY
                                Case Else
                                    objReturned = New CWCTempPartNtSDiscountAA
                            End Select
                    End Select

                Case "TPD" 'no rate, earnings required, Temporary Partial Benefits
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "AK"
                                    objReturned = New CWCTempPartSpendAK
                                Case "IA"
                                    objReturned = New CWCTempPartSpendIA
                                Case "WA"
                                    objReturned = New CWCTotalDisabilityWashington
                                Case Else
                                    'Set objReturned = New CWCTempTotlDis
                                    objReturned = New CWCWorkLossDis_Occ
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    'Set objReturned = New CWCTempTotlDis
                                    objReturned = New CWCWorkLossDis_Occ
                            End Select
                    End Select
                Case "TPI" 'SC, Temporary Partial Incapacity
                    objReturned = New CWCTempPartNtSDiscountAA

                Case "TT"
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    objReturned = New CWCTempTotalNtSSpendAA
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case Else
                                    objReturned = New CWCTempTotalNtSDiscountAA
                            End Select
                    End Select

                Case "TTC" 'FL, Temporary Total Benefits @ 80%
                    objReturned = New CWCTempTotal80Florida

                Case "TTD"
                    Select Case lSpendableStatus
                        Case g_lYesCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "WA"
                                    objReturned = New CWCTotalDisabilityWashington
                                Case Else
                                    objReturned = New CWCTempSpendable
                            End Select
                        Case g_lNoCodeID
                            Select Case GetStatePostalCode_SQL(lJurisRowID)
                                Case "ID"
                                    objReturned = New CWCTotalDisabilityIdaho

                                Case "IL"
                                    objReturned = New CWCTotalDisabilityIllinois

                                Case "IN"
                                    objReturned = New CWCTotalDisabilityIndiana

                                Case "NH"
                                    objReturned = New CWCTotalDisabilityNewHampsh


                                Case Else
                                    objReturned = New CWCTempTotlDis

                            End Select
                    End Select

                Case "TTE" 'FL, Temporary Total Benefits, T & E
                    objReturned = New CWCTempTotalTEFlorida

                Case "TTI" 'SC, Temporary Total Incapacity
                    objReturned = New CWCTempTotlDis

                Case "VRB" 'Vocational Rehabilitation Benefits
                    objReturned = New CWCVocRehab

                Case "VRM" 'Vocational Rehabilitation Maintenance
                    objReturned = New CWCVocRehab

                Case "VRMA" 'Vocational Rehab Maint. Allow
                    objReturned = New CWCVocRehab

                Case "VRMAS" 'no rate, CA, Vocational Rehab Maint. Allow Supplement from Permanent Partial
                    objReturned = New CWCVocRehabSupp

                Case "WL"
                    objReturned = New CWCWageLoss

                Case "WRB" 'AZ, Wage Reimbursement Benefits
                    objReturned = New CWCWorkLossDis_Occ

                Case Else
                    g_sErrDescription = ""
                    g_sErrDescription = g_sErrDescription & GetStatePostalCode_SQL(lJurisRowID) & " Benefit Abbreviation, '" & sAbbreviation & "', was not found." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "Please inform your Administrator"
                    Err.Raise(vbObjectError + 90000, sModuleName & "." & sFunctionName, g_sErrDescription)
            End Select
            GetCalculator = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If Trim(Err.Description & "") > "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            GetCalculator = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetCalcBenefitRule(ByRef lErrorMask As Integer, ByRef lErrorMaskSAWW As Integer, ByRef lErrorMaskCalcSetup As Integer, ByRef objCalcBenefitRule As Object) As Integer
        Const sFunctionName As String = "GetCalcBenefitRule"
        Dim lReturn As Integer
        Try

#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin SubRoutine"
#End If

            GetCalcBenefitRule = 0

            objCalcBenefitRule = New RMJuRuLib.CJRCalcBenefit()

#If RMXR4 Then
            objCalcBenefitRule.JurisWorkWeekOriginal = 7
            objCalcBenefitRule.JurisWorkWeek = 7
            objCalcBenefitRule.TTDPayPeriodCode = 0
            Exit Function
#End If

            lReturn = objCalcBenefitRule.LoadDataByJurisRowIDEffectiveDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            Select Case lReturn
                Case 0 'no jurisdictional rule
#If DEBUGGER Then
				'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
				LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Call 'objCalcBenefitRule.LoadDataByJurisRowIDEffectiveDate' returned 0 (zero), early exit."
#End If
                    lErrorMaskCalcSetup = g_objXErrorMask.cNoJurisdictionalRule
                    Exit Function
                Case -1 'normally expected
                    If objCalcBenefitRule.TableRowID = 0 Then
                        lErrorMaskCalcSetup = g_objXErrorMask.cNoJurisdictionalRule
                        Exit Function
                    End If
                Case Else 'error

            End Select
            If objCalcBenefitRule.JurisWorkWeekOriginal = 8 Then
                lReturn = modFunctions.JurisWorkWeekByPIWorkWeek(objCalcBenefitRule)
            End If
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Value of objCalcBenefitRule.JurisWorkWeek is:  " & objCalcBenefitRule.JurisWorkWeek
		LogError "|RMWCCalc." & sModuleName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "End SubRoutine"
#End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            GetCalcBenefitRule = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function JurisWorkWeekByPIWorkWeek(ByRef objCalcBenefitRule As Object) As Integer
        Const sFunctionName As String = "JurisWorkWeekByPIWorkWeek"
        Dim objReader As DbReader
        Dim iTemp As Short
        Dim sSQL As String

        Try

            JurisWorkWeekByPIWorkWeek = 0

            sSQL = ""
            sSQL = sSQL & "SELECT * FROM PERSON_INVOLVED"
            sSQL = sSQL & " WHERE PI_EID = " & g_objXClaim.ClaimantEID
            sSQL = sSQL & " AND EVENT_ID = " & g_objXClaim.EventID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                With objCalcBenefitRule
                    iTemp = 0
                    iTemp = .ExcludeSunday + .ExcludeMonday + .ExcludeTuesday + .ExcludeWednesday + .ExcludeThursday + .ExcludeFriDay + .ExcludeSaturday
                    .JurisWorkWeek = 7 - iTemp
                End With
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            JurisWorkWeekByPIWorkWeek = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    Public Function PaymentCruncherMonth(ByRef objCalling As Object, ByRef colWorkSheet As Collection, ByRef dRate As Double) As Integer
        Const sFunctionName As String = "PaymentCruncherMonth"
        Dim lDaysInAutoPayment As Integer

        Dim lReturn As Integer

        Try

            PaymentCruncherMonth = 0

            If objCalling.objCalcBenefitRule.JurisWorkWeekOriginal = 8 Then
                colWorkSheet.Add("|Paid days in Payment have been corrected to match Claimant's Work Week|")
                lReturn = JurisWorkWeekByPIWorkWeek(objCalling.objCalcBenefitRule)
            End If
            If g_objXPaymentParms.AutoNumberOfPayments > 0 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            .AutoDays = modFunctions.DayCounterDayKiller(.AutoStartDate, .AutoEndDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                        End With
                        objCalling.Standard.CalculatedPaymentAuto = dRate * g_objXPaymentParms.AutoMonths
                        colWorkSheet.Add("     |Auto Payment is|" & Format(objCalling.Standard.CalculatedPaymentAuto, "Currency"))
                        colWorkSheet.Add("     |Number of Auto Payments|" & g_objXPaymentParms.AutoNumberOfPayments)
                        colWorkSheet.Add("     |Subtotal of Auto Payments|" & Format(objCalling.Standard.CalculatedPaymentAuto * g_objXPaymentParms.AutoNumberOfPayments, "Currency"))
                    Case 7
                        'lDaysInAutoPayment = g_objXPaymentParms.GetAutoDays()
                        objCalling.Standard.CalculatedPaymentAuto = dRate
                        colWorkSheet.Add("     |Auto Payment is|" & Format(dRate, "Currency"))
                        colWorkSheet.Add("     |Number of Auto Payments|" & g_objXPaymentParms.AutoNumberOfPayments)
                        colWorkSheet.Add("     |Subtotal of Auto Payments|" & Format(objCalling.Standard.CalculatedPaymentAuto * g_objXPaymentParms.AutoNumberOfPayments, "Currency"))
                End Select
            End If

            If g_objXPaymentParms.CatchUpNumberOfPayments = 1 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            .CatchUpDays = modFunctions.DayCounterDayKiller(.CatchUpStartDate, .CatchUpEndDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                        End With
                        objCalling.Standard.CalculatedPaymentCatchUp = dRate * g_objXPaymentParms.CatchUpMonths
                        colWorkSheet.Add("     |Catch Up Payment is|" & Format(objCalling.Standard.CalculatedPaymentCatchUp, "Currency"))
                    Case 7
                        objCalling.Standard.CalculatedPaymentCatchUp = dRate * g_objXPaymentParms.CatchUpMonths
                        colWorkSheet.Add("     |Catch Up Payment is|" & Format(objCalling.Standard.CalculatedPaymentCatchUp, "Currency"))
                End Select
            End If

            If g_objXPaymentParms.RegularNumberOfPayments = 1 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            lReturn = modFunctions.DayCounterDayKiller(.RegularStartDate, .RegularEndDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                            g_objXPaymentParms.RegularDays = lReturn
                        End With
                        objCalling.Standard.CalculatedPaymentRegular = dRate * g_objXPaymentParms.RegularMonths
                        colWorkSheet.Add("     |Regular Payment is|" & Format(objCalling.Standard.CalculatedPaymentRegular, "Currency"))
                    Case 7
                        objCalling.Standard.CalculatedPaymentRegular = dRate * g_objXPaymentParms.RegularMonths
                        colWorkSheet.Add("     |Regular Payment is|" & Format(objCalling.Standard.CalculatedPaymentRegular, "Currency"))
                End Select
            End If

            If g_objXPaymentParms.WaitingNumberOfPayments = 1 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            .WaitingDays = modFunctions.DayCounterDayKiller(.WaitingStartDate, .WaitingStartDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                        End With
                        objCalling.Standard.CalculatedPaymentWaitingPeriod = dRate * g_objXPaymentParms.WaitingMonths
                        colWorkSheet.Add("     |Waiting Period Payment is|" & Format(objCalling.Standard.CalculatedPaymentWaitingPeriod, "Currency"))
                    Case 7
                        objCalling.Standard.CalculatedPaymentWaitingPeriod = dRate * g_objXPaymentParms.WaitingMonths
                        colWorkSheet.Add("     |Waiting Period Payment is|" & Format(objCalling.Standard.CalculatedPaymentWaitingPeriod, "Currency"))
                End Select
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            PaymentCruncherMonth = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function PaymentCruncherWeek(ByRef objCalling As Object, ByRef colWorkSheet As Collection, ByRef dRate As Double) As Integer
        Const sFunctionName As String = "PaymentCruncherWeek"
        Dim dProposedPaymentWeeks As Double
        Dim dWeeksOver As Double
        Dim lDays As Integer
        Dim lDaysInAutoPayment As Integer
        Dim lReturn As Integer
        Dim sTmp As String

        Try

            PaymentCruncherWeek = 0

            If objCalling.objCalcBenefitRule.JurisWorkWeekOriginal = 8 Then
                colWorkSheet.Add("|Paid days in Payment have been corrected to match Claimant's Work Week|")
                lReturn = JurisWorkWeekByPIWorkWeek(objCalling.objCalcBenefitRule)
            End If
            If g_objXPaymentParms.AutoNumberOfPayments > 0 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            .AutoDays = modFunctions.DayCounterDayKiller(.AutoStartDate, .AutoEndDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                        End With
                        objCalling.Standard.CalculatedPaymentAuto = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.AutoDays
                        colWorkSheet.Add("     |Auto Payment is|" & Format(objCalling.Standard.CalculatedPaymentAuto, "Currency"))
                        colWorkSheet.Add("     |Number of Auto Payments|" & g_objXPaymentParms.AutoNumberOfPayments)
                        colWorkSheet.Add("     |Subtotal of Auto Payments|" & Format(objCalling.Standard.CalculatedPaymentAuto * g_objXPaymentParms.AutoNumberOfPayments, "Currency"))
                    Case 7
                        lDaysInAutoPayment = g_objXPaymentParms.GetAutoDays()
                        objCalling.Standard.CalculatedPaymentAuto = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * lDaysInAutoPayment
                        colWorkSheet.Add("     |Auto Payment is|" & Format(objCalling.Standard.CalculatedPaymentAuto, "Currency"))
                        colWorkSheet.Add("     |Number of Auto Payments|" & g_objXPaymentParms.AutoNumberOfPayments)
                        colWorkSheet.Add("     |Subtotal of Auto Payments|" & Format(objCalling.Standard.CalculatedPaymentAuto * g_objXPaymentParms.AutoNumberOfPayments, "Currency"))
                End Select
            End If

            If g_objXPaymentParms.CatchUpNumberOfPayments = 1 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            .CatchUpDays = modFunctions.DayCounterDayKiller(.CatchUpStartDate, .CatchUpEndDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                        End With
                        objCalling.Standard.CalculatedPaymentCatchUp = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.CatchUpDays
                        colWorkSheet.Add("     |Catch Up Payment is|" & Format(objCalling.Standard.CalculatedPaymentCatchUp, "Currency"))
                    Case 7
                        objCalling.Standard.CalculatedPaymentCatchUp = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.CatchUpDays
                        colWorkSheet.Add("     |Catch Up Payment is|" & Format(objCalling.Standard.CalculatedPaymentCatchUp, "Currency"))
                End Select
            End If

            If g_objXPaymentParms.RegularNumberOfPayments = 1 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            lReturn = modFunctions.DayCounterDayKiller(.RegularStartDate, .RegularEndDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                            g_objXPaymentParms.RegularDays = lReturn
                        End With
                        objCalling.Standard.CalculatedPaymentRegular = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.RegularDays
                        colWorkSheet.Add("     |Regular Payment is|" & Format(objCalling.Standard.CalculatedPaymentRegular, "Currency"))
                    Case 7
                        objCalling.Standard.CalculatedPaymentRegular = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.RegularDays
                        colWorkSheet.Add("     |Regular Payment is|" & Format(objCalling.Standard.CalculatedPaymentRegular, "Currency"))
                End Select
            End If

            If g_objXPaymentParms.WaitingNumberOfPayments = 1 Then
                Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                    Case Is < 7
                        With g_objXPaymentParms
                            .WaitingDays = modFunctions.DayCounterDayKiller(.WaitingStartDate, .WaitingStartDate, objCalling.objCalcBenefitRule.ExcludeSunday, objCalling.objCalcBenefitRule.ExcludeMonday, objCalling.objCalcBenefitRule.ExcludeTuesday, objCalling.objCalcBenefitRule.ExcludeWednesday, objCalling.objCalcBenefitRule.ExcludeThursday, objCalling.objCalcBenefitRule.ExcludeFriDay, objCalling.objCalcBenefitRule.ExcludeSaturday)
                        End With
                        objCalling.Standard.CalculatedPaymentWaitingPeriod = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.WaitingDays
                        colWorkSheet.Add("     |Waiting Period Payment is|" & Format(objCalling.Standard.CalculatedPaymentWaitingPeriod, "Currency"))
                    Case 7
                        objCalling.Standard.CalculatedPaymentWaitingPeriod = (dRate / objCalling.objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.WaitingDays
                        colWorkSheet.Add("     |Waiting Period Payment is|" & Format(objCalling.Standard.CalculatedPaymentWaitingPeriod, "Currency"))
                End Select
            End If

            'jtodd22 check for overpayment
            If objCalling.Standard.RuleTotalWeeks > 0 Then
                dProposedPaymentWeeks = 0
                sTmp = ""
                With g_objXPaymentParms
                    lDays = .AutoDays + .CatchUpDays + .RegularDays + .WaitingDays
                End With
                dProposedPaymentWeeks = modFunctions.RoundStandard(lDays / objCalling.objCalcBenefitRule.JurisWorkWeek, 3)

                If dProposedPaymentWeeks > (objCalling.Standard.RuleTotalWeeks - objCalling.Standard.PaidPendingWeeks) Then
                    dWeeksOver = modFunctions.RoundStandard(dProposedPaymentWeeks + objCalling.Standard.PaidPendingWeeks - objCalling.Standard.RuleTotalWeeks, 3)
                    sTmp = ""
                    sTmp = sTmp & "The proposed payments, " & dProposedPaymentWeeks & " weeks, will cause the benefit" & vbCrLf
                    sTmp = sTmp & "to be over paid by " & ((dProposedPaymentWeeks + objCalling.Standard.PaidPendingWeeks) - objCalling.Standard.RuleTotalWeeks) & " weeks." & vbCrLf & vbCrLf

                    Select Case objCalling.objCalcBenefitRule.JurisWorkWeek
                        Case 7
                            If IsDate(objCalling.objUserInputsOriginal.FirstPaymentDateCalendar) Then
                                If CDate(objCalling.objUserInputsOriginal.FirstPaymentDateCalendar) < Today Then
                                    sTmp = sTmp & "Please reduce the days/dates by " & dWeeksOver & " weeks, or " & modFunctions.RoundStandard(dWeeksOver * 7, 0) & " days."
                                Else
                                    sTmp = sTmp & "Please reduce the number of new payments by " & dWeeksOver & " weeks, or " & modFunctions.RoundStandard(dWeeksOver * 7, 0) & " days."
                                End If
                            Else
                                sTmp = sTmp & "Please reduce the number of new payments by " & dWeeksOver & " weeks, or " & modFunctions.RoundStandard(dWeeksOver * 7, 0) & " days."
                            End If
                        Case Else
                            If IsDate(objCalling.objUserInputsOriginal.FirstPaymentDateCalendar) Then
                                If CDate(objCalling.objUserInputsOriginal.FirstPaymentDateCalendar) < Today Then
                                    sTmp = sTmp & "Please reduce the days/dates by " & dWeeksOver & " weeks."
                                Else
                                    sTmp = sTmp & "Please reduce the number of new payments by " & dWeeksOver & " weeks."
                                End If
                            Else
                                sTmp = sTmp & "Please reduce the number of new payments by " & dWeeksOver & " weeks."
                            End If
                    End Select

                    objCalling.Standard.Warning = sTmp

                End If
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            PaymentCruncherWeek = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function GetDateOfDeathDBFormat(ByRef lErrorMask As Integer) As String
        Const sFunctionName As String = "GetDateOfDeathDBFormat"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetDateOfDeathDBFormat = ""
            lErrorMask = 0

            sSQL = ""
            sSQL = sSQL & "SELECT * FROM PERSON_INVOLVED"
            sSQL = sSQL & " WHERE PI_EID = " & g_objXClaim.ClaimantEID
            sSQL = sSQL & " AND EVENT_ID = " & g_objXClaim.EventID
            sSQL = sSQL & " AND DATE_OF_DEATH IS NOT NULL"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetDateOfDeathDBFormat = objReader.GetString("DATE_OF_DEATH")
            Else
                lErrorMask = g_objXErrorMask.cNoDateOfDeath
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function GetNumberOfDependentsRaw(ByRef lErrorMask As Integer) As Integer
        Const sFunctionName As String = "GetNumberOfDependentsRaw"
        Dim l As Integer
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetNumberOfDependentsRaw = 0

            lErrorMask = 0

            sSQL = ""
            sSQL = sSQL & "SELECT COUNT(DEPENDENT_EID)"
            sSQL = sSQL & " FROM PI_X_DEPENDENT, PERSON_INVOLVED"
            sSQL = sSQL & " WHERE PI_X_DEPENDENT.PI_ROW_ID = PERSON_INVOLVED.PI_ROW_ID"
            sSQL = sSQL & " AND PI_EID = " & g_objXClaim.ClaimantEID
            sSQL = sSQL & " AND EVENT_ID = " & g_objXClaim.EventID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                l = objReader.GetInt32(0)
            Else
                lErrorMask = g_objXErrorMask.cNoDependentsOnRecord
            End If

            GetNumberOfDependentsRaw = l
            If l = 0 Then lErrorMask = g_objXErrorMask.cNoDependentsOnRecord

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function
    Public Function GetNumberOfMonths(ByRef sFirstDateDB As String, ByRef sLastDateDB As String) As Integer
        Const sFunctionName As String = "GetNumberOfMonths"
        Dim lFirstDateMonth As Integer
        Dim lLastDateMonth As Integer
        Dim lMonthsDiff As Integer

        Try

            GetNumberOfMonths = 0

            lFirstDateMonth = CInt(Mid(sFirstDateDB, 1, 4)) * 12 + CInt(Mid(sFirstDateDB, 5, 2))
            lLastDateMonth = CInt(Mid(sLastDateDB, 1, 4)) * 12 + CInt(Mid(sLastDateDB, 5, 2))

            lMonthsDiff = lLastDateMonth - lFirstDateMonth

            'jtodd22 special case when both dates are in same month Example 08/01 and 08/31
            If lFirstDateMonth = lLastDateMonth Then lMonthsDiff = 1

            GetNumberOfMonths = lMonthsDiff

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function GetBankAccountWithSub(ByVal lBankAccId As Integer) As Integer
        Const sFunctionName As String = "GetBankAccountWithSub"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetBankAccountWithSub = 0

            sSQL = "SELECT CODES.RELATED_CODE_ID FROM CODES WHERE CODE_ID = " & lBankAccId
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetBankAccountWithSub = objReader.GetInt32(0)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetPayeeType_Claimant() As Integer
        Const sFunctionName As String = "GetPayeeType_Claimant"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetPayeeType_Claimant = -1
            sSQL = ""
            sSQL = sSQL & "SELECT CODES.CODE_ID FROM CODES, CODES_TEXT"
            sSQL = sSQL & " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'PAYEE_TYPE')"
            sSQL = sSQL & " AND CODES_TEXT.CODE_DESC = 'Claimant'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetPayeeType_Claimant = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetPaymentType(ByRef oUserInputs As CWCXUserInputs) As Integer
        GetPaymentType = 0
        Select Case UCase(Trim(Left(oUserInputs.PaymentPeriodText, 3)))
            Case "PB", "BI"
                GetPaymentType = 14
            Case "PW", "WK"
                GetPaymentType = 7
            Case "MO", "PM"
                GetPaymentType = -1
        End Select


    End Function
    Public Function GetTableID(ByRef sTableName As String) As Integer
        Const sFunctionName As String = "GetTableID"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetTableID = 0

            If Trim(sTableName) = "" Then
                Exit Function
            End If

            sSQL = ""
            sSQL = sSQL & "SELECT TABLE_ID FROM GLOSSARY"
            sSQL = sSQL & " WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetTableID = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetPermPartAbbreviation(ByRef sWarning As String, ByRef lJurisRowID As Integer) As String
        Const subRoutineName As String = "GetPermPartAbbreviation"
        Dim colBenefitDefs As Object
        Dim i As Short
        Dim lPosImpairment As Integer
        Dim lPosPartial As Integer
        Dim lPosPermanent As Integer
        Dim lReturn As Integer

        Dim lTypes As Integer
        Dim sAbbr As String
        Dim sTest As String
        Dim sJurisPostalCode As String
        Try

            'find the jurisdiction
            'look for Permanent Partial as part of the definition

            GetPermPartAbbreviation = ""
            sAbbr = ""
            sJurisPostalCode = modFunctions.GetStatePostalCode_SQL(lJurisRowID)
            sWarning = ""

            lTypes = GetIndemnityBenefitTypeNumber()
            If lTypes = 0 Then
                sWarning = g_sWarning
                'jtodd22 this is fatal to loading the Benefit Definitions collection
                'jtodd22 the error is thrown in GetIndemnityBenefitTypeNumber
                Exit Function
            End If

            colBenefitDefs = New RMJuRuLib.CJRJurisBenefitDefs()
            'Public Function LoadData(objuser As RMUser, lJurisRowID As Long, lDefinitionType As Long, lUseInCalculator As Long) As Long
            lReturn = colBenefitDefs.LoadData(g_objUser, lJurisRowID, lTypes, -1)

            If lReturn = -1 Then

                sTest = "PPD"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "U-PPD"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "PPD-U"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "IB"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "IIBS" 'Texas
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i


                sTest = "PD"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "PI"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "PIB"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "PID" 'Alaska
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i


                sTest = "S-PPD"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i

                sTest = "PPD-S"
                For i = 1 To colBenefitDefs.Count
                    If sTest = colBenefitDefs.Item(i).Abbreviation Then
                        GetPermPartAbbreviation = Trim(colBenefitDefs.Item(i).Abbreviation)
                        Exit Function
                    End If
                Next i
            End If


            If Trim(sAbbr & "") = "" Then
                sWarning = ""
                sWarning = sWarning & "Benefit Abbreviation for the Permanent Partial benefit was not found." & vbCrLf
                sWarning = sWarning & "The probable cause is the benefit is not set to 'Use In Calculator'" & vbCrLf
                sWarning = sWarning & "in JurisRules.  Less probable is that the benefit 'Permanent Partial'" & vbCrLf
                sWarning = sWarning & "or 'Permanent Impairment' is mis-spelled in Jurisdictional Rules." & vbCrLf & vbCrLf
                sWarning = sWarning & "Please inform your Administrator"
            End If

            GetPermPartAbbreviation = Trim(sAbbr & "")

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & g_sThisDLLName & "." & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try
    End Function

    Public Function GetPermTotalAbbreviation(ByRef sWarning As String, ByRef lJurisRowID As Integer) As String
        Const subRoutineName As String = "GetTempTotalAbbreviation"
        Dim colBenefitDefs As Object
        Dim i As Short
        Dim lReturn As Integer
        Dim lTypes As Integer
        Dim sAbbr As String
        Dim sJurisPostalCode As String

        Try

            'find the jurisdiction
            'look for temporary total as part of the definition

            GetPermTotalAbbreviation = ""
            sAbbr = ""
            sJurisPostalCode = modFunctions.GetStatePostalCode_SQL(lJurisRowID)
            sWarning = ""

            lTypes = GetIndemnityBenefitTypeNumber()
            If lTypes = 0 Then
                sWarning = g_sWarning
                'jtodd22 this is fatal to loading the Benefit Definitions collection
                'jtodd22 the error is thrown in GetIndemnityBenefitTypeNumber
                Exit Function
            End If

            colBenefitDefs = New RMJuRuLib.CJRJurisBenefitDefs()

            'Public Function LoadData(objuser As RMUser, lJurisRowID As Long, lDefinitionType As Long, lUseInCalculator As Long) As Long
            lReturn = colBenefitDefs.LoadData(g_objUser, lJurisRowID, lTypes, -1)

            Select Case sJurisPostalCode
                Case "TX"
                    sAbbr = "LIBS"
                Case Else
                    If lReturn = -1 Then
                        For i = 1 To colBenefitDefs.Count
                            If InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("PERMANENT"), CompareMethod.Text) > 0 And (InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("TOTAL"), CompareMethod.Text) + InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("INCOME"), CompareMethod.Text)) > 0 Then
                                sAbbr = colBenefitDefs.Item(i).Abbreviation
                                If sJurisPostalCode <> "TX" Then
                                    Exit For
                                End If
                                If sAbbr <> UCase("TIBS") And InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("PARTIAL"), CompareMethod.Text) = 0 Then
                                    Exit For
                                End If
                            End If
                        Next i
                    End If
            End Select

            If sAbbr = "" Then
                sWarning = ""
                sWarning = sWarning & "Benefit Abbreviation for the Permanent Total benefit was not found." & vbCrLf
                sWarning = sWarning & "The probable cause is the benefit is not set to 'Use In Calculator'" & vbCrLf
                sWarning = sWarning & "in JurisRules.  Less probable is that the benefit 'Permanent Total'" & vbCrLf
                sWarning = sWarning & "or 'Permanent Income' is mis-spelled in Jurisdictional Rules." & vbCrLf & vbCrLf
                sWarning = sWarning & "Please inform your Administrator"
            End If

            GetPermTotalAbbreviation = sAbbr

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Public Function GetTempTotalAbbreviation(ByRef sWarning As String, ByRef lJurisRowID As Integer) As String
        Const subRoutineName As String = "GetTempTotalAbbreviation"
        Dim colBenefitDefs As Object
        Dim i As Short
        Dim lReturn As Integer
        Dim lTypes As Integer
        Dim sAbbr As String
        Dim sJurisPostalCode As String
        Try

            'find the jurisdiction
            'look for temporary total as part of the definition

            GetTempTotalAbbreviation = ""
            sAbbr = ""
            sJurisPostalCode = modFunctions.GetStatePostalCode_SQL(lJurisRowID)
            sWarning = ""

            lTypes = GetIndemnityBenefitTypeNumber()
            If lTypes = 0 Then
                sWarning = g_sWarning
                'jtodd22 this is fatal to loading the Benefit Definitions collection
                'jtodd22 the error is thrown in GetIndemnityBenefitTypeNumber
                Exit Function
            End If

            colBenefitDefs = New RMJuRuLib.CJRJurisBenefitDefs()
            'Public Function LoadData(objuser As RMUser, lJurisRowID As Long, lDefinitionType As Long, lUseInCalculator As Long) As Long
            lReturn = colBenefitDefs.LoadData(g_objUser, lJurisRowID, lTypes, -1)

            Select Case modFunctions.GetStatePostalCode_SQL(lJurisRowID)
                Case Else
                    If lReturn = -1 Then
                        For i = 1 To colBenefitDefs.Count
                            If InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("TEMPORARY"), CompareMethod.Text) > 0 And (InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("TOTAL"), CompareMethod.Text) + InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("INCOME"), CompareMethod.Text)) > 0 Then
                                sAbbr = colBenefitDefs.Item(i).Abbreviation
                                If modFunctions.GetStatePostalCode_SQL((g_objXClaim.FilingStateID)) <> "TX" Then
                                    Exit For
                                End If
                                If sAbbr <> UCase("TIBS") And InStr(1, UCase(colBenefitDefs.Item(i).BenefitDesc), UCase("PARTIAL"), CompareMethod.Text) = 0 Then
                                    Exit For
                                End If
                            End If
                        Next i
                    End If
            End Select

            If sAbbr = "" Then
                sWarning = ""
                sWarning = sWarning & "Benefit Abbreviation for the Temporary Total benefit was not found." & vbCrLf
                sWarning = sWarning & "The probable cause is the benefit is not set to 'Use In Calculator'" & vbCrLf
                sWarning = sWarning & "in JurisRules.  Less probable is that the benefit 'Temporary Total'" & vbCrLf
                sWarning = sWarning & "or 'Temporary Income' is mis-spelled in Jurisdictional Rules." & vbCrLf & vbCrLf
                sWarning = sWarning & "Please inform your Administrator"
            End If

            GetTempTotalAbbreviation = sAbbr

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function
    Public Function GetDependentChildCount(ByVal lClaimantEID As Integer, ByVal lEventID As Integer) As Integer
        Const subRoutineName As String = "GetDependentChildCount"
        Dim objReader As DbReader
        Dim iRdSet2 As Short 'jtodd22 03/23/2010
        Dim lChildCodeID As Integer
        Dim sSQL As String
        Dim sSQL2 As String

        Try

            GetDependentChildCount = 0

            lChildCodeID = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT"
            sSQL2 = sSQL2 & " CODES.CODE_ID FROM CODES, CODES_TEXT"
            sSQL2 = sSQL2 & " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL2 = sSQL2 & " AND CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'RELATION_CODE')"
            sSQL2 = sSQL2 & " AND CODES_TEXT.CODE_DESC IN ('child','Child','CHILD')"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL2)
            If (objReader.Read()) Then
                lChildCodeID = objReader.GetInt32(0)
            End If
            objReader.Close()
            'jtodd22 03/23/2010
            If lChildCodeID > 0 Then
                sSQL = ""
                sSQL = sSQL & "SELECT"
                sSQL = sSQL & " COUNT(PI_X_DEPENDENT.PI_DEP_ROW_ID)"
                sSQL = sSQL & " FROM PERSON_INVOLVED, PI_X_DEPENDENT"
                sSQL = sSQL & " WHERE PERSON_INVOLVED.PI_ROW_ID = PI_X_DEPENDENT.PI_ROW_ID"
                sSQL = sSQL & " AND PERSON_INVOLVED.EVENT_ID = " & lEventID
                sSQL = sSQL & " AND PERSON_INVOLVED.PI_EID = " & lClaimantEID
                sSQL = sSQL & " AND PI_X_DEPENDENT.RELATION_CODE = " & lChildCodeID

                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                If objReader.Read() Then
                    GetDependentChildCount = objReader.GetInt32(0)
                End If
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function GetDependentCount(ByVal lClaimantEID As Integer, ByVal lEventID As Integer) As Integer
        Const subRoutineName As String = "GetDependentCount"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetDependentCount = 0

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " COUNT(PI_X_DEPENDENT.PI_DEP_ROW_ID)"
            sSQL = sSQL & " FROM PERSON_INVOLVED, PI_X_DEPENDENT"
            sSQL = sSQL & " WHERE PERSON_INVOLVED.PI_ROW_ID = PI_X_DEPENDENT.PI_ROW_ID"
            sSQL = sSQL & " AND PERSON_INVOLVED.EVENT_ID = " & lEventID
            sSQL = sSQL & " AND PERSON_INVOLVED.PI_EID = " & lClaimantEID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetDependentCount = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function
    Public Function GetDependentSpouseCount(ByVal lClaimantEID As Integer, ByVal lEventID As Integer) As Integer
        Const subRoutineName As String = "GetDependentSpouseCount"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String

        Try

            GetDependentSpouseCount = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT"
            sSQL2 = sSQL2 & " CODES.CODE_ID FROM CODES, CODES_TEXT"
            sSQL2 = sSQL2 & " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL2 = sSQL2 & " AND CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'RELATION_CODE')"
            sSQL2 = sSQL2 & " AND CODES_TEXT.CODE_DESC IN ('husband','Husband','HUSBAND','wife','Wife','WIFE')"

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " COUNT(PI_X_DEPENDENT.PI_DEP_ROW_ID)"
            sSQL = sSQL & " FROM PERSON_INVOLVED, PI_X_DEPENDENT"
            sSQL = sSQL & " WHERE PERSON_INVOLVED.PI_ROW_ID = PI_X_DEPENDENT.PI_ROW_ID"
            sSQL = sSQL & " AND PERSON_INVOLVED.EVENT_ID = " & lEventID
            sSQL = sSQL & " AND PERSON_INVOLVED.PI_EID = " & lClaimantEID
            sSQL = sSQL & " AND PI_X_DEPENDENT.RELATION_CODE IN (" & sSQL2 & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetDependentSpouseCount = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function


    Public Function GetAutoChecksPendingAmount() As Double
        Const subRoutineName As String = "GetAutoChecksPendingAmount"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sTransTypeCodeIDs As String

        Try

            GetAutoChecksPendingAmount = 0

            sTransTypeCodeIDs = GetTransTypeCodeIDsIndemnity()

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " SUM(FUNDS_AUTO_SPLIT.AMOUNT)"
            sSQL = sSQL & " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT"
            sSQL = sSQL & " WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID"
            sSQL = sSQL & " AND FUNDS_AUTO.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND FUNDS_AUTO.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL = sSQL & " AND FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeIDs & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetAutoChecksPendingAmount = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function
    Public Function GetAutoCheckSystemLimit() As Integer
        Const subRoutineName As String = "GetAutoCheckSystemLimit"
        Dim objReader As DbReader
        Dim lMaxAutoChecksLimit As Integer
        Dim sSQL As String

        Try

            GetAutoCheckSystemLimit = 0

            lMaxAutoChecksLimit = 0
            sSQL = ""
            sSQL = sSQL & "SELECT MAX_AUTO_CHECKS FROM CHECK_OPTIONS"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                lMaxAutoChecksLimit = objReader.GetInt32("MAX_AUTO_CHECKS")
            End If
            GetAutoCheckSystemLimit = lMaxAutoChecksLimit

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function RoundBankers(ByRef dInput As Double, ByRef lPlaces As Integer) As Double
        'vb6 Round function does not use the standard rounding taught in grade school
        RoundBankers = System.Math.Round(dInput, lPlaces)


    End Function

    Public Function RoundStandard(ByRef dInput As Double, ByRef lPlaces As Integer) As Double
        Const sFunctionName As String = "RoundStandard"
        Dim sFormat As String
        Dim sOut As String

        Try

            'vb6 Format function does use the standard rounding taught in grade school
            sFormat = "0." & Left("0000000000000000", lPlaces)
            sOut = Format(dInput, sFormat)
            RoundStandard = Val(sOut)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_lErrLine = Erl()
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function ValidateEventDateFirstPaytDate(ByRef oUserInputs As CWCXUserInputs) As Integer

        ValidateEventDateFirstPaytDate = 0

        With oUserInputs
            If IsDate(.FirstPaymentDateCalendar) Then
                If .FirstPaymentDateDBFormat < g_objXClaim.DateOfEventDTG Then
                    'payments start before event
                    .Warning = ""
                    .Warning = .Warning & "This Application will not permit payment of " & vbCrLf
                    .Warning = .Warning & "Indemnity before the Date of Event." & vbCrLf & vbCrLf
                    .Warning = .Warning & "Date of First Date of Payment is " & .FirstPaymentDateCalendar & vbCrLf
                    .Warning = .Warning & "Date of Event is " & sDBDateFormat((g_objXClaim.DateOfEventDTG), "Short Date") & vbCrLf & vbCrLf
                    .Warning = .Warning & "Please adjust the First Date of Payment."
                End If
            End If
        End With


    End Function

    Public Function WriteClaimBenefitData(ByVal sBenefitType As String, ByVal dPaymentRate As Double, ByVal sOrderNumber As String, ByVal lOrderType As Integer) As Boolean
        '*************************************************************
        '*** find the record
        '*** if the record is missing (new benefit) then add a record
        '*************************************************************
        Const sFunctionName As String = "WriteClaimBenefitData"
        Dim dMMIDisabilityRate As Double
        Dim sMMIDateDTG As String

        Dim dPermDisRate As Double
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim iRdSetUpdate As Short
        Dim lReturn As Integer
        Dim lTemp As Integer
        Dim sSQL As String

        Dim sSQLSelect As String
        Dim s_Now As String
        Try
            WriteClaimBenefitData = False

            If g_objXClaim Is Nothing Then Exit Function

            lReturn = modFunctions.GetMMIImpairment(dMMIDisabilityRate, sMMIDateDTG)

            lTemp = -1
            s_Now = Format(Now, "yyyymmddhhnnss")

            sBenefitType = Trim(UCase(sBenefitType))

            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM " & g_sWCPClaimsTableName
            sSQL = sSQL & " WHERE 0 = -1"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If Not (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(g_ConnectionString)
                objReader.Close()
                objWriter.Tables.Add(g_sWCPClaimsTableName)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("BEGIN_PRNT_DATE", sMMIDateDTG)
                objWriter.Fields.Add("BENEFIT_RATE", dPaymentRate)
                objWriter.Fields.Add("BENEFIT_TYPE", UCase(sBenefitType))
                objWriter.Fields.Add("CLAIM_ID", g_objXClaim.ClaimID)
                objWriter.Fields.Add("DISABILITY_RATE", dMMIDisabilityRate)
                objWriter.Fields.Add("DTTM_RCD_ADDED", s_Now)
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", s_Now)
                objWriter.Fields.Add("ROW_ID", lGetNextUID("WCP_CLAIMS"))
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Execute()
            End If

            WriteClaimBenefitData = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
End Module

