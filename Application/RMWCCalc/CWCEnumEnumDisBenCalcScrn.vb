Option Strict Off
Option Explicit On
Public Class CWCEnumEnumDisBenCalcScrn
    Const sClassName As String = "CWCEnumEnumDisBenCalcScrn"

    Public cNoScreen As Short
    Public cTempTotal As Short
    Public cTempPartial As Short
    Public cPermPartial As Short
    Public cPermTotal As Short
    Public cVocRehabScn As Short
    Public cLifePension As Short
    Public cCommutationPP As Short
    Public cCommutationLife As Short
    Public cVocRehabsuppscn As Short
    Public cNoOccShortTermDis As Short

    Public Function SetConstantValues() As Integer

        cNoScreen = 0
        cTempTotal = 2
        cTempPartial = 3
        cPermPartial = 4
        cPermTotal = 6
        cVocRehabScn = 7
        cLifePension = 8
        cCommutationPP = 9
        cCommutationLife = 10
        cVocRehabsuppscn = 11
        cNoOccShortTermDis = 1001



    End Function
End Class

