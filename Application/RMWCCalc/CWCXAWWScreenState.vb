Option Strict Off
Option Explicit On
Public Class CWCXAWWScreenState
    '---------------------------------------------------------------------------------------
    ' Module    : CWCXAWWScreenState
    ' DateTime  : 11/10/2006 08:20
    ' Author    : jtodd22
    ' Purpose   : to provide an object version of the AWW Calculator screen inputs
    '---------------------------------------------------------------------------------------


    Private m_GrossAverageWage As Double
    Private m_TempTotalRate As Double
    Private m_PieceWorkEarningPer As Double
    Private m_PieceWorkPiecesPerPay As Double
    Private m_PieceWorkPayPeriodType As Integer
    Private m_PieceWorkEarningsTotal As Double

    Private m_SalaryEarnings As Double
    Private m_SalaryPayPeriodType As Integer
    Private m_SalaryHours As Double

    Private m_HourEarnings As Double
    Private m_HourHoursPerWeek As Double
    Private m_HourDaysPerWeek As Double
    Private m_HourHourlyRate As Double

    Private m_TaxStatus As Integer
    Private m_Dependents As Integer

    Private m_Warning As String

    Public Property Dependents() As Integer
        Get
            Dependents = m_Dependents
        End Get
        Set(ByVal Value As Integer)
            m_Dependents = Value
        End Set
    End Property

    Public Property GrossAverageWage() As Double
        Get
            GrossAverageWage = m_GrossAverageWage
        End Get
        Set(ByVal Value As Double)
            m_GrossAverageWage = Value
        End Set
    End Property

    Public Property PieceWorkEarningPer() As Double
        Get
            PieceWorkEarningPer = m_PieceWorkEarningPer
        End Get
        Set(ByVal Value As Double)
            m_PieceWorkEarningPer = Value
        End Set
    End Property


    Public Property PieceWorkPiecesPerPay() As Double
        Get
            PieceWorkPiecesPerPay = m_PieceWorkPiecesPerPay
        End Get
        Set(ByVal Value As Double)
            m_PieceWorkPiecesPerPay = Value
        End Set
    End Property

    Public Property PieceWorkPayPeriodType() As Integer
        Get
            PieceWorkPayPeriodType = m_PieceWorkPayPeriodType
        End Get
        Set(ByVal Value As Integer)
            m_PieceWorkPayPeriodType = Value
        End Set
    End Property
    Public Property SalaryEarnings() As Double
        Get
            SalaryEarnings = m_SalaryEarnings
        End Get
        Set(ByVal Value As Double)
            m_SalaryEarnings = Value
        End Set
    End Property

    Public Property SalaryPayPeriodType() As Integer
        Get
            SalaryPayPeriodType = m_SalaryPayPeriodType
        End Get
        Set(ByVal Value As Integer)
            m_SalaryPayPeriodType = Value
        End Set
    End Property

    Public Property SalaryHours() As Double
        Get
            SalaryHours = m_SalaryHours
        End Get
        Set(ByVal Value As Double)
            m_SalaryHours = Value
        End Set
    End Property

    Public Property HourEarnings() As Double
        Get
            HourEarnings = m_HourEarnings
        End Get
        Set(ByVal Value As Double)
            m_HourEarnings = Value
        End Set
    End Property

    Public Property HourHoursPerWeek() As Double
        Get
            HourHoursPerWeek = m_HourHoursPerWeek
        End Get
        Set(ByVal Value As Double)
            m_HourHoursPerWeek = Value
        End Set
    End Property

    Public Property HourDaysPerWeek() As Double
        Get
            HourDaysPerWeek = m_HourDaysPerWeek
        End Get
        Set(ByVal Value As Double)
            m_HourDaysPerWeek = Value
        End Set
    End Property

    Public Property HourHourlyRate() As Double
        Get
            HourHourlyRate = m_HourHourlyRate
        End Get
        Set(ByVal Value As Double)
            m_HourHourlyRate = Value
        End Set
    End Property

    Public Property PieceWorkEarningsTotal() As Double
        Get
            PieceWorkEarningsTotal = m_PieceWorkEarningsTotal
        End Get
        Set(ByVal Value As Double)
            m_PieceWorkEarningsTotal = Value
        End Set
    End Property
    Public Property TaxStatus() As Integer
        Get
            TaxStatus = m_TaxStatus
        End Get
        Set(ByVal Value As Integer)
            m_TaxStatus = Value
        End Set
    End Property

    Public Property TempTotalRate() As Double
        Get
            TempTotalRate = m_TempTotalRate
        End Get
        Set(ByVal Value As Double)
            m_TempTotalRate = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

