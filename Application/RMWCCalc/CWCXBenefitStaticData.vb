Option Strict Off
Option Explicit On
Public Class CWCXBenefitStaticData
    Const sClassName As String = "CWCXBenefitStaticData"
    Private m_TTDRate As Double
    Private m_AverageWage As Double
    Private m_EventDateDBFormat As String
    Private m_RetroactivePeriodDays As Integer
    Private m_WaitingPeriodDays As Integer
    Private m_RetroactivePerSatisfiedCode As Integer
    Private m_WaitingPeriodSatisfiedCode As Integer
    Private m_OriginalDisabilityDateDBFormat As String
    Private m_CurrentDisabilityDateDBFormat As String
    '               1111111111222222222233333333334
    '      1234567890123456789012345678901234567890
    Private m_MostRecentRetToWkDateDBFormat As String
    Private m_CurrentMMIDateDBFormat As String
    Private m_CurrentMMIDisability As Double

    Private m_NoCodeID As Integer
    Private m_YesCodeID As Integer

    Private m_Note As String
    Private m_Warning As String

    Private m_WorkLossWageLossExistsCode As Integer

    Public objBenefitCalcRule As Object
    Public objPIXRestricts As CWCPIXRestricts
    Public objPIXWorkLosses As CWCPIXWorkLosses

    Public Function LoadData() As Integer
        Const sFunctionName As String = "lLoadData"
        Dim i2 As Short
        Dim lErrNumber As Integer
        Dim lIndex As Integer
        Dim lReturn As Integer
        Dim lWorkLossDays As Integer
        Dim lTotalWorkLossDays As Integer

        Dim sDateRTW As String
        Dim sDisabilityDateDTG As String

        Try

            'jtodd22 10/10/2006 we will use the code ids in this function
            m_NoCodeID = modFunctions.GetNoCodeID(lErrNumber)
            m_YesCodeID = modFunctions.GetYesCodeID(lErrNumber)

            m_RetroactivePerSatisfiedCode = m_NoCodeID
            m_WaitingPeriodSatisfiedCode = m_NoCodeID

            With g_objXClaim
                m_TTDRate = .ClaimantTTDRate
                m_AverageWage = .ClaimantOriginalAWW
                m_EventDateDBFormat = .DateOfEventDTG
            End With

            If m_AverageWage < 0.01 Then
                m_Warning = ""
                m_Warning = "The AWW does not exist for this Claim." & vbCrLf & "Please create an AWW on the 'AWW Rates Calculator'."
                Exit Function
            End If

            objBenefitCalcRule = New RMJuRuLib.CJRCalcBenefit(g_ConnectionString)

            lReturn = objBenefitCalcRule.LoadDataByJurisRowIDEffectiveDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)

            Select Case lReturn
                Case -1 'Normal, expected
                    If objBenefitCalcRule.TableRowID = 0 Then
                        m_Warning = ""
                        m_Warning = "The Jurisdictional Benefit Calculator Setup Rule was not found." & vbCrLf & "Claim #;  " & g_objXClaim.ClaimNumber & vbCrLf & "Jurisdiction;  " & modFunctions.GetStateName_SQL((g_objXClaim.FilingStateID)) & vbCrLf & "Date of Event;  " & g_objXClaim.DateOfEventCalar
                        Exit Function
                    End If
                Case 0
                    m_Warning = ""
                    m_Warning = "The Jurisdictional Benefit Calculator Setup Rule was not found." & vbCrLf & "Claim #;  " & g_objXClaim.ClaimNumber & vbCrLf & "Jurisdiction;  " & modFunctions.GetStateName_SQL((g_objXClaim.FilingStateID)) & vbCrLf & "Date of Injury;  " & g_objXClaim.DateOfEventCalar
                    Exit Function
                Case Else
                    'error
                    'should have triggered the bubble up
            End Select

            m_WaitingPeriodDays = objBenefitCalcRule.WaitDays
            m_RetroactivePeriodDays = objBenefitCalcRule.RetroActiveDays

            lReturn = objPIXRestricts.LoadData(0)
            lReturn = objPIXWorkLosses.LoadData(0)

            If objPIXRestricts.Count + objPIXWorkLosses.Count > 0 Then
                m_WorkLossWageLossExistsCode = m_YesCodeID
            Else
                m_WorkLossWageLossExistsCode = m_NoCodeID
            End If

            If objPIXWorkLosses.Count > 0 Then
                lIndex = 1
                m_OriginalDisabilityDateDBFormat = objPIXWorkLosses.Item(lIndex).DisabilityDate
                lIndex = objPIXWorkLosses.Count
                If objPIXWorkLosses.Item(lIndex).PiWlRowId > 0 Then
                    With objPIXWorkLosses
                        lIndex = objPIXWorkLosses.Count
                        m_CurrentDisabilityDateDBFormat = .Item(lIndex).DisabilityDate
                        m_MostRecentRetToWkDateDBFormat = .Item(lIndex).DateReturned
                        Select Case objBenefitCalcRule.CountFromCode
                            Case 2 'count wait days from event date

                            Case 4
                                For i2 = 1 To objPIXWorkLosses.Count
                                    sDateRTW = .Item(i2).DateReturned
                                    If Trim(sDateRTW & "") = "" Then sDateRTW = Format(Now, "YYYYMMDD")
                                    sDisabilityDateDTG = .Item(i2).DisabilityDate
                                    If Trim(sDisabilityDateDTG & "") = "" Then sDisabilityDateDTG = .Item(i2).DateLastWorked
                                    lWorkLossDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sDBDateFormat(sDisabilityDateDTG, "Short Date")), CDate(sDBDateFormat(sDateRTW, "Short Date")))
                                    lTotalWorkLossDays = lTotalWorkLossDays + System.Math.Abs(lWorkLossDays)
                                Next

                            Case 5
                                lIndex = objPIXWorkLosses.Count
                                sDateRTW = .Item(lIndex).DateReturned
                                If Trim(sDateRTW & "") = "" Then sDateRTW = Format(Now, "YYYYMMDD")
                                sDisabilityDateDTG = .Item(lIndex).DisabilityDate
                                If Trim(sDisabilityDateDTG & "") = "" Then sDisabilityDateDTG = .Item(lIndex).DateLastWorked
                                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                                lWorkLossDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sDBDateFormat(sDisabilityDateDTG, "Short Date")), CDate(sDBDateFormat(sDateRTW, "Short Date")))
                                lTotalWorkLossDays = lTotalWorkLossDays + System.Math.Abs(lWorkLossDays)
                        End Select
                        If lTotalWorkLossDays > (m_WaitingPeriodDays - 1) Then
                            m_WaitingPeriodSatisfiedCode = m_YesCodeID
                        End If
                        If lTotalWorkLossDays > (m_RetroactivePeriodDays - 1) Then
                            m_RetroactivePerSatisfiedCode = m_YesCodeID
                        End If

                    End With
                End If
            End If

            lReturn = modFunctions.GetMMIImpairment(m_CurrentMMIDisability, m_CurrentMMIDateDBFormat)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function


    Public Property Note() As String
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property TTDRate() As Double
        Get
            TTDRate = m_TTDRate
        End Get
        Set(ByVal Value As Double)
            m_TTDRate = Value
        End Set
    End Property


    Public Property AverageWage() As Double
        Get
            AverageWage = m_AverageWage
        End Get
        Set(ByVal Value As Double)
            m_AverageWage = Value
        End Set
    End Property


    Public Property EventDateDBFormat() As String
        Get
            EventDateDBFormat = m_EventDateDBFormat
        End Get
        Set(ByVal Value As String)
            m_EventDateDBFormat = Value
        End Set
    End Property


    Public Property WaitingPeriodDays() As Integer
        Get
            WaitingPeriodDays = m_WaitingPeriodDays
        End Get
        Set(ByVal Value As Integer)
            m_WaitingPeriodDays = Value
        End Set
    End Property


    Public Property WaitingPeriodSatisfiedCode() As Integer
        Get
            WaitingPeriodSatisfiedCode = m_WaitingPeriodSatisfiedCode
        End Get
        Set(ByVal Value As Integer)
            m_WaitingPeriodSatisfiedCode = Value
        End Set
    End Property


    Public Property RetroactivePeriodDays() As Integer
        Get
            RetroactivePeriodDays = m_RetroactivePeriodDays
        End Get
        Set(ByVal Value As Integer)
            m_RetroactivePeriodDays = Value
        End Set
    End Property


    Public Property RetroactivePerSatisfiedCode() As Integer
        Get
            RetroactivePerSatisfiedCode = m_RetroactivePerSatisfiedCode
        End Get
        Set(ByVal Value As Integer)
            m_RetroactivePerSatisfiedCode = Value
        End Set
    End Property


    Public Property CurrentDisabilityDateDBFormat() As String
        Get
            CurrentDisabilityDateDBFormat = m_CurrentDisabilityDateDBFormat
        End Get
        Set(ByVal Value As String)
            m_CurrentDisabilityDateDBFormat = Value
        End Set
    End Property


    Public Property MostRecentRetToWkDateDBFormat() As String
        Get
            MostRecentRetToWkDateDBFormat = m_MostRecentRetToWkDateDBFormat
        End Get
        Set(ByVal Value As String)
            m_MostRecentRetToWkDateDBFormat = Value
        End Set
    End Property


    Public Property CurrentMMIDateDBFormat() As String
        Get
            CurrentMMIDateDBFormat = m_CurrentMMIDateDBFormat
        End Get
        Set(ByVal Value As String)
            m_CurrentMMIDateDBFormat = Value
        End Set
    End Property


    Public Property CurrentMMIDisability() As Double
        Get
            CurrentMMIDisability = m_CurrentMMIDisability
        End Get
        Set(ByVal Value As Double)
            m_CurrentMMIDisability = Value
        End Set
    End Property


    Public Property NoCodeID() As Integer
        Get
            NoCodeID = m_NoCodeID
        End Get
        Set(ByVal Value As Integer)
            m_NoCodeID = Value
        End Set
    End Property


    Public Property YesCodeID() As Integer
        Get
            YesCodeID = m_YesCodeID
        End Get
        Set(ByVal Value As Integer)
            m_YesCodeID = Value
        End Set
    End Property


    Public Property WorkLossWageLossExistsCode() As Integer
        Get

            WorkLossWageLossExistsCode = m_WorkLossWageLossExistsCode

        End Get
        Set(ByVal Value As Integer)

            m_WorkLossWageLossExistsCode = Value

        End Set
    End Property


    Public Property OriginalDisabilityDateDBFormat() As String
        Get

            OriginalDisabilityDateDBFormat = m_OriginalDisabilityDateDBFormat

        End Get
        Set(ByVal Value As String)

            m_OriginalDisabilityDateDBFormat = Value

        End Set
    End Property

    Public Sub New()
        MyBase.New()
        objPIXRestricts = New CWCPIXRestricts
        objPIXWorkLosses = New CWCPIXWorkLosses
    End Sub

    Protected Overrides Sub Finalize()
        objBenefitCalcRule = Nothing
        objPIXRestricts = Nothing
        objPIXWorkLosses = Nothing
        MyBase.Finalize()
    End Sub

End Class

