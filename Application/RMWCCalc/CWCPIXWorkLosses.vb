Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPIXWorkLosses
    Implements System.Collections.IEnumerable

    Const sClassName As String = "CWCPIXWorkLosses"
    Const m_TableName As String = "PI_X_WORK_LOSS"

    Private m_DateLastWorked As String
    Private m_DateReturned As String
    Private m_DisabilityDate As String
    Private m_Duration As Integer
    Private m_NonConsecutivePeriod As Integer
    Private m_PhysicalRestrictions As Integer
    Private m_PiRowId As Integer
    Private m_PiWlRowId As Integer
    Private m_RTWQualifier As Integer
    Private m_RTWSameEmployer As Integer
    Private m_RTWType As Integer
    Private m_StateDuration As Integer
    Private m_WorkIntensity As Integer

    'local variable to hold collection
    Private mCol As Collection
    Private Function ClearObject() As Integer



    End Function
    Public Function LoadData(ByRef lPIRowID As Integer) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadData = 0

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " PI_X_WORK_LOSS.DATE_LAST_WORKED"
            sSQL = sSQL & ",PI_X_WORK_LOSS.DATE_RETURNED"
            sSQL = sSQL & ",PI_X_WORK_LOSS.DISABILITY_DATE"
            sSQL = sSQL & ",PI_X_WORK_LOSS.DURATION"
            sSQL = sSQL & ",PI_X_WORK_LOSS.NONCONSEC_PERIOD"
            sSQL = sSQL & ",PI_X_WORK_LOSS.PHYSICAL_RESTRICT"
            sSQL = sSQL & ",PI_X_WORK_LOSS.PI_ROW_ID"
            sSQL = sSQL & ",PI_X_WORK_LOSS.PI_WL_ROW_ID"
            sSQL = sSQL & ",PI_X_WORK_LOSS.RTW_QUALIFIER"
            sSQL = sSQL & ",PI_X_WORK_LOSS.RTW_SAME_EMPLOYER"
            sSQL = sSQL & ",PI_X_WORK_LOSS.RTW_TYPE"
            sSQL = sSQL & ",PI_X_WORK_LOSS.STATE_DURATION"
            sSQL = sSQL & ",PI_X_WORK_LOSS.WORK_INTENSITY"

            sSQL2 = ""
            sSQL2 = sSQL2 & " FROM " & m_TableName
            sSQL2 = sSQL2 & " WHERE PI_ROW_ID = " & lPIRowID
            sSQL2 = sSQL2 & " ORDER BY PI_WL_ROW_ID"

            If lPIRowID = 0 Then
                sSQL2 = ""
                sSQL2 = sSQL2 & " FROM PI_X_WORK_LOSS, PERSON_INVOLVED, CLAIM, CLAIMANT"
                sSQL2 = sSQL2 & " WHERE CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID"
                sSQL2 = sSQL2 & " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
                sSQL2 = sSQL2 & " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID"
                sSQL2 = sSQL2 & " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_WORK_LOSS.PI_ROW_ID"
                sSQL2 = sSQL2 & " AND CLAIM.CLAIM_ID = " & g_objXClaim.ClaimID
                sSQL2 = sSQL2 & " ORDER BY PI_WL_ROW_ID"
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL & sSQL2)
            While objReader.Read()
                m_DateLastWorked = objReader.GetString("DATE_LAST_WORKED")
                m_DateReturned = objReader.GetString("DATE_RETURNED")
                'jtodd22 03/06/2008 this should a string --m_DisabilityDate = objReader.GetInt32("DISABILITY_DATE")
                m_DisabilityDate = objReader.GetString("DISABILITY_DATE")
                m_Duration = objReader.GetInt32("DURATION")
                m_NonConsecutivePeriod = objReader.GetInt32("NONCONSEC_PERIOD")
                m_PhysicalRestrictions = objReader.GetInt32("PHYSICAL_RESTRICT")
                m_PiRowId = objReader.GetInt32("PI_ROW_ID")
                m_PiWlRowId = objReader.GetInt32("PI_WL_ROW_ID")
                m_RTWQualifier = objReader.GetInt32("RTW_QUALIFIER")
                m_RTWSameEmployer = objReader.GetInt32("RTW_SAME_EMPLOYER")
                m_RTWType = objReader.GetInt32("RTW_TYPE")
                m_StateDuration = objReader.GetInt32("STATE_DURATION")
                m_WorkIntensity = objReader.GetInt32("WORK_INTENSITY")
                Add(m_DateLastWorked, m_DateReturned, m_DisabilityDate, m_Duration, m_NonConsecutivePeriod, m_PhysicalRestrictions, m_PiRowId, m_PiWlRowId, m_RTWQualifier, m_RTWSameEmployer, m_RTWType, m_StateDuration, m_WorkIntensity)
            End While

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function Add(ByRef DateLastWorked As String, ByRef DateReturned As String, ByRef DisabilityDate As String, ByRef Duration As Integer, ByRef NonConsecPeriod As Integer, ByRef PhysicalRestrictions As Object, ByRef PiRowId As Integer, ByRef PiWlRowId As Integer, ByRef RTWQualifier As Integer, ByRef RTWSameEmployer As Integer, ByRef RTWType As Integer, ByRef StateDuration As Integer, ByRef WorkIntensity As Integer) As CWCPiXWorkLoss
        Const sFunctionName As String = "Add"
        Try
            'create a new object
            Dim objNewMember As CWCPiXWorkLoss
            objNewMember = New CWCPiXWorkLoss

            'set the properties passed into the method
            objNewMember.DateLastWorked = DateLastWorked
            objNewMember.DateReturned = DateReturned
            objNewMember.DisabilityDate = DisabilityDate
            objNewMember.Duration = Duration
            objNewMember.NonConsecPeriod = NonConsecPeriod
            'UPGRADE_WARNING: Couldn't resolve default property of object PhysicalRestrictions. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objNewMember.PhysicalRestriction = PhysicalRestrictions
            objNewMember.PiRowId = PiRowId
            objNewMember.PiWlRowId = PiWlRowId
            objNewMember.RTWQualifier = RTWQualifier
            objNewMember.RTWSameEmployer = RTWSameEmployer
            objNewMember.RTWType = RTWType
            objNewMember.StateDuration = StateDuration
            objNewMember.WorkIntensity = WorkIntensity

            mCol.Add(objNewMember)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            Dim ErrNum As Integer
            Dim ErrDesc, ErrSrc As String
            ErrNum = Err.Number
            ErrDesc = Err.Description
            ErrSrc = Err.Source
            Err.Raise(ErrNum, sClassName & "." & sFunctionName & "." & ErrSrc, ErrDesc)
            Exit Function


        Finally
        End Try

    End Function
    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CWCPiXWorkLoss
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

