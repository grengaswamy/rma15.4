Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCXTransSplitReCalcs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CWCXTransSplitReCalcs"

    'local variable to hold collection
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String

        GetSQLFieldList = ""

        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " FUNDS.CTL_NUMBER"
        sSQL = sSQL & ",FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE"
        sSQL = sSQL & ",FUNDS_TRANS_SPLIT.AMOUNT"
        sSQL = sSQL & ",FUNDS_TRANS_SPLIT.FROM_DATE"
        sSQL = sSQL & ",FUNDS_TRANS_SPLIT.TO_DATE"
        sSQL = sSQL & ",FUNDS_TRANS_SPLIT.TP_EARNINGS_AMT"
        sSQL = sSQL & ",FUNDS_TRANS_SPLIT.BENEFIT_ACR"

        GetSQLFieldList = sSQL



    End Function

    Private Function GetWhereClauseBasic() As String
        Dim sSQL As String
        Dim sTransTypeCodeIDs As String

        GetWhereClauseBasic = ""

        sTransTypeCodeIDs = GetTransTypeCodeIDsIndemnity()

        sSQL = ""
        sSQL = sSQL & " FROM FUNDS, FUNDS_TRANS_SPLIT"
        sSQL = sSQL & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
        sSQL = sSQL & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID
        sSQL = sSQL & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
        sSQL = sSQL & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeIDs & ")"
        sSQL = sSQL & " AND FUNDS.VOID_FLAG = 0"

        GetWhereClauseBasic = sSQL



    End Function

    Public Function LoadData(ByVal sCompareOperator As String) As Integer
        Const sCheckStatus As String = "printed"
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRecord As CWCXTransSplitReCalc
        Dim sCheckStatusA As String
        Dim sCheckStatusB As String
        Dim sCheckStatusC As String
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadData = 0

            sCheckStatusA = LCase(sCheckStatus)
            sCheckStatusB = UCase(Left(sCheckStatus, 1)) & LCase(Mid(sCheckStatus, 2))
            sCheckStatusC = UCase(sCheckStatus)

            sCompareOperator = Trim(sCompareOperator)

            sSQL2 = ""
            sSQL2 = sSQL2 & ModSQLStatements.SelectCheckStatusCodeIds
            sSQL2 = sSQL2 & " AND (CODES_TEXT.CODE_DESC " & sCompareOperator & " '" & sCheckStatusA & "'"
            sSQL2 = sSQL2 & " OR CODES_TEXT.CODE_DESC " & sCompareOperator & " '" & sCheckStatusB & "'"
            sSQL2 = sSQL2 & " OR CODES_TEXT.CODE_DESC " & sCompareOperator & " '" & sCheckStatusC & "'"
            sSQL2 = sSQL2 & ")"

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & GetWhereClauseBasic()
            If sCompareOperator > "" Then
                sSQL = sSQL & " AND FUNDS.STATUS_CODE IN (" & sSQL2 & ")"
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CWCXTransSplitReCalc
                With objRecord
                    .AmountOriginal = objReader.GetInt32("AMOUNT")
                    .BenefitACR = objReader.GetInt32("BENEFIT_ACR")
                    .ControlNumber = objReader.GetString("CTL_NUMBER")
                    .FromDate = objReader.GetString("FROM_DATE")
                    .ToDate = objReader.GetString("TO_DATE")
                    .TPEarningsAmount = objReader.GetInt32("TP_EARNINGS_AMT")
                    .TransTypeCode = objReader.GetInt32("TRANS_TYPE_CODE")
                End With

                mCol.Add(objRecord)
            End While
            'jlt 07/06/2004 be sure to drop the Recordset

            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function LoadDataNullDates(ByVal sCompareOperator As String) As Integer
        Const sCheckStatus As String = "printed"
        Const sFunctionName As String = "LoadDataNullDates"
        Dim objReader As DbReader
        Dim objRecord As CWCXTransSplitReCalc
        Dim sCheckStatusA As String
        Dim sCheckStatusB As String
        Dim sCheckStatusC As String
        Dim sSQL As String
        Dim sSQL2 As String

        Try

            LoadDataNullDates = 0

            sCheckStatusA = LCase(sCheckStatus)
            sCheckStatusB = UCase(Left(sCheckStatus, 1)) & LCase(Mid(sCheckStatus, 2))
            sCheckStatusC = UCase(sCheckStatus)

            sCompareOperator = Trim(sCompareOperator)

            sSQL2 = ""
            sSQL2 = sSQL2 & ModSQLStatements.SelectCheckStatusCodeIds
            sSQL2 = sSQL2 & " AND (CODES_TEXT.CODE_DESC " & sCompareOperator & " '" & sCheckStatusA & "'"
            sSQL2 = sSQL2 & " OR CODES_TEXT.CODE_DESC " & sCompareOperator & " '" & sCheckStatusB & "'"
            sSQL2 = sSQL2 & " OR CODES_TEXT.CODE_DESC " & sCompareOperator & " '" & sCheckStatusC & "'"
            sSQL2 = sSQL2 & ")"

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & GetWhereClauseBasic()
            If sCompareOperator > "" Then
                sSQL = sSQL & " AND FUNDS.STATUS_CODE IN (" & sSQL2 & ")"
            End If
            sSQL = sSQL & " AND "
            sSQL = sSQL & " (FROM_DATE = ''"
            sSQL = sSQL & " OR FROM_DATE IS NULL"
            sSQL = sSQL & " OR TO_DATE = ''"
            sSQL = sSQL & " OR TO_DATE IS NULL)"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CWCXTransSplitReCalc
                With objRecord
                    .AmountOriginal = objReader.GetInt32("AMOUNT")
                    .BenefitACR = objReader.GetInt32("BENEFIT_ACR")
                    .ControlNumber = objReader.GetString("CTL_NUMBER")
                    .FromDate = objReader.GetString("FROM_DATE")
                    .ToDate = objReader.GetString("TO_DATE")
                    .TPEarningsAmount = objReader.GetInt32("TP_EARNINGS_AMT")
                    .TransTypeCode = objReader.GetInt32("TRANS_TYPE_CODE")
                End With

                mCol.Add(objRecord)
            End While
            'jlt 07/06/2004 be sure to drop the Recordset

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataNullDates = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function Add(ByRef AmountDifference As Double, ByRef AmountNew As Double, ByRef AmountOriginal As Double, ByRef BenefitACR As Integer, ByRef Days As Integer, ByRef FromDate As String, ByRef Months As Integer, ByRef ReserveTypeCode As Integer, ByRef SumAmount As Double, ByRef SplitRowId As Integer, ByRef ToDate As String, ByRef TPEarningsAmount As Double, ByRef TransId As Integer, ByRef TransTypeCode As Integer, ByRef sKey As String) As CWCXTransSplitReCalc
        Try
            'create a new object
            Dim objNewMember As CWCXTransSplitReCalc
            objNewMember = New CWCXTransSplitReCalc
            'set the properties passed into the method
            With objNewMember
                .AmountDifference = AmountDifference
                .AmountNew = AmountNew
                .AmountOriginal = AmountOriginal
                .BenefitACR = BenefitACR
                .Days = Days
                .FromDate = FromDate
                .Months = Months
                .ReserveTypeCode = ReserveTypeCode
                .SumAmount = SumAmount
                .SplitRowId = SplitRowId
                .ToDate = ToDate
                .TPEarningsAmount = TPEarningsAmount
                .TransId = TransId
                .TransTypeCode = TransTypeCode
            End With
            If Trim(sKey & "") > "" Then
                mCol.Add(objNewMember, sKey)
            Else
                mCol.Add(objNewMember)
            End If

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CWCXTransSplitReCalc
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

