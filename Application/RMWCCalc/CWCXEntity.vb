Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCXEntity
    Const sClassName As String = "CWCXEntity"
    Private m_Addr1 As String
    Private m_Addr2 As String
    Private m_Addr3 As String
    Private m_Addr4 As String
    Private m_City As String
    Private m_CountryCode As Integer
    Private m_EntityEID As Integer
    Private m_FirstName As String
    Private m_LastName As String
    Private m_MiddleName As String
    Private m_StateId As Integer
    Private m_ZipCode As String
    Private Function ClearObject() As Integer
        m_Addr1 = ""
        m_Addr2 = ""
        m_Addr3 = ""
        m_Addr4 = ""
        m_City = ""
        m_CountryCode = 0
        m_EntityEID = 0
        m_FirstName = ""
        m_LastName = ""
        m_MiddleName = ""
        m_StateId = 0
        m_ZipCode = ""


    End Function
    Public Function LoadData(ByRef lPayeeEID As Integer) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = g_objXErrorMask.cSuccess
            ClearObject()

            sSQL = ""
            sSQL = sSQL & " SELECT"
            sSQL = sSQL & " ENTITY_EID"
            sSQL = sSQL & ",ADDR1,ADDR2,ADDR3,ADDR4,CITY,STATE_ID,ZIP_CODE"
            sSQL = sSQL & ",FIRST_NAME,MIDDLE_NAME,LAST_NAME"
            sSQL = sSQL & " FROM ENTITY"
            sSQL = sSQL & " WHERE ENTITY.ENTITY_ID = " & lPayeeEID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_EntityEID = objReader.GetInt32("ENTITY_EID")
                m_Addr1 = objReader.GetString("ADDR1")
                m_Addr2 = objReader.GetString("ADDR2")
                m_Addr3 = objReader.GetString("ADDR3")
                m_Addr4 = objReader.GetString("ADDR4")
                m_City = objReader.GetString("CITY")
                m_StateId = objReader.GetInt32("STATE_ID")
                m_ZipCode = objReader.GetString("ZIP_CODE")
                m_CountryCode = objReader.GetInt32("COUNTRY_CODE")
                m_FirstName = objReader.GetString("FIRST_NAME")
                m_LastName = objReader.GetString("LAST_NAME")
                m_MiddleName = objReader.GetString("MIDDLE_NAME")
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function
    Public Property Addr1() As String
        Get
            Addr1 = m_Addr1
        End Get
        Set(ByVal Value As String)
            m_Addr1 = Value
        End Set
    End Property
    Public Property Addr2() As String
        Get
            Addr2 = m_Addr2
        End Get
        Set(ByVal Value As String)
            m_Addr2 = Value
        End Set
    End Property
    Public Property Addr3() As String
        Get
            Addr3 = m_Addr3
        End Get
        Set(ByVal Value As String)
            m_Addr3 = Value
        End Set
    End Property
    Public Property Addr4() As String
        Get
            Addr4 = m_Addr4
        End Get
        Set(ByVal Value As String)
            m_Addr4 = Value
        End Set
    End Property
    Public Property City() As String
        Get
            City = m_City
        End Get
        Set(ByVal Value As String)
            m_City = Value
        End Set
    End Property
    Public Property CountryCode() As String
        Get
            CountryCode = CStr(m_CountryCode)
        End Get
        Set(ByVal Value As String)
            m_CountryCode = CInt(Value)
        End Set
    End Property

    Public Property EntityEID() As Integer
        Get
            EntityEID = m_EntityEID
        End Get
        Set(ByVal Value As Integer)
            m_EntityEID = Value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            FirstName = m_FirstName
        End Get
        Set(ByVal Value As String)
            m_FirstName = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            LastName = m_LastName
        End Get
        Set(ByVal Value As String)
            m_LastName = Value
        End Set
    End Property
    Public Property MiddleName() As String
        Get
            MiddleName = m_MiddleName
        End Get
        Set(ByVal Value As String)
            m_MiddleName = Value
        End Set
    End Property

    Public Property StateId() As Integer
        Get
            StateId = m_StateId
        End Get
        Set(ByVal Value As Integer)
            m_StateId = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            ZipCode = m_ZipCode
        End Get
        Set(ByVal Value As String)
            m_ZipCode = Value
        End Set
    End Property
End Class

