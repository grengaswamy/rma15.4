Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.


' TODO: Review the values of the assembly attributes


<Assembly: AssemblyTitle("DLL for CSC's RiskMaster Workers Comp Calculator using Jurisdictional Rules")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Computer Sciences Corporation")>
<Assembly: AssemblyProduct("RISKMASTER WC Calculator DLL")>
<Assembly: AssemblyCopyright("Computer Sciences Corporation � 2002, 2004")>
<Assembly: AssemblyTrademark("RISKMASTER")>
<Assembly: AssemblyCulture("")>

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Build Number
'	Revision

' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

<Assembly: AssemblyVersion("15.4.0.0")> 
<Assembly: AssemblyFileVersion("15.4.0.0")> 
<Assembly: AssemblyInformationalVersion("15.4.0.0")> 



