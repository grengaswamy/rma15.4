﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;

namespace Riskmaster.Application.PolicySystemInterface
{
    public class GetPolicyLimit
    {
        private string m_sConnectString = string.Empty;
        private string m_sUserName = string.Empty;
        private string m_sDSNName = string.Empty;
        private string m_sPassword = string.Empty;
        private UserLogin m_oUserLogin;
        private int m_iClientId = 0;

        public GetPolicyLimit(UserLogin objUserLogin, int p_iClientId = 0)
        {
            m_sConnectString = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_sUserName = objUserLogin.LoginName;
            m_sDSNName = objUserLogin.objRiskmasterDatabase.DataSourceName;
            m_sPassword = objUserLogin.Password;
            m_iClientId = p_iClientId;
            m_oUserLogin = objUserLogin;
        }
        private string GetCoverageIdsFromPolicy(string sExternalPolicyKey,int iPolicyId)
        {
            string sCovIds = string.Empty;
            string sSQL = "SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY,POLICY_X_UNIT,POLICY_X_CVG_TYPE WHERE POLICY.EXTERNAL_POLICY_KEY='" + sExternalPolicyKey + "' AND POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID";
            sSQL = sSQL + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID";

            if (iPolicyId > 0)
            {
                sSQL = sSQL + " AND POLICY.POLICY_ID=" + iPolicyId;
            }
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objRdr.Read())
                    {
                        if (string.IsNullOrEmpty(sCovIds))
                        {
                            sCovIds = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                        else
                        {
                            sCovIds = sCovIds + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                    }
                }

                return sCovIds;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        private double GetTolalReserve(string sCovIds, string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            try
            {
                LocalCache objCache = new LocalCache(m_sConnectString, m_iClientId);

                if (!string.IsNullOrEmpty(sRsvTypeCode))
                {
                    sSQL = "SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND  COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID AND RESERVE_TYPE_CODE NOT IN (" + objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_TYPE"), objCache.GetCodeId("R", "MASTER_RESERVE")) + ")";



                    return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnectString, sSQL), m_iClientId);
                }
                return 0;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string GetCoverageIdsFromCoverage(string sPolicykey, string sCoverageKey,int iPolicyID)
        {
            string sCovIds = string.Empty;
            string  sSQL = "SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY,POLICY_X_UNIT, POLICY_X_CVG_TYPE WHERE POLICY.EXTERNAL_POLICY_KEY='" + sPolicykey + "' AND  POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID= POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID AND POLICY_X_CVG_TYPE.COVERAGE_KEY='" + sCoverageKey + "'";
            try
            {
                if (iPolicyID > 0)
                {
                    sSQL = sSQL + " AND POLICY.POLICY_ID=" + iPolicyID;
                }

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objRdr.Read())
                    {
                        if (string.IsNullOrEmpty(sCovIds))
                        {
                            sCovIds = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                        else
                        {
                            sCovIds = sCovIds + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                    }
                }

                return sCovIds;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private double GetCollectiononDeductibleReserve(string sCovIds, bool UseDedLimitTracking, int iLOB)
        {
            string sSQL = string.Empty;
            string sDedResvTypeCode = string.Empty;
            try
            {
                if (UseDedLimitTracking)
                {


                    sSQL = "SELECT DISTINCT DED_REC_RESERVE_TYPE FROM SYS_PARMS_LOB  WHERE LINE_OF_BUS_CODE=" + iLOB;
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            if (string.IsNullOrEmpty(sDedResvTypeCode))
                            {
                                sDedResvTypeCode = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                            }
                            else
                            {
                                sDedResvTypeCode = sDedResvTypeCode + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                            }

                        }
                    }

                    sSQL = "SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID AND RESERVE_TYPE_CODE IN (" + sDedResvTypeCode + ")";



                    return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnectString, sSQL), m_iClientId);
                }

                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private double GetPaidAmountonCoverage(string sCovIds,string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            double dblPaidAmount = 0;
            try
            {
                if (!string.IsNullOrEmpty(sRsvTypeCode))
                {
                    sSQL = "SELECT SUM(PAID_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND    COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";



                    dblPaidAmount = Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnectString, sSQL), m_iClientId);
                }

            }
            catch (Exception e)
            {
                throw e;
            }

            return dblPaidAmount;

        }
        private double GetCollectionAmountonCoverage(string sCovIds, string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            try
            {
                if(!string.IsNullOrEmpty(sRsvTypeCode))
                {
                sSQL = "SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND   COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";
                return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnectString, sSQL), m_iClientId);
                }
                return 0;
            }
            catch (Exception e)
            {
                throw e;
            }
            

        }
        

        private string GetUnitDesc(int iUnitId,string sUnitType)
        {
            string sUnitName = string.Empty;
            try
            {
                switch (sUnitType)
                {
                    case "P":
                        sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectString, "SELECT PROPERTY_UNIT.PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID=" + iUnitId));

                        break;
                    case "V":
                        if (m_oUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_SQLSRVR)
                        {

                            sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_oUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT ISNULL(VEHICLE.VEH_DESC, VEHICLE.VIN) FROM VEHICLE WHERE UNIT_ID=" + iUnitId));
                        }
                        else if (m_oUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_ORACLE)
                        {

                            sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_oUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT nvl(VEHICLE.VEH_DESC, VEHICLE.VIN) FROM VEHICLE WHERE UNIT_ID=" + iUnitId));
                        }
                        break;
                    case "SU":

                        if (m_oUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_SQLSRVR)
                        {

                            sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_oUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'')))))  FROM ENTITY WHERE ENTITY_ID=" + iUnitId));
                        }
                        else if (m_oUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_ORACLE)
                        {

                            sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_oUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT  nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,''))))) FROM ENTITY WHERE ENTITY_ID=" + iUnitId));
                        }

                        break;
                    case "S":


                        sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_oUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT SITE_UNIT.NAME FROM SITE_UNIT WHERE SITE_ID=" + iUnitId));

                        break;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return sUnitName;
        }
        private string GetAdjusterName(int iAdjEid, int iClaimId)
        {
            string sAdjuster = string.Empty;
            try
            {
                if (iAdjEid > 0)
                {
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT LAST_NAME,FIRST_NAME,MIDDLE_NAME FROM ENTITY WHERE  ENTITY_ID=" + iAdjEid))
                    {
                        if (objRdr.Read())
                        {
                            sAdjuster = Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("MIDDLE_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME"));
                        }
                    }
                }
                if (string.IsNullOrEmpty(sAdjuster))
                {
                    sAdjuster = GetClaimAdjuster(iClaimId);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return sAdjuster;
        }
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }
        private bool CheckPerRsvSettings(int p_iPerRsvFlag, int p_iReserveTypeCode, int p_iLob)
        {
            List<string> lstResTypeCode = null;
            string sSQL = string.Empty;
            //string sConnectionString = m_sConnectionString;
            try
            {
                lstResTypeCode = new List<string>();
                sSQL = string.Format("SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL WHERE LINE_OF_BUS_CODE = {0}" +
                                   "AND COLL_IN_RSV_BAL = {1}", p_iLob, p_iPerRsvFlag);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objReader.Read())
                    {
                        lstResTypeCode.Add(Convert.ToString(objReader.GetValue("RES_TYPE_CODE")));
                    }
                }

                if (!lstResTypeCode.Contains(Convert.ToString(p_iReserveTypeCode)))
                {
                    return false;
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            finally
            {
                lstResTypeCode = null;
            }

            return true;
        }
        private double CalculateIncurred(double p_dBalance, double p_dPaid, double p_dCollect,
           bool p_bCollInRsvBal, bool p_bCollInIncurredBal, int p_iReserveTypeCode, bool p_bCollInPerRsvBal, bool p_bCollInPerIncurredBal, int p_iLOB)
        {
            double dTmp2 = 0;
            double dTmp = 0;

            bool bIsRecoveryReserve = false;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectString, m_iClientId);
            if (p_iReserveTypeCode != 0 && (objCache.GetRelatedShortCode(p_iReserveTypeCode) == "R"))
            {
                bIsRecoveryReserve = true;
            }
            objCache.Dispose();
            if (bIsRecoveryReserve)
            {
                if (p_dBalance < 0)
                {
                    dTmp = p_dCollect;  //MITS 35837
                }
                else
                {
                    dTmp = p_dBalance + p_dCollect;  //MITS 35837
                }
            }
            else
            {
                
                if (p_bCollInPerRsvBal)
                    p_bCollInPerRsvBal = this.CheckPerRsvSettings(-1, p_iReserveTypeCode, p_iLOB);
                if (p_bCollInPerIncurredBal)
                    p_bCollInPerIncurredBal = this.CheckPerRsvSettings(0, p_iReserveTypeCode, p_iLOB);
                if (p_bCollInRsvBal || p_bCollInPerRsvBal)
                {
                    dTmp2 = p_dPaid - p_dCollect;
                    
                    if (p_dBalance < 0)
                    {
                        dTmp = dTmp2;
                    }
                    else
                    {
                        dTmp = p_dBalance + dTmp2;
                    }
                }
                else
                {
                    if (p_dBalance < 0)
                    {
                        dTmp = p_dPaid;
                    }
                    else
                    {
                        dTmp = p_dBalance + p_dPaid;
                    }
                }
            }
            if (((p_bCollInIncurredBal) && (!bIsRecoveryReserve)) || p_bCollInPerIncurredBal)
            {
                dTmp = dTmp - p_dCollect;
            }
            
            return dTmp;
        }
        public XmlDocument GetPolicyLimits(XmlDocument objDoc)
        {
            XmlElement objPolicyLimits = null;
            XmlDocument p_objXmlDoc = null;
            SysSettings objSysSettings = null;
            string sPolicyKey=string.Empty;
            
             LocalCache objCache=null;
             ColLobSettings objCLOB =null;
            int iPolicyId=0;
            string sExternalPolicyKey=string.Empty;
            int iPolicyLob=0;
            int iLOB =0;
            XmlElement objUserPrefXmlElement = null;
            string strJsonUserPref = "";
            string sLimitGridId = string.Empty;
            string sSQL = "";
            Dictionary<string, object> dicGridData = null;
            List<Dictionary<string, object>> l_ListDictData = null;
            XmlElement objDataElement = null;
            string sCovIds = string.Empty;
            string sRsvTypeCode = string.Empty;
            try
            {
                objSysSettings = new SysSettings(m_sConnectString, m_iClientId);
                p_objXmlDoc = new XmlDocument();
                objPolicyLimits = p_objXmlDoc.CreateElement("PolicyLimits");

                iPolicyId = Conversion.ConvertObjToInt(objDoc.SelectSingleNode("//PolicyId").InnerText, m_iClientId);
                sExternalPolicyKey = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectString, "SELECT EXTERNAL_POLICY_KEY FROM POLICY WHERE POLICY_ID=" + iPolicyId));

                iPolicyLob = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + iPolicyId), m_iClientId);
                iLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + iPolicyLob), m_iClientId);



                objCache = new LocalCache(m_sConnectString, m_iClientId);

                objCLOB = new ColLobSettings(m_sConnectString, m_iClientId);
                 sLimitGridId = Conversion.ConvertObjToStr(objDoc.SelectSingleNode("//GridId").InnerText);
                
                strJsonUserPref = GetOrCreateUserPreference(m_oUserLogin.UserId, "PolicyLimits.aspx", m_oUserLogin.DatabaseId, sLimitGridId);
                 objUserPrefXmlElement = p_objXmlDoc.CreateElement("UserPref");
                objUserPrefXmlElement.InnerText = strJsonUserPref;
                objPolicyLimits.AppendChild(objUserPrefXmlElement);


                sSQL = "SELECT POL_COV_LIMIT.*,POLICY.POLICY_NAME FROM POL_COV_LIMIT,POLICY WHERE POLICY.POLICY_ID=" + iPolicyId + " AND POLICY.EXTERNAL_POLICY_KEY = POL_COV_LIMIT.POLICY_KEY  AND (COVERAGE_KEY IN ('') OR COVERAGE_KEY IS NULL )";
                 dicGridData = new Dictionary<string, object>();
                 l_ListDictData = new List<Dictionary<string, object>>();

                p_objXmlDoc.AppendChild(objPolicyLimits);
                
                CreateElement(p_objXmlDoc.DocumentElement, "Data", ref objDataElement);
                objCache = new LocalCache(m_sConnectString, m_iClientId);

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objRdr.Read())
                    {
                        sPolicyKey = Conversion.ConvertObjToStr(objRdr.GetValue("POLICY_KEY"));

                        sCovIds = string.Empty;
                        sRsvTypeCode = string.Empty;

                        
                        using (DbReader objrdr1 = DbFactory.GetDbReader(m_sConnectString, "SELECT CODE1 FROM CODE_X_CODE WHERE DELETED_FLAG=0 AND CODE2=" + Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId) + " AND REL_TYPE_CODE=" + objCache.GetCodeId("RTTOLT", "CODE_REL_TYPE")))
                        {
                            while (objrdr1.Read())
                            {
                                if (string.IsNullOrEmpty(sRsvTypeCode))
                                {
                                    sRsvTypeCode = Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                }
                                else
                                {
                                    sRsvTypeCode = sRsvTypeCode + "," + Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                }
                            }
                        }
                        double dblPaidTotal = 0;
                        double dblDedCollection = 0;
                        double dblCollectionTotal = 0;
                        double dblTotalReserve = 0;
                        if (objCache.GetRelatedShortCode(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId)) == "AGG")
                        {
                            if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY"))))
                            {
                                sCovIds = GetCoverageIdsFromPolicy(sExternalPolicyKey,0);
                            }
                            else
                            {
                                sCovIds = GetCoverageIdsFromCoverage(sExternalPolicyKey, Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY")),0);
                            }
                            if (!string.IsNullOrEmpty(sCovIds))
                            {
                                if (objSysSettings.PolicyLimitExceeded && objCLOB[iLOB].UseLimitTracking)
                                {
                                     dblPaidTotal = GetPaidAmountonCoverage(sCovIds, sRsvTypeCode);

                                    if (!string.IsNullOrEmpty(sRsvTypeCode))
                                    {

                                        dblDedCollection = GetCollectiononDeductibleReserve(sCovIds, Conversion.ConvertStrToBool(objCLOB[iLOB].ApplyDedToPaymentsFlag.ToString()), iLOB);
                                    }
                                     dblCollectionTotal = GetCollectionAmountonCoverage(sCovIds, sRsvTypeCode);
                                     dblTotalReserve = GetTolalReserve(sCovIds, sRsvTypeCode);
                                }
                                dicGridData = new Dictionary<string, object>();
                                dicGridData.Add("limit_level", Conversion.ConvertObjToStr(objRdr.GetValue("POLICY_NAME")));
                                dicGridData.Add("limit_type", objCache.GetCodeDesc(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId)) + " (" + objCache.GetCodeDesc(objCache.GetRelatedCodeId( Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId))) + ")");

                                dicGridData.Add("limit_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("limit_amount.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("LIMIT_AMOUNT")));


                                dicGridData.Add("total_reserve", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblTotalReserve, m_sConnectString, m_iClientId));
                                dicGridData.Add("total_reserve.DbValue", Convert.ToString(dblTotalReserve));

                                dicGridData.Add("paid_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblPaidTotal - dblDedCollection, m_sConnectString, m_iClientId));
                                dicGridData.Add("paid_amount.DbValue", Convert.ToString(dblPaidTotal - dblDedCollection));

                                dicGridData.Add("collection_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblCollectionTotal, m_sConnectString, m_iClientId));
                                dicGridData.Add("collection_amount.DbValue", Convert.ToString(dblCollectionTotal));



                                dicGridData.Add("limit_remaining", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remaining", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));
                                dicGridData.Add("limit_remaining.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));

                                dicGridData.Add("limit_remainingCol", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remainingCol", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));
                                dicGridData.Add("limit_remainingCol.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));

                                dicGridData.Add("limit_id", Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_ROW_ID"), m_iClientId).ToString());
                                l_ListDictData.Add(dicGridData);
                            }
                        }
                        else
                        {

                          


                            if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY"))))
                            {
                                sCovIds = GetCoverageIdsFromPolicy(sExternalPolicyKey,iPolicyId);
                            }
                            else
                            {
                                sCovIds = GetCoverageIdsFromCoverage(sExternalPolicyKey, Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY")),iPolicyId);
                            }
                            if (!string.IsNullOrEmpty(sCovIds))
                            {
                                if (objSysSettings.PolicyLimitExceeded && objCLOB[iLOB].UseLimitTracking)
                                {
                                     dblPaidTotal = GetPaidAmountonCoverage(sCovIds, sRsvTypeCode);
                                     dblDedCollection = 0;
                                    if (!string.IsNullOrEmpty(sRsvTypeCode))
                                    {
                                        dblDedCollection = GetCollectiononDeductibleReserve(sCovIds, Conversion.ConvertStrToBool(objCLOB[iLOB].ApplyDedToPaymentsFlag.ToString()), iLOB);
                                    }


                                     dblCollectionTotal = GetCollectionAmountonCoverage(sCovIds, sRsvTypeCode);
                                     dblTotalReserve = GetTolalReserve(sCovIds, sRsvTypeCode);
                                }

                                dicGridData = new Dictionary<string, object>();
                                dicGridData.Add("limit_level", Conversion.ConvertObjToStr(objRdr.GetValue("POLICY_NAME")) );
                                dicGridData.Add("limit_type", objCache.GetCodeDesc(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId)) + " (" + objCache.GetCodeDesc(objCache.GetRelatedCodeId(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId))) + ")");

                                dicGridData.Add("limit_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("limit_amount.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("LIMIT_AMOUNT")));


                                dicGridData.Add("total_reserve", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblTotalReserve, m_sConnectString, m_iClientId));
                                dicGridData.Add("total_reserve.DbValue", Convert.ToString(dblTotalReserve));

                                dicGridData.Add("paid_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblPaidTotal - dblDedCollection, m_sConnectString, m_iClientId));
                                dicGridData.Add("paid_amount.DbValue", Convert.ToString(dblPaidTotal - dblDedCollection));

                                dicGridData.Add("collection_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblCollectionTotal, m_sConnectString, m_iClientId));
                                dicGridData.Add("collection_amount.DbValue", Convert.ToString(dblCollectionTotal));



                                dicGridData.Add("limit_remaining", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remaining", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));
                                dicGridData.Add("limit_remaining.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));

                                dicGridData.Add("limit_remainingCol", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remainingCol", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));
                                dicGridData.Add("limit_remainingCol.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));

                                dicGridData.Add("limit_id", Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_ROW_ID"), m_iClientId).ToString());
                                l_ListDictData.Add(dicGridData);
                            }
                        }
                    }

                }

                sSQL = "SELECT POL_COV_LIMIT.*,POLICY_X_UNIT.UNIT_ID,POLICY_X_UNIT.UNIT_TYPE,POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE,POLICY_X_CVG_TYPE.COVERAGE_TEXT FROM POLICY_X_UNIT,POLICY_X_CVG_TYPE,POL_COV_LIMIT WHERE POLICY_X_UNIT.POLICY_ID=" + iPolicyId + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID= POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID AND POL_COV_LIMIT.POLICY_KEY='" + sPolicyKey + "' AND POL_COV_LIMIT.COVERAGE_KEY=POLICY_X_CVG_TYPE.COVERAGE_KEY";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objRdr.Read())
                    {


                         sCovIds = string.Empty;
                         sRsvTypeCode = string.Empty;
                         using (DbReader objrdr1 = DbFactory.GetDbReader(m_sConnectString, "SELECT CODE1 FROM CODE_X_CODE WHERE DELETED_FLAG=0 AND CODE2=" + Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId) + " AND REL_TYPE_CODE=" + objCache.GetCodeId("RTTOLT", "CODE_REL_TYPE")))
                         {
                             while (objrdr1.Read())
                             {
                                 if (string.IsNullOrEmpty(sRsvTypeCode))
                                 {
                                     sRsvTypeCode = Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                 }
                                 else
                                 {
                                     sRsvTypeCode = sRsvTypeCode + "," + Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                 }
                             }
                         }
                         double dblPaidTotal = 0;
                         double dblDedCollection = 0;
                         double dblCollectionTotal = 0;
                         double dblTotalReserve = 0;
                        if (objCache.GetRelatedShortCode(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId)) == "AGG")
                        {
                            if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY"))))
                            {
                                sCovIds = GetCoverageIdsFromPolicy(sExternalPolicyKey, 0);
                            }
                            else
                            {
                                sCovIds = GetCoverageIdsFromCoverage(sExternalPolicyKey, Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY")), 0);
                            }

                            if (!string.IsNullOrEmpty(sCovIds))
                            {
                                if (objSysSettings.PolicyLimitExceeded && objCLOB[iLOB].UseLimitTracking)
                                {
                                     dblPaidTotal = GetPaidAmountonCoverage(sCovIds, sRsvTypeCode);
                                     dblDedCollection = 0;
                                    if (!string.IsNullOrEmpty(sRsvTypeCode))
                                    {
                                        dblDedCollection = GetCollectiononDeductibleReserve(sCovIds, Conversion.ConvertStrToBool(objCLOB[iLOB].ApplyDedToPaymentsFlag.ToString()), iLOB);
                                    }

                                     dblCollectionTotal = GetCollectionAmountonCoverage(sCovIds, sRsvTypeCode);
                                     dblTotalReserve = GetTolalReserve(sCovIds, sRsvTypeCode);
                                }
                                dicGridData = new Dictionary<string, object>();
                                dicGridData.Add("limit_level", GetUnitDesc(Conversion.ConvertObjToInt(objRdr.GetValue("UNIT_ID"), m_iClientId), Conversion.ConvertObjToStr(objRdr.GetValue("UNIT_TYPE"))) + "-" +objCache.GetShortCode(Conversion.ConvertObjToInt(objRdr.GetValue("COVERAGE_TYPE_CODE"),m_iClientId)) +" "+ Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_TEXT")));
                                dicGridData.Add("limit_type", objCache.GetCodeDesc(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId)) + " (" + objCache.GetCodeDesc(objCache.GetRelatedCodeId(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId))) + ")");

                                dicGridData.Add("limit_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("limit_amount.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("LIMIT_AMOUNT")));


                                dicGridData.Add("total_reserve", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblTotalReserve, m_sConnectString, m_iClientId));
                                dicGridData.Add("total_reserve.DbValue", Convert.ToString(dblTotalReserve));

                                dicGridData.Add("paid_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblPaidTotal - dblDedCollection, m_sConnectString, m_iClientId));
                                dicGridData.Add("paid_amount.DbValue", Convert.ToString(dblPaidTotal - dblDedCollection));

                                dicGridData.Add("collection_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblCollectionTotal, m_sConnectString, m_iClientId));
                                dicGridData.Add("collection_amount.DbValue", Convert.ToString(dblCollectionTotal));



                                dicGridData.Add("limit_remaining", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remaining", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));
                                dicGridData.Add("limit_remaining.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));

                                dicGridData.Add("limit_remainingCol", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remainingCol", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));
                                dicGridData.Add("limit_remainingCol.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));

                                dicGridData.Add("limit_id", Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_ROW_ID"), m_iClientId).ToString());
                                l_ListDictData.Add(dicGridData);
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY"))))
                            {
                                sCovIds = GetCoverageIdsFromPolicy(sExternalPolicyKey, iPolicyId);
                            }
                            else
                            {
                                sCovIds = GetCoverageIdsFromCoverage(sExternalPolicyKey, Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY")), iPolicyId);
                            }

                            if (!string.IsNullOrEmpty(sCovIds))
                            {
                                if (objSysSettings.PolicyLimitExceeded && objCLOB[iLOB].UseLimitTracking)
                                {
                                     dblPaidTotal = GetPaidAmountonCoverage(sCovIds, sRsvTypeCode);
                                     dblDedCollection = 0;
                                    if (!string.IsNullOrEmpty(sRsvTypeCode))
                                    {
                                        dblDedCollection = GetCollectiononDeductibleReserve(sCovIds, Conversion.ConvertStrToBool(objCLOB[iLOB].ApplyDedToPaymentsFlag.ToString()), iLOB);
                                    }

                                     dblCollectionTotal = GetCollectionAmountonCoverage(sCovIds, sRsvTypeCode);
                                     dblTotalReserve = GetTolalReserve(sCovIds, sRsvTypeCode);
                                }
                                dicGridData = new Dictionary<string, object>();
                                dicGridData.Add("limit_level", GetUnitDesc(Conversion.ConvertObjToInt(objRdr.GetValue("UNIT_ID"), m_iClientId), Conversion.ConvertObjToStr(objRdr.GetValue("UNIT_TYPE"))) + "-" + objCache.GetShortCode(Conversion.ConvertObjToInt(objRdr.GetValue("COVERAGE_TYPE_CODE"), m_iClientId)) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_TEXT")));
                                dicGridData.Add("limit_type", objCache.GetCodeDesc(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId)) + " (" + objCache.GetCodeDesc(objCache.GetRelatedCodeId(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId))) + ")");

                                dicGridData.Add("limit_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("limit_amount.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("LIMIT_AMOUNT")));


                                dicGridData.Add("total_reserve", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblTotalReserve, m_sConnectString, m_iClientId));
                                dicGridData.Add("total_reserve.DbValue", Convert.ToString(dblTotalReserve));

                                dicGridData.Add("paid_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblPaidTotal - dblDedCollection, m_sConnectString, m_iClientId));
                                dicGridData.Add("paid_amount.DbValue", Convert.ToString(dblPaidTotal - dblDedCollection));

                                dicGridData.Add("collection_amount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, dblCollectionTotal, m_sConnectString, m_iClientId));
                                dicGridData.Add("collection_amount.DbValue", Convert.ToString(dblCollectionTotal));



                                dicGridData.Add("limit_remaining", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remaining", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));
                                dicGridData.Add("limit_remaining.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblDedCollection)));

                                dicGridData.Add("limit_remainingCol", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection), m_sConnectString, m_iClientId));
                                //dicGridData.Add("limit_remainingCol", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));
                                dicGridData.Add("limit_remainingCol.DbValue", Convert.ToString(Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) - (dblPaidTotal - dblCollectionTotal - dblDedCollection)));

                                dicGridData.Add("limit_id", Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_ROW_ID"), m_iClientId).ToString());
                                l_ListDictData.Add(dicGridData);
                            }
                        }
                        
                        }
                    }

                string strJsonResponse = Conversion.CreateJSONStringFromDictionary(l_ListDictData);
                objDataElement.InnerText = strJsonResponse;
                objPolicyLimits.AppendChild(objDataElement);

                dicGridData = new Dictionary<string, object>();
                dicGridData.Add("TotalCount", l_ListDictData.Count.ToString());
                l_ListDictData.Clear();
                dicGridData.Add("ApplyLimits", (objSysSettings.PolicyLimitExceeded && objCLOB[iLOB].UseLimitTracking).ToString());
                l_ListDictData.Add(dicGridData);
                string sAddData = Conversion.CreateJSONStringFromDictionary(l_ListDictData);
                XmlElement objAdditionalDataXmlElement = p_objXmlDoc.CreateElement("AdditionalData");
                objAdditionalDataXmlElement.InnerText = sAddData;
                objPolicyLimits.AppendChild(objAdditionalDataXmlElement);
                return p_objXmlDoc;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objSysSettings = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objCLOB = null;
            }
        }
        
        private string GetOrCreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            bool IsUserPrefExist = false;
            try
            {
                Riskmaster.Common.GridCommonFunctions.GetUserHeaderAndPreference(ref sJsonUserPref, p_iUserID, m_iClientId, p_sPageName, p_iDSNId.ToString(), p_sGridId);
                if (String.IsNullOrEmpty(sJsonUserPref))
                    IsUserPrefExist = false;
                else
                    IsUserPrefExist = true;

                if (!IsUserPrefExist)
                {
                    return CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("GetPolicyLimit.GetOrCreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }
        public string RestoreDefaultUserPref(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = "";
            try
            {
                if (Riskmaster.Common.GridCommonFunctions.DeleteUserPreference(p_iUserID, m_iClientId, p_sPageName, p_iDSNId, p_sGridId))
                    sJsonUserPref = CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("GetPolicyLimit.RestoreDefaultUserPref.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }


        private string CreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            try
            {
                //if there are multiple grids on the page, each grid can have different user pref.                
                if (p_sGridId == "gridPolicyLimits")
                    sJsonUserPref = CreateUserPreferenceForPolicyLimits(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                else if (p_sGridId == "gridErosions")
                {
                    sJsonUserPref = CreateUserPreferenceForErosion(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("GetPolicyLimit.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;

        }
        private string getMLHeaderText(string key, string sPageID)
        {
            string sUserLangCode = "0";
            try
            {
                sUserLangCode = m_oUserLogin.objUser.NlsCode.ToString();
                if (sUserLangCode == "0")
                {
                    //set base lang code as user lang code if it is not defined
                    sUserLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                }
                return CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString(key, 0, sPageID, m_iClientId), sUserLangCode);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }
        private string CreateUserPreferenceForPolicyLimits(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();
            string sPageID = "0";
            try
            {

                objSysSettings = new SysSettings(m_sConnectString, m_iClientId);
                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                lstColDef = new List<GridColumnDefHelper>();

                //By Default 5 visible columns
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_level", p_sdisplayName: getMLHeaderText("lbllimitlevel", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_type", p_sdisplayName: getMLHeaderText("lbllimitType", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_amount", p_sdisplayName: getMLHeaderText("lblLimitAmount", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_amount.DbValue", p_sdisplayName: getMLHeaderText("lblLimitAmount", sPageID),p_bvisible:false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "total_reserve", p_sdisplayName: getMLHeaderText("lbltotalreserve", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "total_reserve.DbValue", p_sdisplayName: getMLHeaderText("lbltotalreserve", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "paid_amount", p_sdisplayName: getMLHeaderText("lblpaidamount", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "paid_amount.DbValue", p_sdisplayName: getMLHeaderText("lblpaidamount", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "collection_amount", p_sdisplayName: getMLHeaderText("lblcollectionamount", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "collection_amount.DbValue", p_sdisplayName: getMLHeaderText("lblcollectionamount", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_remaining", p_sdisplayName: getMLHeaderText("lbllimitremaining", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_remaining.DbValue",p_sdisplayName: getMLHeaderText("lbllimitremaining", sPageID),p_bvisible:false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_remainingCol", p_sdisplayName: getMLHeaderText("lbllimitremainingWCol", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_remainingCol.DbValue", p_sdisplayName: getMLHeaderText("lbllimitremainingWCol", sPageID),p_bvisible:false,p_bAlwaysInvisibleOnColumnMenu: true));


                //By Default invisible columns
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "limit_id", p_sdisplayName: getMLHeaderText("lblLimitId", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.PageZise = "";
                oGridPref.SortColumn = new string[1] { "limit_id.DbValue" };
                oGridPref.SortDirection = new string[1] { "asc" };            
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;
                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("GetPolicyLimit.CreateUserPreferenceForPolicyLimits.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }

        public XmlDocument GetErorsionDetails(XmlDocument objDoc)
        {

            string sql=string.Empty;
            string sPolKey=string.Empty;
            string sCovKey=string.Empty;
             Dictionary<string, object> dicGridData = new Dictionary<string, object>();
            List<Dictionary<string, object>> l_ListDictData = new List<Dictionary<string, object>>();

            XmlElement objErosionDetails = null;
            XmlDocument p_objXmlDoc = null;
            SysSettings objSysSettings=null;
            int iLimitTypeCode = 0;
            string sRsvTypeCode = string.Empty;
            LocalCache objCache = null;
            XmlElement objUserPrefXmlElement = null;
            XmlElement objDataElement = null;
            int iPolicyId = 0;
            bool bIsAggregateLimit = false;
            double dRBalance = 0;
            double dRIncurred = 0;
            double dBalTotal = 0;
            double dPaidTotal = 0;
            double dCollTotal = 0;
            double dIncTotal = 0;
            bool bCollInRsvBal = false, bPerRsvColInRsvBal = false;
            bool bCollInIncurredBal = false, bPerRsvColInIncrBal = false;
            bool bResByClaimType = false;
            int iPolicyLob = 0;
            int iLOB = 0;
            StringBuilder sbSQL = null;
            try
            {
                objSysSettings = new SysSettings(m_sConnectString, m_iClientId);

                p_objXmlDoc = new XmlDocument();
                objErosionDetails = p_objXmlDoc.CreateElement("ErosionDetails");
                int iLimitRowId = Conversion.ConvertStrToInteger(objDoc.SelectSingleNode("//LimitRowId").InnerText);
                iPolicyId = Conversion.ConvertStrToInteger(objDoc.SelectSingleNode("//PolicyId").InnerText);

                //ColLobSettings objCLOB = new ColLobSettings(m_sConnectString, m_iClientId);
                string sLimitGridId = Conversion.ConvertObjToStr(objDoc.SelectSingleNode("//GridId").InnerText);
                string strJsonUserPref = "";
                strJsonUserPref = GetOrCreateUserPreference(m_oUserLogin.UserId, "PolicyLimits.aspx", m_oUserLogin.DatabaseId, sLimitGridId);
                 objUserPrefXmlElement = p_objXmlDoc.CreateElement("UserPref");
                objUserPrefXmlElement.InnerText = strJsonUserPref;
                objErosionDetails.AppendChild(objUserPrefXmlElement);

                p_objXmlDoc.AppendChild(objErosionDetails);
                 objDataElement = null;
                CreateElement(p_objXmlDoc.DocumentElement, "Data", ref objDataElement);

                

                iPolicyLob = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + iPolicyId), m_iClientId);
                iLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectString, "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + iPolicyLob), m_iClientId);

                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT RESERVE_TRACKING, COLL_IN_RSV_BAL,PER_RSV_COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL,BAL_TO_ZERO , NEG_BAL_TO_ZERO ,SET_TO_ZERO, COLL_IN_INCUR_BAL, RES_BY_CLM_TYPE "); //Parijat: MITS 7977
                sbSQL.Append("FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOB);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {

                            bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("COLL_IN_RSV_BAL").ToString()), m_iClientId);
                            bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), m_iClientId);
                            bResByClaimType = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("RES_BY_CLM_TYPE").ToString()), m_iClientId);
                            bPerRsvColInRsvBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                            bPerRsvColInIncrBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870

                        }
                        objReader.Close();
                    }
                }


                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT POLICY_KEY,COVERAGE_KEY,LIMIT_TYPE_CODE FROM POL_COV_LIMIT WHERE LIMIT_ROW_ID=" + iLimitRowId))
                {
                    if (objRdr.Read())
                    {
                        sPolKey = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        sCovKey = Conversion.ConvertObjToStr(objRdr.GetValue(1));
                        iLimitTypeCode = Conversion.ConvertObjToInt(objRdr.GetValue(2), m_iClientId);

                    }
                }

                if (iLimitTypeCode > 0)
                {
                    objCache = new LocalCache(m_sConnectString, m_iClientId);
                    using (DbReader objrdr = DbFactory.GetDbReader(m_sConnectString, "SELECT CODE1 FROM CODE_X_CODE WHERE DELETED_FLAG=0 AND CODE2=" + iLimitTypeCode + " AND REL_TYPE_CODE=" + objCache.GetCodeId("RTTOLT", "CODE_REL_TYPE")))
                    {
                        while (objrdr.Read())
                        {
                            if (string.IsNullOrEmpty(sRsvTypeCode))
                            {
                                sRsvTypeCode = Conversion.ConvertObjToStr(objrdr.GetValue(0));
                            }
                            else
                            {
                                sRsvTypeCode = sRsvTypeCode + "," + Conversion.ConvertObjToStr(objrdr.GetValue(0));
                            }
                        }
                    }

                    if (objCache.GetRelatedShortCode(iLimitTypeCode) == "AGG")
                    {
                        bIsAggregateLimit = true;
                    }
                    else
                        bIsAggregateLimit = false;
                }

                if (!string.IsNullOrEmpty(sPolKey))
                {
                    StringBuilder sSQL = new StringBuilder();

                    sSQL = sSQL.Append("SELECT CL.CLAIM_NUMBER, RC.ASSIGNADJ_EID,RC.BALANCE_AMOUNT,RC.PAID_TOTAL,RC.COLLECTION_TOTAL,RC.INCURRED_AMOUNT,RC.RC_ROW_ID,RC.CLAIM_ID,RC.RESERVE_TYPE_CODE ");
                    sSQL = sSQL.Append(" FROM POLICY POL,POLICY_X_UNIT POLU, POLICY_X_CVG_TYPE PCT, COVERAGE_X_LOSS CXL, RESERVE_CURRENT RC,CLAIM CL");


                    sSQL = sSQL.Append(" WHERE ");
                    if (bIsAggregateLimit)
                    {
                        sSQL = sSQL.Append(" POL.EXTERNAL_POLICY_KEY IN ('" + sPolKey + "')");
                    }
                    else
                    {
                        sSQL = sSQL.Append(" POL.POLICY_ID =" + iPolicyId);
                    }

                    sSQL = sSQL.Append(" AND POL.POLICY_ID = POLU.POLICY_ID");
                    sSQL = sSQL.Append(" AND POLU.POLICY_UNIT_ROW_ID= PCT.POLICY_UNIT_ROW_ID AND PCT.POLCVG_ROW_ID= CXL.POLCVG_ROW_ID AND CXL.CVG_LOSS_ROW_ID = RC.POLCVG_LOSS_ROW_ID ");
                    sSQL = sSQL.Append("   AND  RC.CLAIM_ID=CL.CLAIM_ID ");

                    if (!string.IsNullOrEmpty(sCovKey))
                    {
                        sSQL = sSQL.Append("   AND  PCT.COVERAGE_KEY='" + sCovKey + "' ");
                    }
                    if (!string.IsNullOrEmpty(sRsvTypeCode))
                    {
                        sSQL = sSQL.Append("   AND  RC.RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") ");



                        using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL.ToString()))
                        {
                            while (objRdr.Read())
                            {
                                dicGridData = new Dictionary<string, object>();

                                dicGridData.Add("ClaimNumber", Conversion.ConvertObjToStr(objRdr.GetValue("CLAIM_NUMBER")));
                                dicGridData.Add("Adjuster", GetAdjusterName(Conversion.ConvertObjToInt(objRdr.GetValue("ASSIGNADJ_EID"), m_iClientId), Conversion.ConvertObjToInt(objRdr.GetValue("CLAIM_ID"), m_iClientId)));



                                dicGridData.Add("RsvBalance", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("BALANCE_AMOUNT"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("RsvBalance.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("BALANCE_AMOUNT")));

                                dicGridData.Add("PaidAmount", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("PAID_TOTAL"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("PaidAmount.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("PAID_TOTAL")));

                                dicGridData.Add("Collection", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("COLLECTION_TOTAL"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("Collection.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("COLLECTION_TOTAL")));

                                dicGridData.Add("Incurred", CommonFunctions.ConvertByCurrencyCode(objSysSettings.BaseCurrencyType, Conversion.ConvertObjToDouble(objRdr.GetValue("INCURRED_AMOUNT"), m_iClientId), m_sConnectString, m_iClientId));
                                dicGridData.Add("Incurred.DbValue", CalculateIncurred(Conversion.ConvertObjToDouble(objRdr.GetValue("BALANCE_AMOUNT"), m_iClientId), Conversion.ConvertObjToDouble(objRdr.GetValue("PAID_TOTAL"), m_iClientId), Conversion.ConvertObjToDouble(objRdr.GetValue("COLLECTION_TOTAL"), m_iClientId), bCollInRsvBal, bCollInIncurredBal, Conversion.ConvertObjToInt(objRdr.GetValue("RESERVE_TYPE_CODE"), m_iClientId), bPerRsvColInRsvBal, bPerRsvColInIncrBal, iLOB));

                                dicGridData.Add("ErosionRowId", Conversion.ConvertObjToStr(objRdr.GetValue("RC_ROW_ID")));
                                dicGridData.Add("ErosionRowId.DbValue", Conversion.ConvertObjToStr(objRdr.GetValue("RC_ROW_ID")));
                                l_ListDictData.Add(dicGridData);
                            }
                        }
                    }
                    //else
                    //{
                    //    return p_objXmlDoc;
                    //}
                }
                string strJsonResponse = Conversion.CreateJSONStringFromDictionary(l_ListDictData);
                objDataElement.InnerText = strJsonResponse;
                objErosionDetails.AppendChild(objDataElement);

                dicGridData = new Dictionary<string, object>();
                dicGridData.Add("TotalCount", l_ListDictData.Count.ToString());
                l_ListDictData.Clear();
                l_ListDictData.Add(dicGridData);
                string sAddData = Conversion.CreateJSONStringFromDictionary(l_ListDictData);
                XmlElement objAdditionalDataXmlElement = p_objXmlDoc.CreateElement("AdditionalData");
                objAdditionalDataXmlElement.InnerText = sAddData;
                objErosionDetails.AppendChild(objAdditionalDataXmlElement);
                return p_objXmlDoc;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string CreateUserPreferenceForErosion(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();
            string sPageID = "0";
            try
            {

                objSysSettings = new SysSettings(m_sConnectString, m_iClientId);
                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                lstColDef = new List<GridColumnDefHelper>();

                //By Default 5 visible columns
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "ClaimNumber", p_sdisplayName: getMLHeaderText("ClaimNumber", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "Adjuster", p_sdisplayName: getMLHeaderText("Adjuster", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "RsvBalance", p_sdisplayName: getMLHeaderText("RsvBalance", sPageID)));
                //lstColDef.Add(new GridColumnDefHelper(p_sfield: "RsvBalance", p_sdisplayName: getMLHeaderText("RsvBalance", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "RsvBalance.DBValue", p_sdisplayName: getMLHeaderText("RsvBalance", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "PaidAmount", p_sdisplayName: getMLHeaderText("PaidAmount", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "PaidAmount.DBValue", p_sdisplayName: getMLHeaderText("PaidAmount", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "Collection", p_sdisplayName: getMLHeaderText("Collection", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "Collection.DbValue", p_sdisplayName: getMLHeaderText("Collection", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                lstColDef.Add(new GridColumnDefHelper(p_sfield: "Incurred", p_sdisplayName: getMLHeaderText("Incurred", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "Incurred.DbValue", p_sdisplayName: getMLHeaderText("Incurred", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                

                //By Default invisible columns
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "ErosionRowId", p_sdisplayName: getMLHeaderText("ErosionRowId", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.PageZise = "";
                oGridPref.SortColumn = new string[1] { "ErosionRowId.DbValue" };
                oGridPref.SortDirection = new string[1] { "asc" };
                dicAdditionalUserPref["showAll"] = false;
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;
                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("GetPolicyLimit.CreateUserPreferenceForErosion.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string GetClaimAdjuster(int iClaimId)
        {
            string sAdjuster = string.Empty;

            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectString, "SELECT LAST_NAME,FIRST_NAME,MIDDLE_NAME,CURRENT_ADJ_FLAG,ADJ_ROW_ID FROM ENTITY,CLAIM_ADJUSTER WHERE CLAIM_ID=" + iClaimId + " AND ADJUSTER_EID= ENTITY_ID ORDER BY ADJ_ROW_ID DESC"))
                {
                    while (objRdr.Read())
                    {


                        if (Conversion.ConvertObjToBool(objRdr.GetValue("CURRENT_ADJ_FLAG"), m_iClientId))
                        {
                            sAdjuster = Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("MIDDLE_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME"));
                            break;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(sAdjuster))
                            {
                                sAdjuster = Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("MIDDLE_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME"));
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return sAdjuster;
        }
       
      
    }
}
