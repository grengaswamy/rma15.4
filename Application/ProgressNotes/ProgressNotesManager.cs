using Riskmaster.Security.Encryption;
using System;
using System.Xml;
using System.Collections.Specialized;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Collections;
using System.Collections.Generic;
using C1.C1PrintDocument;
//rsolanki2(aug 2011): When we migrate to use the C1 2011v2 editions of the control, 
// there are some changes int he C1 we will need to take cognizance of. 
// The C1.PrintDocument has been shifted. We will need to add
// reference to the C1.C1Preview namespace which lies in the C1.C1Report assembly. 
// The Object Model between the two has been radically changed. Will need updates to this entire file when upgrading.

//using C1.C1Report;
//using C1.C1Preview;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Xml.Linq;
using System.Data;
using Riskmaster.Models;
using Riskmaster.Application.Search;
using Riskmaster.Security.Authentication;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Security;

namespace Riskmaster.Application.ProgressNotes
{
    ///************************************************************** 
    ///* $File		: ProgressNotesManager.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 2-Oct-2005 
    ///* $Author	: Raman Bhatia and Vaibhav Kaushik
    ///* $Comment	: ProgressNotes Class used for ClaimProgressNotes Module
    ///* $Source	: 
    ///**************************************************************
    public class ProgressNotesManager : IDisposable, IComparer
    {
        #region Variable Declaration
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;
        /// <summary>
        /// Private variable to store the instance of LocalCache object
        /// </summary>
        private LocalCache m_objLocalCache = null;
        /// <summary>
        /// Private variable to store the Db Type
        /// </summary>
        private string p_sDbType = string.Empty;
        /// <summary>
        /// Private variable to store the header of the report
        /// </summary>
        private string m_sReportHeader = string.Empty;
        /// <summary>
        /// Private variable to store the Claim Selected Flag
        /// </summary>
        private bool m_bClaimSelected = false;
        /// <summary>
        /// Private varibale to store clientID in multi-tenant environment, cloud
        /// </summary>
        private int m_iClientId = 0;

        #endregion
        //Added Rakhi for R6: Changes for re-ordering of Headers for Enhanced Notes - start
        private const string HEADER_ACTIVITY_DATE = "Activity Date";
        private const string HEADER_ATTACHED_TO = "Attached To";
        private const string HEADER_NOTE_TYPE = "Note Type";
        private const string HEADER_NOTE_TEXT = "Note Text";
        private const string HEADER_ENTERED_BY = "Entered By";
        //Added by Amitosh for mits 23691 (05/11/2011)
        private const string HEADER_DATE_CREATED = "DateCreated";
        private const string HEADER_TIME_CREATED = "TimeCreated";
        private const string HEADER_USER_TYPE = "UserTypeCode";
        private const string HEADER_SUBJECT = "Subject";  //zmohammad MITS 30218
        //Added Rakhi for R6: Changes for re-ordering of Headers for Enhanced Notes - end
        //Added by Amitosh for mits 23473 (05/11/2011)
        private string sort_by = string.Empty;
        //Start by Shivendu for MITS 18098
        private const string ACTIVITY_DATE = "DateEntered";
        private const string NOTE_MEMO = "NoteMemo";
        private const string ATTACHED_TO = "AttachedTo";
        private const string NOTE_TYPE = "NoteTypeCode";
        private const string ENTERED_BY = "AdjusterName";
        private const string sNoSort = "NoSort";
        //End by Shivendu for MITS 18098
        private static string HeaderKeyShowList = string.Empty;//Deb MITS 30185
        //rsolanki2: ExecSummaryScheduler enhacments 
        private string m_ExecutiveSummaryModuleID = string.Empty;
        public string m_sTMConnectionString = string.Empty;
        public UserLogin m_UserLogin;
        private const int m_MAXNOTESCOUNT = 9999999;
        public int iCaseManagement = 0;//averma62 Mits 28988

        //MITS 26763 : Client requests change to custom print process for Large Notes 
        //modified by Raman Bhatia on 12/06/2011
        private int m_iGenerateReportThresholdExceeded = 0;

        // Added by akaushik5 for MITS 30789 Starts
        private string orderBy = string.Empty;
        // Added by akaushik5 for MITS 30789 Ends

        #region Constructor
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DsnName</param>
        public ProgressNotesManager(string p_sDsnName, string p_sUserName, string p_sPassword,int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
             m_iClientId = p_iClientId; //Ash - cloud
            m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
        }
       
        ~ProgressNotesManager()
        {
            Dispose();
        }
        #endregion

        public void Dispose()
        {            
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }

        /// <summary>
        /// Read Property for LocalCache.
        /// </summary>
        private LocalCache LocalCache
        {
            get
            {
                if (m_objLocalCache == null)
                {
                    m_objLocalCache = m_objDataModelFactory.Context.LocalCache;
                }
                return (m_objLocalCache);
            }
        }

        /// <summary>
        /// Read/Write property for DBType.
        /// </summary>
        private string DBType
        {
            get
            {
                if (p_sDbType == "")
                {
                    p_sDbType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                }
                return (p_sDbType);
            }
        }

        //MITS 26763 : Client requests change to custom print process for Large Notes 
        //modified by Raman Bhatia on 12/06/2011

        public int GenerateReportThresholdExceeded
        {
            get
            {                
                return m_iGenerateReportThresholdExceeded;
            }
            set
            {
                m_iGenerateReportThresholdExceeded = value;
            }
        }

        public enum GenerateReportThresholdExceededValues
        {
            GenerateReport = 1,
            ScheduleReport = 2,
            DoNothing = 0
        }


        #region Print Notes

        /// Name		: NewPage
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for printing notes
        /// </summary>

        private void NewPage(C1PrintDocument p_objPrintDoc, NewPageStartedEventArgs p_objArgs)
        {
            RenderTable objHeaderTable = null;
            string sHeaderText = string.Empty;

            try
            {
                sHeaderText = "Enhanced Notes                             Page "
                    + p_objPrintDoc.PageCount + "                             "
                    + DateTime.Now.ToShortDateString();

                objHeaderTable = new RenderTable(p_objPrintDoc);
                objHeaderTable.Style.Borders.AllEmpty = false;
                objHeaderTable.Style.Borders.Left = new LineDef(Color.Black, 0);
                objHeaderTable.Style.Borders.Right = new LineDef(Color.Black, 0);
                objHeaderTable.Style.Borders.Bottom = new LineDef(Color.Black, 0);
                objHeaderTable.Style.Borders.Top = new LineDef(Color.Black, 0);

                objHeaderTable.StyleTableCell.BorderTableHorz.Empty = true;
                objHeaderTable.StyleTableCell.BorderTableVert.Empty = true;
                objHeaderTable.StyleTableCell.BorderTableHorz.WidthPt = 0;
                objHeaderTable.StyleTableCell.BorderTableVert.WidthPt = 0;

                objHeaderTable.Columns.AddSome(1);
                objHeaderTable.Body.Rows.AddSome(2);
                objHeaderTable.Columns[0].StyleTableCell.TextAlignHorz = AlignHorzEnum.Center;
                objHeaderTable.Columns[0].Width = 11000;

                objHeaderTable.Body.Cell(0, 0).StyleTableCell.Font = new Font("Arial", 10, FontStyle.Bold);
                objHeaderTable.Body.Cell(0, 0).RenderText.Text = sHeaderText;

                objHeaderTable.Body.Cell(1, 0).StyleTableCell.Font = new Font("Arial", 10, FontStyle.Bold);
                objHeaderTable.Body.Cell(1, 0).RenderText.Text = m_sReportHeader;

                p_objPrintDoc.PageHeader.RenderObject = objHeaderTable;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.NewPage.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objHeaderTable != null)
                {
                    objHeaderTable.Dispose();
                    objHeaderTable = null;
                }
            }

        }

        /// Name		: PrintProgressNotesReport
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for printing notes
        /// </summary>
        // p_sClaimantId: Added by gbindra MITS#34104 WWIG GAP15 02122014
        //Changed by Gagan for MITS 8697 : Start
        //Changed Rakhi for R6-Print Note by Note Type
        //public bool PrintProgressNotesReport(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, ref string p_sPdfDocPath, bool p_bPrintSelectedNotes, int p_iClaimProgressNoteId)
        //williams-neha goel:added iPolicyID for policy enhanced notes::MITS 21704
        //averma62 MITS - 27826 public bool PrintProgressNotesReport(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, ref string p_sPdfDocPath, bool p_bPrintSelectedNotes, int p_iClaimProgressNoteId, int p_iNoteTypeCode)
        public bool PrintProgressNotesReport(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, ref string p_sPdfDocPath, bool p_bPrintSelectedNotes, int p_iClaimProgressNoteId, int p_iNoteTypeCode, int p_iPolicyId, int p_iClaimantId)  //averma62 MITS - 27826 - added one more parameter policy id
        {
            //Changed by Gagan for MITS 8697 : End
            ProgressNotes structProgressNotes;
            ArrayList arrlstProgressNotes = null;
            C1PrintDocument objPrintDoc = null;
            RenderRichText objRichText = null;
            RenderTable objTable = null;
            TableColumn objTableColumn = null;
            //williams-neha goel--start::MITS 21704
            string sPolicyName = string.Empty;
            string sPolicyNumber = string.Empty;
            string sPolicyIds = string.Empty;//averma62
            string sPolSql = string.Empty;//averma62
            //williams-neha goel--end::MITS 21704

            string sClaimantName = "";     //gagnihotri MITS 11280 03/11/2008
            string sProgressNotesPDFPath = "";
            int iIndex = 0;
            int iRowIndex = -1;
            string sSql = string.Empty;
            long iNotesCharCount = 0;
            bool bConversionSuccess = false;
            SysSettings objSettings = null;
            long lPolNotesCharCount = 0;
            try
            {
                #region chking notes count for passing to the ExecutiveSumaryScheduler
                if (!(AppDomain.CurrentDomain.FriendlyName.StartsWith("ExecutiveSummaryScheduler", StringComparison.OrdinalIgnoreCase)))
                {

                    //rsolanki2: retriving notes character count here
                    if (p_iClaimId > 0 || p_iEventId > 0)
                    {
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString().ToUpper().IndexOf("ORACLE") == -1)
                        {
                            //sql server
                            //sSql = "SELECT SUM(LEN(CAST (NOTE_MEMO AS VARCHAR(MAX)))) FROM CLAIM_PRG_NOTE ";
                            sSql = "SELECT SUM(LEN(NOTE_MEMO)) FROM CLAIM_PRG_NOTE WHERE CLAIM_ID = "
                                + p_iClaimId.ToString()
                                + " AND EVENT_ID = "
                                + p_iEventId.ToString();

                            //MITS 26763 : Client requests change to custom print process for Large Notes 
                            //modified by Raman Bhatia on 12/06/2011
                            if (p_iClaimProgressNoteId != 0)
                            {
                                sSql = sSql + " AND CL_PROG_NOTE_ID = " + p_iClaimProgressNoteId.ToString();
                            }

                            if (p_iNoteTypeCode != 0)
                            {
                                sSql = sSql + " AND NOTE_TYPE_CODE = " + p_iNoteTypeCode.ToString();
                            }

                        }
                        else
                        {
                            //oracle
                            sSql = "SELECT SUM(LENGTH(NOTE_MEMO)) FROM CLAIM_PRG_NOTE WHERE CLAIM_ID = "
                                + p_iClaimId.ToString()
                                + " AND EVENT_ID = "
                                + p_iEventId.ToString();

                            //MITS 26763 : Client requests change to custom print process for Large Notes 
                            //modified by Raman Bhatia on 12/06/2011
                            if (p_iClaimProgressNoteId != 0)
                            {
                                sSql = sSql + " AND CL_PROG_NOTE_ID = " + p_iClaimProgressNoteId.ToString();
                            }

                            if (p_iNoteTypeCode != 0)
                            {
                                sSql = sSql + " AND NOTE_TYPE_CODE = " + p_iNoteTypeCode.ToString();
                            }
                        }

                        DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

                        if (objReader != null)
                        {
                            while (objReader.Read())
                            {
                                iNotesCharCount = Conversion.CastToType<long>(objReader[0].ToString(), out bConversionSuccess);
                            }
                        }
                        objReader.Close();
                        objSettings = new SysSettings(m_sConnectionString, m_iClientId);
                        //Start averma62
                        if (objSettings.EnhNotesPolicyClaimView && (p_iClaimId > 0))
                        {
                            if (objSettings.MultiCovgPerClm.ToString() == "-1")
                            {
                                sPolSql = "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + p_iClaimId;
                                using (DbReader objPolReader = DbFactory.GetDbReader(m_sConnectionString, sPolSql))
                                {
                                    while (objPolReader.Read())
                                    {
                                        if (string.IsNullOrEmpty(sPolicyIds))
                                            sPolicyIds = Conversion.ConvertObjToStr(objPolReader.GetValue("POLICY_ID"));
                                        else
                                            sPolicyIds = sPolicyIds + "," + Conversion.ConvertObjToStr(objPolReader.GetValue("POLICY_ID"));
                                    }
                                }
                            }
                            //End averma62
                            else
                            {
                                sPolSql = "SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                                using (DbReader objPolReader = DbFactory.GetDbReader(m_sConnectionString, sPolSql))
                                {
                                    while (objPolReader.Read())
                                    {
                                        sPolicyIds = Conversion.ConvertObjToStr(objPolReader.GetValue("PRIMARY_POLICY_ID"));
                                    }
                                }
                            }

                        }

                    }
                    else if (p_iPolicyId > 0)
                    {
                        sPolicyIds = p_iPolicyId.ToString();
                    }

                    if (!string.IsNullOrEmpty(sPolicyIds))
                    {
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString().ToUpper().IndexOf("ORACLE") == -1)
                        {
                            sSql = "SELECT SUM(LEN(NOTE_MEMO)) FROM CLAIM_PRG_NOTE WHERE POLICY_ID IN( "
                                + sPolicyIds + ")";

                            if (p_iClaimProgressNoteId != 0)
                            {
                                sSql = sSql + " AND CL_PROG_NOTE_ID = " + p_iClaimProgressNoteId.ToString();
                            }

                            if (p_iNoteTypeCode != 0)
                            {
                                sSql = sSql + " AND NOTE_TYPE_CODE = " + p_iNoteTypeCode.ToString();
                            }

                        }
                        else
                        {
                            sSql = "SELECT SUM(LENGTH(NOTE_MEMO)) FROM CLAIM_PRG_NOTE WHERE POLICY_ID IN( "
                                + sPolicyIds + ")";

                            if (p_iClaimProgressNoteId != 0)
                            {
                                sSql = sSql + " AND CL_PROG_NOTE_ID = " + p_iClaimProgressNoteId.ToString();
                            }

                            if (p_iNoteTypeCode != 0)
                            {
                                sSql = sSql + " AND NOTE_TYPE_CODE = " + p_iNoteTypeCode.ToString();
                            }
                        }

                        DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

                        if (objReader != null)
                        {
                            while (objReader.Read())
                            {
                                lPolNotesCharCount = Conversion.CastToType<long>(objReader[0].ToString(), out bConversionSuccess);
                            }
                        }
                        objReader.Close();
                    }
                    iNotesCharCount = iNotesCharCount + lPolNotesCharCount;
                    int iThresHoldLimit = 0;
                    // Ash - cloud, fetch config setting from db
                    //int.TryParse(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold").ToString(), out iThresHoldLimit);
                    int.TryParse(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold", m_sConnectionString, m_iClientId).ToString(), out iThresHoldLimit);
                    
                    if (iNotesCharCount > iThresHoldLimit * 2500)
                    //rsolanki2: we are assuming that one page contains around 2500 characters
                    {
                        //MITS 26763 : Client requests change to custom print process for Large Notes 
                        //modified by Raman Bhatia on 12/06/2011
                        if (m_iGenerateReportThresholdExceeded == (int)GenerateReportThresholdExceededValues.ScheduleReport)
                        {
                            //averma62 CreateTMEntry(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, p_bPrintSelectedNotes, p_iClaimProgressNoteId, p_iNoteTypeCode);
                            foreach (string sPolicyId in sPolicyIds.Split(','))
                            {
                                //CreateTMEntry(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, p_bPrintSelectedNotes, p_iClaimProgressNoteId, p_iNoteTypeCode, p_iPolicyId);
                                CreateTMEntry(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, p_bPrintSelectedNotes, p_iClaimProgressNoteId, p_iNoteTypeCode, Conversion.ConvertStrToInteger(sPolicyId));
                            }
                        }
                        //if (m_iGenerateReportThresholdExceeded != (int)GenerateReportThresholdExceededValues.GenerateReport)
                        //{
                        //    throw new InvalidCriteriaException();
                        //}
                        return false;


                    }
                }
                #endregion

                sProgressNotesPDFPath = RMConfigurator.TempPath;

                //williams-neha goel-start:added p_iPolicyID for policy enhanced notes::MITS 21704
                //arrlstProgressNotes = this.GetNotes( p_iEventId , p_iClaimId , p_bActivateFilter , p_sFilterSQL, p_iNoteTypeCode,"","");
                //  mkaran2 - MITS 27038 - Added string p_sViewNotesOptionList, string p_sViewNotesTypeList
                arrlstProgressNotes = this.GetNotes(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, p_iNoteTypeCode, "", "", p_iPolicyId, "", "", p_iClaimantId);//Added by gbindra MITS#34104 WWIG GAP15
                //willimas:end:MITS 21704
                if (p_iClaimId != 0)
                //gagnihotri MITS 11280 03/11/2008 Start
                {
                    sClaimantName = this.GetClaimantName(p_iClaimId.ToString());
                    if (sClaimantName != string.Empty)
                        //Replaced RMUser Information with Claimant Info.
                        m_sReportHeader = "Event Number: " + this.GetEventName(p_iEventId) + "  *  " + "Claim Number: " + this.GetClaimName(p_iClaimId)
                            //                + "  *  " + m_objDataModelFactory.Context.RMUser.objUser.LastName 
                            //                + " " + m_objDataModelFactory.Context.RMUser.objUser.FirstName ;
                                          + "  *  " + "Claimant Name: " + sClaimantName;
                    else
                        //Removed RMUser Information
                        m_sReportHeader = "Event Number: " + this.GetEventName(p_iEventId) + "  *  " + "Claim Number: " + this.GetClaimName(p_iClaimId);
                }
                //williams-neha goel---start:added if condition for POlicy Enhanced notes::MITS 21704
                else if (p_iPolicyId != 0)
                {
                    Policy objPolicy = null;
                    objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                    objPolicy.MoveTo(p_iPolicyId);
                    if (objPolicy != null)
                    {
                        sPolicyName = this.GetPolicyName(p_iPolicyId, ref sPolicyNumber);
                        if (sPolicyName != string.Empty)
                            m_sReportHeader = "Policy Name: " + sPolicyName + " * " + sPolicyNumber;
                        else
                            m_sReportHeader = "Policy Name: " + sPolicyName;
                    }
                    if (objPolicy != null)
                        objPolicy.Dispose();
                    objPolicy = null;
                }
                //williams-neha goel---end:added if condition for POlicy Enhanced notes::MITS 21704
                else
                {
                    Event objEvent = null;
                    objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                    objEvent.MoveTo(p_iEventId);
                    if (objEvent.PiList.GetPrimaryPi() != null)
                        //Replaced RMUser Information with Person Involved Info.
                        m_sReportHeader = "Event Number: " + this.GetEventName(p_iEventId)
                        + "  *  " + "Person Involved: " + objEvent.PiList.GetPrimaryPi().PiEntity.GetLastFirstName();
                    else
                        //Removed RMUser Information
                        m_sReportHeader = "Event Number: " + this.GetEventName(p_iEventId); //+ "     *      " + m_objDataModelFactory.Context.RMUser.objUser.LastName
                    //+ " " + m_objDataModelFactory.Context.RMUser.objUser.FirstName;

                    if (objEvent != null)
                        objEvent.Dispose();
                    objEvent = null;
                }
                //gagnihotri MITS 11280 03/11/2008 End
                #region Initialize document and set the layout properties.
                objPrintDoc = new C1PrintDocument();
                objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
                objPrintDoc.PageSettings.Margins.Top = 45;
                objPrintDoc.PageSettings.Margins.Left = 40;
                objPrintDoc.PageSettings.Margins.Bottom = 40;
                objPrintDoc.PageSettings.Margins.Right = 40;
                objPrintDoc.PageSettings.Landscape = false;
                objPrintDoc.NewPageStarted += new NewPageStartedEventHandler(this.NewPage);
                objPrintDoc.StartDoc();

                objTable = new RenderTable(objPrintDoc);
                objPrintDoc.Style.Borders.AllEmpty = true;
                objTable.Body.StyleTableCell.Font = new Font("Courier New", 10);

                objTable.Columns.AddSome(1);
                objTableColumn = new TableColumn(objPrintDoc);
                objTableColumn.Width = 10000;
                objTable.Columns[0] = objTableColumn;

                objTable.Body.AutoHeight = true;

                objTable.StyleTableCell.Borders.Bottom = new LineDef(Color.Black, 0);
                objTable.StyleTableCell.Borders.Top = new LineDef(Color.Black, 0);
                objTable.StyleTableCell.Borders.Left = new LineDef(Color.Black, 0);
                objTable.StyleTableCell.Borders.Right = new LineDef(Color.Black, 0);

                objTable.Style.Borders.Left = new LineDef(Color.Black, 0);
                objTable.Style.Borders.Right = new LineDef(Color.Black, 0);
                objTable.Style.Borders.Bottom = new LineDef(Color.Black, 0);
                objTable.Style.Borders.Top = new LineDef(Color.Black, 0);

                objTable.StyleTableCell.Padding.Right = 0;
                objTable.StyleTableCell.Padding.Top = 0;
                objTable.StyleTableCell.Padding.Bottom = 0;

                objTable.StyleTableCell.BorderTableHorz.Empty = false;
                objTable.StyleTableCell.BorderTableVert.Empty = false;

                objTable.StyleTableCell.BorderTableHorz.WidthPt = 0;
                objTable.StyleTableCell.BorderTableVert.WidthPt = 0;

                objRichText = new RenderRichText(objPrintDoc);

                #endregion

                // Set cell text.
                for (iIndex = 0; iIndex < arrlstProgressNotes.Count; iIndex++)
                {
                    structProgressNotes = (ProgressNotes)arrlstProgressNotes[iIndex];

                    //Changed by Gagan for MITS 8697 : Start
                    //If only selected progress notes needs to be printed
                    if (p_bPrintSelectedNotes == true)
                    {
                        //Skip printing if not selected notes 
                        if (structProgressNotes.ClaimProgressNoteId != p_iClaimProgressNoteId)
                            continue;
                    }
                    //Changed by Gagan for MITS 8697 : End					
                    objTable.Body.Rows.AddSome(7);//Parijat: 19591 --  Increasing the row for 'EnteredBy' user name
                    //mkaran2 : MITS 33735 : Increased size from 6 to 7; New field "subject"
                    iRowIndex++;
                    //objTable.Body.Cell(iRowIndex, 0).RenderText.Text = structProgressNotes.DateEntered;
                    objTable.Body.Cell(iRowIndex, 0).RenderText.Text = "Activity Date : " + structProgressNotes.DateEntered; //mkaran2 - JIRA-RMA-1679 -MITS 36922

                    iRowIndex++;
                    objTable.Body.Cell(iRowIndex, 0).StyleTableCell.Font = new Font("Courier New", 10, FontStyle.Italic);
                    //objTable.Body.Cell(iRowIndex, 0).RenderText.Text = structProgressNotes.DateCreated + "   " + structProgressNotes.TimeCreated;
                    objTable.Body.Cell(iRowIndex, 0).RenderText.Text = "Date Created : " + structProgressNotes.DateCreated + "   " + structProgressNotes.TimeCreated; //mkaran2 - JIRA-RMA-1679 -MITS 36922
                    
                    iRowIndex++;
                    objTable.Body.Cell(iRowIndex, 0).StyleTableCell.Font = new Font("Courier New", 10, FontStyle.Bold);
                    //objTable.Body.Cell(iRowIndex, 0).RenderText.Text = structProgressNotes.NoteType;
                    objTable.Body.Cell(iRowIndex, 0).RenderText.Text = "Note Type : " + structProgressNotes.NoteType; //mkaran2 - JIRA-RMA-1679 -MITS 36922

                    //rsharma220 MITS 37161
                    //mkaran2 : MITS 33735 :Start
                    iRowIndex++;
                    //objTable.Body.Cell(iRowIndex, 0).RenderText.Text = structProgressNotes.Subject;
                    objTable.Body.Cell(iRowIndex, 0).RenderText.Text = "Subject : " + structProgressNotes.Subject;//mkaran2 - JIRA-RMA-1679 -MITS 36922
                

                    //mkaran2 : MITS 33735 :End 

                    iRowIndex++;
                    if (structProgressNotes.NoteMemo == "")
                        structProgressNotes.NoteMemo = "p";
                    objRichText.CanSplit = true;
                    //Parijat:14970
                    PrintMemoString(structProgressNotes.NoteMemoCareTech, ref objTable, ref iRowIndex);
                    //objTable.Body.Cell( iRowIndex , 0 ).RenderText.Text =  structProgressNotes.NoteMemoCareTech;
                    //Parijat: 19591 Including 'Entered By' User there                                     

                    iRowIndex++;
                    //objTable.Body.Cell(iRowIndex, 0).RenderText.Text = structProgressNotes.EnteredByName;
                    objTable.Body.Cell(iRowIndex, 0).RenderText.Text = "Entered By : " + structProgressNotes.EnteredByName;//mkaran2 - JIRA-RMA-1679 -MITS 36922
                    //End 19591
                    iRowIndex++;
                    objTable.Body.Cell(iRowIndex, 0).RenderText.Text = " ";
                }
                // Render the table in block flow.
                objPrintDoc.RenderBlock(objTable);

                // End doc.
                objPrintDoc.EndDoc();

                // Save the PDF.
                //Deb MITS 26390
                sProgressNotesPDFPath += "\\ProgressNotes" + DateTime.Now.ToString("yyyyMMddHHmmss") + AppDomain.GetCurrentThreadId() + ".pdf";
                //Deb MITS 26390
                objPrintDoc.ExportToPDF(sProgressNotesPDFPath, false);
                p_sPdfDocPath = sProgressNotesPDFPath;
                return (true);
            }
            catch (InvalidCriteriaException p_objException)
            {
                //
                throw new InvalidCriteriaException(Globalization.GetString("ProgressNotesManager.PrintProgressNotesReport.ThresholdExceeded", m_iClientId), p_objException);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.PrintProgressNotesReport.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objPrintDoc != null)
                {
                    objPrintDoc.Dispose();
                    objPrintDoc = null;
                }
                if (objTable != null)
                {
                    objTable.Dispose();
                    objTable = null;
                }
                if (objTableColumn != null)
                {
                    objTableColumn.Dispose();
                    objTableColumn = null;
                }
                if (objRichText != null)
                {
                    objRichText.Dispose();
                    objRichText = null;
                }
                arrlstProgressNotes = null;
            }
        }
        //Parijat : Stripping the HTML tags
        private string StripHTMLTags(string InString) //Parijat :19880 --Strip HTML tags and creating Plain text
        {
            //debugger;
            //alert(InString);
            string cleanedbuffer = "";
            string PatternString = "\\<[^<>]+\\>";
            Regex re = new Regex(PatternString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            cleanedbuffer = re.Replace(InString, "");//InString.Replace(re, "");

            //replace special characters
            PatternString = "(&nbsp;)|(&quot;)|(&ldquo;)|(&rdquo;)|(&lsquo;)|(&rsquo;)|(&bull;)";
            re = new Regex(PatternString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            cleanedbuffer = re.Replace(cleanedbuffer, " ");//cleanedbuffer.Replace(//gim, ' ');
            cleanedbuffer = cleanedbuffer.Replace("&amp;", "&").Replace(@"&lt;", "<").Replace(@"&gt;", ">");//Parijat: replacing &amp; with &--19297
            //alert(cleanedbuffer);
            return cleanedbuffer;
        }
        //Parijat:14970
        /// <summary>
        /// This method would print the Memo string.
        /// </summary>	
        /// <param name="p_sMemoString">Memo string</param>
        /// <param name="p_objTable">Table</param>
        /// <param name="p_iRowIndex">Row Index</param>
        private void PrintMemoString(string p_sMemoString, ref RenderTable p_objTable, ref int p_iRowIndex)
        {
            ////const int NOT_EOL = 0;
            ////const int NEXT_EOL = 1;
            ////const int THIS_EOL = 2;
            ////const int BOTH_EOL = 3;
            const int iCharPerLine = 85;

            string sRemainStr = "";
            ////string sNewStr = "";
            ////string sToPrint = "";
            string sConcat = "";
            ////int i = 0;
            ////int iPtr = 0;
            ////int iEOL = 0;

            //MITS 16892
            string[] strArr;
            string[] stringSeparators = new string[] { "\r\n" };

            try
            {
                #region Old Logic
                //sRemainStr = p_sMemoString;
                ////MITS 17095 : Umesh
                ////replacing &nbsp; by single space
                //sRemainStr = sRemainStr.Replace("&nbsp;", " ");
                //do
                //{
                //    iPtr = sRemainStr.IndexOf(" ");
                //    if (iPtr == -1)
                //        iPtr = sRemainStr.Length + 1;

                //    sNewStr = GetLeftString(sRemainStr, iPtr + 1);
                //    i = sNewStr.IndexOf(Microsoft.VisualBasic.Constants.vbCr);
                //    if (i > -1)
                //    {
                //        iPtr = i + 1;
                //        sNewStr = GetLeftString(sNewStr, i);
                //        if (iEOL == NOT_EOL)
                //            iEOL = NEXT_EOL;
                //        else
                //            iEOL = BOTH_EOL;
                //    }
                //    else
                //    {
                //        i = sNewStr.IndexOf("\n");
                //        if (i > -1)
                //        {                       
                //            iPtr = i;
                //            sNewStr = GetLeftString(sNewStr, i);
                //            if (iEOL == NOT_EOL)
                //                iEOL = NEXT_EOL;
                //            else
                //                iEOL = BOTH_EOL;
                //        }
                //    }
                //    sConcat = sToPrint + sNewStr;
                //    if ((sConcat.Length > iCharPerLine) || (iEOL == THIS_EOL) || (iEOL == BOTH_EOL))
                //    {
                //        //GeneratePrintString(GlobalExecSummConstant.CHARSTRING, sToPrint.Trim(), false, false);
                //        //m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                //        p_objTable.Body.Rows.AddSome(1);
                //        p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sToPrint.Trim();
                //        p_iRowIndex++;
                //        //m_arrlstReportData.Clear();
                //        sToPrint = sNewStr;
                //    }
                //    else
                //        sToPrint = sToPrint + sNewStr;
                //    sRemainStr = GetRemainingString(sRemainStr, iPtr + 1);
                //    if ((iEOL == NEXT_EOL) || (iEOL == BOTH_EOL))
                //        iEOL = THIS_EOL;
                //    else
                //        iEOL = NOT_EOL;
                //} while (sRemainStr != "");
                //if (sToPrint.Trim() != "")
                //{
                //    p_objTable.Body.Rows.AddSome(1);
                //    p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sToPrint.Trim();
                //    p_iRowIndex++;                    
                //    //GeneratePrintString(GlobalExecSummConstant.CHARSTRING, sToPrint.Trim(), false, false);
                //    //m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                //   // m_arrlstReportData.Clear();
                //}
                #endregion Old Logic

                //MITS 16892 New Logic
                sRemainStr = p_sMemoString;
                //sRemainStr = StripHTMLTags(sRemainStr);//Parijat Stripping the HTML tags
                //replacing &nbsp; by single space
                sRemainStr = sRemainStr.Replace("&nbsp;", " ");

                strArr = sRemainStr.Split(stringSeparators, StringSplitOptions.None);
                foreach (string sNewStr in strArr)
                {
                    string sLeftOver = string.Empty;

                    sConcat = sNewStr;
                    while (sConcat.Length > iCharPerLine)
                    {
                        if (sConcat.Substring(iCharPerLine, sConcat.Length - iCharPerLine).IndexOf(" ") == 0)
                        {
                            //In this case no word truncates
                            sLeftOver = string.Empty;

                            p_objTable.Body.Rows.AddSome(1);
                            p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Substring(0, iCharPerLine).Trim();
                            p_iRowIndex++;

                            sConcat = sConcat.Substring(iCharPerLine, sConcat.Length - iCharPerLine);
                        }
                        else
                        {
                            //Word truncates. Get the position of last space character in the string
                            int iPos = 0;
                            iPos = sConcat.Substring(0, iCharPerLine).LastIndexOf(" ");

                            if (iPos != -1)
                            {
                                sLeftOver = sConcat.Substring(iPos, sConcat.Length - iPos).Trim();
                                p_objTable.Body.Rows.AddSome(1);
                                p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Substring(0, iPos).Trim();
                                p_iRowIndex++;
                            }
                            else
                            {
                                sLeftOver = sConcat.Substring(iCharPerLine + 1, sConcat.Length - (iCharPerLine + 1)).Trim();
                                p_objTable.Body.Rows.AddSome(1);
                                p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Substring(0, iCharPerLine).Trim();
                                p_iRowIndex++;
                            }
                            sConcat = sLeftOver;
                        }
                    }
                    if (sConcat != "")
                    {
                        p_objTable.Body.Rows.AddSome(1);
                        if (sConcat.Trim() == "")
                            p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = " ";
                        else
                            p_objTable.Body.Cell(p_iRowIndex, 0).RenderText.Text = sConcat.Trim();
                        p_iRowIndex++;
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.PrintMemoString.Error", m_iClientId), p_objException);
            }
        }


        /// <summary>
        /// This method will return the specified number of characters from a string starting from Left.
        /// </summary>
        /// <param name="p_sInputStr">Input String</param>
        /// <param name="p_iNoOfChars">Number of Characters to be read from the left of the string</param>
        /// <returns>String</returns>
        private string GetLeftString(string p_sInputStr, int p_iNoOfChars)
        {
            int iStrLength = 0;
            try
            {
                iStrLength = p_sInputStr.Length;
                if (iStrLength < p_iNoOfChars)
                    p_sInputStr = p_sInputStr.Substring(0, iStrLength);
                else
                    p_sInputStr = p_sInputStr.Substring(0, p_iNoOfChars);
                return (p_sInputStr);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetLeftString.Error", m_iClientId), p_objException);
            }
        }
        /// <summary>
        /// This method will return the entire string starting from a specified character position.
        /// </summary>
        /// <param name="p_sInputStr">Input String</param>
        /// <param name="p_iPos">Start Character Position.</param>
        /// <returns>String</returns>
        private string GetRemainingString(string p_sInputStr, int p_iPos)
        {
            int iStrLength = 0;
            try
            {
                iStrLength = p_sInputStr.Length;
                if (iStrLength < p_iPos)
                    p_sInputStr = "";
                else
                    p_sInputStr = p_sInputStr.Substring(p_iPos);
                return (p_sInputStr);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetRemainingString.Error", m_iClientId), p_objException);
            }
        }
        #endregion

        #region OnLoad Notes

        struct ProgressNotes
        {
            private int m_iClaimProgressNoteId;
            private int m_iClaimId;
            private string m_sClaimNumber;
            private int m_iEventId;
            private string m_sEventNumber;
            private string m_sEnteredBy;
            private string m_sEnteredByName;
            private string m_sSubject;//zmohammad MITS 30218
            private string m_sDateEntered;
            private string m_sDateCreated;
            private string m_sTimeCreated;
            private int m_iNoteTypeCode;
            private string m_sNoteType;
            private int m_iUserTypeCode;
            private string m_sUserType;
            private string m_sNoteMemo;
            private string m_sNoteMemoCareTech;
            private string m_sAdjusterName;
            //nadim for Enhancement 12821
            private string m_sClaimant;
            //nadim MITS 12821
            //R6: Rakhi- Notepad Screens templates - start
            private string m_TemplateId;
            private string m_TemplateName;
            //Added by Amitosh for mits 23473(05/11/2011)
            private string m_sortBy;

            internal string SortBy
            {
                get { return m_sortBy; }
                set { m_sortBy = value; }
            }
            //end Amitosh
            internal string TemplateName
            {
                get { return m_TemplateName; }
                set { m_TemplateName = value; }
            }

            internal string TemplateId
            {
                get { return m_TemplateId; }
                set { m_TemplateId = value; }
            }
            //R6: Rakhi- Notepad Screens templates - end

            //williams-neha goel--start::MITS 21704
            private int m_iPolicyId;
            private string m_sPolicyName;
            private string m_sPolicyNumber;
            private string m_sCodeId;
            //skhare7 27285
            private string m_sPIName;
            internal string PolicyName
            {
                get { return m_sPolicyName; }
                set { m_sPolicyName = value; }
            }

            internal int PolicyId
            {
                get { return m_iPolicyId; }
                set { m_iPolicyId = value; }
            }
            internal string PolicyNumber
            {
                get { return m_sPolicyNumber; }
                set { m_sPolicyNumber = value; }
            }
            internal string CodeId
            {
                get { return m_sCodeId; }
                set { m_sCodeId = value; }
            }
            //skhare7 27285
            internal string PIName
            {
                get { return m_sPIName; }
                set { m_sPIName = value; }
            }
            //williams-neha goel--end::MITS 21704
            internal int ClaimProgressNoteId
            {
                get
                {
                    return m_iClaimProgressNoteId;
                }
                set
                {
                    m_iClaimProgressNoteId = value;
                }
            }
            internal int ClaimId
            {
                get
                {
                    return m_iClaimId;
                }
                set
                {
                    m_iClaimId = value;
                }
            }
            //nadim for Enhancement 12821
            internal string Claimant
            {
                get
                {
                    return m_sClaimant;
                }
                set
                {
                    m_sClaimant = value;
                }

            }
            //nadim  12821
            internal string ClaimNumber
            {
                get
                {
                    return m_sClaimNumber;
                }
                set
                {
                    m_sClaimNumber = value;
                }
            }
            internal int EventId
            {
                get
                {
                    return m_iEventId;
                }
                set
                {
                    m_iEventId = value;
                }
            }
            internal string EventNumber
            {
                get
                {
                    return m_sEventNumber;
                }
                set
                {
                    m_sEventNumber = value;
                }
            }
            internal string EnteredBy
            {
                get
                {
                    return m_sEnteredBy;
                }
                set
                {
                    m_sEnteredBy = value;
                }
            }
            internal string EnteredByName
            {
                get
                {
                    return m_sEnteredByName;
                }
                set
                {
                    m_sEnteredByName = value;
                }
            }
            //zmohammad MITS 30218
            internal string Subject
            {
                get
                {
                    return m_sSubject;
                }
                set
                {
                    m_sSubject = value;
                }
            }
            internal string DateEntered
            {
                get
                {
                    return m_sDateEntered;
                }
                set
                {
                    m_sDateEntered = value;
                }
            }
            internal string DateCreated
            {
                get
                {
                    return m_sDateCreated;
                }
                set
                {
                    m_sDateCreated = value;
                }
            }
            internal string TimeCreated
            {
                get
                {
                    return m_sTimeCreated;
                }
                set
                {
                    m_sTimeCreated = value;
                }
            }
            internal int NoteTypeCode
            {
                get
                {
                    return m_iNoteTypeCode;
                }
                set
                {
                    m_iNoteTypeCode = value;
                }
            }
            internal string NoteType
            {
                get
                {
                    return m_sNoteType;
                }
                set
                {
                    m_sNoteType = value;
                }
            }
            internal int UserTypeCode
            {
                get
                {
                    return m_iUserTypeCode;
                }
                set
                {
                    m_iUserTypeCode = value;
                }
            }
            internal string UserType
            {
                get
                {
                    return m_sUserType;
                }
                set
                {
                    m_sUserType = value;
                }
            }
            internal string NoteMemo
            {
                get
                {
                    return m_sNoteMemo;
                }
                set
                {
                    m_sNoteMemo = value;
                }
            }
            internal string NoteMemoCareTech
            {
                get
                {
                    return m_sNoteMemoCareTech;
                }
                set
                {
                    m_sNoteMemoCareTech = value;
                }
            }
            internal string AdjusterName
            {
                get
                {
                    return m_sAdjusterName;
                }
                set
                {
                    m_sAdjusterName = value;
                }
            }
        }
        ///MITS 11403
        /// Name		: DelteNote
        /// Author		: Abhishek Rathore
        /// Date Created: 12 March 2008
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method delete the select note
        /// </summary>
        /// 
        //p_sClaimantId Added by gbindra MITS#34104 WWIG GAP15 02122014
        //williams-neha goel:added for Policy::MITS 21704
        //public void DeleteNote(int p_iEventId, int p_iClaimId, int p_iClaimProgressNoteId)
        public void DeleteNote(int p_iEventId, int p_iClaimId, int p_iClaimProgressNoteId, int p_iPolicyId, int p_iClaimantId)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            SysSettings objSysSettings = null; //added by gbindra MITS#34104 WWIG GAP15
            try
            {
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId); //added by gbindra MITS#34104 WWIG GAP15
                /*added by gbindra MITS#34104 WWIG GAP15 02122014 START*/
                //williams-neha goel added for PolicyId--start:MITS 21704
                // sSQL = "DELETE FROM CLAIM_PRG_NOTE WHERE  CL_PROG_NOTE_ID = " + p_iClaimProgressNoteId +
                //" AND CLAIM_ID = " + p_iClaimId + " AND EVENT_ID = " + p_iEventId;
                if (p_iClaimantId > 0 && objSysSettings.AllowNotesAtClaimant == true)
                {
                    sSQL = "DELETE FROM CLAIM_PRG_NOTE WHERE  CL_PROG_NOTE_ID = " + p_iClaimProgressNoteId +
                            " AND EVENT_ID = " + p_iEventId + " AND POLICY_ID= " + p_iPolicyId +
                            " AND ATTACH_TABLE = 'CLAIMANT' AND ATTACH_RECORD_ID= " + Conversion.ConvertObjToStr(p_iClaimantId);
                }
                else
                {
                    sSQL = "DELETE FROM CLAIM_PRG_NOTE WHERE  CL_PROG_NOTE_ID = " + p_iClaimProgressNoteId +
                        " AND CLAIM_ID = " + p_iClaimId + " AND EVENT_ID = " + p_iEventId + " AND POLICY_ID= " + p_iPolicyId;
                }
                //williams-neha goel added for PolicyId--end:MITS 21704
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.DeleteNote.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }



        }
        /// Name		: GetNotes
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching notes
        /// </summary>
        /// 
        // p_sClaimantiD: Added by GBINDRA MITS#34104 02122014 WWIG GAP15
        //p_iNoteTypeCode:Added by Rakhi for R6-Print Note by Note Type
        //private ArrayList GetNotes( int p_iEventId , int p_iClaimId ,bool p_bActivateFilter , string p_sFilterSQL)
        //williams-neha goel:added int p_iPolicyId:MITS 21704
        //private ArrayList GetNotes(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, int p_iNoteTypeCode, string p_sSortColumn, string p_sDirection)
        private ArrayList GetNotes(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, int p_iNoteTypeCode, string p_sSortColumn, string p_sDirection, int p_iPolicyId, string p_sViewNotesOption, string p_sViewNotesTypeList, int p_iClaimantiD) //  mkaran2 - MITS 27038 - Added string p_sViewNotesOptionList, string p_sViewNotesTypeList
        {
            //rsolanki2: optimizations
            StringDictionary sdFullNames = new StringDictionary();
            string sFullName;
            string sEnteredByName;

            ProgressNotes structProgressNotes;

            //DbReader objReader = null;//Merge by kuladeep for mits:25103(From ExecutiveSummary)
            ArrayList arrlstConfidentialTypes = null;
            ArrayList arrlstSubOrdinateUsers = null;
            ArrayList arrlstProgressNotes = null;

            int iAdjusterId = 0;
            string sSQL = string.Empty;
            string sFrom = "CLAIM_PRG_NOTE";
            string sWhere = string.Empty;
            string sSQLRmUser = string.Empty;
            string sSQLUserLastName = string.Empty;
            string sSQLUserFirstName = string.Empty;
            string sSQLSubOrdinateUsers = string.Empty;
            string sSelect = string.Empty;
            string sAdjusterName = string.Empty;
            string sUserType = string.Empty;
            string sClaimCondition = string.Empty;
            string sNodeTypeCondition = string.Empty;
            //williams-neha goel--start:MITS 21704
            string sPolicyNumber = string.Empty;
            int iAttachedPolicy = 0;
            string sPolicyIds = string.Empty;//averma62
            //williams-neha goel--end:MITS 21704

            //nadim for Enhancement 12820                
            int iPrintOrder1 = 0;
            int iPrintOrder2 = 0;
            int iPrintOrder3 = 0;
            string sOrderBy = string.Empty;
            string sClaimant = string.Empty;
            SysSettings objSettings = null;
            //nadim 12820
            //Added by Shivendu for MITS 18098
            SearchResults objSearchResults = new SearchResults(m_sConnectionString,m_iClientId);//sonali
            //Aman ML Change
            int iLangCode = 0;
            int iBaseLangCode = 0;
            //Aman ML Change
            string sViewNotesAdvanceSearchCondition = string.Empty; // mkaran2 - MITS 27038 - View Notes on Advance Search Window
            try
            {
                arrlstProgressNotes = new ArrayList();
                arrlstConfidentialTypes = new ArrayList();

                // npadhy Jira 6415 Also pass userlogin object to Search results
                objSearchResults.UserLogin = m_UserLogin;
                //Aman ML Change
                iBaseLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                if (m_UserLogin != null)//asharma326 MITS 33716
                { iLangCode = m_UserLogin.objUser.NlsCode; }
                //Aman ML Change
                sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + LocalCache.GetTableId("NOTE_CNF_CODE");
                //Merge and Change by kuladeep for mits:25103(From ExecutiveSummary) 
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objReader != null)
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                        arrlstConfidentialTypes.Add(Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), m_iClientId));
                }
                //objReader.Close();
                //nadim for Enhancement 12820

                //williams-neha goel-07/21/2010--start:MITS 21704
                //Start averma62
                objSettings = new SysSettings(m_sConnectionString, m_iClientId);
                if (objSettings.MultiCovgPerClm == -1)
                {
                    sSQL = "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + p_iClaimId;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            if (string.IsNullOrEmpty(sPolicyIds))
                                sPolicyIds = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_ID"));
                            else
                                sPolicyIds = sPolicyIds + "," + Conversion.ConvertObjToStr(objReader.GetValue("POLICY_ID"));
                        }
                    }
                }
                else
                {
                    sSQL = "SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                            iAttachedPolicy = Conversion.ConvertObjToInt(objReader.GetValue("PRIMARY_POLICY_ID"), m_iClientId);
                    }
                }
                //End averma62

                //Start averma62 
                //sSQL = "SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                ////Merge and Change by kuladeep for mits:25103(From ExecutiveSummary)                  
                ////objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                ////if (objReader != null)
                //using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                //{
                //    while (objReader.Read())
                //        iAttachedPolicy = Conversion.ConvertObjToInt(objReader.GetValue("PRIMARY_POLICY_ID"), m_iClientId);
                //}
                //End averma62
                //objReader.Close();
                //williams-neha goel-07/21/2010--end:MITS 21704
                objSettings = new SysSettings(m_sConnectionString, m_iClientId);

                iPrintOrder1 = objSettings.EnhPrintOrder1;
                iPrintOrder2 = objSettings.EnhPrintOrder2;
                iPrintOrder3 = objSettings.EnhPrintOrder3;
                //nadim 12820
                //this.CreateJoinSQL( ref sFrom , ref sWhere , "CLAIM_PRG_NOTE" , "NOTE_X_SYS_CNF" , "NOTE_TYPE_CODE" , "NOTE_TYPE_ID" );

                //asharma326 add CLAIM_ADJUSTER.ADJUSTER_EID = ENTITY.ENTITY_ID  for handling ambigious column 
                sSQLRmUser = this.GetSQLWithFirstRow("CLAIM_ADJUSTER , ENTITY", "RM_USER_ID", "CLAIM_ADJUSTER.CLAIM_ID = CLAIM_PRG_NOTE.CLAIM_ID And CLAIM_ADJUSTER.ADJUSTER_EID = ENTITY.ENTITY_ID AND CURRENT_ADJ_FLAG = -1");


                // npadhy Reverting the changes of Umesh for MITS 15501. MITS 15501 was to optimize the query.
                // At that time we were not using ADJUSTER_LASTNAME, ADJUSTER_FIRSTNAME and USER_TYPE_DESC.
                // But now using the Advanced Search for Enhanced Notes, we can sort the results by 
                // ADJUSTER_LASTNAME, ADJUSTER_FIRSTNAME and USER_TYPE_DESC. So we need to have these in the Query as well.
                // So we are first checking whether the Notes are getting fetched on load or is it using the Advanced Search.
                // If the search is using the Advanced Search we are adding these columns to be fetched in the Query


                sSelect = " SELECT  CLAIM_PRG_NOTE.CL_PROG_NOTE_ID,	CLAIM_PRG_NOTE.CLAIM_ID,	CLAIM_PRG_NOTE.EVENT_ID, CLAIM_PRG_NOTE.POLICY_ID,	CLAIM_PRG_NOTE.ENTERED_BY,	CLAIM_PRG_NOTE.DATE_ENTERED,	CLAIM_PRG_NOTE.DATE_CREATED,	CLAIM_PRG_NOTE.TIME_CREATED,	CLAIM_PRG_NOTE.NOTE_TYPE_CODE,	CLAIM_PRG_NOTE.USER_TYPE_CODE,"


                   + "CLAIM_PRG_NOTE.NOTE_MEMO,	CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH ,	"



                   + "CLAIM_PRG_NOTE.ENTERED_BY_NAME,	CLAIM_PRG_NOTE.TEMPLATE_ID,	"
                   + "CLAIM_PRG_NOTE.SUBJECT,"//zmohammad MITS 30218
                    //R6: Rakhi:Notepad Screens Templates - start
                    + " (SELECT TEMPLATE_NAME FROM CLAIM_PRG_NOTE_TEMPLATES "
                    + " WHERE CLAIM_PRG_NOTE_TEMPLATES.CL_PRGN_TEMP_ID=CLAIM_PRG_NOTE.TEMPLATE_ID) TEMPLATE_NAME, "
                    //R6: Rakhi:Notepad Screens Templates - end

                    + " (" + sSQLRmUser + ")  CLAIM_ADJUSTER "
                    //MGaba2: 22186: Sorting on basis of NoteType was working on basis of Description rather short code
                    //+ ", (SELECT SHORT_CODE FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_SHORT_CODE"  //Aman ML Change
                    + ", (SELECT SHORT_CODE FROM CODES C WHERE C.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_SHORT_CODE"  //Aman ML Change
                    //  + ", (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_DESC";
                + ", ((SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + ") UNION (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iBaseLangCode + " AND CODE_ID NOT IN (SELECT CODE_ID FROM CODES_TEXT CT,CLAIM_PRG_NOTE WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + "))) NOTE_TYPE_DESC";                       //Aman ML Change
                //MITS 15501 : Umesh
                if (p_bActivateFilter && p_sFilterSQL.Contains("ADJUSTER_LASTNAME"))
                {
                    //rsushilaggar - Removed entity_table_id is Entity Role is ON.
                    if (objSettings.UseEntityRole)
                    {
                        sSQLUserLastName = this.GetSQLWithFirstRow("ENTITY", "LAST_NAME", " RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                        sSQLUserFirstName = this.GetSQLWithFirstRow("ENTITY", "FIRST_NAME", " RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                    }
                    else
                    {
                        sSQLUserLastName = this.GetSQLWithFirstRow("ENTITY", "LAST_NAME", "ENTITY_TABLE_ID=" + LocalCache.GetTableId("ADJUSTERS") + " AND RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                        sSQLUserFirstName = this.GetSQLWithFirstRow("ENTITY", "FIRST_NAME", "ENTITY_TABLE_ID=" + LocalCache.GetTableId("ADJUSTERS") + " AND RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                    }
                    //MITS 15501 : End
                    sSelect += ", (" + sSQLUserLastName + ") ADJUSTER_LASTNAME "
                    + ", (" + sSQLUserFirstName + ") ADJUSTER_FIRSTNAME ";
                }
                // The following commented out section causes performance issue in BES by kuladeep for mits:25103(From ExecutiveSummary)
                //if (p_bActivateFilter && p_sFilterSQL.Contains("USER_TYPE_DESC"))
                //{
                //sSelect += ", ( SELECT GROUP_NAME FROM	  "
                //                + " (SELECT C.USER_TYPE_CODE,'SYSTEM GENERATED'  GROUP_NAME"
                //                + " FROM CLAIM_PRG_NOTE C,USER_GROUPS"
                //                + " WHERE  C.USER_TYPE_CODE =0"
                //                + " UNION "
                //                + " SELECT C.USER_TYPE_CODE,USER_GROUPS.GROUP_NAME  GROUP_NAME"
                //                + " FROM CLAIM_PRG_NOTE C,USER_GROUPS"
                //                + " WHERE USER_GROUPS.GROUP_ID=C.USER_TYPE_CODE "
                //                + " ) X"
                //                + " WHERE X.USER_TYPE_CODE=CLAIM_PRG_NOTE.USER_TYPE_CODE"
                //        + " )  USER_TYPE_DESC";
                //}
                sSelect += ", CLAIM_PRG_NOTE.ENTERED_BY_NAME ENTERED_BY_NAME "
                        + " FROM ";
                //Added by Rakhi for R6-Print Note By Note Type
                if (p_iNoteTypeCode > 0)
                {
                    sNodeTypeCondition = " AND CLAIM_PRG_NOTE.NOTE_TYPE_CODE = " + p_iNoteTypeCode;
                }
                //Added by Rakhi for R6-Print Note By Note Type
                if (p_bActivateFilter == false && string.Compare(p_sSortColumn, sNoSort) == 0 && p_sDirection == null && iPrintOrder1 == 0 && iPrintOrder2 == 0 && iPrintOrder3 == 0)
                {
                    p_sSortColumn = "DateEntered";
                    p_sDirection = " DESC";
                }
                //
                //mkaran2 - MITS 27038 - View Notes on Advance Search window
                if (!string.IsNullOrEmpty(p_sViewNotesOption) && Convert.ToInt32(p_sViewNotesOption) > 0) // p_sViewNotesTypeList =0 or null is skipped to show all notes
                {
                    if (!string.IsNullOrEmpty(p_sViewNotesTypeList))
                    {
                        if (Convert.ToInt32(p_sViewNotesOption) == 1) // View Selected Notes
                            sViewNotesAdvanceSearchCondition = " AND CLAIM_PRG_NOTE.NOTE_TYPE_CODE IN (" + p_sViewNotesTypeList + ")";

                        if (Convert.ToInt32(p_sViewNotesOption) == 2) // // View Unselected Notes
                            sViewNotesAdvanceSearchCondition = " AND CLAIM_PRG_NOTE.NOTE_TYPE_CODE NOT IN (" + p_sViewNotesTypeList + ")";
                    }
                }
                //
                if (p_bActivateFilter == false)
                {
                    //p_iClaimantiD check added by swati for MITS # 35530 Gap 15 WWIG
                    //if (p_iClaimId > 0 && p_iClaimantiD < 0)
                    if (p_iClaimId > 0 && p_iClaimantiD <= 0) // added by gbindra for MITS # 35530 Gap 15 WWIG notes change handling 
                    {
                        //nadim For Enhancement 12820
                        if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                        {
                            if (iPrintOrder1 != 0)
                            {
                                switch (iPrintOrder1)
                                {
                                    case 1:
                                        sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                        break;
                                    case 2:
                                        sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                        break;
                                    case 3:
                                        sOrderBy = "NOTE_TYPE_SHORT_CODE" + ",";
                                        break;

                                }
                            }
                            if (iPrintOrder2 != 0)
                            {
                                switch (iPrintOrder2)
                                {
                                    case 1:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                        break;
                                    case 2:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                        break;
                                    case 3:
                                        sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE" + ",";
                                        break;

                                }
                            }
                            if (iPrintOrder3 != 0)
                            {
                                switch (iPrintOrder3)
                                {
                                    case 1:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                        break;
                                    case 2:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                        break;
                                    case 3:
                                        sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE";
                                        break;

                                }
                            }

                        }
                        else
                            sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                        if (sOrderBy.EndsWith(","))
                            sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                        //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                        if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                        {
                            GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 0);

                        }
                        //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                        //williams-neha goel:added to view policy notes in claim based on utility setting--start 07/21/2010: :MITS 21704
                        //Start - averma62
                        if (objSettings.EnhNotesPolicyClaimView)
                        {
                            if (!string.IsNullOrEmpty(sPolicyIds))
                            {
                                sSQL = sSelect + sFrom + " WHERE (CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                                   + " OR CLAIM_PRG_NOTE.POLICY_ID IN (" + sPolicyIds + "))"
                                   + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " // added by gbindra for MITS # 35530 Gap 15 WWIG notes change handling 
                                       + sNodeTypeCondition
                                       + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                       + " ORDER BY " + sOrderBy;
                            }
                            else if (iAttachedPolicy != 0)
                            {
                                sSQL = sSelect + sFrom + " WHERE (CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                                    + " OR CLAIM_PRG_NOTE.POLICY_ID=" + iAttachedPolicy + ")"
                                    + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " // added by gbindra for MITS # 35530 Gap 15 WWIG notes change handling 
                                    //+	" AND " + sWhere
                                        + sNodeTypeCondition
                                        + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                        + " ORDER BY " + sOrderBy;
                            }
                            else
                            {
                                sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                                    + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " // added by gbindra for MITS # 35530 Gap 15 WWIG notes change handling 
                                    //+	" AND " + sWhere
                                + sNodeTypeCondition
                                + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                + " ORDER BY " + sOrderBy;
                            }

                        }
                        else
                        {
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                                + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " // added by gbindra for MITS # 35530 Gap 15 WWIG notes change handling 
                                //+	" AND " + sWhere
                                + sNodeTypeCondition
                                + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                + " ORDER BY " + sOrderBy;
                        }

                        //if (objSettings.EnhNotesPolicyClaimView && iAttachedPolicy != 0)
                        //{
                        //    sSQL = sSelect + sFrom + " WHERE (CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                        //        + " OR CLAIM_PRG_NOTE.POLICY_ID=" + iAttachedPolicy + ")"
                        //        //+	" AND " + sWhere
                        //            + sNodeTypeCondition
                        //            + " ORDER BY " + sOrderBy;
                        //}
                        //else  //williams-neha goel--end:MITS 21704
                        //{
                        //    sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId 
                        //        //+	" AND " + sWhere
                        //            + sNodeTypeCondition
                        //            + " ORDER BY " + sOrderBy;
                        //}
                        //nadim 12820
                        //End - averma62
                    }
                    else
                    {
                        /*Added by GBINDRA MITS#34104 02122014 WWIG GAP15 START*/
                        if (p_iClaimantiD > 0 && objSettings.AllowNotesAtClaimant == true)
                        {
                            if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                            {
                                if (iPrintOrder1 != 0)
                                {
                                    switch (iPrintOrder1)
                                    {
                                        case 1:
                                            sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                            break;
                                        case 2:
                                            sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                            break;
                                        case 3:
                                            sOrderBy = "NOTE_TYPE_SHORT_CODE" + ",";
                                            break;

                                    }
                                }
                                if (iPrintOrder2 != 0)
                                {
                                    switch (iPrintOrder2)
                                    {
                                        case 1:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                            break;
                                        case 2:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                            break;
                                        case 3:
                                            sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE" + ",";
                                            break;

                                    }
                                }
                                if (iPrintOrder3 != 0)
                                {
                                    switch (iPrintOrder3)
                                    {
                                        case 1:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                            break;
                                        case 2:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                            break;
                                        case 3:
                                            sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE";
                                            break;

                                    }
                                }

                            }
                            else
                                sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                            if (sOrderBy.EndsWith(","))
                                sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                            //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                            if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                            {
                                GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 1);

                            }
                            //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId +
                                " AND CLAIM_PRG_NOTE.ATTACH_RECORD_ID= " + p_iClaimantiD +
                                " AND CLAIM_PRG_NOTE.ATTACH_TABLE = 'CLAIMANT' "
                                //+	" AND " +	sWhere 
                                  + sNodeTypeCondition
                                  + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                        + " ORDER BY " + sOrderBy;
                        }
                        //if (p_iEventId > 0)
                        else if (p_iEventId > 0)
                        /*Added by GBINDRA MITS#34104 02122014 WWIG GAP15 END*/
                        {
                            //nadim for Enhancement 12820
                            if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                            {
                                if (iPrintOrder1 != 0)
                                {
                                    switch (iPrintOrder1)
                                    {
                                        case 1:
                                            sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                            break;
                                        case 2:
                                            sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                            break;
                                        case 3:
                                            sOrderBy = "NOTE_TYPE_SHORT_CODE" + ",";
                                            break;

                                    }
                                }
                                if (iPrintOrder2 != 0)
                                {
                                    switch (iPrintOrder2)
                                    {
                                        case 1:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                            break;
                                        case 2:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                            break;
                                        case 3:
                                            sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE" + ",";
                                            break;

                                    }
                                }
                                if (iPrintOrder3 != 0)
                                {
                                    switch (iPrintOrder3)
                                    {
                                        case 1:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                            break;
                                        case 2:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                            break;
                                        case 3:
                                            sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE";
                                            break;

                                    }
                                }

                            }
                            else
                                sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                            if (sOrderBy.EndsWith(","))
                                sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                            //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                            if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                            {
                                GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 1);

                            }
                            //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId +
                                " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 02132014 WWIG GAP15
                                //+	" AND " +	sWhere 
                                  + sNodeTypeCondition
                                    + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                        + " ORDER BY " + sOrderBy;
                        }
                        //williams-neha goel:added forpolicy--start:MITS 21704
                        else
                        {
                            if (p_iPolicyId > 0)
                            {
                                //nadim for Enhancement 12820
                                if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                                {
                                    if (iPrintOrder1 != 0)
                                    {
                                        switch (iPrintOrder1)
                                        {
                                            case 1:
                                                sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                                break;
                                            case 2:
                                                sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                                break;
                                            case 3:
                                                sOrderBy = "NOTE_TYPE_DESC" + ",";
                                                break;

                                        }
                                    }
                                    if (iPrintOrder2 != 0)
                                    {
                                        switch (iPrintOrder2)
                                        {
                                            case 1:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                                break;
                                            case 2:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                                break;
                                            case 3:
                                                sOrderBy = sOrderBy + "NOTE_TYPE_DESC" + ",";
                                                break;

                                        }
                                    }
                                    if (iPrintOrder3 != 0)
                                    {
                                        switch (iPrintOrder3)
                                        {
                                            case 1:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                                break;
                                            case 2:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                                break;
                                            case 3:
                                                sOrderBy = sOrderBy + "NOTE_TYPE_DESC";
                                                break;

                                        }
                                    }

                                }
                                else
                                    sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                if (sOrderBy.EndsWith(","))
                                    sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                                //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                                if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                                {
                                    GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 3);

                                }
                                //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                                sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.POLICY_ID=" + p_iPolicyId +
                                    " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                    " AND (CLAIM_PRG_NOTE.EVENT_ID IS NULL OR CLAIM_PRG_NOTE.EVENT_ID = 0) " +
                                    " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                    //+	" AND " +	sWhere 
                                      + sNodeTypeCondition
                                        + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                            + " ORDER BY " + sOrderBy;
                            }
                        }
                        //williams-neha goel---end:MITS 21704
                    }
                    //nadim 12820
                }
                else
                {
                    //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                    if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                    {
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.LastIndexOf("ORDER BY") > -1)
                            {
                                p_sFilterSQL = p_sFilterSQL.Remove(p_sFilterSQL.LastIndexOf("ORDER BY"));
                            }

                            GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 2);
                            p_sFilterSQL = p_sFilterSQL + " ORDER BY " + sOrderBy;
                        }
                    }
                    //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                    if (p_iClaimId > 0)
                    {
                        //Start averma62 - MITS 27787
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Substring(5, 11) == "CLAIM_ID IN")
                            {
                                sClaimCondition = " ";
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("CLAIM_ID IN", "CLAIM_PRG_NOTE.CLAIM_ID IN");
                                }
                            }
                            else
                                sClaimCondition = " AND CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId.ToString();
                        }

                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += " CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }

                        //End by Shivendu for MITS 18098
                        if (sNodeTypeCondition != string.Empty)
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                            + sClaimCondition
                            + sNodeTypeCondition//skhare7 MITS 27287
                            + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                            + p_sFilterSQL;
                        else
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                                + sClaimCondition
                                + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                + p_sFilterSQL;
                    }
                    /*Added by GBINDRA MITS#34104 02122014 WWIG GAP15 START*/
                    else if (p_iClaimantiD > 0 && objSettings.AllowNotesAtClaimant == true)
                    {
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Contains("ATTACH_TABLE IN"))
                            {
                                sClaimCondition = " ";
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("ATTACH_TABLE IN", "CLAIM_PRG_NOTE.ATTACH_TABLE IN");
                                }
                                if (p_sFilterSQL.Contains("ATTACH_RECORD_ID IN"))
                                {
                                    if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                    {
                                        p_sFilterSQL = p_sFilterSQL.Replace("ATTACH_RECORD_ID IN", "CLAIM_PRG_NOTE.ATTACH_RECORD_ID IN");
                                    }
                                }
                            }
                            else
                                sClaimCondition = " AND CLAIM_PRG_NOTE.ATTACH_TABLE = 'CLAIMANT'  ";
                        }
                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition == " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += "CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "EVENT", "EVENT_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "EVENT.EVENT_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        //End by Shivendu for MITS 18098
                        if (sNodeTypeCondition != string.Empty)
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                           + " AND CLAIM_PRG_NOTE.ATTACH_RECORD_ID=" + p_iClaimantiD
                           + sClaimCondition
                           + sNodeTypeCondition//skhare7 MITS 27287
                                + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                                + p_sFilterSQL;
                        else
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                            + " AND CLAIM_PRG_NOTE.ATTACH_RECORD_ID=" + p_iClaimantiD
                            + sClaimCondition
                            + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                            + p_sFilterSQL;
                    }
                    /*Added by GBINDRA MITS#34104 02122014 WWIG GAP15 END*/
                    else if (p_iEventId > 0)
                    {
                        //Start averma62 - MITS 27787
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Substring(5, 11) == "CLAIM_ID IN")
                            {
                                sClaimCondition = " ";
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("CLAIM_ID IN", "CLAIM_PRG_NOTE.CLAIM_ID IN");
                                }

                            }
                            else
                                sClaimCondition = " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                                  " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) ";//Added by GBINDRA MITS#34104 WWIG GAP15
                        }
                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition == " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += "CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "EVENT", "EVENT_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "EVENT.EVENT_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        //End by Shivendu for MITS 18098
                        if (sNodeTypeCondition != string.Empty)
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                           + sClaimCondition
                              + sNodeTypeCondition//skhare7 MITS 27287
                                + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                           + p_sFilterSQL;

                        else
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                            + sClaimCondition
                              + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                            + p_sFilterSQL;


                    }
                    //williams-neha goel---start:MITS 21704
                    else if (p_iPolicyId > 0)
                    {
                        //Start averma62 - MITS 27787
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Substring(5, 11) == "CLAIM_ID IN")
                            {
                                sClaimCondition = " ";
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("CLAIM_ID IN", "CLAIM_PRG_NOTE.CLAIM_ID IN");
                                }

                            }
                            else
                                sClaimCondition = " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                                  " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) ";//Added by GBINDRA MITS#34104 WWIG GAP15
                        }
                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition == " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += "CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "EVENT", "EVENT_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "EVENT.EVENT_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "POLICY", "POLICY_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "POLICY.POLICY_NAME" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        //End by Shivendu for MITS 18098
                        sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.POLICY_ID=" + p_iPolicyId
                            + sClaimCondition
                              + sViewNotesAdvanceSearchCondition // mkaran2- MITS 27038
                            + p_sFilterSQL;
                    }
                    //williams-neha goel---end:MITS 21704

                }



                // Get the Sub Ordinalte Users list, Find subordinates in security db.
                arrlstSubOrdinateUsers = new ArrayList();
                sSQLSubOrdinateUsers = "SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID = " + m_objDataModelFactory.Context.RMUser.UserId;
                //Merge and Change by kuladeep for mits:25103(From ExecutiveSummary) 
                //objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), sSQLSubOrdinateUsers);
                //if (objReader != null)
                using (DbReader objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), sSQLSubOrdinateUsers))
                {
                    while (objReader.Read())
                        arrlstSubOrdinateUsers.Add(Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId));
                }
                //objReader.Close();
                //Merge and Change by kuladeep for mits:25103(From ExecutiveSummary) 
                //use a dictionary object to cache GROUP_NAME
                Dictionary<int, string> dicGroupName = new Dictionary<int, string>();
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objReader != null)
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        //iSysTypeId = Conversion.ConvertObjToInt( objReader.GetValue( "SYS_TYPE_ID" ), m_iClientId );
                        iAdjusterId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ADJUSTER"), m_iClientId);

                        //if( !(arrlstConfidentialTypes.Contains( iSysTypeId ) && arrlstSubOrdinateUsers.Contains(iAdjusterId)) )
                        // JP 9-26-2006:   This was a bug to begin with. It was locking the adjuster's manager 
                        //                 *out* of claims for that adjuster instead of the other way around.
                        //                 HOWEVER, fixing this particular security check would only cause
                        //                 more confusion since most clients don't have appropriate manager/adjuster
                        //                 relationships setup. So I am disabling the check.
                        // JP 9-26-2006    if( !arrlstSubOrdinateUsers.Contains(iAdjusterId) )
                        {
                            structProgressNotes = new ProgressNotes();

                            structProgressNotes.ClaimProgressNoteId = Conversion.ConvertObjToInt(objReader.GetValue("CL_PROG_NOTE_ID"), m_iClientId);
                            structProgressNotes.ClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId);
                            structProgressNotes.ClaimNumber = this.GetClaimName(structProgressNotes.ClaimId);
                            structProgressNotes.EventId = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), m_iClientId);
                            structProgressNotes.EventNumber = this.GetEventName(structProgressNotes.EventId);
                            //williams-nehagoel-start:MITS 21704
                            structProgressNotes.PolicyId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_ID"), m_iClientId);
                            structProgressNotes.PolicyName = this.GetPolicyName(structProgressNotes.PolicyId, ref sPolicyNumber);
                            structProgressNotes.PolicyNumber = sPolicyNumber;
                            //structProgressNotes.CodeId = this.GetCodeID(structProgressNotes.PolicyId,structProgressNotes.EventId,structProgressNotes.ClaimId);
                            //williams-nehagoel-end:MITS 21704
                            structProgressNotes.EnteredBy = objReader.GetString("ENTERED_BY");
                            //Raman 02/10/2010: We need to get the full name instead of loginname
                            sFullName = string.Empty;
                            sEnteredByName = objReader.GetString("ENTERED_BY_NAME");
                            //rsolanki2: optimizations  
                            if (sdFullNames.ContainsKey(sEnteredByName))
                            {
                                structProgressNotes.EnteredByName = sdFullNames[sEnteredByName];
                            }
                            else
                            {
                                if (PublicFunctions.GetFullNameFromLoginName(sEnteredByName, out sFullName, m_iClientId))
                                {
                                    structProgressNotes.EnteredByName = sFullName;
                                    sdFullNames.Add(sEnteredByName, sFullName);
                                }
                                else
                                {
                                    structProgressNotes.EnteredByName = sEnteredByName;
                                    sdFullNames.Add(sEnteredByName, sEnteredByName);
                                }
                            }

                            //rsolanki2 : mits 23101: we are now saving the full name in the "Entered by" column, thereby we dont need to 
                            //the full name at runtime for every row.
                            //structProgressNotes.EnteredByName = objReader.GetString("ENTERED_BY_NAME");

                            //structProgressNotes.EnteredByName = objReader.GetString("ENTERED_BY_NAME");
                            structProgressNotes.DateEntered = Conversion.GetDBDateFormat(objReader.GetString("DATE_ENTERED"), "d");
                            structProgressNotes.DateCreated = Conversion.GetDBDateFormat(objReader.GetString("DATE_CREATED"), "d");
                            structProgressNotes.TimeCreated = Conversion.GetDBTimeFormat(objReader.GetString("TIME_CREATED"), "T");
                            structProgressNotes.NoteTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("NOTE_TYPE_CODE"), m_iClientId);
                            //MGaba2: MITS 22184: Note Type was not getting updated
                            //    structProgressNotes.NoteType = LocalCache.GetShortCode(structProgressNotes.NoteTypeCode) + " " + LocalCache.GetCodeDesc(structProgressNotes.NoteTypeCode);
                            structProgressNotes.NoteType = objReader.GetString("NOTE_TYPE_SHORT_CODE") + " " + objReader.GetString("NOTE_TYPE_DESC");

                            structProgressNotes.UserTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("USER_TYPE_CODE"), m_iClientId);
                            //Merge by kuladeep for mits:25103(From ExecutiveSummary) Start
                            //if (structProgressNotes.UserTypeCode != 0)
                            //{
                            //    sSQL = " SELECT GROUP_NAME from USER_GROUPS WHERE GROUP_ID =" + structProgressNotes.UserTypeCode;
                            //    //MITS 12838 : Umes        //nadim for 13396
                            //    using (DbReader objReaderUser = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                            //    {
                            //        if (objReaderUser.Read())
                            //        {
                            //            structProgressNotes.UserType = objReaderUser.GetString("GROUP_NAME");
                            //            if (structProgressNotes.UserType.IndexOf("'") > -1)
                            //            {
                            //                structProgressNotes.UserType = structProgressNotes.UserType.Replace("'", "");
                            //            }
                            //        }
                            //        else
                            //            structProgressNotes.UserType = "";
                            //        //nadim for 13396
                            //    }
                            //}
                            //else
                            //    structProgressNotes.UserType = "System Generated";
                            if (dicGroupName.ContainsKey(structProgressNotes.UserTypeCode))
                            {
                                structProgressNotes.UserType = dicGroupName[structProgressNotes.UserTypeCode];
                            }
                            else
                            {
                                if (structProgressNotes.UserTypeCode != 0)
                                {
                                    sSQL = " SELECT GROUP_NAME from USER_GROUPS WHERE GROUP_ID =" + structProgressNotes.UserTypeCode;
                                    //MITS 12838 : Umes        //nadim for 13396
                                    using (DbReader objReaderUser = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                    {
                                        if (objReaderUser.Read())
                                        {
                                            structProgressNotes.UserType = objReaderUser.GetString("GROUP_NAME");
                                            if (structProgressNotes.UserType.IndexOf("'") > -1)
                                            {
                                                structProgressNotes.UserType = structProgressNotes.UserType.Replace("'", "");
                                            }
                                        }
                                        else
                                            structProgressNotes.UserType = "";
                                        //nadim for 13396
                                    }
                                }
                                else
                                    structProgressNotes.UserType = "System Generated";

                                dicGroupName.Add(structProgressNotes.UserTypeCode, structProgressNotes.UserType);
                            }
                            //Merge by kuladeep for mits:25103(From ExecutiveSummary) End
                            //zmohammad MITS 31046
                            structProgressNotes.Subject = objReader.GetString("SUBJECT");
                            structProgressNotes.NoteMemo = objReader.GetString("NOTE_MEMO");
                            structProgressNotes.NoteMemoCareTech = objReader.GetString("NOTE_MEMO_CARETECH");

                            // rsolanki2 : we have been assuming that the enteredby name is the same as adjuster. However it 
                            // can be different for some clients. right now, just equating them directly. 
                            // it should be taken care of as part of mits 20537
                            structProgressNotes.AdjusterName = structProgressNotes.EnteredByName;

                            ////Raman 02/10/2010: We need to get the full name instead of loginname
                            //sFullName = string.Empty;
                            //if (PublicFunctions.GetFullNameFromLoginName(objReader.GetString("ENTERED_BY_NAME"), out sFullName))
                            //{
                            //    structProgressNotes.AdjusterName = sFullName;
                            //}
                            //else
                            //{
                            //    structProgressNotes.AdjusterName = objReader.GetString("ENTERED_BY_NAME");
                            //}
                            //structProgressNotes.AdjusterName = objReader.GetString("ENTERED_BY_NAME");

                            /*structProgressNotes.AdjusterName = objReader.GetString( "ADJUSTER_FIRSTNAME" );
                            if( structProgressNotes.AdjusterName.Trim() != "" )
                                structProgressNotes.AdjusterName = objReader.GetString( "ADJUSTER_LASTNAME" ) + ", " + structProgressNotes.AdjusterName ;
                            else
                                structProgressNotes.AdjusterName = objReader.GetString( "ADJUSTER_LASTNAME" ) ;*/
                            //R6: Rakhi-Notepad Screens Templates - start
                            structProgressNotes.TemplateId = objReader.GetValue("TEMPLATE_ID").ToString();
                            structProgressNotes.TemplateName = objReader.GetValue("TEMPLATE_NAME").ToString();
                            //R6: Rakhi - Notepad Screens Templates - end

                            arrlstProgressNotes.Add(structProgressNotes);
                        }
                    }
                }
                return (arrlstProgressNotes);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetNotes.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //Comment by kuladeep for mits:25103(From ExecutiveSummary)
                //if( objReader != null )
                //{
                //    objReader.Close();
                //    objReader.Dispose();
                //}

                if (objSearchResults != null)
                {
                    objSearchResults = null;


                }

                //nadim  12820
                objSettings = null;
                sOrderBy = null;
                //nadim  12820
                arrlstConfidentialTypes = null;
                arrlstSubOrdinateUsers = null;
            }
        }
        //rsolanki2: Enhc Notes ajax updates

        /// <summary>
        /// Get notes for one page only rather than all the notes to improve performance
        /// MITS 25148 
        /// Claimant Id added by gbindra mits#34104 02112014 wwig gap15
        /// </summary>
        /// <param name="p_iEventId">Event Id</param>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_bActivateFilter">If filter is activated</param>
        /// <param name="p_sFilterSQL">SQL statement for filter</param>
        /// <param name="p_iNoteTypeCode">Note type code</param>
        /// <param name="p_sSortColumn">Sort column name</param>
        /// <param name="p_sDirection">Sort direction</param>
        /// <returns></returns>
        private ArrayList GetNotesPartial(int p_iEventId, int p_iClaimId, bool p_bActivateFilter,
                                            string p_sFilterSQL, int p_iNoteTypeCode, string p_sSortColumn,
                                            string p_sDirection, int p_iStartIndex, int p_iEndIndex, ref int p_iTotalCount, int p_iPolicyId, bool b_IsMobileAdjuster, bool b_IsMobilityAdjuster, int p_iClaimantId)
        {
            //rsolanki2: optimizations
            StringDictionary sdFullNames = new StringDictionary();
            string sFullName;
            string sEnteredByName;

            ProgressNotes structProgressNotes;

            //DbReader objReader = null; //Change by kuladeep for mits:25103(From ExecutiveSummary)
            ArrayList arrlstConfidentialTypes = null;
            ArrayList arrlstSubOrdinateUsers = null;
            ArrayList arrlstProgressNotes = null;

            int iAdjusterId = 0;
            string sSQL = string.Empty;
            string sSQLCount = string.Empty;
            string sFrom = "CLAIM_PRG_NOTE";
            string sWhere = string.Empty;
            string sSQLRmUser = string.Empty;
            string sSQLUserLastName = string.Empty;
            string sSQLUserFirstName = string.Empty;
            string sSQLSubOrdinateUsers = string.Empty;
            string sSelect = string.Empty;
            string sAdjusterName = string.Empty;
            string sUserType = string.Empty;
            string sClaimCondition = string.Empty;
            string sNodeTypeCondition = string.Empty;
            //williams-neha goel--start:MITS 21704
            string sPolicyNumber = string.Empty;
            int iAttachedPolicy = 0;
            //williams-neha goel--end:MITS 21704
            //nadim for Enhancement 12820                
            int iPrintOrder1 = 0;
            int iPrintOrder2 = 0;
            int iPrintOrder3 = 0;
            string sOrderBy = string.Empty;
            string sClaimant = string.Empty;
            SysSettings objSettings = null;
            //nadim 12820
            //Added by Shivendu for MITS 18098
            SearchResults objSearchResults = new SearchResults(m_sConnectionString,m_iClientId);//sonali
            string sPolicyIds = string.Empty;
            string sESql = string.Empty; //averma62 Mits 28988
            int iLangCode = 0;  //Aman ML Change
            int iBaseLangCode = 0;
            try
            {
                // npadhy Jira 6415 Also pass userlogin object to Search results
                objSearchResults.UserLogin = m_UserLogin;
                iBaseLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                iLangCode = m_UserLogin.objUser.NlsCode;  //Aman ML Change
                if (iLangCode == 0)
                    iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                //Start averma62 Mits 28988 
                if (p_iClaimId == 0 && p_iEventId == 0 && p_iPolicyId == 0)
                {
                    sESql = @"select event_id, CASE_MANAGEMENT.DTTM_RCD_ADDED,CASE_MANAGEMENT.ADDED_BY_USER from CM_X_ACCOMMODATION ,CASE_MANAGEMENT,PERSON_INVOLVED
                    where CM_X_ACCOMMODATION.CM_ROW_ID=CASE_MANAGEMENT.CASEMGT_ROW_ID and 
                    CASE_MANAGEMENT.PI_ROW_ID=PERSON_INVOLVED.PI_ROW_ID AND CASE_MANAGEMENT.CASEMGT_ROW_ID = " + this.iCaseManagement + "";

                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sESql))
                    {
                        while (objReader.Read())
                            p_iEventId = Conversion.ConvertObjToInt(objReader.GetValue("event_id"), m_iClientId);
                    }
                }
                // End averma62 Mits 28988

                arrlstProgressNotes = new ArrayList();
                arrlstConfidentialTypes = new ArrayList();

                sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + LocalCache.GetTableId("NOTE_CNF_CODE");
                //Merge and Change by kuladeep for mits:25103(From ExecutiveSummary)
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objReader != null)
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                        arrlstConfidentialTypes.Add(Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), m_iClientId));
                }
                //objReader.Close();
                //nadim for Enhancement 12820

                //williams-neha goel-07/21/2010--start:MITS 21704
                objSettings = new SysSettings(m_sConnectionString, m_iClientId);
                if (objSettings.MultiCovgPerClm == -1)
                {
                    sSQL = "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID = " + p_iClaimId;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            if (string.IsNullOrEmpty(sPolicyIds))
                                sPolicyIds = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_ID"));
                            else
                                sPolicyIds = sPolicyIds + "," + Conversion.ConvertObjToStr(objReader.GetValue("POLICY_ID"));
                        }
                    }
                }
                else
                {
                    sSQL = "SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                    //Merge and Change by kuladeep for mits:25103(From ExecutiveSummary)
                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    //if (objReader != null)
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                            iAttachedPolicy = Conversion.ConvertObjToInt(objReader.GetValue("PRIMARY_POLICY_ID"), m_iClientId);
                    }
                }
                //objReader.Close();
                //williams-neha goel-07/21/2010--end:MITS 21704
                //objSettings = new SysSettings(m_sConnectionString);

                iPrintOrder1 = objSettings.EnhPrintOrder1;
                iPrintOrder2 = objSettings.EnhPrintOrder2;
                iPrintOrder3 = objSettings.EnhPrintOrder3;
                //nadim 12820
                //this.CreateJoinSQL( ref sFrom , ref sWhere , "CLAIM_PRG_NOTE" , "NOTE_X_SYS_CNF" , "NOTE_TYPE_CODE" , "NOTE_TYPE_ID" );

                //asharma326 add CLAIM_ADJUSTER.ADJUSTER_EID = ENTITY.ENTITY_ID  for handling ambigious column 
                sSQLRmUser = this.GetSQLWithFirstRow("CLAIM_ADJUSTER , ENTITY", "RM_USER_ID", "CLAIM_ADJUSTER.CLAIM_ID = CLAIM_PRG_NOTE.CLAIM_ID And CLAIM_ADJUSTER.ADJUSTER_EID = ENTITY.ENTITY_ID AND CURRENT_ADJ_FLAG = -1");


                // npadhy Reverting the changes of Umesh for MITS 15501. MITS 15501 was to optimize the query.
                // At that time we were not using ADJUSTER_LASTNAME, ADJUSTER_FIRSTNAME and USER_TYPE_DESC.
                // But now using the Advanced Search for Enhanced Notes, we can sort the results by 
                // ADJUSTER_LASTNAME, ADJUSTER_FIRSTNAME and USER_TYPE_DESC. So we need to have these in the Query as well.
                // So we are first checking whether the Notes are getting fetched on load or is it using the Advanced Search.
                // If the search is using the Advanced Search we are adding these columns to be fetched in the Query

                //rsolanki2: todo - identify and remove the unused columns here.>>
                //willimas-neha goel:added CLAIM_PRG_NOTE.POLICY_ID                
                if (!b_IsMobileAdjuster && !b_IsMobilityAdjuster)
                {
                    if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString().ToUpper().IndexOf("ORACLE") == -1)
                    {
                        sSelect = " SELECT  CLAIM_PRG_NOTE.CL_PROG_NOTE_ID,	CLAIM_PRG_NOTE.CLAIM_ID,	CLAIM_PRG_NOTE.EVENT_ID, CLAIM_PRG_NOTE.POLICY_ID,	CLAIM_PRG_NOTE.ENTERED_BY,	CLAIM_PRG_NOTE.DATE_ENTERED,	CLAIM_PRG_NOTE.DATE_CREATED,	CLAIM_PRG_NOTE.TIME_CREATED,	CLAIM_PRG_NOTE.NOTE_TYPE_CODE,	CLAIM_PRG_NOTE.USER_TYPE_CODE,"

                   //rsolanki2:- add the oracle counterpart here.
                       + "SUBSTRING(CLAIM_PRG_NOTE.NOTE_MEMO,1,45) NOTE_MEMO,	SUBSTRING(CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,1,45) NOTE_MEMO_CARETECH,	"

                       //+ "SUBSTR(CLAIM_PRG_NOTE.NOTE_MEMO,1,25),	SUBSTR(CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,1,25),	" 

                       + "CLAIM_PRG_NOTE.ENTERED_BY_NAME,	CLAIM_PRG_NOTE.TEMPLATE_ID,	"


                       //R6: Rakhi:Notepad Screens Templates - start
                       + "CLAIM_PRG_NOTE.SUBJECT,"//zmohammad MITS 30218
                       + " (SELECT TEMPLATE_NAME FROM CLAIM_PRG_NOTE_TEMPLATES "
                       + " WHERE CLAIM_PRG_NOTE_TEMPLATES.CL_PRGN_TEMP_ID=CLAIM_PRG_NOTE.TEMPLATE_ID) TEMPLATE_NAME, "
                            //R6: Rakhi:Notepad Screens Templates - end

                       + " (" + sSQLRmUser + ")  CLAIM_ADJUSTER "
                            //MGaba2: 22186: Sorting on basis of NoteType was working on basis of Description rather short code
                       + ", (SELECT SHORT_CODE FROM CODES C WHERE C.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_SHORT_CODE"   //Aman ML Change
                            //  + ", (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode +" UNION SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iBaseLangCode +")  NOTE_TYPE_DESC";
                            //Aman ML Change modified the query
                        + ", ((SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + ") UNION (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iBaseLangCode + " AND CODE_ID NOT IN (SELECT CODE_ID FROM CODES_TEXT CT,CLAIM_PRG_NOTE WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + "))) NOTE_TYPE_DESC";
                    }
                    else
                    {


                        sSelect = " SELECT  CLAIM_PRG_NOTE.CL_PROG_NOTE_ID,	CLAIM_PRG_NOTE.CLAIM_ID,	CLAIM_PRG_NOTE.EVENT_ID, CLAIM_PRG_NOTE.POLICY_ID,	CLAIM_PRG_NOTE.ENTERED_BY,	CLAIM_PRG_NOTE.DATE_ENTERED,	CLAIM_PRG_NOTE.DATE_CREATED,	CLAIM_PRG_NOTE.TIME_CREATED,	CLAIM_PRG_NOTE.NOTE_TYPE_CODE,	CLAIM_PRG_NOTE.USER_TYPE_CODE,"

                        //rsolanki2:- add the oracle counterpart here.
                            // + "SUBSTRING(CLAIM_PRG_NOTE.NOTE_MEMO,1,45) NOTE_MEMO,	SUBSTRING(CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,1,45) NOTE_MEMO_CARETECH,	"

                            + "SUBSTR(CLAIM_PRG_NOTE.NOTE_MEMO,1,45) NOTE_MEMO,	SUBSTR(CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,1,45) NOTE_MEMO_CARETECH,	"

                            + "CLAIM_PRG_NOTE.ENTERED_BY_NAME,	CLAIM_PRG_NOTE.TEMPLATE_ID,	"
                            + "CLAIM_PRG_NOTE.SUBJECT,"//zmohammad MITS 30218

                            //R6: Rakhi:Notepad Screens Templates - start
                            + " (SELECT TEMPLATE_NAME FROM CLAIM_PRG_NOTE_TEMPLATES "
                            + " WHERE CLAIM_PRG_NOTE_TEMPLATES.CL_PRGN_TEMP_ID=CLAIM_PRG_NOTE.TEMPLATE_ID) TEMPLATE_NAME, "
                            //R6: Rakhi:Notepad Screens Templates - end

                            + " (" + sSQLRmUser + ")  CLAIM_ADJUSTER "
                            //MGaba2: 22186: Sorting on basis of NoteType was working on basis of Description rather short code
                            + ", (SELECT SHORT_CODE FROM CODES C WHERE C.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_SHORT_CODE"  //Aman ML Change
                            //+ ", (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_DESC"; 
                            //Aman ML Change modified the query
                             + ", ((SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + ") UNION (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iBaseLangCode + " AND CODE_ID NOT IN (SELECT CODE_ID FROM CODES_TEXT CT,CLAIM_PRG_NOTE WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + "))) NOTE_TYPE_DESC";
                    }
                }
                else
                {
                    if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString().ToUpper().IndexOf("ORACLE") == -1)
                    {
                        sSelect = " SELECT  CLAIM_PRG_NOTE.CL_PROG_NOTE_ID,	CLAIM_PRG_NOTE.CLAIM_ID,	CLAIM_PRG_NOTE.EVENT_ID, CLAIM_PRG_NOTE.POLICY_ID,	CLAIM_PRG_NOTE.ENTERED_BY,	CLAIM_PRG_NOTE.DATE_ENTERED,	CLAIM_PRG_NOTE.DATE_CREATED,	CLAIM_PRG_NOTE.TIME_CREATED,	CLAIM_PRG_NOTE.NOTE_TYPE_CODE,	CLAIM_PRG_NOTE.USER_TYPE_CODE,"

                   //rsolanki2:- add the oracle counterpart here.
                       + "CLAIM_PRG_NOTE.NOTE_MEMO,	CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,	"

                       //+ "SUBSTR(CLAIM_PRG_NOTE.NOTE_MEMO,1,25),	SUBSTR(CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,1,25),	" 

                       + "CLAIM_PRG_NOTE.ENTERED_BY_NAME,	CLAIM_PRG_NOTE.TEMPLATE_ID,	"
                       + "CLAIM_PRG_NOTE.SUBJECT,"//zmohammad MITS 30218

                       //R6: Rakhi:Notepad Screens Templates - start
                       + " (SELECT TEMPLATE_NAME FROM CLAIM_PRG_NOTE_TEMPLATES "
                       + " WHERE CLAIM_PRG_NOTE_TEMPLATES.CL_PRGN_TEMP_ID=CLAIM_PRG_NOTE.TEMPLATE_ID) TEMPLATE_NAME, "
                            //R6: Rakhi:Notepad Screens Templates - end

                       + " (" + sSQLRmUser + ")  CLAIM_ADJUSTER "
                            //MGaba2: 22186: Sorting on basis of NoteType was working on basis of Description rather short code
                       + ", (SELECT SHORT_CODE FROM CODES C WHERE C.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_SHORT_CODE"
                            // + ", (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_DESC";
                            //Aman ML Change modified the query
                        + ", ((SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + ") UNION (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iBaseLangCode + " AND CODE_ID NOT IN (SELECT CODE_ID FROM CODES_TEXT CT,CLAIM_PRG_NOTE WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + "))) NOTE_TYPE_DESC";
                    }
                    else
                    {
                        sSelect = " SELECT  CLAIM_PRG_NOTE.CL_PROG_NOTE_ID,	CLAIM_PRG_NOTE.CLAIM_ID,	CLAIM_PRG_NOTE.EVENT_ID, CLAIM_PRG_NOTE.POLICY_ID,	CLAIM_PRG_NOTE.ENTERED_BY,	CLAIM_PRG_NOTE.DATE_ENTERED,	CLAIM_PRG_NOTE.DATE_CREATED,	CLAIM_PRG_NOTE.TIME_CREATED,	CLAIM_PRG_NOTE.NOTE_TYPE_CODE,	CLAIM_PRG_NOTE.USER_TYPE_CODE,"

                        //rsolanki2:- add the oracle counterpart here.
                            // + "SUBSTRING(CLAIM_PRG_NOTE.NOTE_MEMO,1,45) NOTE_MEMO,	SUBSTRING(CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,1,45) NOTE_MEMO_CARETECH,	"

                       + "CLAIM_PRG_NOTE.NOTE_MEMO,	CLAIM_PRG_NOTE.NOTE_MEMO_CARETECH,	"
                            + "CLAIM_PRG_NOTE.ENTERED_BY_NAME,	CLAIM_PRG_NOTE.TEMPLATE_ID,	"
                            + "CLAIM_PRG_NOTE.SUBJECT," //zmohammad MITS 30218

                            //R6: Rakhi:Notepad Screens Templates - start
                            + " (SELECT TEMPLATE_NAME FROM CLAIM_PRG_NOTE_TEMPLATES "
                            + " WHERE CLAIM_PRG_NOTE_TEMPLATES.CL_PRGN_TEMP_ID=CLAIM_PRG_NOTE.TEMPLATE_ID) TEMPLATE_NAME, "
                            //R6: Rakhi:Notepad Screens Templates - end

                            + " (" + sSQLRmUser + ")  CLAIM_ADJUSTER "
                            //MGaba2: 22186: Sorting on basis of NoteType was working on basis of Description rather short code
                            + ", (SELECT SHORT_CODE FROM CODES C WHERE C.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_SHORT_CODE"
                            //	+ ", (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE)  NOTE_TYPE_DESC";
                            //Aman ML Change modified the query
                        + ", ((SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + ") UNION (SELECT CODE_DESC FROM CODES_TEXT CT WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iBaseLangCode + " AND CODE_ID NOT IN (SELECT CODE_ID FROM CODES_TEXT CT,CLAIM_PRG_NOTE WHERE CT.CODE_ID = CLAIM_PRG_NOTE.NOTE_TYPE_CODE AND CT.LANGUAGE_CODE = " + iLangCode + "))) NOTE_TYPE_DESC";
                    }
                }
                //MITS 15501 : Umesh
                if (p_bActivateFilter && p_sFilterSQL.Contains("ADJUSTER_LASTNAME"))
                {
                    //rsushilaggar - Removed entity_table_id is Entity Role is ON.
                    if (objSettings.UseEntityRole)
                    {
                        sSQLUserLastName = this.GetSQLWithFirstRow("ENTITY", "LAST_NAME", " RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                        sSQLUserFirstName = this.GetSQLWithFirstRow("ENTITY", "FIRST_NAME", " RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                    }
                    else
                    {
                        sSQLUserLastName = this.GetSQLWithFirstRow("ENTITY", "LAST_NAME", "ENTITY_TABLE_ID=" + LocalCache.GetTableId("ADJUSTERS") + " AND RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                        sSQLUserFirstName = this.GetSQLWithFirstRow("ENTITY", "FIRST_NAME", "ENTITY_TABLE_ID=" + LocalCache.GetTableId("ADJUSTERS") + " AND RM_USER_ID=CLAIM_PRG_NOTE.ENTERED_BY AND RM_USER_ID <>0 ");
                    }
                        //MITS 15501 : End
                    sSelect += ", (" + sSQLUserLastName + ") ADJUSTER_LASTNAME "
                    + ", (" + sSQLUserFirstName + ") ADJUSTER_FIRSTNAME ";
                }
                //Merge and Comment by kuladeep for mits:25103(From ExecutiveSummary)
                //if (p_bActivateFilter && p_sFilterSQL.Contains("USER_TYPE_DESC"))
                //{
                //    sSelect += ", ( SELECT GROUP_NAME FROM	  "
                //                    + " (SELECT C.USER_TYPE_CODE,'SYSTEM GENERATED'  GROUP_NAME"
                //                    + " FROM CLAIM_PRG_NOTE C,USER_GROUPS"
                //                    + " WHERE  C.USER_TYPE_CODE =0"
                //                    + " UNION "
                //                    + " SELECT C.USER_TYPE_CODE,USER_GROUPS.GROUP_NAME  GROUP_NAME"
                //                    + " FROM CLAIM_PRG_NOTE C,USER_GROUPS"
                //                    + " WHERE USER_GROUPS.GROUP_ID=C.USER_TYPE_CODE "
                //                    + " ) X"
                //                    + " WHERE X.USER_TYPE_CODE=CLAIM_PRG_NOTE.USER_TYPE_CODE"
                //            + " )  USER_TYPE_DESC";
                //}
                sSelect += ", CLAIM_PRG_NOTE.ENTERED_BY_NAME ENTERED_BY_NAME "
                        + " FROM ";
                //Added by Rakhi for R6-Print Note By Note Type
                if (p_iNoteTypeCode > 0)
                {
                    sNodeTypeCondition = " AND CLAIM_PRG_NOTE.NOTE_TYPE_CODE = " + p_iNoteTypeCode;
                }
                //Added by Rakhi for R6-Print Note By Note Type
                //Parijat :Default sorting issues --chubb-19914
                if (p_bActivateFilter == false && string.Compare(p_sSortColumn, sNoSort) == 0 && p_sDirection == null && iPrintOrder1 == 0 && iPrintOrder2 == 0 && iPrintOrder3 == 0)
                {
                    p_sSortColumn = "DateEntered";
                    p_sDirection = " DESC";
                }
                //
                if (p_bActivateFilter == false)
                {
                    /*Added by GBINDRA MITS#34104 02112014 WWIG GAP15 START*/
                    if (Conversion.ConvertObjToInt(p_iClaimantId, m_iClientId) > 0 && objSettings.AllowNotesAtClaimant == true)
                    {
                        if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                        {
                            if (iPrintOrder1 != 0)
                            {
                                switch (iPrintOrder1)
                                {
                                    case 1:
                                        sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                        break;
                                    case 2:
                                        sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                        break;
                                    case 3:
                                        sOrderBy = "NOTE_TYPE_SHORT_CODE" + ",";
                                        break;

                                }
                            }
                            if (iPrintOrder2 != 0)
                            {
                                switch (iPrintOrder2)
                                {
                                    case 1:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                        break;
                                    case 2:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                        break;
                                    case 3:
                                        sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE" + ",";
                                        break;

                                }
                            }
                            if (iPrintOrder3 != 0)
                            {
                                switch (iPrintOrder3)
                                {
                                    case 1:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                        break;
                                    case 2:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                        break;
                                    case 3:
                                        sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE";
                                        break;

                                }
                            }

                        }
                        else
                            sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                        if (sOrderBy.EndsWith(","))
                            sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                        if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                        {
                            GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 1);

                        }
                        sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId +
                            " AND CLAIM_PRG_NOTE.ATTACH_RECORD_ID= " + p_iClaimantId
                              + " AND CLAIM_PRG_NOTE.ATTACH_TABLE = 'CLAIMANT' "
                              + sNodeTypeCondition;
                        sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId +
                            " AND CLAIM_PRG_NOTE.ATTACH_RECORD_ID= " + p_iClaimantId
                              + " AND CLAIM_PRG_NOTE.ATTACH_TABLE = 'CLAIMANT' "
                              + sNodeTypeCondition
                              + " ORDER BY " + sOrderBy;
                    }
                    //if (p_iClaimId > 0)
                    else if (p_iClaimId > 0)
                    /*Added by GBINDRA MITS#34104 02112014 WWIG GAP15 END*/
                    {
                        //nadim For Enhancement 12820
                        if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                        {
                            if (iPrintOrder1 != 0)
                            {
                                switch (iPrintOrder1)
                                {
                                    case 1:
                                        sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                        break;
                                    case 2:
                                        sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                        break;
                                    case 3:
                                        sOrderBy = "NOTE_TYPE_SHORT_CODE" + ",";
                                        break;

                                }
                            }
                            if (iPrintOrder2 != 0)
                            {
                                switch (iPrintOrder2)
                                {
                                    case 1:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                        break;
                                    case 2:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                        break;
                                    case 3:
                                        sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE" + ",";
                                        break;

                                }
                            }
                            if (iPrintOrder3 != 0)
                            {
                                switch (iPrintOrder3)
                                {
                                    case 1:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                        break;
                                    case 2:
                                        sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                        break;
                                    case 3:
                                        sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE";
                                        break;

                                }
                            }

                        }
                        else
                            sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                        if (sOrderBy.EndsWith(","))
                            sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                        //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                        if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                        {
                            GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 0);

                        }
                        //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                        //williams-neha goel:added to view policy notes in claim based on utility setting--start:MITS 21704
                        sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                            + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                            + sNodeTypeCondition;
                        if (objSettings.EnhNotesPolicyClaimView)
                        {
                            //vsharma205 Start : Jira: RMA-15765
                            //if (!string.IsNullOrEmpty(sPolicyIds)) Obsolete
                            if ((!string.IsNullOrEmpty(sPolicyIds)) && (sPolicyIds!="0")) 
                            //vsharma205 End : Jira: RMA-15765
                            {
                                sSQL = sSelect + sFrom + " WHERE (CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                                   + "OR CLAIM_PRG_NOTE.POLICY_ID IN (" + sPolicyIds + "))"
                                   + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                       + sNodeTypeCondition
                                       + " ORDER BY " + sOrderBy;
                            }
                            else if (iAttachedPolicy != 0)
                            {
                                sSQL = sSelect + sFrom + " WHERE (CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                                    + "OR CLAIM_PRG_NOTE.POLICY_ID=" + iAttachedPolicy + ")"
                                    + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                    //+	" AND " + sWhere
                                        + sNodeTypeCondition
                                        + " ORDER BY " + sOrderBy;
                            }
                            else
                            {
                                sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId +
                                    " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                    //+	" AND " + sWhere
                                        + sNodeTypeCondition
                                        + " ORDER BY " + sOrderBy;
                            }
                        }
                        else    //williams-neha goel--end:MITS 21704
                        {
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId
                                    + " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                //+	" AND " + sWhere
                                    + sNodeTypeCondition
                                    + " ORDER BY " + sOrderBy;
                        }
                        //nadim 12820
                    }
                    else
                    {
                        if (p_iEventId > 0)
                        {
                            //nadim for Enhancement 12820
                            if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                            {
                                if (iPrintOrder1 != 0)
                                {
                                    switch (iPrintOrder1)
                                    {
                                        case 1:
                                            sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                            break;
                                        case 2:
                                            sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                            break;
                                        case 3:
                                            sOrderBy = "NOTE_TYPE_SHORT_CODE" + ",";
                                            break;

                                    }
                                }
                                if (iPrintOrder2 != 0)
                                {
                                    switch (iPrintOrder2)
                                    {
                                        case 1:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                            break;
                                        case 2:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                            break;
                                        case 3:
                                            sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE" + ",";
                                            break;

                                    }
                                }
                                if (iPrintOrder3 != 0)
                                {
                                    switch (iPrintOrder3)
                                    {
                                        case 1:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                            break;
                                        case 2:
                                            sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                            break;
                                        case 3:
                                            sOrderBy = sOrderBy + "NOTE_TYPE_SHORT_CODE";
                                            break;

                                    }
                                }

                            }
                            else
                                sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                            if (sOrderBy.EndsWith(","))
                                sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                            //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                            if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                            {
                                GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 1);

                            }
                            //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                            sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId +
                                " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                //+	" AND " +	sWhere 
                                  + sNodeTypeCondition;
                            //Deb MITS 26121
                            // + " ORDER BY " + sOrderBy;
                            sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId +
                                " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                //+	" AND " +	sWhere 
                                  + sNodeTypeCondition
                                  + " ORDER BY " + sOrderBy;
                            //Deb MITS 26121
                        }
                        //williams-neha goel:added forpolicy--start:MITS 21704
                        else
                        {
                            if (p_iPolicyId > 0)
                            {
                                //nadim for Enhancement 12820
                                if (iPrintOrder1 != 0 || iPrintOrder2 != 0 || iPrintOrder3 != 0)
                                {
                                    if (iPrintOrder1 != 0)
                                    {
                                        switch (iPrintOrder1)
                                        {
                                            case 1:
                                                sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                                break;
                                            case 2:
                                                sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                                break;
                                            case 3:
                                                sOrderBy = "NOTE_TYPE_DESC" + ",";
                                                break;

                                        }
                                    }
                                    if (iPrintOrder2 != 0)
                                    {
                                        switch (iPrintOrder2)
                                        {
                                            case 1:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED" + ",";
                                                break;
                                            case 2:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED" + ",";
                                                break;
                                            case 3:
                                                sOrderBy = sOrderBy + "NOTE_TYPE_DESC" + ",";
                                                break;

                                        }
                                    }
                                    if (iPrintOrder3 != 0)
                                    {
                                        switch (iPrintOrder3)
                                        {
                                            case 1:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_ENTERED";
                                                break;
                                            case 2:
                                                sOrderBy = sOrderBy + "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                                break;
                                            case 3:
                                                sOrderBy = sOrderBy + "NOTE_TYPE_DESC";
                                                break;

                                        }
                                    }

                                }
                                else
                                    sOrderBy = "CLAIM_PRG_NOTE.DATE_CREATED,CLAIM_PRG_NOTE.TIME_CREATED";
                                if (sOrderBy.EndsWith(","))
                                    sOrderBy = GetLeftString(sOrderBy, sOrderBy.Length - 1);
                                //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                                if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                                {
                                    GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 3);

                                }
                                //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                                //Deb MITS 26121
                                sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE CLAIM_PRG_NOTE.POLICY_ID=" + p_iPolicyId +
                                            " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                            " AND (CLAIM_PRG_NOTE.EVENT_ID IS NULL OR CLAIM_PRG_NOTE.EVENT_ID = 0) " +
                                            " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                    //+	" AND " +	sWhere 
                                            + sNodeTypeCondition;
                                //Deb MITS 26121
                                sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.POLICY_ID=" + p_iPolicyId +
                                    " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                    " AND (CLAIM_PRG_NOTE.EVENT_ID IS NULL OR CLAIM_PRG_NOTE.EVENT_ID = 0) " +
                                    " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) " //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                    //+	" AND " +	sWhere 
                                      + sNodeTypeCondition
                                            + " ORDER BY " + sOrderBy;
                            }
                        }
                        //williams-neha goel---end:MITS 21704
                    }
                    //nadim 12820
                }
                else
                {
                    //Start by Shivendu for MITS 18098. SQL sorting instead of Grid
                    if (!string.IsNullOrEmpty(p_sSortColumn) && string.Compare(p_sSortColumn, sNoSort) != 0)
                    {
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.LastIndexOf("ORDER BY") > -1)
                            {
                                p_sFilterSQL = p_sFilterSQL.Remove(p_sFilterSQL.LastIndexOf("ORDER BY"));
                            }

                            GetSortString(p_sSortColumn, p_sDirection, ref sFrom, ref sNodeTypeCondition, ref sOrderBy, objSearchResults, 2);
                            p_sFilterSQL = p_sFilterSQL + " ORDER BY " + sOrderBy;
                        }
                    }
                    //End by Shivendu for MITS 18098. SQL sorting instead of Grid
                    /*added by GBINDRA MIST#34104 02122014 WWIG GAP15 START*/
                    //if (p_iClaimId > 0)
                    if (p_iClaimantId > 0 && objSettings.AllowNotesAtClaimant == true)
                    {
                        //Start averma62 - MITS 27787
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Substring(5, 11) == "CLAIM_ID IN")
                            {
                                sClaimCondition = " ";
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("CLAIM_ID IN", "CLAIM_PRG_NOTE.CLAIM_ID IN");
                                }
                            }
                            else
                                sClaimCondition = " AND CLAIM_PRG_NOTE.ATTACH_TABLE = 'CLAIMANT' "
                                                  + " AND CLAIM_PRG_NOTE.ATTACH_RECORD_ID= " + p_iClaimantId;
                        }
                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition == " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += "CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "EVENT", "EVENT_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "EVENT.EVENT_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        //End by Shivendu for MITS 18098
                        sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                            + sClaimCondition
                            //Deb MITS 26121
                            //+ p_sFilterSQL;
                            + p_sFilterSQL.Substring(0, p_sFilterSQL.Length - p_sFilterSQL.Substring(p_sFilterSQL.IndexOf("ORDER BY")).Length);
                        //sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId;
                        sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                               + sClaimCondition
                               + p_sFilterSQL;
                        //Deb MITS 26121
                    }
                    /*added by GBINDRA MIST#34104 02122014 WWIG GAP15 END*/
                    else if (p_iClaimId > 0)
                    {
                        //Start averma62 - MITS 27787
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Substring(5, 11) == "CLAIM_ID IN")
                            {
                                sClaimCondition = " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) "; //Added by gbindra MITS#34104 WWIG GAP15 02132014
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("CLAIM_ID IN", "CLAIM_PRG_NOTE.CLAIM_ID IN");
                                }
                            }
                            else
                                sClaimCondition = " AND CLAIM_PRG_NOTE.CLAIM_ID=" + p_iClaimId.ToString() +
                                                  " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) "; //Added by gbindra MITS#34104 WWIG GAP15 02132014
                        }
                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += " CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }

                        //End by Shivendu for MITS 18098
                        sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                            + sClaimCondition
                            + p_sFilterSQL.Substring(0, p_sFilterSQL.Length - p_sFilterSQL.Substring(p_sFilterSQL.IndexOf("ORDER BY")).Length);//Deb

                        sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                            + sClaimCondition
                            + p_sFilterSQL;
                    }
                    else if (p_iEventId > 0)
                    {
                        //Start averma62 - MITS 27787
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Substring(5, 11) == "CLAIM_ID IN")
                            {
                                sClaimCondition = " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) "; //Added by gbindra MITS#34104 WWIG GAP15 02132014;
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("CLAIM_ID IN", "CLAIM_PRG_NOTE.CLAIM_ID IN");
                                }

                            }
                            else
                                sClaimCondition = " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                                  " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) "; //Added by gbindra MITS#34104 WWIG GAP15 02132014;
                        }
                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition == " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += "CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "EVENT", "EVENT_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "EVENT.EVENT_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        //End by Shivendu for MITS 18098
                        sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                            + sClaimCondition
                            //Deb MITS 26121
                            //+ p_sFilterSQL;
                            + p_sFilterSQL.Substring(0, p_sFilterSQL.Length - p_sFilterSQL.Substring(p_sFilterSQL.IndexOf("ORDER BY")).Length);
                        //sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId;
                        sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.EVENT_ID=" + p_iEventId
                               + sClaimCondition
                               + p_sFilterSQL;
                        //Deb MITS 26121
                    }
                    //williams-neha goel---start:MITS 21704
                    else if (p_iPolicyId > 0)
                    {
                        //Start averma62 - MITS 27787
                        if (!string.IsNullOrEmpty(p_sFilterSQL))
                        {
                            if (p_sFilterSQL.Substring(5, 11) == "CLAIM_ID IN")
                            {
                                sClaimCondition = " ";
                                if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0)
                                {
                                    p_sFilterSQL = p_sFilterSQL.Replace("CLAIM_ID IN", "CLAIM_PRG_NOTE.CLAIM_ID IN");
                                }

                            }
                            else
                                sClaimCondition = " AND (CLAIM_PRG_NOTE.CLAIM_ID IS NULL OR CLAIM_PRG_NOTE.CLAIM_ID = 0) " +
                                                  " AND (CLAIM_PRG_NOTE.ATTACH_TABLE <> 'CLAIMANT' OR  CLAIM_PRG_NOTE.ATTACH_TABLE IS NULL) "; //Added by gbindra MITS#34104 WWIG GAP15 02132014
                        }
                        //End averma62 - MITS 27787
                        //Start by Shivendu for MITS 18098
                        if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition == " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            //sFrom = sFrom + " , CLAIM ";
                            p_sFilterSQL += "CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.CLAIM_ID = CLAIM.CLAIM_ID ";

                        }
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "EVENT", "EVENT_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "EVENT.EVENT_NUMBER" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        //End by Shivendu for MITS 18098
                        else if (string.Compare(p_sSortColumn, ATTACHED_TO) == 0 && sClaimCondition != " ")
                        {
                            objSearchResults.JoinIt(ref sFrom, ref sClaimCondition, "CLAIM_PRG_NOTE", "POLICY", "POLICY_ID");
                            //sFrom = sFrom + " , EVENT ";
                            p_sFilterSQL += "POLICY.POLICY_NAME" + " " + p_sDirection.Trim().ToUpper();
                            //sClaimCondition += " AND CLAIM_PRG_NOTE.EVENT_ID = EVENT.EVENT_ID ";

                        }
                        //Deb MITS 26121
                        sSQLCount = "SELECT COUNT(*) FROM " + sFrom + " WHERE  CLAIM_PRG_NOTE.POLICY_ID=" + p_iPolicyId
                           + sClaimCondition
                           + p_sFilterSQL.Substring(0, p_sFilterSQL.Length - p_sFilterSQL.Substring(p_sFilterSQL.IndexOf("ORDER BY")).Length);
                        //Deb MITS 26121
                        sSQL = sSelect + sFrom + " WHERE CLAIM_PRG_NOTE.POLICY_ID=" + p_iPolicyId
                            + sClaimCondition
                            + p_sFilterSQL;
                    }
                    //williams-neha goel---end:MITS 21704
                }



                // Get the Sub Ordinalte Users list, Find subordinates in security db.
                arrlstSubOrdinateUsers = new ArrayList();
                sSQLSubOrdinateUsers = "SELECT USER_ID FROM USER_TABLE WHERE MANAGER_ID = " + m_objDataModelFactory.Context.RMUser.UserId;
                //Merge and Change by kuladeep for mits:25103(From ExecutiveSummary)
                //objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), sSQLSubOrdinateUsers);              
                //if (objReader != null) 
                using (DbReader objReader = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), sSQLSubOrdinateUsers))
                {
                    while (objReader.Read())
                        arrlstSubOrdinateUsers.Add(Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId));
                }
                //objReader.Close();
                //Start averma62 Mits 28988
                if (!string.IsNullOrEmpty(sSQLCount)) p_iTotalCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSQLCount), m_iClientId);
                //use a dictionary object to cache GROUP_NAME
                //End averma62 Mits 28988
                Dictionary<int, string> dicGroupName = new Dictionary<int, string>();

                int index = 0;
                //Merge and Change by kuladeep for mits:25103(From ExecutiveSummary)
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objReader != null)
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        if (index < p_iStartIndex)
                        {
                            index++;
                            continue;
                        }

                        if (index > p_iEndIndex)
                            break;

                        index++;

                        //iSysTypeId = Conversion.ConvertObjToInt( objReader.GetValue( "SYS_TYPE_ID" ), m_iClientId );
                        iAdjusterId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ADJUSTER"), m_iClientId);

                        //if( !(arrlstConfidentialTypes.Contains( iSysTypeId ) && arrlstSubOrdinateUsers.Contains(iAdjusterId)) )
                        // JP 9-26-2006:   This was a bug to begin with. It was locking the adjuster's manager 
                        //                 *out* of claims for that adjuster instead of the other way around.
                        //                 HOWEVER, fixing this particular security check would only cause
                        //                 more confusion since most clients don't have appropriate manager/adjuster
                        //                 relationships setup. So I am disabling the check.
                        // JP 9-26-2006    if( !arrlstSubOrdinateUsers.Contains(iAdjusterId) )
                        {
                            structProgressNotes = new ProgressNotes();

                            structProgressNotes.ClaimProgressNoteId = Conversion.ConvertObjToInt(objReader.GetValue("CL_PROG_NOTE_ID"), m_iClientId);
                            structProgressNotes.ClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId);
                            structProgressNotes.ClaimNumber = this.GetClaimName(structProgressNotes.ClaimId);
                            structProgressNotes.EventId = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), m_iClientId);
                            structProgressNotes.EventNumber = this.GetEventName(structProgressNotes.EventId);
                            //williams-nehagoel-start:MITS 21704
                            structProgressNotes.PolicyId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_ID"), m_iClientId);
                            structProgressNotes.PolicyName = this.GetPolicyName(structProgressNotes.PolicyId, ref sPolicyNumber);
                            structProgressNotes.PolicyNumber = sPolicyNumber;
                            //structProgressNotes.CodeId = this.GetCodeID(structProgressNotes.PolicyId, structProgressNotes.EventId, structProgressNotes.ClaimId);
                            //williams-nehagoel-end:MITS 21704
                            structProgressNotes.EnteredBy = objReader.GetString("ENTERED_BY");
                            structProgressNotes.Subject = objReader.GetString("SUBJECT");//zmohammad MITS 30218
                            //Raman 02/10/2010: We need to get the full name instead of loginname
                            //string sFullName=objReader.GetString("ENTERED_BY_NAME");
                            //bool bReturnVal=false;
                            //if (!sdUserNames.ContainsKey(sFullName))
                            //{
                            //    bReturnVal = PublicFunctions.GetFullNameFromLoginName(sFullName, out sFullName);
                            //    if (bReturnVal )
                            //    {

                            //    }
                            //    sdUserNames.Add(sFullName,sFullName);
                            //    structProgressNotes.EnteredByName = objReader.GetString("ENTERED_BY_NAME");
                            //}
                            //else
                            //{
                            //    sFullName = sdUserNames[sFullName];                                
                            //}
                            //rsolanki2: optimizations
                            sFullName = string.Empty;
                            sEnteredByName = objReader.GetString("ENTERED_BY_NAME");

                            if (sdFullNames.ContainsKey(sEnteredByName))
                            {
                                structProgressNotes.EnteredByName = sdFullNames[sEnteredByName];
                            }
                            else
                            {
                                if (PublicFunctions.GetFullNameFromLoginName(sEnteredByName, out sFullName, m_iClientId))
                                {
                                    structProgressNotes.EnteredByName = sFullName;
                                    sdFullNames.Add(sEnteredByName, sFullName);
                                }
                                else
                                {
                                    structProgressNotes.EnteredByName = sEnteredByName;
                                    sdFullNames.Add(sEnteredByName, sEnteredByName);
                                }
                            }

                            //string sFullName;
                            //if (PublicFunctions.GetFullNameFromLoginName(, out sFullName))
                            //{
                            //    structProgressNotes.EnteredByName = sFullName;
                            //}
                            //else
                            //{
                            //    structProgressNotes.EnteredByName = objReader.GetString("ENTERED_BY_NAME");
                            //}

                            //rsolanki2 : mits 23101: we are now saving the full name in the "Entered by" column, thereby we dont need to 
                            //the full name at runtime for every row.
                            //structProgressNotes.EnteredByName = objReader.GetString("ENTERED_BY_NAME");

                            //structProgressNotes.EnteredByName = objReader.GetString("ENTERED_BY_NAME");
                            structProgressNotes.DateEntered = Conversion.GetUIDate(objReader.GetString("DATE_ENTERED"), m_UserLogin.objUser.NlsCode.ToString(),m_iClientId);//Conversion.ToDate(objReader.GetString("DATE_ENTERED"), m_UserLogin.objUser.NlsCode.ToString());  //Conversion.GetDBDateFormat(objReader.GetString("DATE_ENTERED"), "d");
                            structProgressNotes.DateCreated = Conversion.GetUIDate(objReader.GetString("DATE_CREATED"), m_UserLogin.objUser.NlsCode.ToString(), m_iClientId);//Conversion.ToDate(objReader.GetString("DATE_CREATED"), m_UserLogin.objUser.NlsCode.ToString());  //Conversion.GetDBDateFormat(objReader.GetString("DATE_CREATED"), "d");
                            //structProgressNotes.TimeCreated = Conversion.GetTime(Conversion.GetDBTimeFormat(objReader.GetString("TIME_CREATED"), "T"), m_UserLogin.objUser.NlsCode.ToString()); //Conversion.GetDBTimeFormat(objReader.GetString("TIME_CREATED"), "T");
                            structProgressNotes.TimeCreated = Conversion.GetTime(Conversion.GetDBTimeFormat(objReader.GetString("TIME_CREATED"), "T"), m_UserLogin.objUser.NlsCode.ToString(),m_iClientId); 
                            structProgressNotes.NoteTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("NOTE_TYPE_CODE"), m_iClientId);
                            //MGaba2: MITS 22184: Note Type was not getting updated
                            //    structProgressNotes.NoteType = LocalCache.GetShortCode(structProgressNotes.NoteTypeCode) + " " + LocalCache.GetCodeDesc(structProgressNotes.NoteTypeCode);
                            structProgressNotes.NoteType = objReader.GetString("NOTE_TYPE_SHORT_CODE") + " " + objReader.GetString("NOTE_TYPE_DESC");

                            structProgressNotes.UserTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("USER_TYPE_CODE"), m_iClientId);

                            if (dicGroupName.ContainsKey(structProgressNotes.UserTypeCode))
                            {
                                structProgressNotes.UserType = dicGroupName[structProgressNotes.UserTypeCode];
                            }
                            else
                            {
                                if (structProgressNotes.UserTypeCode != 0)
                                {
                                    sSQL = " SELECT GROUP_NAME from USER_GROUPS WHERE GROUP_ID =" + structProgressNotes.UserTypeCode;
                                    //MITS 12838 : Umes        //nadim for 13396
                                    using (DbReader objReaderUser = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                                    {
                                        if (objReaderUser.Read())
                                        {
                                            structProgressNotes.UserType = objReaderUser.GetString("GROUP_NAME");
                                            if (structProgressNotes.UserType.IndexOf("'") > -1)
                                            {
                                                structProgressNotes.UserType = structProgressNotes.UserType.Replace("'", "");
                                            }
                                        }
                                        else
                                            structProgressNotes.UserType = "";
                                        //nadim for 13396
                                    }
                                }
                                else
                                    structProgressNotes.UserType = "System Generated";

                                dicGroupName.Add(structProgressNotes.UserTypeCode, structProgressNotes.UserType);
                            }

                            structProgressNotes.NoteMemo = objReader.GetString("NOTE_MEMO");
                            structProgressNotes.NoteMemoCareTech = objReader.GetString("NOTE_MEMO_CARETECH");

                            // rsolanki2 : we have been assuming that the enteredby name is the same as adjuster. However it 
                            // can be different for some clients. right now, just equating them directly. 
                            // it should be taken care of as part of mits 20537
                            structProgressNotes.AdjusterName = structProgressNotes.EnteredByName;

                            ////Raman 02/10/2010: We need to get the full name instead of loginname
                            //sFullName = string.Empty;
                            //if (PublicFunctions.GetFullNameFromLoginName(objReader.GetString("ENTERED_BY_NAME"), out sFullName))
                            //{
                            //    structProgressNotes.AdjusterName = sFullName;
                            //}
                            //else
                            //{
                            //    structProgressNotes.AdjusterName = objReader.GetString("ENTERED_BY_NAME");
                            //}
                            //structProgressNotes.AdjusterName = objReader.GetString("ENTERED_BY_NAME");

                            /*structProgressNotes.AdjusterName = objReader.GetString( "ADJUSTER_FIRSTNAME" );
                            if( structProgressNotes.AdjusterName.Trim() != "" )
                                structProgressNotes.AdjusterName = objReader.GetString( "ADJUSTER_LASTNAME" ) + ", " + structProgressNotes.AdjusterName ;
                            else
                                structProgressNotes.AdjusterName = objReader.GetString( "ADJUSTER_LASTNAME" ) ;*/
                            //R6: Rakhi-Notepad Screens Templates - start
                            structProgressNotes.TemplateId = objReader.GetValue("TEMPLATE_ID").ToString();
                            structProgressNotes.TemplateName = objReader.GetValue("TEMPLATE_NAME").ToString();
                            //R6: Rakhi - Notepad Screens Templates - end

                            arrlstProgressNotes.Add(structProgressNotes);
                        }
                    }
                }
                return (arrlstProgressNotes);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetNotes.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //Comment by kuladeep for mits:25103(From ExecutiveSummary)
                //if (objReader != null)
                //{
                //    objReader.Close();
                //    objReader.Dispose();
                //}

                if (objSearchResults != null)
                {
                    objSearchResults = null;


                }

                //nadim  12820
                objSettings = null;
                sOrderBy = null;
                //nadim  12820
                arrlstConfidentialTypes = null;
                arrlstSubOrdinateUsers = null;
            }
        }

        /// <summary>
        /// Returns the sort order based on column
        /// </summary>
        private void GetSortString(string p_sSortColumn, string p_sDirection, ref string sFrom, ref string sNodeTypeCondition, ref string sOrderBy, SearchResults objSearchResults, int iCallee)
        {
            try
            {
                switch (p_sSortColumn)
                {
                    case ACTIVITY_DATE:
                        sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + " " + p_sDirection.Trim().ToUpper() + ", CLAIM_PRG_NOTE.DATE_CREATED " + p_sDirection.Trim().ToUpper()
                                       + ", CLAIM_PRG_NOTE.TIME_CREATED " + p_sDirection.Trim().ToUpper();
                        break;

                    case NOTE_MEMO:
                        sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + " " + p_sDirection.Trim().ToUpper() + ", CLAIM_PRG_NOTE.DATE_CREATED " + p_sDirection.Trim().ToUpper()
                                       + ", CLAIM_PRG_NOTE.TIME_CREATED " + p_sDirection.Trim().ToUpper();
                        break;

                    case ATTACHED_TO:
                        if (iCallee == 0)//Claim
                        {
                            if (sNodeTypeCondition == string.Empty)
                            {
                                sNodeTypeCondition = "   ";
                            }

                            objSearchResults.JoinIt(ref sFrom, ref sNodeTypeCondition, "CLAIM_PRG_NOTE", "CLAIM", "CLAIM_ID");
                            sOrderBy = "CLAIM.CLAIM_NUMBER" + " " + p_sDirection.Trim().ToUpper();

                        }
                        else if (iCallee == 1)//Event
                        {
                            if (sNodeTypeCondition == string.Empty)
                            {
                                sNodeTypeCondition = "   ";
                            }
                            objSearchResults.JoinIt(ref sFrom, ref sNodeTypeCondition, "CLAIM_PRG_NOTE", "EVENT", "EVENT_ID");
                            sOrderBy = "EVENT.EVENT_NUMBER" + " " + p_sDirection.Trim().ToUpper();

                        }
                        else if (iCallee == 2)//Advanced Search
                        {
                            sOrderBy = "";
                        }
                        //williams-neha goel--start:MITS 21704
                        else if (iCallee == 3)//policy
                        {
                            if (sNodeTypeCondition == string.Empty)
                            {
                                sNodeTypeCondition = "   ";
                            }
                            objSearchResults.JoinIt(ref sFrom, ref sNodeTypeCondition, "CLAIM_PRG_NOTE", "POLICY", "POLICY_ID");
                            sOrderBy = "POLICY.POLICY_NAME" + " " + p_sDirection.Trim().ToUpper();

                        }
                        //williams-neha goel--end:MITS 21704
                        break;
                    case NOTE_TYPE:
                        sOrderBy = "NOTE_TYPE_SHORT_CODE " + " " + p_sDirection.Trim().ToUpper();
                        break;

                    case ENTERED_BY:
                        sOrderBy = "CLAIM_PRG_NOTE.ENTERED_BY_NAME" + " " + p_sDirection.Trim().ToUpper();
                        break;
                    default:
                        sOrderBy = "CLAIM_PRG_NOTE.DATE_ENTERED" + " " + p_sDirection.Trim().ToUpper();
                        break;
                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.OnLoad.Error", m_iClientId), p_objEx);
            }

        }
        /// Name		: OnLoad
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching notes
        /// </summary>
        /// 
        //williams:nehagoel added int p_iPolicyId
        //mkaran2 - MITS 27038 - added to new fields to have "View All Notes" func on Advance Search screen of Enhanced notes
        //gbindra added p_sClaimantId for MITS#34104 WWIG GAP15
        public XmlDocument OnLoad(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, int p_iPageNumber, string p_sSortColumn, string p_sDirection, bool p_bPrintAllPages, int p_iPolicyId, string p_sPolicyName, string p_sViewNotesOption, string p_sViewNotesTypeList, int p_iClaimantId)
        {
            ProgressNotes structProgressNotes;
            ArrayList arrlstProgressNotes = null;

            XmlElement objRootNode = null;
            XmlElement objProgressNoteNode = null;
            XmlElement objTempNode = null;
            //nadim for enhancement 12821
            string sClaimant = "";
            //nadim 12821
            //Changed by Gagan for MITS 12334 : Start
            SysSettings objSysSettings = null;
            //Changed by Gagan for MITS 12334 : End
            int iIndex = 0;
            int iPrintOrder1 = 0;
            int iPrintOrder2 = 0;
            int iPrintOrder3 = 0;
            int iTotalNumberOfPages = 0;//Added by Shivendu for MITS 18098
            int iStartIndex = 0;//Added by Shivendu for MITS 18098
            int iEndIndex = 0;//Added by Shivendu for MITS 18098
            int iRecordsPerPage = 0;
            //skhare7 MITS 27285
            string PIName = string.Empty;
            ////Added by Shivendu for MITS 18098
            //ResultList objResultList = new ResultList(m_sConnectionString);
            DbReader objReader = null;
            try
            {
                //By Rakhi for Safeway Notepad Screens
                //Added extra parameter :iNoteTypeCode : here it is hardcoded as 0 in order to show list of all notes
                //williams -neha goel-start:added  p_iPolicyId, p_sPolicyName:MITS 21704
                //arrlstProgressNotes = this.GetNotes(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, 0, p_sSortColumn, p_sDirection);
                //  mkaran2 - MITS 27038 - Added string p_sViewNotesOptionList, string p_sViewNotesTypeList
                arrlstProgressNotes = this.GetNotes(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, 0, p_sSortColumn, p_sDirection, p_iPolicyId, p_sViewNotesOption, p_sViewNotesTypeList, p_iClaimantId); //Added by gbindra for MITS#34104 WWIG GAP15
                //williams -neha goel-end:added  p_iPolicyId, p_sPolicyName:MITS 21704
                //Changed by Gagan for MITS 12334 : Start
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                //Changed by Gagan for MITS 12334 : End

                ////Start by Shivendu for MITS 18098
                //iRecordsPerPage = objResultList.GetRecordsPerPage();

                //Parijat MITS 19713
                iRecordsPerPage = GetNotesPerPage();
                //skhare7 MITS 27285
                if (p_iClaimId == 0 && p_iEventId != 0)
                {
                    Event objEvent = null;
                    objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                    objEvent.MoveTo(p_iEventId);
                    //skhare7 27285
                    if (objEvent.PiList.GetPrimaryPi() != null)

                        PIName = objEvent.PiList.GetPrimaryPi().PiEntity.GetLastFirstName();
                }
                //rsolanki2 : mits 21294 

                if (!p_bPrintAllPages)
                {
                    if (p_iPageNumber > 0)
                    {
                        if (arrlstProgressNotes.Count > iRecordsPerPage)
                        { //MGaba2:MITS 18913:Deleting record was disabling links
                            iTotalNumberOfPages = (int)Math.Ceiling((double)arrlstProgressNotes.Count / iRecordsPerPage);
                        }
                        else
                        {
                            iTotalNumberOfPages = 1;
                        }
                        iStartIndex = (p_iPageNumber - 1) * iRecordsPerPage;
                        iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                        if (iEndIndex > (arrlstProgressNotes.Count - 1))
                        {
                            //MGaba2:MITS 18913:Deleting record was disabling links
                            if (p_iPageNumber != 1 && (arrlstProgressNotes.Count == iStartIndex))
                            {
                                p_iPageNumber -= 1;
                                iStartIndex = (p_iPageNumber - 1) * iRecordsPerPage;
                                iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                            }
                            else
                                iEndIndex = arrlstProgressNotes.Count - 1;
                        }
                    }
                    else //MITS 21696: Else condition added.
                    {
                        iStartIndex = 0;
                        iEndIndex = arrlstProgressNotes.Count - 1;
                    }
                }
                else
                {
                    iStartIndex = 0;
                    iEndIndex = arrlstProgressNotes.Count - 1;
                }
                //End by Shivendu for MITS 18098

                Functions.StartDocument(ref m_objDocument, ref objRootNode, "ProgressNotes", m_iClientId);
                /*Added by gbindra MITS#34104 WWIG GAP 15 START*/
                if (p_iClaimantId > 0)
                {
                    sClaimant = GetClaimantName(p_iClaimantId);
                }
                //nadim for Enhancement 12821
                //if (p_iClaimId > 0)
                else if (p_iClaimId > 0)
                    /*Added by gbindra MITS#34104 WWIG GAP 15 END*/
                    sClaimant = GetClaimantName(p_iClaimId.ToString());
                else
                    sClaimant = "";
                Functions.CreateAndSetElement(objRootNode, "Claimant", sClaimant, m_iClientId);
                //nadim 12821
                //Changed by Gagan for MITS 12334 : Start
                if (objSysSettings.FreezeExistingNotes == -1)
                    Functions.CreateAndSetElement(objRootNode, "FreezeText", "true", m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "FreezeText", "false", m_iClientId);
                //Parijat: 20555 Full Name instead of Login Name
                string sFullName = string.Empty;
                if (PublicFunctions.GetFullNameFromLoginName(m_sUserName, out sFullName, m_iClientId))
                {
                    Functions.CreateAndSetElement(objRootNode, "UserName", sFullName, m_iClientId);
                }
                else
                {
                    Functions.CreateAndSetElement(objRootNode, "UserName", m_sUserName, m_iClientId);
                    sFullName = m_sUserName;
                }
                //Parijat: 20555
                iPrintOrder1 = objSysSettings.EnhPrintOrder1;
                iPrintOrder2 = objSysSettings.EnhPrintOrder2;
                iPrintOrder3 = objSysSettings.EnhPrintOrder3;
                Functions.CreateAndSetElement(objRootNode, "PrintOrder1", iPrintOrder1.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "PrintOrder2", iPrintOrder2.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "PrintOrder3", iPrintOrder3.ToString(), m_iClientId);
                //Changed by Gagan for MITS 12334 : End               

                //Changed by gagan for mits 14626 : start
                Functions.CreateAndSetElement(objRootNode, "ShowDateStamp", objSysSettings.DateStampFlag.ToString(), m_iClientId);
                //Changed by gagan for mits 14626 : end

                //Start by Shivendu for MITS 18098
                Functions.CreateAndSetElement(objRootNode, "TotalNumberOfPages", iTotalNumberOfPages.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "PageNumber", p_iPageNumber.ToString(), m_iClientId);  //MGaba2:MITS 18913:Deleting record was disabling links
                //End by Shivendu for MITS 18098

                //Parijat: Post Editable Enhanced Notes Start - To set the Time Limit and Freeze Text Flag
                Double iTimeLimit = 0;//decimal value
                int flagRights = 0;
                bool editFlag = false;
                string sSQLTimeLimit = String.Empty;

                sSQLTimeLimit = "SELECT ENH_NOTE_TIME_LIMIT,ENH_NOTE_EDT_RIGHTS FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLTimeLimit);
                if (objReader.Read())
                {
                    iTimeLimit = Conversion.ConvertObjToDouble(objReader.GetValue("ENH_NOTE_TIME_LIMIT"), m_iClientId);
                    flagRights = Conversion.ConvertObjToInt(objReader.GetValue("ENH_NOTE_EDT_RIGHTS"), m_iClientId);
                }
                if (flagRights == -1)
                {
                    editFlag = true;
                }
                Functions.CreateAndSetElement(objRootNode, "TimeLimit", iTimeLimit.ToString(), m_iClientId);
                //Functions.CreateAndSetElement(objRootNode, "FreezeTextFlag", bFreeze.ToString().ToLower());
                Functions.CreateAndSetElement(objRootNode, "EnhNoteEditRight", editFlag.ToString(), m_iClientId);

                ////rsolanki2 : mits  20796
                //HtmlAgilityPack.HtmlDocument objHtmDoc = new HtmlAgilityPack.HtmlDocument();                
                //objHtmDoc.OptionFixNestedTags = true;
                //System.IO.Stream objStream;

                //Parijat:Post Editable Enhanced Notes  End  
                //williams--neha goel:start--for filtring the notes code type on tne basis of parent:MITS 21704
                string NotesParentCodeId = string.Empty;
                NotesParentCodeId = this.GetCodeID(p_iPolicyId, p_iEventId, p_iClaimId);
                Functions.CreateAndSetElement(objRootNode, "NotesParentCodeId", NotesParentCodeId, m_iClientId);

                if (objSysSettings.EnhNotesPolicyClaimView)
                    Functions.CreateAndSetElement(objRootNode, "NotesPolicyClaimView", "true", m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "NotesPolicyClaimView", "false", m_iClientId);
                //williams--neha goel:end:MITS 21704
                //Add condition by kuladeep for mits:23101 07/18/2011
                //rsolanki2: sorting by Enteredby field
                if (p_sSortColumn == "AdjusterName" || string.Compare(p_sFilterSQL.Trim(), "ORDER BY CLAIM_PRG_NOTE.ENTERED_BY_NAME") == 0)
                {
                    //Add condition by kuladeep for mits:23101 07/18/2011 Start
                    if (string.IsNullOrEmpty(p_sDirection) == true)
                    {
                        this.m_direction = SortDirection.Asc;
                    }
                    else//Add condition by kuladeep for mits:23101 07/18/2011 End
                    {
                        if (p_sDirection.Trim().ToUpper() == "ASC")
                        {
                            this.m_direction = SortDirection.Asc;
                        }
                        else
                        {
                            this.m_direction = SortDirection.Desc;
                        }
                    }

                    arrlstProgressNotes.Sort(this);
                }

                for (iIndex = iStartIndex; iIndex <= iEndIndex; iIndex++)//Modified by Shivendu for MITS 18098
                {
                    structProgressNotes = (ProgressNotes)arrlstProgressNotes[iIndex];

                    Functions.CreateElement(objRootNode, "ProgressNote", ref objProgressNoteNode, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "ClaimProgressNoteId", structProgressNotes.ClaimProgressNoteId.ToString(), m_iClientId);

                    // Get Claim Number
                    Functions.CreateAndSetElement(objProgressNoteNode, "ClaimNumber", structProgressNotes.ClaimNumber, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("Id", structProgressNotes.ClaimId.ToString());

                    // Get Event Number.
                    Functions.CreateAndSetElement(objProgressNoteNode, "EventNumber", structProgressNotes.EventNumber, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("Id", structProgressNotes.EventId.ToString());

                    //willaims-neha goel--start
                    // Get Policy Name.
                    Functions.CreateAndSetElement(objProgressNoteNode, "PolicyName", structProgressNotes.PolicyName, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("Id", structProgressNotes.PolicyId.ToString());
                    Functions.CreateAndSetElement(objProgressNoteNode, "PolicyId", structProgressNotes.PolicyId.ToString(), m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "PolicyNumber", structProgressNotes.PolicyNumber, m_iClientId);
                    //Functions.CreateAndSetElement(objProgressNoteNode, "CodeId", structProgressNotes.CodeId);
                    //willaims-neha goel--end
                    /*ADDED BY GBINDRA MITS#34104 02122014 WWIG GAP15 START*/
                    if (p_iClaimantId > 0 && objSysSettings.AllowNotesAtClaimant == true)
                        Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "CLAIMANT: " + structProgressNotes.Claimant, m_iClientId);
                    //if( structProgressNotes.ClaimId != 0 )
                    else if (structProgressNotes.ClaimId != 0)
                        /*ADDED BY GBINDRA MITS#34104 02122014 WWIG GAP15 END*/
                        Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "CLAIM: " + structProgressNotes.ClaimNumber, m_iClientId);
                    //williams-neha goel---start--added if for Policy:MITS 21704
                    else if (structProgressNotes.PolicyId != 0)
                    {
                        if (structProgressNotes.PolicyNumber != string.Empty)
                            Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "POLICY: " + structProgressNotes.PolicyName + "*" + structProgressNotes.PolicyNumber, m_iClientId);
                        else
                            Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "POLICY: " + structProgressNotes.PolicyName, m_iClientId);
                    }
                    //williams-neha goel---end:MITS 21704
                    else
                        Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "EVENT: " + structProgressNotes.EventNumber, m_iClientId);

                    Functions.CreateAndSetElement(objProgressNoteNode, "EnteredBy", structProgressNotes.EnteredBy, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "EnteredByName", structProgressNotes.EnteredByName, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "DateEntered", structProgressNotes.DateEntered, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "DateEnteredForSort", Conversion.GetDate(structProgressNotes.DateEntered), m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "DateCreated", structProgressNotes.DateCreated, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "TimeCreated", structProgressNotes.TimeCreated, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "Subject", structProgressNotes.Subject, m_iClientId);//zmohammad MITS 30218
                    //wiliams:neha goel:isnoteeditable will be false if viewing policy notes in claims---start
                    if (objSysSettings.EnhNotesPolicyClaimView && structProgressNotes.PolicyId != 0 && p_iClaimId > 0 && p_iPolicyId == 0)
                    {
                        Functions.CreateAndSetElement(objProgressNoteNode, "IsNoteEditable", "false", m_iClientId);
                    }
                    else
                    {
                        Functions.CreateAndSetElement(objProgressNoteNode, "IsNoteEditable", Functions.IsNoteEditable(structProgressNotes.DateCreated, structProgressNotes.TimeCreated, iTimeLimit.ToString(), editFlag.ToString(), structProgressNotes.EnteredByName, sFullName), m_iClientId);
                    }
                    Functions.CreateAndSetElement(objProgressNoteNode, "TimeCreatedForSort", Conversion.GetTime(structProgressNotes.TimeCreated), m_iClientId);	//MITS 13550 Umesh					

                    // Get Note Type Code.
                    Functions.CreateAndSetElement(objProgressNoteNode, "NoteTypeCode", structProgressNotes.NoteType, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("CodeId", structProgressNotes.NoteTypeCode.ToString());

                    // Get User Type.
                    Functions.CreateAndSetElement(objProgressNoteNode, "UserTypeCode", structProgressNotes.UserType, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("CodeId", structProgressNotes.UserTypeCode.ToString());

                    Functions.CreateAndSetElement(objProgressNoteNode, "NoteMemo", structProgressNotes.NoteMemo, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "NoteMemoCareTech", structProgressNotes.NoteMemoCareTech, m_iClientId);

                    //rsolanki2 : commenting this code section. we've moved part of this to UI layer. 
                    //rsolanki2 : mits  20796

                    //objStream = new System.IO.MemoryStream(ASCIIEncoding.Default.GetBytes(structProgressNotes.NoteMemo));
                    //objHtmDoc.Load(objStream);
                    //objStream.Close();

                    //Functions.CreateAndSetElement(objProgressNoteNode, "NoteMemo"
                    //    , objHtmDoc.DocumentNode.InnerHtml.ToString());

                    //rsolanki2 : mits  20796
                    //objStream = new System.IO.MemoryStream(ASCIIEncoding.Default.GetBytes(structProgressNotes.NoteMemoCareTech));
                    //objHtmDoc.Load(objStream);
                    //objStream.Close();

                    //Functions.CreateAndSetElement(objProgressNoteNode, "NoteMemoCareTech"
                    //    , objHtmDoc.DocumentNode.InnerText.ToString());

                    Functions.CreateAndSetElement(objProgressNoteNode, "AdjusterName", structProgressNotes.AdjusterName, m_iClientId);
                    //Added Rakhi for R6-Progress Notes Templates
                    Functions.CreateAndSetElement(objProgressNoteNode, "TemplateId", structProgressNotes.TemplateId, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "TemplateName", structProgressNotes.TemplateName, m_iClientId);
                    //Added Rakhi for R6-Progress Notes Templates
                    //skahre7 27285
                    if (PIName != string.Empty)
                        Functions.CreateAndSetElement(objProgressNoteNode, "PIName", PIName, m_iClientId);
                    //objStream.Dispose();
                }

                return (m_objDocument);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.OnLoad.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //objResultList = null;  19713
                objRootNode = null;
                objProgressNoteNode = null;
                objTempNode = null;
                arrlstProgressNotes = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
        }

        //Changed by gbindra MITS#34104 02112014 WWIG GAP15 added ClaimantId
        //rsolanki2: Enhc Notes ajax updates
        //williams:nehagoel added int p_iPolicyId:MITS 21704
        public XmlDocument OnLoadPartial(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, int p_iPageNumber, string p_sSortColumn, string p_sDirection, int p_iPolicyId, string p_sPolicyName, bool b_IsMobileAdjuster, bool b_IsMobilityAdjuster, int p_iClaimantId)
        {
            ProgressNotes structProgressNotes;
            ArrayList arrlstProgressNotes = null;

            XmlElement objRootNode = null;
            XmlElement objProgressNoteNode = null;
            XmlElement objTempNode = null;
            //nadim for enhancement 12821
            string sClaimant = "";
            //nadim 12821
            //Changed by Gagan for MITS 12334 : Start
            SysSettings objSysSettings = null;
            //Changed by Gagan for MITS 12334 : End
            int iIndex = 0;
            int iPrintOrder1 = 0;
            int iPrintOrder2 = 0;
            int iPrintOrder3 = 0;
            int iTotalNumberOfPages = 0;//Added by Shivendu for MITS 18098
            int iStartIndex = 0;//Added by Shivendu for MITS 18098
            int iEndIndex = 0;//Added by Shivendu for MITS 18098
            int iRecordsPerPage = 0;
            ////Added by Shivendu for MITS 18098
            //ResultList objResultList = new ResultList(m_sConnectionString);

            try
            {
                //By Rakhi for Safeway Notepad Screens
                //Added extra parameter :iNoteTypeCode : here it is hardcoded as 0 in order to show list of all notes
                //williams -neha goel-start:added  p_iPolicyId, p_sPolicyName:MITS 21704
                //arrlstProgressNotes = this.GetNotesPartial(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, 0, p_sSortColumn, p_sDirection);
                //arrlstProgressNotes = this.GetNotesPartial(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, 0, p_sSortColumn, p_sDirection, p_iPolicyId);
                //Changed by Gagan for MITS 12334 : Start
                //objSysSettings = new SysSettings(m_sConnectionString);
                //Changed by Gagan for MITS 12334 : End

                ////Start by Shivendu for MITS 18098
                //iRecordsPerPage = objResultList.GetRecordsPerPage();

                //Parijat MITS 19713
                iRecordsPerPage = GetNotesPerPage();
                if (p_iPageNumber > 0)
                {
                    iStartIndex = (p_iPageNumber - 1) * iRecordsPerPage;
                    iEndIndex = iStartIndex + (iRecordsPerPage - 1);
                }
                else
                {
                    iStartIndex = 0;
                    iEndIndex = iRecordsPerPage - 1;
                }

                //Changed by gbindra MITS#34104 02112014 WWIG GAP15 added ClaimantId
                //By Rakhi for Safeway Notepad Screens
                //Added extra parameter :iNoteTypeCode : here it is hardcoded as 0 in order to show list of all notes
                int iTotalCount = 0;
                arrlstProgressNotes = this.GetNotesPartial(p_iEventId, p_iClaimId, p_bActivateFilter, p_sFilterSQL, 0, p_sSortColumn, p_sDirection, iStartIndex, iEndIndex, ref iTotalCount, p_iPolicyId, b_IsMobileAdjuster, b_IsMobilityAdjuster, p_iClaimantId);                //Changed by Gagan for MITS 12334 : Start //Added by gbindra for MITS#34104 WWIG GAP15
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                //Changed by Gagan for MITS 12334 : End

                ////Start by Shivendu for MITS 18098
                //iRecordsPerPage = objResultList.GetRecordsPerPage();

                //Parijat MITS 19713
                if (iTotalCount > iRecordsPerPage)
                { //MGaba2:MITS 18913:Deleting record was disabling links
                    iTotalNumberOfPages = (int)Math.Ceiling((double)iTotalCount / iRecordsPerPage);
                    if (iEndIndex > (iTotalCount - 1))
                    {
                        //MGaba2:MITS 18913:Deleting record was disabling links
                        if (p_iPageNumber > 1 && (iTotalCount == iStartIndex))
                        {
                            p_iPageNumber -= 1;
                        }
                    }
                }
                else
                {
                    iTotalNumberOfPages = 1;
                }
                //End by Shivendu for MITS 18098

                Functions.StartDocument(ref m_objDocument, ref objRootNode, "ProgressNotes", m_iClientId);
                //nadim for Enhancement 12821
                /*Added by gbindra MITS#34104 WWIG GAP 15 START*/
                if (p_iClaimantId > 0)
                {
                    sClaimant = GetClaimantName(p_iClaimantId);
                }
                //if (p_iClaimId > 0)
                else if (p_iClaimId > 0)
                    /*Added by gbindra MITS#34104 WWIG GAP 15 END*/
                    sClaimant = GetClaimantName(p_iClaimId.ToString());
                else
                    sClaimant = "";
                Functions.CreateAndSetElement(objRootNode, "Claimant", sClaimant, m_iClientId);
                //nadim 12821
                //Changed by Gagan for MITS 12334 : Start
                if (objSysSettings.FreezeExistingNotes == -1)
                    Functions.CreateAndSetElement(objRootNode, "FreezeText", "true", m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "FreezeText", "false", m_iClientId);
                //Parijat: 20555 Full Name instead of Login Name
                string sFullName;
                if (PublicFunctions.GetFullNameFromLoginName(m_sUserName, out sFullName, m_iClientId))
                {
                    Functions.CreateAndSetElement(objRootNode, "UserName", sFullName, m_iClientId);
                }
                else
                {
                    Functions.CreateAndSetElement(objRootNode, "UserName", m_sUserName, m_iClientId);
                }
                //Parijat: 20555
                //Functions.CreateAndSetElement(objRootNode, "UserName", m_sUserName);
                iPrintOrder1 = objSysSettings.EnhPrintOrder1;
                iPrintOrder2 = objSysSettings.EnhPrintOrder2;
                iPrintOrder3 = objSysSettings.EnhPrintOrder3;
                Functions.CreateAndSetElement(objRootNode, "PrintOrder1", iPrintOrder1.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "PrintOrder2", iPrintOrder2.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "PrintOrder3", iPrintOrder3.ToString(), m_iClientId);
                //Changed by Gagan for MITS 12334 : End               

                //Changed by gagan for mits 14626 : start
                Functions.CreateAndSetElement(objRootNode, "ShowDateStamp", objSysSettings.DateStampFlag.ToString(), m_iClientId);
                //Changed by gagan for mits 14626 : end

                //Start by Shivendu for MITS 18098
                Functions.CreateAndSetElement(objRootNode, "TotalNumberOfPages", iTotalNumberOfPages.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "PageNumber", p_iPageNumber.ToString(), m_iClientId);  //MGaba2:MITS 18913:Deleting record was disabling links
                //End by Shivendu for MITS 18098

                //williams--neha goel:start--for filtring the notes code type on tne basis of parent:MITS 21704
                string NotesParentCodeId = string.Empty;
                NotesParentCodeId = this.GetCodeID(p_iPolicyId, p_iEventId, p_iClaimId);
                Functions.CreateAndSetElement(objRootNode, "NotesParentCodeId", NotesParentCodeId, m_iClientId);

                if (objSysSettings.EnhNotesPolicyClaimView)
                    Functions.CreateAndSetElement(objRootNode, "NotesPolicyClaimView", "true", m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "NotesPolicyClaimView", "false", m_iClientId);
                //williams--neha goel:end:MITS 21704
                //Add condition by kuladeep for mits:23101 07/18/2011
                //rsolanki2: sorting by Enteredby field
                if (p_sSortColumn == "AdjusterName" || string.Compare(p_sFilterSQL.Trim(), "ORDER BY CLAIM_PRG_NOTE.ENTERED_BY_NAME") == 0)
                {
                    //Add condition by kuladeep for mits:23101 07/18/2011 Start
                    if (string.IsNullOrEmpty(p_sDirection) == true)
                    {
                        this.m_direction = SortDirection.Asc;
                    }//Add condition by kuladeep for mits:23101 07/18/2011 End
                    else
                    {
                        if (p_sDirection.Trim().ToUpper() == "ASC")
                        {
                            this.m_direction = SortDirection.Asc;
                        }
                        else
                        {
                            this.m_direction = SortDirection.Desc;
                        }
                    }

                    arrlstProgressNotes.Sort(this);
                }
                for (iIndex = 0; iIndex < arrlstProgressNotes.Count; iIndex++)//Modified by Shivendu for MITS 18098
                {
                    structProgressNotes = (ProgressNotes)arrlstProgressNotes[iIndex];

                    Functions.CreateElement(objRootNode, "ProgressNote", ref objProgressNoteNode, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "ClaimProgressNoteId", structProgressNotes.ClaimProgressNoteId.ToString(), m_iClientId);

                    // Get Claim Number
                    Functions.CreateAndSetElement(objProgressNoteNode, "ClaimNumber", structProgressNotes.ClaimNumber, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("Id", structProgressNotes.ClaimId.ToString());

                    // Get Event Number.
                    Functions.CreateAndSetElement(objProgressNoteNode, "EventNumber", structProgressNotes.EventNumber, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("Id", structProgressNotes.EventId.ToString());

                    //willaims-neha goel--start
                    // Get Policy Name.
                    Functions.CreateAndSetElement(objProgressNoteNode, "PolicyName", structProgressNotes.PolicyName, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("Id", structProgressNotes.PolicyId.ToString());
                    Functions.CreateAndSetElement(objProgressNoteNode, "PolicyId", structProgressNotes.PolicyId.ToString(), m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "PolicyNumber", structProgressNotes.PolicyNumber, m_iClientId);
                    //Functions.CreateAndSetElement(objProgressNoteNode, "CodeId", structProgressNotes.CodeId);
                    //willaims-neha goel--end
                    /*Added by GBINDRA MITS#34104 WWIG GAP15 START*/
                    if (p_iClaimantId > 0 && objSysSettings.AllowNotesAtClaimant == true)
                    {
                        Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "CLAIMANT: " + sClaimant, m_iClientId);
                    }
                    //if (structProgressNotes.ClaimId != 0)
                    else if (structProgressNotes.ClaimId != 0)
                        /*Added by GBINDRA MITS#34104 WWIG GAP15 END*/
                        Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "CLAIM: " + structProgressNotes.ClaimNumber, m_iClientId);
                    //williams-neha goel---start--modified else and added if for Policy:MITS 21704
                    else if (structProgressNotes.EventId != 0)
                        Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "EVENT: " + structProgressNotes.EventNumber, m_iClientId);
                    else if (structProgressNotes.PolicyId != 0)
                    {
                        if (structProgressNotes.PolicyNumber != string.Empty)
                            Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "POLICY: " + structProgressNotes.PolicyName + "*" + structProgressNotes.PolicyNumber, m_iClientId);
                        else
                            Functions.CreateAndSetElement(objProgressNoteNode, "AttachedTo", "POLICY: " + structProgressNotes.PolicyName, m_iClientId);
                    }
                    //willimas:end:MITS 21704
                    Functions.CreateAndSetElement(objProgressNoteNode, "EnteredBy", structProgressNotes.EnteredBy, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "EnteredByName", structProgressNotes.EnteredByName, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "DateEntered", structProgressNotes.DateEntered, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "DateEnteredForSort", Conversion.GetDate(structProgressNotes.DateEntered), m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "DateCreated", structProgressNotes.DateCreated, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "TimeCreated", structProgressNotes.TimeCreated, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "TimeCreatedForSort", Conversion.GetTime(structProgressNotes.TimeCreated), m_iClientId);	//MITS 13550 Umesh					
                    Functions.CreateAndSetElement(objProgressNoteNode, "Subject", structProgressNotes.Subject, m_iClientId); //zmohammad MITS 30218
                    // Get Note Type Code.
                    Functions.CreateAndSetElement(objProgressNoteNode, "NoteTypeCode", structProgressNotes.NoteType, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("CodeId", structProgressNotes.NoteTypeCode.ToString());

                    // Get User Type.
                    Functions.CreateAndSetElement(objProgressNoteNode, "UserTypeCode", structProgressNotes.UserType, ref objTempNode, m_iClientId);
                    objTempNode.SetAttribute("CodeId", structProgressNotes.UserTypeCode.ToString());

                    Functions.CreateAndSetElement(objProgressNoteNode, "NoteMemo", structProgressNotes.NoteMemo, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "NoteMemoCareTech", structProgressNotes.NoteMemoCareTech, m_iClientId);

                    Functions.CreateAndSetElement(objProgressNoteNode, "AdjusterName", structProgressNotes.AdjusterName, m_iClientId);
                    //Added Rakhi for R6-Progress Notes Templates
                    Functions.CreateAndSetElement(objProgressNoteNode, "TemplateId", structProgressNotes.TemplateId, m_iClientId);
                    Functions.CreateAndSetElement(objProgressNoteNode, "TemplateName", structProgressNotes.TemplateName, m_iClientId);
                    //Added Rakhi for R6-Progress Notes Templates

                }
                //Parijat: Post Editable Enhanced Notes Start - To set the Time Limit and Freeze Text Flag
                Double iTimeLimit = 0;//decimal value
                int flagRights = 0;
                bool editFlag = false;
                string sSQLTimeLimit = String.Empty;
                DbReader objReader = null;
                sSQLTimeLimit = "SELECT ENH_NOTE_TIME_LIMIT,ENH_NOTE_EDT_RIGHTS FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLTimeLimit);
                if (objReader.Read())
                {
                    iTimeLimit = Conversion.ConvertObjToDouble(objReader.GetValue("ENH_NOTE_TIME_LIMIT"), m_iClientId);
                    flagRights = Conversion.ConvertObjToInt(objReader.GetValue("ENH_NOTE_EDT_RIGHTS"), m_iClientId);
                }
                if (flagRights == -1)
                {
                    editFlag = true;
                }
                Functions.CreateAndSetElement(objRootNode, "TimeLimit", iTimeLimit.ToString(), m_iClientId);
                //Functions.CreateAndSetElement(objRootNode, "FreezeTextFlag", bFreeze.ToString().ToLower());
                Functions.CreateAndSetElement(objRootNode, "EnhNoteEditRight", editFlag.ToString(), m_iClientId);
                //Parijat:Post Editable Enhanced Notes  End  

                //Added by Amitosh for mits 23473 (05/11/2011)
                string[,] sortpref;//Deb MITS 30185

                sortpref = GetNotesSettings(m_objDataModelFactory.Context.RMUser.UserId);
                sort_by = sortpref[9, 0];//Deb MITS 30185
                Functions.CreateAndSetElement(objRootNode, "SortPref", sort_by, m_iClientId);
                //end Amitosh
                // Added by akaushik5 for MITS 30789 Starts
                this.orderBy = sortpref[10, 0];
                Functions.CreateAndSetElement(objRootNode, "OrderPref", this.orderBy, m_iClientId);
                // Added by akaushik5 for MITS 30789 Ends
                return (m_objDocument);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.OnLoad.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //objResultList = null;
                objRootNode = null;
                objProgressNoteNode = null;
                objTempNode = null;
                arrlstProgressNotes = null;
            }
        }

        /// Name		: CreateJoinSQL
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for generating SQL
        /// </summary>

        private void CreateJoinSQL(ref string p_sFrom, ref string p_sWhere, string p_sTableOne, string p_sTableTwo, string p_sFieldOne, string p_sFieldTwo)
        {
            try
            {
                switch (DBType)
                {
                    case Constants.DB_ACCESS:
                        p_sFrom = "(" + p_sFrom + " LEFT JOIN " + p_sTableTwo;
                        p_sFrom = p_sFrom + " ON " + p_sTableOne + "." + p_sFieldOne + " = " + p_sTableTwo + "." + p_sFieldTwo + ")";
                        break;
                    case Constants.DB_SQLSRVR:
                    case Constants.DB_SYBASE:
                        p_sFrom = p_sFrom + "," + p_sTableTwo;
                        if (p_sWhere != "")
                            p_sWhere = p_sWhere + " AND ";
                        p_sWhere = p_sWhere + p_sTableOne + "." + p_sFieldOne + " *= " + p_sTableTwo + "." + p_sFieldTwo;
                        break;
                    case Constants.DB_INFORMIX:
                        p_sFrom = p_sFrom + ",OUTER " + p_sTableTwo;
                        if (p_sWhere != "")
                            p_sWhere = p_sWhere + " AND ";
                        p_sWhere = p_sWhere + p_sTableOne + "." + p_sFieldOne + " = " + p_sTableTwo + "." + p_sFieldTwo;
                        break;
                    case Constants.DB_ORACLE:
                        p_sFrom = p_sFrom + "," + p_sTableTwo;
                        if (p_sWhere != "")
                            p_sWhere = p_sWhere + " AND ";
                        p_sWhere = p_sWhere + p_sTableOne + "." + p_sFieldOne + " = " + p_sTableTwo + "." + p_sFieldTwo + "(+)";
                        break;
                    case Constants.DB_DB2:
                        p_sFrom = "(" + p_sFrom + " LEFT OUTER JOIN " + p_sTableTwo;
                        p_sFrom = p_sFrom + " ON " + p_sTableOne + "." + p_sFieldOne + " = " + p_sTableTwo + "." + p_sFieldTwo + ")";
                        break;
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.OnLoad.Error", m_iClientId), p_objEx);
            }
        }


        /// Name		: GetClaimName
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching claim name
        /// </summary>
        /// 
        private string GetClaimName(int p_iClaimId)
        {
            DbReader objReader = null;
            string sClaimName = string.Empty;
            string sSQL = string.Empty;

            try
            {
                if (p_iClaimId == 0)
                {
                    return "";
                }


                sSQL = " SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                        sClaimName = objReader.GetString("CLAIM_NUMBER");
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetClaimName.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sClaimName);
        }


        /// Name		: GetEventName
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching event name
        /// </summary>
        /// 
        private string GetEventName(int p_iEventId)
        {
            DbReader objReader = null;
            string sEventName = string.Empty;
            string sSQL = string.Empty;

            try
            {
                sSQL = " SELECT Event_NUMBER FROM EVENT WHERE EVENT_ID = " + p_iEventId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                        sEventName = objReader.GetString("EVENT_NUMBER");
                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetEventName.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sEventName);
        }
        //williams-added by neha goel---start:MITS 21704
        /// Name		: GetPolicyName
        /// Author		: Neha Goel
        /// Date Created:  July 2010
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching policy name
        /// </summary>
        /// 
        private string GetPolicyName(int p_iPolicyId, ref string p_sPolicyNumber)
        {
            DbReader objReader = null;
            string sPolicyName = string.Empty;
            p_sPolicyNumber = string.Empty;
            string sSQL = string.Empty;

            try
            {
                if (p_iPolicyId == 0)
                {
                    return "";
                }

                sSQL = " SELECT POLICY_NAME,POLICY_NUMBER FROM POLICY WHERE POLICY_ID = " + p_iPolicyId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                        sPolicyName = objReader.GetString("POLICY_NAME");
                    p_sPolicyNumber = objReader.GetString("POLICY_NUMBER");
                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetEventName.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sPolicyName);
        }



        /// Name		: GetCodeID
        /// Author		: Neha Goel
        /// Date Created: 08 July 2010
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching policy name
        /// </summary>
        /// 
        private string GetCodeID(int p_iPolicyID, int p_iEventID, int p_iClaimID)
        {
            DbReader objReader = null;
            string CodeId = string.Empty;
            try
            {
                if (p_iPolicyID != 0 && p_iEventID == 0 && p_iClaimID == 0)
                {
                    CodeId = Convert.ToString(LocalCache.GetCodeId("P", "PARENT_NOTE_TYPE"));
                }
                else if (p_iPolicyID == 0 && p_iEventID != 0 && p_iClaimID == 0)
                {
                    CodeId = Convert.ToString(LocalCache.GetCodeId("E", "PARENT_NOTE_TYPE"));
                    CodeId = CodeId + "," + Convert.ToString(LocalCache.GetCodeId("CE", "PARENT_NOTE_TYPE"));
                }
                else if ((p_iPolicyID == 0 && p_iEventID == 0 && p_iClaimID != 0) || (p_iPolicyID == 0 && p_iEventID != 0 && p_iClaimID != 0))
                {
                    CodeId = Convert.ToString(LocalCache.GetCodeId("C", "PARENT_NOTE_TYPE"));
                    CodeId = CodeId + "," + Convert.ToString(LocalCache.GetCodeId("CE", "PARENT_NOTE_TYPE"));
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetEventName.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (CodeId);
        }

        //williams-added by neha goel--end:MITS 21704

        /// Name		: GetSQLWithFirstRow
        /// Author		: Vaibhav Kaushik
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for generating SQL
        /// </summary>
        /// 
        private string GetSQLWithFirstRow(string p_sTableName, string p_sFieldName, string p_sWhereClause)
        {
            string sReturnString = string.Empty;

            try
            {
                switch (DBType)
                {
                    case Constants.DB_ACCESS:
                    case Constants.DB_SQLSRVR:
                    case Constants.DB_SYBASE:
                    case Constants.DB_ODBC:
                        sReturnString = "SELECT TOP 1 " + p_sFieldName + " FROM " + p_sTableName + " WHERE " + p_sWhereClause;
                        break;
                    case Constants.DB_DB2:
                        sReturnString = "SELECT " + p_sFieldName + " FROM " + p_sTableName + " WHERE " + p_sWhereClause + " FETCH FIRST 1 ROWS ONLY";
                        break;
                    case Constants.DB_INFORMIX:
                        sReturnString = "SELECT FIRST 1 " + p_sFieldName + " FROM " + p_sTableName + " WHERE " + p_sWhereClause;
                        break;
                    case Constants.DB_ORACLE:
                        sReturnString = "SELECT " + p_sFieldName + " FROM " + p_sTableName + " WHERE " + p_sWhereClause + " AND ROWNUM <= 1";
                        break;
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.OnLoad.Error", m_iClientId), p_objEx);
            }
            return (sReturnString);
        }

        #endregion

        #region Advanced Search

        /// Name		: SelectClaim
        /// Author		: Raman Bhatia
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching claims for a particular event
        /// </summary>
        /// 
        public XmlDocument SelectClaim(long p_lEventId)
        {
            string sSQL = "";
            string sClaimId = "";
            string sClaimDate = "";
            string sClaimNumber = "";
            string sLastName = "";
            string sFirstName = "";
            string sClaimantName = "";
            string sClaimStatusDesc = "";
            string sClaimStatusShortCode = "";
            long lClaimStatusCode = 0;
            long lClaimTypeCode = 0;
            long lLOBCode = 0;
            long lClaimantEID = 0;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement = null;
            XmlElement objChildElement = null;
            DbReader objReader = null;
            DbReader objDbReader = null;
            int iLangCode = 0; //Aman ML Change	
            try
            {
                iLangCode = m_UserLogin.objUser.NlsCode;   //Aman ML Change
                Functions.StartDocument(ref objXmlDoc, ref objXmlElement, "ClaimList", m_iClientId);
                m_objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);

                //MGaba2 :- 04/04/2008-MITS 11985-Added Order by clause
                //sSQL = "SELECT CLAIM_NUMBER, DATE_OF_CLAIM, CLAIM_STATUS_CODE, CLAIM_ID,CLAIM_TYPE_CODE, LINE_OF_BUS_CODE FROM CLAIM WHERE EVENT_ID = " + p_lEventId.ToString();
                sSQL = "SELECT CLAIM_NUMBER, DATE_OF_CLAIM, CLAIM_STATUS_CODE, CLAIM_ID,CLAIM_TYPE_CODE, LINE_OF_BUS_CODE FROM CLAIM WHERE EVENT_ID = " + p_lEventId.ToString() + " ORDER BY CLAIM_NUMBER";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        Functions.CreateElement(objXmlElement, "Claim", ref objChildElement, m_iClientId);

                        sClaimId = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_ID"));
                        sClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));
                        sClaimDate = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CLAIM"));
                        lClaimStatusCode = Conversion.ConvertObjToInt64(objReader.GetValue("CLAIM_STATUS_CODE"), m_iClientId);
                        lClaimTypeCode = Conversion.ConvertObjToInt64(objReader.GetValue("CLAIM_TYPE_CODE"), m_iClientId);
                        lLOBCode = Conversion.ConvertObjToInt64(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);

                        //m_objLocalCache.GetCodeInfo(Convert.ToInt32(lClaimStatusCode) , ref sClaimStatusShortCode , ref sClaimStatusDesc);	  //Aman ML Change
                        m_objLocalCache.GetCodeInfo(Convert.ToInt32(lClaimStatusCode), ref sClaimStatusShortCode, ref sClaimStatusDesc, iLangCode);

                        objChildElement.SetAttribute("ClaimID", sClaimId);
                        objChildElement.SetAttribute("ClaimNumber", sClaimNumber);
                        objChildElement.SetAttribute("ClaimDate", sClaimDate);
                        objChildElement.SetAttribute("ClaimStatusDesc", sClaimStatusDesc);
                        objChildElement.SetAttribute("ClaimTypeCode", lClaimTypeCode.ToString());
                        objChildElement.SetAttribute("LOB", lLOBCode.ToString());

                        sClaimantName = GetClaimantName(sClaimId);
                        objChildElement.SetAttribute("ClaimantName", sClaimantName);
                    }
                }
                objReader.Close();
                return objXmlDoc;
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.XmlError", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                sSQL = null;
                if (m_objLocalCache != null)
                {
                    m_objLocalCache.Dispose();
                    m_objLocalCache = null;
                }
            }

        }
        /// Name		: SelectClaimant
        /// Author		: Gurpreet Singh Bindra
        /// Date Created: 4 feb 2014
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for fetching claimant for a particular event
        /// created for MITS#34104 WWIG Gap 15 by GBINDRA
        /// </summary>
        /// 
        public XmlDocument SelectClaimant(long p_lEventId, long p_lClaimId)
        {
            string sSQL = string.Empty;
            string sClaimId = string.Empty;
            string sClaimNumber = string.Empty;
            string sClaimantFirstName = string.Empty;
            string sClaimantLastName = string.Empty;
            long lClaimantRowID = 0;
            long lLOBCode = 0;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement = null;
            XmlElement objChildElement = null;
            DbReader objReader = null;
            DbReader objDbReader = null;
            int iLangCode = 0;
            bool bSuccess = false;

            try
            {
                iLangCode = m_UserLogin.objUser.NlsCode;
                Functions.StartDocument(ref objXmlDoc, ref objXmlElement, "ClaimantList", m_iClientId);
                m_objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);

                if (!(p_lEventId > 0))
                {
                    sSQL = "SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID = " + p_lClaimId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    while (objReader.Read())
                    {
                        p_lEventId = Conversion.CastToType<long>(objReader.GetValue("EVENT_ID").ToString(), out bSuccess);
                    }
                    objReader.Close();
                    objReader = null;
                }

                sSQL = "SELECT CLAIM.CLAIM_ID AS CLAIM_ID, CLAIM.CLAIM_NUMBER AS CLAIM_NUMBER, ENTITY.FIRST_NAME AS FIRST_NAME, ENTITY.LAST_NAME AS LAST_NAME, CLAIMANT.CLAIMANT_ROW_ID AS CLAIMANT_ROW_ID, CLAIM.LINE_OF_BUS_CODE AS LINE_OF_BUS_CODE FROM CLAIM, CLAIMANT, ENTITY WHERE CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID AND ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID AND CLAIM.EVENT_ID  = " + p_lEventId.ToString() + " ORDER BY CLAIM.CLAIM_NUMBER";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                while (objReader.Read())
                {
                    Functions.CreateElement(objXmlElement, "Claimant", ref objChildElement, m_iClientId);

                    sClaimId = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_ID"));
                    sClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));
                    sClaimantFirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                    sClaimantLastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                    lClaimantRowID = Conversion.CastToType<long>(objReader.GetValue("CLAIMANT_ROW_ID").ToString(), out bSuccess);
                    lLOBCode = Conversion.CastToType<long>(objReader.GetValue("LINE_OF_BUS_CODE").ToString(), out bSuccess);

                    objChildElement.SetAttribute("ClaimID", sClaimId);
                    objChildElement.SetAttribute("ClaimantRowId", lClaimantRowID.ToString());
                    objChildElement.SetAttribute("ClaimNumber", sClaimNumber);
                    objChildElement.SetAttribute("ClaimantFirstName", sClaimantFirstName);
                    objChildElement.SetAttribute("ClaimantLastName", sClaimantLastName);
                    objChildElement.SetAttribute("LOB", lLOBCode.ToString());

                }
                objReader.Close();
                objReader = null;

                if (string.Compare(sClaimId, string.Empty, true) == 0)
                {
                    sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE EVENT_ID = " + p_lEventId.ToString() + " ORDER BY CLAIM_NUMBER";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    while (objReader.Read())
                    {
                        lLOBCode = Conversion.CastToType<long>(objReader.GetValue("LINE_OF_BUS_CODE").ToString(), out bSuccess);
                    }

                    Functions.CreateElement(objXmlElement, "Claimant", ref objChildElement, m_iClientId);

                    sClaimId = "-1";
                    sClaimNumber = "-1";
                    sClaimantFirstName = "-1";
                    sClaimantLastName = "-1";
                    lClaimantRowID = -1L;

                    objChildElement.SetAttribute("ClaimID", sClaimId);
                    objChildElement.SetAttribute("ClaimantRowId", lClaimantRowID.ToString());
                    objChildElement.SetAttribute("ClaimNumber", sClaimNumber);
                    objChildElement.SetAttribute("ClaimantFirstName", sClaimantFirstName);
                    objChildElement.SetAttribute("ClaimantLastName", sClaimantLastName);
                    objChildElement.SetAttribute("LOB", lLOBCode.ToString());
                }


                return objXmlDoc;
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaimant.XmlError", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaimant.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                sSQL = null;
                if (m_objLocalCache != null)
                {
                    m_objLocalCache.Dispose();
                    m_objLocalCache = null;
                }
            }

        }

        /// <summary>
        /// Get the claimant name. 
        /// If primary claimant exists, get its name
        /// If no primary claimant and more than one, get the last claimant's name
        /// </summary>
        /// <param name="p_sClaimId">claim ID</param>
        /// <returns></returns>
        private string GetClaimantName(string p_sClaimId)
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            string sClaimantName = string.Empty;
            int iClaimantEID = 0;

            try
            {
                //try to retrieve the primary claimant
                sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE (CLAIM_ID=" + p_sClaimId + ") AND (PRIMARY_CLMNT_FLAG <> 0) AND (PRIMARY_CLMNT_FLAG IS NOT NULL)";
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader.Read())
                {
                    iClaimantEID = objDbReader.GetInt("CLAIMANT_EID");
                }
                objDbReader.Close();
                objDbReader = null;

                //If no primary claimant found, get the latest claimant
                if (iClaimantEID == 0)
                {
                    sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID=" + p_sClaimId + " ORDER BY CLAIMANT_ROW_ID DESC ";
                    objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objDbReader.Read())
                    {
                        iClaimantEID = objDbReader.GetInt("CLAIMANT_EID");
                    }
                    objDbReader.Close();
                    objDbReader = null;
                }

                //retrieve the claimant's name
                if (iClaimantEID != 0)
                {
                    sSQL = "SELECT FIRST_NAME, LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iClaimantEID.ToString();
                    objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objDbReader.Read())
                    {
                        sClaimantName = objDbReader.GetString("LAST_NAME").Trim();
                        string sFirstName = string.Empty;
                        sFirstName = objDbReader.GetString("FIRST_NAME").Trim();
                        if (sFirstName != string.Empty)
                        {
                            sClaimantName += ", " + sFirstName;
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                string sError = objException.Message;
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Close();
            }

            return sClaimantName;
        }

        /// <summary>
        /// Get the claimant name. 
        /// For a Claimant Id 
        /// Created by gbindra for MITS#34104 WWIG GAP15
        /// </summary>
        /// <param name="p_sClaimantId">claimant ID</param>
        /// <returns></returns>
        private string GetClaimantName(int p_iClaimantId)
        {
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            string sClaimantName = string.Empty;

            try
            {
                //try to retrieve the primary claimant
                sSQL = "SELECT ISNULL(FIRST_NAME,'') + ' ' + ISNULL(LAST_NAME,'') AS CLAIMANT_NAME FROM ENTITY WHERE ENTITY_ID IN (SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIMANT_ROW_ID = " + p_iClaimantId.ToString() + " )";
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader.Read())
                {
                    sClaimantName = objDbReader.GetString("CLAIMANT_NAME");
                }
                else
                    sClaimantName = " ";
            }
            catch (Exception objException)
            {
                string sError = objException.Message;
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Close();
                objDbReader = null;

            }
            return sClaimantName;
        }
        /// Name		: CreateFilterString
        /// Author		: Raman Bhatia
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is used to create the filter string of advanced search
        /// </summary>

        // Changed by akaushik5 for MITS 30789 Starts
        //p_sClaimantId: added by gbindra MITS#34104  WWIG GAP15 02122014
        // public string CreateFilterString(string p_sClaimIDList , string p_sEnteredByList , string p_sNoteTypeList ,string p_sUserTypeList , string p_sActivityFromDate , string p_sActivityToDate , string p_sNotesTextContains , string p_sSortBy) 
        public string CreateFilterString(string p_sClaimIDList, string p_sEnteredByList, string p_sNoteTypeList, string p_sUserTypeList, string p_sActivityFromDate, string p_sActivityToDate, string p_sNotesTextContains, string p_sSortBy, string p_sSubjectList, string p_sOrderBy, int p_iClaimantId)
        // Changed by akaushik5 for MITS 30789 Ends
        {
            string sFilterSQL = "";
            string sDefaultOrderBy = "";
            SysSettings objSysSettings = null; //added by gbindra MITS#34104 WWIG GAP15

            try
            {
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId); //added by gbindra MITS#34104 WWIG GAP15
                //Added by Amitosh for mits 23473 (05/11/2011)
                if (!string.IsNullOrEmpty(p_sSortBy))
                // Changed by akaushik5 for MITS 30789 Starts
                //p_sSortBy = p_sSortBy.Replace('|', ',');
                {
                    string[] sortBy = p_sSortBy.Split('|');
                    string[] orderBy = new string[sortBy.Length];
                    string tempSortBy = string.Empty;

                    if (!string.IsNullOrEmpty(p_sOrderBy))
                    {
                        orderBy = p_sOrderBy.Split('|');
                    }

                    for (int i = 0; i < sortBy.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(sortBy[i]))
                        {
                            if (sortBy[i].Contains(','))
                            {
                                string[] subSortBy = sortBy[i].Split(',');

                                foreach (string str in subSortBy)
                                {
                                    tempSortBy += string.Format("{0} {1},", str, orderBy[i]);
                                }
                            }
                            else
                            {
                                tempSortBy += string.Format("{0} {1},", sortBy[i], orderBy[i]);
                            }
                        }
                    }

                    p_sSortBy = !string.IsNullOrEmpty(tempSortBy) ? tempSortBy.EndsWith(",") ? tempSortBy.Substring(0, tempSortBy.Length - 1) : tempSortBy : string.Empty;
                }
                // Changed by akaushik5 for MITS 30789 Ends
                sDefaultOrderBy = " ORDER BY DATE_CREATED DESC, TIME_CREATED DESC";

                if (!string.IsNullOrEmpty(p_sClaimIDList))
                {
                    sFilterSQL = " AND CLAIM_ID IN (" + p_sClaimIDList + ")";
                    m_bClaimSelected = true;
                }

                if (!string.IsNullOrEmpty(p_sEnteredByList))
                    sFilterSQL = sFilterSQL + " AND ENTERED_BY IN (" + p_sEnteredByList + ")";

                if (!string.IsNullOrEmpty(p_sNoteTypeList))
                    sFilterSQL = sFilterSQL + " AND NOTE_TYPE_CODE IN (" + p_sNoteTypeList + ")";

                if (!string.IsNullOrEmpty(p_sUserTypeList))
                    sFilterSQL = sFilterSQL + " AND USER_TYPE_CODE IN (" + p_sUserTypeList + ")";

                if (!string.IsNullOrEmpty(p_sActivityFromDate))
                    sFilterSQL = sFilterSQL + " AND DATE_ENTERED >= '" + Conversion.GetDate(p_sActivityFromDate) + "'";

                if (!string.IsNullOrEmpty(p_sActivityToDate))
                    sFilterSQL = sFilterSQL + " AND DATE_ENTERED <= '" + Conversion.GetDate(p_sActivityToDate) + "'";

                if (!string.IsNullOrEmpty(p_sNotesTextContains))
                    sFilterSQL = sFilterSQL + " AND NOTE_MEMO_CARETECH LIKE '%" + p_sNotesTextContains.Replace("*", "%").Replace("'", "''") + "%'";
                //zmohammad MITS 30218
                if (!string.IsNullOrEmpty(p_sSubjectList))
                    sFilterSQL = sFilterSQL + " AND SUBJECT LIKE '%" + p_sSubjectList.Replace("*", "%").Replace("'", "''") + "%'";

                /*added by gbindra MITS#34104 WWIG GAP15 02122014 START*/
                if (p_iClaimantId > 0 && objSysSettings.AllowNotesAtClaimant == true)
                {
                    sFilterSQL += " AND CLAIM_PRG_NOTE.ATTACH_TABLE = 'CLAIMANT' AND CLAIM_PRG_NOTE.ATTACH_RECORD_ID= " + p_iClaimantId;
                }
                /*added by gbindra MITS#34104 WWIG GAP15 02122014 END*/
                if (!string.IsNullOrEmpty(p_sSortBy))
                    sFilterSQL = sFilterSQL + " ORDER BY " + p_sSortBy;
                else
                    sFilterSQL = sFilterSQL + sDefaultOrderBy.ToString();

            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.XmlError", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.OnLoad.Error", m_iClientId), p_objException);
            }
            finally
            {

            }
            return sFilterSQL;

        }

        #endregion

        #region Save Notes
        /// <summary>
        /// returns the claimProgressNoteId generated 
        /// created by: Parijat For R5
        /// </summary>
        /// <param name="p_iEventID"></param>
        /// <param name="p_iClaimID"></param>
        /// <param name="p_iEnteredBy"></param>
        /// <param name="p_sDateEntered"></param>
        /// <param name="p_iNoteTypeCode"></param>
        /// <param name="p_iUserID"></param>
        /// <param name="p_sNoteMemo"></param>
        /// <param name="p_sNoteMemoCareTech"></param>
        /// <param name="p_sEnteredByName"></param>
        /// <param name="p_iClaimProgressNoteId"></param>
        /// <returns></returns>
        // Changed by GBINDRA MITS#34104 02102014 WWIG GAP15 added p_sAttachRecodId and p_sAttachTableName
        //iTemplateId:Added by Rakhi for R6-Progress Notes Templates
        //public int SaveNotesIntegrate(int p_iEventID, int p_iClaimID, int p_iEnteredBy, string p_sDateEntered, int p_iNoteTypeCode, int p_iUserID, string p_sNoteMemo, string p_sNoteMemoCareTech, string p_sEnteredByName, int p_iClaimProgressNoteId)
        //iPolicyID:Added by Neha for williams Policy Enhanced Notes:MITS 21704
        //public int SaveNotesIntegrate(int p_iEventID, int p_iClaimID, int p_iEnteredBy, string p_sDateEntered,string p_sSubject, int p_iNoteTypeCode, int p_iUserID, string p_sNoteMemo, string p_sNoteMemoCareTech, string p_sEnteredByName, int p_iClaimProgressNoteId, int iTemplateId, int p_iPolicyId, ref BusinessAdaptorErrors p_objErrOut)
        public int SaveNotesIntegrate(int p_iEventID, int p_iClaimID, int p_iEnteredBy, string p_sDateEntered, string p_sSubject, int p_iNoteTypeCode, int p_iUserID, string p_sNoteMemo, string p_sNoteMemoCareTech, string p_sEnteredByName, int p_iClaimProgressNoteId, int iTemplateId, int p_iPolicyId, string p_sAttachTableName, int p_iAttachRecodId, ref BusinessAdaptorErrors p_objErrOut)
        {
            ClaimProgressNote objClaimProgressNotes = null;
            //Changes for MITS 12334 : Start
            //SysSettings objSysSettings = null; mini for performance
            //objSysSettings = new SysSettings(m_sConnectionString); mini for performance
            //Changes for MITS 12334 : End
            try
            {
                //Get ClaimProgressNotes instance from DataModel
                objClaimProgressNotes = (ClaimProgressNote)m_objDataModelFactory.GetDataModelObject("ClaimProgressNote", false);
                objClaimProgressNotes.FiringScriptFlag = 2;

                if (p_iClaimProgressNoteId > 0)
                {
                    objClaimProgressNotes.MoveTo(p_iClaimProgressNoteId);
                }
                else
                    p_iClaimProgressNoteId = 0;

                //Add if condition by kuladeep for mits:25414 --12/07/2011 Start
                if (objClaimProgressNotes.ClaimId == 0)
                {
                    objClaimProgressNotes.ClaimId = p_iClaimID;
                }
                //Add if condition by kuladeep for mits:25414 --12/07/2011 End

                objClaimProgressNotes.EventId = p_iEventID;
                //williams-neha goel-start:MITS 21704
                objClaimProgressNotes.PolicyId = p_iPolicyId;
                //williams-neha goel-end:MITS 21704
                objClaimProgressNotes.NoteTypeCode = p_iNoteTypeCode;

                //Changes for MITS 12334 : Start
                //objClaimProgressNotes.NoteMemoCareTech = p_sNoteMemoCareTech;                               

                if (p_sNoteMemoCareTech != "")
                {
                    //rsolanki2: mits 25152 (june 24 )
                    //The same code is used at "View all notes" and "print notes"
                    //I've added the extra newline in the note for the FreezeExistingText mode.
                    //This resolves the issue where the text was  getting clubbed together in the PDF generated by "print note" button. 
                    // However, now, if the same text is viewed using the "View all notes" button on the browser., 
                    // It will displays an extra row after every note there.

                    if (objClaimProgressNotes.Context.InternalSettings.SysSettings.FreezeExistingNotes == -1)
                        //if (objSysSettings.FreezeExistingNotes == -1) commented mini for performance
                        objClaimProgressNotes.NoteMemoCareTech += p_sNoteMemoCareTech + Environment.NewLine;
                    else
                        objClaimProgressNotes.NoteMemoCareTech = p_sNoteMemoCareTech;
                }

                //Changes for MITS 12334 : End
                //MITS 9103 Remove the div tag which get from copy/paste another note
                string sSearchedString = "id=\"NoteMemoDiv\"";
                //Changes for MITS 12334 : Start
                //objClaimProgressNotes.NoteMemo = RemoveSpecialDivID(p_sNoteMemo, "div", sSearchedString);
                // if (objSysSettings.FreezeExistingNotes == -1) // commented mini for performance
                if (objClaimProgressNotes.Context.InternalSettings.SysSettings.FreezeExistingNotes == -1)
                {
                    if (p_sNoteMemo != "")
                    {
                        if (objClaimProgressNotes.NoteMemo != "")
                            objClaimProgressNotes.NoteMemo += "<br /><br />  ";
                        //objClaimProgressNotes.NoteMemo += DateTime.Now + " (" + m_sUserName + ")  ";
                        objClaimProgressNotes.NoteMemo += RemoveSpecialDivID(p_sNoteMemo, "div", sSearchedString);
                    }
                }
                else
                {
                    if (objClaimProgressNotes.NoteMemo != "")
                        objClaimProgressNotes.NoteMemo = "<br /> <br />";
                    objClaimProgressNotes.NoteMemo = RemoveSpecialDivID(p_sNoteMemo, "div", sSearchedString);
                }
                //Changes for MITS 12334 : End
                objClaimProgressNotes.Subject = p_sSubject;//zmohammad MITS 30218
                if (p_iClaimProgressNoteId == 0)
                {
                    objClaimProgressNotes.UserTypeCode = GetGroupID(p_iUserID);
                    objClaimProgressNotes.DateEntered = Conversion.GetDate(p_sDateEntered);
                    objClaimProgressNotes.DateCreated = System.DateTime.Now.Date.ToString();
                    objClaimProgressNotes.TimeCreated = System.DateTime.Now.TimeOfDay.ToString();

                    //MITS 8784..Enhanced Notes should be editable.. Raman Bhatia
                    //Moving following code to above if condition
                    //objClaimProgressNotes.NoteMemoCareTech = p_sNoteMemoCareTech;

                    /* 
                    //MITS 9103 Remove the div tag which get from copy/paste another note
                    string sSearchedString = "id=\"NoteMemoDiv\"";
                    objClaimProgressNotes.NoteMemo = RemoveSpecialDivID(p_sNoteMemo, "div", sSearchedString);
                     */

                    objClaimProgressNotes.EnteredBy = p_iEnteredBy;
                    objClaimProgressNotes.EnteredByName = p_sEnteredByName;
                }
                //R6: Rakhi: Notepad Screens Templates - start
                objClaimProgressNotes.TemplateId = iTemplateId;
                //R6: Rakhi: Notepad Screens Templates - start
                //MITS 24745 Raman
                /*Added by GBINDRA MITS#34104 02102014 WWIG GAP15 START*/
                objClaimProgressNotes.NotesAttachedTable = p_sAttachTableName;
                objClaimProgressNotes.AttachRecordId = p_iAttachRecodId;
                /*Added by GBINDRA MITS#34104 02102014 WWIG GAP15 END*/
                if (!(string.IsNullOrEmpty(objClaimProgressNotes.NoteMemo)))
                {
                    objClaimProgressNotes.Save();
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("ProgressNotesManager.BlankNotes.Error", m_iClientId));

                }
                foreach (System.Collections.DictionaryEntry objError in objClaimProgressNotes.Context.ScriptWarningErrors)
                    p_objErrOut.Add("WarningScriptError", objError.Value.ToString(), BusinessAdaptorErrorType.Warning);

                return objClaimProgressNotes.ClaimProgNoteId;
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.XmlError", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                foreach (System.Collections.DictionaryEntry objError in objClaimProgressNotes.Context.ScriptValidationErrors)
                    p_objErrOut.Add("ValidationScriptError", objError.Value.ToString(), BusinessAdaptorErrorType.Error);
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveNotes.Error", m_iClientId), p_objException);
            }
            finally
            {
                objClaimProgressNotes = null;
            }
        }

//mbahl3 cloud changes for mobile
        public void GetNotesDateTimeForMobile(int iNotesId,ref string sCreateDate,ref string sCreateTime)
        {
            ClaimProgressNote objClaimProgressNotes = null;
            try
            {
                objClaimProgressNotes = (ClaimProgressNote)m_objDataModelFactory.GetDataModelObject("ClaimProgressNote", false);
                objClaimProgressNotes.MoveTo(iNotesId);
                sCreateDate = Conversion.GetDBDateFormat(objClaimProgressNotes.DateCreated, "d");
                sCreateTime = Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr(objClaimProgressNotes.TimeCreated), "t");

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.NotesDatetime.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objClaimProgressNotes != null)
                {
                    objClaimProgressNotes.Dispose();
                    objClaimProgressNotes = null;
                }
            }
            //mbahl3 catch code added 
        }
		//mbahl3 cloud changes for mobile

        /// Name		: SaveNotes
        /// Author		: Raman Bhatia
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for saving notes
        /// </summary>
        //williams-neha goel--start:added iPolicyID for policy enhanced notes:MITS 21704
        //public void SaveNotes(int p_iEventID , int p_iClaimID , int p_iEnteredBy , string p_sDateEntered , int p_iNoteTypeCode , int p_iUserID , string p_sNoteMemo , string p_sNoteMemoCareTech , string p_sEnteredByName , int p_iClaimProgressNoteId)
        public void SaveNotes(int p_iEventID, int p_iClaimID, int p_iEnteredBy, string p_sDateEntered, int p_iNoteTypeCode, int p_iUserID, string p_sNoteMemo, string p_sNoteMemoCareTech, string p_sEnteredByName, int p_iClaimProgressNoteId, int p_iPolicyID)
        {
            ClaimProgressNote objClaimProgressNotes = null;
            //Changed by Gagan for MITS 12334 : Start
            // SysSettings objSysSettings = null; commented mini for performance
            // objSysSettings = new SysSettings(m_sConnectionString); commented mini for performance
            //Changed by Gagan for MITS 12334 : End
            try
            {
                //Get ClaimProgressNotes instance from DataModel
                objClaimProgressNotes = (ClaimProgressNote)m_objDataModelFactory.GetDataModelObject("ClaimProgressNote", false);

                if (p_iClaimProgressNoteId > 0)
                {
                    objClaimProgressNotes.MoveTo(p_iClaimProgressNoteId);
                }
                else
                    p_iClaimProgressNoteId = 0;

                objClaimProgressNotes.ClaimId = p_iClaimID;
                objClaimProgressNotes.EventId = p_iEventID;
                //williams-neha goel--start:MITS 21704
                objClaimProgressNotes.PolicyId = p_iPolicyID;
                //williams-neha goel--end:MITS 21704
                objClaimProgressNotes.NoteTypeCode = p_iNoteTypeCode;

                //Changed by Gagan for MITS 12334 : Start
                //objClaimProgressNotes.NoteMemoCareTech = p_sNoteMemoCareTech;                               

                if (p_sNoteMemoCareTech != "")
                {
                    if (objClaimProgressNotes.Context.InternalSettings.SysSettings.FreezeExistingNotes == -1)
                        // if (objSysSettings.FreezeExistingNotes == -1) commented mini for performance
                        //      objClaimProgressNotes.NoteMemoCareTech += DateTime.Now + " (" + m_sUserName + ")  " + p_sNoteMemoCareTech;
                        objClaimProgressNotes.NoteMemoCareTech += p_sNoteMemoCareTech;
                    else
                        objClaimProgressNotes.NoteMemoCareTech = p_sNoteMemoCareTech;
                }

                //Changed by Gagan for MITS 12334 : End

                //MITS 9103 Remove the div tag which get from copy/paste another note
                string sSearchedString = "id=\"NoteMemoDiv\"";
                //Changed by Gagan for MITS 12334 : Start
                if (objClaimProgressNotes.Context.InternalSettings.SysSettings.FreezeExistingNotes == -1)
                // if (objSysSettings.FreezeExistingNotes == -1) commented mini for performance
                {
                    if (p_sNoteMemo != "")
                    {
                        if (objClaimProgressNotes.NoteMemo != "")
                            objClaimProgressNotes.NoteMemo += "<br /><br />  ";
                        //objClaimProgressNotes.NoteMemo += DateTime.Now + " (" + m_sUserName + ")  ";
                        objClaimProgressNotes.NoteMemo += RemoveSpecialDivID(p_sNoteMemo, "div", sSearchedString);
                    }
                }
                else
                {
                    if (objClaimProgressNotes.NoteMemo != "")
                        objClaimProgressNotes.NoteMemo = "<br /> <br />";
                    objClaimProgressNotes.NoteMemo = RemoveSpecialDivID(p_sNoteMemo, "div", sSearchedString);
                }
                //Changed by Gagan for MITS 12334 : End

                if (p_iClaimProgressNoteId == 0)
                {
                    objClaimProgressNotes.UserTypeCode = GetGroupID(p_iUserID);
                    objClaimProgressNotes.DateEntered = Conversion.GetDate(p_sDateEntered);
                    objClaimProgressNotes.DateCreated = System.DateTime.Now.Date.ToString();
                    objClaimProgressNotes.TimeCreated = System.DateTime.Now.TimeOfDay.ToString();

                    //MITS 8784..Enhanced Notes should be editable.. Raman Bhatia
                    //Moving following code to above if condition
                    //objClaimProgressNotes.NoteMemoCareTech = p_sNoteMemoCareTech;

                    /*
                    //MITS 9103 Remove the div tag which get from copy/paste another note
                    string sSearchedString = "id=\"NoteMemoDiv\"";
                    objClaimProgressNotes.NoteMemo = RemoveSpecialDivID(p_sNoteMemo, "div", sSearchedString);
                     */

                    objClaimProgressNotes.EnteredBy = p_iEnteredBy;
                    objClaimProgressNotes.EnteredByName = p_sEnteredByName;
                }
                objClaimProgressNotes.Save();
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.XmlError", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveNotes.Error", m_iClientId), p_objException);
            }
            finally
            {
                objClaimProgressNotes = null;
            }
        }

        /// <summary>
        /// Remove <div id="NoteMemoDiv"></div> which comes from copy/paste of 
        /// another enhanced note.
        /// </summary>
        /// <param name="sInput">input string</param>
        /// <param name="sSearchedAttribute">the attribute name and value to be searched</param>
        /// <param name="sTag">Html tab name</param>
        /// <returns></returns>
        private string RemoveSpecialDivID(string sInput, string sTag, string sSearchedAttribute)
        {
            string sOutput = sInput;

            //Check if the searched string exists
            if (sInput.IndexOf(sSearchedAttribute) < 0)
                return sOutput;

            ArrayList oBeginTagList = new ArrayList();

            //Create regular expression for begin/end tag
            string sEndTag = "</" + sTag + ">";
            int iEngTagLength = sEndTag.Length;
            Regex rDivBegin = new Regex("<" + sTag + "[^<]*>");
            Regex rDivEnd = new Regex(sEndTag, RegexOptions.RightToLeft);

            //Search for begin tag and end tag
            try
            {
                MatchCollection mcBegin = rDivBegin.Matches(sInput);
                MatchCollection mcEnd = rDivEnd.Matches(sInput);
                if (mcBegin.Count == mcEnd.Count)
                {
                    for (int iBegin = 0; iBegin < mcBegin.Count; iBegin++)
                    {
                        Match mBegin = mcBegin[iBegin];
                        string sBegin = mBegin.ToString();

                        //If the begin tag contains searched string
                        if (sBegin.IndexOf(sSearchedAttribute) >= 0)
                        {
                            oBeginTagList.Add(sBegin);

                            //Remove the end tag
                            Match mEnd = mcEnd[iBegin];
                            if (mEnd.Groups.Count > 0)
                            {
                                Group gEnd = mEnd.Groups[0];
                                CaptureCollection cc = gEnd.Captures;
                                if (cc.Count > 0)
                                {
                                    Capture c = cc[0];
                                    int iStartIndex = c.Index;
                                    sOutput = sOutput.Remove(iStartIndex, iEngTagLength);
                                }
                            }
                        }
                    }

                    //Remove the begin tag
                    for (int i = 0; i < oBeginTagList.Count; i++)
                    {
                        string sTagString = oBeginTagList[i].ToString();
                        sOutput = sOutput.Replace(sTagString, "");
                    }
                }

                return sOutput;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.XmlError", m_iClientId), p_objException);
            }
            finally
            {
                oBeginTagList = null;
            }
        }

        /// Name		: GetGroupID
        /// Author		: Raman Bhatia
        /// Date Created: 4 Oct 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used to fetch the user group ID
        /// </summary>

        private int GetGroupID(int p_iUserID)
        {
            int iGroupID = 0;
            string sSQL = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT GROUP_ID from USER_MEMBERSHIP WHERE USER_ID =" + p_iUserID.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iGroupID = Conversion.ConvertObjToInt(objReader.GetValue("GROUP_ID"), m_iClientId);
                    }
                    objReader.Close();
                }
                return iGroupID;
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.XmlError", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetGroupID.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
            }

        }
        #endregion

        #region Get Note Details

        /// <summary>
        /// This Function would fetch text and html contents for a given progress note
        /// </summary>
        /// <param name="p_iClaimProgressNoteId"></param>
        /// <returns></returns>
        public XmlDocument GetNoteDetails(int p_iClaimProgressNoteId)
        {
            string sSQL = "";
            DbReader objReader = null;
            string sText = string.Empty;
            string sHTML = string.Empty;
            //Ashish Ahuja - Mits 33124
            string subject = string.Empty;
            int noteTypeCode = 0;
            string sNoteTypeDesc = string.Empty;
            int templateId = 0;
            string sTemplateName = string.Empty;
            string sActivityDate = string.Empty;
            string sDataChangedByInitScript = Boolean.FalseString;
            XmlDocument outXML = null;
            XmlElement oParentEle = null;
            XmlElement oChildEle = null;
            DateTime dt;

            Riskmaster.DataModel.ClaimProgressNote objClaimProgressNotes = null;

            LocalCache objLocalCache = null;

            ////rsolanki2 : mits  20796
            //HtmlAgilityPack.HtmlDocument objHtmDoc = new HtmlAgilityPack.HtmlDocument();
            //objHtmDoc.OptionFixNestedTags = true;
            //System.IO.Stream objStream;
            int iLangCode = 0;  //Aman ML Change                  

            try
            {
                iLangCode = m_UserLogin.objUser.NlsCode;  //Aman ML Change
                //Ashish Ahuja - Mits 33124 - added subject column in query
                //Changed by Amitosh for mits 23691 (05/11/2011)
                //sSQL = "SELECT NOTE_MEMO, NOTE_MEMO_CARETECH, NOTE_TYPE_CODE FROM CLAIM_PRG_NOTE WHERE CL_PROG_NOTE_ID =" + p_iClaimProgressNoteId.ToString();
                sSQL = "SELECT NOTE_MEMO, NOTE_MEMO_CARETECH, NOTE_TYPE_CODE,USER_TYPE_CODE,DATE_CREATED,TIME_CREATED,SUBJECT FROM CLAIM_PRG_NOTE WHERE CL_PROG_NOTE_ID =" + p_iClaimProgressNoteId.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        sText = Conversion.ConvertObjToStr(objReader.GetValue("NOTE_MEMO_CARETECH"));
                        sHTML = Conversion.ConvertObjToStr(objReader.GetValue("NOTE_MEMO"));
                        noteTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("NOTE_TYPE_CODE"), m_iClientId);
                        //Ashish Ahuja - Mits 33124
                        subject = Conversion.ConvertObjToStr(objReader.GetValue("SUBJECT"));

                        //rsolanki2: moving the code section to UI layer

                        //rsolanki2 : mits  20796
                        //objStream = new System.IO.MemoryStream
                        //    (ASCIIEncoding.Default.GetBytes
                        //        (Conversion.ConvertObjToStr
                        //            (objReader.GetValue("NOTE_MEMO_CARETECH"))));
                        //objHtmDoc.Load(objStream);
                        //objStream.Close();
                        //sText = objHtmDoc.DocumentNode.InnerText;

                        ////rsolanki2 : mits  20796
                        //objStream = new System.IO.MemoryStream
                        //    (ASCIIEncoding.Default.GetBytes
                        //        (Conversion.ConvertObjToStr
                        //            (objReader.GetValue("NOTE_MEMO"))));
                        //objHtmDoc.Load(objStream);
                        //objStream.Close();

                        //sHTML = objHtmDoc.DocumentNode.InnerHtml;
                    }
                    objReader.Close();
                }

                objClaimProgressNotes = (ClaimProgressNote)m_objDataModelFactory.GetDataModelObject("ClaimProgressNote", false);

                if (p_iClaimProgressNoteId == 0)
                {
                    objClaimProgressNotes = (ClaimProgressNote)m_objDataModelFactory.GetDataModelObject("ClaimProgressNote", false);
                    objClaimProgressNotes.FiringScriptFlag = 2;
                    objClaimProgressNotes.InitializeScriptData();
                    if (objClaimProgressNotes.DataChanged)
                    {
                        sDataChangedByInitScript = Boolean.TrueString;
                        sText = objClaimProgressNotes.NoteMemoCareTech;
                        sHTML = objClaimProgressNotes.NoteMemo;
                        noteTypeCode = objClaimProgressNotes.NoteTypeCode;
                        sNoteTypeDesc = objClaimProgressNotes.Context.LocalCache.GetShortCode(noteTypeCode) + " " + objClaimProgressNotes.Context.LocalCache.GetCodeDesc(noteTypeCode, iLangCode);  //Aman ML Change
                        templateId = objClaimProgressNotes.TemplateId;
                        if (templateId != 0)
                        {
                            sSQL = "SELECT TEMPLATE_NAME FROM CLAIM_PRG_NOTE_TEMPLATES WHERE CL_PRGN_TEMP_ID = " + templateId.ToString();
                            using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                            {
                                if (objReader != null && objReader.Read())
                                {
                                    sTemplateName = objReader[0].ToString();
                                }
                            }
                        }
                        sActivityDate = objClaimProgressNotes.DateEntered;
                        if (sActivityDate != string.Empty)
                        {
                            dt = Conversion.ToDate(sActivityDate);
                            sActivityDate = dt.ToString("MM/dd/yyyy");
                        }

                    }
                }
                else
                {
                    objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
                    //Aman ML Change
                    //sNoteTypeDesc = objLocalCache.GetShortCode(noteTypeCode) + " " + objLocalCache.GetCodeDesc(noteTypeCode);
                    sNoteTypeDesc = objLocalCache.GetShortCode(noteTypeCode) + " " + objLocalCache.GetCodeDesc(noteTypeCode, iLangCode); //Aman ML Change
                }

                outXML = new XmlDocument();
                oParentEle = outXML.CreateElement("Document");
                oParentEle.SetAttribute("ProgressNoteID", p_iClaimProgressNoteId.ToString());
                outXML.AppendChild(oParentEle);
                oChildEle = outXML.CreateElement("Text");
                oChildEle.InnerText = sText;
                oParentEle.AppendChild(oChildEle);
                oChildEle = outXML.CreateElement("HTML");
                oChildEle.InnerText = sHTML;
                oParentEle.AppendChild(oChildEle);
                oChildEle = outXML.CreateElement("NoteTypeCode");
                oChildEle.InnerText = noteTypeCode.ToString();
                oParentEle.AppendChild(oChildEle);
                oChildEle = outXML.CreateElement("NoteTypeDesc");
                oChildEle.InnerText = sNoteTypeDesc;
                oParentEle.AppendChild(oChildEle);
                oChildEle = outXML.CreateElement("TemplateId");
                oChildEle.InnerText = templateId.ToString();
                oParentEle.AppendChild(oChildEle);
                oChildEle = outXML.CreateElement("TemplateName");
                oChildEle.InnerText = sTemplateName;
                oParentEle.AppendChild(oChildEle);
                oChildEle = outXML.CreateElement("ActivityDate");
                oChildEle.InnerText = sActivityDate;
                oParentEle.AppendChild(oChildEle);
                oChildEle = outXML.CreateElement("DataChangedByInitScript");
                oChildEle.InnerText = sDataChangedByInitScript;
                oParentEle.AppendChild(oChildEle);
                //Ashish Ahuja - Mits 33124 -start
                oChildEle = outXML.CreateElement("Subject");
                oChildEle.InnerText = subject;
                oParentEle.AppendChild(oChildEle);
                //Ashish Ahuja - Mits 33124-end

            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetNoteDetails.Error", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetNoteDetails.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;

                //if (objStream!=null)
                //{
                //     objStream.Dispose();
                //}
            }
            return outXML;
        }
        #endregion
        #region Enhanced Notes Templates

        /// <summary>
        /// Gets the list of all existing templates
        /// </summary>
        /// <returns>XmlDocument</returns>
        public void LoadTemplates(ref ProgressNoteTemplates outDocument)
        {
            DbReader objreader = null;
            Riskmaster.Models.ProgressNoteTemplates objProgressNoteTemplates = new ProgressNoteTemplates();
            string sSql = string.Empty;
            //outDocument = new ProgressNoteTemplates();   //pmittal5 Mits 21514
            string sFullName = string.Empty;    //MGaba2:MITS 22552
            try
            {
                sSql = "SELECT * FROM CLAIM_PRG_NOTE_TEMPLATES ORDER BY DTTM_RCD_LAST_UPD DESC";
                objreader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                while (objreader.Read())
                {
                    objProgressNoteTemplates = new ProgressNoteTemplates();
                    objProgressNoteTemplates.TemplateId = objreader.GetValue("CL_PRGN_TEMP_ID").ToString();
                    objProgressNoteTemplates.TemplateName = objreader.GetValue("TEMPLATE_NAME").ToString();
                    objProgressNoteTemplates.TemplateMemo = objreader.GetValue("TEMPLATE_MEMO").ToString();
                    objProgressNoteTemplates.DateAdded = objreader.GetValue("DTTM_RCD_ADDED").ToString();
                    objProgressNoteTemplates.DateUpdated = objreader.GetValue("DTTM_RCD_LAST_UPD").ToString();
                    objProgressNoteTemplates.AddedBy = objreader.GetValue("ADDED_BY_USER").ToString();
                    //MGaba2:MITS 22552:Entered By field should display the Full user name in case of templates              
                    if (PublicFunctions.GetFullNameFromLoginName(objreader.GetString("UPDATED_BY_USER"), out sFullName, m_iClientId))
                    {
                        objProgressNoteTemplates.UpdatedBy = sFullName;
                    }
                    else
                    {
                        objProgressNoteTemplates.UpdatedBy = objreader.GetValue("UPDATED_BY_USER").ToString();
                    }
                    //objProgressNoteTemplates.objProgressNoteTemplateList.Add(objProgressNoteTemplates);
                    outDocument.objProgressNoteTemplateList.Add(objProgressNoteTemplates);
                }

            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetTemplates.Error", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetTemplates.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objreader != null)
                {
                    objreader.Close();
                    objreader.Dispose();
                }
            }
        }
        /// <summary>
        /// Save the templates
        /// </summary>
        public string SaveTemplates(int p_iTemplateId, string p_sTemplateName, string p_sTemplateMemo, string p_sAddedBy, string p_sUpdatedBy, string p_sDateAdded, string p_sDateUpdated)
        {
            StringBuilder sSql = new StringBuilder(string.Empty);
            DbConnection objConn = null;
            DbReader objreader = null;
            bool bResult = false;
            int iCount = 0;
            try
            {
                p_sTemplateMemo = p_sTemplateMemo.Replace("'", "''");
                p_sTemplateName = p_sTemplateName.Replace("'", "''");

                if (p_iTemplateId == 0)
                {
                    sSql.Append(" SELECT MAX(CL_PRGN_TEMP_ID) TEMPLATE_ID FROM CLAIM_PRG_NOTE_TEMPLATES");
                    objreader = DbFactory.GetDbReader(m_sConnectionString, sSql.ToString());
                    if (objreader.Read())
                    {
                        int ordinal = objreader.GetOrdinal("TEMPLATE_ID");
                        int newTemplateId = 1;
                        if (!objreader.IsDBNull(ordinal))
                        {
                            newTemplateId = Convert.ToInt32(objreader.GetValue(ordinal)) + 1;
                        }
                        p_iTemplateId = newTemplateId;
                    }
                    else
                    {
                        p_iTemplateId = 1;//let the new row index start from 1
                    }
                    objreader.Close();
                    sSql = new StringBuilder(string.Empty);
                    sSql.Append("SELECT COUNT(*) FROM CLAIM_PRG_NOTE_TEMPLATES ");
                    sSql.Append(" WHERE TEMPLATE_NAME ='" + p_sTemplateName.Trim() + "'"); //Mits 18902:Added .Trim() 
                    iCount = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnectionString, sSql.ToString()).ToString(), out bResult);
                    if (iCount > 0)
                    {
                        throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveTemplates.DuplicateKey", m_iClientId), new Exception());
                    }
                    sSql = new StringBuilder(string.Empty);
                    sSql.Append(" INSERT INTO CLAIM_PRG_NOTE_TEMPLATES VALUES ");
                    sSql.Append("(" + p_iTemplateId + ",'" + p_sTemplateName.Trim() + "','"); //Mits 18902:Added .Trim() 
                    sSql.Append(p_sTemplateMemo + "','" + p_sAddedBy + "','");
                    sSql.Append(p_sUpdatedBy + "','");
                    sSql.Append(Conversion.GetDateTime(DateTime.Now.ToString()) + "','");
                    sSql.Append(Conversion.GetDateTime(DateTime.Now.ToString()) + "')");
                }
                else
                {

                    sSql.Append("SELECT COUNT(*) FROM CLAIM_PRG_NOTE_TEMPLATES ");
                    sSql.Append(" WHERE TEMPLATE_NAME ='" + p_sTemplateName.Trim() + "'"); //Mits 18902:Added .Trim() 
                    sSql.Append(" AND CL_PRGN_TEMP_ID <>'" + p_iTemplateId + "'");
                    iCount = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnectionString, sSql.ToString()).ToString(), out bResult);
                    if (iCount > 0)
                    {
                        throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveTemplates.DuplicateKey", m_iClientId), new Exception());
                    }
                    sSql = new StringBuilder(string.Empty);
                    sSql.Append("UPDATE CLAIM_PRG_NOTE_TEMPLATES SET ");
                    sSql.Append(" TEMPLATE_NAME = '" + p_sTemplateName.Trim() + "',"); //Mits 18902:Added .Trim()
                    sSql.Append(" TEMPLATE_MEMO ='" + p_sTemplateMemo + "', ");
                    sSql.Append(" UPDATED_BY_USER = '" + p_sUpdatedBy + "', ");
                    sSql.Append(" DTTM_RCD_LAST_UPD = '" + Conversion.GetDateTime(DateTime.Now.ToString()) + "'");
                    sSql.Append(" WHERE CL_PRGN_TEMP_ID = " + p_iTemplateId);
                }
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSql.ToString());
                return (Convert.ToString(p_iTemplateId));
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveTemplates.Error", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveTemplates.Error", m_iClientId), p_objException);
            }
            finally
            {

                if (objreader != null)
                {
                    objreader.Close();
                    objreader = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn = null;
                }
            }
        }
        /// <summary>
        /// Deletes the template
        /// </summary>
        /// <param name="templateId">integer</param>
        public void DeleteTemplate(int p_iTemplateId)
        {
            string sSQL = string.Empty;
            string sSQL1 = string.Empty;
            DbConnection objConn = null;
            try
            {
                sSQL = "DELETE FROM CLAIM_PRG_NOTE_TEMPLATES WHERE  "
                + " CL_PRGN_TEMP_ID = " + p_iTemplateId;
                //To update Template Id in Notes table if the template is deleted.
                sSQL1 = "UPDATE CLAIM_PRG_NOTE SET TEMPLATE_ID = 0 WHERE  "
                + " TEMPLATE_ID = " + p_iTemplateId;
                //To update Template Id in Notes table if the template is deleted.
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.ExecuteNonQuery(sSQL1);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.DeleteTemplate.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }
        }
        public string GetTemplateDetails(int p_iTemplateId)
        {
            DbReader objreader = null;
            Riskmaster.Models.ProgressNoteTemplates objProgressNoteTemplates = new ProgressNoteTemplates();
            string sSql = string.Empty;
            try
            {
                sSql = "SELECT * FROM CLAIM_PRG_NOTE_TEMPLATES WHERE CL_PRGN_TEMP_ID=" + p_iTemplateId;
                objreader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objreader.Read())
                {
                    objProgressNoteTemplates.TemplateMemo = objreader.GetValue("TEMPLATE_MEMO").ToString();
                }
                return objProgressNoteTemplates.TemplateMemo;

            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetTemplates.Error", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetTemplates.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objreader != null)
                {
                    objreader.Close();
                    objreader.Dispose();
                }
            }

        }
        #endregion
        #region Added Rakhi for R6 Enhanced Notes Settings
        /// <summary>
        /// Gets the order of headers
        /// </summary>
        /// <param name="p_objSettings"></param>
        /// <param name="p_iuserId"></param>
        /// <returns></returns>
        public ProgressNoteSettings GetNotesHeaderOrder(ProgressNoteSettings p_objSettings, int p_iuserId)
        {
            //string[] header = new string[5];
            string[,] header = new string[9, 2];//Deb MITS 30185

            try
            {
                //Deb MITS 30185
                header = GetNotesSettings(p_iuserId);
                p_objSettings.Header1 = header[0, 0];
                p_objSettings.Header2 = header[1, 0];
                p_objSettings.Header3 = header[2, 0];
                p_objSettings.Header4 = header[3, 0];
                p_objSettings.Header5 = header[4, 0];
                //Added by Amitosh for mits 23691(05/11/2011)
                p_objSettings.Header6 = header[5, 0];
                p_objSettings.Header7 = header[6, 0];
                p_objSettings.Header8 = header[7, 0];
                p_objSettings.Header9 = header[8, 0];

                p_objSettings.Header1Text = header[0, 1];
                p_objSettings.Header2Text = header[1, 1];
                p_objSettings.Header3Text = header[2, 1];
                p_objSettings.Header4Text = header[3, 1];
                p_objSettings.Header5Text = header[4, 1];
                p_objSettings.Header6Text = header[5, 1];
                p_objSettings.Header7Text = header[6, 1];
                p_objSettings.Header8Text = header[7, 1];
                p_objSettings.Header9Text = header[8, 1];
                p_objSettings.HeaderKeyShowList = HeaderKeyShowList;
                HeaderKeyShowList = string.Empty;
                //Deb MITS 30185
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetNotesSettings.Error", m_iClientId), p_objEx);
            }
            return (p_objSettings);
        }
        private string[,] GetNotesSettings(int userId)//Deb MITS 30185
        {
            string sSQL = string.Empty;
            string sXml = string.Empty;
            DbReader objReader = null;
            DbCommand objCmd = null;
            DbParameter objParam = null;
            XmlDocument objUserPrefXML = null;
            string sUserPrefsXML = string.Empty;
            //string[] header = new string[5];
            string[,] header = new string[11, 2];//Changed by Amitosh for mits 23691(05/11/2011)//Deb MITS 30185
            try
            {
                sSQL = "SELECT * FROM USER_PREF_XML WHERE USER_ID"
                        + " = '" + userId.ToString() + "'";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                    objReader.Close();
                    sUserPrefsXML = sUserPrefsXML.Trim();
                    objUserPrefXML = new XmlDocument();
                    if (sUserPrefsXML != "")
                    {
                        objUserPrefXML.LoadXml(sUserPrefsXML);
                    }
                    CreateUserPreferXML(objUserPrefXML);
                    string order = string.Empty;
                    //Deb MITS 30185
                    XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/ActivityDate/@Order");
                    order = objNode.Value;
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_ACTIVITY_DATE;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/ActivityHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/Activity").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_ACTIVITY_DATE;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_ACTIVITY_DATE;
                        }
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/AttachedTo/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_ATTACHED_TO;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/AttachedToHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/AttachedTo").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_ATTACHED_TO;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_ATTACHED_TO;
                        }
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/NoteType/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_NOTE_TYPE;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteTypeHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteType").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_NOTE_TYPE;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_NOTE_TYPE;
                        }
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/NoteText/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_NOTE_TEXT;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteTextHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteText").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_NOTE_TEXT;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_NOTE_TEXT;
                        }
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/EnteredBy/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_ENTERED_BY;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/EnteredByHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/EnteredBy").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_ENTERED_BY;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_ENTERED_BY;
                        }
                    }
                    //Added by Amitosh for mits 23691(05/11/2011)
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/DateCreated/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_DATE_CREATED;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/DateCreatedHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/DateCreated").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_DATE_CREATED;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_DATE_CREATED;
                        }
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/TimeCreated/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_TIME_CREATED;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/TimeCreatedHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/TimeCreated").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_TIME_CREATED;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_TIME_CREATED;
                        }
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/UserTypeCode/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_USER_TYPE;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/UserTypeHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/UserType").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_USER_TYPE;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_USER_TYPE;
                        }
                    }
                    //zmohammad MITS 30218
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/Subject/@Order");
                    order = objNode.Value;
                    while (!string.IsNullOrEmpty(header[Convert.ToInt32(order) - 1, 0]))
                    {
                        order = (Convert.ToInt32(10) - 1).ToString();
                    }
                    header[Convert.ToInt32(order) - 1, 0] = HEADER_SUBJECT;
                    header[Convert.ToInt32(order) - 1, 1] = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/SubjectHeader").Attributes["value"].Value;
                    if (objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/Subject").Attributes["selected"].Value == "1")
                    {
                        if (HeaderKeyShowList.Length > 0)
                        {
                            HeaderKeyShowList = HeaderKeyShowList + "|" + HEADER_SUBJECT;
                        }
                        else
                        {
                            HeaderKeyShowList = HEADER_SUBJECT;
                        }
                    }

                    //end Amitosh
                    //Added by Amitosh for mits 23473 (05/11/2011)
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/sortby/@value");
                    if (objNode != null)
                        header[9, 0] = header[9, 1] = objNode.Value;
                    //Deb MITS 30185
                    // Added by akaushik5 for MITS 30789 Starts
                    objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/OrderBy/@value");
                    if (!object.ReferenceEquals(objNode, null))
                    {
                        header[10, 0] = header[10, 1] = objNode.Value;
                    }
                    // Added by akaushik5 for MITS 30789 Ends
                }
                else
                {
                    //Deb MITS 30185
                    header[0, 0] = header[0, 1] = HEADER_ACTIVITY_DATE;
                    header[1, 0] = header[1, 1] = HEADER_ATTACHED_TO;
                    header[2, 0] = header[2, 1] = HEADER_NOTE_TYPE;
                    header[3, 0] = header[3, 1] = HEADER_NOTE_TEXT;
                    header[4, 0] = header[4, 1] = HEADER_ENTERED_BY;
                    //Added by Amitosh for mits 23691(05/11/2011)
                    header[5, 0] = header[5, 1] = HEADER_DATE_CREATED;
                    header[6, 0] = header[6, 1] = HEADER_TIME_CREATED;
                    header[7, 0] = header[7, 1] = HEADER_USER_TYPE;
                    header[8, 0] = header[8, 1] = HEADER_SUBJECT; //zmohammad MITS 30218
                    //end Amitosh
                    //Deb MITS 30185
                    sXml = "<setting></setting>";
                    //sSql = sSql = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + p_iUserId + ",'" + sXml + "')";
                    DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objConn.Open();
                    objCmd = objConn.CreateCommand();
                    objParam = objCmd.CreateParameter();
                    objParam.Direction = ParameterDirection.Input;
                    objParam.Value = sXml;
                    objParam.ParameterName = "XML";
                    objParam.SourceColumn = "PREF_XML";
                    objCmd.Parameters.Add(objParam);
                    sSQL = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + userId + ",~XML~)";
                    //objConn.Open();
                    //objConn.ExecuteNonQuery (sSql);
                    objCmd.CommandText = sSQL;
                    objCmd.ExecuteNonQuery();
                    objConn.Close();
                    objConn.Dispose();
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetNotesSettings.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                objUserPrefXML = null;
            }
            return header;
        }
        /// <summary>
        /// Create the xml with the header information for
        /// Enhanced notes
        /// </summary>
        private void CreateUserPreferXML(XmlDocument p_objUserPrefXML)
        {
            XmlElement objTempNode = null;

            XmlElement objParentNode = null;
            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }

                // Create EnhancedNotes node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                    objTempNode = p_objUserPrefXML.CreateElement("EnhancedNotes");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/ActivityDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("ActivityDate");
                    objTempNode.SetAttribute("Order", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/AttachedTo");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedTo");
                    objTempNode.SetAttribute("Order", "2");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/NoteType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteType");
                    objTempNode.SetAttribute("Order", "3");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/NoteText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteText");
                    objTempNode.SetAttribute("Order", "4");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/EnteredBy");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("EnteredBy");
                    objTempNode.SetAttribute("Order", "5");
                    objParentNode.AppendChild(objTempNode);
                }
                //Added by Amitosh for mits 23691(05/11/2011)
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/DateCreated");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("DateCreated");
                    objTempNode.SetAttribute("Order", "6");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/TimeCreated");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("TimeCreated");
                    objTempNode.SetAttribute("Order", "7");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/UserTypeCode");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("UserTypeCode");
                    objTempNode.SetAttribute("Order", "8");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/Subject");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("Subject");
                    objTempNode.SetAttribute("Order", "9");
                    objParentNode.AppendChild(objTempNode);
                }
                //end Amitosh
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/sortby");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("sortby");
                    objTempNode.SetAttribute("value", sort_by);
                    objParentNode.AppendChild(objTempNode);
                }
                //Deb MITS 30185
                // Added by akaushik5 for MITS 30789 Starts
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes/OrderBy");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//EnhancedNotes");
                    objTempNode = p_objUserPrefXML.CreateElement("OrderBy");
                    objTempNode.SetAttribute("value", this.orderBy);
                    objParentNode.AppendChild(objTempNode);
                }
                // Added by akaushik5 for MITS 30789 Ends
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting");
                    objTempNode = p_objUserPrefXML.CreateElement("ProgNotesConfig");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/Activity");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Activity");
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteText");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/AttachedTo");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedTo");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/EnteredBy");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("EnteredBy");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/DateCreated");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateCreated");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/TimeCreated");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TimeCreated");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/UserType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/Subject");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Subject");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/ActivityHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ActivityHeader");
                    objTempNode.SetAttribute("value", "Activity");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteTypeHeader");
                    objTempNode.SetAttribute("value", "Note Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteTextHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteTextHeader");
                    objTempNode.SetAttribute("value", "Note Text");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/AttachedToHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedToHeader");
                    objTempNode.SetAttribute("value", "Attached To");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/EnteredByHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("EnteredByHeader");
                    objTempNode.SetAttribute("value", "Entered By");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/DateCreatedHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateCreatedHeader");
                    objTempNode.SetAttribute("value", "Date Created");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/TimeCreatedHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TimeCreatedHeader");
                    objTempNode.SetAttribute("value", "Time Created");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/UserTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserTypeHeader");
                    objTempNode.SetAttribute("value", "User Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/SubjectHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SubjectHeader");
                    objTempNode.SetAttribute("value", "Subject");
                    objParentNode.AppendChild(objTempNode);
                }
                //Deb MITS 30185                
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException("An error has occurred", p_objException);
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }
        /// <summary>
        /// Saves the order of the headers
        /// </summary>
        public void SaveNotesSettings(ProgressNoteSettings objSettings, int userId)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            XmlDocument objUserPrefXML = null;
            string sUserPrefsXML = string.Empty;
            string header1 = string.Empty;
            string header2 = string.Empty;
            string header3 = string.Empty;
            string header4 = string.Empty;
            string header5 = string.Empty;
            string header6 = string.Empty;
            string header7 = string.Empty;
            string header8 = string.Empty;
            string header9 = string.Empty;
            //Added by Amitosh for mits 23691 (05/11/2011)
            string sortby = string.Empty;
            // Added by akaushik5 for MITS 30789 Starts
            string orderBy = string.Empty;
            // Added by akaushik5 for MITS 30789 Ends
            XmlNode objNode = null;

            try
            {
                sSQL = "SELECT * FROM USER_PREF_XML WHERE USER_ID"
                    + " = '" + userId + "'";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                    objReader.Close();
                    sUserPrefsXML = sUserPrefsXML.Trim();
                    objUserPrefXML = new XmlDocument();
                    if (sUserPrefsXML != "")
                    {
                        objUserPrefXML.LoadXml(sUserPrefsXML);
                    }

                    header1 = objSettings.Header1;
                    header2 = objSettings.Header2;
                    header3 = objSettings.Header3;
                    header4 = objSettings.Header4;
                    header5 = objSettings.Header5;
                    header6 = objSettings.Header6;
                    header7 = objSettings.Header7;
                    header8 = objSettings.Header8;
                    header9 = objSettings.Header9;
                    sortby = objSettings.sortby;
                    // Added by akaushik5 for MITS 30789 Starts
                    orderBy = objSettings.OrderBy;
                    // Added by akaushik5 for MITS 30789 Ends

                    CreateUserPreferXML(objUserPrefXML);


                    //XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header1.Replace(" ", "") + "/@Order");
                    //objNode.Value = "1";
                    //objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header2.Replace(" ", "") + "/@Order");
                    //objNode.Value = "2";
                    //objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header3.Replace(" ", "") + "/@Order");
                    //objNode.Value = "3";
                    //objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header4.Replace(" ", "") + "/@Order");
                    //objNode.Value = "4";
                    //objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header5.Replace(" ", "") + "/@Order");
                    //objNode.Value = "5";
                    if (!string.IsNullOrEmpty(header1))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header1.Replace(" ", "") + "/@Order");
                        objNode.Value = "1";
                    }
                    if (!string.IsNullOrEmpty(header2))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header2.Replace(" ", "") + "/@Order");
                        objNode.Value = "2";
                    }
                    if (!string.IsNullOrEmpty(header3))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header3.Replace(" ", "") + "/@Order");
                        objNode.Value = "3";
                    }
                    if (!string.IsNullOrEmpty(header4))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header4.Replace(" ", "") + "/@Order");
                        objNode.Value = "4";
                    }
                    if (!string.IsNullOrEmpty(header5))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header5.Replace(" ", "") + "/@Order");
                        objNode.Value = "5";

                    }
                    //Added by Amitosh for mits 23691(05/11/2011)
                    if (!string.IsNullOrEmpty(header6))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header6.Replace(" ", "") + "/@Order");
                        objNode.Value = "6";

                    }
                    if (!string.IsNullOrEmpty(header7))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header7.Replace(" ", "") + "/@Order");
                        objNode.Value = "7";

                    }
                    if (!string.IsNullOrEmpty(header8))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header8.Replace(" ", "") + "/@Order");
                        objNode.Value = "8";

                    }
                    if (!string.IsNullOrEmpty(header9))
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/" + header9.Replace(" ", "") + "/@Order");
                        objNode.Value = "9";

                    }
                    //end Amitosh
                    if (sortby != null || sortby != "")
                    {
                        objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/sortby/@value");
                        objNode.Value = sortby;
                        // Added by akaushik5 for MITS 30789 Starts
                        if (!string.IsNullOrEmpty(orderBy))
                        {
                            objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/OrderBy/@value");
                            if (!object.ReferenceEquals(objNode, null))
                            {
                                objNode.Value = orderBy;
                            }
                        }
                        else
                        {
                            objNode = objUserPrefXML.SelectSingleNode("//setting/EnhancedNotes/OrderBy/@value");
                            if (!object.ReferenceEquals(objNode, null))
                            {
                                objNode.Value = string.Empty;
                            }
                        }
                        // Added by akaushik5 for MITS 30789 Ends
                    }

                    this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(userId));

                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveNotesSettings.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader = null;
                }
            }
        }

        /// <summary>
        /// Save User Preference Xml to Database.
        /// </summary>
        /// <param name="p_sUserPrefXml">User Preference Xml Doc</param>
        /// <param name="p_iUserId">User Id</param>
        private void SaveUserPrefersXmlToDb(string p_sUserPrefXml, int p_iUserId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            string sSQL = string.Empty;

            try
            {
                //Try to delete the old one.
                sSQL = "DELETE FROM USER_PREF_XML WHERE USER_ID = " + p_iUserId;
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.Close();

                objWriter = DbFactory.GetDbWriter(m_sConnectionString);
                objWriter.Tables.Add("USER_PREF_XML");
                objWriter.Fields.Add("USER_ID", p_iUserId);
                objWriter.Fields.Add("PREF_XML", p_sUserPrefXml);
                objWriter.Execute();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SaveNotesSettings.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
            }
        }
        /// <summary>
        /// Gets the number records displayed per page 
        /// </summary>
        /// <returns></returns>
        public int GetNotesPerPage()//Parijat : MITS 19713
        {
            StringBuilder sbSQL = null;
            int iReturn = 0;
            DbReader objReader = null;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                sbSQL.Append(" WHERE PARM_NAME = 'ENH_USER_LIMIT' ");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader.Read())
                {
                    iReturn = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
                    if (iReturn < 10)
                        iReturn = 40;//Record per Page should greater than equal to 10 and the recommended is 40
                }
                else
                {
                    //iReturn=0;
                    iReturn = 40;//If nothing had been mentioned then Record per Page should as 40 
                }
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                sbSQL = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    //Shivendu to dispose the object
                    objReader.Dispose();
                }
                objReader = null;
            }
            return iReturn;
        }


        #endregion

        //rsolanki: mits 23101 : sorting by first name + last name 
        public enum SortDirection
        {
            Asc,
            Desc
        }

        private SortDirection m_direction = SortDirection.Asc;

        int IComparer.Compare(object x, object y)
        {
            ProgressNotes categoryX = (ProgressNotes)x;
            ProgressNotes categoryY = (ProgressNotes)y;

            return (this.m_direction == SortDirection.Asc)
                ? categoryX.EnteredByName.CompareTo(categoryY.EnteredByName)
                : categoryY.EnteredByName.CompareTo(categoryX.EnteredByName);

        }

        //rsolanki2 : ExecSummaryScheduler Enhacements:
        //This function creates an entry in the Task manager for Offline generation of the Enhc Notes PDF.
        //averma62 MITS - 27826 private void CreateTMEntry(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL, bool p_bPrintSelectedNotes, int p_iClaimProgressNoteId, int p_iNoteTypeCod)
        private void CreateTMEntry(int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL
            , bool p_bPrintSelectedNotes, int p_iClaimProgressNoteId, int p_iNoteTypeCod, int p_iPolicyId)  //averma62 MITS - 27826 - added one more parameter policy id
        {


            TaskManager objTaskManager;
            string sModule = string.Empty;
            string sReportType = string.Empty;
            //Ashish Ahuja MITS 34537 - Made Parameterized Query
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            DbCommand objCmd = null;
            DbConnection objCon = null;
            try
            {
                objTaskManager = new TaskManager(m_UserLogin,m_iClientId);//dvatsa

                ScheduleDetails objSchedule; ;
                string sModulename = string.Empty;
                //int iReportType;

                int iTypeId = 0;
                objSchedule = new ScheduleDetails();
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 1;

                string sSql = "SELECT TASK_TYPE_ID  FROM TM_TASK_TYPE  WHERE LOWER(NAME)  = 'executivesummaryscheduler' ";

                using (DbReader objRdr = DbFactory.ExecuteReader(m_sTMConnectionString, sSql))
                {
                    if (objRdr.Read())
                    {
                        Int32.TryParse(objRdr[0].ToString(), out iTypeId);
                    }
                    else
                    {
                        throw new Exception("TM Task Id not found");
                    }
                }

                objSchedule.TaskTypeId = iTypeId;
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";

                objSchedule.FinalRunDTTM = "";
                objSchedule.TaskName = "ProgressNotes"; //hardcoded for exec summary comes from TM_TASK_TYPE table
                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;



                //if (m_ExecutiveSummaryReportType == 1) // fro claims
                //{
                //    sModule = "cl";
                //}
                //else if (m_ExecutiveSummaryReportType == 0)
                //{
                //    sModule = "ev";
                //}
                //else if (m_ExecutiveSummaryReportType == 2)
                //{
                //    sModule = "at";
                //}

                sModule = "nt";

                //    (int p_iEventId, int p_iClaimId, bool p_bActivateFilter, string p_sFilterSQL
                //, bool p_bPrintSelectedNotes, int p_iClaimProgressNoteId, int p_iNoteTypeCod)

                objSchedule.Config = "<Task Name=\"ExecutiveSummary\" cmdline=\"yes\"><Path>ExecutiveSummaryScheduler.exe</Path><Args><arg>-ru"
                    + m_UserLogin.LoginName
                    + "</arg><arg>-rp"
                    + System.Security.SecurityElement.Escape(RMCryptography.EncryptString(m_UserLogin.Password))
                    + "</arg><arg>-ds"
                    + m_UserLogin.objRiskmasterDatabase.DataSourceName

                    + "</arg><arg>-ne" + p_iEventId
                    + "</arg><arg>-nc" + p_iClaimId
                    + "</arg><arg>-na" + p_bActivateFilter
                    + "</arg><arg>-nf" + p_sFilterSQL
                    + "</arg><arg>-np" + p_bPrintSelectedNotes
                    + "</arg><arg>-nn" + p_iClaimProgressNoteId
                    + "</arg><arg>-ny" + p_iNoteTypeCod
                    + "</arg><arg>-no" + p_iPolicyId
                    + "</arg><arg>-" + sModule
                    + "</arg></Args></Task>";

                #region Duplicacy check

                int iAlreadyScheduledCount = 0;

                Regex regex = new Regex(@"^(?<start>.*){(?<driver>Oracle.*)}(?<end>.*)$", RegexOptions.IgnoreCase);
                if (regex.Match(m_sTMConnectionString).Success)
                {
                    //for Oracle db
                    //Ashish Ahuja MITS 34537 
                    //sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE TO_CHAR(LOWER(CONFIG)) = TO_CHAR(LOWER('" + objSchedule.Config + "'))";
                    sSql = string.Format("SELECT COUNT(*) FROM TM_SCHEDULE WHERE TO_CHAR(LOWER(CONFIG)) = TO_CHAR(LOWER('{0}'))", "~Config~");
                    dictParams.Add("Config", objSchedule.Config);
                }
                else
                {
                    //for sql serevr db
                    //Ashish Ahuja MITS 34537 
                    //sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE LOWER(CONFIG) = LOWER('" + objSchedule.Config + "')";
                    sSql = string.Format("SELECT COUNT(*) FROM TM_SCHEDULE WHERE LOWER(CONFIG) = LOWER('{0}')", "~Config~");
                    dictParams.Add("Config", objSchedule.Config);
                }

                //Ashish Ahuja MITS 34537 
                //using (DbReader objRdr = DbFactory.ExecuteReader(m_sTMConnectionString, sSql))
                objCon = DbFactory.GetDbConnection(m_sTMConnectionString);
                objCon.Open();
                objCmd = objCon.CreateCommand();
                objCmd.CommandText = sSql.ToString();
                using (DbReader objRdr = DbFactory.ExecuteReader(objCmd, dictParams))
                {
                    if (objRdr.Read())
                    {
                        Int32.TryParse(objRdr[0].ToString(), out iAlreadyScheduledCount);
                    }
                }

                if (iAlreadyScheduledCount > 0)
                {
                    // the task is already scheduled. Exiting in which case.
                    return;
                }

                #endregion


                #region Offline Hours check

                string sOfflineHoursStart;
                bool bSuccess = false;
                DateTime dtOfflineHoursStart;
                // Ash - cloud, get config setting from db
                //sOfflineHoursStart = RMConfigurationManager.GetAppSetting("ExecSummaryOfflineHours");
                sOfflineHoursStart = RMConfigurationManager.GetAppSetting("ExecSummaryOfflineHours", m_sConnectionString, m_iClientId);
                
                bSuccess = DateTime.TryParse(sOfflineHoursStart, out dtOfflineHoursStart);
                if (bSuccess)
                {
                    //when the Offline Hours are defined
                    objSchedule.TimeToRun = Conversion.GetTime(dtOfflineHoursStart.ToString());
                    objSchedule.NextRunDTTM = Conversion.ToDbDateTime(dtOfflineHoursStart);
                }
                else
                {
                    //when the Offline Hours are Not defined. 25 sec are added to curretn DT and the task is scheduled to it.
                    objSchedule.TimeToRun = Conversion.GetTime(DateTime.Now.TimeOfDay.Add(new TimeSpan(25000)).ToString());
                    objSchedule.NextRunDTTM = Conversion.ToDbDateTime(DateTime.Now);
                }

                objSchedule.DayOfMonth = 0;

                #endregion


                objTaskManager.SaveSettings(objSchedule);

                //bLimitExceeded = true;
            }

            catch (Exception e)
            {
                throw new Exception("Exception while creating TM entry:" + e.Message);
            }

        }

        //mini enhance notes enhancement mits 30513
        //mits 34160
       
        public string GetNotesCaption(string sRecordType, int iRecordId, int PIEId)
        {
            string sCaption = string.Empty;
            Claim objClaim = null;
            Claimant objClaimant = null; //Added by GBINDRA 02102014 MIST#34104 WWIG gap15
            Event objEvent = null;
            LobSettings objLobSettings = null;
            int captionLevel = 0;
            SysSettings objSysSettings = null; //added by gbindra MITS#34104 WWIG GAP15
            try
            {
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId); //added by gbindra MITS#34104 WWIG GAP15
                if (string.Equals(sRecordType, "EVENT", StringComparison.InvariantCultureIgnoreCase))
                {
                    objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                    captionLevel = 1006;
                    objEvent.MoveTo(iRecordId);
                    string sTmp = "";
                    if (objEvent.Context.InternalSettings.SysSettings.EvCaptionLevel != 0)
                        captionLevel = objEvent.Context.InternalSettings.SysSettings.EvCaptionLevel;
                    else
                        captionLevel = 0;

                    if (captionLevel < 1005 || captionLevel > 1012)
                        captionLevel = 0;

                    if (captionLevel == 0)
                    {
                        sCaption = "";
                    }
                    else if (captionLevel != 1012)
                        sCaption = objEvent.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                    else
                    {
                        objEvent.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid, ref sCaption, ref sTmp);
                        sCaption += " " + sTmp;
                    }
                    sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                    //mits 34160
                    if (objEvent.EventId != 0 && objEvent.PiList.GetPiByEID(PIEId) != null)
                        sCaption = " [" + objEvent.EventNumber + " * " + sCaption + " * " +
                            objEvent.PiList.GetPiByEID(PIEId).PiEntity.GetLastFirstName() + "]";
                    else if (objEvent.EventId != 0 && objEvent.PiList.GetPrimaryPi() != null)
                        sCaption = " [" + objEvent.EventNumber + " * " + sCaption + " * " +
                            objEvent.PiList.GetPrimaryPi().PiEntity.GetLastFirstName() + "]";
                    else
                        sCaption = " [" + objEvent.EventNumber + " * " + sCaption + " * - ]";
                }
                else if (string.Equals(sRecordType, "CLAIM", StringComparison.InvariantCultureIgnoreCase))
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(iRecordId);
                    objLobSettings = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];

                    string sTmp = "";
                    captionLevel = objLobSettings.CaptionLevel;
                    switch (objClaim.LineOfBusCode.ToString())
                    {
                        case "241":
                            //     LOB = "GC";

                            if (objLobSettings.CaptionLevel != 0)
                                captionLevel = objLobSettings.CaptionLevel;
                            else
                                captionLevel = 1006;
                            //nadim for 15014
                            if (captionLevel < 1005 || captionLevel > 1012)
                                captionLevel = 0;

                            if (captionLevel != 1012)
                                sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                            else
                            {
                                objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo((objClaim.Parent as Event).DeptEid, ref sCaption, ref sTmp);
                                sCaption += sTmp;
                            }
                            sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                            if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                            {
                                sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                                    objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
                            }
                            else
                            {
                                sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                            }
                            break;
                        case "243":
                            //   LOB = "WC";
                            objLobSettings = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
                            captionLevel = objLobSettings.CaptionLevel;
                            sCaption = m_objDataModelFactory.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, captionLevel, 1, ref captionLevel);
                            sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
                            //  sCaption = " [" + objClaim.ClaimNumber + " * ";
                            Entity objPiEntity = objClaim.PrimaryPiEmployee.PiEntity;
                            sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption
                                + " * " +
                                objPiEntity.LastName + " " + objPiEntity.FirstName + "] ";

                            break;
                        case "242":
                            // LOB = "VA";
                            //if (!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId != 0)
                            //{

                            if (objLobSettings.CaptionLevel != 0)
                                captionLevel = objLobSettings.CaptionLevel;
                            else
                                captionLevel = 1006;
                            //nadim for 15014
                            if (captionLevel < 1005 || captionLevel > 1012)
                                captionLevel = 0;

                            if (captionLevel != 1012)
                                sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                            else
                            {
                                objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo((objClaim.Parent as Event).DeptEid, ref sCaption, ref sTmp);
                                sCaption += sTmp;
                            }
                            sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                            if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null)
                            {
                                sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                                    objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
                            }
                            else
                            {
                                sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                            }
                            // }
                            break;
                        case "844":
                            //LOB = "DI";
                            objLobSettings = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
                            sCaption = " [" + objClaim.ClaimNumber + " * " +
                                m_objDataModelFactory.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, captionLevel, 1, ref captionLevel) + " * " +
                                objClaim.PrimaryPiEmployee.PiEntity.LastName + ", " + objClaim.PrimaryPiEmployee.PiEntity.FirstName + "] ";

                            break;
                        case "845":
                            // LOB = "PC";

                            //if (!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId != 0)
                            //{

                            if (objLobSettings.CaptionLevel != 0)
                                captionLevel = objLobSettings.CaptionLevel;
                            else
                                captionLevel = 1006;
                            //nadim for 15014
                            if (captionLevel < 1005 || captionLevel > 1012)
                                captionLevel = 0;

                            if (captionLevel != 1012)
                                sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                            else
                            {
                                objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo((objClaim.Parent as Event).DeptEid, ref sCaption, ref sTmp);
                                sCaption += sTmp;
                            }
                            sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                            if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                            {
                                sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                                    objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
                            }
                            else
                            {
                                sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                            }
                            //        }

                            break;
                    }


                }
                #region/*GBINDRA MITS#34104 02062014 WWIG GAP-15 START*/
                else if (string.Equals(sRecordType, "CLAIMANT", StringComparison.InvariantCultureIgnoreCase) && objSysSettings.AllowNotesAtClaimant == true)
                {
                    objClaimant = (Claimant)m_objDataModelFactory.GetDataModelObject("Claimant", false);
                    objClaimant.MoveTo(iRecordId);
                    captionLevel = 1006;
                    string sTmp = "";

                    if (objClaimant.Context.InternalSettings.SysSettings.EvCaptionLevel != 0)
                        captionLevel = objClaimant.Context.InternalSettings.SysSettings.EvCaptionLevel;
                    else
                        captionLevel = 0;

                    if (captionLevel < 1005 || captionLevel > 1012)
                        captionLevel = 0;

                    if (captionLevel == 0)
                    {
                        sCaption = "";
                    }
                    else if (captionLevel != 1012)
                        sCaption = objClaimant.Context.InternalSettings.CacheFunctions.GetOrgParent(objClaimant.ClaimantEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                    else
                    {
                        objClaimant.Context.InternalSettings.CacheFunctions.GetOrgInfo(objClaimant.ClaimantEid, ref sCaption, ref sTmp);
                        sCaption += " " + sTmp;
                    }
                    sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                    //if (objClaimant.ClaimantEid != 0 && objClaimant.PrimaryClmntFlag == true)//==true check kar bhai
                    sCaption = " [" + objClaimant.ClaimantEid + " * " + sCaption + " * " +
                        objClaimant.ClaimantEntity.GetLastFirstName() + "]";
                    /*else
                        sCaption = " [" + objClaimant.EventNumber + " * " + sCaption + " * - ]";*/
                }
                #endregion
            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.GetNotesCaption.Error", m_iClientId), e);
            }
            finally
            {
                if (objEvent != null)
                {
                    objEvent.Dispose();
                    objEvent = null;
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }

                objLobSettings = null;
            }
            return sCaption;
        }
        //mini enhance notes enhancement

        #region Filter Session

        public void CreateFilterSession(SessionManager objSessionManager, string p_sClaimIDList, string p_sEnteredByList, string p_sNoteTypeList, string p_sUserTypeList, string p_sActivityFromDate,
            string p_sActivityToDate, string p_sNotesTextContains, string p_sSortBy)
        {
            XmlDocument xSession = null;
            XmlElement xFilter = null;

            try
            {
                xSession = new XmlDocument();

                xFilter = xSession.CreateElement("Filter");

                xFilter.Attributes.Append(xSession.CreateAttribute("ClaimIdList"));
                xFilter.Attributes.Append(xSession.CreateAttribute("EnteredByList"));
                xFilter.Attributes.Append(xSession.CreateAttribute("NoteTypeList"));
                xFilter.Attributes.Append(xSession.CreateAttribute("UserTypeList"));
                xFilter.Attributes.Append(xSession.CreateAttribute("ActivityFromDate"));
                xFilter.Attributes.Append(xSession.CreateAttribute("ActivityToDate"));
                xFilter.Attributes.Append(xSession.CreateAttribute("NotesTextContains"));
                xFilter.Attributes.Append(xSession.CreateAttribute("SortBy"));

                if (!string.IsNullOrEmpty(p_sClaimIDList))
                {
                    xFilter.Attributes["ClaimIdList"].Value = p_sClaimIDList;
                }

                if (!string.IsNullOrEmpty(p_sEnteredByList))
                {
                    xFilter.Attributes["EnteredByList"].Value = p_sEnteredByList;
                }

                if (!string.IsNullOrEmpty(p_sNoteTypeList))
                {
                    xFilter.Attributes["NoteTypeList"].Value = p_sNoteTypeList;
                }

                if (!string.IsNullOrEmpty(p_sUserTypeList))
                {
                    xFilter.Attributes["UserTypeList"].Value = p_sUserTypeList;
                }

                if (!string.IsNullOrEmpty(p_sActivityFromDate))
                {
                    xFilter.Attributes["ActivityFromDate"].Value = p_sActivityFromDate;
                }

                if (!string.IsNullOrEmpty(p_sActivityToDate))
                {
                    xFilter.Attributes["ActivityToDate"].Value = p_sActivityToDate;
                }

                if (!string.IsNullOrEmpty(p_sNotesTextContains))
                {
                    xFilter.Attributes["NotesTextContains"].Value = p_sNotesTextContains;
                }

                if (!string.IsNullOrEmpty(p_sSortBy))
                {
                    xFilter.Attributes["SortBy"].Value = p_sSortBy;
                }

                xSession.AppendChild(xFilter);

                objSessionManager.SetBinaryItem("filter", Utilities.BinarySerialize(xSession.OuterXml), m_iClientId);
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.SelectClaim.XmlError", m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesManager.OnLoad.Error", m_iClientId), p_objException);
            }
            finally
            {

            }
        }


        public void ApplyFilterFromSession(SessionManager objSessionManager, ProgressNotesType objDocument)
        {
            XmlDocument xSessionDoc = null;
            XmlNode xFilterNode = null;
            string sSessionString = string.Empty;


            sSessionString = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem("filter")).ToString();

            xSessionDoc = new XmlDocument();
            xSessionDoc.LoadXml(sSessionString);

            xFilterNode = xSessionDoc.SelectSingleNode("//Filter");

            if (xFilterNode.Attributes["ClaimIdList"] != null)
            {
                objDocument.objFilter.ClaimIDList = xFilterNode.Attributes["ClaimIdList"].Value;
            }

            if (xFilterNode.Attributes["EnteredByList"] != null)
            {
                objDocument.objFilter.EnteredByList = xFilterNode.Attributes["EnteredByList"].Value;
            }

            if (xFilterNode.Attributes["NoteTypeList"] != null)
            {
                objDocument.objFilter.NoteTypeList = xFilterNode.Attributes["NoteTypeList"].Value;
            }

            if (xFilterNode.Attributes["UserTypeList"] != null)
            {
                objDocument.objFilter.UserTypeList = xFilterNode.Attributes["UserTypeList"].Value;
            }

            if (xFilterNode.Attributes["ActivityFromDate"] != null)
            {
                objDocument.objFilter.ActivityFromDate = xFilterNode.Attributes["ActivityFromDate"].Value;
            }

            if (xFilterNode.Attributes["ActivityToDate"] != null)
            {
                objDocument.objFilter.ActivityToDate = xFilterNode.Attributes["ActivityToDate"].Value;
            }

            if (xFilterNode.Attributes["NotesTextContains"] != null)
            {
                objDocument.objFilter.NotesTextContains = xFilterNode.Attributes["NotesTextContains"].Value;
            }

            if (xFilterNode.Attributes["SortBy"] != null)
            {
                objDocument.objFilter.SortBy = xFilterNode.Attributes["SortBy"].Value;
            }
        }
        #endregion
    }
}