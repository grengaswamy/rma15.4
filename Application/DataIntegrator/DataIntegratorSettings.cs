﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Db;
using Riskmaster.Models;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Data;
using System.Data.OracleClient; //dsharma70 MITS# 35315, 35143
//smahajan6 - 06/22/2010 - MITS# 21191 : Start
using System.Data.OleDb;
//smahajan6 - 06/22/2010 - MITS# 21191 : End

//dsharma70 - 11/14/2014 - MITS# 35315, 35413 & JIRA RMA-4579, RMA-4578 Start
using System.Data.Odbc;
//dsharma70 - 11/14/2014 - MITS# 35315, 35413 & JIRA RMA-4579, RMA-4578 End
using Riskmaster.Common;
using Microsoft.Win32;//npradeepshar - MITS 23121
using Riskmaster.Security;      //ipuri Mits:30519
//Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | Start
using Riskmaster.Application.RMUtilities;
using Riskmaster.Settings; //sagarwal54 MITS 37006 09/08/2014
//Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | End
using System.Text.RegularExpressions;
using System.Linq;



namespace Riskmaster.Application.DataIntegrator
{
    //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | Start
    public class Settings : UtilitiesUIListBase
    //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | End
    {
        private string m_moduleName;
        private string m_connectionString;
        private string m_TMConnectionString;
        private string m_DAConnectionString;
        //npradeepshar - MITS 23121
        private const string strProviderName_2003 = "Microsoft.Jet.OLEDB.4.0";
        private const string strProviderName_2007 = "Microsoft.ACE.OLEDB.12.0";
        //npradeepshar - MITS 23121

        //dsharma70 - 11/14/2014 - MITS# 35315, 35413 & JIRA RMA-4579, RMA-4578 Start
        private const string strProviderName_2013 = "Microsoft.ACE.OLEDB.15.0";     
        private const string strProviderName_2010 = "Microsoft.ACE.OLEDB.14.0";
        //dsharma70 - 11/14/2014 - MITS# 35315, 35413 & JIRA RMA-4579, RMA-4578 End


        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
        private static int iCarrier = 0;
        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End

        //mkaran2 - MITS 34134 - Start
        /// <summary>
        /// Stores the PageSize of the Tier Grid
        /// </summary>
        private string m_iISOPageSize = string.Empty;

        // <summary>
        /// Stores the Current Page of the ISO Map Grid  
        /// </summary>
        private string m_iISOCurrentPage = string.Empty;
        private int m_iClientId = 0;

        //mkaran2 - MITS 34134 - End

        /// <summary>
        /// //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | Start -Default Constructor
        /// </summary>
        public Settings(int p_iClientId):base(p_iClientId)
        {
            m_iClientId = p_iClientId;
        }
        //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | End
        public string moduleName
        {
            set { m_moduleName = value; }
            get { return m_moduleName; }
        }

        public string connectionString
        {
            set { m_connectionString = value; }
            get { return m_connectionString; }
        }

        public string TaskManagerConnectionString
        {
            set { m_TMConnectionString = value; }
            get { return m_TMConnectionString; }
        }

        public string DAConnectionString
        {
            set { m_DAConnectionString = value; }
            get { return m_DAConnectionString; }
        }
        //mkaran2 - MITS 34134 - Start
        // ISO Map Paging
        public string ISOPageSize
        {
            get { return m_iISOPageSize; }
            set { m_iISOPageSize = value; }
        }

        public string ISOCurrentPage
        {
            get { return m_iISOCurrentPage; }
            set { m_iISOCurrentPage = value; }
        }

        //mkaran2 - MITS 34134 - End

        //sagarwal54 MITS 37006 09/02/2014 start
        public bool IsPolicySystemInterface
        {
            get
            {
                SysSettings objSettings = new SysSettings(m_connectionString, m_iClientId);
                return objSettings.UsePolicyInterface;
            }
        }
        //sagarwal54 MITS 37006 09/02/2014 end

        public List<string> GetOptionSetNames(int userId)
        {
            string sql = "SELECT DISTINCT(OPTIONSET_NAME) FROM DATA_INTEGRATOR"
                + " WHERE USER_ID = " + userId
                + " AND MODULE_NAME = " + "'" + m_moduleName + "'";

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);

            List<string> Names = new List<string>();
            while (objReader.Read())
                Names.Add(objReader.GetString("OPTIONSET_NAME"));

            return Names;
        }

        //------sgupta320:jira-10616 for 2GB Enhancement
        public int InsertSettings(int userId, string optionSetName, Dictionary<string, string> Settings,int BatchID=0)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDocument xmlDoc4Reserve = new XmlDocument();
            int iSubAccEnabled = 0;
            string sTemp = string.Empty;
            XmlDoc.LoadXml("<" + m_moduleName + ">" + "</" + m_moduleName + ">");
            xmlDoc4Reserve.LoadXml("<" + m_moduleName + ">" + "</" + m_moduleName + ">");
            if (String.Equals(m_moduleName, "DDS"))
            {
                foreach (KeyValuePair<string, string> kvp in Settings)
                {
                    if (kvp.Key.StartsWith("g"))
                    {
                        XmlNode node = XmlDoc.CreateElement(kvp.Key);
                        node.InnerText = kvp.Value;
                        XmlDoc.ChildNodes[0].AppendChild(node);
                    }
                    //else
                    //{
                    //    XmlNode node = xmlDoc4Reserve.CreateElement(kvp.Key);
                    //    node.InnerText = kvp.Value;
                    //    xmlDoc4Reserve.ChildNodes[0].AppendChild(node);
                    //}
                }
            }
            else
            {
                //Developer :- Subhendu | Module - DA CLAIM EXPORT CSSTARS | Start
                string sClaimType = "";
                string sReserveType = "";
                string sLOB = "";
                if (string.Equals(m_moduleName, "CLAIM_EXPORT_CSStars"))
                {
                    CleanUpDAClaimExportTables(false, 0);
                    Settings.TryGetValue("CLAIM_TYPE_CODE_GL", out sClaimType);
                    Settings.TryGetValue("RESERVE_CODE_GL", out sReserveType);
                    Settings.TryGetValue("Line_Of_Business_GL", out sLOB);
                    if (sLOB == "1")
                    {
                        SaveClaimTypeCode(sClaimType, 241, false, 0);
                        SaveReserveType(sReserveType, 241, false, 0);
                    }
                    SaveCEDeptTable(false, 0);
                    Settings.Remove("CLAIM_TYPE_CODE_GL");
                    Settings.Remove("RESERVE_CODE_GL");
                }
                //Developer :- Subhendu | Module - DA CLAIM EXPORT CSSTARS | End
                foreach (KeyValuePair<string, string> kvp in Settings)
                {
                    //Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                      XmlNode node = XmlDoc.CreateElement(kvp.Key);
                        node.InnerText = kvp.Value;

                        XmlDoc.ChildNodes[0].AppendChild(node);
                    
                }
                //-----sgupta320:jira-10616 for 2GB Enhancement.
                if (String.Equals(m_moduleName, "DIS"))
                {
                    XmlNode Node_BatchID = XmlDoc.CreateElement("txtBatchID");
                    Node_BatchID.InnerText = Convert.ToString(BatchID);
                    XmlDoc.ChildNodes[0].AppendChild(Node_BatchID);
                }
                //----end


            }
            //ipuri Start: User Verification Changes. User Id is not required
            //if (XmlDoc.SelectSingleNode("//Diary_To_Users") != null)        // for DIS Criteria Screen
            //    XmlDoc.SelectSingleNode("//Diary_To_Users").InnerText = userId.ToString();    // csingh7   06/10/2010 : Store USerID for Diary_To_User
            //else if (XmlDoc.SelectSingleNode("//gDiary_To_Users") != null)   // for DDS Criteria Screen
            //    XmlDoc.SelectSingleNode("//gDiary_To_Users").InnerText = userId.ToString();  
            //ipuri End
            
            int optionSetId = GetMaxOptionSetId() + 1;

            if (String.Equals(m_moduleName, "DDS"))
            {
                if (XmlDoc.SelectSingleNode("//gPayments_Available") != null)
                {
                    if (XmlDoc.SelectSingleNode("//gPayments_Available").InnerText == "1")
                    {
                        iSubAccEnabled = GetSubAccEnabled();
                        if (iSubAccEnabled != 0)    // Use_Sub_Account is enabled.
                        {
                            if (XmlDoc.SelectSingleNode("//gPayment_Bank_Account") != null)
                                sTemp = XmlDoc.SelectSingleNode("//gPayment_Bank_Account").InnerText;
                            if (XmlDoc.SelectSingleNode("//gPayment_Sub_Account") != null)
                            {
                                XmlDoc.SelectSingleNode("//gPayment_Sub_Account").InnerText = sTemp;
                                XmlDoc.SelectSingleNode("//gPayment_Bank_Account").InnerText = null;
                            }
                        }
                    }
                }
            }
            InsertIntoDB(optionSetId, userId, optionSetName, XmlDoc.InnerXml);

            //Find new record in the database
            optionSetId = GetOptionSetId(userId, optionSetName);

            //if (String.Equals(m_moduleName, "DDS"))
            //{
            //    InsertReserveInfo(optionSetId, optionSetName, xmlDoc4Reserve);
            //}

            return optionSetId;
        }

        //public int InsertSettings(int userId, string optionSetName, string settings)
        //{
        //    XmlDocument XmlDoc = new XmlDocument();
        //    XmlDoc.LoadXml("<" + m_moduleName + ">" + "</" + m_moduleName + ">");

        //    int optionSetId = GetMaxOptionSetId() + 1;

        //    InsertIntoDB(optionSetId, userId, optionSetName, XmlDoc.InnerXml);

        //    //Find new record in the database
        //    return GetOptionSetId(userId, optionSetName);
        //}

        //------sgupta320:jira-10616 add new parameter to funtion "BatchID" for more than 2GB data functionality
        public int SaveSettings(int userId, string optionSetName, Dictionary<string, string> moduleSettings,int BatchID=0)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDocument xmlDoc4Reserve = new XmlDocument();
            XmlDoc.LoadXml("<" + m_moduleName + ">" + "</" + m_moduleName + ">");
            xmlDoc4Reserve.LoadXml("<" + m_moduleName + ">" + "</" + m_moduleName + ">");
            int iSubAccEnabled = 0;
            int iOptionSetId = 0;
            string sTemp = string.Empty;

            if (String.Equals(m_moduleName, "DDS"))
            {
                foreach (KeyValuePair<string, string> kvp in moduleSettings)
                {
                    if (kvp.Key.StartsWith("g"))
                    {
                        XmlNode node = XmlDoc.CreateElement(kvp.Key);
                        node.InnerText = kvp.Value;
                        XmlDoc.ChildNodes[0].AppendChild(node);
                    }
                    //else
                    //{
                    //    XmlNode node = xmlDoc4Reserve.CreateElement(kvp.Key);
                    //    node.InnerText = kvp.Value;
                    //    xmlDoc4Reserve.ChildNodes[0].AppendChild(node);
                    //}
                }
            }
            else
            {
                //Developer :- Subhendu | Module - DA CLAIM EXPORT CSSTARS | Start
                string sClaimType = "";
                string sReserveType = "";
                string sLOB = "";
                if (string.Equals(m_moduleName, "CLAIM_EXPORT_CSStars"))
                {
                    CleanUpDAClaimExportTables(true, GetOptionSetId(userId, optionSetName));
                    moduleSettings.TryGetValue("CLAIM_TYPE_CODE_GL", out sClaimType);
                    moduleSettings.TryGetValue("RESERVE_CODE_GL", out sReserveType);
                    moduleSettings.TryGetValue("Line_Of_Business_GL", out sLOB);
                    if (sLOB == "1")
                    {
                        SaveClaimTypeCode(sClaimType, 241, true, GetOptionSetId(userId, optionSetName));
                        SaveReserveType(sReserveType, 241, true, GetOptionSetId(userId, optionSetName));
                    }
                    SaveCEDeptTable(true, GetOptionSetId(userId, optionSetName));
                    moduleSettings.Remove("CLAIM_TYPE_CODE_GL");
                    moduleSettings.Remove("RESERVE_CODE_GL");
                }
                //Developer :- Subhendu | Module - DA CLAIM EXPORT CSSTARS | End
                foreach (KeyValuePair<string, string> kvp in moduleSettings)
                {
                    //Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                  
                    
                        XmlNode node = XmlDoc.CreateElement(kvp.Key);
                        node.InnerText = kvp.Value;

                        XmlDoc.ChildNodes[0].AppendChild(node);
                   
                }
                //-----sgupta320:jira-10616 for 2GB Enhancement.
                if (String.Equals(m_moduleName, "DIS"))
                {
                    XmlNode Node_BatchID = XmlDoc.CreateElement("txtBatchID");
                    Node_BatchID.InnerText = Convert.ToString(BatchID);
                    XmlDoc.ChildNodes[0].AppendChild(Node_BatchID);
                }
                //----end
            }
			//ipuri Start: User Verification Changes. User Id is not required
			//if (XmlDoc.SelectSingleNode("//Diary_To_Users") != null)        // for DIS Criteria Screen
            //    XmlDoc.SelectSingleNode("//Diary_To_Users").InnerText = userId.ToString();    // csingh7   06/10/2010 : Store USerID for Diary_To_User
            //else if (XmlDoc.SelectSingleNode("//gDiary_To_Users") != null)   // for DDS Criteria Screen
            //    XmlDoc.SelectSingleNode("//gDiary_To_Users").InnerText = userId.ToString();
            //ipuri

            if (String.Equals(m_moduleName, "DDS"))
            {
                if (XmlDoc.SelectSingleNode("//gPayments_Available") != null)
                {
                    if (XmlDoc.SelectSingleNode("//gPayments_Available").InnerText == "1")
                    {
                        iSubAccEnabled = GetSubAccEnabled();
                        if (iSubAccEnabled != 0)    // Use_Sub_Account is enabled.
                        {
                            if (XmlDoc.SelectSingleNode("//gPayment_Bank_Account") != null)
                                sTemp = XmlDoc.SelectSingleNode("//gPayment_Bank_Account").InnerText;
                            if (XmlDoc.SelectSingleNode("//gPayment_Sub_Account") != null)
                            {
                                XmlDoc.SelectSingleNode("//gPayment_Sub_Account").InnerText = sTemp;
                                XmlDoc.SelectSingleNode("//gPayment_Bank_Account").InnerText = null;
                            }
                        }
                    }
                }
            }

            SaveXML2DB(userId, optionSetName, XmlDoc.InnerXml);

            iOptionSetId = GetOptionSetId(userId, optionSetName);

            //if (String.Equals(m_moduleName,"DDS"))
            //    SaveReserveXML2DB(iOptionSetId ,optionSetName ,xmlDoc4Reserve);

            return iOptionSetId;
        }

        public Dictionary<string, string> RetrieveOptionSetSettings(int userId, string optionSetName)
        {
            Dictionary<string, string> Settings = null;

            //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
            int iIsCarrier = 0;
            //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End
            string s_xml = RetrieveXMLFromDB(userId, optionSetName);

            if (s_xml != "")
            {
                // Create a new dictionary of strings with string keys.
                Settings = new Dictionary<string, string>();

                XmlDocument XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(s_xml);

                foreach (XmlNode xmlNode in XmlDoc.ChildNodes[0].ChildNodes)
                    Settings.Add(xmlNode.Name, xmlNode.InnerText);
            }


            //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
            //string sql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME ='MULTI_COVG_CLM'";
            //iIsCarrier = Convert.ToInt32(DbFactory.ExecuteScalar(m_connectionString, sql));

            //Settings.Add("bIsCarrier", iIsCarrier.ToString());
            //iCarrier = iIsCarrier;
            //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End
            return Settings;
        }

        public Dictionary<string, string> RetrieveDefaultSettings()
        {
            Dictionary<string, string> Settings = null;
            XmlDocument XmlDoc = null;
            //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
            int iIsCarrier = 0;
            //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End

            //Vsoni5 : MITS 22565 : Exception handling done for Error display on UI and error logging in service error log.
            try
            {
                string sql = "SELECT XML_STRING"
                    + " FROM DATA_INTEGRATOR_TEMPLATES"
                    + " WHERE MODULE_NAME = " + "'" + m_moduleName + "'";

                //JIRA-RMA-9651
                //using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql))
                //{
                DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);
                    //if added by swati
                   objReader.Read();
                   // {
                        string s_xml = objReader.GetString("XML_STRING");

                        if (s_xml != "")
                        {
                            // Create a new dictionary of strings with string keys.
                            Settings = new Dictionary<string, string>();

                            XmlDoc = new XmlDocument();
                            XmlDoc.LoadXml(s_xml);

                            foreach (XmlNode xmlNode in XmlDoc.ChildNodes[0].ChildNodes)
                                Settings.Add(xmlNode.Name, xmlNode.InnerText);
                        }
                   // }
                //}
                //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
                //sql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME ='MULTI_COVG_CLM'";
                //iIsCarrier = Convert.ToInt32(DbFactory.ExecuteScalar(m_connectionString, sql));

                //Settings.Add("bIsCarrier", iIsCarrier.ToString());
                //iCarrier = iIsCarrier;

                //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End
                return Settings;
            }
            catch
            {
                throw new RMAppException("Please check the database for default Xml settings.");
            }
            finally
            {
                XmlDoc = null;
            }

        }

        //public string RetrieveDefaultSettings()
        //{
        //    string sql = "SELECT XML_STRING"
        //        + " FROM DATA_INTEGRATOR_TEMPLATES"
        //        + " WHERE MODULE_NAME = " + "'" + m_moduleName + "'";

        //    DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);
        //    objReader.Read();

        //    string s_xml = objReader.GetString("XML_STRING");

        //    return s_xml;
        //}

        public string GetOptionSetName(int optionSetId)
        {
            string sql = "SELECT OPTIONSET_NAME FROM DATA_INTEGRATOR"
                + " WHERE MODULE_NAME = " + "'" + m_moduleName + "'"
                + " AND OPTIONSET_ID = " + "'" + optionSetId + "'";

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);
            objReader.Read();

            string rtn = objReader.GetString("OPTIONSET_NAME");

            return rtn;
        }

        public int GetOptionSetId(int userId, string optionSetName)
        {
            string sql = "SELECT OPTIONSET_ID FROM DATA_INTEGRATOR"
                + " WHERE USER_ID = " + userId
                + " AND MODULE_NAME = " + "'" + m_moduleName + "'"
                + " AND OPTIONSET_NAME = " + "'" + optionSetName + "'";

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);
            objReader.Read();

            int id = objReader.GetInt32("OPTIONSET_ID");

            return id;
        }
        public DataSet GetMedicalReserve()
        {   //Vsoni5 : MITS 25884. Removed hardcoding for Related code ID and REserve_Type table_id.
            string sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE, CODES_TEXT.CODE_DESC "
                        + " FROM CODES, CODES_TEXT , CODES CODES1, GLOSSARY "
                        + " WHERE CODES_TEXT.CODE_ID = CODES.CODE_ID AND CODES.RELATED_CODE_ID = CODES1.CODE_ID "
                        + " AND UPPER( CODES1.SHORT_CODE ) = 'M' AND CODES1.TABLE_ID = GLOSSARY.TABLE_ID "
                        + " AND UPPER(SYSTEM_TABLE_NAME) = 'RESERVE_TYPE' AND CODES.DELETED_FLAG <> -1 ORDER BY CODES.SHORT_CODE";

            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            return objResult;
        }
        public DataSet GetOtherReserves()
        {   //Vsoni5 : MITS 25884. Removed hardcoding for Related code ID and REserve_Type table_id.
            string sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE, CODES_TEXT.CODE_DESC "
                        + " FROM CODES, CODES_TEXT , CODES CODES1, GLOSSARY "
                        + " WHERE CODES_TEXT.CODE_ID = CODES.CODE_ID AND CODES.RELATED_CODE_ID = CODES1.CODE_ID "
                        + " AND UPPER( CODES1.SHORT_CODE ) != 'M' AND CODES1.TABLE_ID = GLOSSARY.TABLE_ID "
                        + " AND UPPER(SYSTEM_TABLE_NAME) = 'RESERVE_TYPE' AND CODES.DELETED_FLAG <> -1 ORDER BY CODES.SHORT_CODE";
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            return objResult;
        }
        public DataSet GetStates()
        {
            string sSQL = "SELECT STATE_ID FROM STATES WHERE STATE_NAME IS NOT NULL AND DELETED_FLAG != -1 ORDER BY STATE_ID";
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            return objResult;
        }
        // vchaturvedi2 MITS : 20850 Selecting Last Run Export Date From Database 
        public string GetExportDateOnUI()
        {
            string sDTTM_LAST_UPDATE = string.Empty;
            string sDefaultDTTM = "19000101235959";
            string sNow = String.Format(DateTime.Now.ToString(), "YYYYMMDDHHNNSS");
            string sSQL = string.Empty;

            sSQL = "SELECT DTTM_LAST_UPDATE FROM GLOSSARY WHERE SYSTEM_TABLE_NAME ='CORVEL_DTTM'";
            sDTTM_LAST_UPDATE = (String)DbFactory.ExecuteScalar(m_connectionString, sSQL);

            if (!String.IsNullOrEmpty(sDTTM_LAST_UPDATE))
            {
                sDTTM_LAST_UPDATE = sDTTM_LAST_UPDATE.Substring(0, 8);
                return sDTTM_LAST_UPDATE;
            }
            //Update Table For First Time Run
            else
            {
                int iNextGlossID;
                sSQL = "SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME ='GLOSSARY'";
                iNextGlossID = (int)DbFactory.ExecuteScalar(m_connectionString, sSQL);
                sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = '" + (iNextGlossID + 1) + "' WHERE SYSTEM_TABLE_NAME ='GLOSSARY'";
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
                sSQL = "INSERT INTO GLOSSARY ( IND_STAND_TABLE_ID, LINE_OF_BUS_FLAG, REQD_IND_TABL_FLAG ,TABLE_ID ,   SYSTEM_TABLE_NAME , GLOSSARY_TYPE_CODE ,ATTACHMENTS_FLAG ,RELATED_TABLE_ID ,REQD_REL_TABL_FLAG, NEXT_UNIQUE_ID ,RM_USER_ID  , DTTM_LAST_UPDATE ,DELETED_FLAG  )";
                sSQL += " VALUES (0 , 0,  0," + iNextGlossID + ",'CORVEL_DTTM',    2, 0, 0, 0 , 0, 0, '19000101235959' , 0 )";
                DbFactory.ExecuteNonQuery(m_connectionString, sSQL);
                return sDefaultDTTM.Substring(0, 8);
            }
        }  // End vchaturvedi2
//mrishi2 - JIRA 16461 - Changes BEGINS
		public DataSet GetRREId(string rreidval)
        {
            string sSQL ;

            if (rreidval == null)
            {
            sSQL = "select ENTITY_X_MMSEA.MMSEA_REPRTER_TEXT,ENTITY.Last_Name from ENTITY_X_MMSEA LEFT OUTER JOIN ENTITY " +
                            "on ENTITY_X_MMSEA.Entity_Id=Entity.Entity_Id ";
            }
            else
            {
                sSQL = "select LINE_OF_BUS_CODE,SECOND_LOB_CODE from ENTITY_X_MMSEA where MMSEA_REPRTER_TEXT =" + "'" + rreidval + "'";
            
            }
			//mrishi2 - JIRA 16461 - Changes ENDS
			
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
          
            return objResult;

        }

        public DataSet GetTransactionTypeCodes()
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            string sSQL = "";

            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'TRANS_TYPES' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }
            else
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'TRANS_TYPES' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return objResult;
        }

        public DataSet GetReserveTypeCodes()
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            string sSQL = "";

            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }
            else
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }

            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return objResult;
        }

        /// <summary>
        /// Developer :- Subhendu | Module - DA CLAIM EXPORT CSSTARS | Function - Retrieve column details for table Policy_supp
        /// </summary>
        /// <returns></returns>
        public DataSet GetPolicySupp()
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            string sSQL = "";

            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                int iIndexUID = m_connectionString.IndexOf("UID=") + 4;

                string sOwner = m_connectionString.Substring(iIndexUID, m_connectionString.IndexOf(";PWD") - iIndexUID);

                sSQL = "SELECT COLUMN_NAME FROM all_tab_cols WHERE TABLE_NAME = 'POLICY_SUPP' and OWNER ='" + sOwner.ToUpper() + "'";
                //zmohammad Oracle fix for this query also, it would fail if DB name was in small letters.
            }
            else
            {
                sSQL = "SELECT NAME FROM sys.columns WHERE object_id = OBJECT_ID('dbo.POLICY_SUPP')";
            }

            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return objResult;
        }
        //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | End


        public DataSet GetDDSReserveTypeCodes(int iLOB)
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            string sSQL = "";

            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G, SYS_LOB_RESERVES SLR WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE' AND C.DELETED_FLAG <> -1 AND C.CODE_ID = SLR.RESERVE_TYPE_CODE"
                     + " AND SLR.LINE_OF_BUS_CODE = " + iLOB + " ORDER BY C.SHORT_CODE";
            }
            else
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G, SYS_LOB_RESERVES SLR WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE' AND C.DELETED_FLAG <> -1 AND C.CODE_ID = SLR.RESERVE_TYPE_CODE"
                     + " AND SLR.LINE_OF_BUS_CODE = " + iLOB + " ORDER BY C.SHORT_CODE";
            }

            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return objResult;
        }

        public DataSet GetReserveTypeCodes(string sShortCode)
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            string sSQL = "";

            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT C.CODE_ID, C.SHORT_CODE ,CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }
            else
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }

            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return objResult;
        }

        public DataSet GetLineOfBusinessCodes()
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();

            string sSQL = "";

            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'LINE_OF_BUSINESS' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }
            else
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'LINE_OF_BUSINESS' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }

            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL,m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return objResult;
        }

        public DataSet GetBankAccounts()
        {
            string sSQL = "SELECT ACCOUNT_NAME, ACCOUNT_ID, OWNER_EID FROM ACCOUNT";
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            return objResult;
        }

        public DataSet GetEntities()
        {
            string sSQL = "SELECT ENTITY_ID, ABBREVIATION, LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID IN (1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012)";
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            return objResult;
        }

        public bool GetAccountNames(ref PPAccountList objAccount)
        {
            bool bReturnValue = false;
            string sSql = string.Empty;
            string sTmp = string.Empty;
            string sAccountID = string.Empty;


            sSql = "SELECT ACCOUNT_ID, ACCOUNT_NAME, ACCOUNT_NUMBER FROM ACCOUNT WHERE ACCOUNT_NAME IS NOT NULL AND ACCOUNT_NUMBER IS NOT NULL";

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSql);
            try
            {
                while (objReader.Read())
                {
                    sAccountID = objReader.GetInt("ACCOUNT_ID").ToString();
                    sTmp = objReader.GetString("ACCOUNT_NAME");
                    sTmp = sTmp + " --- " + objReader.GetString("ACCOUNT_NUMBER");
                    objAccount.AccountNameNumber.Add(sTmp);
                    objAccount.AccountID.Add(sAccountID);
                }

                objReader.Close();
                bReturnValue = true;
            }
            catch (Exception e)
            {
                bReturnValue = false;
            }

            return bReturnValue;
        }

        /// <summary>
        /// Manika : Vsoni5 : 07/13/2011 : MITS 24924 : New function added to retrieve tables of Entity Type
        /// </summary>
        /// <returns></returns>
        public DataSet GetECTableName()
        {
            string sSQL = "select GLOSSARY_TEXT.TABLE_NAME, GLOSSARY_TEXT.TABLE_ID from GLOSSARY_TEXT,GLOSSARY " +
                          "where GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID and GLOSSARY.GLOSSARY_TYPE_CODE = 4 ORDER BY GLOSSARY_TEXT.TABLE_NAME";
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            return objResult;
        }
        /// <summary>
        /// Manika : Vsoni5 : 07/13/2011 : MITS 24924 : New function added to retrieve tables of People Type
        /// </summary>
        /// <returns></returns>
        public DataSet GetPeopleTableName()
        {
            string sSQL = "select GLOSSARY_TEXT.TABLE_NAME, GLOSSARY_TEXT.TABLE_ID from GLOSSARY_TEXT,GLOSSARY " +
                          "where GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID and GLOSSARY.GLOSSARY_TYPE_CODE = 7 ORDER BY GLOSSARY_TEXT.TABLE_NAME";
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            return objResult;
        }

        ////manika

        public int RetrieveJobFile(int p_iJobId, out MemoryStream p_objFileContents, ref string p_sFileName)
        {
            int iReturnValue = 0;
            MemoryStream objFileStream = null;
            DbReader objReader = null;
            StringBuilder sbSQL = null;
            byte[] arrDocBlob = null;
            try
            {
                //if ((p_sFileName == null) || (p_sFileName.Trim() == ""))
                //    throw new InvalidValueException
                //        (Globalization.GetString("FileStorageManager.RetrieveFile.FileNotSpecified"));

                // Strip off the path from the filename passed in
                //this.ParseFilePath(p_sFileName, ref sPathOnly, ref sFNOnly);

                //if (m_enmStorageType == StorageType.FileSystemStorage)
                //{
                //    sFileName = this.MaterializeFilename(sPathOnly, sFNOnly);
                //    if (!File.Exists(sFileName))
                //        throw new FileInputOutputException
                //            (Globalization.GetString("FileStorageManager.RetrieveFile.SourceFileNotExist"));

                //    objFileStream = new MemoryStream(this.FileAsByteArray(sFileName));
                //}
                //else if (m_enmStorageType == StorageType.DatabaseStorage)
                //{
                //if ((m_sDestinationStoragePath == null) || (m_sDestinationStoragePath.Trim() == ""))
                //    throw new InvalidValueException
                //        (Globalization.GetString("FileStorageManager.RetrieveFile.DestPathNotSpecified"));

                sbSQL = new StringBuilder();
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append("SELECT FILE_DATA, FILE_NAME FROM TM_JOBS_DOCUMENT WHERE");
                sbSQL.Append(" JOB_ID = '" + p_iJobId + "'");
                objReader = DbFactory.GetDbReader(m_TMConnectionString, sbSQL.ToString());
                if (objReader.Read())
                {
                    arrDocBlob = objReader.GetAllBytes("FILE_DATA");
                    p_sFileName = Conversion.ConvertObjToStr(objReader.GetValue("FILE_NAME"));
                    if (arrDocBlob != null)
                        objFileStream = new MemoryStream(arrDocBlob);
                }
                else
                {
                    throw new FileInputOutputException
                        (Globalization.GetString("Settings.RetrieveFile.SourceFileNotExist", m_iClientId));
                }

                //}
                p_objFileContents = objFileStream;
                iReturnValue = 1;
            }

            catch (InvalidValueException p_objException)
            {
                throw p_objException;
            }
            catch (FileInputOutputException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    (Globalization.GetString("Settings.RetrieveFile.Error", m_iClientId), p_objException);
            }
            finally
            {
                sbSQL = null;
                if (objFileStream != null)
                {
                    // BSB 03.24.2006 How can we expect calling code to use the MemoryStream reference
                    // if we close it before they get it ?????? 
                    // It's their object - let them use and close it...
                    // objFileStream.Close();

                    objFileStream = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }

            }
            return iReturnValue;
        }

        public int SetClaimForReplacement(DataIntegratorModel objDIModel)
        {
            int iRecordCount = 0;

            string sDateTime = Conversion.ToDbDateTime(System.DateTime.Now);
            string sSql = string.Empty;
            sSql = "UPDATE CLAIMANT SET INDSYS_EXTRT_DATE ='" + sDateTime + "' WHERE CLAIM_ID  = (SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + objDIModel.ClaimNumber + "')";

            //DbReader objReader = DbFactory.ExecuteScalar(m_connectionString, sSql);

            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                iRecordCount = objConn.ExecuteNonQuery(sSql);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
            return iRecordCount;
        }

        public int ResetClaimForInitialSubmit(DataIntegratorModel objDIModel)
        {
            int iRecordCount;
            string sDateTime = DateTime.Now.Year.ToString("yyyy") + DateTime.Now.Month.ToString("mm") + DateTime.Now.Day.ToString("dd") + DateTime.Now.Hour.ToString("hh") + DateTime.Now.Minute.ToString("mm") + DateTime.Now.Second.ToString("ss");
            string sSql = string.Empty;
            sSql = "UPDATE CLAIMANT SET INDSYS_EXTRT_DATE =NULL WHERE CLAIM_ID  = (SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + objDIModel.ClaimNumber + "')";

            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                iRecordCount = objConn.ExecuteNonQuery(sSql);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
            return iRecordCount;
        }

        public bool CreateISOCodeTables(string sTableName, string sSQL)
        {
            bool bStatus = false;
            string sSql2 = string.Empty;
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                sSql2 = "SELECT COUNT(*) FROM " + sTableName;
                objConn.ExecuteNonQuery(sSql2);
            }
            catch (Exception e)
            {
                objConn.ExecuteNonQuery(sSQL);
                bStatus = true;
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }

            return bStatus;
        }
        // Developer - Subhendu : MITS 30839 : Function CreateMappingTable Not Required any more. : Start
        //public void CreateMappingTable()
        //{
        //    DbConnection objConn = null;

        //    try
        //    {
        //        objConn = DbFactory.GetDbConnection(m_connectionString);
        //        objConn.Open();

        //        objConn.ExecuteNonQuery("SELECT COUNT(*) FROM CL_ISO_LOSS_TYPE_MAP");
        //    }
        //    catch (Exception e)
        //    {

        //        //objConn.ExecuteNonQuery("CREATE TABLE CL_ISO_LOSS_TYPE_MAP (CLAIM_TYPE_CODE INT NOT NULL, LOB INT NOT NULL, ISO_POLICY_CODE VARCHAR(5) NOT NULL, ISO_COVERAGE_CODE VARCHAR(5) NOT NULL, ISO_LOSS_CODE VARCHAR(5) NOT NULL)");
        //        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start

        //        objConn.ExecuteNonQuery("CREATE TABLE CL_ISO_LOSS_TYPE_MAP (ISO_ROW_ID INT PRIMARY KEY IDENTITY,CLAIM_TYPE_CODE INT NOT NULL, LOB INT, ISO_POLICY_CODE VARCHAR(5) NOT NULL, ISO_COVERAGE_CODE VARCHAR(5) NOT NULL, ISO_LOSS_CODE VARCHAR(5) NOT NULL, POLICY_LOB_CODE INT NOT NULL, COVERAGE_TYPE_CODE VARCHAR(5) NOT NULL, LOSS_TYPE_CODE VARCHAR(5) NOT NULL, DISABILITY_CODE VARCHAR(5) NOT NULL)");
        //        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End
        //        //objConn.ExecuteNonQuery("CREATE UNIQUE INDEX PK_ISO_LTM ON CL_ISO_LOSS_TYPE_MAP (LOB, CLAIM_TYPE_CODE)");
        //        //objConn.ExecuteNonQuery("CREATE UNIQUE INDEX PK_ISO_LTM ON CL_ISO_LOSS_TYPE_MAP (ISO_ROW_ID)");
        //    }
        //    finally
        //    {
        //        if (objConn != null)
        //        {
        //            objConn.Dispose();
        //        }
        //    }
        //}
        // Developer - Subhendu : MITS 30839 : Function CreateMappingTable Not Required any more. : End

        public DataSet GetISOLossMappings(int iLOB)
        {
            DataSet objDataSet = null;

            int iTableID = GetRMTableID("CLAIM_TYPE");

            string sSql = string.Empty;

            if (iLOB == 241)
            {
                sSql = "SELECT CODE_DESC, CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
                        + " AND CODES.LINE_OF_BUS_CODE = 241 AND CODES.TABLE_ID = " + iTableID + " AND CODES.DELETED_FLAG = 0 ORDER BY CODE_DESC";
            }
            else if (iLOB == 242)
            {
                sSql = "SELECT CODE_DESC, CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
                        + " AND CODES.LINE_OF_BUS_CODE = 242 AND CODES.TABLE_ID = " + iTableID + " AND CODES.DELETED_FLAG = 0 ORDER BY CODE_DESC";
            }
            else if (iLOB == 243)
            {
                sSql = "SELECT CODE_DESC, CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
                        + " AND CODES.LINE_OF_BUS_CODE = 243 AND CODES.TABLE_ID = " + iTableID + " AND CODES.DELETED_FLAG = 0 ORDER BY CODE_DESC";
            }
            //Subhendu 11/02/2010 MITS 22785: Function GetISOLossMappings is modified for Property claims LOB(845)
            else if (iLOB == 845)
            {
                sSql = "SELECT CODE_DESC, CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
                        + " AND CODES.LINE_OF_BUS_CODE = 845 AND CODES.TABLE_ID = " + iTableID + " AND CODES.DELETED_FLAG = 0 ORDER BY CODE_DESC";
            }

            objDataSet = DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);
            return objDataSet;
        }
        //Subhendu : 3/7/2013 : MITS 31855 : Function modified.
        public void GetRMClaimTypeMapping(ref DataIntegratorModel objDIModel)
        {
            string sSql = string.Empty;
            int iCovCode = (iCarrier == -1) ? objDIModel.iCovCode : 0;
            int iLossCode = (iCarrier == -1) ? objDIModel.iLossTypeCode : 0;
            int iDisabilityCode = (iCarrier == -1) ? objDIModel.iDisabilityCode : 0;
            int iPolicyLOB = (iCarrier == -1) ? objDIModel.iPolicyType : 0;

            if (DbFactory.IsOracleDatabase(m_connectionString))
            {
                sSql = "SELECT DESCRIP || '(' || RECORD_TYPE || ')' , POLICY_CODE || '(' || RECORD_TYPE || ')' ,RECORD_TYPE FROM CL_ISO_POLICY_CODES ORDER BY DESCRIP";
            }
            else
            {
                sSql = "SELECT DESCRIP + '(' + RECORD_TYPE + ')' , POLICY_CODE + '(' + RECORD_TYPE + ')' ,RECORD_TYPE FROM CL_ISO_POLICY_CODES ORDER BY DESCRIP";
            }

            objDIModel.dsISOPolicyType = DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);

            sSql = "SELECT CLMAP.ISO_POLICY_CODE, CLMAP.ISO_COVERAGE_CODE, CLMAP.ISO_LOSS_CODE, "
                    + "CL_ISO_LOSS_CODES.DESCRIP DESC1, CL_ISO_COVERAGE_CODES.DESCRIP DESC2, CL_ISO_POLICY_CODES.DESCRIP DESC3, CL_ISO_LOSS_CODES.RECORD_TYPE RECORD_TYPE "
                    + "FROM CL_ISO_LOSS_TYPE_MAP CLMAP, CL_ISO_POLICY_CODES, CL_ISO_COVERAGE_CODES, CL_ISO_LOSS_CODES "
                    + "WHERE (CLMAP.LOB = " + objDIModel.LOB + ") AND "
                    + "CLAIM_TYPE_CODE = " + objDIModel.RMClaimTypeID + " AND "
                    + "CLMAP.ISO_POLICY_CODE = CL_ISO_POLICY_CODES.POLICY_CODE AND "
                    + "CLMAP.ISO_POLICY_CODE = CL_ISO_COVERAGE_CODES.POLICY_CODE AND CLMAP.ISO_COVERAGE_CODE =  CL_ISO_COVERAGE_CODES.COVERAGE_CODE AND "
                    + "CLMAP.ISO_POLICY_CODE = CL_ISO_LOSS_CODES.POLICY_CODE AND "
                    + "CLMAP.ISO_COVERAGE_CODE = CL_ISO_LOSS_CODES.COVERAGE_CODE AND "
                    + "CLMAP.ISO_LOSS_CODE = CL_ISO_LOSS_CODES.LOSS_CODE AND "
                    + "CLMAP.POLICY_LOB_CODE = " + iPolicyLOB + " AND "
                    + "CLMAP.COVERAGE_TYPE_CODE = " + iCovCode + " AND "
                    + "CLMAP.LOSS_TYPE_CODE = " + iLossCode + " AND "
                    + "CLMAP.DISABILITY_CODE = " + iDisabilityCode;

            DataSet dsRMClaimTypeMapping = DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);
            string strCode = string.Empty;
            string sRecordType = string.Empty;

            if (dsRMClaimTypeMapping.Tables[0] != null && dsRMClaimTypeMapping.Tables[0].Rows.Count > 0)
            {
                sRecordType = dsRMClaimTypeMapping.Tables[0].Rows[0]["RECORD_TYPE"].ToString();

                strCode = dsRMClaimTypeMapping.Tables[0].Rows[0]["ISO_POLICY_CODE"].ToString();
                strCode = strCode + ": " + dsRMClaimTypeMapping.Tables[0].Rows[0]["DESC3"].ToString() + "(" + sRecordType + ")";
                objDIModel.sISOPolicyCode = strCode;

                strCode = dsRMClaimTypeMapping.Tables[0].Rows[0]["ISO_COVERAGE_CODE"].ToString();
                strCode = strCode + ": " + dsRMClaimTypeMapping.Tables[0].Rows[0]["DESC2"].ToString() + "(" + sRecordType + ")";
                objDIModel.sISOCoverageCode = strCode;

                strCode = dsRMClaimTypeMapping.Tables[0].Rows[0]["ISO_LOSS_CODE"].ToString();
                strCode = strCode + ": " + dsRMClaimTypeMapping.Tables[0].Rows[0]["DESC1"].ToString() + "(" + sRecordType + ")";
                objDIModel.sISOLossCode = strCode;

                objDIModel.sISORecordType = dsRMClaimTypeMapping.Tables[0].Rows[0]["RECORD_TYPE"].ToString();
            }
        }
        //Subhendu : 3/7/2013 : MITS 31855 : Function modified.
        public DataSet GetCoverageTypeMapping(int iLOB, string strPolicyCode, string strRecordType)
        {
            string sSql = string.Empty;
            string sPolicyCode = string.Empty;
            string sRecordType = string.Empty;
            string strPolicyCodeTmp = (strPolicyCode.IndexOf('(') == -1 ? strPolicyCode : strPolicyCode.Substring(0, strPolicyCode.IndexOf('(')));
            sRecordType = strPolicyCode.Substring(strPolicyCode.IndexOf('(') + 1, 4);

            if (DbFactory.IsOracleDatabase(m_connectionString))
            {
                sSql = "SELECT DESCRIP || '(' || RECORD_TYPE || ')' , COVERAGE_CODE FROM CL_ISO_COVERAGE_CODES WHERE POLICY_CODE ='" + strPolicyCodeTmp + "' AND RECORD_TYPE = '" + sRecordType + "' ORDER BY DESCRIP";
            }
            else
            {
                sSql = "SELECT DESCRIP +  '('  + RECORD_TYPE + ')' ,  COVERAGE_CODE FROM CL_ISO_COVERAGE_CODES WHERE POLICY_CODE ='" + strPolicyCodeTmp + "' AND RECORD_TYPE = '" + sRecordType + "' ORDER BY DESCRIP";
            }
            return DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);
        }
        //Subhendu : 3/7/2013 : MITS 31855 : Function modified.
        public DataSet GetLossTypeMapping(int iLOB, string strPolicyCode, string strCoverageCode, string strRecordType)
        {
            string sSql = string.Empty;
            string sPolicyCodeTmp = (strPolicyCode.IndexOf('(') == -1 ? strPolicyCode : strPolicyCode.Substring(0, strPolicyCode.IndexOf('(')));
            string sRecordType = strPolicyCode.Substring(strPolicyCode.IndexOf('(') + 1, 4);
            strCoverageCode = (strCoverageCode.IndexOf('(') == -1 ? strCoverageCode : strCoverageCode.Substring(0, strCoverageCode.IndexOf('(')));

            if (DbFactory.IsOracleDatabase(m_connectionString))
            {
                sSql = "SELECT DESCRIP || '(' || RECORD_TYPE || ')' , LOSS_CODE FROM CL_ISO_LOSS_CODES WHERE COVERAGE_CODE ='" + strCoverageCode + "' AND POLICY_CODE='" + sPolicyCodeTmp + "' AND RECORD_TYPE = '" + sRecordType + "' ORDER BY DESCRIP";
            }
            else
            {
                sSql = "SELECT DESCRIP + '(' + RECORD_TYPE + ')' , LOSS_CODE FROM CL_ISO_LOSS_CODES WHERE COVERAGE_CODE ='" + strCoverageCode + "' AND POLICY_CODE='" + sPolicyCodeTmp + "' AND RECORD_TYPE = '" + sRecordType + "' ORDER BY DESCRIP";
            }
            return DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);
        }
        //Subhendu : 3/7/2013 : MITS 31855 : Function modified.
        public void SaveRMLossTypeMapping(int iLOB, int iRMClaimTypeID, string sPolicyCode, string sCoverageCode, string sLossCode, int iPolicyType, int iLossTypeCode, int iDisabilityCode)
        {
            string sSql = string.Empty;
            sSql = "DELETE FROM CL_ISO_LOSS_TYPE_MAP WHERE LOB = " + iLOB + " AND CLAIM_TYPE_CODE=" + iRMClaimTypeID;

            string sPolicyCodeTmp = (sPolicyCode.IndexOf('(') == -1 ? sPolicyCode : sPolicyCode.Substring(0, sPolicyCode.IndexOf('(')));
            string sRecordType = sPolicyCode.Substring(sPolicyCode.IndexOf('(') + 1, 4);
            sCoverageCode = (sCoverageCode.IndexOf('(') == -1 ? sCoverageCode : sCoverageCode.Substring(0, sCoverageCode.IndexOf('(')));
            sLossCode = (sLossCode.IndexOf('(') == -1 ? sLossCode : sLossCode.Substring(0, sLossCode.IndexOf('(')));

            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSql);

                sSql = "INSERT INTO CL_ISO_LOSS_TYPE_MAP(CLAIM_TYPE_CODE,LOB,ISO_POLICY_CODE,ISO_COVERAGE_CODE,ISO_LOSS_CODE,ISO_RECORD_TYPE) VALUES (" + iRMClaimTypeID + "," + iLOB + ",'" + sPolicyCodeTmp + "','" + sCoverageCode + "','" + sLossCode + "','" + sRecordType + "')";
                objConn.ExecuteNonQuery(sSql);

            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }

        public DataSet GetLOBClaimType(int iLOB)
        {
            int iTableID = GetRMTableID("CLAIM_TYPE");
            string sSql = string.Empty;
            // Subhendu 6/22/2011 MITS 25287 - Start.
            DbConnection objconn = null;
            objconn = DbFactory.GetDbConnection(m_connectionString);
            objconn.Open();

            if (objconn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSql = "SELECT (CODES.SHORT_CODE || ' ' || CODES_TEXT.CODE_DESC), CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
                    + " AND CODES.LINE_OF_BUS_CODE = " + iLOB + " AND CODES.TABLE_ID = " + iTableID + " AND CODES.DELETED_FLAG = 0 ORDER BY CODE_DESC";
            }
            else
            {
                //Subhendu 4/14/2011 MITS- 23991 Short code prefixed with Code description for Claimtypecode
                sSql = "SELECT (CODES.SHORT_CODE + ' ' + CODES_TEXT.CODE_DESC), CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
                    + " AND CODES.LINE_OF_BUS_CODE = " + iLOB + " AND CODES.TABLE_ID = " + iTableID + " AND CODES.DELETED_FLAG = 0 ORDER BY CODE_DESC";
            }

            if (objconn != null)
            {
                objconn.Close();
                objconn.Dispose();
            }
            // Subhendu 6/22/2011 MITS 25287 - End.
            return DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);
        }

        public DataSet GetClaimantTypes()
        {
            CreateClmtMappingTable();

            int iTableID = GetRMTableID("CLAIMANT_TYPE");
            string sSql = string.Empty;

            sSql = "SELECT CODE_DESC, CODES.CODE_ID FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID =" + iTableID + " ORDER BY CODE_DESC";

            return DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);
        }
        //sagarwal54 08/14/2013 MITS 33168 start

        public DataSet GetLossInjury()
        {
            try
            {
                int iTableID = GetRMTableID("ISO_LOSS_INJURY");
                string sSQL = string.Empty;

                sSQL = "SELECT CT.CODE_DESC,C.SHORT_CODE FROM CODES_TEXT CT, CODES C WHERE c.TABLE_ID=" + iTableID + " and c.CODE_ID=ct.CODE_ID";

                return DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }

        }

        public DataSet GetDocumentTypes()
        {
            try
            {
                int iTableID = GetRMTableID("DOCUMENT_TYPE");
                string sSQL = string.Empty;

                sSQL = "SELECT CODE_DESC,ct.CODE_ID FROM CODES_TEXT CT, CODES C WHERE c.TABLE_ID=" + iTableID + " and c.CODE_ID=ct.CODE_ID";

                return DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }

        }

        public DataSet GetEnhancedNoteTypes()
        {
            try
            {
                int iTableID = GetRMTableID("NOTE_TYPE_CODE");
                string sSQL = string.Empty;

                sSQL = "SELECT ct.CODE_DESC, ct.CODE_ID FROM CODES C, CODES_TEXT CT WHERE C.TABLE_ID=" + iTableID + " AND C.CODE_ID = CT.CODE_ID";

                return DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }

        }

        public DataSet GetDiaryTypes()
        {
            try
            {
                int iTableID = GetRMTableID("WPA_ACTIVITIES");
                string sSQL = string.Empty;

                sSQL = "SELECT ct.CODE_DESC, ct.CODE_ID FROM CODES C, CODES_TEXT CT WHERE C.TABLE_ID=" + iTableID + " AND C.CODE_ID = CT.CODE_ID";

                return DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }

        }
        //sagarwal54 08/14/2013 MITS 33168 end

        public void GetClaimantTypeMapping(ref DataIntegratorModel objDIModel)
        {
            int iTableID = GetRMTableID("CLAIMANT_TYPE");
            string sSql = string.Empty;
            sSql = "SELECT CODE_ID, REL_CODE FROM CL_ISO_CODE_MAP WHERE CODE_ID =" + objDIModel.iClaimantMapping + " AND GROUP_ID =" + iTableID;

            DataSet dsClmtTypeMapping = DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);

            if (dsClmtTypeMapping.Tables[0] != null && dsClmtTypeMapping.Tables[0].Rows.Count > 0)
            {
                objDIModel.iClaimantMapping = Convert.ToInt32(dsClmtTypeMapping.Tables[0].Rows[0]["REL_CODE"].ToString());
            }
        }

        public void SaveClaimantTypeMapping(int iClmtTypeID, int iClmtMapping)
        {
            int iTableID = GetRMTableID("CLAIMANT_TYPE");
            string sSql = string.Empty;
            sSql = "DELETE FROM CL_ISO_CODE_MAP WHERE GROUP_ID =" + iTableID + " AND CODE_ID=" + iClmtTypeID;

            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSql);

                sSql = "INSERT INTO CL_ISO_CODE_MAP(CODE_ID,GROUP_ID,REL_CODE) VALUES (" + iClmtTypeID + "," + iTableID + "," + iClmtMapping + ")";
                objConn.ExecuteNonQuery(sSql);

            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }

        //Developer - Iti | Template - DA ISO Import Enhancement | Start

        /// </summary>
        /// <param name="sUserID"></param>
        /// <param name="iDSNID"></param>
        /// <param name="sSecurityConnection"></param>
        /// <returns></returns>
        public DataSet GetUserName(string sUserID, int iDSNID, string sSecurityConnection)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            try
            {
                sSQL = "SELECT LOGIN_NAME,USER_ID FROM USER_DETAILS_TABLE WHERE DSNID =" + iDSNID + " and USER_ID IN (" + sUserID + ")";
                objConn = DbFactory.GetDbConnection(sSecurityConnection);
                objConn.Open();
                return (DbFactory.GetDataSet(sSecurityConnection, sSQL, m_iClientId));
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }
        //Developer - Iti | Template - DA ISO Import Enhancement | End


        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
        /// <summary>
        /// Developer - Subhendu/Ihtesham : MITS 30839 : Function GetPolicyClaimType to fetch Claim Type based on selected Policy LOB or Claim LOB: Start
        /// </summary>
        /// <param name="iPolicy"></param>
        /// <returns></returns>
        public DataSet GetPolicyClaimType(int iPolicy)
        {
            int iTableID = GetRMTableID("CODE_REL_TYPE");
            string sSql = string.Empty;

            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                if (DbFactory.IsOracleDatabase(m_connectionString))
                {
                    sSql = "select CX.CODE2, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE  from CODE_X_CODE CX, CODES C, CODES_TEXT CT where CX.CODE1 =" + iPolicy + " and CX.REL_TYPE_CODE = C.CODE_ID and CT.CODE_ID = CX.CODE2 and c.TABLE_ID =" + iTableID + " AND C.SHORT_CODE ='POLTOCT' AND CX.DELETED_FLAG = 0 ORDER BY CT.SHORT_CODE";
                }
                else
                {
                    sSql = "select CX.CODE2, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE  from CODE_X_CODE CX, CODES C, CODES_TEXT CT where CX.CODE1 =" + iPolicy + " and CX.REL_TYPE_CODE = C.CODE_ID and CT.CODE_ID = CX.CODE2 and c.TABLE_ID =" + iTableID + " AND C.SHORT_CODE ='POLTOCT' AND CX.DELETED_FLAG = 0 ORDER BY CT.SHORT_CODE";
                }
                return (DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId));
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }
        /// Developer - Subhendu/Ihtesham : MITS 30839 : Function GetPolicyClaimType : End

        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
        /// <summary>
        /// Developer - Subhendu/Ihtesham : MITS 30839 : Function GetPolicyCoverageType to fetch Coverage Type based on selected Policy LOB or Claim LOB: Start
        /// </summary>
        /// <param name="iPolicy"></param>
        /// <returns></returns>
        //public DataSet GetPolicyCoverageType(int iPolicy) original
        public DataSet GetPolicyCoverageType(int iPolicy, int iPolicySystemId)//Developer – abharti5 |MITS 36676|
        {
            int iTableID = GetRMTableID("CVG_LOSS_LOB_MAPPING");
            string sSql = string.Empty;

            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                if (DbFactory.IsOracleDatabase(m_connectionString))
                {
                    //sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_LOB = " + iPolicy + " AND CT.CODE_ID = CVG.CVG_TYPE_CODE AND CT.CODE_ID = C.CODE_ID ORDER BY SHORT_CODE"; original
                    sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_SYSTEM_ID = " + iPolicySystemId + " AND CVG.POLICY_LOB = " + iPolicy + " AND CT.CODE_ID = CVG.CVG_TYPE_CODE AND CT.CODE_ID = C.CODE_ID ORDER BY SHORT_CODE"; //Developer – abharti5 |MITS 36676|
                }
                else
                {
                    //sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_LOB = " + iPolicy + " AND CT.CODE_ID = CVG.CVG_TYPE_CODE AND CT.CODE_ID = C.CODE_ID ORDER BY SHORT_CODE"; orignial
                    sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_SYSTEM_ID = " + iPolicySystemId + " AND CVG.POLICY_LOB = " + iPolicy + " AND CT.CODE_ID = CVG.CVG_TYPE_CODE AND CT.CODE_ID = C.CODE_ID ORDER BY SHORT_CODE"; //Developer – abharti5 |MITS 36676|
                }
                return (DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId));
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }
        /// Developer - Subhendu/Ihtesham : MITS 30839 : Function GetPolicyCoverageType : End
        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End

        /// <summary>
        /// Developer - Subhendu : MITS 30839 : Function GetPolicyTypeOfLossOrDisabilityCode to populate Type of loss or Disablity Code : Start
        /// </summary>
        /// <param name="iPolicy"></param>
        /// <param name="iCovCode"></param>
        /// <param name="sColumnName"></param>
        /// <returns></returns>
        //public DataSet GetPolicyTypeOfLossOrDisabilityCode(int iPolicy, int iCovCode, string sColumnName) original
        public DataSet GetPolicyTypeOfLossOrDisabilityCode(int iPolicy, int iCovCode, string sColumnName, int iPolicySystemId) //Developer – abharti5 |MITS 36676|
        {
            int iTableID = GetRMTableID("CVG_LOSS_LOB_MAPPING");
            string sSql = string.Empty;
            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                if (DbFactory.IsOracleDatabase(m_connectionString))
                {
                    //spahariya MITS 31978 to avoid duplicate codes for multiple reserve type
                    //sSql = "SELECT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_LOB = " + iPolicy + " AND CVG.CVG_TYPE_CODE = " + iCovCode + " AND CT.CODE_ID = CVG." + sColumnName + " AND CT.CODE_ID = C.CODE_ID";//LOSS_CODE
                    //sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_LOB = " + iPolicy + " AND CVG.CVG_TYPE_CODE = " + iCovCode + " AND CT.CODE_ID = CVG." + sColumnName + " AND CT.CODE_ID = C.CODE_ID";//LOSS_CODE original
                    sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_SYSTEM_ID = " + iPolicySystemId + " AND CVG.POLICY_LOB = " + iPolicy + " AND CVG.CVG_TYPE_CODE = " + iCovCode + " AND CT.CODE_ID = CVG." + sColumnName + " AND CT.CODE_ID = C.CODE_ID"; //Developer – abharti5 |MITS 36676|
                }
                else
                {
                    //spahariya MITS 31978 to avoid duplicate codes for multiple reserve type
                    //sSql = "SELECT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_LOB = " + iPolicy + " AND CVG.CVG_TYPE_CODE = " + iCovCode + " AND CT.CODE_ID = CVG." + sColumnName + " AND CT.CODE_ID = C.CODE_ID";
                    //sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_LOB = " + iPolicy + " AND CVG.CVG_TYPE_CODE = " + iCovCode + " AND CT.CODE_ID = CVG." + sColumnName + " AND CT.CODE_ID = C.CODE_ID"; original
                    sSql = "SELECT DISTINCT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE  FROM CVG_LOSS_LOB_MAPPING CVG, CODES C, CODES_TEXT CT WHERE CVG.POLICY_SYSTEM_ID = " + iPolicySystemId + " AND CVG.POLICY_LOB = " + iPolicy + " AND CVG.CVG_TYPE_CODE = " + iCovCode + " AND CT.CODE_ID = CVG." + sColumnName + " AND CT.CODE_ID = C.CODE_ID"; //Developer – abharti5 |MITS 36676|
                }
                return (DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId));
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }

        //kkaur25-UI JIRA 9651 start function for checking sequence in oracle
        public int GetDataS(string Cloud_StagingConn,string sSql, string tablename)
        {
            int iCount = 0;
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(Cloud_StagingConn);
            
            try
            {
                objConn.Open();
                DbCommand objCommand = objConn.CreateCommand();
                objCommand.CommandText = sSql;
                iCount = Convert.ToInt32(objCommand.ExecuteScalar());
                return iCount;
            }
            
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }
        //kkaur25-UI JIRA 9651 end function for checking sequence in oracle



        //Sagarwal54 MITS 35386 start
        //function to bind the drop down lists on ISOPropertyMapping page
        public DataSet GetrmAPropTypeAndISOPropType()
        {
            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            StringBuilder sSql = new StringBuilder();
            try
            {
                if (DbFactory.IsOracleDatabase(m_connectionString))
                {
                    DataSet dsOracle = new DataSet();
                    DataTable dt, dt1 = null;
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID", GetRMTableID("PROPERTY_TYPE")));
                    dt = (DataTable)DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId).Tables[0].Copy();
                    dt.TableName = "Table";
                    dsOracle.Tables.Add(dt);
                    sSql.Remove(0, sSql.Length);
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID", GetRMTableID("CL_ISO_PROPERTY_CODES")));
                    dt1 = (DataTable)(DataTable)DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId).Tables[0].Copy();
                    dt1.TableName = "Table1";
                    dsOracle.Tables.Add(dt1);
                    return dsOracle;
                }
                else
                {
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID;", GetRMTableID("PROPERTY_TYPE")));
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID;", GetRMTableID("CL_ISO_PROPERTY_CODES")));


                }

            }
            catch (Exception exe)
            {
                //no need to handle

            }
            return (DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId));

        }

        public XmlDocument ISOSavePropertyMapping(XmlDocument p_objXmlDocument)
        {
            int sISORowID = 0;
            int srmAPropType = 0;
            int sISOPropType = 0;
            string sSQL = "";

            XmlElement objElm = null;
            DbConnection objConn = null;
            try
            {

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='rmA Property Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        srmAPropType = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Property Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISOPropType = Convert.ToInt32(objElm.InnerText);
                    }
                }


                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO_ROW_ID']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISORowID = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                if (sISORowID == 0)
                {
                    if (DbFactory.IsOracleDatabase(m_connectionString))
                    {
                        sSQL = "INSERT INTO CL_ISO_PROPERTY_TYPE_MAP(ISO_ROW_ID,RMA_PROPERTY_TYPE,ISO_PROPERTY_TYPE) VALUES(SEQ_CL_ISO_PROPERTY_TYPE_MAP.nextval," + srmAPropType + "," + sISOPropType + ")";
                    }
                    else
                    {
                        sSQL = "INSERT INTO CL_ISO_PROPERTY_TYPE_MAP VALUES(" + srmAPropType + "," + sISOPropType + ")";
                    }
                }
                else
                {
                    sSQL = "UPDATE CL_ISO_PROPERTY_TYPE_MAP SET RMA_PROPERTY_TYPE =" + srmAPropType + " , ISO_PROPERTY_TYPE =" + sISOPropType + " WHERE ISO_ROW_ID= " + sISORowID;// +" AND ISO_PROPERTY_TYPE= " + sISOPropType;
                }
                objConn.ExecuteNonQuery(sSQL);

                return p_objXmlDocument;
            }
            catch (System.Data.SqlClient.SqlException sqe)
            {
                Log.Write(sqe.ToString(),m_iClientId);
                throw new RMAppException("Mapping for this rmA Property type already exists!");
            }
            catch (Oracle.DataAccess.Client.OracleException oex)
            {
                Log.Write(oex.ToString(), m_iClientId);
                throw new RMAppException("Mapping for this rmA Property type already exists!");
            }
            catch (Exception e)
            {
                Log.Write(e.ToString(), m_iClientId);
                //Add code to catch exception for Primary key violation
                throw e;// new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        //function to display existing mapping in grid      
        public XmlDocument ISOGetPropType(XmlDocument p_objXmlIn)
        {
            GetISOPromapping("ISO_ROW_ID,RMA_PROPERTY_TYPE,ISO_PROPERTY_TYPE", "CL_ISO_PROPERTY_TYPE_MAP", ref p_objXmlIn);
            return p_objXmlIn;
        }

        private void GetISOPromapping(string sFields, string sTableName, ref XmlDocument p_objXmlDocument)
        {
            DataSet objDs = null;
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            XmlElement objLstRow = null;
            string sSQL = string.Empty;
            XmlElement objElm = null;
            XmlElement objRowTxt = null;
            try
            {
                sSQL = String.Format("SELECT {0} FROM {1}", sFields, sTableName);
                objDs = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
                objNode = p_objXmlDocument.SelectSingleNode("//*[name()='" + "IsoPropertyList" + "']");
                objNodLst = objNode.ChildNodes.Item(0).ChildNodes;
                foreach (DataRow objRow in objDs.Tables[0].Rows)
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");

                    for (int i = 0; i < objNodLst.Count - 1; i++)
                    {
                        objElm = (XmlElement)objNodLst[i];
                        objRowTxt = p_objXmlDocument.CreateElement(objNodLst[i].Name);
                        //if (objElm.Name == "ISO_ROW_ID")
                        //{
                        //    objRowTxt.InnerText = Convert.ToString(Convert.ToInt32(objRow["ISO_ROW_ID"]));
                        //}
                        //else
                        if (objElm.Name == "RMA_PROPERTY_TYPE")
                        {
                            objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["RMA_PROPERTY_TYPE"]));
                        }
                        if (objElm.Name == "ISO_PROPERTY_TYPE")
                        {
                            objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["ISO_PROPERTY_TYPE"]));
                        }
                        objLstRow.AppendChild(objRowTxt);
                    }
                    objRowTxt = p_objXmlDocument.CreateElement("RowId");
                    objRowTxt.InnerText = Convert.ToString(Convert.ToInt32(objRow["ISO_ROW_ID"])) + "|" + objRow["RMA_PROPERTY_TYPE"].ToString() + "|" + objRow["ISO_PROPERTY_TYPE"].ToString();
                    objLstRow.AppendChild(objRowTxt);
                    objNode.AppendChild((XmlNode)objLstRow);
                }
            }
            catch (Exception exe)
            {
                throw;
            }
        }


        public XmlDocument ISODeletePropertyMapping(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                string WhereClause = string.Empty;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='rmA Property Type']");
                if (objElm != null)
                {
                    WhereClause = "RMA_PROPERTY_TYPE =" + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Property Type']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND ISO_PROPERTY_TYPE = " + objElm.InnerText;
                }
                DeleteGridEntry(WhereClause, "CL_ISO_PROPERTY_TYPE_MAP");
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
            }
        }


        //Sagarwal54 MITS 35386 end

        //Sagarwal54 MITS 35704 start
        public XmlDocument ISOGetPartyToClaimTypes(XmlDocument p_objXmlIn)
        {
            GetMMSEAPartyToClaimMapping("ISO_ROW_ID,RMA_CLAIM_PARTY_TYPE,ISO_CLAIM_PARTY_TYPE", "CL_ISO_ADDITIONAL_CLAIMANT_MAP", ref p_objXmlIn);
            return p_objXmlIn;
        }

        private void GetMMSEAPartyToClaimMapping(string sFields, string sTableName, ref XmlDocument p_objXmlDocument)
        {
            DataSet objDs = null;
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            XmlElement objLstRow = null;
            string sSQL = string.Empty;
            XmlElement objElm = null;
            XmlElement objRowTxt = null;
            try
            {
                sSQL = String.Format("SELECT {0} FROM {1}", sFields, sTableName);
                objDs = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
                objNode = p_objXmlDocument.SelectSingleNode("//*[name()='" + "ClaimParty" + "']");
                objNodLst = objNode.ChildNodes.Item(0).ChildNodes;
                foreach (DataRow objRow in objDs.Tables[0].Rows)
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");

                    for (int i = 0; i < objNodLst.Count - 1; i++)
                    {
                        objElm = (XmlElement)objNodLst[i];
                        objRowTxt = p_objXmlDocument.CreateElement(objNodLst[i].Name);
                        //if (objElm.Name == "ISO_ROW_ID")
                        //{
                        //    objRowTxt.InnerText = Convert.ToString(Convert.ToInt32(objRow["ISO_ROW_ID"]));
                        //}
                        //else
                        if (objElm.Name == "RMA_CLAIM_PARTY_TYPE")
                        {
                            objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["RMA_CLAIM_PARTY_TYPE"]));
                        }
                        if (objElm.Name == "ISO_CLAIM_PARTY_TYPE")
                        {
                            objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["ISO_CLAIM_PARTY_TYPE"]));
                        }
                        objLstRow.AppendChild(objRowTxt);
                    }
                    objRowTxt = p_objXmlDocument.CreateElement("RowId");
                    objRowTxt.InnerText = Convert.ToString(Convert.ToInt32(objRow["ISO_ROW_ID"])) + "|" + objRow["RMA_CLAIM_PARTY_TYPE"].ToString() + "|" + objRow["ISO_CLAIM_PARTY_TYPE"].ToString();
                    objLstRow.AppendChild(objRowTxt);
                    objNode.AppendChild((XmlNode)objLstRow);
                }
            }
            catch (Exception exe)
            {
                throw;
            }
        }



        /// <summary>
        /// This function is used to populate drop downs on ISOClaimPartyType.aspx
        /// </summary>
        /// <returns></returns>
        public DataSet GetAdditionalClaimantRoleCodes()
        {

            StringBuilder sSql = new StringBuilder();
            try
            {
                if (DbFactory.IsOracleDatabase(m_connectionString))
                {
                    DataSet dsOracle = new DataSet();
                    DataTable dt, dt1 = null;
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID", GetRMTableID("MMSEA_CLMPRTY_CODE")));
                    dt = (DataTable)DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId).Tables[0].Copy();
                    dt.TableName = "Table";
                    dsOracle.Tables.Add(dt);
                    sSql.Remove(0, sSql.Length);
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID", GetRMTableID("CL_ISO_ADD_CLAIMANT_ROLES")));
                    dt1 = (DataTable)(DataTable)DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId).Tables[0].Copy();
                    dt1.TableName = "Table1";
                    dsOracle.Tables.Add(dt1);
                    return dsOracle;
                }
                else
                {
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID;", GetRMTableID("MMSEA_CLMPRTY_CODE")));
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID;", GetRMTableID("CL_ISO_ADD_CLAIMANT_ROLES")));
                }

            }
            catch (Exception exe)
            {
                //no need to handle

            }
            return (DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId));

        }

        public XmlDocument ISOSaveAdditionalClaimantMapping(XmlDocument p_objXmlDocument)
        {
            int sISORowID = 0;
            int srmAPartyType = 0;
            int sISOPartyType = 0;
            string sSQL = "";

            XmlElement objElm = null;
            DbConnection objConn = null;
            try
            {

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='rmA Additional Claimant Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        srmAPartyType = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Additional Claimant Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISOPartyType = Convert.ToInt32(objElm.InnerText);
                    }
                }


                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO_ROW_ID']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISORowID = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                if (sISORowID == 0)
                {
                    if (DbFactory.IsOracleDatabase(m_connectionString))
                    {
                        sSQL = "INSERT INTO CL_ISO_ADDITIONAL_CLAIMANT_MAP(ISO_ROW_ID, RMA_CLAIM_PARTY_TYPE, ISO_CLAIM_PARTY_TYPE) VALUES(SEQ_CL_ISO_ADD_CLAIMANT_MAP.nextval," + srmAPartyType + "," + sISOPartyType + ")";
                    }
                    else
                    {
                        sSQL = "INSERT INTO CL_ISO_ADDITIONAL_CLAIMANT_MAP VALUES(" + srmAPartyType + "," + sISOPartyType + ")";
                    }
                }
                else
                {
                    sSQL = "UPDATE CL_ISO_ADDITIONAL_CLAIMANT_MAP SET RMA_CLAIM_PARTY_TYPE =" + srmAPartyType + " , ISO_CLAIM_PARTY_TYPE =" + sISOPartyType + " WHERE ISO_ROW_ID= " + sISORowID;// +" AND ISO_PROPERTY_TYPE= " + sISOPropType;
                }
                objConn.ExecuteNonQuery(sSQL);

                return p_objXmlDocument;
            }
            catch (System.Data.SqlClient.SqlException sqe)
            {
                Log.Write(sqe.ToString(), m_iClientId);
                throw new RMAppException("Mapping for this rmA Additional Claimant type already exists!");
            }
            catch (Oracle.DataAccess.Client.OracleException oex)
            {
                Log.Write(oex.ToString(), m_iClientId);
                throw new RMAppException("Mapping for this rmA Additional Claimant type already exists!");
            }

            catch (Exception e)
            {
                Log.Write(e.ToString(), m_iClientId);
                throw e;// new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        public XmlDocument ISODeleteAdditionalClaimantMapping(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                string WhereClause = string.Empty;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='rmA Additional Claimant Type']");
                if (objElm != null)
                {
                    WhereClause = "RMA_CLAIM_PARTY_TYPE =" + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Additional Claimant Type']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND ISO_CLAIM_PARTY_TYPE = " + objElm.InnerText;
                }
                DeleteGridEntry(WhereClause, "CL_ISO_ADDITIONAL_CLAIMANT_MAP");
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
            }
        }

        //Sagarwal54 MITS 35704 End
        // Developer - Subhendu : MITS 30839 : Function GetPolicyTypeOfLossOrDisabilityCode  : End
        public DataSet GetISOJurisStates()
        {
            string sSQL = "SELECT STATE_NAME, STATE_ROW_ID FROM STATES WHERE STATE_NAME IS NOT NULL AND DELETED_FLAG != -1 ORDER BY STATE_NAME";
            DataSet objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            return objResult;
        }

        public void UpdateClaimantTable()
        {
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                objConn.ExecuteNonQuery("SELECT INDSYS_EXTRT_DATE FROM CLAIMANT WHERE CLAIMANT_ROW_ID = -1");
            }
            catch (Exception e)
            {
                objConn.ExecuteNonQuery("ALTER TABLE CLAIMANT ADD INDSYS_EXTRT_DATE VARCHAR(14) NULL");
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }

        // Private functions

        private int GetMaxOptionSetId()
        {
            string sql = "SELECT MAX(OPTIONSET_ID) FROM DATA_INTEGRATOR";

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);
            objReader.Read();

            int id = objReader.GetInt32(0);

            return id;
        }

        /// <summary>
        /// // Developer - Subhendu : MITS 30839 : Function GetMaxId to get maximum of a provided column in a table  : Start
        /// </summary>
        /// <param name="sColumnname"></param>
        /// <param name="sTablename"></param>
        /// <returns></returns>
        private int GetMaxId(string sColumnname, string sTablename)
        {
            string sSQL = string.Format(@"SELECT {0} FROM {1}", sColumnname, sTablename);
            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL);
            objReader.Read();

            return (objReader.GetInt32(0) + 1);
        }
        // Developer - Subhendu : MITS 30839 : Function GetMaxId to get maximum of a provided column in a table  : End

        private string RetrieveXMLFromDB(int userId, string optionSetName)
        {
            string s_xml = string.Empty;
            int s_uid;
            string sql = "SELECT XML_STRING,USER_ID"
                + " FROM DATA_INTEGRATOR "
                + " WHERE MODULE_NAME = " + "'" + m_moduleName + "'"
                + " AND OPTIONSET_NAME = " + "'" + optionSetName + "'";

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);
            objReader.Read();
            //kkaur25 -JIRA 14097 start
            s_uid = objReader.GetInt("USER_ID");
            s_xml = objReader.GetString("XML_STRING");
            
                if (s_uid != userId)
                {
                    throw new RMAppException("You are not authorized to access this data as user is different.");
                }

                //kkaur25 -JIRA 14097 end  
               return s_xml; 
        }

        private void InsertIntoDB(int optionSetId, int userId, string optionSetName, string xmlString)
        {
            DbConnection objConn = null;
            DbCommand objCommand = null;

            // kkaur25  08/05/2014 : MITS 37067 : Modifying oracle connection to the function InsertIntoDB to create or update the optionsest : End

            //begin



            using (objConn = DbFactory.GetDbConnection(m_connectionString))
                {
                    if (DbFactory.IsOracleDatabase(m_connectionString))
                    {
                        try
                        {
                            objConn.Open();
                            using (DbCommand oDbCommand = objConn.CreateCommand())
                            {
                                oDbCommand.CommandText = "USP_INS_DATA_INTEGRATOR_ORACLE";
                                oDbCommand.Connection = objConn;
                                oDbCommand.CommandType = CommandType.StoredProcedure;
                                DbParameter oParam = oDbCommand.CreateParameter();
                                oParam.SourceColumn = "P_OPTIONSET_ID";
                                oParam.Value = optionSetId;
                                oParam.Direction = ParameterDirection.Input;
                                DbParameter oParam1 = oDbCommand.CreateParameter();
                                oParam1.SourceColumn = "P_USER_ID";
                                oParam1.Value = userId;
                                oParam1.Direction = ParameterDirection.Input;
                                DbParameter oParam2 = oDbCommand.CreateParameter();
                                oParam2.SourceColumn = "P_MODULE_NAME";
                                oParam2.Value = m_moduleName;
                                oParam2.Direction = ParameterDirection.Input;
                                DbParameter oParam3 = oDbCommand.CreateParameter();
                                oParam3.SourceColumn = "P_OPTIONSET_NAME";
                                oParam3.Value = optionSetName;
                                oParam3.Direction = ParameterDirection.Input;
                                DbParameter oParam4 = oDbCommand.CreateParameter();
                                oParam4.SourceColumn = "P_XML_STRING";
                                oParam4.Value = xmlString;
                                oParam4.Direction = ParameterDirection.Input;
                                DbParameter oParam5 = oDbCommand.CreateParameter();
                                oParam5.SourceColumn = "P_INPUT_TYPE";
                                oParam5.Value = 'I';
                                oParam5.Direction = ParameterDirection.Input;
                                oDbCommand.Parameters.Add(oParam);
                                oDbCommand.Parameters.Add(oParam1);
                                oDbCommand.Parameters.Add(oParam2);
                                oDbCommand.Parameters.Add(oParam3);
                                oDbCommand.Parameters.Add(oParam4);
                                oDbCommand.Parameters.Add(oParam5);
                                oDbCommand.ExecuteNonQuery();
                            }

                        }

                        catch (Exception ex)
                        {
                            System.Console.WriteLine("Exception: {0}", ex.ToString());
                        }
                    }

                    else
                    {
                         string sql = "INSERT INTO DATA_INTEGRATOR"
                        + " (OPTIONSET_ID, USER_ID, MODULE_NAME, OPTIONSET_NAME, XML_STRING)"
                        + " VALUES (" + optionSetId + ", "
                                + userId + ", "
                                + "'" + m_moduleName + "', "
                                + "'" + optionSetName + "', "
                                + "'" + xmlString + "')";
                        objConn.Open();
                        objCommand = objConn.CreateCommand();
                        objCommand.CommandText = sql;
                        objCommand.ExecuteNonQuery();
                    }
            }

            if (objConn != null)
                objConn.Dispose();
        }


        //private void InsertIntoDB(int optionSetId, int userId, string optionSetName, string xmlString)
        //{
        //    DbConnection objConn = null;
        //    DbCommand objCommand = null;

        //    string sql = "INSERT INTO DATA_INTEGRATOR"
        //        + " (OPTIONSET_ID, USER_ID, MODULE_NAME, OPTIONSET_NAME, XML_STRING)"
        //        + " VALUES (" + optionSetId + ", "
        //                + userId + ", "
        //                + "'" + m_moduleName + "', "
        //                + "'" + optionSetName + "', "
        //                + "'" + xmlString + "')";

        //    objConn = DbFactory.GetDbConnection(m_connectionString);
        //    objConn.Open();
        //    objCommand = objConn.CreateCommand();
        //    objCommand.CommandText = sql;
        //    objCommand.ExecuteNonQuery();

        //    if (objConn != null)
        //        objConn.Dispose();
        //}

        // end kkaur25 MITS 37067


        public void InsertReserveMappings(int optionSetId, DataSet reserveMappings)
        {
            string sLOB = string.Empty;
            string sTemp = string.Empty;
            string sql = string.Empty;
            string sCodeDesc = string.Empty;
            int iCodeID = 0;
            DbConnection objConn = null;
            DbCommand objCommand = null;

            if (reserveMappings != null && reserveMappings.Tables != null)
            {
                foreach (DataRow row in reserveMappings.Tables[0].Rows)
                {
                    sLOB = row["LOB"].ToString();
                    sTemp = row["ReserveType"].ToString();
                    sTemp = sTemp.Substring(0, sTemp.IndexOf(" "));
                    sql = "SELECT CT.CODE_ID , CT.CODE_DESC FROM CODES C , CODES_TEXT CT , SYS_LOB_RESERVES SLR "
                       + " WHERE CT.CODE_ID = SLR.RESERVE_TYPE_CODE "
                       + " AND CT.CODE_ID = C.CODE_ID "
                       + " AND C.SHORT_CODE  = '" + sTemp + "' "
                       + " AND SLR.LINE_OF_BUS_CODE = " + sLOB;
                    DbReader objReader = DbFactory.GetDbReader(m_connectionString, sql);
                    try
                    {
                        while (objReader.Read())
                        {
                            iCodeID = objReader.GetInt("CODE_ID");
                            sCodeDesc = objReader.GetString("CODE_DESC");
                        }
                        objReader.Close();
                    }
                    catch (Exception e)
                    {
                    }

                    sql = "INSERT INTO USER_DEF_RESERVE_MAP"
                    + " (OPTIONSET_ID, LOB_CODE, BUCKET, RSV_TYPE_SHORT_CODE, RSV_TYPE_CODE, CODE_DESC)"
                    + " VALUES (" + optionSetId + ", "
                            + sLOB + ", "
                            + "'" + row["Bucket"] + "', "
                            + "'" + sTemp + "', "
                            + "'" + iCodeID + "', "
                            + "'" + sCodeDesc + "')";
                    // npradeepshar 03/24/2011 Merging changes fROM R6PS1
                    //Start rsushilaggar MITS 21761,22550 Date 30-Aug-200
                    try
                    {
                        objConn = DbFactory.GetDbConnection(m_DAConnectionString);
                        objConn.Open();
                        objCommand = objConn.CreateCommand();
                        objCommand.CommandText = sql;
                        objCommand.ExecuteNonQuery();
                        if (objConn != null)
                            objConn.Dispose();
                    }
                    catch (Exception ex)
                    {

                    }
                    //End rsushilaggar
                }

            }

        }


        private void SaveXML2DB(int userId, string optionSetName, string Xml)
        {
            DbConnection objConn = null;
            DbCommand objCommand = null;


  
            // kkaur25  08/05/2014 : MITS 37067 : Adding oracle connection to the function SaveXML2DB to create the optionsest : End
            
           //begin



            using (objConn = DbFactory.GetDbConnection(m_connectionString))
            {
                if (DbFactory.IsOracleDatabase(m_connectionString))
                {
                    try
                    {
                        objConn.Open();
                        using (DbCommand oDbCommand = objConn.CreateCommand())
                        {
                            oDbCommand.CommandText = "USP_INS_DATA_INTEGRATOR_ORACLE";
                            oDbCommand.Connection = objConn;
                            oDbCommand.CommandType = CommandType.StoredProcedure;
                            DbParameter oParam = oDbCommand.CreateParameter();
                            oParam.SourceColumn = "P_OPTIONSET_ID";
                            oParam.Value = "";
                            oParam.Direction = ParameterDirection.Input;
                            DbParameter oParam1 = oDbCommand.CreateParameter();
                            oParam1.SourceColumn = "P_USER_ID";
                            oParam1.Value = userId;
                            oParam1.Direction = ParameterDirection.Input;
                            DbParameter oParam2 = oDbCommand.CreateParameter();
                            oParam2.SourceColumn = "P_MODULE_NAME";
                            oParam2.Value = m_moduleName;
                            oParam2.Direction = ParameterDirection.Input;
                            DbParameter oParam3 = oDbCommand.CreateParameter();
                            oParam3.SourceColumn = "P_OPTIONSET_NAME";
                            oParam3.Value = optionSetName;
                            oParam3.Direction = ParameterDirection.Input;
                            DbParameter oParam4 = oDbCommand.CreateParameter();
                            oParam4.SourceColumn = "P_XML_STRING";
                            oParam4.Value = Xml;
                            oParam4.Direction = ParameterDirection.Input;
                            DbParameter oParam5 = oDbCommand.CreateParameter();
                            oParam5.SourceColumn = "P_INPUT_TYPE";
                            oParam5.Value = 'U';
                            oParam5.Direction = ParameterDirection.Input;
                            oDbCommand.Parameters.Add(oParam);
                            oDbCommand.Parameters.Add(oParam1);
                            oDbCommand.Parameters.Add(oParam2);
                            oDbCommand.Parameters.Add(oParam3);
                            oDbCommand.Parameters.Add(oParam4);
                            oDbCommand.Parameters.Add(oParam5);
                            oDbCommand.ExecuteNonQuery();
                        }

                    }

                    catch (Exception ex)
                    {
                        System.Console.WriteLine("Exception: {0}", ex.ToString());
                    }
                }

                else
                {


                    string sql = "UPDATE DATA_INTEGRATOR"
                        + " SET XML_STRING = " + "'" + Xml + "'"
                        + " WHERE USER_ID = " + userId
                        + " AND MODULE_NAME = " + "'" + m_moduleName + "'"
                        + " AND OPTIONSET_NAME = " + "'" + optionSetName + "'";

                    objConn = DbFactory.GetDbConnection(m_connectionString);
                    objConn.Open();
                    objCommand = objConn.CreateCommand();
                    objCommand.CommandText = sql;
                    objCommand.ExecuteNonQuery();
                }
            }

            if (objConn != null)
                objConn.Dispose();
        }
        //end  MITS 37067

        private int GetRMTableID(string sTableName)
        {
            int iTableID = 0;
            string sSql = string.Empty;
            sSql = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'";

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSql);

            try
            {
                while (objReader.Read())
                {
                    iTableID = objReader.GetInt("TABLE_ID");
                }

                objReader.Close();
            }
            catch (Exception e)
            {
            }

            return iTableID;
        }

        private void CreateClmtMappingTable()
        {
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                objConn.ExecuteNonQuery("SELECT COUNT(*) FROM CL_ISO_CODE_MAP");
            }
            catch (Exception e)
            {
                objConn.ExecuteNonQuery("CREATE TABLE CL_ISO_CODE_MAP(CODE_ID INTEGER NOT NULL, GROUP_ID INTEGER NOT NULL, REL_CODE SMALLINT)");
                objConn.ExecuteNonQuery("CREATE UNIQUE INDEX PK_CLRELRES ON CL_ISO_CODE_MAP(CODE_ID,GROUP_ID)");
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }
        //pmittal5 
        //Fetch existing Admin Tracking tables in case of DIS 
        public DataSet GetAdminTrackingTables()
        {
            string sSQL = string.Empty;
            DataSet objResult = null;
            try
            {
                sSQL = "SELECT GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY.GLOSSARY_TYPE_CODE = 468 AND GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID";
                objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            }
            catch (Exception e)
            {
            }
            return objResult;
        }

        //Fetch the fields of selected Admin Tracking table
        public DataSet GetAdminTrackFields(string sTableName)
        {
            string sSQL = string.Empty;
            DataSet objResult = null;
            DbReader objReader = null;
            try
            {
                if (sTableName != "")
                {
                    sSQL = "SELECT SYS_FIELD_NAME FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sTableName + "'";
                    objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
            return objResult;
        }
        //End- pmittal5

        //pmittal5  
        //Fetch existing Bank Accounts/Sub Accounts in case of DDS 
        public DataSet GetDDSBankAccounts()
        {
            string sSQL = string.Empty;
            int iUseSubAccounts = 0;
            DataSet objResult = null;

            try
            {
                sSQL = "SELECT USE_SUB_ACCOUNT FROM SYS_PARMS";
                using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL))
                {
                    if (objReader != null && objReader.Read())
                        iUseSubAccounts = objReader.GetInt16("USE_SUB_ACCOUNT");
                }
                if (iUseSubAccounts == 0)
                    sSQL = "SELECT ACCOUNT_ID, ACCOUNT_NAME, ACCOUNT_NUMBER FROM ACCOUNT WHERE ACCOUNT_NAME IS NOT NULL AND ACCOUNT_NUMBER IS NOT NULL";
                else
                    sSQL = "SELECT SUB_ROW_ID,SUB_ACC_NAME,SUB_ACC_NUMBER FROM BANK_ACC_SUB WHERE SUB_ACC_NAME IS NOT NULL AND SUB_ACC_NUMBER IS NOT NULL";
                objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);

            }
            catch (Exception e)
            {
            }
            finally
            {
                if (objResult != null)
                    objResult.Dispose();
            }
            return objResult;
        }

        // csingh7 : To know whether USE_SUB_ACCOUNT setting is enabled in the utilities.
        private int GetSubAccEnabled()
        {
            string sSQL = string.Empty;
            int iUseSubAccounts = 0;

            sSQL = "SELECT USE_SUB_ACCOUNT FROM SYS_PARMS";
            using (DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSQL))
            {
                if (objReader != null && objReader.Read())
                    iUseSubAccounts = objReader.GetInt16("USE_SUB_ACCOUNT");
            }
            return iUseSubAccounts;
        }

        public void SaveReserveMappings(int optionsetID, DataSet reserveMappings)
        {
            DbConnection objConn = null;
            DbCommand objCommand = null;
            string sql = string.Empty;

            sql = "DELETE FROM USER_DEF_RESERVE_MAP WHERE OPTIONSET_ID = " + optionsetID;

            objConn = DbFactory.GetDbConnection(m_DAConnectionString);
            objConn.Open();
            objCommand = objConn.CreateCommand();
            objCommand.CommandText = sql;
            objCommand.ExecuteNonQuery();
            if (objConn != null)
                objConn.Dispose();
            InsertReserveMappings(optionsetID, reserveMappings);
        }

        //kkaur25 start UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder
        public void RetrieveFile(string sFileName, string sFilePath,string sStagingDataSource)
        {
            MemoryStream objReturnValue = null;
            objReturnValue = CommonFunctions.RetreiveTempFileFromDB(sFileName, sFilePath, sStagingDataSource, m_iClientId, CommonFunctions.StorageType.Permanent);
            File.WriteAllBytes(sFilePath +"\\"+ sFileName, objReturnValue.ToArray());
        }
        //kkaur25 end UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder


        //smahajan6 - 06/28/2010 - MITS# 21261 : Start
        //-----sgupta320:Jira-10616 add new parameter BatchID,OptionSetID,AdminTrackTable,Update_falg for more Than 2GB data
        public void PeekDAStagingDatabase(string sDAStagingDBImportFile, string sAccessStagingDataSource, Dictionary<string, string> Settings,string i_Module,int BatchID=0,int OptionSetID=0,string AdminTrackTable="",bool Update_falg=false)
        {
            string sSourcePath = string.Empty;
            string sSourceFile = string.Empty;
            string sSrcConnectionString = string.Empty;
            string sSQL = string.Empty;
            string sExceptionMessage = string.Empty;
            string DELIMITER = ",";
            string sDesConnectionString = string.Empty;
            string[,] sTableInfo = null;
            string sFileName = string.Empty;
            
            //npradeepshar - MITS 23121 -- Start
            string strProviderName = string.Empty;
            int flag = 0;//---sgupta320 for 2GB Enhancement
            try
            {
                //dsharma70 Mits 35315 and 35413 start

                //----sgupta320 for dis 2GB enhancement              
                foreach (KeyValuePair<string, string> kvp2GB in Settings)
                {
                    if (kvp2GB.Key.Contains("chk2GBData") && kvp2GB.Value == "1")
                    {
                        flag = 1;
                        break;
                    }
                   
                }

                if (flag == 0)
                {
                    if (GetComponentPath(strProviderName_2013))
                    {
                        strProviderName = strProviderName_2013;
                    }
                    else if (GetComponentPath(strProviderName_2010))
                    {
                        strProviderName = strProviderName_2010;
                    }
                    else if (GetComponentPath(strProviderName_2007))    //dsharma70 Mits 35315 and 35413 end
                    {
                        strProviderName = strProviderName_2007;
                    }//npradeepshar MITS 24110 03/29/2011 Fix failed as we add optionset id in the last. replaced it with contains
                    else if (GetComponentPath(strProviderName_2003) && sDAStagingDBImportFile.Contains(".mdb."))
                    {
                        strProviderName = strProviderName_2003;
                    }
                    else // provider not present.
                    {
                        throw new DllNotFoundException();

                    }
                    //dsharma70 Mits 35315 and 35413 Start OLEDB Connection replaed by ODBC Connection
                    //sSrcConnectionString = string.Format(@"Provider=" + strProviderName + ";Data Source = {0}", sDAStagingDBImportFile);
                    sSrcConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb, *.accdb)}};Dbq={0};Uid=;Pwd=;", sDAStagingDBImportFile);
                }

                    //sSrcConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;", sDAStagingDBImportFile);

                    //dsharma70 Mits 35315 and 35413 End
                    //npradeepshar - MITS 23121 -- End
                    sDesConnectionString = sAccessStagingDataSource; 

                //kkaur25-start JIRA-10608 ui mdb change
                string mod_name = string.Empty;
                mod_name = i_Module;
                string sIarea = string.Empty;
                string sImportArea = string.Empty;
                bool bSuppflag = false;
                string aTrack = string.Empty;
                if (String.Equals(i_Module, "DIS"))
                {
                    foreach (KeyValuePair<string, string> kvp in Settings)
                    {
                        if(kvp.Key.Contains("_Flag"))
                        {
                        if(kvp.Value =="1")
                          {
                            sImportArea = kvp.Key;
                            sIarea = kvp.Key.Replace("_Flag", "");
                          }
                   
                        }

                        if (sImportArea == "AdminTrack_Flag")
                        {
                            if (kvp.Key.Equals("AdminTrack_Area"))
                            {
                                aTrack = kvp.Value;
                            }
                        }
                        string sSupp = string.Empty;
                        sSupp = sIarea + "_Import_Supp";
                        if(kvp.Key.Contains(sSupp))
                        {
                            if (kvp.Value == "1")
                            {
                                bSuppflag = true;
                            }
                        }

                        }
                    }
                   
                    //kkaur25-end 10608JIRA ui mdb change

                 //-----sgupta320: jira-10616 add flag condition for more than 2 gb data
                //GetImportDBSchemaInfo(sSrcConnectionString, ref sTableInfo, sImportArea, sDesConnectionString, mod_name, bSuppflag, aTrack);   
                if (flag == 0)
                {
                    GetImportDBSchemaInfo(sSrcConnectionString, ref sTableInfo, sImportArea, sDesConnectionString, mod_name, bSuppflag, aTrack);

                }

                else
                {
                    if (Update_falg == false)
                    {
                        GetImportDBSchemaInfoFor2GBData(ref sTableInfo, sImportArea, sDesConnectionString, mod_name, bSuppflag, BatchID, OptionSetID, AdminTrackTable);
                    }
                    else
                    {
                        string chk2GBFlag=string.Empty;
                        Settings.TryGetValue("chk2GBData", out chk2GBFlag);

                        if (bSuppflag == true && chk2GBFlag=="1")
                        {
                            DbReader objReader = null;
                            using (OdbcConnection objOdbcConnection = new OdbcConnection(sDesConnectionString))
                            {
                                objOdbcConnection.Open();
                                DataSet dsPSOData = new DataSet();
                                string sTablenames = string.Empty;
                                string tablename = string.Empty;

                                sTablenames = "Select TABLES_NAME from IMPORT_AREAS_STRUCTURE WHERE IMPORT_AREA='" + sImportArea + "' and Tables_Name like'%_SUPP'";

                                objReader = DbFactory.GetDbReader(sDesConnectionString, sTablenames.ToString());

                                while (objReader.Read())
                                {
                                    UpdateOptionSetIdFor_MoreThan2GB(objReader.GetString("TABLES_NAME"), OptionSetID, BatchID, sDesConnectionString);
                                }

                                objOdbcConnection.Close();

                            }

                        }

                    }
                }

                    if (flag == 0)
                    {
                        for (int iTableCounter = 0; iTableCounter <= sTableInfo.GetUpperBound(0); iTableCounter++)
                        {
                            //dsharma70 Mits 35315 and 35413 Start
                            string sTableName = string.Empty;
                            if (sTableInfo[iTableCounter, 0] != null && sTableInfo[iTableCounter, 0].ToString() != "")
                            {
                                sTableName = sTableInfo[iTableCounter, 0].ToString();

                                if (!sTableName.StartsWith("MSys"))      //dsharma70 Mits 35315 and 35413 End
                                {
                                    sSQL = string.Format(@"SELECT {0} FROM {1} WHERE 1 = 0", sTableInfo[iTableCounter, 1], sTableInfo[iTableCounter, 0]);

                                    try
                                    {
                                        DbFactory.ExecuteNonQuery(sDesConnectionString, sSQL);
                                    }
                                    catch (Exception ex)
                                    {
                                        Utilities.AddDelimited(ref sExceptionMessage, sTableInfo[iTableCounter, 0], DELIMITER, m_iClientId);
                                        //kkaur25-start JIRA-10608 ui mdb change
                                        sExceptionMessage = sExceptionMessage + " " + " Reason: " + ex.Message;
                                        sExceptionMessage = sExceptionMessage.Replace(".\r\nInvalid column name", ",");
                                        //kkaur25-end JIRA-10608 ui mdb change

                                    }
                                }
                            }
                        }

                        if (sExceptionMessage.Length > 1)
                        {
                            sFileName = sDAStagingDBImportFile;
                            sFileName = sFileName.Substring(sFileName.LastIndexOf("\\") + 1, sFileName.Length - sFileName.LastIndexOf("\\") - 1);
                            //sFileName = sFileName.Substring(0, sFileName.LastIndexOf("_")) + sFileName.Substring(sFileName.LastIndexOf("."), sFileName.Length - sFileName.LastIndexOf("."));
                            sFileName = sFileName.Substring(0, sFileName.IndexOf("."));  // ipuri Mits: 29687 

                            sExceptionMessage = string.Format(@"Import Database : {0}. Error occured while importing table(s) : {1}", sFileName, sExceptionMessage);
                            throw new RMAppException(sExceptionMessage);
                        }

                }
            }
            catch (DllNotFoundException ex) //npradeepshar - MITS 23121
            {//npradeepshar MITS 24110 03/29/2011 Fix failed as we add optionset id in the last. replaced it with contains
                if (GetComponentPath(strProviderName_2003) && sDAStagingDBImportFile.Contains(".accdb."))
                    throw new RMAppException("Unrecognized Database Format. Only Files with Extensions .mdb; accepted");
                else
                    throw new RMAppException("No Provider found.");
            }
            catch (Exception ex)
            {
                throw new RMAppException(ex.Message);
            }
        }
        //smahajan6 - 06/28/2010 - MITS# 21261 : End
        //smahajan6 - 06/22/2010 - MITS# 21191 : Start
        public void UpdateDAStagingDatabase(bool bUpdateFlag, int iOptionSetID, string sDAStagingDBImportFile, string sAccessStagingDataSource, Dictionary<string, string> Settings, string i_Module)
        {
            string sSQL = string.Empty;
            string sSrcConnectionString = string.Empty;
            string sDesConnectionString = string.Empty;
            List<string> TableNames = new List<string>();
            string[,] sTableInfo = null;
            int iColumnCount = 0;
            string sColumnValues = string.Empty;
            string sExceptionMessage = string.Empty;
            bool bConvSuccess = false;
            string DELIMITER = ",";
            //npradeepshar - MITS 23121 - Start
            string strProviderName = string.Empty;
            try
            {
                //dsharma70 Mits 35315 and 35413 start
                if (GetComponentPath(strProviderName_2013))
                {
                    strProviderName = strProviderName_2013;
                }
                else if (GetComponentPath(strProviderName_2010))
                {
                    strProviderName = strProviderName_2010;
                }
                else if (GetComponentPath(strProviderName_2007))    //dsharma70 Mits 35315 and 35413 end
                {
                    strProviderName = strProviderName_2007;
                }
                else if (GetComponentPath(strProviderName_2003) && sDAStagingDBImportFile.Contains(".mdb."))
                {//npradeepshar MITS 24110 03/29/2011 Fix failed as we add optionset id in the last. replaced it with contains
                    strProviderName = strProviderName_2003;
                }
                else // provider not present.
                {
                    throw new DllNotFoundException();

                }
                //dsharma70 Mits 35315 and 35414 Start OLEDB Connection replaed by ODBC Connection
                //sSrcConnectionString = string.Format(@"Provider=" + strProviderName + ";Data Source = {0}", sDAStagingDBImportFile);
                sSrcConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb, *.accdb)}};Dbq={0};Uid=;Pwd=;", sDAStagingDBImportFile);
                //dsharma70 Mits 35315 and 35413 End
                //npradeepshar - MITS 23121 END
                sDesConnectionString = sAccessStagingDataSource;

                #region Delete Existing Database Entries

                if (bUpdateFlag)
                {
                    DeleteDAStagingDatabaseEntries(iOptionSetID, sDesConnectionString);
                }

                #endregion

                #region Get Import DB Schema Information

                //kkaur25-start JIRA-10608 ui mdb change
                string sImportArea = string.Empty;
                string mod_name = string.Empty;
                mod_name = i_Module;
                string sIarea = string.Empty;
                bool bSuppflag = false;
                string aTrack = string.Empty;
                if (String.Equals(i_Module, "DIS"))
                {
                    foreach (KeyValuePair<string, string> kvp in Settings)
                    {
                        if (kvp.Key.Contains("_Flag"))
                        {
                            if (kvp.Value == "1")
                            {
                                sImportArea = kvp.Key;
                                sIarea = kvp.Key.Replace("_Flag", "");

                            }

                        }
                        if (sImportArea == "AdminTrack_Flag")
                        {
                            if (kvp.Key.Equals("AdminTrack_Area"))
                            {
                                aTrack = kvp.Value;
                            }
                        }
                        string sSupp = string.Empty;
                        sSupp = sIarea + "_Import_Supp";
                        if(kvp.Key.Contains(sSupp))
                        {
                            if (kvp.Value == "1")
                            {
                                bSuppflag = true;
                            }
                        }
                    }
                }
                //kkaur25-end JIRA-10608 ui mdb change

                GetImportDBSchemaInfo(sSrcConnectionString, ref sTableInfo, sImportArea, sDesConnectionString, mod_name,bSuppflag,aTrack);

                #endregion

                #region Import Database


                for (int iTableCounter = 0; iTableCounter <= sTableInfo.GetUpperBound(0); iTableCounter++)
                {
                    //dsharma70 Mits 35315 and 35413 Start
                    string sTableName = string.Empty;
                    if (sTableInfo[iTableCounter, 0] != null && sTableInfo[iTableCounter, 0].ToString() != "")
                    {
                        sTableName = sTableInfo[iTableCounter, 0].ToString();

                        if (!sTableName.StartsWith("MSys"))      //dsharma70 Mits 35315 and 35413 End
                        {
                            if (sTableInfo[iTableCounter, 1].Length > 1)
                            {
                                sSQL = string.Format(@"SELECT {0} FROM {1}", sTableInfo[iTableCounter, 1], sTableInfo[iTableCounter, 0]);
                                using (DbReader objDbreader = DbFactory.ExecuteReader(sSrcConnectionString, sSQL))
                                {
                                    
                                    while (objDbreader.Read())
                                    {
                                        sColumnValues = "";
                                        iColumnCount = Conversion.CastToType<int>(sTableInfo[iTableCounter, 2], out bConvSuccess);
                                        for (int iColumnCounter = 0; iColumnCounter < iColumnCount; iColumnCounter++)
                                        {
                                            Utilities.AddDelimited(ref sColumnValues, Utilities.FormatSqlFieldValue(objDbreader.GetValue(iColumnCounter).ToString()), DELIMITER, m_iClientId);
                                        }
                                        sSQL = string.Format(@"INSERT INTO {0} ( OPTIONSET_ID, {1} ) VALUES ( {2}, {3})",
                                            sTableInfo[iTableCounter, 0], sTableInfo[iTableCounter, 1], iOptionSetID, sColumnValues);
                                        try
                                        {
                                            DbFactory.ExecuteNonQuery(sDesConnectionString, sSQL);
                                        }
                                        catch (Exception ex)
                                        {
                                            Utilities.AddDelimited(ref sExceptionMessage, sTableInfo[iTableCounter, 0], DELIMITER, m_iClientId);
                                            sExceptionMessage = sExceptionMessage + " " + " Reason: " + ex.Message; //kkaur25-start JIRA-10608 ui mdb change
                                            break; //to break the while loop if the error occurs in Data Import Table
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                if (sExceptionMessage.Length > 1)
                {
                    sExceptionMessage = string.Format(@"OptionSet ID : {0}. Error occured while importing table(s) : {1}",
                                                            iOptionSetID, sExceptionMessage);
                    //throw new RMAppException("No Provider found."); mwalia2 12/14/2011 Error message changed from "No Provider Found" to generic one
                    throw new RMAppException(sExceptionMessage.ToString());
                }
            }
            catch (DllNotFoundException ex)//npradeepshar - MITS 23121
            {//npradeepshar MITS 24110 03/29/2011 Fix failed as we add optionset id in the last. reaplaced it with contains
                if (GetComponentPath(strProviderName_2003) && sDAStagingDBImportFile.Contains(".accdb."))
                    throw new RMAppException("Unrecognized Database Format. Only Files with Extensions .mdb; accepted");
                else
                    throw new RMAppException("No Provider found.");
            }
            catch (Exception ex)
            {
                throw new RMAppException(ex.Message);
            }
        }

        private void GetImportDBSchemaInfo(string sSrcConnectionString, ref string[,] sTableInfo, string sImparea, string sAccessstaging, string mod_name,bool sflag,string AdminTrack)
        {
            string sSQL = string.Empty;
            string sTableName = string.Empty;
            string sColumnNames = string.Empty;
            List<string> TableNames = new List<string>();
            DataTable objDataTable = null;
            DataTable objobjDataTableColumns = null;
            string[] sTableRestriction = { null, null, null, "TABLE" };
            string[] sColumnRestriction = { null, null, null, null };
            string DELIMITER = ",";

            //kkaur25-start JIRA-10608 ui mdb change
            if (mod_name == "DIS")
            {

                List<string> sATableName = new List<string>();

                DbReader objReader = null;
                using (OdbcConnection objOdbcConnection = new OdbcConnection(sAccessstaging))
                {

                    objOdbcConnection.Open();
                    DataSet dsPSOData = new DataSet();
                    string sTablenames = string.Empty;
                    int sTablescount = 0;
                    string tablename = string.Empty;
                    sTablenames = "Select TABLES_NAME from IMPORT_AREAS_STRUCTURE WHERE IMPORT_AREA='" + sImparea + "'";
                    try
                    {
                        objReader = DbFactory.GetDbReader(sAccessstaging, sTablenames.ToString());
                        while (objReader.Read())
                        {
                            sTablescount++;
                            sATableName.Add(objReader.GetString("TABLES_NAME"));
                            tablename = objReader.GetString("TABLES_NAME");
                            if (tablename.Contains("_SUPP") && sflag == false)
                            {
                                sATableName.Remove(tablename);
                            }

                            if (sImparea == "AdminTrack_Flag")
                            {
                                if (string.Compare(tablename.Trim() , AdminTrack,true) != 0)
                                {
                                    sATableName.Remove(tablename);
                                }
                            }

                        }

                    }
                    catch (Exception e)
                    {
                        throw new RMAppException("Cannot find IMPORT_AREAS_STRUCTURE table in Access Staging Database");
                    }

                    using (OdbcConnection objOdbcmdbConnection = new OdbcConnection(sSrcConnectionString))
                    {
                        objOdbcmdbConnection.Open();
                        objDataTable = objOdbcmdbConnection.GetSchema("Tables");
                        sTableInfo = new string[objDataTable.Rows.Count, 3];

                        for (int iTableCounter = 0; iTableCounter < objDataTable.Rows.Count; iTableCounter++)
                        {
                            sColumnNames = "";
                            sTableName = objDataTable.Rows[iTableCounter][2].ToString();
                            try
                            {
                                for (int tcount = 0; tcount < sATableName.Count; tcount++)
                                {
                                    string tablval = string.Empty;
                                    tablval = sATableName[tcount];
                                    if (sTableName == tablval)
                                    {

                                        sColumnRestriction[2] = sTableName;

                                        objobjDataTableColumns = objOdbcmdbConnection.GetSchema("columns", sColumnRestriction);
                                        for (int iColumnCounter = 0; iColumnCounter < objobjDataTableColumns.Rows.Count; iColumnCounter++)
                                        {
                                            Utilities.AddDelimited(ref sColumnNames, objobjDataTableColumns.Rows[iColumnCounter][3].ToString(), DELIMITER, m_iClientId);
                                        }
                                        sTableInfo[iTableCounter, 0] = sTableName;
                                        sTableInfo[iTableCounter, 1] = sColumnNames;
                                        sTableInfo[iTableCounter, 2] = objobjDataTableColumns.Rows.Count.ToString();
                                        
                                    }
                                }


                            }
                            catch (Exception e)
                            {
                                throw new RMAppException(sTableName);
                            }

                        }
                        objOdbcmdbConnection.Close();
                    }
                    objOdbcConnection.Close();



                }
            }
            //kkaur25-end JIRA-10608 ui mdb change

            else
            {

                //dsharma70 Mits 35315 and 35413 Start
                using (OdbcConnection objOdbcConnection = new OdbcConnection(sSrcConnectionString))
                {
                    objOdbcConnection.Open();
                    objDataTable = objOdbcConnection.GetSchema("Tables");
                    sTableInfo = new string[objDataTable.Rows.Count, 3];

                    for (int iTableCounter = 0; iTableCounter < objDataTable.Rows.Count; iTableCounter++)
                    {
                        sColumnNames = "";
                        sTableName = objDataTable.Rows[iTableCounter][2].ToString();
                        if (!sTableName.StartsWith("MSys"))
                        {
                            sColumnRestriction[2] = sTableName;

                            objobjDataTableColumns = objOdbcConnection.GetSchema("columns", sColumnRestriction);
                            for (int iColumnCounter = 0; iColumnCounter < objobjDataTableColumns.Rows.Count; iColumnCounter++)
                            {
                                Utilities.AddDelimited(ref sColumnNames, objobjDataTableColumns.Rows[iColumnCounter][3].ToString(), DELIMITER, m_iClientId);
                            }
                            sTableInfo[iTableCounter, 0] = sTableName;
                            sTableInfo[iTableCounter, 1] = sColumnNames;
                            sTableInfo[iTableCounter, 2] = objobjDataTableColumns.Rows.Count.ToString();
                        }
                    }
                    objOdbcConnection.Close();
                }
            } 
        }

        private void GetImportDBSchemaInfoFor2GBData(ref string[,] sTableInfo, string sImparea, string sAccessstaging, string mod_name, bool sflag, int BatchID, int OptionSetID, string AdminTracktable)
        {
            string sSQL = string.Empty;
            string sTableName = string.Empty;
            string sColumnNames = string.Empty;
            List<string> TableNames = new List<string>();
            //DataTable objDataTable = null;
            //DataTable objobjDataTableColumns = null;
            string[] sTableRestriction = { null, null, null, "TABLE" };
            string[] sColumnRestriction = { null, null, null, null };
            //string DELIMITER = ",";

            //kkaur25-start JIRA ui mdb change
            if (mod_name == "DIS")
            {
                Dictionary<string, string> sATableName = new Dictionary<string, string>();

                List<string> sSchemaTable = new List<string>();

                StringBuilder SB = new StringBuilder();

                StringBuilder SB_TblExist = new StringBuilder();

                DbReader objReader = null;
                using (OdbcConnection objOdbcConnection = new OdbcConnection(sAccessstaging))
                {

                    objOdbcConnection.Open();
                    DataSet dsPSOData = new DataSet();
                    string sTablenames = string.Empty;
                    int sTablescount = 0;
                    string tablename = string.Empty;
                    if (sImparea == "AdminTrack_Flag")
                    {
                        sTablenames = "Select TABLES_NAME,TABLE_STRUCTURE from IMPORT_AREAS_STRUCTURE WHERE IMPORT_AREA='" + sImparea + "' and Tables_name='" + AdminTracktable + "'";
                    }
                    else
                    {
                        sTablenames = "Select TABLES_NAME,TABLE_STRUCTURE from IMPORT_AREAS_STRUCTURE WHERE IMPORT_AREA='" + sImparea + "'";
 
                    }
                    try
                    {
                        objReader = DbFactory.GetDbReader(sAccessstaging, sTablenames.ToString());
                        while (objReader.Read())
                        {
                            sTablescount++;
                            sATableName.Add(objReader.GetString("TABLES_NAME"), objReader.GetString("TABLE_STRUCTURE"));
                            tablename = objReader.GetString("TABLES_NAME");
                            if (tablename.Contains("_SUPP") && sflag == false)
                            {
                                sATableName.Remove(tablename);
                            }

                        }                      

                        //-------sgupta320: check table is exist or not
                        DbReader objtblExistReader = null;
                        
                        int tblExistFlag = 0;
                        foreach (KeyValuePair<string, string> kvpChktblExist in sATableName)
                        {
                            string queryValue = string.Empty;

                            if (!DbFactory.IsOracleDatabase(sAccessstaging))
                            {
                                queryValue = "select count(*) as tblCount FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + kvpChktblExist.Key + "'";
                            }
                            else
                            {
                                queryValue = "select count(*) as tblCount FROM user_tab_columns where table_name='" + kvpChktblExist.Key + "'";
 
                            }

                            objtblExistReader = DbFactory.GetDbReader(sAccessstaging, queryValue.ToString());
                            while (objtblExistReader.Read())
                            {

                                if (objtblExistReader.GetInt("tblCount") == 0)
                                {
                                    if (tblExistFlag == 0)
                                    {
                                        SB_TblExist.Append(kvpChktblExist.Key);
                                    }
                                    else
                                    {
                                        SB_TblExist.Append(" ," + kvpChktblExist.Key);

                                    }
                                    tblExistFlag++;
                                }
                            }

                        }

                        if (SB_TblExist.Length != 0)
                        {
                            throw new RMAppException(SB_TblExist + ": table/s does not exists in RMXAccessStaging database");
                        }
                        //-------------end--------------------------------------------


                        int flag = 0;
                        foreach (KeyValuePair<string, string> kvp2GBData in sATableName)
                        {
                            string strQuery = string.Empty;

                            if (!DbFactory.IsOracleDatabase(sAccessstaging))
                            {
                                //strQuery = "select replace(tbl1.[COLUMNS],'-1','') as SchemaColumn from (";
                                //strQuery = strQuery + "select (tbl.Column_Name+tbl.DATA_TYPE+Convert(varchar,tbl.Lenght)+tbl.NULLABLE) as COLUMNS from (";
                                //strQuery = strQuery + "SELECT *,'Lenght' = case when CHARACTER_MAXIMUM_LENGTH Is Null then -1 else CHARACTER_MAXIMUM_LENGTH END,";
                                //strQuery = strQuery + "'NULLABLE' = case when Is_NULLABLE = 'YES' then 'NULL' else 'NOT NULL' end";
                                //strQuery = strQuery + " FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + kvp2GBData.Key + "') as tbl)as tbl1 ";

                                strQuery = "select replace(tbl2.[SchemaColumn],'-2111','max') as SchemaColumn from (select replace(tbl1.[COLUMNS],'-1','') as SchemaColumn from (";
                                strQuery = strQuery + "select (tbl.Column_Name+tbl.DATA_TYPE+Convert(varchar,tbl.Lenght)+tbl.NULLABLE) as COLUMNS from (";
                                strQuery = strQuery + "SELECT *,'Lenght' = case when CHARACTER_MAXIMUM_LENGTH Is Null then -1 when CHARACTER_MAXIMUM_LENGTH =-1 then -2111 else CHARACTER_MAXIMUM_LENGTH END,";
                                strQuery = strQuery + "'NULLABLE' = case when Is_NULLABLE = 'YES' then 'NULL' else 'NOTNULL' end";
                                strQuery = strQuery + " FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + kvp2GBData.Key + "') as tbl)as tbl1) as tbl2 ";


                            }
                            else
                            {
                                //strQuery = "select COLUMN_NAME || data_type|| data_length||decode(nullable,'Y','NULL','N','NOT NULL') as SchemaColumn";
                                
                                //strQuery = "select COLUMN_NAME || data_type|| case when data_type='NUMBER' then data_precision else data_length end||decode(nullable,'Y','NULL','N','NOT NULL') as SchemaColumn";
                                //strQuery = strQuery + " from user_tab_columns where table_name='" + kvp2GBData.Key + "'";

                                strQuery = "select COLUMN_NAME || data_type|| decode(data_type,'FLOAT', NULL,'NUMBER', data_precision || case when data_scale>0 then data_scale else null end,CASE WHEN  data_type='CLOB' AND data_length=4000 THEN NULL ELSE DATA_LENGTH END)||decode(nullable,'Y','NULL','N','NOTNULL') as SchemaColumn";
                                strQuery = strQuery + " from user_tab_columns where table_name='" + kvp2GBData.Key + "'";
 
                            }
                            string query = kvp2GBData.Value;
                            //string query = kvp2GBData.Value.ToUpper();
                          //  if (query.ToUpper().Contains("NULL"))
                           // {
                           //     query = query.ToUpper();
                           // }

                            if (!DbFactory.IsOracleDatabase(sAccessstaging))
                            {
                                
                                query = query.Replace("CREATE TABLE [dbo].[" + (kvp2GBData.Key.TrimEnd()).Trim() + "](", "");
                            }
                            else
                            {
                                query = query.Replace("\n", "");
                                query = query.Replace("CREATE TABLE " + (kvp2GBData.Key.TrimEnd()).Trim() + "(", "");
 
                            }

                            query = query.Replace("\r\n\t", "");

                            query = query.Replace("\r\n", "");

                            query = query.Replace("\t", "");

                            query = query.Replace("\n", "");
                           

                            string[] Column;
                            if (!DbFactory.IsOracleDatabase(sAccessstaging))
                            {
                                Column = query.Split(',');
                            }
                            else
                            {
                                
                                query = Regex.Replace(query, "Null,", "NULL;", RegexOptions.IgnoreCase); //mkaur24 JIRA12427
                               query = Regex.Replace(query,"[[:space:]]*","" , RegexOptions.IgnoreCase);        //mkaur24 JIRA12427                                                   
                                query.Replace(" ", "");
                                Column = query.Split(';');
                            }
                            DbReader objReaderColumn = null;

                            objReaderColumn = DbFactory.GetDbReader(sAccessstaging, strQuery.ToString());
                          
                            while (objReaderColumn.Read())
                            {

                                sSchemaTable.Add(objReaderColumn.GetString("SchemaColumn").ToUpper());
                            }

                            //------sgupta320: loop for checking schema is column name and schema is same
                            foreach (string ColumnName in Column)
                            {
                                string ReplaceVal = ColumnName.Replace("[", "");
                                ReplaceVal = ReplaceVal.Replace("]", "");
                                ReplaceVal = ReplaceVal.Replace("(", "");
                                ReplaceVal = ReplaceVal.Replace(")", "");
                                ReplaceVal = ReplaceVal.Replace(" ", "");
                                bool exists;
                                if (!DbFactory.IsOracleDatabase(sAccessstaging))
                                {
                                    exists = sSchemaTable.Contains(((ReplaceVal.TrimStart()).TrimEnd()).Trim().ToUpper());
                                }
                                else
                                {
                                    ReplaceVal = ReplaceVal.Replace(",", "");
                                    exists = sSchemaTable.Contains(((ReplaceVal.TrimStart()).TrimEnd()).ToUpper());
                                }
                                
                                
                                if (exists == false)
                                {
                                    string Value = string.Empty;
                                    if (!DbFactory.IsOracleDatabase(sAccessstaging))
                                    {
                                        Value = ColumnName.Substring(0, ColumnName.IndexOf("]"));
                                        Value = ((Value.Replace("]", "")).TrimEnd()).Trim();
                                        Value = ((Value.Replace("[", "")).TrimEnd()).Trim();
                                    }
                                    else
                                    {
                                        Value = ColumnName.TrimStart();
                                        Value = Value.Replace("  ", " ");
                                        Value = Value.Substring(0,Value.IndexOf(" "));
 
                                    }
                                    
                                    if (flag == 0)
                                    {
                                        SB.Append(kvp2GBData.Key + "." + Value);
                                    }
                                    else
                                    {
                                        SB.Append("," + kvp2GBData.Key + "." + Value);
 
                                    }
                                    flag++;
                                }
                            }

                            sSchemaTable.Clear();
 
                        }
                        flag = 0;
                        if (SB.Length != 0)
                        {
                            throw new RMAppException(SB + "-column/s either does not exists or not having the proper datatype in RMXAccessStaging database. Please refer mapping document!");
                        }
                        //---------end loop----------------------------------------------

                        //-------sgupta320: check batch_id is exist or not
                        DbReader objBatchIDReader = null;
                        string CHK_BatchID_Ext = string.Empty;
                        CHK_BatchID_Ext = GetImportTableName(sImparea);

                        foreach (KeyValuePair<string, string> kvpChkBatchID in sATableName)
                        {
                            if (sImparea != "AdminTrack_Flag")
                            {
                                if (kvpChkBatchID.Key == CHK_BatchID_Ext)
                                {
                                    string queryValue = string.Empty;

                                    queryValue = "select count(*) as COUNT from " + kvpChkBatchID.Key + " where batch_id=" + BatchID;

                                    objBatchIDReader = DbFactory.GetDbReader(sAccessstaging, queryValue.ToString());
                                    while (objBatchIDReader.Read())
                                    {

                                        if (objBatchIDReader.GetInt("COUNT") == 0)
                                        {

                                            throw new RMAppException("Batch ID: " + BatchID + " does not exist into " + kvpChkBatchID.Key + " Table of RMXAccessStaging database. Please populate the data first!");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string queryValue = string.Empty;

                                queryValue = "select count(*) as COUNT from " + kvpChkBatchID.Key + " where batch_id=" + BatchID;

                                objBatchIDReader = DbFactory.GetDbReader(sAccessstaging, queryValue.ToString());
                                while (objBatchIDReader.Read())
                                {

                                    if (objBatchIDReader.GetInt("COUNT") == 0)
                                    {

                                        throw new RMAppException("Batch ID: " + BatchID + " does not exist into " + kvpChkBatchID.Key + " Table of RMXAccessStaging database. Please populate the data first!");
                                    }
                                }
 
                            }
                            

                        }
                        //-------------end--------------------------------------------


                        //-------sgupta320: check If OPTIONSET_ID is already exist for given batch_id 
                        DbReader objCountReader = null;

                        string Import_Table_name = string.Empty;
                        Import_Table_name = GetImportTableName(sImparea);

                        foreach (KeyValuePair<string, string> kvpChkBatchID in sATableName)
                        {
                            if (sImparea != "AdminTrack_Flag")
                            {

                                if (kvpChkBatchID.Key == Import_Table_name)
                                {
                                    string queryValue = string.Empty;

                                    queryValue = "select count(*) as COUNT from " + kvpChkBatchID.Key + " where batch_id=" + BatchID + " and OPTIONSET_ID is not null";

                                    objCountReader = DbFactory.GetDbReader(sAccessstaging, queryValue.ToString());
                                    while (objCountReader.Read())
                                    {

                                        if (objCountReader.GetInt("COUNT") != 0)
                                        {

                                            throw new RMAppException("Batch_ID: " + BatchID + " is already associated with another OPTIONSET_ID into " + kvpChkBatchID.Key + " of RMXAccessStaging database. Please enter different BatchID");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string queryValue = string.Empty;

                                queryValue = "select count(*) as COUNT from " + kvpChkBatchID.Key + " where batch_id=" + BatchID + " and OPTIONSET_ID is not null";

                                objCountReader = DbFactory.GetDbReader(sAccessstaging, queryValue.ToString());
                                while (objCountReader.Read())
                                {

                                    if (objCountReader.GetInt("COUNT") != 0)
                                    {

                                        throw new RMAppException("Batch_ID: " + BatchID + " is already associated with another OPTIONSET_ID into " + kvpChkBatchID.Key + " of RMXAccessStaging database. Please enter different BatchID");
                                    }
                                }
 
                            }
 
                        }
                        //-------------end--------------------------------------------

                        //----------sgupta320: update option_Set into database

                        foreach (KeyValuePair<string, string> kvpOptionSetUpdate in sATableName)
                        {

                            //string UpdateQuery = string.Empty;

                            //UpdateQuery = "Update " + kvpOptionSetUpdate.Key + " set OPTIONSET_ID = " + OptionSetID + " where Batch_id = " + BatchID;

                            //DbFactory.ExecuteNonQuery(sAccessstaging, UpdateQuery.ToString());

                            UpdateOptionSetIdFor_MoreThan2GB(kvpOptionSetUpdate.Key, OptionSetID, BatchID, sAccessstaging);
                

                        }


                    }
                    catch (Exception e)
                    {
                        throw new RMAppException(e.Message);
                        //throw new RMAppException("Cannot find IMPORT_AREAS_STRUCTURE table in Access Staging Database");


                    }

                    objOdbcConnection.Close();



                }
            }
            
        }

        //-----sgupta320:jira-10616 add new function for more than 2GB
        private void UpdateOptionSetIdFor_MoreThan2GB(string TableName, int OptionSetID, int BatchID, string sAccessstaging)
        {
            string UpdateQuery = string.Empty;

            UpdateQuery = "Update " + TableName + " set OPTIONSET_ID = " + OptionSetID + " where Batch_id = " + BatchID;

            DbFactory.ExecuteNonQuery(sAccessstaging, UpdateQuery.ToString());
 
        }
              
        //    using (OleDbConnection objOleDbConnection = new OleDbConnection(sSrcConnectionString))
        //    {
        //        objOleDbConnection.Open();
        //        objDataTable = objOleDbConnection.GetSchema("tables", sTableRestriction);
        //        sTableInfo = new string[objDataTable.Rows.Count, 3];
        //        for (int iTableCounter = 0; iTableCounter < objDataTable.Rows.Count; iTableCounter++)
        //        {
        //            sColumnNames = "";
        //            sTableName = objDataTable.Rows[iTableCounter][2].ToString();
        //            sColumnRestriction[2] = sTableName;
        //            objobjDataTableColumns = objOleDbConnection.GetSchema("columns", sColumnRestriction);
        //            for (int iColumnCounter = 0; iColumnCounter < objobjDataTableColumns.Rows.Count; iColumnCounter++)
        //            {
        //                Utilities.AddDelimited(ref sColumnNames, objobjDataTableColumns.Rows[iColumnCounter][3].ToString(), DELIMITER);
        //            }
        //            sTableInfo[iTableCounter, 0] = sTableName;
        //            sTableInfo[iTableCounter, 1] = sColumnNames;
        //            sTableInfo[iTableCounter, 2] = objobjDataTableColumns.Rows.Count.ToString();
        //        }
        //        objOleDbConnection.Close();
        //    }
        //}
        //dsharma70 Mits 35315 and 35413 End

        private string GetImportTableName(string Import_Area)
        {
            string Import_Table_name = string.Empty;
            if (Import_Area == "Employees_Flag")
            {
                Import_Table_name = "EMPLOYEE";
 
            }
            else if (Import_Area == "Entities_Flag")
            {
                Import_Table_name = "ENTITY";
 
            }

            else if (Import_Area == "OrgHier_Flag")
            {
                Import_Table_name = "ENTITY";

            }

            else if (Import_Area == "Funds_Flag")
            {
                Import_Table_name = "FUNDS";

            }

            else if (Import_Area == "Vehicles_Flag")
            {
                Import_Table_name = "VEHICLE";

            }

            else if (Import_Area == "Policies_Flag")
            {
                Import_Table_name = "POLICY";

            }

            else if (Import_Area == "Patients_Flag")
            {
                Import_Table_name = "PATIENT";

            }

            else if (Import_Area == "Physicians_Flag")
            {
                Import_Table_name = "PHYSICIAN";

            }


            else if (Import_Area == "MedicalStaff_Flag")
            {
                Import_Table_name = "MED_STAFF";

            }

            else if (Import_Area == "FundsDeposit_Flag")
            {
                Import_Table_name = "FUNDS_DEPOSIT";

            }

            else if (Import_Area == "Reserves_Flag")
            {
                Import_Table_name = "RESERVES";

            }

            else if (Import_Area == "OrgExpo_Flag")
            {
                Import_Table_name = "ENTITY_EXPOSURE";

            }
            return Import_Table_name;
        }

        private void DeleteDAStagingDatabaseEntries(int iOptionSetID, string sDesConnectionString)
        {
            string sSQL = string.Empty;
            List<string> TableNames = new List<string>();
            //mwalia2 change done for oracle DB MITS 24444 Date 04/19/2012
            if (DbFactory.IsOracleDatabase(sDesConnectionString))
            {
                sSQL = "SELECT TNAME NAME FROM TAB";
            }
            else
            {
                sSQL = "SELECT NAME FROM SYS.TABLES";
            }

            using (DbReader objDbreader = DbFactory.ExecuteReader(sDesConnectionString, sSQL))
            {
                while (objDbreader.Read())
                {
                    sSQL = string.Format(@"DELETE FROM {0} WHERE OPTIONSET_ID = {1}", objDbreader.GetString("NAME"), iOptionSetID);
                    try
                    {
                        DbFactory.ExecuteNonQuery(sDesConnectionString, sSQL);
                    }
                    catch (Exception ex) { }
                }
            }
        }
        //smahajan6 - 06/22/2010 - MITS# 21191 : End

        /// <summary>
        /// npradeepshar - MITS 23121
        /// </summary>
        /// <param name="strComponent">path of the local registry file</param>
        /// <returns></returns>
        private bool GetComponentPath(string strComponent)
        {

            bool blnToReturn = false;
            //looks inside CURRENT_USER:
            RegistryKey _mainKey = Registry.ClassesRoot;
            try
            {
                _mainKey = _mainKey.OpenSubKey(strComponent, false);
                if (_mainKey != null)
                {
                    blnToReturn = true;
                }
            }
            catch
            { }
            //closing the handle:
            if (_mainKey != null)
                _mainKey.Close();
            return blnToReturn;
        }


        /// <summary>
        /// Vsoni5 - MITS 23347, 23357, 23358,23364, 23365, 23439
        /// </summary>
        /// <param name="iOptionsetId">Optionset Id to clear</param>
        /// <param name="RMConnectionString">ConnectionString to RMX DB</param>
        /// <returns></returns>
        /// 
        public void CleanUpOptionset(int iOptionsetId, string RMConnectionString)
        {
            DbConnection objConn = null;
            DbCommand objCommand = null;
            string sql = string.Empty;

            sql = "DELETE FROM DATA_INTEGRATOR WHERE OPTIONSET_ID = " + iOptionsetId;

            objConn = DbFactory.GetDbConnection(RMConnectionString);
            objConn.Open();
            objCommand = objConn.CreateCommand();
            objCommand.CommandText = sql;
            objCommand.ExecuteNonQuery();
            if (objConn != null)
                objConn.Dispose();
        }

        /// <summary>
        /// Vsoni5 - MITS 23347, 23357, 23358,23364, 23365, 23439
        /// </summary>
        /// <param name="iOptionsetId">Optionset Id to clear</param>
        /// <param name="sTaskManagerDataSource">ConnectionString to TaskManager DB</param>
        /// <returns></returns>
        /// 
        public void CleanUpTaskMgr(int iOptionsetId, string sTaskManagerDataSource)
        {
            DbConnection objConn = null;
            DbCommand objCommand = null;
            string sql = string.Empty;

            sql = "DELETE FROM TM_SCHEDULE WHERE OPTIONSET_ID = " + iOptionsetId;

            objConn = DbFactory.GetDbConnection(sTaskManagerDataSource);
            objConn.Open();
            objCommand = objConn.CreateCommand();
            objCommand.CommandText = sql;
            objCommand.ExecuteNonQuery();
            if (objConn != null)
                objConn.Dispose();
        }


        /// <summary>
        /// Vsoni5 - MITS 23347, 23357, 23358,23364, 23365, 23439
        /// </summary>
        /// <param name="iOptionsetId">Optionset Id to clear</param>
        /// <param name="RMConnectionString">ConnectionString to RMX Staging DB</param>
        /// <returns></returns>
        /// 
        public void CleanUpReserveMappings(int iOptionsetId, string RMConnectionString)
        {
            DbConnection objConn = null;
            DbCommand objCommand = null;
            string sql = string.Empty;

            sql = "DELETE FROM USER_DEF_RESERVE_MAP WHERE OPTIONSET_ID = " + iOptionsetId;

            objConn = DbFactory.GetDbConnection(RMConnectionString);
            objConn.Open();
            objCommand = objConn.CreateCommand();
            objCommand.CommandText = sql;
            objCommand.ExecuteNonQuery();
            if (objConn != null)
                objConn.Dispose();
        }


        /// <summary>
        /// Vsoni5 - MITS 23347, 23357, 23358,23364, 23365, 23439
        /// </summary>
        /// <param name="iOptionsetId">Optionset Id to clear</param>
        /// <param name="sAccessStagingDataSource">ConnectionString to Access Staging DB</param>
        /// <returns></returns>
        /// 
        public void CleanUpMSAccessStaging(int iOptionsetId, string sAccessStagingDataSource, Dictionary<string,string> Settings)
        { //mkaur24 JIRA12427 START
                 int flag = 0 ;
                 foreach (KeyValuePair<string, string> kvp2GB in Settings)  
                     {
                         if (kvp2GB.Key.Contains("chk2GBData") && kvp2GB.Value == "1")
                      {
                                  flag = 1;
                                 break;
                        }                 
                     }

                    if (flag != 1 ) //mkaur24 JIRA12427 END
                {
                 DeleteDAStagingDatabaseEntries(iOptionsetId, sAccessStagingDataSource);

                }
         }
        
        /// <summary>
        /// Gets DDS Reserve Mappings MITS 21553
        /// npradeepshar 03/24/2011 Merging changes fROM R6PS1
        /// </summary>
        /// <param name="p_iOptionSetID"></param>
        /// <returns></returns>
        public DataSet GetDDSReserveMappings(int p_iOptionSetID)
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_DAConnectionString);
            objConn.Open();
            string sSQL = "";

            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT BUCKET,RSV_TYPE_CODE, (RSV_TYPE_SHORT_CODE || ' ' || CODE_DESC) SHORT_CODE,LOB_CODE FROM USER_DEF_RESERVE_MAP WHERE"
                     + " OPTIONSET_ID = " + p_iOptionSetID + " ORDER BY BUCKET";
            }
            else
            {
                sSQL = "SELECT BUCKET,RSV_TYPE_CODE, (RSV_TYPE_SHORT_CODE + ' ' + CODE_DESC) SHORT_CODE,LOB_CODE FROM USER_DEF_RESERVE_MAP WHERE"
                        + " OPTIONSET_ID = " + p_iOptionSetID + " ORDER BY BUCKET";
            }

            DataSet objResult = DbFactory.GetDataSet(m_DAConnectionString, sSQL, m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return objResult;
        }

        /// <summary>
        /// Vsoni5 : MITS 25785.
        /// </summary>
        /// <param name="connectionString"> ConnectionString to the RMX Database</param>
        /// <param name="sEntityList">Comma seperated list of Payer Entity Ids selected on UI</param>
        /// <returns>This function return dataset containing TaxID, Abbr. and Last Name of payer</returns>
        public DataSet GetTaxIds(string connectionString, string sEntityList)
        {
            string sSQL = "SELECT TAX_ID, ABBREVIATION, LAST_NAME FROM ENTITY WHERE ENTITY_ID IN (" + sEntityList + ")";
            DataSet objResult = DbFactory.GetDataSet(connectionString, sSQL, m_iClientId);
            return objResult;
        }

        /// <summary>
        /// Developer :- Subhendu | Module - DA CLAIM EXPORT CSStars | Function - Save/Update Claim Type Code into RMX DB(DA_CLAIM_EXPORT_CLAIM_TYPE).
        /// </summary>
        /// <param name="sCodeID"></param>
        /// <param name="iLOB"></param>
        /// <param name="bChkreEdit"></param>
        /// <param name="iOptionset"></param>
        public void SaveClaimTypeCode(string sCodeID, int iLOB, bool bChkreEdit, int iOptionset)
        {
            int iOptionsetID;

            if (bChkreEdit)
                iOptionsetID = iOptionset;
            else
                iOptionsetID = GetMaxOptionSetId() + 1;
            string sSql = string.Empty;
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                //On top delete statement to make sure same optionset does not exists.
                sSql = "DELETE FROM DA_CLAIM_EXPORT_CLAIM_TYPE WHERE OPTIONSET_ID = " + iOptionsetID;
                objConn.ExecuteNonQuery(sSql);
                sSql = string.Empty;
                sSql = "INSERT INTO DA_CLAIM_EXPORT_CLAIM_TYPE VALUES (" + iOptionsetID + "," + iLOB + ", '" + sCodeID + "','" + 0 + "')";
                objConn.ExecuteNonQuery(sSql);
            }
            catch (Exception e)
            {
                throw new RMAppException("Missing script or Primary key violation error related to table:DA_CLAIM_EXPORT_CLAIM_TYPE");
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }
        /// <summary>
        /// Developer :- Subhendu | Module - DA CLAIM EXPORT CSStars |Function - Save/Update Reserve Type into RMX DB(DA_CLAIM_EXPORT_RESERVE_TYPE).
        /// </summary>
        /// <param name="sReserveType"></param>
        /// <param name="iLOB"></param>
        /// <param name="bChkreEdit"></param>
        /// <param name="iOptionset"></param>
        public void SaveReserveType(string sReserveType, int iLOB, bool bChkreEdit, int iOptionset)
        {
            int iOptionsetID;

            if (bChkreEdit)
                iOptionsetID = iOptionset;
            else
                iOptionsetID = GetMaxOptionSetId() + 1;

            string sSql = string.Empty;
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                //On top delete statement to make sure same optionset does not exists.
                sSql = "DELETE FROM DA_CLAIM_EXPORT_RESERVE_TYPE WHERE OPTIONSET_ID = " + iOptionsetID;
                objConn.ExecuteNonQuery(sSql);
                sSql = string.Empty;
                sSql = "INSERT INTO DA_CLAIM_EXPORT_RESERVE_TYPE"
                    + " VALUES (" + iOptionsetID + "," + iLOB + ", '"
                            + sReserveType + "','" + 0 +
                            "')";
                objConn.ExecuteNonQuery(sSql);
            }
            catch (Exception e)
            {
                throw new RMAppException("Missing script or Primary key violation error related to table: DA_CLAIM_EXPORT_RESERVE_TYPE");

            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }
        /// <summary>
        /// Developer :- Subhendu | Module - DA CLAIM EXPORT CSStars | Function - Retrieve Claim type code from RMX DB(DA_CLAIM_EXPORT_CLAIM_TYPE).
        /// </summary>
        /// <param name="iLOB"></param>
        /// <param name="iOptionsetID"></param>
        /// <returns></returns>
        public string GetClaimTypeOnCriteriaEdit(int iLOB, int iOptionsetID)
        {
            string sSql = string.Empty;
            string sClaimtype = "0";
            DbConnection objconn = null;
            objconn = DbFactory.GetDbConnection(m_connectionString);
            objconn.Open();

            sSql = "SELECT SELECTED_CODE_IDs FROM DA_CLAIM_EXPORT_CLAIM_TYPE WHERE OPTIONSET_ID =" + iOptionsetID +
                    " AND LINE_OF_BUS_CODE = " + iLOB;

            if (objconn != null)
            {
                objconn.Close();
                objconn.Dispose();
            }

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSql);
            while (objReader.Read())
            {
                sClaimtype = objReader.GetString("SELECTED_CODE_IDs");
            }

            return sClaimtype;

        }

        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Retrieve Reserve type from RMX DB(DA_CLAIM_EXPORT_RESERVE_TYPE).
        /// </summary>
        /// <param name="iLOB"></param>
        /// <param name="iOptionsetID"></param>
        /// <returns></returns>
        public string GetReserveTypeOnCriteriaEdit(int iLOB, int iOptionsetID)
        {
            string sSql = string.Empty;
            string sReservetype = "0";
            DbConnection objconn = null;
            objconn = DbFactory.GetDbConnection(m_connectionString);
            objconn.Open();

            sSql = "SELECT SELECTED_CODE_IDs FROM DA_CLAIM_EXPORT_RESERVE_TYPE WHERE OPTIONSET_ID =" + iOptionsetID +
                    " AND LINE_OF_BUS_CODE = " + iLOB;

            if (objconn != null)
            {
                objconn.Close();
                objconn.Dispose();
            }

            DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSql);
            while (objReader.Read())
            {
                sReservetype = objReader.GetString("SELECTED_CODE_IDs");
            }
            return sReservetype;

        }


        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSSTars | Function - Always Clean these tables DA_CLAIM_EXPORT_ORG_HIERARCHY,DA_CLAIM_EXPORT_CLAIM_TYPE,DA_CLAIM_EXPORT_CLAIM_TYPE before saving optionset.
        /// </summary>
        /// <param name="bChkreEdit"></param>
        /// <param name="iOptionset"></param>
        public void CleanUpDAClaimExportTables(bool bChkreEdit, int iOptionset)
        {
            int iOptionsetID;
            if (bChkreEdit)
            {
                iOptionsetID = iOptionset;
            }
            else
            {
                iOptionsetID = GetMaxOptionSetId() + 1;
            }

            DbConnection objConn = null;
            DbCommand objCommand = null;
            string sql = string.Empty;
            string[] sArrTableName = new string[3] { "DA_CLAIM_EXPORT_CLAIM_TYPE", "DA_CLAIM_EXPORT_RESERVE_TYPE", "DA_CLAIM_EXPORT_DEPARTMENT" };//"DA_CLAIM_EXPORT_ORG_HIERARCHY"
            int i;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            objCommand = objConn.CreateCommand();
            for (i = 0; i < 3; i++)
            {
                sql = "DELETE FROM " + sArrTableName[i] + " WHERE OPTIONSET_ID = " + iOptionsetID;
                objCommand.CommandText = sql;
                objCommand.ExecuteNonQuery();
            }
            if (objConn != null)
                objConn.Dispose();
        }
        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Get Org Releted info to display on page load
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            LocalCache objCache = null;
            XmlNodeList objNodLst = null;
            XmlElement objElement = null;
            string sOrgID = string.Empty;
            XmlDocument XmlNew = new XmlDocument();
            int iChkOptionset = 0;
            try
            {
                if (p_objXmlDocument.GetElementsByTagName("OptionSet").Count > 0)
                {
                    iChkOptionset = Convert.ToInt32(p_objXmlDocument.SelectSingleNode("ClaimExportOrgList/listhead/OptionSet").FirstChild.Value);
                    p_objXmlDocument.SelectSingleNode("ClaimExportOrgList/listhead/OptionSet").ParentNode.RemoveChild(p_objXmlDocument.SelectSingleNode("ClaimExportOrgList/listhead/OptionSet"));
                }
                WhereClause = "OPTIONSET_ID = " + iChkOptionset.ToString();
                Get("OPTIONSET_ID,ENTITY_ID,FROM_DATE,TO_DATE,ACTIVE_ORG", "DA_CLAIM_EXPORT_ORG_HIERARCHY", WhereClause, "", iChkOptionset, p_objXmlDocument);
                objNodLst = p_objXmlDocument.SelectNodes("//listrow");
                objCache = new LocalCache(m_connectionString,m_iClientId);
                foreach (XmlElement objElm in objNodLst)
                {
                    foreach (XmlElement objChildElement in objElm.ChildNodes)
                    {
                        if (objChildElement.Name == "OrgId")
                        {
                            string sAbbr = "";
                            string sDesc = "";
                            sOrgID = objChildElement.InnerText;
                            objCache.GetOrgInfo(Convert.ToInt32(sOrgID), ref sAbbr, ref sDesc);
                            objChildElement.InnerText = sAbbr + " " + sDesc;
                        }
                        else if (objChildElement.Name == "RowId")
                        {
                            objElement = objChildElement;
                        }
                    }
                    objElement.InnerText = objElement.InnerText + "|" + sOrgID;
                }
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objNodLst = null;
            }
        }
        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Get Optionset criteria for edit optionset case
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument GetCEOrg(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm;
            try
            {
                XMLDoc = p_objXmlDocument;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org Hierarchy']");
                if (objElm != null)
                {
                    if (objElm.GetAttribute("orgid") != "")
                    {
                        base.WhereClause = " ORG_EID=" + objElm.GetAttribute("orgid");
                    }
                }

                int iOptionsetID = Convert.ToInt32(p_objXmlDocument.SelectSingleNode("//control[@name='OptionSet']").FirstChild.Value);
                int iEntityID = Convert.ToInt32(objElm.GetAttribute("orgid"));
                GetCEOrg("DA_CLAIM_EXPORT_ORG_HIERARCHY", iOptionsetID, iEntityID, p_objXmlDocument);
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org Hierarchy']");
                if (objElm != null)
                {
                    objElm.SetAttribute("currentid", objElm.GetAttribute("orgid"));
                    p_objXmlDocument.SelectSingleNode("//control[@name='Org_orglist']").InnerText = objElm.GetAttribute("orgid");
                }
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
            }

        }
        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Get Optionset criteria for edit optionset case
        /// </summary>
        /// <param name="sTableName"></param>
        /// <param name="iOptionsetID"></param>
        /// <param name="iEntityID"></param>
        /// <param name="objXmlDoc"></param>
        protected virtual void GetCEOrg(string sTableName, int iOptionsetID, int iEntityID, XmlDocument objXmlDoc)
        {
            string sSQL = "";
            DbConnection objConn = null;
            DbReader objDbReader = null;
            XmlNodeList objNodeList = null;
            string sValue = "";
            LocalCache objLocalCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sControlType = "";
            string[] arrFieldList1 = { "Org Hierarchy", "Claim From Date", "Claim To Date", "Active" };

            try
            {
                {
                    sSQL = String.Format("SELECT * FROM {0} WHERE OPTIONSET_ID = {1} AND ENTITY_ID = {2}", sTableName, iOptionsetID, iEntityID);
                }
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objDbReader = objConn.ExecuteReader(sSQL);
                objLocalCache = new LocalCache(m_connectionString,m_iClientId);

                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        foreach (string sName in arrFieldList1)
                        {
                            if (sName == "Org Hierarchy")
                                sValue = objDbReader["ENTITY_ID"].ToString();
                            else if (sName == "Claim From Date")
                                sValue = objDbReader["FROM_DATE"].ToString();
                            else if (sName == "Claim To Date")
                                sValue = objDbReader["To_DATE"].ToString();
                            else if (sName == "Active")
                                sValue = objDbReader["ACTIVE_ORG"].ToString();

                            objNodeList = objXmlDoc.GetElementsByTagName("control");
                            foreach (XmlElement objXmlElement in objNodeList)
                            {
                                if (objXmlElement.GetAttribute("name") == sName)
                                {
                                    sControlType = objXmlElement.GetAttribute("type").ToLower();
                                    switch (sControlType)
                                    {
                                        case "checkbox":
                                            if (sValue.Equals("-1") || sValue.Equals("1"))
                                                objXmlElement.InnerText = "True";
                                            else
                                                objXmlElement.InnerText = "";
                                            break;
                                        case "date":
                                            objXmlElement.InnerText = Conversion.GetDBDateFormat(sValue, "d");
                                            break;
                                        case "orglist":
                                            //Textbox--start
                                            objXmlElement.InnerXml = "";
                                            objXmlElement.SetAttribute("orgid", sValue);
                                            int iOrgId = Convert.ToInt32(sValue);
                                            string sAbbr = "";
                                            LocalCache objCache = new LocalCache(m_connectionString,m_iClientId);
                                            objCache.GetOrgInfo(iOrgId, ref sAbbr, ref sDesc);
                                            objXmlElement.InnerXml = sAbbr.Replace("&", "&amp;") + "-" + sDesc.Replace("&", "&amp;");
                                            break;
                                        //Textbox--end
                                        default:
                                            objXmlElement.InnerText = sValue;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    objDbReader.Close();
                }
                objConn.Dispose();
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
                objNodeList = null;
            }
        }

        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Get the saved optionset from DA_CLAIM_EXPORT_ORG_HIERARCHY table and create new node listrow for each record in the XML and pass it to parent page.
        /// </summary>
        /// <param name="sFields"></param>
        /// <param name="sTableName"></param>
        /// <param name="sWhereClause"></param>
        /// <param name="sOrderbyClause"></param>
        /// <param name="iOptionsetID"></param>
        /// <param name="p_objXmlDocument"></param>
        protected virtual void Get(string sFields, string sTableName, string sWhereClause, string sOrderbyClause, int iOptionsetID, XmlDocument p_objXmlDocument)
        {
            DataSet objDs = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            XmlElement objElm = null;
            //string sColumnName = "";
            string sSQL = "";

            LocalCache objCache = null;
            //string sShortCode = "";
            //string sShortDesc = "";

            try
            {
                objCache = new LocalCache(this.ConnectString,m_iClientId);

                sSQL = String.Format("SELECT {0} FROM {1}", sFields, sTableName);
                if (sWhereClause != null && sWhereClause != "")
                    sSQL += " WHERE " + sWhereClause;
                if (sOrderbyClause != null && sOrderbyClause != "")
                    sSQL += " ORDER BY " + sOrderbyClause;

                objDs = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);

                objNode = p_objXmlDocument.SelectSingleNode("//*[name()='" + "ClaimExportOrgList" + "']");
                objNodLst = objNode.ChildNodes.Item(0).ChildNodes;

                foreach (DataRow objRow in objDs.Tables[0].Rows)
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");

                    for (int i = 0; i < objNodLst.Count - 1; i++)
                    {
                        objElm = (XmlElement)objNodLst[i];
                        objRowTxt = p_objXmlDocument.CreateElement(objNodLst[i].Name);
                        if (objNodLst[i].Attributes["type"] != null)
                        {
                            if (objNodLst[i].Attributes["type"].Value.ToLower() == "date")
                            {
                                if (objElm.Name == "FromDate")
                                {
                                    objRowTxt.InnerText = objRow["FROM_DATE"].ToString();
                                }
                                else if (objElm.Name == "ToDate")
                                {
                                    objRowTxt.InnerText = objRow["TO_DATE"].ToString();
                                }
                                if (objRowTxt.InnerText.Trim() == "" || objRowTxt.InnerText == "0")
                                {
                                    objRowTxt.InnerText = "";
                                }
                                objRowTxt.InnerText = Conversion.ToDate(Conversion.GetDate(objRowTxt.InnerText)).ToShortDateString();
                            }
                            else if (objNodLst[i].Attributes["type"].Value.ToLower() == "code")
                            {
                            }
                            else if (objNodLst[i].Attributes["type"].Value.ToLower() == "bool")
                            {
                                objRowTxt.InnerText = objRow["ACTIVE_ORG"].ToString();
                                if (objRowTxt.InnerText == "-1")
                                    objRowTxt.InnerText = "Yes";
                                else
                                    objRowTxt.InnerText = "No";
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                            if (objElm.Name == "OrgId")
                            {
                                objRowTxt.InnerText = objRow["ENTITY_ID"].ToString();
                            }
                        }
                        objLstRow.AppendChild(objRowTxt);
                    }
                    objRowTxt = p_objXmlDocument.CreateElement("RowId");
                    objRowTxt.InnerText = objRow["OPTIONSET_ID"].ToString() + "|" + objRow["FROM_DATE"].ToString() + "|" + objRow["TO_DATE"].ToString() + "|" + ((objRow["ACTIVE_ORG"].ToString() == "-1") ? "YES" : "NO");
                    objLstRow.AppendChild(objRowTxt);
                    objNode.AppendChild((XmlNode)objLstRow);
                }
                objDs.Dispose();
            }
            finally
            {
                if (objDs != null)
                {
                    objDs.Dispose();
                    objDs = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objLstRow = null;
                objRowTxt = null;
                objNode = null;
                objNodLst = null;
                objElm = null;
            }
        }
        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Save Org Related information into table DA_CLAIM_EXPORT_ORG_HIERARCHY.
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string[] arrId;
            string sRowId = "";
            string sOrgId = "";
            string sFromDate = "";
            string sToDate = "";
            string sSQL = "";
            string sCurrentId = "";
            int iActive = -1;
            int iOptionsetFromXml = 0;

            int iOptionsetID = GetMaxOptionSetId() + 1;


            XmlElement objElm = null;
            DbConnection objConn = null;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='OptionSet']");
                if (objElm != null)
                    iOptionsetFromXml = Convert.ToInt32(objElm.InnerText);

                if (iOptionsetFromXml < iOptionsetID && (iOptionsetFromXml != 0))
                    iOptionsetID = iOptionsetFromXml;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objElm != null)
                    sRowId = Conversion.GetDate(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Claim From Date']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sFromDate = Conversion.GetDate(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Claim To Date']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sToDate = Conversion.GetDate(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Active']");
                if (objElm != null)
                {
                    if (objElm.InnerText.ToLower() == "false")
                    {
                        iActive = 0;
                    }
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org Hierarchy']");
                if (objElm != null)
                    sOrgId = objElm.GetAttribute("codeid");

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org Hierarchy']");
                if (objElm != null)
                    sCurrentId = objElm.GetAttribute("currentid");


                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                arrId = p_objXmlDocument.SelectSingleNode("//control[@name='Org_orglist']").InnerText.Split('|');
                for (int iCtr = 0; iCtr < arrId.Length; iCtr++)
                {
                    if (arrId[iCtr] != "")
                    {
                        sSQL = "DELETE FROM DA_CLAIM_EXPORT_ORG_HIERARCHY WHERE OPTIONSET_ID=" + iOptionsetID + " AND ENTITY_ID = " + arrId[iCtr];
                        objConn.ExecuteNonQuery(sSQL);
                        sSQL = "INSERT INTO DA_CLAIM_EXPORT_ORG_HIERARCHY VALUES(" + iOptionsetID + ",'" + arrId[iCtr] + "','" + sFromDate + "','" + sToDate + "'," + iActive + "," + -1 + ")";
                        objConn.ExecuteNonQuery(sSQL);
                        p_objXmlDocument.SelectSingleNode("//control[@name= 'OptionSet']").InnerText = iOptionsetID.ToString();
                    }
                }
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Delete selected enty from table DA_CLAIM_EXPORT_ORG_HIERARCHY.
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                string WhereClause = string.Empty;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org Hierarchy']");
                if (objElm != null)
                {
                    WhereClause = "ENTITY_ID =" + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Optionset']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND OPTIONSET_ID = " + objElm.InnerText;
                }
                DeleteGridEntry(WhereClause, "DA_CLAIM_EXPORT_ORG_HIERARCHY");
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
            }
        }

        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Delete selected enty from table DA_CLAIM_EXPORT_ORG_HIERARCHY according to provided where clause.
        /// </summary>
        /// <param name="sWhereClause"></param>
        /// <param name="sTableName"></param>
        /// <returns></returns>
        protected virtual bool DeleteGridEntry(string sWhereClause, string sTableName)
        {
            DbConnection objConn = null;
            string sSQL = "";
            try
            {
                sSQL = String.Format("DELETE FROM {0} WHERE {1}", sTableName, sWhereClause);
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.Dispose();
                return true;
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }
        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Always Clean these tables DA_CLAIM_EXPORT_ORG_HIERARCHY on Optionset Cancellation.
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public bool CleanDataGridonCancel(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            DbConnection objConn = null;
            DbCommand objCommand = null;
            string sSql = string.Empty;
            int iOptionsetID = 0;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Optionset']");
                if (objElm != null)
                {
                    iOptionsetID = Convert.ToInt32(objElm.InnerText);
                }
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                sSql = "DELETE FROM DA_CLAIM_EXPORT_ORG_HIERARCHY WHERE OPTIONSET_ID = " + iOptionsetID + " AND DEL_FLAG = " + -1;
                objCommand.CommandText = sSql;
                objCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                if (objConn != null)
                    objConn.Dispose();
            }
            return true;
        }

        /// <summary>
        /// Developer :- Subhendu| Module - DA CLAIM EXPORT CSStars | Function - Clean and Insert all dept into DA_CLAIM_EXPORT_DEPARTMENT. Update DEL_FLAG for DA_CLAIM_EXPORT_ORG_HIERARCHY
        /// </summary>
        /// <param name="bChkreEdit">False: Saving Optionset for 1st time. True : Editing the Optionset</param>
        /// <param name="iOptionSetID">Optionset carried from UI</param>
        public void SaveCEDeptTable(bool bChkreEdit, int iOptionSetID)
        {
            string sSql = string.Empty;
            int iOptionset = 0;
            DbConnection objconn = null;
            if (bChkreEdit)
                iOptionset = iOptionSetID;
            else
                iOptionset = GetMaxOptionSetId() + 1;
            try
            {
                objconn = DbFactory.GetDbConnection(m_connectionString);
                objconn.Open();

                sSql = "DELETE FROM DA_CLAIM_EXPORT_DEPARTMENT WHERE OPTIONSET_ID = " + iOptionset;
                objconn.ExecuteNonQuery(sSql);
                sSql = "UPDATE DA_CLAIM_EXPORT_ORG_HIERARCHY SET DEL_FLAG = " + 0 + "WHERE OPTIONSET_ID = " + iOptionset;
                objconn.ExecuteNonQuery(sSql);
                sSql = "SELECT ENTITY_ID FROM DA_CLAIM_EXPORT_ORG_HIERARCHY WHERE OPTIONSET_ID =" + iOptionset + " AND ACTIVE_ORG = " + -1;

                DbReader objReader = DbFactory.GetDbReader(m_connectionString, sSql);
                while (objReader.Read())
                {
                    sSql = "";
                    sSql = "SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID = " + objReader.GetInt32("ENTITY_ID");
                    DataSet objEntityTblID = DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);
                    int iEntTblID = Convert.ToInt32(objEntityTblID.Tables[0].Rows[0][0]);
                    sSql = "";
                    switch (iEntTblID)
                    {
                        case 1005:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE CLIENT_EID = " + objReader.GetInt32("ENTITY_ID");
                            break;
                        case 1006:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE COMPANY_EID = " + objReader.GetInt32("ENTITY_ID");
                            break;
                        case 1007:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE OPERATION_EID = " + objReader.GetInt32("ENTITY_ID");
                            break;
                        case 1008:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE REGION_EID = " + objReader.GetInt32("ENTITY_ID");
                            break;
                        case 1009:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE DIVISION_EID = " + objReader.GetInt32("ENTITY_ID");
                            break;
                        case 1010:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE LOCATION_EID = " + objReader.GetInt32("ENTITY_ID");
                            break;
                        case 1011:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE FACILITY_EID = " + objReader.GetInt32("ENTITY_ID");
                            break;
                        case 1012:
                            sSql = "SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID  = " + objReader.GetInt32("ENTITY_ID");
                            break;
                    }
                    DataSet objDeptID = DbFactory.GetDataSet(m_connectionString, sSql, m_iClientId);

                    objconn = DbFactory.GetDbConnection(m_connectionString);
                    objconn.Open();

                    int iCount = objDeptID.Tables[0].Rows.Count;
                    for (int i = 0; i < iCount; i++)
                    {
                        sSql = "INSERT INTO DA_CLAIM_EXPORT_DEPARTMENT VALUES (" + iOptionset + ", " + objReader.GetInt32("ENTITY_ID") + " , " + objDeptID.Tables[0].Rows[i][0] + " ," + 0 + ")";
                        objconn.ExecuteNonQuery(sSql);
                    }
                }
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                if (objconn != null)
                {
                    objconn.Close();
                    objconn.Dispose();
                }
            }
        }

        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
        /// <summary>
        /// // Developer - Subhendu/Ihtesham : MITS 30839 : Function GetPolicyLOB to Fetch POLICY LOB : Start
        /// </summary>
        /// <param name="stablename"></param>
        /// <returns></returns>
        public DataSet GetPolicyLOB(string stablename)
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            string sSQL = string.Empty;
            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE ||'' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME ='" + stablename + "' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }
            else
            {
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE +'' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME ='" + stablename + "' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            }

            DataSet dsPolicyLOB = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return dsPolicyLOB;
        }
        // Developer - Subhendu/Ihtesham : MITS 30839 : Function GetPolicyLOB : End

        //Developer – abharti5 |MITS 36676| start
        public DataSet GetPolicySystemNames(string stablename)
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();
            string sSQL = string.Empty;
            if (objConn.DatabaseType.ToString() == "DBMS_IS_ORACLE")
            {
                sSQL = "SELECT POLICY_SYSTEM_ID, POLICY_SYSTEM_NAME FROM " + stablename + "";
            }
            else
            {
                sSQL = "SELECT POLICY_SYSTEM_ID, POLICY_SYSTEM_NAME FROM " + stablename + "";
            }

            DataSet dsPolicySystemNames = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return dsPolicySystemNames;
        }
        //Developer – abharti5 |MITS 36676| end

        public DataSet GetWCPolicyType(string stablename)
        {
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_connectionString);
            objConn.Open();

            string sSQL = string.Empty;

            sSQL = "SELECT C.CODE_ID, C.LINE_OF_BUS_CODE FROM CODES C, GLOSSARY G WHERE C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME ='" + stablename + "' AND C.RELATED_CODE_ID IN(SELECT CODE_ID FROM CODES, GLOSSARY WHERE CODES.TABLE_ID = GLOSSARY.TABLE_ID  AND CODES.SHORT_CODE ='WL')  AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";

            DataSet dsWCPolicyType = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);

            if (objConn != null)
            {
                objConn.Close();
                objConn.Dispose();
            }
            return dsWCPolicyType;
        }

        public int GetPolicyClaimLOBType(int sPolicyCode)
        {
            int iLOB = 0;
            string sSQL = string.Empty;

            sSQL = "SELECT C.LINE_OF_BUS_CODE FROM CODES C, GLOSSARY G WHERE C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME ='POLICY_CLAIM_LOB' AND C.CODE_ID ='"
                + sPolicyCode + "'  AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            iLOB = Convert.ToInt32(DbFactory.ExecuteScalar(m_connectionString, sSQL));

            return iLOB;
        }

        /// <summary>
        /// // Developer - Subhendu/Ihtesham : MITS 30839 : Function GetShortCode_ISO to fetch Short code and Code desc for a provided Code id : Start
        /// </summary>
        /// <param name="iCodeID"></param>
        /// <returns></returns>
        public string GetShortCode_ISO(int iCodeID)
        {
            string sShortCode = string.Empty;
            string sSQL = string.Empty;

            if (DbFactory.IsOracleDatabase(m_connectionString))
            {
                sSQL = "SELECT C.SHORT_CODE || '-' ||  C.CODE_DESC FROM CODES_TEXT C WHERE C.CODE_ID = " + iCodeID;
            }
            else
            {
                sSQL = "SELECT C.SHORT_CODE +'-' +  C.CODE_DESC FROM CODES_TEXT C WHERE C.CODE_ID = " + iCodeID;
            }
            return (DbFactory.ExecuteScalar(m_connectionString, sSQL).ToString());
        }
        // Developer - Subhendu/Ihtesham : MITS 30839 : Function GetShortCode_ISO : End

        /// <summary>
        /// // Developer - Subhendu/Ihtesham : MITS 30839 : Function GetDesc_ISO to fetch Description from ISO POLICY CODES/ISO COVERAGE CODES/ISO LOSS CODES: Start
        /// </summary>
        /// <param name="sPolicy"></param>
        /// <param name="sCoverage"></param>
        /// <param name="sLoss"></param>
        /// <param name="stablename"></param>
        /// <returns></returns>
        private string GetDesc_ISO(string sPolicy, string sCoverage, string sLoss, string stablename)
        {
            string sDesc = string.Empty;
            string sSQL = string.Empty;

            switch (stablename)
            {
                case "CL_ISO_POLICY_CODES":
                    sSQL = "SELECT DESCRIP FROM " + stablename + " WHERE POLICY_CODE ='" + sPolicy + "'";
                    break;
                case "CL_ISO_COVERAGE_CODES":
                    sSQL = "SELECT DESCRIP FROM " + stablename + " WHERE POLICY_CODE ='" + sPolicy + "' AND COVERAGE_CODE ='" + sCoverage + "'";
                    break;
                case "CL_ISO_LOSS_CODES":
                    sSQL = "SELECT DESCRIP FROM " + stablename + " WHERE POLICY_CODE ='" + sPolicy + "' AND COVERAGE_CODE ='" + sCoverage + "' AND LOSS_CODE='" + sLoss + "'";
                    break;
            }
            return DbFactory.ExecuteScalar(m_connectionString, sSQL).ToString();
        }
        // Developer - Subhendu/Ihtesham : MITS 30839 : Function GetDesc_ISO : End

        public XmlDocument ISOGet(XmlDocument p_objXmlDocument)
        {
            LocalCache objCache = null;
            XmlNodeList objNodLst = null;
            XmlElement objElement = null;
            XmlDocument XmlNew = new XmlDocument();

            try
            {

                //ISOGet("CLAIM_TYPE_CODE,LOB,ISO_POLICY_CODE,ISO_COVERAGE_CODE,ISO_LOSS_CODE,POLICY_LOB_CODE,COVERAGE_TYPE_CODE,LOSS_TYPE_CODE,DISABILITY_CODE,ISO_RECORD_TYPE", "CL_ISO_LOSS_TYPE_MAP", "", p_objXmlDocument); original
                ISOGet("POLICY_SYSTEM_NAMES,CLAIM_TYPE_CODE,LOB,ISO_POLICY_CODE,ISO_COVERAGE_CODE,ISO_LOSS_CODE,POLICY_LOB_CODE,COVERAGE_TYPE_CODE,LOSS_TYPE_CODE,DISABILITY_CODE,ISO_RECORD_TYPE", "CL_ISO_LOSS_TYPE_MAP", "", p_objXmlDocument); //Developer – abharti5 |MITS 36676|
                objNodLst = p_objXmlDocument.SelectNodes("//listrow");
                objCache = new LocalCache(m_connectionString,m_iClientId);

                foreach (XmlElement objElm in objNodLst)
                {
                    foreach (XmlElement objChildElement in objElm.ChildNodes)
                    {
                        if (objChildElement.Name == "RowId")
                        {
                            objElement = objChildElement;
                        }
                    }
                    objElement.InnerText = objElement.InnerText + "|";
                }
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objNodLst = null;
            }
        }

        /// <summary>
        /// // Developer - Subhendu/Ihtesham : MITS 30839 : Function ISOGet read data to display grid : Start
        /// </summary>
        /// <param name="sFields"></param>
        /// <param name="sTableName"></param>
        /// <param name="sOrderbyClause"></param>
        /// <param name="p_objXmlDocument"></param>

        //Developer – abharti5 |MITS 36676| start
        public string GetShortCode_ISO_PSN(int iPSNID)
        {
            string sShortCode = string.Empty;
            string sSQL = string.Empty;
            object oReturnedValue = null;
            string sPolicySystemName = string.Empty;

            if (DbFactory.IsOracleDatabase(m_connectionString))
            {
                sSQL = "SELECT P.POLICY_SYSTEM_NAME FROM POLICY_X_WEB P WHERE P.POLICY_SYSTEM_ID = " + iPSNID;
            }
            else
            {
                sSQL = "SELECT P.POLICY_SYSTEM_NAME FROM POLICY_X_WEB P WHERE P.POLICY_SYSTEM_ID = " + iPSNID;
            }
            oReturnedValue = DbFactory.ExecuteScalar(m_connectionString, sSQL);
            if (oReturnedValue != null && oReturnedValue != DBNull.Value)
            {
                sPolicySystemName = Convert.ToString(oReturnedValue);
            }
            return sPolicySystemName;
        } 
        //Developer – abharti5 |MITS 36676| end 

        protected virtual void ISOGet(string sFields, string sTableName, string sOrderbyClause, XmlDocument p_objXmlDocument)
        {
            DataSet objDs = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            XmlElement objElm = null;
            string sSQL = "";
            string sTemp = string.Empty;
            //LocalCache objCache = null;
            int iPolicySystemId = 0; //Developer – abharti5 |MITS 36676|
            int iLOB = 0;
            string sPolicy = string.Empty;
            string sCoverage = string.Empty;
            string sLoss = string.Empty;
            //mkaran2 - MITS 34134
            int totalRows = 0;
            int totalPages = 0;

            try
            {
                //objCache = new LocalCache(this.ConnectString);

                sSQL = String.Format("SELECT {0} FROM {1}", sFields, sTableName);
                //mkaran2 - MITS 34134
                sSQL = sSQL.Replace("SELECT", "SELECT * FROM (SELECT  ROW_NUMBER() OVER (ORDER BY CLAIM_TYPE_CODE,ISO_POLICY_CODE, ISO_COVERAGE_CODE, ISO_LOSS_CODE) as rownumber,");

                if (iCarrier == -1)
                    sSQL = sSQL + " WHERE POLICY_LOB_CODE <> 0";
                else
                    sSQL = sSQL + " WHERE POLICY_LOB_CODE= 0 and LOB <> 0";

                //mkaran2 - MITS 34134 - Start
                //if (sOrderbyClause != null && sOrderbyClause != "")
                //    sSQL += " ORDER BY " + sOrderbyClause;

                sSQL = string.Concat(sSQL, ") tblTemp WHERE rownumber between (" + ISOCurrentPage + " -1 ) * " + ISOPageSize + "+ 1 and " + ISOCurrentPage + " * " + ISOPageSize);
                //mkaran2 - MITS 34134 - End

                objDs = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);

                objNode = p_objXmlDocument.SelectSingleNode("//*[name()='" + "ISOMappingList" + "']");
                objNodLst = objNode.ChildNodes.Item(0).ChildNodes;

                foreach (DataRow objRow in objDs.Tables[0].Rows)
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");

                    for (int i = 0; i < objNodLst.Count - 1; i++)
                    {
                        objElm = (XmlElement)objNodLst[i];
                        objRowTxt = p_objXmlDocument.CreateElement(objNodLst[i].Name);
                        if (objNodLst[i].Attributes["type"] != null)
                        {
                            if (objNodLst[i].Attributes["type"].Value.ToLower() == "int")
                            {

                            }
                            else if (objNodLst[i].Attributes["type"].Value.ToLower() == "varchar")
                            {

                            }
                            else
                            {
                            }
                        }
                        else
                        {
                            if (iCarrier == -1)
                            {
                                if (objElm.Name == "ddlPolicyLOB")
                                {
                                    iLOB = Convert.ToInt32(objRow["LOB"]);
                                    objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["POLICY_LOB_CODE"]));
                                }
                                //Developer – abharti5 |MITS 36676| start
                                if (objElm.Name == "ddlPolicySystemNames")
                                {
                                    //int iPolicySystemId = Convert.ToInt32(objRow["POLICY_SYSTEM_ID"]);
                                    objRowTxt.InnerText = GetShortCode_ISO_PSN(Convert.ToInt32(objRow["POLICY_SYSTEM_NAMES"]));
                                    // objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["POLICY_LOB_CODE"])); // to be corrected
                                }
                                //Developer – abharti5 |MITS 36676| end

                            }
                            else
                            {
                                if (objElm.Name == "hdClaimLOB")
                                {
                                    iLOB = Convert.ToInt32(objRow["LOB"]);
                                    objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["LOB"]));
                                }
                            }

                            if (objElm.Name == "ddlCoverage")
                            {
                                objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["COVERAGE_TYPE_CODE"]));
                            }

                            if (objElm.Name == "ddlISOClaimType")
                            {
                                objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["CLAIM_TYPE_CODE"]));
                            }

                            //if (objElm.Name == "hdLOBpopup")
                            //{
                            //    objRowTxt.InnerText=GetShortCode_ISO( Convert.ToInt32(objRow["LOB"]));
                            //    //objRowTxt.InnerText = objRow["LOB"].ToString();
                            //}
                            if (objElm.Name == "ddlISOPolicyType")
                            {
                                sPolicy = objRow["ISO_POLICY_CODE"].ToString();
                                //objRowTxt.InnerText = objRow["ISO_POLICY_CODE"].ToString();
                                //objRowTxt.InnerText = GetDesc_ISO(iLOB,sPolicy, "", "","CL_ISO_POLICY_CODES");
                                objRowTxt.InnerText = GetDesc_ISO(sPolicy, "", "", "CL_ISO_POLICY_CODES");
                            }

                            if (objElm.Name == "ddlISOCoverageType")
                            {
                                sCoverage = objRow["ISO_COVERAGE_CODE"].ToString();
                                //objRowTxt.InnerText = objRow["ISO_COVERAGE_CODE"].ToString();
                                //objRowTxt.InnerText = GetDesc_ISO(iLOB,sPolicy, sCoverage, "", "CL_ISO_COVERAGE_CODES");
                                objRowTxt.InnerText = GetDesc_ISO(sPolicy, sCoverage, "", "CL_ISO_COVERAGE_CODES");
                            }

                            if (objElm.Name == "ddlISOLossType")
                            {
                                sLoss = objRow["ISO_LOSS_CODE"].ToString();
                                //objRowTxt.InnerText = objRow["ISO_LOSS_CODE"].ToString();
                                //objRowTxt.InnerText = GetDesc_ISO(iLOB,sPolicy, sCoverage, sLoss, "CL_ISO_LOSS_CODES");
                                objRowTxt.InnerText = GetDesc_ISO(sPolicy, sCoverage, sLoss, "CL_ISO_LOSS_CODES");
                            }

                            if (objElm.Name == "ddlTypeOfLoss")
                            {
                                sTemp = "";
                                sTemp = GetShortCode_ISO(Convert.ToInt32(objRow["LOSS_TYPE_CODE"]));
                                if (sTemp == "")
                                {
                                    objRowTxt.InnerText = "NA";
                                }
                                else
                                {
                                    objRowTxt.InnerText = sTemp;
                                }
                            }

                            if (objElm.Name == "ddlDisabilityCode")
                            {
                                sTemp = "";
                                sTemp = GetShortCode_ISO(Convert.ToInt32(objRow["DISABILITY_CODE"]));
                                if (sTemp == "")
                                {
                                    objRowTxt.InnerText = "NA";
                                }
                                else
                                {
                                    objRowTxt.InnerText = sTemp;
                                }
                            }
                            if (objElm.Name == "ISORecordType")
                            {
                                objRowTxt.InnerText = objRow["ISO_RECORD_TYPE"].ToString();
                            }
                        }
                        objLstRow.AppendChild(objRowTxt);
                    }

                    objRowTxt = p_objXmlDocument.CreateElement("RowId");

                    //objRowTxt.InnerText = objRow["CLAIM_TYPE_CODE"].ToString() + "|" + objRow["LOB"].ToString() + "|" + objRow["ISO_POLICY_CODE"].ToString() + "|" + objRow["ISO_COVERAGE_CODE"].ToString() + "|" + objRow["ISO_LOSS_CODE"].ToString() + "|" + objRow["POLICY_LOB_CODE"].ToString() + "|" + objRow["COVERAGE_TYPE_CODE"].ToString() + "|" + objRow["LOSS_TYPE_CODE"].ToString() + "|" + objRow["DISABILITY_CODE"].ToString() + "|" + objRow["ISO_RECORD_TYPE"].ToString(); original

                    objRowTxt.InnerText = objRow["POLICY_SYSTEM_NAMES"].ToString() + "|" + objRow["CLAIM_TYPE_CODE"].ToString() + "|" + objRow["LOB"].ToString() + "|" + objRow["ISO_POLICY_CODE"].ToString() + "|" + objRow["ISO_COVERAGE_CODE"].ToString() + "|" + objRow["ISO_LOSS_CODE"].ToString() + "|" + objRow["POLICY_LOB_CODE"].ToString() + "|" + objRow["COVERAGE_TYPE_CODE"].ToString() + "|" + objRow["LOSS_TYPE_CODE"].ToString() + "|" + objRow["DISABILITY_CODE"].ToString() + "|" + objRow["ISO_RECORD_TYPE"].ToString();//Developer – abharti5 |MITS 36676|
                    
                    objLstRow.AppendChild(objRowTxt);
                    objNode.AppendChild((XmlNode)objLstRow);
                }
                objDs.Dispose();
                //mkaran2 - MITS 34134 - Start
                sSQL = string.Empty;
                sSQL = String.Format("SELECT COUNT (ISO_ROW_ID) FROM {0}", sTableName);

                if (iCarrier == -1)
                    sSQL = sSQL + " WHERE POLICY_LOB_CODE <> 0";
                else
                    sSQL = sSQL + " WHERE POLICY_LOB_CODE= 0 and LOB <> 0";

                totalRows = Convert.ToInt32(DbFactory.ExecuteScalar(m_connectionString, sSQL));

                if (totalRows != 0)
                {
                    totalPages = totalRows / Convert.ToInt32(ISOPageSize);
                    if ((totalRows % Convert.ToInt32(ISOPageSize)) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }

                objNode = null;

                objNode = p_objXmlDocument.SelectSingleNode("//ISOMappingList//hdTotalPages");
                if (objNode != null)
                    objNode.InnerText = totalPages.ToString();


                //mkaran2 - MITS 34134 - End                
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                if (objDs != null)
                {
                    objDs.Dispose();
                    objDs = null;
                }
                //if (objCache != null)
                //{
                //    objCache.Dispose();
                //    objCache = null;
                //}
                objLstRow = null;
                objRowTxt = null;
                objNode = null;
                objNodLst = null;
                objElm = null;
            }
        }
        // Developer - Subhendu/Ihtesham : MITS 30839 : Function ISOGet : End

        public XmlDocument ISOEdit(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm;
            //int LOB
            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LOB']");
            try
            {
                //XMLDoc = p_objXmlDocument;
                int iLOB = Convert.ToInt32(p_objXmlDocument.SelectSingleNode("//control[@name='LOB']").FirstChild.Value);

                int iPSN = Convert.ToInt32(p_objXmlDocument.SelectSingleNode("//control[@name='PSN']").FirstChild.Value); //Developer – abharti5 |MITS 36676|
                
                int iClaimType = Convert.ToInt32(p_objXmlDocument.SelectSingleNode("//control[@name='Claim Type']").FirstChild.Value);
                int iLossType = Convert.ToInt32(p_objXmlDocument.SelectSingleNode("//control[@name='Type Of Loss']").FirstChild.Value);
                int iDisability = Convert.ToInt32(p_objXmlDocument.SelectSingleNode("//control[@name='Disability Code']").FirstChild.Value);
                ISOEdit("CL_ISO_LOSS_TYPE_MAP", iLOB, iClaimType, iLossType, iDisability, p_objXmlDocument);
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
            }

        }


        protected virtual void ISOEdit(string sTableName, int iLOB, int iClaimType, int iLossType, int iDisability, XmlDocument objXmlDoc)
        {
            string sSQL = "";
            DbConnection objConn = null;
            DbReader objDbReader = null;
            XmlNodeList objNodeList = null;
            string sValue = "";
            LocalCache objLocalCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sControlType = "";
            string[] arrFieldList1 = { "hdLOBpopup", "Claim Type", "Type Of Loss", "Disability Code" };

            try
            {
                {
                    sSQL = String.Format("SELECT * FROM {0} WHERE POLICY_LOB_CODE = {1} AND CLAIM_TYPE_CODE = {2} AND LOSS_TYPE_CODE = {3} AND DISABILITY_CODE = {4}", sTableName, iLOB, iClaimType, iLossType, iDisability);
                }
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objDbReader = objConn.ExecuteReader(sSQL);
                objLocalCache = new LocalCache(m_connectionString,m_iClientId);

                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        foreach (string sName in arrFieldList1)
                        {
                            if (sName == "hdLOBpopup")
                                sValue = objDbReader["LOB"].ToString();
                            else if (sName == "Claim Type")
                                sValue = objDbReader["CLAIM_TYPE_CODE"].ToString();
                            else if (sName == "Type Of Loss")
                                sValue = objDbReader["LOSS_TYPE_CODE"].ToString();
                            else if (sName == "Disability Code")
                                sValue = objDbReader["DISABILITY_CODE"].ToString();

                            objNodeList = objXmlDoc.GetElementsByTagName("control");
                            foreach (XmlElement objXmlElement in objNodeList)
                            {
                                if (objXmlElement.GetAttribute("name") == sName)
                                {
                                    sControlType = objXmlElement.GetAttribute("type").ToLower();
                                    switch (sControlType)
                                    {
                                        //case "checkbox":
                                        //    if (sValue.Equals("-1") || sValue.Equals("1"))
                                        //        objXmlElement.InnerText = "True";
                                        //    else
                                        //        objXmlElement.InnerText = "";
                                        //    break;
                                        //case "date":
                                        //    objXmlElement.InnerText = Conversion.GetDBDateFormat(sValue, "d");
                                        //    break;
                                        //case "orglist":
                                        //    //Textbox--start
                                        //    objXmlElement.InnerXml = "";
                                        //    objXmlElement.SetAttribute("orgid", sValue);
                                        //    int iOrgId = Convert.ToInt32(sValue);
                                        //    string sAbbr = "";
                                        //    LocalCache objCache = new LocalCache(m_connectionString);
                                        //    objCache.GetOrgInfo(iOrgId, ref sAbbr, ref sDesc);
                                        //    objXmlElement.InnerXml = sAbbr.Replace("&", "&amp;") + "-" + sDesc.Replace("&", "&amp;");
                                        //    break;
                                        ////Textbox--end
                                        default:
                                            objXmlElement.InnerText = sValue;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    objDbReader.Close();
                }
                objConn.Dispose();
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
                objNodeList = null;
            }
        }


        public XmlDocument ISODelete(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                string WhereClause = string.Empty;

                //Developer – abharti5 |MITS 36676| start
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PSN']");
                if (objElm != null)
                {
                    WhereClause = "POLICY_SYSTEM_NAMES =" + objElm.InnerText;
                }
                //Developer – abharti5 |MITS 36676| end


                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Claim Type']");
                if (objElm != null)
                {
                    WhereClause = "CLAIM_TYPE_CODE =" + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='hdClaimLOB']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND LOB = " + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Coverage']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND COVERAGE_TYPE_CODE = " + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Type Of Loss']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND LOSS_TYPE_CODE = " + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Disability Code']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND DISABILITY_CODE = " + objElm.InnerText;
                }
                DeleteGridEntry(WhereClause, "CL_ISO_LOSS_TYPE_MAP");
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
            }
        }

        // Developer - Subhendu : MITS 30839 : Function ISODelete Not Required any more. : Start
        //protected virtual bool ISODelete(string sWhereClause)
        //{
        //    DbConnection objConn = null;
        //    string sSQL = "";
        //    try
        //    {
        //        sSQL = String.Format("DELETE FROM {0} WHERE {1}", "CL_ISO_LOSS_TYPE_MAP", sWhereClause);
        //        objConn = DbFactory.GetDbConnection(m_connectionString);
        //        objConn.Open();
        //        objConn.ExecuteNonQuery(sSQL);
        //        objConn.Dispose();
        //        return true;
        //    }
        //    finally
        //    {
        //        if (objConn != null)
        //        {
        //            objConn.Dispose();
        //            objConn = null;
        //        }
        //    }
        //}
        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End
        // Developer - Subhendu : MITS 30839 : Function ISODelete: End

        /// <summary>
        /// Vsoni5 : MITS 24981
        /// This function return LAst run date for the optionset supplied as a parameter.
        /// </summary>
        /// <param name="iOptionSetId"></param>
        /// <returns></returns>
        public string GetLastRunTime(int iOptionSetId)
        {
            string sDTTM_LAST_RUN = string.Empty;
            object objDTTM_LAST_RUN = null;
            string sNow = String.Format(DateTime.Now.ToString(), "YYYYMMDDHHNNSS");
            string sSQL = string.Empty;

            sSQL = "SELECT DTTM_LAST_RUN FROM DATA_INTEGRATOR WHERE OPTIONSET_ID = " + iOptionSetId;
            objDTTM_LAST_RUN = DbFactory.ExecuteScalar(m_connectionString, sSQL);
            sDTTM_LAST_RUN = objDTTM_LAST_RUN != null ? objDTTM_LAST_RUN.ToString() : String.Empty;

            if (!String.IsNullOrEmpty(sDTTM_LAST_RUN))
            {
                sDTTM_LAST_RUN = sDTTM_LAST_RUN.Substring(0, 14);
                return sDTTM_LAST_RUN;
            }
            //Return default date i.e. 19000101
            return sDTTM_LAST_RUN;
        }
        /// <summary>
        /// // Developer - Subhendu : MITS 30839 : Function bIsCarrier to check whether Carrier setting enabled or not : Start
        /// </summary>
        /// <returns></returns>
        public bool IsCarrier()
        {
            string sSql = string.Empty;

            sSql = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME ='MULTI_COVG_CLM'";
            iCarrier = Convert.ToInt32(DbFactory.ExecuteScalar(m_connectionString, sSql));
            return (iCarrier == -1) ? true : false;
        }
        // Developer - Subhendu : MITS 30839 : Function bIsCarrier : End

        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | Start
        /// <summary>
        /// // Developer - Subhendu/Ihtesham : MITS 30839 : Function ISOSave Save selected data into RMA DB : Start
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument ISOSave(XmlDocument p_objXmlDocument)
        {
            string smode = "";
            int sRowId = 0;
            int sPolicyLOB = 0;
            int sPolicyPSN = 0; //Developer – abharti5 |MITS 36676|
            int sCoverage = 0;
            int sClaimType = 0;
            int sTypeOfLoss = 0;
            int sDisabilityCode = 0;
            string sISOPolicyType = "";
            string sISOCoverageType = "";
            string sISOLossType = "";
            string sSQL = "";
            int LOB = 0;
            int iCount = 0;
            string sRecordType = string.Empty;

            XmlElement objElm = null;
            DbConnection objConn = null;
            try
            {

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objElm != null)
                    sRowId = Convert.ToInt32(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LOB']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sPolicyLOB = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Claim Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sClaimType = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Coverage']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sCoverage = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Type Of Loss']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sTypeOfLoss = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Disability Code']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sDisabilityCode = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Policy Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISOPolicyType = objElm.InnerText;
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Coverage Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISOCoverageType = objElm.InnerText;
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Loss Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISOLossType = objElm.InnerText;
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='mode']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        smode = objElm.InnerText;
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='hdClaimLOB']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        LOB = Convert.ToInt32(objElm.InnerText);
                    }

                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Record Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sRecordType = objElm.InnerText;
                    }

                }

                //Developer – abharti5 |MITS 36676| start
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PSN']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sPolicyPSN = Convert.ToInt32(objElm.InnerText);
                    }
                }
                //Developer – abharti5 |MITS 36676| end
                
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();

                //sSQL = "SELECT COUNT(*) FROM CL_ISO_LOSS_TYPE_MAP WHERE CLAIM_TYPE_CODE =" + sClaimType + " AND LOB = " + LOB + " AND POLICY_LOB_CODE = " + sPolicyLOB + " AND COVERAGE_TYPE_CODE =" + sCoverage + " AND LOSS_TYPE_CODE = " + sTypeOfLoss + " AND DISABILITY_CODE = " + sDisabilityCode; original
                sSQL = "SELECT COUNT(*) FROM CL_ISO_LOSS_TYPE_MAP WHERE POLICY_SYSTEM_NAMES = " + sPolicyPSN + " AND CLAIM_TYPE_CODE =" + sClaimType + " AND LOB = " + LOB + " AND POLICY_LOB_CODE = " + sPolicyLOB + " AND COVERAGE_TYPE_CODE =" + sCoverage + " AND LOSS_TYPE_CODE = " + sTypeOfLoss + " AND DISABILITY_CODE = " + sDisabilityCode; //Developer – abharti5 |MITS 36676|
                iCount = Convert.ToInt32(DbFactory.ExecuteScalar(m_connectionString, sSQL));



                if (iCount == 0)
                {
                    if (DbFactory.IsOracleDatabase(m_connectionString))
                    {
                        //sSQL = "INSERT INTO CL_ISO_LOSS_TYPE_MAP(CLAIM_TYPE_CODE,LOB,ISO_POLICY_CODE,ISO_COVERAGE_CODE,ISO_LOSS_CODE,ISO_ROW_ID,POLICY_LOB_CODE,COVERAGE_TYPE_CODE,LOSS_TYPE_CODE,DISABILITY_CODE,ISO_RECORD_TYPE) VALUES(" + sClaimType + ",'" + LOB + "','" + sISOPolicyType + "','" + sISOCoverageType + "','" + sISOLossType + "'," + GetMaxId("MAX(ISO_ROW_ID)", "CL_ISO_LOSS_TYPE_MAP") + "," + sPolicyLOB + "," + sCoverage + "," + sTypeOfLoss + "," + sDisabilityCode + ", '" + sRecordType + "')"; original
                        sSQL = "INSERT INTO CL_ISO_LOSS_TYPE_MAP(CLAIM_TYPE_CODE,LOB,ISO_POLICY_CODE,ISO_COVERAGE_CODE,ISO_LOSS_CODE,ISO_ROW_ID,POLICY_LOB_CODE,COVERAGE_TYPE_CODE,LOSS_TYPE_CODE,DISABILITY_CODE,ISO_RECORD_TYPE,POLICY_SYSTEM_NAMES) VALUES(" + sClaimType + ",'" + LOB + "','" + sISOPolicyType + "','" + sISOCoverageType + "','" + sISOLossType + "'," + GetMaxId("MAX(ISO_ROW_ID)", "CL_ISO_LOSS_TYPE_MAP") + "," + sPolicyLOB + "," + sCoverage + "," + sTypeOfLoss + "," + sDisabilityCode + ", '" + sRecordType + "'," + sPolicyPSN + ")"; //Developer – abharti5 |MITS 36676|
                    }
                    else
                    {
                        //sSQL = "INSERT INTO CL_ISO_LOSS_TYPE_MAP(CLAIM_TYPE_CODE,LOB,ISO_POLICY_CODE,ISO_COVERAGE_CODE,ISO_LOSS_CODE,POLICY_LOB_CODE,COVERAGE_TYPE_CODE,LOSS_TYPE_CODE,DISABILITY_CODE,ISO_RECORD_TYPE) VALUES(" + sClaimType + ",'" + LOB + "','" + sISOPolicyType + "','" + sISOCoverageType + "','" + sISOLossType + "'," + sPolicyLOB + "," + sCoverage + "," + sTypeOfLoss + "," + sDisabilityCode + ", '" + sRecordType + "')"; original
                        sSQL = "INSERT INTO CL_ISO_LOSS_TYPE_MAP(CLAIM_TYPE_CODE,LOB,ISO_POLICY_CODE,ISO_COVERAGE_CODE,ISO_LOSS_CODE,POLICY_LOB_CODE,COVERAGE_TYPE_CODE,LOSS_TYPE_CODE,DISABILITY_CODE,ISO_RECORD_TYPE,POLICY_SYSTEM_NAMES) VALUES(" + sClaimType + ",'" + LOB + "','" + sISOPolicyType + "','" + sISOCoverageType + "','" + sISOLossType + "'," + sPolicyLOB + "," + sCoverage + "," + sTypeOfLoss + "," + sDisabilityCode + ", '" + sRecordType + "'," + sPolicyPSN + ")"; //Developer – abharti5 |MITS 36676|
                    }
                }
                else
                {
                    //sSQL = "UPDATE CL_ISO_LOSS_TYPE_MAP SET ISO_POLICY_CODE ='" + sISOPolicyType + "' , ISO_COVERAGE_CODE ='" + sISOCoverageType + "' , ISO_LOSS_CODE ='" + sISOLossType + "' , ISO_RECORD_TYPE= '" + sRecordType + "' WHERE CLAIM_TYPE_CODE =" + sClaimType + " AND LOB = " + LOB + " AND POLICY_LOB_CODE = " + sPolicyLOB + " AND COVERAGE_TYPE_CODE =" + sCoverage + " AND LOSS_TYPE_CODE = " + sTypeOfLoss + " AND DISABILITY_CODE = " + sDisabilityCode; original
                    sSQL = "UPDATE CL_ISO_LOSS_TYPE_MAP SET ISO_POLICY_CODE ='" + sISOPolicyType + "' , ISO_COVERAGE_CODE ='" + sISOCoverageType + "' , ISO_LOSS_CODE ='" + sISOLossType + "' , ISO_RECORD_TYPE= '" + sRecordType + "' WHERE POLICY_SYSTEM_NAMES =" + sPolicyPSN + " AND CLAIM_TYPE_CODE =" + sClaimType + " AND LOB = " + LOB + " AND POLICY_LOB_CODE = " + sPolicyLOB + " AND COVERAGE_TYPE_CODE =" + sCoverage + " AND LOSS_TYPE_CODE = " + sTypeOfLoss + " AND DISABILITY_CODE = " + sDisabilityCode; //Developer – abharti5 |MITS 36676|
                }
                objConn.ExecuteNonQuery(sSQL);

                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                //Add code to catch exception for Primary key violation
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }
        // Developer - Subhendu/Ihtesham : MITS 30839 : Function ISOSave : End
        //Developer - Md Ihtesham | Template - DA ISO Carrier Enhancement | End   
        public string GetCompanyName(int iEntityId)
        {
            string sCompanyName = string.Empty;
            string sLastName = string.Empty;
            string sAbbreviation = string.Empty;
            string sSQL = string.Empty;

            sSQL = "SELECT ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + iEntityId;
            using (DbReader objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
            {
                while (objReader.Read())
                {
                    sAbbreviation = Convert.ToString(objReader.GetValue("ABBREVIATION"));
                    sLastName = Convert.ToString(objReader.GetValue("LAST_NAME"));
                }
            }

            sCompanyName = sAbbreviation + "-" + sLastName;
            return sCompanyName;
        }

        ////Developer - Md Ihtesham | Template - DA ISO OMIG Enhancement | MITS 35711 start
        //function to bind the drop down lists on ISOSPRoleMappingGrid page
        public DataSet GetrmASPRoleAndISOSPRole()
        {
            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            StringBuilder sSql = new StringBuilder();
            try
            {
                if (DbFactory.IsOracleDatabase(m_connectionString))
                {
                    DataSet dsOracle = new DataSet();
                    DataTable dt, dt1 = null;
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID", GetRMTableID("MMSEA_CLPYREP_CODE")));
                    dt = (DataTable)DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId).Tables[0].Copy();
                    dt.TableName = "Table";
                    dsOracle.Tables.Add(dt);
                    sSql.Remove(0, sSql.Length);
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE || '-' || CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID", GetRMTableID("CL_ISO_PROVIDER_CODES")));
                    dt1 = (DataTable)(DataTable)DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId).Tables[0].Copy();
                    dt1.TableName = "Table1";
                    dsOracle.Tables.Add(dt1);
                    return dsOracle;
                }
                else
                {
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID;", GetRMTableID("MMSEA_CLPYREP_CODE")));
                    sSql.Append(string.Format("SELECT C.CODE_ID, (CT.SHORT_CODE + '-' + CT.CODE_DESC) SHORT_CODE FROM CODES C , CODES_TEXT CT WHERE TABLE_ID = {0} and C.CODE_ID=CT.CODE_ID;", GetRMTableID("CL_ISO_PROVIDER_CODES")));
                }

            }
            catch (Exception exe)
            {
                //no need to handle

            }
            return (DbFactory.GetDataSet(m_connectionString, sSql.ToString(), m_iClientId));

        }

        public XmlDocument ISOSaveSPRoleMapping(XmlDocument p_objXmlDocument)
        {
            int sISORowID = 0;
            int srmASPRoleType = 0;
            int sISOSPRoleType = 0;
            string sSQL = "";
            int iCount = 0;

            XmlElement objElm = null;
            DbConnection objConn = null;
            try
            {

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='rmA Service Proivder Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        srmASPRoleType = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Service Proivder Type']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISOSPRoleType = Convert.ToInt32(objElm.InnerText);
                    }
                }


                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO_ROW_ID']");
                if (objElm != null)
                {
                    if (!string.IsNullOrEmpty(objElm.InnerText))
                    {
                        sISORowID = Convert.ToInt32(objElm.InnerText);
                    }
                }

                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();


                sSQL = "SELECT COUNT(*) FROM CL_ISO_PROVIDER_TYPE_MAP WHERE RMA_PROVIDER_TYPE =" + srmASPRoleType + " ";
                iCount = Convert.ToInt32(DbFactory.ExecuteScalar(m_connectionString, sSQL));


                if (iCount == 0)
                {
                    if (DbFactory.IsOracleDatabase(m_connectionString))
                    {
                        sSQL = "INSERT INTO CL_ISO_PROVIDER_TYPE_MAP VALUES(" + GetMaxId("MAX(ISO_ROW_ID)", "CL_ISO_PROVIDER_TYPE_MAP") + "," + srmASPRoleType + "," + sISOSPRoleType + ")";
                    }
                    else
                    {
                        sSQL = "INSERT INTO CL_ISO_PROVIDER_TYPE_MAP VALUES(" + srmASPRoleType + "," + sISOSPRoleType + ")";
                    }
                }
                else
                {
                    sSQL = "UPDATE CL_ISO_PROVIDER_TYPE_MAP SET ISO_PROVIDER_TYPE =" + sISOSPRoleType + " WHERE RMA_PROVIDER_TYPE =" + srmASPRoleType;
                }
                objConn.ExecuteNonQuery(sSQL);

                return p_objXmlDocument;
            }
            catch (System.Data.SqlClient.SqlException sqe)
            {
                Log.Write(sqe.ToString(), m_iClientId);
                throw new RMAppException("Mapping for this rmA Service Provider already exists!");
            }
            catch (Exception e)
            {
                Log.Write(e.ToString(), m_iClientId);
                //Add code to catch exception for Primary key violation
                throw e;// new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        //function to display existing mapping in grid      
        public XmlDocument ISOGetSPRoleType(XmlDocument p_objXmlIn)
        {
            GetISOSPRolemapping("ISO_ROW_ID,RMA_PROVIDER_TYPE,ISO_PROVIDER_TYPE", "CL_ISO_PROVIDER_TYPE_MAP", ref p_objXmlIn);
            return p_objXmlIn;
        }

        private void GetISOSPRolemapping(string sFields, string sTableName, ref XmlDocument p_objXmlDocument)
        {
            DataSet objDs = null;
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            XmlElement objLstRow = null;
            string sSQL = string.Empty;
            XmlElement objElm = null;
            XmlElement objRowTxt = null;
            try
            {
                sSQL = String.Format("SELECT {0} FROM {1}", sFields, sTableName);
                objDs = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId);
                objNode = p_objXmlDocument.SelectSingleNode("//*[name()='" + "ISOSPRoleList" + "']");
                objNodLst = objNode.ChildNodes.Item(0).ChildNodes;
                foreach (DataRow objRow in objDs.Tables[0].Rows)
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");

                    for (int i = 0; i < objNodLst.Count - 1; i++)
                    {
                        objElm = (XmlElement)objNodLst[i];
                        objRowTxt = p_objXmlDocument.CreateElement(objNodLst[i].Name);
                        //if (objElm.Name == "ISO_ROW_ID")
                        //{
                        //    objRowTxt.InnerText = Convert.ToString(Convert.ToInt32(objRow["ISO_ROW_ID"]));
                        //}
                        //else
                        if (objElm.Name == "RMA_PROVIDER_TYPE")
                        {
                            objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["RMA_PROVIDER_TYPE"]));
                        }
                        if (objElm.Name == "ISO_PROVIDER_TYPE")
                        {
                            objRowTxt.InnerText = GetShortCode_ISO(Convert.ToInt32(objRow["ISO_PROVIDER_TYPE"]));
                        }
                        objLstRow.AppendChild(objRowTxt);
                    }
                    objRowTxt = p_objXmlDocument.CreateElement("RowId");
                    objRowTxt.InnerText = Convert.ToString(Convert.ToInt32(objRow["ISO_ROW_ID"])) + "|" + objRow["RMA_PROVIDER_TYPE"].ToString() + "|" + objRow["ISO_PROVIDER_TYPE"].ToString();
                    objLstRow.AppendChild(objRowTxt);
                    objNode.AppendChild((XmlNode)objLstRow);
                }
            }
            catch (Exception exe)
            {
                throw;
            }
        }


        public XmlDocument ISODeleteSPRoleMapping(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                string WhereClause = string.Empty;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='rmA Service Proivder Roles']");
                if (objElm != null)
                {
                    WhereClause = "RMA_PROVIDER_TYPE =" + objElm.InnerText;
                }
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISO Service Proivder Roles']");
                if (objElm != null)
                {
                    WhereClause = WhereClause + " AND ISO_PROVIDER_TYPE = " + objElm.InnerText;
                }
                DeleteGridEntry(WhereClause, "CL_ISO_PROVIDER_TYPE_MAP");
                return p_objXmlDocument;
            }
            catch (Exception e)
            {
                throw new RMAppException(e.Message);
            }
            finally
            {
                objElm = null;
            }
        }


        //Developer - Md Ihtesham | Template - DA ISO OMIG Enhancement | MITS 35711 End


        //Start - MITS# 36997 GAP 06 - agupta298 - GAP 6 - RMA 5497 
        public DataTable GetJurisdictionStates(DbConnection dbConn)
        {
            string sSQL = "SELECT STATE_NAME, STATE_ROW_ID FROM STATES WHERE STATE_NAME IS NOT NULL AND DELETED_FLAG != -1 ORDER BY STATE_NAME";
            DataTable objResult = DbFactory.GetDataSet(dbConn, sSQL, m_iClientId).Tables[0];
            return objResult;
        }

        public DataTable GetClaimTypes(DbConnection dbConn)
        {
            string sSQL = string.Empty;
            if (DbFactory.IsOracleDatabase(m_connectionString))
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'CLAIM_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            else
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'CLAIM_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";

            DataTable objResult = DbFactory.GetDataSet(dbConn, sSQL, m_iClientId).Tables[0];
            return objResult;
        }

        public DataTable GetCoverageTypes(DbConnection dbConn)
        {
            string sSQL = string.Empty;
            if (DbFactory.IsOracleDatabase(m_connectionString))
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'COVERAGE_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            else
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'COVERAGE_TYPE' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";

            DataTable objResult = DbFactory.GetDataSet(dbConn, sSQL, m_iClientId).Tables[0];
            return objResult;
        }

        public DataTable GetLossTypes(DbConnection dbConn)
        {
            string sSQL = string.Empty;
            if (DbFactory.IsOracleDatabase(m_connectionString))
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'LOSS_CODES' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            else
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'LOSS_CODES' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";

            DataTable objResult = DbFactory.GetDataSet(dbConn, sSQL, m_iClientId).Tables[0];
            return objResult;
        }

        public DataTable GetPolicyLOB(DbConnection dbConn)
        {
            string sSQL = string.Empty;
            if (DbFactory.IsOracleDatabase(m_connectionString))
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE || ' ' || CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'POLICY_CLAIM_LOB' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";
            else
                sSQL = "SELECT C.CODE_ID, (C.SHORT_CODE + ' ' + CT.CODE_DESC) SHORT_CODE FROM CODES C, CODES_TEXT CT, GLOSSARY G WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = G.TABLE_ID AND G.SYSTEM_TABLE_NAME = 'POLICY_CLAIM_LOB' AND C.DELETED_FLAG <> -1 ORDER BY C.SHORT_CODE";

            DataTable objResult = DbFactory.GetDataSet(dbConn, sSQL, m_iClientId).Tables[0];
            return objResult;
        }

        public DataTable GetTPInterfaceType(DbConnection dbConn)
        {
            string sSQL = "SELECT INTERFACE_ROW_ID, INTERFACE_SYSTEM FROM TPI_INTERFACE_NAME ORDER BY INTERFACE_SYSTEM";
            DataTable objResult = DbFactory.GetDataSet(m_connectionString, sSQL, m_iClientId).Tables[0];
            return objResult;
        }

        public string GetLastRunTime(DbConnection dbConn, int iOptionSetId)
        {
            string sDTTM_LAST_RUN = string.Empty;
            object objDTTM_LAST_RUN = null;
            string sNow = String.Format(DateTime.Now.ToString(), "YYYYMMDDHHNNSS");
            string sSQL = string.Empty;

            sSQL = "SELECT DTTM_LAST_RUN FROM DATA_INTEGRATOR WHERE OPTIONSET_ID = " + iOptionSetId;
            objDTTM_LAST_RUN = dbConn.ExecuteScalar(sSQL);
            sDTTM_LAST_RUN = objDTTM_LAST_RUN != null ? objDTTM_LAST_RUN.ToString() : String.Empty;

            if (!String.IsNullOrEmpty(sDTTM_LAST_RUN))
            {
                sDTTM_LAST_RUN = sDTTM_LAST_RUN.Substring(0, 14);
                return sDTTM_LAST_RUN;
            }
            //Return default date i.e. 19000101
            return sDTTM_LAST_RUN;
        }
        //End - MITS# 36997 GAP 06 - agupta298 - GAP 6 - RMA 5497 
    }
}
