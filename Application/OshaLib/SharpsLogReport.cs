using System;
using DataDynamics.ActiveReports;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using DataDynamics.ActiveReports;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004	
	/// Purpose :  This is the class corresponding to the Sharps Log Report (Active Report).
	///			   It contains methods related to the report layout, report fields initialization and populating them with data.
	///</summary>
	internal class SharpsLogReport : CustomActiveReport
	{
		#region Member Variables
		/// <summary>
		/// This will hold the current report being printed.
		/// </summary>
		private int iCurrentReport=0;
		/// <summary>
		/// Name of the company for current report
		/// </summary>
		string m_sCompanyName="";
		/// <summary>
		/// Start date of the report
		/// </summary>
		DateTime m_datStartDate;
		/// <summary>
		/// End date for the report
		/// </summary>
		DateTime m_datEndDate;
		/// <summary>
		/// Date on which report gets printed
		/// </summary>
		DateTime m_datPrintedOn;
		/// <summary>
		/// This variable keeps track of report in progress
		/// </summary>
		protected  int m_iSharpsLogReportInProcess;
		/// <summary>
		/// Keeps track of count of items.
		/// </summary>
		protected int m_iSharpsLogItemCount;
        protected int m_iClientId;
		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor
		/// </summary>
		internal SharpsLogReport(int p_iClientId):base(p_iClientId)
		{
			InitializeReport();
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method adds the data fields.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void SharpsLogReport_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
		{
			try
			{
				this.Fields.Add("nodata");
				this.Fields.Add("SharpObj");
				this.Fields.Add("SharpBrand");
				this.Fields.Add("EvtNum");
				this.Fields.Add("Employee");
				this.Fields.Add("Occupation");
				this.Fields.Add("EvtDate");
				this.Fields.Add("EvtLocation");
				this.Fields.Add("EvtDescription");//added the data field.
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SharpsLogReport.SharpsLogReport_DataInitialize.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void SharpsLogReport_FetchData(object p_objsender, FetchEventArgs p_objeArgs)
		{
			OSHASharpsLog objSharpLog=null;
			OSHAItem objItem=null;
			try
			{
				if(m_objNotify!=null)
				{
					//TODO Notification logic would be finalized after SMWorker is developed.
					if(m_objNotify.CheckAbort()!=0)
					{
						p_objeArgs.EOF=true;
						Declarations.CheckAbort(m_objNotify,m_iClientId);
					}
				}
				if(this.m_iSharpsLogReportInProcess == 0)
				{
					this.m_iSharpsLogReportInProcess=1;
				}

				
				if(this.m_iSharpsLogReportInProcess>m_arrlstRpts.Count)
				{
					p_objeArgs.EOF=true;
					return;
				}
				else
				{
					p_objeArgs.EOF=false;
				}
				objSharpLog=(OSHASharpsLog)m_arrlstRpts[this.m_iSharpsLogReportInProcess-1];
				this.ClearFields();
				if(objSharpLog.Items.Count!=0)
				{
					iCurrentReport=this.m_iSharpsLogReportInProcess;
					if(this.m_iSharpsLogItemCount==0)
					{
						this.m_iSharpsLogItemCount=1;
					}
					objItem=((OSHAItem)objSharpLog.Items[this.m_iSharpsLogItemCount-1]);
					
					//fill the data
					if(objSharpLog!=null)
					{
						m_sCompanyName=objSharpLog.Company.Name;
						m_datStartDate=objSharpLog.BeginDate;
						m_datEndDate=objSharpLog.EndDate;
						m_datPrintedOn=DateTime.Today;
						if(objSharpLog.Context!=null)
						{
							this.Fields["SharpObj"].Value=objSharpLog.Context.GetCodeDesc(objItem.SharpsObjectId);
						}
						if(objSharpLog.Context!=null)
						{
							this.Fields["SharpBrand"].Value=objSharpLog.Context.GetCodeDesc(objItem.SharpsMakeId);
						}
						this.Fields["EvtNum"].Value=objItem.CaseNumber;
                        //MITS 36359 Begin                           //////
                        //if UseEmployeeNames is checked then it should show the employee name even if privacy case is checked on osha record.
                        if (objSharpLog.UseEmployeeNames)
                        {
                            this.Fields["Employee"].Value = objItem.EmployeeFullName;
                        }
                        else
                        {
                            if (Convert.ToBoolean(objItem.PrivacyCase))
                            {
                                this.Fields["Employee"].Value = "Privacy Case";
                            }
                            else
                            {
                                this.Fields["Employee"].Value = objItem.EmployeeFullName;
                            }
                        }
                        //MITS 36359 End                           //////
						this.Fields["Occupation"].Value=objItem.Occupation;
						this.Fields["EvtDate"].Value=Declarations.ConvertDateTimeToMmDdYyyy(objItem.DateOfEvent,m_iClientId);
						this.Fields["EvtLocation"].Value=objItem.EventLocation;
						this.Fields["EvtDescription"].Value=objItem.ActivityWhenInjured;
						objItem=null;
						this.m_iSharpsLogItemCount=this.m_iSharpsLogItemCount+1;
						if(this.m_iSharpsLogItemCount > objSharpLog.Items.Count)
						{
							objSharpLog = null;
							this.m_iSharpsLogItemCount = 1;
							this.m_iSharpsLogReportInProcess = this.m_iSharpsLogReportInProcess + 1;
						}
						if(iCurrentReport!=this.m_iSharpsLogReportInProcess)
						{
						
							DataDynamics.ActiveReports.Detail obj =((DataDynamics.ActiveReports.Detail)this.Sections["Detail"]);
							obj.NewPage=NewPage.After;	
						}
						else
						{
							DataDynamics.ActiveReports.Detail obj =((DataDynamics.ActiveReports.Detail)this.Sections["Detail"]);
							obj.NewPage=NewPage.None;	
						}
					}
				}
				else
				{

					m_sCompanyName=objSharpLog.Company.Name;
					m_datStartDate=objSharpLog.BeginDate;
					m_datEndDate=objSharpLog.EndDate;
					m_datPrintedOn=DateTime.Today;
					this.m_iSharpsLogReportInProcess = this.m_iSharpsLogReportInProcess + 1;
					this.Sections["Detail"].Controls["nodata"].Visible=true;
					this.Fields["nodata"].Value="(no data)";
					DataDynamics.ActiveReports.Detail obj =((DataDynamics.ActiveReports.Detail)this.Sections["Detail"]);
					obj.NewPage=NewPage.After;

				}
			}			
			catch(RMAppException p_objException)
			{
				p_objeArgs.EOF=true;
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				p_objeArgs.EOF=true;
				throw new RMAppException(Globalization.GetString("SharpsLogReport.SharpsLogReport_FetchData.Error",m_iClientId),p_objException);
			}
			finally
			{
				objSharpLog=null;
				objItem=null;
			}
		}

		/// <summary>
		/// This method would do the page settings before start printing the report 
		/// and invoke base class method to print the soft error log.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void SharpsLogReport_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			try
			{
                //MITS 10630 the user for cws may not have any defalut printer
                this.Document.Printer.PrinterName = string.Empty;
				this.PageSettings.PaperHeight=14;
				this.PageSettings.PaperWidth=(float)8.5;
				this.PageSettings.Orientation=DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
				base.SoftErrorLogPrint();
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SharpsLogReport.SharpsLogReport_ReportStart.Error",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would do the formatting for the Detail section of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void Detail_BeforePrint(object p_objsender, System.EventArgs p_objeArgs)
		{
			float temp=0.0f;
			temp=CurrentPage.Height;
			float maxHeight = 0.0f;
			try
			{
				for(int x = 0; x < this.Sections["Detail"].Controls.Count; x++)
				{
					if(this.Sections["Detail"].Controls[x].GetType().ToString() == "DataDynamics.ActiveReports.TextBox")
					{
						if(((DataDynamics.ActiveReports.TextBox) this.Sections["Detail"].Controls[x]).Height > maxHeight)
							maxHeight = ((DataDynamics.ActiveReports.TextBox) this.Sections["Detail"].Controls[x]).Height;
					}
				}
				//Assign Max Height to allow for the borders to grow correctly.
				for(int y = 0; y < this.Sections["Detail"].Controls.Count; y++)
				{
					if(this.Sections["Detail"].Controls[y].GetType().ToString() == "DataDynamics.ActiveReports.TextBox")
						((DataDynamics.ActiveReports.TextBox) this.Sections["Detail"].Controls[y]).Height = maxHeight;
				}
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SharpsLogReport.Detail_BeforePrint.Error",m_iClientId),p_objException);
			}
		}

		/// <summary>
		/// This method would make the control, in the Detail section for "No Data", as invisible if it is visible.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void Detail_AfterPrint(object p_objsender, System.EventArgs p_objeArgs)
		{
			if(this.Sections["Detail"].Controls["nodata"].Visible)
			{
				this.Sections["Detail"].Controls["nodata"].Visible=false;
			}
		}
		/// <summary>
		/// This method would clear the fields in the report.
		/// </summary>
		private void ClearFields()
		{
			try
			{
				this.Fields["SharpObj"].Value="";
				this.Fields["SharpBrand"].Value="";
				this.Fields["EvtNum"].Value="";
				this.Fields["Employee"].Value="";
				this.Fields["Occupation"].Value="";
				this.Fields["EvtDate"].Value="";
				this.Fields["EvtLocation"].Value="";
				this.Fields["EvtDescription"].Value="";
				this.Fields["nodata"].Value="";
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SharpsLogReport.ClearFields.Error",m_iClientId),p_objException);
			}
			
		}
	
		/// <summary>
		/// This method would set the fields in the page header of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void SharpsLogReport_PageStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			try
			{
				if(m_sCompanyName!="")
				{
					((Label)this.Sections["PageHeader"].Controls["Company"]).Text=m_sCompanyName;
				}
				else
				{
					((Label)this.Sections["PageHeader"].Controls["Company"]).Text="";
				}
				if(m_datStartDate!=DateTime.MinValue)
				{
					((Label)this.Sections["PageHeader"].Controls["StartDate"]).Text=Declarations.ConvertDateTimeToMmDdYyyy(m_datStartDate,m_iClientId);
				}
				else
				{
					((Label)this.Sections["PageHeader"].Controls["StartDate"]).Text="";
				}
				if(m_datEndDate!=DateTime.MinValue)
				{
					((Label)this.Sections["PageHeader"].Controls["EndDate"]).Text=Declarations.ConvertDateTimeToMmDdYyyy(m_datEndDate,m_iClientId);
				}
				else
				{
					((Label)this.Sections["PageHeader"].Controls["EndDate"]).Text="";
				}
				((Label)this.Sections["PageHeader"].Controls["PrintedOnDate"]).Text=Declarations.ConvertDateTimeToMmDdYyyy(DateTime.Today,m_iClientId);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SharpsLogReport.SharpsLogReport_PageStart.Error",m_iClientId),p_objException);
			}
		}
		#endregion

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Line Line18 = null;
		private DataDynamics.ActiveReports.Label Events = null;
		private DataDynamics.ActiveReports.Label EndDate = null;
		private DataDynamics.ActiveReports.Label Label13 = null;
		private DataDynamics.ActiveReports.Label StartDate = null;
		private DataDynamics.ActiveReports.Label Label12 = null;
		private DataDynamics.ActiveReports.Label Label11 = null;
		private DataDynamics.ActiveReports.Label Company = null;
		private DataDynamics.ActiveReports.Line Line15 = null;
		private DataDynamics.ActiveReports.Line Line14 = null;
		private DataDynamics.ActiveReports.Line Line13 = null;
		private DataDynamics.ActiveReports.Line Line12 = null;
		private DataDynamics.ActiveReports.Line Line11 = null;
		private DataDynamics.ActiveReports.Line Line10 = null;
		private DataDynamics.ActiveReports.Line Line9 = null;
		private DataDynamics.ActiveReports.Line Line8 = null;
		private DataDynamics.ActiveReports.Line Line7 = null;
		private DataDynamics.ActiveReports.Label Label9 = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.Label Label6 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.Label Label4 = null;
		private DataDynamics.ActiveReports.Label Label3 = null;
		private DataDynamics.ActiveReports.Label Label2 = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.Line Line2 = null;
		private DataDynamics.ActiveReports.Line Line1 = null;
		private DataDynamics.ActiveReports.Label PrintedOnDate = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox SharpObj = null;
		private DataDynamics.ActiveReports.TextBox SharpBrand = null;
		private DataDynamics.ActiveReports.TextBox EvtNum = null;
		private DataDynamics.ActiveReports.TextBox Employee = null;
		private DataDynamics.ActiveReports.TextBox Occupation = null;
		private DataDynamics.ActiveReports.TextBox EvtDate = null;
		private DataDynamics.ActiveReports.TextBox EvtLocation = null;
		private DataDynamics.ActiveReports.TextBox EvtDescription = null;
		private DataDynamics.ActiveReports.TextBox nodata = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		internal void InitializeReport()
		{
			try
			{
				this.LoadLayout(this.GetType(), "Riskmaster.Application.OSHALib.SharpsLogReport.rpx");
				this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
				this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
				this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
				this.Line18 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[0]));
				this.Events = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
				this.EndDate = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
				this.Label13 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
				this.StartDate = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
				this.Label12 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
				this.Label11 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
				this.Company = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
				this.Line15 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[8]));
				this.Line14 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[9]));
				this.Line13 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[10]));
				this.Line12 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[11]));
				this.Line11 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[12]));
				this.Line10 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[13]));
				this.Line9 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[14]));
				this.Line8 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[15]));
				this.Line7 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[16]));
				this.Label9 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[17]));
				this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
				this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[19]));
				this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[20]));
				this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[21]));
				this.Label3 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[22]));
				this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[23]));
				this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[24]));
				this.Line2 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[25]));
				this.Line1 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[26]));
				this.PrintedOnDate = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[27]));
				this.SharpObj = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
				this.SharpBrand = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
				this.EvtNum = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
				this.Employee = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
				this.Occupation = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
				this.EvtDate = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
				this.EvtLocation = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
				this.EvtDescription = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
				this.nodata = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[8]));
				// Attach Report Events
				this.DataInitialize += new System.EventHandler(this.SharpsLogReport_DataInitialize);
				this.FetchData += new FetchEventHandler(this.SharpsLogReport_FetchData);
				this.ReportStart += new System.EventHandler(this.SharpsLogReport_ReportStart);
				this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
				this.Detail.AfterPrint += new System.EventHandler(this.Detail_AfterPrint);
				this.PageStart += new System.EventHandler(this.SharpsLogReport_PageStart);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("SharpsLogReport.InitializeReport.Error",m_iClientId),p_objException);
			}
		}

		#endregion
	}
}
