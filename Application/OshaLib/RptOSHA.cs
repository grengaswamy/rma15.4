﻿
using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Text;                              //jtodd22 02/17/2011
using Riskmaster.Application.ZipUtil;
namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004
	/// Purpose :  This class contains the method that would fetch data from the database.
	///			   This data would be shown in the reports.
	/// </summary>

internal class RptOSHA
{
	#region Member Variables
	/// <summary>
	/// The connection string used by the database layer for connection with the database.
	/// </summary>
	protected  string m_sConnectString;
	/// <summary>
	/// Reader for reading body parts
	/// </summary>
    DbReader m_objReaderForBodyParts=null;
	/// <summary>
	/// Reader for reading Work Loss
	/// </summary>
	DbReader m_objReaderForWorkLoss=null;
	/// <summary>
	/// Reader for reading Restricted Days
	/// </summary>
	DbReader m_objReaderForRestrictedDays=null;
	/// <summary>
	/// Reader for reading Physician
	/// </summary>
	DbReader m_objReaderForLoadPhysician=null;
	/// <summary>
	/// Reader for reading Hospital
	/// </summary>
	DbReader m_objReaderForLoadHospital=null;
	/// <summary>
	/// Reader for reading Treatments
	/// </summary>
	DbReader m_objReaderForLoadTreatments=null;
	/// <summary>
	/// Reader for reading Injuries
	/// </summary>
	DbReader m_objReaderForLoadInjuries=null;
	/// <summary>
	/// Flag whether State Keeper closes reader or not
	/// </summary>
	bool m_bLoadInjuriesReaderClosedByStateKeeper=false;
	/// <summary>
	/// This is the object that keeps track of the states of various readers.
	/// </summary>
	StateInformerForDbReaders m_objStateKeeper=new StateInformerForDbReaders();
    private int m_iClientId = 0;
	#endregion

	#region Constructor
	/// <summary>
	/// Riskmaster.Application.OSHALib.RptOSHA is the default constructor.
	/// </summary>
    internal RptOSHA(int p_iClientId)
	{
        m_iClientId = p_iClientId;
	}
	/// <summary>
	/// Riskmaster.Application.OSHALib.RptOSHA is the constructor.
	/// </summary>
	/// <param name="p_sConnectString">Connection string for database connection.</param>
	internal RptOSHA(string p_sConnectString,int p_iClientId)
	{
		m_sConnectString = p_sConnectString;
        m_iClientId = p_iClientId;
	}
	#endregion

	#region Methods
	/// <summary>
	/// This method populates Osha Item.
	/// </summary>
	/// <param name="p_objRpt">This is the report whose Osha Items will be populated</param>
	/// <param name="p_sConnectString">Database connection string</param>
	/// <param name="p_bSharpsOnlyFlag">Boolean representing Osha sharp log report.</param>
	/// <returns>Sorted Array list of OSHA Item</returns>
	internal ArrayList FetchOshaData(IOSHAReport p_objRpt,string p_sConnectString, bool p_bSharpsOnlyFlag)
	{
		string sSql="";
		string sSqlFrom = "";		
		string sSqlWhere = "";  
		int iElapsedSeconds=0;
		Hashtable  hsItems=null;
		ArrayList arrlstOshaItems=new ArrayList();;
		OSHAItem objItem=null;
		int iRecordCount=0;
		string sTemp="" ;
		string sBodyParts ="";
		string sPrev="" ;
		string sCurr="" ;
		string sInDeptClause=""; //="(SELECT DISTINCT DEPARTMENT_EID FROM ORG_HIERARCHY  WHERE ORG_HIERARCHY.DEPARTMENT_EID IN  (60))";
		string sInPeopleInvolvedClause="";
		m_objStateKeeper.CloseReaderForBodyPart+=new Riskmaster.Application.OSHALib.StateInformerForDbReaders.Notification(m_objStateKeeper_CloseReaderForBodyPart);
		m_objStateKeeper.CloseReaderLoadInjuries+=new Riskmaster.Application.OSHALib.StateInformerForDbReaders.Notification(m_objStateKeeper_CloseReaderLoadInjuries);
		m_objStateKeeper.CloseReaderForWorkLoss+=new Riskmaster.Application.OSHALib.StateInformerForDbReaders.Notification(m_objStateKeeper_CloseReaderForWorkLoss);
        m_objStateKeeper.CloseReaderLoadHospital+=new Riskmaster.Application.OSHALib.StateInformerForDbReaders.Notification(m_objStateKeeper_CloseReaderLoadHospital);
		m_objStateKeeper.CloseReaderLoadPhysician+=new Riskmaster.Application.OSHALib.StateInformerForDbReaders.Notification(m_objStateKeeper_CloseReaderLoadPhysician);
		m_objStateKeeper.CloseReaderLoadTreatments+=new Riskmaster.Application.OSHALib.StateInformerForDbReaders.Notification(m_objStateKeeper_CloseReaderLoadTreatments);
		m_objStateKeeper.CloseReaderRestrictedDays+=new Riskmaster.Application.OSHALib.StateInformerForDbReaders.Notification(m_objStateKeeper_CloseReaderRestrictedDays);
		DbReader objReader=null;
		try
		{
			#region
		    Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			p_objRpt.StartTime=DateTime.Now;
			m_sConnectString=p_sConnectString;
			hsItems=new Hashtable();
			p_objRpt.StartTime = DateTime.Now;
			sInDeptClause = BuildDeptClause(p_objRpt);
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
                //MITS 37193 Begin
                //sSql = "SELECT EVENT.EVENT_ID, EVENT.DATE_OF_EVENT, EVENT.TIME_OF_EVENT, EVENT.DEPT_EID," +
                sSql = "SELECT EVENT.EVENT_ID, EVENT.DATE_OF_EVENT, EVENT.TIME_OF_EVENT, EVENT.DEPT_EID, EVENT.LOCATION_AREA_DESC," +
                //MITS 37193 End
			 " EVENT.EVENT_NUMBER, EVENT.EVENT_DESCRIPTION,EVENT.PRIMARY_LOC_CODE,"+
             " EVENT.ADDR1  EV_ADDR1, EVENT.ADDR2  EV_ADDR2, EVENT.ADDR3  EV_ADDR3,EVENT.ADDR4  EV_ADDR4, EVENT.CITY  EV_CITY,EVENT.STATE_ID" +
			 "  EV_STATE_ID, EVENT.ZIP_CODE  EV_ZIP_CODE, EV_DEPT_ENTITY.LAST_NAME  EV_DEPT_LAST_NAME, "+
             " EV_DEPT_ENTITY.ADDR1  EV_DEPT_ADDR1, EV_DEPT_ENTITY.ADDR2  EV_DEPT_ADDR2, EV_DEPT_ENTITY.ADDR3  EV_DEPT_ADDR3, EV_DEPT_ENTITY.ADDR4  EV_DEPT_ADDR4, EV_DEPT_ENTITY.CITY " +
			 "  EV_DEPT_CITY, EV_DEPT_ENTITY.STATE_ID  EV_DEPT_STATE_ID, EV_DEPT_ENTITY.ZIP_CODE "+
			 "  EV_DEPT_ZIP_CODE, PI_ENTITY.FIRST_NAME  PI_FIRST_NAME, PI_ENTITY.MIDDLE_NAME "+
			 "  PI_MIDDLE_NAME, PI_ENTITY.LAST_NAME  PI_LAST_NAME,  PI_ENTITY.ADDR1  PI_ADDR1,"+
             " PI_ENTITY.ADDR2  PI_ADDR2, PI_ENTITY.ADDR3  PI_ADDR3, PI_ENTITY.ADDR4  PI_ADDR4, PI_ENTITY.CITY  PI_CITY,  PI_ENTITY.STATE_ID  PI_STATE_ID, " +
			 " PI_ENTITY.ZIP_CODE  PI_ZIP_CODE, PI_ENTITY.BIRTH_DATE  PI_BIRTH_DATE, "+
			 " PI_ENTITY.SEX_CODE  PI_SEX_CODE, PERSON_INVOLVED.DEPT_ASSIGNED_EID, "+
			 " PERSON_INVOLVED.POSITION_CODE, PERSON_INVOLVED.DISABILITY_CODE, "+
			 " PERSON_INVOLVED.PI_ROW_ID, PERSON_INVOLVED.DATE_OF_DEATH,"+
			 " PERSON_INVOLVED.DATE_HIRED, PERSON_INVOLVED.WORKDAY_START_TIME, "+
			 " PERSON_INVOLVED.WORK_SUN_FLAG, PERSON_INVOLVED.WORK_MON_FLAG,  "+
			 " PERSON_INVOLVED.WORK_TUE_FLAG, PERSON_INVOLVED.WORK_WED_FLAG,  "+
			 " PERSON_INVOLVED.WORK_THU_FLAG, PERSON_INVOLVED.WORK_FRI_FLAG,  "+
			 " PERSON_INVOLVED.WORK_SAT_FLAG,"+
			 " PERSON_INVOLVED.OSHA_ACC_DESC , PERSON_INVOLVED.ILLNESS_CODE,"+
			 " EVENT_X_OSHA.*";


			sSqlFrom = " FROM EVENT, EVENT_X_OSHA, PERSON_INVOLVED, ENTITY PI_ENTITY, ENTITY EV_DEPT_ENTITY  ";

			sSqlWhere = " WHERE  EVENT.EVENT_ID = EVENT_X_OSHA.EVENT_ID AND    "+
			            " EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID AND     "+
			            " PERSON_INVOLVED.PI_EID = PI_ENTITY.ENTITY_ID AND  "+
			            " EVENT.DEPT_EID = EV_DEPT_ENTITY.ENTITY_ID ";

			if(p_objRpt.ByOSHAEstablishmentFlag)
			{//(1) is by OSHA Establishment
				sSqlWhere = sSqlWhere + " AND EVENT_X_OSHA.OSHA_ESTAB_EID IN " + sInDeptClause ;
				if(p_objRpt.BeginDate == DateTime.MinValue)
				{
					sSqlWhere=sSqlWhere+ "" ;
				}
				else
				{
					sSqlWhere=sSqlWhere+" AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(p_objRpt.BeginDate) + "'" ;
				}
				if(p_objRpt.EndDate == DateTime.MinValue)
				{
					sSqlWhere=sSqlWhere+ "" ;
				}
				else
				{
					sSqlWhere=sSqlWhere+" AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(p_objRpt.EndDate) + "'" ;
				}
			}
			else
			{//(2) is by employer
				sSqlWhere = sSqlWhere + " AND PERSON_INVOLVED.DEPT_ASSIGNED_EID IN " + sInDeptClause ;
				if(p_objRpt.BeginDate == DateTime.MinValue)
				{
					sSqlWhere=sSqlWhere+ "" ;
				}
				else
				{
					sSqlWhere=sSqlWhere+" AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(p_objRpt.BeginDate) + "'" ;
				}
				if(p_objRpt.EndDate == DateTime.MinValue)
				{
					sSqlWhere=sSqlWhere+ "" ;
				}
				else
				{
					sSqlWhere=sSqlWhere+" AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(p_objRpt.EndDate) + "'" ;
				}
			}
			sSqlWhere = sSqlWhere + " AND PERSON_INVOLVED.PI_TYPE_CODE = " + p_objRpt.PITypeEmployee;
			if(p_bSharpsOnlyFlag)
			{//Sharps log
				sSqlWhere = sSqlWhere + " AND EVENT_X_OSHA.SHARPS_OBJECT > 0";
			}

			if(!p_objRpt.EventBasedFlag)
			{
				 //Event based
				sSqlWhere = sSqlWhere + " AND PERSON_INVOLVED.OSHA_REC_FLAG <> 0";
			}   
			else
			{
				sSqlWhere = sSqlWhere + " AND EVENT_X_OSHA.RECORDABLE_FLAG <> 0";
			}

			sSql = sSql + sSqlFrom + sSqlWhere + " ORDER BY  PERSON_INVOLVED.PI_ROW_ID";
			try
			{
              objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
				while(objReader.Read())
				{
                  iRecordCount=iRecordCount+1;
				}
			}
			catch{}
			finally
			{
				if(objReader!=null)
				objReader.Close();
			}
			if(iRecordCount==0)
			{
				return arrlstOshaItems;
			}
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Incident Records",0,1,0);
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
            Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Incident Records",iElapsedSeconds,(iElapsedSeconds/(hsItems.Count!=0 ? hsItems.Count:1))*(iRecordCount-hsItems.Count),hsItems.Count/iRecordCount);
            sSql=sSql.Substring(sSql.IndexOf(" FROM "));
			sSql=sSql.Substring(0,sSql.IndexOf(" ORDER BY"));
			sInPeopleInvolvedClause="(SELECT DISTINCT PERSON_INVOLVED.PI_ROW_ID" +" " + sSql + " "+ ")" ;
			if(objReader!=null)
			{ 
				while(objReader.Read())
				{
					Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
					sPrev = sCurr;
					sCurr=Convert.ToString(objReader.GetInt("PI_ROW_ID"));
					if(sPrev!=sCurr)
					{
						objItem = new OSHAItem();
						objItem.EventId=objReader.GetInt("EVENT_ID");
						
						
                        //Raman Bhatia: We need to check OSHA Recordable flag of person involved
                        //MITS 14844
                        bool bIsEmployeeOshaRecordable = IsEmployeeOshaRecordable(sCurr);
                        if (bIsEmployeeOshaRecordable)
                            objItem.EmployeeOshaRecordable = true;
                        else
                            objItem.EmployeeOshaRecordable = false;

                        //MITS 19659 : Raman Bhatia: 02/01/2010
                        //if "Event Osha Recordable" checkbox is checked then all employees involved data should be visible in OSHA 301 report
                        //if "Event Osha Recordable" checkbox is unchecked the all employees involved whose "OSHA recordable" flag is set should be visible in OSHA 301 report.

                        if (p_objRpt.EventBasedFlag)
                        {
                            objItem.EmployeeOshaRecordable = true;
                        }
                        
                        objItem.CaseNumber=objReader.GetString("EVENT_NUMBER");
						objItem.TimeOfEvent= objReader.GetString("TIME_OF_EVENT").Trim()=="000000" ? "" : Declarations.GetPmAmTimeFromDbTimeFormat(objReader.GetString("TIME_OF_EVENT"));
						objItem.DateOfDeath=Conversion.ToDate(objReader.GetString("DATE_OF_DEATH"));
						objItem.ChkBoxDeath=objItem.DateOfDeath==DateTime.MinValue ? "NO": "YES";
						objItem.DateOfHire=Conversion.ToDate(objReader.GetString("DATE_HIRED"));
						objItem.EmployeeStartTime=Declarations.GetPmAmTimeFromDbTimeFormat(objReader.GetString("WORKDAY_START_TIME"));
                        objItem.EmployeeAddress = objReader.GetString("PI_ADDR1") + " " + objReader.GetString("PI_ADDR2") + " " + objReader.GetString("PI_ADDR3") + " " + objReader.GetString("PI_ADDR4");
						objItem.EmployeeCity=objReader.GetString("PI_CITY");
						objItem.EmployeeState=p_objRpt.Context.GetStateCode(objReader.GetInt("PI_STATE_ID"));
						objItem.EmployeeZipCode=objReader.GetString("PI_ZIP_CODE");
						objItem.EmployeeDateOfBirth=Conversion.ToDate(objReader.GetString("PI_BIRTH_DATE"));
						objItem.EmployeeSex=p_objRpt.Context.GetShortCode(objReader.GetInt("PI_SEX_CODE"));
						objItem.SortOn=objReader.GetString("PI_LAST_NAME")+objReader.GetString("PI_FIRST_NAME")+objReader.GetString("PI_MIDDLE_NAME")+objReader.GetString("EVENT_NUMBER");

						switch((SortOrder)p_objRpt.ItemSortOrder)
						{
							case SortOrder.SORT_EVENTDATE :
							{
								objItem.SortOn =objReader.GetString("DATE_OF_EVENT")+objReader.GetString("EVENT_NUMBER");
								break;
							}
							case SortOrder.SORT_EVENTNUMBER :
							{
								objItem.SortOn =objReader.GetString("EVENT_NUMBER")+objReader.GetString("PI_LAST_NAME")+objReader.GetString("PI_FIRST_NAME")+objReader.GetString("PI_MIDDLE_NAME");
								break;
							}
							case SortOrder.SORT_PERSONNAME :
							{
								break;
							}
						}
						objItem.EmployeeFullName=objReader.GetString("PI_FIRST_NAME")+" "+objReader.GetString("PI_MIDDLE_NAME")+" "+objReader.GetString("PI_LAST_NAME");
						objItem.EmployeeFullName=objItem.EmployeeFullName.Replace("  "," ");
						objItem.Occupation=p_objRpt.Context.GetCodeDesc(objReader.GetInt("POSITION_CODE"));
						if(objItem.Occupation.Trim()=="")
						{
							objItem.Occupation="(No Data)";
						}
						objItem.DateOfEvent=Conversion.ToDate(objReader.GetString("DATE_OF_EVENT"));
						if(p_objRpt.PrimaryLocationFlag)
						{
							objItem.EventLocation=p_objRpt.Context.GetCodeDesc(objReader.GetInt("PRIMARY_LOC_CODE"));
						}
						else
						{
							sTemp= objReader.GetString("EV_ADDR1")!="" ? "EV_" :"EV_DEPT_";
                            objItem.EventLocation = string.Empty;
							if(sTemp=="EV_DEPT_")
							{
                                objItem.EventLocation = objReader.GetString("EV_DEPT_LAST_NAME");
							}
                           // objItem.EventLocation = objItem.EventLocation;
                            string sAddress1 = objReader.GetString(sTemp + "ADDR1");
                            string sAddress2 = objReader.GetString(sTemp + "ADDR2");
                            string sAddress3 = objReader.GetString(sTemp + "ADDR3");
                            string sAddress4 = objReader.GetString(sTemp + "ADDR4");
                            string sCity = objReader.GetString(sTemp + "CITY");
                            string sStateId = p_objRpt.Context.GetStateCode(objReader.GetInt(sTemp + "STATE_ID")).Trim();
                            string sZipCode = objReader.GetString(sTemp+"ZIP_CODE");

                            if (sAddress1 != string.Empty)
                                objItem.EventLocation += ", " + sAddress1;

                            if (sAddress2 != string.Empty)
                                objItem.EventLocation += ", " + sAddress2;

                            if (sAddress3 != string.Empty)
                                objItem.EventLocation += ", " + sAddress3;

                            if (sAddress4 != string.Empty)
                                objItem.EventLocation += ", " + sAddress4;

                            if(sCity != string.Empty)
                                objItem.EventLocation += ", " + sCity;

                            if (sStateId != string.Empty)
                                objItem.EventLocation += ", " + sStateId;

                            if (sZipCode != string.Empty)
                                objItem.EventLocation += ", " + sZipCode;

                            if (objItem.EventLocation.Replace(",", "").Trim() == string.Empty)
                            {
                                objItem.EventLocation = "(no data)";
                            }
                            if((objItem.EventLocation.Substring(0,2)==", "))
                                objItem.EventLocation = objItem.EventLocation.Substring(2);
                            //objItem.EventLocation=objItem.EventLocation.Replace(", , ","");
                            //if(objItem.EventLocation.Substring(objItem.EventLocation.Length-1)==", ")
                            //{
                            //    objItem.EventLocation=objItem.EventLocation.Substring(0,objItem.EventLocation.Length-2);

                            //}
						}

                            //MITS 37193 Begins
                            switch ((ColumnESourceEnum)p_objRpt.ColumnESource)
                            {
                                case ColumnESourceEnum.SRC_E_PRIMARY_LOCATION:
                                    objItem.EventLocation = p_objRpt.Context.GetCodeDesc(objReader.GetInt("PRIMARY_LOC_CODE"));
                                    break;
                                case ColumnESourceEnum.SRC_E_LOCATION_DESCRIPTION: 
                                    objItem.EventLocation = objReader.GetString("LOCATION_AREA_DESC");
                                    objItem.EventLocation = Declarations.RemoveDateTimeStamp(objItem.EventLocation, m_iClientId);
                                    objItem.EventLocation = objItem.EventLocation.Replace(Environment.NewLine, " ");
                                    break;
                                case ColumnESourceEnum.SRC_E_DEPARTMENT_ADDRESS:
                                    objItem.EventLocation = string.Empty;
                                    sTemp = objReader.GetString("EV_ADDR1") != "" ? "EV_" : "EV_DEPT_";
                                    if (sTemp == "EV_DEPT_")
                                    {
                                        objItem.EventLocation = objReader.GetString("EV_DEPT_LAST_NAME");
                                    }

                                    string sAddress1 = objReader.GetString(sTemp + "ADDR1");
                                    string sAddress2 = objReader.GetString(sTemp + "ADDR2");
                                    string sAddress3 = objReader.GetString(sTemp + "ADDR3");
                                    string sAddress4 = objReader.GetString(sTemp + "ADDR4");
                                    string sCity = objReader.GetString(sTemp + "CITY");
                                    string sStateId = p_objRpt.Context.GetStateCode(objReader.GetInt(sTemp + "STATE_ID")).Trim();
                                    string sZipCode = objReader.GetString(sTemp+"ZIP_CODE");

                                    if (sAddress1 != string.Empty)
                                        objItem.EventLocation += ", " + sAddress1;

                                    if (sAddress2 != string.Empty)
                                        objItem.EventLocation += ", " + sAddress2;

                                    if (sAddress3 != string.Empty)
                                        objItem.EventLocation += ", " + sAddress3;

                                    if (sAddress4 != string.Empty)
                                        objItem.EventLocation += ", " + sAddress4;

                                    if(sCity != string.Empty)
                                        objItem.EventLocation += ", " + sCity;

                                    if (sStateId != string.Empty)
                                        objItem.EventLocation += ", " + sStateId;

                                    if (sZipCode != string.Empty)
                                        objItem.EventLocation += ", " + sZipCode;

                                    if (objItem.EventLocation.Replace(",", "").Trim() == string.Empty)
                                    {
                                        objItem.EventLocation = "(no data)";
                                    }
                                    if((objItem.EventLocation.Substring(0,2)==", "))
                                        objItem.EventLocation = objItem.EventLocation.Substring(2);
                                    //objItem.EventLocation = objItem.EventLocation + objReader.GetString(sTemp + "ADDR1") +
                                    //    objReader.GetString(sTemp + "ADDR2") + ", " +
                                    //    objReader.GetString(sTemp + "CITY") + ", " +
                                    //    p_objRpt.Context.GetStateCode(objReader.GetInt(sTemp + "STATE_ID")).Trim() + ", " +
                                    //    objReader.GetString(sTemp + "ZIP_CODE");
                                    //if (objItem.EventLocation == ", , , ")
                                    //{
                                    //    objItem.EventLocation = "(no data)";
                                    //}
                                    //objItem.EventLocation = objItem.EventLocation.Replace(", , ", "");
                                    //if (objItem.EventLocation.Substring(objItem.EventLocation.Length - 1) == ", ")
                                    //{
                                    //    objItem.EventLocation = objItem.EventLocation.Substring(0, objItem.EventLocation.Length - 2);

                                    //}
                                    break;
                            }
                            //MITS 37193 ends


                            


						objItem.ActivityWhenInjured= p_objRpt.PrintOSHADescFlag ? objReader.GetString("ACTIVITY_WHEN_INJ") : objReader.GetString("EVENT_DESCRIPTION");
						if(!p_objRpt.EventBasedFlag)
						{
							objItem.ActivityWhenInjured=objReader.GetString("OSHA_ACC_DESC");

						}
						objItem.ActivityWhenInjured=Declarations.RemoveDateTimeStamp(objItem.ActivityWhenInjured.Trim(),m_iClientId);
						objItem.DisabilityCode=p_objRpt.Context.GetShortCode(objReader.GetInt("DISABILITY_CODE")).Trim();
						if(objItem.DisabilityCode.ToUpper()!="INJ" && objItem.DisabilityCode.ToUpper()!="ILL")
						{
							p_objRpt.Errors.Add("FetchOshaData",SoftErrFormat(objItem.CaseNumber,"Unknown Disability Code:'" + objItem.DisabilityCode + " '  (Expected 'ILL' or 'INJ')"),m_iClientId);
						}
						objItem.PiRowId=Convert.ToString(objReader.GetInt("PI_ROW_ID"));
						objItem.IllnessDesc=p_objRpt.Context.GetCodeDesc(objReader.GetInt("ILLNESS_CODE"));
						RecordIllnessAs(ref objItem,p_objRpt.Context.GetSingleString("PARENT.SHORT_CODE", "CODES CHILD, CODES PARENT", "CHILD.CODE_ID =" + objReader.GetInt("ILLNESS_CODE") + " AND CHILD.RELATED_CODE_ID = PARENT.CODE_ID"));
						if(m_objStateKeeper.CanReaderReadBodyParts)
						{
							sBodyParts=LoadBodyParts(ref objItem,ref p_objRpt,sInPeopleInvolvedClause,ref m_objReaderForBodyParts);
						}
						// Check for reader state is not implemented Intentionally, as this function has to be called everytime...
							LoadInjuries(ref objItem,ref p_objRpt,sInPeopleInvolvedClause,ref m_objReaderForLoadInjuries,ref m_bLoadInjuriesReaderClosedByStateKeeper);
						
						if(m_objStateKeeper.CanReaderReadLoadTreatments)
						{
							LoadTreatments(ref objItem,ref p_objRpt,sInPeopleInvolvedClause,ref m_objReaderForLoadTreatments);
						}
						objItem.DaysOfWeek=GetDaysOfWeek(ref objReader);
						if(m_objStateKeeper.CanReaderReadWorkLoss)
						{
							LoadWorkLoss(ref objItem,ref p_objRpt,sInPeopleInvolvedClause,ref m_objReaderForWorkLoss);
						}
						if(objItem.DaysWorkLoss==-1)
						{
							p_objRpt.Errors.Add("FetchOshaData",SoftErrFormat(objItem.CaseNumber,"Invalid date range for work loss."),m_iClientId);
						}
						if(m_objStateKeeper.CanReaderReadRestrictedDays)
						{
							LoadRestrictedDays(ref objItem,ref p_objRpt,sInPeopleInvolvedClause,ref m_objReaderForRestrictedDays);
						}
						if(objItem.DaysRestriction==-1)
						{
							p_objRpt.Errors.Add("FetchOshaData",SoftErrFormat(objItem.CaseNumber,"Invalid date range for work restriction."),m_iClientId);

						}
						if(p_objRpt.Enforce180DayRule && ((objItem.DaysWorkLoss + objItem.DaysRestriction) >= 180)) 
						{
							objItem.DaysWorkLoss = objItem.DaysWorkLoss < 180 ? objItem.DaysWorkLoss : 180;
							objItem.DaysRestriction = 180 - objItem.DaysWorkLoss;
						}
						if(m_objStateKeeper.CanReaderReadLoadPhysician)
						{
							LoadPhysician(ref objItem,ref p_objRpt,sInPeopleInvolvedClause,ref m_objReaderForLoadPhysician);
						}
						if(m_objStateKeeper.CanReaderReadLoadHospital)
						{
							LoadHospital(ref objItem,ref p_objRpt,sInPeopleInvolvedClause,ref m_objReaderForLoadHospital);
						}
						objItem.ObjectSubstanceThatInjured=Declarations.RemoveDateTimeStamp(objReader.GetString("OBJ_SUBST_THAT_INJ").Trim(),m_iClientId);
						objItem.HowEventOccurred=Declarations.RemoveDateTimeStamp(objReader.GetString("HOW_ACC_OCCURRED").Trim(),m_iClientId);
						if (objItem.InjuryDesc.Trim() != "")
						{
							objItem.InjuryDesc =Declarations.RemoveDateTimeStamp(objItem.InjuryDesc.Trim(),m_iClientId) + "; ";
						}
						//CODE ADDED BY YATHARTH - MITS 13979 - START
                        if (objItem.IllnessDesc.Trim() != "")
                        {
                            objItem.IllnessDesc = Declarations.RemoveDateTimeStamp(objItem.IllnessDesc.Trim(),m_iClientId) + "; ";
                        }

                        if (sBodyParts.Trim() != "" && objItem.InjuryDesc.Trim() != "")
						{
							objItem.InjuryDesc = objItem.InjuryDesc + Declarations.RemoveDateTimeStamp(sBodyParts.Trim(),m_iClientId) + "; ";
						}
                        else if (sBodyParts.Trim() != "" && objItem.IllnessDesc.Trim() != "")
                        {
                            objItem.IllnessDesc = objItem.IllnessDesc + Declarations.RemoveDateTimeStamp(sBodyParts.Trim(),m_iClientId) + "; ";
                        }
                        //YATHARTH - MITS 13979  - END
                        switch ((ColumnFSourceEnum)p_objRpt.ColumnFSource)
                        {
                            case ColumnFSourceEnum.SRC_DEFAULT:
                                break;
                            case ColumnFSourceEnum.SRC_EVENTDESC://Just use event description.
                                //rkulavil: RMA-16527 starts
                                //objItem.InjuryDesc = Declarations.RemoveDateTimeStamp(objReader.GetString("EVENT_DESCRIPTION").Trim(), m_iClientId);
                                objItem.IllnessDesc = objItem.InjuryDesc = Declarations.RemoveDateTimeStamp(objReader.GetString("EVENT_DESCRIPTION").Trim(), m_iClientId);
                                //rkulavil: RMA-16527 ends
                                break;
                            case ColumnFSourceEnum.SRC_OSHAACCDESC://Just use osha how acc. occurred field.
                                //rkulavil: RMA-16527 starts
                                //objItem.InjuryDesc = Declarations.RemoveDateTimeStamp(objItem.HowEventOccurred.Trim(), m_iClientId);
                                objItem.IllnessDesc = objItem.InjuryDesc = Declarations.RemoveDateTimeStamp(objItem.HowEventOccurred.Trim(), m_iClientId);
                                //rkulavil: RMA-16527 ends
                                break;
                            case ColumnFSourceEnum.SRC_OSHAHOWACC:
                                //rkulavil: RMA-16527 starts
                                //objItem.InjuryDesc = Declarations.RemoveDateTimeStamp(objReader.GetString("OSHA_ACC_DESC").Trim(), m_iClientId);
                                objItem.IllnessDesc = objItem.InjuryDesc = Declarations.RemoveDateTimeStamp(objReader.GetString("OSHA_ACC_DESC").Trim(), m_iClientId);
                                //rkulavil: RMA-16527 ends
                                break;
                        }
						if(objItem.InjuryDesc == "")
						{
							objItem.InjuryDesc = "(No Data: Selection=" + Convert.ToString(p_objRpt.ColumnFSource + 1) + " )";
						}
						if(p_objRpt.ColumnFSource==0 && (p_objRpt is OSHA300Report))//added this, to make the report look similar to vb reports.As printitem gets called every time and appends "--" with injurt desc.
						{
                          objItem.InjuryDesc=objItem.InjuryDesc+"--"+ objItem.ObjectSubstanceThatInjured;
						}

						objItem.PrivacyCase = objReader.GetInt32("PRIVACY_CASE_FLAG");
		  
						objItem.SharpsObjectId = objReader.GetInt32("SHARPS_OBJECT");
						objItem.SharpsMakeId = objReader.GetInt32("SHARPS_BRAND_MAKE");//filling of entity stops here
                        hsItems.Add("Key:" + objItem.PiRowId,objItem);
                    }
				    sBodyParts="";
					iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
					Declarations.UpdateProgress(p_objRpt.Notify,"Constructing Osha Incident (" + hsItems.Count + " of " + iRecordCount + ")",iElapsedSeconds,(iElapsedSeconds/(hsItems.Count!=0 ? hsItems.Count:1))*(iRecordCount-hsItems.Count),hsItems.Count/iRecordCount);
				}
				if(hsItems.Count>0)
				{
					ArrangeItemsInSortedOrder(ref hsItems,ref arrlstOshaItems);//Arrange the entities in sorted order..
				}
				
			}
			else
			{

				return arrlstOshaItems;
			}
	       
			#endregion
		
		}	
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
			throw new RMAppException(Globalization.GetString("RptOSHA.FetchOshaData.Error",m_iClientId),p_objException);
		}	
		finally
		{
			//clean up the readers
			if(m_objReaderForBodyParts!=null)
			{
                m_objReaderForBodyParts.Close();
				m_objReaderForBodyParts.Dispose();

			}
			if(m_objReaderForWorkLoss!=null)
			{
				m_objReaderForWorkLoss.Close();
				m_objReaderForWorkLoss.Dispose();
			}
			if(m_objReaderForLoadPhysician!=null)
			{
				m_objReaderForLoadPhysician.Close();
				m_objReaderForLoadPhysician.Dispose();

			}
			if(m_objReaderForLoadHospital!=null)
			{
				m_objReaderForLoadHospital.Close();
				m_objReaderForLoadHospital.Dispose();

			}
			if(m_objReaderForLoadInjuries!=null)
			{
				m_objReaderForLoadInjuries.Close();
				m_objReaderForLoadInjuries.Dispose();

			}
			if(m_objReaderForLoadTreatments!=null)
			{
				m_objReaderForLoadTreatments.Close();
				m_objReaderForLoadTreatments.Dispose();

			}
			if(m_objReaderForRestrictedDays!=null)
			{
				m_objReaderForRestrictedDays.Close();
				m_objReaderForRestrictedDays.Dispose();

			}
			if(hsItems!=null)
			{
				hsItems=null;
			}
			if(objReader!=null)
			{
				objReader.Close();
				objReader.Dispose();
			}
			//Clean up for state keeper..
			m_objStateKeeper=null;
			objItem=null;
			
		}
	return arrlstOshaItems;
	}
	
	/// <summary>
	/// This method will populate the company information.
	/// </summary>
	/// <param name="p_objCompany">Object of type EntityItem.</param>
	/// <param name="p_objRpt">Report item</param>
	internal void LoadReportOnCompanyData(EntityItem p_objCompany,IOSHAReport p_objRpt)
	{
		string sSql="";
		DbReader objReader=null;
		try
		{

			sSql = " SELECT " +
				" REPORT_ON_ENTITY.LAST_NAME  RPT_LAST_NAME, REPORT_ON_ENTITY.ABBREVIATION  RPT_ABBREVIATION," +
				" REPORT_ON_ENTITY.ADDR1  RPT_ADDR1, REPORT_ON_ENTITY.ADDR2  RPT_ADDR2," +
                " REPORT_ON_ENTITY.ADDR3  RPT_ADDR3, REPORT_ON_ENTITY.ADDR4  RPT_ADDR4," +
				" REPORT_ON_ENTITY.CITY  RPT_CITY,REPORT_ON_ENTITY.STATE_ID  RPT_STATE_ID," +
				" REPORT_ON_ENTITY.ZIP_CODE  RPT_ZIP_CODE,REPORT_ON_ENTITY.NATURE_OF_BUSINESS  RPT_NATURE_OF_BUSINESS," +
				" REPORT_ON_ENTITY.SIC_CODE  RPT_SIC_CODE," +
				" REPORT_ON_ENTITY.NAICS_CODE  RPT_NAICS_CODE" +            
				" FROM" +
				" ENTITY REPORT_ON_ENTITY" +
				" WHERE REPORT_ON_ENTITY.ENTITY_ID = " + p_objRpt.ReportOn;

			objReader= DbFactory.GetDbReader(m_sConnectString,sSql);

			if (objReader != null)
				if(objReader.Read())
				{
					p_objCompany.Name = objReader.GetString("RPT_LAST_NAME") + " " + objReader.GetString("RPT_ABBREVIATION");
                    p_objCompany.Address = objReader.GetString("RPT_ADDR1") + " " + objReader.GetString("RPT_ADDR2") + " " + objReader.GetString("RPT_ADDR3") + " " + objReader.GetString("RPT_ADDR4");
					p_objCompany.City = objReader.GetString("RPT_CITY");
                    p_objCompany.State = p_objRpt.Context.GetStateCode(Conversion.ConvertObjToInt64(objReader.GetValue("RPT_STATE_ID"), m_iClientId));
					p_objCompany.ZipCode = objReader.GetString("RPT_ZIP_CODE");
					p_objCompany.NatureOfBusiness = objReader.GetString("RPT_NATURE_OF_BUSINESS");
                    p_objCompany.SicCode = p_objRpt.Context.GetShortCode(Conversion.ConvertObjToInt64(objReader.GetValue("RPT_SIC_CODE"), m_iClientId));
                    p_objCompany.NaicsText = p_objRpt.Context.GetShortCode(Conversion.ConvertObjToInt64(objReader.GetValue("RPT_NAICS_CODE"), m_iClientId));
				}
            if (p_objRpt.EstablishmentNamePrefix.Trim() != "" && p_objRpt.EstablishmentNamePrefix.Trim() != "0")
			{
                p_objCompany.Name = p_objRpt.Context.GetSingleString("LAST_NAME", "ORG_HIERARCHY, ENTITY", p_objRpt.sReportLevel + " = " + p_objRpt.ReportOn + " AND ENTITY.ENTITY_ID = " + Declarations.OrgLevelToEIDColumn(Conversion.ConvertObjToInt(p_objRpt.EstablishmentNamePrefix,m_iClientId),m_iClientId)) + " ; " + p_objCompany.Name;
			}
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.LoadReportOnCompanyData.Error", m_iClientId), p_objException);
		}	
		finally
		{
			if(objReader!=null)
			{
				objReader.Close();
			    objReader.Dispose();
			}
		}
	}

    /// <summary>
    /// This method will check employee osha recordable flag.
    /// </summary>
    /// <param name="p_objCompany">Object of type EntityItem.</param>
    /// <param name="p_objRpt">Report item</param>
    internal bool IsEmployeeOshaRecordable(string p_sPiRowId)
    {
        string sSql = "";
        DbReader objReader = null;
        bool bRecordableFlag = false;
        try
        {

            sSql = " SELECT OSHA_REC_FLAG FROM PERSON_INVOLVED " +
                " WHERE PI_ROW_ID = " + p_sPiRowId;

            objReader = DbFactory.GetDbReader(m_sConnectString, sSql);

            if (objReader != null)
            {
                if (objReader.Read())
                {
                    bRecordableFlag = objReader.GetBoolean("OSHA_REC_FLAG");
                }
            }
        }
        catch (Exception p_objException)
        {
           
        }
        finally
        {
            if (objReader != null)
            {
                objReader.Close();
                objReader.Dispose();
            }
            
        }
        return bRecordableFlag;
    }

	/// <summary>
	/// This method records illness.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_sParentShortCode">Parent short code</param>
	internal  void RecordIllnessAs(ref OSHAItem p_objItem, string p_sParentShortCode)
	{
		try
		{
			if(p_objItem.DisabilityCode.ToUpper() != "ILL")
			{
				return;
			}
		
			switch(p_sParentShortCode.Trim())
			{
				case "01" :
					p_objItem.RecordAsSkinDisor = true;
					break;
				case "02" :
					p_objItem.RecordAsRespCond = true;
					break;
				case "03" :
					p_objItem.RecordAsPoison = true;
					break;
				case "04" :
					p_objItem.RecordAsPoison = true;
					break;
				case "05" :
					p_objItem.RecordAsOther = true;
					break;
				case "07" :
					p_objItem.RecordAsOther = true;
					break;
				case "SD" :
					p_objItem.RecordAsSkinDisor = true;
					break;
				case "HL" :
					if(p_objItem.DateOfEvent.CompareTo(Conversion.ToDate("20031231"))>0)
					{
						p_objItem.RecordAsHearLoss = true;
					}
					else
					{
						p_objItem.RecordAsOther = true;
					}
					break;
				case "MISC" :
					p_objItem.RecordAsOther = true;
					break;
				case "PO" :
					p_objItem.RecordAsPoison = true;
					break;
				case "RC" :
					p_objItem.RecordAsRespCond = true;
					break;
				default :
					p_objItem.RecordAsOther = true;
					break;
			}
		}		
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.RecordIllnessAs.Error", m_iClientId), p_objException);
		}	
	}	

	/// <summary>
	/// This method populates injury data.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_objRpt">Report containing the criteria.</param>
	/// <param name="p_sInPeopleInvolvedClause">People Involved Clause</param>
	/// <param name="p_objReader">Reader will contain the data for injuries.</param> 
	/// <param name="p_ClosedByStateKeeper">Flag whether State Keeper closes reader or not.</param>
	internal void LoadInjuries(ref OSHAItem p_objItem,ref IOSHAReport p_objRpt,string p_sInPeopleInvolvedClause,ref DbReader p_objReader,ref bool p_ClosedByStateKeeper)
	{
		int iElapsedSeconds=0; ;
		string  sRetVal = "";
		int iRowId = 0;
		try
		{
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Injury Info",iElapsedSeconds,0,0);

			
			if((p_objReader==null) && (!p_ClosedByStateKeeper))
			{
				string sSql = "";
				sSql = "SELECT PI_ROW_ID, CODES_TEXT.CODE_DESC,MAIN_CODE.CODE_ID " +  
					" FROM CODES MAIN_CODE, CODES_TEXT, PI_X_INJURY " +
					" WHERE PI_X_INJURY.INJURY_CODE = MAIN_CODE.CODE_ID" +
					" AND CODES_TEXT.CODE_ID = MAIN_CODE.CODE_ID" +
					" AND  PI_ROW_ID IN " + p_sInPeopleInvolvedClause + " ORDER BY PI_ROW_ID, MAIN_CODE.CODE_ID";
			
				p_objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
				m_objStateKeeper.CanReaderReadLoadInjuries=p_objReader.Read();
			}	
			if(p_objItem.DisabilityCode!="INJ")
			{
				return;
			}
		
			if(m_objStateKeeper.CanReaderReadLoadInjuries)
			{
                iRowId = p_objReader.GetInt("PI_ROW_ID");
			}
			while(m_objStateKeeper.CanReaderReadLoadInjuries && iRowId <= Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
			{
				if(iRowId ==Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
				{
                    Common.Utilities.AddDelimited(ref sRetVal, p_objReader.GetString("CODE_DESC"), null, m_iClientId);
				}
                m_objStateKeeper.CanReaderReadLoadInjuries=p_objReader.Read();
				if(m_objStateKeeper.CanReaderReadLoadInjuries)
				{
                    iRowId = p_objReader.GetInt("PI_ROW_ID");
				}
			}
				p_objItem.RecordAsInjury = true;
                p_objItem.InjuryDesc = sRetVal;
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.LoadInjuries.Error", m_iClientId), p_objException);
		}
	}
	
	
	/// <summary>
	/// This method populates the treatment information.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_objRpt">Report containing the criteria</param>
	/// <param name="p_sInPeopleInvolvedClause">People Involved Clause</param>
	/// <param name="p_objReader">Reader will contain the data for treatments</param>
	internal  void LoadTreatments(ref OSHAItem p_objItem,ref IOSHAReport p_objRpt,string p_sInPeopleInvolvedClause,ref DbReader p_objReader)
	{
		
		int iRowId = 0;
	    string sShortCode = "";
		int iElapsedSeconds=0 ;
		try
		{
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Treatment Info",iElapsedSeconds,0,0);
			if(p_objReader==null)
			{
				string sSql = "";
				sSql ="SELECT PI_ROW_ID, SHORT_CODE FROM PI_X_TREATMENT, CODES WHERE PI_X_TREATMENT.PI_ROW_ID IN " + p_sInPeopleInvolvedClause + "  AND PI_X_TREATMENT.TREATMENT_CODE=CODES.CODE_ID ORDER BY PI_ROW_ID";
			
				p_objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
				m_objStateKeeper.CanReaderReadLoadTreatments=p_objReader.Read();
			}		
			if (p_objReader != null)
			{
				if(m_objStateKeeper.CanReaderReadLoadTreatments)
				{
					iRowId=p_objReader.GetInt("PI_ROW_ID");
				}
		
				while((m_objStateKeeper.CanReaderReadLoadTreatments) && (iRowId <= Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId)))
				{
					if(iRowId ==Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
					{
						sShortCode = p_objReader.GetString("SHORT_CODE");
						switch(sShortCode)
						{
							case "3":
								p_objItem.EmergencyRoomTreatment = true;
								break;
							case "4":
								p_objItem.EmergencyRoomTreatment = true;
								p_objItem.HospitalizedOverNight = true;
								break;
							case "5":
								p_objItem.EmergencyRoomTreatment = true;
								p_objItem.HospitalizedOverNight = true;
								break;
							default :
								break;

						}
			
					}
					m_objStateKeeper.CanReaderReadLoadTreatments=p_objReader.Read();
					if(m_objStateKeeper.CanReaderReadLoadTreatments)
					{
						iRowId=p_objReader.GetInt("PI_ROW_ID");
					}
				}
			
			}
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.LoadTreatments.Error", m_iClientId), p_objException);
		}
	}
	/// <summary>
	/// This method populates the Body Part information.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_objRpt">Report containing the criteria</param>
	/// <param name="p_sInPeopleInvolvedClause">People Involved Clause</param>
	/// <param name="p_objReader">Reader will contain the data for body parts</param>
	///	<returns>String containing the Body parts description</returns>
	internal string LoadBodyParts(ref OSHAItem p_objItem,ref IOSHAReport p_objRpt,string p_sInPeopleInvolvedClause,ref DbReader p_objReader)
	{
		int iElapsedSeconds=0; ;
	    int iNumParts = 0;
		string sRetVal = "";
		int iRowId = 0;
		try
		{
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Body Part Info",iElapsedSeconds,0,0);
			if(p_objReader==null) 
			{
				string sSql = "";
				sSql = "SELECT PI_ROW_ID, CODE_DESC FROM CODES_TEXT, PI_X_BODY_PART " +
					" WHERE PI_ROW_ID IN " + p_sInPeopleInvolvedClause +
					" AND PI_X_BODY_PART.BODY_PART_CODE = CODES_TEXT.CODE_ID " +
					" ORDER BY PI_ROW_ID" ;
		
				p_objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
				m_objStateKeeper.CanReaderReadBodyParts=p_objReader.Read();
			}
			if (p_objReader != null)
			{
				if(m_objStateKeeper.CanReaderReadBodyParts)
				{
					iRowId=p_objReader.GetInt("PI_ROW_ID");
				}
				while(m_objStateKeeper.CanReaderReadBodyParts && iRowId<=Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
				{
					if(iRowId ==Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId)) 
					{
                        Common.Utilities.AddDelimited(ref sRetVal, p_objReader.GetString("CODE_DESC").Trim(), null, m_iClientId);
                         iNumParts = iNumParts + 1;      
							
					}
					m_objStateKeeper.CanReaderReadBodyParts=p_objReader.Read();
					if(m_objStateKeeper.CanReaderReadBodyParts)
					{
						iRowId=p_objReader.GetInt("PI_ROW_ID");
					}
						
				}
				
			}
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.LoadBodyParts.Error", m_iClientId), p_objException);
		}
		return sRetVal;

	}

	/// <summary>
	/// This method populates the Work Loss information.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_objRpt">Report containing the criteria</param>
	/// <param name="p_sInPeopleInvolvedClause">People Involved Clause</param>
	/// <param name="p_objReader">Reader will contain the data for work loss</param>	
	internal  void LoadWorkLoss(ref OSHAItem p_objItem,ref IOSHAReport p_objRpt,string p_sInPeopleInvolvedClause,ref DbReader p_objReader)
	{
		int iElapsedSeconds ;
		int iDaysOff=0 ;
		int iRowId=0;
		DateTime datAsOfDate;
		string sAsOfDateDTG = "";
		string sDateLastWorkedDTG="";
		string sDateReturnedDTG = "";
		try
		{
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Work Loss Info",iElapsedSeconds,0,0);
			if(p_objReader==null)
			{
                StringBuilder sbSQL = new StringBuilder();          //jtodd22 02/17/2011
				sbSQL.Append("SELECT");
                //bpaskova 10/9/2015 JIRA 7261
                //sbSQL.Append(" PI_ROW_ID, PI_X_WORK_LOSS.DATE_RETURNED, PI_X_WORK_LOSS.DATE_LAST_WORKED, PI_X_WORK_LOSS.DURATION");
                sbSQL.Append(" PI_ROW_ID, PI_X_WORK_LOSS.DATE_RETURNED, PI_X_WORK_LOSS.DATE_LAST_WORKED, PI_X_WORK_LOSS.DURATION, PI_X_WORK_LOSS.RELEASED_RETURN_DATE");
                sbSQL.Append(" FROM PI_X_WORK_LOSS");
                sbSQL.Append(" WHERE PI_X_WORK_LOSS.PI_ROW_ID IN " + p_sInPeopleInvolvedClause + " ORDER BY PI_ROW_ID");
				p_objReader = DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString());
				m_objStateKeeper.CanReaderReadWorkLoss=p_objReader.Read();
                sbSQL = null;
			}
		
			if(p_objRpt.AsOfDate==DateTime.MinValue)
			{
				datAsOfDate=DateTime.Today;
				sAsOfDateDTG=Conversion.ToDbDate(datAsOfDate);
			}
			else
			{
				datAsOfDate=p_objRpt.AsOfDate;
				sAsOfDateDTG=Conversion.ToDbDate(datAsOfDate);
			}

			if (p_objReader != null)
			{
			
				if(m_objStateKeeper.CanReaderReadWorkLoss)
					iRowId=p_objReader.GetInt("PI_ROW_ID");
		
				while((m_objStateKeeper.CanReaderReadWorkLoss) && (iRowId <= Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId)))
				{
				
					sDateLastWorkedDTG =p_objReader.GetString("DATE_LAST_WORKED");
					if(sDateLastWorkedDTG.CompareTo(Conversion.ToDbDate(p_objItem.DateOfEvent)) < 0)
					{
						p_objRpt.Errors.Add("FetchOshaData",SoftErrFormat(p_objItem.CaseNumber,"'Date Lasted Worked' is before 'Date of Event', 'Date Lasted Worked' was changed for Calculation Only"),m_iClientId);
						sDateLastWorkedDTG=Conversion.ToDbDate(p_objItem.DateOfEvent);
					}
					if(sDateLastWorkedDTG.CompareTo(sAsOfDateDTG) <=0)
					{
						sDateReturnedDTG = p_objReader.GetString("DATE_RETURNED");
                        //bpaskova 10/9/2015 JIRA 7261 start
                        string sDateReleasedReturnDTG = p_objReader.GetString("RELEASED_RETURN_DATE");
                        if (!sDateReleasedReturnDTG.Equals(""))
                        {
                            sDateReturnedDTG = sDateReleasedReturnDTG;
                        }
                        //bpaskova 10/9/2015 JIRA 7261 end
						if((sDateReturnedDTG.CompareTo(sDateLastWorkedDTG)>0) || sDateReturnedDTG.Trim()=="")
						{
							if(iRowId ==Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
							{
								if(Conversion.ToDbDate(p_objItem.DateOfEvent).CompareTo(Declarations.WORK_LOSS_RULE_CHANGE_DATE) > 0)
								{
                                    //bpaskova 10/9/2015 JIRA 7261
                                    //if(p_objReader.GetString("DATE_RETURNED")!="")  
                                    if (sDateReturnedDTG != "")
                                    {
                                        //jtodd22, 02/17/2011, MITS 23694
                                        // akaushik5 Changed for MITS 38284 Starts
                                        //if (string.IsNullOrEmpty(p_objRpt.AsOfDate.ToString()))
                                        // bpaskova MITS 38495 added third condition
                                        if (string.IsNullOrEmpty(p_objRpt.AsOfDate.ToString()) || p_objRpt.AsOfDate.Equals(DateTime.MinValue) || sDateReturnedDTG.CompareTo(sAsOfDateDTG) <= 0)
                                        // akaushik5 Changed for MITS 38284 Ends
                                        {
                                            //bpaskova 10/9/2015 JIRA 7261
                                            //iDaysOff = iDaysOff + Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateLastWorkedDTG), Conversion.ToDate(p_objReader.GetString("DATE_RETURNED")), m_iClientId) - 1;
                                            iDaysOff = iDaysOff + Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateLastWorkedDTG), Conversion.ToDate(sDateReturnedDTG), m_iClientId) - 1;
                                        }
                                        else
                                        {
                                            iDaysOff = iDaysOff + Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateLastWorkedDTG), datAsOfDate, m_iClientId);
                                        }
									}
									else
									{
										
										iDaysOff= iDaysOff+Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateLastWorkedDTG),datAsOfDate,m_iClientId);
									
									}
								}
								else
								{
									if(Convert.ToString(p_objReader.GetInt32("DURATION")).CompareTo("")>0)
									{
										iDaysOff = iDaysOff +p_objReader.GetInt32("DURATION");
									}
									else
									{
										iDaysOff = iDaysOff + this.GetDaysOffWork(p_objReader.GetString("DATE_LAST_WORKED"),Conversion.ToDbDate(p_objRpt.AsOfDate),p_objItem.DaysOfWeek);
									}                                
								}
							}
						}
                        //bpaskova JIRA RMA-11751 start
                        //m_objStateKeeper.CanReaderReadWorkLoss=p_objReader.Read();
                        //if(m_objStateKeeper.CanReaderReadWorkLoss)
                        //{
                        //    iRowId=p_objReader.GetInt("PI_ROW_ID");
						
                        //}
					}
                    m_objStateKeeper.CanReaderReadWorkLoss = p_objReader.Read();
                    if (m_objStateKeeper.CanReaderReadWorkLoss)
                    {
                        iRowId = p_objReader.GetInt("PI_ROW_ID");

                    }
                    //bpaskova JIRA RMA-11751 end	
				}
				p_objItem.DaysWorkLoss=iDaysOff;
				if(iDaysOff>0)
				{
					p_objItem.ChkBoxWorkLoss="YES";
				}
			}
		}
		catch
		{
			p_objRpt.Errors.Add("LoadWorkLoss",SoftErrFormat(p_objItem.CaseNumber,"Work Loss Days calculation failed due to bad WorkLoss record(s) for " + p_objItem.EmployeeFullName + "."),m_iClientId);
		}
	}
	
	/// <summary>
	/// This method populates the restricted  days information.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_objRpt">Report containing the criteria</param>
	/// <param name="p_sInPeopleInvolvedClause">People Involved Clause</param>
	/// <param name="p_objReader">Reader will contain the data for restricted days</param>
	internal  void LoadRestrictedDays(ref OSHAItem p_objItem,ref IOSHAReport p_objRpt,string p_sInPeopleInvolvedClause,ref DbReader p_objReader)
	{
		int iDaysRestrict=0 ;
		int iRowId=0;
		string sDateFirstRestrictDTG ;        
		string sDateLastRestrictDTG ;     
		DateTime datAsOfDate ;          
		string sAsOfDateDTG ;
		int iElapsedSeconds=0;
		
		try
		{
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Restriction Info",iElapsedSeconds,0,0);
			if(p_objReader==null)
			{
				string sSql="";				
				sSql = "SELECT PI_ROW_ID," +
					" PI_X_RESTRICT.DATE_FIRST_RESTRCT" +
					", PI_X_RESTRICT.DATE_LAST_RESTRCT" +
					", PI_X_RESTRICT.DURATION" +
					" FROM PI_X_RESTRICT WHERE PI_ROW_ID IN " + p_sInPeopleInvolvedClause + " ORDER BY PI_ROW_ID";
				p_objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
				m_objStateKeeper.CanReaderReadRestrictedDays=p_objReader.Read();

			}
			if(p_objRpt.AsOfDate==DateTime.MinValue)
			{
				datAsOfDate=DateTime.Today;
				sAsOfDateDTG=Conversion.ToDbDate(datAsOfDate);
			}
			else
			{
				datAsOfDate=p_objRpt.AsOfDate;
				sAsOfDateDTG=Conversion.ToDbDate(datAsOfDate);
			}
			if(p_objReader!=null)
			{
				if(m_objStateKeeper.CanReaderReadRestrictedDays)
				{
					iRowId=p_objReader.GetInt("PI_ROW_ID");
				}
				while((m_objStateKeeper.CanReaderReadRestrictedDays) && (iRowId <= Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId)))
				{
					if(iRowId ==Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
					{
						sDateFirstRestrictDTG=p_objReader.GetString("DATE_FIRST_RESTRCT").Trim();
						if((Declarations.GetIntValFromString(sDateFirstRestrictDTG,m_iClientId) )<= (Declarations.GetIntValFromString(Conversion.ToDbDate(p_objItem.DateOfEvent),m_iClientId)))
						{
							p_objRpt.Errors.Add("LoadRestrictedDays",SoftErrFormat(p_objItem.CaseNumber,"'Date First Restricted' is before 'Date of Event', 'Date First Restricted' was changed for Calculation Only"),m_iClientId);
							sDateFirstRestrictDTG=Conversion.ToDbDate(p_objItem.DateOfEvent.AddDays(1));
						}
						sDateLastRestrictDTG=p_objReader.GetString("DATE_LAST_RESTRCT").Trim();
						if((Conversion.ToDate(sDateLastRestrictDTG))!=DateTime.MinValue)
						{
							if((Declarations.GetIntValFromString(sDateLastRestrictDTG,m_iClientId)) < (Declarations.GetIntValFromString(sDateFirstRestrictDTG,m_iClientId)))
							{
								p_objRpt.Errors.Add("LoadRestrictedDays",SoftErrFormat(p_objItem.CaseNumber,"'Date First Restricted' is after 'Date Last Restricted', Record was not used for Calculation"),m_iClientId);
								sDateFirstRestrictDTG=Conversion.ToDbDate(Conversion.ToDate(sAsOfDateDTG).AddDays(10000));
							}
							 
							if((Declarations.GetIntValFromString(sDateFirstRestrictDTG,m_iClientId)) <= (Declarations.GetIntValFromString(sAsOfDateDTG,m_iClientId)))
							{
								if((Declarations.GetIntValFromString(Conversion.ToDbDate(p_objItem.DateOfEvent),m_iClientId)) >(Declarations.GetIntValFromString(Declarations.WORK_LOSS_RULE_CHANGE_DATE,m_iClientId)))
								{
									if((p_objReader.GetString("DATE_LAST_RESTRCT"))!= "")
									{
                                        //jtodd22, 02/17/2011, MIT S23694
                                        // akaushik5 Changed for MITS 38284 Starts
                                        //if (p_objRpt.AsOfDate.ToString().Length != 8)
                                        // bpaskova MITS 38495 added second condition
                                        if (p_objRpt.AsOfDate.Equals(DateTime.MinValue) || sDateLastRestrictDTG.CompareTo(sAsOfDateDTG) <= 0)
                                        // akaushik5 Changed for MITS 38284 Ends
                                        {
                                            iDaysRestrict = iDaysRestrict + Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateFirstRestrictDTG), Conversion.ToDate(sDateLastRestrictDTG),m_iClientId) + 1;
                                        }
										else
										{
											iDaysRestrict=iDaysRestrict+Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateFirstRestrictDTG),datAsOfDate,m_iClientId)+1;
										}
									}
									else
									{
										iDaysRestrict=iDaysRestrict+Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateFirstRestrictDTG),datAsOfDate,m_iClientId)+1;
									}
								}
								else
								{
									if(Convert.ToString(p_objReader.GetInt32("DURATION")).CompareTo("")>0)
									{
                                     iDaysRestrict=iDaysRestrict+p_objReader.GetInt32("DURATION");
									}
									else
									{
										iDaysRestrict = iDaysRestrict + this.GetDaysOffWork(p_objReader.GetString("DATE_FIRST_RESTRCT"),Conversion.ToDbDate(DateTime.Today),p_objItem.DaysOfWeek);
									}
						
								}
							}	
							
						}
                            //MITS 37193
                            else
                            {
                                iDaysRestrict = iDaysRestrict + Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sDateFirstRestrictDTG), datAsOfDate, m_iClientId) + 1;

                            }

					}
					m_objStateKeeper.CanReaderReadRestrictedDays=p_objReader.Read();
					if(m_objStateKeeper.CanReaderReadRestrictedDays)
					{
						iRowId=p_objReader.GetInt("PI_ROW_ID");
						
					}
				}
				if(iDaysRestrict>0)
				{
					p_objItem.ChkBoxRestriction = "YES";
				}
				else
				{
					p_objItem.ChkBoxRestriction = "NO";
				}
				p_objItem.DaysRestriction=iDaysRestrict;
			
			}
		}
		catch
		{
			p_objRpt.Errors.Add("LoadRestrictedDays",SoftErrFormat(p_objItem.CaseNumber,"LoadRestrictedDays failed for people involved " + p_sInPeopleInvolvedClause + "."),m_iClientId);
			p_objRpt.Errors.Add("LoadRestrictedDays",SoftErrFormat(p_objItem.CaseNumber,"LoadRestrictedDays calculation failed due to bad restriction record(s) for " + p_objItem.EmployeeFullName + "."),m_iClientId);
		}
	}

	/// <summary>
	/// This method populates the Physician information.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_objRpt">Report containing the criteria</param>
	/// <param name="p_sInPeopleInvolvedClause">People Involved Clause</param>
	/// <param name="p_objReader">Reader will contain the data for physician</param>
	internal void LoadPhysician(ref OSHAItem p_objItem,ref IOSHAReport p_objRpt,string p_sInPeopleInvolvedClause,ref DbReader p_objReader)
	{
		
		int iRowId=0;
		int iElapsedSeconds=0;
		try
		{
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Physician Info",iElapsedSeconds,0,0);
			if(p_objReader==null)
			{
				string sSql="";				
				sSql = " SELECT PI_ROW_ID," +
				" PHYS_ENTITY.FIRST_NAME  PHYS_FIRST_NAME,PHYS_ENTITY.MIDDLE_NAME  PHYS_MIDDLE_NAME," +
				" PHYS_ENTITY.LAST_NAME  PHYS_LAST_NAME, PHYS_ENTITY.ADDR1  PHYS_ADDR1," +
                " PHYS_ENTITY.ADDR2  PHYS_ADDR2, PHYS_ENTITY.ADDR3  PHYS_ADDR3, PHYS_ENTITY.ADDR4  PHYS_ADDR4, PHYS_ENTITY.CITY  PHYS_CITY, PHYS_ENTITY.STATE_ID " +
				" PHYS_STATE_ID, PHYS_ENTITY.ZIP_CODE  PHYS_ZIP_CODE FROM" +
				" PI_X_PHYSICIAN, ENTITY PHYS_ENTITY" +
				" WHERE PI_ROW_ID IN " + p_sInPeopleInvolvedClause +
				" AND PHYS_ENTITY.ENTITY_ID = PI_X_PHYSICIAN.PHYSICIAN_EID ORDER BY PI_ROW_ID";
				p_objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
				m_objStateKeeper.CanReaderReadLoadPhysician=p_objReader.Read();
			}
			if(p_objReader!=null)
			{
				if(m_objStateKeeper.CanReaderReadLoadPhysician)
				{
					iRowId=p_objReader.GetInt("PI_ROW_ID");
				}
				while((m_objStateKeeper.CanReaderReadLoadPhysician) && (iRowId <=Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId)))
				{
					if(iRowId ==Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
					{
						p_objItem.HealthCareProf=p_objReader.GetString("PHYS_FIRST_NAME") + " " + p_objReader.GetString("PHYS_MIDDLE_NAME") + " " +p_objReader.GetString("PHYS_LAST_NAME");
						return;
					}
					m_objStateKeeper.CanReaderReadLoadPhysician=p_objReader.Read();
					if(m_objStateKeeper.CanReaderReadLoadPhysician)
					{
						iRowId=p_objReader.GetInt("PI_ROW_ID");
					}
				}					
			}
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.LoadPhysician.Error", m_iClientId), p_objException);
		}
	}
	/// <summary>
	/// This method populates the Hospital information.
	/// </summary>
	/// <param name="p_objItem">Osha Item to be populated</param>
	/// <param name="p_objRpt">Report containing the criteria</param>
	/// <param name="p_sInPeopleInvolvedClause">People Involved Clause</param>
	/// <param name="p_objReader">Reader will contain the data for hospital</param>
	internal void LoadHospital(ref OSHAItem p_objItem,ref IOSHAReport p_objRpt,string p_sInPeopleInvolvedClause,ref DbReader p_objReader)
	{
	
		int iRowId=0;
		int iElapsedSeconds=0;
		try
		{
			Declarations.CheckAbort(p_objRpt.Notify,m_iClientId);
			iElapsedSeconds=Declarations.DateDiff(p_objRpt.StartTime,m_iClientId);
			Declarations.UpdateProgress(p_objRpt.Notify,"Retrieving Osha Hospital Info",iElapsedSeconds,0,0);
			if(p_objReader==null)
			{
				string sSql="";
				 sSql = " SELECT  PI_ROW_ID, HOSP_ENTITY.LAST_NAME  HOSP_LAST_NAME, HOSP_ENTITY.ADDR1  HOSP_ADDR1," +
                 " HOSP_ENTITY.ADDR2  HOSP_ADDR2, HOSP_ENTITY.ADDR3  HOSP_ADDR3, HOSP_ENTITY.ADDR4  HOSP_ADDR4, HOSP_ENTITY.CITY  HOSP_CITY, HOSP_ENTITY.STATE_ID" +
                 "  HOSP_STATE_ID, HOSP_ENTITY.ZIP_CODE  HOSP_ZIP_CODE FROM" +
                 " PI_X_HOSPITAL, ENTITY HOSP_ENTITY" +
                 " WHERE PI_ROW_ID IN " + p_sInPeopleInvolvedClause +
                 " AND HOSP_ENTITY.ENTITY_ID = PI_X_HOSPITAL.HOSPITAL_EID ORDER BY PI_ROW_ID" ;
				 p_objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
				 m_objStateKeeper.CanReaderReadLoadHospital=p_objReader.Read();
			}
			if(p_objReader!=null)
			{
				if(m_objStateKeeper.CanReaderReadLoadHospital)
				{
					iRowId=p_objReader.GetInt("PI_ROW_ID");
				}
				while((m_objStateKeeper.CanReaderReadLoadHospital) && (iRowId <=Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId)))
				{
					if(iRowId ==Declarations.GetIntValFromString(p_objItem.PiRowId,m_iClientId))
					{
						 p_objItem.HealthCareEntityFacility = p_objReader.GetString("HOSP_LAST_NAME");
                         p_objItem.HealthCareEntityAddress = p_objReader.GetString("HOSP_ADDR1") + " " + p_objReader.GetString("HOSP_ADDR2") + " " + p_objReader.GetString("HOSP_ADDR3") + " " + p_objReader.GetString("HOSP_ADDR4");
                         p_objItem.HealthCareEntityCity = p_objReader.GetString("HOSP_CITY");
                         p_objItem.HealthCareEntityState = (p_objRpt.Context.GetStateCode(p_objReader.GetInt("HOSP_STATE_ID")));
                         p_objItem.HealthCareEntityZipCode = p_objReader.GetString("HOSP_ZIP_CODE");
						return;
					}
					m_objStateKeeper.CanReaderReadLoadHospital=p_objReader.Read();
					if(m_objStateKeeper.CanReaderReadLoadHospital)
					{
						iRowId=p_objReader.GetInt("PI_ROW_ID");
					}
				}					
			}
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.LoadHospital.Error", m_iClientId), p_objException);
		}
	}
	/// <summary>
	/// This method populates the Exposure information.
	/// </summary>	
	/// <param name="p_objRpt">OSHA 300A Report</param>	
	/// <returns>True (on successful completion)/False</returns>
	
	//TODO This function has not been tested because of lack of data.
	internal bool LoadExposureInfo3(OSHA300AReport p_objRpt)
	{
		bool bRetVal=false;
		bool bPresent ;
		bool bHave24Pays ;
		bool bHave26Pays ;
		int iDays=0 ;
		int iPayRange=0 ;
		int iTotalHours =0;
		int iTotalPersons =0;
		string sEndDate="";
		string sStartDate;
		ArrayList colPayRanges=null;
	    ColExposure colExposure =null;
		DbReader objReader=null;
        string sSql="";
		int iCount=0;
		int iCountInner=0;
		
		try
		{
			colExposure=new ColExposure(m_iClientId);
			p_objRpt.ExposureAvgEmployeeCount=0;
			p_objRpt.ExposureTotalHoursWorked=0;

			 sSql = "SELECT DISTINCT" +
					" ENTITY_EXPOSURE.EXPOSURE_ROW_ID" +
					", ENTITY_EXPOSURE.NO_OF_EMPLOYEES" +
					", ENTITY_EXPOSURE.NO_OF_WORK_HOURS" +
					", ENTITY_EXPOSURE.ENTITY_ID" +
					", ENTITY_EXPOSURE.START_DATE" +
					", ENTITY_EXPOSURE.END_DATE";

			if(p_objRpt.ByOSHAEstablishmentFlag!=true)
			{
				sSql = sSql + " FROM ENTITY_EXPOSURE, ORG_HIERARCHY" +
						" WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " + BuildDeptClause(p_objRpt) +
						" AND ENTITY_EXPOSURE.ENTITY_ID = ORG_HIERARCHY.DEPARTMENT_EID";
			}
			else
			{
				sSql = sSql + " FROM ENTITY_EXPOSURE" +
					" WHERE  ENTITY_EXPOSURE.ENTITY_ID IN " + BuildDeptClause(p_objRpt);
			}
			 sSql = sSql + " AND ENTITY_EXPOSURE.START_DATE >= '" + Conversion.ToDbDate(p_objRpt.BeginDate) + "'" +
					" AND ENTITY_EXPOSURE.END_DATE <= '" + Conversion.ToDbDate(p_objRpt.EndDate) + "'" +
					" AND ENTITY_EXPOSURE.NO_OF_EMPLOYEES IS NOT NULL" +
					" AND ENTITY_EXPOSURE.NO_OF_WORK_HOURS IS NOT NULL";

			 sSql = sSql + " ORDER BY ENTITY_EXPOSURE.START_DATE";
			 bHave24Pays = false;
             bHave26Pays = false;

             objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
		
			if(objReader!=null)
			{
				while(objReader.Read())
				{
					iDays=Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(objReader.GetString("START_DATE")),Conversion.ToDate(objReader.GetString("END_DATE")),m_iClientId)+1;
					if((iDays > 88) && (iDays < 94))
					{
						iDays=90;
					}
					if((iDays > 27) && (iDays < 32))
					{
						iDays=30;
					}
					if((iDays == 13) || (iDays == 15) || (iDays == 16))
					{
						iDays=15;
                         bHave24Pays = true;
					}
					           
					if(Conversion.ToDate(objReader.GetString("END_DATE")).Year == 0229 && iDays == 14 && bHave24Pays == true) 
					{
						if(bHave26Pays==false)
						{
							iDays=15;
						}
						else
						{
							//check this line of code.....not correct...!!!!
                            if(objReader.GetString("END_DATE").Substring(objReader.GetString("END_DATE").Length-3).IndexOf("2004,2020,2032")==0)
								iDays=15;
						}
                       
					}

                    if (((Declarations.GetIntValFromString(objReader.GetString("END_DATE"), m_iClientId)) < (Declarations.GetIntValFromString(Conversion.ToDate(objReader.GetString("END_DATE")).Year.ToString() + "0228", m_iClientId))) && iDays == 14) 
					{
						bHave26Pays = true;
					}
					
					colExposure.Add(iDays,Conversion.ToDate(objReader.GetString("END_DATE")), Conversion.ToDate(objReader.GetString("START_DATE")), objReader.GetInt("NO_OF_WORK_HOURS"), objReader.GetInt("NO_OF_EMPLOYEES"), objReader.GetInt("EXPOSURE_ROW_ID"));

				}
				colPayRanges=new ArrayList();
				for(iCount=0;iCount<colExposure.Count;iCount++)
				{
                    bPresent=false;
					for(iCountInner=0;iCountInner<colPayRanges.Count;iCountInner++)
					{
						if((((Exposure)colExposure.m_arrlstExposure[iCount]).DayCount)==(Common.Conversion.ConvertObjToInt(colPayRanges[iCountInner], m_iClientId)))
						{
							bPresent = true;
						}

					}
					if(bPresent==false)
					{
						colPayRanges.Add(((Exposure)(colExposure.m_arrlstExposure[iCount])).DayCount);
					}
				}

				foreach(object obj in colPayRanges)
				{
					iPayRange=Common.Conversion.ConvertObjToInt(obj, m_iClientId);					
					iTotalPersons = 0;
                    iTotalHours = 0;
                    sStartDate = "21000000" ;
					for(iCount = 0; iCount <  colExposure.Count;iCount++)
					{
						if(((Exposure)colExposure.m_arrlstExposure[iCount]).DayCount==iPayRange)
						{
							if(Declarations.GetIntValFromString(Conversion.ToDbDate(((Exposure)(colExposure.m_arrlstExposure[iCount])).EndDate),m_iClientId)>Declarations.GetIntValFromString(sEndDate,m_iClientId))
							{
								sEndDate = Conversion.ToDbDate(((Exposure)(colExposure.m_arrlstExposure[iCount])).EndDate);
							}
							if(Declarations.GetIntValFromString(Conversion.ToDbDate(((Exposure)(colExposure.m_arrlstExposure[iCount])).StartDate),m_iClientId)<Declarations.GetIntValFromString(sStartDate,m_iClientId))
							{
								sStartDate = Conversion.ToDbDate(((Exposure)(colExposure.m_arrlstExposure[iCount])).StartDate);
							}
							 iTotalPersons = iTotalPersons + ((Exposure)(colExposure.m_arrlstExposure[iCount])).Employees;
                             iTotalHours = iTotalHours + ((Exposure)(colExposure.m_arrlstExposure[iCount])).Hours;
						}
					}
					iDays =Declarations.DateDiffInAbsoluteDays(Conversion.ToDate(sStartDate),Conversion.ToDate(sEndDate),m_iClientId) + 1;
                    iTotalPersons = (iTotalPersons) / (iDays / (Common.Conversion.ConvertObjToInt(obj, m_iClientId)));
					p_objRpt.ExposureAvgEmployeeCount = p_objRpt.ExposureAvgEmployeeCount + iTotalPersons;
                    p_objRpt.ExposureTotalHoursWorked = p_objRpt.ExposureTotalHoursWorked + iTotalHours;
																		   
				}

			}
     bRetVal=true;
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
			throw new RMAppException(Globalization.GetString("RptOSHA.LoadExposureInfo3.Error",m_iClientId),p_objException);
		}
		finally
		{
			if(objReader!=null)
			{
				objReader.Dispose();
				objReader.Close();
			}
			colPayRanges=null;
			colExposure =null;
		}
		return bRetVal;
	}
	/// <summary>
	/// This method returns the formatted string to be printed in the soft error log.
	/// </summary>	
	/// <param name="p_sCaseNumber">Case number to be printed</param>	
	/// <param name="p_sDesc">Error Description</param>
	/// <returns>Formatted error string</returns>
	internal string  SoftErrFormat(string p_sCaseNumber,string p_sDesc)
	{
		string sRetVal="";
		try
		{
			if(p_sCaseNumber==null)
			{
             p_sCaseNumber="";
			}
			if(p_sDesc==null)
			{
            p_sDesc="";
			}
			sRetVal="Case Number:" + p_sCaseNumber + "    " + "Message: " + p_sDesc;

		}		
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.SoftErrFormat.Error", m_iClientId), p_objException);
		}
		return sRetVal;
	}
	/// <summary>
	/// This method reads the value for of flags like WORK_SUN_FLAG to WORK_SAT_FLAG for person involved.
	/// It returns the array containing Boolean value corresponding to these flags.
	/// </summary>	
	/// <param name="p_objReader">Reader will contain data for person involved.</param>
	/// <returns>Array containing Boolean value corresponding to the flags</returns>
	internal bool[] GetDaysOfWeek(ref DbReader p_objReader)
	{
		bool[] arrDaysOfWeek=new bool[7];
		try
		{
			arrDaysOfWeek[0]=Convert.ToBoolean(p_objReader.GetInt16("WORK_SUN_FLAG"));
			arrDaysOfWeek[1]=Convert.ToBoolean(p_objReader.GetInt16("WORK_MON_FLAG"));
			arrDaysOfWeek[2]=Convert.ToBoolean(p_objReader.GetInt16("WORK_TUE_FLAG"));
			arrDaysOfWeek[3]=Convert.ToBoolean(p_objReader.GetInt16("WORK_WED_FLAG"));
			arrDaysOfWeek[4]=Convert.ToBoolean(p_objReader.GetInt16("WORK_THU_FLAG"));
			arrDaysOfWeek[5]=Convert.ToBoolean(p_objReader.GetInt16("WORK_FRI_FLAG"));
			arrDaysOfWeek[6]=Convert.ToBoolean(p_objReader.GetInt16("WORK_SAT_FLAG"));
		}		
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.GetDaysOfWeek.Error", m_iClientId), p_objException);
		}
		return arrDaysOfWeek;
	}
	/// <summary>
	/// This method calculates the number of working days that are off between two dates.	
	/// </summary>	
	/// <param name="p_date1">Start Date</param>
	/// <param name="p_date2">End Date</param>
	/// <param name="DaysOfWeek">An array containing Boolean value for the days of week</param>
	/// <returns>It returns the number of working days that are off between two dates.</returns>
	internal int GetDaysOffWork(string p_date1, string p_date2, bool[] DaysOfWeek)
	{
		int iNewdays=0;
		DateTime datDate1;
		DateTime datDate2;
		int iForLoop=0;
		int iDays=0;
		try
		{
			DateTime.Parse(p_date1);
			DateTime.Parse(p_date2);
			datDate1=Conversion.ToDate(p_date1);
			datDate2=Conversion.ToDate(p_date2);
			if(datDate1==datDate2)
			{
				return iNewdays;
			}
			if(datDate1.AddDays(1)==datDate2)
			{
				return iNewdays;
			}
			datDate1=datDate1.AddDays(-1);
			iDays=((TimeSpan)datDate1.Subtract(datDate2)).Days;
			iNewdays=0;
			
			for(iForLoop=1;iForLoop<=iDays ;iForLoop++)
			{
				if(DaysOfWeek[((int)(datDate1.AddDays(iForLoop-1).DayOfWeek))])
				{
					iNewdays=iNewdays+1;
				}
			}

		}
		
		catch(Exception p_objException)
		{
			if(p_objException is FormatException)
			{
				iNewdays=-1;
			}
			else
			{
                throw new RMAppException(Globalization.GetString("RptOSHA.GetDaysOffWork.Error", m_iClientId), p_objException);
			}
		}
		return iNewdays;
	}
	
	/// <summary>
	/// This method builds the SQL for fetching the departments.
	/// </summary>	
	/// <param name="p_objRpt">OSHA Report</param>
	/// <returns>SQL for fetching the departments</returns>
	internal  string BuildDeptClause(IOSHAReport p_objRpt)
	{
		string sSql = "";
		string sTemp = "";
		string sRetVal="";
		try
		{    
			if(p_objRpt.ByOSHAEstablishmentFlag)
			{
				foreach(object obj in p_objRpt.SelectedEntities)
				{
                    Common.Utilities.AddDelimited(ref sTemp, Convert.ToString(obj), null, m_iClientId);
				}
				sSql = " (" + sTemp + ")";
			}
			else
			{
				if(p_objRpt.AllEntitiesFlag)
				{
					sSql = "SELECT DISTINCT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE " +
						p_objRpt.sUseReportLevel + "=" + p_objRpt.ReportOn;
				}
				else
				{
					foreach(object obj in p_objRpt.SelectedEntities)
					{
                        Common.Utilities.AddDelimited(ref sTemp, Convert.ToString(obj), null, m_iClientId);
					}
                      sTemp = " (" + sTemp + ")";
					sSql = "SELECT DISTINCT DEPARTMENT_EID FROM ORG_HIERARCHY " +
						" WHERE ORG_HIERARCHY." + p_objRpt.sReportLevel + " IN " + sTemp;
				}
			 }
			 sRetVal=" (" + sSql + ")";
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.BuildDeptClause.Error", m_iClientId), p_objException);
		}	
		return sRetVal;
	}

	/// <summary>
	/// This method would arrange the items in Sorted order.
	/// It will return the sorted list of items.
	/// </summary>	
	/// <param name="hsOshaItems">List of Items to be sorted</param>
	/// <param name="arrlstOshaItems">Sorted list of items</param>	
	internal void ArrangeItemsInSortedOrder(ref Hashtable hsOshaItems,ref ArrayList arrlstOshaItems)
	{
		string[] arrSortOnVals=new string[hsOshaItems.Count];
        string[] arrKeys =new string[hsOshaItems.Count];
		int iTemp=0;
		try
		{
			
			foreach(string keys in hsOshaItems.Keys)
			{
				arrKeys[iTemp]=keys;
				arrSortOnVals[iTemp]=((OSHAItem)hsOshaItems[keys]).SortOn;
				++iTemp;
			}
            Array.Sort(arrSortOnVals,arrKeys);
			foreach(string keys in arrKeys)
			{
               arrlstOshaItems.Add(hsOshaItems[keys]);
			}

		}		
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.ArrangeItemsInSortedOrder.Error", m_iClientId), p_objException);
		}			
	}
	/// <summary>
	/// This method would fetch the data for the Report On Company.	
	/// </summary>	
	/// <param name="p_objCompanyData">Company data fetched for an OSHA Report is returned back</param>
	/// <param name="p_objRpt">OSHA Report</param>
	internal void LoadReportOnCompanyData(ref EntityItem p_objCompanyData,IOSHAReport p_objRpt)
	{
		string sSql="";
		DbReader objReader=null;
		try
		{
			sSql =   sSql + " SELECT"+
				" REPORT_ON_ENTITY.LAST_NAME  RPT_LAST_NAME, REPORT_ON_ENTITY.ABBREVIATION  RPT_ABBREVIATION,"+
				" REPORT_ON_ENTITY.ADDR1 RPT_ADDR1, REPORT_ON_ENTITY.ADDR2 RPT_ADDR2,"+
                " REPORT_ON_ENTITY.ADDR3 RPT_ADDR3, REPORT_ON_ENTITY.ADDR4 RPT_ADDR4," +
				" REPORT_ON_ENTITY.CITY  RPT_CITY,REPORT_ON_ENTITY.STATE_ID  RPT_STATE_ID,"+
				" REPORT_ON_ENTITY.ZIP_CODE  RPT_ZIP_CODE,REPORT_ON_ENTITY.NATURE_OF_BUSINESS  RPT_NATURE_OF_BUSINESS,"+
				" REPORT_ON_ENTITY.SIC_CODE  RPT_SIC_CODE,"+
				" REPORT_ON_ENTITY.NAICS_CODE  RPT_NAICS_CODE"+     
				" FROM"+
				" ENTITY REPORT_ON_ENTITY"+
				" WHERE REPORT_ON_ENTITY.ENTITY_ID = " + p_objRpt.ReportOn;

			objReader = DbFactory.GetDbReader(m_sConnectString,sSql);
			if(objReader!=null)
			{
				if(objReader.Read())
				{
					p_objCompanyData.Name = objReader.GetString("RPT_LAST_NAME") + "  " + objReader.GetString("RPT_ABBREVIATION");
                    p_objCompanyData.Address = objReader.GetString("RPT_ADDR1") + "  " + objReader.GetString("RPT_ADDR2") + "  " + objReader.GetString("RPT_ADDR3") + "  " + objReader.GetString("RPT_ADDR4");
					p_objCompanyData.City = objReader.GetString("RPT_CITY").Trim();
					p_objCompanyData.State = p_objRpt.Context.GetStateCode(objReader.GetInt32("RPT_STATE_ID"));
					p_objCompanyData.ZipCode = objReader.GetString("RPT_ZIP_CODE").Trim();;
					p_objCompanyData.NatureOfBusiness = objReader.GetString("RPT_NATURE_OF_BUSINESS").Trim();
					p_objCompanyData.SicCode = p_objRpt.Context.GetStateCode(objReader.GetInt32("RPT_SIC_CODE"));
					p_objCompanyData.NaicsText = p_objRpt.Context.GetStateCode(objReader.GetInt32("RPT_NAICS_CODE"));
				}
			}

		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.LoadReportOnCompanyData.Error", m_iClientId), p_objException);
		}	
		finally
		{
			if (objReader != null)
			{
				objReader.Dispose();
				objReader.Close();
			}
		}
	}

	/// <summary>
	/// This method would close the Reader opened for fetching the Body Part data.	
	/// </summary>	
	private void m_objStateKeeper_CloseReaderForBodyPart()
	{
		if(this.m_objReaderForBodyParts!=null)
		{
         m_objReaderForBodyParts.Close();
		 m_objReaderForBodyParts.Dispose();
		 m_objReaderForBodyParts=null;
		}
	}
	/// <summary>
	/// This method would close the Reader opened for fetching the Injury data.	
	/// </summary>	
	private void m_objStateKeeper_CloseReaderLoadInjuries()
	{
		this.m_bLoadInjuriesReaderClosedByStateKeeper=true;
		if(this.m_objReaderForLoadInjuries!=null)
		{
          m_objReaderForLoadInjuries.Close();
		  m_objReaderForLoadInjuries.Dispose();
		  m_objReaderForLoadInjuries=null;
		}  
   }
   /// <summary>
   /// This method would close the Reader opened for fetching the Work Loss data.	
   /// </summary>	
	private void m_objStateKeeper_CloseReaderForWorkLoss()
	{
		if(this.m_objReaderForWorkLoss!=null)
		{
          m_objReaderForWorkLoss.Close();
		  m_objReaderForWorkLoss.Dispose();
			m_objReaderForWorkLoss=null;
		}
	}
	/// <summary>
	/// This method would close the Reader opened for fetching the Hospital data.	
	/// </summary>	
	private void m_objStateKeeper_CloseReaderLoadHospital()
	{
		if(this.m_objReaderForLoadHospital!=null)
		{
			m_objReaderForLoadHospital.Close();
			m_objReaderForLoadHospital.Dispose();
			m_objReaderForLoadHospital=null;
		}
	}

	/// <summary>
	/// This method would close the Reader opened for fetching the Physician data.	
	/// </summary>	
	private void m_objStateKeeper_CloseReaderLoadPhysician()
	{
		if(this.m_objReaderForLoadPhysician!=null)
		{
			m_objReaderForLoadPhysician.Close();
			m_objReaderForLoadPhysician.Dispose();
			m_objReaderForLoadHospital=null;
		}
	}
	/// <summary>
	/// This method would close the Reader opened for fetching the Treatment data.	
	/// </summary>
	private void m_objStateKeeper_CloseReaderLoadTreatments()
	{
		if(this.m_objReaderForLoadTreatments!=null)
		{
         m_objReaderForLoadTreatments.Close();
		m_objReaderForLoadTreatments.Dispose();
			m_objReaderForLoadTreatments=null;

		}

	}
	/// <summary>
	/// This method would close the Reader opened for fetching the Restricted Days data.	
	/// </summary>
	private void m_objStateKeeper_CloseReaderRestrictedDays()
	{
		if(this.m_objReaderForRestrictedDays!=null)
		{
           m_objReaderForRestrictedDays.Close();
			m_objReaderForRestrictedDays.Dispose();
			m_objReaderForRestrictedDays=null;
		}
	}
	/// <summary>
	/// This method would fetch the signature for OSHA Report.
	/// </summary>
	/// <returns>An image corresponding to the signature fetched for the OSHA Report.</returns>
	internal Bitmap FetchSignature()
	{
		string sVal="";
		string sSql="SELECT * FROM OSHA_OPTIONS WHERE ROW_ID = 1";
		DbReader objReader=null;
		ZipUtility objZip=null;
		Bitmap objImage=null;
		MemoryStream objMs=null;
		try
		{
            objZip = new ZipUtility(m_iClientId);
			objReader=DbFactory.GetDbReader(m_sConnectString,sSql);
			if(objReader.Read())
			{
				sVal=objReader.GetString("OSHA300A_SIGNATURE");
				if(sVal!="" && sVal!=null)
				{
					objMs=objZip.UnZIPBufferToMemoryStream(ref sVal);
				    objImage=new Bitmap(objMs);//Load the image into BitMap.
				}
			}
		}
		catch(RMAppException p_objException)
		{
			throw p_objException;
		}
		catch(Exception p_objException)
		{
            throw new RMAppException(Globalization.GetString("RptOSHA.FetchSignature.Error", m_iClientId), p_objException);
		}	
		finally
		{
			if(objReader!=null)
			{
				objReader.Close();
				objReader.Dispose();
			}			
			objZip=null;			
			objMs=null;
		}
		return objImage;
	}
	#endregion
}
}
