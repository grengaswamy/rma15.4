using System;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using DataDynamics.ActiveReports;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004	
	/// Purpose :  This is the class corresponding to the OSHA300A Report (Active Report)for the year 2004 and above.
	///			   It contains methods related to the report layout, report fields initialization and populating them with data.
	///</summary>
	internal class OSHA300A_2004 : CommonOsha300A
	{
        protected int m_iClientId;
		#region Constructor
		
        //internal OSHA300A_2004()
        //{
        //    InitializeReport();
        //}
		/// <summary>
		/// Constructor with year of report as the parameter.
		/// </summary>
		/// <param name="p_iYear">Year of Report</param>
		internal OSHA300A_2004(int p_iReportOfYear,int p_iClientId):base(p_iReportOfYear,p_iClientId)
		{
			InitializeReport();
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method would invoke the method of base class to initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void OSHA300A_2004_DataInitialize(object p_objsender, System.EventArgs p_objeArgs)
		{
			base.DataInitializeBase(ref p_objsender,ref p_objeArgs);
		}
		/// <summary>
		/// This method would invoke the method of base class to fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void OSHA300A_2004_FetchData(object p_objsender, FetchEventArgs p_objeArgs)
		{
			base.FetchDataBase(ref p_objsender,ref p_objeArgs);
		}
		/// <summary>
		/// This method would invoke the method of base class to do the page settings before start printing 
		/// the report and also to print the soft error log.		
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void OSHA300A_2004_ReportStart(object p_objsender, System.EventArgs p_objeArgs)
		{
			 base.ReportStartBase(ref p_objsender,ref p_objeArgs);
		}
		#endregion

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.Label Label2 = null;
		private DataDynamics.ActiveReports.Label Label3 = null;
		private DataDynamics.ActiveReports.Line Line1 = null;
		private DataDynamics.ActiveReports.Label Label55 = null;
		private DataDynamics.ActiveReports.Label Label56 = null;
		private DataDynamics.ActiveReports.Label Label57 = null;
		private DataDynamics.ActiveReports.Picture Image1 = null;
		private DataDynamics.ActiveReports.Label Label60 = null;
		private DataDynamics.ActiveReports.Line Line36 = null;
		private DataDynamics.ActiveReports.Line Line37 = null;
		private DataDynamics.ActiveReports.TextBox Config = null;
		private DataDynamics.ActiveReports.Label Label4 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.Label Label6 = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.Label Label8 = null;
		private DataDynamics.ActiveReports.Label Label9 = null;
		private DataDynamics.ActiveReports.Label Label10 = null;
		private DataDynamics.ActiveReports.Label Label11 = null;
		private DataDynamics.ActiveReports.Line Line2 = null;
		private DataDynamics.ActiveReports.Line Line3 = null;
		private DataDynamics.ActiveReports.Line Line4 = null;
		private DataDynamics.ActiveReports.Line Line5 = null;
		private DataDynamics.ActiveReports.Shape Shape1 = null;
		private DataDynamics.ActiveReports.Label Label12 = null;
		private DataDynamics.ActiveReports.Label Label13 = null;
		private DataDynamics.ActiveReports.Label Label14 = null;
		private DataDynamics.ActiveReports.Label Label17 = null;
		private DataDynamics.ActiveReports.Label Label18 = null;
		private DataDynamics.ActiveReports.Label Label19 = null;
		private DataDynamics.ActiveReports.Label Label20 = null;
		private DataDynamics.ActiveReports.Label Label21 = null;
		private DataDynamics.ActiveReports.Label Label22 = null;
		private DataDynamics.ActiveReports.Line Line8 = null;
		private DataDynamics.ActiveReports.Line Line9 = null;
		private DataDynamics.ActiveReports.Line Line10 = null;
		private DataDynamics.ActiveReports.Label Label23 = null;
		private DataDynamics.ActiveReports.Label Label24 = null;
		private DataDynamics.ActiveReports.Label Label25 = null;
		private DataDynamics.ActiveReports.Line Line11 = null;
		private DataDynamics.ActiveReports.Line Line12 = null;
		private DataDynamics.ActiveReports.Line Line13 = null;
		private DataDynamics.ActiveReports.Label Label26 = null;
		private DataDynamics.ActiveReports.Label Label27 = null;
		private DataDynamics.ActiveReports.Label Label28 = null;
		private DataDynamics.ActiveReports.Line Line15 = null;
		private DataDynamics.ActiveReports.Line Line16 = null;
		private DataDynamics.ActiveReports.Label Label31 = null;
		private DataDynamics.ActiveReports.Line Line17 = null;
		private DataDynamics.ActiveReports.Label Label32 = null;
		private DataDynamics.ActiveReports.Label Label33 = null;
		private DataDynamics.ActiveReports.Label Label34 = null;
		private DataDynamics.ActiveReports.Line Line20 = null;
		private DataDynamics.ActiveReports.Line Line21 = null;
		private DataDynamics.ActiveReports.Line Line22 = null;
		private DataDynamics.ActiveReports.Line Line23 = null;
		private DataDynamics.ActiveReports.Label Label35 = null;
		private DataDynamics.ActiveReports.Label Label36 = null;
		private DataDynamics.ActiveReports.Line Line24 = null;
		private DataDynamics.ActiveReports.Line Line25 = null;
		private DataDynamics.ActiveReports.Line Line26 = null;
		private DataDynamics.ActiveReports.Line Line27 = null;
		private DataDynamics.ActiveReports.Line Line28 = null;
		private DataDynamics.ActiveReports.Line Line29 = null;
		private DataDynamics.ActiveReports.Label Label37 = null;
		private DataDynamics.ActiveReports.Label Label38 = null;
		private DataDynamics.ActiveReports.Label Label39 = null;
		private DataDynamics.ActiveReports.Label Label42 = null;
		private DataDynamics.ActiveReports.Line Line30 = null;
		private DataDynamics.ActiveReports.Label Label43 = null;
		private DataDynamics.ActiveReports.Label Label44 = null;
		private DataDynamics.ActiveReports.Label Label45 = null;
		private DataDynamics.ActiveReports.Line Line31 = null;
		private DataDynamics.ActiveReports.Label Label46 = null;
		private DataDynamics.ActiveReports.Line Line32 = null;
		private DataDynamics.ActiveReports.Label Label48 = null;
		private DataDynamics.ActiveReports.Label Label49 = null;
		private DataDynamics.ActiveReports.Label Label50 = null;
		private DataDynamics.ActiveReports.Label Label51 = null;
		private DataDynamics.ActiveReports.Line Line33 = null;
		private DataDynamics.ActiveReports.Line Line34 = null;
		private DataDynamics.ActiveReports.Label Label52 = null;
		private DataDynamics.ActiveReports.Line Line35 = null;
		private DataDynamics.ActiveReports.Label Label53 = null;
		private DataDynamics.ActiveReports.Label Label54 = null;
		private DataDynamics.ActiveReports.Label Label58 = null;
		private DataDynamics.ActiveReports.Label Label59 = null;
		private DataDynamics.ActiveReports.TextBox ColumnG = null;
		private DataDynamics.ActiveReports.TextBox ColumnH = null;
		private DataDynamics.ActiveReports.TextBox ColumnI = null;
		private DataDynamics.ActiveReports.TextBox ColumnJ = null;
		private DataDynamics.ActiveReports.TextBox ColumnM1 = null;
		private DataDynamics.ActiveReports.TextBox ColumnM2 = null;
		private DataDynamics.ActiveReports.TextBox ColumnM4 = null;
		private DataDynamics.ActiveReports.TextBox HearingLoss = null;
		private DataDynamics.ActiveReports.TextBox ColumnM6 = null;
		private DataDynamics.ActiveReports.TextBox ColumnM3 = null;
		private DataDynamics.ActiveReports.TextBox EstablishmentName = null;
		private DataDynamics.ActiveReports.TextBox EstablishmentStreet = null;
		private DataDynamics.ActiveReports.TextBox EstablishmentCity = null;
		private DataDynamics.ActiveReports.TextBox EstablishmentState = null;
		private DataDynamics.ActiveReports.TextBox EstablishmentZip = null;
		private DataDynamics.ActiveReports.TextBox EstablishmentBusiness = null;
		private DataDynamics.ActiveReports.TextBox SIC1 = null;
		private DataDynamics.ActiveReports.TextBox SIC2 = null;
		private DataDynamics.ActiveReports.TextBox SIC3 = null;
		private DataDynamics.ActiveReports.TextBox SIC4 = null;
		private DataDynamics.ActiveReports.TextBox NAICS1 = null;
		private DataDynamics.ActiveReports.TextBox NAICS2 = null;
		private DataDynamics.ActiveReports.TextBox NACIS3 = null;
		private DataDynamics.ActiveReports.TextBox NAICS4 = null;
		private DataDynamics.ActiveReports.TextBox NAICS5 = null;
		private DataDynamics.ActiveReports.TextBox NAICS6 = null;
		private DataDynamics.ActiveReports.TextBox EmployeeCount = null;
		private DataDynamics.ActiveReports.TextBox EmployeeHours = null;
		private DataDynamics.ActiveReports.TextBox ExecTitle = null;
		private DataDynamics.ActiveReports.TextBox ExecPhone = null;
		private DataDynamics.ActiveReports.TextBox Date = null;
		private DataDynamics.ActiveReports.Picture ExecSign = null;
		private DataDynamics.ActiveReports.Label Label47 = null;
		private DataDynamics.ActiveReports.Label Label15 = null;
		private DataDynamics.ActiveReports.Label Label16 = null;
		private DataDynamics.ActiveReports.Line Line6 = null;
		private DataDynamics.ActiveReports.Line Line7 = null;
		private DataDynamics.ActiveReports.Line Line14 = null;
		private DataDynamics.ActiveReports.Label Label29 = null;
		private DataDynamics.ActiveReports.Label Label30 = null;
		private DataDynamics.ActiveReports.Label Label40 = null;
		private DataDynamics.ActiveReports.Label Label41 = null;
		private DataDynamics.ActiveReports.TextBox WorkLoss = null;
		private DataDynamics.ActiveReports.TextBox ColumnL = null;
		private DataDynamics.ActiveReports.TextBox Field2 = null;
		private DataDynamics.ActiveReports.Line Line18 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		internal void InitializeReport()
		{
			try
			{
				this.LoadLayout(this.GetType(), "Riskmaster.Application.OSHALib.OSHA300A_2004.rpx");
				this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
				this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
				this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
				this.Label1 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[0]));
				this.Label2 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[1]));
				this.Label3 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[2]));
				this.Line1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[3]));
				this.Label55 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[4]));
				this.Label56 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[5]));
				this.Label57 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[6]));
				this.Image1 = ((DataDynamics.ActiveReports.Picture)(this.Detail.Controls[7]));
				this.Label60 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[8]));
				this.Line36 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
				this.Line37 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
				this.Config = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[11]));
				this.Label4 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[12]));
				this.Label5 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[13]));
				this.Label6 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[14]));
				this.Label7 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[15]));
				this.Label8 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[16]));
				this.Label9 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[17]));
				this.Label10 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[18]));
				this.Label11 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[19]));
				this.Line2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[20]));
				this.Line3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[21]));
				this.Line4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[22]));
				this.Line5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[23]));
				this.Shape1 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[24]));
				this.Label12 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[25]));
				this.Label13 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[26]));
				this.Label14 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[27]));
				this.Label17 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[28]));
				this.Label18 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[29]));
				this.Label19 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[30]));
				this.Label20 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[31]));
				this.Label21 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[32]));
				this.Label22 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[33]));
				this.Line8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[34]));
				this.Line9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[35]));
				this.Line10 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[36]));
				this.Label23 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[37]));
				this.Label24 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[38]));
				this.Label25 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[39]));
				this.Line11 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[40]));
				this.Line12 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[41]));
				this.Line13 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[42]));
				this.Label26 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[43]));
				this.Label27 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[44]));
				this.Label28 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[45]));
				this.Line15 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[46]));
				this.Line16 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[47]));
				this.Label31 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[48]));
				this.Line17 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[49]));
				this.Label32 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[50]));
				this.Label33 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[51]));
				this.Label34 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[52]));
				this.Line20 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[53]));
				this.Line21 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[54]));
				this.Line22 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[55]));
				this.Line23 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[56]));
				this.Label35 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[57]));
				this.Label36 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[58]));
				this.Line24 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[59]));
				this.Line25 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[60]));
				this.Line26 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[61]));
				this.Line27 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[62]));
				this.Line28 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[63]));
				this.Line29 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[64]));
				this.Label37 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[65]));
				this.Label38 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[66]));
				this.Label39 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[67]));
				this.Label42 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[68]));
				this.Line30 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[69]));
				this.Label43 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[70]));
				this.Label44 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[71]));
				this.Label45 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[72]));
				this.Line31 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[73]));
				this.Label46 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[74]));
				this.Line32 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[75]));
				this.Label48 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[76]));
				this.Label49 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[77]));
				this.Label50 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[78]));
				this.Label51 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[79]));
				this.Line33 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[80]));
				this.Line34 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[81]));
				this.Label52 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[82]));
				this.Line35 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[83]));
				this.Label53 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[84]));
				this.Label54 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[85]));
				this.Label58 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[86]));
				this.Label59 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[87]));
				this.ColumnG = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[88]));
				this.ColumnH = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[89]));
				this.ColumnI = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[90]));
				this.ColumnJ = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[91]));
				this.ColumnM1 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[92]));
				this.ColumnM2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[93]));
				this.ColumnM4 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[94]));
				this.HearingLoss = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[95]));
				this.ColumnM6 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[96]));
				this.ColumnM3 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[97]));
				this.EstablishmentName = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[98]));
				this.EstablishmentStreet = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[99]));
				this.EstablishmentCity = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[100]));
				this.EstablishmentState = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[101]));
				this.EstablishmentZip = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[102]));
				this.EstablishmentBusiness = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[103]));
				this.SIC1 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[104]));
				this.SIC2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[105]));
				this.SIC3 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[106]));
				this.SIC4 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[107]));
				this.NAICS1 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[108]));
				this.NAICS2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[109]));
				this.NACIS3 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[110]));
				this.NAICS4 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[111]));
				this.NAICS5 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[112]));
				this.NAICS6 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[113]));
				this.EmployeeCount = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[114]));
				this.EmployeeHours = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[115]));
				this.ExecTitle = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[116]));
				this.ExecPhone = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[117]));
				this.Date = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[118]));
				this.ExecSign = ((DataDynamics.ActiveReports.Picture)(this.Detail.Controls[119]));
				this.Label47 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[120]));
				this.Label15 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[121]));
				this.Label16 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[122]));
				this.Line6 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[123]));
				this.Line7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[124]));
				this.Line14 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[125]));
				this.Label29 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[126]));
				this.Label30 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[127]));
				this.Label40 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[128]));
				this.Label41 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[129]));
				this.WorkLoss = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[130]));
				this.ColumnL = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[131]));
				this.Field2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[132]));
				this.Line18 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[133]));
				// Attach Report Events
				this.DataInitialize += new System.EventHandler(this.OSHA300A_2004_DataInitialize);
				this.FetchData += new FetchEventHandler(this.OSHA300A_2004_FetchData);
				this.ReportStart += new System.EventHandler(this.OSHA300A_2004_ReportStart);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OSHA300A_2004.InitializeReport.Error",m_iClientId),p_objException);
			}
		}

		#endregion
	}
}
