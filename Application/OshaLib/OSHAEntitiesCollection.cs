using System;
using System.Collections;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004	
	/// </summary>
	
	/// <summary>
	/// This will contain the collection of error objects.
	/// </summary>
	public class ColError 
	{   
		#region Member Variables
		/// <summary>
		/// Arraylist to contain the errors.
		/// </summary>
		internal ArrayList m_arrlstError =new ArrayList();
#endregion

		#region Properties
		/// <summary>
		/// Count of error in the collection of errors.
		/// </summary>
		public int count
		{
			get
			{
				return m_arrlstError.Count;
			}
		}
		#endregion

		#region Methods
       /// <summary>
       /// This method adds an error to the error collection.
       /// </summary>
       /// <param name="p_sProcedure">Procedure that has caused the error</param>
       /// <param name="p_sErrDescription">Description of the error</param>
		internal void Add(string p_sProcedure,string p_sErrDescription,int p_iClientId)
		{
			
			Error objError=null;
			try
			{
				objError = new Error();
				objError.Description = p_sErrDescription;
				m_arrlstError.Add(objError);			
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ColError.Add.Error",p_iClientId),p_objException);
			}	
			finally
			{
				objError=null;
			}
		}

		#endregion
	}
	/// <summary>
	/// This will contain the collection of exposure objects.
	/// </summary>
	internal class ColExposure
	{
		#region Member Variables
		/// <summary>
		/// Arrarlist to contain the exposure information.
		/// </summary>
        protected int m_iClientId;
        internal ArrayList m_arrlstExposure = new ArrayList();
		#endregion

		#region Properties
		/// <summary>
		/// Count of the exposure units in the exposure collection.
		/// </summary>
		internal int Count
		{
			get
			{
				return m_arrlstExposure.Count;
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		internal ColExposure(int p_ClientId)
		{
            m_iClientId = p_ClientId;
		}
		#endregion

		#region Methods
		/// <summary>
		/// This method will add the exposure information to the collection.
		/// </summary>
		/// <param name="p_iDayCount">Count of the days.</param>
		/// <param name="p_datEndDate">End date</param>
		/// <param name="p_datStartDate">Start date</param>
		/// <param name="p_iHours">Number of hours</param>
		/// <param name="p_iEmployees">Employees</param>
		/// <param name="p_iRowID">Row id</param>
		/// <returns>The Exposure object added to the collection of such exposure units.</returns>
		internal Exposure Add(int p_iDayCount,DateTime p_datEndDate,DateTime p_datStartDate,int p_iHours,int p_iEmployees,int p_iRowID)
		{
			try
			{
				Exposure objExposure = new Exposure();
				//set the properties passed into the method
				objExposure.DayCount = p_iDayCount ;
				objExposure.EndDate = p_datEndDate; 
				objExposure.StartDate = p_datStartDate;
				objExposure.Hours = p_iHours;
				objExposure.Employees = p_iEmployees;
				objExposure.RowID = p_iRowID;
				m_arrlstExposure.Add(objExposure);
				return objExposure;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ColExposure.Add.Error", m_iClientId), p_objException);
			}			
		}
		#endregion		

	}
}
