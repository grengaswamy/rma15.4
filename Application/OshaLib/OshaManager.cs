using System;
using System.Collections;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Application.ReportInterfaces;
using Riskmaster.ExceptionTypes;
using DataDynamics.ActiveReports.Export.Pdf;
using System.Collections.Generic;

namespace Riskmaster.Application.OSHALib
{
	/// <summary>	
	/// Author  :  Tanuj Narula
	/// Date    :  29 October 2004
	/// Purpose :  This is the main class that would identify the type of OSHA Report to be generated.
	///			   It would invoke the methods to generate the desired report.	
	/// </summary>
	public class OshaManager:IReportEngine
	{		
		#region Member Variables
		/// <summary>
		/// Complete path of the xml template corresponding to a specific report type
		/// </summary>
		private string m_sXmlLoadPath="";
		/// <summary>
		/// Print OSHA description flag.
		/// </summary>
		private bool m_bPrintOSHADesc=false;
		/// <summary>
		/// An instance of InstanceUtilities class
		/// </summary>
		private InstanceUtilities m_objInstance=null;
		/// <summary>
		/// Array list containing the Report items
		/// </summary>
		private ArrayList m_arrlstReports=null;
		/// <summary>
		/// XML Document containing the report criteria
		/// </summary>
		private XmlDocument m_objCriteriaXML=null;
		/// <summary>
		/// Array list containing the error objects
		/// </summary>
        private ColError m_objColErrors=new ColError();
        /// <summary>
		/// An instance of IWorkerNotifications.
		/// </summary>
		private IWorkerNotifications m_objNotify=null;
		/// <summary>
		/// Database connection string
		/// </summary>
		private string m_sDsn ="";
		/// <summary>
		/// Event Id 
		/// </summary>
		private int m_iFilterEventId ;
		/// <summary>
		/// Claim Id 
		/// </summary>
		private int m_iFilterClaimId;
		/// <summary>
		/// By OSHA Establishment flag
		/// </summary>
		private bool m_bByOSHAEstablishmentFlag ;
		/// <summary>
		/// All Entities flag
		/// </summary>
		private bool m_bAllEntitiesFlag  ;
		/// <summary>
		/// Value of Report At Level
		/// </summary>
		private int m_iReportLevel;
		/// <summary>
		/// Description for Report At Level
		/// </summary>
		private string m_sReportLevel="";
		/// <summary>
		/// Value of Use Reports At Level
		/// </summary>
		private int m_iUseReportLevel ;
		/// <summary>
		/// Description for Use Reports At Level
		/// </summary>
		private string m_sUseReportLevel="" ;
		/// <summary>
		/// Report Type
		/// </summary>
		private int m_iReportType;		
		/// <summary>
		/// Report Beginning Date
		/// </summary>
		private DateTime m_datBeginDate;
		/// <summary>
		/// Report Ending Date
		/// </summary>
		private DateTime m_datEndDate;
		/// <summary>
		/// Year of Report
		/// </summary>
		private string m_sYearOfReport="";
		/// <summary>
		/// Use relative dates flag
		/// </summary>
		private bool m_bUseRelDates ;
		/// <summary>
		/// Use year flag
		/// </summary>
		private bool m_bUseYear ;
		/// <summary>
		/// Value of Relative date scalar time value
		/// </summary>
		private int m_iRelDateScalar;
		/// <summary>
		/// Value of Relative date scalar time unit
		/// </summary>
		private string m_sRelDateUnit ="";
		/// <summary>
		/// Instance of Preparer Class.
		/// </summary>
		private Preparer m_objPreparer;
		/// <summary>
		/// Enforce 180Day Rule flag.
		/// </summary>
		private bool m_bEnforce180DayRule ;
		/// <summary>
		/// Use Primary Location flag.
		/// </summary>
		private bool m_bPrimaryLocationFlag;
		/// <summary>
		/// Value of As of Date
		/// </summary>
		private DateTime m_datAsOfDate ;
		/// <summary>
		/// Value of Sort Order
		/// </summary>
		private int m_iItemSortOrder ;
		/// <summary>
		/// Value of Prefix for Establishment Name
		/// </summary>
		private string m_sEstablishmentNamePrefix ;
        
        /// <summary>
        /// Value of org level for Establishment Name
        /// </summary>
        private Int32 m_lEstablishmentNamePrefixOrgLevel;
        //MITS 37193
        /// <summary>
        /// Value of column F data source
        /// </summary>
        private int m_iColumnESource;
        /// <summary>
		/// Value of column F data source
		/// </summary>
		private int m_iColumnFSource ;
		/// <summary>
		/// Print Problem Log flag.
		/// </summary>
		public bool m_bPrintSoftErrLog;
		/// <summary>
		/// Event Based Report flag.
		/// </summary>
		private bool m_bEventBasedFlag ;
		/// <summary>
		/// Print OSHA Description flag.
		/// </summary>
		private bool m_bPrintOSHADescFlag ;
		/// <summary>
		/// Use Employee Names flag.
		/// </summary>
		private bool m_bUseEmployeeNames ;
		/// <summary>
		/// Output directory for saving the reports
		/// </summary>
		private string m_sOutputDirectory="";
		/// <summary>
		/// Format of the OSHA report
		/// </summary>
		private eReportFormat m_enumReportFormat;
		/// <summary>
		/// Array list containing the Selected entities
		/// </summary>
		private ArrayList m_arrlstEntities ;
		private ArrayList m_arrlstReportOutput;

        private int m_iClientId = 0;//rkaur27
		#endregion

		#region Properties
		/// <summary>
		/// Riskmaster.Application.OSHALib.OSHAManager.Notify property 
		/// accesses the value of the m_objNotify.
		/// </summary>
		public IWorkerNotifications Notify
		{
			get
			{
				return m_objNotify;
			}
			set
			{
				m_objNotify=value;
			}
		}
		
		/// <summary>
		/// Property to access the error objects
		/// </summary>
		public ColError Errors 
		{
			get
			{
				return m_objColErrors;
			}
			
		}
		/// <summary>
		/// Property to access the report items
		/// </summary>
		public ArrayList Reports
		{
			get
			{
				return m_arrlstReports;
			}
		}
		/// <summary>
		/// Property to access the value of Event Id.
		/// </summary>
		public int FilterEventId
		{
			get
			{
				return m_iFilterEventId;
			}
			set
			{
				m_iFilterEventId=value;
			}
		}
		/// <summary>
		/// Property to access the value of Claim Id.
		/// </summary>
		public int FilterClaimId
		{
			get
			{
				return m_iFilterClaimId;
			}
			set
			{
				m_iFilterClaimId=value;
			}
		}
		/// <summary>
		/// Property to access the value of Report Type.
		/// </summary>
		public int  ReportType
		{
			get
			{
				return m_iReportType;
			}
			set
			{
				try
				{
					m_iReportType=value;
					this.LoadEmptyXMLTemplate();
				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("OshaManager.ReportType.ErrorSet", m_iClientId),p_objException);
				}				
			}
		}
		/// <summary>
		/// Property to access the value of Report Beginning date.
		/// </summary>
		public DateTime BeginDate
		{
			get
			{
				return m_datBeginDate;
			}
			set
			{
				m_datBeginDate = value;
			
			}
		}
		/// <summary>
		/// Property to access the value of Report ending date
		/// </summary>
		public DateTime EndDate
		{
			get
			{
				return m_datEndDate;
			}
			set
			{
				if(m_datBeginDate.Year==value.Year)
				{
					m_datEndDate = value;
				}
				else
				{
					throw new RMAppException(Globalization.GetString("OshaManager.EndDate.ErrorSet", m_iClientId));													
				}
			}
		}
		/// <summary>
		/// Property to access the value of Year of Report
		/// </summary>
		public string YearOfReport
		{
			get
			{
				return m_sYearOfReport;
			}
			set
			{
				if(value!=null)
				{
					m_datBeginDate=Conversion.ToDate(value.Trim() + "0101");
					m_datEndDate=Conversion.ToDate(value.Trim() + "1231");
					m_sYearOfReport=value.Trim();
				}
			}
		}
		/// <summary>
		/// Property to access the value of By OSHA Establishment Flag
		/// </summary>
		public  bool ByOSHAEstablishmentFlag
		{
			get
			{
				return m_bByOSHAEstablishmentFlag;
			}
			set
			{
				//This is not present in VB version, this is added because if ByOSHAEstablishmentFlag is true, ReportLevel 
				// will not be selected by user, so ValidateCriteria() of this class will lways raise the error.
				if(value)
				{
					this.EstablishmentNamePrefix = "";
					this.ReportLevel = 1012;
					this.UseReportLevel = 1012;
				}
				m_bByOSHAEstablishmentFlag = value;
			}
		}

		/// <summary>
		/// Property to access the value of instance of Preparer class.
		/// </summary>
		internal Preparer Preparer
		{
			get
			{
				return m_objPreparer;
			}
		}
		/// <summary>
		/// Property to access the value of Report At Level
		/// </summary>
		public int ReportLevel
		{
			get
			{
				return m_iReportLevel;
			}
			set
			{
				try
				{
					m_iReportLevel=value;
					m_sReportLevel=Declarations.OrgLevelToEIDColumn(value,m_iClientId);
				}
				catch(Exception p_objException)
				{
					throw p_objException;
				}
			}
		}
		/// <summary>
		/// Property to access the value of Use Reports At Level
		/// </summary>
		public int UseReportLevel
		{
			get
			{
				return m_iUseReportLevel;
			}
			set
			{
				try
				{
					m_iUseReportLevel=value;
					m_sUseReportLevel=Declarations.OrgLevelToEIDColumn(value,m_iClientId);
				}
				catch(Exception p_objException)
				{
					throw p_objException;
				}
			}
		}

		/// <summary>
		/// Property to access the value of Use Employee Names flag
		/// </summary>
		public bool UseEmployeeNames
		{
			get
			{
				return m_bUseEmployeeNames;
			}
			set
			{
				m_bUseEmployeeNames=value;
			}
		}						
		/// <summary>
		/// Property to access the value of Use relative dates flag
		/// </summary>
		public bool UseRelDates
		{
			get
			{
				return m_bUseRelDates;
			}
			set
			{
				m_bUseRelDates=value;
			}
		}
		/// <summary>
		/// Property to access the value of All Entities flag
		/// </summary>									
		public bool AllEntitiesFlag
		{
			get
			{
				return m_bAllEntitiesFlag;
			}
			set
			{
				m_bAllEntitiesFlag = value;
			}
		}
		/// <summary>
		/// Property to access the array list of selected entities
		/// </summary>	
		public ArrayList SelectedEntities
		{
			get
			{
				return m_arrlstEntities;
			}
			set
			{
				m_arrlstEntities=value;
			}
		}
		/// <summary>
		/// Property to access the value of As of Date
		/// </summary>
		public DateTime AsOfDate
		{
			get
			{
				return m_datAsOfDate;
			}
			set
			{
				m_datAsOfDate = value;
			}
		}
		/// <summary>
		/// Property to access the value of Enforce 180Day Rule flag
		/// </summary>
		public bool Enforce180DayRule
		{
			get
			{
				return m_bEnforce180DayRule;
			}
			set
			{
				m_bEnforce180DayRule = value;
			}
		}

		/// <summary>
		/// Property to access the value of Sort Order
		/// </summary>
		public int ItemSortOrder
		{
			get
			{
				return m_iItemSortOrder;
			}
			set
			{
				m_iItemSortOrder = value;
			}
		}
        //MITS 37193
        /// <summary>
        /// Property to access the value of column F Data source
        /// </summary>
        public int ColumnESource
        {
            get
            {
                return m_iColumnESource;
            }
            set
            {
                m_iColumnESource = value;
            }
        }

		/// <summary>
		/// Property to access the value of column F Data source
		/// </summary>
		public int ColumnFSource
		{
			get
			{
				return m_iColumnFSource;
			}
			set
			{
				m_iColumnFSource = value;
			}
		}
		/// <summary>
		/// Property to access the value of Prefix for Establishment Name flag
		/// </summary>
		public string EstablishmentNamePrefix
		{
			get
			{
				return m_sEstablishmentNamePrefix;
			}
			set
			{
				m_sEstablishmentNamePrefix = value;
			}
		}
        /// <summary>
        /// Property to access the value of org level of Establishment
        /// </summary>
        public Int32 EstablishmentNamePrefixOrgLevel
        {
            get
            {
                return m_lEstablishmentNamePrefixOrgLevel;
            }
            set
            {

                m_lEstablishmentNamePrefixOrgLevel = value;
            }
        }

        /// <summary>
		/// Property to access the value of Print Problem flag
		/// </summary>
		public bool PrintSoftErrLog
		{
			get
			{
				return m_bPrintSoftErrLog;
			}
			set
			{
				m_bPrintSoftErrLog=value;
			}
		}
		/// <summary>
		/// Property to access the value of Event Based Report flag
		/// </summary>
		public bool EventBasedFlag
		{
			get
			{
				return m_bEventBasedFlag;
			}
			set
			{
				m_bEventBasedFlag = value;
			}
		}
		
		/// <summary>
		/// This property accesses the XML document loaded with the XML template for the specific OSHA Report.
		/// </summary>
		public XmlDocument CriteriaXmlDom
		{
			get
			{
				XmlNodeList objControlNodes=null;
				XmlElement objEntityElt=null;
				XmlElement objEntityTmpElt=null;
				XmlElement objElt=null;
				XmlDocument objCriteriaXML=null;
				try
				{
					objCriteriaXML=this.m_objCriteriaXML;
					objControlNodes=objCriteriaXML.GetElementsByTagName("control");
					foreach(XmlElement objTmpElt in objControlNodes)
					{
						if(objTmpElt.Attributes.GetNamedItem("checked")!=null)
						{
							objTmpElt.Attributes.RemoveNamedItem("checked");
						}
						if(objTmpElt.Attributes.GetNamedItem("checked")!=null)
						{
							objTmpElt.Attributes.RemoveNamedItem("codeid");
						}
					}
					objControlNodes = objCriteriaXML.GetElementsByTagName("control");
					if(objControlNodes!=null)
					{
						foreach(XmlElement objTmpElt in objControlNodes)
						{
							switch(objTmpElt.GetAttribute("name").ToLower())
							{
								case "reportlevel" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(this.ReportLevel,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + this.ReportLevel + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "usereportlevel" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(this.UseReportLevel,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + this.UseReportLevel + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "begindate" :
								{
								
									objTmpElt.SetAttribute("value",Conversion.ToDbDate(this.BeginDate));
									break;
									
								}
								case "enddate" :
								{
									
									objTmpElt.SetAttribute("value",Conversion.ToDbDate(this.EndDate));
									
									break;
								}
								case "allentitiesflag" :
								{
									if(this.AllEntitiesFlag)
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @value='true']");
									}
									else
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='allentitiesflag' and @value='false']");
									}
									if(objEntityElt!=null)
										objEntityElt.SetAttribute("checked", "");
									break;
								}
								case "selectedentities" :
								{
									if(m_arrlstEntities!=null)
									{
										foreach(string vTmp in m_arrlstEntities)
										{
											objEntityElt = objCriteriaXML.CreateElement("option");
											objEntityElt.SetAttribute("value",vTmp);
											objEntityElt.InnerText = m_objInstance.GetOrgAbbreviation(Declarations.GetIntValFromString(vTmp,m_iClientId)) + " - " + m_objInstance.GetOrgName(Declarations.GetIntValFromString(vTmp,m_iClientId));
											objTmpElt.AppendChild(objEntityElt);
										}
									}
									break;
								}
								case "yearofreport" :
								{
									objTmpElt.SetAttribute("codeid", this.YearOfReport);
									break;
								}
								case "eventbasedflag" :
								{
									if(this.EventBasedFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "printsofterrlog" :
								{
									if(this.PrintSoftErrLog)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "printoshadescflag" :
								{
									if(m_bPrintOSHADescFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "primarylocationflag" :
								{
									if(m_bPrimaryLocationFlag)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "timescalar" :
								{
									
									objTmpElt.SetAttribute("value",Declarations.GetStrValFromInt(m_iRelDateScalar,m_iClientId));
									
									break;
								}
								case "timeunit" :
								{
									
									objTmpElt.SetAttribute("codeid",m_sRelDateUnit);
									objElt=(XmlElement)objTmpElt.SelectSingleNode("option[@value='" + m_sRelDateUnit + "']");
									if(objElt!=null)
									{
										objElt.SetAttribute("selected","");
									}
									
									break;
								}
								case "datemethod" :
									if(m_bUseRelDates)
									{
										objElt=(XmlElement)objCriteriaXML.SelectSingleNode("//control[@value = 'userelative']");
									}
									else
									{
										if(m_bUseYear)
										{
											objElt=(XmlElement)objCriteriaXML.SelectSingleNode("//control[@value = 'useyear']");
										}
										else
										{
											objElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@value = 'userange']");
										}

									}
									if(objElt!=null)
									{
										objElt.SetAttribute("checked","");
									}
									break;
								case "preparername" :
								{
									
									objTmpElt.SetAttribute("value",this.Preparer.Name);
									break;
								}
								case "preparertitle" :
								{
									objTmpElt.SetAttribute("value",this.Preparer.Title);
									break;
								}
								case "preparerphone" :
								{
									objTmpElt.SetAttribute("value",this.Preparer.Phone);
									break;
								}
								case "preparersignaturedate" :
									objTmpElt.SetAttribute("value",Conversion.ToDbDate(this.Preparer.SignatureDate));
									break;
								case "useemployeenames" :
								{
									if(this.UseEmployeeNames)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}

								case "enforce180dayrule" :
								{
									if(this.Enforce180DayRule)
									{
										objTmpElt.SetAttribute("checked","");
									}
									break;
								}
								case "asofdate" :
								{
									objTmpElt.SetAttribute("value",Conversion.ToDbDate(this.AsOfDate));
									break;
								}
								case "establishmentnameprefix" :
								{
									objTmpElt.SetAttribute("codeid",this.EstablishmentNamePrefix); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + this.EstablishmentNamePrefix + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "sortorder" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(this.ItemSortOrder,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + this.ItemSortOrder + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
                                //MITS 37193 Begins
                                case "columnesource":
                                {
                                    objTmpElt.SetAttribute("codeid", Declarations.GetStrValFromInt(this.ColumnESource,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
                                    objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + this.ColumnESource + "']");
                                    if (objEntityElt != null)
                                    {
                                        objEntityElt.SetAttribute("selected", "");
                                    }
                                    break;
                                }
                                //MITS 37193 Ends
								case "columnfsource" :
								{
									objTmpElt.SetAttribute("codeid",Declarations.GetStrValFromInt(this.ColumnFSource,m_iClientId)); //any numeric combo box is set as a "codeid" in RequestToXML()
									objEntityElt = (XmlElement)objTmpElt.SelectSingleNode("option[@value='" + this.ColumnFSource + "']");
									if(objEntityElt!=null)
									{
										objEntityElt.SetAttribute("selected","");
									}
									break;
								}
								case "filtereventid" :
								{
									objTmpElt.SetAttribute("value",this.FilterEventId.ToString());
									break;
								}
								case "filterclaimid" :
								{
									objTmpElt.SetAttribute("value",this.FilterClaimId.ToString());
									break;
								}
								case "byoshaestablishmentflag":
								{
									if(this.ByOSHAEstablishmentFlag)
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @value='true']");
									}
									else
									{
										objEntityElt = (XmlElement)objCriteriaXML.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @value='false']");
									}
									if(objEntityElt!=null)
										objEntityElt.SetAttribute("checked","");
									break;
								}
							}
						}
					}
       

					objControlNodes = objCriteriaXML.GetElementsByTagName("internal");
					foreach(XmlElement objTmpElt in objControlNodes)
					{
						switch(objTmpElt.GetAttribute("name"))
						{
							case "reporttype" :
								objEntityTmpElt=(XmlElement)m_objCriteriaXML.SelectSingleNode("//report");
								objEntityTmpElt.SetAttribute("type",Declarations.GetStrValFromInt(this.ReportType,m_iClientId));
								break;
						}
					}

				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("OshaManager.CriteriaXmlDom.ErrorGet", m_iClientId),p_objException);
				}					
				finally
				{
					objControlNodes=null;
					objEntityElt=null;
					objEntityTmpElt=null;
					objElt=null;					
				}
				return objCriteriaXML;
			}
			set
			{
				XmlDocument objXmlCriteriaDoc=null;
				try
				{
					objXmlCriteriaDoc=value;
					this.ParseCriteria(ref objXmlCriteriaDoc);
					this.ValidateCriteria();
					
				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("OshaManager.CriteriaXmlDom.ErrorSet", m_iClientId),p_objException);
				}	
				finally
				{
					objXmlCriteriaDoc=null;
				}
			}
		}
		/// <summary>
		/// This property accesses the value output directory where generated OSHA Reports would be saved.
		/// </summary>
		public string OutputDirectory
		{
			set
			{
				m_sOutputDirectory=value;
			}
			get
			{
				return m_sOutputDirectory;
			}
		}
		/// <summary>
		/// This property accesses the value output format of OSHA Report.
		/// </summary>
		public eReportFormat OutputFormat
		{
			set
			{
				m_enumReportFormat=value;
			}
			get
			{
				return m_enumReportFormat;
			}
		}

        public Dictionary<int, string> OshaReports
        {
            get;
            set;
        }
#endregion

		#region Constructor
        public OshaManager(int p_iClientId)
        {
            m_iClientId = p_iClientId;
        }

		/// <summary>
		/// This constructor will initializes the sub object required by Osha Manager object.
		/// </summary>
		/// <param name="p_sDsn">Database connection string.</param>
		/// <param name="p_sXmlLoadPath">Complete path of the xml template corresponding to a specific report type.</param>
		public OshaManager(string p_sDsn,int p_iReportType, int p_iClientId)
		{
			
            //Build the Dictionary collection
            Dictionary<int, string> dictOSHAReports = new Dictionary<int, string>();
            dictOSHAReports.Add(1, RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"osha\oshareport.xml"));
            dictOSHAReports.Add(3, RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"osha\osha300areport.xml"));
            dictOSHAReports.Add(4, RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"osha\osha301report.xml"));
            dictOSHAReports.Add(5, RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"osha\oshasharpslog.xml"));

            //Set the property to the Dictionary collection
            this.OshaReports = dictOSHAReports;

            m_iClientId = p_iClientId;//rkaur27

			if(p_iReportType!=1 && p_iReportType!=3 && p_iReportType!=4 && p_iReportType!=5)
			{
				throw new RMAppException(Globalization.GetString("Oshamanager.RunReport.UnknownReportType", m_iClientId));
			}
           
			this.m_sDsn=p_sDsn;
            this.m_sXmlLoadPath = dictOSHAReports[p_iReportType];
			m_objInstance = new InstanceUtilities(p_sDsn,m_iClientId);
			m_arrlstReports = new ArrayList();
			this.m_objPreparer = new Preparer();
			m_objCriteriaXML = new XmlDocument();
			m_arrlstEntities=new ArrayList();
			m_arrlstReportOutput=new ArrayList();
				
		}
		
		#endregion

		#region Methods
		/// <summary>
		/// This method would validate that the connection string is not blank.
		/// </summary>
		public void Init()
		{
			try
			{
				if(this.m_sDsn=="")
				{
					throw new RMAppException(Globalization.GetString("OshaManager.Init.ErrorLogin", m_iClientId));
				}
				
				this.m_bPrintOSHADesc =Convert.ToBoolean(m_objInstance.GetSingleInt("PRINT_OSHA_DESC", "SYS_PARMS", ""));				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("OshaManager.Init.ErrorRun", m_iClientId),p_objException);
			}				
		}
		
		/// <summary>
		/// This function would call methods to validate the report criteria and generate the OSHA Report.
		/// </summary>
		public void RunReport()
		{
			string sReportType="";
			IOSHAReport objRpt=null;
			ArrayList arrlstDDPDF=new ArrayList();
			OSHA301_2004 obj301_2004_AR=null;
			OSHA300A_2004 obj300A_2004_AR=null;
			OSHA300A_2003 obj300A_2003_AR=null;
			OSHA300_2004 obj300_2004_AR=null;
			OSHA300_2003 obj300_2003_AR=null;
			SharpsLogReport objSharpLog_AR=null;
			PdfExport objPDF_AR=null;
			string sOutputAs="";
			try
			{
				this.ValidateCriteria();
                //MITS 37836 Starts
                if (string.IsNullOrEmpty(this.EstablishmentNamePrefix))
                    this.EstablishmentNamePrefixOrgLevel = 0;
                else
                    this.EstablishmentNamePrefixOrgLevel = Convert.ToInt32((this.EstablishmentNamePrefix));
                //MITS 37836 Ends
                if ((Convert.ToBoolean(this.FilterEventId)) || (Convert.ToBoolean(this.FilterClaimId)))
				{
					this.GenerateFilteredReports();
				}
				else
				{
					this.GenerateIncludedReports();
				}
				foreach(ReportItem objRptItem in Reports)
				{
					if(this.Notify!=null)
					{
						//TODO Notification logic would be finalized after SMWorker is developed.
						if(this.Notify.CheckAbort()!=0)
						{
							throw new RMAppException(Globalization.GetString("OshaManager.RunReport.ErrorRun", m_iClientId));					
						}
					}
					objRpt=this.CreateReport(objRptItem.ReportOnEID,objRptItem.SelectedEntities);
					arrlstDDPDF.Add(objRpt);
					if(this.Notify!=null)
					{
						//TODO Notification logic would be finalized after SMWorker is developed.
						if(this.Notify.CheckAbort()!=0)
						{
							throw new RMAppException(Globalization.GetString("OshaManager.RunReport.ErrorRun", m_iClientId));					
						}
					}
					objRpt.LoadEntities();		
					
				}
                 
				switch(this.ReportType)
				{
					case 1:
						sReportType = "OSHA300_";
						break;
					case 3 :
						sReportType = "OSHA300A_";
						break;
					case 4 :
						bool bHasOshaItems=false;
						foreach(IOSHAReport obj301Report in arrlstDDPDF)
						{
							if(obj301Report.Items.Count!=0)
							{
								bHasOshaItems=true;
								break;
							}
						}
						if(!bHasOshaItems)
						{
                            throw new RMAppException(string.Format(Globalization.GetString("Oshamanager.RunReport.NoReportsToRun", m_iClientId), "<br/>", "<br/>"));
						}
						sReportType = "OSHA301_";
						break;
					case 5 :
						sReportType = "OSHA_SHARPS_LOG_";
						break;
					default:
					{
                        throw new RMAppException(Globalization.GetString("Oshamanager.RunReport.UnknownReportType", m_iClientId));
						
					}
				}

				/*Important Note-:Do remember to pass in the  begin year in the constructor 
				 *                of the active report to be generated.
				 *                If not passed, reports may throw errors.
				 */
				switch(m_enumReportFormat)
				{
					case eReportFormat.Pdf :
					{
						switch(this.ReportType)
						{
							case 1:
							{
								if(this.BeginDate.Year>2003)
								{
                                    obj300_2004_AR = new OSHA300_2004(this.BeginDate.Year, m_iClientId);
									obj300_2004_AR.ReportCollection=arrlstDDPDF;
									obj300_2004_AR.Notify=this.Notify;
									Declarations.CheckAbort(m_objNotify,m_iClientId);
									obj300_2004_AR.Run();
									objPDF_AR=new PdfExport();
									sOutputAs=this.OutputDirectory + sReportType + DateTime.Now.ToString("yymmddhhss")+".pdf";
									objPDF_AR.Export(obj300_2004_AR.Document,sOutputAs);
								}
								else
								{
                                    obj300_2003_AR = new OSHA300_2003(this.BeginDate.Year, m_iClientId);
									obj300_2003_AR.ReportCollection=arrlstDDPDF;
									obj300_2003_AR.Notify=this.Notify;
									Declarations.CheckAbort(m_objNotify,m_iClientId);
									obj300_2003_AR.Run();
									objPDF_AR=new PdfExport();
									sOutputAs=this.OutputDirectory + sReportType + DateTime.Now.ToString("yymmddhhss")+".pdf";
									objPDF_AR.Export(obj300_2003_AR.Document,sOutputAs);
								}
								break;
								
							}
							case 3:
							{
								if(this.BeginDate.Year>2003)
								{
									obj300A_2004_AR=new OSHA300A_2004(this.BeginDate.Year,m_iClientId);
									obj300A_2004_AR.ReportCollection=arrlstDDPDF;
									obj300A_2004_AR.Notify=this.Notify;
									Declarations.CheckAbort(m_objNotify,m_iClientId);
									obj300A_2004_AR.Run();
									objPDF_AR=new PdfExport();
									sOutputAs=this.OutputDirectory + sReportType + DateTime.Now.ToString("yymmddhhss")+".pdf";
									objPDF_AR.Export(obj300A_2004_AR.Document,sOutputAs);
								}
								else
								{

									obj300A_2003_AR=new OSHA300A_2003(this.BeginDate.Year,m_iClientId);
									obj300A_2003_AR.ReportCollection=arrlstDDPDF;
									obj300A_2003_AR.Notify=this.Notify;
									Declarations.CheckAbort(m_objNotify,m_iClientId);
									obj300A_2003_AR.Run();
									objPDF_AR=new PdfExport();
									sOutputAs=this.OutputDirectory + sReportType + DateTime.Now.ToString("yymmddhhss")+".pdf";
									objPDF_AR.Export(obj300A_2003_AR.Document,sOutputAs);
								}
								break;
							}
							case 4:
							{
								obj301_2004_AR = new OSHA301_2004(m_iClientId) ;
								obj301_2004_AR.ReportCollection=arrlstDDPDF;
								obj301_2004_AR.Notify=this.Notify;
								Declarations.CheckAbort(m_objNotify,m_iClientId);
								obj301_2004_AR.Run();
								objPDF_AR=new PdfExport();
								sOutputAs=this.OutputDirectory + sReportType + DateTime.Now.ToString("yymmddhhss")+".pdf";
								objPDF_AR.Export(obj301_2004_AR.Document,sOutputAs);
								break;
							}
							case 5:
							{
								objSharpLog_AR = new SharpsLogReport(m_iClientId) ;
								objSharpLog_AR.ReportCollection=arrlstDDPDF;
								objSharpLog_AR.Notify=this.Notify;
								Declarations.CheckAbort(m_objNotify,m_iClientId);
								objSharpLog_AR.Run();
								objPDF_AR=new PdfExport();
								sOutputAs=this.OutputDirectory + sReportType + DateTime.Now.ToString("yymmddhhss")+".pdf";
								objPDF_AR.Export(objSharpLog_AR.Document,sOutputAs);
								break;
							}

						}
						break;
					}
					
					default :
					{
                        throw new RMAppException(Globalization.GetString("Oshamanager.RunReport.UnknownReportFormat", m_iClientId));						
					}
				}
				if(sOutputAs!=null && sOutputAs!="")
				{
					if(this.ReportOutput!=null)
						this.ReportOutput.Add(sOutputAs);
				}
			}
			catch(AbortRequestedException p_objException)
			{
                this.Errors.Add("RunReport", p_objException.Message,m_iClientId);
                throw p_objException;
			}
			catch(RMAppException p_objException)
			{
                this.Errors.Add("RunReport", p_objException.Message,m_iClientId);
                throw p_objException;
			}
			
			catch(Exception p_objException)
			{
                this.Errors.Add("RunReport", p_objException.Message,m_iClientId);
                throw new RMAppException(Globalization.GetString("OshaManager.Runreport.GeneralError", m_iClientId), p_objException);
			}	
			finally
			{
				objRpt=null;
				arrlstDDPDF=null;				
				if(obj301_2004_AR!=null)
				{
					obj301_2004_AR.Dispose();
					obj301_2004_AR=null;
				}
				if( obj300A_2004_AR!=null)
				{
					obj300A_2004_AR.Dispose();
					obj300A_2004_AR=null;
				}
				if(obj300_2004_AR!=null)
				{
					obj300_2004_AR.Dispose();
					obj300_2004_AR=null;
				}
				if(obj300A_2003_AR!=null)
				{
					obj300A_2003_AR.Dispose();
					obj300A_2003_AR=null;
				}
				if(obj300_2003_AR!=null)
				{
					obj300_2003_AR.Dispose();
					obj300_2003_AR=null;
				}
				if(objSharpLog_AR!=null)
				{
					objSharpLog_AR.Dispose();
					objSharpLog_AR=null;
				}
				if(objPDF_AR!=null)
				{
					objPDF_AR.Dispose();
					objPDF_AR=null;
				}
				this.m_objInstance=null;
				m_arrlstReports=null;
				m_objCriteriaXML=null;
				m_objPreparer=null;
				m_objColErrors=null;
				m_objNotify=null;
				
			}
			
		}
		/// <summary>
		/// This function would fetch the department id for the person involved depending upon the 
		/// filter event id and/or filter claim id, if specified.		
		/// </summary>
		protected virtual void GenerateFilteredReports()
		{
			//MITS 14844: Raman Bhatia
            //Multiple Employee Involved can be attached to an event
            //int iEid=0;
            ArrayList iEid = new ArrayList();
            int iOshaRecordable = 0;
            ArrayList arrlstCol = new ArrayList();
			ReportItem objItem=null;
			try
			{
				if(!(Convert.ToBoolean(this.FilterClaimId) || Convert.ToBoolean(this.FilterEventId)))
				{
					return;
				}
				if(Convert.ToBoolean(this.FilterClaimId)) 
				{
					iEid = m_objInstance.GetMultipleInt("DEPT_ASSIGNED_EID", "CLAIM, PERSON_INVOLVED", "CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID AND CLAIM.CLAIM_ID=" + this.FilterClaimId);
                    
				}
				else
				{
					if(Convert.ToBoolean(this.FilterEventId))
					{
                        iEid = m_objInstance.GetMultipleInt("DEPT_ASSIGNED_EID", "EVENT, PERSON_INVOLVED", "EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID AND EVENT.EVENT_ID = " + this.FilterEventId);
                    }
				}


                foreach(int iItem in iEid)
                {
                    //MITS 19659 : Raman Bhatia: 02/01/2010
                    //if "Event Osha Recordable" checkbox is checked then all employees involved data should be visible in OSHA 301 report
                    //if "Event Osha Recordable" checkbox is unchecked the all employees involved whose "OSHA recordable" flag is set should be visible in OSHA 301 report.

                    if (this.EventBasedFlag)
                    {
                        objItem = new ReportItem();
                        arrlstCol.Add(iItem);
                        objItem.ReportOnEID = iItem;
                        objItem.SelectedEntities = arrlstCol;
                        this.Reports.Add(objItem);
                    }
                    else
                    {
                        iOshaRecordable = m_objInstance.GetSingleInt("OSHA_REC_FLAG", "EVENT, PERSON_INVOLVED", "EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID AND EVENT.EVENT_ID = " + this.FilterEventId + " AND DEPT_ASSIGNED_EID = " + iItem.ToString());
                        if (iOshaRecordable == -1)
                        {
                            objItem = new ReportItem();
                            arrlstCol.Add(iItem);
                            objItem.ReportOnEID = iItem;
                            objItem.SelectedEntities = arrlstCol;
                            this.Reports.Add(objItem);
                        }
                    }
                    
                    /*
                    if (iEid.Count > 1)
                    {
                        iOshaRecordable = m_objInstance.GetSingleInt("OSHA_REC_FLAG", "EVENT, PERSON_INVOLVED", "EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID AND EVENT.EVENT_ID = " + this.FilterEventId + " AND DEPT_ASSIGNED_EID = " + iItem.ToString());
                        if (iOshaRecordable == -1)
                        {
                            objItem = new ReportItem();
                            arrlstCol.Add(iItem);
                            objItem.ReportOnEID = iItem;
                            objItem.SelectedEntities = arrlstCol;
                            this.Reports.Add(objItem);
                        }
                    }
                    else
                    {
                        objItem = new ReportItem();
                        arrlstCol.Add(iItem);
                        objItem.ReportOnEID = iItem;
                        objItem.SelectedEntities = arrlstCol;
                        this.Reports.Add(objItem);
                    }
                     */
                }
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OshaManager.GenerateFilteredReports.Error", m_iClientId), p_objException);
			}				
			finally
			{
				arrlstCol=null;
				objItem=null;				
			}

		}
		
		/// <summary>
		/// This function would fetch the data for all the entities selected for showing in the report.
		/// </summary>
		protected virtual void GenerateIncludedReports()
		{
			string sTemp="";
			string sSql="";
			string sCurr="";
			string sPrev="";
            int OSHA_f = 0;
            int OSHA_p = 0;
            string sOSHA = "";
			ArrayList arrlstCol=null;
			DbReader objReader=null;
			ReportItem objItem=null;
			try
			{
				//This part of code has not been tested because of lack of data.
				if(this.ByOSHAEstablishmentFlag)
				{
					if(this.AllEntitiesFlag)
					{
						sTemp = "";
						sTemp = sTemp + "SELECT ENTITY_ID FROM ENTITY WHERE ENTITY_TABLE_ID = " + m_objInstance.GetTableID("OSHA_ESTABLISHMENT");
					}
					else
					{
						foreach(string vTmp in this.SelectedEntities)
						{
                            Common.Utilities.AddDelimited(ref sTemp, vTmp, null, m_iClientId);
						}
            
					}
					if(sTemp!="")
					{
						sTemp = " (" + sTemp + ")";
					}
					else
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.GenerateIncludedReports.NoEntriesSelect", m_iClientId));					
					}
      
					sSql = "";
					sSql = sSql + "SELECT DISTINCT ENTITY.LAST_NAME, ENTITY.ABBREVIATION"+
						", ENTITY_ID  USEREPORTLEVEL, ENTITY_ID  REPORTLEVEL"+
						" FROM ENTITY WHERE ENTITY_ID IN " + sTemp+
						" ORDER BY ENTITY.LAST_NAME, ENTITY.ABBREVIATION";
				}
				else
				{
					if(this.AllEntitiesFlag)
					{
                        //jtodd22 12/06/2004--MITS 4851(printing current reports for places with past "Effective Dates")
                        sSql = "";
                        sSql = sSql + "SELECT PARM_NAME FROM PARMS_NAME_VALUE";
                        sSql = sSql + " WHERE PARM_NAME = 'OSHA_F' OR PARM_NAME = 'OSHA_P'";

                        objReader = DbFactory.GetDbReader(m_sDsn, sSql);

                        OSHA_f = 0;
                        OSHA_p = 0;
                        while (objReader.Read())
                        {
                            sOSHA = objReader.GetString("PARM_NAME");
                            switch (sOSHA)
                            {
                                case "OSHA_F":
                                    OSHA_f = 1;
                                    break;
                                case "OSHA_P":
                                    OSHA_p = 1;
                                    break;
                                default:
                                    break;
                            }
                        }

                        objReader.Close();
						//Get the entities at the "Use report" level.
						
                        if (this.EstablishmentNamePrefixOrgLevel > 1004)
                        {
                            sSql = "SELECT DISTINCT";
                            sSql = sSql + " ENTITY.LAST_NAME, ENTITY.ABBREVIATION";
                            sSql = sSql + ", " + m_sUseReportLevel + " USEREPORTLEVEL ," + m_sReportLevel + " REPORTLEVEL";
                            sSql = sSql + ", ENTITY_PREFIX.LAST_NAME PREFIX_LAST_NAME";
                            sSql = sSql + " FROM ORG_HIERARCHY, ENTITY, ENTITY ENTITY_PREFIX";
                            sSql = sSql + " WHERE ENTITY.ENTITY_ID = ORG_HIERARCHY." + m_sUseReportLevel;
                            sSql = sSql + " AND ENTITY_PREFIX.ENTITY_ID = ORG_HIERARCHY." + Declarations.OrgLevelToEIDColumn(this.EstablishmentNamePrefixOrgLevel,m_iClientId);
                            sSql = sSql + " AND ENTITY_PREFIX.ENTITY_TABLE_ID = " + this.EstablishmentNamePrefixOrgLevel;
                            if (OSHA_f + OSHA_p > 0)
                            {           //jtodd22 12/06/2004--one client
                                if (OSHA_f == 1)
                                    //jtodd22 this excludes all entities with an EFF_END_DATE before the report end date
                                    sSql = sSql + " AND (ENTITY.EFF_END_DATE IS NULL OR ENTITY.EFF_END_DATE > " + this.EndDate + ")";

                                //jtodd22 OSHA_p is not needed as we are excluding entities, not including entities
                                //jtodd22 OSHA_p would be needed only to include entities open part of report time frame.
                                //If OSHA_p = 1 Then
                                //                    SQL = SQL & " AND EFF_END_DATE IS NULL OR EFF_END_DATE > " & Me.BeginDate & " AND EFF_END_DATE < " & Me.EndDate
                                //                End If
                            }
                            sSql = sSql + " ORDER BY PREFIX_LAST_NAME, ENTITY.LAST_NAME, ENTITY.ABBREVIATION";
                        }
                        else
                        {
                            if (OSHA_f + OSHA_p == 0)          //jtodd22 12/06/2004--all but one client
                            {
                                sSql = "SELECT DISTINCT";
                                sSql = sSql + " ENTITY.LAST_NAME, ENTITY.ABBREVIATION";
                                sSql = sSql + ", " + m_sUseReportLevel + "  USEREPORTLEVEL ," + m_sReportLevel + " REPORTLEVEL";
                                sSql = sSql + " FROM ORG_HIERARCHY, ENTITY";
                                sSql = sSql + " WHERE ENTITY.ENTITY_ID = ORG_HIERARCHY." + m_sUseReportLevel;
                                sSql = sSql + " ORDER BY ENTITY.LAST_NAME, ENTITY.ABBREVIATION";

                            }
                            if (OSHA_f + OSHA_p > 0)
                            {                       //jtodd22 12/06/2004--one client
                                sSql = "SELECT DISTINCT";
                                sSql = sSql + " ENTITY.LAST_NAME, ENTITY.ABBREVIATION";
                                sSql = sSql + ", " + m_sUseReportLevel + " USEREPORTLEVEL ," + m_sReportLevel + " REPORTLEVEL";
                                sSql = sSql + " FROM ORG_HIERARCHY, ENTITY";
                                sSql = sSql + " WHERE ENTITY.ENTITY_ID = ORG_HIERARCHY." + m_sUseReportLevel;
                                if (OSHA_f == 1)
                                {
                                    //jtodd22 this excludes all entities with an EFF_END_DATE before the report end date
                                    sSql = sSql + " AND (EFF_END_DATE IS NULL OR EFF_END_DATE > " + this.EndDate + ")";
                                }
                                //jtodd22 OSHA_p is not needed as we are excluding entities, not including entities
                                //jtodd22 OSHA_p would be needed only to include entities open part of report time frame.
                                //                If OSHA_p = 1 Then
                                //                    SQL = SQL & " AND EFF_END_DATE IS NULL OR EFF_END_DATE > " & Me.BeginDate & " AND EFF_END_DATE < " & Me.EndDate
                                //                End If
                                sSql = sSql + " ORDER BY ENTITY.LAST_NAME, ENTITY.ABBREVIATION";
                            }
                        }
                        /* //Get the entities at the "Use report" level.

                         sSql = "SELECT DISTINCT ENTITY.LAST_NAME, ENTITY.ABBREVIATION, " + m_sUseReportLevel + "  USEREPORTLEVEL ," + m_sReportLevel + "  REPORTLEVEL" +
                             " FROM ORG_HIERARCHY, ENTITY" +
                             " WHERE ENTITY.ENTITY_ID = ORG_HIERARCHY." + m_sUseReportLevel +
                             " ORDER BY ENTITY.LAST_NAME, ENTITY.ABBREVIATION";*/
                    }
					else
					{
						foreach(string vTmp in this.SelectedEntities)
						{
                            Common.Utilities.AddDelimited(ref sTemp, vTmp, null, m_iClientId);
						}
            
						if(sTemp!="")
						{
							sTemp = " (" + sTemp + ")";
						}
						else
						{
                            throw new RMAppException(Globalization.GetString("Oshamanager.GenerateIncludedReports.NoEntitiesSelected", m_iClientId));
						}
						
						sSql = "SELECT DISTINCT ENTITY.LAST_NAME, ENTITY.ABBREVIATION, " + m_sUseReportLevel + "  USEREPORTLEVEL ," + m_sReportLevel + "  REPORTLEVEL FROM ORG_HIERARCHY, ENTITY "+
							" WHERE ENTITY.ENTITY_ID = ORG_HIERARCHY." + m_sUseReportLevel+
							" AND ORG_HIERARCHY." + m_sReportLevel + " IN " + sTemp+
							" ORDER BY ENTITY.LAST_NAME, ENTITY.ABBREVIATION";
					}
				}
			
				objReader = DbFactory.GetDbReader(m_sDsn,sSql);

				bool bReaderState=false;
				if(objReader!=null)
				{
					bReaderState =objReader.Read();
                
					while(bReaderState)
					{
                        sCurr = Declarations.GetStrValFromLong(Conversion.ConvertObjToInt64(objReader.GetValue("USEREPORTLEVEL"), m_iClientId),m_iClientId);
						if((sCurr != sPrev) && (Declarations.GetLongValFromString(sCurr,m_iClientId) !=0))  
						{
							sPrev = sCurr;
							arrlstCol = new ArrayList();
							while((bReaderState) && (sCurr == sPrev))
							{
                                arrlstCol.Add(Declarations.GetStrValFromLong(Conversion.ConvertObjToInt64(objReader.GetValue("REPORTLEVEL"), m_iClientId),m_iClientId));
								bReaderState=objReader.Read();
								if(bReaderState)
								{
                                    sCurr = Declarations.GetStrValFromLong(Conversion.ConvertObjToInt64(objReader.GetValue("USEREPORTLEVEL"), m_iClientId),m_iClientId);
								}
							}	
							objItem = new ReportItem();
							objItem.ReportOnEID=Declarations.GetIntValFromString(sPrev,m_iClientId);
							objItem.SelectedEntities=arrlstCol;
							this.Reports.Add(objItem);						   
						}
						else
						{
							bReaderState=objReader.Read();
						}
					}
				}
				if(this.Reports.Count==0)
				{
                    throw new RMAppException(Globalization.GetString("Oshamanager.GenerateIncludedReports.NoEntitiesUseReportLevel", m_iClientId));
				}

			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OshaManager.GenerateIncludedReports.Error", m_iClientId), p_objException);
			}	
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				arrlstCol=null;				
				objItem=null;
			}
		}
		
		/// <summary>
		/// This function would validate the report criteria(s).
		/// </summary>
		protected virtual void ValidateCriteria()
		{
			try
			{				
				if((this.ReportType == 4) && ((Convert.ToBoolean(this.FilterClaimId)) || (Convert.ToBoolean(this.FilterEventId))))
				{
					return;//OSHA301
				}        
				if(this.YearOfReport == "")
				{
					if(this.BeginDate==DateTime.MinValue)
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.BeginDate", m_iClientId));
					}
					if(this.EndDate==DateTime.MinValue)
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.EndDate", m_iClientId));
					}
					if((this.BeginDate.Year)!=(this.EndDate.Year))
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.DateRange", m_iClientId));
					}
					if(Declarations.GetIntValFromString(Conversion.ToDbDate(this.EndDate),m_iClientId)-Declarations.GetIntValFromString(Conversion.ToDbDate(this.BeginDate),m_iClientId)<=0)
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.InvalidDateRange", m_iClientId));
					}        
					if((this.BeginDate.Year) < 2002)
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.OshaRule", m_iClientId));					
					}
				}
				else
				{
					if(Declarations.GetIntValFromString(this.YearOfReport,m_iClientId) < 2002)
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.OshaRule", m_iClientId));
					}
				}
				if(this.ReportType == 0)
				{
                    throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.NoReportType", m_iClientId));
				}

				if((this.ReportLevel <1005) || (this.ReportLevel > 1012))
				{
                    throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.InvalidReportlevel", m_iClientId));
				}
				if((this.UseReportLevel <1005) || (this.UseReportLevel > 1012))
				{
                    throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.InvalidUseReportlevel", m_iClientId));
				}
				if(this.UseReportLevel > this.ReportLevel)
				{
                    throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.InvalidLevels", m_iClientId));
				}
				if(!this.AllEntitiesFlag && this.SelectedEntities.Count==0)
				{
                    throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.NoEntity", m_iClientId));
				}
				if((this.AsOfDate!=DateTime.MinValue) && (!m_bUseYear))
				{
					if(Declarations.GetIntValFromString(Conversion.ToDbDate(this.AsOfDate),m_iClientId) < Declarations.GetIntValFromString(Conversion.ToDbDate(this.EndDate),m_iClientId))
					{
                        throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.InvalidAsOfDate", m_iClientId));
					}
				}
                if ((Declarations.GetIntValFromString(this.EstablishmentNamePrefix, m_iClientId)) > (this.UseReportLevel))
				{
                    throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.InvalidEstablishmentPrefix", m_iClientId));					
				}
				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OshaManager.ValidateCriteria.ErrorValidate", m_iClientId), p_objException);
			}			
		}
		/// <summary>
		/// This function would load the XML Document with the xml template for the specific OSHA Report to be generated.
		/// </summary>
		private void LoadEmptyXMLTemplate()
		{
			try
			{
				if(m_sXmlLoadPath!=null && m_sXmlLoadPath!="")
				{
					m_objCriteriaXML.Load(m_sXmlLoadPath);
				}

			}			
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OshaManager.LoadEmptyXMLTemplate.Error", m_iClientId), p_objException);
			}	
		}

		
		/// <summary>
		/// This method would read the XML template loaded corresponding to a OSHA Report.
		/// It would set the report criteria(s)/parameter(s) for generating the report, as specified in the XML Template.
		/// </summary>
		/// <param name="p_objCriteriaDom">XML Document loaded with the XML Template corresponding to an OSHA Report</param>
		protected virtual void ParseCriteria(ref XmlDocument p_objCriteriaDom)
		{
			#region for setting criteria...
			XmlNodeList objControlNodes=null;
			bool bUseYear=false;
			bool bUseRange =false;
			bool bUseRelative=false; 
			int iTemp=0;
			XmlElement objEntityElt=null;
			XmlElement objElt=null;
			try
			{
				
				objElt = ((XmlElement)p_objCriteriaDom.SelectSingleNode("//control[@name = 'datemethod' and @checked]"));
				if(objElt!=null)
				{
					switch(objElt.GetAttribute("value").ToLower())
					{
						case "useyear" :
							bUseYear = true;
							m_bUseYear = true;
							break;
						case "userange" :
							bUseRange = true;
							bUseYear = false; //(Implies that we're using a range.)
							m_bUseYear = false;
							break;
						case "userelative" :
							bUseRelative = true;
							this.UseRelDates = true;
							break;
					}
        
				}
				objControlNodes = p_objCriteriaDom.GetElementsByTagName("control");
				if(objControlNodes!=null)
				{
					foreach(XmlElement objTmpElt in objControlNodes)
					{
						switch(objTmpElt.GetAttribute("name").ToLower())
						{
							case "reportlevel" :
							{
								if(objTmpElt.GetAttribute("codeid")!="")//Enhanced functionality,not present in VB Com.
								{
									this.ReportLevel=Declarations.GetIntValFromString(objTmpElt.GetAttribute("codeid"),m_iClientId) ;//any numeric combo box is set as a "codeid" in RequestToXML()
								}
								break;
									
							}
							case "usereportlevel" :
							{
								if(objTmpElt.GetAttribute("codeid")!="")//Enhanced functionality,not present in VB Com.
								{
									this.UseReportLevel = Declarations.GetIntValFromString(objTmpElt.GetAttribute("codeid"),m_iClientId) ;//any numeric combo box is set as a "codeid" in RequestToXML()
								}
								break;
							}
							case "begindate" :
							{
								if(bUseRange)
								{
									this.BeginDate =Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
								}
								break;
							}
							case "enddate" :
							{
								if(bUseRange)
								{
									this.EndDate = Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
								}
								break;
							}
							case "allentitiesflag" :
							{
								objElt = ((XmlElement)p_objCriteriaDom.SelectSingleNode("//control[@name='allentitiesflag' and @checked]"));
								if(objElt!=null)
								{
									this.AllEntitiesFlag =Declarations.GetBoolValFromXmlAttribute(objElt.GetAttribute("value"),m_iClientId);
									objElt=null;
								}
								break;
							}
							case "selectedentities" :
							{
								m_arrlstEntities = new ArrayList();
								foreach(XmlElement objTemp in objTmpElt.ChildNodes)
								{
									this.SelectedEntities.Add(Declarations.GetStrValFromXmlAttribute(objTemp.GetAttribute("value"),m_iClientId));
								}
								break;
							}
							case "yearofreport" :
							{
								if(bUseYear)
								{
									this.YearOfReport =Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("codeid"),m_iClientId);
								}
								break;
							}
							case "eventbasedflag" :
							{
                                //MITS 19659 : Raman Bhatia: 02/01/2010
                                //if "Event Osha Recordable" checkbox is checked then all employees involved data should be visible in OSHA 301 report
                                //if "Event Osha Recordable" checkbox is unchecked the all employees involved whose "OSHA recordable" flag is set should be visible in OSHA 301 report.
                                //this.EventBasedFlag = Declarations.IsChecked(objTmpElt);
                                objElt = ((XmlElement)p_objCriteriaDom.SelectSingleNode("//control[@name='eventbasedflag' and @checked]"));
                                if (objElt != null)
                                {
                                    this.EventBasedFlag = Declarations.GetBoolValFromXmlAttribute(objElt.GetAttribute("checked"),m_iClientId);
                                    objElt = null;
                                }
                                break;
							}
							case "printsofterrlog" :
							{
								this.PrintSoftErrLog = Declarations.IsChecked(objTmpElt,m_iClientId);
								break;
							}
							case "printoshadescflag" :
							{
								this.m_bPrintOSHADescFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
								break;
							}
							case "primarylocationflag" :
							{
								this.m_bPrimaryLocationFlag = Declarations.IsChecked(objTmpElt,m_iClientId);
								break;
							}
							case "timescalar" :
								iTemp=0;
								if(bUseRelative)
								{
									iTemp =Declarations.GetIntValFromString(objTmpElt.GetAttribute("value"),m_iClientId);
									m_iRelDateScalar = iTemp;
									objElt = (XmlElement)p_objCriteriaDom.SelectSingleNode("//control[@name = 'timeunit']");
									m_sRelDateUnit = objElt.GetAttribute("codeid");
									this.BeginDate =Declarations.DateAdd(objElt.GetAttribute("codeid"), -1 * iTemp, DateTime.Today,m_iClientId);
									this.EndDate = DateTime.Today;
									
								}
								break;
							case "preparername" :
							{
								this.Preparer.Name =Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
								break;
							}
							case "preparertitle" :
							{
								this.Preparer.Title = Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
								break;
							}
							case "preparerphone" :
							{
								this.Preparer.Phone = Declarations.GetStrValFromXmlAttribute(objTmpElt.GetAttribute("value"),m_iClientId);
								break;
							}
							case "preparersignaturedate" :
							{
								this.Preparer.SignatureDate = Conversion.ToDate(Conversion.GetDate(objTmpElt.GetAttribute("value")));
								break;
							}
							case "useemployeenames" :
							{
								this.UseEmployeeNames =Declarations.IsChecked(objTmpElt,m_iClientId);
								break;
							}
							case "enforce180dayrule" :
							{
								this.Enforce180DayRule =Declarations.GetBoolValFromXmlAttribute(Declarations.GetXMLValue("enforce180dayrule", p_objCriteriaDom,m_iClientId),m_iClientId);
								break;
							}
							case "asofdate" :
							{
								this.AsOfDate =Conversion.ToDate(Declarations.GetXMLValue("asofdate", p_objCriteriaDom,m_iClientId));
								break;
							}
							case "establishmentnameprefix" :
							{
								this.EstablishmentNamePrefix = Declarations.GetXMLValue("establishmentnameprefix", p_objCriteriaDom,m_iClientId);
								break;
							}
							case "sortorder" :
							{
								this.ItemSortOrder =Declarations.GetIntValFromString(Declarations.GetXMLValue("sortorder", p_objCriteriaDom,m_iClientId),m_iClientId);
								break;
							}
                            //MITS 37193 begin
                            case "columnesource":
                            {
                                this.ColumnESource = Declarations.GetIntValFromString(Declarations.GetXMLValue("columnesource", p_objCriteriaDom,m_iClientId),m_iClientId);
                                break;
                            }
                            //MITS 37193 end

							case "columnfsource" :
							{
								this.ColumnFSource =Declarations.GetIntValFromString(Declarations.GetXMLValue("columnfsource", p_objCriteriaDom,m_iClientId),m_iClientId);
								break;
							}
							case "filtereventid" :
							{
								this.FilterEventId =Declarations.GetIntValFromString(objTmpElt.GetAttribute("value"),m_iClientId);
								break;
							}
							case "filterclaimid" :
							{
								this.FilterClaimId = Declarations.GetIntValFromString(objTmpElt.GetAttribute("value"),m_iClientId);
								break;
							}
							case "byoshaestablishmentflag":
							{
								objElt =((XmlElement)p_objCriteriaDom.SelectSingleNode("//control[@name='byoshaestablishmentflag' and @checked]"));
								if(objElt!=null)
								{
									this.ByOSHAEstablishmentFlag =Declarations.GetBoolValFromXmlAttribute(objElt.GetAttribute("value"),m_iClientId);
								}
								break;
							}
						}
					}
				}
				objControlNodes = p_objCriteriaDom.GetElementsByTagName("internal");
				foreach(XmlElement objTmpElt in objControlNodes)
				{
					switch(objTmpElt.GetAttribute("name"))
					{
						case "reporttype" :
							this.ReportType=Declarations.GetIntValFromString(objTmpElt.GetAttribute("value"),m_iClientId);
							objEntityElt=(XmlElement)p_objCriteriaDom.SelectSingleNode("//report");
							if(objEntityElt==null)
							{
                                throw new RMAppException(Globalization.GetString("OshaManager.ParseCriteria.MissingReportType", m_iClientId));					
							}
							if(this.ReportType!=(Declarations.GetIntValFromString(objEntityElt.GetAttribute("type"),m_iClientId)))
                                throw new RMAppException(Globalization.GetString("OshaManager.ParseCriteria.InconsistentReportType", m_iClientId));					
							break;
					}
				}
				this.m_objCriteriaXML=p_objCriteriaDom;		
			}
				#endregion
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OshaManager.ParseCriteria.ErrorParse", m_iClientId), p_objException);
			}				
			finally
			{
				objControlNodes=null;				
				objEntityElt=null;
				objElt=null;
			}
		}
		/// <summary>
		/// This method would create the instance of specific OSHA Report corresponding to the report type selected.		
		/// It would load the template XML corresponding to that.
		/// </summary>
		/// <param name="p_iEntityId">Reported On EID</param>
		/// <param name="p_arrlstSelectedEntities">Selected Entity</param>
		/// <returns>instance of IOSHAReport</returns>
		protected virtual IOSHAReport CreateReport(int p_iEntityId,ArrayList p_arrlstSelectedEntities)
		{  
			IOSHAReport objReport=null;
			try
			{
				switch(this.ReportType)
				{
					case 1 :
						objReport  = new OSHA300Report(m_sDsn,m_iClientId);
						break;
					case 3 :
						objReport = new OSHA300AReport(m_sDsn,m_iClientId);
						break;
					case 4:
						objReport= new OSHA301Report(m_sDsn,m_iClientId);
						break;
					case 5:
						objReport = new OSHASharpsLog(m_sDsn,m_iClientId);
						break;
					default:
                        throw new RMAppException(Globalization.GetString("OshaManager.CreateReport.UnknownReportType", m_iClientId));
						
				}
				
				objReport.Init();
				objReport.CriteriaXmlDom =this.CriteriaXmlDom;
				objReport.Notify = this.Notify;
				objReport.SelectedEntities=p_arrlstSelectedEntities;
				objReport.ReportOn=p_iEntityId;
				objReport.ByOSHAEstablishmentFlag=this.ByOSHAEstablishmentFlag;

			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OshaManager.CreateReport.ErrorCreate", m_iClientId), p_objException);
			}			
			return objReport;
		}
		#endregion

		#region Osha Recordability Wizard
		public XmlDocument OshaRecordabilityWizard(string p_sEventId,string p_sConnString)
		{
			XmlDocument objXmlOut=null;
			string sSQL=string.Empty;
			string sEventID=string.Empty;
			DbReader objReader=null;
			string sOshaRecFlag=string.Empty;
			string sPIFlags=string.Empty;
			string sLastName=string.Empty;
			string sFirstName=string.Empty;
			string sPINames=string.Empty;
			string sDelimiter="|^%";
			string sOutputXml=string.Empty;

			try
			{
				sEventID=p_sEventId;
				sSQL="SELECT PERSON_INVOLVED.OSHA_REC_FLAG, ENTITY.LAST_NAME, ENTITY.FIRST_NAME " +
					"FROM PERSON_INVOLVED,CODES,ENTITY WHERE PERSON_INVOLVED.PI_TYPE_CODE = CODES.CODE_ID " +
					" AND PERSON_INVOLVED.PI_EID = ENTITY.ENTITY_ID AND (CODES.SHORT_CODE = 'E') AND " +
					" (PERSON_INVOLVED.EVENT_ID=" + sEventID + ")";
                using (objReader = DbFactory.GetDbReader(p_sConnString, sSQL))
                {
                    sPIFlags = "";
                    sPINames = "";
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sOshaRecFlag = objReader.GetValue("OSHA_REC_FLAG").ToString();
                            if (sOshaRecFlag == "0" || sOshaRecFlag == "")
                                sOshaRecFlag = "0";
                            else
                                sOshaRecFlag = "1";
                            sLastName = objReader.GetString("LAST_NAME");
                            sFirstName = objReader.GetString("FIRST_NAME");
                            if (sPINames != "")
                            {
                                sPINames = sPINames + sDelimiter;
                                sPIFlags = sPIFlags + sDelimiter;
                            }
                            sPINames = sPINames + sLastName;
                            if (sFirstName != "")
                            {
                                sPINames = sPINames + "," + sFirstName;
                            }
                            sPIFlags = sPIFlags + sOshaRecFlag;
                        }
                    }
                }
				sOutputXml="<PI><PINames>" + sPINames + "</PINames><PIFlags>" + sPIFlags + "</PIFlags></PI>";
				objXmlOut=new XmlDocument();
				objXmlOut.LoadXml(sOutputXml);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("OshaManager.OshaRecordabilityWizard.ErrorCreate", m_iClientId), p_objException);
			}			
			return objXmlOut;
		}
		#endregion

		#region IReportEngine Members

		public ArrayList ReportOutput
		{
			get
			{
				return m_arrlstReportOutput;
			}
		}

		public int OutputsSupported
		{
			get
			{
				return (int)eReportFormat.Pdf;
				
			}
		}

		public string CriteriaHtml
		{
			get
			{
				return null;
			}
		}

		public IWorkerNotifications WorkerNotifications
		{
			get
			{
				return m_objNotify;
			}
			set
			{
				m_objNotify=value;
			}
		}

		public bool HtmlCriteriaSupported
		{
			get
			{
				// TODO:  Add OshaManager.HtmlCriteriaSupported getter implementation
				return false;
			}
		}

		public bool XmlCriteriaSupported
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Database Connection String
		/// </summary>
		public string ConnectionString
		{
			get
			{
				return m_sDsn; 
			}
			set
			{
				if(value!=null)	
				{
					m_sDsn = value; 
				}
				else
				{
					m_sDsn = string.Empty;
				}
			}
		}

		#endregion
	}
}