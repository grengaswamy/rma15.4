using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.DataModel;



namespace Riskmaster.Application.LeaveLibrary
{
    ///************************************************************** 
    ///* $File				: LeaveHistory.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-31-2007
    ///* $Author			: Aashish Bhateja
    ///***************************************************************	
    /// <summary>	
    ///	Leave History object definition.Used in the update function and 
    /// retrieving leave history.
    /// </summary>
    public class LeaveHistory
	{
		private const string m_sTableName = "CLAIM_LV_X_HIST";

		#region Variables Declaration


        /// <summary>
        /// Root tag
        /// </summary>
        private const string ROOT_TAG = "<LeaveHistory></LeaveHistory>";

        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;






        //private string m_sConnectionString="";
		/// <summary>
		/// Leave History Row Id
		/// </summary>
		private int m_iLHRowId=0; 
		/// <summary>
		/// Leave Plan Row Id
		/// </summary>
		private int m_iLPRowId=0; 
		/// <summary>
        /// EmployeeEid
		/// </summary>
		private int m_iEmployeeEid=0; 
		/// <summary>
		/// Eligibility Date.
		/// </summary>
		private string m_sDateElig=""; 
		/// <summary>
		/// Time frame start date.
		/// </summary>
		private string m_sDateTFStart=""; 
		/// <summary>
        /// Time frame end date.
		/// </summary>
		private string m_sDateTFEnd=""; 
		/// <summary>
		/// Added by user.
		/// </summary>
		private string m_sAddedByUser=""; 
		/// <summary>
		/// Date time record added.
		/// </summary>
		private string m_sDttmRcdAdded=""; 
		/// <summary>
		/// Updated by user.
		/// </summary>
		private string m_sUpdatedByUser=""; 
		/// <summary>
		/// Last updated.
		/// </summary>
		private string m_sDttmRcdLastUpd=""; 
		/// <summary>
		/// Hours worked.
		/// </summary>
		private double m_dHoursWorked=0.0; 
		/// <summary>
		/// Hours used.
		/// </summary>
		private double m_dHoursUsed=0.0;

        private int m_iClientId = 0;
		#endregion

		#region Properties Declaration
		/// <summary>
		/// Leave History Row Id.
		/// </summary>
		public int LHRowId
		{
			get
			{
				return m_iLHRowId;
			}
			set
			{
				m_iLHRowId=value;
			}
		}
		/// <summary>
		/// Leave Plan Row Id.
		/// </summary>
		public int LPRowId
		{
			get
			{
				return m_iLPRowId;
			}
			set
			{
				m_iLPRowId=value;
			}
		}
		/// <summary>
        /// EmployeeEid.
		/// </summary>
		public int EmployeeEid
		{
			get
			{
				return m_iEmployeeEid;
			}
			set
			{
				m_iEmployeeEid=value;
			}
		}
		/// <summary>
		/// Eligibility Date
		/// </summary>
		public string DateElig
		{
			get
			{
				return m_sDateElig;
			}
			set
			{
				m_sDateElig=value;
			}
		}
		/// <summary>
		/// Time frame start Date.
		/// </summary>
		public string DateTFStart
		{
			get
			{
				return m_sDateTFStart;
			}
			set
			{
				m_sDateTFStart=value;
			}
		}
		/// <summary>
        /// Time frame end Date.
		/// </summary>
		public string DateTFEnd
		{
			get
			{
				return m_sDateTFEnd;
			}
			set
			{
				m_sDateTFEnd=value;
			}
		}
		/// <summary>
        /// Added By User.
		/// </summary>
		public string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser=value;
			}
		}
		/// <summary>
		/// Date time added.
		/// </summary>
		public string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded=value;
			}
		}
		/// <summary>
        /// Updated By User.
		/// </summary>
		public string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser=value;
			}
		}
		/// <summary>
		/// Date time last updated.
		/// </summary>
		public string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd=value;
			}
		}
		/// <summary>
        /// Hours Worked.
		/// </summary>
		public double HoursWorked
		{
			get
			{
				return m_dHoursWorked;
			}
			set
			{
				m_dHoursWorked=value;
			}
		}
		/// <summary>
        /// Hours Used.
		/// </summary>
		public double HoursUsed
		{
			get
			{
				return m_dHoursUsed;
			}
			set
			{
				m_dHoursUsed=value;
			}
		}
		#endregion


        /// <summary>
        /// Initialize objects
        /// </summary>
        private void Initialize()
        {
            try
            {
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LeaveHistory.Initialize.ErrorInit", m_iClientId), p_objEx);
            }
        }

        /// Name		    :   GetLeaveHistory
        /// Author		    :   Geeta Sharma
        /// Date Created	:   2 Jan 2007
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author              
        /// ************************************************************
        /// <summary>
        /// Creats a new XMLDocument with the Employee RowId passed as parameter.         
        /// </summary>
        /// <param name="p_iEmpRowId">Employee Row Id</param>  
        /// <returns>Leave History XML Document</returns>  
        public XmlDocument GetLeaveHistory(int p_iEmpRowId)
        {
            XmlDocument objXMLDocument = new XmlDocument();
            XmlElement objXMLElement = null;
            XmlElement objXMLChildElement = null;
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            int iEmpId;

            string sLeaveAuthCode = string.Empty;
            string sLeaveStatusCode = string.Empty;
            string sLeaveTypeCode = string.Empty;
            string sLeaveReasonCode = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sCaption = string.Empty;
            string sStartDate = string.Empty;
            string sEndDate = string.Empty;

            LocalCache objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);
            Generic objGeneric = new Generic(m_sConnectionString, m_iClientId);

            //Get the Employee Id            
            iEmpId = objGeneric.GetPiEmpID(p_iEmpRowId);

            sSQL = "SELECT CLAIM_LEAVE.CLAIM_ID,DATE_LEAVE_START, DATE_LEAVE_END, LEAVE_REASON_CODE, ";
            sSQL = sSQL + "LEAVE_TYPE_CODE, HOURS_LEAVE, LEAVE_AUTH_CODE, LD_ROW_ID, ";
            sSQL = sSQL + "LEAVE_STATUS_CODE, LEAVE_PLAN_CODE, CLAIM_NUMBER, ";
            sSQL = sSQL + "ENTITY.FIRST_NAME, ENTITY.LAST_NAME ";
            sSQL = sSQL + "FROM ((((CLAIM_LEAVE INNER JOIN CLAIM_LV_X_DETAIL ";
            sSQL = sSQL + "ON CLAIM_LEAVE.LEAVE_ROW_ID = CLAIM_LV_X_DETAIL.LEAVE_ROW_ID) ";
            sSQL = sSQL + "INNER JOIN LEAVE_PLAN ON CLAIM_LEAVE.LP_ROW_ID = LEAVE_PLAN.LP_ROW_ID) ";
            sSQL = sSQL + "INNER JOIN CLAIM ON CLAIM_LEAVE.CLAIM_ID = CLAIM.CLAIM_ID) ";
            sSQL = sSQL + "INNER JOIN CLAIMANT ON CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID) ";
            sSQL = sSQL + "INNER JOIN ENTITY ON CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID ";
            sSQL = sSQL + "WHERE (((CLAIMANT.CLAIMANT_EID)=" + iEmpId + ") AND ";
            sSQL = sSQL + "((CLAIMANT.PRIMARY_CLMNT_FLAG)=-1))";

            try
            {
                objXMLDocument.LoadXml(ROOT_TAG);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("LeaveHistory");

                using (objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objDbReader.Read())
                    {
                        objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("option"), false);
                        
                        //Geeta 10/01/07 : MITS 10455
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimId", objDbReader["CLAIM_ID"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("ClaimNumber", objDbReader["CLAIM_NUMBER"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("PlanCode", objDbReader["LEAVE_PLAN_CODE"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("DateLeaveStart", objDbReader["DATE_LEAVE_START"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("DateLeaveEnd", objDbReader["DATE_LEAVE_END"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("HoursLeave", objDbReader["HOURS_LEAVE"].ToString()), true));

                        //Get the description for code fields
                        sLeaveTypeCode = objLocalCache.GetCodeDesc(Convert.ToInt32(objDbReader["LEAVE_TYPE_CODE"].ToString()));
                        sLeaveReasonCode = objLocalCache.GetCodeDesc(Convert.ToInt32(objDbReader["LEAVE_REASON_CODE"].ToString()));
                        sLeaveAuthCode = objLocalCache.GetCodeDesc(Convert.ToInt32(objDbReader["LEAVE_AUTH_CODE"].ToString()));
                        sLeaveStatusCode = objLocalCache.GetCodeDesc(Convert.ToInt32(objDbReader["LEAVE_STATUS_CODE"].ToString()));

                        sFirstName = objDbReader["FIRST_NAME"].ToString();
                        sLastName = objDbReader["LAST_NAME"].ToString();

                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LeaveTypeCode", sLeaveTypeCode), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LeaveReasonCode", sLeaveReasonCode), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LeaveAuthCode", sLeaveAuthCode), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LeaveStatusCode", sLeaveStatusCode), true));

                        objXMLElement.AppendChild(objXMLChildElement);
                    }
                }
                //Get the Leave Plan Summary 				 
                sEndDate = Conversion.ToDbDate(DateTime.Now).ToString();
                sStartDate = Conversion.ToDbDate(DateTime.Now.AddMonths(-12)).ToString();

                sSQL = "SELECT LEAVE_PLAN_CODE, LEAVE_TYPE_CODE, ";
                sSQL = sSQL + "Sum(CLAIM_LV_X_DETAIL.HOURS_LEAVE) AS SumOfHOURS_LEAVE ";
                sSQL = sSQL + "FROM CLAIMANT INNER JOIN ";
                sSQL = sSQL + "((LEAVE_PLAN INNER JOIN (CLAIM_LV_X_DETAIL INNER JOIN ";
                sSQL = sSQL + "CLAIM_LEAVE ON CLAIM_LV_X_DETAIL.LEAVE_ROW_ID = CLAIM_LEAVE.LEAVE_ROW_ID) ";
                sSQL = sSQL + "ON LEAVE_PLAN.LP_ROW_ID = CLAIM_LEAVE.LP_ROW_ID) ";
                sSQL = sSQL + "INNER JOIN CLAIM ON CLAIM_LEAVE.CLAIM_ID = CLAIM.CLAIM_ID) ";
                sSQL = sSQL + "ON CLAIMANT.CLAIM_ID = CLAIM.CLAIM_ID ";
                sSQL = sSQL + "WHERE (((CLAIMANT.CLAIMANT_EID)=" + iEmpId + ") AND ";
                sSQL = sSQL + "((CLAIMANT.PRIMARY_CLMNT_FLAG)=-1) AND ";
                sSQL = sSQL + "((CLAIM_LV_X_DETAIL.DATE_LEAVE_START)>=" + sStartDate + ") ";
                sSQL = sSQL + "AND ((CLAIM_LV_X_DETAIL.DATE_LEAVE_END)<=" + sEndDate + ")) ";
                sSQL = sSQL + "GROUP BY LEAVE_PLAN.LEAVE_PLAN_CODE, CLAIM_LV_X_DETAIL.LEAVE_TYPE_CODE";

                using (objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objDbReader.Read())
                    {
                        objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("LeaveSummary"), false);
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("Hours", objDbReader["SumOfHOURS_LEAVE"].ToString()), true));
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("PlanCode", objDbReader["LEAVE_PLAN_CODE"].ToString()), true));

                        sLeaveTypeCode = objLocalCache.GetCodeDesc(Convert.ToInt32(objDbReader["LEAVE_TYPE_CODE"].ToString()));

                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(GetNewEleWithValue("LeaveType", sLeaveTypeCode), true));

                        objXMLElement.AppendChild(objXMLChildElement);
                    }
                }

                if (sFirstName != "")
                {
                    sCaption = "Leave History for [" + sFirstName + " " + sLastName + "]";
                }
                else
                {
                    sCaption = "Leave History for [" + sLastName + "]";
                }
                objXMLDocument.DocumentElement.SetAttribute("LeaveHistoryTitle", sCaption);
                sCaption = "Leave Plan Summary for Past 12 Months (" + Conversion.GetDBDateFormat(sStartDate, "MM/dd/yyyy") + " - " + Conversion.GetDBDateFormat(sEndDate, "MM/dd/yyyy") + ")";
                objXMLDocument.DocumentElement.SetAttribute("LeaveSummaryTitle", sCaption);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LeaveHistory.GetLeaveHistory.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objGeneric = null;                
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();
            }

            return objXMLDocument;
        }

        /// Name		    :   GetNewEleWithValue
        /// Author		    :   Geeta Sharma
        /// Date Created	:   2 Jan 2007
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author              
        /// ************************************************************
        /// <summary>
        /// Creats a new XMLElement with the nodename passed as parameter. 
        /// Also sets the Innertext to the new XMLElement
        /// </summary>
        /// <param name="p_sNewNodeName">New Node to be Created</param>
        /// <param name="p_sNodeValue">Value for the node</param>
        /// <returns>New XML Element with Innertext set</returns>
        internal static XmlElement GetNewEleWithValue(string p_sNewNodeName, string p_sNodeValue)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;
            objXDoc = new XmlDocument();
            objXMLElement = objXDoc.CreateElement(p_sNewNodeName);
            objXMLElement.InnerText = p_sNodeValue;
            objXDoc = null;
            return objXMLElement;
        }

        /// <summary>
        /// Creates a SQL Query.
        /// </summary>
        /// <returns></returns>
		private string GetSQLFieldList()
		{
			string sSQL = ""; 

			sSQL = "";
			sSQL = sSQL + "SELECT";
			sSQL = sSQL + " LP_ROW_ID, LH_ROW_ID";
			sSQL = sSQL + ", DTTM_RCD_ADDED,ADDED_BY_USER";
			sSQL = sSQL + ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER";
			sSQL = sSQL + ", EMPLOYEE_EID,DATE_ELIGIBILITY";
			sSQL = sSQL + ", DATE_TF_START";
			sSQL = sSQL + ", DATE_TF_END";
			sSQL = sSQL + ", HOURS_WORKED";
			sSQL = sSQL + ", HOURS_USED";
			return sSQL;
		}

        /// <summary>
        /// Loads leave history data.
        /// </summary>
        /// <param name="p_iEmpID"></param>
        /// <param name="p_iPlanID"></param>
        /// <returns></returns>
		public bool LoadData(int p_iEmpID,int p_iPlanID)
		{    
			//Const sFunctionName As String = "LoadData"
			bool bRetVal = false;
			string sSQL = string.Empty;
			DbConnection objConn = null; 
			DbReader objReader = null;
		    
			try
			{
				sSQL = "";
				sSQL = "SELECT * FROM " + m_sTableName;
				sSQL = sSQL + " WHERE EMPLOYEE_EID=" + p_iEmpID + " AND LP_ROW_ID=" + p_iPlanID;
		    
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        bRetVal = true;
                        m_iLPRowId = objReader.GetInt32("LP_ROW_ID");
                        m_iEmployeeEid = objReader.GetInt32("EMPLOYEE_EID");
                        m_sDateElig = objReader.GetString("DATE_ELIGIBILITY");
                        m_sDateTFStart = objReader.GetString("DATE_TF_START");
                        m_sDateTFEnd = objReader.GetString("DATE_TF_END");
                        m_dHoursWorked = objReader.GetDouble("HOURS_WORKED");
                        m_dHoursUsed = objReader.GetDouble("HOURS_USED");
                        m_sAddedByUser = objReader.GetString("ADDED_BY_USER");
                        m_sDttmRcdAdded = objReader.GetString("DTTM_RCD_ADDED");
                        m_sUpdatedByUser = objReader.GetString("UPDATED_BY_USER");
                        m_sDttmRcdLastUpd = objReader.GetString("DTTM_RCD_LAST_UPD");
                    }                    
                }
			}				
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LeaveHistory.LoadData.Err", m_iClientId), p_objEx);
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
			}
			return bRetVal;
		}

        /// <summary>
        /// Saves leave history data.
        /// </summary>
        /// <returns></returns>
        public bool SaveData()
		{    
			//Const sFunctionName As String = "LoadData"
			bool bRetVal=false;
			bool bUpdate=false;
			string sSQL = string.Empty;
			DbConnection objConn = null; 
			DbReader objReader = null;
			DbWriter objDbWriter = null;
		    
			try
			{
				//Pull Leave History records
				sSQL = GetSQLFieldList();
				sSQL = sSQL + " FROM CLAIM_LV_X_HIST";
				sSQL = sSQL + " WHERE LP_ROW_ID = " + m_iLPRowId;
				sSQL = sSQL + " AND EMPLOYEE_EID = " + m_iEmployeeEid;
		    
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                        bUpdate = true;
                }
				if(objConn!=null)
					objConn.Dispose();

				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				
				objDbWriter = DbFactory.GetDbWriter(objConn);

				objDbWriter.Tables.Add("CLAIM_LV_X_HIST");
				objDbWriter.Fields.Add("DTTM_RCD_LAST_UPD",this.DttmRcdLastUpd);
				objDbWriter.Fields.Add("UPDATED_BY_USER",this.UpdatedByUser);
				objDbWriter.Fields.Add("LP_ROW_ID",this.LPRowId);
				objDbWriter.Fields.Add("EMPLOYEE_EID",this.EmployeeEid);					
				objDbWriter.Fields.Add("DATE_ELIGIBILITY",this.DateElig);
				objDbWriter.Fields.Add("DATE_TF_START",this.DateTFStart);
				objDbWriter.Fields.Add("DATE_TF_END",this.DateTFEnd);
				objDbWriter.Fields.Add("HOURS_USED",this.HoursUsed);
				objDbWriter.Fields.Add("HOURS_WORKED",this.HoursWorked);

				if(bUpdate)
				{
					// Update existing record				
					objDbWriter.Where.Add(" LP_ROW_ID=" + m_iLPRowId);    
					objDbWriter.Where.Add(" AND EMPLOYEE_EID=" + m_iEmployeeEid);    
				}
				else
				{
					// Insert new record
                    m_iLHRowId = Utilities.GetNextUID(m_sConnectionString, "CLAIM_LV_X_HIST", m_iClientId);
					objDbWriter.Fields.Add("LH_ROW_ID",m_iLHRowId);
					objDbWriter.Fields.Add("ADDED_BY_USER",m_sAddedByUser);
					objDbWriter.Fields.Add("DTTM_RCD_ADDED",m_sDttmRcdAdded);
				}
				objDbWriter.Execute(); 
				bRetVal=true;
			}				
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LeaveHistory.SaveData.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objConn!=null)
				{				
					objConn.Close();
					objConn.Dispose();
				}
				objDbWriter = null;
			}
			return bRetVal;

		}

        #region Get new element
        /// Name		    :   GetNewElement
        /// Author		    :   Geeta Sharma
        /// Date Created	:   2 Jan 2007
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author              
        /// ************************************************************
        /// <summary>
        /// Creats a new XmlElement with the Node Name passed as parameter.         
        /// </summary>
        /// <param name="p_sNodeName">Node Name</param>  
        /// <returns>XMLElement to be added in the Xml Document</returns>
        private XmlElement GetNewElement(string p_sNodeName)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;

            try
            {
                objXDoc = new XmlDocument();
                objXMLElement = objXDoc.CreateElement(p_sNodeName);
                return objXMLElement;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("LeaveHistory.GetNewElement.XMLError", m_iClientId), p_objException);
            }
            finally
            {
                objXDoc = null;
                objXMLElement = null;
            }
        }
        #endregion

        /// <summary>
        /// Constructor used for datamodel case while using GetLeaveHistory().
        /// </summary>
        /// <param name="p_sDsnName"></param>
        /// <param name="p_sUserName"></param>
        /// <param name="p_sPassword"></param>
        public LeaveHistory(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            this.Initialize();
        }		

        /// <summary>
        /// Constructor when connection string is the parameter.
        /// </summary>
        /// <param name="p_sConnectionString"></param>
        public LeaveHistory(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
		}

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
                m_objDataModelFactory.Dispose();
        }
	}
}
