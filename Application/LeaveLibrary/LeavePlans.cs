using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.LeaveLibrary
{
    ///************************************************************** 
    ///* $File				: LeavePlans.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-31-2007
    ///* $Author			: Aashish Bhateja
    ///***************************************************************	
    /// <summary>	
    ///	Leave Plan collection definition - collection of Leave Plans for LeaveEmp.
    /// </summary>
    public class LeavePlans
	{
		/// <summary>
		/// Holds the connection string.
		/// </summary>
        private string m_sConnectionString="";
		
        /// <summary>
        /// Holds the plans collection.
        /// </summary>
        public Hashtable m_ColPlans = new Hashtable();

        private int m_iClientId = 0;

        /// <summary>
        /// Returns leave plan count.
        /// </summary>
		public int Count
		{
			get
			{
				return this.m_ColPlans.Count;
			}
		}	

	    /// <summary>
	    /// Used to get a particular leave plan based on the key value.
	    /// </summary>
	    /// <param name="keyValue"></param>
	    /// <returns></returns>
		public LeavePlan Get(string keyValue)
		{
			return (LeavePlan)this.m_ColPlans[keyValue];
		}		

        /// <summary>
        /// Used to get a plan collection.
        /// </summary>
        /// <returns></returns>
        public string[] Keys()
        {
            string[] KeysColl=new string[m_ColPlans.Keys.Count];
            int i = 0;
            foreach (string Key in m_ColPlans.Keys)
            {
                KeysColl.SetValue(Key, i++);
            }
            return KeysColl;
        }
        
        /// <summary>
        /// Used to delete a particular plan from the collection.
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
		public bool Delete(int keyValue)
		{
			if(!m_ColPlans.ContainsKey(keyValue))
				return false;

			m_ColPlans.Remove(keyValue);
			return true;
		}
        /// <summary>
        /// Adds a plan to the collection.
        /// </summary>
        /// <param name="p_iLPRowId"></param>
        /// <param name="p_sLPName"></param>
        /// <param name="p_iLPType"></param>
        /// <param name="p_sLeavePlanCode"></param>
        /// <param name="p_iDefaultFlag"></param>
        /// <param name="p_iAllowedNumber"></param>
        /// <param name="p_iAllowedPrdType"></param>
        /// <param name="p_iPerNumber"></param>
        /// <param name="p_iPerPrdType"></param>
        /// <param name="p_dHoursPerWeek"></param>
        /// <param name="p_iEligBeneNumber"></param>
        /// <param name="p_iBenePrdType"></param>
        /// <param name="p_iBeneFirstFlag"></param>
        /// <param name="p_iUseLeavesFlag"></param>
        /// <param name="p_dEligHours"></param>
        /// <param name="p_sEffDate"></param>
        /// <param name="p_sExpDate"></param>
        /// <param name="p_sKey"></param>
        /// <returns></returns>
		public bool Add(int p_iLPRowId,string p_sLPName,int p_iLPType,string p_sLeavePlanCode,int p_iDefaultFlag,int p_iAllowedNumber,int p_iAllowedPrdType,int p_iPerNumber,int p_iPerPrdType,double p_dHoursPerWeek,int p_iEligBeneNumber,int p_iBenePrdType,int p_iBeneFirstFlag,int p_iUseLeavesFlag,double p_dEligHours,string p_sEffDate,string p_sExpDate,string p_sKey)
		{
			bool bRetVal=false;
			LeavePlan objNewMember = null;

			string sSQL = string.Empty;
			try
			{
                objNewMember = new LeavePlan(m_sConnectionString, m_iClientId);
				
				//-- set the properties passed into the method
				objNewMember.LPRowId = p_iLPRowId;
				objNewMember.LeavePlanName = p_sLPName;
				objNewMember.LPTypeCode = p_iLPType;
				objNewMember.LeavePlanCode = p_sLeavePlanCode;
				objNewMember.DefaultFlag = p_iDefaultFlag;
				objNewMember.AllowedNumber = p_iAllowedNumber;
				objNewMember.AllowedPeriodType = p_iAllowedPrdType;
				objNewMember.PerNumber = p_iPerNumber;
				objNewMember.PerPeriodType = p_iPerPrdType;
				objNewMember.HoursPerWeek = p_dHoursPerWeek;
				objNewMember.EligBeneNumber = p_iEligBeneNumber;
				objNewMember.BenePeriodType = p_iBenePrdType;
				objNewMember.UseLeavesFlag = p_iUseLeavesFlag;
				objNewMember.EligHours = p_dEligHours;
				objNewMember.EffectiveDate = p_sEffDate;
				objNewMember.ExpirationDate = p_sExpDate;
				
				m_ColPlans.Add(p_sKey,objNewMember);

				bRetVal=true;
			}
			
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LeavePlans.Add.Err", m_iClientId), p_objEx);
			}
			finally
			{
			}
			return bRetVal;
		}

        /// <summary>
        /// Loads leave plan data.
        /// </summary>
        /// <param name="p_lDeptID"></param>
        /// <returns></returns>
		public bool LoadData(long p_lDeptID)
		{    
			//Const sFunctionName As String = "LoadData"
			bool bRetVal=false;
			string sSQL = string.Empty;
			DbConnection objConn = null; 
			DbReader objReader = null;
            LeavePlan objLeavePlan = new LeavePlan(m_sConnectionString, m_iClientId);
    
			try
			{
				sSQL = "";
				sSQL = sSQL + "SELECT ";
				sSQL = sSQL + "LEAVE_PLAN.LP_ROW_ID,LEAVE_PLAN_NAME,LEAVE_PLAN_CODE,LEAVE_PLAN_TYPE";
				sSQL = sSQL + ",DEFAULT_FLAG,ALLOWED_NUMBER,ALLOWED_PRD_TYPE,PER_NUMBER";
				sSQL = sSQL + ",PER_PRD_TYPE,HOURS_PER_WEEK,ELIG_BENE_NUMBER,BENE_PRD_TYPE";
				sSQL = sSQL + ",BENE_FIRST_FLAG,ELIG_HOURS";
				sSQL = sSQL + ",ENTITY_EID,LP_STATUS_CODE,EFFECTIVE_DATE,EXPIRATION_DATE";
				sSQL = sSQL + " FROM LEAVE_PLAN INNER JOIN LP_X_ENTITY ON LEAVE_PLAN.LP_ROW_ID = LP_X_ENTITY.LP_ROW_ID";
				sSQL = sSQL + " WHERE LP_X_ENTITY.ENTITY_EID IN (" + p_lDeptID + ")";
				sSQL = sSQL + " ORDER BY LEAVE_PLAN_CODE";
		    
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        objLeavePlan.LPRowId = objReader.GetInt32("LP_ROW_ID");
                        objLeavePlan.LPStatusCode = objReader.GetInt32("LP_STATUS_CODE");
                        objLeavePlan.LeavePlanName = objReader.GetString("LEAVE_PLAN_NAME");
                        objLeavePlan.LeavePlanCode = objReader.GetString("LEAVE_PLAN_CODE");
                        objLeavePlan.LPTypeCode = objReader.GetInt32("LEAVE_PLAN_TYPE");
                        objLeavePlan.DefaultFlag = objReader.GetInt32("DEFAULT_FLAG");
                        objLeavePlan.AllowedNumber = objReader.GetInt32("ALLOWED_NUMBER");
                        objLeavePlan.AllowedPeriodType = objReader.GetInt32("ALLOWED_PRD_TYPE");
                        objLeavePlan.PerNumber = objReader.GetInt32("PER_NUMBER");
                        objLeavePlan.PerPeriodType = objReader.GetInt32("PER_PRD_TYPE");
                        objLeavePlan.HoursPerWeek = objReader.GetDouble("HOURS_PER_WEEK");
                        objLeavePlan.EligBeneNumber = objReader.GetInt32("ELIG_BENE_NUMBER");
                        objLeavePlan.BenePeriodType = objReader.GetInt32("BENE_PRD_TYPE");
                        objLeavePlan.BeneFirstFlag = objReader.GetInt32("BENE_FIRST_FLAG");
                        objLeavePlan.EligHours = objReader.GetDouble("ELIG_HOURS");
                        objLeavePlan.EffectiveDate = objReader.GetString("EFFECTIVE_DATE");
                        objLeavePlan.ExpirationDate = objReader.GetString("EXPIRATION_DATE");
                        //					not using this function now but if change in future, need
                        //					to modify the add call below
                        //					Add .LPRowId, .LeavePlanName, "k" & CStr(.LPRowId)
                    }
                }
				bRetVal=true;
			}				
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LeavePlans.Add.Err", m_iClientId), p_objEx);
			}
			finally
			{
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
			}
			return bRetVal;

		}
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="p_sConnectionString"></param>
        public LeavePlans(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
		}
	}
}
