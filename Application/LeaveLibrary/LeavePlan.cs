using System;
using System.Collections;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.LeaveLibrary
{
    ///************************************************************** 
    ///* $File				: LeavePlan.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-31-2007
    ///* $Author			: Aashish Bhateja
    ///***************************************************************	
    /// <summary>	
    ///	Leave Plan definition.
    /// </summary>
    public class LeavePlan
	{
		/// <summary>
		/// Contains the table name.
		/// </summary>
        private const string m_sTableName = "LEAVE_PLAN";

		#region Variables Declaration

		private string m_sConnectionString="";

		/// <summary>
		/// Leave Plan Row Id.
		/// </summary>
		private int m_iLPRowId=0; 
		/// <summary>
		/// Leave Plan name.
		/// </summary>
		private string m_sLeavePlanName=""; 
		/// <summary>
        /// Leave Plan Code.
		/// </summary>
		private string m_sLeavePlanCode=""; 
		/// <summary>
		/// Leave Plan Type Code.
		/// </summary>
		private int m_iLPTypeCode=0; 
		/// <summary>
		/// Leave Plan Description.
		/// </summary>
		private string m_sLeavePlanDesc=""; 
		/// <summary>
		/// Leave Plan Status Code.
		/// </summary>
		private int m_iLPStatusCode=0; 
		/// <summary>
		/// Flag to determine whether it is a default plan and applicable to all
        /// entities.
		/// </summary>
		private int m_DefaultFlag=0; 
		/// <summary>
        /// Allowed number of leaves. It is used in conjunction with m_AllowedPeriodType.
		/// </summary>
		private int m_AllowedNumber=0; 
		/// <summary>
		/// Determines the allowed period type like days,weeks or months.
		/// </summary>
		private int m_AllowedPeriodType=0; 
		/// <summary>
		/// Determines the time frame start date based on the end date.
        /// It is used in conjunction with m_iPerPeriodType.
		/// </summary>
		private int m_PerNumber=0; 
		/// <summary>
        /// Determines the per period type like days,weeks or months.
		/// </summary>
		private int m_iPerPeriodType=0; 
		/// <summary>
		/// Hours per week for the employee under this plan.
		/// </summary>
		private double m_HoursPerWeek=0.0; 
		/// <summary>
		/// Determines the number of days/weeks/months after which the employee
        /// becomes eligible for this plan.
		/// </summary>
		private int m_EligBeneNumber=0; 
		/// <summary>
        /// Determines the benefit period type like days,weeks or months.
		/// </summary>
		private int m_BenePeriodType=0; 
		/// <summary>
		/// Benefit flag.
		/// </summary>
		private int m_BeneFirstFlag=0; 
		/// <summary>
		/// Hours employee must have worked in order to become eligible for
        /// this plan.
		/// </summary>
		private double m_EligHours=0.0; 
		/// <summary>
		/// Plan effective date.
		/// </summary>
		private string m_EffectiveDate=""; 
		/// <summary>
		/// Plan expiration date.
		/// </summary>
		private string m_ExpirationDate=""; 
		/// <summary>
        /// Use Existing Leaves to Update Leave History?
		/// </summary>
		private int m_UseLeavesFlag=0;
        private int m_iClientId = 0; //rkaur27

		#endregion

		#region Properties Declaration
		/// <summary>
        /// Leave Plan Row Id.
		/// </summary>
		public int LPRowId
		{
			get
			{
				return m_iLPRowId;
			}
			set
			{
				m_iLPRowId=value;
			}
		}
		/// <summary>
        /// Leave Plan name.
		/// </summary>
		public string LeavePlanName
		{
			get
			{
				return m_sLeavePlanName;
			}
			set
			{
				m_sLeavePlanName=value;
			}
		}
		/// <summary>
        /// Leave Plan Code.
		/// </summary>
		public string LeavePlanCode
		{
			get
			{
				return m_sLeavePlanCode;
			}
			set
			{
				m_sLeavePlanCode=value;
			}
		}
		/// <summary>
        /// Leave Plan Type Code.
		/// </summary>
		public int LPTypeCode
		{
			get
			{
				return m_iLPTypeCode;
			}
			set
			{
				m_iLPTypeCode=value;
			}
		}
		/// <summary>
        /// Leave Plan Description.
		/// </summary>
		public string LeavePlanDesc
		{
			get
			{
				return m_sLeavePlanDesc;
			}
			set
			{
				m_sLeavePlanDesc=value;
			}
		}
		/// <summary>
        /// Leave Plan Status Code.
		/// </summary>
		public int LPStatusCode
		{
			get
			{
				return m_iLPStatusCode;
			}
			set
			{
				m_iLPStatusCode=value;
			}
		}

        /// <summary>
        /// Flag to determine whether it is a default plan and applicable to all
        /// entities.
        /// </summary>
		public int DefaultFlag
		{
			get
			{
				return m_DefaultFlag;
			}
			set
			{
				m_DefaultFlag=value;
			}
		}

        /// <summary>
        /// Allowed number of leaves. It is used in conjunction with m_AllowedPeriodType.
        /// </summary>
		public int AllowedNumber
		{
			get
			{
				return m_AllowedNumber;
			}
			set
			{
				m_AllowedNumber=value;
			}
		}
        /// <summary>
        /// Determines the allowed period type like days,weeks or months.
        /// </summary>
		public int AllowedPeriodType
		{
			get
			{
				return m_AllowedPeriodType;
			}
			set
			{
				m_AllowedPeriodType=value;
			}
		}
        /// <summary>
        /// Determines the time frame start date based on the end date.
        /// It is used in conjunction with m_iPerPeriodType.
        /// </summary>
		public int PerNumber
		{
			get
			{
				return m_PerNumber;
			}
			set
			{
				m_PerNumber=value;
			}
		}
        /// <summary>
        /// Determines the per period type like days,weeks or months.
        /// </summary>
		public int PerPeriodType
		{
			get
			{
				return m_iPerPeriodType;
			}
			set
			{
				m_iPerPeriodType=value;
			}
		}
        /// <summary>
        /// Hours per week for the employee under this plan.
        /// </summary>
		public double HoursPerWeek
		{
			get
			{
				return m_HoursPerWeek;
			}
			set
			{
				m_HoursPerWeek=value;
			}
		}
        /// <summary>
        /// Determines the number of days/weeks/months after which the employee
        /// becomes eligible for this plan.
        /// </summary>
		public int EligBeneNumber
		{
			get
			{
				return m_EligBeneNumber;
			}
			set
			{
				m_EligBeneNumber=value;
			}
		}
        /// <summary>
        /// Determines the benefit period type like days,weeks or months.
        /// </summary>
		public int BenePeriodType
		{
			get
			{
				return m_BenePeriodType;
			}
			set
			{
				m_BenePeriodType=value;
			}
		}
        /// <summary>
        /// Benefit flag.
        /// </summary>
		public int BeneFirstFlag
		{
			get
			{
				return m_BeneFirstFlag;
			}
			set
			{
				m_BeneFirstFlag=value;
			}
		}
        /// <summary>
        /// Hours employee must have worked in order to become eligible for
        /// this plan.
        /// </summary>
		public double EligHours
		{
			get
			{
				return m_EligHours;
			}
			set
			{
				m_EligHours=value;
			}
		}
        /// <summary>
        /// Plan effective date.
        /// </summary>
		public string EffectiveDate
		{
			get
			{
				return m_EffectiveDate;
			}
			set
			{
				m_EffectiveDate=value;
			}
		}
        /// <summary>
        /// Plan expiration date.
        /// </summary>
		public string ExpirationDate
		{
			get
			{
				return m_ExpirationDate;
			}
			set
			{
				m_ExpirationDate=value;
			}
		}
        /// <summary>
        /// Use Existing Leaves to Update Leave History?
        /// </summary>
		public int UseLeavesFlag
		{
			get
			{
				return m_UseLeavesFlag;
			}
			set
			{
				m_UseLeavesFlag=value;
			}
		}

		#endregion

        /// <summary>
        /// Loads leave plan data based on plan id.
        /// </summary>
        /// <param name="p_iPlanId"></param>
        /// <returns></returns>
		public bool LoadData(int p_iPlanId)
		{
			bool bRetVal=false;
			DbConnection objConn = null; 
			DbReader objReader=null;
			string sSQL = string.Empty;

			try
			{
				sSQL = "SELECT * FROM " + m_sTableName;
				sSQL = sSQL + " WHERE LP_ROW_ID=" + p_iPlanId;

				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();

                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        m_iLPRowId = objReader.GetInt32("LP_ROW_ID");
                        m_sLeavePlanName = objReader.GetString("LEAVE_PLAN_NAME");
                        m_sLeavePlanCode = objReader.GetString("LEAVE_PLAN_CODE");
                        m_iLPTypeCode = objReader.GetInt32("LEAVE_PLAN_TYPE");
                        m_sLeavePlanDesc = objReader.GetString("LEAVE_PLAN_DESC");
                        m_iLPStatusCode = objReader.GetInt32("LP_STATUS_CODE");
                        m_DefaultFlag = objReader.GetInt16("DEFAULT_FLAG");
                        m_AllowedNumber = objReader.GetInt32("ALLOWED_NUMBER");
                        m_AllowedPeriodType = objReader.GetInt32("ALLOWED_PRD_TYPE");
                        m_PerNumber = objReader.GetInt32("PER_NUMBER");
                        m_iPerPeriodType = objReader.GetInt32("PER_PRD_TYPE");
                        m_HoursPerWeek = objReader.GetDouble("HOURS_PER_WEEK");
                        m_EligBeneNumber = objReader.GetInt32("ELIG_BENE_NUMBER");
                        m_BenePeriodType = objReader.GetInt32("BENE_PRD_TYPE");
                        m_BeneFirstFlag = objReader.GetInt16("BENE_FIRST_FLAG");
                        m_EligHours = objReader.GetDouble("ELIG_HOURS");
                        m_EffectiveDate = objReader.GetString("EFFECTIVE_DATE");
                        m_ExpirationDate = objReader.GetString("EXPIRATION_DATE");
                        m_UseLeavesFlag = objReader.GetInt16("USE_LEAVES_FLAG");
                 
                        bRetVal = true;
                    }
                }
			}
			
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LeavePlan.LoadData.Err", m_iClientId),p_objEx);
			}
			finally
			{
				if(objConn!=null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
			}
			return bRetVal;
		}
		
        /// <summary>
        /// Used to load leave plans for utilities.Called directly from 
        /// LeavePlanAdaptor.
        /// </summary>
        /// <param name="p_objLeavePlan"></param>
        /// <returns></returns>
		public XmlDocument LoadLeavePlans(XmlDocument p_objLeavePlan)
		{
			XmlElement objElement=null;
			XmlDocument objXmlDocument=null;

			try
			{
				objXmlDocument=new XmlDocument();

				objElement = objXmlDocument.CreateElement("LeavePlans");
				objXmlDocument.AppendChild(objElement);

				XmlNode objNode= objXmlDocument.ImportNode((XmlNode)GetData("leaveplan").SelectSingleNode("//leaveplan"),true);
				objXmlDocument.FirstChild.AppendChild(objNode);
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LeavePlan.LoadData.Err", m_iClientId),p_objEx);
			}
			finally
			{
			}
			return objXmlDocument;
		}

        /// <summary>
        /// Called from LoadLeavePlans function.
        /// </summary>
        /// <param name="p_sDataType"></param>
        /// <returns></returns>
		private XmlDocument GetData(string p_sDataType)
		{
			XmlDocument objType=null;
			XmlElement objTmpElement=null;
			DbReader objReader=null;
			string sCode="";
			string sName="";
			string sSQL="";
			objType=new XmlDocument();
            try
            {
                objTmpElement = objType.CreateElement(p_sDataType);
                objType.AppendChild(objTmpElement);
                
                sSQL = "SELECT LP_ROW_ID, LEAVE_PLAN_NAME, LEAVE_PLAN_CODE FROM LEAVE_PLAN";
                
                objTmpElement = objType.CreateElement("option");
                objTmpElement.InnerText = "";
                objTmpElement.SetAttribute("value", "");
                objType.FirstChild.AppendChild(objTmpElement);
                
                if (sSQL != "")
                {
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            objTmpElement = objType.CreateElement("option");
                            sCode = objReader.GetString("LEAVE_PLAN_CODE");
                            sName = objReader.GetString("LEAVE_PLAN_NAME");
                            objTmpElement.InnerText = sCode + " " + sName;
                            objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));
                            objType.FirstChild.AppendChild(objTmpElement);
                        }
                    }                    
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LeavePlan.LoadData.Err", m_iClientId), p_objEx);
            }
            finally
            {
                
            }
			return objType;
		}

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="p_sConnectionString"></param>
		public LeavePlan(string p_sConnectionString, int p_iClientId) //rkaur27
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;//rkaur27
        }
	}
}
