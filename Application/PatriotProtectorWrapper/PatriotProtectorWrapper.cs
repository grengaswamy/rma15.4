﻿
using System;
using System.IO;
using System.Xml;
using Riskmaster.Settings;
using Riskmaster.DataModel ;
using Riskmaster.Db;
using Riskmaster.Common;
using PatriotProtector;
using Riskmaster.ExceptionTypes;
using System.Text;

namespace Riskmaster.Application.PatriotProtectorWrapper
{
	///************************************************************** 
	///* $File		: PatriotProtectorWrapper.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 1-July-2005 
	///* $Author	: Raman Bhatia
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	///<summary>
	///Author  :   Raman Bhatia
	///Dated   :   1st July 2005
	///Purpose :   This is a Wrapper Class for .NET Patriot Protector Class.
	/// </summary>
	public class PatriotProtectorWrapper
	{
		#region Variable Declarations

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store Reference Number
		/// </summary>
		private int m_iRefNum = 1;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
        private string m_resultsFromAsyncCall = "";

        private int m_Entityid = 0;

        private string m_lastname;
        private string m_firstname;
        private string m_attachtable;
        private long m_AttachRecordID = 0;
        private Entity m_Entity;
        private int m_TransId = 0;
        private int m_AutoTransId = 0;
        private long m_AutoBatchId = 0;
        private bool m_EntryInPayeeCheck = false;
        private string m_DisplayRd;
        private string m_UpdatedByUser = string.Empty;

        private DataModelFactory m_objDataModelFactory = null;
        private Context m_objContext;
        private int m_iClientId = 0;//davtsa-cloud
        public delegate string LookUpDummy(string lastname, string firstname);

        #endregion

		#region Constructor		
		
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		 

		public PatriotProtectorWrapper(string p_sDsnName , string p_sUserName , string p_sPassword,int p_iClientId)
		{
            m_iClientId = p_iClientId;//dvatsa-cloud
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			this.Initialize();
		}
		/// <summary>
		/// Destructor
		/// </summary>
		~PatriotProtectorWrapper()
		{
            Dispose();
		}
		#endregion

		#region Wrapper Function for .NET Lookup
		
		/// <summary>
		/// Calls the .Net Patriotprotector Class
		/// </summary>
		/// <param name="p_sLastName">Last Name.</param>
		/// <param name="p_sFirstName">First Name.</param>
		/// <returns>String representing the results</returns>
		
		public string Lookup(string p_sLastName , string p_sFirstName)
		{
			string sPPUserId="";
			string sPPPassword="";
			string sResults="";
            //tanwar2 - mits  - start
            string sPPUrl = string.Empty;
            //tanwar2 - mits  - end
            PatriotProtector.PatriotProtector objPatriotProtector = null;
			
			if(p_sFirstName=="") p_sFirstName=" "; 
			
			try 
			{
				//fetching Patriot Protector Login Credentials 
				sPPUserId = GetPPUserId();
				sPPPassword = GetPPPassword();

                //tanwar2 - mits 33189 - start
                sPPUrl = GetPPUrl();
                //tanwar2 - mits 33189 - end

				m_iRefNum = m_iRefNum + 1;
				//creating object of PatriotProtector class and calling the lookup function

                objPatriotProtector = new PatriotProtector.PatriotProtector();
                //tanwar2 - mits 33189 - start
                //sResults = objPatriotProtector.LookUp(sPPUserId, sPPPassword, p_sLastName, p_sFirstName, m_iRefNum.ToString()).Replace("~", "");
                sResults = objPatriotProtector.LookUp(sPPUserId, sPPPassword, p_sLastName, p_sFirstName, m_iRefNum.ToString(), sPPUrl).Replace("~", "");
                //tanwar2 - mits 33189 - end
			}

			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.Lookup.LookupError", m_iClientId), p_objEx);		//dvatsa-cloud		
			}
			finally
			{
				objPatriotProtector = null;				
			}
			return (sResults);

		}

        //tanwar2 - mits   
        private string GetPPUrl()
        {
            SysSettings objSysSettings = null;

            try
            {
                objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
                return (objSysSettings.P2Url);
            }

            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.CreateOFACCheckDiary.Error",m_iClientId), p_objException);//dvatsa-cloud
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objSysSettings = null;
            }
        }


		#endregion

		#region Licence Verification
		
		/// <summary>
		/// Verifies whether License File Exists.
		/// </summary>
		/// <returns>true/false</returns>
		/// 
		public bool AdvertiseOnly()
		{
			string sFolder = "";
			string sFileLocation = "";
			string sContents = "";
			FileStream objFileStream = null;
			bool bAdvertiseOnly = true;

			try
			{
                sFolder = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "bin");
                sFileLocation = String.Format(@"{0}\PPLicense.bin", sFolder);
				if(File.Exists(sFileLocation))
				{
					objFileStream = new FileStream(sFileLocation , FileMode.Open , FileAccess.Read );
					StreamReader objStreamReader = new StreamReader(objFileStream);
					sContents = objStreamReader.ReadToEnd();
					objStreamReader.Close();
					objFileStream.Close();

					if (sContents=="0x110V347C06") 
					{
						bAdvertiseOnly = false;
					}
				}
									
			}

			catch (IOException p_objException)
			{
                throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.AdvertiseOnly.FileInputOutputError", m_iClientId), p_objException);//dvatsa-cloud

			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.AdvertiseOnly.Error", m_iClientId), p_objException);//dvatsa-cloud
			}
			finally
			{
                if (objFileStream != null)
                {
                    objFileStream.Dispose();
                }
				
			}
			return (bAdvertiseOnly);
		}


		#endregion

		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{

			try
			{
                using (DataModelFactory dataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId))//dvatsa-cloud
                {
                    m_sConnectionString = dataModelFactory.Context.DbConn.ConnectionString;
                }
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);//dvatsa-cloud
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.Initialize.ErrorInit", m_iClientId), p_objEx);//dvatsa-cloud				
			}	
		}

        private bool _isDisposed = false;
		/// <summary>
		/// Un Initialize the data model factory object. 
		/// </summary>
		public void Dispose()
		{
            //tkr nothing to do, but left method in case is being called somewhere
            if (!_isDisposed)
            {
                _isDisposed = true;
            }
            GC.SuppressFinalize(this);
		}
		/// <summary>
		/// GetPPUserId
		/// </summary>
		/// <returns>Patriot Protector UserId</returns>
		private string GetPPUserId()
		{
			SysSettings objSysSettings = null;
			
			try
			{
				objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
				return(objSysSettings.P2Login);
			}

            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.CreateOFACCheckDiary.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
			finally
			{
				objSysSettings = null;
			}
			
		}
		/// <summary>
		/// GetPPPassword
		/// </summary>
		/// <returns>Patriot Protector Password</returns>
		private string GetPPPassword()
		{
			SysSettings objSysSettings = null;
			
			try
			{
				objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);//dvatsa-cloud
				return(objSysSettings.P2Pass);
			}
            catch (RMAppException p_objException)
            {
                throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.CreateOFACCheckDiary.Error", m_iClientId), p_objException);//dvatsa-cloud
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
			finally
			{
				objSysSettings = null;
			}
			
		}
        /// <summary>
        ///  Sends Asynchronous call to Patriot Web Service
        /// </summary>
        /// <param name="sFirstname">First name of the Entity to be searched during OFAC Check</param>
        /// <param name="sLastName">Last name of the Entity to be searched during OFAC Check</param>
        /// <param name="sAttachTable">Attach Table value: Screen/Table record is referred to</param>
        /// <param name="lAttachRecordID">Record Id on which OFAC Check has to be performed</param>
        /// <param name="Entity">Entity Object</param>
        /// <param name="iTransId">Transaction Id in case of Checks</param>
        /// <param name="iAutoTransId">Auto Trans Id in case of Auto Checks</param>
        /// <param name="lAuToBatchId">Auto Batch Id in case of Auto Checks</param>

        public void AsynchPatriotProtectorCall(string sFirstname, string sLastName, string sAttachTable, long lAttachRecordID, Entity Entity, int iTransId, int iAutoTransId, long lAutoBatchId, bool bEntryInPayeeCheck, string sDisplayRd)
        {
            //assigning the arguments of AsynchPatriotProtectorCall Function to global variables as
            // they cannot be passed as arguments to the CallBack Function
            string sEntryName;
            string sEntryNotes;
            string sRegarding;

            int Flag = 0;
            try
            {
                m_lastname = sLastName;
                m_firstname = sFirstname;
                m_attachtable = sAttachTable;
                m_AttachRecordID = lAttachRecordID;
                m_Entity = Entity;
                m_Entityid = Entity.EntityId;
                m_TransId = iTransId;
                m_AutoTransId = iAutoTransId;
                m_AutoBatchId = lAutoBatchId;
                m_EntryInPayeeCheck = bEntryInPayeeCheck;
                m_DisplayRd = sDisplayRd;
                //PatriotProtector objPatriotProtector = new PatriotProtector();

                LookUpDummy lookupCaller = new LookUpDummy(this.Lookup);

                IAsyncResult result = lookupCaller.BeginInvoke(sLastName, sFirstname, new AsyncCallback(CallbackPatriotProtector), lookupCaller);
            }
            catch (RMAppException p_objException)
            {
                Flag = 1;
                //throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.AsynchPatriotProtectorCall.Error"), p_objException);
            }
            catch (Exception p_objException)
            {
                Flag = 1;
                //throw p_objException;
            }
            finally
            {
                if (Flag == 1)
                {
                    sEntryName = "OFAC Check Not Complete";
                    sEntryNotes = "OFAC Check could not be completed for the Record. Please do a manual Check for the same";
                    sRegarding = "";

                    if (String.Equals(m_attachtable ,"ENTITY"))
                    {
                        sRegarding = "Entity: " + m_Entity.LastName;
                        m_attachtable = null;
                        m_AttachRecordID = 0;
                    }
                    else if (String.Equals(m_attachtable, "FUNDS"))
                    {
                        sRegarding = "Payment: " + m_DisplayRd;
                    }
                    else if (String.Equals(m_attachtable, "FUNDS_AUTO"))
                    {
                        sRegarding = "Batch: " + m_DisplayRd;
                    }
                    m_UpdatedByUser = m_Entity.UpdatedByUser;
                    m_Entity.Context.DbConn.Close();
                    CreateOFACCheckDiary(sEntryName, sEntryNotes, sRegarding, m_UpdatedByUser, m_attachtable, m_AttachRecordID.ToString());
                }
            }
            
        }
        /// <summary>
        ///Call Back Function for the Asynchronous Call sent to Patriot Protector Web Service 
        /// </summary>
        /// <param name="asyncResult"></param>
        public void CallbackPatriotProtector(IAsyncResult asyncResult)
        {
            XmlDocument objPatOutXml;

            try
            {
                LookUpDummy dlgt = (LookUpDummy)asyncResult.AsyncState;

                string sEntryName = string.Empty;
                string sEntryNotes = string.Empty;
                string sRegarding = string.Empty;

                string ret = dlgt.EndInvoke(asyncResult);

                ret = "<Result>" + ret + "</Result>";

                objPatOutXml = new XmlDocument();
                objPatOutXml.LoadXml(ret);

                m_resultsFromAsyncCall = ret;
                
                XmlNode objResCode = objPatOutXml.SelectSingleNode("//ReturnCode");

                if (objResCode != null)
                {
                    //Case when the Patriot Webservice Check is for an Entity
                    if ((String.Equals(objResCode.InnerText.ToString(), "0")) && (String.Equals(m_attachtable, "ENTITY")))
                    {
                        //Freeze the Payment of an Entity. Set the flag freeze Payee to -1
                        FreezePayee();

                        if (m_EntryInPayeeCheck)
                        {
                            EntryInPayeeCheckDetailTable();
                        }
                        sEntryName = "Payment for Payee Frozen";
                        sEntryNotes = "Payments for Payee are frozen. (Reason: Entry found while OFAC Check)";
                        sRegarding = "Entity: " + m_Entity.LastName;
                        CreateOFACCheckDiary(sEntryName, sEntryNotes, sRegarding, m_UpdatedByUser, null, null);
                                                
                    }
                    //Case when the Patriot Webservice Check is for Payment Created
                    else if ((String.Equals(objResCode.InnerText.ToString(), "0")) && (String.Equals(m_attachtable, "FUNDS")))
                    {
                        //Freeze the Payment
                        FreezeFundsPayment();
                        if (m_EntryInPayeeCheck)
                        {
                            FreezePayee();
                            EntryInPayeeCheckDetailTable();
                            //Create a Diary for the Payee Frozen
                            sEntryName = "Payment for Payee Frozen";
                            sEntryNotes = "Payments for Payee are frozen. (Reason: Entry found while OFAC Check)";
                            sRegarding = "Entity: " + m_Entity.LastName;
                            CreateOFACCheckDiary(sEntryName, sEntryNotes, sRegarding, m_UpdatedByUser, null, null);
                        }
                        //Create A Diary for the Payment Freezed
                        sEntryName = "Payment on Hold";
                        sEntryNotes = "The Payment has been put on hold. (Reason: Payee Entry found while OFAC Check)";
                        sRegarding = "Payment: " + m_DisplayRd;
                        CreateOFACCheckDiary(sEntryName, sEntryNotes, sRegarding, m_UpdatedByUser, "FUNDS", m_TransId.ToString());
                                                
                    }
                    //Case when the Patriot Webservice Check is for Auto Check Created
                    else if ((String.Equals(objResCode.InnerText.ToString(), "0")) && (String.Equals(m_attachtable, "FUNDS_AUTO")))
                    {
                        //Freeze Auto Check Batch

                        FreezeFundsAutoPayment();
                        if (m_EntryInPayeeCheck)
                        {
                            FreezePayee();
                            EntryInPayeeCheckDetailTable();
                            //Create a Diary for the Payee Frozen
                            sEntryName = "Payment for Payee Frozen";
                            sEntryNotes = "Payments for Payee are frozen. (Reason: Entry found while OFAC Check)";
                            sRegarding = "Entity: " + m_Entity.LastName;
                            CreateOFACCheckDiary(sEntryName, sEntryNotes, sRegarding, m_UpdatedByUser, null, null);
                        }
                        //Create A Diary for the Batch Freezed
                        sEntryName = "Batch on Hold";
                        sEntryNotes = "The Auto Batch has been put on hold. (Reason: Payee Entry found while OFAC Check)";
                        sRegarding = "Batch: " + m_DisplayRd;

                        CreateOFACCheckDiary(sEntryName, sEntryNotes, sRegarding, m_UpdatedByUser, "FUNDS_AUTO", m_AutoBatchId.ToString());
                    }

                    //A call for OFAC Check of Entity is sent incase the name of Entity is not same as that mentioned in Funds
                    if ((String.Equals(m_attachtable, "FUNDS")) || (String.Equals(m_attachtable, "FUNDS_AUTO")))
                    {
                        if (!m_EntryInPayeeCheck)
                        {
                            AsynchPatriotProtectorCall(m_Entity.FirstName, m_Entity.LastName, m_Entity.Table, m_Entity.EntityId, m_Entity, -1, -1, -1, true,null);
                        }
                    }
                }
                else
                {
                    sEntryName = "OFAC Check Not Complete";
                    sEntryNotes = "OFAC Check could not be completed for the Record. Please do a manual Check for the same";
                    sRegarding = "";

                    if (String.Equals(m_attachtable, "ENTITY"))
                    {
                        sRegarding = "Entity: " + m_Entity.LastName;
                        m_attachtable = null;
                        m_AttachRecordID = 0;
                    }
                    else if (String.Equals(m_attachtable, "FUNDS"))
                    {
                        sRegarding = "Payment: " + m_DisplayRd;
                    }
                    else if (String.Equals(m_attachtable, "FUNDS_AUTO"))
                    {
                        sRegarding = "Batch: " + m_DisplayRd;
                    }
                    m_UpdatedByUser = m_Entity.UpdatedByUser;
                    m_Entity.Context.DbConn.Close();
                    CreateOFACCheckDiary(sEntryName, sEntryNotes, sRegarding, m_UpdatedByUser, m_attachtable, m_AttachRecordID.ToString());
                }
            }
            catch (RMAppException p_objException)
            {
               // throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.CallbackPatriotProtector.Error"), p_objException);
            }
            catch (Exception p_objException)
            {
               // throw p_objException;
            }
        }
        /// <summary>
        /// Set the FreezePayements Flag for the Entity Created
        /// </summary>
        protected void FreezePayee()
        {
            string sSQL = string.Empty;

            try
            {
                m_Entity.FreezePayments = true;
                m_UpdatedByUser = m_Entity.UpdatedByUser;
                m_AttachRecordID = m_Entity.EntityId;
                if (m_Entity.Context.DbConn.State != System.Data.ConnectionState.Open)
                {
                    m_Entity.Context.DbConn.Open();
                }
                m_Entity.Save();
            }
            catch (RMAppException p_objException)
            {
               // throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.FreezePayee.Error"), p_objException);
            }
            catch (Exception p_objException)
            {
              //  throw p_objException;
            }
            finally
            {
                m_Entity.Context.DbConn.Close();
            }
        }

        protected void EntryInPayeeCheckDetailTable()
        {
            StringBuilder sbSQL = new StringBuilder();
            DbCommand oCommand = null;
            DbParameter objParam = null;
            try
            {
                if (m_Entity.Context.DbConn.State != System.Data.ConnectionState.Open)
                {
                    m_Entity.Context.DbConn.Open();
                }
                using (oCommand = m_Entity.Context.DbConn.CreateCommand())
                {
                    //sSQL = "INSERT INTO PAYEE_CHECK_DETAIL (PAYEE_CHECK_ID, ENTITY_ID, DTTM_FREEZE, DTTM_UNFREEZE, USER_UNFREEZE, PPSERVICEXML, REVIEWED) VALUES (";
                    //sSQL = sSQL + Utilities.GetNextUID(m_Entity.Context.DbConn.ConnectionString, "PAYEE_CHECK_DETAIL") + "," + m_Entity.EntityId + ",'" + Conversion.ToDbDateTime(System.DateTime.Now) + "','" + null + "','" + null + "','" + m_resultsFromAsyncCall + "'," + 0 + ")";
                    sbSQL.AppendFormat("INSERT INTO PAYEE_CHECK_DETAIL (PAYEE_CHECK_ID, ENTITY_ID, DTTM_FREEZE, DTTM_UNFREEZE, USER_UNFREEZE, PPSERVICEXML, REVIEWED) VALUES ({0},{1},'{2}','{3}','{4}',~resultsFromAsyncCall~,{5})"
                        , Utilities.GetNextUID(m_Entity.Context.DbConn.ConnectionString, "PAYEE_CHECK_DETAIL", m_iClientId)
                        , m_Entity.EntityId
                        , Conversion.ToDbDateTime(System.DateTime.Now)
                        , null
                        , null
                        , 0);

                    oCommand.Parameters.Clear();
                    objParam = oCommand.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.Value = m_resultsFromAsyncCall;
                    objParam.SourceColumn = "PPSERVICEXML";
                    objParam.ParameterName = "resultsFromAsyncCall";
                    oCommand.Parameters.Add(objParam);
                    oCommand.CommandText = sbSQL.ToString(); ;
                    oCommand.ExecuteNonQuery();
                }
                //m_Entity.Context.DbConn.ExecuteNonQuery(sSQL);
                
            }
            catch (RMAppException p_objException)
            {
              //  throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.FreezePayee.Error"), p_objException);
            }
            catch (Exception p_objException)
            {
               // throw p_objException;
            }
            finally
            {
                m_Entity.Context.DbConn.Close();
                sbSQL = null;
            }
        }

        /// <summary>
        /// Create Diary if the Entity is List in the OFAC Office.
        /// </summary>
        /// <param name="sEntryName"></param>
        /// <param name="sEntryNotes"></param>
        /// <param name="sRegarding"></param>

        protected void CreateOFACCheckDiary(string sEntryName, string sEntryNotes, string sRegarding, string UpdatedByUser, string sAttachTable, string sAttachedRecordId)
        {
            try
            {
                if (m_Entity.Context.DbConn.State != System.Data.ConnectionState.Open)
                {
                    m_Entity.Context.DbConn.Open();
                }

                WpaDiaryEntry objWpaDiaryEntry = null;

                objWpaDiaryEntry = (WpaDiaryEntry)m_objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);

                objWpaDiaryEntry.EntryName = sEntryName;
                objWpaDiaryEntry.EntryId = Utilities.GetNextUID(m_Entity.Context.DbConn.ConnectionString, "WPA_DIARY_ENTRY", m_iClientId);
                objWpaDiaryEntry.EntryNotes = sEntryNotes;
                objWpaDiaryEntry.CreateDate = Conversion.ToDbDateTime(System.DateTime.Now);
                objWpaDiaryEntry.AssigningUser = UpdatedByUser;
                objWpaDiaryEntry.AssignedUser = UpdatedByUser;
                objWpaDiaryEntry.AttachTable = sAttachTable;
                objWpaDiaryEntry.AttachRecordid = Conversion.ConvertStrToInteger(sAttachedRecordId);
                objWpaDiaryEntry.Regarding = sRegarding;
                objWpaDiaryEntry.CompleteDate = Conversion.ToDbDate(System.DateTime.Today);
                objWpaDiaryEntry.StatusOpen = true;
                objWpaDiaryEntry.Priority = 1;
                objWpaDiaryEntry.IsAttached = true;
                //Indu - Adding att_parent_code for Diary enhancement - Mits 33843
                string scSQL = string.Empty;
                DbReader objRead = null;
                scSQL = "SELECT CLAIM_ID FROM FUNDS WHERE TRANS_ID =" + Conversion.ConvertStrToInteger(sAttachedRecordId);
                objRead = DbFactory.GetDbReader(m_sConnectionString, scSQL);
                if (objRead != null)
                {
                    while (objRead.Read())
                    {
                        objWpaDiaryEntry .AttParentCode = Conversion.ConvertObjToInt(objRead.GetInt32("CLAIM_ID"), m_iClientId);
                    }
                    objRead.Close();
                    objRead.Dispose();
                }
               //End of Mits 33843

                objWpaDiaryEntry.Save();
                
            }
            catch (RMAppException p_objException)
            {
               // throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.CreateOFACCheckDiary.Error"), p_objException);
            }
            catch (Exception p_objException)
            {
              //  throw p_objException;
            }
            finally
            {
                m_Entity.Context.DbConn.Close();
            }
        }

        protected void FreezeFundsPayment()
        {
            int i_StatusCode = -1;
            int i_OldStatusCode = -1;
            StringBuilder sbSQL = new StringBuilder();
            DbCommand oCommand = null;
            DbParameter objParam = null;
            try
            {
                if (m_Entity.Context.DbConn.State != System.Data.ConnectionState.Open)
                {
                    m_Entity.Context.DbConn.Open();
                }

                Funds Fundsobj = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);

                Fundsobj.MoveTo(m_TransId);

                m_UpdatedByUser = Fundsobj.UpdatedByUser;
                m_AttachRecordID = Fundsobj.TransId;
                i_OldStatusCode = Fundsobj.StatusCode;

                sbSQL.Append("SELECT CODE_ID FROM CODES WHERE SHORT_CODE = 'H' AND TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME LIKE 'CHECK_STATUS')");

                using (DbReader objReader = DbFactory.ExecuteReader(m_Entity.Context.DbConn.ConnectionString, sbSQL.ToString()))
                {
                    if (objReader.Read())
                    {
                        i_StatusCode = Int32.Parse(objReader["CODE_ID"].ToString());
                    }
                }

                if (i_StatusCode != -1)
                {
                    Fundsobj.StatusCode = i_StatusCode;
                    Fundsobj.Save();
                }

                //Insert an Entry in HOLD_CHECK_DETAIL Table
                sbSQL.Length = 0;
                using (oCommand = m_Entity.Context.DbConn.CreateCommand())
                {
                    sbSQL.AppendFormat("INSERT INTO HOLD_CHECK_DETAIL (HOLD_CHECK_ID,TRANS_ID,OLD_STATUS_CODE,DTTM_HOLD,DTTM_UNHOLD,USER_UNHOLD,PPSERVICEXML,REVIEWED) VALUES ({0},{1},{2},'{3}','{4}','{5}',~resultsFromAsyncCall~,{6})"
                        , Utilities.GetNextUID(m_Entity.Context.DbConn.ConnectionString, "HOLD_CHECK_DETAIL", m_iClientId)
                        , m_TransId
                        , i_OldStatusCode
                        , Conversion.ToDbDateTime(System.DateTime.Now)
                        , null
                        , null
                        , 0);

                    oCommand.Parameters.Clear();
                    objParam = oCommand.CreateParameter();
                    objParam.Direction = System.Data.ParameterDirection.Input;
                    objParam.Value = m_resultsFromAsyncCall;
                    objParam.SourceColumn = "PPSERVICEXML";
                    objParam.ParameterName = "resultsFromAsyncCall";
                    oCommand.Parameters.Add(objParam);
                    oCommand.CommandText = sbSQL.ToString(); ;
                    oCommand.ExecuteNonQuery();
                    //m_Entity.Context.DbConn.ExecuteNonQuery(sSQL);
                }
            }
            catch (RMAppException p_objException)
            {
               // throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.FreezeFundsPayment.Error"), p_objException);
            }
            catch (Exception p_objException)
            {
              //  throw p_objException;
            }
            finally
            {
                m_Entity.Context.DbConn.Close();
                sbSQL = null;
            }
        }

        protected void FreezeFundsAutoPayment()
        {
            FundsAutoBatch FundsAutoBatchObj = null;
            string sSQLRead = string.Empty;
            StringBuilder sbSQLWrite = new StringBuilder();
            DbCommand oCommand = null;
            DbParameter objParam = null;
            try
            {
                if (m_Entity.Context.DbConn.State != System.Data.ConnectionState.Open)
                {
                    m_Entity.Context.DbConn.Open();
                }
                FundsAutoBatchObj = (FundsAutoBatch)m_objDataModelFactory.GetDataModelObject("FundsAutoBatch", false);
                FundsAutoBatchObj.MoveTo(Int32.Parse(m_AutoBatchId.ToString()));

                FundsAuto FundsAutoObj = (FundsAuto)m_objDataModelFactory.GetDataModelObject("FundsAuto", false);

                FundsAutoObj.MoveTo(m_AutoTransId);

                m_UpdatedByUser = FundsAutoObj.UpdatedByUser;
                //m_AttachRecordID = FundsAutoBatchObj.AutoTransId;
                m_AttachRecordID = long.Parse(FundsAutoBatchObj.AutoBatchId.ToString());


                FundsAutoBatchObj.FreezeBatchFlag = true;

                FundsAutoBatchObj.Save();

                //Inserting the Auto Batch Transactions in the FREEZE_BAT_DETAIL

                sSQLRead = "SELECT AUTO_TRANS_ID, AUTO_BATCH_ID FROM FUNDS_AUTO WHERE AUTO_BATCH_ID = " + m_AutoBatchId;

                using (DbReader objReader = DbFactory.ExecuteReader(m_Entity.Context.DbConn.ConnectionString, sSQLRead))
                {
                    while (objReader.Read())
                    {
                        using (oCommand = m_Entity.Context.DbConn.CreateCommand())
                        {
                            sbSQLWrite.AppendFormat("INSERT INTO FREEZE_BAT_DETAIL (FREEZE_BAT_ID, AUTO_BATCH_ID, AUTO_TRANS_ID, DTTM_FREEZE, DTTM_UNFREEZE, USER_UNFREEZE, PPSERVICEXML, REVIEWED) VALUES ({0},{1},{2},'{3}','{4}','{5}',~resultsFromAsyncCall~,{6})"
                                , Utilities.GetNextUID(m_Entity.Context.DbConn.ConnectionString, "FREEZE_BAT_DETAIL", m_iClientId)
                                , m_AutoBatchId
                                , Int32.Parse(objReader["AUTO_TRANS_ID"].ToString())
                                , Conversion.ToDbDateTime(System.DateTime.Now)
                                , null
                                , null
                                , 0);
                            oCommand.Parameters.Clear();
                            objParam = oCommand.CreateParameter();
                            objParam.Direction = System.Data.ParameterDirection.Input;
                            objParam.Value = m_resultsFromAsyncCall;
                            objParam.SourceColumn = "PPSERVICEXML";
                            objParam.ParameterName = "resultsFromAsyncCall";
                            oCommand.Parameters.Add(objParam);
                            oCommand.CommandText = sbSQLWrite.ToString(); ;
                            oCommand.ExecuteNonQuery();
                            //m_Entity.Context.DbConn.ExecuteNonQuery(sSQLWrite);
                        }
                    }
                }
            }
            catch (RMAppException p_objException)
            {
              //  throw new RMAppException(Globalization.GetString("PatriotProtectorWrapper.FreezeFundsAutoPayment.Error"), p_objException);
            }
            catch (Exception p_objException)
            {
              //  throw p_objException;
            }
            finally
            {
                m_Entity.Context.DbConn.Close();
                sbSQLWrite = null;               
            }
        }
	}
}
