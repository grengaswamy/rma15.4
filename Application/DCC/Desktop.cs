using System;
using System.Xml;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Configuration;

namespace Riskmaster.Application.DCC
{	
	/// <summary>	
	/// Author  :  Neelima Dabral
	/// Date    :  19 August 2004
	/// Purpose :  Riskmaster.Application.DCC.Desktop class is used to define Templates and Reports.
	///			   It generates the report data that can either be viewed in tabular format or in graphical form.	
	/// </summary>
	
	public class Desktop
	{		
		#region Constants
		
		/// <summary>
		/// Represents filter type - CheckBox
		/// </summary>
		const int FT_CHECKBOX = 1; 
		/// <summary>
		/// Represents filter type - ListBox
		/// </summary>
		const int FT_LIST = 2;
		/// <summary>
		/// Represents filter type - TopX
		/// </summary>
		const int FT_TOPX = 3;
		/// <summary>
		/// Represents filter type - ComboBox
		/// </summary>
		const int FT_COMBOBOX = 4;
		/// <summary>
		/// Represents filter type - Spin
		/// </summary>
		const int FT_SPIN =5;
		/// <summary>
		/// Represents filter type - Date
		/// </summary>
		const int FT_DATE = 6;
		/// <summary>
		/// A constant string to identify the item type as template
		/// </summary>
		const string TEMPLATE = "template";
		/// <summary>
		/// A constant string to identify the item type as report
		/// </summary>
		const string REPORT = "report";
		/// <summary>
		/// A constant string to identify the item type as filter or output
		/// </summary>
		const string FILTER_OUTPUT = "filteroutput";
		/// <summary>
		/// Represents output type ("percenttowhole")
		/// </summary>
		const string PERCENTAGE_TO_WHOLE = "percenttowhole";
		/// <summary>
		/// Represents output type ("amountpaid")
		/// </summary>
		const string AMOUNT_PAID = "amountpaid";		
		/// <summary>
		/// Represents output type ("rank")
		/// </summary>
		const string RANK = "rank";		
		/// <summary>
		/// Represents show graph value
		/// </summary>
		const string ON = "on";		
		/// <summary>
		/// Represents a constant string used for comparison
		/// </summary>
		const string RANK_ON ="rankon";
		/// <summary>
		/// Represents output type ("ENTRY_COUNT")
		/// </summary>
		const string ENTRY_COUNT ="ENTRY_COUNT";
		

		#endregion

		#region Class Members			
		/// <summary>		
		/// This variable will contain database connection string				
		/// </summary>
		private string m_sConnectString="";		
		/// <summary>		
		/// This variable will contain the report id
		/// </summary>
		private long   m_lReportId=0;		
		/// <summary>		
		/// This variable will contain the report name
		/// </summary>
		private string m_sReportName="";
		/// <summary>		
		/// This variable will contain the report title 
		/// </summary>
		private string m_sReportTitle="";		
		/// <summary>		
		/// This variable will contain the graph data for the report, as selected by the user
		/// </summary>
		private string m_sSelectedGraphData="";
		/// <summary>		
		/// This variable will contain the graph group for the report, as selected by the user
		/// </summary>
		private string m_sSelectedGraphGroup="";
		/// <summary>		
		/// This variable will contain the graph type for the report, as selected by the user
		/// </summary>
		private string m_sSelectedGraphType="";		
		/// <summary>		
		/// This variable will contain the show percent value as selected by user
		/// </summary>
		private string m_sShowPercent="";	
		/// <summary>		
		/// This variable will contain the template id
		/// </summary>
		private long   m_lTemplateId=0;
		/// <summary>		
		/// This variable will contain the template name
		/// </summary>
		private string m_sTemplateName="";	
		/// <summary>		
		/// This variable will contain the SELECT part of the SQL Query for the report
		/// </summary>		
		private string m_sSelect="";	
		/// <summary>		
		/// This variable will contain the FROM part of the SQL Query for the report
		/// </summary>		
		private string m_sFrom="";		
		/// <summary>		
		/// This variable will contain the WHERE part of the SQL Query for the report
		/// </summary>		
		private string m_sWhere="";		
		/// <summary>		
		/// This variable will contain the GROUP BY part of the SQL Query for the report 
		/// </summary>
		private string m_sGroupBy="";	
		/// <summary>		
		/// This variable is used for assigning column index attribute to the output tag of report xml
		/// </summary>
		private int    m_iNextColumnIndex=0;
		/// <summary>		
		/// This variable will contain the template xml loaded into XML Document format.
		/// Any manipulations will be done on this XML Document and finally this XML will be updated in the database.
		/// </summary>
		XmlDocument m_objTemplateXML=null;		
		/// <summary>		
		/// This variable will contain the report xml loaded into XML Document format.
		/// Any manipulations will be done on this XML Document and finally this XML will be updated in the database.
		/// </summary>
		XmlDocument m_objReportXML=null;
		/// <summary>		
		/// This variable will contain the root element of the template xml loaded in the m_objTemplateXML 	
		/// </summary>
		XmlElement  m_objTemplateElem=null;
		/// <summary>		
		/// This variable will contain the root element of the report xml loaded in the m_objReportXML 	
		/// </summary>
		XmlElement  m_objReportElem=null;

        private int m_iClientId = 0;	
		
		#endregion	

		#region Properties
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.ConnectionString property accesses the value of the connection string.
		/// </summary>
		private  string ConnectionString{get{return m_sConnectString;}set{m_sConnectString = value; }}			
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.ReportId property accesses the value of the report id.
		/// </summary>
		private long   ReportId{get{return m_lReportId;}set{m_lReportId = value; }}
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.ReportName property accesses the value of the report name.
		/// </summary>
		private string ReportName{get{return m_sReportName;}set{m_sReportName = value; }}	
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.ReportName property accesses the value of the report title.
		/// </summary>
		private string ReportTitle{get{return m_sReportTitle;}set{m_sReportTitle = value; }}		
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.SelectedGraphData property accesses the value of graph data attribute of the template.
		/// </summary>
		private string SelectedGraphData{get{return m_sSelectedGraphData;}set{m_sSelectedGraphData = value; }}
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.SelectedGraphGroup property accesses the value of graph group attribute of the template.
		/// </summary>
		private string SelectedGraphGroup{get{return m_sSelectedGraphGroup;}set{m_sSelectedGraphGroup = value; }}
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.SelectedGraphType property accesses the value of graph type attribute of the report.
		/// </summary>
		private string SelectedGraphType{get{return m_sSelectedGraphType;}set{m_sSelectedGraphType = value; }}
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.ShowGraph property accesses the value of showgraph attribute of the report.
		/// </summary>
		private string ShowGraph
		{
			get
			{
				if ((m_objReportElem.GetAttribute("showgraph") == "") || (m_objReportElem.GetAttribute("showgraph") == null))
					return  m_objTemplateElem.GetAttribute("showgraph");
				else
					return  m_objReportElem.GetAttribute("showgraph");
			}
			set
			{m_objReportElem.SetAttribute("showgraph",value);}
		}

		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.ShowPercent property accesses the value of showpercent attribute of the report.
		/// </summary>
		private string ShowPercent{get{return m_sShowPercent;}set{m_sShowPercent = value; }}		
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.TemplateId property accesses the value of id of the template.
		/// </summary>
		private long   TemplateId{get{return m_lTemplateId;}set{m_lTemplateId = value; }}
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop.TemplateName property accesses the value of name of the template.
		/// </summary>
		private string TemplateName{get{return m_sTemplateName;}set{m_sTemplateName = value; }}	
		
		#endregion				
		
		/// <summary>
		/// Riskmaster.Application.DCC.Desktop overload allows setting the connection string
		/// </summary>
		/// <param name="p_sConnectionString">Connection String</param>
		public Desktop(string p_sConnectionString,int p_iClientId)
		{
            m_iClientId = p_iClientId;
            m_sConnectString=p_sConnectionString;			
		}
		
		/// <summary>		
		/// This method will give listing of all the DCC reports (not marked deleted);
		/// also whether the report definition can be modified or not
		/// Along with the report details, it will also return which all buttons would be visible to the user
		/// on the basis of user permissions passed to the method. 
		/// </summary>
		/// <param name="p_bEditPermission">(True/False) User has permission for editing DCC reports or not</param>
		/// <param name="p_bAddPermission">(True/False) User has permission to add DCC reports or not</param>
		/// <param name="p_bDeletePermission">(True/False) User has permission to delete DCC reports or not</param>
		/// <param name="p_bRestorePermission">(True/False) User has permission to restore the deleted DCC reports or not</param>
		/// <param name="p_bDCCDisplayPermission">(True/False) User has permission to have display of DCC reports or not</param>
	    /// <param name="p_bTemplatesPermission">(True/False) User has permission to view DCC templates or not</param>
	    /// <returns>The XML listing all the DCC reports (not marked deleted) and whether it can be edited or not.
	    /// Along with the report details, it also returns which all buttons would be visible to the user
	    /// on the basis of user permissions passed to the method</returns>
		public XmlDocument LoadAllReports(bool p_bEditPermission,bool p_bAddPermission,
						bool p_bDeletePermission,bool p_bRestorePermission,bool p_bDCCDisplayPermission,bool p_bTemplatesPermission)
		{
			DbConnection objConn = null;			
			DbReader	 objReader = null;	
			string		 sSql="";
            string sCustomContent = string.Empty;//Added by Shivendu for MITS 17643			
			XmlElement   objXMLElem=null;				
			XmlAttribute objAttribute=null;
			XmlDocument	 objOutXML=null;			
			int          iTotalCnt=0;

            //Added by Nitin on 28-Feb-2009 for the merging of Mits 13184
            //Added Rakhi for MITS 13184-START:Need to change DCCDisplay to QCDisplay
            XmlDocument objXmlDocument = null;
            //Added Rakhi for MITS 13184-END:Need to change DCCDisplay to QCDisplay
            //End by Nitin on 28-Feb-2009 for the merging of Mits 13184

            try
			{
				objOutXML = new XmlDocument();	
				objConn = DbFactory.GetDbConnection(m_sConnectString);			
				objConn.Open();
				sSql = "SELECT REPORT_ID, TEMPLATE_ID, REPORT_NAME FROM DCC_REPORTS WHERE (DELETED_FLAG IS NULL OR DELETED_FLAG=0) ORDER BY REPORT_NAME";
				objReader = objConn.ExecuteReader(sSql);	
				objXMLElem=objOutXML.CreateElement("reports");
				objOutXML.AppendChild(objXMLElem);						
				while(objReader.Read())
				{
					objXMLElem = objOutXML.CreateElement("report");
					
					objXMLElem.SetAttribute("name",Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_NAME")));					
					objXMLElem.SetAttribute("id",Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_ID")));
					objXMLElem.SetAttribute("templateid",Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_ID")));
					if (p_bEditPermission)
						objXMLElem.SetAttribute("editallow","-1");
					else
						objXMLElem.SetAttribute("editallow","0");

					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);						
					++iTotalCnt;					
				}		
				
				objReader.Close();
				objConn.Close();				

				objAttribute = objOutXML.CreateAttribute("count");
				objAttribute.Value = iTotalCnt.ToString();				
				objOutXML.DocumentElement.Attributes.Append(objAttribute);			
				
				// Adding the buttons to the xml	
				if (p_bAddPermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Add");
					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				if (p_bDeletePermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Delete");
					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				if (p_bRestorePermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Restore");
					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				if (p_bDCCDisplayPermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","DCCDisplay");

                    objXmlDocument = new XmlDocument();
                    sCustomContent = RMSessionManager.GetCustomContent(m_iClientId);
                    if (!string.IsNullOrEmpty(sCustomContent))
                    {
                        objXmlDocument.LoadXml(RMSessionManager.GetCustomContent(m_iClientId));
                        string strDCCDisplay = objXmlDocument.SelectSingleNode("//RMAdminSettings/Caption/DCCDisplay").InnerText;
                        objXMLElem.SetAttribute("value", strDCCDisplay);
                    }
                    
                    //Changed Rakhi for MITS 13184-END:Need to change DCCDisplay to QCDisplay
                    //End by Nitin on 28-Feb-2009 for the merging of Mits 13184
                    objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}   

				if (p_bTemplatesPermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Templates");
					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				return(objOutXML);					
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.LoadAllReports.ErrorLoad",m_iClientId),p_objException);
			}			
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}				
				objOutXML = null;				
				objXMLElem = null;				
				objAttribute = null;				

				DeallocateGlobalXML();
			}			
		}
		
		/// <summary>		
		/// This method will give listing of all the DCC templates (not marked deleted);
		/// also whether it can be edited or not.		
		/// Along with the template details, it will return which all buttons would be visible to the user
		/// on the basis of user permissions passed to the method. 
		/// </summary>
		/// <param name="p_bEditPermission">(True/False) User has permission for editing DCC template or not</param>
		/// <param name="p_bAddPermission">(True/False) User has permission to add DCC template or not</param>
		/// <param name="p_bDeletePermission">(True/False) User has permission to delete DCC template or not</param>
		/// <param name="p_bRestorePermission">(True/False) User has permission to restore the deleted DCC template or not</param>		
		/// <param name="p_bReportsPermission">(True/False) User has permission to view DCC report or not</param>
		/// <returns>The XML listing all the DCC templates (not marked deleted) and whether it can be edited or not.
		/// Along with the template details, it also returns which all buttons would be visible to the user
		/// on the basis of user permissions passed to the method</returns>
		public XmlDocument LoadAllTemplates(bool p_bEditPermission,bool p_bAddPermission,
								bool p_bDeletePermission,bool p_bRestorePermission,bool p_bReportsPermission)
		{
			DbConnection  objConn = null;
			string        sSql="";				
			DbReader      objReader = null;				
			XmlDocument	  objOutXML = null;	
			XmlElement    objXMLElem=null;
			XmlAttribute  objAttribute=null;
			int           iTotalCnt=0;
            XmlElement objTemplate = null;
			try
			{
				objOutXML = new XmlDocument();	
				objConn = DbFactory.GetDbConnection(m_sConnectString);	
				objConn.Open();
				sSql = "SELECT TEMPLATE_ID, TEMPLATE_NAME FROM DCC_TEMPLATES WHERE (DELETED_FLAG IS NULL OR DELETED_FLAG=0) ORDER BY TEMPLATE_NAME";
				objReader = objConn.ExecuteReader(sSql);
				objXMLElem=objOutXML.CreateElement("templates");
				objOutXML.AppendChild(objXMLElem);
                objXMLElem = objOutXML.CreateElement("template");
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);

                objTemplate = (XmlElement)objOutXML.SelectSingleNode("//template");
				while(objReader.Read())
				{					
					objXMLElem = objOutXML.CreateElement("option");
					//objXMLElem.SetAttribute("value",Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_NAME")));

                    objXMLElem.InnerText = Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_NAME"));
                    objXMLElem.SetAttribute("value",Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_ID")));		
					if (p_bEditPermission)
						objXMLElem.SetAttribute("editallow","-1");
					else
						objXMLElem.SetAttribute("editallow","0");
					
                    //objOutXML.SelectSingleNode("//template").ChildNodes.Item(0).AppendChild(objXMLElem);								
                    objTemplate.AppendChild(objXMLElem);
                    ++iTotalCnt;
				}			
				objReader.Close();
				objConn.Close();
				
				objAttribute = objOutXML.CreateAttribute("count");
				objAttribute.Value = iTotalCnt.ToString();				
				objOutXML.DocumentElement.Attributes.Append(objAttribute);

				// Adding the buttons to the xml	
				if (p_bAddPermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Add");
                    //objOutXML.AppendChild(objXMLElem);
                    objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				if (p_bDeletePermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Delete");
                    //objOutXML.AppendChild(objXMLElem);
                    objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				if (p_bRestorePermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Restore");
                    //objOutXML.AppendChild(objXMLElem);
                    objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				if (p_bReportsPermission)
				{
					objXMLElem = objOutXML.CreateElement("button");
					objXMLElem.SetAttribute("name","Reports");
                    //objOutXML.AppendChild(objXMLElem);
                    objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				return(objOutXML);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.LoadAllTemplates.ErrorLoad",m_iClientId),p_objException);
			}			
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				objOutXML = null;				
				objOutXML = null;
				objOutXML = null;

				DeallocateGlobalXML();
			}			
		}

		/// <summary>		
		/// This method will add a new template or update an existing template.
		/// It will load an existing template xml into a XML Document using LoadTemplate(),incase of updating an existing template.
		/// It will update the values in the xml and save the template xml in database using CreateTemplate().		
		/// </summary>
		/// <param name="p_lTemplateId">0 if a new template has to be added; else the id of the template that needs to be modified</param>
		/// <param name="p_sTemplateName">Template Name</param>
		/// <param name="p_iShowPercentage">0/1</param>
		/// <param name="p_iShowGraph">0/1</param>				
		/// <returns>The XML with all the details of the new or the updated template is returned back </returns>
		public XmlDocument AddUpdateTemplate(long p_lTemplateId,string p_sTemplateName,int p_iShowPercentage, int p_iShowGraph)
		{
			XmlDocument  objXMLDoc=null;
			try
			{
				m_objTemplateXML=new XmlDocument();						

				if (p_lTemplateId != 0)
					LoadTemplate(p_lTemplateId);

				CreateTemplate(p_lTemplateId,p_sTemplateName,p_iShowPercentage,p_iShowGraph);
				// If new template is added
				if (p_lTemplateId == 0)
					p_lTemplateId = m_lTemplateId;
				// The XML is returned for the template
				objXMLDoc = GetSelectedTemplate(p_lTemplateId);					
				return(objXMLDoc);
			}						
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.AddUpdateTemplate.ErrorAddUpdate",m_iClientId),p_objException);
			}			
			finally
			{
				objXMLDoc = null;
				DeallocateGlobalXML();
			}			
		}
		/// <summary>		
		/// This method will create/modify a template definition and stores it in the database.	
		/// </summary>		
		/// <param name="p_lTemplateId">Template Id</param>
		/// <param name="p_sTemplateName">Template Name</param>
		/// <param name="p_iShowPercentage">0/1 value for show percentage attribute</param>
		/// <param name="p_iShowGraph">0/1 value for show graph attribute</param>		
		private void CreateTemplate(long p_lTemplateId, string p_sTemplateName,int p_iShowPercentage,int p_iShowGraph)
		{
			DbConnection objConn = null;				
			long	    lNextTemplateID=0;
			string      sSql="";
			string      sTemplateName ="";
			DbReader    objReader = null;
			XmlElement  objGraphElem=null; 	
			string      sXML="";			
			try
			{
				if (p_lTemplateId == 0)
				{
					m_objTemplateElem =m_objTemplateXML.CreateElement("template");
					m_objTemplateXML.AppendChild(m_objTemplateElem);					
				}
				m_objTemplateElem.SetAttribute("name", p_sTemplateName);
				m_objTemplateElem.SetAttribute("showpercent", p_iShowPercentage.ToString());
				m_objTemplateElem.SetAttribute("showgraph", p_iShowGraph.ToString());

				objGraphElem = m_objTemplateXML.CreateElement("graph");
				objGraphElem.SetAttribute("group", "");
				objGraphElem.SetAttribute("data", "");
				objGraphElem.SetAttribute("type", "");
				m_objTemplateElem.AppendChild(objGraphElem);

				sXML=m_objTemplateXML.OuterXml;    				
				sXML =sXML.Replace("'","''");
				sTemplateName = p_sTemplateName.Replace("'","''");
				
				objConn = DbFactory.GetDbConnection(m_sConnectString);	
				
				// Create a new Template
				if (p_lTemplateId == 0)
				{
                    lNextTemplateID = (long)Utilities.GetNextUID(m_sConnectString, "DCC_TEMPLATES", m_iClientId);	

					objConn.Open();
					m_lTemplateId  =lNextTemplateID;					
					sSql = "SELECT * FROM DCC_TEMPLATES WHERE TEMPLATE_ID = " + lNextTemplateID;
					objReader = objConn.ExecuteReader(sSql);
					if(objReader.Read())						
						throw new RMAppException(Globalization.GetString("Desktop.CreateTemplate.TemplateIDInUse",m_iClientId) + lNextTemplateID);
					objReader.Close();

					sSql = "INSERT INTO DCC_TEMPLATES(TEMPLATE_ID, TEMPLATE_NAME,TEMPLATE_XML) VALUES(" + lNextTemplateID + ",'" + sTemplateName + "','" + sXML + "')";
					objConn.ExecuteNonQuery(sSql);					
			        
					sSql = "SELECT TEMPLATE_XML FROM DCC_TEMPLATES WHERE TEMPLATE_ID = " + lNextTemplateID;
					objReader = objConn.ExecuteReader(sSql);
					if(!objReader.Read())						
						throw new RMAppException(Globalization.GetString("Desktop.TemplateIDNotFound",m_iClientId) + lNextTemplateID);					
					objReader.Close();					
				}
				// Update the existing Template
				else
				{
					objConn.Open();
					sSql = "SELECT TEMPLATE_NAME, TEMPLATE_XML FROM DCC_TEMPLATES WHERE TEMPLATE_ID = " + p_lTemplateId;
					objReader = objConn.ExecuteReader(sSql);
					if(!objReader.Read())						
						throw new RMAppException(Globalization.GetString("Desktop.TemplateIDNotFound",m_iClientId) + p_lTemplateId);					
					objReader.Close();
					sSql = "UPDATE DCC_TEMPLATES SET TEMPLATE_NAME='" + sTemplateName + "',TEMPLATE_XML ='"  + sXML + "' WHERE TEMPLATE_ID = " + p_lTemplateId;
					objConn.ExecuteNonQuery(sSql);				
				}				
			}		
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.CreateTemplate.ErrorCreate",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}								
				objGraphElem = null;								
			}
			
		}

		/// <summary>		
		/// This method will update the passed xml with the existing filters for the template/report that will be loaded into XML Document. 
		/// </summary>
		/// <param name="p_objOutXML">(By Ref.) The filter list would be updated in this XML</param>
		/// <param name="p_sFilterListFor">Filter list to be generated for Template/Report</param>
		/// <param name="p_sSelectedFilters">The filters list separated by ","</param>				
		private void SetFilterList(ref XmlDocument p_objOutXML,string p_sFilterListFor,string p_sSelectedFilters)
		{
			XmlNodeList objXMLFilterList=null;
			XmlElement objXMLElem=null;			
			string sFilterName="";
			XmlElement objXMLTempElem=null;						
			try
			{
				objXMLFilterList= m_objTemplateElem.GetElementsByTagName("filter");
				objXMLElem=p_objOutXML.CreateElement("filters");							
				foreach(XmlElement	objXMLFilterElem in objXMLFilterList)
				{
					sFilterName=objXMLFilterElem.GetAttribute("name");
					objXMLTempElem = p_objOutXML.CreateElement("filter");
					objXMLTempElem.SetAttribute("name",sFilterName);
					if(p_sFilterListFor.ToLower() == REPORT)					
					{
						if (p_sSelectedFilters.IndexOf(sFilterName) > -1)
						{
							objXMLTempElem.SetAttribute("selected","-1");						
							objXMLTempElem.SetAttribute("disabled","0");
						}
						else
						{
							objXMLTempElem.SetAttribute("selected","0");						
							objXMLTempElem.SetAttribute("disabled","-1");
						}																		
					}					
					objXMLElem.AppendChild(objXMLTempElem);
					objXMLTempElem = null;
				}
				p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SetFilterList.ErrorSet",m_iClientId),p_objException);
			}			
			finally
			{
				objXMLFilterList = null;
				objXMLElem = null;
				objXMLTempElem = null;
			}

		}

		/// <summary>		
		/// This method will update the passed xml with the existing outputs for the template/report that will be loaded into XML Document. 
		/// </summary>
		/// <param name="p_objOutXML">(By Ref.)The output list would be updated in this XML</param>		
		/// <param name="p_sSelectedOutput">The outputs list separated by ","</param>				
		private void SetOutputList(ref XmlDocument p_objOutXML,string p_sSelectedOutput)
		{
			XmlNodeList objXMLOutputList=null;
			XmlElement objXMLElem=null;
			XmlElement objXMLTempElem = null;										
			try
			{
				objXMLOutputList= m_objTemplateElem.GetElementsByTagName("output");
				objXMLElem=p_objOutXML.CreateElement("outputs");				
				foreach(XmlElement	objXMLOutputElem in objXMLOutputList)
				{
					objXMLTempElem = p_objOutXML.CreateElement("option");
					//objXMLTempElem.SetAttribute("value",objXMLOutputElem.GetAttribute("name"));																			
                    objXMLTempElem.InnerText = objXMLOutputElem.GetAttribute("name");
                    objXMLElem.AppendChild(objXMLTempElem);					
					objXMLTempElem = null;
				}
				p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SetOutputList.ErrorSet",m_iClientId),p_objException);
			}	
			finally
			{
				objXMLOutputList = null;
				objXMLElem = null;
				objXMLTempElem=null;
			}
		}

		/// <summary>		
		/// This method will update the passed xml with outputs that will be selected for a report.		
		/// </summary>
		/// <param name="p_objOutXML">(By Ref.)The selected outputs list would be updated in this XML</param>		
		/// <param name="p_sSelectedOutput">The selected outputs list for a report separated by "," for which XML has to be generated</param>				
		private void SetSelectedOutputList(ref XmlDocument p_objOutXML,string p_sSelectedOutput)
		{
			XmlNodeList objXMLOutputList=null;
			XmlElement objXMLElem=null;							
			string sOutputName="";													
			XmlElement objXMLTempElem =null;
			string[] arrSelectedOutputs=null;
			try
			{
				
				objXMLOutputList= m_objReportXML.GetElementsByTagName("output");
				objXMLElem=p_objOutXML.CreateElement("selectedoutputs");					
				arrSelectedOutputs = p_sSelectedOutput.Split(',');				
				for(int iCounter=0;iCounter<arrSelectedOutputs.Length;++iCounter)
				{
					foreach(XmlElement	objXMLOutputElem in objXMLOutputList)
					{
						sOutputName=objXMLOutputElem.GetAttribute("name");
						if(sOutputName == arrSelectedOutputs[iCounter].ToString())
						{
							objXMLTempElem = p_objOutXML.CreateElement("option");
							//objXMLTempElem.SetAttribute("value",sOutputName);															
                            objXMLTempElem.InnerText = sOutputName;                            
                            objXMLElem.AppendChild(objXMLTempElem);					
							break;
						}
					}					
				}
				
				p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SetSelectedOutputList.ErrorSet",m_iClientId),p_objException);
			}	
			finally
			{
				objXMLOutputList = null;
				objXMLElem = null;				
				objXMLTempElem = null;
			}
		}

		/// <summary>		
		/// This method will delete the item(s) selected by the user.
		/// The selected item will be reports, templates, filters and outputs. 
		/// It will call DeleteFilterOutPut() incase filters/outputs have to be deleted.
		/// </summary>
		/// <param name="p_sItemList">Would contain list of items separated by "," that have to be deleted</param>		
		/// <param name="p_sItemType">Tell what items needs to be deleted- "report"/"template"/"filteroutput"</param>				
		/// <param name="p_lTemplateId">0 if report/template needs to be deleted; else contains the template id for which filters or outputs have to be deleted</param>				
		public void DeleteItem(string p_sItemList, string p_sItemType,long p_lTemplateId)
		{
			DbConnection objConn = null;
			string sTempItemList="";
			string sSql ="";		
			try
			{
				if (p_sItemType == null)
					throw new InvalidValueException(Globalization.GetString("Desktop.DeleteItem.NullItemType",m_iClientId));						

				sTempItemList = p_sItemList;									
				if (sTempItemList.EndsWith(","))
					sTempItemList = sTempItemList.Substring(0,sTempItemList.Length-1);
				if (sTempItemList.StartsWith(","))
					sTempItemList = sTempItemList.Substring(1);				
				switch (p_sItemType.ToLower())
				{
					case REPORT :
						objConn = DbFactory.GetDbConnection(m_sConnectString);
						objConn.Open();
						sSql = "UPDATE DCC_REPORTS SET DELETED_FLAG = -1 WHERE REPORT_ID IN (" + sTempItemList + ")";						
						objConn.ExecuteNonQuery(sSql);						
						objConn.Close();
						break;					
					case TEMPLATE :					
						objConn = DbFactory.GetDbConnection(m_sConnectString);
						objConn.Open();
						sSql = "UPDATE DCC_TEMPLATES SET DELETED_FLAG = -1 WHERE TEMPLATE_ID IN (" + sTempItemList + ")";
						objConn.ExecuteNonQuery(sSql);						
						objConn.Close();
						break;					
					case FILTER_OUTPUT :
						m_objTemplateXML=new XmlDocument();							
						// Template id is required in case of deleting a filter or output						
						LoadTemplate(p_lTemplateId);				
						DeleteFilterOutPut(p_sItemList);
						break;					
					default :
						throw new InvalidValueException(Globalization.GetString("Desktop.DeleteItem.InvalidItemType",m_iClientId) + p_sItemType);						
				}				
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}	
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.DeleteItem.ErrorDelete",m_iClientId),p_objException);
			}
			finally
			{
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				DeallocateGlobalXML();
			}
			
		}

		/// <summary>		
		/// This method will delete the filters/outputs for a template.
		/// It will save the modified template xml into the database.
		/// </summary>
		/// <param name="p_sTempItemList">List of filters/outputs separated by "," (in the form |F|claimstatus,claimtype|O|claimcount),that needs to be deleted</param>		
		private void DeleteFilterOutPut(string p_sTempItemList)
		{
			
			string		 sDeleteItemList="";			
			string[]	 arrItemList=null;
			string       sFilterName="";
			string       sOutputName="";
			string		 sXML="";
			int			 iCounter = 0;
			bool		 bFlag=false;			
			XmlNodeList  objXMLDeleteList=null;
			DbReader     objReader = null;	
			DbConnection objConn = null;

			//Added By: Nikhil Garg    Dated:06/08/2005
			int iFilterIndex=0;
			int iOutputIndex=0;

			try
			{
				//Changed By: Nikhil Garg    Dated:06/08/2005
				//Problem: Only deleting the Output was not working
				iOutputIndex=p_sTempItemList.IndexOf("|O|");
				sDeleteItemList=p_sTempItemList.Substring(iFilterIndex+3,iOutputIndex-(iFilterIndex+3));
				
				if (sDeleteItemList.Trim().Length > 0)     
				{
					arrItemList = sDeleteItemList.Split(',');				
					objXMLDeleteList  = m_objTemplateElem.GetElementsByTagName("filter");					
					bFlag=false;
					iCounter=0;
					while(iCounter< objXMLDeleteList.Count)
					{
						sFilterName = objXMLDeleteList[iCounter].Attributes.GetNamedItem("name").Value;
						bFlag=false;
						for(int iLoopCounter = 0;iLoopCounter<arrItemList.Length;++iLoopCounter)						
						{							
							if (arrItemList[iLoopCounter].ToLower() == sFilterName.ToLower())		
							{
								objXMLDeleteList[iCounter].ParentNode.RemoveChild(objXMLDeleteList[iCounter]);
								bFlag = true;
							}									
						}
						if (!bFlag)
						{
							++iCounter;
						}	
					}	
				}
				
				objXMLDeleteList = null;
				//Changed By: Nikhil Garg    Dated:06/08/2005
				//Problem: Only deleting the Output was not working
				sDeleteItemList=p_sTempItemList.Substring(iOutputIndex+3,p_sTempItemList.Length-(iOutputIndex+3));
				
				if (sDeleteItemList.Trim().Length > 0)     
				{
					arrItemList = sDeleteItemList.Split(',');		
					objXMLDeleteList  = m_objTemplateElem.GetElementsByTagName("output");					
					bFlag=false;
					iCounter=0;
					while(iCounter< objXMLDeleteList.Count)
					{
						sOutputName = objXMLDeleteList[iCounter].Attributes.GetNamedItem("name").Value;
						bFlag=false;
						for(int iLoopCounter = 0;iLoopCounter<arrItemList.Length;++iLoopCounter)						
						{
							
							if (arrItemList[iLoopCounter].ToLower() == sOutputName.ToLower())		
							{
								objXMLDeleteList[iCounter].ParentNode.RemoveChild(objXMLDeleteList[iCounter]);
								bFlag = true;
							}									
						}
						if (!bFlag)
						{
							++iCounter;
						}	
					}	
				}							

				sXML = m_objTemplateXML.OuterXml;       
				objConn  = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				string	     sSql="";
				sSql = "SELECT TEMPLATE_XML FROM DCC_TEMPLATES WHERE TEMPLATE_ID = " + m_lTemplateId;
				objReader = objConn.ExecuteReader(sSql);	
				if(!objReader.Read())
					throw new RMAppException(Globalization.GetString("Desktop.TemplateIDNotFound",m_iClientId) + m_lTemplateId);					

				objReader.Close();		
				sSql = "UPDATE DCC_TEMPLATES SET TEMPLATE_XML ='" +  sXML + "' WHERE TEMPLATE_ID = " + m_lTemplateId;
				objConn.ExecuteNonQuery(sSql);																				
				
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}	
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.DeleteFilterOutPut.ErrorDelete",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				objXMLDeleteList = null;								
			}
			
		}

		/// <summary>		
		/// This method will load the template definition in the XML Document.
		/// This XML Document will be worked upon by various methods to update the template definition.		
		/// </summary>
		/// <param name="p_lTemplateId">The id of the template for which the XML Document will be loaded</param>		
		private void LoadTemplate(long p_lTemplateId)
		{
			DbConnection objConn = null;	
			DbReader objReader = null;
			try
			{				
				
				if (p_lTemplateId == 0)					
					throw new InvalidValueException(Globalization.GetString("Desktop.LoadTemplate.InvalidTemplateId",m_iClientId) + p_lTemplateId);
				
				objConn = DbFactory.GetDbConnection(m_sConnectString);	
				objConn.Open();
				TemplateId = p_lTemplateId;			
				string sSql = "SELECT TEMPLATE_XML FROM DCC_TEMPLATES WHERE TEMPLATE_ID = " + p_lTemplateId;
				objReader = objConn.ExecuteReader(sSql);
				if(!objReader.Read())
					throw new RMAppException(Globalization.GetString("Desktop.TemplateIDNotFound",m_iClientId) + p_lTemplateId);				
				else
				{
					string sTemplateXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_XML"));
					objReader.Close();
					objConn.Close();
					m_objTemplateXML.LoadXml(sTemplateXML);
					m_objTemplateElem = m_objTemplateXML.DocumentElement;
				} 				
			}						
			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.LoadTemplate.ErrorLoad",m_iClientId),p_objException);
			}			
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
			}
		}
			

		/// <summary>				
		/// This method will add a new filter to the template or update an existing filter.
		/// It will load the template definition in the XML Document using LoadTemplate().
		/// It will update the loaded XML with the values passed as parameters and save the updated XML in the database.		
		/// </summary>
		/// <param name="p_lTemplateId">The id of the template for which the filter has to be added/updated</param>		
		/// <param name="p_sOriginalFilterName">Blank in case a new filter has to be added else name of the filter that has to be updated</param>		
		/// <param name="p_sNewFilterName">Name of new filter or the name that has to be updated for the old filter name</param>		
		/// <param name="p_iType">Filter Type</param>		
		/// <param name="p_sSQL">SQL part of the query that fetches the value of filter</param>		
		/// <param name="p_sSQLFrom">FROM part of the query that fetches the value of filter</param>		
		/// <param name="p_sSQLWhere">JOIN part of the query that fetches the value of filter</param>		
		/// <param name="p_sParameter">IN part of the query that fetches the value of filter</param>		
		/// <param name="p_sOrderby">ORDERBY part of the query that fetches the value of filter</param>			
		/// <param name="p_sMinValue">Minimum value of filter (in case of Spin /TopX filter)</param>
		/// <param name="p_sMaxValue">Maximum value of filter (in case of Spin /TopX filter)</param>
		/// <param name="p_sDefaultValue">Default value of filter (in case of Spin /TopX filter)</param>
		public void CreateFilter(long p_lTemplateId,string p_sOriginalFilterName,string p_sNewFilterName,int p_iType,string p_sSQL,string p_sSQLFrom,string p_sSQLWhere,string p_sParameter,string p_sOrderby,string p_sMinValue,string p_sMaxValue,string p_sDefaultValue)
		{
			
			string		 sSql="";			
			string       sXML="";							
			DbConnection objConn = null;	
			DbReader     objReader = null;						
			XmlElement   objFilterElem=null;
			XmlElement   objXMLElem=null;            			
			try
			{
				m_objTemplateXML=new XmlDocument();						
				LoadTemplate(p_lTemplateId);				
				if (p_sOriginalFilterName.Trim().Length > 0 )
				{
					objFilterElem = (XmlElement) m_objTemplateXML.SelectSingleNode("//filter[@name='" + p_sOriginalFilterName + "']");
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("sql"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("sqlfrom"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("sqlwhere"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("parameter"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("orderby"));																							  
				}
				else
				{
					objFilterElem =m_objTemplateXML.CreateElement("filter");						
				}
				
				objFilterElem.SetAttribute("name",p_sNewFilterName);
				objFilterElem.SetAttribute("type",p_iType.ToString());
				objFilterElem.SetAttribute("selected","0");
				objFilterElem.SetAttribute("data","");
				objFilterElem.SetAttribute("all","0");
				objFilterElem.SetAttribute("rankon","");
				if((p_iType == FT_SPIN) || (p_iType == FT_TOPX))
				{
					objFilterElem.SetAttribute("minvalue",p_sMinValue);
					objFilterElem.SetAttribute("maxvalue",p_sMaxValue);
					objFilterElem.SetAttribute("defaultvalue",p_sDefaultValue);
				}

				objXMLElem = m_objTemplateXML.CreateElement("sql");
				objXMLElem.InnerText = p_sSQL;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("sqlfrom");
				objXMLElem.InnerText = p_sSQLFrom;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("sqlwhere");
				objXMLElem.InnerText = p_sSQLWhere;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("parameter");
				objXMLElem.InnerText = p_sParameter;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("orderby");
				objXMLElem.InnerText = p_sOrderby;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;				
				
				m_objTemplateXML.ChildNodes.Item(0).AppendChild(objFilterElem);
				sXML = m_objTemplateXML.OuterXml;							
				sXML =sXML.Replace("'","''");
							
				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				sSql= "SELECT TEMPLATE_ID FROM DCC_TEMPLATES WHERE TEMPLATE_ID = " + m_lTemplateId;
				objReader = objConn.ExecuteReader(sSql);
				if(!objReader.Read())									
					throw new RMAppException(Globalization.GetString("Desktop.TemplateIDNotFound",m_iClientId) + m_lTemplateId);					
				
				objReader.Close();

				sSql= "UPDATE DCC_TEMPLATES SET TEMPLATE_XML ='" + sXML + "' WHERE TEMPLATE_ID = " + m_lTemplateId;
				objConn.ExecuteNonQuery(sSql);
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.CreateFilter.Error",m_iClientId),p_objException);
			}			
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}						
				objFilterElem = null;				
				objXMLElem = null;

				DeallocateGlobalXML();     
			}
			
		}

		/// <summary>		
		/// This method will return the details of the selected filter in the XML Format.
		/// </summary>
		/// <param name="p_lTemplateId">The id of the template for which the filter details are required</param>		
		/// <param name="p_sFilterName">Name of the filter for which details are required</param>		
		/// <return>The XML that will contain the details of the filter</return>
		public XmlDocument GetFilterNode(long p_lTemplateId,string p_sFilterName)
		{
			XmlDocument objXMLDoc=null;
			XmlElement  objXMLElem=null;
			string		sValue="";
			string		sFilterType="";
			try
			{
				m_objTemplateXML=new XmlDocument();						
				LoadTemplate(p_lTemplateId);
				objXMLElem = (XmlElement) m_objTemplateXML.SelectSingleNode("//filter[@name='" + p_sFilterName + "']");				
				if (objXMLElem != null)
				{
					sValue = objXMLElem.InnerXml;
					sFilterType= objXMLElem.GetAttribute("type");
				}

				objXMLElem = null;
				objXMLDoc = new XmlDocument();	
				objXMLElem=objXMLDoc.CreateElement("filter");
				objXMLElem.SetAttribute("name",p_sFilterName);
				objXMLElem.SetAttribute("type",sFilterType);
				objXMLElem.InnerXml = sValue;
				objXMLDoc.AppendChild(objXMLElem);									
				return objXMLDoc;
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetFilterNode.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objXMLDoc=null;
				objXMLElem=null;
				DeallocateGlobalXML();
			}			
		}

		/// <summary>		
		/// This method will add a new output to the template or update an existing output.
		/// It will load the template definition in the XML Document using LoadTemplate().
		/// It will then updates the XML with the values passed as parameters and finally save the updated XML in the database.
		/// </summary>
		/// <param name="p_lTemplateId">The id of the template for which the output has to be added/updated</param>		
		/// <param name="p_sOriginalOutputName">Blank in case a new output has to be added else name of the output that has to be updated</param>		
		/// <param name="p_sNewOutputName">Name of new output or the name that has to be updated for the old output name</param>		
		/// <param name="p_sLabel">output label</param>		
		/// <param name="p_sGraphType">X/Y</param>		
		/// <param name="p_sDataField">Data to be shown</param>		
		/// <param name="p_sJoinSelect">Join Select</param>		
		/// <param name="p_sJoinFrom">Join Table</param>		
		/// <param name="p_sJoinWhere">Where Condition</param>		
		/// <param name="p_sFDependencies">Filter Dependency</param>		
		/// <param name="p_sODependencies">Output Dependency</param>		
		public void CreateOutput(long p_lTemplateId,string p_sOriginalOutputName,string p_sNewOutputName,string p_sLabel,string p_sGraphType,string p_sDataField,string p_sJoinSelect,string p_sJoinFrom,string p_sJoinWhere,string p_sFDependencies,string p_sODependencies)
		{
			string sSql="";			
			DbReader objReader = null;
			DbConnection objConn = null;	
			string sXML="";											
			XmlElement objFilterElem=null ;
			XmlElement objXMLElem=null;						
			try
			{
				m_objTemplateXML=new XmlDocument();						
				LoadTemplate(p_lTemplateId);							
				if (p_sOriginalOutputName.Trim().Length > 0 ) //editing
				{
					objFilterElem =(XmlElement)m_objTemplateXML.SelectSingleNode("//output[@name='" + p_sOriginalOutputName + "']");
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("datafield"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("joinselect"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("joinfrom"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("joinwhere"));
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("fdependencies"));																							  
					objFilterElem.RemoveChild(objFilterElem.SelectSingleNode("odependencies"));
				}
				else
				{
					objFilterElem =m_objTemplateXML.CreateElement("output");								
				}
				
				objFilterElem.SetAttribute("name",p_sNewOutputName);
				objFilterElem.SetAttribute("label",p_sLabel);
				objFilterElem.SetAttribute("graph",p_sGraphType);
				objFilterElem.SetAttribute("selected","0");
				objFilterElem.SetAttribute("dependentselected","0");
				objFilterElem.SetAttribute("groupby","0");
				objFilterElem.SetAttribute("order","0");
		
				objXMLElem = m_objTemplateXML.CreateElement("datafield");
				objXMLElem.InnerText = p_sDataField;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("joinselect");				
				objXMLElem.InnerText = p_sJoinSelect;				
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("joinfrom");
				objXMLElem.InnerText = p_sJoinFrom;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("joinwhere");
				objXMLElem.InnerText = p_sJoinWhere;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("fdependencies");
				objXMLElem.InnerText = p_sFDependencies;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				objXMLElem = m_objTemplateXML.CreateElement("odependencies");
				objXMLElem.InnerText = p_sODependencies;
				objFilterElem.AppendChild(objXMLElem);
				objXMLElem = null;

				m_objTemplateXML.ChildNodes.Item(0).AppendChild(objFilterElem);
				sXML = m_objTemplateXML.OuterXml;	
				sXML =sXML.Replace("'","''");

				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				sSql= "SELECT TEMPLATE_ID FROM DCC_TEMPLATES WHERE TEMPLATE_ID = " + m_lTemplateId;
				objReader = objConn.ExecuteReader(sSql);
				if(!objReader.Read())					
					throw new RMAppException(Globalization.GetString("Desktop.TemplateIDNotFound",m_iClientId) + m_lTemplateId);					
				objReader.Close();

				sSql= "UPDATE DCC_TEMPLATES SET TEMPLATE_XML ='" + sXML + "' WHERE TEMPLATE_ID = " + m_lTemplateId;
				objConn.ExecuteNonQuery(sSql);
			}						
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.CreateOutput.Error",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}			
				objFilterElem = null;
				objXMLElem = null;
				
				DeallocateGlobalXML();
			}			
		}
		
		/// <summary>		
		/// This method will give the details of the selected output in the XML Format.
		/// </summary>
		/// <param name="p_lTemplateId">The id of the template for which the output details are required</param>		
		/// <param name="p_sOutputName">Name of the output for which details are required</param>		
		/// <return>The XML that will contain the details of the selected output</return>
		public XmlDocument GetOutputNode(long p_lTemplateId,string p_sOutputName)
		{
			XmlDocument objXMLDoc=null;
			XmlElement	objXMLElem = null;
			string		sValue="";
			string		sLabel="";
			string		sGraph="";
			try
			{
				m_objTemplateXML=new XmlDocument();					
				LoadTemplate(p_lTemplateId);
				objXMLElem = (XmlElement) m_objTemplateXML.SelectSingleNode("//output[@name='" + p_sOutputName + "']");
				if (objXMLElem != null)
				{
					sValue = objXMLElem.InnerXml;
					sGraph = objXMLElem.GetAttribute("graph");
					sLabel =  objXMLElem.GetAttribute("label");
				}

				objXMLElem = null;
				objXMLDoc = new XmlDocument();	
				objXMLElem=objXMLDoc.CreateElement("output");
				objXMLElem.SetAttribute("name",p_sOutputName);								
				objXMLElem.SetAttribute("label",sLabel);								
				objXMLElem.SetAttribute("graph",sGraph);								
				objXMLElem.InnerXml = sValue;
				objXMLDoc.AppendChild(objXMLElem);	
				return objXMLDoc;
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetOutputNode.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objXMLDoc = null;
				objXMLElem = null;
				DeallocateGlobalXML();
			}			
		}

		/// <summary>		
		/// This method will give list of all the deleted items (DELETE_FLG != -1), in the XML Format.
		/// The list would be generated either for report or template, depending on the parameter passed.
		/// </summary>		
		/// <param name="p_sItemType">"report"/"template"</param>		
		/// <return>The XML listing all the deleted items</return>
		public XmlDocument LoadDeletedItem(string p_sItemType)
		{
			string sSql="";			
			DbReader objReader = null;
			XmlDocument objOutXML = null;
			XmlAttribute objAttribute=null;
			XmlElement objXMLElem=null;
			XmlElement objXMLTempElem=null;
			DbConnection objConn =null;	
			try
			{
				
				if (p_sItemType == null)
					throw new InvalidValueException(Globalization.GetString("Desktop.LoadDeletedItem.NullItemType",m_iClientId));						

				switch (p_sItemType.ToLower())
				{
					case REPORT :
						sSql = "SELECT REPORT_ID, REPORT_NAME FROM DCC_REPORTS WHERE (DELETED_FLAG <> 0 AND DELETED_FLAG IS NOT NULL) ORDER BY REPORT_NAME";
						break;
					case TEMPLATE :
						sSql="SELECT TEMPLATE_ID, TEMPLATE_NAME FROM DCC_TEMPLATES WHERE (DELETED_FLAG <> 0 AND DELETED_FLAG IS NOT NULL) ORDER BY TEMPLATE_NAME";
						break;
					default:
						throw new InvalidValueException(Globalization.GetString("Desktop.LoadDeletedItem.InvalidItemType",m_iClientId) + p_sItemType);						

				}
			
				objOutXML = new XmlDocument();
				objXMLElem=objOutXML.CreateElement("restorelist");
				objOutXML.AppendChild(objXMLElem);			

				objAttribute = objOutXML.CreateAttribute("itemtype");
				objAttribute.Value = p_sItemType.ToLower();
				objOutXML.DocumentElement.Attributes.Append(objAttribute);

				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				objReader = objConn.ExecuteReader(sSql);
				while(objReader.Read())
				{
					objXMLTempElem = objOutXML.CreateElement("item");
					objXMLTempElem.SetAttribute("id",Common.Conversion.ConvertObjToStr(objReader[0]));
					objXMLTempElem.SetAttribute("name",Common.Conversion.ConvertObjToStr(objReader[1]));

					objXMLElem.AppendChild(objXMLTempElem);
					objXMLTempElem = null;
				}								
				return objOutXML;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.LoadDeletedItem.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}				
				objOutXML = null;				
				objAttribute = null;				
				objXMLElem = null;				
				objXMLTempElem = null;
			}			
		}

		/// <summary>		
		/// This method will restore the item(s) that have been deleted.
		/// Either report or template would be restored, depending on the parameter passed.
		/// </summary>		
		/// <param name="p_sItemList">List of deleted report/template id(s) separated by "," that have to be restored</param>		
		/// <param name="p_sItemType">"report"/"template"</param>				
		public void RestoreItem(string p_sItemList,string p_sItemType)
		{
			string sSql="";
			DbConnection objConn = null;	
			try
			{
				if (p_sItemType == null)
					throw new InvalidValueException(Globalization.GetString("Desktop.RestoreItem.NullItemType",m_iClientId));

				if (p_sItemList.StartsWith(",") )
					p_sItemList = p_sItemList.Substring(1);
				if (p_sItemList.EndsWith(","))
					p_sItemList = p_sItemList.Substring(0,p_sItemList.Length -1);

				switch(p_sItemType.ToLower())
				{
					case REPORT :
						sSql="UPDATE DCC_REPORTS SET DELETED_FLAG = 0 WHERE REPORT_ID IN (" + p_sItemList + ")";
						break;
					case TEMPLATE :
						sSql = "UPDATE DCC_TEMPLATES SET DELETED_FLAG = 0 WHERE TEMPLATE_ID IN (" + p_sItemList + ")";
						break;
					default:						
						throw new InvalidValueException(Globalization.GetString("Desktop.RestoreItem.InvalidItemType",m_iClientId) + p_sItemType);
						
				}
				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				objConn.ExecuteNonQuery(sSql);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.RestoreItem.Error",m_iClientId),p_objException);
			}	
			finally
			{
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
			}
		}		
		/// <summary>		
		/// This method will give listing of all the templates, in the XML format.
		/// It will mark the selected attribute as 1 for the template id that will be passed as parameter.
		/// </summary>		
		/// <param name="p_lSelTemplateId">Template Id that has to be marked as selected in the returned XML</param>		
		/// <returns>The XML listing all the templates</returns>
		public XmlDocument TemplateOptions(long p_lSelTemplateId)
		{
			string sSql="";						
			int	 iTotalCnt=0;						
			long lTemplateId=0;
			DbReader  objReader = null;	
			XmlDocument objOutXML = null;	
			XmlElement objXMLElem=null;			
			DbConnection objConn = null;	
			try
			{
				objOutXML = new XmlDocument();	
				objXMLElem=objOutXML.CreateElement("templates");
				objOutXML.AppendChild(objXMLElem);		

				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				sSql = "SELECT TEMPLATE_NAME, TEMPLATE_ID FROM DCC_TEMPLATES";
				objReader = objConn.ExecuteReader(sSql);				
				while(objReader.Read())
				{					
					++iTotalCnt;
					lTemplateId = Common.Conversion.ConvertStrToLong(Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_ID")));
					objXMLElem = objOutXML.CreateElement("template");
					objXMLElem.SetAttribute("name",Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_NAME")));
					objXMLElem.SetAttribute("id",lTemplateId.ToString());
					
					if (p_lSelTemplateId != 0)
					{
						if (lTemplateId == p_lSelTemplateId)
							objXMLElem.SetAttribute("selected","-1");					
						else
							objXMLElem.SetAttribute("selected","0");
					}
					
					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);		
					objXMLElem=null;
				}		
				
				objReader.Close();
				objConn.Close();

				XmlAttribute objAttribute=null;
				objAttribute = objOutXML.CreateAttribute("count");
				objAttribute.Value = iTotalCnt.ToString();				
				objOutXML.DocumentElement.Attributes.Append(objAttribute);	
				objAttribute=null;

				return objOutXML;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.TemplateOptions.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}			
				objOutXML = null;				
				objXMLElem = null;
			}			
		}

		/// <summary>		
		/// This method will add a new DCC report for a selected template. 
		/// It will load the template definition in the XML Document, using LoadTemplate().
		/// It will create xml for the new report with the values passed as parameters, using NewReportXML().
		/// This xml would finally be stored in the database.	
		/// </summary>		
		/// <param name="p_lTemplateId">Id of the template for which the report has been added</param>		
		/// <param name="p_sReportName">Name of the new report</param>		
		/// <param name="p_sReportTitle">Title of the new report</param>		
		/// <returns>XML containing details for the new report</returns>
		public XmlDocument NewReport(long p_lTemplateId,string p_sReportName,string p_sReportTitle)
		{
			XmlDocument objXMLDoc=null;
			try
			{
				m_objTemplateXML=new XmlDocument();						
				LoadTemplate(p_lTemplateId);				
				ReportName=p_sReportName;
				ReportTitle=p_sReportTitle;	
				NewReportXML();
				objXMLDoc = GetSelectedReport(m_lReportId,p_lTemplateId);				
				return objXMLDoc;
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.NewReport.ErrorCreate",m_iClientId),p_objException);
			}	
			finally
			{	
				DeallocateGlobalXML();
				objXMLDoc=null;
			}			
		}
		/// <summary>		
		/// Overloaded method.
		/// This method will create the XML for the report (using MergeFilterSQL() and MergeOutputSQL()).		
		/// It will save the report xml into the database.	
		/// This method would be called in case of updating an existing report.
		/// </summary>						
		private void UpdateReportXML()
		{
			string       sSql="";			
			DbReader     objReader = null;
			XmlDocument  objReportXML = null;
			XmlElement   objReportElem=null;		
			XmlElement   objOneElem=null;		
			XmlNodeList  objXMLNodeList=null;
			string       sFieldName ="";
			int			 iCounter=1;
			string		 sRankOn="";
			string		 sTemplateName="";
			string		 sSelected="";			
			DbConnection objConn = null;				
			try
			{
				sTemplateName = m_objTemplateElem.GetAttribute("name");				
				if (sTemplateName == null)
					throw new InvalidValueException(Globalization.GetString("Desktop.UpdateReportXML.NullTemplateName",m_iClientId));						

				sTemplateName = sTemplateName.ToLower();
				objReportXML = new XmlDocument();
				objReportElem  = objReportXML.CreateElement("report");
				objReportElem.SetAttribute("templatename",sTemplateName);
				objReportElem.SetAttribute("name",ReportName);
				objReportElem.SetAttribute("showgraph",m_objTemplateElem.GetAttribute("showgraph").ToString());

				objOneElem = objReportXML.CreateElement("sql");
				objReportElem.AppendChild(objOneElem);
				objOneElem = null;
				m_sShowPercent=m_objTemplateElem.GetAttribute("showpercent");										

				#region Creating output elements for report xml and the SQL Query(SELECT,FROM,WHERE,GROUP BY)for the report data
				objXMLNodeList = m_objTemplateElem.GetElementsByTagName("output");
				while(iCounter <= objXMLNodeList.Count)
				{
					foreach(XmlElement objXMLElement in objXMLNodeList)
					{
						sSelected =objXMLElement.GetAttribute("selected");						
						if ((sSelected == "-1" ) && (Common.Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("order")) == iCounter) )
						{
							objOneElem =objReportXML.CreateElement("output");
							objOneElem.SetAttribute("name",objXMLElement.GetAttribute("name"));
							objOneElem.SetAttribute("label",objXMLElement.GetAttribute("label"));
							objOneElem.SetAttribute("order",objXMLElement.GetAttribute("order"));
							if(objXMLElement.GetAttribute("graph") == "X")
								objOneElem.SetAttribute("groupby","-1");
							else
								objOneElem.SetAttribute("groupby","0");
							objReportElem.AppendChild(objOneElem);
							MergeOutputSQL(objXMLElement,ref objOneElem) ;
							objOneElem = null;
							break;
						}
					}
					++iCounter;
				}
				objXMLNodeList = null;

				#endregion		

				#region Creating filter elements for report & updating the SQL Query(FROM and WHERE parts) for the report as per the filters 
				objXMLNodeList = m_objTemplateElem.GetElementsByTagName("filter");
				foreach(XmlElement objXMLElement in objXMLNodeList)
				{
					if ((objXMLElement.GetAttribute("selected")=="-1") || (objXMLElement.GetAttribute("dependentselected") == "-1"))
						sSelected = "-1";
					else
						sSelected = "0";
					if (sSelected == "-1")
					{
						objOneElem = objReportXML.CreateElement("filter");
						objOneElem.SetAttribute("name",objXMLElement.GetAttribute("name"));
						objOneElem.SetAttribute("data",objXMLElement.GetAttribute("data"));
						objOneElem.SetAttribute("all",objXMLElement.GetAttribute("all"));						
						if (objXMLElement.GetAttribute("name") == RANK)
						{
							objOneElem.SetAttribute("rankon",objXMLElement.GetAttribute("rankon"));
							sRankOn = objXMLElement.GetAttribute("rankon");
						}
						objReportElem.AppendChild(objOneElem);
						MergeFilterSQL(objXMLElement);
					}					
				}
				objXMLNodeList = null;
				#endregion				

				#region Creating the graph element for report
				objOneElem = objReportXML.CreateElement("graph");
				objXMLNodeList = m_objTemplateElem.GetElementsByTagName("graph");
				if (objXMLNodeList.Count == 0)
				{
					objOneElem.SetAttribute("group","");
					objOneElem.SetAttribute("data","");
					objOneElem.SetAttribute("type","");
				}
				else
				{
					XmlElement objXMLElement = (XmlElement) m_objTemplateElem.GetElementsByTagName("graph").Item(0);
					objOneElem.SetAttribute("group",objXMLElement.GetAttribute("group"));
					objOneElem.SetAttribute("data",objXMLElement.GetAttribute("data"));
					objOneElem.SetAttribute("type",objXMLElement.GetAttribute("type"));
				}
				objReportElem.AppendChild(objOneElem);
				objXMLNodeList = null;
				#endregion

				#region Creating the Order By part of the SQL Query for report data
				string       sOrderBy="";	
				string sGroupName= objOneElem.GetAttribute("group");					
				objOneElem = null;				
				if((sGroupName != "") && (sRankOn ==""))
				{
					if (sOrderBy =="")
						sOrderBy = " ORDER BY ";
					objXMLNodeList = objReportElem.GetElementsByTagName("output");
					foreach(XmlElement objXMLElement in objXMLNodeList)
					{
						if (objXMLElement.GetAttribute("name") ==sGroupName)
						{
							sFieldName = objXMLElement.GetAttribute("field");
							break;
						}
					}
                    //tkr 6/19/07 strip alias from field name before it is appended to order by clause
                    //SQL Server alias can be " AS " or a space " "
                    //Oracle alias surrounded by double quotes
                    int alias = sFieldName.ToUpper().IndexOf(" AS ");
                    if(alias == -1)
                    {
                        int closingBracket = sFieldName.IndexOf("]") == -1 ? 0 : sFieldName.IndexOf("]");
                        alias = sFieldName.Substring(closingBracket).IndexOf(" ");
                    } // if
                    if(alias == -1)
                    {
                        alias = sFieldName.IndexOf("\"");
                    } // if
                    if(alias != -1)
                    {
                        sFieldName = sFieldName.Substring(0, alias);
                    } // if
			
					if(sFieldName.IndexOf("&&JOINSELECT&&",1) != -1)
						sFieldName=sFieldName.Replace("&&JOINSELECT&&",",");
					sOrderBy = sOrderBy + sFieldName;
				}
				objXMLNodeList = null;
				objOneElem = null;
				
				if(sRankOn != "")
				{
					sFieldName ="";
					objXMLNodeList = objReportElem.GetElementsByTagName("output");
					foreach(XmlElement objXMLElement in objXMLNodeList)
					{
						if(objXMLElement.GetAttribute("name") == sRankOn )
						{
							sFieldName = objXMLElement.GetAttribute("field");
							break;
						}
					}
					if (sFieldName != "")
					{
						sOrderBy = " ORDER BY ";
						if(sFieldName.IndexOf(" AS ") != -1)
						{
							
							int iPos = sFieldName.Length - sFieldName.IndexOf(" as ",1) -4;
							sFieldName=sFieldName.Substring(sFieldName.Length-iPos,iPos);			
						}
						if(sFieldName.IndexOf("&&JOINSELECT&&",1) != -1)
							sFieldName=sFieldName.Replace("&&JOINSELECT&&",",");
						sOrderBy = sOrderBy + sFieldName +  " DESC ";
					}
				}
				objXMLNodeList=null;
				objOneElem = null;
				#endregion

				string sReportSQL=m_sSelect + " " + m_sFrom + " " + m_sWhere + m_sGroupBy + sOrderBy;			
				
				objReportElem.GetElementsByTagName("sql").Item(0).InnerText=sReportSQL;
				objReportXML.AppendChild(objReportElem);
				string sXML= objReportXML.OuterXml;				
				sXML =sXML.Replace("'","''");

				#region Updating the report definition in the database
				string       sReportName="";
				sReportName= m_sReportName.Replace("'","''");

				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();

				sSql = "SELECT REPORT_NAME, REPORT_XML FROM DCC_REPORTS WHERE REPORT_ID = " + m_lReportId;
				objReader = objConn.ExecuteReader(sSql);
				if(!objReader.Read())
					throw new RMAppException(Globalization.GetString("Desktop.ReportIDNotFound",m_iClientId) + m_lReportId);
				
				objReader.Close();
				sSql = "UPDATE DCC_REPORTS SET REPORT_NAME='" + sReportName +"', REPORT_XML='" + sXML + "' WHERE REPORT_ID = " + m_lReportId;
				objConn.ExecuteNonQuery(sSql);
				
				#endregion
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.UpdateReportXML.ErrorCreate",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				objReportXML = null;
				objReportElem = null;
				objOneElem = null;
				objXMLNodeList = null;							
			}			
		}				

		/// <summary>		
		/// Overloaded method.
		/// This method will create the XML for the report.
		/// It will save the report xml into the database.
		/// This method would be called in case of new report addition.
		/// </summary>						
		private void NewReportXML()
		{
			string       sSql="";			
			DbReader     objReader = null;
			XmlDocument  objReportXML = null;
			XmlElement   objReportElem=null;		
			XmlElement   objOneElem=null;		
			XmlNodeList  objXMLNodeList=null;						
			string		 sTemplateName="";			
			DbConnection objConn = null;				
			try
			{
				sTemplateName = m_objTemplateElem.GetAttribute("name");				
				if (sTemplateName == null)
					throw new InvalidValueException(Globalization.GetString("Desktop.NewReportXML.NullTemplateName",m_iClientId));						

				sTemplateName = sTemplateName.ToLower();
				objReportXML = new XmlDocument();
				objReportElem  = objReportXML.CreateElement("report");
				objReportElem.SetAttribute("templatename",sTemplateName);
				objReportElem.SetAttribute("name",ReportName);
				objReportElem.SetAttribute("showgraph",m_objTemplateElem.GetAttribute("showgraph").ToString());

				objOneElem = objReportXML.CreateElement("sql");
				objReportElem.AppendChild(objOneElem);
				objOneElem = null;
				m_sShowPercent=m_objTemplateElem.GetAttribute("showpercent");														

				#region Creating the graph element for report
				objOneElem = objReportXML.CreateElement("graph");
				objXMLNodeList = m_objTemplateElem.GetElementsByTagName("graph");
				if (objXMLNodeList.Count == 0)
				{
					objOneElem.SetAttribute("group","");
					objOneElem.SetAttribute("data","");
					objOneElem.SetAttribute("type","");
				}
				else
				{
					XmlElement objXMLElement = (XmlElement) m_objTemplateElem.GetElementsByTagName("graph").Item(0);
					objOneElem.SetAttribute("group",objXMLElement.GetAttribute("group"));
					objOneElem.SetAttribute("data",objXMLElement.GetAttribute("data"));
					objOneElem.SetAttribute("type",objXMLElement.GetAttribute("type"));
				}
				objReportElem.AppendChild(objOneElem);
				objXMLNodeList = null;
#endregion
				
				objReportElem.GetElementsByTagName("sql").Item(0).InnerText=" ";
				objReportXML.AppendChild(objReportElem);
				string sXML= objReportXML.OuterXml;								
				sXML =sXML.Replace("'","''");

				#region Inserting the new report definition in the database
				string       sReportName="";
				sReportName= m_sReportName.Replace("'","''");

				objConn = DbFactory.GetDbConnection(m_sConnectString);
				objConn.Open();
				if(ReportId == 0)
				{
					long  lReportId=0;
                    lReportId = (long)Utilities.GetNextUID(m_sConnectString, "DCC_REPORTS", m_iClientId);										
					 
					ReportId = lReportId;
					sSql = "SELECT * FROM DCC_REPORTS WHERE REPORT_ID = " + lReportId;
					objReader = objConn.ExecuteReader(sSql);
					if(objReader.Read())						
						throw new RMAppException(Globalization.GetString("Desktop.NewReportXML.ReportIDInUse",m_iClientId) + lReportId);

					objReader.Close();
					sSql = "INSERT INTO DCC_REPORTS(REPORT_ID, TEMPLATE_ID, REPORT_NAME,REPORT_XML) VALUES(" + m_lReportId + "," + m_lTemplateId + ",'" + sReportName + "','" + sXML +"')";
					objConn.ExecuteNonQuery(sSql);

					sSql = "SELECT REPORT_XML FROM DCC_REPORTS WHERE REPORT_ID = " + m_lReportId;
					objReader = objConn.ExecuteReader(sSql);
					if(!objReader.Read())
						throw new RMAppException(Globalization.GetString("Desktop.ReportIDNotFound",m_iClientId) + m_lReportId);
					
					objReader.Close();
				}				
				#endregion
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.NewReportXML.ErrorCreate",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				objReportXML = null;
				objReportElem = null;
				objOneElem = null;
				objXMLNodeList = null;						
			}
			
		}
				
		/// <summary>		
		/// This method will generate the FROM and WHERE part of the SQL query for a report.
		/// </summary>
		/// <param name="p_objXMLElement">The xml element contains the filter of the template from which the SQL query of the report is formed </param>		
		private void MergeFilterSQL(XmlElement p_objXMLElement)
		{
			string sFilterType="";
			string sFilterData="";
			string sFilterSQLFrom="";
			string sFilterSQLWhere="";
			string sFilterParameter="";			
			string[]	 arrFilterData=null;
			string[]	 arrFromList=null;
			try
			{
				sFilterType = p_objXMLElement.GetAttribute("type");
				sFilterData = p_objXMLElement.GetAttribute("data");
				sFilterSQLFrom = p_objXMLElement.GetElementsByTagName("sqlfrom").Item(0).InnerText;
				sFilterSQLWhere = p_objXMLElement.GetElementsByTagName("sqlwhere").Item(0).InnerText;
				sFilterParameter = p_objXMLElement.GetElementsByTagName("parameter").Item(0).InnerText;

				//Appending FROM part for this filter
				
				arrFromList = sFilterSQLFrom.Split(',');				
				for(int iCounter= 0;iCounter<arrFromList.Length;++iCounter)
				{
					if (m_sFrom.IndexOf(arrFromList[iCounter])== -1)
						m_sFrom = m_sFrom + " , " + arrFromList[iCounter];
				}
				//Appending WHERE part for this filter
				if (sFilterSQLWhere.Trim() != "")
					m_sWhere = m_sWhere + " AND " + sFilterSQLWhere;

				
				#region Appending the Filter parameter to the SQL Query of the report(on the basis of filter type)
				switch(Common.Conversion.ConvertStrToInteger(sFilterType))
				{
					case FT_LIST:						
					case FT_CHECKBOX:						
					case FT_COMBOBOX:
						
							arrFilterData = sFilterData.Split(',');
							sFilterData="";
							for(int iCounter =0;iCounter<arrFilterData.Length;++iCounter)
							{
								if (iCounter > 0)
									sFilterData = sFilterData + ",";
								if (arrFilterData[iCounter].Trim() != "")
								{
									if (!Common.Utilities.IsNumeric(arrFilterData[iCounter].Trim()))
										sFilterData = sFilterData + "'" + arrFilterData[iCounter] + "'";
									else
										sFilterData = sFilterData + arrFilterData[iCounter] ;
								}
							}							
						
						//If there are data for the parameter
						if (sFilterData.Trim() != "")
							if (m_sWhere.Trim() != "")
								m_sWhere = m_sWhere + " AND " + sFilterParameter + " (" + sFilterData + ") ";
							else
								m_sWhere = " WHERE " + sFilterParameter + " " + sFilterData;
						break;
					case FT_SPIN:						
						//If there are data for the parameter
						if (sFilterData.Trim() != "")
						{
							int iPos = 0;
							int iNoOfCharfromRight=0;
							iPos = sFilterParameter.IndexOf("[TODAY]");
							//Replace [TODAY] with today's date
							if (iPos > -1)
							{
								
								iNoOfCharfromRight = sFilterParameter.Length - (iPos + 7);
								sFilterParameter = (sFilterParameter.Substring(0,iPos - 1)) + System.DateTime.Now.ToString("d") + sFilterParameter.Substring(sFilterParameter.Length - iNoOfCharfromRight,iNoOfCharfromRight);
								if (m_sWhere.Trim() != "")
									m_sWhere = m_sWhere + " AND " + sFilterParameter + " " + sFilterData;
								else
									m_sWhere = " WHERE " + sFilterParameter + " " + sFilterData;								
							}
							else
							{
								iPos = 0;
								iNoOfCharfromRight=0;
								do
								{
									iPos = sFilterParameter.IndexOf("[OPTION]");
									iNoOfCharfromRight = sFilterParameter.Length - (iPos + 8);
									if (iPos > -1)										
										sFilterParameter = sFilterParameter.Substring(0,iPos) +  sFilterData +  sFilterParameter.Substring(sFilterParameter.Length - iNoOfCharfromRight,iNoOfCharfromRight);
								}
								while(iPos != -1);
								if (m_sWhere.Trim() != "")
									m_sWhere = m_sWhere + " AND " + sFilterParameter ;
								else
									m_sWhere = " WHERE " + sFilterParameter ;														
							}
						}		
						break;
					
					case FT_TOPX:
						//If there are data for the parameter					
						int iTopX=0;
						if (sFilterData.Trim() != "")
							iTopX =Common.Conversion.ConvertStrToInteger(sFilterData.Trim());
						break;											
					case FT_DATE:
						if (sFilterData.Trim() != "")
							if (m_sWhere.Trim() != "")
								m_sWhere = m_sWhere + " AND " + sFilterParameter + " " + sFilterData;
							else
								m_sWhere = " WHERE " + sFilterParameter + " " + sFilterData;							
						break;
				}
				#endregion

			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.MergeFilterSQL.ErrorCreate",m_iClientId),p_objException);
			}	
		}
		/// <summary>		
		/// This method will form the SELECT and ORDERBY part of the SQL query for a report.
		/// </summary>
		/// <param name="p_objTemplateElem">The XML element contains the output of the template from which the SQL query of the report is formed </param>		
		/// <param name="p_objOutputElem">(By Ref.) The XML element containing the output for a report</param>		
		private void MergeOutputSQL(XmlElement p_objTemplateElem,ref XmlElement p_objOutputElem)
		{
			string sGroupBy="";
			string sDataField="";
			string sOutputJoinSelect="";
			string sOutputJoinFrom="";
			string sOutputJoinWhere="";			
			string[]	 arrSelectColumn=null;
			string[]	 arrSelectFromList=null;
			string[]	 arrSelectWhereList=null;
			try
			{
				sGroupBy = p_objOutputElem.GetAttribute("groupby");
				sDataField = p_objTemplateElem.GetElementsByTagName("datafield").Item(0).InnerText;
				sOutputJoinSelect = p_objTemplateElem.GetElementsByTagName("joinselect").Item(0).InnerText;
				sOutputJoinFrom = p_objTemplateElem.GetElementsByTagName("joinfrom").Item(0).InnerText;
				sOutputJoinWhere = p_objTemplateElem.GetElementsByTagName("joinwhere").Item(0).InnerText;
				
				p_objOutputElem.SetAttribute("field", sOutputJoinSelect);
				//The next 2 DO loops are for setting up the column header in RunReport
				//Get the # of fields to be added		
				
				int iNumOfFields=0; //Number of field for this column.  eg: Last Name + First Name = Name
				int iCounter = 0;
				int iPos=0;

				#region Calculating the column index
				if(sOutputJoinSelect.Length > 0)
				{
					do
					{
						iPos = sOutputJoinSelect.IndexOf("&&JOINSELECT&&",iPos);
						if (iPos > -1)
						{
							++iCounter;
							++iPos;
						}
					}
					while(iPos != -1);
					if(iCounter > 0)
						//if at least 1 comma found, there are at least 2 fields.
						iNumOfFields = iCounter +1;
					else
						//At least one field is in sOutputJoinSelect
						iNumOfFields = 1;
				}

				string sColumnIndex="";
				//Set the index of the adding fields
				if(m_iNextColumnIndex > 0)
				{
					//If at least 1 comma found, there are 2 fields in m_sSelect, so this will be field #3
					if (iNumOfFields > 0)
					{
						for(iCounter = 1;iCounter <= iNumOfFields;++iCounter)
							sColumnIndex = sColumnIndex + Convert.ToString((m_iNextColumnIndex  + (iCounter -1))) + ",";
						m_iNextColumnIndex = m_iNextColumnIndex + (iCounter-1);
					}
					else
					{
						sColumnIndex = m_iNextColumnIndex + ",";
						++m_iNextColumnIndex;
					}
				}
					//Nothing in the SQL string
				else
				{
					sColumnIndex="";
					//If no comma found, there is 1 field in m_sSelect, so this will be field #2
					if (iNumOfFields > 0)
					{
						for(iCounter = 1;iCounter <= iNumOfFields;++iCounter)
							sColumnIndex = sColumnIndex + Convert.ToString(iCounter) + ",";
						m_iNextColumnIndex = iCounter;
					}
					else
					{
						sColumnIndex = "1,";
						m_iNextColumnIndex=2;
					}
				}
				//Remove extra "," at the end
				if (sColumnIndex.Length > 0)
					sColumnIndex = sColumnIndex.Substring(0,sColumnIndex.Length -1);
				
				#endregion				
				
				#region Forming the SELECT part of the SQL Query

				
				p_objOutputElem.SetAttribute("columnindex",sColumnIndex);
				if (sOutputJoinSelect != "")
				{
					// Array for containing the fields delimeted by the specified delimiter string					
					arrSelectColumn = Regex.Split(sOutputJoinSelect,"&&JOINSELECT&&");
					for(iCounter=0;iCounter<arrSelectColumn.Length;++iCounter)
					{
						
						if (m_sSelect == "")
							m_sSelect = "SELECT " + arrSelectColumn.GetValue(iCounter) + " ";
						else
						{
							iPos = m_sSelect.IndexOf(" " + arrSelectColumn.GetValue(iCounter) + " ");
							if (iPos == -1)
								m_sSelect = m_sSelect + " , " + arrSelectColumn.GetValue(iCounter) + " ";
						}
					}					
				}
				
				#endregion

				#region Forming the FROM part of the SQL Query
				
				if (sOutputJoinFrom != "")
				{
					arrSelectFromList = sOutputJoinFrom.Split(',');
					for(iCounter=0;iCounter<arrSelectFromList.Length;++iCounter)
					{
						if (m_sFrom == "")
							m_sFrom = " FROM " + arrSelectFromList[iCounter] + " ";
						else
						{
							iPos = m_sFrom.IndexOf(" " + arrSelectFromList[iCounter] + " ");
							if (iPos == -1)
								m_sFrom = m_sFrom + " , " + arrSelectFromList[iCounter] + " ";
						}
					}					
				}
				
				#endregion

				#region Forming the WHERE part of the SQL Query
				
				if (sOutputJoinWhere != "")
				{
					arrSelectWhereList = sOutputJoinWhere.Split(',');
					for(iCounter=0;iCounter<arrSelectWhereList.Length;++iCounter)
					{
						if (m_sWhere == "")
							m_sWhere = " WHERE " + arrSelectWhereList[iCounter] + " ";
						else
						{
							iPos = sOutputJoinFrom.IndexOf(" " + arrSelectWhereList[iCounter] + " ");
							if (iPos == -1)
								m_sWhere = m_sWhere + " AND " + arrSelectWhereList[iCounter] + " ";
						}
					}
					
				}
				
				#endregion

				#region Forming the GROUP BY part of the SQL Query				
				if (sGroupBy == "-1")
				{
					if (m_sGroupBy == "")
						m_sGroupBy = " GROUP BY " + sDataField;
					else
						m_sGroupBy = m_sGroupBy + "," + sDataField;
				}			
				#endregion
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.MergeOutputSQL.ErrorCreate",m_iClientId),p_objException);
			}	
		}
		
		/// <summary>		
		/// This method will load the report definition and the template definition in the XML Documents
		/// These XML Documents will be worked upon by various methods.
		/// </summary>
		/// <param name="p_lReportId">The id of the report for which the XML Document has to be loaded</param>		
		/// <param name="p_lTemplateId">The id of the template on which the report is based</param>		
		private void LoadReport(long p_lReportId,long p_lTemplateId)
		{
			string sSql="";						
			DbReader objReader = null;	
			DbConnection objConn =null;	
			string sMsg="";
			try
			{
				if (p_lReportId != 0)
				{
					ReportId = p_lReportId;
					TemplateId = p_lTemplateId;						
					if (p_lTemplateId > 0)
					{
						sSql = "SELECT REPORT_NAME, REPORT_XML, TEMPLATE_NAME, TEMPLATE_XML";
						sSql = sSql + " FROM DCC_REPORTS, DCC_TEMPLATES";
						sSql = sSql + " WHERE DCC_REPORTS.TEMPLATE_ID = DCC_TEMPLATES.TEMPLATE_ID";
						sSql = sSql + " AND DCC_REPORTS.REPORT_ID = " + p_lReportId ;
						sSql = sSql + " AND DCC_TEMPLATES.TEMPLATE_ID = " + p_lTemplateId;

						sMsg = "for template id " + p_lTemplateId + " and report id " + p_lReportId;
					}
					else
					{
						sSql = "SELECT REPORT_NAME, REPORT_XML";
						sSql = sSql + " FROM DCC_REPORTS";
						sSql = sSql + " WHERE DCC_REPORTS.REPORT_ID = " + p_lReportId;

						sMsg = " for report id " + p_lReportId;
					}	
					objConn = DbFactory.GetDbConnection(m_sConnectString);
					objConn.Open();
					objReader = objConn.ExecuteReader(sSql);
					if (objReader.Read())
					{
						string sReportName= Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_NAME"));
						ReportName=sReportName;
						string sReportXML= Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_XML"));
						m_objReportXML.LoadXml(sReportXML);
						m_objReportElem = m_objReportXML.DocumentElement;

						if(p_lTemplateId > 0)
						{
							string sTemplateName = Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_NAME"));
							TemplateName =sTemplateName;
							string sTemplateXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("TEMPLATE_XML"));
							m_objTemplateXML.LoadXml(sTemplateXML);
							m_objTemplateElem = m_objTemplateXML.DocumentElement;
						}					
					}
					else
						throw new RMAppException(Globalization.GetString("Desktop.ReportIDNotFound",m_iClientId) + sMsg);
				}						
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.LoadReport.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}	
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
			}			
		}
		
		/// <summary>
		/// Overloaded method.		
		/// This method will load the report definition in the XML Document
		/// This XML Document will be worked upon by various methods.
		/// </summary>
		/// <param name="p_lReportId">The id of the report for which the XML Document has to be loaded</param>				
		private void LoadReport(long p_lReportId)
		{
			string sSql="";						
			DbReader objReader = null;		
			DbConnection objConn = null;	
			try
			{
				if (p_lReportId != 0)
				{
					ReportId = p_lReportId;										
					sSql = "SELECT REPORT_NAME, REPORT_XML";
					sSql = sSql + " FROM DCC_REPORTS";
					sSql = sSql + " WHERE DCC_REPORTS.REPORT_ID = " + p_lReportId;		

					objConn=DbFactory.GetDbConnection(m_sConnectString);
					objConn.Open();
					objReader = objConn.ExecuteReader(sSql);
					if (objReader.Read())
					{
						string sReportName= Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_NAME"));
						ReportName=sReportName;
						string sReportXML= Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_XML"));
						m_objReportXML.LoadXml(sReportXML);
						m_objReportElem = m_objReportXML.DocumentElement;					
					}
					else
						throw new RMAppException(Globalization.GetString("Desktop.ReportIDNotFound",m_iClientId) + p_lReportId);
				}								
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.LoadReport.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}				
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}			
			}			
		}
		
		/// <summary>		
		/// This method will update the existing report in the database.
		/// It will load the report and corresponding template definitions in the XML Documents using LoadReport().
		/// It will update the filters in the xml document for the report, using SelectFilters().
		/// It will update the outputs in the xml document for the report, using SelectOutputs().
		/// It will update the graphgroups, graphdata, graphtype, using UpdateReport().
		/// Finally, the SQL for the report data will be formed and will be stored in the database, using UpdateReportXML().		
		/// </summary>
		/// <param name="p_lReportId">The id of report that has to be updated</param>				
		/// <param name="p_lTemplateId">Template Id of the report (to be updated)</param>				
		/// <param name="p_sReportName">Report name</param>				
		/// <param name="p_sFilterList">Filter list separated by ","</param>				
		/// <param name="p_sOutputList">Output list separated by ","</param>				
		/// <param name="p_sGraphGroup">Graph Group</param>				
		/// <param name="p_sGraphData">Graph Data</param>				
		/// <param name="p_sGraphType">Graph Type</param>				
		/// <param name="p_sShowGraph">Report data has to be shown as graph or not</param>						
		/// <returns>The XML giving the updated report definition</returns>
		public XmlDocument EditReport(long p_lTemplateId,long p_lReportId,string p_sReportName,string p_sFilterList,string p_sOutputList,string p_sGraphGroup,string p_sGraphData,string p_sGraphType,string p_sShowGraph)
		{
			XmlDocument  objXMLDoc = null;
			try
			{
				m_objTemplateXML=new XmlDocument();		
				m_objReportXML=new XmlDocument();

				LoadReport(p_lReportId,p_lTemplateId);
				ReportName = p_sReportName;				
				SelectFilters(p_sFilterList);
				SelectOutputs(p_sOutputList);
				SelectedGraphGroup = p_sGraphGroup;
				SelectedGraphData = p_sGraphData;
				SelectedGraphType =p_sGraphType;			
				if (p_sShowGraph == null)
					throw new InvalidValueException(Globalization.GetString("Desktop.EditReport.NullShowGraphValue",m_iClientId));						
				if (p_sShowGraph.ToLower()== ON)
					ShowGraph="-1";
				else
					ShowGraph="0";
				UpdateReport();				
				UpdateReportXML();
				objXMLDoc = GetSelectedReport(p_lReportId,p_lTemplateId);
				return objXMLDoc;
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.EditReport.ErrorEdit",m_iClientId),p_objException);
			}	
			finally
			{
				DeallocateGlobalXML();
				objXMLDoc = null;
			}			
		}
		
		/// <summary>		
		/// This method will set the value of the selected attribute for all those filters that will be selected for a report.
		/// </summary>
		/// <param name="p_sSelectedFilterList">Selected filters separated by ","</param>				
		private void SelectFilters(string p_sSelectedFilterList)
		{
			string[]	 arrSelectedFiltersList=null;
			XmlNodeList objXMLFilterList=null;
			try
			{
				if (p_sSelectedFilterList != "")
				{
					arrSelectedFiltersList = p_sSelectedFilterList.Split(',');	
					objXMLFilterList = m_objTemplateElem.GetElementsByTagName("filter");
					foreach(XmlElement objFilterElem in  objXMLFilterList)
					{
						for(int iCounter=0;iCounter<arrSelectedFiltersList.Length;++iCounter)
						{
							if(objFilterElem.GetAttribute("name") == arrSelectedFiltersList[iCounter])
								objFilterElem.SetAttribute("selected","-1");
						}
					}
				}

			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SelectFilters.Error",m_iClientId),p_objException);
			}	
			finally
			{
				objXMLFilterList = null;				
			}
		}
		
		/// <summary>		
		/// This method will set the value of the selected attribute for all those outputs that will be selected for report.		
		/// </summary>
		/// <param name="p_sSelectedOutputList">Selected outputs separated by ","</param>
		private void SelectOutputs(string p_sSelectedOutputList)
		{
			string[]	 arrSelectedOutputs=null;
			XmlNodeList objXMLOutputList=null;
			try
			{
				if (p_sSelectedOutputList != "")
				{
					
					arrSelectedOutputs = p_sSelectedOutputList.Split(',');
					objXMLOutputList = m_objTemplateElem.GetElementsByTagName("output");
					foreach(XmlElement objOutputElem in  objXMLOutputList)
					{
						for(int iCounter=0;iCounter<arrSelectedOutputs.Length;++iCounter)
						{
							if(objOutputElem.GetAttribute("name") == arrSelectedOutputs[iCounter])
							{
								objOutputElem.SetAttribute("selected","-1");
								objOutputElem.SetAttribute("order",Convert.ToString(iCounter+1));								   
							}
						}
					}
				}
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SelectOutputs.Error",m_iClientId),p_objException);
			}	
			finally
			{
				objXMLOutputList = null;				
			}
		}
		
		/// <summary>		
		/// This method will sets the value of graph group, graph data and graph type for the report. 		
		/// </summary>		
		private void UpdateReport()
		{
			XmlNodeList objTemplateFilterList=null;
			XmlNodeList objReportFilterList=null;			
			XmlNodeList objGraphGroupList=null;
			XmlElement	objReportElem=null;
			try
			{
				m_objTemplateElem.SetAttribute("showgraph",ShowGraph);
				objTemplateFilterList = m_objTemplateElem.GetElementsByTagName("filter");
				objReportFilterList = m_objReportElem.GetElementsByTagName("filter");
				foreach(XmlElement objTemplateFilterElem in objTemplateFilterList)
				{
					foreach(XmlElement objReportFilterElem in objReportFilterList)
					{
						if(objTemplateFilterElem.GetAttribute("name") == objReportFilterElem.GetAttribute("name"))
						{
							objTemplateFilterElem.SetAttribute("data",objReportFilterElem.GetAttribute("data"));
							objTemplateFilterElem.SetAttribute("all",objReportFilterElem.GetAttribute("all"));
							if (objReportFilterElem.GetAttribute("name") == RANK)
								objTemplateFilterElem.SetAttribute("rankon",objReportFilterElem.GetAttribute("rankon"));
						}
					}
				}
				objTemplateFilterList = null;
				objReportFilterList = null;

				objGraphGroupList =m_objTemplateElem.GetElementsByTagName("graph");
				if (objGraphGroupList.Count == 0)
					objReportElem = m_objTemplateXML.CreateElement("graph");
				else
					objReportElem =(XmlElement) m_objTemplateXML.GetElementsByTagName("graph").Item(0);
				
				objReportElem.SetAttribute("group",SelectedGraphGroup);
				objReportElem.SetAttribute("data",SelectedGraphData);
				objReportElem.SetAttribute("type",SelectedGraphType);

				if (objGraphGroupList.Count == 0)
					m_objTemplateElem.AppendChild(objReportElem);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.UpdateReport.Error",m_iClientId),p_objException);
			}
			finally
			{
				objTemplateFilterList = null;
				objReportFilterList = null;
				objGraphGroupList = null;
				objReportElem = null;				
			}
		}

		/// <summary>		
		/// This method will generate the xml giving all the details for the selected report.		
		/// </summary>	
		/// <param name="p_lReportId">Report Id of the selected report</param>				
		/// <param name="p_lTemplateId">Template Id of the selected report</param>	
		/// <returns>The XML contains all the details of the selected report</returns>			
		public XmlDocument GetSelectedReport(long p_lReportId,long p_lTemplateId)
		{
			XmlElement objXMLElem=null;
			XmlAttribute objAttribute=null;
			XmlDocument objOutXML=null;
			try
			{
				m_objTemplateXML=new XmlDocument();		
				m_objReportXML=new XmlDocument();

				LoadReport(p_lReportId,p_lTemplateId);
				
				objOutXML= new XmlDocument();
				// Creating the xml that would be returned back
				objXMLElem=objOutXML.CreateElement("report");
				objOutXML.AppendChild(objXMLElem);		
	
				objXMLElem = null;

				objAttribute = objOutXML.CreateAttribute("name");
				objAttribute.Value = ReportName;				
				objOutXML.DocumentElement.Attributes.Append(objAttribute);

				objAttribute = objOutXML.CreateAttribute("templatename");
				objAttribute.Value = TemplateName;
				objOutXML.DocumentElement.Attributes.Append(objAttribute);

				GetSelectedFilters(ref objOutXML);
				GetSelectedOutputs(ref objOutXML);			
				SetGraphGroupList(ref objOutXML);				
				SetGraphDataList(ref objOutXML,"");
				SetGraphType(ref objOutXML);				
				objXMLElem=objOutXML.CreateElement("showgraph");
				//objXMLElem.SetAttribute("value",ShowGraph);				
                objXMLElem.InnerText = ShowGraph;                
                objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	

				return objOutXML;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetSelectedReport.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objXMLElem = null;
				objAttribute = null;
				objOutXML = null;				
				DeallocateGlobalXML();
			}			
		}
		/// <summary>		
		/// This method will generate a comma-separated list of all the selected filters for a report.
		/// This comma separated filters list is passed to SetFilterList() that will update the output xml with the selected filters.
		/// </summary>	
		/// <param name="p_objOutXML">(By ref) Selected filters are added in this XML</param>								
		private void GetSelectedFilters(ref XmlDocument p_objOutXML)
		{
			XmlNodeList objFilterList=null;
			string sFilterList="";
			try
			{
				objFilterList = m_objReportXML.GetElementsByTagName("filter"); 
				foreach(XmlElement objFilterElem in objFilterList)
				{
					if (sFilterList == "")
                        sFilterList = objFilterElem.GetAttribute("name");
					else
						sFilterList = sFilterList + "," + objFilterElem.GetAttribute("name");
				}
				SetFilterList(ref p_objOutXML,REPORT,sFilterList);
			}	
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetSelectedFilters.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objFilterList = null;
			}
			
		}
		/// <summary>		
		/// This method will generate a comma-separated list of all the outputs for a report.
		/// This comma separated list of outputs will be passed to SetOutputList() that will update the output xml with the all those outputs.  		
		/// This comma separated list of outputs will be passed to SetSelectedOutputList() that will update the output xml with the selected outputs.  		
		/// </summary>	
		/// <param name="p_objOutXML">(By Ref.)Selected outputs are added in this XML</param>								
		private void GetSelectedOutputs(ref XmlDocument p_objOutXML)
		{
			XmlNodeList objOutputList=null;
			string sOutputList="";
			int iCounter=1;
			string sOutputOrder="";
			try
			{
				objOutputList = m_objReportXML.GetElementsByTagName("output"); 
				while(iCounter <= objOutputList.Count)
				{
					foreach(XmlElement objOutputElem in objOutputList)
					{
						sOutputOrder = objOutputElem.GetAttribute("order");
						if(Common.Conversion.ConvertStrToInteger(sOutputOrder) == iCounter)
						{
							
							if (sOutputList == "")
								sOutputList = objOutputElem.GetAttribute("name");
							else
								sOutputList = sOutputList + "," + objOutputElem.GetAttribute("name");
							break;
						}
					}
					++iCounter;
				}
				SetOutputList(ref p_objOutXML,sOutputList);
				SetSelectedOutputList(ref p_objOutXML,sOutputList);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetSelectedOutputs.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objOutputList = null;
			}
			
		}

		/// <summary>		
		/// This method will add the graph group list in the xml that would finally be output for the selected report.		
		/// </summary>	
		/// <param name="p_objOutXML">(By Ref.)Graph group will be added in this XML</param>								
		private void SetGraphGroupList(ref XmlDocument p_objOutXML)
		{
			XmlNodeList objOutputList=null;
			XmlElement  objGraphElem=null;
			XmlElement objXMLElem=null;		
			XmlElement objXMLGraphGroupElem = null; 
			string sGroupName ="";			
			try
			{
				objOutputList = m_objReportXML.GetElementsByTagName("output");
				objXMLElem=p_objOutXML.CreateElement("graphgrouplist");	
				foreach(XmlElement objOutputElem in objOutputList)
				{
					if(objOutputElem.GetAttribute("groupby") == "-1")
					{
						objGraphElem = (XmlElement) m_objReportXML.GetElementsByTagName("graph").Item(0);
						sGroupName = objOutputElem.GetAttribute("name");
						objXMLGraphGroupElem = p_objOutXML.CreateElement("graphgroup");
						objXMLGraphGroupElem.SetAttribute("name",sGroupName);
						if (objGraphElem.GetAttribute("group") == sGroupName)							
							objXMLGraphGroupElem.SetAttribute("selected","-1");	
						else
							objXMLGraphGroupElem.SetAttribute("selected","0");	
						objXMLElem.AppendChild(objXMLGraphGroupElem);
					}					
				}
				p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);				
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SetGraphGroupList.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objOutputList = null;
				objGraphElem = null;
				objXMLElem = null;			
				objXMLGraphGroupElem = null;
			}
		}

		/// <summary>		
		/// This method will add graph data list in the xml that would finally be output for the selected report.				
		/// </summary>	
		/// <param name="p_objOutXML">(By Ref.)Graph data list will be added in this XML</param>								
		/// <param name="p_sRank">String to identify whether graphdata list has to be generated for report
		///  or a rank-on field has to be generated for filter  </param>											
		private void SetGraphDataList(ref XmlDocument p_objOutXML,string p_sRank)
		{
			XmlNodeList objOutputList=null;
			XmlNodeList objFilterList=null;
			XmlElement objXMLElem=null;
			XmlElement objGraphElem=null;
			XmlElement objXMLGraphDataElem = null;
			string sRankOn="";
			string sSelected="";				
			try
			{
				
				objXMLElem=p_objOutXML.CreateElement("graphdatalist");	
				objOutputList = m_objReportXML.GetElementsByTagName("output");
				objFilterList = m_objReportXML.GetElementsByTagName("filter");
				foreach(XmlElement objFilterElem in objFilterList)
				{
					if(objFilterElem.GetAttribute("name") == RANK)
						sRankOn = objFilterElem.GetAttribute("rankon");
				}
				foreach(XmlElement objOutputElem in objOutputList)
				{
					if(objOutputElem.GetAttribute("groupby") == "0")
					{
						if (objOutputElem.GetAttribute("name") != RANK) 
						{
							if (p_sRank == RANK_ON)
							{
								if (sRankOn == objOutputElem.GetAttribute("name"))
									sSelected = "-1";
								else
									sSelected = "0";
							}
							else
							{
								objGraphElem = (XmlElement) m_objReportXML.GetElementsByTagName("graph").Item(0);
								if (objGraphElem.GetAttribute("data") == objOutputElem.GetAttribute("name"))
									sSelected = "-1";
								else
									sSelected = "0";
							}
							objXMLGraphDataElem = p_objOutXML.CreateElement("graphdata");
							objXMLGraphDataElem.SetAttribute("name",objOutputElem.GetAttribute("name"));
							objXMLGraphDataElem.SetAttribute("selected",sSelected);	
							objXMLElem.AppendChild(objXMLGraphDataElem);
						}
					}
				}
				p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);																				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SetGraphDataList.ErrorGenerate",m_iClientId),p_objException);
			}		
			finally
			{
				objOutputList = null;
				objFilterList = null;
				objXMLElem = null;
				objGraphElem = null;
				objXMLGraphDataElem = null;				 
			}
		}

		/// <summary>		
		/// This method will add the graph type in the XML that would finally be out for the selected report.		
		/// </summary>	
		/// <param name="p_objOutXML">(By Ref.)Graph type is added in this XML</param>								
		private void SetGraphType(ref XmlDocument p_objOutXML)
		{
			XmlNodeList objGraphList=null;
			XmlElement objXMLElem=null;
			string sGraphType="";			
			XmlElement objGraphElement=null;;
			try
			{
			
				objXMLElem=p_objOutXML.CreateElement("graphtype");	
				objGraphList = m_objReportXML.GetElementsByTagName("graph");
				if (objGraphList.Count ==0)
					sGraphType="-1";
				else
				{
					
					objGraphElement =(XmlElement) m_objReportXML.GetElementsByTagName("graph").Item(0);
					if (objGraphElement.GetAttribute("type")=="")
						sGraphType="-1";
					else
						sGraphType=objGraphElement.GetAttribute("type");
				}
				objXMLElem.SetAttribute("value",sGraphType);
				p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);					
			}	
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SetGraphType.ErrorSet",m_iClientId),p_objException);
			}
			finally
			{
				objGraphList = null;
				objXMLElem = null;			
				objGraphElement = null;				  
			}
		}

		/// <summary>		
		/// This method will generate the xml giving all the details for the selected template.		
		/// </summary>			
		/// <param name="p_lTemplateId">Template Id of the selected template</param>	
		/// <returns>The XML containing all the details for the selected template</returns>
		public XmlDocument GetSelectedTemplate(long p_lTemplateId)
		{
			XmlDocument objOutXML = null;
			XmlElement objXMLElem=null;
			XmlAttribute objAttribute=null;						
			try
			{
				m_objTemplateXML=new XmlDocument();		
				LoadTemplate(p_lTemplateId);

				objOutXML = new XmlDocument();
				objXMLElem=objOutXML.CreateElement("template");
				objOutXML.AppendChild(objXMLElem);			

				objAttribute = objOutXML.CreateAttribute("id");
				objAttribute.Value = p_lTemplateId.ToString();				
				objOutXML.DocumentElement.Attributes.Append(objAttribute);
				
				string sTemplateName = m_objTemplateElem.GetAttribute("name");
				objAttribute = objOutXML.CreateAttribute("name");
				objAttribute.Value = sTemplateName;				
				objOutXML.DocumentElement.Attributes.Append(objAttribute);
			
				string sShowPercentage = m_objTemplateElem.GetAttribute("showpercent");
				objAttribute = objOutXML.CreateAttribute("showpercentage");
				objAttribute.Value = sShowPercentage;
				objOutXML.DocumentElement.Attributes.Append(objAttribute);

				string sShowgraph = m_objTemplateElem.GetAttribute("showgraph");
				objAttribute = objOutXML.CreateAttribute("showgraph");
				objAttribute.Value = sShowgraph;				
				objOutXML.DocumentElement.Attributes.Append(objAttribute);

				SetFilterList(ref objOutXML,TEMPLATE,"");
				SetOutputList(ref objOutXML,"");

				return(objOutXML);
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetSelectedTemplate.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objOutXML = null;
				objXMLElem = null;
				objAttribute = null;	
				DeallocateGlobalXML();
			}			
		}

		/// <summary>		
		/// This method will return the data corresponding to a selected filter, in xml format.
		/// It will call GetFilter() to get the filter data.
		/// </summary>			
		/// <param name="p_lReportId">Id of the report whose filter has been selected</param>	
		/// <param name="p_lTemplateId">Template Id of the report</param>	
		/// <param name="p_sFilterName">Name of the filter that has been selected</param>	
		/// <returns>The XML contains all the details (data) of the selected filter</returns>
		public XmlDocument GetSelectedFilterData(long p_lTemplateId,long p_lReportId,string p_sFilterName)
		{
			XmlDocument objOutXML = null;
			XmlElement objXMLElem=null;
			try
			{
				m_objTemplateXML=new XmlDocument();		
				m_objReportXML=new XmlDocument();

				LoadReport(p_lReportId,p_lTemplateId);	

				objOutXML = new XmlDocument();
				objXMLElem=objOutXML.CreateElement("Selectedfilter");					
				objOutXML.AppendChild(objXMLElem);			
				SetGraphDataList(ref objOutXML,"rankon");				
				GetFilter(p_sFilterName,ref objOutXML);
				return objOutXML;
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetFilterData.ErrorLoad",m_iClientId),p_objException);
			}	
			finally
			{
				objOutXML = null;
				objXMLElem = null;				
				DeallocateGlobalXML();
			}			
		}

		/// <summary>		
		/// This method will fetch the data from the data form the database for the selected filter, on the basis of filter definition.
		/// </summary>						
		/// <param name="p_sFilterName">Name of the filter that has been selected</param>	
		/// <param name="p_objOutXML">(By Ref.) Filter data is added in this XML.</param>	
		private void GetFilter(string p_sFilterName,ref XmlDocument p_objOutXML)
		{
			XmlNodeList objTemplateFilterList=null;
			XmlElement objXMLElem=null;
			string sSql="";			
			string sCheckAll="";
			DbReader objReader=null;
			DbConnection objConn =null;				
			string[]	 arrFilterDataList=null;
			XmlElement objXMLFilterDataElem =null;
			string sFilterValue ="";
			string sFilterData ="";
			int iCounter=0;
			string sSelected="";
			try
			{
				objTemplateFilterList = m_objTemplateElem.GetElementsByTagName("filter");
				foreach(XmlElement objTemplateElem in objTemplateFilterList)
				{
					if(objTemplateElem.GetAttribute("name") ==p_sFilterName )
					{
						switch (Common.Conversion.ConvertStrToInteger(objTemplateElem.GetAttribute("type")))
						{
							case FT_LIST:
								sSql = objTemplateElem.GetElementsByTagName("sql").Item(0).InnerText + " " + objTemplateElem.GetElementsByTagName("orderby").Item(0).InnerText;
								
								foreach(XmlElement objReportElem in m_objReportXML.GetElementsByTagName("filter"))
								{
									if(objReportElem.GetAttribute("name") == p_sFilterName)
									{
										sCheckAll = objReportElem.GetAttribute("all");										
										arrFilterDataList = objReportElem.GetAttribute("data").Split(',');
										break;
									}
								}
								
								if(sCheckAll != "0")
									sCheckAll = "-1";							

								objConn=DbFactory.GetDbConnection(m_sConnectString);
								objConn.Open();
								objReader = objConn.ExecuteReader(sSql);								
								objXMLElem=p_objOutXML.CreateElement("filter");	
								while(objReader.Read())
								{
									sFilterData ="";																		
									for(iCounter =0;iCounter< objReader.FieldCount-1;++iCounter)
									{
										sFilterValue= objReader[iCounter].ToString();
										if (sFilterData !="") sFilterData = sFilterData + " ";
										sFilterData = sFilterData + sFilterValue;
									}
									sFilterValue= objReader[iCounter].ToString();
									sSelected="0";
									if ((arrFilterDataList!=null)&&(arrFilterDataList.Length > 0))
									{
										for(iCounter = 0;iCounter< arrFilterDataList.Length;++iCounter)
										{
											if(arrFilterDataList[iCounter].Trim() == sFilterValue)
												sSelected="-1";
										}
									}
									// Create the return XML
									objXMLFilterDataElem = p_objOutXML.CreateElement("filterdata");
									objXMLFilterDataElem.SetAttribute("data",sFilterData);
									objXMLFilterDataElem.SetAttribute("value",sFilterValue);	
									objXMLFilterDataElem.SetAttribute("selected",sSelected);	
									objXMLElem.AppendChild(objXMLFilterDataElem);

								}								
								objReader.Close();
								objConn.Close();

								objXMLElem.SetAttribute("name",p_sFilterName);
								objXMLElem.SetAttribute("type",FT_LIST.ToString());	
								objXMLElem.SetAttribute("checkall",sCheckAll);	
								p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	

								break;
							case FT_TOPX:
								objXMLElem=p_objOutXML.CreateElement("filter");	
								foreach(XmlElement objReportElem in m_objReportXML.GetElementsByTagName("filter") )
								{
									if(objReportElem.GetAttribute("name") == p_sFilterName)
									{
										objXMLFilterDataElem = p_objOutXML.CreateElement("filterdata");
										objXMLFilterDataElem.SetAttribute("data",objReportElem.GetAttribute("data"));										
										objXMLFilterDataElem.SetAttribute("value","0");										
										objXMLFilterDataElem.SetAttribute("selected","0");										
										objXMLElem.AppendChild(objXMLFilterDataElem);
										break;
									}
								}
								objXMLElem.SetAttribute("name",p_sFilterName);
								objXMLElem.SetAttribute("type",FT_TOPX.ToString());	
								objXMLElem.SetAttribute("checkall","0");
								p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);								
								break;
							case FT_CHECKBOX:
							case FT_SPIN:
							case FT_DATE:

								objXMLElem=p_objOutXML.CreateElement("filter");
								foreach(XmlElement objReportElem in m_objReportXML.GetElementsByTagName("filter"))
								{
									if (objReportElem.GetAttribute("name") == p_sFilterName )
									{
										sCheckAll=objReportElem.GetAttribute("all");
										objXMLFilterDataElem = p_objOutXML.CreateElement("filterdata");
										objXMLFilterDataElem.SetAttribute("data",objReportElem.GetAttribute("data"));										
										objXMLFilterDataElem.SetAttribute("value","0");										
										objXMLFilterDataElem.SetAttribute("selected",sCheckAll);										
										objXMLElem.AppendChild(objXMLFilterDataElem);
										break;
									}
								}
								objXMLElem.SetAttribute("name",p_sFilterName);
								objXMLElem.SetAttribute("type",Common.Conversion.ConvertStrToInteger(objTemplateElem.GetAttribute("type")).ToString());		
								objXMLElem.SetAttribute("checkall",sCheckAll);
								p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);																
								break;
							case FT_COMBOBOX:
								sSql = objTemplateElem.GetElementsByTagName("sql").Item(0).InnerText + " " + objTemplateElem.GetElementsByTagName("orderby").Item(0).InnerText;
								string sSelectedFilterData="";
								foreach(XmlElement objReportElem in m_objReportXML.GetElementsByTagName("filter"))
								{
									if(objReportElem.GetAttribute("name") == p_sFilterName)
									{
										sSelectedFilterData = objReportElem.GetAttribute("data");
										break;
									}
								}
								objConn=DbFactory.GetDbConnection(m_sConnectString);
								objConn.Open();
								objReader = objConn.ExecuteReader(sSql);								
								objXMLElem=p_objOutXML.CreateElement("filter");	
								while(objReader.Read())
								{
									sFilterData ="";																		
									for(iCounter =0;iCounter< objReader.FieldCount-1;++iCounter)
									{
										sFilterValue= objReader[iCounter].ToString();
										if (sFilterData !="") sFilterData = sFilterData + " ";
										sFilterData = sFilterData + sFilterValue;
									}
									sFilterValue= objReader[iCounter].ToString();
									sSelected="0";
									if(sSelectedFilterData == sFilterValue)
										sSelected="-1";
									
									// Create the return XML
									objXMLFilterDataElem = p_objOutXML.CreateElement("filterdata");
									objXMLFilterDataElem.SetAttribute("data",sFilterData);
									objXMLFilterDataElem.SetAttribute("value",sFilterValue);	
									objXMLFilterDataElem.SetAttribute("selected",sSelected);	
									objXMLElem.AppendChild(objXMLFilterDataElem);

								}								
								objReader.Close();
								objConn.Close();

								objXMLElem.SetAttribute("name",p_sFilterName);
								objXMLElem.SetAttribute("type",FT_COMBOBOX.ToString());									
								objXMLElem.SetAttribute("checkall","0");
								p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	

								break;							
						}
					}
				}    
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetFilter.ErrorLoad",m_iClientId),p_objException);
			}				
			finally
			{
				if(objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				objTemplateFilterList = null;
				objXMLElem = null;					
				objXMLFilterDataElem = null;
			}
		}
		
		/// <summary>		
		/// This method will update the filter criteria as selected by user.
		/// It will load the corresponding report and template into XML Document (LoadReport).
		/// It will update the filter criteria as selected by the user in that loaded xml and will finally save
		/// that report xml into the database (SaveFilter).
		/// </summary>				
		/// <param name="p_lTemplateId">Template id</param>	
		/// <param name="p_lReportId">Report Id</param>					
		/// <param name="p_sFilterName">Name of the filter, for which the criteria has to be updated</param>					
		/// <param name="p_sFilterAll">Represent whether all the values of the filter have been selected </param>					
		/// <param name="p_sFilterData">Selected filter data separated by "," </param>					
		/// <param name="p_sFilterType">Represent the filter type</param>					
		/// <param name="p_sSelectedRank">Represent the selected rank-on field</param>					
		public void UpdateFilterData(long p_lTemplateId,long p_lReportId,string p_sFilterName,string p_sFilterAll,string p_sFilterData,string p_sFilterType,string p_sSelectedRank)
		{
			try
			{
				m_objTemplateXML=new XmlDocument();		
				m_objReportXML=new XmlDocument();

				LoadReport(p_lReportId,p_lTemplateId);				
				SaveFilter(p_sFilterName,p_sFilterAll,p_sFilterData,p_sFilterType,p_sSelectedRank);
			}						
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.UpdateFilterData.ErrorUpdate",m_iClientId),p_objException);
			}	
			finally
			{
				DeallocateGlobalXML();
			}
		}
		
		/// <summary>		
		/// This method will update the filter criteria as selected by the user in the xml loaded for the report.
		/// It will save the updated report xml into the database.
		/// </summary>				
		/// <param name="p_sFilterName">Name of the filter, for which the criteria has to be updated</param>					
		/// <param name="p_sFilterAll">Represent whether all the data values of the filter have been selected </param>					
		/// <param name="p_sFilterData">Selected filter data separated by "," </param>					
		/// <param name="p_sFilterType">Represent the filter type</param>					
		/// <param name="p_sSelectedRank">Represent the selected rank-on field</param>					
		private void SaveFilter(string p_sFilterName,string p_sFilterAll,string p_sFilterData,string p_sFilterType,string p_sSelectedRank)
		{
			XmlNodeList objReportFilterList=null;			
			DbConnection objConn = null;	
			try
			{
				
				objReportFilterList = m_objReportElem.GetElementsByTagName("filter");
				foreach(XmlElement objReportElem in objReportFilterList)
				{
					if(objReportElem.GetAttribute("name") == p_sFilterName)
					{
						objReportElem.SetAttribute("data",p_sFilterData);
						objReportElem.SetAttribute("all",p_sFilterAll);						
						objReportElem.SetAttribute("rankon",p_sSelectedRank);
					}
				}				
				string sXML = m_objReportXML.OuterXml;
				sXML=sXML.Replace("'","''");
				string sSql = "UPDATE DCC_REPORTS SET REPORT_NAME = '" + ReportName + "', REPORT_XML = '" + sXML + "' WHERE REPORT_ID = " + ReportId;
				objConn = DbFactory.GetDbConnection(m_sConnectString);	
				objConn.Open();
				objConn.ExecuteNonQuery(sSql);				
			}	
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.SaveFilter.ErrorUpdate",m_iClientId),p_objException);
			}		
			finally
			{
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				objReportFilterList = null;							
			}
		}

		/// <summary>		
		/// This method will return the report data in the XML format.
		/// It will call the method to generate the report data - RunReport().
		/// The xml would be generated for the report data, using ReportHtml().
		/// </summary>		
		/// <param name="p_lReportId">Id of the report that has to be generated</param>	
		/// <returns>Report data in the XML format</returns>
		public XmlDocument GetReportData(long p_lReportId)
		{
			XmlDocument objOutXML = null;
			XmlElement objXMLElem=null;			
			try
			{
				m_objReportXML=new XmlDocument();
				LoadReport(p_lReportId);				
				RunReport();

				objOutXML = new XmlDocument();
				objXMLElem=objOutXML.CreateElement("report");	
				objXMLElem.SetAttribute("name",ReportName);
				objXMLElem.SetAttribute("showgraph",ShowGraph);				
				objOutXML.AppendChild(objXMLElem);
				ReportHtml(objOutXML);
				return objOutXML;
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetReportData.ErrorGenerate",m_iClientId),p_objException);
			}	
			finally
			{
				objOutXML = null;
				objXMLElem = null;			
				DeallocateGlobalXML();
			}			
		}				
		/// <summary>		
		/// This method will run the SQL query for generating the data from the database, for the report.
		/// </summary>		
		private void RunReport()
		{
			XmlElement objXMLElem=null;			
			XmlElement objReportElem=null;
			XmlNodeList objXMLNodeList=null;
			XmlElement objOneElem=null;
			XmlElement objColumnElem=null;
			XmlElement objRowElem=null;
			DbDataAdapter objDataAdapter=null;
			DataSet objDataSet =null;				
			string[]	 arrColumnIndexList=null;
			string sSql="";									
			int iCounter=0;
			int iPosition=0;
			long lRank=0;
			long lTotalCount=0;						
			long lEntryCount=0;					
			try
			{
				
				objXMLElem = (XmlElement) m_objReportXML.GetElementsByTagName("sql").Item(0);				
				if (objXMLElem == null)					
					throw new XmlOperationException(Globalization.GetString("Desktop.RunReport.ElementNotFound",m_iClientId));
				else
				{
					sSql = objXMLElem.InnerText;
					//*************Added by:Mohit Yadav for Bug No. 000667*************
					sSql = sSql.Replace("''","'");					
					if (sSql.IndexOf('?')>=0)
						sSql = sSql.Replace("?",",");
					if (sSql.IndexOf("\"")>=0)
						sSql = sSql.Replace("\"","'");
				}
				
				objXMLElem = null;

				objReportElem = (XmlElement) m_objReportXML.GetElementsByTagName("report").Item(0);
				if (objReportElem.GetElementsByTagName("data").Count > 0)
					objReportElem.RemoveChild(objReportElem.SelectSingleNode("data"));
				
				objXMLNodeList = m_objReportXML.GetElementsByTagName("filter");				
				if(objXMLNodeList != null)
					foreach(XmlElement objOneElem1 in objXMLNodeList)
						if(objOneElem1.GetAttribute("name") == RANK)
							lRank = (objOneElem1.GetAttribute("data") == "") ? 0: Common.Conversion.ConvertStrToLong(objOneElem1.GetAttribute("data"));
				
				objDataSet = DbFactory.GetDataSet(m_sConnectString,sSql,m_iClientId);//sonali
				
				// Getting the count of rows in order to check that the dataset for the
				// Query is not empty
				long lRowCount =0;
				lRowCount = objDataSet.Tables[0].Rows.Count;				
				if(lRowCount > 0)
				{
					objOneElem = m_objReportXML.CreateElement("data");
					objReportElem.AppendChild(objOneElem);

					iCounter = 1;
					objXMLNodeList = m_objReportElem.GetElementsByTagName("output");
					string sColumnIndex="";
					int iPercentageColumn=0;
					int iRankColumn =0;

					#region Adding columnheader elements to the report data
					foreach(XmlElement objOutputElem in objXMLNodeList)
					{
						iPosition = 1;
						sColumnIndex = objOutputElem.GetAttribute("columnindex");
						do
						{
							iPosition = sColumnIndex.IndexOf(",",iPosition);
							if (iPosition != -1)
							{
								++iPosition;
								++iCounter;
							}
						}
						while(iPosition != -1);
						objOneElem = m_objReportXML.CreateElement("columnheader");
						objOneElem.SetAttribute("name", objOutputElem.GetAttribute("name"));

						switch(objOneElem.GetAttribute("name"))
						{
							
							case PERCENTAGE_TO_WHOLE :
								iPercentageColumn = iCounter;
								break;
							case RANK :
								iRankColumn = iCounter;
								break;
						}
						++iCounter;
						objOneElem.InnerText = objOutputElem.GetAttribute("label");
						objOneElem.SetAttribute("field",objOutputElem.GetAttribute("field"));
						objOneElem.SetAttribute("columnindex",objOutputElem.GetAttribute("columnindex"));
						objReportElem.GetElementsByTagName("data").Item(0).AppendChild(objOneElem);
					}
					#endregion

					bool bEntryCountExist = false;

					foreach(DataColumn objDataCol in objDataSet.Tables[0].Columns)
					{
						if (objDataCol.ColumnName == ENTRY_COUNT)
						{
							bEntryCountExist = true;
							break;
						}
					}
					
					if(bEntryCountExist)
					{
						foreach(DataRow objDataRow in objDataSet.Tables[0].Rows)						
							lTotalCount = lTotalCount + Common.Conversion.ConvertStrToLong(objDataRow["ENTRY_COUNT"].ToString());						
					}
					
					int iColumnPos=0;
					string sColumnData="";
					string sColumnName="";
					int iTotalColumnCnt = 0;
					iTotalColumnCnt  = objDataSet.Tables[0].Columns.Count;
					// This boolean variable is used to exit the following for loop
					// in case the TOP N data rows have been accessed
					bool bAllTop = false;

					#region Adding the row elements to the report data
					foreach(DataRow objDataRow in objDataSet.Tables[0].Rows)					
					{						
						objRowElem = m_objReportXML.CreateElement("row");
						 
						for(iColumnPos=1;iColumnPos <= iTotalColumnCnt;++iColumnPos)
						{
							sColumnData=null;
							sColumnData= objDataRow[iColumnPos-1].ToString();							
							if((sColumnData== null) || (sColumnData== ""))
								sColumnData = "N/A";	
							sColumnName=objDataSet.Tables[0].Columns[iColumnPos-1].ColumnName;
							if(sColumnName.IndexOf("DATE") > -1)
								// d is the format for the short date
								sColumnData= Common.Conversion.GetDBDateFormat(sColumnData,"d");
							else if(sColumnName.IndexOf("DTTM") > -1)
								sColumnData= Common.Conversion.GetDBDTTMFormat(sColumnData,"d","t");
							else if(sColumnName.IndexOf("TIME") > -1)
								sColumnData= Common.Conversion.GetDBTimeFormat(sColumnData,"T");

							objColumnElem = m_objReportXML.CreateElement("column");

							if(iColumnPos == iPercentageColumn)
							{
								objColumnElem.SetAttribute("columnname","PERCENT");
								objColumnElem.SetAttribute("name","percenttowhole");
								objRowElem.AppendChild(objColumnElem);							
								objColumnElem = null;
								objColumnElem = m_objReportXML.CreateElement("column");
							}
							else if(iColumnPos == iRankColumn)
							{
								objColumnElem.SetAttribute("columnname","RANK");
								objColumnElem.SetAttribute("name","rank");
								objColumnElem.InnerText=Convert.ToString(objReportElem.GetElementsByTagName("row").Count+1);
								objRowElem.AppendChild(objColumnElem);
								objColumnElem=null;
								objColumnElem = m_objReportXML.CreateElement("column");								
								if (iColumnPos != 1)
									++iColumnPos;
							}
							objColumnElem.SetAttribute("columnname",sColumnName);

							objXMLNodeList = m_objReportElem.GetElementsByTagName("columnheader");							
							foreach(XmlElement	objTempElem in objXMLNodeList)
							{
								arrColumnIndexList = objTempElem.GetAttribute("columnindex").Split(',');
								for(int iTempCounter=0;iTempCounter<arrColumnIndexList.Length;iTempCounter++)
								{
									if(iColumnPos == Common.Conversion.ConvertStrToInteger(arrColumnIndexList[iTempCounter]))
									{
										objColumnElem.SetAttribute("name",objTempElem.GetAttribute("name"));
										break;
									}
								}
							}						
							objXMLNodeList = null;

							if(sColumnName == ENTRY_COUNT)	
							{
								objColumnElem.InnerText = (sColumnData == null) ? "0": sColumnData;
								lEntryCount=Common.Conversion.ConvertStrToLong(sColumnData);
							}
							else
								objColumnElem.InnerText = (sColumnData == null) ? "": sColumnData;

							objRowElem.AppendChild(objColumnElem);
							if(iColumnPos  ==iTotalColumnCnt)
							{
								//Percent Column is the last column								
								if( (iPercentageColumn != 0) && (iPercentageColumn == iColumnPos+1))
								{
									objColumnElem = null;
									objColumnElem = m_objReportXML.CreateElement("column");
									objColumnElem.SetAttribute("columnname", "PERCENT");
									objColumnElem.SetAttribute( "name", "percenttowhole");
									objRowElem.AppendChild(objColumnElem);
								}
									
								// If Rank column is the last column
								else if( (iRankColumn != 0) && (iRankColumn == iColumnPos+1) )
								{
									objColumnElem = null;
									objColumnElem = m_objReportXML.CreateElement("column");
									objColumnElem.SetAttribute("columnname", "RANK");
									objColumnElem.SetAttribute("name", "rank");
									objColumnElem.InnerText =Convert.ToString(objReportElem.GetElementsByTagName("row").Count+1);
									objRowElem.AppendChild(objColumnElem);
								}
							}
							
							if((objReportElem.GetElementsByTagName("row").Count + 1 > lRank) && (lRank > 0))
							{
								bAllTop = true;
								break;						
							}
						}	
						if (bAllTop)
							break;
						if(iPercentageColumn > 0)
							objRowElem.ChildNodes.Item(iPercentageColumn-1).InnerText = (lTotalCount == 0) ? "" : Common.Conversion.GetPercentFormat((double)(lEntryCount)/(double)(lTotalCount));
													
							objReportElem.GetElementsByTagName("data").Item(0).AppendChild(objRowElem);
					
						objRowElem = null;
					}	
#endregion
				}	
			}	
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}			
			finally
			{
				objXMLElem = null;
				objReportElem = null;
				objXMLNodeList = null;
				objOneElem = null;
				if (objDataAdapter != null)
					objDataAdapter = null;
				if (objDataSet != null)
					objDataSet.Dispose();				
				objRowElem=null;
			}
		}
		/// <summary>		
		/// This method will generate the xml for the report data, as generated by RunReport().
		/// </summary>		
		/// <param name="p_objOutXML">XML containing the report data</param>							
		private void ReportHtml(XmlDocument p_objOutXML)
		{
			
			XmlNodeList objNodeList=null;
			XmlElement objXMLElem=null;	
			XmlElement objXMLReportDataElem=null;
			int iPosition=0;
			string sColumnIndex="";
			string sAttValue="";			
			try
			{
				objXMLElem=p_objOutXML.CreateElement("columns");	
				objNodeList = m_objReportXML.GetElementsByTagName("columnheader");
				
				foreach(XmlElement objElement in objNodeList)
				{
					iPosition = 1;
					sAttValue=objElement.GetAttribute("columnindex");
					while((iPosition != -1) && (iPosition <= sAttValue.Length))
					{
						iPosition = sAttValue.IndexOf(",",iPosition);						
						if (iPosition != -1)
							++iPosition;
					}
					
					sColumnIndex = sColumnIndex + "," +  sAttValue;

					objXMLReportDataElem = p_objOutXML.CreateElement("columnheader");
					objXMLReportDataElem.SetAttribute("name",objElement.InnerText);						
					objXMLElem.AppendChild(objXMLReportDataElem);
				}
				p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	

				objNodeList = m_objReportXML.GetElementsByTagName("row");
				foreach(XmlElement objElement in objNodeList)
				{
					objXMLElem = p_objOutXML.CreateElement("row");
					for(int iTempCounter=0;iTempCounter<=objElement.ChildNodes.Count;++iTempCounter)
					{
						if(sColumnIndex.IndexOf(Convert.ToString(iTempCounter+1)) > -1)
						{
							objXMLReportDataElem = p_objOutXML.CreateElement("column");								
							objXMLReportDataElem.SetAttribute("value",objElement.ChildNodes.Item(iTempCounter).InnerText);	
							objXMLElem.AppendChild(objXMLReportDataElem);
						}
					}
					p_objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}					
			}	
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.ReportHtml.ErrorGenerate",m_iClientId),p_objException);
			}	
			finally
			{
				objNodeList = null;
				objXMLElem = null;
				objXMLReportDataElem = null;				
			}			
		}
				
		/// <summary>		
		/// This method will list all the DCC reports that are assigned to a user.		
		/// </summary>		
		/// <param name="p_iUserId">User Id</param>					
		/// <returns>XML containing list of all the reports for to a user</returns>
		public XmlDocument LoadReportsForPortal(int p_iUserId)
		{
			string sSql="";						
			XmlElement objXMLElem=null;			
			XmlDocument objOutXML= null;
			DbReader objReader = null;
			DbConnection objConn = null;	
			try
			{
				sSql = "SELECT DCC_REPORTS.REPORT_ID, TEMPLATE_ID, REPORT_NAME FROM DCC_REPORTS,DCC_REPORTS_USER WHERE (DELETED_FLAG IS NULL OR DELETED_FLAG=0) ";
				sSql = sSql + " AND DCC_REPORTS_USER.REPORT_ID=DCC_REPORTS.REPORT_ID AND DCC_REPORTS_USER.USERID=" + p_iUserId + " ORDER BY REPORT_NAME";
				
				objConn = DbFactory.GetDbConnection(m_sConnectString);	
				objConn.Open();
				objReader = objConn.ExecuteReader(sSql);

				objOutXML= new XmlDocument();
				objXMLElem=objOutXML.CreateElement("reportsforportal");
				objOutXML.AppendChild(objXMLElem);			
				while(objReader.Read())
				{
					objXMLElem = objOutXML.CreateElement("report");
					objXMLElem.SetAttribute("id",Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_ID")));
					objXMLElem.SetAttribute("name",Common.Conversion.ConvertObjToStr(objReader.GetValue("REPORT_NAME")));
					objOutXML.ChildNodes.Item(0).AppendChild(objXMLElem);	
				}
				
				objReader.Close();
				objConn.Close();
				return objOutXML;
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.LoadReportsForPortal.ErrorLoad",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)					
					{	
						objConn.Close();
						objConn.Dispose();
					}
				}
				objXMLElem = null;				
			}			
		}
		
		/// <summary>		
		/// This method will give the value of ShowGraph Property for a selected report.
		/// </summary>		
		/// <param name="p_lReportId">Report Id</param>					
		/// <returns>XML containing the value of Show Graph Property </returns>
		public XmlDocument GetShowGraphValue(long p_lReportId)
		{
			XmlDocument objOutXML = null;	
			XmlElement  objXMLElem=null;
			try
			{
				m_objReportXML=new XmlDocument();

				LoadReport(p_lReportId);
				objOutXML= new XmlDocument();
				objXMLElem=objOutXML.CreateElement("Report");
				objXMLElem.SetAttribute("id",p_lReportId.ToString());
				objXMLElem.SetAttribute("showgraph",ShowGraph.ToString());
				objOutXML.AppendChild(objXMLElem);	

				return objOutXML;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetShowGraphValue.ErrorFetch",m_iClientId),p_objException);
			}	
			finally
			{
				DeallocateGlobalXML();
				objXMLElem=null;
				objOutXML = null;
			}			
		}
		/// <summary>		
		/// This method will generate the chart displaying the report data.				
		/// </summary>		
		/// <param name="p_lReportId">Report Id</param>					
		/// <returns>XML corresponding to the chart generated.</returns>
		public XmlDocument GetChart(long p_lReportId)
		{
			XmlDocument		objXMLDoc = null;
			XmlElement		objGraphElement =null;
			XmlNodeList		objRowList=null;
			XmlElement		objReportNameElem =null;			
			MemoryStream	objStream = null;
			Chart			objChart =null;
			ArrayList		arrlstData = null;
			ArrayList		arrlstLegend = null;
			string			sNewGroup="";
			string			sCurrentGroup="";			
			string			sReportName="";
			string			sExtension="";	
			int				iCurrentIndex=0;
			int				iGraphType=0;
			int				iShowGraph=0;

			XmlElement		objElementNode=null;
			XmlDocument		objXMLDocOut = null;

			RMConfigurator objConfigurator = null;
			XmlNode		   objReportStorage = null;
			string		   sStoragePath="";

			try
			{
				m_objReportXML=new XmlDocument();
				LoadReport(p_lReportId);
				RunReport();
				
				objXMLDoc = m_objReportXML;
				arrlstData = new ArrayList();
				arrlstLegend = new ArrayList();

				objReportNameElem = (XmlElement) objXMLDoc.GetElementsByTagName("report").Item(0);
				sReportName = objReportNameElem.GetAttribute("name");
				iShowGraph = Conversion.ConvertStrToInteger(objReportNameElem.GetAttribute("showgraph"));
				objReportNameElem = null;

				if(iShowGraph==1)
				{
		
					objGraphElement= (XmlElement)objXMLDoc.GetElementsByTagName("graph").Item(0);	
					iGraphType = int.Parse(objGraphElement.GetAttribute("type"));

					objChart = new Chart(m_iClientId);
					//************Added by: Mohit Yadav for Bug No. 000667*******
					if (iGraphType >= 0)
						objChart.SetChartType(iGraphType+1);
					objChart.SetTitle(sReportName,"Arial", 13);			
					objChart.SetWidth(900);
					objChart.SetHeight(550);
					objChart.SetBackColor(System.Drawing.Color.White);
					objChart.SetLegend();

					objRowList = objXMLDoc.GetElementsByTagName("row");
					if (objRowList.Count >0)
					{
						iCurrentIndex=0;
						for (int iRowCounter=1; iRowCounter <= objRowList.Count ;++iRowCounter)
						{
							foreach (XmlElement objElement in objRowList[iRowCounter-1].ChildNodes)
							{
								if (objElement.GetAttribute("name") == objGraphElement.GetAttribute("group") )
								{
									if (sNewGroup != objElement.InnerText)
									{
										sNewGroup=objElement.InnerText;
										break;
									}
								}
							}

							foreach (XmlElement objElement in objRowList[iRowCounter-1].ChildNodes)
							{
								if (objElement.GetAttribute("name") == objGraphElement.GetAttribute("data"))
								{
									if (sCurrentGroup != sNewGroup)
									{
										sCurrentGroup = sNewGroup;
										iCurrentIndex++;	

									
										if (objElement.GetAttribute("name") == PERCENTAGE_TO_WHOLE)
										{
											//************Changed by: Mohit Yadav for Bug No. 000667*****
											//arrlstData.Add(double.Parse(objElement.InnerText.Substring(0,objElement.InnerText.Length-1)));
											if (objElement.InnerText.Length > 0)
												arrlstData.Add(double.Parse(objElement.InnerText.Substring(0,objElement.InnerText.Length-1)));
											else
												arrlstData.Add(double.Parse("0"));
										}
										else if(objElement.GetAttribute("name") == AMOUNT_PAID)
											arrlstData.Add(double.Parse(objElement.InnerText));	
										else										
											arrlstData.Add(double.Parse(objElement.InnerText));
										//arrlstData.Add(Int32.Parse(objElement.InnerText));
										//***************Changed by: Mohit Yadav for Bug No. 000667******

										arrlstLegend.Add(sNewGroup);
									}
									else
									{
									
										if (objElement.GetAttribute("name") == PERCENTAGE_TO_WHOLE)
										{
											//*********Changed by: Mohit Yadav for Bug No. 000667************
											if (objElement.InnerText.Length >= 1)
											{
												string sFString1 = objElement.InnerText.Substring(0,objElement.InnerText.Length-1);
												if (sFString1=="")sFString1="0";

												string sSString1 = arrlstData[iCurrentIndex-1].ToString();
												arrlstData.RemoveAt(iCurrentIndex-1);
												arrlstData.Add(double.Parse(sSString1) + double.Parse(sFString1));
											}
											//***************************************************************
										}
										else										
										{
											//**********Changed by: Mohit Yadav for Bug No. 000667************
											string sFString2 = objElement.InnerText;
											if (sFString2=="")sFString2="0";
											string sSString2 = arrlstData[iCurrentIndex-1].ToString();
											arrlstData.RemoveAt(iCurrentIndex-1);
											arrlstData.Add(double.Parse(sSString2) + double.Parse(sFString2));
											//***************************************************************
										}
									
									}
								}
							}
						}
					}	
					objChart.DrawChart(arrlstData,arrlstLegend);
					objStream = objChart.RenderToStream();

					sExtension = ".gif";
					
					//*********Changed By: Mohit Yadav for Bug No. 000667******************
					//if (!Directory.Exists(objReportStorage.InnerText))
					//	Directory.CreateDirectory(objReportStorage.InnerText);

                    sStoragePath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "DCCReports");
					sStoragePath += "\\";				
					//File.Delete(sStoragePath + sReportName + sExtension);

					objXMLDocOut = new XmlDocument();

					objElementNode=objXMLDocOut.CreateElement("file");
					objElementNode.SetAttribute("path", sStoragePath);
					objElementNode.SetAttribute("name", sReportName);
					objElementNode.SetAttribute("extension", sExtension);				
					objElementNode.InnerText=Convert.ToBase64String(objStream.ToArray());
					objXMLDocOut.AppendChild(objElementNode);
					return objXMLDocOut;
				}
				//******************Added by: Mohit Yadav for Bug No. 000667**************
				else
				{
					sExtension = ".html";
					objXMLDocOut = new XmlDocument();
					objElementNode = objXMLDocOut.CreateElement("file");
					objElementNode.SetAttribute("path", "");
					objElementNode.SetAttribute("name", sReportName);
					objElementNode.SetAttribute("extension", sExtension);
					objXMLDocOut.AppendChild(objElementNode);
					ReportHtml(objXMLDocOut);									
					objElementNode.InnerXml = objXMLDocOut.SelectSingleNode("//file").InnerXml;
					
					return objXMLDocOut;
				}				
				//************************************************************************
			}			
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Desktop.GetChart.Error",m_iClientId),p_objException);
			}	
			finally
			{
				DeallocateGlobalXML();				
				objXMLDoc = null;
				objGraphElement =null;
				objRowList=null;
				objReportNameElem =null;
				if(objStream != null)
				{
					objStream.Close();
					objStream = null;
				}				
				arrlstData = null;
				arrlstLegend = null;
				if (objChart != null)
					objChart.Dispose();
			}
			
		}
		
		/// <summary>		
		/// This method will nullify the xml document and xml element that has been declared for report and
		/// template definition.
		/// </summary>				
		private void DeallocateGlobalXML()
		{
			try
			{
				if (m_objTemplateXML != null)
					m_objTemplateXML = null;
				if (m_objTemplateElem != null)
					m_objTemplateElem = null;
				if (m_objReportXML != null)
					m_objReportXML = null;
				if (m_objReportElem != null)
					m_objReportElem = null;
			}
			catch{}
		}			
	}
}
		