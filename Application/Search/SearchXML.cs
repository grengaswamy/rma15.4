﻿using System;
using System.Xml;
using System.Data;
using System.Text;

using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Riskmaster.Cache;

namespace Riskmaster.Application.Search
{
	/// <summary>
	///Author  :   Sumeet Rathod
	///Dated   :   24th August 2004
	///Purpose :   This class generates the XML corresponding to the search view.
	/// </summary>
    public class SearchXML : IDisposable
	{ 
		#region Private Variables

		/// <summary>
		/// The DataModelFactory Object.
		/// </summary>		
		private DataModelFactory m_objDmf = null;
		
		/// <summary>
		/// Represents the DSN id.
		/// </summary>
		private long m_lDSN = 0;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";
        /// <summary>
        /// ClientId for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;

		#endregion

		#region Constructor

		/// <summary>
		/// This is default constructor.
		/// </summary>
		/// <param name="p_sConnectionstring">Database connection string</param>
        public SearchXML(string p_sConnectionstring, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Overloaded constructor, which sets the DataModelFactory Object.
		/// </summary>
		/// <param name="p_objDmf"> The DataModelFactory Object</param>
		public SearchXML(DataModelFactory p_objDmf)
		{
			m_objDmf = p_objDmf;
			m_sConnectionString = ((Riskmaster.Db.DbConnection)(((Riskmaster.DataModel.Context)(m_objDmf.Context)).DbConn)).ConnectionString.ToString();
            m_iClientId = m_objDmf.Context.ClientId;
		}
		# endregion

		#region Destructor
		~SearchXML()
		{
            Dispose();
		}
        public void Dispose()
        {
            if (m_objDmf != null)
                m_objDmf.Dispose();
        }
		# endregion

		#region Properties
		/// <summary>
		/// This property sets the DSN name for the underlying Riskmaster database.
		/// </summary>
        public long DSNId { get { return m_lDSN; } set { m_lDSN = value; } }


		#endregion

		#region Public Functions
		private XmlDocument GetUserPrefNode(int p_iUserId)
		{
            string sSql = "";
            DbReader objReader = null;
            XmlDocument objPrefXml = null;
            string sXml = "";
			sSql = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + p_iUserId;
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
			
			//if user xml is present in the database
			if (objReader != null && objReader.Read())
			{

				if ((Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != null) || 
					(Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != ""))
				{
                    objPrefXml = new XmlDocument();
                    sXml = Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
					objReader.Close();
                    objPrefXml.LoadXml(sXml);
				}
			}
			if (!objReader.IsClosed)
			objReader.Close();
			objReader.Dispose();
			return objPrefXml;

		}
		#region Generate Search view XML
		/// <summary>
		/// This function generates the XML containing fields corresponding to the given search criteria.
		/// </summary>
		/// <param name="p_iViewId">View id corresponding to the search module</param>
		/// <param name="p_iUserID">User id</param>
		/// <param name="p_iGroupID">Group id</param>
		/// <param name="p_sTableRestrict">Table restriction</param>
		/// <param name="p_sFormName">Form Name</param>
		/// <param name="p_sSettings">Settings related to the display</param>
		/// <param name="p_objXML">XML document containing the final search XML</param>
        public void GetSearchXML(ref int p_iViewId, int p_iUserID, int p_iGroupID, string p_sTableRestrict, string p_sFormName, string p_sSettings, ref XmlDocument p_objXML, string p_sFieldRestrict, string sLangCode, string sPageId, string p_sFullEntitySearch, string p_sUseFullEntitySearch) //JIRA-RMA-1535 //skhare7 JIRA 340 entity Role//JIRA 4633
		{
			bool bFirst = false;
			bool bSet = false;
			bool bDisplayOnly = false;
            //Start by Shivendu for MITS 7921
            bool bBestSearchDone = false;
            bool bRecordForViewId = false;
            //End by Shivendu for MITS 7921
			int iViewId = 0;
			int iSeqNum = 0;
			int iDispNum = 0;
			int iCatID = 0;
			int iCount = 0;			
            int iCountDict = 0;
            int iCountSupp = 0;
			int iDict = 0;
			int iSupp = 0;
			int iFinalCount = 0;
            int iTabIndex = 1;      //gagnihotri MITS 11441 03/12/2008
			long lFieldType = 0;
			long lLevel = 0;			
			string sSoundex = string.Empty;
            string sViewName = string.Empty;
            string sAdmTable = string.Empty;
            string sDefaultChkboxValue = string.Empty;
            //string sSQL = string.Empty;
            StringBuilder sbSQL = new StringBuilder();
            string sSearchDict = string.Empty;
            string sSearchSupp = string.Empty;			
			string[] arrTableDictVal = null;
			string[] arrTableSuppVal = null;
			string[] arrTableDict = null;
			string[] arrTableSupp = null;
			string[] arrCurrentData = null;
			string[] arrSetting = null;
			string[] arrCompleteSetting = null;
            string[] arrRestrictTables = null;
            string sRestrictTables = string.Empty;
			DbReader objReader = null;
            DbReader objRdr = null;               //Umesh
			DbReader objReaderSupp = null;
			DataSet objDataSet = null;
            XmlDocument objXmlSetting = null;     //Umesh
			XmlElement objXmlViews = null;
			XmlElement objXmlView = null;				
			XmlElement objXMLRoot = null;
			XmlElement objXMLElem = null;
			XmlElement objXMLSearchFields = null;
            XmlElement objXMLDisplayFields = null;
			XmlElement objXMLElemTable = null;
            XmlNode objSoundex = null;               //Umesh
//			DataRow objDRSearchDict = null;
//			DataRow objDRSearchSupp = null;
			DbConnection objConn = null;
			StringBuilder objDictBuild = null;
			LocalCache objCache = null;
			DataTable objDataTable = null;
            // npadhy 6415 - A new column added for Supplementals for User Lookup - Multivalue
            int isuppl = 13;
			int isearch = 11;
            bool blnSuccess = false;//skhare7 JIRA 340//JIRA 4633
            //JIRA-RMA-1535 start
            string sCATName = string.Empty;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            string sSQL = string.Empty;
            const string SearchKey = "srh";
            string sResourceVal = string.Empty;
            Dictionary<string, string> strDictResourceValues = new Dictionary<string, string>();
            int iLangCode = 0;
            //JIRA-RMA-1535 end
			try
			{
				if (p_objXML == null)
                    throw new XmlOperationException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("CommonErrors.XMLError", m_iClientId), sLangCode)); //JIRA-RMA-1535 
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objXMLRoot = p_objXML.CreateElement("search");
				objXMLElem = objXMLRoot;
                objXMLElem.SetAttribute("id", iViewId.ToString());
                objXMLElem.SetAttribute("tablerestrict", p_sTableRestrict);
                objXMLElem.SetAttribute("dsnid", m_lDSN.ToString());
				//objXMLElem.SetAttribute ("sys_ex", p_sSysEx);

				//create a attribute-value for each setting
				if (p_sSettings.Trim().Length > 0)
				{
					arrCompleteSetting = p_sSettings.Split(',');
					foreach (string sSetting in arrCompleteSetting)
					{
						arrSetting = sSetting.Split('=');
                        if (arrSetting.Length > 1)
							objXMLElem.SetAttribute(arrSetting[0], arrSetting[1]);
					}
				}
                if (p_sTableRestrict.Trim().Length > 0)
                {
                    arrRestrictTables = p_sTableRestrict.Split(",".ToCharArray());
                    for (int index = 0; index < arrRestrictTables.Length; index++)
                    {
                        if (index == 0)
                            sRestrictTables = "'" + arrRestrictTables[index] + "'";
                        else
                            sRestrictTables = sRestrictTables + ",'" + arrRestrictTables[index] + "'";
                    }
                }

                p_objXML.AppendChild(objXMLRoot);
				
				// Setup three groups of field definitions
				objXMLSearchFields = p_objXML.CreateElement("searchfields");
				objXMLRoot.AppendChild(objXMLSearchFields);
				objXMLDisplayFields = p_objXML.CreateElement("displayfields");
                objXMLRoot.AppendChild(objXMLDisplayFields);
				
                if (p_sFormName != "" && p_iViewId == -1)
				{
					
                    iViewId = GetBestSearch(p_sFormName, p_iUserID, p_iGroupID);
					//05/05/2010 Raman Bhatia: This code seems horribly wrong. ViewID = 1 is normally standard claim search and why would be want to open in if no search is located
                    //is beyond me..
                    //if(iViewId == 0)
					//	iViewId = 1;
                    //Added by Shivendu for MITS 7921
                    bBestSearchDone = true;
				}
				else
					iViewId = p_iViewId;
                p_iViewId = iViewId;
				//fetch view name and cat id
                sbSQL.Append("SELECT VIEW_NAME, CAT_ID,VIEW_AT_TABLE,CHECKBOX_FLAG FROM SEARCH_VIEW WHERE SEARCH_VIEW.VIEW_ID = " + iViewId.ToString());

				objConn.Open();
				objReader = objConn.ExecuteReader(sbSQL.ToString());
                while (objReader.Read())
				{	
					sViewName = Conversion.ConvertObjToStr(objReader.GetValue(0));
					sAdmTable = Conversion.ConvertObjToStr(objReader.GetValue("VIEW_AT_TABLE"));
                    sDefaultChkboxValue = Conversion.ConvertObjToStr(objReader.GetBoolean("CHECKBOX_FLAG"));
					objXMLRoot.SetAttribute("name", sViewName);
					objXMLRoot.SetAttribute("admtable", sAdmTable);
					objXMLRoot.SetAttribute("viewid", p_iViewId.ToString());
                    objXMLRoot.SetAttribute("defaultcheckboxvalue", sDefaultChkboxValue);
					iCatID = Conversion.ConvertStrToInteger(objReader.GetValue(1).ToString());
					objXMLRoot.SetAttribute("catid", iCatID.ToString());
                    //Added by Shivendu for MITS 7921
                    bBestSearchDone = true;
				}
                objReader.Close();
                //objConn.Close();
                //Start by Shivendu for MITS 7921
                if (!bBestSearchDone && !bBestSearchDone)
                {
                    switch (iViewId)
                    {
                        case 1: iViewId = GetBestSearch("claim", p_iUserID, p_iGroupID);
                            if (iViewId == 0)
                                iViewId = 1;
                            break;
                        case 2: iViewId = GetBestSearch("event", p_iUserID, p_iGroupID);
                            if (iViewId == 0)
                                iViewId = 1;
                            break;
                        case 4: iViewId = GetBestSearch("entity", p_iUserID, p_iGroupID);
                            if (iViewId == 0)
                                iViewId = 1;
                            break;
                        case 5: iViewId = GetBestSearch("vehicle", p_iUserID, p_iGroupID);
                            if (iViewId == 0)
                                iViewId = 1;
                            break;
                               
                    }
                    p_iViewId = iViewId;
                    //fetch view name and cat id
                    sbSQL.Length = 0;
                    sbSQL = sbSQL.Append(string.Format("SELECT VIEW_NAME, CAT_ID,VIEW_AT_TABLE FROM SEARCH_VIEW WHERE SEARCH_VIEW.VIEW_ID = {0}", iViewId));

                    //objConn.Open();
                    objReader = objConn.ExecuteReader(sbSQL.ToString());
                    while (objReader.Read())
                    {
                        sViewName = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        sAdmTable = Conversion.ConvertObjToStr(objReader.GetValue("VIEW_AT_TABLE"));
                        objXMLRoot.SetAttribute("name", sViewName);
                        objXMLRoot.SetAttribute("admtable", sAdmTable);
                        objXMLRoot.SetAttribute("viewid", p_iViewId.ToString());
                        iCatID = Conversion.ConvertStrToInteger(objReader.GetValue(1).ToString());
                        objXMLRoot.SetAttribute("catid", iCatID.ToString());
                        //Added by Shivendu for MITS 7921
                        bBestSearchDone = true;
                    }
                    objReader.Close();
                    //objConn.Close();
                }
                //End by Shivendu for MITS 7921

				//Fill, Clean and Return.

                //akaur9 R8 Mobile Adjuster used the app setting to decide whether to display the searches made just for mobile on UI or not --Start
                //the View IDs of the searches of mobile are negative and start from -100 and continue to be less for further searches
                bool bMobileSearchOnUI = false;
                bool.TryParse(RMConfigurationManager.GetAppSetting("DisplayMobileSearchOnUI", m_sConnectionString, m_iClientId).ToString(), out bMobileSearchOnUI);
                //rsharma220 Mobility Windows changes start
                bool bMobilityWindowsSearchOnUI = false;
                bool.TryParse(RMConfigurationManager.GetAppSetting("DisplayMobileWindowsSearchOnUI", m_sConnectionString, m_iClientId).ToString(), out bMobilityWindowsSearchOnUI);

                if (bMobileSearchOnUI)
                {
                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT DISTINCT SEARCH_VIEW.VIEW_ID, SEARCH_VIEW.VIEW_NAME FROM SEARCH_VIEW,SEARCH_VIEW_PERM "
                        + string.Format(" WHERE SEARCH_VIEW.CAT_ID = {0} AND SEARCH_VIEW.VIEW_ID <> {1} AND SEARCH_VIEW.VIEW_ID > -1000 ", iCatID, iViewId)
                        + " AND SEARCH_VIEW.VIEW_ID = SEARCH_VIEW_PERM.VIEW_ID"
                        + string.Format(" AND (SEARCH_VIEW_PERM.USER_ID = {0} OR SEARCH_VIEW_PERM.GROUP_ID = {1} OR (SEARCH_VIEW_PERM.USER_ID = 0 AND SEARCH_VIEW_PERM.GROUP_ID = 0))"
                                , p_iUserID, p_iGroupID)
                    );
                }
                else if (bMobilityWindowsSearchOnUI)
                {
                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT DISTINCT SEARCH_VIEW.VIEW_ID, SEARCH_VIEW.VIEW_NAME FROM SEARCH_VIEW,SEARCH_VIEW_PERM "
                        + string.Format(" WHERE SEARCH_VIEW.CAT_ID = {0} AND SEARCH_VIEW.VIEW_ID <> {1} AND (SEARCH_VIEW.VIEW_ID <= -1000 OR SEARCH_VIEW.VIEW_ID > -100) ", iCatID, iViewId)
                        + " AND SEARCH_VIEW.VIEW_ID = SEARCH_VIEW_PERM.VIEW_ID"
                        + string.Format(" AND (SEARCH_VIEW_PERM.USER_ID = {0} OR SEARCH_VIEW_PERM.GROUP_ID = {1} OR (SEARCH_VIEW_PERM.USER_ID = 0 AND SEARCH_VIEW_PERM.GROUP_ID = 0))"
                                , p_iUserID, p_iGroupID)
                    );
                }
                else
                {
                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT DISTINCT SEARCH_VIEW.VIEW_ID, SEARCH_VIEW.VIEW_NAME FROM SEARCH_VIEW,SEARCH_VIEW_PERM "
                        + string.Format(" WHERE SEARCH_VIEW.CAT_ID = {0} AND SEARCH_VIEW.VIEW_ID <> {1} AND SEARCH_VIEW.VIEW_ID > -100 ", iCatID, iViewId)
                        + " AND SEARCH_VIEW.VIEW_ID = SEARCH_VIEW_PERM.VIEW_ID"
                        + string.Format(" AND (SEARCH_VIEW_PERM.USER_ID = {0} OR SEARCH_VIEW_PERM.GROUP_ID = {1} OR (SEARCH_VIEW_PERM.USER_ID = 0 AND SEARCH_VIEW_PERM.GROUP_ID = 0))"
                                , p_iUserID, p_iGroupID)
                    );
                }
                //rsharma220 Mobility Windows changes End
                //akaur9 R8 Mobile Adjuster used the app setting to decide whether to display the searches made just for mobile on UI or not --End
                //objConn.Open();
				objReader = objConn.ExecuteReader(sbSQL.ToString());

				bFirst = true;
				while (objReader.Read())
				{
					if (bFirst)
					{				
						objXmlViews = p_objXML.CreateElement("views");
						objXMLRoot.AppendChild(objXmlViews);
					}
					objXmlView = p_objXML.CreateElement("view");
					objXmlView.SetAttribute("id", objReader.GetValue(0).ToString());
                    objXmlView.SetAttribute("name", objReader.GetValue(1).ToString());
                    objXmlView.SetAttribute("catid", iCatID.ToString());
                    objXmlViews.AppendChild(objXmlView);
					iCount++;
					bFirst = false;
					//if none for category change message to user
				}
				if (iCount == 0)
				{
					if (objXMLElem.GetAttribute("catid") == "nosubmit")
						objXMLRoot.SetAttribute("name", "User or Group Lacks Permission to Use Any Search In This Category");
				}
                objReader.Close();
                //objConn.Close();

                //JIRA-RMA-1535 start
                if (!strDictParams.ContainsKey("PageId"))
                    strDictParams.Add("PageId", sPageId);
                sbSQL.Length = 0;
                iLangCode = Convert.ToInt32(sLangCode);
                sbSQL = sbSQL.Append(string.Format("SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND LANGUAGE_ID={0}", Convert.ToInt32(iLangCode)));
                objReader = DbFactory.ExecuteReader(ConfigurationInfo.GetViewConnectionString(m_iClientId), sbSQL.ToString(), strDictParams);
                while (objReader.Read())
                {
                    strDictResourceValues.Add(Conversion.ConvertObjToStr(objReader.GetValue(0)), Conversion.ConvertObjToStr(objReader.GetValue(1)));
                }
                objReader.Close();
                //JIRA-RMA-1535 end

                sbSQL.Length = 0;
				sbSQL = sbSQL.Append("SELECT DISTINCT SEARCH_DICTIONARY.FIELD_ID,SEARCH_VIEW_DEF.SEQ_NUM,SEARCH_VIEW_DEF.VIEW_ID,"              
				    + "SEARCH_DICTIONARY.FIELD_DESC,SEARCH_DICTIONARY.FIELD_NAME,"
				    + "SEARCH_DICTIONARY.FIELD_TABLE,SEARCH_DICTIONARY.FIELD_TYPE,"
				    + "SEARCH_DICTIONARY.OPT_MASK, SEARCH_DICTIONARY.CODE_TABLE,SEARCH_VIEW_DEF.ITEM_TYPE,"
                    //MITS 10702 by Gagan : Start 
                    + "SEARCH_VIEW_DEF.QUERY_FIELD_TYPE AS QUERY_FIELD_TYPE,"
                    //MITS 10702 by Gagan : End
                    + "SEARCH_DICTIONARY.DISPLAY_CAT" //JIRA-RMA-1535 
				    + " FROM SEARCH_VIEW_DEF,SEARCH_DICTIONARY,SEARCH_VIEW_PERM"
				    + string.Format(" WHERE SEARCH_VIEW_DEF.VIEW_ID = {0}", iViewId)
				    + " AND SEARCH_VIEW_DEF.FIELD_ID = SEARCH_DICTIONARY.FIELD_ID"
				    + " AND SUPP_FIELD_FLAG = 0"
				    + " AND SEARCH_DICTIONARY.FIELD_TYPE <> 8"
				    + " AND SEARCH_VIEW_DEF.VIEW_ID = SEARCH_VIEW_PERM.VIEW_ID"
                    + string.Format(" AND (SEARCH_VIEW_PERM.USER_ID = {0} OR SEARCH_VIEW_PERM.GROUP_ID = {1} OR (SEARCH_VIEW_PERM.USER_ID = 0 AND SEARCH_VIEW_PERM.GROUP_ID = 0))", p_iUserID, p_iGroupID)
				    + " ORDER BY SEQ_NUM");

                //objConn.Open();
				objReader = objConn.ExecuteReader(sbSQL.ToString());
                objDictBuild = new StringBuilder();
				while (objReader.Read())
				{
                    //JIRA-RMA-1535 start
                    sCATName = Conversion.ConvertObjToStr(objReader.GetValue(3));
                    strDictResourceValues.TryGetValue(Regex.Replace(SearchKey + Conversion.ConvertObjToStr(objReader.GetValue(11)) + sCATName, "[^a-zA-Z0-9]+", " ").Replace(" ", ""), out sResourceVal);
                    //JIRA-RMA-1535 end
                    objDictBuild.Append(Conversion.ConvertObjToStr(objReader.GetValue(0)) + "~" +
                            Conversion.ConvertObjToStr(objReader.GetValue(1)) + "~" +
                            Conversion.ConvertObjToStr(objReader.GetValue(2)) + "~" +
                            (sResourceVal != null ? sResourceVal : sCATName) + "~" + //JIRA-RMA-1535 
                            Conversion.ConvertObjToStr(objReader.GetValue(4)) + "~" +
                            Conversion.ConvertObjToStr(objReader.GetValue(5)) + "~");
                    //MITS 10702 by Gagan : Start
                    if (Conversion.ConvertObjToStr(objReader.GetValue(10)) == "0" || Conversion.ConvertObjToStr(objReader.GetValue(10)) == "")
                            {
                                objDictBuild.Append(Conversion.ConvertObjToStr(objReader.GetValue(6)) + "~");
                            }
                            else
                            {
                                objDictBuild.Append(Conversion.ConvertObjToStr(objReader.GetValue(10)) + "~");
                            }

                            objDictBuild.Append(
                     //MITS 10702 by Gagan : End
                    Conversion.ConvertObjToStr(objReader.GetValue(7)) + "~" +
                    Conversion.ConvertObjToStr(objReader.GetValue(8)) + "~" +
                    Conversion.ConvertObjToStr(objReader.GetValue(9)) + "~" + "^");
				}
                objReader.Close();
                //objConn.Close();

				if (objDictBuild.Length > 0)
				{
                    objDictBuild.Remove(objDictBuild.Length - 1, 1);
					sSearchDict = objDictBuild.ToString();
				}
                objDictBuild.Remove(0, objDictBuild.Length);
                sbSQL.Length = 0;
				sbSQL = sbSQL.Append("SELECT DISTINCT SUPP_DICTIONARY.FIELD_ID,SEARCH_VIEW_DEF.SEQ_NUM,SEARCH_VIEW_DEF.VIEW_ID,"
                   + "SUPP_DICTIONARY.USER_PROMPT,SUPP_DICTIONARY.SUPP_TABLE_NAME,"
                   + "SUPP_DICTIONARY.SYS_FIELD_NAME,SUPP_DICTIONARY.FIELD_TYPE,"
				   + "SUPP_DICTIONARY.IS_PATTERNED,SUPP_DICTIONARY.PATTERN,"
                   + "SUPP_DICTIONARY.CODE_FILE_ID,SEARCH_VIEW_DEF.ITEM_TYPE,"
                   + "SUPP_DICTIONARY.MULTISELECT"
                   + " FROM SEARCH_VIEW_DEF,SUPP_DICTIONARY,SEARCH_VIEW_PERM"
                   + string.Format(" WHERE SEARCH_VIEW_DEF.VIEW_ID = {0}", iViewId)
                   + " AND SEARCH_VIEW_DEF.FIELD_ID = SUPP_DICTIONARY.FIELD_ID"
                   + " AND SUPP_FIELD_FLAG <> 0"
                   + " AND SEARCH_VIEW_DEF.VIEW_ID = SEARCH_VIEW_PERM.VIEW_ID"
                    // npadhy - RMA-6346 Supplemental field which are deleted Should not be available, 
                    // If they are part of the view then also the search while rendering should not pick those fields.
                   + " AND SUPP_DICTIONARY.DELETE_FLAG <> -1"
                   + string.Format(" AND (SEARCH_VIEW_PERM.USER_ID = {0} OR SEARCH_VIEW_PERM.GROUP_ID = {1} OR (SEARCH_VIEW_PERM.USER_ID = 0 AND SEARCH_VIEW_PERM.GROUP_ID = 0))", p_iUserID, p_iGroupID)
                   + " ORDER BY SEARCH_VIEW_DEF.SEQ_NUM");
				
                //objConn.Open();
				objReaderSupp = objConn.ExecuteReader(sbSQL.ToString());
				
				while (objReaderSupp.Read())
				{
                    //RMA-4327 starts
                    sCATName = Conversion.ConvertObjToStr(objReaderSupp.GetValue(3));
                    strDictResourceValues.TryGetValue(Regex.Replace(SearchKey + Conversion.ConvertObjToStr(objReaderSupp.GetValue(4)) + sCATName, "[^a-zA-Z0-9]+", " ").Replace(" ", ""), out sResourceVal);
                    //RMA-4327 ends
                    objDictBuild.Append(Conversion.ConvertObjToStr(objReaderSupp.GetValue(0)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(1)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(2)) + "~" +
                        //RMA-4327 starts
                        //Conversion.ConvertObjToStr( objReaderSupp.GetValue(3)) + "~" + 
                             (sResourceVal != null ? sResourceVal : sCATName) + "~" +
                        //RMA-4327 ends
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(4)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(5)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(6)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(7)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(8)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(9)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(10)) + "~" +
                            Conversion.ConvertObjToStr(objReaderSupp.GetValue(11)) + "~" + "^");
				}
				objReaderSupp.Close();
                //objConn.Close();

				if (objDictBuild.Length > 0)
				{
                    objDictBuild.Remove(objDictBuild.Length - 1, 1);
					sSearchSupp = objDictBuild.ToString();
				}
                objDictBuild.Remove(0, objDictBuild.Length);

				//shruti, MITS - 8439 starts
                if (sSearchDict != "")
				{	
					arrTableDict = sSearchDict.Split('^');
                    iCountDict = arrTableDict.Length;
				}
                if (sSearchSupp != "")
				{
					arrTableSupp = sSearchSupp.Split('^');
                    iCountSupp = arrTableSupp.Length;
				}

		
				
                if (iCountDict >= 1)
                    iFinalCount = iCountDict;
                if (iCountSupp >= 1)
					iFinalCount = iFinalCount + iCountSupp;
				
				objDataTable = new DataTable();
				objDataTable.Columns.Add("Content");
				
				int iDictionary = 0;
				int iSupplemental = 0;

				DataRow objDRSearchCombi = null;
				
                for (int iCnt = 0; iCnt < iFinalCount; iCnt++)
				{
                    if (arrTableDict != null)
					{
                        if (iDictionary <= arrTableDict.Length - 1)
						{
							arrTableDictVal = arrTableDict[iDictionary].Split('~');
						}
						else
						{
							arrTableDictVal[1] = iFinalCount.ToString();
						}
					}
                    if (arrTableSupp != null)
					{
                        if (iSupplemental <= arrTableSupp.Length - 1)
						{
							arrTableSuppVal = arrTableSupp[iSupplemental].Split('~');
						}
						else
						{
							arrTableSuppVal[1] = iFinalCount.ToString();
						}
					}
                    if (arrTableDictVal != null && arrTableSuppVal != null)
					{
                        if (Conversion.ConvertStrToInteger(arrTableDictVal[1]) == Conversion.ConvertStrToInteger(arrTableSuppVal[1]))
						{
							objDRSearchCombi = objDataTable.NewRow();
							objDRSearchCombi["Content"] = arrTableDict[iDictionary];
							objDataTable.Rows.Add(objDRSearchCombi);

							objDRSearchCombi = objDataTable.NewRow();
							objDRSearchCombi["Content"] = arrTableSupp[iSupplemental];
							objDataTable.Rows.Add(objDRSearchCombi);
							iDictionary++;
							iSupplemental++;
							iCnt++;
						}
                        else if (Conversion.ConvertStrToInteger(arrTableDictVal[1]) < Conversion.ConvertStrToInteger(arrTableSuppVal[1]))
						{
							objDRSearchCombi = objDataTable.NewRow();
							objDRSearchCombi["Content"] = arrTableDict[iDictionary];
							objDataTable.Rows.Add(objDRSearchCombi);
							iDictionary++;
						}
                        else if (Conversion.ConvertStrToInteger(arrTableDictVal[1]) > Conversion.ConvertStrToInteger(arrTableSuppVal[1]))
						{
							objDRSearchCombi = objDataTable.NewRow();
							objDRSearchCombi["Content"] = arrTableSupp[iSupplemental];
							objDataTable.Rows.Add(objDRSearchCombi);
							iSupplemental++;
						}
					}
                    else if (arrTableDictVal != null)
					{
						objDRSearchCombi = objDataTable.NewRow();
						objDRSearchCombi["Content"] = arrTableDict[iDictionary];
						objDataTable.Rows.Add(objDRSearchCombi);
						iDictionary++;
					}
                    else if (arrTableSuppVal != null)
					{
						objDRSearchCombi = objDataTable.NewRow();
						objDRSearchCombi["Content"] = arrTableSupp[iSupplemental];
						objDataTable.Rows.Add(objDRSearchCombi);
						iSupplemental++;
					}

				}

                for (int iCnt = 0; iCnt < objDataTable.Rows.Count; iCnt++)
				{
					bDisplayOnly = false;

					arrTableDictVal = objDataTable.Rows[iCnt]["Content"].ToString().Split('~');

                    if (arrTableDictVal.Length == isearch)
					{
						//Row processing of Dict table
						if (arrTableDictVal[0] != "")
						{
							if (arrTableDictVal[9] == Conversion.ConvertObjToStr(0) && 
                                (arrTableDictVal[1] == Conversion.ConvertObjToStr((iSeqNum + 1))))
							{
                                if (arrTableDictVal[1] == Conversion.ConvertObjToStr((iSeqNum + 1)))
								{	
									iSeqNum = iSeqNum + 1;
									bSet = true;
									arrCurrentData = arrTableDictVal;
									
								}
							}
							else if (arrTableDictVal[9] == Conversion.ConvertObjToStr(1))
							{
								//if (arrTableDictVal[1] == Conversion.ConvertObjToStr(iDispNum + 1))
								//{
								iDispNum = iDispNum + 1;
								bSet = true;
								arrCurrentData = arrTableDictVal;
								bDisplayOnly = true;
								
								//}
							}
						}
					}
                    else if (arrTableDictVal.Length == isuppl)
					{
				
						//Row processing of Dict table
						if (arrTableDictVal[0] != "")
						{
							if (arrTableDictVal[10] == Conversion.ConvertObjToStr(0))
							{
								iSeqNum = iSeqNum + 1;
								bSet = true;
								arrCurrentData = arrTableDictVal;
									
							}
							else
							{								
								iDispNum = iDispNum + 1;
								bSet = true;
								arrCurrentData = arrTableDictVal;
								bDisplayOnly = true;

									
							}
						
						}

					}
								
					//shruti, MITS - 8439 ends
					// skip any entity table selection if search is a specific entity type search
					// Defect no. 1661: Check for cases where 'p_sTableRestrict' has a value of -1
                    //PARAG MITS 11015 
                    if ((!bDisplayOnly) && (p_sTableRestrict != "" && p_sTableRestrict != "-1" && p_sTableRestrict != "-4" && p_sTableRestrict != "otherpeople") && (arrCurrentData == arrTableDictVal))
					{					
						if ((arrCurrentData[4] == "ENTITY_TABLE_ID" && arrRestrictTables.Length < 2) || arrCurrentData[4] == "PRIMARY_POLICY_FLG")
							goto SKIP_FIELD;
					}
				
                    //Tushar:MITS#18229
                    if (arrCurrentData[0] == p_sFieldRestrict.ToUpper())
                    {
                        goto SKIP_FIELD;
                    }
                    //End:MITS#18229
					//Construct fields
					objXMLElem = p_objXML.CreateElement("field");
						
					//normal fields
					if (arrTableDictVal.Length == isearch)
					{
                        objXMLElem.SetAttribute("fieldtype", arrCurrentData[6]);
                        objXMLElem.SetAttribute("id", "FLD" + arrCurrentData[0]);
                        objXMLElem.SetAttribute("issupp", Conversion.ConvertObjToStr(0));
                        objXMLElem.SetAttribute("table", arrCurrentData[5]);
                        objXMLElem.SetAttribute("name", arrCurrentData[4]);
						objXMLElem.InnerText = arrCurrentData[3];

						long iCurr = Conversion.ConvertStrToLong(arrCurrentData[6]);
						//Add optional attributes to special field types
                        switch (iCurr)
						{
                            case 1:  //Text
								if (arrCurrentData[7] != "")
								{
                                    objXMLElem.SetAttribute("mask", arrCurrentData[7]);
								}
                                if (objXMLElem.GetAttribute("name") == "TAX_ID")//UnCommented by Amitosh for mits 27378
                                {
                                    objXMLElem.SetAttribute("type", "ssn");
                                }
                                else
                                { 
                                    objXMLElem.SetAttribute("type", "text");
                                }
								//Bug no. 000617
                              //  objXMLElem.SetAttribute ("type", "text");
								break;
                            case 2:  //Currency
                                objXMLElem.SetAttribute("type", "currency");
								break;
                            case 3:  //Code
                                objXMLElem.SetAttribute("codetable", arrCurrentData[8]);
                                objXMLElem.SetAttribute("type", "code");
								break;
                            case 4:  //Org. Hierarchy
                                objXMLElem.SetAttribute("type", "orgh");
                                objXMLElem.SetAttribute("level", "0");
								break;
                            case 5:  //Flag
                                objXMLElem.SetAttribute("type", "checkbox");
								break;
                            case 6:  //State
                                objXMLElem.SetAttribute("codetable", "STATES");
                                objXMLElem.SetAttribute("type", "code");
								break;
                            case 7:  //Date
                                objXMLElem.SetAttribute("type", "date");
								break;
                            case 9:  //Glossary
                                objXMLElem.SetAttribute("codetable", arrCurrentData[8]);
                                objXMLElem.SetAttribute("type", "tablelist");
                                //objConn.Open();
                                sbSQL.Length = 0;
                                sbSQL = sbSQL.Append("SELECT GLOSSARY.TABLE_ID,GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT");
                                if (sRestrictTables != "'otherpeople'")
                                {
                                    sbSQL = sbSQL.Append(string.Format(" WHERE GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND GLOSSARY.GLOSSARY_TYPE_CODE IN ( {0} ) ", arrCurrentData[8].Replace("|", ",")));
                                if (arrRestrictTables != null && arrRestrictTables.Length > 1)
                                    sbSQL = sbSQL.Append(string.Format("AND GLOSSARY.SYSTEM_TABLE_NAME IN ( {0} ) ORDER BY GLOSSARY_TEXT.TABLE_NAME", sRestrictTables));
                                else
                                    sbSQL = sbSQL.Append(" ORDER BY GLOSSARY_TEXT.TABLE_NAME");
                                }
                                else
                                {
                                    sbSQL = sbSQL.Append(" WHERE GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND GLOSSARY.GLOSSARY_TYPE_CODE=7 ORDER BY GLOSSARY_TEXT.TABLE_NAME");
                                }
								objReader = objConn.ExecuteReader(sbSQL.ToString());
								while (objReader.Read())
								{									
									objXMLElemTable = p_objXML.CreateElement("tablevalue");
                                    objXMLElemTable.SetAttribute("tableid", Conversion.ConvertObjToStr(objReader.GetValue(0)));
									objXMLElemTable.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(1));
									objXMLElem.AppendChild(objXMLElemTable);
								}
								objReader.Close();
                                //objConn.Close();
								break;
                            case 10:  //Free Text (memo)
                                objXMLElem.SetAttribute("type", "textml");
                                objXMLElem.SetAttribute("cols", "30");
                                objXMLElem.SetAttribute("rows", "5");
                                objXMLElem.SetAttribute("subtable", arrCurrentData[8]); //csingh7 R6 Claim Comment Enhancement
								break;
                            case 11:  //Number
                                objXMLElem.SetAttribute("type", "numeric");
								break;
                            //MITS 10702 by Gagan : Start
                            case 14:   //multicode
                                objXMLElem.SetAttribute("codetable", arrCurrentData[8]);
                                objXMLElem.SetAttribute("type", "codelist");
                                break;
                            //Shruti for MITS 11640
                            case 15:   //multistate
                                objXMLElem.SetAttribute("codetable", "STATES");
                                objXMLElem.SetAttribute("type", "codelist");
                                break;
                            //Shruti for MITS 11640 ends
                            //Shruti Leave Plan Search
                            case 18:  //Float
                                objXMLElem.SetAttribute("type", "double");
                                break;
                            //Shruti Leave Plan Search
                                //skhare7
                            case 21:  //attached record
                                objXMLElem.SetAttribute("type", "attachedrecord");
                                break;
                            //Added:Yukti, JIRA :14718
                            case 31: //check Box
                                objXMLElem.SetAttribute("type", "checkbox");
                                break;


						}	
					}
                    else if (arrTableDictVal.Length == isuppl)
					{
						lFieldType = Conversion.ConvertStrToLong(arrCurrentData[6]);
						objXMLElem.SetAttribute("fieldtype", Conversion.ConvertObjToStr(lFieldType));
                        objXMLElem.SetAttribute("id", "FLDSP" + arrCurrentData[0]);  //supp fields have negative ids so they don't collide with search_dictionary ids
                        objXMLElem.SetAttribute("issupp", Conversion.ConvertObjToStr(-1));
						objXMLElem.SetAttribute("table", arrCurrentData[4]);
						objXMLElem.SetAttribute("name", arrCurrentData[5]);
						objXMLElem.InnerText = arrCurrentData[3];

                        switch (lFieldType)
						{
                            case 0: // Text
                            case 10:
                            case 12:
                            case 13:
                            case 18: //mbahl3 mits  34144
								if (arrCurrentData[8] != "")
								{
									objXMLElem.SetAttribute("mask", arrCurrentData[8]);
								}
                                objXMLElem.SetAttribute("type", "text");
								break;
                            case 1:
								objXMLElem.SetAttribute("type", "numeric");
								break;
                            case 2:  // Currency
                                objXMLElem.SetAttribute("type", "currency");
								break;
                            case 3: // Date
                                objXMLElem.SetAttribute("type", "date");
								break;
                            case 5: //Mgaba2: Multi Text/Codes.MITS 16476                                
                                    objXMLElem.SetAttribute("type", "freecode");
                                    objXMLElem.SetAttribute("codetable", objCache.GetTableName(Conversion.ConvertStrToInteger(arrCurrentData[9])));
                                    objXMLElem.SetAttribute("cols", "30");
                                    objXMLElem.SetAttribute("rows", "5");                                                                  
                                break;
                            case 6:  // Code
								string sTableCodeTable = objCache.GetTableName(Conversion.ConvertStrToInteger(arrCurrentData[9]));
                                objXMLElem.SetAttribute("codetable", sTableCodeTable);
                                objXMLElem.SetAttribute("type", "code");
								break;
                            case 8:
								lLevel = Conversion.ConvertStrToLong(arrCurrentData[9]);
								if (lLevel == 0)
								{
                                    objXMLElem.SetAttribute("type", "orgh");
									//all lLevel to be selected
                                    objXMLElem.SetAttribute("level", Conversion.ConvertObjToStr(lLevel));
                                    objXMLElem.SetAttribute("tableid", "");
								}
								else if (lLevel >= 1005 && lLevel <= 1012)
								{									
                                    objXMLElem.SetAttribute("type", "orgh");
                                    objXMLElem.SetAttribute("level", Conversion.ConvertObjToStr(lLevel));
								}
								else
								{									
                                    objXMLElem.SetAttribute("type", "entity");//MITS ID: 33230
									string sTable = objCache.GetTableName(Convert.ToInt32(lLevel));
                                    objXMLElem.SetAttribute("level", sTable);
									//here we need to change ..i did copy from world but we need to show just
									//filtered entity from search by fieldtype
									
									if (sTable != "OTHER_PEOPLE") 
										//dont know why this is called tableid
                                        objXMLElem.SetAttribute("tableid", sTable);
								}
								break;
                            case 9:   //State
                                objXMLElem.SetAttribute("codetable", "STATES");
                                objXMLElem.SetAttribute("type", "code");
								break;
                            case 11: //MGaba2:MITS 16476 Added search for free text field type in supplementals
                                objXMLElem.SetAttribute("type", "textml");
                                objXMLElem.SetAttribute("cols", "30");
                                objXMLElem.SetAttribute("rows", "5");
								break;
                            case 14:   //multicode
								string sTableMulti = objCache.GetTableName(Conversion.ConvertStrToInteger(arrCurrentData[9]));
                                objXMLElem.SetAttribute("codetable", sTableMulti);
                                objXMLElem.SetAttribute("type", "codelist");
								break;
                            case 15: //multistate
                                objXMLElem.SetAttribute("codetable", "STATES");
                                objXMLElem.SetAttribute("type", "codelist");
								break;
                            case 16: //multientity
                                objXMLElem.SetAttribute("type", "entitylist");
								lLevel = Conversion.ConvertStrToLong(arrCurrentData[9]);
								string sTableCode = objCache.GetTableName(Conversion.ConvertStrToInteger(arrCurrentData[9]));

								if (lLevel == 0)
								{
                                    objXMLElem.SetAttribute("level", Conversion.ConvertObjToStr(lLevel));
                                    objXMLElem.SetAttribute("tableid", "");
								}
								else if (lLevel >= 1005 && lLevel <= 1012)
								{									
                                    objXMLElem.SetAttribute("level", Conversion.ConvertObjToStr(lLevel));
								}
								else
								{									
                                    objXMLElem.SetAttribute("level", sTableCode);
									
									if (sTableCode != "OTHER_PEOPLE") 
										//dont know why this is called tableid
                                        objXMLElem.SetAttribute("tableid", sTableCode);
								}

								// objXMLElem.SetAttribute ("tableid", sTableCode);
								break;
                            case 21:  //attached record
                                objXMLElem.SetAttribute("type", "attachedrecord");
                                break;
                            // npadhy Jira 6415 Handled the case for User Lookup
                            case 22:
                                objXMLElem.SetAttribute("type", "userlookup");
                                objXMLElem.SetAttribute("multiselect", Conversion.ConvertObjToStr(arrCurrentData[11]));
                                // Check if the Field is for user or group
                                sbSQL.Clear();
                                sbSQL = sbSQL.AppendFormat("SELECT USER_GROUP_IND FROM SUPP_USERGROUP_DICT WHERE FIELD_ID = {0}", arrCurrentData[0]);
                                objReader = objConn.ExecuteReader(sbSQL.ToString());
                                if (objReader.Read())
                                {
                                    objXMLElem.SetAttribute("usergroups", objReader.GetInt32("USER_GROUP_IND").ToString());
                                }
                                objReader.Close();
                                break;
                                //Added:Yukti, JIRA :14718
                            case 31: //Check Box
                                objXMLElem.SetAttribute("type", "checkbox");
                                break;
						}
					}
					

					if (bDisplayOnly)
                        objXMLDisplayFields.AppendChild(objXMLElem);
					else
                    {
                        //gagnihotri MITS 11441 03/12/2008 Added tabindex attribute to the search fields
                        objXMLElem.SetAttribute("tabindex", iTabIndex.ToString());
                        iTabIndex = iTabIndex + 1;
                        objXMLSearchFields.AppendChild(objXMLElem);
                    }
				SKIP_FIELD:
					bSet = false;
				}
			// 11/01/2007 REM Umesh    Customization of Sound-Alike search
				objXMLRoot.SetAttribute("soundex", "0");
				objXmlSetting = new XmlDocument();
                string strContent = RMSessionManager.GetCustomSettings(m_iClientId);
                if (!string.IsNullOrEmpty(strContent))
				{
					objXmlSetting.LoadXml(strContent);
					//objRdr.Dispose();
					objSoundex = objXmlSetting.SelectSingleNode("//Other/Soundex");
                    if (objSoundex != null)
					{
                        if (objSoundex.InnerText == "-1")
                            sSoundex = "1";
					}
				}

				objXMLRoot.SetAttribute("soundex", sSoundex);			

                //avipinsrivas start : Worked for JIRA - 7767
                //objXMLRoot.SetAttribute("viewfullentitysearch", p_sFullEntitySearch);//JIRA 4633
                //objXMLRoot.SetAttribute("fullentitysearch", p_sUseFullEntitySearch);//JIRA 4633
                ////skhare7 JIRA 340 start entity role end
                //avipinsrivas end
                if (objRdr != null)
                    objRdr.Close();


			}

			catch (XmlOperationException p_objException)
			{
				throw p_objException;
			}

            catch (Exception p_objException)
			{
                throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("SearchXML.GetSearchXML.GetXML", m_iClientId), sLangCode), p_objException);//JIRA-RMA-1535 
			}

			finally
			{
                if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
                if (objReaderSupp != null)
				{
					objReaderSupp.Close();
					objReaderSupp.Dispose();
				}
				if (objDataSet != null)
				{
					objDataSet.Clear();
					objDataSet.Dispose();
				}
                if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}				
                if (objRdr != null)
                    objRdr.Close();
				if (objCache != null) objCache.Dispose();
				objXmlViews = null;
				objXmlView = null;
				objXMLRoot = null;
				objXMLElem = null;
				objXMLSearchFields = null;
				objXMLDisplayFields = null;
				objXMLElemTable = null;
//				objDRSearchDict = null;
//				objDRSearchSupp = null;
				objXmlSetting = null;  // Umesh
				objSoundex = null;     // Umesh
				arrTableDictVal = null;
				arrTableSuppVal = null;
				arrTableDict = null;
				arrTableSupp = null;
				arrCurrentData = null;
				arrSetting = null;
				arrCompleteSetting = null;
				objDictBuild = null;
                sbSQL = null;
                strDictParams = null;
			}
		}
		#endregion

		#region Create search view
		/// <summary>
		/// This function creates the search view for a particular module.
		/// </summary>
		/// <param name="p_sDsn">Database connection string</param>
		/// <param name="p_lDsnId">DSN id</param>
		/// <param name="p_iViewID">View id corresponding to the search module</param>
		/// <param name="p_iUserId">User id</param>
		/// <param name="p_iGroupID">Group id</param>
		/// <param name="p_sTableRestrict">Number of tables to be displayed</param>
		/// <param name="p_sFormName">Form Name for which Search Screen to be displayed</param>
		/// <param name="p_sSettings">Page related settings</param>
		/// <param name="p_sTableID">Table Id corresponding to the search module</param>
		/// <returns>String containing the XML to be sent to Adapter.</returns>
        //pmittal5  MITS 11506  05/16/08
        /// <param name="p_sSettoDefault">Default view set parameter</param>
		//public XmlDocument CreateSearchView (int p_iViewID, int p_iUserId, int p_iGroupID, string p_sTableRestrict, string p_sFormName, string p_sSettings)
        //Tushar MITS#18229
        /// <param name="p_sFieldRestrict">Restricted field will not display</param>
        //End MITS#18229
        public XmlDocument CreateSearchView(int p_iViewID, int p_iUserId, int p_iGroupID, string p_sTableRestrict, string p_sFormName, string p_sSettings, string p_sSettoDefault, string p_sFieldRestrict, string sLangCode, string sPageId, string p_sScreenFlag, bool bHideGlobalSearch)//JIRA-RMA-1535 //JIRA 340 skhare7//JIRA 4633
		{
			XmlDocument objXMLDOM = null;
            string sUseFullEntitySearch = string.Empty;//JIRA 4633
            string sViewFullEntitySearch = string.Empty;//JIRA 4633
            XmlElement objElem = null;//JIRA 4633

			//SearchXML objSearchXML = null;
			try
			{
				objXMLDOM = new XmlDocument();
				//objSearchXML = new SearchXML(p_sDsn);
				//objSearchXML.GetSearchXML (p_iViewID, p_iUserId,p_iGroupID, p_sTableRestrict,p_sSysEx, p_sSettings, ref objXMLDOM);
                //pmittal5  MITS 11506  05/16/08  -Start  Implementing "Default Search View" functionality
                if (p_sSettoDefault == "true" || p_sSettoDefault == "1")
                {
                    p_iViewID = GetBestSearch(p_sFormName, p_iUserId, p_iGroupID);
                    if (p_iViewID == 0)
                        p_iViewID = 1;
                }
                //skhare7 JIRa 340 entity Role start////JIRA 4633

                SysSettings settings = new SysSettings(m_sConnectionString, m_iClientId);
                if (settings != null && settings.UseEntityRole && string.Compare(p_sScreenFlag, "1") != 0)
                {
                    //avipinsrivas start : Worked for JIRA - 7767
                    //if (string.Compare(p_sUseFullEntitySearch, "1") == 0)
                    //{
                    //    XmlDocument objPrefXml = new XmlDocument();
                    //    objPrefXml = GetUserPrefNode(p_iUserId);
                    //    if (objPrefXml != null)
                    //    {
                    //        objElem = (XmlElement)objPrefXml.SelectSingleNode("//UseFullEntitySearch");
                    //        if (objElem != null)
                    //        {

                    //            if (objElem.Attributes.Item(0) != null)
                    //            {
                    //                sUseFullEntitySearch = objElem.Attributes.Item(0).Value;
                    //                p_sUseFullEntitySearch = sUseFullEntitySearch;

                    //                objElem = null;
                    //            }
                    //        }
                    //    }
                    //}
                    //avipinsrivas end
                    // if (string.Compare(p_sUseFullEntitySearch, "1") == 0)
                    //  sUseFullEntitySearch = p_sUseFullEntitySearch;
                    //  string sValue = string.Empty;
                    //  if (p_sUseFullEntitySearch != string.Empty || string.Compare(p_sUseFullEntitySearch, "1") == 0)
                    // {
                    //avipinsrivas Start : Worked for Jira-340
                    bool blnIsFromPersonInvolved = false;
                    blnIsFromPersonInvolved = Enum.IsDefined(typeof(Globalization.PersonInvolvedGlossaryTableNames), p_sFormName.ToUpper());
                    if ((Enum.IsDefined(typeof(Globalization.PersonInvolvedGlossaryTableNames), p_sFormName.ToUpper()) || blnIsFromPersonInvolved) && !bHideGlobalSearch)
                    {
                        sViewFullEntitySearch = "-1";
                        //avipinsrivas start : Worked for JIRA - 7767
                        sUseFullEntitySearch = "-1";

                        p_sFormName = "entity";
                        p_sTableRestrict = "0";
                        if (blnIsFromPersonInvolved)
                            p_iViewID = GetBestSearch(p_sFormName, p_iUserId, p_iGroupID);
                        //avipinsrivas end
                    }
                    //avipinsrivas End
                }
                //skhare7 JIRa 340 entity Role end
                //pmittal5  MITS 11506  05/16/08  -End
                //Tushar:MITS#18229
                //Added p_sFieldRestrict
                GetSearchXML(ref p_iViewID, p_iUserId, p_iGroupID, p_sTableRestrict, p_sFormName, p_sSettings, ref objXMLDOM, p_sFieldRestrict, sLangCode, sPageId, sViewFullEntitySearch, sUseFullEntitySearch);//JIRA-RMA-1535 //skhare7 JIRA 340//JIRA 4633
                //End MITS:18229
                //Start: rsushilaggar vendor's fund approval 05/07/2010 MITS 20606 
                //SysSettings settings = new SysSettings(m_sConnectionString);
                //if (settings != null && !settings.UseEntityApproval)
                //{ 
                //    XmlNode node = objXMLDOM.SelectSingleNode("//search/searchfields/field[@name=\"ENTITY_APPROVAL_STATUS\"]");

                //    if (node != null)
                //        node.ParentNode.RemoveChild(node);
                //    node = objXMLDOM.SelectSingleNode("//search/displayfields/field[@name=\"ENTITY_APPROVAL_STATUS\"]");
                //    if (node != null)
                //        node.ParentNode.RemoveChild(node);
                //}
                //End: rsushilaggar vendor's fund approval
			}

			catch (XmlOperationException p_objException)
			{
				throw p_objException;
			}

			catch (RMAppException p_objException)
			{
				throw p_objException;
			}

			catch (Exception p_objException)
			{
                throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("SearchXML.CreateSearchView.CreateError", m_iClientId), sLangCode), p_objException);//JIRA-RMA-1535 
			}

//			finally
//			{
//				objXMLDOM = null;
//				//objSearchXML = null;
//			}
			
			return (objXMLDOM);
		}
		#endregion

		#region Get Search Results For Selected Entity
		/// <summary>
		/// This function retrieves the record for passed in Entity-Id.
		/// </summary>
		/// <param name="p_iEntityId">The Entity Id for which the results have to be returned</param>
		/// <param name="p_sType">The Type of entity</param>
		/// <param name="p_iLookUpType"></param>
        /// <param name="p_sFormName"> The Form Name</param>
		/// <returns>XML DOM containing data corresponding to passed in entity ID.</returns>
		public XmlDocument GetSearchResultsForSelectedEntity(int p_iEntityId, string p_sType, int p_iLookUpType, string p_sFormName)
		{
			XmlDocument objXMLDOM = null;
			XmlDocument objTempDom = null;
			XmlElement objXmlElem = null;
            XmlElement objXmlElecomb = null;
			XmlNode objNode = null;
			Riskmaster.DataModel.DataObject objEntity = null;
            string sData = string.Empty;
			string sLookUpType = string.Empty;
			string sTemp = string.Empty;
            //R8 Combined Payment
            int iCombinedPayeeId = 0;
            int iAccountId = 0;
            bool bIsCombinedPayee = false;
            LocalCache objCache = null;
			try
			{
				objXMLDOM = new XmlDocument();
				objTempDom = new XmlDocument();
				//TBD check for p_sType.

                if (p_iLookUpType == 9)
				{
					sLookUpType = "Policy";
					objTempDom.LoadXml("<Policy/>");
				}
                else if (p_iLookUpType == 10 || p_iLookUpType == 8)
				{
					sLookUpType = "Vehicle";
					objTempDom.LoadXml("<Vehicle/>");
				}
                //smahajan6 12/09/09 MITS:18230 :Start
                else if (p_iLookUpType == 25)
                {
                    sLookUpType = "PropertyUnit";
                    objTempDom.LoadXml("<PropertyUnit/>");
                }
                //smahajan6 12/09/09 MITS:18230 :End
                else if (p_iLookUpType == 11 || p_iLookUpType == 7)
				{
					sLookUpType = "Event";

					// Aditya - 09-Sep-2005 - Serialization Config Xml
					// made similar to the one in FDM view
					string sSerializationConfigXml = "<Event>" +
													 "	<EventXDatedTextList>" +
													 "		<EventXDatedText/>" +
													 "	</EventXDatedTextList>" +
													 " 	<ReporterEntity/>" +
													 "	<EventQM/>" +
													 "	<EventMedwatch/>" +
													 "	<EventXAction/>" +
													 "	<EventXOutcome/>" +
													 "	<Supplementals/>" +
													 "</Event>";
					objTempDom.LoadXml(sSerializationConfigXml);
					//objTempDom.LoadXml("<Event/>");
				}
                else if (p_iLookUpType == 6)
				{
					sLookUpType = "Claim";
					objTempDom.LoadXml("<Claim/>");
				}
                else if (p_iLookUpType == 4)
				{
					sLookUpType = "Employee";
					objTempDom.LoadXml("<Employee><EmployeeEntity/></Employee>");
				}
                else if (p_iLookUpType == 12)
				{
					sLookUpType = "Funds";
					objTempDom.LoadXml("<Funds/>");
				}
                else if (p_iLookUpType == 13)
                {
                    sLookUpType = "Funds";
                    string sSerializationConfigXml = "<Funds>" +
                                 "	<TransSplitList>" +
                                 "		<FundsTransSplit/>" +
                                 "	</TransSplitList>" +
                                 "</Funds>";
                    objTempDom.LoadXml(sSerializationConfigXml);
                }
                else if (p_iLookUpType == 23)
                {
                    sLookUpType = "LeavePlan";
                    objTempDom.LoadXml("<LeavePlan/>");
                }
                //Mukul(6/7/2007) MITS 9684
                else if (p_iLookUpType == 20)
                {
                    sLookUpType = "PolicyEnh";
                    objTempDom.LoadXml("<PolicyEnh/>");
                }
                //RMA-8753 nshah28 start
                else if (p_iLookUpType == 1102)
                {
                    sLookUpType = "Address";
                    objTempDom.LoadXml("<Address/>");
                } //RMA-8753 nshah28 end
				else
				{
					sLookUpType = "Entity";
					objTempDom.LoadXml("<Entity/>");
				}

                //Amitosh
                if ((p_sFormName.Equals("funds") || p_sFormName.Equals("autoclaimchecks")) && isCombinedPayee(p_iEntityId, ref iCombinedPayeeId))
                {
                    bIsCombinedPayee = true;
                    objEntity = (Riskmaster.DataModel.DataObject)m_objDmf.GetDataModelObject("CombinedPayment", false);
                    objEntity.MoveTo(iCombinedPayeeId);
                }
                else
                {
                    bIsCombinedPayee = false;
                    objEntity = (Riskmaster.DataModel.DataObject)m_objDmf.GetDataModelObject(sLookUpType, false);
				objEntity.MoveTo(p_iEntityId);
                }
                if (objTempDom != null)
					sData = objEntity.SerializeObject(objTempDom);
				else
					sData = objEntity.SerializeObject();

                if (sData.Length > 0)
					objXMLDOM.LoadXml(sData);
				else
                    throw new RMAppException(Globalization.GetString("SearchXML.GetSearchResultsForSelectedEntity.NoDataErr", m_iClientId));
                if (p_iLookUpType == 4)
				{
					//Create a node "LastFirstName" to be used in various search screens.
					objNode = objXMLDOM.DocumentElement.SelectSingleNode("//EmployeeEntity");
					objXmlElem = objXMLDOM.CreateElement("LastFirstName");
                    
                    if (objXMLDOM.SelectSingleNode("//LastName").InnerText != "")
                        sTemp = objXMLDOM.SelectSingleNode("//LastName").InnerText;
                    if (objXMLDOM.SelectSingleNode("//FirstName").InnerText != "")
						sTemp = sTemp + ", " + objXMLDOM.SelectSingleNode("//FirstName").InnerText;

					objXmlElem.InnerText = sTemp;

					objNode.AppendChild(objXmlElem);

                    //Changed for MITS 9307 : Start

                    if (objXMLDOM.SelectSingleNode("//DateOfDeath").InnerText != "")
                    {
                        sTemp = objXMLDOM.SelectSingleNode("//DateOfDeath").InnerText;
                        DateTime objDate = Conversion.ToDate(sTemp);
                        objXMLDOM.SelectSingleNode("//DateOfDeath").InnerText = objDate.ToShortDateString();
                    }

                    if (objXMLDOM.SelectSingleNode("//BirthDate").InnerText != "")
                    {
                        sTemp = objXMLDOM.SelectSingleNode("//BirthDate").InnerText;
                        DateTime objDate = Conversion.ToDate(sTemp);
                        objXMLDOM.SelectSingleNode("//BirthDate").InnerText = objDate.ToShortDateString();
                    }

                    if (objXMLDOM.SelectSingleNode("//TermDate").InnerText != "")
                    {
                        sTemp = objXMLDOM.SelectSingleNode("//TermDate").InnerText;
                        DateTime objDate = Conversion.ToDate(sTemp);
                        objXMLDOM.SelectSingleNode("//TermDate").InnerText = objDate.ToShortDateString();
                    }

                    if (objXMLDOM.SelectSingleNode("//DateHired").InnerText != "")
                    {
                        sTemp = objXMLDOM.SelectSingleNode("//DateHired").InnerText;
                        DateTime objDate = Conversion.ToDate(sTemp);
                        objXMLDOM.SelectSingleNode("//DateHired").InnerText = objDate.ToShortDateString();
                    }

                    if (objXMLDOM.SelectSingleNode("//WorkPermitDate").InnerText != "")
                    {
                        sTemp = objXMLDOM.SelectSingleNode("//WorkPermitDate").InnerText;
                        DateTime objDate = Conversion.ToDate(sTemp);
                        objXMLDOM.SelectSingleNode("//WorkPermitDate").InnerText = objDate.ToShortDateString();
                    }

                    //Changed for MITS 9307 : End
				}
                if (p_iLookUpType < 4)
				{
					//Create a node "LastFirstName" to be used in various search screens.
                    bool bUseFundsSubAccounts = false;
                    SysSettings objSysSetting = null;


                    objSysSetting = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud

                    bUseFundsSubAccounts = objSysSetting.UseFundsSubAcc;

                    if (bIsCombinedPayee)
                    {
                        objNode = objXMLDOM.DocumentElement.SelectSingleNode("//CombinedPayment");
                        objXmlElem = objXMLDOM.CreateElement("iscombinedPayee");
                        objXmlElem.InnerText = "true";
                        objNode.AppendChild(objXmlElem);

                        if (objXMLDOM.SelectSingleNode("//EntityId").InnerText != "" && objXMLDOM.SelectSingleNode("//EntityId") != null)
                        {
                            objXMLDOM.SelectSingleNode("//EntityId").InnerText = p_iEntityId.ToString();
                        }
                      
                        objXmlElem = objXMLDOM.CreateElement("bankaccntid");
                        if (!bUseFundsSubAccounts)
                        {
                            if (objXMLDOM.SelectSingleNode("//AccountId").InnerText != "")
                            {
                                objXmlElem.InnerText = objXMLDOM.SelectSingleNode("//AccountId").InnerText;
                            }
                        }
                        else
                        {
                            if (objXMLDOM.SelectSingleNode("//SubAccId").InnerText != "")
                            {
                                objXmlElem.InnerText = objXMLDOM.SelectSingleNode("//SubAccId").InnerText;
                            }

                        }
                        objNode.AppendChild(objXmlElem);
                        objXmlElecomb = (XmlElement)objXMLDOM.SelectSingleNode("//CombPmtCurrencyType");
                        //
                        if (objXmlElecomb.GetAttribute("codeid") != "" && objXMLDOM.SelectSingleNode("//CombPmtCurrencyType") != null)
                        {
                            objXmlElem = objXMLDOM.CreateElement("currencytypecode");
                            //MITS 28081 skhare7
                          //  objXmlElem.InnerText = objXmlElecomb.GetAttribute("codeid").ToString() +"_"+ objXMLDOM.SelectSingleNode("//CombPmtCurrencyType").InnerText;
                            objXmlElem.InnerText = objXmlElecomb.GetAttribute("codeid").ToString();//+ "_" + objXMLDOM.SelectSingleNode("//CombPmtCurrencyType").InnerText;
                            objNode.AppendChild(objXmlElem);
                        }
                    }
                    else
                    {
                        objNode = objXMLDOM.DocumentElement.SelectSingleNode("//Entity");
                        objXmlElem = objXMLDOM.CreateElement("iscombinedPayee");
                        objXmlElem.InnerText = "false";
                        objNode.AppendChild(objXmlElem);
                       
                        objXmlElem = objXMLDOM.CreateElement("bankaccntid");
                        objXmlElem.InnerText = "0";
                        objNode.AppendChild(objXmlElem);

                        objXmlElem = objXMLDOM.CreateElement("currencytypecode");
                        objXmlElem.InnerText = "0";
                        objNode.AppendChild(objXmlElem);
                    }


                    objXmlElem = objXMLDOM.CreateElement("hasEFtbankInfo");
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                    if (CommonFunctions.hasEFTBankInfo(p_iEntityId, objCache.GetCodeId("A", "EFT_BANKING_STATUS"), m_sConnectionString))
                    {
                        objXmlElem.InnerText = "true";

                    }
                    else
                    {
                        objXmlElem.InnerText = "false";

                    }    
                    objNode.AppendChild(objXmlElem);


					objXmlElem = objXMLDOM.CreateElement("LastFirstName");
                    if (p_sFormName == "policymcoenh")
                    {
                        if (objXMLDOM.SelectSingleNode("//Abbreviation").InnerText != "")
                        {
                            sTemp = objXMLDOM.SelectSingleNode("//Abbreviation").InnerText + " ";
                        }
                    }
                    if (objXMLDOM.SelectSingleNode("//LastName").InnerText != "")
						sTemp += objXMLDOM.SelectSingleNode("//LastName").InnerText;
                        //zalam 08/18/2008 Mits:-13015 Start
                        //if (objXMLDOM.SelectSingleNode("//FirstName").InnerText != "")
                        //    sTemp = sTemp + ", " + objXMLDOM.SelectSingleNode("//FirstName").InnerText;
                        if (objXMLDOM.SelectSingleNode("//FirstName").InnerText != "" && objXMLDOM.SelectSingleNode("//LastName").InnerText != "")
                            sTemp = sTemp + ", " + objXMLDOM.SelectSingleNode("//FirstName").InnerText;
                        else if (objXMLDOM.SelectSingleNode("//LastName").InnerText == "" && objXMLDOM.SelectSingleNode("//FirstName").InnerText != "")
                            sTemp = objXMLDOM.SelectSingleNode("//FirstName").InnerText;
                    //zalam 08/18/2008 Mits:-13015 End
                    ////skhare7 R8 Enhancement
                        // rrachev JIRA RMA-9042 Begin changes
                        // akaushik5 Added for RMA-12920 Starts
                        if (!object.ReferenceEquals(objXMLDOM.SelectSingleNode("//EntityTableId"), null))
                        {
                            // akaushik5 Added for RMA-12920 Ends
                            int iEntityTableId = int.Parse(objXMLDOM.SelectSingleNode("//EntityTableId").InnerText);
                            int iGlossaryType = objCache.GetGlossaryType(iEntityTableId);
                            //if (p_sFormName == "funds")
                            if (p_sFormName == "funds" && iGlossaryType != 7 && iGlossaryType != 4) // Glossary Types: 7 - People Tables, 4 - Entity Code Tables
                        {
                            sTemp = ""; ;
                        }
                        }
                        // rrachev JIRA RMA-9042 End changes

                    objXmlElem.InnerText = sTemp;

                    objNode.AppendChild(objXmlElem);

                    //RMA-8753 nshah28 start
                    string sIsPrimaryAddressExpired = string.Empty;
                    objXmlElem = objXMLDOM.CreateElement("PrimaryAddressExpired");
                    if (p_sFormName.Equals("funds") && objSysSetting.UseMultipleAddresses)
                    {
                        string sbSql = string.Format("SELECT ROW_ID FROM ENTITY_X_ADDRESSES WHERE ENTITY_ID={0} AND PRIMARY_ADD_FLAG=-1 AND (EXPIRATION_DATE<{1})", p_iEntityId, DateTime.Now.ToString("yyyyMMdd"));
                        using (DbReader objDr = DbFactory.GetDbReader(m_sConnectionString, sbSql))
                        {
                            if (objDr.Read())
                            {
                                sIsPrimaryAddressExpired = "-1";
                            }
                        }
                    }
                    objXmlElem.InnerText = sIsPrimaryAddressExpired;
                    objNode.AppendChild(objXmlElem);
                    //RMA-8753 nshah28 end
                    // Added by Amitosh for Mits 23186 (2/17/2011)
                    if (objXMLDOM.SelectSingleNode("//BirthDate") != null && objXMLDOM.SelectSingleNode("//BirthDate").InnerText != "")
                    {
                        sTemp = objXMLDOM.SelectSingleNode("//BirthDate").InnerText;
                        DateTime objDate = Conversion.ToDate(sTemp);
                        objXMLDOM.SelectSingleNode("//BirthDate").InnerText = objDate.ToShortDateString();

                        objNode = objXMLDOM.DocumentElement.SelectSingleNode("//Entity");
                        objXmlElem = objXMLDOM.CreateElement("Age");

                        int iDateOfBirth = Conversion.ConvertStrToInteger(objDate.Day.ToString());
                        int iMonthOfBirth = Conversion.ConvertStrToInteger(objDate.Month.ToString());
                        int iYearOfBirth = Conversion.ConvertStrToInteger(objDate.Year.ToString());
                        DateTime sysDateTime = DateTime.Now;
                        int iSysDate = Conversion.ConvertStrToInteger(sysDateTime.Day.ToString());
                        int iSysMonth = Conversion.ConvertStrToInteger(sysDateTime.Month.ToString());
                        int iSysYear = Conversion.ConvertStrToInteger(sysDateTime.Year.ToString());
                        int iYearsDiff = iSysYear - iYearOfBirth - 1;

                        if (iSysMonth > iMonthOfBirth || (iSysMonth == iMonthOfBirth && iSysDate > (iDateOfBirth - 1)))
                        {
                            iYearsDiff++;
                        }
                        objXmlElem.InnerText = iYearsDiff.ToString();
                        objNode.AppendChild(objXmlElem);
                    }
                    // end Amitosh
                }

			}
			catch (XmlOperationException p_objException)
			{
				throw p_objException;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SearchXML.GetSearchResultsForSelectedEntity.GenericError", m_iClientId), p_objException);
			}
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }
		
			return (objXMLDOM);
		}
		#endregion
        #region Add New Entity
        /// <summary>
        /// This function retrieves the record for passed in Entity-Id.
        /// </summary>
        /// <param name="p_sLastName">The Entity last name send from UI</param>
        /// <param name="p_sFirstName">The Entity First name send from UI</param>
        /// <param name="p_sFormName"> The Form Name</param>
        /// <returns>XML DOM </returns>
        ///  //skhare7 R8 26969
        ///  // Start : Changed By : ngupta73 MITS No : 29409
        /// AddNewEntity(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        public XmlDocument AddNewEntity(string p_sFormName, string p_sLastName, string p_sFirstName)
        {
            XmlDocument objXMLDOM = null;
            XmlDocument objTempDom = null;
           
            string sData = string.Empty;
          
            string sTemp = string.Empty;
            //R8 Combined Payment
           
            LocalCache objCache = null;
            Entity objEntity = null;
            try
            {
                objXMLDOM = new XmlDocument();
                objTempDom = new XmlDocument();
              
                  
                    objTempDom.LoadXml("<Entity/>");
                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                    objEntity = (Entity)m_objDmf.GetDataModelObject("Entity", false);
                   
               
                    objEntity.EntityTableId = objCache.GetTableId("OTHER_PEOPLE");
                    if (p_sLastName != null)
                        p_sLastName = p_sLastName.Replace("^@", "&"); // ijha : MITS 28664 :use of &
                    objEntity.LastName = p_sLastName;
                    // Start : MITS No : 29409 ngupta73
                    objEntity.FirstName = p_sFirstName;
                    // End : MITS No : 29409 ngupta73
                
                    objEntity.Save();
                    if (objTempDom != null)
                        sData = objEntity.SerializeObject(objTempDom);
                    else
                        sData = objEntity.SerializeObject();


                if (sData.Length > 0)
                    objXMLDOM.LoadXml(sData);
                else
                    throw new RMAppException(Globalization.GetString("SearchXML.AddNewEntity.SaveDataErr", m_iClientId));
             
                ////avipinsrivas Start : Worked for Jira-340
                //XmlElement EntityRoleID = objXMLDOM.CreateElement("EntityRoleID");
                //EntityRoleID.InnerText = GetEntityRoleIDs(objEntity, objCache.GetTableId("OTHER_PEOPLE"));
                //objXMLDOM.DocumentElement.AppendChild(EntityRoleID);
                ////avipinsrivas Start : Worked for Jira-340

            }
            catch (XmlOperationException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
           
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objEntity != null)
                    objEntity.Dispose();
            }

            return (objXMLDOM);
        }
        #endregion
		#endregion

        ////avipinsrivas Start : Worked for Jira-340
        //private string GetEntityRoleIDs(Entity objEntity, int iTableID)
        //{
        //    string sEntityRoleIDs = string.Empty;

        //    if (objEntity != null)
        //    {
        //        if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
        //        {
        //            if (iTableID > 0 && objEntity.EntityXRoleList.Count > 0)
        //            {
        //                foreach (EntityXRole objEXR in objEntity.EntityXRoleList)
        //                {
        //                    if (int.Equals(objEntity.EntityId, objEXR.EntityId) && int.Equals(iTableID, objEXR.EntityTableId))
        //                        sEntityRoleIDs = string.Concat(objEXR.EntityId, '~', objEXR.ERRowId);
        //                }
        //            }
        //        }
        //    }

        //    return sEntityRoleIDs;
        //}
        ////avipinsrivas End

		#region Private Function

		#region Get Best Search
		
		
		/// <summary>
		/// This function retrieves the best view Id.
		/// </summary>
		/// <param name="p_sSearchCat"></param>
		/// <param name="p_iGroupId"></param>
		/// <param name="p_iUserId"></param>
		/// <returns>The View Id</returns>
        private int GetBestSearch(string p_sSearchCat, int p_iUserId, int p_iGroupId, bool bNeedDefaultEntitySearch = true)     //avipinsrivas Start : Worked for Jira-340
		{
			int iReturnValue = 0;
			int iCatId = 0;
			StringBuilder sbSQL = null;

			DbConnection objConn = null;
			DbReader objReader = null;
			try
			{
                switch (p_sSearchCat.ToLower())
				{
                    case "claimgc":  //Claim search
                    case "claimwc":
                    case "claimva":
                    case "claimdi":
                    case "claim":
						iCatId = 1;
						break;
                    case "event":  //Event search
						iCatId = 2;
						break;
					case "piemployee":
                    case "employee":  //employee search
                    case "employee.lastname":  //PJS MITS # 16661 - for employeelastnamelookup control
                    // akaushik5 Addeded for MITS 38299/RMA-10912 Starts
                    case "employees":
                        // akaushik5 Addeded for MITS 38299/RMA-10912 Ends
						iCatId = 3;
						break;
                    case "entity":  //entity search
                    case "people":	 //People search
                    case "entitymaint":
						iCatId = 4;
						break;
                    case "vehicle":  //vehicle search
						iCatId = 5;
						break;
                    case "policy":  //policy search
						iCatId = 6;
						break;
                    case "payment":  //payment search
						iCatId = 7;
						break;
					case "pipatient":
                    case "patient":  //patient search
						iCatId = 8;
						break;
					case "piphysician":
                    case "physician":  //physician search
						iCatId = 9;
						break;
					case "pimedstaff":
                    case "medstaff":  //medstaff search
					case "staff":
						iCatId = 10;
						break;
					case "adm":
					case "at":
						iCatId = 20;
						break;
					case "dp":
                    case "plan": //Geeta 06/13/07 : MITS 9623
					case "displan": // Adding "displan" to execute "Return to Search" in case of Disability Plan
						iCatId = 11;
						break;
                    // Start Naresh Enhanced Policy Search
                    case "policyenh":
                    //Mridul. 12/07/09. MITS#:18229
                    case "policyenhal":
                    case "policyenhgl":
                    case "policyenhpc":
                    case "policyenhwc":
		            case "policybilling":
                        iCatId = 21;
                        break;
                    // End Naresh Enhanced Policy Search
                    //Shruti Leave Plan Search starts
                    case "leaveplan":
                        iCatId = 23;
                        break;
                    //Shruti Leave Plan Search ends
                    //Changed by Gagan for MITS 9646 : Start
                    case "admintracking":
                        iCatId = 20;
                        break;
                    //Changed by Gagan for MITS 9646 : End
                    //Anu Tennyson for MITS 18291 : STARTS 10/25/2009
                    case "propertyunit":
                        iCatId = 12;
                        break;
                    //Anu Tennyson for MITS 18291 : ENDS    
                        //skhare7
                    case "diary":
                        iCatId = 13;
                        break;
                    //averma62 - MITS - 28528
                    case "catastrophe":
                        iCatId = 24;
                        break;
                    //Amandeep Driver Search
                    case "driver":
                        iCatId = 25;
                        break;
                    //Ashish Ahuja : Address Master RMA-8753
                    case "address":
                        iCatId = 26;
                        break;
					default:  
						iCatId = 4; // Changing the default search from admin tracking to standard entity search
						break;
				}//end switch
                XmlDocument objPrefXml = new XmlDocument();
                XmlElement objElem = null;
                objPrefXml = GetUserPrefNode(p_iUserId);
                if (objPrefXml != null && bNeedDefaultEntitySearch)         //avipinsrivas Start : Worked for Jira-340
				{
                    objElem = (XmlElement)objPrefXml.SelectSingleNode("//SearchDefault");
                    if (objElem != null)
					{
                        if (objElem.GetAttribute("Cat_" + iCatId.ToString()) != "")
						{
                            iReturnValue = Conversion.ConvertObjToInt(objElem.GetAttribute("Cat_" + iCatId.ToString()), m_iClientId);

							// Check whether the view is present or not
							sbSQL = new StringBuilder();
							sbSQL.Append("SELECT * FROM SEARCH_VIEW WHERE VIEW_ID = ");
							sbSQL.Append(iReturnValue);

							using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
							{
								if (objRdr.Read())
									return iReturnValue;
							}
							// End of check for view present or not
						}
					}
				}
					sbSQL = new StringBuilder();
                    //jramkumar for MITS 33729
                    //if(p_iUserId != 0 && p_iGroupId != 0)
                    //{
                    //    sbSQL.Append("SELECT SEARCH_VIEW.VIEW_ID FROM SEARCH_VIEW WHERE SEARCH_VIEW.CAT_ID = ");
                    //    sbSQL.Append(iCatId);
                    //}
                    //else
                    //{
					sbSQL.Append("SELECT SEARCH_VIEW.VIEW_ID FROM SEARCH_VIEW,SEARCH_VIEW_PERM ");
					sbSQL.Append(" WHERE SEARCH_VIEW.CAT_ID = ").Append(iCatId);
					sbSQL.Append(" AND SEARCH_VIEW.VIEW_ID = SEARCH_VIEW_PERM.VIEW_ID ");
					sbSQL.Append(" AND (SEARCH_VIEW_PERM.USER_ID = ").Append(p_iUserId);
					sbSQL.Append(" OR SEARCH_VIEW_PERM.GROUP_ID = ").Append(p_iGroupId);
					sbSQL.Append(" OR (SEARCH_VIEW_PERM.USER_ID = 0 AND SEARCH_VIEW_PERM.GROUP_ID = 0))");
                    //}

					//if(iCatId == 20)
					//	sbSQL.Append("AND SEARCH_VIEW.VIEW_AT_TABLE = '").Append(sADMTable).Append("'");

					sbSQL.Append(" ORDER BY SEARCH_VIEW.VIEW_ID");

					objConn = DbFactory.GetDbConnection(m_sConnectionString);
					objConn.Open();
					objReader = objConn.ExecuteReader(sbSQL.ToString());

                    //akaur9 R8 Mobile Adjuster used the app setting to decide whether to display the searches made just for mobile on UI or not --Start
                   // the View IDs of the seraches of mobile are negative and start from -100 and continue to be less for further searches
                    bool bMobileSearchOnUI = false;
                bool.TryParse(RMConfigurationManager.GetAppSetting("DisplayMobileSearchOnUI", m_sConnectionString, m_iClientId).ToString(), out bMobileSearchOnUI);
                    if (bMobileSearchOnUI)
                    {
                        if (objReader.Read())
                            iReturnValue = Conversion.ConvertStrToInteger(objReader.GetValue(0).ToString());
                    }
                    else
                    {
                        while (objReader.Read())
                        {
                            if (Conversion.ConvertStrToInteger(objReader.GetValue(0).ToString()) >= 1)
                            {
                                iReturnValue = Conversion.ConvertStrToInteger(objReader.GetValue(0).ToString());
                                break;
                            }
                        }
                    }
                    //akaur9 R8 Mobile Adjuster used the app setting to decide whether to display the searches made just for mobile on UI or not --end
					objReader.Close();
					objConn.Close();
				
			}
			finally
			{
                if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
                if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
			}
			return iReturnValue;
		}
		#endregion

		#endregion


        //Amitosh
        private bool isCombinedPayee(int p_iEntityId, ref int iCombinedPayeeId)
    {
        bool bReturnValue = false;
        string sCurrentDate = string.Empty;  //35649

        sCurrentDate = DateTime.Now.ToString("yyyyMMdd");  //35649
        
     
     
        try
        {

            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT COMBINED_PAY_ROW_ID FROM COMBINED_PAYMENTS WHERE COMBINED_PAY_EID = " + p_iEntityId + " AND STOP_FLAG = 0 AND STARTING_ON_DATE <= '" + sCurrentDate + "' AND ((STOP_DATE IS NULL) OR (STOP_DATE >= '" + sCurrentDate + "'))"))  //35649
            {
                if (objReader.Read())
                {
                    iCombinedPayeeId = Conversion.ConvertStrToInteger(objReader.GetValue("COMBINED_PAY_ROW_ID").ToString());
                    

                    bReturnValue = true;
                }

            }

    }
        catch
    {
    }
        return bReturnValue;
    }



             //Amitosh for EFT

             private bool hasEFTBankInfo(int p_iEntityId)
             {
                 LocalCache objCache = null;
                 string sBankName = string.Empty;
                 bool bReturnValue = false;
                 try
                 {
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT BANK_NAME FROM ENTITY_X_BANKING WHERE ENTITY_ID =" + p_iEntityId + "AND BANK_STATUS_CODE = " + objCache.GetCodeId("A", "BANKING_STATUS")))
                     {
                         if (objReader.Read())
                         {
                        sBankName = objReader.GetValue("BANK_NAME").ToString();


                             bReturnValue = true;
                         }

                     }

                 }
                 catch
                 {
                 }
                 finally
                 {
                     if (objCache != null)
                     {
                         objCache.Dispose();
                     }
                 }
                 return bReturnValue;
             }
	}

}
