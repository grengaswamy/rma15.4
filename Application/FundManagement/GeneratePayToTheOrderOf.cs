﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Security;

namespace Riskmaster.Application.FundManagement
{
    public class GeneratePayToTheOrderOf
    {
        string m_sConnString = string.Empty;
        private int m_iClientId = 0;

        public GeneratePayToTheOrderOf(string p_sConnectString, int p_iClientId)
		{
			m_sConnString = p_sConnectString;
            m_iClientId = p_iClientId;
		}

        public XmlDocument GetCodeType(XmlDocument p_objXmlDocument, UserLogin objUser)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            StringBuilder sbSQL = null;
            string sSystemTableName = string.Empty;  
            //Aman ML Change
            int iUserLangCode = 0;
            int iBaseLangCode = 0;
            bool isBaseLangCode = false;           
            try
            {
                iUserLangCode = objUser.objUser.NlsCode;
                iBaseLangCode = Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
               
                if (int.Equals(iUserLangCode, 0) || int.Equals(iBaseLangCode, iUserLangCode))
                    isBaseLangCode = true;

                sbSQL = new StringBuilder();
                sSystemTableName = p_objXmlDocument.SelectSingleNode("SystemTableName").InnerText;

                sbSQL.Append(" SELECT G.SYSTEM_TABLE_NAME, CT.CODE_ID, CT.CODE_DESC, C.SHORT_CODE,  ");
                if (!isBaseLangCode) 
                    sbSQL.Append(" UCT.CODE_DESC USER_CODE_DESC  ");
                else
                    sbSQL.Append(" '' USER_CODE_DESC  ");
                sbSQL.Append(" FROM CODES C ");
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT CT On C.CODE_ID = CT.CODE_ID And CT.LANGUAGE_CODE = " + iBaseLangCode + " ");
                if (!isBaseLangCode)
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCT On C.CODE_ID = UCT.CODE_ID And UCT.LANGUAGE_CODE = " + iUserLangCode + " ");
                sbSQL.Append(" LEFT OUTER JOIN GLOSSARY G ON G.TABLE_ID = C.TABLE_ID ");
                sbSQL.Append(" WHERE UPPER(G.SYSTEM_TABLE_NAME) In (" + sSystemTableName + ") ");
                sbSQL.Append(" And C.DELETED_FLAG <> -1 ");
                sbSQL.Append(" ORDER BY G.SYSTEM_TABLE_NAME, CT.CODE_ID, CT.CODE_DESC ");    //Added by kkaur8 for MITS 32413
                //Aman ML Change
                objReader = DbFactory.GetDbReader(m_sConnString, sbSQL.ToString());
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("GeneratePayToTheOrderOf");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("CodeTypes");
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("option");
                    objElemTemp.SetAttribute("table_name", Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_TABLE_NAME")));
                    if (string.Equals(Conversion.ConvertObjToStr(objReader.GetValue("SYSTEM_TABLE_NAME")).ToUpper(), "PAYEE_PHRASE_LOCATION", StringComparison.OrdinalIgnoreCase))
                    {
                        if (string.Equals(Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")).ToUpper(), "AFTER", StringComparison.OrdinalIgnoreCase))
                            objElemTemp.SetAttribute("value", "0");
                        else if (string.Equals(Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")).ToUpper(), "BEFORE", StringComparison.OrdinalIgnoreCase))
                            objElemTemp.SetAttribute("value", "-1");
                        else
                            objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")));
                    }
                    else
                        objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")));

                    if (string.IsNullOrEmpty(Convert.ToString((objReader.GetValue("USER_CODE_DESC")))))
                        objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                    else
                        objElemTemp.SetAttribute("option_text", Conversion.ConvertObjToStr(objReader.GetValue("USER_CODE_DESC")));
                    objElemChild.AppendChild(objElemTemp);

                }

                objDOM.FirstChild.AppendChild(objElemChild);
                objReader.Close();

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {

                throw new RMAppException(Globalization.GetString("GeneratePayToTheOrderOf.GetCodeType.Error", m_iClientId), p_objEx);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }
        }
    }
}
