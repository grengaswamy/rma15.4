﻿

using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;

using C1.C1PrintDocument;

using System.Globalization;
using System.Threading;
using Riskmaster.Security;

namespace Riskmaster.Application.FundManagement
{
	/**************************************************************
	 * $File		: PaymentHistory.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 08/01/2005
	 * $Author		: Mihika Agrawal
	 * $Comment		: PaymentHistory class has functions to create Payment History Report.
	 * $Source		:  	
	**************************************************************/
	/// <summary>
	/// PaymentHistory class.
	/// </summary>
	public class PaymentHistory
	{
		#region private variables Declaration

        public bool  bCarrierClaim=false;
		private string m_sConnectionString = "";

        //nadim for 14476
        private string m_sUserName="";
        private string m_sPassword= "";
        private string m_sDsnName = "";       
        //nadim for 14476

		/// <summary>
		/// C1PrintDocumnet object instance.
		/// </summary>
		private C1PrintDocument m_objPrintDoc = null;

		/// <summary>
		/// CurrentX
		/// </summary>
		private double m_dblCurrentX = 0;

		/// <summary>
		/// CurrentY
		/// </summary>
		private double m_dblCurrentY = 0;

		/// <summary>
		/// Font object.
		/// </summary>
		private Font m_objFont = null;

		/// <summary>
		/// Text object.
		/// </summary>
		private RenderText m_objText = null;

		/// <summary>
		/// PageWidth
		/// </summary>
		private double m_dblPageWidth = 0.0;

		/// <summary>
		/// PageHeight
		/// </summary>
		private double m_dblPageHeight = 0.0;

		/// <summary>
		/// ClientWidth
		/// </summary>
		private double m_dblClientWidth = 0.0;

        //Ash - cloud, config settings moved to DB
        /// <summary>
        /// ClientID for multi-tenant environment, cloud
        /// </summary>
        private int m_iClientId = 0;
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

		#endregion

		#region Constants Declaration
		/// <summary>
		/// Left alignment
		/// </summary>
		private const string LEFT_ALIGN = "1" ;
		/// <summary>
		/// Right alignment
		/// </summary>
		private const string RIGHT_ALIGN = "2" ;
		/// <summary>
		/// Center alignment
		/// </summary>
		private const string CENTER_ALIGN = "3" ;
		/// <summary>
		/// Number of rows in a report page
		/// </summary>
		private const int NO_OF_ROWS=54;
		#endregion
        //nadim for 14476..Added three parameters in PaymentHistory class constructor-Start
		#region PaymntHistory class Constructor 
		/// <summary>
		/// PaymentHistory class constructor 
		/// </summary>
		/// <param name="p_sConnectionString">Connection string to database</param>
		public PaymentHistory(string p_sConnectionString,string p_sDsnName,string p_sUserName,string p_sPassword, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionString;
            m_sDsnName = p_sDsnName;
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;

			m_objPrintDoc = new C1PrintDocument();
			m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
			// Can't have this dependent on a printer    m_objPrintDoc.PageSettings.PrinterSettings.PrinterName = RMConfigurator.Value("DefaultPrinterName").ToString();
            // Ash - cloud, config settings moved to Db
            m_iClientId = p_iClientId;
            m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId);
		}
        //nadim for 14476-End
		#endregion

		#region Property Declarations
		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		private double PageHeight 
		{
			get
			{
				return( m_dblPageHeight ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double PageWidth 
		{
			get
			{
				return( m_dblPageWidth ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double ClientWidth 
		{
			get
			{
				return( m_dblClientWidth ) ;
			}	
			set
			{
				m_dblClientWidth=value;
			}
		}
		
		/// <summary>
		/// Read/Write property for CurrentX.
		/// </summary>
		private double CurrentX 
		{
			get
			{
				return( m_dblCurrentX ) ;
			}
			set
			{
				m_dblCurrentX = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CurrentY.
		/// </summary>
		private double CurrentY 
		{
			get
			{
				return( m_dblCurrentY ) ;
			}
			set
			{
				m_dblCurrentY = value ;
			}
		}		
		/// <summary>
		/// Gets unique filename
		/// </summary>
		private string TempFile
		{
			get
			{
                return FundManager.GetPaymentFilePath();
			}
		}
		#endregion
		//RMA-6086
        public enum CurrencyType : int
        {
            BASE_CURRENCY = 0,
            CLAIM_CURRENCY = 1,
            PAYMENT_CURRENCY = 2,
        }
		//RMA-6086
		#region Public Functions
		/// Name		: PaymentHistoryReport
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Creates report based on passed parameters
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iClaimantEid">Claimaint EId</param>
		/// <param name="p_iUnitId">Unit Id</param>
		/// <param name="p_sSortCol">Sort Column</param>
		/// <param name="p_bAscending">Ascending Flag</param>
		/// <param name="p_sSubTitle">SubTitle for the report</param>
        /// <param name="p_sFilterExp">Filterexp for Radgrid Column</param>
		/// <param name="p_objMemory">Memory object containing report</param>
		/// <returns>Success: 1 or Failure: 0</returns>
        //public int PaymentHistoryReport(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, bool p_bAscending, string p_sSubTitle, string p_sFilterExp, out MemoryStream p_objMemory,int p_iEntityId)
        public int PaymentHistoryReport(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, bool p_bAscending, string p_sSubTitle, string p_sFilterExp, out MemoryStream p_objMemory, int p_iEntityId, UserLogin p_objUserLogin, int p_CurrCode)
		{
			DataSet objDS = null;
			StringReader objReader = null;
            //nadim for 14476
            DataModelFactory objDataModelFacFortitle=null;
            Claim objClaimForTitle = null;
            Event objEventForTitle = null;
            LocalCache objLocalCacheForTitle = null;
            LobSettings objLobSettings = null;
            string sClaimTitle = string.Empty;
            string sClaimant = string.Empty;
            string sClaimnumberTitle = string.Empty;
            string sDepartment = string.Empty;
            string sTempForDept = string.Empty;
            string sCaptionLevel = string.Empty;           
            int iCaptionlevel = 0;
            string sEventNum = string.Empty;   
          
            //nadim for 14476
			double dblCharWidth = 0.0;
			double dblLineHeight = 0.0;
			double dblLeftMargin = 10.0;
			double dblRightMargin = 10.0;
			double dblTopMargin = 10.0;
			double dblPageWidth = 0.0;
			double dblPageHeight = 0.0;
			double dblBottomHeader = 0.0;
			double dblCurrentY = 0.0;
			double dblCurrentX = 0.0;	
			double iCounter = 0.0;
			double[] arrColWidth = null;
			int iReturnValue = 0;

			string sHeader = string.Empty;
			string sFile = string.Empty;
			string sSQL = string.Empty;
			string[] arrColAlign = null;
			string[] arrCol = null;
            string s_ModifyFilterExp = string.Empty; // MITS Manish filter expression Payment History

			p_objMemory = null;

			string sPrevTransId = "";
			bool bLeadingLine = false;
			bool bCalTotal = false;
			Hashtable htTranId = new Hashtable();

			double dblTotalAll = 0.0;
			double dblTotalPay = 0.0;
			double dblTotalCollect = 0.0;
			double dblTotalVoid = 0.0;
            double dblCurrentPay = 0.0;
            double dblCurrentCollect = 0.0;
            double dblCurrentVoid = 0.0;
            SysSettings objSysSettings = null;//RMA-6086
			try
			{
				arrColAlign = new string[22] ;
				arrColWidth = new double[22] ;	
				arrCol = new string[22];
				//RMA-6086
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                int p_iCurrType = 0;
                CurrencyType eSelectedCurrType = CurrencyType.BASE_CURRENCY;
                if (objSysSettings.UseMultiCurrency != 0)
                {
                    int iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                    if (p_CurrCode > 0)
                    {
                        p_iCurrType = p_CurrCode;
                        eSelectedCurrType = CurrencyType.CLAIM_CURRENCY;
                        if (iBaseCurrCode == p_iCurrType)
                        {
                            eSelectedCurrType = CurrencyType.BASE_CURRENCY;
                        }
                        else if (p_CurrCode == int.MaxValue)
                        {
                            p_iCurrType = int.MaxValue;
                            eSelectedCurrType = CurrencyType.PAYMENT_CURRENCY;
                        }
                        //JIRA RMA-16874 (RMA-6086) ajohari2 START
                        else if (p_CurrCode.Equals(int.MinValue))
                        {
                            p_iCurrType = int.MinValue;
                            eSelectedCurrType = CurrencyType.CLAIM_CURRENCY;
                        }
                        //JIRA RMA-16874 (RMA-6086) ajohari2 START
                    }
                    else
                    {
                        p_iCurrType = iBaseCurrCode;
                        eSelectedCurrType = CurrencyType.BASE_CURRENCY;
                        //JIRA RMA-16874 (RMA-6086) ajohari2 START
                        if (p_CurrCode.Equals(int.MinValue) || p_CurrCode.Equals(0))
                        {
                            p_iCurrType = int.MinValue;
                            p_CurrCode = int.MinValue;
                            eSelectedCurrType = CurrencyType.CLAIM_CURRENCY;
                        }
                        //JIRA RMA-16874 (RMA-6086) ajohari2 START
                    }
                }
				//RMA-6086
                // MITS Manish Added filter string for Payment Hisotry
                s_ModifyFilterExp = ModifyFilterexp(p_sFilterExp);
                //Added:Yukti RMA -360, Dt:07/02/2014
                //sSQL = this.CreatePaymentSQL(p_iClaimId, p_iClaimantEid, p_iUnitId, p_sSortCol, s_ModifyFilterExp, p_bAscending,p_iEntityId);
                sSQL = this.CreatePaymentSQL(p_iClaimId, p_iClaimantEid, p_iUnitId, p_sSortCol, s_ModifyFilterExp, p_bAscending, p_iEntityId, p_objUserLogin, objSysSettings.UseMultiCurrency, eSelectedCurrType);
                //Ended:Yukti
				
				objDS = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);

				StartDoc(true, 10, 10, 10, 10);				
				SetFontBold(true);
				ClientWidth = 15400;

				dblCharWidth = GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ")/40;
				dblLineHeight = GetTextHeight("W");
				dblLeftMargin = dblCharWidth * 2;
				dblRightMargin = dblCharWidth * 2;
			
				dblTopMargin = dblLineHeight * 5;
				dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin;
				dblPageHeight = PageHeight;
				dblBottomHeader = dblPageHeight - (5 * dblLineHeight);
			

				arrColWidth[0] = dblCharWidth * 18;// Control #
				arrColWidth[1] = dblCharWidth * 9;// Check #
				arrColWidth[2] = dblCharWidth * 9;// Date
				arrColWidth[3] = dblCharWidth * 9;// Type
                arrColWidth[4] = dblCharWidth * 8;// Clr.       //Ankit Start : Worked on MITS - 33629
				arrColWidth[5] = dblCharWidth * 8;// Void       //Ankit Start : Worked on MITS - 33629
				arrColWidth[6] = dblCharWidth * 6;// Status
                arrColWidth[7] = dblCharWidth * 18;// Payee       //Ankit Start : Worked on MITS - 33629
				arrColWidth[8] = dblCharWidth * 14;// Check Amount
				arrColWidth[9] = dblCharWidth * 18;// From/To Date
				arrColWidth[10] = dblCharWidth * 13;// Invoice #
                arrColWidth[11] = dblCharWidth * 18;// Transaction type       //Ankit Start : Worked on MITS - 33629
				arrColWidth[12] = dblCharWidth * 14;// Split Amount
				arrColWidth[13] = dblCharWidth * 6;// User
				arrColWidth[14] = dblCharWidth * 9;// Check date
                arrColWidth[20] = dblCharWidth * 14;// Invoice Amount

                    arrColWidth[15] = dblCharWidth * 10;//combinedpayflag
                if (bCarrierClaim)
                {
                    arrColWidth[16] = dblCharWidth * 6;// policy
                    arrColWidth[17] = dblCharWidth * 10;// coverage
                    arrColWidth[18] = dblCharWidth * 10;//unit
                    arrColWidth[19] = dblCharWidth * 10;//unit
                }
                //skhare7 r8
                //Extra came during auto merge removed now
                //arrColWidth[15] = dblCharWidth * 10;//combinedpayflag
                //if (bCarrierClaim)
                //{
                //    arrColWidth[16] = dblCharWidth * 6;// policy
                //    arrColWidth[17] = dblCharWidth * 10;// coverage
                //    arrColWidth[18] = dblCharWidth * 10;//unit
                //    arrColWidth[19] = dblCharWidth * 10;//unit
                //}

				dblCurrentX = dblLeftMargin;
				dblCurrentY = 900;
                //nadim for 14476..set pdf title with claim number,department and claimant name. because p_sSubTitle does not contains correct value    
                //skh      
                    objDataModelFacFortitle = new DataModelFactory(m_sDsnName, m_sUserName,m_sPassword, m_iClientId);
                //skhare7 R8
                    if (p_iClaimId != 0)
                    {
                    objClaimForTitle = (DataModel.Claim)objDataModelFacFortitle.GetDataModelObject("Claim", false);
                    objClaimForTitle.MoveTo(p_iClaimId);                   
                  
                //get department for caption as setting in LOB-start
                    objLobSettings = objDataModelFacFortitle.Context.InternalSettings.ColLobSettings[objClaimForTitle.LineOfBusCode];                    
                        iCaptionlevel = objLobSettings.CaptionLevel;               
                       
                      sDepartment = objClaimForTitle.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaimForTitle.Parent as Event).DeptEid, iCaptionlevel, 0, ref iCaptionlevel);                   
                      sDepartment = sDepartment.Substring(0, Math.Min(sDepartment.Length, 25));

                      //get department for caption as setting in LOB-End             
                      objLocalCacheForTitle = new LocalCache(m_sConnectionString, m_iClientId);                                   
                   sClaimTitle = objLocalCacheForTitle.GetShortCode(objClaimForTitle.LineOfBusCode);           
                
               
                    if (objClaimForTitle.PrimaryClaimant != null)
                    {
                        sClaimant = objClaimForTitle.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                    }
                    else
                    {
                        sClaimant =  objClaimForTitle.PrimaryPiEmployee.PiEntity.LastName.Trim() + ',' + objClaimForTitle.PrimaryPiEmployee.PiEntity.FirstName.Trim();
                    }
                    if (sClaimant.EndsWith(","))
                    {
                        sClaimant = sClaimant.Remove(sClaimant.LastIndexOf(','));
                    }
                   
                    sClaimnumberTitle = objClaimForTitle.ClaimNumber;
                    p_sSubTitle = sClaimTitle + " [" + sClaimnumberTitle + "*" + sDepartment + "*" + sClaimant + "]";          
                    }   
                //nadim for 14476
				CreateNewPage(m_sConnectionString, true, p_sSubTitle);
			
				arrColAlign[0] = LEFT_ALIGN ; //Control #
				arrColAlign[1] = LEFT_ALIGN ; // Check #			
				arrColAlign[2] = LEFT_ALIGN ; // Date			
				arrColAlign[3] = LEFT_ALIGN ; // Type
				arrColAlign[4] = LEFT_ALIGN ; // Clr.			
				arrColAlign[5] = LEFT_ALIGN ; // Void			
				arrColAlign[6] = LEFT_ALIGN ; // Status			
				arrColAlign[7] = LEFT_ALIGN ; // Payee			
                //bganta - MITS : 22684 - Start 1
				//arrColAlign[8] = RIGHT_ALIGN; // Check Amount
                arrColAlign[8] = LEFT_ALIGN; // Check Amount
                //bganta - MITS : 22684 - End 1
				arrColAlign[9] = LEFT_ALIGN ; // From/To Date
				arrColAlign[10] = LEFT_ALIGN ; // Invoice #
				arrColAlign[11] = LEFT_ALIGN ; // Transaction Type
                //bganta - MITS : 22684 - Start 2
				//arrColAlign[12] = RIGHT_ALIGN ; // Split Amount
                arrColAlign[12] = LEFT_ALIGN; // Split Amount
                //bganta - MITS : 22684 - End 2
				arrColAlign[13] = LEFT_ALIGN ; // User
				arrColAlign[14] = LEFT_ALIGN ; // Check Date
                arrColAlign[15] =LEFT_ALIGN;//combinedpayflag
                arrColAlign[20] = LEFT_ALIGN; // Invoice Amount
                arrColAlign[21] = LEFT_ALIGN; // Reserve Type
                if (bCarrierClaim)
                {
                    arrColAlign[16] = LEFT_ALIGN;// policy
                    arrColAlign[17] = LEFT_ALIGN;// coverage
                    arrColAlign[18] = LEFT_ALIGN;//unit
                    arrColAlign[19] = LEFT_ALIGN;//unit
                }
				CurrentX = dblLeftMargin;
				CurrentY = dblCurrentY;

				#region Intialize array for heading
			//	rsharma220 MITS 31131 Start
                arrCol[0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.ControlNumber", m_iClientId));
                arrCol[1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.CheckNumber", m_iClientId));
                arrCol[2] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.TransDate", m_iClientId));
                arrCol[3] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.Type", m_iClientId));
                arrCol[4] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.Clear", m_iClientId));
                arrCol[5] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.Void", m_iClientId));
                arrCol[6] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.Status", m_iClientId));
                arrCol[7] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.Payee", m_iClientId));
                arrCol[8] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.CheckAmount", m_iClientId));
                arrCol[20] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.InvoiceAmount", m_iClientId));
                arrCol[9] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.FromToDate", m_iClientId));
                arrCol[10] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.InvoiceNumber", m_iClientId));
                arrCol[11] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.TransactionType", m_iClientId));
                arrCol[12] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.SplitAmount", m_iClientId));
                arrCol[13] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.User", m_iClientId));
                arrCol[14] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.CheckDate", m_iClientId));
                arrCol[15] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.CombinedPay", m_iClientId));
                arrCol[21] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.ReserveType", m_iClientId));
                //Added by Amitosh for R8 enhancement.Adding new columns to payment history grid
                if (bCarrierClaim)
                {
                    arrCol[16] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.PolicyName", m_iClientId));
                    arrCol[17] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.CoverageType", m_iClientId));
                    arrCol[18] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.Unit", m_iClientId));
                    arrCol[19] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Heading.LossType", m_iClientId));
             //rsharma220 MITS 31131 End
			        PrintHeader(ref dblCurrentY, 21, dblPageWidth, dblLeftMargin, dblLineHeight, arrCol, arrColWidth, arrColAlign);
                }
                else
                {
                    PrintHeader(ref dblCurrentY, 17, dblPageWidth, dblLeftMargin, dblLineHeight, arrCol, arrColWidth, arrColAlign);
                }
                //End Amitosh
				#endregion
				//Print header
				

				SetFontBold(false);					
				dblCurrentY = 1200;
				iCounter = 0;

                //Manish for multi currency MITS 27209
                string culture = CommonFunctions.GetCulture(p_iClaimId, CommonFunctions.NavFormType.None, m_sConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
			
				for (int i = 0; i < objDS.Tables[0].Rows.Count; i++)
				{
					if (i == 0)
					{
						bLeadingLine = true;
						htTranId.Add(objDS.Tables[0].Rows[i]["TRANS_ID"], objDS.Tables[0].Rows[i]["TRANS_ID"]);
						bCalTotal = true;
					}
					else
					{
						if (htTranId.Contains(objDS.Tables[0].Rows[i]["TRANS_ID"]))
							bCalTotal = false;
						else
						{
							bCalTotal = true;
							htTranId.Add(objDS.Tables[0].Rows[i]["TRANS_ID"], objDS.Tables[0].Rows[i]["TRANS_ID"]);
						}
                        
                        //bganta - 24/6/2013 - MITS : 18840 - Start
                        /*
						if (objDS.Tables[0].Rows[i]["TRANS_ID"].ToString().Equals(sPrevTransId))
							bLeadingLine = false;
						else
							bLeadingLine = true;
                        */
                        //bganta - 24/6/2013 - MITS : 18840 - End
					}
                    //skhare7 r8 added carrierclaimflag
					CreateRow(objDS.Tables[0].Rows[i], dblLeftMargin, ref dblCurrentY, arrColAlign, 
						dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth, bLeadingLine, bCalTotal,
                        ref dblTotalAll, ref dblCurrentPay, ref dblCurrentCollect, ref dblCurrentVoid, bCarrierClaim, ref p_iCurrType, eSelectedCurrType);
					//RMA-6086
                    if (eSelectedCurrType == CurrencyType.PAYMENT_CURRENCY)
                    {
                        int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                        if (iClaimCurrCode != p_iCurrType)
                        {
                            //dblTotalAll = dblTotalAll * CommonFunctions.ExchangeRateSrc2Dest(p_iCurrType, objSysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            //dblTotalAll = dblTotalAll * CommonFunctions.ExchangeRateSrc2Dest( objSysSettings.BaseCurrencyType, iClaimCurrCode, m_sConnectionString, m_iClientId);
                            dblCurrentPay = dblCurrentPay * CommonFunctions.ExchangeRateSrc2Dest(p_iCurrType, objSysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dblCurrentPay = dblCurrentPay * CommonFunctions.ExchangeRateSrc2Dest(objSysSettings.BaseCurrencyType, iClaimCurrCode, m_sConnectionString, m_iClientId);
                            dblCurrentCollect = dblCurrentCollect * CommonFunctions.ExchangeRateSrc2Dest(p_iCurrType, objSysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dblCurrentCollect = dblCurrentCollect * CommonFunctions.ExchangeRateSrc2Dest(objSysSettings.BaseCurrencyType, iClaimCurrCode, m_sConnectionString, m_iClientId);
                            dblCurrentVoid = dblCurrentVoid * CommonFunctions.ExchangeRateSrc2Dest(p_iCurrType, objSysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dblCurrentVoid = dblCurrentVoid * CommonFunctions.ExchangeRateSrc2Dest(objSysSettings.BaseCurrencyType, iClaimCurrCode, m_sConnectionString, m_iClientId);
                            p_iCurrType = int.MaxValue;
                        }
                    }
                    //JIRA RMA-16874 (RMA-6086) ajohari2 START
                    else if (eSelectedCurrType == CurrencyType.CLAIM_CURRENCY && p_CurrCode.Equals(int.MinValue))
                    {
                        int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                        if (iClaimCurrCode != p_iCurrType)
                        {
                            dblCurrentPay = dblCurrentPay * CommonFunctions.ExchangeRateSrc2Dest(p_iCurrType, objSysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dblCurrentPay = dblCurrentPay * CommonFunctions.ExchangeRateSrc2Dest(objSysSettings.BaseCurrencyType, iClaimCurrCode, m_sConnectionString, m_iClientId);
                            dblCurrentCollect = dblCurrentCollect * CommonFunctions.ExchangeRateSrc2Dest(p_iCurrType, objSysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dblCurrentCollect = dblCurrentCollect * CommonFunctions.ExchangeRateSrc2Dest(objSysSettings.BaseCurrencyType, iClaimCurrCode, m_sConnectionString, m_iClientId);
                            dblCurrentVoid = dblCurrentVoid * CommonFunctions.ExchangeRateSrc2Dest(p_iCurrType, objSysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dblCurrentVoid = dblCurrentVoid * CommonFunctions.ExchangeRateSrc2Dest(objSysSettings.BaseCurrencyType, iClaimCurrCode, m_sConnectionString, m_iClientId);
                            p_iCurrType = int.MinValue;
                        }
                    }
                    //JIRA RMA-16874 (RMA-6086) ajohari2 END
					//RMA-6086
                    dblTotalPay = dblTotalPay + dblCurrentPay;
                    dblTotalCollect = dblTotalCollect + dblCurrentCollect;
                    dblTotalVoid = dblTotalVoid + dblCurrentVoid;
                    dblCurrentPay = 0;
                    dblCurrentCollect = 0;
                    dblCurrentVoid = 0;
					iCounter ++;
					sPrevTransId = objDS.Tables[0].Rows[i]["TRANS_ID"].ToString();
					if (iCounter == NO_OF_ROWS)
					{
						CreateNewPage(m_sConnectionString, false, p_sSubTitle);
						iCounter = 0;
						dblCurrentY = 900;
					}
				}

                //pmittal5 MITS:9777 04/07/08
                dblTotalAll = dblTotalPay + dblTotalCollect;

				// Printing the Summary
				if ((iCounter + 3) > NO_OF_ROWS)
				{
					CreateNewPage(m_sConnectionString, false, p_sSubTitle);
					dblCurrentY = 900;
					iCounter = 0;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight;
				}
                //RMA-6086//JIRA RMA-16874 (RMA-6086) ajohari2
                if (p_CurrCode == int.MaxValue || p_CurrCode == int.MinValue)
                {
                    p_iCurrType = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                }

                //RMA-6086//JIRA RMA-16874 (RMA-6086) ajohari2 END
				SetFontBold(true);
				CurrentX = 78 * dblCharWidth;
				CurrentY = dblCurrentY;
                PrintTextNoCr(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Summary.TotalPay", m_iClientId)) + FetchDataAsPerCurrency(p_iCurrType, dblTotalPay, eSelectedCurrType));//RMA-6086
                CurrentX = 106 * dblCharWidth;
                //Total collection amount needs to be displayed as positive MITS 8823
                dblTotalCollect = -dblTotalCollect;
                PrintTextNoCr(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Summary.TotalCollect", m_iClientId)) + FetchDataAsPerCurrency(p_iCurrType, dblTotalCollect, eSelectedCurrType));//RMA-6086
                CurrentX = 134 * dblCharWidth;
                PrintTextNoCr(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Summary.TotalAll", m_iClientId)) + FetchDataAsPerCurrency(p_iCurrType, dblTotalAll, eSelectedCurrType));//RMA-6086
				CurrentX = 162 * dblCharWidth;
                // akaushik5 Fixed for MITS 36765 Starts
                PrintTextNoCr(CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.Summary.TotalVoid", m_iClientId)) + FetchDataAsPerCurrency(p_iCurrType, dblTotalVoid, eSelectedCurrType));//RMA-6086
                // akaushik5 Fixed for MITS 36765 Ends
                //rsharma220 MITS 31131 End
				// Printing the Footer
                
				if ((iCounter + 7) > NO_OF_ROWS)
				{
					CreateNewPage(m_sConnectionString, false, p_sSubTitle);
					dblCurrentY = 900;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight * 2;
				}

				CreateFooter(dblLeftMargin, dblCurrentY, dblLineHeight, 250);
				EndDoc();
				sFile = TempFile;
				Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);

				File.Delete(sFile);
				iReturnValue = 1;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.CreateReport.Error", m_iClientId), p_objException);
			}
			finally
			{
                if (objDS != null)
                {
                    objDS.Dispose();
                }
				if (objReader!=null)
				{
					objReader.Close();
                    objReader.Dispose();
				}
                //nadim for 14476
                if (objDataModelFacFortitle != null)
                {
                    objDataModelFacFortitle.UnInitialize();
                    objDataModelFacFortitle = null;
                }
                if (objClaimForTitle != null)
                {
                    objClaimForTitle.Dispose();
                    objClaimForTitle = null;
                }
                if (objEventForTitle != null)
                {
                    objEventForTitle.Dispose();
                    objEventForTitle = null;
                }
                if (objLocalCacheForTitle != null)
                {
                    objLocalCacheForTitle.Dispose();
                    objLocalCacheForTitle = null;
                }
                if (objLobSettings != null)
                {
                    objLobSettings = null;
                }
                objSysSettings = null;
                //nadim for 14476
				arrColAlign = null;
				arrColWidth = null;	
				m_objPrintDoc = null;
				m_objFont = null;
				m_objText = null;
				htTranId = null;
			}
			return iReturnValue;

		}
		#endregion

        /// <summary>//MITS Manish Jain
        /// 
        ///   /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 04 Oct 2011     Function Added to   Manish Jain
        ///                 modify filter
        ///                   string 
        ///************************************************************
        /// </summary>
        /// <param name="s_FilterExp">Filterexp for Radgrid Column</param>


        private string ModifyFilterexp(string s_FilterExp)
        {
            string s_DtOrgfilter = null;   // Date original filter
            string s_CondSep = null;      // condition seprator
            DbConnection objConn = null;
            objConn = DbFactory.GetDbConnection(m_sConnectionString);
            objConn.Open();
            String m_sDBType = objConn.DatabaseType.ToString();
            objConn.Close();

			//rkulavil :RMA-12789/MITS 38521 starts
            //if (s_FilterExp.Contains("[CtlNumber]"))                //Control #
            //{
              //  s_FilterExp = s_FilterExp.Replace("[CtlNumber]", "FUNDS.CTL_NUMBER");
            //}
			//rkulavil :RMA-12789/MITS 38521 ends
            if (s_FilterExp.Contains("[TransNumber]"))           // Check #
            {
                s_FilterExp = s_FilterExp.Replace("[TransNumber]", "FUNDS.TRANS_NUMBER");

            }

            if (s_FilterExp.Contains("[PaymentFlag]"))         //Type
            {
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[PaymentFlag]"));

                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);

                string s_dtFexp_Upper = s_dtFexp.ToUpper();

                if (s_dtFexp_Upper == "PAYMENT")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.PAYMENT_FLAG  in (1,-1)");
                }
                //else
                //{
                //    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.PAYMENT_FLAG " + s_CondSep + "(0)");
                //}
                // mkaran2 - MITS 32620 - Start
                else if (s_dtFexp_Upper == "COLLECTION") // Collection
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.COLLECTION_FLAG  in (1,-1)");
                }
                else // random int value
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.PAYMENT_FLAG " + s_CondSep + "(2)");
                }
                // mkaran2 - MITS 32620 - End

            }

            if (s_FilterExp.Contains("[ClearedFlag]"))       //Clr?
            {
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[ClearedFlag]"));

                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);

                string s_dtFexp_Upper = s_dtFexp.ToUpper();
				//rkulavil :RMA-12789/MITS 38521 starts
                //if (s_dtFexp_Upper == "YES")
                //{
                //    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.CLEARED_FLAG" + s_CondSep + "(-1)");
                //}
                //else
                //{
                //    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.VOID_FLAG" + s_CondSep + "(0)");
                //}
                if (s_dtFexp_Upper == "NO")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.CLEARED_FLAG" + s_CondSep + "(0)");
                }
                else
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.CLEARED_FLAG" + s_CondSep + "(-1) AND FUNDS.VOID_DATE='" + Conversion.GetDate(s_dtFexp_Upper)+"'");
                }
				//rkulavil :RMA-12789/MITS 38521 ends
            }

            if (s_FilterExp.Contains("[VoidFlag]"))           //Void?
            {
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[VoidFlag]"));

                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);

                string s_dtFexp_Upper = s_dtFexp.ToUpper();

                if (s_dtFexp_Upper == "YES")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.VOID_FLAG in (1,-1)");
                }
                //avipinsrivas start : Worked on JIRA - 12104
                else if (int.Equals(s_dtFexp_Upper.Length, 8))
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.VOID_FLAG=-1 AND FUNDS.VOID_DATE" + s_CondSep + "" + s_dtFexp_Upper);
                }
                //avipinsrivas end
                else
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.VOID_FLAG" + s_CondSep + "(0)");
                }
            }
            if (s_FilterExp.Contains("[StopPayFlag]"))           //Stop Pay
            {
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[StopPayFlag]"));

                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);

                string s_dtFexp_Upper = s_dtFexp.ToUpper();

                if (s_dtFexp_Upper == "NO")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.STOP_PAY_FLAG IS NULL OR FUNDS.STOP_PAY_FLAG" + s_CondSep + "(0)");
                }
                else
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.STOP_PAY_FLAG in (1,-1)");
                }
			//rkulavil :RMA-12789/MITS 38521 ends
            }


            if (s_FilterExp.Contains("[Amount]"))       // CHECK AMOUNT         
            {
                //avipinsrivas start : Worked for JIRA - 11949
                //s_FilterExp = s_FilterExp.Replace("[Amount]", "FUNDS.AMOUNT");
                s_FilterExp = FundManager.GetFilteredAmount(s_FilterExp);
                if (m_sDBType == Constants.DB_ORACLE)
                    s_FilterExp = s_FilterExp.Replace("[Amount]", "ROUND(FUNDS.AMOUNT,2)");
                else
                    s_FilterExp = s_FilterExp.Replace("[Amount]", "CONVERT(DECIMAL(37,2),FUNDS.AMOUNT)");
                //avipinsrivas end
            }
            if (s_FilterExp.Contains("[InvoiceAmount]"))       // Invoice AMOUNT         
            {
                //avipinsrivas start : Worked for JIRA - 11949
                //s_FilterExp = s_FilterExp.Replace("[InvoiceAmount]", "FUNDS_TRANS_SPLIT.INVOICE_AMOUNT");
                s_FilterExp = FundManager.GetFilteredAmount(s_FilterExp);
                if (m_sDBType == Constants.DB_ORACLE)
                    s_FilterExp = s_FilterExp.Replace("[InvoiceAmount]", "ROUND(FUNDS_TRANS_SPLIT.INVOICE_AMOUNT,2)");
                else
                    s_FilterExp = s_FilterExp.Replace("[InvoiceAmount]", "CONVERT(DECIMAL(37,2),FUNDS_TRANS_SPLIT.INVOICE_AMOUNT)");
                //avipinsrivas end
            }


            if (s_FilterExp.Contains("[InvoiceNumber]"))  //Invoice#   
            {
                s_FilterExp = s_FilterExp.Replace("[InvoiceNumber]", "FUNDS_TRANS_SPLIT.INVOICE_NUMBER");
				//rkulavil :RMA-12789/MITS 38521 starts
                if (s_FilterExp.Contains("NOT LIKE") || s_FilterExp.Contains("<>"))
                {
                    int idx = s_FilterExp.LastIndexOf(')');
                    s_FilterExp = s_FilterExp.Insert(idx, " OR FUNDS_TRANS_SPLIT.INVOICE_NUMBER IS NULL )");
                    s_FilterExp = "(" + s_FilterExp;
                }
				//rkulavil :RMA-12789/MITS 38521 ends
            }

            // akaushik5 Commented for MITS 36765 Starts
            //if (s_FilterExp.Contains("[CodeDesc]"))  // Transaction Type
            //{
            //    s_FilterExp = s_FilterExp.Replace("[CodeDesc]", "CODES_TEXT.CODE_DESC");
            //}
            // akaushik5 Commented for MITS 36765 Ends


            if (s_FilterExp.Contains("[SplitAmount]"))  //Split Amount
            {
                //avipinsrivas start : Worked for JIRA - 11949
                //s_FilterExp = s_FilterExp.Replace("[SplitAmount]", "FUNDS_TRANS_SPLIT.AMOUNT");
                s_FilterExp = FundManager.GetFilteredAmount(s_FilterExp);
                if (m_sDBType == Constants.DB_ORACLE)
                    s_FilterExp = s_FilterExp.Replace("[SplitAmount]", "ROUND(FUNDS_TRANS_SPLIT.AMOUNT,2)");
                else
                    s_FilterExp = s_FilterExp.Replace("[SplitAmount]", "CONVERT(DECIMAL(37,2),FUNDS_TRANS_SPLIT.AMOUNT)");
                //avipinsrivas end
            }
            //avipinsrivas start : Worked for JIRA - 11949
            if (s_FilterExp.Contains("[CheckTotal]"))  //Check Total
            {
                s_FilterExp = FundManager.GetFilteredAmount(s_FilterExp);
                if (m_sDBType == Constants.DB_ORACLE)
                    s_FilterExp = s_FilterExp.Replace("[CheckTotal]", "ROUND(FUNDS.CHECK_TOTAL,2)");
                else
                    s_FilterExp = s_FilterExp.Replace("[CheckTotal]", "CONVERT(DECIMAL(37,2),FUNDS.CHECK_TOTAL)");
            }
            //avipinsrivas end
			//rkulavil :RMA-12789/MITS 38521 starts
            //if (s_FilterExp.Contains("[User]"))    // User
            //{
            //    s_FilterExp = s_FilterExp.Replace("[User]", "FUNDS_TRANS_SPLIT.ADDED_BY_USER");
            //}
			//rkulavil :RMA-12789/MITS 38521 ends

            if (s_FilterExp.Contains("[IsFirstFinal]"))         //First and Final Payment
            {
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[IsFirstFinal]"));

                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);

                string s_dtFexp_Upper = s_dtFexp.ToUpper();

                if (s_dtFexp_Upper == "NO")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "((FUNDS_TRANS_SPLIT.IS_FIRST_FINAL is null) or (FUNDS_TRANS_SPLIT.IS_FIRST_FINAL = '0'))");
                }
                else
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS_TRANS_SPLIT.IS_FIRST_FINAL" + s_CondSep + "(-1)");
                }

            }



            if (s_FilterExp.Contains("[CheckStatus]"))         //CheckStatus
            {
                LocalCache objLocalCache =  new LocalCache(m_sConnectionString,m_iClientId);
                //int a =  filterexp.IndexOf("[CheckStatus]");
                string s_Subfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[CheckStatus]"));
                //string s_Orgfilter = s_Subfilter.Substring(0, s_Subfilter.IndexOf(")"));            //avipinsrivas start : Worked for JIRA - 12104
                
                 //int b =  subfilter.IndexOf("'");
                string s_Exp = s_Subfilter.Substring((s_Subfilter.IndexOf("'")) + 1);
                  //int l = exp.IndexOf("'");
                string s_Fexp = s_Exp.Substring(0, s_Exp.IndexOf("'"));
                int s_CodeId = objLocalCache.GetCheckStatus(s_Fexp.ToUpper());

                if (s_FilterExp.Contains(s_Fexp))
                {
                    //avipinsrivas start : Worked for JIRA - 12104
                    string sRepString = string.Concat("[CheckStatus] = '", s_Fexp, "'");
                    s_FilterExp = s_FilterExp.Replace(sRepString, "FUNDS.STATUS_CODE = " + s_CodeId);
                    //s_FilterExp = s_FilterExp.Replace(s_Orgfilter, "FUNDS.STATUS_CODE = " + s_CodeId);
                    //avipinsrivas end
                }

            }

           
			//rkulavil :RMA-12789/MITS 38521 starts
            if (s_FilterExp.Contains("[CombinedPay]"))    // Combined Pay
            {

                //s_FilterExp = s_FilterExp.Replace("[CombinedPay]", "FUNDS.COMBINED_PAY_FLAG");
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[CombinedPay]"));
                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                string s_dtFexp_Upper = s_dtFexp.ToUpper();

                if (s_dtFexp_Upper == "NO")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.COMBINED_PAY_FLAG = 0");
                }
                else if (s_dtFexp_Upper == "YES")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.COMBINED_PAY_FLAG <> 0");
                }
                else 
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.COMBINED_PAY_FLAG = 2");
                }
            }

            if (s_FilterExp.Contains("[ManualCheck]"))    // Manual Check
            {
                //s_FilterExp = s_FilterExp.Replace("[ManualCheck]", "FUNDS.MANUAL_CHECK");
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[ManualCheck]"));
                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                string s_dtFexp_Upper = s_dtFexp.ToUpper();

                if (s_dtFexp_Upper == "NO")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.MANUAL_CHECK = 0");
                }
                else if (s_dtFexp_Upper == "YES")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.MANUAL_CHECK <> 0");
                }
                else
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.MANUAL_CHECK = 2");
                }
            }

            if (s_FilterExp.Contains("[Offset]"))    // Offset
            {
                //s_FilterExp = s_FilterExp.Replace("[Offset]", "FUNDS.OFFSET_FLAG");
                string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[Offset]"));
                string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                string s_dtFexp_Upper = s_dtFexp.ToUpper();

                if (s_dtFexp_Upper == "NA")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.VOID_FLAG=-1 OR FUNDS.AMOUNT < 0 OR FUNDS.COLLECTION_FLAG <> 0");
                }
                else if (s_dtFexp_Upper == "NO")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.VOID_FLAG <> -1 AND FUNDS.AMOUNT >= 0 AND FUNDS.COLLECTION_FLAG = 0 AND (FUNDS.OFFSET_FLAG = 0 OR FUNDS.OFFSET_FLAG IS NULL)");
                }
                else if (s_dtFexp_Upper == "YES")
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.OFFSET_FLAG = -1");
                }
                else
                {
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.MANUAL_CHECK = 2");
                }
            }
            //rkulavil :RMA-12789/MITS 38521 starts
            if (s_FilterExp.Contains("[TransDate]"))         // TransDate 
                {
                    string dsubfilter = default(string);
                    string s_dtFexp = default(string);
                    string dsubfilter2 = default(string);
                    string s_dtFexp2 = default(string);
                    string s_DtOrgfilter2 = null;
                    string s_CondSep2 = null;
                    int idx = s_FilterExp.IndexOf("[TransDate]");
                    int idx2 = s_FilterExp.LastIndexOf("[TransDate]");

                    if (idx.CompareTo(idx2) == 0) // either trans from or trans to 
                    {
                        dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[TransDate]"));
                        dsubfilter2 = dsubfilter.Substring((dsubfilter.IndexOf("AND")) + 5);
                        s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                        s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.TRANS_DATE " + s_CondSep + "'" + Conversion.GetDate(s_dtFexp) + "'");
                    }
                    else //trans from and trans to only 
                    {
                        dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[TransDate]"));
                        dsubfilter2 = dsubfilter.Substring((dsubfilter.IndexOf("AND")) + 5);
                        s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                        s_dtFexp2 = GetDateStrng(dsubfilter2, out  s_DtOrgfilter2, out  s_CondSep2);
                        s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.TRANS_DATE " + s_CondSep + "'" + Conversion.GetDate(s_dtFexp) + "'");
                        s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter2, "FUNDS.TRANS_DATE " + s_CondSep2 + "'" + Conversion.GetDate(s_dtFexp2) + "'");

                    }

                    //dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[TransDate]"));
                    //dsubfilter2 = dsubfilter.Substring((dsubfilter.IndexOf("AND"))+5);
                    //s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                    //s_dtFexp2 = GetDateStrng(dsubfilter2, out  s_DtOrgfilter2, out  s_CondSep2);

                    //string[] transdate = s_dtFexp.Split(' ');
                    //if (transdate.Length > 1)
                    //{
                    //     s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter,string.Format("{0}{1}{2}{3}{4}","FUNDS.TRANS_DATE >='" , Conversion.GetDate(transdate[0]),"' AND FUNDS.TRANS_DATE <= '",Conversion.GetDate(transdate[1]),"'"));
                    //}
                    //else
                    //{
                    //    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "FUNDS.TRANS_DATE " + s_CondSep + "'" + Conversion.GetDate(s_dtFexp) + "'");
                    //    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter2, "FUNDS.TRANS_DATE" + s_CondSep2 + "'" + Conversion.GetDate(s_dtFexp2) + "'");
                    //}
                }
           	//rkulavil :RMA-12789/MITS 38521 ends
            if (m_sDBType == Constants.DB_ORACLE)
            {
				//rkulavil :RMA-12789/MITS 38521 starts
                if (s_FilterExp.Contains("[CtlNumber]"))                //Control #
                {
                    string sActualFilterValue=default(string);
                    string sOrigFilter = default(string);
                    string s_dtFexp = GetProperFilterValue(s_FilterExp, out sActualFilterValue);
                    string sfilter = (sActualFilterValue.IndexOf("AND") != -1 ? sActualFilterValue.Substring(0, sActualFilterValue.IndexOf("AND") - 3) : sActualFilterValue);
                    sOrigFilter = (s_dtFexp.IndexOf("AND") != -1 ? s_dtFexp.Substring(0, s_dtFexp.IndexOf("AND") - 3) : s_dtFexp);
                    s_FilterExp = s_FilterExp.Replace("[CtlNumber]", "UPPER(TRIM(FUNDS.CTL_NUMBER))");
                    s_FilterExp = s_FilterExp.Replace(sfilter, sOrigFilter.ToUpper());
                }
				//rkulavil :RMA-12789/MITS 38521 ends
                // akaushik5 Added for MITS 36765 Starts
                if (s_FilterExp.Contains("[CodeDesc]"))  // Transaction Type
                {
					//rkulavil :RMA-12789/MITS 38521 starts
                    //s_FilterExp = s_FilterExp.Replace("[CodeDesc]", " UPPER(LTRIM(RTRIM(CONCAT(CONCAT(CODES_TEXT.SHORT_CODE, ' '),CODES_TEXT.CODE_DESC)))) ").ToUpper();
                    s_FilterExp = s_FilterExp.Replace("[CodeDesc]", " UPPER(LTRIM(RTRIM(CONCAT(CONCAT(CODES_TEXT.SHORT_CODE, ' '),CODES_TEXT.CODE_DESC)))) ");
					//rkulavil :RMA-12789/MITS 38521 ends
                }
                // akaushik5 Added for MITS 36765 Ends
                
                if (s_FilterExp.Contains("[Name]"))    // PAYEE 
                {
					//rkulavil :RMA-12789/MITS 38521 starts
                    string sActualFilterValue = default(string);
                    string s_dtFexp = GetProperFilterValue(s_FilterExp, out sActualFilterValue).ToUpper();
                    s_FilterExp = s_FilterExp.Replace("[Name]", "(nvl(upper(FUNDS.LAST_NAME), '') ||  nvl(upper(FUNDS.FIRST_NAME),''))").Replace(sActualFilterValue, s_dtFexp);//jramkumar for MITS 32753
					//rkulavil :RMA-12789/MITS 38521 ends
                }

                //rkulavil :RMA-12789/MITS 38521 starts
                if (s_FilterExp.Contains("[CoverageType]"))  // CoverageType
                {
                    s_FilterExp = s_FilterExp.Replace("[CoverageType]", " UPPER(LTRIM(RTRIM(CONCAT(CONCAT(POL.SHORT_CODE, ' '),POL.CODE_DESC)))) ");
				}
				if (s_FilterExp.Contains("[TransDate]"))  // CoverageType 
                {
                    string s_DtOrgfilter2 = null;
                    string s_CondSep2 = null;
                    string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[TransDate]"));
                    string dsubfilter2 = dsubfilter.Substring((dsubfilter.IndexOf("AND"))+5);

                    string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                    string s_dtFexp2 = GetDateStrng(dsubfilter2, out  s_DtOrgfilter2, out  s_CondSep2);

                    //avipinsrivas start : Worked for JIRA - 12104
                    //s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS.TRANS_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','mm/dd/yyyy')");
                    //s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter2, "TO_DATE(FUNDS.TRANS_DATE,'yyyymmdd') " + s_CondSep2 + "TO_DATE('" + s_dtFexp2 + "','mm/dd/yyyy')");
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS.TRANS_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','yyyymmdd')");
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter2, "TO_DATE(FUNDS.TRANS_DATE,'yyyymmdd') " + s_CondSep2 + "TO_DATE('" + s_dtFexp2 + "','yyyymmdd')");
                    //avipinsrivas end

                }

                if (s_FilterExp.Contains("[FromDate]"))  // FROM DATE 
                {
                    string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[FromDate]"));

                    string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);
                    //avipinsrivas start : Worked for JIRA - 12104
                    //s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS_TRANS_SPLIT.FROM_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','mm/dd/yyyy')"); 
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS_TRANS_SPLIT.FROM_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','yyyymmdd')"); 
                    //avipinsrivas end

                    //filterexp = filterexp.Replace("[FromDate]", "CONVERT(DATE,(FUNDS_TRANS_SPLIT.FROM_DATE))");
                }

                if (s_FilterExp.Contains("[ToDate]"))    // TO DATE 
                {
                    string dsubfilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[ToDate]"));

                    string s_dtFexp = GetDateStrng(dsubfilter, out  s_DtOrgfilter, out  s_CondSep);

                    //avipinsrivas start : Worked for JIRA - 12104
                    //s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS_TRANS_SPLIT.TO_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','mm/dd/yyyy')"); 
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS_TRANS_SPLIT.TO_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','yyyymmdd')"); 
                    //avipinsrivas end
                    //filterexp = filterexp.Replace("[ToDate]", "CONVERT(DATE,(FUNDS_TRANS_SPLIT.TO_DATE))");
                }


                if (s_FilterExp.Contains("[CheckDate]"))   // Check Date 
                {
                    string s_dtSubFilter = s_FilterExp.Substring(s_FilterExp.IndexOf("[CheckDate]"));

                    string s_dtFexp = GetDateStrng(s_dtSubFilter, out  s_DtOrgfilter, out  s_CondSep);
                    //avipinsrivas start : Worked for JIRA - 12104
                    //s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS.TRANS_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','mm/dd/yyyy')");
                    s_FilterExp = s_FilterExp.Replace(s_DtOrgfilter, "TO_DATE(FUNDS.TRANS_DATE,'yyyymmdd') " + s_CondSep + "TO_DATE('" + s_dtFexp + "','yyyymmdd')");
                    //avipinsrivas end
                    //filterexp = filterexp.Replace("[CheckDate]", "CONVERT(DATE,(FUNDS.DATE_OF_CHECK))");
                }
				//rkulavil :RMA-12789/MITS 38521 starts
                if (s_FilterExp.Contains("[CheckMemo]"))                //CheckMemo
                {
                    s_FilterExp = s_FilterExp.Replace("[CheckMemo]", "UPPER(FUNDS.CHECK_MEMO)");
                    if (s_FilterExp.Contains("NOT LIKE") || s_FilterExp.Contains("<>"))
                    {
                        int idx = s_FilterExp.LastIndexOf(')');
                        s_FilterExp=s_FilterExp.Insert(idx," OR FUNDS.CHECK_MEMO IS NULL )");
                        s_FilterExp = "(" + s_FilterExp;
                    }
                }

                if (s_FilterExp.Contains("[User]"))    // User
                {
                    s_FilterExp = s_FilterExp.Replace("[User]", "UPPER(FUNDS_TRANS_SPLIT.ADDED_BY_USER)");
                }

                if (s_FilterExp.Contains("[PolicyName]"))    // PolicyName
                {
                    s_FilterExp = s_FilterExp.Replace("[PolicyName]", "UPPER(POLICY.POLICY_NAME)");
                }

                if (s_FilterExp.Contains("[ReserveType]"))  // Transaction Type
                {
                    s_FilterExp = s_FilterExp.Replace("[ReserveType]", " UPPER(LTRIM(RTRIM(CONCAT(CONCAT(RESERVE_TYPE.SHORT_CODE, ' '),RESERVE_TYPE.CODE_DESC)))) ");
                }
				//rsriharsha RMA-16958 starts
                if (s_FilterExp.Contains("[PayToTheOrderOf]"))    // Pay to the order of
                {
                    //rkulavil :RMA-16958 starts
                    //s_FilterExp = s_FilterExp.Replace("[PayToTheOrderOf]", "UPPER(FUNDS.PAY_TO_THE_ORDER_OF)");
                    s_FilterExp = s_FilterExp.Replace("[PayToTheOrderOf]", "UPPER(DBMS_LOB.substr(FUNDS.PAY_TO_THE_ORDER_OF, 4000))");
                    //rkulavil :RMA-16958 ends
                }
				//rsriharsha RMA-16958 ends
            }
            else
            {
				//rkulavil :RMA-12789/MITS 38521 starts
                if (s_FilterExp.Contains("[CtlNumber]"))                //Control #
                {
                    string sActualFilterValue = default(string);
                    string sOrigFilter = default(string);
                    string s_dtFexp = GetProperFilterValue(s_FilterExp, out sActualFilterValue);
                    string sfilter = (sActualFilterValue.IndexOf("AND") != -1 ? sActualFilterValue.Substring(0, sActualFilterValue.IndexOf("AND") - 3) : sActualFilterValue);
                    sOrigFilter = (s_dtFexp.IndexOf("AND") != -1 ? s_dtFexp.Substring(0, s_dtFexp.IndexOf("AND") - 3) : s_dtFexp);
                    s_FilterExp = s_FilterExp.Replace("[CtlNumber]", "FUNDS.CTL_NUMBER");
                    s_FilterExp = s_FilterExp.Replace(sfilter, sOrigFilter.ToUpper());
                }
				//rkulavil :RMA-12789/MITS 38521 ends
                // akaushik5 Added for MITS 36765 Starts
                if (s_FilterExp.Contains("[CodeDesc]"))  // Transaction Type
                {
                    s_FilterExp = s_FilterExp.Replace("[CodeDesc]", "(CODES_TEXT.SHORT_CODE + ' ' + CODES_TEXT.CODE_DESC)");
                }
                // akaushik5 Added for MITS 36765 Ends
                
                if (s_FilterExp.Contains("[Name]"))    // PAYEE 
                {
                    //s_FilterExp = s_FilterExp.Replace("[Name]", "(ISNULL(FUNDS.LAST_NAME,'')+ISNULL(FUNDS.FIRST_NAME,''))");
                    //Ashish Ahuja : MITS 32340
                    s_FilterExp = s_FilterExp.Replace("[Name]", "(ISNULL(FUNDS.FIRST_NAME,'')+ISNULL(FUNDS.LAST_NAME,''))");
                    //Ashish Ahuja : MITS 32340
                }

                //rkulavil :RMA-12789/MITS 38521 starts 
                if (s_FilterExp.Contains("[TransDate]"))  // CoverageType 
                {
                    s_FilterExp = s_FilterExp.Replace("[TransDate]", "CONVERT(DATE,(FUNDS.TRANS_DATE))");
                }
                if (s_FilterExp.Contains("[CoverageType]"))  // CoverageType 
                {
                    s_FilterExp = s_FilterExp.Replace("[CoverageType]", "(POL.SHORT_CODE + ' ' + POL.CODE_DESC)");
                }

                if (s_FilterExp.Contains("[FromDate]"))  // FROM DATE 
                {
                    s_FilterExp = s_FilterExp.Replace("[FromDate]", "CONVERT(DATE,(FUNDS_TRANS_SPLIT.FROM_DATE))");
                }

                if (s_FilterExp.Contains("[ToDate]"))    // TO DATE 
                {
                    s_FilterExp = s_FilterExp.Replace("[ToDate]", "CONVERT(DATE,(FUNDS_TRANS_SPLIT.TO_DATE))");
                }


                if (s_FilterExp.Contains("[CheckDate]"))   // Check Date 
                {
                    s_FilterExp = s_FilterExp.Replace("[CheckDate]", "CONVERT(DATE,(FUNDS.DATE_OF_CHECK))");
                }
				//rkulavil :RMA-12789/MITS 38521 starts
                if (s_FilterExp.Contains("[CheckMemo]"))                //CheckMemo 
                {
                    s_FilterExp = s_FilterExp.Replace("[CheckMemo]", "FUNDS.CHECK_MEMO");
                    if (s_FilterExp.Contains("NOT LIKE"))
                    {
                        int idx = s_FilterExp.LastIndexOf(')');
                        s_FilterExp = s_FilterExp.Insert(idx, " OR FUNDS.CHECK_MEMO IS NULL )");
                        s_FilterExp = "(" + s_FilterExp;
                    }

                }

                if (s_FilterExp.Contains("[User]"))    // User
                {
                    s_FilterExp = s_FilterExp.Replace("[User]", "FUNDS_TRANS_SPLIT.ADDED_BY_USER");
                }

                if (s_FilterExp.Contains("[PolicyName]"))    // PolicyName
                {
                    s_FilterExp = s_FilterExp.Replace("[PolicyName]", "POLICY.POLICY_NAME");
                }

                if (s_FilterExp.Contains("[ReserveType]"))  // Transaction Type
                {
                    s_FilterExp = s_FilterExp.Replace("[ReserveType]", "(RESERVE_TYPE.SHORT_CODE + ' ' + RESERVE_TYPE.CODE_DESC)");
                }
				//rkulavil :RMA-12789/MITS 38521 ends
				//rsriharsha RMA-16958 starts
                if (s_FilterExp.Contains("[PayToTheOrderOf]"))    // Pay to the order of
                {
                    s_FilterExp = s_FilterExp.Replace("[PayToTheOrderOf]", "FUNDS.PAY_TO_THE_ORDER_OF");
                }
                //rsriharsha RMA-16958 ends
            
            }
            return s_FilterExp;
        }



        /// <summary>//MITS Manish Jain
        /// 
        ///   /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
      
        ///************************************************************
        /// </summary>
        /// <param name="s_DtSubfilter">Date sub filter string</param>
        ///  <param name="s_Dtorgfilter">Date original Filter string</param>
        /// <param name="s_CondSep">Condition Seprator</param>
        private string GetDateStrng(string s_DtSubfilter, out string s_Dtorgfilter, out string s_CondSep)
        {

            s_Dtorgfilter = s_DtSubfilter.Substring(0, s_DtSubfilter.IndexOf(")"));

            string s_Dtorgsubfilter = s_Dtorgfilter.Substring(s_Dtorgfilter.IndexOf("]") + 1);

            s_CondSep = s_Dtorgsubfilter.Substring(0, s_Dtorgsubfilter.IndexOf("'"));

            //int b =  subfilter.IndexOf("'");
             string dexp = s_DtSubfilter.Substring((s_DtSubfilter.IndexOf("'")) + 1);
            //int l = exp.IndexOf("'");
            string dfexp = dexp.Substring(0, dexp.IndexOf("'"));

            return dfexp;
            
        }
		//rkulavil :RMA-12789/MITS 38521 starts
        private string GetProperFilterValue(string p_sFilterValue, out string sActualFilterValue)
        {
            string sTempFilterString = default(string);
            string sFilterColumn = default(string);
            string sFilterOperator = default(string);
            int iStartIndex = default(int);
            int iEndIndex = default(int);
            sActualFilterValue = default(string);
            if (p_sFilterValue.CompareTo(string.Empty) != 0)
            {
                sTempFilterString = p_sFilterValue.Substring(iStartIndex);
                //Fetch table column name
                iStartIndex = sTempFilterString.IndexOf("[");
                iEndIndex = sTempFilterString.IndexOf("]");
                sFilterColumn = (sTempFilterString.Substring(iStartIndex + 1, (iEndIndex - iStartIndex) - 1)).Trim();
                //Fetch comparison operator
                iStartIndex = iEndIndex + 2;
                iEndIndex = sTempFilterString.IndexOf("'", iStartIndex);
                sFilterOperator = (sTempFilterString.Substring(iStartIndex, (iEndIndex - iStartIndex))).Trim();
                //Fetch comparison value
                iStartIndex = sTempFilterString.IndexOf("'");
                iEndIndex = (sTempFilterString.LastIndexOf("')")) - 1;
                sActualFilterValue = (sTempFilterString.Substring(iStartIndex + 1, iEndIndex - iStartIndex)).Trim();
                //sFilterValue = sActualFilterValue.Replace("'", "''");
            }
            return sActualFilterValue;
        }
		//rkulavil :RMA-12789/MITS 38521 ends

		#region private Report Functions

		/// Name		: CreatePaymentSQL
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005
		///************************************************************
		/// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 27 Sept 2011     Changes made to    Manish Jain
        ///                  add filter string
        ///                  in parameter
        ///************************************************************
		/// <summary>
		/// Creates the SQL query for the report
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_iClaimantEid">Claimant ID</param>
		/// <param name="p_iUnitId">Unit ID</param>
		/// <param name="p_sSortCol">Sort Column</param>
		/// <param name="p_sFilterexp">String for filter expression</param>
		/// <param name="p_bAscending">Ascending Flag</param>
        /// private string CreatePaymentSQL(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, string p_sFilterexp, bool p_bAscending,int p_iEntityId )
        private string CreatePaymentSQL(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, string p_sFilterexp, bool p_bAscending,int p_iEntityId, UserLogin p_objUserLogin,int p_iUseMultiCurrency, CurrencyType eSelectedCurrencyType)
		{
			StringBuilder sbSQL = new StringBuilder();
			string sSortCol = string.Empty;
			bool bAscending = false;
            SysSettings objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
            //Added:Yukti, RMA -360, Dt:07/02/2014
            int iLangCode = p_objUserLogin.objUser.NlsCode;
            //Added by Amitosh for R8 enhancement.Adding new columns to payment history grid
            if (objSysSetting.MultiCovgPerClm == -1)
            {
                bCarrierClaim = true;
                sbSQL.Append(" SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.DATE_OF_CHECK,");
                if (p_iUseMultiCurrency != 0)
                {
                    if (eSelectedCurrencyType == CurrencyType.BASE_CURRENCY)
                    {
                        sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT  FAMT, FUNDS_TRANS_SPLIT.INVOICE_AMOUNT FIAMT, FUNDS.FIRST_NAME, ");
                        sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT  FTSAMT,");
                    }
                    else if (eSelectedCurrencyType == CurrencyType.PAYMENT_CURRENCY)
                    {
                        sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.PMT_CURRENCY_AMOUNT  FAMT, FUNDS_TRANS_SPLIT.PMT_CURRENCY_INVOICE_AMOUNT FIAMT, FUNDS.FIRST_NAME, ");
                        sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.PMT_CURRENCY_AMOUNT  FTSAMT,");
                    }
                    else
                    {
                        sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.CLAIM_CURRENCY_AMOUNT  FAMT, FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_INVOICE_AMOUNT FIAMT, FUNDS.FIRST_NAME, ");
                        sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_AMOUNT  FTSAMT,");
                    }
                }
                else
                {
                    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT  FAMT, FUNDS_TRANS_SPLIT.INVOICE_AMOUNT FIAMT, FUNDS.FIRST_NAME, ");
                    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT  FTSAMT,");
                }
                sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE,FUNDS.COMBINED_PAY_FLAG, ");
                sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, ");
                sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.CHECK_MEMO, FUNDS.STATUS_CODE");
                sbSQL.Append(" ,POL.CODE_DESC COVERAGETYPE,POLICY.POLICY_NAME,LOSSCODE.CODE_DESC LOSS_CODE, ");
                //RUpal:start,policy interface change
                //sbSQL.Append("  CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT VIN FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID) WHEN 'E' THEN (SELECT EMPLOYEE_DETAILS FROM WC_POLICY_EMPLOYEE) END UNITTYPE");
                sbSQL.Append(" CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT VIN FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT ENTITY.LAST_NAME FROM ENTITY,OTHER_UNIT WHERE OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNITTYPE, RESERVE_TYPE.SHORT_CODE RES_TYPE_SHORT_CODE, RESERVE_TYPE.CODE_DESC RES_TYPE_CODE_DESC");
                sbSQL.Append(" ,FUNDS.VOID_DATE");      //Ankit Start : Worked on MITS - 33629
                
                //rupal:end

               // sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT,CODES_TEXT,FUNDS  ");
                // mkaran2 - MITS 32340 : Start
                sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT,CODES_TEXT,FUNDS ,CODES_TEXT B, RESERVE_CURRENT,COVERAGE_X_LOSS ");                
                //  mkaran2 - MITS 32340 : End
                sbSQL.Append(" ,POLICY_X_CVG_TYPE,CODES_TEXT POL,POLICY, POLICY_X_UNIT, CODES_TEXT LOSSCODE, CODES_TEXT RESERVE_TYPE");

                sbSQL.Append(" WHERE  FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                //sbSQL.Append(" AND  FUNDS_TRANS_SPLIT.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RC_ROW_ID= RESERVE_CURRENT.RC_ROW_ID");
                sbSQL.Append(" AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID");
                sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID ");
                sbSQL.Append("   AND LOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE  ");
                sbSQL.Append("   AND POL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE  ");
                sbSQL.Append("  AND   POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                sbSQL.Append(" AND POLICY.POLICY_ID =POLICY_X_UNIT.POLICY_ID");
                sbSQL.Append(" AND RESERVE_TYPE.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE AND RESERVE_TYPE.LANGUAGE_CODE = ");
                sbSQL.Append(iLangCode);

            }
            else
            {
                bCarrierClaim = false;
                sbSQL.Append(" SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.DATE_OF_CHECK,");
                if (p_iUseMultiCurrency != 0)
                {
                    if (eSelectedCurrencyType == CurrencyType.BASE_CURRENCY)
                    {
                        sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT  FAMT,FUNDS_TRANS_SPLIT.INVOICE_AMOUNT  FIAMT , FUNDS.FIRST_NAME, ");
                        sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT  FTSAMT,");
                    }
                    else if (eSelectedCurrencyType == CurrencyType.PAYMENT_CURRENCY)
                    {
                        sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.PMT_CURRENCY_AMOUNT  FAMT,FUNDS_TRANS_SPLIT.PMT_CURRENCY_INVOICE_AMOUNT  FIAMT , FUNDS.FIRST_NAME, ");
                        sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.PMT_CURRENCY_AMOUNT  FTSAMT,");
                    }
                    else
                    {
                        sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.CLAIM_CURRENCY_AMOUNT  FAMT,FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_INVOICE_AMOUNT  FIAMT , FUNDS.FIRST_NAME, ");
                        sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_AMOUNT  FTSAMT,");
                    }
                }
                else
                {
                    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT  FAMT,FUNDS_TRANS_SPLIT.INVOICE_AMOUNT  FIAMT , FUNDS.FIRST_NAME, ");
                    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT  FTSAMT,");
                }
                sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE, ");
                sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, ");
            sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.CHECK_MEMO, FUNDS.STATUS_CODE, RESERVE_TYPE.SHORT_CODE RES_TYPE_SHORT_CODE, RESERVE_TYPE.CODE_DESC RES_TYPE_CODE_DESC");
            sbSQL.Append(" ,FUNDS.VOID_DATE");      //Ankit Start : Worked on MITS - 33629
			//rsriharsha RMA-16958 starts
            sbSQL.Append(" ,FUNDS.PAY_TO_THE_ORDER_OF");
			//rsriharsha RMA-16958 ends     
			//sbSQL.Append( " FROM FUNDS, FUNDS_TRANS_SPLIT,CODES_TEXT " );
            // mkaran2 - MITS 32340 : Start
            sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT,CODES_TEXT ,CODES_TEXT B , CODES_TEXT RESERVE_TYPE");             
            //  mkaran2 - MITS 32340 : End

                //ddhiman : MITS 27009
                //sbSQL.Append( " WHERE FUNDS.CLAIM_ID = " + p_iClaimId );
                //sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID " );
                sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                //End ddhiman
            }
            //End Amitosh
            if (p_iEntityId != 0)
            {

                sbSQL.Append("  AND FUNDS.PAYEE_EID = " + p_iEntityId);
            }
            else
            {
                sbSQL.Append(" AND FUNDS.CLAIM_ID = " + p_iClaimId);
            }
            //sbSQL.Append( " SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.DATE_OF_CHECK," );
            //sbSQL.Append( " FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, " );
            //sbSQL.Append( " FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT," );
            //sbSQL.Append( " FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE, " );
            //sbSQL.Append( " FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, " );
            //sbSQL.Append( " FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.CHECK_MEMO, FUNDS.STATUS_CODE" );
            //sbSQL.Append( " FROM FUNDS, FUNDS_TRANS_SPLIT,CODES_TEXT " );
            //sbSQL.Append( " WHERE FUNDS.CLAIM_ID = " + p_iClaimId );
            //sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID " );

            if (p_iClaimantEid > 0)
            {
                if (objSysSetting.MultiCovgPerClm == -1)
                    sbSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID = " + p_iClaimantEid);
                else
                {
                    sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid);
                }
            }
			else
			{
				if( p_iUnitId > 0 )
					sbSQL.Append( " AND FUNDS.UNIT_ID = " + p_iUnitId );
			}

			sbSQL.Append( " AND CODES_TEXT.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE " );

            //Added:Yukti, RMA -360, DT:07/02/2014
            sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode);

            // mkaran2 - MITS 32340 : Start
            sbSQL.Append(" AND B.CODE_ID = FUNDS.STATUS_CODE ");
            //  mkaran2 - MITS 32340 : End
            if (objSysSetting.MultiCovgPerClm != 0)
            {
                sbSQL.Append(" AND RESERVE_TYPE.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE AND RESERVE_TYPE.LANGUAGE_CODE = ");
            }
            else
            {
                sbSQL.Append(" AND RESERVE_TYPE.CODE_ID=FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE AND RESERVE_TYPE.LANGUAGE_CODE = ");
            }
            sbSQL.Append(iLangCode);

            // MITS Manish to add filter string
            if (p_sFilterexp != "")
            {
                sbSQL.Append(" AND " + p_sFilterexp.ToUpper());//jramkumar for MITS 32753

            }

			if( p_sSortCol == "" )
			{
				sbSQL.Append( " ORDER BY FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID " );
				sSortCol = "FUNDS.TRANS_DATE" ;
				bAscending = false ;
			}
			else
			{
				sSortCol = p_sSortCol ;
				bAscending = p_bAscending ;

                //nnithiyanand - 32286 - Starts  - this code change is to avoid the error on clicking the print button once the Transacation Type column is sorted.(alias name was not used in sql which is used for printing )
                if (sSortCol == "A.CODE_DESC")
                {
                    sSortCol = "CODES_TEXT.CODE_DESC";
                
                }
                //nnithiyanand - 32286 - Ends  - this code change is to avoid the error on clicking the print button once the Transacation Type column is sorted.(alias name was not used in sql which is used for printing )

                if (bAscending)
                {
                    if (sSortCol.Contains(","))
                    {
                        sbSQL.Append(" ORDER BY " + sSortCol + " DESC");
                    }
                    else   
                    {
                        if (sSortCol == "FUNDS.TRANS_ID")
                        {
                            sbSQL.Append(" ORDER BY FUNDS.TRANS_ID");//nnithiyanand - 32286 - this condition is to avoid the error on clicking the print button once the Offset column is sorted.(same column is used for sorting in order clause )
                        }
                        else if (sSortCol != "FUNDS.TRANS_ID")
                        {
                            sbSQL.Append(" ORDER BY " + sSortCol + " ASC, FUNDS.TRANS_ID");
                        }
                    }
                }
                else
                {
                    //Ashish Ahuja: MITS 32340 Start
                    //sbSQL.Append(" ORDER BY " + sSortCol + " DESC, FUNDS.TRANS_ID");
                    if (sSortCol.Contains(","))
                    {
                        sbSQL.Append(" ORDER BY " + sSortCol + " DESC");
                    }
                    else
                    {
                        if (sSortCol == "FUNDS.TRANS_DATE" && p_sFilterexp == "")
                            sbSQL.Append(" ORDER BY " + sSortCol + " DESC, FUNDS.TRANS_ID DESC");
                        else if (sSortCol == "FUNDS.TRANS_ID")
                            sbSQL.Append(" ORDER BY " + sSortCol + " DESC, FUNDS.TRANS_DATE");
                        else
                            sbSQL.Append(" ORDER BY " + sSortCol + " DESC, FUNDS.TRANS_ID");
                        //Ashish Ahuja: MITS 32340 End
                    }  
                }
            }

            //sbSQL = sbSQL.Replace( "|", " " );

			return sbSQL.ToString();
		}

		/// Name		: PrintHeader
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Prints report header.
		/// </summary>
		/// <param name="p_dblCurrentY">Current Y</param>
		/// <param name="p_iColumnCount">Columns Count</param>
		/// <param name="p_dblPageWidth">Page Width</param>
		/// <param name="p_dblLeftMargin">Left Margin</param>
		/// <param name="p_dblLineHeight">Line Height</param>
		/// <param name="p_arrCol">Columns Text</param>
		/// <param name="p_arrColWidth">Columns Width</param>
		/// <param name="p_arrColAlign">Columns Alignment.</param>
		private void PrintHeader( ref double p_dblCurrentY , int p_iColumnCount , double p_dblPageWidth , 
			double p_dblLeftMargin , double p_dblLineHeight , string[] p_arrCol , 
			double[] p_arrColWidth , string[] p_arrColAlign )
		{
			double dblCurrentX = 0.0 ;
			int iIndex = 0 ;
				
			try
			{				
				dblCurrentX = p_dblLeftMargin ;
				for( iIndex = 0 ; iIndex < p_iColumnCount ; iIndex++ )
				{
					CurrentY = p_dblCurrentY;
					if( p_arrColAlign[iIndex] == LEFT_ALIGN )
					{
						CurrentX = dblCurrentX ;
						PrintText( p_arrCol[iIndex] );
					}
					else
					{
						CurrentX = dblCurrentX + p_arrColWidth[iIndex]
							- GetTextWidth( p_arrCol[iIndex] );
						PrintText( p_arrCol[iIndex] );
					}
					
					dblCurrentX += p_arrColWidth[iIndex] ;
				}
				p_dblCurrentY += p_dblLineHeight * 2 ;

			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.PrintHeader.Error", m_iClientId), p_objException);
			}						
		}

		/// Name		: CreateRow
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a record in a report
		/// </summary>
		/// <param name="p_objData">DataRow object containing a row data</param>
		/// <param name="p_dblCurrentX">Current X position</param>
		/// <param name="p_dblCurrentY">Current Y position</param>
		/// <param name="p_arrColAlign">Columns alignment</param>
		/// <param name="p_dblLeftMargin">Left margin</param>
		/// <param name="p_dblLineHeight">Line height</param>
		/// <param name="p_arrColWidth">Column width</param>
		/// <param name="p_dblCharWidth">Character width</param>
		/// <param name="p_bLeadingLine">Leading Line Flag</param>
		/// <param name="p_bCalTotal">Calculate Total Flag</param>
		/// <param name="p_dblTotalAll">Over all Total</param>
		/// <param name="p_dblTotalPay">Total Payments</param>
		/// <param name="p_dblTotalCollect">Total Collections</param>
		/// <param name="p_dblTotalVoid">Total Voids</param>
		private void CreateRow(DataRow p_objData, double p_dblCurrentX,
			ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
			double[] p_arrColWidth, double p_dblCharWidth, bool p_bLeadingLine, bool p_bCalTotal,
			ref double p_dblTotalAll, ref double p_dblTotalPay, ref double p_dblTotalCollect, ref double p_dblTotalVoid,bool p_bCarrierClaim,ref int p_iCurrCode , CurrencyType eSelectedCurrType)
		{
			string sData = "";
			LocalCache objLocalCache = null;
			string sCheckStatusCode = string.Empty;
			string sCheckStatusDesc = string.Empty;
			bool bPayment = false;
			bool bVoid = false;
			double dblAmount = 0.0;
            double dblIAmount = 0.0;
			double dblSplitAmount = 0.0;
            bool bShowShortCodeOnEntity = false;//JIRA RMA-18410 (RMA-6086) ajohari2

			try
			{
				CurrentX = p_dblCurrentX;
				CurrentY = p_dblCurrentY;
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
				//RMA-6086
                //if (p_iCurrCode == int.MaxValue)
                if(eSelectedCurrType == CurrencyType.PAYMENT_CURRENCY)
                {
                    object oPaymtCurrCode = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT PMT_CURRENCY_CODE FROM FUNDS WHERE TRANS_ID=" + Conversion.ConvertObjToInt(p_objData["TRANS_ID"], m_iClientId));
                    p_iCurrCode = Conversion.ConvertObjToInt(oPaymtCurrCode, m_iClientId);
                }
                //JIRA RMA-16874 (RMA-6086) ajohari2
                else if (eSelectedCurrType == CurrencyType.CLAIM_CURRENCY && p_iCurrCode.Equals(int.MinValue))
                {
                    object oClaimCurrCode = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CLAIM_CURRENCY_CODE FROM FUNDS WHERE TRANS_ID=" + Conversion.ConvertObjToInt(p_objData["TRANS_ID"], m_iClientId));
                    p_iCurrCode = Conversion.ConvertObjToInt(oClaimCurrCode, m_iClientId);
                    bShowShortCodeOnEntity = true;
                }
                //JIRA RMA-16874 (RMA-6086) ajohari2 END
				//RMA-6086
                if (p_bCarrierClaim)
                {
                    for (int iIndexJ = 0; iIndexJ < 20; iIndexJ++)
				{
					sData = " ";
					switch(iIndexJ)
					{
						case 0:
							if (p_bLeadingLine)
								sData = Conversion.ConvertObjToStr(p_objData["CTL_NUMBER"]) ;
							break;
						case 1:
							if (p_bLeadingLine)
								sData = Conversion.ConvertObjToStr(p_objData["TRANS_NUMBER"]);
							break;
						case 2:
							if (p_bLeadingLine)
								sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["TRANS_DATE"]), "d");
							break;
						case 3:
                            bPayment = Common.Conversion.ConvertObjToBool(p_objData["PAYMENT_FLAG"], m_iClientId);
							if (p_bLeadingLine)
								sData = bPayment ? "Payment" : "Collection";
							break;
						case 4:
                            if (p_bLeadingLine)
                            {
                                //Ankit Start : Worked on MITS - 33629
                                //sData = Common.Conversion.ConvertObjToBool(p_objData["CLEARED_FLAG"]) ? "Yes" : "No" ;
                                bVoid = Convert.ToBoolean(p_objData["CLEARED_FLAG"]);
                                sData = string.Empty;
                                if (bVoid)
                                {
                                    if (p_objData["VOID_DATE"] != null && p_objData["VOID_DATE"] != "")
                                        sData = Conversion.GetDBDateFormat(p_objData["VOID_DATE"].ToString(), "d");
                                }
                                else
                                    sData = "No";
                                //Ankit End
                            }
							break;
						case 5:
                            bVoid = Common.Conversion.ConvertObjToBool(p_objData["VOID_FLAG"], m_iClientId);
                            if (p_bLeadingLine)
                            {
                                //Ankit Start : Worked on MITS - 33629
                                //sData = bVoid ? "Yes" : "No" ;
                                sData = string.Empty;
                                if (bVoid)
                                {
                                    if (p_objData["VOID_DATE"] != null && p_objData["VOID_DATE"] != "")
                                        sData = Conversion.GetDBDateFormat(p_objData["VOID_DATE"].ToString(), "d");
                                }
                                else
                                    sData = "No";
                                //Ankit End
                            }
							break;
						case 6:
							if (p_bLeadingLine)
							{
								objLocalCache.GetCodeInfo(Common.Conversion.ConvertObjToInt(p_objData["STATUS_CODE"], m_iClientId), ref sCheckStatusCode, ref sCheckStatusDesc);
								sData = sCheckStatusCode;
							}
							break;
						case 7:
							if (p_bLeadingLine)
							{
								sData = Conversion.ConvertObjToStr(p_objData["FIRST_NAME"]);
								if(sData != "")
									sData += " " + Conversion.ConvertObjToStr(p_objData["LAST_NAME"]);
								else
									sData = Conversion.ConvertObjToStr(p_objData["LAST_NAME"]);
							}
							break;
						case 8:
							dblAmount = Common.Conversion.ConvertObjToDouble(p_objData["FAMT"], m_iClientId);
							if(!bPayment)
								dblAmount = -dblAmount;
                            if (p_bLeadingLine)
                            {
                                sData = FetchDataAsPerCurrency(p_iCurrCode, dblAmount, eSelectedCurrType, bShowShortCodeOnEntity); // 6086
                            }
							break;
						case 9:
							sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["FROM_DATE"]), "d");
							if (Conversion.ConvertObjToStr(p_objData["TO_DATE"]) != "")
								sData = sData + " - " + Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["TO_DATE"]), "d");
							break;
						case 10:
							sData = Conversion.ConvertObjToStr(p_objData["INVOICE_NUMBER"]);
							break;
						case 11:
							sData = Conversion.ConvertObjToStr(p_objData["CODE_DESC"]);
							break;
						case 12:
                            dblSplitAmount = Common.Conversion.ConvertObjToDouble(p_objData["FTSAMT"], m_iClientId);
                            if (!bPayment)
                                dblSplitAmount = -dblSplitAmount;
                            sData = FetchDataAsPerCurrency(p_iCurrCode, dblSplitAmount, eSelectedCurrType,bShowShortCodeOnEntity); // 6086
                            
                             
							break;
						case 13:
							if (p_bLeadingLine)
								sData = Conversion.ConvertObjToStr(p_objData["ADDED_BY_USER"]);
							break;
						case 14:
							if (p_bLeadingLine)
								sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["DATE_OF_CHECK"]), "d");
							break;
                            case 15:
                            if (p_bLeadingLine)
                                sData = Conversion.ConvertObjToBool(p_objData["COMBINED_PAY_FLAG"], m_iClientId).ToString();
                                break;
                            case 16:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["POLICY_NAME"]);
                                break;
                            case 17:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["COVERAGETYPE"]);
                                break;
                            case 18:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["UNITTYPE"]);
                                break;
                            case 19:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["LOSS_CODE"]);
                                break;
                            case 21:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["RES_TYPE_SHORT_CODE"] + " " + p_objData["RES_TYPE_CODE_DESC"]);
                                break;
					}

					if (sData == " ")
						if(!p_bLeadingLine)
							if(p_arrColAlign[iIndexJ] == LEFT_ALIGN)
								sData = "\"" ;
							else
								sData = "\"" + "   " ;

					// In case of null data
					if (sData == "")
						sData = " ";

					while(GetTextWidth(sData) > p_arrColWidth[iIndexJ])
						sData = sData.Substring(0 ,sData.Length - 1);

					// Print using alignment
					if(p_arrColAlign[iIndexJ] == LEFT_ALIGN)
					{
						CurrentX = p_dblCurrentX;
						PrintTextNoCr(" " + sData);
					}
					else
					{
						CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ] 
							- GetTextWidth(sData) - p_dblCharWidth ;
						PrintTextNoCr(sData);
					}
					p_dblCurrentX += p_arrColWidth[iIndexJ];
                    }
                }
                else
                {
                    for (int iIndexJ = 0; iIndexJ < 15; iIndexJ++)
                    {
                        sData = " ";
                        switch (iIndexJ)
                        {
                            case 0:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["CTL_NUMBER"]);
                                break;
                            case 1:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["TRANS_NUMBER"]);
                                break;
                            case 2:
                                if (p_bLeadingLine)
                                    sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["TRANS_DATE"]), "d");
                                break;
                            case 3:
                                bPayment = Common.Conversion.ConvertObjToBool(p_objData["PAYMENT_FLAG"], m_iClientId);
                                if (p_bLeadingLine)
                                    sData = bPayment ? "Payment" : "Collection";
                                break;
                            case 4:
                                if (p_bLeadingLine)
                                {
                                    //Ankit Start : Worked on MITS - 33629
                                    //sData = Common.Conversion.ConvertObjToBool(p_objData["CLEARED_FLAG"]) ? "Yes" : "No" ;
                                    // akaushik5 Changed for MITS 37880/ RMA-8606 Starts
                                    //bVoid = Convert.ToBoolean(p_objData["CLEARED_FLAG"]);
                                    bVoid = Conversion.ConvertObjToBool(p_objData["CLEARED_FLAG"], m_iClientId);
                                    // akaushik5 Changed for MITS 37880/ RMA-8606 Ends
                                    sData = string.Empty;
                                    if (bVoid)
                                    {
                                        if (p_objData["VOID_DATE"] != null && p_objData["VOID_DATE"] != "")
                                            sData = Conversion.GetDBDateFormat(p_objData["VOID_DATE"].ToString(), "d");
                                    }
                                    else
                                        sData = "No";
                                    //Ankit End
                                }
                                break;
                            case 5:
                                bVoid = Common.Conversion.ConvertObjToBool(p_objData["VOID_FLAG"], m_iClientId);
                                if (p_bLeadingLine)
                                {
                                    //Ankit Start : Worked on MITS - 33629
                                    //sData = bVoid ? "Yes" : "No" ;
                                    sData = string.Empty;
                                    if (bVoid)
                                    {
                                        if (p_objData["VOID_DATE"] != null && p_objData["VOID_DATE"] != "")
                                            sData = Conversion.GetDBDateFormat(p_objData["VOID_DATE"].ToString(), "d");
                                    }
                                    else
                                        sData = "No";
                                    //Ankit End
                                }
                                break;
                            case 6:
                                if (p_bLeadingLine)
                                {
                                    objLocalCache.GetCodeInfo(Common.Conversion.ConvertObjToInt(p_objData["STATUS_CODE"], m_iClientId), ref sCheckStatusCode, ref sCheckStatusDesc);
                                    sData = sCheckStatusCode;
                                }
                                break;
                            case 7:
                                if (p_bLeadingLine)
                                {
                                    sData = Conversion.ConvertObjToStr(p_objData["FIRST_NAME"]);
                                    if (sData != "")
                                        sData += " " + Conversion.ConvertObjToStr(p_objData["LAST_NAME"]);
                                    else
                                        sData = Conversion.ConvertObjToStr(p_objData["LAST_NAME"]);
                                }
                                break;
                            case 8:
                                dblAmount = Common.Conversion.ConvertObjToDouble(p_objData["FAMT"], m_iClientId);
                                if (!bPayment)
                                    dblAmount = -dblAmount;

                                if (p_bLeadingLine)
                                {
                                    sData = FetchDataAsPerCurrency(p_iCurrCode, dblAmount, eSelectedCurrType, bShowShortCodeOnEntity);
                                }
                                break;
                            case 9:
                                sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["FROM_DATE"]), "d");
                                if (Conversion.ConvertObjToStr(p_objData["TO_DATE"]) != "")
                                    sData = sData + " - " + Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["TO_DATE"]), "d");
                                break;
                            case 10:
                                sData = Conversion.ConvertObjToStr(p_objData["INVOICE_NUMBER"]);
                                break;
                            case 11:
                                sData = Conversion.ConvertObjToStr(p_objData["CODE_DESC"]);
                                break;
                            case 12:
                                dblSplitAmount = Common.Conversion.ConvertObjToDouble(p_objData["FTSAMT"], m_iClientId);
                                if (!bPayment)
                                    dblSplitAmount = -dblSplitAmount;
                                sData = FetchDataAsPerCurrency(p_iCurrCode, dblSplitAmount, eSelectedCurrType, bShowShortCodeOnEntity);
                                break;
                            case 13:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["ADDED_BY_USER"]);
                                break;
                            case 14:
                                if (p_bLeadingLine)
                                    sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["DATE_OF_CHECK"]), "d");
                                break;
                            case 15:
                                if (p_bLeadingLine)
                                    sData = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(p_objData["COMBINED_PAY_FLAG"]), "d");
                                break;
                            case 16:
                               dblIAmount = Common.Conversion.ConvertObjToDouble(p_objData["FIAMT"], m_iClientId);
                               sData = FetchDataAsPerCurrency(p_iCurrCode, dblIAmount, eSelectedCurrType, bShowShortCodeOnEntity);
                                break;
                            case 17:
                                if (p_bLeadingLine)
                                    sData = Conversion.ConvertObjToStr(p_objData["RES_TYPE_SHORT_CODE"] + " " + p_objData["RES_TYPE_CODE_DESC"]);
                                break;
                          
                        }

                        if (sData == " ")
                            if (!p_bLeadingLine)
                                if (p_arrColAlign[iIndexJ] == LEFT_ALIGN)
                                    sData = "\"";
                                else
                                    sData = "\"" + "   ";

                        // In case of null data
                        if (sData == "")
                            sData = " ";

                        while (GetTextWidth(sData) > p_arrColWidth[iIndexJ])
                            sData = sData.Substring(0, sData.Length - 1);

                        // Print using alignment
                        if (p_arrColAlign[iIndexJ] == LEFT_ALIGN)
                        {
                            CurrentX = p_dblCurrentX;
                            PrintTextNoCr(" " + sData);
                        }
                        else
                        {
                            CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ]
                                - GetTextWidth(sData) - p_dblCharWidth;
                            PrintTextNoCr(sData);
                        }
                        p_dblCurrentX += p_arrColWidth[iIndexJ];
                    }
				}

				if (p_bCalTotal)
				{
                    //pmittal5 MITS:9777 04/07/08
					//p_dblTotalAll = p_dblTotalAll + dblAmount;
                    if (bVoid)
                        p_dblTotalVoid = p_dblTotalVoid + dblAmount;
                    else                      
                    {
                        if (bPayment)
                            p_dblTotalPay = p_dblTotalPay + dblAmount;
                        else
                            //pmittal5 MITS:9777 04/07/08
                            //p_dblTotalCollect = p_dblTotalCollect - dblAmount;
                            p_dblTotalCollect = p_dblTotalCollect + dblAmount;
                    }
				}
				p_dblCurrentX = p_dblLeftMargin;
				p_dblCurrentY += p_dblLineHeight;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.CreateRow.Error", m_iClientId), p_objException);
			}
            //abisht MITS 11394 added finally to dispose objlocalcache object.
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
		}

        //JIRA RMA-18410 (RMA-6086) ajohari2
        private string FetchDataAsPerCurrency(int p_iCurrCode, double dblAmount, CurrencyType p_eSelectedCurrType)
        {
            return FetchDataAsPerCurrency(p_iCurrCode, dblAmount, p_eSelectedCurrType, false);
        }
        //JIRA RMA-18410 (RMA-6086) ajohari2 END
        /// <summary>
        /// Author : Raman Bhatia
        /// Payment Currency would be displayed with short code (USD , CAD etc) so that we can distinguish between
        /// currencies with similar symbol like USD , CAD
        /// </summary>
        /// <param name="p_iCurrCode"></param>
        /// <param name="dblSplitAmount"></param>
        /// <returns></returns>
        private string FetchDataAsPerCurrency (int p_iCurrCode, double dblAmount , CurrencyType p_eSelectedCurrType,bool bShowShortCode)
        {
            if (p_eSelectedCurrType == CurrencyType.PAYMENT_CURRENCY)
            {
                return string.Format("{0:N}", CommonFunctions.LongConvertByCurrencyCode(p_iCurrCode, dblAmount, m_sConnectionString, m_iClientId));//RMA-6086
            }
            else
            {
                //JIRA RMA-16874 (RMA-6086) ajohari2
                if ((p_iCurrCode.Equals(0) || bShowShortCode) && p_eSelectedCurrType == CurrencyType.CLAIM_CURRENCY)
                {
                    return string.Format("{0:N}", CommonFunctions.LongConvertByCurrencyCode(p_iCurrCode, dblAmount, m_sConnectionString, m_iClientId));
                }
                else
                {
                    return string.Format("{0:C}", CommonFunctions.ConvertByCurrencyCode(p_iCurrCode, dblAmount, m_sConnectionString, m_iClientId));//RMA-6086
                }
                //JIRA RMA-16874 (RMA-6086) ajohari2 END
            }
        }
		/// Name		: StartDoc
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_bIsLandscape">Landscape or Portrait</param>
		/// <param name="p_Left">Left</param>
		/// <param name="p_Right">Right</param>
		/// <param name="p_Top">Top</param>
		/// <param name="p_Bottom">Bottom</param>
		private void StartDoc(bool p_bIsLandscape, int p_Left, int p_Right, int p_Top, int p_Bottom)
		{		
			try
			{
				m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape ;
				m_objPrintDoc.PageSettings.Margins.Left = p_Left;
				m_objPrintDoc.PageSettings.Margins.Right = p_Right;
				m_objPrintDoc.PageSettings.Margins.Top = p_Top;
				m_objPrintDoc.PageSettings.Margins.Bottom = p_Bottom;

				m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;

				m_objPrintDoc.StartDoc();

				// Causes access to default printer     objRect = m_objPrintDoc.PageSettings.Bounds;
				m_dblPageHeight = 11 * 1440;
				m_dblPageWidth = 8.5 * 1440;
				if(p_bIsLandscape)
					m_dblPageHeight += 700 ;
				else
					m_dblPageWidth += 700 ;
			
				m_objFont = new Font("Arial", 7);		
				m_objText = new RenderText(m_objPrintDoc);			
				m_objText.Style.Font = m_objFont ;
				m_objText.Style.WordWrap = false ;
				m_objText.AutoWidth = true ;				
			}
			catch( RMAppException p_objEx )
			{								
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.StartDoc.Error", m_iClientId), p_objEx);				
			}				
		}

		/// Name		: GetTextHeight
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Text Height
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Height</returns>
		private double GetTextHeight( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundHeight );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.GetTextHeight.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: SetFont
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_sFontName">Font Name</param>
		/// <param name="p_dblFontSize">Font Size</param>
		private void SetFont( string p_sFontName , double p_dblFontSize )
		{
			try
			{
				m_objFont = new Font( p_sFontName , (float)p_dblFontSize );
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: SetFontBold
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// Set font bold
		/// </summary>
		/// <param name="p_bBold">Bool flag</param>
		private void SetFontBold( bool p_bBold )
		{		
			try
			{
				if( p_bBold )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold );
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font = m_objFont ;

			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.SetFontBold.Error", m_iClientId), p_objEx);				
			}
 
		}

		/// Name		: PrintText
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintText( string p_sPrintText )
		{
			try
			{
				m_objText.Text = p_sPrintText + "\r" ;	
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect( m_dblCurrentX , m_dblCurrentY , m_objText );			
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.PrintText.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: PrintTextNoCr
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintTextNoCr(string p_sPrintText)
		{
			try
			{
				m_objText.Text = p_sPrintText + "";			
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);						
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.PrintTextNoCr.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: Save
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		private void Save(string p_sPath)
		{
			try
			{
				m_objPrintDoc.ExportToPDF(p_sPath ,false );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.Save.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: EndDoc
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Call the end event.
		/// </summary>
		private void EndDoc()
		{
			try
			{
				m_objPrintDoc.EndDoc();
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.EndDoc.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: CreateNewPage
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a new page in a report
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		/// <param name="p_bFirstPage">First page or not</param>
		/// <param name="p_sSubTitle">SubTitle for the page</param>
		public void CreateNewPage(string p_sDSN, bool p_bFirstPage, string p_sSubTitle)
		{
			SysParms objSysSetting = null;
		
			RenderText objText = null;
			try
			{
				if (!p_bFirstPage)
				{
					m_objPrintDoc.NewPage();
				}

				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 11700);
				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 500, Color.Black, 1, Color.LightGray);
				m_objPrintDoc.RenderDirectRectangle(0, 11300, 15600, 11900, Color.Black, 1, Color.LightGray);
				objText = new RenderText(m_objPrintDoc);	
				objText.Text = " ";
				objText.AutoWidth = true;
				CurrentX = objText.BoundWidth;
//rsharma220 MITS 31131
                objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.CreateReport.Caption", m_iClientId)) + " " + p_sSubTitle;
				m_objPrintDoc.RenderDirectText(CurrentX, 100, objText.Text, objText.Width, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left);
				objText = new RenderText(m_objPrintDoc);

                objSysSetting = new SysParms(p_sDSN, m_iClientId);
                //nadim for 14476..
                //objText.Text = objSysSetting.SysSettings.ClientName + "  " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "  ";
                objText.Text = objSysSetting.SysSettings.ClientName + " " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") ;
                //nadim for 14476
				objSysSetting = null;
				objText.AutoWidth = true;
				CurrentX = (ClientWidth - objText.BoundWidth);

				m_objPrintDoc.RenderDirectText(CurrentX, 100, objText.Text, objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Right);
				objText = new RenderText(m_objPrintDoc);
//rsharma220 MITS 31131
                objText.Text = CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.CreateReport.Page", m_iClientId)) + " [@@PageNo@@]";
				CurrentX = (PageWidth - objText.BoundWidth)/2;
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, objText.Text, objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center);

				//objText.Text = Globalization.GetString("PaymentHistory.CreateReport.ConfidentialData") + " - " + RMConfigurator.Text("message", "id", "DEF_RMCAPTION") + "  ";
         ////rsharma220 MITS 31131
                objText.Text = String.Format("{0} - {1}  ", CommonFunctions.FilterBusinessMessage(Globalization.GetString("PaymentHistory.CreateReport.ConfidentialData", m_iClientId)), m_MessageSettings["DEF_RMCAPTION"]);

				CurrentX = (ClientWidth - objText.BoundWidth);
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, objText.Text, objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold|FontStyle.Italic), Color.Black, AlignHorzEnum.Right);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.CreateNewPage.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: CreateFooter
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates Report Footer
		/// </summary>
		/// <param name="p_dCurrentX">X Position</param>
		/// <param name="p_dCurrentY">Y Position</param>
		/// <param name="p_dHeight">Height</param>
		/// <param name="p_dblSpace">Space between 2 words</param>
		private void CreateFooter(double p_dCurrentX, double p_dCurrentY, double p_dHeight, double p_dblSpace)
		{
			try
			{
				m_objText.Style.Font=new Font("Arial", 7f, FontStyle.Underline|FontStyle.Bold);
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_NOTE");
                m_objText.Text = m_MessageSettings["DEF_NOTE"].ToString();
				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objPrintDoc.RenderDirect(p_dCurrentX ,p_dCurrentY ,m_objText);

				m_objText.Style.Font = new Font("Arial", 7f, FontStyle.Bold);
				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMPROPRIETRY");
                m_objText.Text = m_MessageSettings["DEF_RMPROPRIETARY"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_NAME");
                m_objText.Text = m_MessageSettings["DEF_NAME"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMCOPYRIGHT");
                m_objText.Text = m_MessageSettings["DEF_RMCOPYRIGHT"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMRIGHTSRESERVED");
                m_objText.Text = m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.CreateFooter.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: GetTextWidth
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get text width.
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Width</returns>
		private double GetTextWidth( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundWidth );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentHistory.GetTextWidth.Error", m_iClientId), p_objEx);				
			}
		}

		#endregion
	}
}
