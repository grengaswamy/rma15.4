using System;
using System.Collections;
using C1.C1Report;
using Riskmaster.Common;
using System.IO;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.FundManagement
{
	/// <summary>	
	/// Class contains the constants for identifying the type of data that would be printed on the report.
	///</summary>
	internal class GlobalTransDetConstant
	{
		internal const int MONEY = 1;
		internal const int CHARSTRING = 2;
		internal const int DATESTRING = 3;
		internal const int NUMERIC = 4;
		internal const int YESNO = 5;
		internal const int TIMESTRING = 7;	
	}

	
	/// <summary>	
	/// This structure would contain data to be displayed on the report.
	/// Along with the data other information like data type, font style etc is also stored in this structure.
	///</summary>
	internal struct ReportData
	{
		private string sReportColData;
		private int iReportColDataType;
		private bool bSetBold;
		private bool bRightAlign;
		internal string Data{get{return sReportColData;}set{sReportColData = value;} }
		internal int DataType{get{return iReportColDataType;}set{iReportColDataType = value;} }
		internal bool SetBold{get{return bSetBold;}set{bSetBold = value;} }
		internal bool RightAlign{get{return bRightAlign;}set{bRightAlign = value;} }
	}
	/// <summary>	
	/// Author  :  Neelima Dabral
	/// Date    :  05 October 2004
	/// Purpose :  Riskmaster.Application.FundManagement.PrintPDF class contains the methods 
	///			   that would generate the Transaction Summary Reoprt into a PDF Format.
	/// </summary>
	internal class PrintPDF
	{
		/// <summary>		
		/// C1Report object.
		/// </summary>
		private C1Report m_objTransSummReport = null;
		/// <summary>		
		/// Report Layout.
		/// </summary>
		private Layout	 m_objReportLayout=null;
		/// <summary>		
		/// Report Section (Detail)
		/// </summary>
		private Section	 m_objReportDetail=null;
		/// <summary>		
		/// Report Section (Header)
		/// </summary>
		private Section	 m_objReportPageHeader=null;
		/// <summary>		
		/// Report Section (Footer)
		/// </summary>
		private Section	 m_objReportPageFooter=null;
		/// <summary>		
		/// This variable will contain the current number of fields in the report.
		/// </summary>
		private Double	 m_dblNoOfFields=0;
		/// <summary>		
		/// This variable will contain the number of lines that needs to be displayed in a report page.
		/// </summary>
		private int		 m_iTotalNoLinesInPage =40;
		/// <summary>		
		/// This variable will contain the value of current line number on the current page.
		/// </summary>
		private int		 m_iCurrentLine=0;
		/// <summary>		
		/// This variable will contain X-Coordinate value where the data would be printed on the report.
		/// </summary>
		private int		 m_iCurrentXPos=0;
		/// <summary>		
		/// The structure contains the data that would be printed.
		/// </summary>
		private ReportData  m_structReportData;
		/// <summary>		
		/// This variable will be used to calculate the Y-Coordinate  value where the data would be printed on the report.
		/// </summary>
		private double   m_dblCurrentY=0;
		/// <summary>		
		/// The variable contains value for the line spacing between two lines in a report.
		/// </summary>
		private int		 m_iLineSpace=0;
		/// <summary>		
		/// The variable identifies the first page of the report.
		/// </summary>
		private bool	 m_bFirstPage=false;
        //Deb
        /// <summary>
        /// The variable identifies the connection string of the report.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        //Deb

        private int m_iClientId = 0;
		/// <summary>
		/// It would initialize the objects needed for printing the PDF Report.
		/// </summary>
		internal PrintPDF(int p_iClientId)
		{
			try
			{
				m_objTransSummReport = new C1Report();				
				m_objTransSummReport.Clear();					
				m_objReportLayout = m_objTransSummReport.Layout;
				m_objReportLayout.Orientation = OrientationEnum.Portrait;
				m_objReportLayout.MarginLeft = 620;						
				m_objReportLayout.Width = 11000;										
				m_objReportDetail= m_objTransSummReport.Sections[SectionTypeEnum.Detail];
				m_objReportDetail.Visible=true;
				m_bFirstPage = true;
                m_iClientId = p_iClientId;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintPDF.ErrorCreate",m_iClientId),p_objException);
			}	
		}
        //Deb
        /// <summary>
        /// This method would set the coonection string for the report.		
        /// </summary>
        /// <param name="p_sConnectionstring">Connection String</param>		
        internal void SetConnectionString(string p_sConnectionstring)
        {
            m_sConnectionString = p_sConnectionstring;
        }
        //Deb
		/// <summary>
		/// This method would set the line spacing for the report.		
		/// </summary>
		/// <param name="p_iLineSpace">Line Space</param>		
		internal void SetLineSpacing(int p_iLineSpace)
		{
			try
			{
				m_iLineSpace = p_iLineSpace;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.SetLineSpacing.ErrorSet",m_iClientId),p_objException);
			}	
		}
		/// <summary>
		/// This method would set the font for the report.		
		/// </summary>
		/// <param name="p_sFont">Font</param>	
		internal void SetFont(string p_sFont)
		{
			try
			{			
				m_objTransSummReport.Font.Name = p_sFont;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.SetFont.ErrorSet",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would set the font size for the report.		
		/// </summary>
		/// <param name="p_fFontSize">Font size</param>	
		internal void SetFontSize(float p_fFontSize)
		{
			try
			{
				m_objTransSummReport.Font.Size=p_fFontSize;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.SetFontSize.ErrorSet",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would set the font style as Bold/Normal for the report.		
		/// </summary>
		/// <param name="p_bFontBold">True/False</param>	
		internal void SetFontBold(bool p_bFontBold)
		{
			try
			{
				m_objTransSummReport.Font.Bold=p_bFontBold;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.SetFontBold.ErrorSet",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would set the font style as Italic/Normal for the report.		
		/// </summary>
		/// <param name="p_bFontItalic">True/False</param>	
		internal void SetFontItalic(bool p_bFontItalic)
		{
			try
			{
				m_objTransSummReport.Font.Italic=p_bFontItalic;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.SetFontItalic.ErrorSet",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would set the font style as Underline/Normal for the report.		
		/// </summary>
		/// <param name="p_bFontUnderline">True/False</param>	
		internal void SetFontUnderline(bool p_bFontUnderline)
		{
			try
			{
				m_objTransSummReport.Font.Underline=p_bFontUnderline;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.SetFontUnderline.ErrorSet",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// This method would call methods to print the header and footer for the report.		
		/// </summary>
		/// <param name="p_sReportTitle">Report Title</param>	
		/// <param name="p_sReportFooter">Report Footer</param>	
		/// <param name="p_sProductName">Product Name</param>	
		/// <param name="p_sCompanyName">Company Name</param>	
		/// <param name="p_sClaimHeader">Claimant Name</param>	
		internal void PrintOutline(string p_sReportTitle,string p_sReportFooter,string p_sProductName,string p_sCompanyName,string p_sClaimHeader)
		{
			string sHeader="";		
			string sFooter2="";
			try
			{
				
				sHeader = p_sReportTitle + " Transaction Summary";
				sFooter2="Confidential Data - " + p_sProductName;				
				SetFontSize(10);
				SetFontBold(true);
				PrintPageHeader(sHeader,p_sCompanyName,p_sClaimHeader);
				
				SetFontItalic(true);
				PrintPageFooter(p_sReportFooter,sFooter2);
				SetFontItalic(false);
				SetFontSize(10);				
				SetFontBold(false);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintOutline.ErrorPrint",m_iClientId),p_objException);
			}			
		}
		/// <summary>
		/// This method would print the header for the report.		
		/// </summary>
		/// <param name="p_sHeader">Report Title</param>			
		/// <param name="p_sCompanyName">Company Name</param>	
		/// <param name="p_sClaimHeader">Claimant Name</param>	
		internal void PrintPageHeader(string p_sHeader,string p_sCompanyName,string p_sClaimHeader)
		{
			Field objField = null;
			double dblColPos=0;
			try
			{
				m_objReportPageHeader = m_objTransSummReport.Sections[SectionTypeEnum.PageHeader];
				m_objReportPageHeader.Visible=true;				
				
				objField = m_objReportPageHeader.Fields.Add("Rect", "", 0,0, m_objReportLayout.Width, 1200);				
				objField.BorderStyle = BorderStyleEnum.Solid;		

				objField =m_objReportPageHeader.Fields.Add("FldHeader0" ,"Transaction Summary",40,50,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				dblColPos = dblColPos +objField.Width;
				objField.CanGrow = true;							

				objField =m_objReportPageHeader.Fields.Add("FldHeader1" ,p_sCompanyName,(m_objReportLayout.Width)/2-1000,50,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				dblColPos = dblColPos +objField.Width;
				objField.CanGrow = true;
								
				objField =m_objReportPageHeader.Fields.Add("FldHeader2" ,System.DateTime.Now.ToString("d"),(m_objReportLayout.Width - 1500),50,0,300);				
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;

				objField = m_objReportPageHeader.Fields.Add("FldLine2", "", 0,400, m_objReportLayout.Width, 20);
				objField.LineSlant = LineSlantEnum.NoSlant;
				objField.LineWidth = 10;
				objField.BorderStyle = BorderStyleEnum.Solid;

				if (p_sClaimHeader.Trim() != "")
				{
					objField =m_objReportPageHeader.Fields.Add("FldHeader4" ,p_sClaimHeader,100,500,0,300);
					objField.Width=m_objReportLayout.Width - objField.Left;				
					objField.CanGrow = true;
				}
				SetLineSpacing(900);
				objField =m_objReportPageHeader.Fields.Add("FldHeader3" ,p_sHeader,(m_objReportLayout.Width/2)-2000,m_iLineSpace,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;
				SetLineSpacing(400);		
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintPageHeader.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objField = null;
			}

		}
		/// <summary>
		/// This method would print the footer for the report.		
		/// </summary>
		/// <param name="p_sFooterLeft">Left Footer String</param>			
		/// <param name="p_sFooterRight">Right Footer String</param>			
		internal void PrintPageFooter(string p_sFooterLeft,string p_sFooterRight)
		{
			Field objField = null;
			try
			{
				m_objReportPageFooter = m_objTransSummReport.Sections[SectionTypeEnum.PageFooter];
				m_objReportPageFooter.Visible=true;


				objField = m_objReportPageFooter.Fields.Add("Rect2", "", 0,0, m_objReportLayout.Width, 500);				
				objField.BorderStyle = BorderStyleEnum.Solid;

				objField = m_objReportPageFooter.Fields.Add("FldFooter0", "", 0,0, m_objReportLayout.Width, 20);
				objField.LineSlant = LineSlantEnum.NoSlant;
				objField.LineWidth = 10;
				objField.BorderStyle = BorderStyleEnum.Solid;				

				objField =m_objReportPageFooter.Fields.Add("FldFooter1",p_sFooterLeft,0,50,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;
				
				objField = m_objReportPageFooter.Fields.Add("FldFooter2", @"""Page "" & Page ", 4000, 50, 4000, 300);
				objField.Width=m_objReportLayout.Width - objField.Left;	
				objField.CanGrow = true;
				objField.Calculated = true;

				objField =m_objReportPageFooter.Fields.Add("FldFooter3",p_sFooterRight,7800,50,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;

				SetLineSpacing(400);			
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintPageFooter.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objField=null;
			}
		}
		/// <summary>
		/// This method would print the sub-headings in the report.		
		/// </summary>
		/// <param name="p_sHeading">Heading to be printed</param>			
		/// <param name="p_bFontBold">Bold (True/False)</param>
		/// <param name="p_fFontSize">Font size</param>
		/// <param name="p_bFontUnderline">Underline (True/False)</param>
		internal void PrintSubHeading(string p_sHeading,bool p_bFontBold,float p_fFontSize,bool p_bFontUnderline)
		{
			FieldCollection objFieldColl=null;
			try
			{
				objFieldColl = m_objTransSummReport.Fields;

				if ((m_iCurrentLine == m_iTotalNoLinesInPage))
				{
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;					
					objFieldColl.Add("Rect4" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect4" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect4" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect4" + m_dblNoOfFields].Visible  = true;
				}
				SetFontUnderline(p_bFontUnderline);
				SetFontBold(p_bFontBold);
				SetFontSize(p_fFontSize);
				
				objFieldColl.Add("Fld" + m_dblNoOfFields, p_sHeading ,100,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);			
				objFieldColl["Fld" + m_dblNoOfFields].Width = m_objReportLayout.Width -objFieldColl["Fld" + m_dblNoOfFields].Left;				
				objFieldColl["Fld" + m_dblNoOfFields].CanGrow = true;				
				objFieldColl["Fld" + m_dblNoOfFields].Visible = true;	
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;
				objFieldColl["Fld" + m_dblNoOfFields].Align = FieldAlignEnum.JustTop;				
				
				
				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;

				SetFontBold(false);				
				SetFontUnderline(false);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintSubHeading.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objFieldColl = null;
			}
		}
		/// <summary>
		/// This method would print a string in the center of the report.		
		/// </summary>
		/// <param name="p_sHeading">Heading to be printed</param>			
		/// <param name="p_bFontBold">Bold (True/False)</param>
		/// <param name="p_fFontSize">Font size</param>
		/// <param name="p_bFontUnderline">Underline (True/False)</param>
		internal void PrintCenter(string p_sHeading,bool p_bFontBold,float p_fFontSize,bool p_bFontUnderline)
		{
			FieldCollection objFieldColl=null;
			try
			{
				objFieldColl = m_objTransSummReport.Fields;

				if ((m_iCurrentLine == m_iTotalNoLinesInPage))
				{
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;	
					objFieldColl.Add("Rect5" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect5" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect5" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect5" + m_dblNoOfFields].Visible  = true;
				}
				SetFontUnderline(p_bFontUnderline);
				SetFontBold(p_bFontBold);
				SetFontSize(p_fFontSize);
				if(p_sHeading.Length < 25)
					objFieldColl.Add("Fld" + m_dblNoOfFields, p_sHeading ,(m_objReportLayout.Width/2)-1000,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);			
				else
					objFieldColl.Add("Fld" + m_dblNoOfFields, p_sHeading ,(m_objReportLayout.Width/2)-1500,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);			
				objFieldColl["Fld" + m_dblNoOfFields].Width = m_objReportLayout.Width -objFieldColl["Fld" + m_dblNoOfFields].Left;				
				objFieldColl["Fld" + m_dblNoOfFields].CanGrow = true;				
				objFieldColl["Fld" + m_dblNoOfFields].Visible = true;	
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;
				objFieldColl["Fld" + m_dblNoOfFields].Align = FieldAlignEnum.JustTop;				
				
				
				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;

				SetFontBold(false);				
				SetFontUnderline(false);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintCenter.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objFieldColl=null;
			}
		}
		/// <summary>
		/// This method would render the PDF report to a specified file.
		/// </summary>
		/// <param name="p_sReportFile">Filename</param>					
		internal void RenderToFile(string p_sReportFile)
		{
			try
			{
				m_objTransSummReport.RenderToFile(p_sReportFile,FileFormatEnum.PDF);
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.RenderToFile.ErrorPrint",m_iClientId),p_objException);
			}			
		}
		/// <summary>
		/// This method would render the PDF report to memory stream.
		/// </summary>
		/// <returns>Memory Stream</returns>
		internal MemoryStream RenderToStream()
		{
			MemoryStream objPDFMemoryStream=null;
			try
			{
				objPDFMemoryStream=new MemoryStream();
				m_objTransSummReport.RenderToStream(objPDFMemoryStream,FileFormatEnum.PDF);
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.RenderToStream.ErrorPrint",m_iClientId),p_objException);
			}			
			return objPDFMemoryStream;
		}
		/// <summary>
		/// This method would print double lines in the report.
		/// </summary>		
		internal void PrintDoubleLine()
		{
			FieldCollection objFieldColl=null;
			bool bPageBreak=false;	
			try
			{
				objFieldColl = m_objTransSummReport.Fields;
				if ((m_iCurrentLine == m_iTotalNoLinesInPage) && !(bPageBreak))
				{
					bPageBreak = true;
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;		
					objFieldColl.Add("Rect10" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect10" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect10" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect10" + m_dblNoOfFields].Visible  = true;	
				}
				objFieldColl.Add("Fld" + m_dblNoOfFields, "", 0,m_dblCurrentY * m_iLineSpace, m_objReportLayout.Width, 50);
				objFieldColl["Fld" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;			
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;				

				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintLine.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objFieldColl = null;
			}
		}
		/// <summary>
		/// This method would print single line in the report.
		/// </summary>		
		internal void PrintLine()
		{
			FieldCollection objFieldColl=null;
			bool bPageBreak=false;	
			try
			{
				objFieldColl = m_objTransSummReport.Fields;
				if ((m_iCurrentLine == m_iTotalNoLinesInPage) && !(bPageBreak))
				{
					bPageBreak = true;
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;		
					objFieldColl.Add("Rect6" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect6" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect6" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect6" + m_dblNoOfFields].Visible  = true;	
				}
				objFieldColl.Add("Fld" + m_dblNoOfFields, "", 0,m_dblCurrentY * m_iLineSpace, m_objReportLayout.Width, 20);
				objFieldColl["Fld" + m_dblNoOfFields].LineSlant = LineSlantEnum.NoSlant;
				objFieldColl["Fld" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;			
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;

				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintLine.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objFieldColl = null;
			}
		}		
		/// <summary>
		/// This method would print data string in the report.
		/// </summary>
		/// <param name="p_arrlstTabAt">Arraylist specifying the columns starting position</param>
		/// <param name="p_arrlstPrintData">Arraylist containing the data to be printed</param>
		internal void PrintString(ArrayList p_arrlstTabAt,ArrayList p_arrlstPrintData)
		{
			
			FieldCollection  objField=null;						
			string sData ="";
			int   iDataType=0;
			int iCounter=0;
			bool bPageBreak=false;	
			bool bSetBold = false;
			bool bRightAlign=false;
			try
			{
				//SetFontSize(10);				
				SetFontBold(false);
				objField = m_objTransSummReport.Fields;				
				for(iCounter=0;iCounter < p_arrlstPrintData.Count;++iCounter)
				{					
					m_structReportData =(ReportData) p_arrlstPrintData[iCounter];
					sData = m_structReportData.Data;
					iDataType = m_structReportData.DataType;	
					bSetBold = m_structReportData.SetBold;
					bRightAlign=m_structReportData.RightAlign;
					
					if(bSetBold)
						SetFontBold(true);
					else
						SetFontBold(false);	

					m_iCurrentXPos = Common.Conversion.ConvertStrToInteger(p_arrlstTabAt[iCounter].ToString());
					switch(iDataType)
					{
						case GlobalTransDetConstant.CHARSTRING:																			
						case GlobalTransDetConstant.NUMERIC:
							break;
						case GlobalTransDetConstant.DATESTRING:	
							sData = Common.Conversion.GetDBDateFormat(sData,"d");
							break;						
						case GlobalTransDetConstant.MONEY:								
							//Deb
                            if(!string.IsNullOrEmpty(this.m_sConnectionString))
                                sData = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(this.m_sConnectionString), Common.Conversion.ConvertStrToDouble(sData), this.m_sConnectionString, m_iClientId);
                            else
                             sData = String.Format("{0:C}",Common.Conversion.ConvertStrToDouble(sData));
							break;
						case GlobalTransDetConstant.YESNO:								
							sData = (sData == "0" ) ? "No" :"Yes";														
							break;
						case GlobalTransDetConstant.TIMESTRING:												
							if (sData.Trim() != "")
							{
								sData = sData.Substring(0,2) + ":" + sData.Substring(2,2);
								DateTime datDataValue=DateTime.MinValue;
								datDataValue = System.DateTime.Parse(sData);
								sData= datDataValue.ToString("T");
							}
							break;
					}
					if ((m_iCurrentLine == m_iTotalNoLinesInPage) && !(bPageBreak))
					{
						bPageBreak = true;
						objField["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
						m_iCurrentLine = 0;											

						objField.Add("Rect2" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
						objField["Rect2" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
						objField["Rect2" + m_dblNoOfFields].Section = 0;
						objField["Rect2" + m_dblNoOfFields].Visible  = true;	
					}					
					objField.Add("Fld" + m_dblNoOfFields, sData ,m_iCurrentXPos,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);			
					objField["Fld" + m_dblNoOfFields].Width = m_objReportLayout.Width -objField["Fld" + m_dblNoOfFields].Left;				
					objField["Fld" + m_dblNoOfFields].CanGrow = true;									
					objField["Fld" + m_dblNoOfFields].Visible = true;	
					objField["Fld" + m_dblNoOfFields].Section = 0;
					if(bRightAlign)
						objField["Fld" + m_dblNoOfFields].Align = FieldAlignEnum.RightMiddle;
					else
						objField["Fld" + m_dblNoOfFields].Align = FieldAlignEnum.JustTop;					
					
					++m_dblNoOfFields;	
				}	
				++m_iCurrentLine;
				++m_dblCurrentY;				
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintString.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objField = null;
			}			
		}
		/// <summary>
		/// This method will find where the copyright needs to be printed on the report.
		/// </summary>		
		internal void PrintCopyright()
		{
			FieldCollection  objField=null;		
			try
			{
				objField= m_objTransSummReport.Fields;			
				if (m_iCurrentLine + 8 > m_iTotalNoLinesInPage)
				{
					objField["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;											

					objField.Add("Rect9" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objField["Rect9" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objField["Rect9" + m_dblNoOfFields].Section = 0;
					objField["Rect9" + m_dblNoOfFields].Visible  = true;	

					++m_dblNoOfFields;	
					++m_dblCurrentY;
				}	
			}	
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintCopyright.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objField = null;
			}
		}
		/// <summary>
		/// This method would print the outline of the report.
		/// </summary>
		internal void PrintRectangle()
		{
			FieldCollection  objField=null;			
			try
			{
				objField = m_objTransSummReport.Fields;			
				if(m_bFirstPage)
				{
					m_bFirstPage = false;
					objField.Add("Rect3" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objField["Rect3" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objField["Rect3" + m_dblNoOfFields].Section = 0;
					objField["Rect3" + m_dblNoOfFields].Visible  = true;				
				}
			}			
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintRectangle.ErrorPrint",m_iClientId),p_objException);
			}
			finally
			{
				objField = null;

			}
		}						
		
	}		
}
