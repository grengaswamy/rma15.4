﻿using System;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Application.Reserves;
using System.Collections;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.Models;
using System.Xml;

using System.Globalization;     //Declare by Sumit Agarwal for NI on 11/07/2014 to convert the double values properly

namespace Riskmaster.Application.FundManagement
{
    public class DeductibleManager:IDisposable
    {//skhare7 changes done for JIRA 12883
        private string m_sConnectionString = string.Empty;
        private Security.UserLogin m_UserLogin = null;
        private const int GC_DED_EDIT = 192;
        private const int WC_DED_EDIT = 3042;
        private const int GC_AGG_EDIT = 300;
        private const int WC_AGG_EDIT = 5800;
        //Cloud changes by Nikhil for NI
        private int m_iClientId = 0;
     //ddhiman 10/13/2014, SMS Settings for deductible fields
        private const int GC_DED_TYP_EDIT = 220;
        private const int GC_DED_AMT_EDIT = 221;
        private const int WC_DED_TYP_EDIT = 3070;
        private const int WC_DED_AMT_EDIT = 3071;
        //skhare7 changes done for JIRA 12883
        //End ddhiman
        ////jira# RMA-6135.Multicurrency
        private CommonFunctions.NavFormType eNavType = CommonFunctions.NavFormType.None;
        private int iBaseCurrCode = 0;
        private string sBaseCurr = string.Empty;
        ////jira# RMA-6135.Multicurrency

        //Added by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory
        /// Private variable to store the instance of Datamodel factory object.Added by Shivendu for MITS 11594
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        public DeductibleManager(string p_sConnectionString, Security.UserLogin p_UserLogin, int p_iClientId)
        {
            m_sConnectionString = p_sConnectionString;
            m_UserLogin = p_UserLogin;
            //Cloud changes by Nikhil for NI
            m_iClientId = p_iClientId;
            //Added by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory
            ////Cloud changes by Nikhil for NI
            m_objDataModelFactory = new DataModelFactory(p_UserLogin, m_iClientId);
        }
        #region Dispose
        /// <summary>
        /// Un Initialize the data model factory object. 
        /// </summary>
        public void Dispose()
        {
            try
            {
                // m_objDataModelFactory.UnInitialize();
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                
            }
            catch
            {
                // Avoid raising any exception here.				
            }
        }

        #endregion

        /// <summary>
        /// Gets All Deductibles for a given claim
        /// </summary>
        /// <param name="p_iClaimId">The Claim Id for which the deductible is to be retrieved</param>
        /// <param name="p_objXmlOut">Xml Containing list of all deductibles for the claim</param>
        /// <returns>true on success</returns>
        public bool LoadDeductibleForClaim(int p_iClaimId, XmlDocument p_objXmlOut)
        {
            StringBuilder sbSql = null;
            bool bSuccess = false;
            int iClmXPolDedId = 0;
            string sUnit = string.Empty;
            Double dRemainingAmt = 0d;
            Double dReducedAmt = 0d;
            int iCovGroupId = 0;
			//NI-PCR Changes by Nikhil -  Start
            int iPreventEditDedPerEvent = 0;
	        //NI-PCR Changes by Nikhil -  End
            DbReader objReader = null;
            DbConnection objDbConnection = null;
          //Commented by Nikhil. Code review changes.Replaced objFacotry with  m_objDataModelFactory
            
            ClaimXPolDed objClaimXPolDed = null;

            XmlElement objRootNode = null;
            XmlElement objDeductibleNode = null;
            XmlNodeList xCvgTypes = null;
            XmlNodeList xDedTypes = null;
            XmlNodeList xDiminishingTypes = null;
            XmlNodeList xAmts = null;
            XmlNodeList xCovGroupIds = null;
            int iSharedAggDedThirdParty = 0;
            
             
            int iFPTypeCode = 0;

            try
            {
                sbSql = new StringBuilder();
                p_objXmlOut.InnerXml = "<DEDUCTIBLES></DEDUCTIBLES>";
                objRootNode = (XmlElement)p_objXmlOut.SelectSingleNode("//DEDUCTIBLES");
                
                if (DbFactory.IsOracleDatabase(m_sConnectionString))
                {
                    sbSql.Append("SELECT P.POLICY_SYMBOL||'|'||P.POLICY_NUMBER||'|'||P.MODULE AS POLICY_NAME, CVG.COVERAGE_TYPE_CODE, CVG.DED_TYPE_CODE, CVG.COVERAGE_TEXT, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE ");
                }
                else
                {
                    sbSql.Append("SELECT P.POLICY_SYMBOL+'|'+P.POLICY_NUMBER+'|'+P.MODULE AS POLICY_NAME, CVG.COVERAGE_TYPE_CODE, CVG.DED_TYPE_CODE, CVG.COVERAGE_TEXT, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE ");
                }
                sbSql.Append("FROM POLICY_X_CVG_TYPE CVG ");
                sbSql.Append("INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID=PXU.POLICY_UNIT_ROW_ID ");
                sbSql.Append("INNER JOIN POLICY P ON PXU.POLICY_ID=P.POLICY_ID ");
                sbSql.Append("INNER JOIN CLAIM_X_POLICY CXP ON P.POLICY_ID=CXP.POLICY_ID ");
                sbSql.Append("INNER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID=PXU.UNIT_ID AND PUD.UNIT_TYPE=PXU.UNIT_TYPE ");
                sbSql.Append("WHERE CXP.CLAIM_ID=");
                sbSql.Append(p_iClaimId);

                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString()))
                {
                   
                    objClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;

                    iFPTypeCode = objClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");

                    while (objReader.Read())
                    {
                        sbSql.Remove(0, sbSql.Length);
                        sbSql.Append("SELECT CPD.CLM_X_POL_DED_ID ");
                        sbSql.Append("FROM CLAIM_X_POL_DED CPD ");
                        sbSql.Append("WHERE CPD.POLCVG_ROW_ID=");
                        sbSql.Append(objReader["POLCVG_ROW_ID"]);
                        sbSql.Append(" AND CPD.CLAIM_ID=");
                        sbSql.Append(p_iClaimId);

                        using (objDbConnection = DbFactory.GetDbConnection(m_sConnectionString))
                        {
                            objDbConnection.Open();
                            iClmXPolDedId = objDbConnection.ExecuteInt(sbSql.ToString());
                            if (iClmXPolDedId == 0)
                            {
                                continue;
                            }
                        }

                        sUnit = this.GetUnit(objReader["UNIT_ID"].ToString(), objReader["UNIT_TYPE"].ToString());

                        CreateElement(objRootNode, "DEDUCTIBLE", ref objDeductibleNode);

                        objClaimXPolDed.MoveTo(iClmXPolDedId);

                        iCovGroupId = DbFactory.ExecuteAsType<int>(objClaimXPolDed.Context.DbConnLookup.ConnectionString, "SELECT COV_GROUP_CODE FROM POLICY_X_INSLINE_GROUP WHERE POLICY_X_INSLINE_GROUP_ID=" + objClaimXPolDed.PolicyXInslineGroupId);
                        dReducedAmt = objClaimXPolDed.CalculateReducedDeductibleAmount(objClaimXPolDed.ClaimId, objClaimXPolDed.DiminishingTypeCode, objClaimXPolDed.DedTypeCode, objClaimXPolDed.SirDedAmt, objClaimXPolDed.DimPercentNum, objClaimXPolDed.ClmXPolDedId);
                        //Different method for calculating remaining amount when coverage group is to be applied or otherwise
                        if (iCovGroupId == 0 || objClaimXPolDed.DedTypeCode == 0 || objClaimXPolDed.DedTypeCode == iFPTypeCode)
                        {
                            dRemainingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, p_iClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode);
                        }
                        else
                        {
                            //NI-PCR changes by Nikhil for handling deductable check at event level -  Start
                            
                            iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
                            if (iPreventEditDedPerEvent == 0)
                                dRemainingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode, iCovGroupId, false);
                            else
                                dRemainingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, p_iClaimId, iCovGroupId, false);
                            //NI-PCR changes by Nikhil for handling deductable check at event level -  End
                        }

                        CreateAndSetElement(objDeductibleNode, "CLM_X_POL_DED_ID", iClmXPolDedId.ToString());
                        //CreateAndSetElement(objDeductibleNode, "SIR_DEDUCTIBLE", objClaimXPolDed.DedTypeCode.ToString());
                        CreateAndSetElement(objDeductibleNode, "SIR_DEDUCTIBLE", objClaimXPolDed.DedTypeCode == 0 ? objClaimXPolDed.Context.LocalCache.GetCodeId("None", "DEDUCTIBLE_TYPE").ToString() : objClaimXPolDed.DedTypeCode.ToString());
                        CreateAndSetElement(objDeductibleNode, "DIMINISHING_TYPE", objClaimXPolDed.DiminishingTypeCode.ToString());
                        CreateAndSetElement(objDeductibleNode, "AMOUNT", objClaimXPolDed.SirDedAmt.ToString());
                        CreateAndSetElement(objDeductibleNode, "REMAINING_AMT", dRemainingAmt.ToString());
                        CreateAndSetElement(objDeductibleNode, "EXCLUDE_EXPENSE_PAYMENT", objClaimXPolDed.ExcludeExpenseFlag ? "YES" : "NO");
                       CreateAndSetElement(objDeductibleNode, "REDUCED_AMOUNT", (objClaimXPolDed.DiminishingTypeCode == 0 ? "NA" : dReducedAmt.ToString()));
                        CreateAndSetElement(objDeductibleNode, "POLICY_ID", (objClaimXPolDed.PolicyId.ToString()));
                        CreateAndSetElement(objDeductibleNode, "EVENT_ID", objClaimXPolDed.EventId.ToString());
                        CreateAndSetElement(objDeductibleNode, "POLICY", objReader["POLICY_NAME"].ToString());  
                        //To display coverage text on the lines of Funds transaction split and reserve screen.
                        
                        CreateAndSetElement(objDeductibleNode, "COVERAGE_TYPE", objReader["COVERAGE_TEXT"].ToString());
                        CreateAndSetElement(objDeductibleNode, "POLCVG_ROW_ID", objReader["POLCVG_ROW_ID"].ToString());
                        CreateAndSetElement(objDeductibleNode, "INSURANCE_LINE", objReader["INS_LINE"].ToString());
                        CreateAndSetElement(objDeductibleNode, "UNIT", sUnit);
                        CreateAndSetElement(objDeductibleNode, "UNIT_STATE_ROW_ID", objClaimXPolDed.UnitStateRowId.ToString());
                        if (objClaimXPolDed.DedTypeCode == iFPTypeCode)
                        {
                            CreateAndSetElement(objDeductibleNode, "COV_GROUP_CODE", "NA");
                        }
                        else
                        {
                            CreateAndSetElement(objDeductibleNode, "COV_GROUP_CODE", iCovGroupId.ToString());
                        }
                    }
                    
                    iSharedAggDedThirdParty = objClaimXPolDed.Context.InternalSettings.ColLobSettings[GetLOB(p_iClaimId)].SharedAggDedFlag;
                    //End ddhiman
                    CreateAndSetElement(objRootNode, "SHARED_AGG_DED_FLAG", iSharedAggDedThirdParty.ToString());
                    //jira# RMA-6135.Multicurrency
                    iBaseCurrCode = objClaimXPolDed.Context.InternalSettings.SysSettings.BaseCurrencyType;
                    sBaseCurr = objClaimXPolDed.Context.DbConnLookup.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode.ToString());
                    CreateAndSetElement(objRootNode, "BaseCurrencyType", sBaseCurr.ToString().Split('|')[1]);
                    //jira# RMA-6135.Multicurrency
                    //End: Added the following code by Sumit Agarwal for NI: 10/14/2014
                }

                #region formatting
              

                xDedTypes = objRootNode.SelectNodes("//SIR_DEDUCTIBLE");
                foreach (XmlNode xDedType in xDedTypes)
                {
                    if (!string.IsNullOrEmpty(xDedType.InnerText))
                    {
                        xDedType.InnerText = this.GetCodeInfo(xDedType.InnerText);
                    }
                }

                xDiminishingTypes = objRootNode.SelectNodes("//DIMINISHING_TYPE");
                foreach (XmlNode xDimType in xDiminishingTypes)
                {
                    if (!string.IsNullOrEmpty(xDimType.InnerText))
                    {
                        xDimType.InnerText = this.GetCodeInfo(xDimType.InnerText);
                    }
                }

                xAmts = objRootNode.SelectNodes("//REDUCED_AMOUNT");
                foreach (XmlNode xAmt in xAmts)
                {
                    if (!string.IsNullOrEmpty(xAmt.InnerText) && string.Compare(xAmt.InnerText, "NA") != 0)
                    {
                        xAmt.InnerText = String.Format("{0:c}", Conversion.CastToType<Double>(xAmt.InnerText, out bSuccess));
                    }
                }

                xCovGroupIds = objRootNode.SelectNodes("//COV_GROUP_CODE");
                foreach (XmlNode xCovGroupId in xCovGroupIds)
                {
                    if (!string.IsNullOrEmpty(xCovGroupId.InnerText))
                    {
                        if (string.Compare(xCovGroupId.InnerText,"NA",true)!=0)
                        {
                            if (string.Compare(xCovGroupId.InnerText,"0")==0)
                            {
                                CreateAndSetElement(((XmlElement)xCovGroupId.ParentNode), "COV_GROUP", "-");
                            }
                            else
                            {
                                CreateAndSetElement(((XmlElement)xCovGroupId.ParentNode), "COV_GROUP", this.GetCovGroupInfo(xCovGroupId.InnerText));
                            }
                        }
                        else
                        {
                            CreateAndSetElement(((XmlElement)xCovGroupId.ParentNode), "COV_GROUP", "NA");
                        }
                    }
                }
                #endregion
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
               
                if (objDbConnection!=null)
                {
                    objDbConnection.Dispose();
                }
                if (objClaimXPolDed!=null)
                {
                    objClaimXPolDed.Dispose();
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the Deductible for the given Deductible Id
        /// </summary>
        /// <param name="p_iClaimXPolDedId">Claim_x_Pol_Ded_id</param>
        /// <param name="p_iPolCvgRowId">Policy Coverage Id</param>
        /// <param name="p_objXmlIn">Input Xml</param>
        /// <returns>true on success</returns>
        public bool LoadDeductible(int p_iClaimXPolDedId, int p_iPolCvgRowId, XmlDocument p_objXmlIn)
        {
            StringBuilder sbSql = null;
            string sUnit = string.Empty;
            Double dRemianingAmt = 0d;
            bool bIsEditAllowed = false;
            Double dReducedAmt = 0d;
            int isPayment = 0;
            string sDiminishingEvalDate = string.Empty;
            bool bSuccess = false;
            XmlNode objNode = null;
         //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory
            
            ClaimXPolDed objClaimXPolDed = null;
            Claim objClaim = null;
            DbReader objReader = null;
            int iFPTypeCode = 0;
            int iCovGroupId = 0;
            int iPreventEditDedPerEvent = 0;
            bool bIsPaymentMade = false;
            int iTPTypeCode = 0;

            //ddhiman 10/13/2014, SMS Settings for deductible fields
            bool bIsDedTypEditAllowed = false;
            bool bIsDedAmtEditAllowed = false;
            string sSharedAggDedThirdParty = string.Empty;
            string sDiminishingFlag = string.Empty;
            //End ddhiman

            
            try
            {
               
                sbSql = new StringBuilder(string.Empty);
              
                objClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
                objClaim = m_objDataModelFactory.GetDataModelObject("Claim", false) as Claim;

                if (DbFactory.IsOracleDatabase(m_sConnectionString))
                {
                    sbSql.Append("SELECT P.POLICY_SYMBOL||'|'||P.POLICY_NUMBER||'|'||P.MODULE AS POLICY_NAME, C.CLAIM_NUMBER, CVG.DED_TYPE_CODE, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, CVG.COVERAGE_TEXT, CVG.COVERAGE_TYPE_CODE, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE ");
                }
                else
                {
                    sbSql.Append("SELECT P.POLICY_SYMBOL+'|'+P.POLICY_NUMBER+'|'+P.MODULE AS POLICY_NAME, C.CLAIM_NUMBER, CVG.DED_TYPE_CODE, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, CVG.COVERAGE_TEXT, CVG.COVERAGE_TYPE_CODE, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE ");
                }
                sbSql.Append("FROM POLICY_X_CVG_TYPE CVG ");
                sbSql.Append("INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID=PXU.POLICY_UNIT_ROW_ID ");
                sbSql.Append("INNER JOIN POLICY P ON PXU.POLICY_ID=P.POLICY_ID ");
                sbSql.Append("INNER JOIN CLAIM_X_POLICY CXP ON P.POLICY_ID=CXP.POLICY_ID ");
                sbSql.Append("INNER JOIN CLAIM C ON C.CLAIM_ID = CXP.CLAIM_ID ");
                sbSql.Append("INNER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID=PXU.UNIT_ID AND PUD.UNIT_TYPE=PXU.UNIT_TYPE ");
                sbSql.Append("WHERE CVG.POLCVG_ROW_ID=");
                sbSql.Append(p_iPolCvgRowId);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                if (objReader.Read())
                {
                    sUnit = this.GetUnit(objReader["UNIT_ID"].ToString(), objReader["UNIT_TYPE"].ToString());

                    objNode = p_objXmlIn.SelectSingleNode("//control[@name='ClaimName']");
                    if (objNode!=null)
                    {
                        objNode.InnerText = objReader["CLAIM_NUMBER"].ToString();
                    }
                    objNode = p_objXmlIn.SelectSingleNode("//control[@name='PolicyName']");
                    if (objNode != null)
                    {
                        objNode.InnerText = objReader["POLICY_NAME"].ToString();
                    }
                    objNode = p_objXmlIn.SelectSingleNode("//control[@name='UnitName']");
                    if (objNode != null)
                    {
                        objNode.InnerText = sUnit;
                    }
                    objNode = p_objXmlIn.SelectSingleNode("//control[@name='CoverageType']");
                    if (objNode != null)
                    { //To display coverage text on the lines of Funds transaction split and reserve screen.
                        
                        objNode.InnerText = objReader["COVERAGE_TEXT"].ToString();
                    }
                    objNode = p_objXmlIn.SelectSingleNode("//control[@name='InsuranceLine']");
                    if (objNode != null)
                    {
                        objNode.InnerText = objReader["INS_LINE"].ToString();
                    }

                    if (p_iClaimXPolDedId > 0)
                    {
                        objClaimXPolDed.MoveTo(p_iClaimXPolDedId);

                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='DedTypeCode']");
                        if (objNode != null)
                        {
                            objNode.InnerText = objClaimXPolDed.DedTypeCode.ToString();
                            //aaggarwal29: Added by Achla to make Deductible Type field on UI as None instead of blank.
                            if (objClaimXPolDed.DedTypeCode == 0)
                            {
                                objNode.InnerText = m_objDataModelFactory.Context.LocalCache.GetCodeId("None", "DEDUCTIBLE_TYPE").ToString();
                            }
                        }
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='SirDedAmt']");
                        if (objNode != null)
                        {
                            objNode.InnerText = objClaimXPolDed.SirDedAmt.ToString();
                        }
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='ExcludeExpenseFlag']");
                        if (objNode != null)
                        {
                            objNode.InnerText = objClaimXPolDed.ExcludeExpenseFlag.ToString();
                        }
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='DiminishingTypeCode']");
                        if (objNode != null)
                        {
                            objNode.InnerText = objClaimXPolDed.DiminishingTypeCode.ToString();
                        }
                   
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='DiminishingPercentage']");
                        if (objNode != null)
                        {
                            objNode.InnerText = string.Format("{0:f2}",objClaimXPolDed.DimPercentNum);
                        }
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='CoverageEvaluationDate']");
                        if (objNode != null )
                        {
                            if (!string.IsNullOrEmpty(objClaimXPolDed.DimEvaluationDate))
                            {
                                objNode.InnerText = Conversion.GetDBDateFormat(objClaimXPolDed.DimEvaluationDate, "MM/dd/yyyy");
                            }                           
                        }
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='DiminishingEvaluationDate']");
                        if (objNode != null && objClaimXPolDed.DiminishingTypeCode > 0)
                        {
                            sDiminishingEvalDate = this.GetEvaluationDate(objClaimXPolDed.ClaimId, objClaimXPolDed.UnitRowId, 
                                objClaimXPolDed.DiminishingTypeCode, objClaimXPolDed.DimEvaluationDate);

                            if (!string.IsNullOrEmpty(sDiminishingEvalDate))
                            {
                                objNode.InnerText = Conversion.GetDBDateFormat(sDiminishingEvalDate, "MM/dd/yyyy");
                            }
                        }
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='CustomerNum']");
                        if (objNode != null)
                        {
                            objNode.InnerText = objClaimXPolDed.CustomerNum.ToString();
                        }
                        
                        iFPTypeCode = objClaim.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");
                        iCovGroupId = DbFactory.ExecuteAsType<int>(objClaimXPolDed.Context.DbConnLookup.ConnectionString, "SELECT COV_GROUP_CODE FROM POLICY_X_INSLINE_GROUP WHERE POLICY_X_INSLINE_GROUP_ID=" + objClaimXPolDed.PolicyXInslineGroupId);
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='CovGroupId']");
                        if (objNode != null)
                        {
                           
                            {
                                if (objClaimXPolDed.DedTypeCode == iFPTypeCode)
                                {
                                    objNode.InnerText = "NA";
                                }
                                else
                                {
                                    objNode.InnerText = this.GetCovGroupInfo(iCovGroupId.ToString());
                                }
                            }
                    
                        }

                        
                        
                        objClaim.MoveTo(objClaimXPolDed.ClaimId);
                        switch (objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode))
                        {
                            case "GC":
                                bIsEditAllowed = m_UserLogin.IsAllowedEx(GC_DED_EDIT);
                                //ddhiman 10/13/2014, SMS Settings for deductible fields
                                bIsDedTypEditAllowed = m_UserLogin.IsAllowedEx(GC_DED_TYP_EDIT);
                                bIsDedAmtEditAllowed = m_UserLogin.IsAllowedEx(GC_DED_AMT_EDIT);
                                //End ddhiman
                                break;
                            case "WC":
                                bIsEditAllowed = m_UserLogin.IsAllowedEx(WC_DED_EDIT);
                                //ddhiman 10/13/2014, SMS Settings for deductible fields
                                bIsDedTypEditAllowed = m_UserLogin.IsAllowedEx(WC_DED_TYP_EDIT);
                                bIsDedAmtEditAllowed = m_UserLogin.IsAllowedEx(WC_DED_AMT_EDIT);
                                //End ddhiman
                                break;
                            default:
                                bIsEditAllowed = false;
                                break;
                        }

                        objNode = p_objXmlIn.SelectSingleNode("//FirstParty");
                        if (objNode != null)
                        {
                            objNode.InnerText = iFPTypeCode.ToString();
                        }
                        //jira# RMA-6135.Multicurrency
                        objNode = p_objXmlIn.SelectSingleNode("//BaseCurrencyType");
                        if (objNode != null)
                        {
                            iBaseCurrCode = objClaim.Context.InternalSettings.SysSettings.BaseCurrencyType;
                            sBaseCurr = objClaim.Context.DbConnLookup.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode.ToString());
                            objNode.InnerText = sBaseCurr.ToString().Split('|')[1];
                        }
                        //jira# RMA-6135.Multicurrency
                        //PCR changes by Nikhil on 17/12/2013 for Retaining Group - Start
                        iTPTypeCode = objClaim.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
                        objNode = p_objXmlIn.SelectSingleNode("//ThirdParty");
                        if (objNode != null)
                        {
                            objNode.InnerText = iTPTypeCode.ToString();
                        }
                        //PCR changes by Nikhil on 17/12/2013 for Retaining Group - End
                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='ReducedAmt']");
                        if (objNode != null)
                        {
                            dReducedAmt = objClaimXPolDed.CalculateReducedDeductibleAmount(objClaimXPolDed.ClaimId, objClaimXPolDed.DiminishingTypeCode,
                                objClaimXPolDed.DedTypeCode, objClaimXPolDed.SirDedAmt,
                                objClaimXPolDed.DimPercentNum, objClaimXPolDed.ClmXPolDedId);

                            objNode.InnerText = dReducedAmt.ToString();

                            //Different method for calculating remaining amount when coverage group is to be applied or otherwise
                            if (iCovGroupId == 0 || objClaimXPolDed.DedTypeCode == 0 || objClaimXPolDed.DedTypeCode == iFPTypeCode)
                            {
                                dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode);
                            }
                            else
                            {
                                //NI-PCR changes by Nikhil for handling deductable check at event level -  Start

                                iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
                                if (iPreventEditDedPerEvent == 0)
                                    dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode, iCovGroupId,false);
                                else
                                   dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, objClaimXPolDed.ClaimId, iCovGroupId, false);
                                //NI-PCR changes by Nikhil for handling deductable check at event level -  End
                            }
                        }
                        

                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='RemainingAmt']");
                        if (objNode != null)
                        {
                            objNode.InnerText = dRemianingAmt.ToString();
                        }

                        objNode = p_objXmlIn.SelectSingleNode("//control[@name='PolicyLOB']");
                        if (objNode != null)
                        {
                            objNode.InnerText = objClaimXPolDed.PolicyLobCode.ToString();
                        }

                     
                            bIsPaymentMade = IsPaymentMadeOnCoverage(objClaimXPolDed.ClaimId, objClaimXPolDed.ClmXPolDedId);
                            if (bIsPaymentMade)
                            {
                                isPayment = 1;
                            }
                            //Commented By Nikhil on 12/27/2013 -  start
                 
                 
                            //Commented By Nikhil on 12/27/2013 -  End
                          if (isPayment == 0 && objClaimXPolDed.DedTypeCode == iFPTypeCode && (objClaimXPolDed.DiminishingTypeCode == objClaimXPolDed.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE")))
                         {
                            isPayment = IsPaymentMade(objClaimXPolDed.UnitRowId, objClaimXPolDed.ClaimId, objClaimXPolDed.DiminishingTypeCode);
                        }
                        //end:for PCRs Changes, Added by Nitin Goel, 12/16/2013                       

                        objNode = p_objXmlIn.SelectSingleNode("//displaycolumn");
                        if (objNode != null)
                        {
                            ((XmlElement)objNode).SetAttribute("ControlsReadOnly", isPayment.ToString());
                            ((XmlElement)objNode).SetAttribute("PageReadOnly", bIsEditAllowed ? "0" : "-1");

                            //ddhiman 10/13/2014, SMS Settings for deductible fields
                            ((XmlElement)objNode).SetAttribute("DeductibleTypEdit", bIsDedTypEditAllowed ? "-1" : "0");
                            ((XmlElement)objNode).SetAttribute("DeductibleAmtEdit", bIsDedAmtEditAllowed ? "-1" : "0");
                            //End ddhiman
                        }

                        if (iCovGroupId > 0)
                        {
                 
                            objNode = p_objXmlIn.SelectSingleNode("//control[@name='hdn_CovGroup']");
                            if (objNode != null)
                            {
                                objNode.InnerText = iCovGroupId.ToString();
                                //Start:added by Nitin goel, for PCR Changes to track if ded per event is enabled or not.
                                iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
                                ((XmlElement)objNode).SetAttribute("IsDedPerEventEnabled", Convert.ToString(iPreventEditDedPerEvent));
                                //end: added by Nitin goel, for PCR changes.
                            }
                                      

                        }
                        //Start: ddhiman Added For NI: 10/14/2014
                        objNode = p_objXmlIn.SelectSingleNode("//displaycolumn");
                        sSharedAggDedThirdParty = Conversion.ConvertObjToStr(objClaim.Context.InternalSettings.ColLobSettings[GetLOB(objClaim.ClaimId)].SharedAggDedFlag);
                        sDiminishingFlag = Conversion.ConvertObjToStr(objClaim.Context.InternalSettings.ColLobSettings[GetLOB(objClaim.ClaimId)].DimnishingFlag);
                        
                        CreateAndSetElement((XmlElement)objNode, "SHARED_AGG_DED_FLAG", sSharedAggDedThirdParty);
                        CreateAndSetElement((XmlElement)objNode, "DIMNISHING_FLAG", sDiminishingFlag);
                        //End: ddhiman                     
                    }
                }

                #region formatting manipulation
                XmlNode xDedType = p_objXmlIn.SelectSingleNode("//control[@name='DedTypeCode']");
                if (xDedType!=null)
                {
                    if (!string.IsNullOrEmpty(xDedType.InnerText) && xDedType.Attributes["codeid"]!=null)
                    {
                        xDedType.Attributes["codeid"].Value = xDedType.InnerText;
                        xDedType.InnerText = this.GetCodeInfo(xDedType.InnerText);
                    }
                }
                

                XmlNode xRecTransType = p_objXmlIn.SelectSingleNode("//control[@name='RecTransTypeCode']");
                if (xRecTransType != null)
                {
                    if (!string.IsNullOrEmpty(xRecTransType.InnerText) && xRecTransType.Attributes["codeid"] != null)
                    {
                        xRecTransType.Attributes["codeid"].Value = xRecTransType.InnerText;
                        xRecTransType.InnerText = this.GetCodeInfo(xRecTransType.InnerText);
                    }
                }

                XmlNode xDedAppliesToCode = p_objXmlIn.SelectSingleNode("//control[@name='DeductibleApplies']");
                if (xDedAppliesToCode != null)
                {
                    if (!string.IsNullOrEmpty(xDedAppliesToCode.InnerText) && xDedAppliesToCode.Attributes["codeid"] != null)
                    {
                        xDedAppliesToCode.Attributes["codeid"].Value = xDedAppliesToCode.InnerText;
                        xDedAppliesToCode.InnerText = this.GetCodeInfo(xDedAppliesToCode.InnerText);
                    }
                }

                XmlNode xDiminishingType = p_objXmlIn.SelectSingleNode("//control[@name='DiminishingTypeCode']");
                if (xDiminishingType != null)
                {
                    if (!string.IsNullOrEmpty(xDiminishingType.InnerText) && xDiminishingType.Attributes["codeid"]!=null)
                    {
                        xDiminishingType.Attributes["codeid"].Value = xDiminishingType.InnerText;
                        xDiminishingType.InnerText = this.GetCodeInfo(xDiminishingType.InnerText);
                    }
                }
                XmlNode xPolicyLob = p_objXmlIn.SelectSingleNode("//control[@name='PolicyLOB']");
                if (xPolicyLob != null)
                {
                    if (!string.IsNullOrEmpty(xPolicyLob.InnerText))
                    {
                        xPolicyLob.InnerText = this.GetCodeInfo(xPolicyLob.InnerText);
                    }
                }

                XmlNode xAmt = p_objXmlIn.SelectSingleNode("//control[@name='ReducedAmt']");
                if(xAmt != null)
                {
                    xAmt.InnerText = String.Format("{0:c}", Conversion.CastToType<Double>(xAmt.InnerText, out bSuccess));
                    if (objClaimXPolDed.DiminishingTypeCode == 0)
                    {
                        xAmt.InnerText = "NA";
                    }
                }

                #endregion
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objClaim!=null)
                {
                    objClaim.Dispose();
                }
                if (objClaimXPolDed != null)
                {
                    objClaimXPolDed.Dispose();
                }
            
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
            
            return true;
        }

        public XmlDocument LoadThirdPartyData(XmlDocument p_objXmlIn)
        {
            XmlNode objNode = null;
            XmlNode objCvgGroup = null;
            XmlNode objSirDedAmt = null;
            XmlNode objRemainingAmt = null;
            XmlNode objClaimId = null;
            int iCvgGroupCode = 0;
            int iClaimId = 0;
            bool bSuccess = false;
            double dCvgSirDedAmt = 0d;
            double dRemainingAmt = 0d;
            int iDedPerEventFlag = 0;
           // DataModelFactory objFactory = null;
            ClaimXPolDed oClaimXPolDed = null;

            objNode = p_objXmlIn.SelectSingleNode("//control[@name='hdn_CovGroup']");
            if (objNode!=null)
            {
                iCvgGroupCode = Conversion.CastToType<int>(objNode.InnerText, out bSuccess);
            }
            //Start:added by Nitin goel, NI PCrs Changes.
           
            if (iCvgGroupCode > 0 && GetDeductiblePerEventFlag(iCvgGroupCode)==-1)
            //End:added by Nitin goel
            {
            
                oClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", true) as ClaimXPolDed;
                objCvgGroup = p_objXmlIn.SelectSingleNode("//control[@name='CovGroupId']");
                if (objCvgGroup != null)
                {
                    objCvgGroup.InnerText = this.GetCovGroupInfo(iCvgGroupCode.ToString());
                }
                objSirDedAmt = p_objXmlIn.SelectSingleNode("//control[@name='SirDedAmt']");
                if (objSirDedAmt != null)
                {
                    dCvgSirDedAmt = DbFactory.ExecuteAsType<double>(m_objDataModelFactory.Context.DbConnLookup.ConnectionString, "SELECT SIR_DED_PEREVENT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE=" + iCvgGroupCode);
                    objSirDedAmt.InnerText = dCvgSirDedAmt.ToString();
                }
                objRemainingAmt = p_objXmlIn.SelectSingleNode("//control[@name='RemainingAmt']");
                if (objRemainingAmt != null)
                {
                    objClaimId = p_objXmlIn.SelectSingleNode("//ClaimId");
                    iClaimId = Conversion.CastToType<int>(objClaimId.InnerText, out bSuccess);
                    dRemainingAmt = oClaimXPolDed.CalculateRemainingAmount(dCvgSirDedAmt,iClaimId,iCvgGroupCode,false);
                    objRemainingAmt.InnerText = dRemainingAmt.ToString();
                }
            }
            
            return p_objXmlIn;
        }
        public XmlDocument GetAggregateDetails(XmlDocument p_objXmlIn)
        {
            XmlNode xPolGroup = null;
            XmlNode xCoverageGroup = null;
            XmlNode xAggLimit = null;
            XmlNode xAggBalance = null;
            XmlNode xDedEventAmt = null;
            XmlNode xClaimId = null;
            XmlNode xApplyAtAllCoverage = null;
           // ExpenseFlagAddition - nbhatia6 - 07/03/14
            XmlNode xExcludeExpFlag = null;
            XmlNode xPolicyId = null;
            XmlNode xDedId = null;
            int iCovGroupId = 0;
            int iOldCovGroupId = 0;
            bool bSuccess = false;
            bool bPreventEdit = false;
            int iPreventEditDedPerEvent = 0;
            int iClaimId = 0;
            int iCountNonGroupedCoverage = 0;
            int iPolicyId=0;
            int iDedId = 0;
			//NI-PCR changes by Nikhil - Start
            int iCoverageUnitStateId = 0;
          //NI-PCR changes by Nikhil - End
            StringBuilder sbSql= null;
            //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory
         //   DataModelFactory objFacotry = null;
            DbReader oDbReader = null;
       //     objFacotry = new DataModelFactory(m_UserLogin);
            xPolGroup = p_objXmlIn.SelectSingleNode("//Pol_Grp_Id");
            xCoverageGroup = p_objXmlIn.SelectSingleNode("//control[@name='CovGroupId']");
            xAggLimit = p_objXmlIn.SelectSingleNode("//control[@name='AggregateLimit']");
            xAggBalance = p_objXmlIn.SelectSingleNode("//control[@name='RemainingAggregateAmount']");
            xDedEventAmt = p_objXmlIn.SelectSingleNode("//control[@name='DedPerEvent']");
            xClaimId = p_objXmlIn.SelectSingleNode("//ClaimId");
            xApplyAtAllCoverage = p_objXmlIn.SelectSingleNode("//control[@name='AllCoverageChk']");
          //  ExpenseFlagAddition - nbhatia6 - 07/03/14
            xExcludeExpFlag = p_objXmlIn.SelectSingleNode("//control[@name='ExcludeExpenseFlag']");
             xPolicyId = p_objXmlIn.SelectSingleNode("//PolicyId");
             xDedId = p_objXmlIn.SelectSingleNode("//DedId");
            

            if (xClaimId != null)
            {
                iClaimId = Conversion.CastToType<int>(xClaimId.InnerText, out bSuccess);
            }
            switch (GetLOB(iClaimId))
            {
                case 241:
                    bPreventEdit = m_UserLogin.IsAllowedEx(GC_AGG_EDIT);
                    break;
                case 243:
                    bPreventEdit = m_UserLogin.IsAllowedEx(WC_AGG_EDIT);
                    break;
                default:
                    bPreventEdit = false;
                    break;
            }
            if (xPolGroup!=null)
            {
                iOldCovGroupId = Conversion.CastToType<int>(xPolGroup.InnerText, out bSuccess);
                ((XmlElement)xPolGroup).SetAttribute("IsEditAllowed", bPreventEdit ? "0" : "-1");
                
            }
            if (xCoverageGroup!=null)
            {
                if (xCoverageGroup.Attributes["codeid"] != null)
                {
                    iCovGroupId = Conversion.CastToType<int>(xCoverageGroup.Attributes["codeid"].Value, out bSuccess);
                    //Start:added by Nitin goel, for PCR Changes to track if ded per event is enabled or not.
                    iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
                    ((XmlElement)xPolGroup).SetAttribute("IsDedPerEventEnabled", Convert.ToString(iPreventEditDedPerEvent));
                    //end: added by Nitin goel, for PCR changes.
                }
            }
            
            if (xDedId != null)
            {
                iDedId = Conversion.CastToType<int>(xDedId.InnerText, out bSuccess);
            }
            ////            //**************************************************************
            //Start:added by Nitin Goel, for NI PCRs Changes
            //Uncommented By Nikhil on 04/17/14 -  Start
            if (IsPaymentMade(iClaimId, iDedId) && iOldCovGroupId != iCovGroupId)
            {
                xPolGroup.InnerText = iOldCovGroupId.ToString();
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("DeductibleManager.PaymentMade.Error",m_iClientId));
                
            }
            else
            {
                if (xPolGroup != null)
                {
                    xPolGroup.InnerText = iCovGroupId.ToString();
                }
            }
           
            if (iCovGroupId>0)
            {
                if (xCoverageGroup != null)
                {
                    if (xCoverageGroup.Attributes["codeid"]!=null)
                    {
                        xCoverageGroup.Attributes["codeid"].Value = iCovGroupId.ToString();
                    }

                    xCoverageGroup.InnerText = this.GetCovGroupInfo(iCovGroupId.ToString());
                }
                using (oDbReader = DbFactory.ExecuteReader(m_sConnectionString, "SELECT * FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE=" + iCovGroupId))
                {
                    if (oDbReader.Read())
                    {
                        if (xAggLimit != null)
                        {
                            xAggLimit.InnerText = string.Format("{0}", oDbReader["AGG_LIMIT_AMT"]);
                        }
                        if (xAggBalance != null)
                        {
                            xAggBalance.InnerText = string.Format("{0}", this.GetAgregateBalance(iCovGroupId, this.GetLOB(iClaimId), iClaimId));
                        }
                        if (xDedEventAmt != null)
                        {
                            xDedEventAmt.InnerText = string.Format("{0}", oDbReader["SIR_DED_PEREVENT_AMT"]);
                        }
                       // Start -  ExpenseFlagAddition - nbhatia6 - 07/03/14
                        if (xExcludeExpFlag != null)
                        {
                            if (oDbReader.GetInt32("EXCLUDE_EXPENSE_FLAG") == 0)
                                xExcludeExpFlag.InnerText = "False";
                            else
                                xExcludeExpFlag.InnerText = "True";
                        }

                        // End -  ExpenseFlagAddition - nbhatia6 - 07/03/14
                    }
                    else
                    {
                        if (xAggLimit != null)
                        {
                            xAggLimit.InnerText = "0";
                        }
                        if (xAggBalance != null)
                        {
                            xAggBalance.InnerText = "0";
                        }
                        if (xDedEventAmt!=null)
                        {
                            xDedEventAmt.InnerText = "0";
                        }
                        // Start -  ExpenseFlagAddition - nbhatia6 - 07/03/14
                        if (xExcludeExpFlag != null)
                        {
                            xExcludeExpFlag.InnerText = "False";
                        }
                        // End -  ExpenseFlagAddition - nbhatia6 - 07/03/14
                    }
                }
                if(xPolicyId!=null)
                {
                    iPolicyId= Conversion.CastToType<int>(xPolicyId.InnerText, out bSuccess);
                
                }
                if(xApplyAtAllCoverage!=null)
                {
                    //NI-PCR changes by Nikhil, Retrieval of usit state ID -  Start
                    iCoverageUnitStateId = DbFactory.ExecuteAsType<int>(m_sConnectionString,String.Format("SELECT UNIT_STATE_ROW_ID FROM CLAIM_X_POL_DED WHERE CLM_X_POL_DED_ID ={0}",iDedId));
                    //NI-PCR changes by Nikhil, Retrieval of usit state ID -  End
                    sbSql= new StringBuilder();
                    sbSql=sbSql.Append("SELECT COUNT(*) FROM POLICY_X_INSLINE_GROUP PXI INNER JOIN POLICY P ON PXI.POLICY_SYSTEM_ID=P.POLICY_SYSTEM_ID");
                    sbSql=sbSql.Append(" AND PXI.POLICY_NUMBER=P.POLICY_NUMBER AND PXI.POLICY_SYMBOL =P.POLICY_SYMBOL AND PXI.MODULE=P.MODULE"); 
                    sbSql=sbSql.Append(" AND PXI.MASTER_COMPANY=P.MASTER_COMPANY AND PXI.LOCATION_COMPANY=P.LOCATION_COMPANY");
                    sbSql=sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLICY_ID=P.POLICY_ID AND CXPD.POLICY_X_INSLINE_GROUP_ID =PXI.POLICY_X_INSLINE_GROUP_ID");                    
                    //NI-PCR changes by Nikhil,Added a condition in where clause for state -  Start
				 	sbSql = sbSql.Append(String.Format(" WHERE(PXI.COV_GROUP_CODE = 0 OR PXI.COV_GROUP_CODE IS NULL OR PXI.COV_GROUP_CODE<>{0}) AND CXPD.DED_TYPE_CODE<>{1} AND P.POLICY_ID={2} AND CXPD.UNIT_STATE_ROW_ID = {3}", iCovGroupId, m_objDataModelFactory.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"), iPolicyId, iCoverageUnitStateId));                                                     
                    //NI-PCR changes by Nikhil,Added a condition in where clause for state -  End
                    iCountNonGroupedCoverage= DbFactory.ExecuteAsType<int>(m_sConnectionString, sbSql.ToString() );
                    if(iCountNonGroupedCoverage==0)
                    {
                    xApplyAtAllCoverage.InnerText="True";
                    }
                }
            }
            if (sbSql!=null)
                sbSql=null;
            
            return p_objXmlIn;
        }

        private double GetAgregateBalance(int p_iCovGroupId, int p_iLobCode, int p_iClaimId)
        {
            double dAggBalance = 0d;
            double dAggLimit = 0d;
            double dAppliedAmt = 0d;
           
            int iDedReserveTypeCode = 0;
            int iTPDedCode = 0;
            int iSirDedCode = 0;
            StringBuilder sbSql = null;
            bool bSuccess = false;
          

            iTPDedCode = m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
            iSirDedCode = m_objDataModelFactory.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE");
            iDedReserveTypeCode = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[p_iLobCode].DedRecReserveType;
            sbSql = new StringBuilder();
            //Start:Added by Nitin goel, MITS 36319,05/09/2014
       
            sbSql.Append(" SELECT SUM(COLLECTION_TOTAL) AS DED_APPLIED_AMT ");
            //end:added by Nitin goel            
            sbSql.Append(" FROM RESERVE_CURRENT RC ");
            sbSql.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON CXL.CVG_LOSS_ROW_ID=RC.POLCVG_LOSS_ROW_ID ");
            sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON RC.POLCVG_ROW_ID = CXD.POLCVG_ROW_ID AND RC.CLAIM_ID = CXD.CLAIM_ID ");
            sbSql.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXG ON PXG.POLICY_X_INSLINE_GROUP_ID = CXD.POLICY_X_INSLINE_GROUP_ID ");
            sbSql.Append(" WHERE RC.RESERVE_TYPE_CODE = ").Append(iDedReserveTypeCode);
            sbSql.Append(" AND PXG.COV_GROUP_CODE = ").Append(p_iCovGroupId);
            sbSql.Append(" AND (CXD.DED_TYPE_CODE=").Append(iTPDedCode);
            sbSql.Append(" OR CXD.DED_TYPE_CODE=").Append(iSirDedCode).Append(")");

            using (DbReader oDbReader = m_objDataModelFactory.Context.DbConnLookup.ExecuteReader(sbSql.ToString()))
            {
                if (oDbReader.Read())
                {
                   
                    if (oDbReader["DED_APPLIED_AMT"] != DBNull.Value && oDbReader["DED_APPLIED_AMT"] != null)
                    {
                        dAppliedAmt = Conversion.CastToType<double>(oDbReader["DED_APPLIED_AMT"].ToString(), out bSuccess);
                    }
                }
            }


            dAggLimit = m_objDataModelFactory.Context.DbConnLookup.ExecuteDouble("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE=" + p_iCovGroupId);

          
            dAggBalance = dAggLimit - (dAppliedAmt);
            if (dAggBalance < 0d)
            {
                dAggBalance = 0d;
            }
            return dAggBalance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <returns></returns>
        public XmlDocument SaveAggregateDetails(XmlDocument p_objXmlIn)
        {
            XmlNode xPolGroup = null;
            XmlNode xCoverageGroup = null;
            XmlNode xAggLimit = null;
            XmlNode xAggBalance = null;
            XmlNode xDedPerEvent = null;
            XmlNode xPolicyId = null;
            XmlNode xDeductibleId = null;
            XmlNode xEventId = null;
            XmlNode xApplyAtAllCoverage = null;
          //  ExpenseFlagAddition - nbhatia6 - 07/03/14
            XmlNode xExcludeExpenseFlag = null;
            XmlNode xClaimId = null;

            int iCovGroupId = 0;
            int iClmXPolDedAggLmtId = 0;
			//NI-PCR changes by Nikhil  - Start
            int iCoverageUnitStateId =0;
            //NI-PCR changes by Nikhil  - End
            bool bSuccess = false;
            double dAggLimit = 0d;
            double dDedPerEvent = 0d;
            bool bIsDataChanged = false;
            //  ExpenseFlagAddition - nbhatia6 - 07/03/14
            bool bExpenseFlag = false;
            int iPolicyId = 0;
            int iDedId = 0;
            int iClaimId = 0;
            int iEventId = 0;
            int iDeductiblePerEventEnabled = 0;
            string sSql = string.Empty;
            string sGroupSql = string.Empty;
			

            ClaimXPolDedAggLimit oClaimXPolDedAggLimit = null;
            PolicyXInslineGroup oPolicyXInslineGroup = null;
            ClaimXPolDed oClaimXPolDed = null;
            //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory
           DataModelFactory objFacotry = null;

            List<int> lstClmXPolDedIds = null;
            List<int> lstPolXInsLineIds = null;

            xCoverageGroup = p_objXmlIn.SelectSingleNode("//control[@name='CovGroupId']");
            xAggLimit = p_objXmlIn.SelectSingleNode("//control[@name='AggregateLimit']");
            xAggBalance = p_objXmlIn.SelectSingleNode("//control[@name='RemainingAggregateAmount']");
            xApplyAtAllCoverage = p_objXmlIn.SelectSingleNode("//control[@name='AllCoverageChk']");
            //ExpenseFlagAddition - nbhatia6 - 07/03/14
            xExcludeExpenseFlag = p_objXmlIn.SelectSingleNode("//control[@name='ExcludeExpenseFlag']");
            xDedPerEvent = p_objXmlIn.SelectSingleNode("//control[@name='DedPerEvent']");
            xPolGroup = p_objXmlIn.SelectSingleNode("//Pol_Grp_Id");
            xPolicyId = p_objXmlIn.SelectSingleNode("//PolicyId");
            xDeductibleId = p_objXmlIn.SelectSingleNode("//DedId");
            xEventId = p_objXmlIn.SelectSingleNode("//EventId");
            xClaimId = p_objXmlIn.SelectSingleNode("//ClaimId");

            objFacotry = new DataModelFactory(m_UserLogin, m_iClientId);

            if (xCoverageGroup != null && xCoverageGroup.Attributes["codeid"]!=null)
            {
                iCovGroupId = Conversion.CastToType<int>(xCoverageGroup.Attributes["codeid"].Value, out bSuccess);
            }

            if (xEventId != null)
            {
                iEventId = Conversion.CastToType<int>(xEventId.InnerText, out bSuccess);
            }

            if (iCovGroupId > 0)
            {
                oClaimXPolDedAggLimit = objFacotry.GetDataModelObject("ClaimXPolDedAggLimit", false) as ClaimXPolDedAggLimit;

                iClmXPolDedAggLmtId = oClaimXPolDedAggLimit.Context.DbConnLookup.ExecuteInt(String.Format("SELECT {1} FROM {0} WHERE COV_GROUP_CODE ={2}", oClaimXPolDedAggLimit.Table, oClaimXPolDedAggLimit.KeyFieldName, iCovGroupId));
                if (iClmXPolDedAggLmtId > 0)
                {
                    oClaimXPolDedAggLimit.MoveTo(iClmXPolDedAggLmtId);
                    oClaimXPolDedAggLimit.UpdatedByUser = oClaimXPolDedAggLimit.Context.RMUser.LoginName;
                }
                else
                {
                    oClaimXPolDedAggLimit.CovGroupId = iCovGroupId;
                }

                if (xAggLimit != null && !string.IsNullOrEmpty(xAggLimit.InnerText))
                {
                    dAggLimit = Conversion.CastToType<double>(xAggLimit.InnerText, out bSuccess);
                    if (bSuccess && dAggLimit != oClaimXPolDedAggLimit.AggLimitAmt)
                    {
                        bIsDataChanged = true;
                        oClaimXPolDedAggLimit.AggLimitAmt = dAggLimit;
                    }
                }

                if (xDedPerEvent != null && !string.IsNullOrEmpty(xDedPerEvent.InnerText))
                {
                    dDedPerEvent = Conversion.CastToType<double>(xDedPerEvent.InnerText, out bSuccess);
                    if (bSuccess && dDedPerEvent != oClaimXPolDedAggLimit.SirDedPreventAmt)
                    {
                        bIsDataChanged = true;
                        oClaimXPolDedAggLimit.SirDedPreventAmt = dDedPerEvent;

                        sSql = string.Format("SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED CXD INNER JOIN POLICY_X_INSLINE_GROUP PXG ON CXD.POLICY_X_INSLINE_GROUP_ID=PXG.POLICY_X_INSLINE_GROUP_ID WHERE PXG.COV_GROUP_CODE={0} AND CXD.DED_TYPE_CODE<>{1}", iCovGroupId, objFacotry.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"));
                     
                    }
                }
                //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
                if (xExcludeExpenseFlag != null && !string.IsNullOrEmpty(xExcludeExpenseFlag.InnerText))
                {
                    bExpenseFlag = Conversion.CastToType<bool>(xExcludeExpenseFlag.InnerText, out bSuccess);
                    if (bSuccess && !bool.Equals(bExpenseFlag,oClaimXPolDedAggLimit.ExcludeExpenseFlag))
                    {
                        bIsDataChanged = true;
                        oClaimXPolDedAggLimit.ExcludeExpenseFlag = bExpenseFlag;
                        sSql = string.Format("SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED CXD INNER JOIN POLICY_X_INSLINE_GROUP PXG ON CXD.POLICY_X_INSLINE_GROUP_ID=PXG.POLICY_X_INSLINE_GROUP_ID WHERE PXG.COV_GROUP_CODE={0} AND CXD.DED_TYPE_CODE<>{1}", iCovGroupId, objFacotry.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"));
                    }
                }
                //End - ExpenseFlagAddition - nbhatia6 - 07/03/14

                if (bIsDataChanged)
                {
                    if (IsPaymentMade(oClaimXPolDedAggLimit.CovGroupId)) // TO DO: Need to verify the scenerio.
                    {
                        //Cloud changes by Nikhil for NI
                        throw new RMAppException(Globalization.GetString("DeductibleManager.NonEditable.Error",m_iClientId));
                    }
                    //Changed By Nikhil on 01/08/2014 for validate event shared deductible amount with group aggregate limit  - Start
                    if (dAggLimit > 0 && dDedPerEvent > dAggLimit)
                    {
                        //Cloud changes by Nikhil for NI
                        throw new RMAppException(Globalization.GetString("DeductibleManager.EventAmountLimit.Error",m_iClientId));
                    }
                    //start - jira# rma-6292
                    if (dAggLimit < 0 || dDedPerEvent < 0)
                    {

                        throw new RMAppException(Globalization.GetString("DeductibleManager.AmountLessThanZero.Error", m_iClientId));
                    }
                    //End - jira# rma-6292
                    //Changed By Nikhil on 01/08/2014 for validate event shared deductible amount with group aggregate limit  - Start
                    oClaimXPolDedAggLimit.Save();

                }
                //Changed by Nikhil on 01/14/2014  - Start
         
                if (xAggBalance != null && !IsPaymentMade(oClaimXPolDedAggLimit.CovGroupId))
                //Changed by Nikhil on 01/14/2014  - End
                {
                    xAggBalance.InnerText = xAggLimit.InnerText;
                }
            }
            else
            {
                xAggBalance.InnerText = "";
                xAggLimit.InnerText = "";
                xDedPerEvent.InnerText = "";
            }

            if (xApplyAtAllCoverage != null)
            {
                if (xPolicyId != null)
                {
                    iPolicyId = Conversion.CastToType<int>(xPolicyId.InnerText, out bSuccess);
                }
                if (xDeductibleId != null)
                {
                    iDedId = Conversion.CastToType<int>(xDeductibleId.InnerText, out bSuccess);
                }
                
                if (xClaimId != null)
                {
                    iClaimId = Conversion.CastToType<int>(xClaimId.InnerText, out bSuccess);
                }

                if (string.Compare(xApplyAtAllCoverage.InnerText, "True", true) == 0)
                {
                       sGroupSql = string.Format("SELECT CXPD.CLM_X_POL_DED_ID, CXPD.POLICY_X_INSLINE_GROUP_ID,PXIG.COV_GROUP_CODE,CXPD.UNIT_STATE_ROW_ID FROM CLAIM_X_POL_DED CXPD INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID= PXIG.POLICY_X_INSLINE_GROUP_ID WHERE CXPD.POLICY_ID={0} AND CXPD.CLAIM_ID={1} AND CXPD.DED_TYPE_CODE<>{2}", iPolicyId, iClaimId, objFacotry.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"));
                    sSql = string.Format("SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED CXD INNER JOIN POLICY_X_INSLINE_GROUP PXG ON CXD.POLICY_X_INSLINE_GROUP_ID=PXG.POLICY_X_INSLINE_GROUP_ID WHERE PXG.COV_GROUP_CODE={0} AND CXD.DED_TYPE_CODE<>{1}", iCovGroupId, objFacotry.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"));
                }
                else
                {
                    sGroupSql = string.Format("SELECT CXPD.CLM_X_POL_DED_ID, CXPD.POLICY_X_INSLINE_GROUP_ID,PXIG.COV_GROUP_CODE,CXPD.UNIT_STATE_ROW_ID FROM CLAIM_X_POL_DED CXPD INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID= PXIG.POLICY_X_INSLINE_GROUP_ID WHERE CXPD.CLM_X_POL_DED_ID={0}", iDedId);
                    if (string.IsNullOrEmpty(sSql))
                    {
                      
                        sSql = string.Format("SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED WHERE POLICY_X_INSLINE_GROUP_ID=(SELECT POLICY_X_INSLINE_GROUP_ID FROM CLAIM_X_POL_DED WHERE CLM_X_POL_DED_ID={0})", iDedId);
                    }
                    
                }
                

                if (!string.IsNullOrEmpty(sGroupSql))
                {
                    //NI-PCR changes by Nikhil,retrieval of unit state id  - Start
                    iCoverageUnitStateId = objFacotry.Context.DbConnLookup.ExecuteInt(String.Format("SELECT UNIT_STATE_ROW_ID FROM CLAIM_X_POL_DED WHERE CLM_X_POL_DED_ID ={0}", iDedId));
                    //NI-PCR changes by Nikhil,retrieval of unit state id  - End
                    oPolicyXInslineGroup = objFacotry.GetDataModelObject("PolicyXInslineGroup", false) as PolicyXInslineGroup;
                    lstPolXInsLineIds = new List<int>();
                    int iOtherCovGroupId=0;
                    int iOtherCovDedId = 0;
					//NI-PCR changes by Nikhil  - Start
                    int iOtherCovUnitStateID = 0 ; 
                   //NI-PCR changes by Nikhil  - End
                    using (DbReader oDbReader = objFacotry.Context.DbConnLookup.ExecuteReader(sGroupSql))
                    {
                        while (oDbReader.Read())
                        {
                            iOtherCovGroupId= Conversion.CastToType<int>(oDbReader["COV_GROUP_CODE"].ToString(), out bSuccess);
                            iOtherCovDedId = Conversion.CastToType<int>(oDbReader["CLM_X_POL_DED_ID"].ToString(), out bSuccess);
                            //NI-PCR changes by Nikhil,If condition added for state check  - Start
                            iOtherCovUnitStateID = Conversion.CastToType<int>(oDbReader["UNIT_STATE_ROW_ID"].ToString(), out bSuccess);
							if (iCoverageUnitStateId == iOtherCovUnitStateID)
                            {

                             
                                    if (!IsPaymentMade(iClaimId, iOtherCovDedId))
                                    {
                                        lstPolXInsLineIds.Add(oDbReader.GetInt32("POLICY_X_INSLINE_GROUP_ID"));
                                    }
                           
                            }
							//NI-PCR changes by Nikhil,If condition added for state check  - End
                        }
                    }

                    foreach (int iPolicyXInslineGroupId in lstPolXInsLineIds)
                    {
                        oPolicyXInslineGroup.MoveTo(iPolicyXInslineGroupId);
                        oPolicyXInslineGroup.CovGroupId = iCovGroupId;
                        oPolicyXInslineGroup.Save();
                    }
                }
            }
            //Start: added by nitin goel, for PCR Changes, deductible amount should only be overridden when deductible per event is enabled for the coverage group.
            iDeductiblePerEventEnabled = objFacotry.Context.DbConnLookup.ExecuteInt("SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
            if (iDeductiblePerEventEnabled == -1)
            {
                //end:added by nitin goel.
                if (!string.IsNullOrEmpty(sSql))
                {
                   
                    lstClmXPolDedIds = new List<int>();
                    using (DbReader oDbReader = objFacotry.Context.DbConnLookup.ExecuteReader(sSql))
                    {
                        while (oDbReader.Read())
                        {
                            int iCurrentClaimXPolDedId = 0;
                            iCurrentClaimXPolDedId = oDbReader.GetInt32("CLM_X_POL_DED_ID");
                            if (!IsPaymentMade(iClaimId, iCurrentClaimXPolDedId))
                            {
                                lstClmXPolDedIds.Add(iCurrentClaimXPolDedId);
                            }
                        }
                    }

                    foreach (int iClaimXPolDedId in lstClmXPolDedIds)
                    {
					 oClaimXPolDed = objFacotry.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
                        oClaimXPolDed.MoveTo(iClaimXPolDedId);
                        oClaimXPolDed.SirDedAmt = dDedPerEvent;
                        //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
                        oClaimXPolDed.ExcludeExpenseFlag = bExpenseFlag;
                        //End - ExpenseFlagAddition - nbhatia6 - 07/03/14
                        bSuccess = this.ModifyRecoveryreserves(dDedPerEvent, oClaimXPolDed.ClaimId, oClaimXPolDed.PolcvgRowId, oClaimXPolDed.PolicyId);
                        if (bSuccess)
                        {
                            oClaimXPolDed.Save();
                        }
						if (oClaimXPolDed!=null)
						{
						oClaimXPolDed.Dispose();
						}
                    }
                }
                //Start: added by nitin goel,
            }
            //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
            else if (iCovGroupId>0)
            {
                if (!string.IsNullOrEmpty(sSql))
                {
                    lstClmXPolDedIds = new List<int>();
                    using (DbReader oDbReader = objFacotry.Context.DbConnLookup.ExecuteReader(sSql))
                    {
                        while (oDbReader.Read())
                        {
                            int iCurrentClaimXPolDedId = 0;
                            iCurrentClaimXPolDedId = oDbReader.GetInt32("CLM_X_POL_DED_ID");
                            if (!IsPaymentMade(iClaimId, iCurrentClaimXPolDedId))
                            {
                                lstClmXPolDedIds.Add(iCurrentClaimXPolDedId);
                            }
                        }
                    }

                    foreach (int iClaimXPolDedId in lstClmXPolDedIds)
                    {
                        oClaimXPolDed = objFacotry.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
                        oClaimXPolDed.MoveTo(iClaimXPolDedId);
                        oClaimXPolDed.ExcludeExpenseFlag = bExpenseFlag;
                        oClaimXPolDed.Save();
                    }
                }
            }
            //End - ExpenseFlagAddition - nbhatia6 - 07/03/14
            //end:added by nitin goel

            return p_objXmlIn;
        }

        public XmlDocument GetAggregateHistory(XmlDocument p_objXmlIn)
        {
            int iCovGroupId = 0;
            bool bSuccess = false;
            DateTime date = DateTime.MinValue;

            XmlNode xPolGroup = null;
            DbReader oDbReader = null;
            
            XmlDocument xDoc = null;
            XmlElement xObjRootNode = null;
            XmlElement xObjDeductibleNode = null;
            double dDedPerEvntAmt = 0d;
            double dAggrLimit = 0d;
            xDoc = new XmlDocument();
            xDoc.InnerXml = string.Format("<AGGREGATE_HISTORY></AGGREGATE_HISTORY>");
            xObjRootNode = (XmlElement)xDoc.SelectSingleNode("//AGGREGATE_HISTORY");

            xPolGroup = p_objXmlIn.SelectSingleNode("//POL_GRP_ID");
            if (xPolGroup != null)
            {
                iCovGroupId = Conversion.CastToType<int>(xPolGroup.InnerText, out bSuccess);
            }

            if (iCovGroupId > 0)
            {
                using (oDbReader = DbFactory.ExecuteReader(m_sConnectionString, "SELECT * FROM CLAIM_X_POL_DED_AGG_LIMIT_HIST WHERE COV_GROUP_CODE=" + iCovGroupId + " ORDER BY DED_AGG_LIMIT_HIST_ID DESC"))
                {
                    while (oDbReader.Read())
                    {
                        CreateElement(xObjRootNode, "AGGREGATE", ref xObjDeductibleNode);

                        dAggrLimit = Conversion.CastToType<Double>(oDbReader["AGG_LIMIT_AMT"].ToString(), out bSuccess);
                       dDedPerEvntAmt = Conversion.CastToType<Double>(oDbReader["SIR_DED_PEREVENT_AMT"].ToString(), out bSuccess);

                        date = Conversion.ToDate(oDbReader["DATE_AGG_LIMIT_CHGD"].ToString());
                        CreateAndSetElement(xObjDeductibleNode, "DATE_DATA_CHANGED", date.ToString("MM/dd/yyyy"));
                        CreateAndSetElement(xObjDeductibleNode, "CHANGED_BY", oDbReader["AGG_LIMIT_CHGD_BY"].ToString());
                        //jira# RMA-6135.Multicurrency
                    //  CreateAndSetElement(xObjDeductibleNode, "AGGREGATE_LIMIT", oDbReader["AGG_LIMIT_AMT"].ToString());
                       CreateAndSetElement(xObjDeductibleNode, "AGGREGATE_LIMIT",CommonFunctions.ConvertCurrency(0, dAggrLimit, eNavType, m_sConnectionString, m_iClientId));
                       //jira# RMA-6135.Multicurrency
                        CreateAndSetElement(xObjDeductibleNode, "COV_GROUP_CODE", iCovGroupId.ToString());
                        //jira# RMA-6135.Multicurrency
                      //  CreateAndSetElement(xObjDeductibleNode, "DED_PER_EVENT", oDbReader["SIR_DED_PEREVENT_AMT"].ToString());
                        CreateAndSetElement(xObjDeductibleNode, "DED_PER_EVENT", CommonFunctions.ConvertCurrency(0, dDedPerEvntAmt, eNavType, m_sConnectionString, m_iClientId));
                        //jira# RMA-6135.Multicurrency
                        //Start - ExpenseFlagAddition - nbhatia6 - 07/03/14
                        CreateAndSetElement(xObjDeductibleNode, "EXCLUDE_EXPENSE_FLAG", oDbReader["EXCLUDE_EXPENSE_FLAG"].ToString());
                        //End - ExpenseFlagAddition - nbhatia6 - 07/03/14
                    }
                }
               
            }

            //XmlNodeList xAmts = xObjRootNode.SelectNodes("//AGGREGATE_LIMIT");
            //foreach (XmlNode xAmt in xAmts)
            //{
            //    if (!string.IsNullOrEmpty(xAmt.InnerText))
            //    {
            //        xAmt.InnerText = String.Format("{0:c}", Conversion.CastToType<Double>(xAmt.InnerText, out bSuccess));
            //    }
            //}

            //XmlNodeList xDedAmts = xObjRootNode.SelectNodes("//DED_PER_EVENT");
            //foreach (XmlNode xAmt in xDedAmts)
            //{
            //    if (!string.IsNullOrEmpty(xAmt.InnerText))
            //    {
            //        xAmt.InnerText = String.Format("{0:c}", Conversion.CastToType<Double>(xAmt.InnerText, out bSuccess));
            //    }
            //}

            XmlNodeList xCoverageGroups = xObjRootNode.SelectNodes("//COV_GROUP_CODE");
            foreach (XmlNode xCoverageGroup in xCoverageGroups)
            {
                if (!string.IsNullOrEmpty(xCoverageGroup.InnerText))
                {
                    xCoverageGroup.InnerText = this.GetCovGroupInfo(xCoverageGroup.InnerText);
                }
            }

            return xDoc;
        }

        public string GetEvaluationDate(int iClaimId, int iUnitRowId, int iDiminishingTypeCode, string sCvgEvaluationDate)
        {
            string sEvaluationDate = string.Empty;
            string sSql = string.Empty;
            int iBasic = 0;
            LocalCache objCache = null;

            try
            {
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                iBasic = objCache.GetCodeId("B", "DIMINISHING_TYPE");

                if (iBasic == iDiminishingTypeCode)
                {
                    
                    sSql  =  String.Format("SELECT MAX(COV_EVALUATION_DATE) FROM CLAIM_X_POL_DED WHERE CLAIM_ID = {0} AND POLICY_UNIT_ROW_ID = {1}", iClaimId, iUnitRowId);

                    sEvaluationDate = DbFactory.ExecuteAsType<string>(m_sConnectionString, sSql);
                }
                else
                {
                    sEvaluationDate = sCvgEvaluationDate;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }

            return sEvaluationDate;
        }

        public bool IsPaymentMade(int iCovGroupId)
        {
            StringBuilder sbSql = null;
           //Commented by Nikhil.Code review changes.
          
            int iCount = 0;
            try
            {
                if (iCovGroupId>0)
                {

                    sbSql = new StringBuilder();
                   // sbSql.Append("SELECT COUNT(*) FROM FUNDS_X_DED_COL_MAP FXC ");
                   // sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON FXC.POLCVG_ROW_ID = CXD.POLCVG_ROW_ID ");
                   ////PCR Changes by Nikhil on 17/12/2013 -  Start   
                    
                   // sbSql.Append(" INNER JOIN FUNDS F ON FXC.DED_PAY_TRANS_ID = F.TRANS_ID ");
                   // //PCR Changes by Nikhil on 17/12/2013 -  End
                   // sbSql.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXG ON PXG.POLICY_X_INSLINE_GROUP_ID=CXD.POLICY_X_INSLINE_GROUP_ID ");
                   // sbSql.Append(" WHERE (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG !=0) AND F.STATUS_CODE=").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));
                   // sbSql.Append(" AND (CXD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE"));
                   // sbSql.Append(" OR CXD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE")).Append(")");          
                   // sbSql.Append(" AND PXG.COV_GROUP_CODE=").Append(iCovGroupId);


                   
                    sbSql.Append(" SELECT COUNT(*) FROM FUNDS_TRANS_SPLIT FTS INNER JOIN FUNDS F ON F.TRANS_ID=FTS.TRANS_ID ");
                    sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.CLAIM_ID=F.CLAIM_ID AND CXPD.POLCVG_ROW_ID =FTS.POLCVG_ROW_ID ");
                    sbSql.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID=CXPD.POLICY_X_INSLINE_GROUP_ID");
                    sbSql.Append(" WHERE (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG <>0) AND F.STATUS_CODE=").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));
                    sbSql.Append(" AND (CXPD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE"));
                    sbSql.Append(" OR CXPD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE")).Append(")");
                    sbSql.Append(" AND PXIG.COV_GROUP_CODE=").Append(iCovGroupId);
                    iCount = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());
                }
            }
            catch
            {
                throw;
            }
            finally
            {
              
            }

            return (iCount > 0);
        }
        private bool IsPaymentMade(int iClaimId, int iDedId)
        {
            StringBuilder sbSql = null;
            //Commented by Nikhil.Code review changes.
         
            int iCount = 0;
            string sPolicyNumber = string.Empty;
            string sPolicySymbol = string.Empty;
            string sPolicyModlule = string.Empty;
            string sPolicyMasterCompany = string.Empty;
            string sPolicyLocation = string.Empty;
            string sCovProdLine = string.Empty;
            string sCovAslLine = string.Empty;
            string sUnitInsLine = string.Empty;
            int iCovTypeCode = 0;
            DbReader oDbReader = null;
            bool bSuccess = false;
            string sStatUnitNumber = string.Empty;
            string sClassCode = string.Empty;
            string sSublineCode = string.Empty;
            try
            {
             
                    sbSql = new StringBuilder();
                   
                    sbSql.Length = 0;
                    
                    
                    sbSql.Append(" SELECT PXCT.COVERAGE_CLASS_CODE,PXCT.SUB_LINE,PUD.STAT_UNIT_NUMBER,PXCT.COVERAGE_TYPE_CODE,PXCT.PRODLINE,PXCT.ASLINE,P.POLICY_NUMBER,P.MODULE,P.POLICY_SYMBOL,P.MASTER_COMPANY,P.LOCATION_COMPANY,PUD.INS_LINE FROM POLICY_X_CVG_TYPE PXCT");    
                    sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLCVG_ROW_ID= PXCT.POLCVG_ROW_ID");
                    sbSql.Append(" INNER JOIN POLICY P ON P.POLICY_ID=CXPD.POLICY_ID");
                    sbSql.Append(" INNER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID=CXPD.POLICY_UNIT_ROW_ID AND CXPD.POLICY_ID=PXU.POLICY_ID");
                    sbSql.Append(" INNER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID=PXU.UNIT_ID AND PUD.UNIT_TYPE=PXU.UNIT_TYPE");
                    sbSql.Append(" WHERE CXPD.CLM_X_POL_DED_ID=").Append(iDedId);          
                    sbSql.Append(" AND CXPD.CLAIM_ID=").Append(iClaimId);
                    using (oDbReader = DbFactory.ExecuteReader(m_sConnectionString,sbSql.ToString()))
                    {
                        if (oDbReader.Read())
                        {

                            iCovTypeCode = Conversion.CastToType<int>(Convert.ToString(oDbReader["COVERAGE_TYPE_CODE"]),out bSuccess);
                            sCovProdLine = Convert.ToString(oDbReader["PRODLINE"]);
                            sCovAslLine = Convert.ToString(oDbReader["ASLINE"]);
                            sPolicyNumber = Convert.ToString(oDbReader["POLICY_NUMBER"]);
                            sPolicySymbol = Convert.ToString(oDbReader["POLICY_SYMBOL"]);
                            sPolicyModlule = Convert.ToString(oDbReader["MODULE"]);
                            sPolicyMasterCompany = Convert.ToString(oDbReader["MASTER_COMPANY"]);
                            sPolicyLocation = Convert.ToString(oDbReader["LOCATION_COMPANY"]);                           
                            sUnitInsLine = Convert.ToString(oDbReader["INS_LINE"]);
                            sStatUnitNumber = Convert.ToString(oDbReader["STAT_UNIT_NUMBER"]);
                            sClassCode = Convert.ToString(oDbReader["COVERAGE_CLASS_CODE"]);
                            sSublineCode = Convert.ToString(oDbReader["SUB_LINE"]);
                        }
                    }
                    sbSql.Length = 0;
                    sbSql.Append(" SELECT COUNT(*) FROM FUNDS_TRANS_SPLIT FTS INNER JOIN FUNDS F ON F.TRANS_ID=FTS.TRANS_ID ");
                    sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.CLAIM_ID=F.CLAIM_ID AND CXPD.POLCVG_ROW_ID =FTS.POLCVG_ROW_ID ");
                    sbSql.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID=CXPD.POLICY_X_INSLINE_GROUP_ID");
                    sbSql.Append(" WHERE (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG <>0) AND F.STATUS_CODE=").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));
                    sbSql.Append(string.Format(" AND PXIG.POLICY_NUMBER='{0}' AND PXIG.POLICY_SYMBOL='{1}' AND PXIG.MODULE='{2}' AND PXIG.MASTER_COMPANY='{3}' AND PXIG.LOCATION_COMPANY='{4}'", new object[]{sPolicyNumber,sPolicySymbol,sPolicyModlule,sPolicyMasterCompany,sPolicyLocation}));
                    
                    //sbSql.Append(string.Format(" AND ISNULL(PXIG.INS_LINE,'')='{0}' AND ISNULL(PXIG.PRODLINE,'')='{1}' AND ISNULL(PXIG.ASLINE,'')='{2}' AND PXIG.COVERAGE_CODE={3} AND ISNULL(PXIG.STAT_UNIT_NUMBER,'')='{4}' AND ISNULL(PXIG.COVERAGE_CLASS_CODE,'')='{5}' AND ISNULL(PXIG.COVERAGE_SUBLINE_CODE,'')='{6}' ", new object[] { sUnitInsLine, sCovProdLine, sCovAslLine, iCovTypeCode, sStatUnitNumber, sClassCode, sSublineCode }));
                    sbSql.Append(string.Format(" AND COALESCE(PXIG.INS_LINE,'')='{0}' AND COALESCE(PXIG.PRODLINE,'')='{1}' AND COALESCE(PXIG.ASLINE,'')='{2}' AND PXIG.COVERAGE_CODE={3} AND COALESCE(PXIG.STAT_UNIT_NUMBER,'')='{4}' AND COALESCE(PXIG.COVERAGE_CLASS_CODE,'')='{5}' AND COALESCE(PXIG.COVERAGE_SUBLINE_CODE,'')='{6}' ", new object[] { sUnitInsLine, sCovProdLine, sCovAslLine, iCovTypeCode, sStatUnitNumber, sClassCode, sSublineCode }));
                    iCount = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString()); 
                //end: added by nitin goel


            }
            catch
            {
                throw;
            }
            finally
            {
            
            }

            return (iCount > 0);
        }
        private bool IsPaymentMadeOnCoverage(int iClaimId, int iDedId)
        {
            StringBuilder sbSql = null;
          
            int iCount = 0; 
            try
            {
               
                sbSql = new StringBuilder();               
                sbSql.Append(" SELECT COUNT(*) FROM FUNDS_TRANS_SPLIT FTS INNER JOIN FUNDS F ON F.TRANS_ID=FTS.TRANS_ID ");
                sbSql.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.CLAIM_ID=F.CLAIM_ID AND CXPD.POLCVG_ROW_ID =FTS.POLCVG_ROW_ID ");
                sbSql.Append(" WHERE (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG <>0) AND F.STATUS_CODE=").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));
                sbSql.Append(" AND CXPD.CLAIM_ID=").Append(iClaimId);
                sbSql.Append(" AND CXPD.CLM_X_POL_DED_ID=").Append(iDedId);                
                iCount = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());                
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }

            return (iCount > 0);
        }
        private bool IsExpenseReserveSetupOnCovGroup(int iCovGroupId)
        {
            StringBuilder sbSql = null;
         
            int iCount = 0;
            try
            {
             
                sbSql = new StringBuilder();
                if (iCovGroupId > 0)
                {
                    sbSql.Length = 0;
                    sbSql.Append(" SELECT COUNT(*) FROM RESERVE_CURRENT RC INNER JOIN CLAIM_X_POL_DED CXPD ON RC.POLCVG_ROW_ID=CXPD.POLCVG_ROW_ID AND RC.CLAIM_ID=CXPD.CLAIM_ID ");
                    sbSql.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID=PXIG.POLICY_X_INSLINE_GROUP_ID ");
                    sbSql.Append(" INNER JOIN CODES C ON C.CODE_ID=RC.RESERVE_TYPE_CODE ");
                    sbSql.Append(" WHERE RC.RESERVE_AMOUNT>0 ");
                    sbSql.Append(" ANDPXIG.COV_GROUP_CODE= ").Append(iCovGroupId);
                    sbSql.Append(" AND C.RELATED_CODE_ID= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE"));
                    iCount = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());                    
                }               
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }

            return (iCount > 0);
        }
        private bool IsExpenseReserveSetupOnCoverage(int iClaimId, int iDedId)
        {
            StringBuilder sbSql = null;
       
            int iCount = 0;
            try
            {
             
                sbSql = new StringBuilder();
                sbSql.Append(" SELECT COUNT(*)  FROM RESERVE_CURRENT RC INNER JOIN CLAIM_X_POL_DED CXPD ON RC.POLCVG_ROW_ID=CXPD.POLCVG_ROW_ID AND RC.CLAIM_ID=CXPD.CLAIM_ID ");
                sbSql.Append(" INNER JOIN CODES C ON C.CODE_ID=RC.RESERVE_TYPE_CODE ");
                sbSql.Append(" WHERE RC.RESERVE_AMOUNT>0 "); 
                sbSql.Append(" AND CXPD.CLAIM_ID=").Append(iClaimId);
                sbSql.Append(" AND CXPD.CLM_X_POL_DED_ID=").Append(iDedId);
                iCount = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }

            return (iCount > 0);
        }
        private int IsPaymentMade(int iUnitRowId, int iClaimId, int iDimTypeCode)
        {
            string sSql = string.Empty;
            int iCount = 0;
          
            int iDiminishingTypeCode = 0;
            int iReturn = 0;
            StringBuilder sbSQL = new StringBuilder(string.Empty);
            try
            {
           

                iDiminishingTypeCode = m_objDataModelFactory.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE");
                //start  -changed on Nikhil on 01/06/15
                //sbSQL.Append(" SELECT  COUNT(*) from FUNDS_X_DED_COL_MAP FXC ");
                //    sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON FXC.POLCVG_ROW_ID = CXD.POLCVG_ROW_ID ");                    
                //   //Start:added by Nitin goel, Issue fix during unit testing of NI PCRs changes
				
                //    sbSQL.Append(" INNER JOIN FUNDS F ON FXC.DED_PAY_TRANS_ID = F.TRANS_ID ");
                    //end:added by Nitin goel, Issue fix
                    sbSQL.Append(" SELECT COUNT(*) FROM FUNDS_TRANS_SPLIT FTS INNER JOIN FUNDS F ON F.TRANS_ID=FTS.TRANS_ID ");
                    sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.CLAIM_ID=F.CLAIM_ID AND CXD.POLCVG_ROW_ID =FTS.POLCVG_ROW_ID ");
                    //end -  changed on Nikhil on 01/06/15
				    sbSQL.Append(" WHERE CXD.DIMNSHNG_TYPE_CODE = ").Append(iDiminishingTypeCode);
                    sbSQL.Append(" AND CXD.CLAIM_ID = ").Append(iClaimId);
                    sbSQL.Append(" AND CXD.POLICY_UNIT_ROW_ID = " ).Append( iUnitRowId);
                                //Start:added by Nitin goel, Issue fix during unit testing of NI PCRs changes
				  
                    sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG !=0) AND F.STATUS_CODE=").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));
                             //end:added by Nitin goel, Issue fix during unit testing of NI PCRs changes
                    iCount = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(Convert.ToString(sbSQL));
            }
            catch
            {
                throw;
            }
            finally
            {
                
            }

            if (iCount > 0)
            {
                if (iDimTypeCode == iDiminishingTypeCode)
                {
                    iReturn = 1;
                }
                else
                {
                    iReturn = 2;
                }
            }

            return iReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        public void GetDiminishingDetails(XElement p_objXmlIn)
        {            
            int iNumberOfYears = 0;
            double dDimPercent = 0d;
            int iDimTypeCodeId = 0;
            int iClaimId = 0;
            double dDeductibleAmt = 0d;
            double dReducedAmt = 0d;
            bool bSuccess = false;
            int iClaimXPolDedId = 0;
            int iLossDate = 0;
            int iDimEvalDate = 0;
            string sCvgEvaluationDate = string.Empty;
            string sDimEvaluationDate = string.Empty;
            string sLossDate = string.Empty;
            bool bDataFromPoint = false;

            XElement xDiminishingTypeCode = null;
            XElement xClaimId = null;
            XElement xDeductibleAmount = null;
            XElement xReducedAmount = null;
            XElement xRemainingAmount = null;
            XElement xDiminishingpercentage = null;
            XElement xClaimXPolDedId = null;
            XElement xCvgEvaluationDate = null;
            XElement xDimEvaluationDate = null;

            NumberStyles nsStyle = NumberStyles.AllowCurrencySymbol | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;     //Added by Sumit Agarwal on 11/07/2014 for NI

            Claim objClaim = null;
            //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory
        
            ClaimXPolDed objClaimXPolDed = null;

            xDiminishingTypeCode = p_objXmlIn.XPathSelectElement("//control[@name='DiminishingTypeCode']");
            xClaimId = p_objXmlIn.XPathSelectElement("//ClaimId");
            xDeductibleAmount = p_objXmlIn.XPathSelectElement("//control[@name='SirDedAmt']");
            xReducedAmount = p_objXmlIn.XPathSelectElement("//control[@name='ReducedAmt']");
            xRemainingAmount = p_objXmlIn.XPathSelectElement("//control[@name='RemainingAmt']");
            xDiminishingpercentage = p_objXmlIn.XPathSelectElement("//control[@name='DiminishingPercentage']");
            xClaimXPolDedId = p_objXmlIn.XPathSelectElement("//CLM_X_POL_DED_ID");
            xCvgEvaluationDate = p_objXmlIn.XPathSelectElement("//control[@name='CoverageEvaluationDate']");
            xDimEvaluationDate = p_objXmlIn.XPathSelectElement("//control[@name='DiminishingEvaluationDate']");

            if (xClaimId != null)
            {
                iClaimId = Conversion.CastToType<int>(xClaimId.Value, out bSuccess);
            }
            if (xClaimXPolDedId != null)
            {
                iClaimXPolDedId = Conversion.CastToType<int>(xClaimXPolDedId.Value, out bSuccess);
            }
            if (xDeductibleAmount != null)
            {
                
                dDeductibleAmt = double.Parse(xDeductibleAmount.Value, nsStyle);
            }
            if (xDiminishingTypeCode != null && xDiminishingTypeCode.Attribute("codeid") != null)
            {
                iDimTypeCodeId = Conversion.CastToType<Int32>(xDiminishingTypeCode.Attribute("codeid").Value, out bSuccess);
            }
            
           
            objClaim = m_objDataModelFactory.GetDataModelObject("Claim", false) as Claim;
            objClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
            
            objClaim.MoveTo(iClaimId);
            objClaimXPolDed.MoveTo(iClaimXPolDedId);

            //If coverage Evaluation date is already available - the point system was not used
            if (!string.IsNullOrEmpty(objClaimXPolDed.DimEvaluationDate))
            {
                sCvgEvaluationDate = objClaimXPolDed.DimEvaluationDate;
            }
            else if (xDimEvaluationDate != null && !string.IsNullOrEmpty(xDimEvaluationDate.Value))
            {
                sCvgEvaluationDate = Conversion.GetDBDateFormat(xDimEvaluationDate.Value, "yyyyMMdd");
            }
            else
            {
                bDataFromPoint = true;
                dDimPercent = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DimPercentNum;
                sCvgEvaluationDate = xCvgEvaluationDate.Value;
            }
            sDimEvaluationDate = this.GetEvaluationDate(objClaimXPolDed.ClaimId, objClaimXPolDed.UnitRowId, iDimTypeCodeId, sCvgEvaluationDate);

            //because the coverage date is not yet saved - so it is possible 
            //that the actual evaluation date is greater than the date in table.
            if (string.Compare(sDimEvaluationDate, sCvgEvaluationDate, false) < 0)
            {
                //Control can reach here only when the date is read from point
                sDimEvaluationDate = sCvgEvaluationDate;
            }
            if (objClaim.Context.InternalSettings.SysSettings.PolicyCvgType == 0)
            {
                sLossDate = objClaim.DateOfClaim;
            }
            else
            {
                sLossDate = ((Event)objClaim.Parent).DateOfEvent;
            }
            if (bDataFromPoint)
            {
                iLossDate = Conversion.CastToType<int>(sLossDate, out bSuccess);
                iDimEvalDate = Conversion.CastToType<int>(sDimEvaluationDate, out bSuccess);
                //Start:Modified by Nitin Goel,JIRA 6826,01/14/2015
                //iNumberOfYears = (int)((iLossDate - iDimEvalDate) / 1000d);
                iNumberOfYears = (int)((iLossDate - iDimEvalDate) / 10000d);
                //End:Modified by Nitin Goel,JIRA 6826,01/14/2015
                dDimPercent *= iNumberOfYears;
            }
            else
            {
                iNumberOfYears = 1;
                bSuccess = false;
                if (xDiminishingpercentage!=null && !string.IsNullOrEmpty(xDimEvaluationDate.Value))
                {
                    dDimPercent = Conversion.CastToType<double>(xDimEvaluationDate.Value, out bSuccess);
                }
                if (!bSuccess)
                {
                    dDimPercent = objClaimXPolDed.DimPercentNum;
                }
            }

            sCvgEvaluationDate = Conversion.GetDBDateFormat(sCvgEvaluationDate, "MM/dd/yyyy");
            sDimEvaluationDate = Conversion.GetDBDateFormat(sDimEvaluationDate, "MM/dd/yyyy");
            
            if (dDimPercent >100d)
            {
                dDimPercent = 100d;
            }
            else if (dDimPercent<0d)
            {
                dDimPercent = 0d;
            }
            dReducedAmt = objClaimXPolDed.CalculateReducedDeductibleAmount(dDeductibleAmt, dDimPercent);

            if (xReducedAmount != null && xRemainingAmount != null && xDiminishingpercentage != null && xDimEvaluationDate != null && xCvgEvaluationDate != null)
            {
                xReducedAmount.Value = dReducedAmt.ToString();
                xRemainingAmount.Value = dReducedAmt.ToString();
                xDiminishingpercentage.Value = dDimPercent.ToString();
                xCvgEvaluationDate.Value = sCvgEvaluationDate;
                xDimEvaluationDate.Value = sDimEvaluationDate;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="oRequest"></param>
        /// <returns></returns>
        public bool IsCoverageEvalDateAvailable(XElement p_objXmlIn, PolicyEnquiry oRequest)
        {
            //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory
            
            ClaimXPolDed objClaimXPolDed = null;
            DbReader objDbreader = null;
            XElement xClaimXPolDedId = null;
            int iClaimXPolDedId = 0;
            bool bIsCvgEvalDateAvailable = false;
            bool bSuccess = false;
            StringBuilder sbSql = new StringBuilder(string.Empty);
            try
            {
                xClaimXPolDedId = p_objXmlIn.XPathSelectElement("//CLM_X_POL_DED_ID");
                if (xClaimXPolDedId != null)
                {
                    iClaimXPolDedId = Conversion.CastToType<int>(xClaimXPolDedId.Value, out bSuccess);
                    if (iClaimXPolDedId > 0)
                    {
                        
                        objClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;

                        objClaimXPolDed.MoveTo(iClaimXPolDedId);
                        if (!string.IsNullOrEmpty(objClaimXPolDed.DimEvaluationDate))
                        {
                            bIsCvgEvalDateAvailable = true;
                        }
                    }
                }

                if (!bIsCvgEvalDateAvailable)
                {
                    sbSql.Append(" SELECT P.POLICY_SYMBOL, P.POLICY_NUMBER,P.MODULE, P.POLICY_SYSTEM_ID, P.LOCATION_COMPANY, P.MASTER_COMPANY, CVG.TRANS_SEQ_NO, CVG.CVG_SEQUENCE_NO, PUD.STAT_UNIT_NUMBER");
                        sbSql.Append(" FROM POLICY_X_CVG_TYPE CVG");
                        sbSql.Append(" INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID=PXU.POLICY_UNIT_ROW_ID");
                        sbSql.Append(" INNER JOIN POLICY P ON PXU.POLICY_ID=P.POLICY_ID");
                        sbSql.Append(" INNER JOIN CLAIM_X_POLICY CXP ON P.POLICY_ID=CXP.POLICY_ID");
                        sbSql.Append(" INNER JOIN POINT_UNIT_DATA PUD ON PXU.UNIT_ID = PUD.UNIT_ID");
                        sbSql.Append(" WHERE PXU.UNIT_TYPE = PUD.UNIT_TYPE");
                        sbSql.Append(string.Format(" AND CXP.CLAIM_ID={0} AND P.POLICY_ID={1} AND POLCVG_ROW_ID={2}", objClaimXPolDed.ClaimId, objClaimXPolDed.PolicyId, objClaimXPolDed.PolcvgRowId));

                        using (objDbreader = m_objDataModelFactory.Context.DbConnLookup.ExecuteReader(Convert.ToString(sbSql)))
                    {
                        if (objDbreader.Read())
                        {
                            oRequest.PolicySymbol = objDbreader["POLICY_SYMBOL"].ToString();
                            oRequest.PolicyNumber = objDbreader["POLICY_NUMBER"].ToString();
                            oRequest.Module = objDbreader["MODULE"].ToString();
                            oRequest.Location = objDbreader["LOCATION_COMPANY"].ToString();
                            oRequest.MasterCompany = objDbreader["MASTER_COMPANY"].ToString();
                            oRequest.TransSeq = objDbreader["TRANS_SEQ_NO"].ToString();
                            oRequest.CovSeqNo = objDbreader["CVG_SEQUENCE_NO"].ToString();
                            oRequest.UnitNumber = objDbreader["STAT_UNIT_NUMBER"].ToString();
                            oRequest.PolicySystemId = Conversion.CastToType<int>(objDbreader["POLICY_SYSTEM_ID"].ToString(), out bSuccess);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (objClaimXPolDed != null)
                {
                    objClaimXPolDed.Dispose();
                }
               
                if (objDbreader != null)
                {
                    objDbreader.Dispose();
                }
            }
            return bIsCvgEvalDateAvailable;
        }

        /// <summary>
        /// Saves deductible into database
        /// </summary>
        /// <param name="iClaimXPolDedId"></param>
        /// <param name="iPOLCVG_ROW_ID"></param>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <returns></returns>
        public bool SaveDeductible(int iClaimXPolDedId, int iPOLCVG_ROW_ID, XmlDocument p_objXmlIn, XmlDocument p_objXmlOut)
        {
            double dDedSirAmt = 0d;
            bool bSuccess = true;
            double dReducedAmt = 0d;
            double dDimPercentNum = 0d;
            XmlNode xDedSirAmt = null;
            XmlNode xDedTypeCode = null;
            XmlNode xRemainingAmt = null;
            XmlNode xExcludeExpenseFlag = null;
            XmlNode xDiminishingTypeCode = null;
         //Commented by Nikhil on 09/18/14
            
            XmlNode xDimPercent = null;
            XmlNode xReducedDeductibleAmt = null;
            XmlNode xCvgEvalDate = null;
            XmlNode xCoverageGroup = null;
            //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory.Replaced objFacotry with  m_objDataModelFactory
           
            ClaimXPolDed objClaimXPolDed = null;
            Claim objClaim = null;
            //PCR Changes by Nikhil on 17/12/2013 for calculate remaining amount -  Start
            XmlNode xCoverageGroupID = null;
            int iFPTypeCode = 0;
            double dRemianingAmt = 0d; ;
            int iCovGroupId = 0;
            int iPreventEditDedPerEvent = 0;            
            //PCR Changes by Nikhil on 17/12/2013 for calculate remaining amount -  End
            //Added By Nikhil on 01/08/2014 for validate deductible amount with group aggregate limit  - Start
            double dGroupAggrLimit = 0d;
            //Added By Nikhil on 01/08/2014 for validate deductible amount with group aggregate limit  - End
            StringBuilder sbSql = new StringBuilder(string.Empty);
            int iCountExpenseReserve = 0;
            try
            {
               // objFacotry = new DataModelFactory(m_UserLogin);
                objClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
                objClaim = m_objDataModelFactory.GetDataModelObject("Claim", false) as Claim;

                if (iClaimXPolDedId > 0)
                {
                    objClaimXPolDed.MoveTo(iClaimXPolDedId);
                    objClaim.MoveTo(objClaimXPolDed.ClaimId);
                }

                xDedSirAmt = p_objXmlIn.SelectSingleNode("//control[@name='SirDedAmt']");
                xRemainingAmt = p_objXmlIn.SelectSingleNode("//control[@name='RemainingAmt']");
                xExcludeExpenseFlag = p_objXmlIn.SelectSingleNode("//control[@name='ExcludeExpenseFlag']");
                xDedTypeCode = p_objXmlIn.SelectSingleNode("//control[@name='DedTypeCode']");
                xDiminishingTypeCode = p_objXmlIn.SelectSingleNode("//control[@name='DiminishingTypeCode']");
                //Commented by Nikhil on 09/18/14
                
                xDimPercent = p_objXmlIn.SelectSingleNode("//control[@name='DiminishingPercentage']");
                xReducedDeductibleAmt = p_objXmlIn.SelectSingleNode("//control[@name='ReducedAmt']");
                xCvgEvalDate = p_objXmlIn.SelectSingleNode("//control[@name='CoverageEvaluationDate']");
                xCoverageGroup = p_objXmlIn.SelectSingleNode("//control[@name='CovGroupId']");

                xCoverageGroupID = p_objXmlIn.SelectSingleNode("//control[@name='hdn_CovGroup']");


                if (xDedSirAmt != null && !string.IsNullOrEmpty(xDedSirAmt.InnerText))
                {
                    dDedSirAmt = Conversion.CastToType<Double>(xDedSirAmt.InnerText, out bSuccess);
                }
              //Start:added by Nitin goel, If Exclude Expense flag is modified for any coverarge which is part of the group then exclude expense flag should be modified for other coverages also which are part of the same group.
                //Start -  JIRA # rma-6292
                if (dDedSirAmt < 0)
                {
                    throw new RMAppException(Globalization.GetString("DeductibleManager.AmountLessThanZero.Error", m_iClientId));
                }
                //end -  JIRA # rma-6292

                if (xExcludeExpenseFlag !=null && !String.IsNullOrEmpty(xExcludeExpenseFlag.InnerText))
                {
                    //Start:added by Nitin goel, If Exclude Expense flag is modified for any coverarge which is part of the group then exclude expense flag should be modified for other coverages also which are part of the same group.
                    if (iClaimXPolDedId > 0)
                    {
                        if (objClaimXPolDed.ExcludeExpenseFlag != Conversion.CastToType<bool>(xExcludeExpenseFlag.InnerText, out bSuccess))
                        {
                            objClaimXPolDed.IsExcludeExpenseFlagModified = true;
                        }
                    }
                    //end:added by Nitin goel
                    objClaimXPolDed.ExcludeExpenseFlag = Conversion.CastToType<bool>(xExcludeExpenseFlag.InnerText, out bSuccess);

                    
                }                



                if (xDedTypeCode != null && xDedTypeCode.Attributes["codeid"] != null)
                {
                    objClaimXPolDed.DedTypeCode = Conversion.CastToType<Int32>(xDedTypeCode.Attributes["codeid"].Value, out bSuccess);
                }
                if (xDiminishingTypeCode != null && xDiminishingTypeCode.Attributes["codeid"] != null && !string.IsNullOrEmpty(xDiminishingTypeCode.Attributes["codeid"].Value))
                {
                    objClaimXPolDed.DiminishingTypeCode = Conversion.CastToType<Int32>(xDiminishingTypeCode.Attributes["codeid"].Value, out bSuccess);
                }
             
                if (xDimPercent != null && !String.IsNullOrEmpty(xDimPercent.InnerText))
                {
                    dDimPercentNum = Conversion.CastToType<double>(xDimPercent.InnerText, out bSuccess);
                }
                if (xCvgEvalDate!=null && objClaimXPolDed.DiminishingTypeCode>0)
                {
               
                    objClaimXPolDed.DimEvaluationDate = Conversion.GetDate(xCvgEvalDate.InnerText);
                }
               
                if (xCoverageGroupID != null && !string.IsNullOrEmpty(xCoverageGroupID.InnerText))
                {
                    iCovGroupId = Conversion.CastToType<int>(xCoverageGroupID.InnerText, out bSuccess);
                }

                
                if (objClaimXPolDed.SirDedAmt != dDedSirAmt && iCovGroupId > 0)
                {
                   dGroupAggrLimit = DbFactory.ExecuteAsType<double>(m_sConnectionString, "SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = " + iCovGroupId);
                   if (dGroupAggrLimit > 0 && dDedSirAmt > dGroupAggrLimit)
                   {
                       //Cloud changes by Nikhil for NI
                       throw new RMAppException(Globalization.GetString("DeductibleManager.DedAmountLimit.Error",m_iClientId));
                   }
                }

                //Changed By Nikhil on 01/08/2014 for validate deductible amount with group aggregate limit  - End



                IsDiminishingTypeValid(objClaimXPolDed.DiminishingTypeCode, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.UnitRowId);

                dReducedAmt = objClaimXPolDed.CalculateReducedDeductibleAmount(objClaimXPolDed.ClaimId, objClaimXPolDed.DiminishingTypeCode, objClaimXPolDed.DedTypeCode,
                         dDedSirAmt, dDimPercentNum, objClaimXPolDed.ClmXPolDedId);

                if (objClaimXPolDed.SirDedAmt!=dDedSirAmt || objClaimXPolDed.DimPercentNum != dDimPercentNum)
                {
                    objClaimXPolDed.DimPercentNum = dDimPercentNum;
                    //Start: added by nitin goel, 07/07/2014, to redistribute updated deductible amount among all recovery reserve.
                    
                    bSuccess = this.ModifyRecoveryreserves(dDedSirAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.PolicyId);
                    //End: added by Nitin goel
                    //PCR Changes by Nikhil on 17/12/2013 for calculate remaining amount -  Start
                  
                    iFPTypeCode = objClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");
                    if (iCovGroupId == 0 || objClaimXPolDed.DedTypeCode == 0 || objClaimXPolDed.DedTypeCode == iFPTypeCode)
                    {
                        dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode);
                    }
                    else
                    {
                       
                        iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
                        if (iPreventEditDedPerEvent == 0)
                            dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode, iCovGroupId, false);
                        else
                            dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dReducedAmt, objClaimXPolDed.ClaimId, iCovGroupId, false);
                        
                    }

                    xRemainingAmt.InnerText = dRemianingAmt.ToString();
                    //PCR Changes by Nikhil on 17/12/2013 for calculate remaining amount -  End
                    xReducedDeductibleAmt.InnerText = string.Format("{0:c}", dReducedAmt);
                }
                
                if (bSuccess)
                {
                    if (objClaimXPolDed.DedTypeCode == objClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"))
                    {
                        
                        xCoverageGroup.InnerText = "NA";
                    }
             

                    objClaimXPolDed.SirDedAmt = dDedSirAmt;
                    objClaimXPolDed.Save();

                    if (objClaimXPolDed.DiminishingTypeCode <= 0)
                    {
                        xReducedDeductibleAmt.InnerText = "NA";
                    }
                    p_objXmlOut.InnerXml = p_objXmlIn.InnerXml;
                }               
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objClaimXPolDed != null)
                {
                    objClaimXPolDed.Dispose();
                }
             
                if (objClaim!=null)
                {
                    objClaim.Dispose();
                }
            }
            return bSuccess;
        }

        /// <summary>
        /// Gets the history of the deductible
        /// </summary>
        /// <param name="iClaimXPolDedId">The Deductible Id</param>
        /// <returns>Output Xml</returns>
        public XmlDocument GetDeductibleHistory(int iClaimXPolDedId)
        {
            DbReader objReader = null;
            string sSql = string.Empty;
            XmlDocument xDoc = null;
            XmlElement objRootNode = null;
            XmlElement objDeductibleNode = null;
            bool bSuccess = false;
            DateTime date = new DateTime();
            double dReducedAmt = 0d;

            ClaimXPolDed objClaimXPolDed = null;
            //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory.
    
            objClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
            objClaimXPolDed.MoveTo(iClaimXPolDedId);

            xDoc = new XmlDocument();
            xDoc.InnerXml = string.Format("<DEDUCTIBLE_HISTORY ClaimXPolDedId=\"{0}\"></DEDUCTIBLE_HISTORY>", iClaimXPolDedId);
            objRootNode = (XmlElement)xDoc.SelectSingleNode("//DEDUCTIBLE_HISTORY");

            SortedList objSortedList = new SortedList();
            int iKeyValue = 0;
            foreach (ClaimXPolDedHist objHist in objClaimXPolDed.ClaimXPolDedHistList)
            {
                iKeyValue = objHist.ClmXPolDedHistId;
                objSortedList.Add(iKeyValue, objHist);
                objHist.Dispose();
            }

            ClaimXPolDedHist objPolDedHist = null;
            for (int i = objSortedList.Count - 1; i >= 0; i--)
            {
                objPolDedHist = objSortedList.GetByIndex(i) as ClaimXPolDedHist;

                date = Conversion.ToDate(objPolDedHist.DateDedctblChgd);
                CreateElement(objRootNode, "RECORD", ref objDeductibleNode);

                CreateAndSetElement(objDeductibleNode, "DATE_DATA_CHANGED", date.ToString("MM/dd/yyyy"));
                CreateAndSetElement(objDeductibleNode, "DEDUCTIBLE_TYPE", objPolDedHist.DedTypeCode.ToString());
                CreateAndSetElement(objDeductibleNode, "USER", objPolDedHist.DedctblChgdBy);
                //jira# RMA-6135.Multicurrency
              // CreateAndSetElement(objDeductibleNode, "AMOUNT", objPolDedHist.SirDedAmt.ToString());
                CreateAndSetElement(objDeductibleNode, "AMOUNT", CommonFunctions.ConvertCurrency(objClaimXPolDed.ClaimId, objPolDedHist.SirDedAmt, eNavType, m_sConnectionString, m_iClientId));
                //jira# RMA-6135.Multicurrency
                CreateAndSetElement(objDeductibleNode, "EXCLUDE_EXPENSE_FLAG", objPolDedHist.ExcludeExpenseFlag ? "-1" : "0");
                CreateAndSetElement(objDeductibleNode, "DIMINISHING_TYPE", objPolDedHist.DiminishingTypeCode.ToString());
                CreateAndSetElement(objDeductibleNode, "DIMINISHING_PERCENT", objPolDedHist.DimPercentNum.ToString());
                //Commented by Nikhil on 09/18/14
                //  CreateAndSetElement(objDeductibleNode, "CLAIMANT", objPolDedHist.Context.LocalCache.GetEntityLastFirstName(objPolDedHist.ClaimantEid));
                if (objPolDedHist.DiminishingTypeCode>0)
                {
                    dReducedAmt = objClaimXPolDed.CalculateReducedDeductibleAmount(objPolDedHist.SirDedAmt, objPolDedHist.DimPercentNum);
                    //jira# RMA-6135.Multicurrency
                  //  CreateAndSetElement(objDeductibleNode, "REDUCED_AMOUNT", dReducedAmt.ToString());
                    CreateAndSetElement(objDeductibleNode, "REDUCED_AMOUNT", CommonFunctions.ConvertCurrency(objClaimXPolDed.ClaimId, dReducedAmt, eNavType, m_sConnectionString, m_iClientId));
                    //jira# RMA-6135.Multicurrency
                }
                else
                {
                    CreateAndSetElement(objDeductibleNode, "REDUCED_AMOUNT", "NA");
                }
            }

            //ddhiman added 10/21/2014
            string sDiminishingFlag=string.Empty;
            sDiminishingFlag = Conversion.ConvertObjToStr(objPolDedHist.Context.InternalSettings.ColLobSettings[GetLOB(objClaimXPolDed.ClaimId)].DimnishingFlag);
            CreateAndSetElement(objDeductibleNode, "DIMNISHING_FLAG", sDiminishingFlag);
            //End ddhiman
            //jira# RMA-6135.Multicurrency
        //    XmlNodeList xAmts = objRootNode.SelectNodes("//AMOUNT");
            //foreach (XmlNode xAmt in xAmts)
            //{
            //    if (!string.IsNullOrEmpty(xAmt.InnerText))
            //    {
            //        xAmt.InnerText = String.Format("{0:c}", Conversion.CastToType<Double>(xAmt.InnerText, out bSuccess));
            //    }
            //}

            //XmlNodeList xRedAmts = objRootNode.SelectNodes("//REDUCED_AMOUNT");
            //foreach (XmlNode xAmt in xRedAmts)
            //{
            //    if (!string.IsNullOrEmpty(xAmt.InnerText) && string.Compare(xAmt.InnerText, "NA") != 0)
            //    {
            //        xAmt.InnerText = String.Format("{0:c}", Conversion.CastToType<Double>(xAmt.InnerText, out bSuccess));
            //    }
            //}
            //jira# RMA-6135.Multicurrency
            XmlNodeList xDedTypes = objRootNode.SelectNodes("//DEDUCTIBLE_TYPE");
            foreach (XmlNode xDedType in xDedTypes)
            {
                if (!string.IsNullOrEmpty(xDedType.InnerText))
                {
                    xDedType.InnerText = this.GetCodeInfo(xDedType.InnerText);
                }
            }

            XmlNodeList xDiminishingTypes = objRootNode.SelectNodes("//DIMINISHING_TYPE");
            foreach (XmlNode xDimType in xDiminishingTypes)
            {
                if (!string.IsNullOrEmpty(xDimType.InnerText))
                {
                    xDimType.InnerText = this.GetCodeInfo(xDimType.InnerText);
                }
            }

            if (objReader!=null)
            {
                objReader.Dispose();
            }
            return xDoc;
        }

        #region private methods
        private bool IsDiminishingTypeValid(int iDimType, int iClaimId, int iPolCvgRowId, int iUnitRowId)
        {
            string sSql = string.Empty;
            LocalCache objCache = null;
            int iBasicDim = 0;
            int iEnhancedDim = 0;
            int iDimTypeToCheck = 0;
            int iCount = 0;

            objCache = new LocalCache(m_sConnectionString, m_iClientId);
            iBasicDim = objCache.GetCodeId("B", "DIMINISHING_TYPE");
            iEnhancedDim = objCache.GetCodeId("E", "DIMINISHING_TYPE");

            if (iDimType == iBasicDim)
            {
                iDimTypeToCheck = iEnhancedDim;
            }
            else if (iDimType == iEnhancedDim)
            {
                iDimTypeToCheck = iBasicDim;
            }
            else
            {
                return true;
            }

            sSql = "SELECT COUNT(*) FROM CLAIM_X_POL_DED WHERE DIMNSHNG_TYPE_CODE=" + iDimTypeToCheck
                + " AND CLAIM_ID=" + iClaimId + " AND POLICY_UNIT_ROW_ID=" + iUnitRowId;
            iCount = DbFactory.ExecuteAsType<int>(m_sConnectionString, sSql);

            if (iCount > 0)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("DeductibleManager.InvalidDiminishingType",m_iClientId));
            }

            return true;
        }
        
        private void Claimants(XmlNode xNode, int iClaimId)
        {
          
            Claim objClaim = null;
            XmlElement xElement = null;

          
            objClaim = m_objDataModelFactory.GetDataModelObject("Claim", false) as Claim;
            objClaim.MoveTo(iClaimId);

            foreach (Claimant objClaimant in objClaim.ClaimantList)
            {
                CreateAndSetElement((XmlElement)xNode, "option", objClaimant.Context.LocalCache.GetEntityLastFirstName(objClaimant.ClaimantEid), ref xElement);
                xElement.SetAttribute("value", objClaimant.ClaimantEid.ToString());
            }
        }

        public bool ModifyRecoveryreserves(double dAmt, int iClaimId, int iPolCvgRowId, int iPolicyId)
        {
            ReserveCurrent objReserve = null;
            //Commented by Nikhil.Code review changes.Replaced objFacotry with  m_objDataModelFactory.
          
            Claim objClaim = null;
            StringBuilder sbSql = null;
            DbReader objReader = null;
            bool bModifySuccess = false;
            int iReserveTypeCode=0;
            int iRcRowId = 0;
            int iClaimantEid = 0;
            int iLossId = 0;
            //Start - Added adjuster ID 
            int iAdjusterEID = 0;
            //end - Added adjuster ID 
            bool bSuccess = false;
            ReserveFunds reserveFunds = null;
            Hashtable hashtable = new Hashtable();
            int iStatusCode = 0;

            try
            {
                sbSql = new StringBuilder(string.Empty);
            
                objClaim = m_objDataModelFactory.GetDataModelObject("Claim", false) as Claim;

                objClaim.MoveTo(iClaimId);
                iReserveTypeCode = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecReserveType;
                iStatusCode = objClaim.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS");

                sbSql.Append("SELECT RC.RC_ROW_ID, RC.CLAIMANT_EID,RC.POLCVG_LOSS_ROW_ID,RC.ASSIGNADJ_EID FROM RESERVE_CURRENT RC ");
                sbSql.Append("INNER JOIN COVERAGE_X_LOSS CXL ON CXL.CVG_LOSS_ROW_ID = RC.POLCVG_LOSS_ROW_ID ");
                sbSql.Append(string.Format("WHERE CXL.POLCVG_ROW_ID={0} ", iPolCvgRowId));
                sbSql.Append(string.Format("AND RC.CLAIM_ID={0} ", iClaimId));
                sbSql.Append(string.Format("AND RC.RESERVE_TYPE_CODE={0}", iReserveTypeCode));
                sbSql.Append(" ORDER BY RC.RC_ROW_ID ");

                reserveFunds = new ReserveFunds(m_sConnectionString, m_UserLogin, m_iClientId);
                objReserve = m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false) as ReserveCurrent;
                using (objReader = m_objDataModelFactory.Context.DbConnLookup.ExecuteReader(sbSql.ToString()))
                {
                    //Start: Added by Nitin goel, commented Edit Reserve function,07/07/2014
                    while (objReader.Read())
             
                    //End:Nitin Goel,07/07/2014
                    {
                        iRcRowId = Conversion.CastToType<int>(objReader["RC_ROW_ID"].ToString(), out bSuccess);
                        iClaimantEid = Conversion.CastToType<int>(objReader["CLAIMANT_EID"].ToString(), out bSuccess);
                        iLossId = Conversion.CastToType<int>(objReader["POLCVG_LOSS_ROW_ID"].ToString(), out bSuccess);
                        //Start - Added adjuster ID
                        iAdjusterEID = Conversion.CastToType<int>(objReader["ASSIGNADJ_EID"].ToString(), out bSuccess);
                        //End - Added adjuster ID
                        if (iRcRowId > 0)
                        {
                          
                            //Start:added by nitin goel to handle case of multiple deductible reserve and multiple claimant.
                            reserveFunds.EditRecoveryReserveBasedOnDedAmt(ref hashtable, iClaimId, iClaimantEid, iPolicyId, iPolCvgRowId, iReserveTypeCode, iAdjusterEID, dAmt, iStatusCode, "Deductibles Updated", iLossId);
                            //End:Nitin Goel,07/07/2014
                        }
                    }
                }

                bModifySuccess = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objReserve != null)
                {
                    objReserve.Dispose();
                }
             
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
            }

            return bModifySuccess;
        }

        private string GetUnit(string sUnitId, string sUnitType)
        {
            string sUnitName = string.Empty;
            DbConnection objDbConnection = null;
            StringBuilder sSql = null;
            DbReader objReader = null;
            string sUnitDetails =string.Empty;
            string sVehcicleDesc = string.Empty;
            string sVehicleVin =string.Empty;
            try
            {
                sSql = new StringBuilder();
                if (String.Compare(sUnitType, "V", true) == 0)
                {
                    sUnitName = "Vehicle Unit: ";
                    sSql.Append("SELECT V.VEH_DESC,V.VIN ");
                    sSql.Append("FROM VEHICLE V ");
                    sSql.Append("WHERE V.UNIT_ID=");
                    sSql.Append(sUnitId);

                    using (objReader = m_objDataModelFactory.Context.DbConnLookup.ExecuteReader(sSql.ToString()))
                    {
                        if (objReader.Read())
                        {
                            if (objReader["VEH_DESC"] != null && objReader["VEH_DESC"] != DBNull.Value)
                                sVehcicleDesc = objReader.GetString("VEH_DESC");
                            if (objReader["VEH_DESC"] != null && objReader["VEH_DESC"] != DBNull.Value)                            
                                 sVehicleVin = objReader.GetString("VIN");
                        }
                    }
                    sUnitDetails = sVehcicleDesc == string.Empty ? sVehicleVin : sVehcicleDesc;
                    sUnitName = sUnitName + sUnitDetails;
                }
                else
                {
                    if (String.Compare(sUnitType, "SU", true) == 0)
                    {
                        sUnitName = "Stat Unit: ";
                        sSql.Append("SELECT E.LAST_NAME ");
                        sSql.Append("FROM ENTITY E ");
                        sSql.Append("INNER JOIN OTHER_UNIT O ON E.ENTITY_ID = O.ENTITY_ID ");
                        sSql.Append("WHERE O.OTHER_UNIT_ID=");
                        sSql.Append(sUnitId);
                    }
                    else if (String.Compare(sUnitType, "P", true) == 0)
                    {
                        sUnitName = "Property Unit: ";
                        sSql.Append("SELECT P.PIN ");
                        sSql.Append("FROM PROPERTY_UNIT P ");
                        sSql.Append("WHERE P.PROPERTY_ID=");
                        sSql.Append(sUnitId);
                    }
                    else if (String.Compare(sUnitType, "S", true) == 0)
                    {
                        sUnitName = "Site Unit: ";
                        sSql.Append("SELECT S.SITE_NUMBER ");
                        sSql.Append("FROM SITE_UNIT S ");
                        sSql.Append("WHERE S.SITE_ID=");
                        sSql.Append(sUnitId);
                    }

                    if (sSql.Length > 0)
                    {
                        using (objDbConnection = DbFactory.GetDbConnection(m_sConnectionString))
                        {
                            objDbConnection.Open();                            
                            sUnitName = sUnitName + objDbConnection.ExecuteString(sSql.ToString());
                        }
                    }
                }
                
            }
            catch
            {
                throw;
            }
            finally
            {
                if (objDbConnection!=null)
                {
                    objDbConnection.Dispose();
                }
            }

            return sUnitName;
        }

        private string GetCodeInfo(string sCodeId)
        {
            bool bSuccess = false;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
         
     
            try
            {
           
                m_objDataModelFactory.Context.LocalCache.GetCodeInfo(Conversion.CastToType<int>(sCodeId, out bSuccess), ref sShortCode, ref sCodeDesc);

                return string.Format("{0} {1}", sShortCode, sCodeDesc);
            }
            catch
            {
                throw;
            }
            finally
            {
             
            }
        }
        public string GetCovGroupInfo(string sCodeId)
        {
            bool bSuccess = false;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
           

            try
            {
               
                m_objDataModelFactory.Context.LocalCache.GetCoverageGroupInfo(Conversion.CastToType<int>(sCodeId, out bSuccess), ref sShortCode, ref sCodeDesc);

                return string.Format("{0} {1}", sShortCode, sCodeDesc);
            }
            catch
            {
                throw;
            }
            finally
            {
                
            }
        }
        public int GetLOB(int p_iClaimId)
        {
            return DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId);
        }
        //Start: Added by Nitin Goel on 12/26/2013, To retrieve deductible per event flag for coverage group.
        private int GetDeductiblePerEventFlag(int p_iCovGroupId)
        {
            int iPreventEditDedPerEvent = 0;
            if (p_iCovGroupId > 0)
                iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + p_iCovGroupId);

            return iPreventEditDedPerEvent;
        }
        //End: Added by Nitin Goel on 12/26/2013, To retrieve deductible per event flag for coverage group.
        #region XML Manipulation Methods
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            try
            {
                objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(objChildNode);
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("DeductibleManager.CreateElement.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objChildNode = null;
            }

        }

        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("Deductiblemanager.CreateElement.Error",m_iClientId), p_objEx);
            }

        }

        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("Deductiblemanager.CreateElement.Error",m_iClientId), p_objEx);
            }
        }

        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText, ref XmlElement p_objChildNode)
        {
            try
            {
                if (sText == null)
                {
                    sText = string.Empty;
                }
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("Deductiblemanager.CreateElement.Error",m_iClientId), p_objEx);
            }
        }
        #endregion
        #endregion

        #region Applying Deductibles
        public double GetDeductibleInfo(int iPolCvgRowId, int iClaimId, out double dSirDedAmt, out int iDedTypeCode, out int iExcludeExpenseFlag,
            out int iPolicyId, out int iClaimantEid,out int iDimTypeCode, out double dDimPercent)
        {
            string sSql = string.Empty;
            DbReader objReader = null;
            bool bSuccess;            
            string sClaimDate;
            string sDimEvaluationDate;            
            int iClmXPolDedId;
            int iFPTypeCode;
            double dRemianingAmt;
            int iCovGroupId;
			//NI-PCR changes by Nikhil  - Start
            int iPreventEditDedPerEvent = 0;
			//NI-PCR changes by Nikhil  - End
            ClaimXPolDed objClaimXPolDed = null;

            StringBuilder sbSql = new StringBuilder(string.Empty);
            bSuccess = false;
            iExcludeExpenseFlag = 0;
            iDedTypeCode = 0;
            dSirDedAmt = 0d;
            iPolicyId = 0;
            iClaimantEid = 0;
            iDimTypeCode = 0;
            sClaimDate = string.Empty;
            sDimEvaluationDate = string.Empty;
            dDimPercent = 0d;
            iClmXPolDedId = 0;
            dRemianingAmt = 0;
            iCovGroupId = 0;
            iFPTypeCode = 0;

            sbSql.Append(" SELECT DISTINCT CPD.CLM_X_POL_DED_ID, CPD.POLICY_ID, CPD.SIR_DED_AMT, CPD.DED_TYPE_CODE, CPD.EXCLUDE_EXPENSE_FLAG, CPD.CLAIMANT_EID, ");
            sbSql.Append(" C.DATE_OF_CLAIM, CPD.DIMNSHNG_TYPE_CODE, CPD.COV_EVALUATION_DATE, CPD.DIM_PERCENT_NUM, PXG.COV_GROUP_CODE  ");
            sbSql.Append(" FROM CLAIM_X_POL_DED CPD ");
            sbSql.Append(" INNER JOIN CLAIM C ON CPD.CLAIM_ID = C.CLAIM_ID ");
            sbSql.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXG ON CPD.POLICY_X_INSLINE_GROUP_ID = PXG.POLICY_X_INSLINE_GROUP_ID ");
            sbSql.Append(string.Format(" WHERE CPD.POLCVG_ROW_ID={0} AND CPD.CLAIM_ID={1}", iPolCvgRowId, iClaimId));

            objReader = DbFactory.GetDbReader(m_sConnectionString, Convert.ToString(sbSql));
            if (objReader.Read())
            {
                iExcludeExpenseFlag = Conversion.CastToType<int>(objReader["EXCLUDE_EXPENSE_FLAG"].ToString(), out bSuccess);
                iDedTypeCode = Conversion.CastToType<int>(objReader["DED_TYPE_CODE"].ToString(), out bSuccess);
                dSirDedAmt = Conversion.CastToType<double>(objReader["SIR_DED_AMT"].ToString(), out bSuccess);
                iPolicyId = Conversion.CastToType<int>(objReader["POLICY_ID"].ToString(), out bSuccess);
                iClaimantEid = Conversion.CastToType<int>(objReader["CLAIMANT_EID"].ToString(), out bSuccess);
                iCovGroupId = Conversion.CastToType<int>(objReader["COV_GROUP_CODE"].ToString(), out bSuccess);
                iDimTypeCode = Conversion.CastToType<int>(objReader["DIMNSHNG_TYPE_CODE"].ToString(), out bSuccess);
                sClaimDate = objReader["DATE_OF_CLAIM"].ToString();
                sDimEvaluationDate = objReader["COV_EVALUATION_DATE"].ToString();
                dDimPercent = Conversion.CastToType<double>(objReader["DIM_PERCENT_NUM"].ToString(), out bSuccess);
                iClmXPolDedId = Conversion.CastToType<int>(objReader["CLM_X_POL_DED_ID"].ToString(), out bSuccess);
            }
			//MITS 38090: aaggarwal29 start
            if (iClmXPolDedId <= 0)
            {
                return 0d;
            }
			//MITS 38090: aaggarwal29 end           
            objClaimXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
            objClaimXPolDed.MoveTo(iClmXPolDedId);

            iFPTypeCode = objClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");

            dSirDedAmt = objClaimXPolDed.CalculateReducedDeductibleAmount(iClaimId, iDimTypeCode, iDedTypeCode, dSirDedAmt, dDimPercent, iClmXPolDedId);

            //Different method for calculating remaining amount when coverage group is to be applied or otherwise
            if (iCovGroupId == 0 || objClaimXPolDed.DedTypeCode == 0 || objClaimXPolDed.DedTypeCode == iFPTypeCode)
            {
                dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dSirDedAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode);
            }
            else
            {
                //NI-PCR changes by Nikhil for handling deductable check at event level - Start
                
                iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
                if (iPreventEditDedPerEvent == 0)
                    dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dSirDedAmt, objClaimXPolDed.ClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode, iCovGroupId, false);
                else
                    dRemianingAmt = objClaimXPolDed.CalculateRemainingAmount(dSirDedAmt, objClaimXPolDed.ClaimId, iCovGroupId, false);
                //NI-PCR changes by Nikhil for handling deductable check at event level - End
            }
            return dRemianingAmt;
        }

        public int GetRecoveryReserveId(int iPolCvgRowId, int iRecoveryReserveType, int iClaimId, int iLossTypeId)
        {
            int iCvgLossCode = 0;
            StringBuilder sbSql = new StringBuilder(string.Empty);
          

            sbSql.Append("SELECT CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE ");
            sbSql.Append(string.Format("POLCVG_ROW_ID={0} AND LOSS_CODE={1}", iPolCvgRowId, iLossTypeId));
            iCvgLossCode = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());

            sbSql.Remove(0, sbSql.Length);
            sbSql.Append("SELECT RC_ROW_ID FROM RESERVE_CURRENT ");
            sbSql.Append(string.Format("WHERE POLCVG_LOSS_ROW_ID={0} AND RESERVE_TYPE_CODE={1} AND CLAIM_ID={2}",
            iCvgLossCode, iRecoveryReserveType, iClaimId));


            return m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());
        }
        public int GetRecoveryReserveId(int iPolCvgRowId, int iRecoveryReserveType, int iClaimId, int iLossTypeId,int iClaimantEid)
        {
            int iCvgLossCode = 0;
            StringBuilder sbSql = new StringBuilder(string.Empty);


            sbSql.Append("SELECT CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE ");
            sbSql.Append(string.Format("POLCVG_ROW_ID={0} AND LOSS_CODE={1}", iPolCvgRowId, iLossTypeId));
            iCvgLossCode = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());

            sbSql.Remove(0, sbSql.Length);
            sbSql.Append("SELECT RC_ROW_ID FROM RESERVE_CURRENT ");
            sbSql.Append(string.Format("WHERE POLCVG_LOSS_ROW_ID={0} AND RESERVE_TYPE_CODE={1} AND CLAIM_ID={2} AND CLAIMANT_EID={3}",
            iCvgLossCode, iRecoveryReserveType, iClaimId,iClaimantEid));


            return m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSql.ToString());
        }
        #endregion

        #region Event Level Deductible Tracking
        public XmlDocument GetDeductibleDetailsForEvent(XmlDocument p_objXmlIn)
        {
            int iClaimId = 0;
            int iEventId = 0;
            double dTotalPaidAmt = 0d;
            double dTotalRecovery = 0d;
            int iDedRecoveryReserveTypeCode = 0;
            int iRecoveryMaster = 0;
            int iTPDed = 0;
            int iSIRDed = 0;
            int iCovGroupCode = 0;
            int iExcludeExpense = 0;
            bool bSuccess = false;
            XmlNode xClaimId = null;
            StringBuilder sbSQL = null;
            
            string sSqlCollection = string.Empty;
            string sClaimNumber = string.Empty;
            double dSirDedPerEventAmt = 0d;
            double dDeductibleApplied = 0d;

            XmlDocument xReturnDoc = null;
            XmlElement objRootNode = null;
            XmlElement objDeductibleNode = null;
        
            try
            {
                xReturnDoc = new XmlDocument();
                xReturnDoc.InnerXml = "<GROUP_EVENT_DED></GROUP_EVENT_DED>";
                objRootNode = (XmlElement)xReturnDoc.SelectSingleNode("//GROUP_EVENT_DED");

                xClaimId = p_objXmlIn.SelectSingleNode("//ClaimId");
                if (xClaimId != null)
                {
                    iClaimId = Conversion.CastToType<int>(xClaimId.InnerText, out bSuccess);
                    if (bSuccess)
                    {
                        iEventId = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID =" + iClaimId);
                    }
                }

                if (iEventId > 0)
                {
                  
                    iRecoveryMaster = m_objDataModelFactory.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE");
                    iTPDed = m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
                    iSIRDed = m_objDataModelFactory.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE");

                    sbSQL = new StringBuilder();
                    sbSQL.Append(" SELECT DISTINCT C.CLAIM_NUMBER, C.CLAIM_ID, PXIG.COV_GROUP_CODE, CXDAL.SIR_DED_PEREVENT_AMT FROM CLAIM C ");
                    sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ");
                    sbSQL.Append(" ON C.CLAIM_ID = CXD.CLAIM_ID ");
                    sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ");
                    //Start:added by nitin goel, FOr NI PCRs change dedcutible event summary should only be available for the coverage group which flag is set only.
                    sbSQL.Append(" INNER JOIN COVERAGE_GROUP CG ON CG.COVERAGE_GROUP_ID=PXIG.COV_GROUP_CODE");
                    //End:added by Nitin goel
                    sbSQL.Append(" ON CXD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                    sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED_AGG_LIMIT CXDAL ");
                    sbSQL.Append(" ON PXIG.COV_GROUP_CODE = CXDAL.COV_GROUP_CODE ");
                    sbSQL.Append(" WHERE C.EVENT_ID =  ").Append(iEventId);
                    sbSQL.Append(" AND (CXD.DED_TYPE_CODE =").Append(iTPDed).Append(" OR CXD.DED_TYPE_CODE =").Append(iSIRDed).Append(" ) ");
                    sbSQL.Append(" AND PXIG.COV_GROUP_CODE>0 ");
                    //Start:added by nitin goel, FOr NI PCRs change dedcutible event summary should only be available for the coverage group which flag is set only.
                    sbSQL.Append(" AND CG.USE_SHARED_DED_FLAG=-1 ");                   
                    //End:added by Nitin goel
                    using (DbReader objDbReader = DbFactory.ExecuteReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        while (objDbReader.Read())
                        {
                            if (objDbReader["CLAIM_ID"] != null && objDbReader["CLAIM_ID"] != DBNull.Value)
                            {
                                iClaimId = Conversion.CastToType<int>(objDbReader["CLAIM_ID"].ToString(), out bSuccess);
                            }
                            if (objDbReader["COV_GROUP_CODE"] != null && objDbReader["COV_GROUP_CODE"] != DBNull.Value)
                            {
                                iCovGroupCode = Conversion.CastToType<int>(objDbReader["COV_GROUP_CODE"].ToString(), out bSuccess);
                            }
                            if (objDbReader["CLAIM_NUMBER"] != null && objDbReader["CLAIM_NUMBER"] != DBNull.Value)
                            {
                                sClaimNumber = objDbReader["CLAIM_NUMBER"].ToString();
                            }
                            if (objDbReader["SIR_DED_PEREVENT_AMT"] != null && objDbReader["SIR_DED_PEREVENT_AMT"] != DBNull.Value)
                            {
                                dSirDedPerEventAmt = Conversion.CastToType<double>(objDbReader["SIR_DED_PEREVENT_AMT"].ToString(), out bSuccess);
                            }

                            iDedRecoveryReserveTypeCode = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[GetLOB(iClaimId)].DedRecReserveType;

                            sbSQL.Length = 0;
                            sbSQL.Append(" SELECT SUM(FTS.SUM_AMOUNT) AS TOTAL_PAID ");
                            sbSQL.Append(" FROM FUNDS_TRANS_SPLIT FTS ");
                            sbSQL.Append(" INNER JOIN FUNDS F ON FTS.TRANS_ID = F.TRANS_ID ");
                            sbSQL.Append(" INNER JOIN RESERVE_CURRENT RC ON FTS.RC_ROW_ID = RC.RC_ROW_ID ");
                            sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID=CXD.POLICY_X_INSLINE_GROUP_ID ");
                            sbSQL.Append(" WHERE (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG<>0) ");
                            sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG = 0) AND CXD.CLAIM_ID = ").Append(iClaimId);
                            sbSQL.Append(" AND F.STATUS_CODE=").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));
                            sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(iCovGroupCode);
                            sbSQL.Append(" AND (CXD.DED_TYPE_CODE=").Append(iTPDed);
                            sbSQL.Append(" OR CXD.DED_TYPE_CODE=").Append(iSIRDed).Append(")");
                            dTotalPaidAmt = m_objDataModelFactory.Context.DbConnLookup.ExecuteDouble(Convert.ToString(sbSQL));

                            //If Exclude expense flag is checked then decrease the total Paid amount by expense amount
                            //If Flag is checked for any  coverage on the group - then exclude expense payment will be subtracted.
                            sbSQL.Length = 0;
                            sbSQL.Append(" SELECT CXD.EXCLUDE_EXPENSE_FLAG FROM CLAIM_X_POL_DED CXD ");
                            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                            
                            sbSQL.Append(" WHERE CXD.EVENT_ID=").Append(iEventId); 
                            sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(iCovGroupCode);
                            sbSQL.Append(" ORDER BY CXD.EXCLUDE_EXPENSE_FLAG ASC ");
                            iExcludeExpense = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                            if (iExcludeExpense == -1)
                            {
                                dTotalPaidAmt -= GetExpensePaymentsAmount(iClaimId, iCovGroupCode, 0, m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"), m_objDataModelFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE"), iEventId);
                            }
                            if (dTotalPaidAmt < 0)
                            {
                                dTotalPaidAmt = 0d;
 
                            }

                          


                            sSqlCollection = "(F.COLLECTION_FLAG IS NOT NULL AND F.COLLECTION_FLAG <>0 "
                            + " AND F.STATUS_CODE=" + m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")
                            + "AND RC.RESERVE_TYPE_CODE <> " + iDedRecoveryReserveTypeCode
                            + " AND C.RELATED_CODE_ID =" + iRecoveryMaster + ")";
                            sbSQL.Remove(0, sbSQL.Length);
                            sbSQL.Append(" SELECT SUM(FTS.SUM_AMOUNT) AS TOTAL_PAID ");
                            sbSQL.Append(" FROM FUNDS_TRANS_SPLIT FTS ");
                            sbSQL.Append(" INNER JOIN FUNDS F ON FTS.TRANS_ID = F.TRANS_ID ");
                            sbSQL.Append(" INNER JOIN RESERVE_CURRENT RC ON FTS.RC_ROW_ID = RC.RC_ROW_ID ");
                            sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CODES C ON RC.RESERVE_TYPE_CODE = C.CODE_ID ");
                            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID=CXD.POLICY_X_INSLINE_GROUP_ID ");
                            sbSQL.Append(" WHERE (").Append(sSqlCollection).Append(")");
                            sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG = 0) AND CXD.CLAIM_ID = ").Append(iClaimId);
                            sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(iCovGroupCode);
                            sbSQL.Append(" AND (CXD.DED_TYPE_CODE=").Append(iTPDed);
                            sbSQL.Append(" OR CXD.DED_TYPE_CODE=").Append(iSIRDed).Append(")");
                            dTotalRecovery = -1 * m_objDataModelFactory.Context.DbConnLookup.ExecuteDouble(Convert.ToString(sbSQL));

                            if (dTotalRecovery < 0)
                            {
                                dTotalRecovery = 0d;
                            }
                            sbSQL.Remove(0, sbSQL.Length);
                         
                            sbSQL.Append(" SELECT SUM(COLLECTION_TOTAL) AS DED_APPLIED_AMT ");
                            
                            sbSQL.Append(" FROM RESERVE_CURRENT RC ");
                            sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON CXL.CVG_LOSS_ROW_ID=RC.POLCVG_LOSS_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.CLAIM_ID = RC.CLAIM_ID AND CXD.POLCVG_ROW_ID =CXL.POLCVG_ROW_ID ");
                            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID=CXD.POLICY_X_INSLINE_GROUP_ID ");
                            sbSQL.Append(string.Format(" WHERE CXD.CLAIM_ID={0} AND RC.RESERVE_TYPE_CODE={1} ", iClaimId, iDedRecoveryReserveTypeCode));
                            sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(iCovGroupCode);
                            sbSQL.Append(" AND (CXD.DED_TYPE_CODE=").Append(iTPDed);
                            sbSQL.Append(" OR CXD.DED_TYPE_CODE=").Append(iSIRDed).Append(")");
                            dDeductibleApplied = m_objDataModelFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());

                            CreateElement(objRootNode, "DEDUCTIBLE", ref objDeductibleNode);
                            CreateAndSetElement(objDeductibleNode, "CLAIM_ID", iClaimId.ToString());
                            CreateAndSetElement(objDeductibleNode, "CLAIM_NUMBER", sClaimNumber);
                          CreateAndSetElement(objDeductibleNode, "TOTAL_PAID", dTotalPaidAmt.ToString());
                        
                           
                          CreateAndSetElement(objDeductibleNode, "TOTAL_RECOVERY", dTotalRecovery.ToString());
                       

                            CreateAndSetElement(objDeductibleNode, "DED_PER_EVENT", dSirDedPerEventAmt.ToString());

                            CreateAndSetElement(objDeductibleNode, "DEDUCTIBLE_APPLIED", (dDeductibleApplied > 0) ? dDeductibleApplied.ToString() : "0");
                        

                            CreateAndSetElement(objDeductibleNode, "COVERAGE_GROUP", GetCovGroupInfo(iCovGroupCode.ToString()));
                            CreateAndSetElement(objDeductibleNode, "COVERAGE_GROUP_CODE", iCovGroupCode.ToString());
                        }
                    }
                    //jira# RMA-6135.Multicurrency
                    iBaseCurrCode = m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType;
                    sBaseCurr = m_objDataModelFactory.Context.DbConnLookup.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode.ToString());
                    CreateAndSetElement(objRootNode, "BaseCurrencyType", sBaseCurr.ToString().Split('|')[1]);
                    //jira# RMA-6135.Multicurrency
                }
            }
            catch
            {
                throw;
            }
            finally
            {
              
            }

            return xReturnDoc;
        }

        public XmlDocument GetCoverageDeductibleForClaim(XmlDocument p_objXmlIn)
        {
            StringBuilder sbSQL = null;
            int iClaimId = 0;
            int iCovGroupCode = 0;
            bool bSuccess = false;
            XmlNode xClaimId = null;
            XmlNode xCvgGroup = null;
            int iTPDed = 0;
            int iSIRDed = 0;
            int iPolCvgRowId = 0;
            int iDedRecoveryReserveTypeCode = 0;
            int iRecoveryMaster = 0;
            double dTotalPaidAmt = 0d;
            double dTotalRecovery = 0d;
            double dDeductibleApplied = 0d;
            string sSqlCollection = string.Empty;
            string sPolicyName = string.Empty;
            string sInsLine = string.Empty;
            string sUnitType = string.Empty;
            int iUnitId = 0;
            string sUnitName = string.Empty;
            int iCvgTypeCode = 0;
            int iExcludeExpense = 0;
            int iDedPerEventFlag = 0;
            string sCoverageText = string.Empty;
         
            XmlDocument xReturnDoc = null;
            XmlElement objRootNode = null;
            XmlElement objDeductibleNode = null;

            try
            {
                xReturnDoc = new XmlDocument();
                xReturnDoc.InnerXml = "<GROUP_CLAIM_DED></GROUP_CLAIM_DED>";
                objRootNode = (XmlElement)xReturnDoc.SelectSingleNode("//GROUP_CLAIM_DED");

                xClaimId = p_objXmlIn.SelectSingleNode("//ClaimId");
                if (xClaimId != null)
                {
                    iClaimId = Conversion.CastToType<int>(xClaimId.InnerText, out bSuccess);
                }
                xCvgGroup = p_objXmlIn.SelectSingleNode("//CvgGroupId");
                if (xCvgGroup != null)
                {
                    iCovGroupCode = Conversion.CastToType<int>(xCvgGroup.InnerText, out bSuccess);
                }
               
                if (iClaimId > 0 && iCovGroupCode > 0 && GetDeductiblePerEventFlag(iCovGroupCode)==-1)
                
                {            
                   
                    iTPDed = m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
                    iSIRDed = m_objDataModelFactory.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE");
                    iDedRecoveryReserveTypeCode = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[GetLOB(iClaimId)].DedRecReserveType;
                    iRecoveryMaster = m_objDataModelFactory.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE");

                    sbSQL = new StringBuilder();

                    if (DbFactory.IsOracleDatabase(m_sConnectionString))
                    {
                        sbSQL.Append(" SELECT P.POLICY_SYMBOL||'|'||P.POLICY_NUMBER||'|'||P.MODULE AS POLICY_NAME, CVG.COVERAGE_TYPE_CODE, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE, CXD.POLCVG_ROW_ID, CVG.COVERAGE_TEXT ");
                    }
                    else
                    {
                        sbSQL.Append(" SELECT P.POLICY_SYMBOL+'|'+P.POLICY_NUMBER+'|'+P.MODULE AS POLICY_NAME, CVG.COVERAGE_TYPE_CODE, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE, CXD.POLCVG_ROW_ID, CVG.COVERAGE_TEXT ");
                    }
                    sbSQL.Append(" FROM POLICY_X_CVG_TYPE CVG ");
                    sbSQL.Append(" INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID=PXU.POLICY_UNIT_ROW_ID ");
                    sbSQL.Append(" INNER JOIN POLICY P ON PXU.POLICY_ID=P.POLICY_ID ");
                    sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON P.POLICY_ID=CXD.POLICY_ID AND CVG.POLCVG_ROW_ID = CXD.POLCVG_ROW_ID ");
                    sbSQL.Append(" INNER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID=PXU.UNIT_ID AND PUD.UNIT_TYPE=PXU.UNIT_TYPE ");
                    sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID=CXD.POLICY_X_INSLINE_GROUP_ID ");
                    sbSQL.Append(" WHERE CXD.CLAIM_ID=").Append(iClaimId);
                    sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(iCovGroupCode);
                    sbSQL.Append(" AND (CXD.DED_TYPE_CODE=").Append(iTPDed);
                    sbSQL.Append(" OR CXD.DED_TYPE_CODE=").Append(iSIRDed).Append(")");

                    using (DbReader objDbReader = DbFactory.ExecuteReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        while (objDbReader.Read())
                        {
                            if (objDbReader["POLCVG_ROW_ID"]!=null && objDbReader["POLCVG_ROW_ID"]!=DBNull.Value)
                            {
                                iPolCvgRowId = Conversion.CastToType<int>(objDbReader["POLCVG_ROW_ID"].ToString(), out bSuccess);
                            }
                            if (objDbReader["POLICY_NAME"] != null && objDbReader["POLICY_NAME"] != DBNull.Value)
                            {
                                sPolicyName = objDbReader["POLICY_NAME"].ToString();
                            }
                            if (objDbReader["COVERAGE_TYPE_CODE"] != null && objDbReader["COVERAGE_TYPE_CODE"] != DBNull.Value)
                            {
                                iCvgTypeCode = Conversion.CastToType<int>(objDbReader["COVERAGE_TYPE_CODE"].ToString(), out bSuccess);
                            }
                            //Start:Added by Nitin Goel, NI PCRs Changes
                            if (objDbReader["COVERAGE_TEXT"] != null && objDbReader["COVERAGE_TEXT"] != DBNull.Value)
                            {
                                sCoverageText = Convert.ToString(objDbReader["COVERAGE_TEXT"]);
                            }
                            //End: Added by Nitin  goel
                            if (objDbReader["UNIT_ID"] != null && objDbReader["UNIT_ID"] != DBNull.Value)
                            {
                                iUnitId = Conversion.CastToType<int>(objDbReader["UNIT_ID"].ToString(), out bSuccess);
                            }
                            if (objDbReader["UNIT_TYPE"] != null && objDbReader["UNIT_TYPE"] != DBNull.Value)
                            {
                                sUnitType = objDbReader["UNIT_TYPE"].ToString();
                            }
                            if (objDbReader["INS_LINE"] != null && objDbReader["INS_LINE"] != DBNull.Value)
                            {
                                sInsLine = objDbReader["INS_LINE"].ToString();
                            }

                            sUnitName = GetUnit(iUnitId.ToString(), sUnitType);

                            sbSQL.Length = 0;
                            sbSQL.Append(" SELECT SUM(FTS.SUM_AMOUNT) AS TOTAL_PAID ");
                            sbSQL.Append(" FROM FUNDS_TRANS_SPLIT FTS ");
                            sbSQL.Append(" INNER JOIN FUNDS F ON FTS.TRANS_ID = F.TRANS_ID ");
                            sbSQL.Append(" INNER JOIN RESERVE_CURRENT RC ON FTS.RC_ROW_ID = RC.RC_ROW_ID ");
                            sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                            sbSQL.Append(" WHERE (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG<>0) ");
                            sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG = 0) AND CXD.CLAIM_ID = ").Append(iClaimId);
                            sbSQL.Append(" AND F.STATUS_CODE=").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"));
                            sbSQL.Append(" AND CXD.POLCVG_ROW_ID=").Append(iPolCvgRowId);
                            dTotalPaidAmt = m_objDataModelFactory.Context.DbConnLookup.ExecuteDouble(Convert.ToString(sbSQL));

                            //If Exclude expense flag is checked then decrease the total Paid amount by expense amount
                            //If Flag is checked for any  coverage on the group - then exclude expense payment will be subtracted.
                            sbSQL.Length = 0;
                            sbSQL.Append(" SELECT CXD.EXCLUDE_EXPENSE_FLAG FROM CLAIM_X_POL_DED CXD ");
                            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                            sbSQL.Append(" WHERE CXD.CLAIM_ID=").Append(iClaimId);
                            sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(iCovGroupCode);
                            sbSQL.Append(" ORDER BY CXD.EXCLUDE_EXPENSE_FLAG ASC ");
                            iExcludeExpense = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                            if (iExcludeExpense == -1)
                            {
                                dTotalPaidAmt -= GetExpensePaymentsAmount(iClaimId, 0, iPolCvgRowId, m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"), m_objDataModelFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE"),0);
                                if (dTotalPaidAmt < 0d)
                                {
                                    dTotalPaidAmt = 0d;
                                }
                            }



                            sSqlCollection = "(F.COLLECTION_FLAG IS NOT NULL AND F.COLLECTION_FLAG <>0 "
                            + " AND F.STATUS_CODE=" + m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")
                            + "AND RC.RESERVE_TYPE_CODE <> " + iDedRecoveryReserveTypeCode
                            + " AND C.RELATED_CODE_ID =" + iRecoveryMaster + ")";
                            sbSQL.Remove(0, sbSQL.Length);
                            sbSQL.Append(" SELECT SUM(FTS.SUM_AMOUNT) AS TOTAL_PAID ");
                            sbSQL.Append(" FROM FUNDS_TRANS_SPLIT FTS ");
                            sbSQL.Append(" INNER JOIN FUNDS F ON FTS.TRANS_ID = F.TRANS_ID ");
                            sbSQL.Append(" INNER JOIN RESERVE_CURRENT RC ON FTS.RC_ROW_ID = RC.RC_ROW_ID ");
                            sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CODES C ON RC.RESERVE_TYPE_CODE = C.CODE_ID ");
                            sbSQL.Append(" WHERE (").Append(sSqlCollection).Append(")");
                            sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG = 0) AND CXD.CLAIM_ID = ").Append(iClaimId);
                            sbSQL.Append(" AND CXD.POLCVG_ROW_ID=").Append(iPolCvgRowId);
                            dTotalRecovery = -1 * m_objDataModelFactory.Context.DbConnLookup.ExecuteDouble(Convert.ToString(sbSQL));

                            sbSQL.Remove(0, sbSQL.Length);
                         
                            sbSQL.Append(" SELECT SUM(COLLECTION_TOTAL) AS DED_APPLIED_AMT ");
                            
                            sbSQL.Append(" FROM RESERVE_CURRENT RC ");
                            sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS CXL ON CXL.CVG_LOSS_ROW_ID =RC.POLCVG_LOSS_ROW_ID ");
                            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXD ON CXD.CLAIM_ID = RC.CLAIM_ID AND CXD.POLCVG_ROW_ID =CXL.POLCVG_ROW_ID ");
                            sbSQL.Append(string.Format(" WHERE CXD.CLAIM_ID={0} AND RC.RESERVE_TYPE_CODE={1} ", iClaimId, iDedRecoveryReserveTypeCode));
                            sbSQL.Append(" AND CXD.POLCVG_ROW_ID=").Append(iPolCvgRowId);
                            dDeductibleApplied = m_objDataModelFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());

                            CreateElement(objRootNode, "CVG_DEDUCTIBLE", ref objDeductibleNode);
                            CreateAndSetElement(objDeductibleNode, "POLICY", sPolicyName);
                            CreateAndSetElement(objDeductibleNode, "INS_LINE", sInsLine);
                            CreateAndSetElement(objDeductibleNode, "UNIT", sUnitName);
                            
                            CreateAndSetElement(objDeductibleNode, "COVERAGE", sCoverageText);
                           CreateAndSetElement(objDeductibleNode, "TOTAL_PAID", dTotalPaidAmt.ToString());
                       
                          CreateAndSetElement(objDeductibleNode, "TOTAL_RECOVERY", dTotalRecovery.ToString());

                           CreateAndSetElement(objDeductibleNode, "DEDCTIBLE_APPLIED", (dDeductibleApplied > 0) ? dDeductibleApplied.ToString() : "0");
                        }
                    }
                    //jira# RMA-6135.Multicurrency
                    iBaseCurrCode = m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType;
                    sBaseCurr = m_objDataModelFactory.Context.DbConnLookup.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode.ToString());
                    CreateAndSetElement(objRootNode, "BaseCurrencyType", sBaseCurr.ToString().Split('|')[1]);
                    //jira# RMA-6135.Multicurrency

                }
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }

            return xReturnDoc;
        }

        private double GetExpensePaymentsAmount(int p_iClaimId, int p_iCovGroupCode, int p_iPolCvgRowId, int p_iPrintedStatusCode, int p_iMasterReserveCode, int p_iEventId)
        {
            StringBuilder sbSQL = null;
            double dExpensePaymentAmount = 0d;          
          
            try
            {
           
                sbSQL = new StringBuilder();
                sbSQL.Length = 0;
                sbSQL.Append(" SELECT SUM(FTS.AMOUNT) FROM FUNDS_TRANS_SPLIT FTS ");
                sbSQL.Append(" INNER JOIN FUNDS F ON F.TRANS_ID =FTS.TRANS_ID ");
                sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLCVG_ROW_ID =FTS.POLCVG_ROW_ID AND F.CLAIM_ID = CXPD.CLAIM_ID ");
                sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID =CXPD.POLICY_X_INSLINE_GROUP_ID ");
                sbSQL.Append(" INNER JOIN CODES CR ON CR.CODE_ID=FTS.RESERVE_TYPE_CODE ");
                sbSQL.Append(" WHERE ");
                sbSQL.Append(" (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG <>0) ");
                sbSQL.Append(" AND F.STATUS_CODE=").Append(p_iPrintedStatusCode);
                sbSQL.Append(" AND CR.RELATED_CODE_ID= ").Append(p_iMasterReserveCode);
               
                sbSQL.Append(" AND (F.TRANS_ID NOT IN (SELECT DISTINCT(DED_PAY_TRANS_ID) FROM FUNDS_X_DED_COL_MAP)) ");
                sbSQL.Append(" AND (CXPD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE"));
                sbSQL.Append(" OR CXPD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE")).Append(")");


                if (p_iCovGroupCode > 0 && GetDeductiblePerEventFlag(p_iCovGroupCode) == -1)
                {
                    sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(p_iCovGroupCode); //TO do: Add condition for Event Id
                    sbSQL.Append(" AND CXPD.EVENT_ID=").Append(p_iEventId);
                }
                else
                {

                    sbSQL.Append(" AND CXPD.CLAIM_ID=").Append(p_iClaimId);
                    sbSQL.Append(" AND CXPD.POLCVG_ROW_ID=").Append(p_iPolCvgRowId);
                }
                //Change By Nikhil on 01/13/2014 - End 
                dExpensePaymentAmount = DbFactory.ExecuteAsType<double>(m_sConnectionString, Convert.ToString(sbSQL));
            }
            catch
            {
                throw;
            }
            finally
            {
            }
                return dExpensePaymentAmount;            
        }


        //Nikhil -calculate payment with applicable reserve type -  Start

        private double GetExcludedReservePaymentAmount(int p_iClaimId, int p_iCovGroupCode, int p_iPolCvgRowId, int p_iPrintedStatusCode, int p_iEventId)
        {
            StringBuilder sbSQL = null;
            double dExecReservePaymentAmount = 0d;
           
            try
            {
              
                sbSQL = new StringBuilder();
                sbSQL.Length = 0;
                sbSQL.Append(" SELECT SUM(FTS.AMOUNT) FROM FUNDS_TRANS_SPLIT FTS ");
                sbSQL.Append(" INNER JOIN FUNDS F ON F.TRANS_ID =FTS.TRANS_ID ");
                sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLCVG_ROW_ID =FTS.POLCVG_ROW_ID AND F.CLAIM_ID = CXPD.CLAIM_ID ");
                sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON PXIG.POLICY_X_INSLINE_GROUP_ID =CXPD.POLICY_X_INSLINE_GROUP_ID ");
              
                sbSQL.Append(" WHERE ");
                sbSQL.Append(" (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG <>0) ");
                sbSQL.Append(" AND F.STATUS_CODE=").Append(p_iPrintedStatusCode);
             

                sbSQL.Append(" AND ( FTS.RESERVE_TYPE_CODE NOT IN ( SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = ").Append(p_iCovGroupCode).Append(" ))");
                
                sbSQL.Append(" AND (F.TRANS_ID NOT IN (SELECT DISTINCT(DED_PAY_TRANS_ID) FROM FUNDS_X_DED_COL_MAP)) ");
                sbSQL.Append(" AND (CXPD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE"));
                sbSQL.Append(" OR CXPD.DED_TYPE_CODE= ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE")).Append(")");


                if (p_iCovGroupCode > 0 && GetDeductiblePerEventFlag(p_iCovGroupCode) == -1)
                {
                    sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(p_iCovGroupCode); //TO do: Add condition for Event Id
                    sbSQL.Append(" AND CXPD.EVENT_ID=").Append(p_iEventId);
                }
                else
                {

                    sbSQL.Append(" AND CXPD.CLAIM_ID=").Append(p_iClaimId);
                    sbSQL.Append(" AND CXPD.POLCVG_ROW_ID=").Append(p_iPolCvgRowId);
                }

                
                dExecReservePaymentAmount = DbFactory.ExecuteAsType<double>(m_sConnectionString, Convert.ToString(sbSQL));
            }
            catch
            {
                throw;
            }
            finally
            {

            }
            return dExecReservePaymentAmount;
        }




        //Nikhil -calculate payment with applicable reserve type -  End



         #endregion
    }
}
