﻿
using System;
using System.Xml ;
using Riskmaster.DataModel ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;
using Riskmaster.Settings ;
using System.Collections ;
using System.Data;

namespace Riskmaster.Application.FundManagement
{
	/// <summary>
	/// Author : Vaibhav Kaushik. 
	/// Summary description for AutoChecks.
	/// </summary>
	public class AutoChecks
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
        private int m_iClientId = 0;//dvatsa-cloud
		private DataModelFactory m_objDataModelFactory = null ;
     
		#endregion
		
		#region Constructors
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		public AutoChecks(string p_sDsnName , string p_sUserName , string p_sPassword,int p_iClientId )
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;//dvatsa-cloud
			m_objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword ,m_iClientId);	//dvatsa-cloud
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;									
		}
		~AutoChecks()
		{
			if( m_objDataModelFactory != null )
				this.Dispose();
		}

		public void Dispose()
		{
			try
			{
				m_objDataModelFactory.UnInitialize();
                m_objDataModelFactory.Dispose();				
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}

		#endregion		

		public XmlDocument GetAllAutoChecksBatch(int p_iClaimID)
		{
			XmlDocument objXmlDocument = null ;
			XmlElement objRootNode = null ;
			XmlElement objAutoCheckBatchNode = null ;
			DbReader objReader = null ;
			string sSQL = string.Empty ; 
			string sStateAbb = string.Empty ;
			string sStateName = string.Empty ;
			try
			{
                //MITS 12091 Raman Bhatia 
                //If claimid is present then filter batchlist by claimid 
                if (p_iClaimID == 0)
                {
                    sSQL = " SELECT FUNDS_AUTO.AUTO_BATCH_ID,FUNDS_AUTO.AUTO_BATCH_ID  BATCH_NUMBER, CLAIM_NUMBER "
                        + ",CTL_NUMBER,AMOUNT,PRINT_DATE,PAYEE_TYPE_CODE,FIRST_NAME,LAST_NAME,ADDR1  ADDRESS,CITY,STATE_ID,ZIP_CODE, FUNDS_AUTO_BATCH.DISABILITY_FLAG  DISABILITY_FLAG"
                        + " FROM FUNDS_AUTO, FUNDS_AUTO_BATCH WHERE AUTO_TRANS_ID IN (SELECT MIN(AUTO_TRANS_ID) FROM FUNDS_AUTO GROUP BY AUTO_BATCH_ID) "
                        + " AND FUNDS_AUTO.AUTO_BATCH_ID = FUNDS_AUTO_BATCH.AUTO_BATCH_ID AND FUNDS_AUTO_BATCH.DISABILITY_FLAG <> -1";
                }
                else
                {
                    sSQL = " SELECT FUNDS_AUTO.AUTO_BATCH_ID,FUNDS_AUTO.AUTO_BATCH_ID  BATCH_NUMBER, CLAIM_NUMBER "
                        + ",CTL_NUMBER,AMOUNT,PRINT_DATE,PAYEE_TYPE_CODE,FIRST_NAME,LAST_NAME,ADDR1  ADDRESS,CITY,STATE_ID,ZIP_CODE, FUNDS_AUTO_BATCH.DISABILITY_FLAG  DISABILITY_FLAG"
                        + " FROM FUNDS_AUTO, FUNDS_AUTO_BATCH WHERE FUNDS_AUTO.CLAIM_ID = '" + p_iClaimID.ToString() + "' AND AUTO_TRANS_ID IN (SELECT MIN(AUTO_TRANS_ID) FROM FUNDS_AUTO GROUP BY AUTO_BATCH_ID) "
                        + " AND FUNDS_AUTO.AUTO_BATCH_ID = FUNDS_AUTO_BATCH.AUTO_BATCH_ID AND FUNDS_AUTO_BATCH.DISABILITY_FLAG <> -1";
                }
                //pmittal5  MITS:8120  05/30/08
                sSQL = sSQL + " ORDER BY BATCH_NUMBER ";
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL  );

				this.StartDocument( ref objXmlDocument , ref objRootNode , "AutoCheckBatchs" );
				while( objReader.Read() )
				{ 
                    this.CreateElement(objRootNode, "AutoCheckBatch", ref objAutoCheckBatchNode);
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Batch_Number", Conversion.ConvertObjToInt(objReader.GetValue("BATCH_NUMBER"), m_iClientId).ToString());
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Claim_Number", objReader.GetString("CLAIM_NUMBER"));
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Ctl_Number", objReader.GetString("CTL_NUMBER"));
                    //Ankit Start : MITS - 31412 (Should Come Base Currency instead of USD)
                    //this.CreateAndSetElement(objAutoCheckBatchNode, "Amount", string.Format("{0:C}", Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"))));
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Amount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), Convert.ToDouble(objReader.GetValue("AMOUNT")), m_sConnectionString, m_iClientId));
                    //Ankit End
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Print_Date", Conversion.GetDBDateFormat(objReader.GetString("PRINT_DATE"), "d"));

                    m_objDataModelFactory.Context.LocalCache.GetCodeInfo(Conversion.ConvertObjToInt(objReader.GetValue("PAYEE_TYPE_CODE"), m_iClientId), ref sStateAbb, ref sStateName);
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Payee_Type", sStateName);

                    this.CreateAndSetElement(objAutoCheckBatchNode, "First_Name", objReader.GetString("FIRST_NAME"));
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Last_Name", objReader.GetString("LAST_NAME"));
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Address", objReader.GetString("ADDRESS"));
                    this.CreateAndSetElement(objAutoCheckBatchNode, "City", objReader.GetString("CITY"));

                    m_objDataModelFactory.Context.LocalCache.GetStateInfo(Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), m_iClientId), ref sStateAbb, ref sStateName);
                    this.CreateAndSetElement(objAutoCheckBatchNode, "State", sStateAbb + " " + sStateName);
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Zip_Code", objReader.GetString("ZIP_CODE"));
                    this.CreateAndSetElement(objAutoCheckBatchNode, "Disability_Flag", objReader.GetBoolean("DISABILITY_FLAG") ? "Yes" : "No");
                }

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("AutoChecks.GetAllAutoChecksBatch.Error",m_iClientId) , p_objEx );				
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
			}
			return(objXmlDocument);
		}


		private void StartDocument( ref XmlDocument objXmlDocument , ref XmlElement p_objRootNode, string p_sRootNodeName )
		{
			objXmlDocument = new XmlDocument();
			p_objRootNode = objXmlDocument.CreateElement( p_sRootNodeName );
			objXmlDocument.AppendChild( p_objRootNode );							
		}

		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName )
		{
			XmlElement objChildNode = null ;
			objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
			p_objParentNode.AppendChild( objChildNode );			
		}	
	
		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode )
		{
			p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
			p_objParentNode.AppendChild( p_objChildNode );			
		}
		
		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText )
		{
			XmlElement objChildNode = null ;
			this.CreateAndSetElement( p_objParentNode , p_sNodeName , p_sText , ref objChildNode );			
		}
		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode )
		{
			CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode );
			p_objChildNode.InnerText = p_sText ;						
		}
		

		
	}
}
