Option Strict Off
Option Explicit On
Imports Riskmaster.Db

Module modFunctions
    Const sClassName As String = "modFunctions"
    Dim m_JurisRowID As Int64
    Public Function GetBooleanValue(ByVal s As String) As Boolean
        s = LCase(Trim(s))
        If s = "true" Or s = "-1" Or s = "1" Then
            GetBooleanValue = True
        Else
            GetBooleanValue = False
        End If


    End Function
    Public Function GetFormattedDate(ByVal sDate As String) As String
        'takes a DTG style date (YYYYMMDD) and returns a string (as "Short Date")
        Dim sFmtDate As String
        Dim sShortDate As Date
        On Error Resume Next

        GetFormattedDate = ""

        If sDate = "" Then Exit Function
        sDate = DateSerial(Val(Left(sDate, 4)), Val(Mid(sDate, 5, 2)), Val(Mid(sDate, 7, 2)))
        sFmtDate = sShortDate.ToShortDateString()

        'VB6.Format(DateSerial(Val(Left(sDate, 4)), Val(Mid(sDate, 5, 2)), Val(Mid(sDate, 7, 2))), "Short Date")

        GetFormattedDate = sFmtDate



    End Function
    'e.g. return what goes in parens in sql statement like "...WHERE CODE_ID IN(15,78,84,87)..."
    Public Sub GetCodeList(ByRef sSQL As String, ByRef sList As String)
        Dim objReader As dbReader

        objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
        Do Until objReader.Read()
            sList = sList & "," & objReader.GetInt32(1)

        Loop
        SafeCloseRecordset(objReader)

        If Len(sList) > 0 Then sList = Mid(sList, 2)



    End Sub
    Public Function GetStatePostalCode_SQL(ByRef lStateID As Integer) As String

        Dim sSQL As String
        Dim hMyConnection As Integer
        Dim bCleanUp As Boolean
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Try
            GetStatePostalCode_SQL = ""
            bCleanUp = False
            hMyConnection = g_ConnectionString
            'If g_DBObject.DB_GetDBMake(g_ConnectionString) = RMLoginLib.DTGDatabaseTypes.DBMS_IS_SQLSRVR Then
            '    If g_DBObject.DB_ConnInUse(g_ConnectionString) Then
            '        hMyConnection = g_DBObject.DB_OpenDatabase(g_hEnv, g_RmDSN, 0)
            '        bCleanUp = True
            '    Else
            '    End If
            'End If
            sSQL = "SELECT STATE_ID FROM STATES WHERE STATE_ROW_ID = " & lStateID
            'iRdSt = g_DBObject.DB_CreateRecordset(hMyConnection, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If Not g_DBObject.DB_EOF(iRdSt) Then
            '	GetStatePostalCode_SQL = GetDataString(iRdSt, 1)
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetStatePostalCode_SQL = objReader.GetString("TABLE_ROW_ID")
            End If

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.GetCodeDesc_SQL|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
        End Try

    End Function

    Function sDBDateFormat(ByRef sDate As String, ByRef sFormat As String) As String

        Dim vTmp As Object
        sDBDateFormat = ""
        If Trim(sDate) <> "" Then
            'UPGRADE_WARNING: Couldn't resolve default property of object vTmp. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            vTmp = DateSerial(Val(Left(sDate, 4)), Val(Mid(sDate, 5, 2)), Val(Mid(sDate, 7, 2)))
            'UPGRADE_WARNING: Couldn't resolve default property of object vTmp. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sDBDateFormat = vTmp.ToString(sFormat)
        End If


    End Function
    '               1234567890123456789012345678901234567890
    Public Function GetClaimCountAffdByBeneDelete(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Dim objReader As dbReader
        Dim sSQL As String
        GetClaimCountAffdByBeneDelete = 0
        sSQL = ""
        sSQL = sSQL & "SELECT COUNT(CLAIM_ID)"
        sSQL = sSQL & " FROM CLAIM"
        sSQL = sSQL & " WHERE FILING_STATE_ID = " & lJurisRowID
        sSQL = sSQL & " AND LINE_BUS_CODE = 243"

        '      objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
        'If Not g_DBObject.DB_EOF(iRdset) Then
        '	GetClaimCountAffdByBeneDelete = objReader.GetInt32( 1)
        '      End If

        objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
        If (objReader.Read()) Then
            GetClaimCountAffdByBeneDelete = objReader.GetInt32(1)
        End If


        SafeCloseRecordset(objReader)



    End Function
    Public Function GetClientDictionaryPath(Optional p_iClientId As Integer = 0) As String
        Dim objDS As DataSet
        Dim sSQL As String
        g_sClientDictPath = ""
        sSQL = "SELECT * FROM SYS_PARMS"
        objDS = DbFactory.GetDataSet(g_ConnectionString, sSQL, p_iClientId)
        If (objDS.Tables.Count > 0) Then
            If Not IsNothing(objDS.Tables(0).Columns("USER_DICT_PATH")) Then
                If (objDS.Tables(0).Rows.Count > 0) Then
                    g_sClientDictPath = (objDS.Tables(0).Rows(0)("USER_DICT_PATH")).ToString()
                End If
            End If
        End If
    End Function

    Public Function GetCodeDesc_SQL(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lCodeId As Integer) As String
        Const sFunctionName As String = "GetCodeDesc_SQL"

        Dim sSQL As String
        Dim hMyConnection As Integer
        Dim bCleanUp As Boolean

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Try

            GetCodeDesc_SQL = ""
            bCleanUp = False



            hMyConnection = g_ConnectionString
            'If g_DBObject.DB_GetDBMake(g_ConnectionString) = RMLoginLib.DTGDatabaseTypes.DBMS_IS_SQLSRVR Then
            '    If g_DBObject.DB_ConnInUse(g_ConnectionString) Then
            '        hMyConnection = g_DBObject.DB_OpenDatabase(g_hEnv, g_objUser.DBConnectString, 0)
            '        bCleanUp = True
            '    Else
            '    End If
            'End If
            sSQL = "SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = " & lCodeId

            '      iRdSt = g_DBObject.DB_CreateRecordset(hMyConnection, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If Not g_DBObject.DB_EOF(iRdSt) Then
            '	GetCodeDesc_SQL = GetDataString(iRdSt, 1)
            '      End If

            objReader = DbFactory.GetDbReader(hMyConnection, sSQL)
            If (objReader.Read()) Then
                GetCodeDesc_SQL = objReader.GetString(1)
            End If


        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.GetCodeDesc_SQL|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
            'If bCleanUp Then g_DBObject.DB_CloseDatabase(hMyConnection)

        End Try

    End Function
    Public Function GetSAWWMaxCompRateWeekly(ByRef lErrorMaskSAWW As Integer, ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Double
        Const sFunctionName As String = "GetSAWWMaxCompRateWeekly"
        Dim lReturn As Integer

        Try

            Dim objSAWW As CJRSAWW
            objSAWW = New CJRSAWW
            lReturn = objSAWW.LoadDataByEventDate(objUser, lJurisRowID, sDateOfEventDTG)
            Select Case lReturn
                Case -1 'Normal, expected
                    If objSAWW.TableRowID = 0 Then
                        lErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcNoJurisdictionalRule
                        'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        objSAWW = Nothing
                        g_sErrProcedure = "|" & sClassName & "." & sFunctionName & "|"
                        g_sErrDescription = "objSAWW is Nothing"
                        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                        Exit Function
                    End If
                Case Else 'error
                    'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objSAWW = Nothing
                    'error should/will bubble up
                    g_sErrProcedure = "|" & sClassName & "." & sFunctionName & "|"
                    g_sErrDescription = "objSAWW is Nothing"
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                    Exit Function
            End Select

            GetSAWWMaxCompRateWeekly = objSAWW.SAWWAmount * objSAWW.PercentMax
            If objSAWW.MaxAmountAsPublished > 0 Then
                GetSAWWMaxCompRateWeekly = objSAWW.MaxAmountAsPublished
            End If

            'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objSAWW = Nothing

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function GetSAWWMinCompRate(ByRef lErrorMaskSAWW As Integer, ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Double
        Const sFunctionName As String = "GetSAWWMinCompRate"
        Dim lReturn As Integer

        Try

            Dim objSAWW As CJRSAWW
            objSAWW = New CJRSAWW
            lReturn = objSAWW.LoadDataByEventDate(objUser, lJurisRowID, sDateOfEventDTG)
            Select Case lReturn
                Case -1 'Normal, expected
                    If objSAWW.TableRowID = 0 Then
                        lErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcNoJurisdictionalRule
                        'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        objSAWW = Nothing
                        g_sErrProcedure = "|" & sClassName & "." & sFunctionName & "|"
                        g_sErrDescription = "objSAWW is Nothing"
                        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                        Exit Function
                    End If
                Case Else 'error
                    'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objSAWW = Nothing
                    'error should/will bubble up
                    g_sErrProcedure = "|" & sClassName & "." & sFunctionName & "|"
                    g_sErrDescription = "objSAWW is Nothing"
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                    Exit Function
            End Select

            GetSAWWMinCompRate = objSAWW.SAWWAmount * objSAWW.PercentMin
            If objSAWW.MinAmountAsPublished > 0 Then
                GetSAWWMinCompRate = objSAWW.MinAmountAsPublished
            End If

            'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objSAWW = Nothing

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function GetShortCode_SQL(ByRef lCodeId As Integer) As String
        Dim iRdSt As Short
        Dim sSQL As String
        Dim hMyConnection As Integer
        Dim bCleanUp As Boolean
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Try
            GetShortCode_SQL = ""
            bCleanUp = False
            hMyConnection = g_ConnectionString
            'If g_DBObject.DB_GetDBMake(g_ConnectionString) = RMLoginLib.DTGDatabaseTypes.DBMS_IS_SQLSRVR Then
            '    If g_DBObject.DB_ConnInUse(g_ConnectionString) Then
            '        hMyConnection = g_DBObject.DB_OpenDatabase(g_hEnv, g_RmDSN, 0)
            '        bCleanUp = True
            '    Else
            '    End If
            'End If
            sSQL = "SELECT SHORT_CODE FROM CODES WHERE CODE_ID = " & lCodeId

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetShortCode_SQL = objReader.GetString(1)
            End If

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.GetShortCode_SQL|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
            'If bCleanUp Then g_DBObject.DB_CloseDatabase(hMyConnection)
        End Try

    End Function
    Public Sub SafeCloseRecordset(ByRef objReader As DbReader)
        If Not objReader Is Nothing Then
            objReader.Close()
        End If
    End Sub

    'Public Sub SafeCloseRecordset(ByRef iRdset As Short)
    '	If iRdset <> 0 Then
    '		On Error Resume Next
    '           g_DBObject.DB_CloseRecordset(iRdset, ROCKETCOMLib.DTGDBCloseOptions.DB_CLOSE) 'Nanda
    '		On Error GoTo 0
    '	End If
    'End Sub
    Public Sub SafeDropRecordset(ByRef objReader As DbReader)
        If Not objReader Is Nothing Then
            objReader.Close()
            objReader.Dispose()
        End If
    End Sub

    Public Function sDBTimeFormat(ByRef sTime As String, ByRef sFormat As String) As String
        Dim sTmp As String
        sDBTimeFormat = ""
        If Trim(sTime) <> "" Then
            sTmp = Left(sTime, 2) & ":" & Mid(sTime, 3, 2) & ":" & Mid(sTime, 5, 2)
            sDBTimeFormat = sTmp.ToString("sFormat")
        End If



    End Function
    Function GetStateRowID_SQL(ByRef sShort As String) As Integer

        Dim sSQL As String
        Dim hMyConnection As Integer
        Dim bCleanUp As Boolean

        Dim objReader As DbReader
        Try
            GetStateRowID_SQL = 0
            bCleanUp = False
            hMyConnection = g_ConnectionString
            'If g_DBObject.DB_GetDBMake(g_ConnectionString) = RMLoginLib.DTGDatabaseTypes.DBMS_IS_SQLSRVR Then
            '    If g_DBObject.DB_ConnInUse(g_ConnectionString) Then
            '        hMyConnection = g_DBObject.DB_OpenDatabase(g_hEnv, g_RmDSN, 0)
            '        bCleanUp = True
            '    Else
            '    End If
            'End If
            sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = '" & sShort & "'"

            '      iRdSt = g_DBObject.DB_CreateRecordset(hMyConnection, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If Not g_DBObject.DB_EOF(iRdSt) Then
            '	GetStateRowID_SQL = CInt(GetDataString(iRdSt, 1))
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                GetStateRowID_SQL = CInt(objReader.GetString(1))
            End If


            SafeDropRecordset(objReader)
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.GetStateRowID_SQL|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeDropRecordset(objReader)
        End Try

    End Function
    Function lAnyVarToLong(ByRef a As Object) As Integer
        'UPGRADE_WARNING: VarType has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        Select Case VarType(a)
            Case VariantType.Null, VariantType.Empty
                lAnyVarToLong = 0
                Exit Function
            Case VariantType.Short, VariantType.Integer
                'UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lAnyVarToLong = a
            Case VariantType.Single, VariantType.Double
                'UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lAnyVarToLong = CInt(a)
                Exit Function
            Case VariantType.String
                'UPGRADE_WARNING: Couldn't resolve default property of object a. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lAnyVarToLong = CInt(a)
                Exit Function
        End Select


    End Function
    Function GetStateName_SQL(ByRef lStateID As Integer) As String

        Dim sSQL As String
        Dim hMyConnection As Integer
        Dim bCleanUp As Boolean

        Dim objReader As DbReader
        Try
            GetStateName_SQL = ""
            bCleanUp = False
            'hMyConnection = g_ConnectionString
            'If g_DBObject.DB_GetDBMake(g_ConnectionString) = RMLoginLib.DTGDatabaseTypes.DBMS_IS_SQLSRVR Then
            '    If g_DBObject.DB_ConnInUse(g_ConnectionString) Then
            '        hMyConnection = g_DBObject.DB_OpenDatabase(g_hEnv, g_RmDSN, 0)
            '        bCleanUp = True
            '    Else
            '    End If
            'End If
            sSQL = ""
            sSQL = sSQL & "SELECT STATE_NAME"
            sSQL = sSQL & " FROM STATES WHERE STATE_ROW_ID = " & lStateID

            '      iRdSt = g_DBObject.DB_CreateRecordset(hMyConnection, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If Not g_DBObject.DB_EOF(iRdSt) Then
            '	GetStateName_SQL = GetDataString(iRdSt, 1)
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetStateName_SQL = objReader.GetString("STATE_NAME")
                '.GetString("STATE_NAME")
            End If


            SafeDropRecordset(objReader)
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.GetStateName_SQL|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)

        End Try

    End Function

    Function lGetTableID(ByRef sTableName As String) As Integer
        Dim lTableID As Integer
        Dim sSQL As String
        Dim iRdSet1 As Short
        Dim objReader As DbReader
        Try
            lGetTableID = 0
            lTableID = 0
            If Trim(sTableName) = "" Then
                Exit Function
            End If
            If lTableID = 0 Then
                sSQL = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'"

                '         iRdSet1 = g_DBObject.DB_CreateRecordset(g_ConnectionString, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
                'If Not g_DBObject.DB_EOF(iRdSet1) Then
                '	lTableID = GetDataLong(iRdSet1, 1)
                '         End If

                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                If (objReader.Read()) Then
                    lTableID = objReader.GetInt32(1)
                End If

            End If
            SafeDropRecordset(objReader)
            lGetTableID = lTableID
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.lGetTableID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
        End Try

    End Function

    Public Function GetCodeIDWithShort_SQL(ByRef objUser As Riskmaster.Security.UserLogin, ByVal sShortCode As String, ByVal sSysTableName As String) As Integer
        Const sFunctionName As String = "GetCodeIDWithShort_SQL"

        Dim sSQL As String
        Dim hMyConnection As Integer
        Dim bCleanUp As Boolean
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Try

            GetCodeIDWithShort_SQL = 0
            bCleanUp = False



            hMyConnection = g_ConnectionString

            'If g_DBObject.DB_GetDBMake(g_ConnectionString) = RMLoginLib.DTGDatabaseTypes.DBMS_IS_SQLSRVR Then
            '    If g_DBObject.DB_ConnInUse(g_ConnectionString) Then
            '        hMyConnection = g_DBObject.DB_OpenDatabase(g_hEnv, g_RmDSN, 0)
            '        bCleanUp = True
            '    Else
            '    End If
            'End If
            sSQL = ""
            sSQL = sSQL & "SELECT CODE_ID"
            sSQL = sSQL & " FROM CODES, GLOSSARY"
            sSQL = sSQL & " WHERE CODES.TABLE_ID = GLOSSARY.TABLE_ID"
            sSQL = sSQL & " AND CODES.SHORT_CODE = '" & sShortCode & "'"
            sSQL = sSQL & " AND GLOSSARY.SYSTEM_TABLE_NAME = '" & sSysTableName & "'"

            objReader = DbFactory.GetDbReader(hMyConnection, sSQL)
            If (objReader.Read()) Then
                GetCodeIDWithShort_SQL = objReader.GetInt32(1)
            End If
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.GetCodeIDWithShort_SQL|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
        End Try

    End Function

    Public Function GetRelatedCodeID_SQL(ByVal lCodeId As Integer) As Integer

        Dim sSQL As String
        Dim hMyConnection As Integer
        Dim bCleanUp As Boolean
        Dim objReader As DbReader
        Dim objWriter As DbWriter

        Try
            GetRelatedCodeID_SQL = 0
            bCleanUp = False
            hMyConnection = g_ConnectionString
            'If g_DBObject.DB_GetDBMake(g_ConnectionString) = RMLoginLib.DTGDatabaseTypes.DBMS_IS_SQLSRVR Then
            '    If g_DBObject.DB_ConnInUse(g_ConnectionString) Then
            '        hMyConnection = g_DBObject.DB_OpenDatabase(g_hEnv, g_RmDSN, 0)
            '        bCleanUp = True
            '    Else
            '    End If
            'End If
            sSQL = ""
            sSQL = sSQL & "SELECT RELATED_CODE_ID"
            sSQL = sSQL & " FROM CODES"
            sSQL = sSQL & " WHERE CODES.CODE_ID = " & lCodeId

            '      iRdSt = g_DBObject.DB_CreateRecordset(hMyConnection, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If Not g_DBObject.DB_EOF(iRdSt) Then
            '	GetRelatedCodeID_SQL = GetDataLong(iRdSt, 1)
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetRelatedCodeID_SQL = objReader.GetInt32(1)
            End If

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction.GetRelatedCodeID_SQL|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
        End Try

    End Function


    Sub Dbg(ByRef s As String)
#If BB_DEBUG Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression BB_DEBUG did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "Dbg", -1, -1, "RMJuRuLib.dll", s, False
#End If


    End Sub

    '---------------------------------------------------------------------------------------
    ' Procedure : DeleteRecord
    ' DateTime  : 4/18/2006 07:43
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : lReturnCode is a number defining why the record was not deleted
    '---------------------------------------------------------------------------------------
    '
    Public Function DeleteRecordTemporary(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDBFormat As String, ByRef lReturnCode As Integer, ByRef sTableName As String) As Integer
        Const sFunctionName As String = "DeleteRecordTemporary"
        Dim objReader As dbReader
        Dim lTemp As Integer
        Dim objDeleteReturn As CJRDeleteReturn
        Dim sPreDeleteDateDBFormat As String
        Dim sSQL2 As String
        Dim sSQL3 As String
        Dim sSQL4 As String
        Dim sSQL5 As String
        Dim sSQL6 As String
        Dim sSQLDelete As String
        Dim sSQLDestroy As String
        Dim sTemp As String
        Dim sTwoYearDate As String
        Dim objDbConnection As DbConnection
        Dim objDbTransaction As DbTransaction

        Try
            objDbConnection = DbFactory.GetDbConnection(g_ConnectionString)
            objDbConnection.Open()
            objDbTransaction = objDbConnection.BeginTransaction()

            objDeleteReturn = New CJRDeleteReturn
            objDeleteReturn.SetValues()

            DeleteRecordTemporary = objDeleteReturn.cSuccess

            lReturnCode = objDeleteReturn.cSuccess

            sPreDeleteDateDBFormat = ""

            sSQLDelete = ""
            sSQLDelete = sSQLDelete & "UPDATE"
            sSQLDelete = sSQLDelete & " " & sTableName
            sSQLDelete = sSQLDelete & " SET DELETED_FLAG = 1"
            sSQLDelete = sSQLDelete & " DTTM_RCD_LAST_UPD = '" & System.DateTime.Now().ToString("yyyyMMddHHmmss") & "'"
            sSQLDelete = sSQLDelete & " UPDATED_BY_USER = '" & objUser.LoginName & "'"
            sSQLDelete = sSQLDelete & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQLDelete = sSQLDelete & " AND EFFECTIVE_DATE = '" & sEffectiveDateDBFormat & "'"

            sSQLDestroy = ""
            sSQLDestroy = sSQLDestroy & "DELETE FROM"
            sSQLDestroy = sSQLDestroy & " " & sTableName
            sSQLDestroy = sSQLDestroy & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQLDestroy = sSQLDestroy & " AND EFFECTIVE_DATE = '" & sEffectiveDateDBFormat & "'"

            sTwoYearDate = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Year, 2, Today))


            'if the rule is more then two years in the future you can DESTROY it
            If sEffectiveDateDBFormat > System.DateTime.Now().ToString("yyyyMMdd") Then
                objDbConnection.ExecuteNonQuery(sSQLDestroy, objDbTransaction)
                lReturnCode = objDeleteReturn.cSuccess
                'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objDeleteReturn = Nothing
                objDbTransaction.Commit()
                Exit Function
            End If

            'do not delete the only rule
            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT COUNT(JURIS_ROW_ID)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL2)
            If (objReader.Read()) Then
                lReturnCode = objDeleteReturn.cOnlyRule
                'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objDeleteReturn = Nothing
                Exit Function
            Else
                lTemp = objReader.GetInt32(1)
                If lTemp < 2 Then
                    SafeCloseRecordset(objReader)
                    lReturnCode = objDeleteReturn.cOnlyRule
                    'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objDeleteReturn = Nothing

                    Exit Function
                End If
            End If

            SafeCloseRecordset(objReader)

            'do not delete the most current rule
            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT MIN(EFFECTIVE_DATE)"
            sSQL3 = sSQL3 & " FROM " & sTableName
            sSQL3 = sSQL3 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL3 = sSQL3 & " AND EFFECTIVE_DATE > '" & sEffectiveDateDBFormat & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If (objReader.Read()) Then
                lReturnCode = objDeleteReturn.cMostCurrentRule
                objDeleteReturn = Nothing
                Exit Function
            Else
                sTemp = Trim(objReader.GetString(1) & "")
                If sTemp = "" Then
                    SafeCloseRecordset(objReader)
                    lReturnCode = objDeleteReturn.cMostCurrentRule
                    'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objDeleteReturn = Nothing

                    Exit Function
                End If
            End If

            'iRdset = g_DBObject.DB_CreateRecordset(g_ConnectionString, sSQL3, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If g_DBObject.DB_EOF(iRdset) Then
            '	SafeCloseRecordset(objReader)
            '	lReturnCode = objDeleteReturn.cMostCurrentRule
            '	'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '	objDeleteReturn = Nothing
            '	
            '	Exit Function

            'End If
            SafeCloseRecordset(objReader)

            'check for existing claim that is effected by deleting rule
            'this means trapping events between rule effective dates
            'to delete a rule there can be no event after the rule not covered by a different rule

            'because sSQL3 finds the next rule see if any EVENT-CLAIMs will be affected
            'find the rule before the delete target
            sSQL4 = ""
            sSQL4 = sSQL4 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL4 = sSQL4 & " FROM " & sTableName
            sSQL4 = sSQL4 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL4 = sSQL4 & " AND EFFECTIVE_DATE < '" & sEffectiveDateDBFormat & "'"

            '      iRdset = g_DBObject.DB_CreateRecordset(g_ConnectionString, sSQL4, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If g_DBObject.DB_EOF(iRdset) Then
            'Else
            '	sPreDeleteDateDBFormat = Trim(objReader.GetString( 1) & "")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL4)
            If (objReader.Read()) Then
                sPreDeleteDateDBFormat = Trim(objReader.GetString(1) & "")
            End If

            SafeCloseRecordset(objReader)

            'if we have a preceding rule the delete may be ok.
            'If Trim(sPreDeleteDateDBFormat) > "" Then
            '    g_DBObject.DB_SQLExecute(g_ConnectionString, sSQLDestroy)
            '    lReturnCode = objDeleteReturn.cSuccess
            '    'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '    objDeleteReturn = Nothing

            '    Exit Function
            'End If

            'find the Line-Of-Business value from codes
            sSQL5 = ""
            sSQL5 = sSQL5 & "SELECT CODE_ID"
            sSQL5 = sSQL5 & " FROM CODES, GLOSSARY"
            sSQL5 = sSQL5 & " WHERE CODES.TABLE_ID = GLOSSARY.TABLE_ID"
            sSQL5 = sSQL5 & " AND CODES.SHORT_CODE = 'WC'"
            sSQL5 = sSQL5 & " AND GLOSSARY.SYSTEM_TABLE_NAME = 'LINE_OF_BUSINESS'"

            sSQL6 = ""
            sSQL6 = sSQL6 & "SELECT EVENT.DATE_OF_EVENT"
            sSQL6 = sSQL6 & " FROM EVENT, CLAIM"
            sSQL6 = sSQL6 & " WHERE EVENT.EVENT_ID = CLAIM.EVENT_ID"
            sSQL6 = sSQL6 & " AND EVENT.DATE_OF_EVENT < (" & sSQL3 & ")"
            sSQL6 = sSQL6 & " AND EVENT.DATE_OF_EVENT >= '" & sEffectiveDateDBFormat & "'"
            sSQL6 = sSQL6 & " AND CLAIM.LINE_OF_BUS_CODE = (" & sSQL5 & ")"
            sSQL6 = sSQL6 & " AND CLAIM.FILING_STATE_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL5)
            If objReader.Read() Then
                DbFactory.ExecuteNonQuery(g_ConnectionString, sSQLDestroy)
                lReturnCode = objDeleteReturn.cSuccess
                'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objDeleteReturn = Nothing

                Exit Function
            Else
                lReturnCode = objDeleteReturn.cAffectedEvents
            End If

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteRecordTemporary = Err.Number
            SafeCloseRecordset(objReader)

            'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objDeleteReturn = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

            'UPGRADE_NOTE: Object objDeleteReturn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objDeleteReturn = Nothing
        End Try

    End Function
    Public Function lGetNextUID(ByVal sTableName As String) As Integer


        Dim rs As Short
        Dim sSQL As String
        Dim lTableID As Integer
        Dim lNextUID As Integer
        Dim lOrigUID As Integer
        Dim lRows As Integer
        Dim lCollisionRetryCount As Integer
        Dim lErrRetryCount As Integer
        Dim sSaveError As String
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim objDbConnection As DbConnection
        Dim objDbTransaction As DbTransaction

        Try
            sTableName = Trim(sTableName)

            Do
                objReader = DbFactory.GetDbReader(g_ConnectionString, "SELECT TABLE_ID,NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" & sTableName & "'")
                If (objReader.Read()) Then
                    g_sErrDescription = "modFunctions.lGetNextUID(" & sTableName & ") Specified table does not exist in glossary.;  "
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                    Err.Raise(32001, "modFunctions.lGetNextUID(" & sTableName & ")", "Specified table does not exist in glossary.")
                End If

                lTableID = objReader.GetInt32(1)
                lNextUID = objReader.GetInt32(2)

                ' Compute next id
                lOrigUID = lNextUID

                If lOrigUID Then lNextUID = lNextUID + 1 Else lNextUID = 2

                ' try to reserve id (searched update)
                sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " & lNextUID & " WHERE TABLE_ID = " & lTableID

                ' only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
                If lOrigUID Then sSQL = sSQL & " AND NEXT_UNIQUE_ID = " & lOrigUID

                ' Try update

                objDbConnection = DbFactory.GetDbConnection(g_ConnectionString)
                objDbConnection.Open()
                objDbTransaction = objDbConnection.BeginTransaction()

                objDbConnection.ExecuteNonQuery(sSQL)

                objDbTransaction.Commit()

                If Err.Number <> 0 Then
                    sSaveError = Err.Description

                    lErrRetryCount = lErrRetryCount + 1

                    If lErrRetryCount >= 5 Then
                        Err.Raise(29999, "modFunctions.lGetNextUID(" & sTableName & ")", sSaveError)
                        Exit Function
                    End If
                Else
                    ' if success, return
                    If lRows = 1 Then
                        lGetNextUID = lNextUID - 1
                        Exit Function ' success
                    Else
                        lCollisionRetryCount = lCollisionRetryCount + 1 ' collided with another user - try again (up to 1000 times)
                    End If
                End If



            Loop While (lErrRetryCount < 5) And (lCollisionRetryCount < 1000)

            If lCollisionRetryCount >= 1000 Then

                Err.Raise(32001, "modFunctions.lGetNextUID(" & sTableName & ")", "Collision timeout. Server load too high. Please wait and try again.")
                Exit Function
            End If

            lGetNextUID = 0

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Trim(g_sErrDescription) & Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modSaveFunctions.lGetNextUID(" & sTableName & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetYesCodeID() As Integer
        Const sFunctionName As String = "GetYesCodeID"
        Dim objReader As dbReader
        Dim sSQL As String

        Try

            GetYesCodeID = 0
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " CODES.CODE_ID"
            sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY_TEXT"
            sSQL = sSQL & " WHERE TABLE_NAME = 'Yes/No'"
            sSQL = sSQL & " AND GLOSSARY_TEXT.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES_TEXT.CODE_DESC = 'Yes'"

            '      iRdset = g_DBObject.DB_CreateRecordset(g_ConnectionString, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	GetYesCodeID = objReader.GetInt32( "CODE_ID")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                GetYesCodeID = objReader.GetInt32("CODE_ID")
            End While



        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'GetYesCodeID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function GetYesCodeIDNoConn(ByRef objUser As Riskmaster.Security.UserLogin) As Integer

        Const sFunctionName As String = "GetYesCodeIDNoConn"
        Dim objReader As dbReader
        Dim sSQL As String
        Dim m_JurisRowID As String

        Try

            GetYesCodeIDNoConn = 0


            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " CODES.CODE_ID"
            sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY_TEXT"
            sSQL = sSQL & " WHERE TABLE_NAME = 'Yes/No'"
            sSQL = sSQL & " AND GLOSSARY_TEXT.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES_TEXT.CODE_DESC = 'Yes'"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_JurisRowID = objReader.GetInt32("CODE_ID")
            End If


        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'GetYesCodeIDNoConn = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function GetNoCodeID() As Integer
        Const sFunctionName As String = "GetNoCodeID"
        Dim objReader As dbReader
        Dim sSQL As String

        Try

            GetNoCodeID = -1
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " CODES.CODE_ID"
            sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY_TEXT"
            sSQL = sSQL & " WHERE TABLE_NAME = 'Yes/No'"
            sSQL = sSQL & " AND GLOSSARY_TEXT.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES_TEXT.CODE_DESC = 'No'"

            '      iRdset = g_DBObject.DB_CreateRecordset(g_ConnectionString, sSQL, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	GetNoCodeID = objReader.GetInt32( "CODE_ID")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetNoCodeID = objReader.GetInt32("CODE_ID")
            End If


        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetNoCodeID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function GetNoCodeIDNoConn(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "GetNoCodeIDNoConn"
        Dim objReader As dbReader
        Dim sSQL As String

        Try

            GetNoCodeIDNoConn = -1

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " CODES.CODE_ID"
            sSQL = sSQL & " FROM CODES, CODES_TEXT, GLOSSARY_TEXT"
            sSQL = sSQL & " WHERE TABLE_NAME = 'Yes/No'"
            sSQL = sSQL & " AND GLOSSARY_TEXT.TABLE_ID = CODES.TABLE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND CODES_TEXT.CODE_DESC = 'No'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetNoCodeIDNoConn = objReader.GetInt32("CODE_ID")
            End If

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            'jtodd22 02/13/2008 do not return any value --GetNoCodeIDNoConn = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CloneEDI
    ' DateTime  : 3/17/2005 07:37
    ' Author    : jtodd22
    ' Purpose   :
    ' Notes     : ***There are three types of db records to create******************
    ' ..........: ***The Release Type record*****************************************
    ' ..........: ***The Release Record Types****************************************
    ' ..........: ***The Release Record Details (DNs)********************************
    ' ..........: *******************************************************************
    ' ..........: ***Create it all in memory and then save as a Transaction**********
    ' ..........: *******************************************************************
    ' ..........: ***Create the two target collections by loading the release and**
    ' ..........: ***jurisdiction.  If any one of the three has a .Count > 0 then****
    ' ..........: ***fail the cloning because the release and jurisdiction exists.***
    ' ..........: *******************************************************************

    '---------------------------------------------------------------------------------------
    '
    Public Function CloneEDI(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lEDIStdReleaseRowID As Integer, ByRef lTargetJurisRowID As Integer, ByRef sEffectiveDateDTG As String) As Integer
        Const sFunctionName As String = "CloneEDI"
        Dim CheckSum As Short
        Dim i As Short
        Dim ii As Short

        Dim objTarRelease As CJREDIStdRelease
        Dim colTarRecordTypes As CJREDIStdRecordTypes
        Dim colTarRecordFields As CJREDIStdRecordDNs
        Dim objTarSTDConditions As CJREDIStdConditional
        Dim lEDIRecdIfRowId As Integer

        Dim objDbConnection As DbConnection
        Dim objDbTransaction As DbTransaction

        Try

            CloneEDI = -1

            objTarRelease = New CJREDIStdRelease
            colTarRecordTypes = New CJREDIStdRecordTypes
            colTarRecordFields = New CJREDIStdRecordDNs
            'Do the existence check
            CheckSum = 0
            CheckSum = CheckSum + objTarRelease.LoadData(objUser, lTargetJurisRowID, 0, lEDIStdReleaseRowID, sEffectiveDateDTG)
            CheckSum = CheckSum + colTarRecordTypes.LoadData(objUser, lTargetJurisRowID, (objTarRelease.TableRowID), lEDIStdReleaseRowID)
            CheckSum = CheckSum + colTarRecordFields.CheckForExistingJurisData(objUser, lTargetJurisRowID, lEDIStdReleaseRowID, sEffectiveDateDTG)

            'If there were error, should have exited by this point

            If objTarRelease.TableRowID > 0 Then
                Err.Raise(vbObjectError + 70001, sClassName & "." & sFunctionName, "Data exists.")
                CloneEDI = Err.Number
                Exit Function
            End If
            If colTarRecordTypes.Count > 0 Then
                Err.Raise(vbObjectError + 70001, sClassName & "." & sFunctionName, "Data exists.")
                CloneEDI = Err.Number
                Exit Function
            End If
            If colTarRecordFields.Count > 0 Then
                Err.Raise(vbObjectError + 70001, sClassName & "." & sFunctionName, "Data exists.")
                CloneEDI = Err.Number
                Exit Function
            End If

            'clear the objects
            'UPGRADE_NOTE: Object objTarRelease may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objTarRelease = Nothing
            'UPGRADE_NOTE: Object colTarRecordTypes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            colTarRecordTypes = Nothing
            'UPGRADE_NOTE: Object colTarRecordFields may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            colTarRecordFields = Nothing

            objTarRelease = New CJREDIStdRelease
            colTarRecordTypes = New CJREDIStdRecordTypes
            colTarRecordFields = New CJREDIStdRecordDNs

            'do the Industry standard data loads
            CheckSum = 0
            CheckSum = CheckSum + objTarRelease.LoadData(objUser, 0, 0, lEDIStdReleaseRowID)
            CheckSum = CheckSum + colTarRecordTypes.LoadData(objUser, 0, 0, lEDIStdReleaseRowID)
            CheckSum = CheckSum + colTarRecordFields.LoadDataForCloning(objUser, 0, lEDIStdReleaseRowID, sEffectiveDateDTG)


            objDbConnection = DbFactory.GetDbConnection(g_ConnectionString)
            objDbConnection.Open()
            objDbTransaction = objDbConnection.BeginTransaction()
            objTarRelease.EffectiveDate = sEffectiveDateDTG
            objTarRelease.JurisRowID = lTargetJurisRowID
            objTarRelease.ReleaseRowID = lEDIStdReleaseRowID
            objTarRelease.SaveDataClone(objUser)

            For i = 1 To colTarRecordTypes.Count
                colTarRecordTypes.Item(i).JurisRowID = lTargetJurisRowID
                colTarRecordTypes.Item(i).PtTableRowID = objTarRelease.TableRowID
                colTarRecordTypes.Item(i).ReleaseRowID = objTarRelease.ReleaseRowID
                colTarRecordTypes.Item(i).SaveDataClone(objUser)
                With colTarRecordFields
                    For ii = 1 To .Count
                        If .Item(ii).RecordTypeID = colTarRecordTypes.Item(i).RecordTypeID Then
                            .Item(ii).PtPtTableRowID = objTarRelease.TableRowID
                            .Item(ii).PtTableRowID = colTarRecordTypes.Item(i).TableRowID
                            .Item(ii).ReleaseRowID = objTarRelease.ReleaseRowID
                            .Item(ii).JurisRowID = lTargetJurisRowID
                            'This statement should always be before the savedataclone statement
                            lEDIRecdIfRowId = .Item(ii).TableRowID
                            .Item(ii).SaveDataClone(objUser)

                            ''''''DO NOT DELETE THIS COMMENTED PORTION. THIS IS NEEDED FOR FUTURE ENHANCEMENT
                            ''''''                        'Load Conditional data for the DN field
                            ''''''                        Set objTarSTDConditions = New CJREDIStdConditional
                            ''''''                        If objTarSTDConditions.LoadData(objUser, lEDIRecdIfRowId, 0) = -1 Then
                            ''''''
                            ''''''                            If objTarSTDConditions.TableRowID <> 0 Then
                            ''''''                                objTarSTDConditions.RecdIfRowID = .Item(ii).TableRowID
                            ''''''                                objTarSTDConditions.TableRowID = 0
                            ''''''                                objTarSTDConditions.SaveData objUser, lTargetJurisRowID
                            ''''''                            End If
                            ''''''                        End If


                        End If
                    Next
                End With
            Next

            objDbTransaction.Commit()

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            CloneEDI = Err.Number
            'g_DBObject.DB_AbortTrans(g_ConnectionString)
            'UPGRADE_NOTE: Object objTarRelease may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objTarRelease = Nothing
            'UPGRADE_NOTE: Object colTarRecordTypes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            colTarRecordTypes = Nothing
            'UPGRADE_NOTE: Object colTarRecordFields may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            colTarRecordFields = Nothing

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objTarRelease may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objTarRelease = Nothing
            'UPGRADE_NOTE: Object colTarRecordTypes may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            colTarRecordTypes = Nothing
            'UPGRADE_NOTE: Object colTarRecordFields may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            colTarRecordFields = Nothing

        End Try

    End Function


    Public Function IsAlphaNumeric(ByRef sAlphaNum As String) As Boolean
        Dim blnOnlyAlphaNumFound As Boolean
        Dim sAlphaNumChar As String

        blnOnlyAlphaNumFound = True

        Dim intCnt As Object
        'UPGRADE_WARNING: Couldn't resolve default property of object intCnt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        intCnt = 1
        'UPGRADE_WARNING: Couldn't resolve default property of object intCnt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        While intCnt <= Len(sAlphaNum) And blnOnlyAlphaNumFound
            'UPGRADE_WARNING: Couldn't resolve default property of object intCnt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sAlphaNumChar = Mid(sAlphaNum, intCnt, 1)
            If Not (IsDigit(sAlphaNumChar) Or IsAlpha(sAlphaNumChar)) Then
                blnOnlyAlphaNumFound = False
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object intCnt. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            intCnt = intCnt + 1
        End While

        IsAlphaNumeric = blnOnlyAlphaNumFound


    End Function


    Public Function IsAlpha(ByRef sAlpha As String) As Boolean
        IsAlpha = False

        If Len(sAlpha) = 1 Then
            If (sAlpha >= "a" And sAlpha <= "z") Or (sAlpha >= "A" And sAlpha <= "Z") Then
                IsAlpha = True
            End If
        End If


    End Function


    Public Function IsDigit(ByRef sDigit As String) As Boolean
        IsDigit = False

        If Len(sDigit) = 1 Then
            If sDigit >= "0" And sDigit <= "9" Then
                IsDigit = True
            End If
        End If


    End Function

    Function bIsDate(ByVal sDate As String) As Short
        If IsNumeric(sDate) And Len(sDate) = 8 Then
            bIsDate = True
        Else
            bIsDate = False
        End If


    End Function

    Public Function AdjusterDiary(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "AdjusterDiary"
        Dim objReader As dbReader
        Try

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AdjusterDiary = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : AdjusterDiarySpamerRateTableLoad
    ' DateTime  : 2/15/2006 16:53
    ' Author    : jtodd22
    ' Purpose   : To send Diaries to Adjuster when a new spendable income table
    '...........: when the table is loaded after the tables effective date (Alaska)
    ' Notes.....: sEntryName..should be "New Rate Table"
    ' Notes.....: sEntryName..Name for Diary
    '---------------------------------------------------------------------------------------
    '               1234567890123456789012345678901234567890
    Public Function AdjusterDiarySpamerRateTableLoad(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sMessage As String, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDTG As String, ByRef sDBLoadDateDTG As String, ByRef sDSN As String) As Integer
        Const sFunctionName As String = "AdjusterDiarySpamerRateTableLoad"
        Const sEntryName As String = "New Rate Table"
        Const sEntryNotes As String = " TTD Rate and Payments May Be Affected By New Rate Table"
        Dim db As Short
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim iRdSet2 As Short
        Dim iRdSet3 As Short
        Dim hEnv As Integer
        Dim lDiaryID As Integer
        Dim sAdjusterLogin As String
        Dim sTmp As String
        Dim sSQL2 As String
        Dim sSQL3 As String

        Try

            AdjusterDiarySpamerRateTableLoad = 0

            'we need a list of claims that maybe affected by new rate tables
            sSQL2 = ClaimsAffectedByRateTable(lJurisRowID, sDBLoadDateDTG, sEffectiveDateDTG)

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL2)
            If (objReader.Read()) Then
                Exit Function
            End If




            SafeCloseRecordset(objReader)

            'we need a list of current adjusters to notify, get the claim numbers at the same time
            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT"
            sSQL3 = sSQL3 & " CLAIM.CLAIM_NUMBER, ENTITY.RM_USER_ID"
            sSQL3 = sSQL3 & " FROM CLAIM,CLAIM_ADJUSTER,ENTITY"
            sSQL3 = sSQL3 & " WHERE CLAIM_ADJUSTER.ADJUSTER_EID = ENTITY.ENTITY_ID"
            sSQL3 = sSQL3 & " AND CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID"
            sSQL3 = sSQL3 & " AND CLAIM_ADJUSTER.CLAIM_ID IN (" & sSQL2 & ")"
            sSQL3 = sSQL3 & " AND CURRENT_ADJ_FLAG <> 0"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If Not objReader.Read() Then
                Exit Function
            End If
            'we have adjusters to notify
            ''g_DBObject.DB_MoveFirst iRdset2
            While objReader.Read()
                sAdjusterLogin = GetLogin(objUser, objReader.GetInt32("RM_USER_ID"), sDSN)
                If sAdjusterLogin > "" Then
                    'the lGetNextUID function uses g_ConnectionString so do lGetNextUID before doing the add new
                    'otherwise you will have three recordsets open at the same time
                    lDiaryID = lGetNextUID("WPA_DIARY_ENTRY")

                    objReader = DbFactory.GetDbReader(g_ConnectionString, "SELECT * FROM WPA_DIARY_ENTRY WHERE ENTRY_ID = -1")

                    objWriter = DbFactory.GetDbWriter(objReader, False)

                    objWriter.Fields.Add("ENTRY_ID", lDiaryID)
                    objWriter.Fields.Add("ENTRY_NAME", sEntryName)
                    objWriter.Fields.Add("ENTRY_NOTES", "Claim #" & objReader.GetString("CLAIM_NUMBER") & sEntryNotes)
                    objWriter.Fields.Add("CREATE_DATE", System.DateTime.Now().ToString("yyyyMMdd") & System.DateTime.Now().ToString("HHmmss"))
                    objWriter.Fields.Add("PRIORITY", 2)
                    objWriter.Fields.Add("STATUS_OPEN", 1) 'True
                    objWriter.Fields.Add("AUTO_CONFIRM", 0) 'False
                    '"ASSIGNED_USER" is a string, VARCHAR(8) NULL, is the adjusters login name
                    objWriter.Fields.Add("ASSIGNED_USER", sAdjusterLogin) 'assigned to affected adjuster
                    '"ASSIGNING_USER" is a string, VARCHAR(8) NULL, is creator's login name
                    objWriter.Fields.Add("ASSIGNING_USER", objUser.LoginName) 'current user
                    objWriter.Fields.Add("ASSIGNED_GROUP", "")
                    objWriter.Fields.Add("IS_ATTACHED", 0) 'False
                    objWriter.Fields.Add("ATTACH_TABLE", "")
                    ''result = objWriter.Fields( "ATT_FORM_CODE", "")
                    ''result = objWriter.Fields( "ATTACH_RECORDID", lAttachRecordID)
                    ''result = objWriter.Fields( "REGARDING", Left(sRegarding, 70))
                    ''g_DBObject.DB_PutData iRdSet3, "ATT_SEC_REC_ID", 0
                    objWriter.Fields.Add("COMPLETE_TIME", "235959")
                    objWriter.Fields.Add("COMPLETE_DATE", System.DateTime.Now().AddDays(1).ToString("yyyyMMdd"))
                    objWriter.Fields.Add("DIARY_VOID", 0)
                    objWriter.Fields.Add("DIARY_DELETED", 0)
                    objWriter.Fields.Add("NOTIFY_FLAG", 0)
                    objWriter.Fields.Add("ROUTE_FLAG", 0)
                    objWriter.Fields.Add("ESTIMATE_TIME", 0)
                    objWriter.Fields.Add("AUTO_ID", 0)
                    objWriter.Execute()

                End If
            End While
            AdjusterDiarySpamerRateTableLoad = -1
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AdjusterDiarySpamerRateTableLoad = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
        End Try

    End Function

    Public Function AdjusterEmail(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "AdjusterEmail"
        Dim objReader As dbReader
        Try

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AdjusterEmail = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function AdjusterAffectedMaintainance(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sMessage As String, ByRef lJurisRowID As Integer, ByRef sDBLoadDateDTG As String, ByRef sEffectiveDateDTG As String) As Integer
        Const sFunctionName As String = "AdjusterAffectedMaintainance"
        Dim iCount As Short

        Dim sSQL2 As String
        Dim sSQL3 As String

        Dim objReader As DbReader
        Try

            AdjusterAffectedMaintainance = 0

            iCount = 0
            sMessage = ""

            sSQL2 = ClaimsAffectedByRateTable(lJurisRowID, sDBLoadDateDTG, sEffectiveDateDTG)

            'iRdSet2 = g_DBObject.DB_CreateRecordset(g_dbConn1, sSQL2, ROCKETCOMLib.DTGDBRecordsetType.DB_FORWARD_ONLY, 0)
            'If g_DBObject.DB_EOF(iRdSet2) Then
            '	SafeDropRecordset(iRdSet2)
            '	Exit Function
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL2)
            If Not objReader.Read() Then
                Exit Function
            End If
            SafeDropRecordset(objReader)
            'we need a list of current adjusters to notify
            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT"
            sSQL3 = sSQL3 & " ENTITY.FIRST_NAME,ENTITY.MIDDLE_NAME,ENTITY.LAST_NAME"
            sSQL3 = sSQL3 & ",ENTITY.RM_USER_ID"
            sSQL3 = sSQL3 & " FROM CLAIM_ADJUSTER,ENTITY"
            sSQL3 = sSQL3 & " WHERE CLAIM_ADJUSTER.ADJUSTER_EID = ENTITY.ENTITY_ID"
            ''sSQL3 = sSQL3 & " AND CLAIM_ADJUSTER.CLAIM_ID IN (5,7,8)"
            sSQL3 = sSQL3 & " AND CLAIM_ADJUSTER.CLAIM_ID IN (" & sSQL2 & ")"
            sSQL3 = sSQL3 & " AND CURRENT_ADJ_FLAG <> 0"
            sSQL3 = sSQL3 & " AND (ENTITY.RM_USER_ID < 1 OR ENTITY.RM_USER_ID IS NULL)"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If Not objReader.Read() Then
                Exit Function
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            While objReader.Read()
                sMessage = sMessage & objReader.GetString("LAST_NAME") & ", " & objReader.GetString("FIRST_NAME") & " " & objReader.GetString("MIDDLE_NAME") & vbCrLf
            End While

            AdjusterAffectedMaintainance = -1

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AdjusterAffectedMaintainance = Err.Number
            SafeDropRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeDropRecordset(objReader)
        End Try

    End Function


    Public Function CheckTaxStatusTable(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sMessage As String) As Integer
        Const sFunctionName As String = "CheckTaxStatusTable"
        Dim i As Short
        Dim sJurisPostalAbb As String

        Try

            CheckTaxStatusTable = 0

            '    sJurisPostalAbb = GetStatePostalCode_SQL(lJurisRowID)
            '
            '    Set cTaxCodes = New CJRCodes
            '    If cTaxCodes.LoadData(objUser, "TAX_STATS_" & UCase(sJurisPostalAbb) & "_CODE") = -1 Then
            '        For i = cTaxCodes.Count To 1 Step -1
            '            If IsNull(cTaxCodes.Item(i).CodeDesc) Or Trim$((cTaxCodes.Item(i).CodeDesc)) = "" Then cTaxCodes.Remove (i)
            '        Next i
            '        If cTaxCodes.Count = 0 Then
            '            sMessage = "The Jurisdictional Tax Filing Status Code table was not found or is not populated." & vbCrLf & _
            ''                     "Without this data the display will fail." & vbCrLf & _
            ''                     "Please insure this code table exist (TAX_STATS_" & UCase(sJurisPostalAbb) & "_CODE) and is populated."
            '            Set cTaxCodes = Nothing
            '            CheckTaxStatusTable = 90001
            '            subHourGlass False
            '            Exit Function
            '        End If
            '    Else
            '        sMessage = "There was an error loading the Jurisdictional Tax Filing Status Code table." & vbCrLf & _
            ''                 "Without this data the display will fail." & vbCrLf & _
            ''                 "Please insure this code table exist (TAX_STATS_" & UCase(sJurisPostalAbb) & "_CODE) and is populated."
            '        CheckTaxStatusTable = 90002
            '        Set cTaxCodes = Nothing
            '        Exit Function
            '    End If
            '    CheckTaxStatusTable = -1
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            CheckTaxStatusTable = Err.Number
            '    SafeDropRecordset iRdSet2
            '    SafeDropRecordset iRdSet3
            '    ModMain.DBDisconnect
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            '    SafeDropRecordset iRdSet2
            '    SafeDropRecordset iRdSet3
            '    ModMain.DBDisconnect
        End Try

    End Function

    Private Function ClaimsAffectedByRateTable(ByRef lJurisRowID As Integer, ByRef sDBLoadDateDTG As String, ByRef sEffectiveDateDTG As String) As String
        Dim sSQL2 As String
        'we need a list of claims that maybe affected by new rate tables

        ClaimsAffectedByRateTable = ""

        sSQL2 = ""
        sSQL2 = sSQL2 & "SELECT"
        sSQL2 = sSQL2 & " CLAIM.CLAIM_ID"
        sSQL2 = sSQL2 & " FROM CLAIM, CLAIM_AWW, EVENT"
        sSQL2 = sSQL2 & " WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID"
        sSQL2 = sSQL2 & " AND CLAIM.CLAIM_ID = CLAIM_AWW.CLAIM_ID"
        sSQL2 = sSQL2 & " AND EVENT.DATE_OF_EVENT >= '" & sEffectiveDateDTG & "'"
        sSQL2 = sSQL2 & " AND EVENT.DATE_OF_EVENT <= '" & sDBLoadDateDTG & "'"
        sSQL2 = sSQL2 & " AND CLAIM_AWW.TTD_BASE > 0 "
        sSQL2 = sSQL2 & " AND CLAIM.FILING_STATE_ID = " & lJurisRowID

        ClaimsAffectedByRateTable = sSQL2


    End Function

    Public Function GetLogin(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lRMUserID As Integer, ByRef sDSN As String) As String
        Const sFunctionName As String = "GetLogin"
        Dim lDSNID As Integer
        Dim db As Short
        Dim sSQL As String
        Dim objReader As dbReader
        Dim result As Integer
        Dim sLoginName As String

        Try

            GetLogin = ""

            sLoginName = ""

            If lRMUserID < 1 Then Exit Function

            lDSNID = objUser.DatabaseId

            sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME"
            sSQL = sSQL & " FROM USER_DETAILS_TABLE"
            sSQL = sSQL & " WHERE USER_DETAILS_TABLE.DSNID = " & lDSNID
            sSQL = sSQL & " AND USER_DETAILS_TABLE.USER_ID = " & lRMUserID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                sLoginName = Trim(objReader.GetString("LOGIN_NAME") & "")
            End If


            SafeCloseRecordset(objReader)

            GetLogin = sLoginName

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)
            ''ModMain.DBDisconnect
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            ''ModMain.DBDisconnect
        End Try

    End Function

    Public Function BodyMembersExists(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Boolean
        Const sFunctionName As String = "BodyMembersExists"
        Dim colBodyMembers As CJRBodyMembers
        Dim lReturn As Integer

        Try

            BodyMembersExists = False
            colBodyMembers = New CJRBodyMembers
            lReturn = colBodyMembers.LoadDataByEventDate(objUser, lJurisRowID, sDateOfEventDTG)
            If lReturn = -1 Then
                If colBodyMembers.Count > 0 Then
                    BodyMembersExists = True
                End If
            End If
            'UPGRADE_NOTE: Object colBodyMembers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            colBodyMembers = Nothing
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
End Module

