Option Strict Off
Option Explicit On
Public Class CJRJurisdictions
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRJurisdiction"

    'Class properties, local copy
    Private mCol As Collection

    Private Function Add(ByRef JurisRowID As Integer, ByRef JurisdictionShortName As String, ByRef JurisdictionLongName As String, ByRef sKey As String) As CJRJurisdiction
        'create a new object
        Dim objNewMember As CJRJurisdiction
        objNewMember = New CJRJurisdiction


        'set the properties passed into the method
        objNewMember.JurisRowID = JurisRowID
        objNewMember.JurisdictionShortName = JurisdictionShortName
        objNewMember.JurisdictionLongName = JurisdictionLongName
        mCol.Add(objNewMember, sKey)

        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing




    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRJurisdiction
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Short
        Dim objReader As DbReader
        Dim sSQL As String
        Dim Jurisdiction As CJRJurisdiction
        Try

            LoadData = 0


            sSQL = ""
            sSQL = "SELECT" & " STATE_ID, STATE_NAME, STATE_ROW_ID, DELETED_FLAG" & " FROM STATES WHERE STATE_ROW_ID > 0 ORDER BY STATE_ID ASC"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                Jurisdiction = New CJRJurisdiction
                With Jurisdiction
                    .JurisdictionLongName = Trim(objReader.GetString("STATE_NAME"))
                    .JurisdictionShortName = Trim(objReader.GetString("STATE_ID"))
                    .JurisRowID = objReader.GetInt32("STATE_ROW_ID")
                    Add(.JurisRowID, .JurisdictionShortName, .JurisdictionLongName, "k" & CStr(.JurisRowID))
                End With
                'UPGRADE_NOTE: Object Jurisdiction may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                Jurisdiction = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & " " & Err.Description
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    Public Function GetJurShortDescFromID(ByRef lJurisRowId As Integer) As String
        Dim i As Short
        Dim sDescription As String

        GetJurShortDescFromID = ""

        For i = 1 To mCol.Count()
            If Item(i).JurisRowID = lJurisRowId Then
                sDescription = Item(i).JurisdictionShortName
                sDescription = sDescription & " - "
                sDescription = sDescription & Item(i).JurisdictionLongName
                GetJurShortDescFromID = sDescription
                Exit For
            End If
        Next



    End Function
End Class

