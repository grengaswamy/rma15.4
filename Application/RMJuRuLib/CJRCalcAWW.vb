Option Strict Off
Option Explicit On
Public Class CJRCalcAWW
    '---------------------------------------------------------------------------------------
    ' Module    : CJRCalcAWW
    ' DateTime  : 7/15/2004 16:08
    ' Author    : jtodd22
    ' Purpose   : Defines the fake record for the AWW calculator options.
    ' Notes     : The reason for having a parent with children structure is to add more children
    ' ..........: at a later date without database changes and/or major rewrites.
    ' Notes     : This class has data loads from inside the DLL and from inside the EXE.  This
    '...........: causes issues with the database connections.  Beware of data fetches that
    '...........: are tailored to one or the other.
    '---------------------------------------------------------------------------------------
    Private Const sClassName As String = "CJRCalcAWW"
    Private Const sTableName1 As String = "WCP_AWW_SWCH"
    Private Const sTableName2 As String = "WCP_AWW_X_SWCH"

    'jtodd22 most primitive interface variables
    Private m_DataHasChanged As Boolean
    Private m_EffectiveDateDTG As String
    Private m_JurisRowID As Integer
    Private m_NoCodeID As Integer
    Private m_TableRowID As Integer
    Private m_Warning As String
    Private m_YesCodeID As Integer

    Private m_objUser As Riskmaster.Security.UserLogin
    'Class properties, local copy
    Private m_MonthlyCalculation As Integer
    Private m_BonusCommissionsIncentive As Integer
    Private m_ConcurrentEmployment As Integer
    Private m_Domicile As Integer
    Private m_EmployerPaidBenefitsContinued As Integer
    Private m_EmployerPaidBenefitsDiscontinued As Integer
    Private m_GratuitiesTips As Integer
    Private m_Meals As Integer
    Private m_TaxStatusSpendableIncome As Integer
    Private m_AllOtherAdvanage As Integer
    Private m_NumberOfWeeksInGrid As Integer
    Private m_DaysInGrid As Integer
    Private m_HoursInGrid As Integer
    Private m_ExcludeZeros As Integer 'used as boolean
    Private m_MeanPercentage As Double

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()

        'UPGRADE_NOTE: Object m_objUser may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        m_objUser = Nothing



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub



    '---------------------------------------------------------------------------------------
    ' Procedure : DLLFetchData
    ' DateTime  : 7/15/2004 14:47
    ' Author    : jtodd22
    ' Purpose   : This is an internal Data Fetch of the DLL.  There is no need to connect to the database.
    '---------------------------------------------------------------------------------------
    '
    Public Function DLLFetchData(ByVal lTableRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            DLLFetchData = 0
            ClearObject()
            sSQL = GetBaseSQL_WCP_AWW_SWCH()
            sSQL = sSQL & " FROM WCP_AWW_SWCH"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                DLLFetchData = AssignData(objReader)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DLLFetchData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".DLLFetchData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByTableRowID
    ' DateTime  : 7/15/2004 16:05
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lTableRowIDAWWParent As Integer) As Integer

        Dim objReader As DbReader
        Dim sSQL As String

        Try

            LoadDataByTableRowID = 0



            sSQL = GetBaseSQL_WCP_AWW_SWCH()
            sSQL = sSQL & " FROM WCP_AWW_SWCH"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowIDAWWParent
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                LoadDataByTableRowID = AssignData(objReader)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByTableRowID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function LoadDataByJurisRowIDEffectiveDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByJurisRowIDEffectiveDate = 0
            ClearObject()
            If Trim(sDateOfEventDTG & "") = "" Then sDateOfEventDTG = System.DateTime.Now().ToString("yyyyMMdd") 'jtodd22 04/01/2006
            'jtodd22 04/01/2006--If Trim(sDateOfEventDTG & "") = "" Then Exit Function



            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName1
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetBaseSQL_WCP_AWW_SWCH()
            sSQL = sSQL & " FROM WCP_AWW_SWCH"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"
            'g_ConnectionString = objUser.objRiskmasterDatabase.ConnectionString 'Nanda
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                LoadDataByJurisRowIDEffectiveDate = AssignData(objReader)
            Else
                'jtodd22 this looks odd, it simply means the code reached this point without error
                'jtodd22 the calling procedure must check for a TableRowID > 0
                LoadDataByJurisRowIDEffectiveDate = -1
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByJurisRowIDEffectiveDate = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByJurisRowIDEffectiveDate|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function Delete() As Integer


    End Function

    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_EffectiveDateDTG = ""
        m_MonthlyCalculation = 0
        m_BonusCommissionsIncentive = 0
        m_ConcurrentEmployment = 0
        m_Domicile = 0
        m_EmployerPaidBenefitsContinued = 0
        m_EmployerPaidBenefitsDiscontinued = 0
        m_GratuitiesTips = 0
        m_Meals = 0
        m_TaxStatusSpendableIncome = 0
        m_AllOtherAdvanage = 0
        m_NumberOfWeeksInGrid = 0
        m_DaysInGrid = 0
        m_HoursInGrid = 0
        m_ExcludeZeros = 0
        m_MeanPercentage = 0
        m_Warning = ""


    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim cAdditions As CJRCalcAWWAdditions
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = GetBaseSQL_WCP_AWW_SWCH()
            sSQL = sSQL & " FROM WCP_AWW_SWCH"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("WEEKS_IN_GD_NUM").Value = m_NumberOfWeeksInGrid
                If Not IsNothing(objReader.Item("EXCLUDE_ZEROS")) Then
                    objWriter.Fields("EXCLUDE_ZEROS").Value = m_ExcludeZeros
                    objWriter.Fields("MEAN_PERCENTAGE").Value = m_MeanPercentage
                End If

            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID("WCP_AWW_SWCH")
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("WEEKS_IN_GD_NUM", m_NumberOfWeeksInGrid)
                If Not IsNothing(objReader.Item("EXCLUDE_ZEROS")) Then
                    objWriter.Fields.Add("EXCLUDE_ZEROS", m_ExcludeZeros)
                    objWriter.Fields.Add("MEAN_PERCENTAGE", m_MeanPercentage)
                End If

            End If
            objWriter.Execute()
            SafeCloseRecordset(objReader)

            cAdditions = New CJRCalcAWWAdditions
            'jtodd22--we are using the parent table row id
            lTest = cAdditions.DLLFetchData(m_TableRowID)
            If cAdditions.Count = 0 Then
                ''        cAdditions.Add "", 0, 100, m_TableRowID, 0, "k100"
                ''        cAdditions.Add "", 0, 101, m_TableRowID, 0, "k101" - --Jurisdiction:
                ''        cAdditions.Add "", 0, 102, m_TableRowID, 0, "k102"---Effective Date:
                ''        cAdditions.Add "", 0, 103, m_TableRowID, 0, "k103"---TTD Multipler:
                cAdditions.Add("", 0, 104, m_TableRowID, 0, 0, "k104")
                cAdditions.Add("", 0, 105, m_TableRowID, 0, 0, "k105")
                cAdditions.Add("", 0, 106, m_TableRowID, 0, 0, "k106")
                cAdditions.Add("", 0, 107, m_TableRowID, 0, 0, "k107")
                cAdditions.Add("", 0, 108, m_TableRowID, 0, 0, "k108")
                cAdditions.Add("", 0, 109, m_TableRowID, 0, 0, "k109")
                cAdditions.Add("", 0, 110, m_TableRowID, 0, 0, "k110")
                cAdditions.Add("", 0, 111, m_TableRowID, 0, 0, "k111")
                cAdditions.Add("", 0, 112, m_TableRowID, 0, 0, "k112")
                cAdditions.Add("", 0, 113, m_TableRowID, 0, 0, "k113")
                cAdditions.Add("", 0, 114, m_TableRowID, 0, 0, "k114")
                cAdditions.Add("", 0, 115, m_TableRowID, 0, 0, "k115")
                cAdditions.Add("", 0, 116, m_TableRowID, 0, 0, "k116")
            End If
            cAdditions.Item("k104").TableRowIDFriendlyName = 104
            cAdditions.Item("k104").TableRowIDParent = m_TableRowID
            cAdditions.Item("k104").UseCode = m_MonthlyCalculation
            cAdditions.Item("k105").TableRowIDFriendlyName = 105
            cAdditions.Item("k105").TableRowIDParent = m_TableRowID
            cAdditions.Item("k105").UseCode = m_BonusCommissionsIncentive
            cAdditions.Item("k106").TableRowIDFriendlyName = 106
            cAdditions.Item("k106").TableRowIDParent = m_TableRowID
            cAdditions.Item("k106").UseCode = m_ConcurrentEmployment
            cAdditions.Item("k107").TableRowIDFriendlyName = 107
            cAdditions.Item("k107").TableRowIDParent = m_TableRowID
            cAdditions.Item("k107").UseCode = m_Domicile
            cAdditions.Item("k108").TableRowIDFriendlyName = 108
            cAdditions.Item("k108").TableRowIDParent = m_TableRowID
            cAdditions.Item("k108").UseCode = m_EmployerPaidBenefitsContinued
            cAdditions.Item("k109").TableRowIDFriendlyName = 109
            cAdditions.Item("k109").TableRowIDParent = m_TableRowID
            cAdditions.Item("k109").UseCode = m_EmployerPaidBenefitsDiscontinued
            cAdditions.Item("k110").TableRowIDFriendlyName = 110
            cAdditions.Item("k110").TableRowIDParent = m_TableRowID
            cAdditions.Item("k110").UseCode = m_GratuitiesTips
            cAdditions.Item("k111").TableRowIDFriendlyName = 111
            cAdditions.Item("k111").TableRowIDParent = m_TableRowID
            cAdditions.Item("k111").UseCode = m_Meals
            cAdditions.Item("k112").TableRowIDFriendlyName = 112
            cAdditions.Item("k112").TableRowIDParent = m_TableRowID
            cAdditions.Item("k112").UseCode = m_TaxStatusSpendableIncome
            cAdditions.Item("k113").TableRowIDFriendlyName = 113
            cAdditions.Item("k113").TableRowIDParent = m_TableRowID
            cAdditions.Item("k113").UseCode = m_AllOtherAdvanage
            'cAdditions.Item("k114").TableRowIDFriendlyName = 114
            'cAdditions.Item("k114").TableRowIDParent = m_TableRowID
            'cAdditions.Item("k114").UseCode = m_NumberOfWeeksInGrid
            cAdditions.Item("k115").TableRowIDFriendlyName = 115
            cAdditions.Item("k115").TableRowIDParent = m_TableRowID
            cAdditions.Item("k115").UseCode = m_DaysInGrid
            cAdditions.Item("k116").TableRowIDFriendlyName = 116
            cAdditions.Item("k116").TableRowIDParent = m_TableRowID
            cAdditions.Item("k116").UseCode = m_HoursInGrid
            cAdditions.Item("k104").SaveData()
            cAdditions.Item("k105").SaveData()
            cAdditions.Item("k106").SaveData()
            cAdditions.Item("k107").SaveData()
            cAdditions.Item("k108").SaveData()
            cAdditions.Item("k109").SaveData()
            cAdditions.Item("k110").SaveData()
            cAdditions.Item("k111").SaveData()
            cAdditions.Item("k112").SaveData()
            cAdditions.Item("k113").SaveData()
            'cAdditions.Item("k114").SaveData
            cAdditions.Item("k115").SaveData()
            cAdditions.Item("k116").SaveData()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function ValidateData() As Integer

        ValidateData = 0
        'jtodd22 02/13/2008 function proved useless of use of oUser as RMUser
        m_Warning = "Programmer Error:  Function CJRCalcAWW.ValidateData failed," & vbCrLf
        m_Warning = m_Warning & "use Function CJRCalcAWW.ValidateDataByUser."
        Exit Function

        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_JurisRowID < 1 Then
            m_Warning = m_Warning & "A Jurisdiction is required" & vbCrLf
        End If
        If m_MonthlyCalculation < 1 Then
            m_Warning = m_Warning & "Monthly Calculation must be 'Yes' or 'No'." & vbCrLf
        End If
        If m_TaxStatusSpendableIncome < 1 Then
            m_Warning = m_Warning & "'Tax Status -- Spendable Income' must be 'Yes' or 'No'." & vbCrLf
        End If
        If m_NoCodeID = 0 Then
            'jtodd22 02/13/2008--m_NoCodeID = modFunctions.GetNoCodeIDNoConn(oUser)
        End If
        If m_MeanPercentage > 0 And m_ExcludeZeros = m_NoCodeID Then
            m_Warning = m_Warning & "If Exclude Mean has a non-zero value, then Exclude Zeros must be set to 'Yes'." & vbCrLf
        End If



    End Function
    Public Function ValidateDataByUser(ByRef oUser As Riskmaster.Security.UserLogin) As Integer

        ValidateDataByUser = 0
        m_Warning = ""

        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_JurisRowID < 1 Then
            m_Warning = m_Warning & "A Jurisdiction is required" & vbCrLf
        End If
        If m_MonthlyCalculation < 1 Then
            m_Warning = m_Warning & "Monthly Calculation must be 'Yes' or 'No'." & vbCrLf
        End If
        If m_TaxStatusSpendableIncome < 1 Then
            m_Warning = m_Warning & "'Tax Status -- Spendable Income' must be 'Yes' or 'No'." & vbCrLf
        End If
        If m_NoCodeID = 0 Then
            m_NoCodeID = modFunctions.GetNoCodeIDNoConn(oUser)
        End If
        If m_MeanPercentage > 0 And m_ExcludeZeros = m_NoCodeID Then
            m_Warning = m_Warning & "If Exclude Mean has a non-zero value, then Exclude Zeros must be set to 'Yes'." & vbCrLf
        End If



    End Function

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property MonthlyCalculation() As Integer
        Get
            MonthlyCalculation = m_MonthlyCalculation
        End Get
        Set(ByVal Value As Integer)
            m_MonthlyCalculation = Value
        End Set
    End Property
    Public Property BonusCommissionsIncentive() As Integer
        Get
            BonusCommissionsIncentive = m_BonusCommissionsIncentive
        End Get
        Set(ByVal Value As Integer)
            m_BonusCommissionsIncentive = Value
        End Set
    End Property
    Public Property ConcurrentEmployment() As Integer
        Get
            ConcurrentEmployment = m_ConcurrentEmployment
        End Get
        Set(ByVal Value As Integer)
            m_ConcurrentEmployment = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property
    Public Property Domicile() As Integer
        Get
            Domicile = m_Domicile
        End Get
        Set(ByVal Value As Integer)
            m_Domicile = Value
        End Set
    End Property
    Public Property EmployerPaidBenefitsContinued() As Integer
        Get
            EmployerPaidBenefitsContinued = m_EmployerPaidBenefitsContinued
        End Get
        Set(ByVal Value As Integer)
            m_EmployerPaidBenefitsContinued = Value
        End Set
    End Property
    Public Property EmployerPaidBenefitsDiscontinued() As Integer
        Get
            EmployerPaidBenefitsDiscontinued = m_EmployerPaidBenefitsDiscontinued
        End Get
        Set(ByVal Value As Integer)
            m_EmployerPaidBenefitsDiscontinued = Value
        End Set
    End Property
    Public Property GratuitiesTips() As Integer
        Get
            GratuitiesTips = m_GratuitiesTips
        End Get
        Set(ByVal Value As Integer)
            m_GratuitiesTips = Value
        End Set
    End Property
    Public Property Meals() As Integer
        Get
            Meals = m_Meals
        End Get
        Set(ByVal Value As Integer)
            m_Meals = Value
        End Set
    End Property
    Public Property TaxStatusSpendableIncome() As Integer
        Get
            TaxStatusSpendableIncome = m_TaxStatusSpendableIncome
        End Get
        Set(ByVal Value As Integer)
            m_TaxStatusSpendableIncome = Value
        End Set
    End Property
    Public Property AllOtherAdvanage() As Integer
        Get
            AllOtherAdvanage = m_AllOtherAdvanage
        End Get
        Set(ByVal Value As Integer)
            m_AllOtherAdvanage = Value
        End Set
    End Property

    Public Property NoCodeID() As Integer
        Get
            NoCodeID = m_NoCodeID
        End Get
        Set(ByVal Value As Integer)
            m_NoCodeID = Value
        End Set
    End Property
    Public Property NumberOfWeeksInGrid() As Integer
        Get
            NumberOfWeeksInGrid = m_NumberOfWeeksInGrid
        End Get
        Set(ByVal Value As Integer)
            m_NumberOfWeeksInGrid = Value
        End Set
    End Property
    Public Property DaysInGrid() As Integer
        Get
            DaysInGrid = m_DaysInGrid
        End Get
        Set(ByVal Value As Integer)
            m_DaysInGrid = Value
        End Set
    End Property
    Public Property HoursInGrid() As Integer
        Get
            HoursInGrid = m_HoursInGrid
        End Get
        Set(ByVal Value As Integer)
            m_HoursInGrid = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property ExcludeZeros() As Integer
        Get
            ExcludeZeros = m_ExcludeZeros
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeZeros = Value
        End Set
    End Property

    Public Property MeanPercentage() As Double
        Get
            MeanPercentage = m_MeanPercentage
        End Get
        Set(ByVal Value As Double)
            m_MeanPercentage = Value
        End Set
    End Property

    Public Property YesCodeID() As Integer
        Get
            YesCodeID = m_YesCodeID
        End Get
        Set(ByVal Value As Integer)
            m_YesCodeID = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Public Property objUser() As Riskmaster.Security.UserLogin
        Get
            objUser = m_objUser
        End Get
        Set(ByVal Value As Riskmaster.Security.UserLogin)
            m_objUser = Value
        End Set
    End Property
    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " " & sTableName1 & ".TABLE_ROW_ID"
        sSQL = sSQL & ", " & sTableName1 & ".ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", EFFECTIVE_DATE,JURIS_ROW_ID"
        sSQL = sSQL & ", TTD_MULTIPLER,AMW_CALCULATI_CODE,AW_ALL_OTHER_CODE,AW_CON_EMPLOY_CODE"
        sSQL = sSQL & ", AW_CONBENEFIT_CODE,AW_DIS_CONBEN_CODE,AW_DOMICILE_CODE,AW_GRADITUTES_CODE"
        sSQL = sSQL & ", AW_INCENTIVE_CODE,AW_MEALS_CODE,AW_TAX_STATUS_CODE,AWW_WEEKS_IN_GRID"
        sSQL = sSQL & ", AW_DAYS_IN_CODE,AW_HOUR_IN_CODE"
        sSQL = sSQL & " FROM " & sTableName2
        GetBaseSQL = sSQL


    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : AssignData
    ' DateTime  : 7/1/2004 17:38
    ' Author    : jtodd22
    ' Purpose   :
    ' Notes     : AssignData is a class internal function called by public functions
    '...........: The incoming recordset (iRdSet) must be closed/dropped
    '...........: in this function.
    '---------------------------------------------------------------------------------------
    '
    Private Function AssignData(ByRef objReader As DbReader) As Integer
        Dim cAdditions As CJRCalcAWWAdditions
        Dim lTest As Integer
        Try
            AssignData = 0

            '**********************************************************************
            'jlt 07/01/2004 do not call ModMain.DBConnect as the calling function has done it.
            '**********************************************************************

            m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
            m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
            m_NumberOfWeeksInGrid = objReader.GetInt32("WEEKS_IN_GD_NUM")
            If Not IsNothing(objReader.Item("EXCLUDE_ZEROS")) Then
                m_ExcludeZeros = objReader.GetInt32("EXCLUDE_ZEROS")
                m_MeanPercentage = objReader.GetDouble("MEAN_PERCENTAGE")
            End If
            '************************************************************************
            '***The incoming recordset (iRdSet) must be dropped
            '*** in this function.  We will be opening a new Recordset.
            '************************************************************************
            SafeCloseRecordset(objReader)
            cAdditions = New CJRCalcAWWAdditions
            '************************************************************************
            '*** m_TableRowID is used as a condition on PT_TABLE_ROW_ID
            '************************************************************************
            lTest = cAdditions.DLLFetchData(m_TableRowID)
            If lTest <> -1 Then
                'UPGRADE_NOTE: Object cAdditions may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                cAdditions = Nothing
                Exit Function
            End If
            If cAdditions.Count = 0 Then
                'UPGRADE_NOTE: Object cAdditions may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                cAdditions = Nothing
                Exit Function
            End If

            '(105,1,-1,'k105','20040514','CSC-jlt','20040514','CSC-jlt','Bonus, Commissions, Incentive:')
            '(106,1,-1,'k106','20040514','CSC-jlt','20040514','CSC-jlt','Concurrent Employment:')
            '(107,1,-1,'k107','20040514','CSC-jlt','20040514','CSC-jlt','Domicile (Board, Rent, Housing, Lodging):')
            '(108,1,-1,'k108','20040514','CSC-jlt','20040514','CSC-jlt','Employer Paid Benefits--Continued:')
            '(109,1,-1,'k109','20040514','CSC-jlt','20040514','CSC-jlt','Employer Paid Benefits--Discontinued:')
            '(110,1,-1,'k110','20040514','CSC-jlt','20040514','CSC-jlt','Gratuities And Tips:')
            '(111,1,-1,'k111','20040514','CSC-jlt','20040514','CSC-jlt','Meals:')
            '(112,1,-1,'k112','20040514','CSC-jlt','20040514','CSC-jlt','Tax Status -- Spendable Income:')
            '(113,1,-1,'k113','20040514','CSC-jlt','20040514','CSC-jlt','All Other Advanage:')
            m_MonthlyCalculation = cAdditions.Item("k104").UseCode
            m_BonusCommissionsIncentive = cAdditions.Item("k105").UseCode
            m_ConcurrentEmployment = cAdditions.Item("k106").UseCode
            m_Domicile = cAdditions.Item("k107").UseCode
            m_EmployerPaidBenefitsContinued = cAdditions.Item("k108").UseCode
            m_EmployerPaidBenefitsDiscontinued = cAdditions.Item("k109").UseCode
            m_GratuitiesTips = cAdditions.Item("k110").UseCode
            m_Meals = cAdditions.Item("k111").UseCode
            m_TaxStatusSpendableIncome = cAdditions.Item("k112").UseCode
            m_AllOtherAdvanage = cAdditions.Item("k113").UseCode
            'jtodd22 no-no--m_NumberOfWeeksInGrid = cAdditions.Item("k114").UseCode
            m_DaysInGrid = cAdditions.Item("k115").UseCode
            m_HoursInGrid = cAdditions.Item("k116").UseCode

            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".AssignData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            '*******************************************************************
            'jlt 07/01/2004 do not do
            'jlt 07/01/2004 do not do       SafeDropRecordset iRdSet
            'jlt 07/01/2004 do not do       ModMain.DBDisconnect
            '*******************************************************************
        End Try

    End Function
    Private Function GetBaseSQL_WCP_AWW_SWCH() As String
        Dim sSQL As String
        GetBaseSQL_WCP_AWW_SWCH = ""
        sSQL = ""
        sSQL = sSQL & "SELECT *"
        'jtodd22 02/06/2006  sSQL = sSQL & " TABLE_ROW_ID"
        'jtodd22 02/06/2006  sSQL = sSQL & ", ADDED_BY_USER,DTTM_RCD_ADDED"
        'jtodd22 02/06/2006  sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        'jtodd22 02/06/2006  sSQL = sSQL & ", EFFECTIVE_DATE,JURIS_ROW_ID"
        'jtodd22 02/06/2006  sSQL = sSQL & ", WEEKS_IN_GD_NUM"
        'jtodd22 02/06/2006  sSQL = sSQL & ", EXCLUDE_ZEROS"
        'jtodd22 02/06/2006  sSQL = sSQL & ", MEAN_PERCENTAGE"
        GetBaseSQL_WCP_AWW_SWCH = sSQL


    End Function
    Private Function GetSQLTableRowID(ByVal lTableRowID As Integer) As String



    End Function
End Class

