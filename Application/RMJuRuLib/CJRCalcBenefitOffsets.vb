Option Strict Off
Option Explicit On
Public Class CJRCalcBenefitOffsets
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCalcBenefitOffsets"

    Private mCol As Collection
    Public Function Add(ByRef FriendlyName As String, ByRef TableRowID As Integer, ByRef TableRowIDFriendlyName As Integer, ByRef TableRowIDParent As Integer, ByRef UseCode As Integer, Optional ByRef sKey As String = "") As CJRCalcBenefitOffset
        Try
            'create a new object
            Dim objNewMember As CJRCalcBenefitOffset
            objNewMember = New CJRCalcBenefitOffset


            'set the properties passed into the method
            objNewMember.TableRowID = TableRowID
            objNewMember.UseCode = UseCode
            objNewMember.TableRowIDParent = TableRowIDParent
            objNewMember.TableRowIDFriendlyName = TableRowIDFriendlyName
            objNewMember.FriendlyName = FriendlyName
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)



        Finally
        End Try

    End Function

    Public Function DLLFetchParentTableRowID(ByRef lParentTableRowID As Integer) As Integer
        Dim sSQL As String
        Try

            DLLFetchParentTableRowID = 0
            ClearObject()

            sSQL = GetSQLParentTableRowID(lParentTableRowID)
            DLLFetchParentTableRowID = FetchDataCollection(sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DLLFetchParentTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".DLLFetchParentTableRowID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public Function LoadDataByEventDate(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0
            ClearObject()


            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM WCP_BEN_SWCH"
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = ""
            sSQL = sSQL & GetSQLJurisRowID(lJurisRowID)
            sSQL = sSQL & "AND EFFECTIVE_DATE = (" & sSQL2 & ")"
            LoadDataByEventDate = FetchDataCollection(sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function LoadDataByJurisRowID(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByJurisRowID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDataByJurisRowID = 0
            ClearObject()


            sSQL = GetSQLJurisRowID(lJurisRowID)
            LoadDataByJurisRowID = FetchDataCollection(sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByJurisRowID = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function DLLFetchDataByTableRowIDParent(ByRef lParentTableRowID As Integer) As Integer
        Const sFunctionName As String = "DLLFetchDataByTableRowIDParent"
        Dim sSQL As String
        Try

            DLLFetchDataByTableRowIDParent = 0
            ClearObject()


            sSQL = GetSQLParentTableRowID(lParentTableRowID)
            DLLFetchDataByTableRowIDParent = FetchDataCollection(sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DLLFetchDataByTableRowIDParent = Err.Number
            'jtodd22 08/17/2004  this is a no-no here*****SafeDropRecordset iRdSet
            'jtodd22 08/17/2004  this is a no-no here*****ModMain.DBDisconnect
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'jtodd22 08/17/2004  this is a no-no here*****ModMain.DBDisconnect
        End Try

    End Function
    Public Function SaveData() As Integer
        Dim i As Short
        For i = 1 To mCol.Count()
            'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().SaveData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            mCol.Item(i).SaveData()
        Next i


    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCalcBenefitOffset
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function ClearObject() As Integer



    End Function

    Private Function GetFieldSQL() As String
        Dim sSQL As String
        GetFieldSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " FN_TABLE_ROW_ID"
        sSQL = sSQL & ", PT_TABLE_ROW_ID"
        sSQL = sSQL & ", WCP_FRIENDLY_NAME.TABLE_ROW_ID"
        sSQL = sSQL & ", USE_CODE"
        sSQL = sSQL & ", FRIENDLY_NAME"
        GetFieldSQL = sSQL


    End Function
    Private Function GetSQLJurisRowID(ByRef lJurisRowID As Integer) As String
        Dim lUseCode As Integer
        Dim sSQL As String
        GetSQLJurisRowID = ""

        lUseCode = GetYesCodeID()

        sSQL = ""
        sSQL = sSQL & GetFieldSQL()
        sSQL = sSQL & " FROM WCP_BEN_SWCH, WCP_BEN_X_SWCH, WCP_FRIENDLY_NAME"
        sSQL = sSQL & " WHERE WCP_BEN_X_SWCH.PT_TABLE_ROW_ID = WCP_BEN_SWCH.TABLE_ROW_ID"
        sSQL = sSQL & " AND WCP_BEN_X_SWCH.FN_TABLE_ROW_ID = WCP_FRIENDLY_NAME.TABLE_ROW_ID"
        sSQL = sSQL & " AND WCP_BEN_SWCH.JURIS_ROW_ID = " & lJurisRowID
        sSQL = sSQL & " AND USE_CODE = " & lUseCode

        GetSQLJurisRowID = sSQL


    End Function
    Private Function GetSQLParentTableRowID(ByRef lParentTableRowID As Integer) As String
        Dim sSQL As String

        GetSQLParentTableRowID = ""
        sSQL = ""
        sSQL = sSQL & GetFieldSQL()
        sSQL = sSQL & " FROM WCP_BEN_X_SWCH,WCP_FRIENDLY_NAME"
        sSQL = sSQL & " WHERE WCP_BEN_X_SWCH.PT_TABLE_ROW_ID = " & lParentTableRowID
        sSQL = sSQL & " AND WCP_BEN_X_SWCH.FN_TABLE_ROW_ID = WCP_FRIENDLY_NAME.TABLE_ROW_ID"

        GetSQLParentTableRowID = sSQL


    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : FetchDataCollection
    ' DateTime  : 7/16/2004 09:36
    ' Author    : jtodd22
    ' Purpose   : To load the data collection independent of original function call
    ' Notes     : The database connection must be made in the original function call
    '---------------------------------------------------------------------------------------
    '
    Private Function FetchDataCollection(ByRef sSQL As String) As Integer
        Dim objReader As DbReader
        Dim lTest As Integer
        Try

            FetchDataCollection = 0

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                lTest = objReader.GetInt32("TABLE_ROW_ID")
                Add(objReader.GetString("FRIENDLY_NAME"), lTest, objReader.GetInt32("FN_TABLE_ROW_ID"), objReader.GetInt32("PT_TABLE_ROW_ID"), objReader.GetInt32("USE_CODE"), "k" & lTest)

            End While

            FetchDataCollection = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            FetchDataCollection = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".FetchDataCollection|"
            g_lErrLine = Erl()
            SafeCloseRecordset(objReader)
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
End Class

