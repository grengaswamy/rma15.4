Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDWA
    Const sClassName As String = "CJRBenefitRuleTTDWA"
    Const sTableName As String = "WCP_RULE_TTD_WA"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821

    Public objJurisWorkWeek As New CJRXJurisWorkWeek

    Dim m_sDateOfEventDTG As String 'jtodd22 not a Class Property

    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_ErrorNumber As Integer 'not Jurisdictional Rule
    Private m_TaxStatusCode As Integer
    Private m_Exemptions As Integer
    Private m_BenefitPercentage As Double
    Private m_MinCompRateMonth As Double
    Private m_MinCompRateWeek As Double
    Private m_MaxAWW As Double
    Private m_MaxSAWWPercentage As Double

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim lReturn As Integer

        Try

            AssignData = 0
            ClearObject()

            m_YesCodeID = GetYesCodeID()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_BenefitPercentage = objReader.GetDouble("PERCENT_BENEFIT")
                m_Exemptions = objReader.GetInt32("EXEMP_NUMB")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_TaxStatusCode = objReader.GetInt32("MARTIAL_STTUS_CODE")
                m_MaxSAWWPercentage = objReader.GetDouble("MAX_SAWW_PERCENT")
                m_MinCompRateMonth = objReader.GetDouble("MIN_BENEFIT_AMT")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            End If

            SafeCloseRecordset(objReader)

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            lReturn = Me.objJurisWorkWeek.LoadData(m_JurisRowID, m_EffectiveDateDTG)
            If lReturn = -1 Then
                m_JurisDefinedWorkWeek = Me.objJurisWorkWeek.JurisDefinedWorkWeek
                AssignData = -1
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_ErrorNumber = 0
        m_TaxStatusCode = 0
        m_Exemptions = 0
        m_BenefitPercentage = 0
        m_MinCompRateMonth = 0
        m_MaxSAWWPercentage = 0


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,DELETED_FLAG,EFFECTIVE_DATE"
        sSQL = sSQL & ",MARTIAL_STTUS_CODE"
        sSQL = sSQL & ",EXEMP_NUMB"
        sSQL = sSQL & ",PERCENT_BENEFIT"
        sSQL = sSQL & ",MIN_BENEFIT_AMT"
        sSQL = sSQL & ",MAX_SAWW_PERCENT"
        GetSQLFieldList = sSQL


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : DeleteRecord
    ' DateTime  : 4/18/2006 07:43
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : lReturnCode is a number defining why the record was not deleted
    '---------------------------------------------------------------------------------------
    '
    Public Function DeleteRecord(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDBFormat As String, ByRef lReturnCode As Integer) As Integer
        Const sFunctionName As String = "DeleteRecord"

        Try

            DeleteRecord = modFunctions.DeleteRecordTemporary(objUser, lJurisRowID, sEffectiveDateDBFormat, lReturnCode, sTableName)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteRecord = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByClaimData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef lTaxStatusCode As Integer, ByRef lExemptions As Integer) As Integer
        Const sFunctionName As String = "LoadDataByClaimData"
        Dim lReturn As Integer
        Dim sSQL As String
        Dim sTmp As String
        Try
            LoadDataByClaimData = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_sDateOfEventDTG = sDateOfEventDTG

            sTmp = ""
            sTmp = sTmp & "(SELECT MAX(EFFECTIVE_DATE)"
            sTmp = sTmp & " FROM " & sTableName
            sTmp = sTmp & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sTmp = sTmp & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sTmp = sTmp & " AND MARTIAL_STTUS_CODE = " & lTaxStatusCode
            sTmp = sTmp & " AND EXEMP_NUMB = " & lExemptions & ")"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND MARTIAL_STTUS_CODE = " & lTaxStatusCode
            sSQL = sSQL & " AND EXEMP_NUMB = " & lExemptions
            sSQL = sSQL & " AND EFFECTIVE_DATE = " & sTmp

            lReturn = AssignData(objUser, sSQL)
            If lReturn <> -1 Then Exit Function

            LoadDataByClaimData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByClaimData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByEventDate
    ' DateTime  : 7/5/2006 11:32
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      :
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef lTaxStatusCode As Integer, ByRef lExemptions As Integer) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim lReturn As Integer
        Dim sSQL As String
        Dim sTmp As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_sDateOfEventDTG = sDateOfEventDTG

            sTmp = ""
            sTmp = sTmp & "(SELECT MAX(EFFECTIVE_DATE)"
            sTmp = sTmp & " FROM " & sTableName
            sTmp = sTmp & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sTmp = sTmp & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sTmp = sTmp & " AND MARTIAL_STTUS_CODE = " & lTaxStatusCode
            sTmp = sTmp & " AND EXEMP_NUMB = " & lExemptions & ")"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND MARTIAL_STTUS_CODE = " & lTaxStatusCode
            sSQL = sSQL & " AND EXEMP_NUMB = " & lExemptions
            sSQL = sSQL & " AND EFFECTIVE_DATE = " & sTmp

            lReturn = AssignData(objUser, sSQL)
            If lReturn <> -1 Then Exit Function

            If m_MaxSAWWPercentage > 0 Then 'jtodd 07/05/2006-- RMWCCalu related fix
                lReturn = GetMaxCompRateWeekly(objUser)
                If lReturn = 0 Then
                    Exit Function
                End If
            End If

            LoadDataByEventDate = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("MARTIAL_STTUS_CODE").Value = m_TaxStatusCode
                objWriter.Fields("EXEMP_NUMB").Value = m_Exemptions
                objWriter.Fields("PERCENT_BENEFIT").Value = m_BenefitPercentage
                objWriter.Fields("MIN_BENEFIT_AMT").Value = m_MinCompRateMonth
                objWriter.Fields("MAX_SAWW_PERCENT").Value = m_MaxSAWWPercentage
                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("MARTIAL_STTUS_CODE", m_TaxStatusCode)
                objWriter.Fields.Add("EXEMP_NUMB", m_Exemptions)
                objWriter.Fields.Add("PERCENT_BENEFIT", m_BenefitPercentage)
                objWriter.Fields.Add("MIN_BENEFIT_AMT", m_MinCompRateMonth)
                objWriter.Fields.Add("MAX_SAWW_PERCENT", m_MaxSAWWPercentage)
            End If
            objWriter.Execute()

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_TaxStatusCode < 1 Then
            m_Warning = m_Warning & "A Tax (Martial) Status is required." & vbCrLf
        End If
        If m_Exemptions < 1 Then
            m_Warning = m_Warning & "An Expemption is required." & vbCrLf
        End If
        If Not IsNumeric(m_BenefitPercentage) Then
            m_Warning = m_Warning & "A Percentage of AWW is required." & vbCrLf
        Else
            If m_BenefitPercentage = 0 Then
                m_Warning = m_Warning & "A Percentage of AWW is required." & vbCrLf
            End If
            If m_BenefitPercentage < 1 Then
                m_Warning = m_Warning & "Percentage of AWW can not be less than 1 (one)." & vbCrLf
            End If
        End If
        If Not IsNumeric(MinCompRateMonth) Then
            m_Warning = m_Warning & "A Minimum Amount is required." & vbCrLf
        Else
            If MinCompRateMonth = 0 Then
                m_Warning = m_Warning & "A Minimum Amount is required." & vbCrLf
            End If
        End If
        If Not IsNumeric(m_MaxSAWWPercentage) Then
            m_Warning = m_Warning & "A Maximum of SAWW percentage is required." & vbCrLf
        Else
            If MaxSAWWPercentage = 0 Then
                m_Warning = m_Warning & "A Maximum of SAWW percentage is required." & vbCrLf
            End If
        End If


    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisDefinedWorkWeek() As Integer
        Get
            JurisDefinedWorkWeek = m_JurisDefinedWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisDefinedWorkWeek = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property TaxStatusCode() As Integer
        Get
            TaxStatusCode = m_TaxStatusCode
        End Get
        Set(ByVal Value As Integer)
            m_TaxStatusCode = Value
        End Set
    End Property
    Public Property Exemptions() As Integer
        Get
            Exemptions = m_Exemptions
        End Get
        Set(ByVal Value As Integer)
            m_Exemptions = Value
        End Set
    End Property
    Public Property BenefitPercentage() As Double
        Get
            BenefitPercentage = m_BenefitPercentage
        End Get
        Set(ByVal Value As Double)
            m_BenefitPercentage = Value
        End Set
    End Property

    Public Property MinCompRateMonth() As Double
        Get
            MinCompRateMonth = m_MinCompRateMonth
        End Get
        Set(ByVal Value As Double)
            m_MinCompRateMonth = Value
        End Set
    End Property
    Public Property MaxSAWWPercentage() As Double
        Get
            MaxSAWWPercentage = m_MaxSAWWPercentage
        End Get
        Set(ByVal Value As Double)
            m_MaxSAWWPercentage = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property ErrorNumber() As Integer
        Get
            ErrorNumber = m_ErrorNumber
        End Get
        Set(ByVal Value As Integer)
            ErrorNumber = Value
        End Set
    End Property

    Public ReadOnly Property MaxAWW() As Double
        Get
            MaxAWW = 0
        End Get
    End Property

    Public ReadOnly Property MinCompRateWeek() As Double
        Get
            MinCompRateWeek = m_MinCompRateMonth / m_WeekToMonthConvFactor
        End Get
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            'programmer conveiance(sp??) only
            RuleTotalWeeks = 0
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    Private Function GetJurisDefinedWorkWeek(ByRef lJurisRowID As Integer, ByRef sBeginDateDTG As String) As Integer
        Const sFunctionName As String = "GetJurisDefinedWorkWeek"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            GetJurisDefinedWorkWeek = 0
            m_JurisDefinedWorkWeek = 0

            sSQL = ""
            sSQL = sSQL & " SELECT JURIS_WORK_WEEK, EFFECTIVE_DATE"
            sSQL = sSQL & " FROM WCP_BEN_SWCH"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sBeginDateDTG & "'"
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_JurisDefinedWorkWeek = objReader.GetInt32("JURIS_WORK_WEEK")
            End If
            If m_JurisDefinedWorkWeek = 0 Then m_JurisDefinedWorkWeek = 7
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetJurisDefinedWorkWeek = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Private Function GetMaxCompRateWeekly(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "GetMaxCompRate"
        Dim lReturn As Integer
        Dim objSAWW As CJRSAWW
        Try

            GetMaxCompRateWeekly = 0

            objSAWW = New CJRSAWW
            lReturn = objSAWW.LoadDataByEventDate(objUser, m_JurisRowID, m_EffectiveDateDTG)
            Select Case lReturn
                Case -1
                    If objSAWW.TableRowID > 0 Then
                        m_MaxCompRateWeekly = System.Math.Round((m_MaxSAWWPercentage / 100) * objSAWW.SAWWAmount)
                    Else
                        'data not found --warn user
                        m_ErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcNoJurisdictionalRule
                        Exit Function
                    End If
                Case Else
                    'error
            End Select
            'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objSAWW = Nothing

            GetMaxCompRateWeekly = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If Not g_sErrDescription > "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            GetMaxCompRateWeekly = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        If Not Me.objJurisWorkWeek Is Nothing Then
            'UPGRADE_NOTE: Object Me.objJurisWorkWeek may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            Me.objJurisWorkWeek = Nothing
        End If



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

