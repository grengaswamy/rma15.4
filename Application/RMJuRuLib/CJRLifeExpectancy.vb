Option Strict Off
Option Explicit On
Public Class CJRLifeExpectancy
    Private Const sClassName As String = "CJRLifeExpectancy"
    Private Const sTableName As String = "WCP_CDC_LIFE_EXPCT"

    Private m_TableRowID As Integer
    Private m_JurisRowID As Short
    Private m_DeletedFlag As Short
    Private m_CDCDecennial As String
    Private m_SexCodeID As Integer
    Private m_Age As Short
    Private m_YearsRemaining As Double
    Private m_EffectiveDateDTG As String
    Private m_EndDateDTG As String
    Private m_DataHasChanged As Boolean

    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED"
        sSQL = sSQL & ", ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ", UPDATED_BY_USER"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", CDC_DECENNIAL"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", SEX_CODE_ID"
        sSQL = sSQL & ", AGE"
        sSQL = sSQL & ", YRS_REMAINING"
        sSQL = sSQL & " FROM " & sTableName
        GetBaseSQL = sSQL


    End Function
    Public Function LoadDataByTableRowID(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0


            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_SexCodeID = CInt(objReader.GetString("SEX_CODE_ID"))
                m_CDCDecennial = objReader.GetString("CDC_DECENNIAL")
                m_YearsRemaining = objReader.GetDouble("YRS_REMAINING")
                m_Age = objReader.GetInt32("AGE")
            End If

            LoadDataByTableRowID = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDataClaimant(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lAge As Integer, ByRef lSexCodeID As Integer) As Integer
        Const sFunctionName As String = "LoadDataClaimant"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDataClaimant = 0
            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND AGE = " & lAge
            sSQL = sSQL & " AND SEX_CODE_ID = " & lSexCodeID
            sSQL = sSQL & " ORDER BY CDC_DECENNIAL ASC"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_SexCodeID = CInt(objReader.GetString("SEX_CODE_ID"))
                m_CDCDecennial = objReader.GetString("CDC_DECENNIAL")
                m_YearsRemaining = objReader.GetDouble("YRS_REMAINING")
                m_Age = objReader.GetInt32("AGE")
            End If
            LoadDataClaimant = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataClaimant = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function SaveData(ByRef objuser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Dim sNow As String
        Dim lTest As Integer
        Try

            SaveData = 0
            sNow = System.DateTime.Now().ToString("yyyyMMddHHmmss")



            sSQL = GetBaseSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = sNow
                objWriter.Fields("UPDATED_BY_USER").Value = objuser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("CDC_DECENNIAL").Value = m_CDCDecennial
                objWriter.Fields("SEX_CODE_ID").Value = m_SexCodeID
                objWriter.Fields("AGE").Value = m_Age
                objWriter.Fields("YRS_REMAINING").Value = m_YearsRemaining
                objWriter.Execute()
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objuser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", sNow)
                objWriter.Fields.Add("UPDATED_BY_USER", objuser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("CDC_DECENNIAL", m_CDCDecennial)
                objWriter.Fields.Add("SEX_CODE_ID", m_SexCodeID)
                objWriter.Fields.Add("AGE", m_Age)
                objWriter.Fields.Add("YRS_REMAINING", m_YearsRemaining)
                objWriter.Execute()
            End If
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Property Age() As Short
        Get
            Age = m_Age
        End Get
        Set(ByVal Value As Short)
            m_Age = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property YearsRemaining() As Double
        Get
            YearsRemaining = m_YearsRemaining
        End Get
        Set(ByVal Value As Double)
            m_YearsRemaining = Value
        End Set
    End Property

    Public Property CDCDecennial() As String
        Get
            CDCDecennial = m_CDCDecennial
        End Get
        Set(ByVal Value As String)
            m_CDCDecennial = Value
        End Set
    End Property

    Public Property SexCodeID() As Integer
        Get
            SexCodeID = m_SexCodeID
        End Get
        Set(ByVal Value As Integer)
            m_SexCodeID = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property EndDateDTG() As String
        Get
            EndDateDTG = m_EndDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndDateDTG = Value
        End Set
    End Property
End Class

