Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleImpairIncomes
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleImpairIncomes"
    Const sTableName As String = "WCP_RULE_IIB"

    'local variable to hold collection
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",MAX_AMTPERWEEK"
        sSQL = sSQL & ",MAX_PERCENTOFSAWW"
        sSQL = sSQL & ",MAX_TALBENEFITAMT"
        sSQL = sSQL & ",MAX_WEEKS"
        sSQL = sSQL & ",MIN_AMOUNTPERWEEK"
        sSQL = sSQL & ",MIN_PERCENTOFSAWW"
        sSQL = sSQL & ",MMI_DATEREQUD_CODE"
        sSQL = sSQL & ",PAY_AFTERTEMP_CODE"
        sSQL = sSQL & ",PAY_LEN_DISAB_CODE"
        sSQL = sSQL & ",PAY_NATALLIFE_CODE"
        sSQL = sSQL & ",PAY_MAXWEEKS"
        sSQL = sSQL & ",PAY_PERIOD_CODE"
        sSQL = sSQL & ",PERCENT_OF_AWW"
        sSQL = sSQL & ",USE_TTDRULES_CODE"
        sSQL = sSQL & ",WEEKS_PER_POINT"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRecord As CJRBenefitRuleImpairIncome
        Dim sSQL As String
        Dim TableRowID As Integer
        Try

            LoadData = 0



            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	While objReader.Read()
            '		objRecord = New CJRBenefitRuleImpairIncome
            '		With objRecord
            '			.DataHasChanged = False
            '			.EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '			.JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '			.MaxAmountPerWeek = objReader.GetDouble( "MAX_AMTPERWEEK")
            '			.MaxPercentOfSAWW = objReader.GetDouble( "MAX_PERCENTOFSAWW")
            '			.MaxTotalBenefitAmount = objReader.GetDouble( "MAX_TALBENEFITAMT")
            '			.MaxWeeks = objReader.GetInt32( "MAX_WEEKS")
            '			.MinAmountPerWeek = objReader.GetDouble( "MIN_AMOUNTPERWEEK")
            '			.MinPercentOfSAWW = objReader.GetDouble( "MIN_PERCENTOFSAWW")
            '			.MMIDateRequired = objReader.GetInt32( "MMI_DATEREQUD_CODE")
            '			.PayAfterAllTemporary = objReader.GetInt32( "PAY_AFTERTEMP_CODE")
            '			.PayLengthOfDisability = objReader.GetInt32( "PAY_NATALLIFE_CODE")
            '			.PayNaturalLife = objReader.GetInt32( "PAY_NATALLIFE_CODE")
            '			.PayMaxWeeks = objReader.GetInt32( "PAY_MAXWEEKS")
            '			.PayPeriodCode = objReader.GetInt32( "PAY_PERIOD_CODE")
            '			.PercentageOfAWW = objReader.GetDouble( "PERCENT_OF_AWW")
            '			.TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '			.UseCurrentTTDRules = objReader.GetInt32( "USE_TTDRULES_CODE")
            '			.WeeksPerPercentagePoint = objReader.GetDouble( "WEEKS_PER_POINT")
            '			Add(.DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .JurisRowID, .MaxAmountPerWeek, .MaxPercentOfSAWW, .MaxTotalBenefitAmount, .MaxWeeks, .MinAmountPerWeek, .MinPercentOfSAWW, .MMIDateRequired, .PayAfterAllTemporary, .PayLengthOfDisability, .PayNaturalLife, .PayMaxWeeks, .PayPeriodCode, .PercentageOfAWW, .TableRowID, .UseCurrentTTDRules, .WeeksPerPercentagePoint, "k" & CStr(.TableRowID))
            '		End With
            '		'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '		objRecord = Nothing
            '		
            '          End While


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRuleImpairIncome
                With objRecord
                    .DataHasChanged = False
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxAmountPerWeek = objReader.GetDouble("MAX_AMTPERWEEK")
                    .MaxPercentOfSAWW = objReader.GetDouble("MAX_PERCENTOFSAWW")
                    .MaxTotalBenefitAmount = objReader.GetDouble("MAX_TALBENEFITAMT")
                    .MaxWeeks = objReader.GetInt32("MAX_WEEKS")
                    .MinAmountPerWeek = objReader.GetDouble("MIN_AMOUNTPERWEEK")
                    .MinPercentOfSAWW = objReader.GetDouble("MIN_PERCENTOFSAWW")
                    .MMIDateRequired = objReader.GetInt32("MMI_DATEREQUD_CODE")
                    .PayAfterAllTemporary = objReader.GetInt32("PAY_AFTERTEMP_CODE")
                    .PayLengthOfDisability = objReader.GetInt32("PAY_NATALLIFE_CODE")
                    .PayNaturalLife = objReader.GetInt32("PAY_NATALLIFE_CODE")
                    .PayMaxWeeks = objReader.GetInt32("PAY_MAXWEEKS")
                    .PayPeriodCode = objReader.GetInt32("PAY_PERIOD_CODE")
                    .PercentageOfAWW = objReader.GetDouble("PERCENT_OF_AWW")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .UseCurrentTTDRules = objReader.GetInt32("USE_TTDRULES_CODE")
                    .WeeksPerPercentagePoint = objReader.GetDouble("WEEKS_PER_POINT")
                    Add(.DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .JurisRowID, .MaxAmountPerWeek, .MaxPercentOfSAWW, .MaxTotalBenefitAmount, .MaxWeeks, .MinAmountPerWeek, .MinPercentOfSAWW, .MMIDateRequired, .PayAfterAllTemporary, .PayLengthOfDisability, .PayNaturalLife, .PayMaxWeeks, .PayPeriodCode, .PercentageOfAWW, .TableRowID, .UseCurrentTTDRules, .WeeksPerPercentagePoint, "k" & CStr(.TableRowID))
                End With
            End While




            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objRecord = Nothing
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function Add(ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MaxAmountPerWeek As Double, ByRef MaxPercentOfSAWW As Double, ByRef MaxTotalBenefitAmount As Double, ByRef MaxWeeks As Integer, ByRef MinAmountPerWeek As Double, ByRef MinPercentOfSAWW As Double, ByRef MMIDateRequired As Integer, ByRef PayAfterAllTemporary As Integer, ByRef PayLengthOfDisability As Integer, ByRef PayNaturalLife As Integer, ByRef PayMaxWeeks As Integer, ByRef PayPeriodCode As Integer, ByRef PercentageOfAWW As Double, ByRef TableRowID As Integer, ByRef UseCurrentTTDRules As Integer, ByRef WeeksPerPercentagePoint As Double, ByRef sKey As String) As CJRBenefitRuleImpairIncome
        'create a new object
        Dim objNewMember As CJRBenefitRuleImpairIncome
        objNewMember = New CJRBenefitRuleImpairIncome
        objNewMember.DataHasChanged = DataHasChanged
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.JurisRowID = JurisRowID
        objNewMember.MaxAmountPerWeek = MaxAmountPerWeek
        objNewMember.MaxPercentOfSAWW = MaxPercentOfSAWW
        objNewMember.MaxTotalBenefitAmount = MaxTotalBenefitAmount
        objNewMember.MaxWeeks = MaxWeeks
        objNewMember.MinAmountPerWeek = MinAmountPerWeek
        objNewMember.MinPercentOfSAWW = MinPercentOfSAWW
        objNewMember.MMIDateRequired = MMIDateRequired
        objNewMember.PayAfterAllTemporary = PayAfterAllTemporary
        objNewMember.PayLengthOfDisability = PayLengthOfDisability
        objNewMember.PayNaturalLife = PayNaturalLife
        objNewMember.PayMaxWeeks = PayMaxWeeks
        objNewMember.PayPeriodCode = PayPeriodCode
        objNewMember.PercentageOfAWW = PercentageOfAWW
        objNewMember.TableRowID = TableRowID
        objNewMember.UseCurrentTTDRules = UseCurrentTTDRules
        objNewMember.WeeksPerPercentagePoint = WeeksPerPercentagePoint
        mCol.Add(objNewMember, sKey)

        'set the properties passed into the method
        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing



    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleImpairIncome
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

