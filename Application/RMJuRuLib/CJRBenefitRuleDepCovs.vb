Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleDepCovs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleDepCovs"
    Const sTableName As String = "WCP_RULE_DEPCV"

    'local variable to hold collection
    Private mCol As Collection
    Private Function Add(ByRef AmountPerDependent As Double, ByRef CappedByAWWCode As Integer, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MaxCombinedDollarAmount As Double, ByRef PayPeriodCode As Integer, ByRef TableRowID As Integer, ByRef sKey As String) As CJRBenefitRuleDepCov
        Const sFunctionName As String = "Add"
        'create a new object
        Dim objNewMember As CJRBenefitRuleDepCov
        Try
            objNewMember = New CJRBenefitRuleDepCov


            'set the properties passed into the method
            objNewMember.AmountPerDependent = AmountPerDependent
            objNewMember.CappedByAWWCode = CappedByAWWCode
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.JurisRowID = JurisRowID
            objNewMember.MaxCombinedDollarAmount = MaxCombinedDollarAmount
            objNewMember.PayPeriodCode = PayPeriodCode
            objNewMember.TableRowID = TableRowID
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            'g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER, DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE, DELETED_FLAG"
        sSQL = sSQL & ",AMOUNTPERDEPENDENT"
        sSQL = sSQL & ",CAPPEDBYAWW_CODE"
        sSQL = sSQL & ",MAXCOMBINEDDOLLARS"
        sSQL = sSQL & ",PAY_PERIOD_CODE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRecord As CJRBenefitRuleDepCov
        Dim sSQL As String
        Try

            LoadData = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRuleDepCov
                With objRecord
                    .AmountPerDependent = objReader.GetInt32("AMOUNTPERDEPENDENT")
                    .CappedByAWWCode = objReader.GetInt32("CAPPEDBYAWW_CODE")
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxCombinedDollarAmount = objReader.GetInt32("MAXCOMBINEDDOLLARS")
                    .PayPeriodCode = objReader.GetInt32("PAY_PERIOD_CODE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.AmountPerDependent, .CappedByAWWCode, .DeletedFlag, .EffectiveDateDTG, .JurisRowID, .MaxCombinedDollarAmount, .PayPeriodCode, .TableRowID, "k" & CStr(.TableRowID))

                End With
            End While


            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)
            LoadData = Err.Number

            LoadData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleDepCov
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

