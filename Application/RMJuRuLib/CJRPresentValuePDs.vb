Option Strict Off
Option Explicit On
Public Class CJRPresentValuePDs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRPresentValuePDs"

    'local variable to hold collection
    Private mCol As Collection
    Private Function GetFieldSQL() As String
        Dim sSQL As String

        GetFieldSQL = ""

        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", TIME_PERIOD, PRESENT_VALUE"
        sSQL = sSQL & ", ENDING_DATE,ANNUAL_INTEREST,FUTURE_VALUE"
        sSQL = sSQL & " FROM WCP_PRSNT_VALUE_PD"

        GetFieldSQL = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRPresentValuePD
        Try

            LoadData = 0


            sSQL = GetFieldSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " ORDER BY EFFECTIVE_DATE, TIME_PERIOD"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRPresentValuePD
                With objRecord
                    .AnnualInterest = objReader.GetDouble("ANNUAL_INTEREST")
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                    .EndDateDTG = Trim(objReader.GetString("ENDING_DATE"))
                    .FutureValue = objReader.GetDouble("FUTURE_VALUE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .PresentValue = objReader.GetDouble("PRESENT_VALUE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .TimePeriod = objReader.GetInt32("TIME_PERIOD")
                    Add(.AnnualInterest, .DeletedFlag, .EffectiveDateDTG, .EndDateDTG, .FutureValue, .JurisRowID, .PresentValue, .TableRowID, .TimePeriod, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef AnnualInterest As Double, ByRef DeletedFlag As Short, ByRef EffectiveDateDTG As String, ByRef EndDateDTG As String, ByRef FutureValue As Double, ByRef JurisRowID As Short, ByRef PresentValue As Double, ByRef TableRowID As Integer, ByRef TimePeriod As Integer, ByRef sKey As String) As CJRPresentValuePD
        Const sFunctionName As String = "Add"
        'create a new object
        Dim objNewMember As CJRPresentValuePD

        Try

            objNewMember = New CJRPresentValuePD

            'set the properties passed into the method
            objNewMember.EndDateDTG = EndDateDTG
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.JurisRowID = JurisRowID
            objNewMember.TableRowID = TableRowID
            objNewMember.FutureValue = FutureValue
            objNewMember.AnnualInterest = AnnualInterest
            objNewMember.PresentValue = PresentValue
            objNewMember.TimePeriod = TimePeriod
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRPresentValuePD
        Get
            'used when referencing an element in the collection
            'vntIndexKey contains either the Index or Key to the collection,
            'this is why it is declared as a Variant
            'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        'creates the collection when this class is created
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'destroys collection when this class is terminated
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

