Option Strict Off
Option Explicit On
Public Class CJRCommutations
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCommutations"
    Private Const sTableName As String = "WCP_COMMUTAT_RE"
    'Class properties--local copy
    Private mCol As Collection
    Private Function Add(ByRef CommutTypeCode As Integer, ByRef DeletedFlag As Short, ByRef DiscountJurisDefined As Double, ByRef EffectiveDateDTG As String, ByRef EndDateDTG As String, ByRef JurisApprovalReqCode As Integer, ByRef JurisBenefitID As Integer, ByRef JurisRowID As Short, ByRef MaxDiscount As Double, ByRef MinDiscount As Double, ByRef PermittedCode As Integer, ByRef Reference As String, ByRef TableRowID As Integer, ByRef UseJurisProcedure As Integer, ByRef sKey As String) As CJRCommutation
        Try
            Dim objNewMember As CJRCommutation
            objNewMember = New CJRCommutation

            'set the properties passed into the method
            With objNewMember
                .CommutTypeCode = CommutTypeCode
                .DeletedFlag = DeletedFlag
                .DiscountJurisDefined = DiscountJurisDefined
                .EffectiveDateDTG = EffectiveDateDTG
                .EndDateDTG = EndDateDTG
                .JurisApprovalReqCode = JurisApprovalReqCode
                .JurisBenefitID = JurisBenefitID
                .JurisRowID = JurisRowID
                .MaxDiscount = MaxDiscount
                .MinDiscount = MinDiscount
                .PermittedCode = PermittedCode
                .Reference = Reference
                .TableRowID = TableRowID
                .UseJurisProcedure = UseJurisProcedure
            End With
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID, DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        'jtodd22 03/03/2005--sSQL = sSQL & ", EFF_END_DATE"
        sSQL = sSQL & ", MAX_DISCOUNT, MIN_DISCOUNT, DIS_JURIS_VALUE"
        sSQL = sSQL & ", JURIS_PROCED_CODE"
        sSQL = sSQL & ", JURIS_BENEFIT_ID"
        sSQL = sSQL & ", COMM_TYPE_CODE"
        sSQL = sSQL & ", JURIS_APPVL_CODE"
        sSQL = sSQL & ", REFERENCE"
        GetSQLFieldList = sSQL



    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRecord As CJRCommutation
        Dim sSQL As String
        Try

            LoadData = 0



            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRCommutation
                With objRecord
                    .CommutTypeCode = objReader.GetInt32("COMM_TYPE_CODE")
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .DiscountJurisDefined = objReader.GetInt32("DIS_JURIS_VALUE")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisApprovalReqCode = objReader.GetInt32("JURIS_APPVL_CODE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .JurisBenefitID = objReader.GetInt32("JURIS_BENEFIT_ID")
                    'jtodd22 03/03/2005--.EndDateDTG = objReader.GetString( "EFF_END_DATE")
                    .MaxDiscount = objReader.GetInt32("MAX_DISCOUNT")
                    .MinDiscount = objReader.GetInt32("MIN_DISCOUNT")
                    .Reference = objReader.GetString("REFERENCE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .UseJurisProcedure = objReader.GetInt32("JURIS_PROCED_CODE")
                    Add(.CommutTypeCode, .DeletedFlag, .DiscountJurisDefined, .EffectiveDateDTG, .EndDateDTG, .JurisApprovalReqCode, .JurisBenefitID, .JurisRowID, .MaxDiscount, .MinDiscount, .PermittedCode, .Reference, .TableRowID, .UseJurisProcedure, "k" & CStr(.TableRowID))

                End With
            End While

            LoadData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCommutation
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

