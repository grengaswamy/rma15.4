Option Strict Off
Option Explicit On
Public Class CJRLicense
    Private Const sClassName As String = "CJRLicense"
    'Class properties--local copy
    Private m_TableRowID As Integer
    Private m_JurisRowID As Short
    Private m_DeletedFlag As Short
    Private m_EffectiveDateDTG As String
    Private m_InJurisResidRequidCode As Integer
    Private m_ReciprocalJurisCode As Integer
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0


            sSQL = GetBaseSQLByTableRowID(m_TableRowID)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("RESE_IN_JURIS_CODE").Value = m_InJurisResidRequidCode
                objWriter.Fields("RECIP_JURIS_CODE").Value = m_ReciprocalJurisCode
                objWriter.Execute()
                SafeCloseRecordset(objReader)
            Else
                'new record
                objWriter = DbFactory.GetDbWriter(objReader, False)
                SafeCloseRecordset(objReader)
                m_TableRowID = lGetNextUID("WCP_LICENSE_RULES")
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("RESE_IN_JURIS_CODE").Value = m_InJurisResidRequidCode
                objWriter.Fields("RECIP_JURIS_CODE").Value = m_ReciprocalJurisCode
            End If

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            SaveData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                SaveData = 89999
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0


            ClearObject()
            sSQL = GetBaseSQLByTableRowID(lTableRowID)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                m_InJurisResidRequidCode = objReader.GetInt32("RESE_IN_JURIS_CODE")
                m_ReciprocalJurisCode = objReader.GetInt32("RECIP_JURIS_CODE")
                m_DataHasChanged = False
            End If
            LoadDataByTableRowID = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByTableRowID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Validate() As Integer

        Validate = 0

        m_Warning = ""

        If Len(m_EffectiveDateDTG) Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_JurisRowID < 1 Then
            m_Warning = m_Warning & "A Jurisdiction is required." & vbCrLf
        End If



    End Function
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property InJurisResidRequidCode() As Integer
        Get
            InJurisResidRequidCode = m_InJurisResidRequidCode
        End Get
        Set(ByVal Value As Integer)
            m_InJurisResidRequidCode = Value
        End Set
    End Property
    Public Property ReciprocalJurisCode() As Integer
        Get
            ReciprocalJurisCode = m_ReciprocalJurisCode
        End Get
        Set(ByVal Value As Integer)
            m_ReciprocalJurisCode = Value
        End Set
    End Property
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property


    Public Property Warning() As String
        Get

            Warning = m_Warning

        End Get
        Set(ByVal Value As String)

            m_Warning = Value

        End Set
    End Property
    Private Function GetBaseSQLByTableRowID(ByRef lTableRowID As Integer) As String
        Dim sSQL As String
        GetBaseSQLByTableRowID = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE,JURIS_ROW_ID,DELETED_FLAG,RESE_IN_JURIS_CODE,RECIP_JURIS_CODE"
        sSQL = sSQL & " FROM WCP_LICENSE_RULES"
        sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
        GetBaseSQLByTableRowID = sSQL


    End Function

    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_InJurisResidRequidCode = 0
        m_ReciprocalJurisCode = 0
        m_DataHasChanged = False


    End Function
End Class

