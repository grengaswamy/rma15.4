Option Strict Off
Option Explicit On
Public Class CJREDIStdRecordTypes
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJREDIStdRecordTypes"
    Const sTableName2 As String = "EDI_STDRECTYP_LKUP"
    Const sTableName3 As String = "WCP_STDRECTYP_LKUP"
    'local variable to hold collection
    Private mCol As Collection
    Private mJurisRowID As Integer
    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lPtTableRowID As Integer, ByRef lReleaseRowID As Integer) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJREDIStdRecordType
        Try

            AssignData = 0

            g_sErrDescription = ""
            mJurisRowID = lJurisRowID

            If lReleaseRowID < 1 Then
                '        g_sErrDescription = "Application Error; Release ID must be greater than 0 (zero)."
                '        Err.Raise 700010, sClassName & "." & sFunctionName, g_sErrDescription
                '        Exit Function
            End If

            sSQL = GetFieldSQL()
            If mJurisRowID > 0 Then
                sSQL = sSQL & ", WCP_STDFILING_LKUP.RELEASE_NAME"
                sSQL = sSQL & " FROM " & sTableName3 & ",WCP_STDFILING_LKUP"
                sSQL = sSQL & " WHERE " & sTableName3 & ".PT_TABLE_ROW_ID = WCP_STDFILING_LKUP.TABLE_ROW_ID"
                sSQL = sSQL & " AND " & sTableName3 & ".PT_TABLE_ROW_ID = " & lPtTableRowID
                sSQL = sSQL & " ORDER BY WCP_STDFILING_LKUP.RELEASE_NAME, RECORD_TYPE"
            Else
                sSQL = sSQL & ", EDI_STDFILING_LKUP.RELEASE_NAME"
                sSQL = sSQL & " FROM " & sTableName2 & ",EDI_STDFILING_LKUP"
                sSQL = sSQL & " WHERE " & sTableName2 & ".RELEASE_ROW_ID = EDI_STDFILING_LKUP.TABLE_ROW_ID"
                sSQL = sSQL & " AND " & sTableName2 & ".RELEASE_ROW_ID = " & lReleaseRowID
                sSQL = sSQL & " ORDER BY EDI_STDFILING_LKUP.RELEASE_NAME, RECORD_TYPE"
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CJREDIStdRecordType
                With objRecord
                    .Description = objReader.GetString("RECORD_DESC")
                    If Not IsNothing(objReader.Item("PT_TABLE_ROW_ID")) Then
                        .PtTableRowID = objReader.GetInt32("PT_TABLE_ROW_ID")
                    Else
                        .PtTableRowID = 0
                    End If
                    .RecordTypeID = objReader.GetInt32("RECORD_TYPE_ID")
                    .ReleaseName = objReader.GetString("RELEASE_NAME")
                    If Not IsNothing(objReader.Item("RELEASE_ROW_ID")) Then
                        .ReleaseVersion = objReader.GetInt32("RELEASE_ROW_ID")
                    Else
                        .ReleaseVersion = 0
                    End If
                    .ShortCode = objReader.GetString("RECORD_TYPE")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.TableRowID, .PtTableRowID, .RecordTypeID, .ReleaseName, .ReleaseVersion, .ShortCode, .Description, "k" & CStr(.TableRowID))
                End With

            End While
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If g_sErrDescription = "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Private Function GetFieldSQL() As String
        Dim sSQL As String
        GetFieldSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        If mJurisRowID > 0 Then
            sSQL = sSQL & " " & sTableName3 & ".TABLE_ROW_ID"
            sSQL = sSQL & ", " & sTableName3 & ".ADDED_BY_USER"
            sSQL = sSQL & ", " & sTableName3 & ".DTTM_RCD_ADDED"
            sSQL = sSQL & ", " & sTableName3 & ".UPDATED_BY_USER"
            sSQL = sSQL & ", " & sTableName3 & ".DTTM_RCD_LAST_UPD"
            sSQL = sSQL & ", " & sTableName3 & ".PT_TABLE_ROW_ID"
            sSQL = sSQL & ", " & sTableName3 & ".RECORD_TYPE_ID"
            sSQL = sSQL & ", " & sTableName3 & ".RELEASE_ROW_ID"
        Else
            sSQL = sSQL & " " & sTableName2 & ".TABLE_ROW_ID"
            sSQL = sSQL & ", " & sTableName2 & ".ADDED_BY_USER"
            sSQL = sSQL & ", " & sTableName2 & ".DTTM_RCD_ADDED"
            sSQL = sSQL & ", " & sTableName2 & ".RECORD_TYPE_ID"
            sSQL = sSQL & ", " & sTableName2 & ".UPDATED_BY_USER"
            sSQL = sSQL & ", " & sTableName2 & ".DTTM_RCD_LAST_UPD"
        End If
        sSQL = sSQL & ", RECORD_TYPE"
        sSQL = sSQL & ", RECORD_DESC"
        GetFieldSQL = sSQL


    End Function

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lPtTableRowID As Integer, ByRef lReleaseRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Try

            LoadData = 0

            g_sErrDescription = ""
            mJurisRowID = lJurisRowID

            If lReleaseRowID < 1 Then
                '        g_sErrDescription = "Application Error; Release ID must be greater than 0 (zero)."
                '        Err.Raise 700010, sClassName & "." & sFunctionName, g_sErrDescription
                '        Exit Function
            End If


            LoadData = AssignData(objUser, lJurisRowID, lPtTableRowID, lReleaseRowID)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If g_sErrDescription = "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function LoadDataNoConnInFun(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lPtTableRowID As Integer, ByRef lReleaseRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataNoConnInFun"
        Try

            LoadDataNoConnInFun = 0

            g_sErrDescription = ""
            mJurisRowID = lJurisRowID

            If lReleaseRowID < 1 Then
                '        g_sErrDescription = "Application Error; Release ID must be greater than 0 (zero)."
                '        Err.Raise 700010, sClassName & "." & sFunctionName, g_sErrDescription
                '        Exit Function
            End If

            LoadDataNoConnInFun = AssignData(objUser, lJurisRowID, lPtTableRowID, lReleaseRowID)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If g_sErrDescription = "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            LoadDataNoConnInFun = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function


    Public Function Add(ByRef TableRowID As Integer, ByRef PtTableRowID As Integer, ByRef RecordTypeID As Integer, ByRef ReleaseName As String, ByRef ReleaseVersion As Integer, ByRef ShortCode As String, ByRef Description As String, Optional ByRef sKey As String = "") As CJREDIStdRecordType
        Const sFunctionName As String = "Add"
        Dim objNewMember As CJREDIStdRecordType

        Try

            objNewMember = New CJREDIStdRecordType

            'set the properties passed into the method
            objNewMember.TableRowID = TableRowID
            objNewMember.PtTableRowID = PtTableRowID
            objNewMember.RecordTypeID = RecordTypeID
            objNewMember.ReleaseName = ReleaseName
            objNewMember.ReleaseVersion = ReleaseVersion
            objNewMember.ShortCode = ShortCode
            objNewMember.Description = Description
            objNewMember.ReleaseRowID = ReleaseVersion
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJREDIStdRecordType
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

