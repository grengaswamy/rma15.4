Option Strict Off
Option Explicit On
Imports System.Xml
Public Class CJRPayrollPeriods
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRPayrollPeriods"

    Private mCol As Collection
    Private m_XMLString As String

    Public Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJRPayrollPeriod
        Try

            AssignData = 0


            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " TABLE_ROW_ID"
            'sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
            sSQL = sSQL & ",DELETED_FLAG,SHORT_DESC,LONG_DESC"
            sSQL = sSQL & " FROM WCP_PAYROL_PERID"
            sSQL = sSQL & " WHERE DELETED_FLAG = 0"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CJRPayrollPeriod
                With objRecord
                    .LongDesc = Trim(objReader.GetString("LONG_DESC"))
                    .ShortDesc = Trim(objReader.GetString("SHORT_DESC"))
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    Add(.LongDesc, .ShortDesc, .DeletedFlag, .TableRowID, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_lErrLine = Erl()
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & ", Line " & g_lErrLine & "|"
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function Add(ByRef LongDesc As String, ByRef ShortDesc As String, ByRef DeletedFlag As Short, ByRef TableRowID As Integer, Optional ByRef sKey As String = "") As CJRPayrollPeriod
        'create a new object
        Dim objNewMember As CJRPayrollPeriod
        objNewMember = New CJRPayrollPeriod
        'set the properties passed into the method
        objNewMember.LongDesc = LongDesc
        objNewMember.ShortDesc = ShortDesc
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.TableRowID = TableRowID
        If Len(sKey) = 0 Then
            mCol.Add(objNewMember)
        Else
            mCol.Add(objNewMember, sKey)
        End If
        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing


    End Function

    Public Function FetchDataXMLPlainText(ByRef objUser As Riskmaster.Security.UserLogin) As String
        Const sFunctionName As String = "FetchDataXMLPlainText"
        Dim lIndex As Integer
        Dim lReturn As Integer

        Dim objXML As System.Xml.XmlDocument
        Dim XMLElement As System.Xml.XmlElement
        Dim XMLRoot As System.Xml.XmlElement
        'Dim objXML As New MSXML2.DOMDocument
        'Dim XMLElement As MSXML2.IXMLDOMElement
        'Dim XMLRoot As MSXML2.IXMLDOMElement
        Try

            FetchDataXMLPlainText = ""
            m_XMLString = ""

            lReturn = AssignData(objUser)
            If lReturn <> -1 Then
                Exit Function
            End If

            'create XML
            XMLRoot = objXML.CreateElement("cjrpayrollperiods")
            objXML.AppendChild(XMLRoot)
            If mCol.Count() > 0 Then
                For lIndex = 1 To mCol.Count()
                    XMLElement = objXML.CreateElement("cjrpayrollperiod")
                    'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().DeletedFlag. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    XMLElement.SetAttribute("deletedflag", mCol.Item(lIndex).DeletedFlag)
                    'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().LongDesc. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    XMLElement.SetAttribute("longdesc", mCol.Item(lIndex).LongDesc)
                    'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().ShortDesc. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    XMLElement.SetAttribute("shortdesc", mCol.Item(lIndex).ShortDesc)
                    'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().TableRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    XMLElement.SetAttribute("tablerowid", mCol.Item(lIndex).TableRowID)
                    'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    XMLRoot.AppendChild(XMLElement)
                Next
            End If

            m_XMLString = objXML.InnerXml.ToString()
            FetchDataXMLPlainText = objXML.InnerXml.ToString()

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_lErrLine = Erl()
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & ", Line " & g_lErrLine & "|"
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objXML may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objXML = Nothing

        End Try

    End Function

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "LoadData"
        Try

            LoadData = AssignData(objUser)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            g_lErrLine = Erl()
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & ", Line " & g_lErrLine & "|"
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRPayrollPeriod
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function


    Public Property XMLString() As String
        Get
            XMLString = m_XMLString
        End Get
        Set(ByVal Value As String)
            m_XMLString = Value
        End Set
    End Property
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

