Option Strict Off
Option Explicit On
Public Class CJRPresentValuePD
    Private Const sClassName As String = "CJRPresentValuePD"
    Private Const sTableName As String = "WCP_PRSNT_VALUE_PD"

    Private m_TableRowID As Integer
    Private m_JurisRowID As Short
    Private m_DeletedFlag As Short
    Private m_EffectiveDateDTG As String
    Private m_EndDateDTG As String
    Private m_TimePeriod As Integer
    Private m_PresentValue As Double
    Private m_AnnualInterest As Double
    Private m_FutureValue As Double
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Private Function ClearFields() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_EndDateDTG = ""
        m_TimePeriod = 0
        m_PresentValue = 0
        m_AnnualInterest = 0
        m_FutureValue = 0
        m_DataHasChanged = 0


    End Function
    Private Function GetFieldSQL() As String
        Dim sSQL As String

        GetFieldSQL = ""

        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", TIME_PERIOD, PRESENT_VALUE"
        sSQL = sSQL & ", ENDING_DATE,ANNUAL_INTEREST,FUTURE_VALUE"
        sSQL = sSQL & " FROM WCP_PRSNT_VALUE_PD"

        GetFieldSQL = sSQL


    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property TimePeriod() As Integer
        Get
            TimePeriod = m_TimePeriod
        End Get
        Set(ByVal Value As Integer)
            m_TimePeriod = Value
        End Set
    End Property

    Public Property PresentValue() As Double
        Get
            PresentValue = m_PresentValue
        End Get
        Set(ByVal Value As Double)
            m_PresentValue = Value
        End Set
    End Property

    Public Property AnnualInterest() As Double
        Get
            AnnualInterest = m_AnnualInterest
        End Get
        Set(ByVal Value As Double)
            m_AnnualInterest = Value
        End Set
    End Property

    Public Property FutureValue() As Double
        Get
            FutureValue = m_FutureValue
        End Get
        Set(ByVal Value As Double)
            m_FutureValue = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property EndDateDTG() As String
        Get
            EndDateDTG = m_EndDateDTG
        End Get
        Set(ByVal Value As String)
            m_EndDateDTG = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Dim sNow As String
        Try
            SaveData = 0



            sNow = System.DateTime.Now().ToString("yyyyMMddHHmmss")
            sSQL = GetFieldSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = sNow
                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("ENDING_DATE").Value = m_EndDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("TIME_PERIOD").Value = m_TimePeriod
                objWriter.Fields("PRESENT_VALUE").Value = m_PresentValue
                objWriter.Fields("ANNUAL_INTEREST").Value = m_AnnualInterest
                objWriter.Fields("FUTURE_VALUE").Value = m_FutureValue
                SafeCloseRecordset(objReader)
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", sNow)
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", sNow)
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("ENDING_DATE", m_EndDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("TIME_PERIOD", m_TimePeriod)
                objWriter.Fields.Add("PRESENT_VALUE", m_PresentValue)
                objWriter.Fields.Add("ANNUAL_INTEREST", m_AnnualInterest)
                objWriter.Fields.Add("FUTURE_VALUE", m_FutureValue)
            End If
            objWriter.Execute()
            SaveData = -1


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            SaveData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                SaveData = 89999
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0



            ClearFields()

            sSQL = GetFieldSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                m_EndDateDTG = Trim(objReader.GetString("ENDING_DATE"))
                m_TimePeriod = objReader.GetInt32("TIME_PERIOD")
                m_PresentValue = objReader.GetDouble("PRESENT_VALUE")
                m_AnnualInterest = objReader.GetDouble("ANNUAL_INTEREST")
                m_FutureValue = objReader.GetDouble("FUTURE_VALUE")
                m_DataHasChanged = False
            End If
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""



    End Function
End Class

