Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleDisFig
    Const sClassName As String = "CJRBenefitRuleDisFig"
    Const sTableName As String = "WCP_DFB_RULE"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MinCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MinCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_MinMonthlyRate As Double
    Private m_MinWeeklyRate As Double
    Private m_MonthsToPay As Integer
    Private m_PercentOfAWW As Double
    Private m_UseTTDCode As Integer
    Private m_WeeksToPay As Integer

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()

            '      objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	m_TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '	m_JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '	m_EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '	m_DeletedFlag = objReader.GetInt32( "DELETED_FLAG")
            '	m_PercentOfAWW = objReader.GetDouble( "PERCENT_AWW")
            '	m_UseTTDCode = objReader.GetInt32( "USE_TTD_RULE_CODE")
            '	m_WeeksToPay = objReader.GetInt32( "WEEKS_TO_PAY")

            '	m_UseTwoThirdsCode = objReader.GetInt32( "USE_TWOTHIRDS_CODE")
            '	m_MaxCompRateMonthly = objReader.GetDouble( "MAX_MONTHLY_RATE")
            '	m_MaxCompRateWeekly = objReader.GetDouble( "MAX_WEEKLY_RATE")
            '	m_MinMonthlyRate = objReader.GetDouble( "MIN_MONTHLY_RATE")
            '	m_MinWeeklyRate = objReader.GetDouble( "MIN_WEEKLY_RATE")
            '	m_MonthsToPay = objReader.GetInt32( "MONTHS_TO_PAY")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_PercentOfAWW = objReader.GetDouble("PERCENT_AWW")
                m_UseTTDCode = objReader.GetInt32("USE_TTD_RULE_CODE")
                m_WeeksToPay = objReader.GetInt32("WEEKS_TO_PAY")

                m_UseTwoThirdsCode = objReader.GetInt32("USE_TWOTHIRDS_CODE")
                m_MaxCompRateMonthly = objReader.GetDouble("MAX_MONTHLY_RATE")
                m_MaxCompRateWeekly = objReader.GetDouble("MAX_WEEKLY_RATE")
                m_MinMonthlyRate = objReader.GetDouble("MIN_MONTHLY_RATE")
                m_MinWeeklyRate = objReader.GetDouble("MIN_WEEKLY_RATE")
                m_MonthsToPay = objReader.GetInt32("MONTHS_TO_PAY")
            End If

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_PercentOfAWW = 0
        m_UseTTDCode = 0
        m_WeeksToPay = 0

        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_MinMonthlyRate = 0
        m_MinWeeklyRate = 0
        m_MonthsToPay = 0



    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & "TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,EFFECTIVE_DATE,DELETED_FLAG"
        sSQL = sSQL & ",PERCENT_AWW,USE_TTD_RULE_CODE, WEEKS_TO_PAY"

        sSQL = sSQL & ",USE_TWOTHIRDS_CODE"
        sSQL = sSQL & ",MAX_MONTHLY_RATE"
        sSQL = sSQL & ",MAX_WEEKLY_RATE"
        sSQL = sSQL & ",MIN_MONTHLY_RATE"
        sSQL = sSQL & ",MIN_WEEKLY_RATE"
        sSQL = sSQL & ",MONTHS_TO_PAY"


        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_DFB_RULE"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally

        End Try


    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sFormatted As String
        Dim sSQL As String

        Try
            SaveData = 0



            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = 0
                objWriter.Fields("USE_TTD_RULE_CODE").Value = m_UseTTDCode
                objWriter.Fields("WEEKS_TO_PAY").Value = m_WeeksToPay

                objWriter.Fields("MAX_MONTHLY_RATE").Value = m_MaxCompRateMonthly
                objWriter.Fields("MAX_WEEKLY_RATE").Value = m_MaxCompRateWeekly
                objWriter.Fields("MIN_MONTHLY_RATE").Value = m_MinMonthlyRate
                objWriter.Fields("MIN_WEEKLY_RATE").Value = m_MinWeeklyRate
                objWriter.Fields("MONTHS_TO_PAY").Value = m_MonthsToPay

                sFormatted = m_PercentOfAWW.ToString("0.00")
                If sFormatted = "66.67" Or sFormatted = "66.66" Then
                    objWriter.Fields("PERCENT_AWW").Value = 0
                    objWriter.Fields("USE_TWOTHIRDS_CODE").Value = m_YesCodeID
                Else
                    objWriter.Fields("PERCENT_AWW").Value = m_PercentOfAWW
                    objWriter.Fields("USE_TWOTHIRDS_CODE").Value = m_UseTwoThirdsCode
                End If

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", 0)
                objWriter.Fields.Add("USE_TTD_RULE_CODE", m_UseTTDCode)
                objWriter.Fields.Add("WEEKS_TO_PAY", m_WeeksToPay)

                objWriter.Fields.Add("MAX_MONTHLY_RATE", m_MaxCompRateMonthly)
                objWriter.Fields.Add("MAX_WEEKLY_RATE", m_MaxCompRateWeekly)
                objWriter.Fields.Add("MIN_MONTHLY_RATE", m_MinMonthlyRate)
                objWriter.Fields.Add("MIN_WEEKLY_RATE", m_MinWeeklyRate)
                objWriter.Fields.Add("MONTHS_TO_PAY", m_MonthsToPay)

                sFormatted = m_PercentOfAWW.ToString("0.00")
                If sFormatted = "66.67" Or sFormatted = "66.66" Then
                    objWriter.Fields.Add("PERCENT_AWW", 0)
                    objWriter.Fields.Add("USE_TWOTHIRDS_CODE", m_YesCodeID)
                Else
                    objWriter.Fields.Add("PERCENT_AWW", m_PercentOfAWW)
                    objWriter.Fields.Add("USE_TWOTHIRDS_CODE", m_UseTwoThirdsCode)
                End If

            End If
            objWriter.Execute()


            m_DataHasChanged = False

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function

    Public Function ValidateData() As Integer
        Dim sFormatted As String

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_UseTwoThirdsCode = m_UseTTDCode Then
            m_Warning = m_Warning & "Use Temporary Total Rule and Use 66 2/3% can not be set the same." & vbCrLf
        End If
        sFormatted = m_PercentOfAWW.ToString("#0.00")

        If sFormatted = "66.67" Or sFormatted = "66.66" Then
            m_UseTwoThirdsCode = m_YesCodeID
            m_PercentOfAWW = 0
        End If


    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property PercentOfAWW() As Double
        Get
            PercentOfAWW = m_PercentOfAWW
        End Get
        Set(ByVal Value As Double)
            m_PercentOfAWW = Value
        End Set
    End Property

    Public Property UseTTDCode() As Integer
        Get
            UseTTDCode = m_UseTTDCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTTDCode = Value
        End Set
    End Property

    Public Property WeeksToPay() As Integer
        Get
            WeeksToPay = m_WeeksToPay
        End Get
        Set(ByVal Value As Integer)
            m_WeeksToPay = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property


    Public Property MinMonthlyRate() As Double
        Get
            MinMonthlyRate = m_MinMonthlyRate
        End Get
        Set(ByVal Value As Double)
            m_MinMonthlyRate = Value
        End Set
    End Property


    Public Property MinWeeklyRate() As Double
        Get
            MinWeeklyRate = m_MinWeeklyRate
        End Get
        Set(ByVal Value As Double)
            m_MinWeeklyRate = Value
        End Set
    End Property


    Public Property MonthsToPay() As Integer
        Get
            MonthsToPay = m_MonthsToPay
        End Get
        Set(ByVal Value As Integer)
            m_MonthsToPay = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

